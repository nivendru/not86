//
//  RepeatMyScheduleVC.m
//  Not86
//
//  Created by Interworld on 08/12/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "RepeatMyScheduleVC.h"
#import "CKCalendarView.h"


@interface RepeatMyScheduleVC ()<UITextFieldDelegate, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource, CKCalendarDelegate >
{
    UIImageView *headerImageView;
     AppDelegate *delegate;
    UITableView *table_Repeated;
    UITableView *table_Selected_Date;
    UILabel *repeatLbl;
     NSArray *repeat_Arr;
    UIScrollView *scroll;
    
   NSMutableArray *ary_SelectedDate;
}
@end

@implementation RepeatMyScheduleVC
@synthesize str_ReapetSchedleDate;
@synthesize calendar,enabledDates,str_ServeID, str_Times;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     repeat_Arr = [[NSArray alloc]initWithObjects:@"Don’t repeat (default)",@"Every day this week", @"Every day this month",@"Same day every week",@"Certain dates only", nil];
    ary_SelectedDate = [[NSMutableArray alloc]initWithObjects:@"",@"26 July",@"26 Aug",@"27 Sept",@"29 Oct",@"30 Nov",@"31 Dec", nil];
    
    [self addHeaderView];
    [self IntegrateBodyDesign];
}

-(void)viewWillAppear:(BOOL)animated
{
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addHeaderView
{
    headerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 45)];
    [headerImageView setUserInteractionEnabled:YES];
    headerImageView.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:headerImageView];
    
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [icon_back addTarget:self action:@selector(click_on_back_arrow:) forControlEvents:UIControlEventTouchUpInside];
    [headerImageView   addSubview:icon_back];
    
    
    UILabel  * lbl_Title = [[UILabel alloc]initWithFrame:CGRectMake(50,5, WIDTH-110,35)];
    lbl_Title.text = @"Reapet Schedule";
    lbl_Title.backgroundColor=[UIColor clearColor];
    lbl_Title.textColor=[UIColor whiteColor];
    lbl_Title.textAlignment=NSTextAlignmentLeft;
    lbl_Title.font = [UIFont fontWithName:kFont size:18];
    [headerImageView addSubview:lbl_Title];
    
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 5, 35, 35)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-logo.png"];
    [headerImageView addSubview:img_logo];
    
}


-(void)IntegrateBodyDesign
{
    
    [self.view setBackgroundColor:[UIColor colorWithRed:243/255.0f green:245/255.0f blue:244/255.0f alpha:1.0f]];
    
    UIImageView *img_calender = [[UIImageView alloc]init];
    img_calender.frame = CGRectMake(0,CGRectGetMaxY(headerImageView.frame)+10, WIDTH, 50);
//    [img_calender  setImage:[UIImage imageNamed:@"bg1"]];
    img_calender.backgroundColor = [UIColor whiteColor];
    [img_calender  setUserInteractionEnabled:YES];
    //    img_calender.layer.borderWidth = 1.0;
    [ self.view addSubview:img_calender ];
    
    
    
    UIImageView *servingimges = [[UIImageView alloc]init];
    servingimges.frame =  CGRectMake(25,10, 30, 30);
    //    [servingimges setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_serving_img objectAtIndex:indexPath.row]]]];
    [servingimges setImage:[UIImage imageNamed:@"img_Timehandle.png"]];
    servingimges.backgroundColor =[UIColor clearColor];
    [servingimges setUserInteractionEnabled:YES];
    [img_calender addSubview:servingimges];
    

    UILabel *serving_timeings = [[UILabel alloc]init];
    serving_timeings.frame = CGRectMake(CGRectGetMaxX(servingimges.frame)+10, 10, WIDTH-110, 30);
    serving_timeings.text = str_ReapetSchedleDate;
    serving_timeings .font = [UIFont fontWithName:kFontBold size:13];
    serving_timeings .textColor = [UIColor blackColor];
    serving_timeings .backgroundColor = [UIColor clearColor];
    [img_calender addSubview:serving_timeings ];
    
    
    UIImageView *repeat_BgImg = [[UIImageView alloc]init];
    repeat_BgImg.frame = CGRectMake(0, CGRectGetMaxY(img_calender.frame)+5,WIDTH ,50);
    repeat_BgImg.image = [UIImage imageNamed:@"img_background@2x."];
    repeat_BgImg.userInteractionEnabled = YES;
    //        repeat_BgImg.layer.borderWidth = 1.0;
    [self.view addSubview:repeat_BgImg];
    
    
    UIButton *repeat_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    repeat_Btn.frame = CGRectMake(0, 0,WIDTH ,47);
    [repeat_Btn addTarget:self action:@selector(repeat_BtnMethod:) forControlEvents:UIControlEventTouchUpInside];
    //    repeat_Btn.tag = 1;  CGRectMake(0, CGRectGetMaxY(imgPreview.frame)+5,WIDTH ,47);
    repeat_Btn.backgroundColor = [UIColor clearColor];
    [repeat_BgImg addSubview: repeat_Btn];
    
    
    repeatLbl = [[UILabel alloc]init];
    repeatLbl.frame = CGRectMake(35, 0, WIDTH-100, 47);
    repeatLbl.backgroundColor = [UIColor clearColor];
    
    
    repeatLbl.text = @"Repeat";
    //    repeatLbl.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"]; WIDTH-100
    //    repeatLbl.textAlignment = NSTextAlignmentCenter;
    repeatLbl.font = [UIFont fontWithName:kFontBold size:15];
    repeatLbl.lineBreakMode = NSLineBreakByWordWrapping;
    repeatLbl.numberOfLines = 0;
    [repeat_BgImg addSubview:repeatLbl];
    
    
    UIImageView *dd_BgImg = [[UIImageView alloc]init];
    dd_BgImg.frame = CGRectMake(WIDTH-50, 20, 20 ,10);
    dd_BgImg.backgroundColor = [UIColor clearColor];
    dd_BgImg.image = [UIImage imageNamed:@"drop-down-icon@2x"];
    [repeat_BgImg addSubview:dd_BgImg];

    
   scroll = [[UIScrollView alloc]init];
    scroll.frame=CGRectMake(0, CGRectGetMaxY(repeat_BgImg.frame)+5, WIDTH, HEIGHT-245);
    scroll.backgroundColor = [UIColor colorWithRed:243/255.0f green:245/255.0f blue:244/255.0f alpha:1.0f];
    scroll.bounces=NO;
//            scroll.layer.borderWidth = 1.0;
    scroll.layer.borderColor = [UIColor blueColor].CGColor;
    scroll.showsVerticalScrollIndicator = YES;
    [scroll setScrollEnabled:YES];
    [self.view addSubview:scroll];
    [scroll setContentSize:CGSizeMake(WIDTH, 580)];
    scroll.hidden = YES;
    
    
    
    UIView *calView = [[UIView alloc]init];
    calView.frame=CGRectMake(12, 0, WIDTH-24, 300);
    if (IS_IPHONE_6Plus)
    {
        calView.frame=CGRectMake(12, 0, WIDTH-24, 380);
    }
    else if (IS_IPHONE_6)
    {
        calView.frame=CGRectMake(12, 0, WIDTH-24, 350);
    }
    else if (IS_IPHONE_5)
    {
        calView.frame=CGRectMake(12, 0, WIDTH-24, 310);
    }
    else
    {
        calView.frame=CGRectMake(12, 0, WIDTH-24, 300);
    }
    calView.backgroundColor = [UIColor whiteColor];
    calView.layer.borderWidth=1.0;
    //    [self.view addSubview: notificationView];
    [scroll addSubview: calView];
    
    
    calendar = [[CKCalendarView alloc] initWithStartDay:startSunday];
//   calendar.frame = CGRectMake(0, 0, WIDTH-24, 300);
    if (IS_IPHONE_6Plus)
    {
        calendar.frame = CGRectMake(0, 0, WIDTH-24, 380);
    }
    else if (IS_IPHONE_6)
    {
        calendar.frame = CGRectMake(0, 0, WIDTH-24, 350);
    }
    else if (IS_IPHONE_5)
    {
        calendar.frame = CGRectMake(0, 0, WIDTH-24, 310);
    }
    else
    {
        calendar.frame = CGRectMake(0, 0, WIDTH-24, 300);
    }
    self.calendar = calendar;
    calendar.delegate = self;
    calendar.backgroundColor = [UIColor clearColor];
    calendar.titleColor = [UIColor blackColor];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    //     minimumDate = [dateFormatter dateFromString:dateString];
    //    minimumDate = [dateFormatter dateFromString:@"22/02/1990"];
    calendar.onlyShowCurrentMonth = NO;
    calendar.adaptHeightToNumberOfWeeksInMonth = NO;
    calendar.titleFont = [UIFont fontWithName:kFont size:17.0];
//    calendar.layer.borderWidth = 1.0;
    [calView addSubview:calendar];
    [calendar selectDate:[NSDate date] makeVisible:NO];
    //    ary_AvailableDates = self.enabledDates;
    [calendar reloadData];
    
    
    UILabel  * labl_selDate = [[UILabel alloc]init ];
    labl_selDate.frame=CGRectMake(10,CGRectGetMaxY(calView.frame)+5, WIDTH-20,25);
    labl_selDate.text = @"Date Selected";
    labl_selDate.font = [UIFont fontWithName:kFontBold size:16];
    labl_selDate.backgroundColor=[UIColor clearColor];
    labl_selDate.textAlignment=NSTextAlignmentCenter;
    [scroll addSubview:labl_selDate];
    
    
    UIImageView *imgSelectedDate=[[UIImageView alloc]init];
    imgSelectedDate.frame=CGRectMake( 10, CGRectGetMaxY(labl_selDate.frame)+5, WIDTH-20 , 95 );
    [imgSelectedDate setUserInteractionEnabled:YES];
    imgSelectedDate.image = [UIImage imageNamed:@"img_bg@2x"];
    //    imgSelectedDate.layer.borderWidth = 1.0;
    [scroll addSubview:imgSelectedDate];
    
    table_Selected_Date = [[UITableView alloc]init ];
    table_Selected_Date.frame  = CGRectMake(0,3,WIDTH-20,90);
//    [ table_Selected_Date setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_Selected_Date.delegate = self;
    table_Selected_Date.dataSource = self;
    table_Selected_Date.showsVerticalScrollIndicator = NO;
//    table_Selected_Date.rowHeight = 45.0;
     [table_Selected_Date setSeparatorInset:UIEdgeInsetsZero];
    table_Selected_Date.backgroundColor = [UIColor clearColor];
    [imgSelectedDate addSubview: table_Selected_Date];
    
    
    UIButton   *btn_Save = [[UIButton alloc] init];
    btn_Save.frame = CGRectMake(30, HEIGHT- 60,WIDTH-60, 45);
    btn_Save.layer.cornerRadius=4.0f;
    [btn_Save setTitle:@"SAVE" forState:UIControlStateNormal];
    [btn_Save setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Save setBackgroundColor:[UIColor colorWithRed:39.0/255.0 green:37.0/255.0 blue:48.0/255.0 alpha:1.0f]];
    btn_Save.titleLabel.font=[UIFont fontWithName:kFont size:16];
    [btn_Save setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Save addTarget:self action:@selector(btn_Save_Method:) forControlEvents:UIControlEventTouchUpInside ];
    [self.view addSubview:btn_Save];
    
    
    table_Repeated = [[UITableView alloc]init ];
    table_Repeated.frame  = CGRectMake(30,CGRectGetMaxY(repeat_BgImg.frame),WIDTH-60,150);
//    [ table_Repeated setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_Repeated.delegate = self;
    table_Repeated.dataSource = self;
    table_Repeated.showsVerticalScrollIndicator = NO;
//    table_Repeated.layer.borderWidth = 1.0;
    //    table_Repeated.rowHeight = 30.0;
    [table_Repeated setSeparatorInset:UIEdgeInsetsZero];
    table_Repeated.backgroundColor = [UIColor clearColor];
    [self.view addSubview: table_Repeated];
    table_Repeated.hidden = YES;
    
}

-(void)click_on_back_arrow: (UIButton *)sender
{
    NSLog(@"click_on_back_arrow click");

    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)btn_Save_Method: (UIButton *)sender
{
    
    NSLog(@"btn_Save_Method click");
    [self AFCertainDateSchedule];
}

-(void)repeat_BtnMethod: (UIButton *)sender
{
    
    NSLog(@"repeat_Btn Button Clicked");
    //    table_Repeated.hidden=YES;
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        table_Repeated.hidden=NO;
        
    }
    else
    {
        [sender setSelected:NO];
        table_Repeated.hidden=YES;
}
}

-(void)removeCell_Btn_Method: (UIButton *)sender
{
    NSLog(@"removeCell_Btn_Method click");
   [ary_SelectedDate removeObjectAtIndex:[sender tag]];
}

#pragma mark --CalenderMethod--

- (void)localeDidChange {
    [self.calendar setLocale:[NSLocale currentLocale]];
}

- (BOOL)dateIsDisabled:(NSDate *)date
{
    for (NSDate *disabledDate in self.enabledDates)
    {
        if ([disabledDate isEqualToDate:date])
        {
            return YES;
        }
    }
    return NO;
}


#pragma mark - CKCalendarDelegate

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)myDate
{
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd/MM/yyyy"];
    
    NSString *str_SelectedDate = [format stringFromDate:myDate];
    
}



#pragma mark TableviewDelegate&DataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == table_Selected_Date)
    {
        return [ary_SelectedDate count];
    }
    else if (table_Repeated)
    {
        return [repeat_Arr count];
    }
  return 1;
    
}

-(CGFloat)tableView: (UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table_Selected_Date)
    {
        return 45.0;
    }
    else if (table_Repeated)
    {
        return 30.0;
    }
    return 0.0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    }
    else
    
        for (UIView *view in cell.contentView.subviews)
            [view removeFromSuperview];
    
    
    if (tableView == table_Selected_Date)
    {
        //        cell.textLabel.text = @"Chal Maakhuch";
        
        UIView *cellView = [[UIView alloc]init];
        cellView.frame = CGRectMake(0, 0, WIDTH-20, 45);
        cellView.backgroundColor = [UIColor clearColor];
//        cellView.layer.borderWidth = 1.0;
        [cell.contentView addSubview:cellView];
        
        
        UILabel *titleLbl = [[UILabel alloc]init];
        titleLbl.frame = CGRectMake(10, 0, WIDTH-60, 44);
        titleLbl.backgroundColor = [UIColor clearColor];
        titleLbl.text = [ary_SelectedDate objectAtIndex: indexPath.row];
        titleLbl.textColor = [UIColor blackColor];
        titleLbl.font = [UIFont fontWithName:kFontBold size:15];
        titleLbl.lineBreakMode = NSLineBreakByWordWrapping;
        titleLbl.numberOfLines = 0;
        [cellView addSubview:titleLbl];
        
        
        
        UIButton *removeCell_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
        removeCell_Btn.frame = CGRectMake(WIDTH-45, 15, 16 , 16);
        [removeCell_Btn addTarget:self action:@selector(removeCell_Btn_Method:) forControlEvents:UIControlEventTouchUpInside];
        [removeCell_Btn setBackgroundImage:[UIImage imageNamed:@"cross-img@2x"] forState:UIControlStateNormal];
        removeCell_Btn.tag = indexPath.row;
        [cellView addSubview: removeCell_Btn];
        
//        
//        UIImageView *imgLine=[[UIImageView alloc]init];
//        imgLine.frame=CGRectMake( 10, 44, WIDTH-50 , 0.5 );
//        [imgLine setUserInteractionEnabled:YES];
//        imgLine.image = [UIImage imageNamed:@"line-img@2x"];
//        //    imgLine.layer.borderWidth = 1.0;
//        [cell.contentView addSubview:imgLine];
        
    }

    
    else if (tableView == table_Repeated)
    {
        
        cell.textLabel.text = [repeat_Arr objectAtIndex:indexPath.row];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.font = [UIFont fontWithName:kFont size:13];
        
//        UIImageView *imgLine=[[UIImageView alloc]init];
//        imgLine.frame=CGRectMake( 0, 29, WIDTH-60 , 0.5 );
//        [imgLine setUserInteractionEnabled:YES];
//        imgLine.image = [UIImage imageNamed:@"line-img@2x"];
//        [cell.contentView addSubview:imgLine];
    }
    
    return  cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table_Repeated)
    {
        repeatLbl.text =[repeat_Arr objectAtIndex:indexPath.row];
        [table_Repeated setHidden:YES];
        if ([repeatLbl.text isEqualToString:@"Certain dates only"])
        {
            scroll.hidden = NO;
        }
        else
        {
            scroll.hidden = YES;
        }
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==table_Repeated)
    {
    // Remove seperator inset  [tblDriverList setSeparatorInset:UIEdgeInsetsZero];
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
else if (tableView==table_Selected_Date)
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
    [cell setSeparatorInset:UIEdgeInsetsZero];
    }
                 
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
    [cell setPreservesSuperviewLayoutMargins:NO];
        }
                 
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
    [cell setLayoutMargins:UIEdgeInsetsZero];
      }
    }
}

# pragma mark ScheduleListServices

-(void) AFCertainDateSchedule
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //    NSDictionary *params =@{
    //                            @"user_id"                          :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"UserId"],
    //                            @"type"                             :   @"",
    //                            @"value"                            :   @"",
    //
    //                            };
    
    NSArray* splitString = [str_Times componentsSeparatedByString: @" - "];
    NSString* str1 = [splitString objectAtIndex: 0];
    NSString* str2 = [splitString objectAtIndex: 1];

    NSArray* splitString1 = [str1 componentsSeparatedByString: @" "];
    NSArray* splitString2 = [str2 componentsSeparatedByString: @" "];
    
    NSString* fromTime = [splitString1 objectAtIndex: 0];
    NSString* toTime = [splitString2 objectAtIndex: 0];
    
    
    NSDictionary *params =@{
                            @"uid"                          :  @"2522",
                            @"serve_id"                      : str_ServeID,
                            @"certaindates"                  : @"2015-12-04||2015-12-06",
                            @"from_time"                     : fromTime,
                            @"to_time"                       : toTime
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kAddCertainDatesSchedule
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseCertainDateSchedule:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFCertainDateSchedule];
                                         }
                                     }];
    [operation start];
    
}



-(void) ResponseCertainDateSchedule :(NSDictionary * )TheDict
{
        NSLog(@"Dict CertainDateSchedule is %@", TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        NSLog(@"Added Successfully" );
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        
        NSLog(@"Error message 1");
        
    }
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end


