//
//  RepeatMyScheduleVC.h
//  Not86
//
//  Created by Interworld on 08/12/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CKCalendarView.h"

@interface RepeatMyScheduleVC : UIViewController
@property (nonatomic, strong) NSString *str_ReapetSchedleDate, *str_ServeID, *str_Times;
@property(nonatomic, strong) CKCalendarView *calendar;
@property(nonatomic, strong) NSMutableArray *enabledDates;
@end
