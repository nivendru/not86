//
//  MyScheduleVC.m
//  Not86
//
//  Created by Interwld on 9/1/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "MyScheduleVC.h"
#import "CKCalendarView.h"
#import "AppDelegate.h"
#import "MyScheduleDetailsVC.h"
#import "ChefServeLaterVC.h"
#import "ChefServeNowVC.h"
#import "JWNavigationController.h"
#import "JWSlideMenuController.h"
#import "JWSlideMenuViewController.h"


@interface MyScheduleVC ()
<UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate,UITableViewDataSource,CKCalendarDelegate>
{
    UIImageView *headerImageView;
    UITableView *table_schedule;
    NSMutableArray *array_serving_img;
    NSMutableArray * array_serving_time;
    NSMutableArray * array_serving_items;
    UIScrollView *scrollview_Schedule;
    AppDelegate *delegate;
    
    NSMutableArray *ary_ScheduleList;
    NSMutableArray *ary_ScheduleListSelected;
    MyScheduleDetailsVC *vc;
    
    NSString *str_SelectedDate;
    NSString*str_selectedmaintocompare;
    
    NSDate*date1;
    UIButton *icon_menu ;
    
    
}


@end

@implementation MyScheduleVC
@synthesize calendar,dateFormatter, dateLabel, minimumDate,disabledDates;


#pragma mark - UIView Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    ary_ScheduleList = [NSMutableArray new];
    ary_ScheduleListSelected = [NSMutableArray new];
    str_SelectedDate = [NSString new];
    array_serving_img = [[NSMutableArray alloc]initWithObjects:@"icon-serving@2x.png",@"icon-serving@2x.png",@"icon-serving@2x.png",nil];
    array_serving_time = [[NSMutableArray alloc]initWithObjects:@"Now-12:00PM",@"01:00PM-03:00PM",@"07:00PM-9:00PM", nil];
    array_serving_items =[[NSMutableArray alloc]initWithObjects:@"Chill Chicken,Spicy Nugget,Peac...",@"Chill Chicken,Spicy Nugget,Peac...",@"Chill Chicken,Spicy Nugget,Peac...", nil];
       
    str_selectedmaintocompare = [NSString new];
    date1 = [NSDate new];
    
    
    [self addHeaderView];
    [self IntegrateBodyDesign];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [ary_ScheduleList removeAllObjects];
    [ary_ScheduleListSelected removeAllObjects];
    [table_schedule reloadData];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    
    NSDate *currentDate = [[NSDate alloc] init];
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    dateFormatter1.dateFormat = @"yyyy-MM";
    date1 =currentDate;
    NSString *string = [dateFormatter1 stringFromDate:currentDate];
    str_SelectedDate = string;
    
    NSDateFormatter *dateFormatter2 = [NSDateFormatter new];
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd"];
    NSString *string1 = [dateFormatter2 stringFromDate:currentDate];

    str_selectedmaintocompare = string1;
   
    
    
    if ([_str_comefrom isEqualToString:@"HOME"])
    {
        [self.navigationController.slideMenuController HideHomeButton];
        icon_menu.hidden = NO;
        
    }
    else if ([_str_comefrom isEqualToString:@"NOW"])
    {
        [self.navigationController.slideMenuController UnhideHomeButton];
        icon_menu.hidden = NO;
    }
    else{
        [self.navigationController.slideMenuController UnhideHomeButton];
        icon_menu.hidden = YES;
        
        
    }
    [self AFScheduleList];
}
-(void)viewDidDisappear:(BOOL)animated{
    
     [self.navigationController.slideMenuController UnhideHomeButton];
}

#pragma mark - Add Methods
-(void)addHeaderView
{
    headerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 45)];
    [headerImageView setUserInteractionEnabled:YES];
    headerImageView.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:headerImageView];
    
    
      icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu .frame = CGRectMake(10, 13,25,20);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu  addTarget:self action:@selector(click_on_Back_Btn:)forControlEvents:UIControlEventTouchUpInside];
    [icon_menu setImage:[UIImage imageNamed:@"img_arrow@2x"] forState:UIControlStateNormal];
    //    icon_menu.layer.borderWidth = 1.0;
    [icon_menu setBackgroundColor:[UIColor clearColor]];
    [headerImageView   addSubview:icon_menu ];
    
    UILabel  * lbl_Title = [[UILabel alloc]initWithFrame:CGRectMake(50,5, WIDTH-110,35)];
    lbl_Title.text = @"My Schedule";
    lbl_Title.backgroundColor=[UIColor clearColor];
    lbl_Title.textColor=[UIColor whiteColor];
    lbl_Title.textAlignment=NSTextAlignmentLeft;
    lbl_Title.font = [UIFont fontWithName:kFont size:18];
    [headerImageView addSubview:lbl_Title];
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 5, 35, 35)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-logo.png"];
    [headerImageView addSubview:img_logo];
    
}
-(void)click_on_Back_Btn: (UIButton *)sender

{
    NSLog(@"click_on_Back_Btn Click");
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(void)IntegrateBodyDesign
{
    
    //
    //    UIButton *btn_Signaldish = [[UIButton alloc] init];
    //     btn_Signaldish.frame = CGRectMake(40, CGRectGetMaxY(headerImageView.frame)+10, (WIDTH-80)/2.0,35);
    //    btn_Signaldish.backgroundColor = [UIColor blackColor];
    //    [btn_Signaldish setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [btn_Signaldish setTitle:@"Schedule" forState:UIControlStateNormal];
    //    btn_Signaldish.titleLabel.font = [UIFont fontWithName:kFontBold size:15];
    //    [btn_Signaldish addTarget:self action:@selector(Schedule_btnClick:) forControlEvents:UIControlEventTouchUpInside ];
    //    [self.view addSubview:btn_Signaldish];
    //
    //
    //    UIButton *btn_Meal = [[UIButton alloc] init];
    //    btn_Meal.frame = CGRectMake(CGRectGetMaxX(btn_Signaldish.frame), CGRectGetMaxY(headerImageView.frame)+10, (WIDTH-80)/2.0,35);
    //    btn_Meal.backgroundColor = [UIColor lightGrayColor];
    //    [btn_Meal setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [btn_Meal setTitle:@"On Request" forState:UIControlStateNormal];
    //    btn_Meal.titleLabel.font = [UIFont fontWithName:kFontBold size:15];
    //    [btn_Meal addTarget:self action:@selector(onRequest_btnClick:) forControlEvents:UIControlEventTouchUpInside ];
    //    [self.view addSubview:btn_Meal];
    
    
    UIView *view_schedule = [[UIView alloc]init];
    view_schedule.frame=CGRectMake(0, CGRectGetMaxY(headerImageView.frame)+10, WIDTH, HEIGHT-55);
    view_schedule.backgroundColor=[UIColor clearColor];
    //    view_schedule.layer.borderWidth = 1.0;
    [self.view addSubview:view_schedule];
    
    
    scrollview_Schedule=[[UIScrollView alloc]init];
    scrollview_Schedule.frame=CGRectMake(0, 0, WIDTH, HEIGHT-125);
    [scrollview_Schedule setShowsVerticalScrollIndicator:NO];
    scrollview_Schedule.delegate = self;
    scrollview_Schedule.scrollEnabled = YES;
    scrollview_Schedule.showsVerticalScrollIndicator = NO;
    [scrollview_Schedule setUserInteractionEnabled:YES];
    //    scrollview_Schedule.backgroundColor=[UIColor clearColor];
    scrollview_Schedule.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    //    scrollview_Schedule.layer.borderWidth = 1.0;
    [view_schedule addSubview:scrollview_Schedule];
    if (IS_IPHONE_6Plus){
        [scrollview_Schedule setContentSize:CGSizeMake(WIDTH, 600)];
        
        
    }
    else if (IS_IPHONE_6){
        [scrollview_Schedule setContentSize:CGSizeMake(WIDTH, 565)];
        
    }
    else  {
        [scrollview_Schedule setContentSize:CGSizeMake(WIDTH, 520)];
        
        
    }
    
    
#pragma mark  ---view_for_schedule
    
    
    
    
    UIImageView *img_calender = [[UIImageView alloc]init];
    img_calender.frame = CGRectMake(9,0, WIDTH-18, 310);
    if (IS_IPHONE_6Plus){
        img_calender.frame = CGRectMake(9,0, WIDTH-18, 395);
        
    }
    else if (IS_IPHONE_6){
        img_calender.frame = CGRectMake(9,0, WIDTH-18, 360);
    }
    else  {
        img_calender.frame = CGRectMake(9,0, WIDTH-18, 310);
        
    }
    [img_calender  setImage:[UIImage imageNamed:@"img_backgound.png"]];
    img_calender.backgroundColor = [UIColor whiteColor];
    [img_calender  setUserInteractionEnabled:YES];
    img_calender.layer.borderWidth = 1.0;
    [ scrollview_Schedule addSubview:img_calender ];
    
    
    
    calendar = [[CKCalendarView alloc] initWithStartDay:startSunday];
    calendar.frame = CGRectMake(-3, 0, WIDTH-10, 300);
    if (IS_IPHONE_6Plus)
    {
        calendar.frame = CGRectMake(-3, 0, WIDTH-10, 300);
    }
    else if (IS_IPHONE_6)
    {
        calendar.frame = CGRectMake(-3, 0, WIDTH-10, 300);
    }
    else if (IS_IPHONE_5)
    {
        calendar.frame = CGRectMake(-3, 0, WIDTH-10, 300);
    }
    else
    {
        calendar.frame = CGRectMake(-3, 0, WIDTH-10, 300);
    }
    self.calendar = calendar;
    calendar.delegate = self;
    calendar.backgroundColor = [UIColor clearColor];
    calendar.titleColor = [UIColor blackColor];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    //     minimumDate = [dateFormatter dateFromString:dateString];
    //    minimumDate = [dateFormatter dateFromString:@"22/02/1990"];
    calendar.onlyShowCurrentMonth = NO;
    calendar.adaptHeightToNumberOfWeeksInMonth = NO;
    calendar.titleFont = [UIFont fontWithName:kFont size:17.0];
    //    calendar.layer.borderWidth = 1.0;
    [img_calender addSubview:calendar];
    [calendar selectDate:[NSDate date] makeVisible:NO];
    //    ary_AvailableDates = self.enabledDates;
    [calendar reloadData];
    
    
    
    //    CKCalendarView *calendar = [[CKCalendarView alloc] initWithStartDay:startMonday];
    //    self.calendar = calendar;
    //    calendar.delegate = self;
    //
    //    self.dateFormatter = [[NSDateFormatter alloc] init];
    //    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    //    self.minimumDate = [self.dateFormatter dateFromString:@"20/09/2012"];
    //
    //    self.disabledDates = @[
    //                           [self.dateFormatter dateFromString:@"05/01/2013"],
    //                           [self.dateFormatter dateFromString:@"06/01/2013"],
    //                           [self.dateFormatter dateFromString:@"07/01/2013"]
    //                           ];
    //
    //    calendar.onlyShowCurrentMonth = NO;
    //    calendar.adaptHeightToNumberOfWeeksInMonth = YES;
    //
    //    calendar.frame = CGRectMake(0, 0, WIDTH-20, 300);
    //    [img_calender addSubview:calendar];
    //
    //    //    self.dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(calendar.frame) + 4, self.view.bounds.size.width, 24)];
    //    //    [self.view addSubview:self.dateLabel];
    //
    //    self.view.backgroundColor = [UIColor whiteColor];
    //
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localeDidChange) name:NSCurrentLocaleDidChangeNotification object:nil];
    
    
    
    UIImageView *img_bg3 = [[UIImageView alloc]init];
    img_bg3.frame = CGRectMake(10,CGRectGetMaxY(img_calender.frame)+10, WIDTH-20, 180);
    //    if (IS_IPHONE_6Plus){
    //        img_bg3.frame = CGRectMake(10,CGRectGetMaxY(img_calender.frame)+10, WIDTH-20, 178);
    //
    //    }
    //    else if (IS_IPHONE_6){
    //        img_bg3.frame = CGRectMake(10,CGRectGetMaxY(img_calender.frame)+5, WIDTH-20, 178);
    //    }
    //    else  {
    //        img_bg3.frame = CGRectMake(10,CGRectGetMaxY(img_calender.frame)+10, WIDTH-20, 178);
    //
    //    }
    [img_bg3  setImage:[UIImage imageNamed:@"img_backgound.png"]];
    img_bg3.backgroundColor = [UIColor whiteColor];
    [img_bg3  setUserInteractionEnabled:YES];
    [ scrollview_Schedule addSubview:img_bg3 ];
    
    
    table_schedule = [[UITableView alloc] init ];
    table_schedule.frame  = CGRectMake(0,0,WIDTH-20,180);
    [table_schedule setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_schedule.delegate = self;
    table_schedule.dataSource = self;
    table_schedule.showsVerticalScrollIndicator = NO;
    //    table_schedule.layer.borderWidth = 1.0;
    table_schedule.backgroundColor = [UIColor clearColor];
    [img_bg3 addSubview:table_schedule];
    
    
    UIButton   *btn_Save = [[UIButton alloc] init];
    btn_Save.frame = CGRectMake(50, CGRectGetMaxY(scrollview_Schedule.frame)+12, WIDTH-100, 45);
    btn_Save.layer.cornerRadius=4.0f;
    [btn_Save setTitle:@"Add Schedule" forState:UIControlStateNormal];
    [btn_Save setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Save setBackgroundColor:[UIColor colorWithRed:39.0/255.0 green:37.0/255.0 blue:48.0/255.0 alpha:1.0f]];
    btn_Save.titleLabel.font=[UIFont fontWithName:kFont size:16];
    [btn_Save setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Save addTarget:self action:@selector(addScheduleBtn_Method:) forControlEvents:UIControlEventTouchUpInside ];
    [view_schedule addSubview:btn_Save];
    
    
    
    //    UILabel *lbl_AddSchedule = [[UILabel alloc]init];
    //    if (IS_IPHONE_6Plus){
    //        lbl_AddSchedule.frame = CGRectMake(260,10, 20, 20);
    //
    //    }
    //    else if (IS_IPHONE_6){
    //        lbl_AddSchedule.frame = CGRectMake(40,HEIGHT-65, 300, 55);
    //    }
    //    else  {
    //        lbl_AddSchedule.frame = CGRectMake(260,10, 20, 20);
    //
    //    }
    //
    //
    //    lbl_AddSchedule .text = @"ADD SCHEDULE";
    //    lbl_AddSchedule .font = [UIFont fontWithName:kFont size:18];
    //    lbl_AddSchedule .textColor = [UIColor whiteColor];
    //    lbl_AddSchedule .backgroundColor = [UIColor clearColor];
    //    lbl_AddSchedule.layer.cornerRadius = 4.0f;
    //    lbl_AddSchedule.clipsToBounds = YES;
    //    lbl_AddSchedule.userInteractionEnabled=YES;
    //    lbl_AddSchedule.textAlignment = NSTextAlignmentCenter;
    //    [self.view addSubview:lbl_AddSchedule];
    
    
    //    UIButton *btn_AddSchedule = [UIButton buttonWithType:UIButtonTypeCustom];
    //    if (IS_IPHONE_6Plus){
    //        btn_AddSchedule.frame = CGRectMake(260,10, 20, 20);
    //
    //    }
    //    else if (IS_IPHONE_6){
    //        btn_AddSchedule.frame = CGRectMake(40,HEIGHT-65, 300, 55);
    //    }
    //    else  {
    //        btn_AddSchedule.frame = CGRectMake(260,10, 20, 20);
    //
    //    }
    //
    //    [btn_AddSchedule addTarget:self action:@selector(clickScheduleBtn) forControlEvents:UIControlEventTouchUpInside];
    //    [btn_AddSchedule setImage:[UIImage imageNamed:@"bg_applybtn.png"] forState:UIControlStateNormal];
    //    btn_AddSchedule.userInteractionEnabled=YES;
    //    [self.view  addSubview:btn_AddSchedule];
    
    
    
#pragma mark Tableview
#pragma table_for_schedule
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [ary_ScheduleListSelected count];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    if (tableView == table_schedule)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-20, 60);
        //        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UIImageView *img_line = [[UIImageView alloc]init];
        img_line.frame =  CGRectMake(7,59, WIDTH-35, 0.5);
        img_line.backgroundColor =[UIColor grayColor];
        [img_line setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line];
        
        UIImageView *servingimges = [[UIImageView alloc]init];
        servingimges.frame =  CGRectMake(10,15, 30, 30);
        //        [servingimges setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_serving_img objectAtIndex:indexPath.row]]]];
        [servingimges setImage:[UIImage imageNamed:@"img_Timehandle.png"]];
        servingimges.backgroundColor =[UIColor clearColor];
        [servingimges setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:servingimges];
        
        
        UILabel *serving_timeings = [[UILabel alloc]init];
        serving_timeings.frame = CGRectMake(CGRectGetMaxX(servingimges.frame)+10, 12, WIDTH-90, 15);
        serving_timeings .text = [NSString stringWithFormat:@"%@ - %@",[[ary_ScheduleListSelected objectAtIndex:indexPath.row]valueForKey:@"DisplayFromTime"],[[ary_ScheduleListSelected objectAtIndex:indexPath.row]valueForKey:@"DisplayToTime"] ];
        serving_timeings .font = [UIFont fontWithName:kFontBold size:12];
        serving_timeings .textColor = [UIColor blackColor];
        serving_timeings .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:serving_timeings ];
        
        
        //        UILabel *serving_items = [[UILabel alloc]init];
        //        serving_items .frame = CGRectMake(CGRectGetMaxX(servingimges.frame)+10,CGRectGetMaxY(serving_timeings.frame),WIDTH-90, 20);
        //        serving_items .text = [NSString stringWithFormat:@"%@",[[ary_ScheduleList objectAtIndex:indexPath.row]valueForKey:@"DishName"]];
        //        serving_items .font = [UIFont fontWithName:kFont size:13];
        //        serving_items .textColor = [UIColor blackColor];
        //        serving_items.backgroundColor = [UIColor clearColor];
        //        [img_cellBackGnd addSubview:serving_items ];
        
        UITextView *txtview_adddescription = [[UITextView alloc]init];
        txtview_adddescription.frame = CGRectMake(CGRectGetMaxX(servingimges.frame)+8,CGRectGetMaxY(serving_timeings.frame),WIDTH-90, 25);
        txtview_adddescription.scrollEnabled = YES;
        txtview_adddescription.userInteractionEnabled = NO;
        txtview_adddescription.font = [UIFont fontWithName:kFont size:13];
        txtview_adddescription.backgroundColor = [UIColor clearColor];
        txtview_adddescription.delegate = self;
        txtview_adddescription.textColor = [UIColor blackColor];
        txtview_adddescription.text = [NSString stringWithFormat:@"%@",[[ary_ScheduleListSelected objectAtIndex:indexPath.row]valueForKey:@"DishName"]];
        //        txtview_adddescription.layer.borderWidth = 1.0;
        [img_cellBackGnd addSubview:txtview_adddescription];
        
        
        
        UIButton *icon_right_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
        icon_right_arrow.frame = CGRectMake(WIDTH-50,20, 13, 20);
        [icon_right_arrow addTarget:self action:@selector(icon_right_arrow_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [icon_right_arrow setImage:[UIImage imageNamed:@"arrow_right.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_right_arrow];
        
        
        //        if (IS_IPHONE_6Plus)
        //        {
        //            img_cellBackGnd.frame =  CGRectMake(5,2, WIDTH-20, 50);
        //            img_line.frame =  CGRectMake(7,49, WIDTH-30, 0.5);
        //            servingimges.frame =  CGRectMake(10,10, 30, 30);
        //            serving_timeings .frame = CGRectMake(50,10,WIDTH-10, 10);
        //            serving_items .frame = CGRectMake(50,30,200, 20);
        //            icon_right_arrow.frame = CGRectMake(280,10, 20, 20);
        //
        //        }
        //        else if (IS_IPHONE_6)
        //        {
        //            img_cellBackGnd.frame =  CGRectMake(0,2, WIDTH-25, 180);
        //            img_line.frame =  CGRectMake(15,49, WIDTH-65, 0.5);
        //            servingimges.frame =  CGRectMake(15,5, 30, 30);
        //            serving_timeings .frame = CGRectMake(55,5,250, 10);
        //            serving_items .frame = CGRectMake(55,20,300, 20);
        //            icon_right_arrow.frame = CGRectMake(WIDTH-70,10, 10, 15);
        //
        //        }
        //        else
        //        {
        //            img_cellBackGnd.frame =  CGRectMake(0,2, 320, 50);
        //            img_line.frame =  CGRectMake(7,49, 280, 0.5);
        //            servingimges.frame =  CGRectMake(10,10, 30, 30);
        //            serving_timeings .frame = CGRectMake(50,10,200, 15);
        //            serving_items .frame = CGRectMake(50,30,200, 15);
        //            icon_right_arrow.frame = CGRectMake(260,10, 20, 20);
        //
        //        }
        
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([str_SelectedDate isEqualToString:@""])
    {
        NSLog(@"Select Date First");
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"not86"
                                                         message:@"Select any Date"
                                                        delegate:self
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles: nil];
        //            [alert addButtonWithTitle:@"GOO"];
        [alert show];
        
        
    }
    
    else{
        vc=[[MyScheduleDetailsVC alloc]init];
        vc.str_ServeId = [[ary_ScheduleListSelected objectAtIndex:indexPath.row] valueForKey:@"ServeId"];
        vc.str_ServeDate = str_selectedmaintocompare;
        vc.str_ServeTime = [NSString stringWithFormat:@"%@ - %@",[[ary_ScheduleListSelected objectAtIndex:indexPath.row]valueForKey:@"DisplayFromTime"],[[ary_ScheduleListSelected objectAtIndex:indexPath.row]valueForKey:@"DisplayToTime"] ];
        [self presentViewController:vc animated:NO completion:nil];
    }
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

- (void)localeDidChange {
    [self.calendar setLocale:[NSLocale currentLocale]];
}

- (BOOL)dateIsDisabled:(NSDate *)date {
    for (NSDate *disabledDate in self.disabledDates) {
        if ([disabledDate isEqualToDate:date]) {
            return YES;
        }
    }
    return NO;
}

#pragma mark -
#pragma mark - CKCalendarDelegate

- (void)calendar:(CKCalendarView *)calendar configureDateItem:(CKDateItem *)dateItem forDate:(NSDate *)date {
    // TODO: play with the coloring if we want to...
    
    if ( [[self dateWithOutTime:[NSDate date]] compare:[self dateWithOutTime:date]] == NSOrderedDescending)
    {
        dateItem.backgroundColor = [UIColor lightGrayColor];
        dateItem.textColor = [UIColor whiteColor];
    }
    else {
        for(int i =0; i<[ary_ScheduleList count]; i++){
            if ( [[self dateWithOutTime: [self getDateFromString:[[ary_ScheduleList valueForKey:@"ServeDateTime"] objectAtIndex:i]]] compare:[self dateWithOutTime:date]] == NSOrderedSame)
            {
                dateItem.backgroundColor = [UIColor colorWithRed:189.0/255.0 green:220.0/255.0 blue:234.0/255.0 alpha:1.0];
                dateItem.textColor = [UIColor whiteColor];
            }
        }
    }
    
    NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
    [df1 setDateFormat:@"yyyy-MM-dd"];
    NSDate *dtSelectedDate = [df1 dateFromString:str_selectedmaintocompare];
    
    if([[self dateWithOutTime: dtSelectedDate] compare:[self dateWithOutTime:date]] == NSOrderedSame ){
        dateItem.backgroundColor = [UIColor blueColor];
        dateItem.textColor = [UIColor whiteColor];
    }
}

- (BOOL)calendar:(CKCalendarView *)calendar willSelectDate:(NSDate *)date {
    return ![self dateIsDisabled:date];
}

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date
{
    
    //    NSString *str = [self.dateFormatter stringFromDate:date];
    NSDateFormatter *dateFormatter1 = [NSDateFormatter new];
    [dateFormatter1 setDateFormat:@"yyyy-MM"];
    
    NSDateFormatter *dateFormatter2 = [NSDateFormatter new];
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd"];
    
    str_SelectedDate  = [dateFormatter1 stringFromDate:date];
    
    date1 =date;
    
    
    str_selectedmaintocompare = [dateFormatter2 stringFromDate:date];
    
    [self AFScheduleList];
}

- (BOOL)calendar:(CKCalendarView *)calendar willChangeToMonth:(NSDate *)date {
    
    NSDateFormatter *dateFormatter1 = [NSDateFormatter new];
    [dateFormatter1 setDateFormat:@"yyyy-MM"];
    
    NSDateFormatter *dateFormatter2 = [NSDateFormatter new];
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd"];
    
    str_SelectedDate  = [dateFormatter1 stringFromDate:date];
    
    date1 =date;
    
    
    //str_selectedmaintocompare = [dateFormatter2 stringFromDate:date];
    
    [self AFScheduleList];
    if ([date laterDate:self.minimumDate] == date) {
        //self.calendar.backgroundColor = [UIColor blueColor];
        return YES;
    } else {
        //self.calendar.backgroundColor = [UIColor redColor];
        return NO;
    }
}

- (void)calendar:(CKCalendarView *)calendar didLayoutInRect:(CGRect)frame
{
    NSLog(@"calendar layout: %@", NSStringFromCGRect(frame));
}


-(NSDate *)dateWithOutTime:(NSDate *)datDate {
    if( datDate == nil ) {
        datDate = [NSDate date];
    }
    NSDateComponents* comps = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:datDate];
    return [[NSCalendar currentCalendar] dateFromComponents:comps];
}

-(NSDate *)getDateFromString:(NSString *)pstrDate
{
    NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
    [df1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dtPostDate = [df1 dateFromString:pstrDate];
    return dtPostDate;
}


#pragma mark - ButtonActions

-(void)addScheduleBtn_Method: (UIButton *)sender
{
    NSDate *currentDate = [[NSDate alloc] init];
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    dateFormatter1.dateFormat = @"yyyy-MM-dd";
    NSString *string = [dateFormatter1 stringFromDate:currentDate];
    
    str_SelectedDate = string;

    
    if ( [currentDate compare:date1] == NSOrderedAscending)
    {
        NSLog(@"dfdstgh addScheduleBtn_Method");
        ChefServeLaterVC *vc1 = [ChefServeLaterVC new];
        [self presentViewController:vc1 animated:NO completion:nil];
        
    }
    else if ([currentDate compare:date1] == NSOrderedSame)
    {
        NSLog(@"dfdstgh addScheduleBtn_Method");
        ChefServeNowVC *vc1 = [ChefServeNowVC new];
        [self presentViewController:vc1 animated:NO completion:nil];
        
    }
    else
    {
        ChefServeNowVC *vc1 = [ChefServeNowVC new];
        [self presentViewController:vc1 animated:NO completion:nil];
    }

    
    
    
}


-(void) Schedule_btnClick:(UIButton *) sender
{
    
    
}

-(void) onRequest_btnClick:(UIButton *) sender
{
    
    
}
-(void) icon_right_arrow_BtnClick:(UIButton *) sender
{
    
    
}

# pragma mark ScheduleListServices

-(void) AFScheduleList
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    self.view.userInteractionEnabled = NO;
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //    NSDictionary *params =@{
    //                            @"user_id"                          :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"UserId"],
    //                            @"type"                             :   @"",
    //                            @"value"                            :   @"",
    //
    //                            };  kAddCertainDatesSchedule
    
    NSDictionary *params =@{
                            @"uid"                          :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"date"                         : str_SelectedDate
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kChefScheduleList
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        self.view.userInteractionEnabled = YES;

        [delegate.activityIndicator stopAnimating];
        [self ResponseScheduleList:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         self.view.userInteractionEnabled = YES;

                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFScheduleList];
                                         }
                                     }];
    [operation start];
    
}



-(void) ResponseScheduleList :(NSDictionary * )TheDict
{
    [ary_ScheduleList removeAllObjects];
    [ary_ScheduleListSelected removeAllObjects];
    NSLog(@"Dict is %@", TheDict);
    
    NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
    [df1 setDateFormat:@"yyyy-MM-dd"];
    NSDate *dtSelectedDate = [df1 dateFromString:str_selectedmaintocompare];
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DishDate_Time"] count]; i++)
        {
            
            [ary_ScheduleList addObject:[[TheDict valueForKey:@"DishDate_Time"] objectAtIndex:i]];
            
            if ([[self dateWithOutTime:[self getDateFromString:[[[TheDict valueForKey:@"DishDate_Time"] valueForKey:@"ServeDateTime"] objectAtIndex:i]]] compare:[self dateWithOutTime:dtSelectedDate]] == NSOrderedSame) {
                [ary_ScheduleListSelected addObject:[[TheDict valueForKey:@"DishDate_Time"] objectAtIndex:i]];
            }
            
        }
        
        NSLog(@"Array of Schedule: %@",ary_ScheduleList );
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        
        NSLog(@"Not recive data check service");
        
    }
    [calendar reloadData];
    [table_schedule reloadData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
