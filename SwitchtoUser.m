//
//  SwitchtoUser.m
//  Not86
//
//  Created by Interwld on 9/1/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "SwitchtoUser.h"

@interface SwitchtoUser ()

@end

@implementation SwitchtoUser
-(void)addHeaderView
{
    UIImageView *headerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 45)];
    [headerImageView setUserInteractionEnabled:YES];
    headerImageView.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:headerImageView];
    
    UILabel  * lbl_Title = [[UILabel alloc]initWithFrame:CGRectMake(50,5, 220,30)];
    lbl_Title.text = @"My SwitchUser";
    lbl_Title.backgroundColor=[UIColor clearColor];
    lbl_Title.textColor=[UIColor whiteColor];
    lbl_Title.textAlignment=NSTextAlignmentLeft;
    lbl_Title.font = [UIFont fontWithName:kFont size:18];
    [headerImageView addSubview:lbl_Title];
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 10, 35, 35)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-logo.png"];
    [headerImageView addSubview:img_logo];
}


#pragma mark - UIView Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addHeaderView];
    
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
