//
//  AddDishInChefScheduleVC.m
//  Not86
//
//  Created by Interworld on 07/12/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "AddDishInChefScheduleVC.h"
#import "AppDelegate.h"
#import "ChefServeTypeVC.h"


@interface AddDishInChefScheduleVC ()<UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate,UITableViewDataSource>
{
     UIImageView *headerImageView;
   NSMutableArray * ary_servelistfilter;
   NSMutableArray *  ary_servelist;
    
    AppDelegate *delegate;
    UITableView * tableViewDishes;
    
    NSMutableArray *Arr_Temp;
    int selectedindex;
    NSMutableDictionary *tempDictionary;
    NSIndexPath*indexSelected;
    int search_leg;
    UIImageView *imgViewSerchdish;
    UIImageView *img_Dish;
    UIButton *btn_Search;
    UITextField *textfield_SearchDish;

    UIView *alertviewBg;
    NSDateFormatter *formatter;

}
@end

@implementation AddDishInChefScheduleVC
@synthesize  str_ScheduleDateTime;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    ary_servelist = [NSMutableArray new];
    ary_servelistfilter = [NSMutableArray new];
    Arr_Temp = [NSMutableArray new];
    
    selectedindex = -1;
    indexSelected = nil;
    
    tempDictionary = [[NSMutableDictionary alloc] init];

    
    [self addHeaderView];
    [self IntegrateBodyDesign];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self AFServeNowlist];
}

-(void)addHeaderView
{
    headerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 45)];
    [headerImageView setUserInteractionEnabled:YES];
    headerImageView.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:headerImageView];
    
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [icon_back addTarget:self action:@selector(click_on_back_arrow:) forControlEvents:UIControlEventTouchUpInside];
    [headerImageView   addSubview:icon_back];
    
    
    UILabel  * lbl_Title = [[UILabel alloc]initWithFrame:CGRectMake(50,5, WIDTH-110,35)];
    lbl_Title.text = @"Add Dish";
    lbl_Title.backgroundColor=[UIColor clearColor];
    lbl_Title.textColor=[UIColor whiteColor];
    lbl_Title.textAlignment=NSTextAlignmentLeft;
    lbl_Title.font = [UIFont fontWithName:kFont size:18];
    [headerImageView addSubview:lbl_Title];
    
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 5, 35, 35)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-logo.png"];
    [headerImageView addSubview:img_logo];
    
}


-(void)IntegrateBodyDesign
{
    
    UIImageView *img_calender = [[UIImageView alloc]init];
    img_calender.frame = CGRectMake(7,CGRectGetMaxY(headerImageView.frame)+10, WIDTH-12, 50);
    [img_calender  setImage:[UIImage imageNamed:@"bg1"]];
    //    img_calender.backgroundColor = [UIColor whiteColor];
    [img_calender  setUserInteractionEnabled:YES];
    //    img_calender.layer.borderWidth = 1.0;
    [ self.view addSubview:img_calender ];
    
    
    UIButton *btn_Back_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_Back_arrow.frame = CGRectMake(12,15, 13, 20);
    [btn_Back_arrow addTarget:self action:@selector(click_on_back_arrow:) forControlEvents:UIControlEventTouchUpInside];
    [btn_Back_arrow setImage:[UIImage imageNamed:@"icon-back@2x"] forState:UIControlStateNormal];
    [img_calender   addSubview:btn_Back_arrow];
    
    
    UIImageView *servingimges = [[UIImageView alloc]init];
    servingimges.frame =  CGRectMake(CGRectGetMaxX(btn_Back_arrow.frame)+15,10, 30, 30);
    //    [servingimges setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_serving_img objectAtIndex:indexPath.row]]]];
    [servingimges setImage:[UIImage imageNamed:@"img_Timehandle.png"]];
    servingimges.backgroundColor =[UIColor clearColor];
    [servingimges setUserInteractionEnabled:YES];
    [img_calender addSubview:servingimges];
    
    
    UILabel *serving_timeings = [[UILabel alloc]init];
    serving_timeings.frame = CGRectMake(CGRectGetMaxX(servingimges.frame)+10, 10, WIDTH-110, 30);
    serving_timeings.text = str_ScheduleDateTime;
    serving_timeings .font = [UIFont fontWithName:kFontBold size:13];
    serving_timeings .textColor = [UIColor blackColor];
    serving_timeings .backgroundColor = [UIColor clearColor];
    [img_calender addSubview:serving_timeings ];
    

    UILabel  * labl_Dishes = [[UILabel alloc]init ];
    labl_Dishes.frame=CGRectMake(0,CGRectGetMaxY(img_calender.frame)+5, self.view.frame.size.width,25);
    labl_Dishes.text = @"Select dishes";
    labl_Dishes.textColor=[UIColor darkTextColor];
    labl_Dishes.textAlignment = NSTextAlignmentCenter;
    labl_Dishes.font = [UIFont fontWithName:kFont size:15];
    [self.view addSubview:labl_Dishes];
    
    
    imgViewSerchdish=[[UIImageView alloc]init];
    imgViewSerchdish.frame=CGRectMake(0, CGRectGetMaxY(labl_Dishes.frame)+5,WIDTH+10,50);
    [imgViewSerchdish setUserInteractionEnabled:YES];
    imgViewSerchdish.image=[UIImage imageNamed:@"img_background@2x..png"];
    [self.view addSubview:imgViewSerchdish];
    
    
    img_Dish=[[UIImageView alloc]init];
    img_Dish.frame=CGRectMake(20, 15,32,20);

//    if (IS_IPHONE_6Plus)
//    {
//        img_Dish.frame=CGRectMake(20, 15,35,25);
//    }
//    else if (IS_IPHONE_6)
//    {
//        img_Dish.frame=CGRectMake(20, 15,35,25);
//    }
//    else if (IS_IPHONE_5)
//    {
//        img_Dish.frame=CGRectMake(20, 15,32,20);
//    }
//    else
//    {
//        img_Dish.frame=CGRectMake(20, 13,32,20);
//    }
    [img_Dish setUserInteractionEnabled:YES];
    img_Dish.image = [UIImage imageNamed:@"img_dishplate.png"];
    [imgViewSerchdish addSubview:img_Dish];
    
    
    
    btn_Search = [[UIButton alloc]init];
    btn_Search.frame = CGRectMake(self.view.frame.size.width-55, 15, 20, 20);
//    if (IS_IPHONE_6Plus)
//    {
//        btn_Search.frame = CGRectMake(self.view.frame.size.width-65, 15, 25, 25);
//        
//    }else if (IS_IPHONE_6)
//    {
//        btn_Search.frame = CGRectMake(self.view.frame.size.width-60, 15, 25, 25);
//    }
//    else if (IS_IPHONE_5)
//    {
//        btn_Search.frame = CGRectMake(self.view.frame.size.width-55, 15, 20, 20);
//    }
//    else
//    {
//        btn_Search.frame = CGRectMake(self.view.frame.size.width-55, 13, 20, 20);
//    }
    [btn_Search addTarget:self action:@selector(Search_Method:) forControlEvents:UIControlEventTouchUpInside];
    [btn_Search setBackgroundImage:[UIImage imageNamed:@"img_search.png"] forState:UIControlStateNormal];
    [imgViewSerchdish addSubview:btn_Search];
    
    
    
    textfield_SearchDish = [[UITextField alloc]init];
    textfield_SearchDish.frame=CGRectMake(65, 10, self.view.frame.size.width-140, 30);
//
//    if (IS_IPHONE_6Plus)
//    {
//        textfield_SearchDish.frame=CGRectMake(70, 10, self.view.frame.size.width-150, 35);
//    }
//    else if (IS_IPHONE_6)
//    {
//        textfield_SearchDish.frame=CGRectMake(70, 10, self.view.frame.size.width-150, 35);
//    }
//    else if (IS_IPHONE_5)
//    {
//        textfield_SearchDish.frame=CGRectMake(65, 10, self.view.frame.size.width-140, 30);
//    }
//    else
//    {
//        textfield_SearchDish.frame=CGRectMake(65, 7, self.view.frame.size.width-140, 30);
//    }
    textfield_SearchDish.placeholder = @"Search Dish...";
    textfield_SearchDish.font=[UIFont fontWithName:kFont size:13];
    [imgViewSerchdish addSubview:textfield_SearchDish];
    
    
    
    UIImageView *img_backgroundimage=[[UIImageView alloc]init];
    img_backgroundimage.frame = CGRectMake(10,CGRectGetMaxY(imgViewSerchdish.frame)+10,WIDTH-20,HEIGHT-275);
    [img_backgroundimage setUserInteractionEnabled:YES];
    [img_backgroundimage setContentMode:UIViewContentModeScaleAspectFill];
    img_backgroundimage.clipsToBounds = YES;
    img_backgroundimage.image=[UIImage imageNamed:@"bg1-img@2x"];
    [self.view addSubview:img_backgroundimage];
    
    
//    table_DetailSchedule = [[UITableView alloc] init ];
//    table_DetailSchedule.frame  = CGRectMake(10,CGRectGetMaxY(img_calender.frame)+10,WIDTH-20,HEIGHT-190);
//    [table_DetailSchedule setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//    table_DetailSchedule.delegate = self;
//    table_DetailSchedule.dataSource = self;
//    table_DetailSchedule.showsVerticalScrollIndicator = NO;
//    table_DetailSchedule.layer.borderWidth = 1.0;
//    table_DetailSchedule.backgroundColor = [UIColor clearColor];
//    [self.view addSubview:table_DetailSchedule];
    
    tableViewDishes = [[UITableView alloc]init];
    tableViewDishes.frame =CGRectMake(0,0, WIDTH-20,HEIGHT-275);
    [tableViewDishes setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tableViewDishes.delegate = self;
    tableViewDishes.dataSource = self;
    tableViewDishes.showsVerticalScrollIndicator = NO;
//    tableViewDishes.layer.borderWidth = 1.0;
    [img_backgroundimage addSubview:tableViewDishes];

    
    
    UIButton   *btn_Save = [[UIButton alloc] init];
    btn_Save.frame = CGRectMake(30, CGRectGetMaxY(img_backgroundimage.frame)+18,WIDTH-60, 45);
    btn_Save.layer.cornerRadius=4.0f;
    [btn_Save setTitle:@"NEXT" forState:UIControlStateNormal];
    [btn_Save setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Save setBackgroundColor:[UIColor colorWithRed:39.0/255.0 green:37.0/255.0 blue:48.0/255.0 alpha:1.0f]];
    btn_Save.titleLabel.font=[UIFont fontWithName:kFont size:16];
    [btn_Save setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Save addTarget:self action:@selector(btn_Next_Method:) forControlEvents:UIControlEventTouchUpInside ];
    [self.view addSubview:btn_Save];
    
}

#pragma mark --clickevent

-(void)click_on_back_arrow:(UIButton *)sender
{
    NSLog(@"click_on_back_arrow");
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(void)Search_Method: (UIButton *)sender
{
    NSLog(@"Button Search_Method: Selected ");
    
}

-(void)btn_Next_Method: (UIButton *)sender
{
    if (selectedindex<0)
    {
        [self popup_Alertview:@"plaese select atleast a dish"];
        
    }
    //    else if ([ary_servelistfilter count]>0)
    //    {
    //        NSString*str_quantity;
    //
    //        for (int i=0; i<[ary_servelistfilter count]; i++)
    //        {
    //            if ([[[ary_servelistfilter objectAtIndex:i]valueForKey:@"quantity"] intValue]<=0)
    //            {
    //                str_quantity= @"NO";
    //                [self popup_Alertview:@"Plaese select quantity for selected dishes"];
    //                break;
    //            }
    //            else{
    //                str_quantity= @"YES";
    //            }
    //        }
    //
    //        if ([str_quantity isEqualToString:@"YES"])
    //        {
    //            ChefServeTypeVC *serveTypeVc = [[ChefServeTypeVC alloc]init];
    //            NSMutableArray*ary_mainlisttoshow;
    //            ary_mainlisttoshow = [NSMutableArray new];
    //
    //
    //            for (int i=0; i<[ary_servelistfilter count]; i++)
    //            {
    //                if ([[[ary_servelistfilter objectAtIndex:i]valueForKey:@"quantity"] intValue]>0)
    //                {
    //                    [ary_mainlisttoshow addObject:[ary_servelistfilter objectAtIndex:i]];
    //                }
    //            }
    //
    //
    //            [formatter setDateFormat:@"HH:mm"];
    //            NSString *str=[formatter stringFromDate:datePicker.date];
    //            [txt_EndTime setText:str];
    //
    //            serveTypeVc.ary_servelisting = ary_mainlisttoshow;
    //            serveTypeVc.str_time = txt_EndTime.text;
    //
    //            // [self.navigationController pushViewController:serveTypeVc];
    //
    //            [self presentViewController:serveTypeVc animated:NO completion:nil];
    //
    //        }
    //
    //    }
    else
    {
        ChefServeTypeVC *serveTypeVc = [[ChefServeTypeVC alloc]init];
        NSMutableArray*ary_mainlisttoshow;
        ary_mainlisttoshow = [NSMutableArray new];
        
        
        
        
        for (int i=0; i<[ary_servelistfilter count]; i++)
        {
            if ([[[ary_servelistfilter objectAtIndex:i]valueForKey:@"quantity"] intValue]>0)
            {
                [ary_mainlisttoshow addObject:[ary_servelistfilter objectAtIndex:i]];
            }
            
        }
        
        if([ary_mainlisttoshow count]==0)
        {
            [self popup_Alertview:@"plaese select atleast a quantity of dish"];
            return;
        }
        
        
        
        /*[formatter setDateFormat:@"hh:mm aa"];
        NSString *str=[formatter stringFromDate:datePicker.date];
        [txt_EndTime setText:str];
        
        serveTypeVc.ary_servelisting = ary_mainlisttoshow;
        serveTypeVc.str_time = txt_EndTime.text;*/
        
        
        
        serveTypeVc.ary_servelisting = ary_mainlisttoshow;
        serveTypeVc.str_time = str_ScheduleDateTime;
        
        // [self.navigationController pushViewController:serveTypeVc];
        
        [self presentViewController:serveTypeVc animated:NO completion:nil];
    }
}

-(void)EndTimeMethod: (UIButton *)sender
{
    NSLog(@"Button EndTime Selected");
}

-(void)click_selectObjectAt: (UIButton *)sender
{
    NSLog(@"qwerty  selected");
    //    selectedindex = [sender tag];
    //    if([Arr_Temp containsObject:[arryFoodnames objectAtIndex:sender.tag]])
    //    {
    //        //        [tempDictionary removeObjectForKey:[arryFoodnames objectAtIndex:sender.tag]];
    //        //        [Arr_Temp removeObject:tempDictionary];
    //
    //        [Arr_Temp removeObject:[arryFoodnames objectAtIndex:sender.tag]];
    //    }
    //
    //    else
    //    {
    //        //        [tempDictionary setObject:@"1" forKey:[arryFoodnames objectAtIndex:sender.tag]];
    //        //        [Arr_Temp addObject:tempDictionary];
    //        //        NSLog(@"Array Temp is %@", Arr_Temp);
    //        [Arr_Temp addObject:[arryFoodnames objectAtIndex:sender.tag]];
    //    }
    //
    //    [tableViewDishes reloadData];
    //
    
    NSMutableArray*dummy;
    dummy= [NSMutableArray new];
    
    selectedindex= (int)sender.tag;
    
    if (sender.tag==selectedindex)
    {
        indexSelected = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        
    }
    
    if([Arr_Temp containsObject:[ary_servelistfilter objectAtIndex:sender.tag]])
    {
        [Arr_Temp replaceObjectAtIndex:sender.tag withObject:@""];
        // [Arr_Temp removeObject:[ary_servelistfilter objectAtIndex:sender.tag]];
        //[Arr_Temp removeObjectAtIndex:sender.tag];
        
        NSString* selectedid =[[ary_servelistfilter objectAtIndex:sender.tag]valueForKey:@"DishID"];
        
        for (int i=0; i<[ary_servelistfilter count]; i++)
        {
            if ([selectedid isEqualToString:[[ary_servelistfilter objectAtIndex:i]valueForKey:@"DishID"]])
            {
                //                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                //                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
                //                [dict setValue:@"0"  forKey:@"quantity"];
                //                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
                //                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
                //                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"Description"]]  forKey:@"Description"];
                //                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"ChefCategory"]]  forKey:@"ChefCategory"];
                //                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishCategoryID"]]  forKey:@"DishCategoryID"];
                //                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishCuisineID"]]  forKey:@"DishCuisineID"];
                //                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishPrice"]]  forKey:@"DishPrice"];
                //                [ary_servelist replaceObjectAtIndex:i withObject:dict];
                
                
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
                
                //                [dummy addObject:[[ary_servelist objectAtIndex:i] valueForKey:@"Serving_Type"]];
                //                [dict setValue:dummy forKey:@"Serving_Type"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
                
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
                
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
                [dict setValue:@"0"  forKey:@"quantity"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Delivery_Address"]] forKey:@"Delivery_Address"];
                
                NSLog(@"Crash here");
                [ary_servelistfilter replaceObjectAtIndex:i withObject:dict];
                
            }
        }
        
        [tableViewDishes reloadData];
    }
    
    else
    {
        // new code
        NSLog(@"Crash here 1");
        NSLog(@"%d",sender.tag);
        [Arr_Temp replaceObjectAtIndex:sender.tag withObject:[ary_servelistfilter objectAtIndex:sender.tag]];
    }
    
    [tableViewDishes reloadData];
}

-(void)click_BtnMinusnow: (UIButton *)sender
{
    //    int minimum_Quantity = 1;
    NSLog(@"Button Minus Selected ");
    //    if (minimum_Quantity>1)
    //    {
    //        tableDishesCell *cell;
    //        cell.labl.text = [NSString stringWithFormat:@"%d",[cell.labl.text intValue]-1];
    //        str_newQuantity = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //                [tableViewDishes reloadData];
    //    }
    //    else
    //    {
    //        tableDishesCell *cell;
    //    cell.labl.text = [NSString stringWithFormat:@"%d",[cell.labl.text intValue]];
    //        str_newQuantity = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //    }
    
    //    int i = 1;
    //    if (i > 1)
    //    {
    //        i = i-1;
    //        labl.text = [NSString stringWithFormat:@"%i", i];
    //    }
    //    else
    //    {
    //        labl.text = [NSString stringWithFormat:@"%i", i];
    //    }
    //    selectedindex = [sender tag];
    //    if (minimum_Quantity>1)
    //    {
    //        minimum_Quantity = minimum_Quantity - 1;
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //    }
    //    else
    //    {
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //    }
    
    
    int tag=(int)[[sender superview] tag];
    //    int value=(int)[[[ary_servelist objectAtIndex:tag] valueForKey:@"quantity"] integerValue]-1;
    //    if (value <= 0) value = 1;
    //    [ary_servelist replaceObjectAtIndex:tag withObject:[@(value) stringValue]];
    
    NSMutableArray*dummy;
    dummy = [NSMutableArray new];
    
    
    NSString* selectedid =[[ary_servelistfilter objectAtIndex:tag]valueForKey:@"DishID"];
    
    for (int i=0; i<[ary_servelistfilter count]; i++)
    {
        if ([selectedid isEqualToString:[[ary_servelistfilter objectAtIndex:i]valueForKey:@"DishID"]])
        {
            //            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
            ////            [dummy addObject:[[ary_servelist objectAtIndex:i] valueForKey:@"Serving_Type"]];
            ////            [dict setValue:dummy forKey:@"Serving_Type"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
            //
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
            //
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
            //            NSString *str_facycount=[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"quantity"]];
            //            int int_fancy=[str_facycount integerValue];
            //            int_fancy=int_fancy-1;
            //
            //            if (int_fancy<0)
            //            {
            //                [dict setValue:@"0"  forKey:@"quantity"];
            //
            //            }
            //            else{
            //                [dict setValue:[NSString stringWithFormat:@"%d", int_fancy]  forKey:@"quantity"];
            //
            //            }
            //
            //
            //            [ary_servelist replaceObjectAtIndex:i withObject:dict];
            //
            
            
            
            NSMutableDictionary *dict1=[[NSMutableDictionary alloc] init];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
            //            [dummy addObject:[[ary_servelist objectAtIndex:i] valueForKey:@"Serving_Type"]];
            //            [dict setValue:dummy forKey:@"Serving_Type"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
            NSString *str_facycount1=[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"quantity"]];
            int int_fancy1=[str_facycount1 integerValue];
            int_fancy1=int_fancy1-1;
            
            if (int_fancy1<0)
            {
                [dict1 setValue:@"0"  forKey:@"quantity"];
                
            }
            else{
                [dict1 setValue:[NSString stringWithFormat:@"%d", int_fancy1]  forKey:@"quantity"];
                
            }
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Delivery_Address"]] forKey:@"Delivery_Address"];
            
            [ary_servelistfilter replaceObjectAtIndex:i withObject:dict1];
            
#pragma replacing temp array also to match
            [Arr_Temp replaceObjectAtIndex:i withObject:[ary_servelistfilter objectAtIndex:i]];
            
        }
    }
    
    [tableViewDishes reloadData];
    
}


-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}


-(void)click_BtnPlusnow: (UIButton *)sender
{
    //    int minimum_Quantity = 1;
    NSLog(@"Button Plus Selected");
    //    if (minimum_Quantity<20)
    //    {
    //        tableDishesCell *cell;
    //        cell.labl.text = [NSString stringWithFormat:@"%d",[cell.labl.text intValue]+1];
    //        str_newQuantity = [NSString stringWithFormat:@"%i", minimum_Quantity];
    ////        [tableViewDishes reloadData];
    //    }
    //    else{
    //        tableDishesCell *cell;
    //        cell.labl.text = [NSString stringWithFormat:@"%d",[cell.labl.text intValue]];
    //    str_newQuantity = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //     }
    
    //    int i = 1;
    //    if (i < 20)
    //    {
    //        i = i+1;
    //        labl.text = [NSString stringWithFormat:@"%i", i];
    //    }
    //    else
    //    {
    //        labl.text = [NSString stringWithFormat:@"%i", i];
    //    }
    selectedindex = [sender tag];
    
    //    for (int i = 0; i< [Arr_Temp count]; i++)
    //    {
    //        if (minimum_Quantity<20)
    //        {
    //            minimum_Quantity = minimum_Quantity + 1;
    //            labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //        }
    //        else
    //        {
    //            labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //        }
    //
    //    }
    //    if (minimum_Quantity<20)
    //    {
    //        minimum_Quantity = minimum_Quantity + 1;
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //    }
    //    else
    //    {
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //    }
    
    
    //    NSString *strLikes = @"1";
    //    int likeCount = [strLikes intValue] + 1;
    //    [_marrTest replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
    //
    //    NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:btnClicked.tag-1 inSection:0];
    //
    //    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:selectedIndexPath];
    //
    //    UILabel *requiredLabel = (UILabel*)[cell viewWithTag:1000];
    //
    //    NSString *str = requiredLabel.text;
    //
    //    //str = [str stringByAppendingFormat:@"selected %@", str];
    //    requiredLabel.text = @"";
    //    requiredLabel.text = [NSString stringWithFormat:@"%d likes",[[_marrTest objectAtIndex:btnClicked.tag-1] intValue]];
    //
    //    //do what ever you want with the label
    //
    //    if (minimum_Quantity<20)
    //    {
    //        minimum_Quantity = minimum_Quantity + 1;
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //    }
    //    else
    //    {
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //    }
    
    NSMutableArray*dummy;
    dummy = [NSMutableArray new];
    
    
    int tag=(int)[[sender superview] tag];
    NSString* selectedid =[[ary_servelistfilter objectAtIndex:tag]valueForKey:@"DishID"];
    
    for (int i=0; i<[ary_servelistfilter count]; i++)
    {
        if ([selectedid isEqualToString:[[ary_servelistfilter objectAtIndex:i]valueForKey:@"DishID"]])
        {
            //            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
            //
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"Description"]]  forKey:@"Description"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"ChefCategory"]]  forKey:@"ChefCategory"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishCategoryID"]]  forKey:@"DishCategoryID"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishCuisineID"]]  forKey:@"DishCuisineID"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishPrice"]]  forKey:@"DishPrice"];
            //
            //
            
            
            //            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
            ////            [dummy addObject:[[ary_servelist objectAtIndex:i] valueForKey:@"Serving_Type"]];
            ////            [dict setValue:dummy forKey:@"Serving_Type"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
            //
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
            //
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
            //            NSString *str_facycount=[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"quantity"]];
            //            int int_fancy=[str_facycount integerValue];
            //            int_fancy=int_fancy+1;
            //            [dict setValue:[NSString stringWithFormat:@"%d", int_fancy]  forKey:@"quantity"];
            //
            //
            //
            //
            //            [ary_servelist replaceObjectAtIndex:i withObject:dict];
            
            NSMutableDictionary *dict1=[[NSMutableDictionary alloc] init];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
            //            [dummy addObject:[[ary_servelist objectAtIndex:i] valueForKey:@"Serving_Type"]];
            //            [dict setValue:dummy forKey:@"Serving_Type"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
            NSString *str_facycount1=[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"quantity"]];
            int int_fancy1=[str_facycount1 integerValue];
            int_fancy1=int_fancy1+1;
            [dict1 setValue:[NSString stringWithFormat:@"%d", int_fancy1]  forKey:@"quantity"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Delivery_Address"]] forKey:@"Delivery_Address"];
            [ary_servelistfilter replaceObjectAtIndex:i withObject:dict1];
            
            
#pragma replacing temp array also to match
            
            [Arr_Temp replaceObjectAtIndex:i withObject:[ary_servelistfilter objectAtIndex:i]];
            
            
        }
    }
    
    //    int value=(int)[[[ary_servelist objectAtIndex:tag] valueForKey:@"quantity"] integerValue]+1;
    //    [ary_servelist replaceObjectAtIndex:tag withObject:[@(value) stringValue]];
    [tableViewDishes reloadData];
    
}


#pragma mark TableviewDelegate&DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ary_servelistfilter count];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    }
    
    else
    {
        for (UIView *view in cell.contentView.subviews)
            [view removeFromSuperview];
    }
    //
    //
    //
    //    UILabel *lblTitels = [[UILabel alloc]init];
    //    if (IS_IPHONE_6Plus) {
    //        //        lblTitels.frame=CGRectMake(50, 7, 150, 30);
    //        lblTitels.frame=CGRectMake(50, 12, 150, 30);
    //
    //    }else if (IS_IPHONE_6)
    //
    //    {
    //        lblTitels.frame=CGRectMake(50, 11, 150, 30);
    //
    //    }
    //    else{
    //        lblTitels.frame=CGRectMake(50, 10, 150, 25);
    //
    //    }
    //    lblTitels.text = [arryFoodnames objectAtIndex:indexPath.row];
    //    lblTitels.font=[UIFont fontWithName:kFont size:12];
    //    [cell.contentView addSubview:lblTitels];
    //
    //
    //
    //    UIImageView *image = [[UIImageView alloc]init];
    //    if (IS_IPHONE_6Plus) {
    //        //        image.frame=CGRectMake(10, 5, 30, 30);
    //        image.frame=CGRectMake(10, 12, 30, 30);
    //
    //    }else if (IS_IPHONE_6)
    //
    //    {
    //        image.frame=CGRectMake(10, 11, 30, 30);
    //
    //    }
    //    else{
    //        image.frame=CGRectMake(10, 10, 25, 25);
    //
    //    }
    //    image.image = [UIImage imageNamed:@"img_fork.png"];
    //    [cell.contentView addSubview:image];
    //
    //
    //
    //    UIImageView *imagViewPlusminus  = [[UIImageView alloc]init];
    //
    //    if (IS_IPHONE_6Plus) {
    //            //            imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-170, 10, 120, 25);
    //            imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-170, 12, 120, 30);
    //
    //    }else if (IS_IPHONE_6)
    //
    //    {
    //            imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-170, 11, 120, 30);
    //
    //    }
    //    else{
    //        imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-140, 10, 100, 25);
    //
    //    }
    //    imagViewPlusminus.image = [UIImage imageNamed:@"img_plusminus.png"];
    ////        [cell.contentView addSubview:imagViewPlusminus];
    //
    //
    //        labl = [[UILabel alloc]init];
    //        if (IS_IPHONE_6Plus) {
    //            //            labl.frame=CGRectMake(self.view.frame.size.width-150, 10, 80, 25);
    //            labl.frame=CGRectMake(self.view.frame.size.width-150, 12, 80, 30);
    //
    //        }else if (IS_IPHONE_6)
    //
    //        {
    //            labl.frame=CGRectMake(self.view.frame.size.width-150, 11, 80, 30);
    //
    //        }
    //        else{
    //            labl.frame=CGRectMake(self.view.frame.size.width-130, 10, 80, 25);
    //
    //        }
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //        labl.textAlignment = NSTextAlignmentCenter;
    //        //  labl.layer.borderWidth = 2.0f;
    //        labl.tag = indexPath.row;
    ////         [cell.contentView addSubview:labl];
    //
    //
    //        UIButton *btnMinus = [[UIButton alloc]init];
    //        if (IS_IPHONE_6Plus) {
    //            //            btnMinus.frame=CGRectMake(self.view.frame.size.width-150, 10, 25, 25);
    //            btnMinus.frame=CGRectMake(self.view.frame.size.width-170, 12, 30, 30);
    //
    //        }
    //        else if (IS_IPHONE_6)
    //        {
    //            btnMinus.frame=CGRectMake(self.view.frame.size.width-170, 11, 30, 30);
    //
    //        }
    //        else{
    //            btnMinus.frame=CGRectMake(self.view.frame.size.width-140, 10, 25, 25);
    //
    //        }
    //        btnMinus.tag = indexPath.row;
    //        //        btnMinus.backgroundColor = [UIColor redColor];
    //        [btnMinus addTarget:self action:@selector(click_BtnMinus:) forControlEvents:UIControlEventTouchUpInside];
    ////        [cell.contentView addSubview:btnMinus];
    //
    //
    //        UIButton *btnPlus = [[UIButton alloc]init];
    //        //     btnPlus.frame=CGRectMake(self.view.frame.size.width-50, 10, 25, 25);
    //        if (IS_IPHONE_6Plus) {
    //            //            btnPlus.frame=CGRectMake(self.view.frame.size.width-50, 10, 25, 25);
    //            btnPlus.frame=CGRectMake(self.view.frame.size.width-80, 12, 30, 30);
    //
    //        }
    //        else if (IS_IPHONE_6)
    //        {
    //            btnPlus.frame=CGRectMake(self.view.frame.size.width-80, 11, 30, 30);
    //
    //        }
    //        else
    //        {
    //            btnPlus.frame=CGRectMake(self.view.frame.size.width-65, 10, 25, 25);
    //
    //        }
    //        btnPlus.tag = indexPath.row;
    //        //        btnPlus.backgroundColor = [UIColor redColor];
    //        [btnPlus addTarget:self action:@selector(click_BtnPlus:) forControlEvents:UIControlEventTouchUpInside];
    ////        [cell.contentView addSubview:btnPlus];
    //
    //
    //
    //    UIImageView *img_lineimage=[[UIImageView alloc]init];
    //    img_lineimage.frame=CGRectMake(10, 49, WIDTH-50, 0.5);
    //    if (IS_IPHONE_6Plus) {
    //        //        img_lineimage.frame=CGRectMake(10, 49, WIDTH-40, 0.5);
    //        img_lineimage.frame=CGRectMake(10, 55, WIDTH-60, 0.5);
    //
    //    }else if (IS_IPHONE_6)
    //
    //    {
    //        img_lineimage.frame=CGRectMake(10, 51, WIDTH-50, 0.5);
    //
    //    }
    //    else{
    //        img_lineimage.frame=CGRectMake(10, 44, WIDTH-40, 0.5);
    //
    //    }
    //    [img_lineimage setUserInteractionEnabled:YES];
    //    img_lineimage.backgroundColor=[UIColor clearColor];
    //    img_lineimage.image=[UIImage imageNamed:@"line1.png"];
    //    [cell.contentView addSubview:img_lineimage];
    //
    //
    //
    //    UIButton *btn_TableCell_CheckBox1 = [[UIButton alloc]init];
    //    btn_TableCell_CheckBox1.frame=CGRectMake(10,2,15,15);
    //    if (IS_IPHONE_6Plus)
    //    {
    //        //        img_lineimage.frame=CGRectMake(10, 49, WIDTH-40, 0.5);
    //         btn_TableCell_CheckBox1.frame=CGRectMake(0, 0, tableViewDishes.frame.size.width, 55);
    //
    //    }
    //    else if (IS_IPHONE_6)
    //    {
    //        btn_TableCell_CheckBox1.frame=CGRectMake(0, 0, tableViewDishes.frame.size.width, 51);
    //    }
    //    else
    //    {
    //         btn_TableCell_CheckBox1.frame=CGRectMake(0, 0, tableViewDishes.frame.size.width, 44);
    //    }
    //    btn_TableCell_CheckBox1.backgroundColor=[UIColor clearColor];
    ////    [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img.checkcorrect@2x.png"] forState:UIControlStateSelected];
    ////    [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
    //    [btn_TableCell_CheckBox1 addTarget:self action:@selector(click_selectObjectAt:) forControlEvents:UIControlEventTouchUpInside];
    //    btn_TableCell_CheckBox1.tag = indexPath.row;
    //    [cell.contentView addSubview:btn_TableCell_CheckBox1];
    //
    //
    //    if([Arr_Temp  containsObject:[arryFoodnames objectAtIndex:indexPath.row]])
    //    {
    //        [btn_TableCell_CheckBox1 setSelected:YES];
    //        image.image = [UIImage imageNamed:@"img_forkblue"];
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //        [cell.contentView addSubview:imagViewPlusminus];
    //        [cell.contentView addSubview:labl];
    //        [cell.contentView addSubview:btnMinus];
    //        [cell.contentView addSubview:btnPlus];
    //    }
    //    else{
    //        [btn_TableCell_CheckBox1 setSelected:NO];
    //    }
    //
    //
    //    return cell;
    
    
    //
    //    NSString *kReuseIndentifier = [NSString stringWithFormat:@"Cell-%d",indexPath.row];
    //
    //    UITableViewCell *cell;
    //    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    //
    //    if (cell == nil) {
    //        //        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
    //        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
    //    }
    //    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    //        else
    //        {
    //            for (UIView *view in cell.contentView.subviews)
    //                [view removeFromSuperview];
    //        }
    //
    
    
    
    
    UIImageView *image = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus) {
        //        image.frame=CGRectMake(10, 5, 30, 30);
        image.frame=CGRectMake(10, 12, 30, 30);
        
    }else if (IS_IPHONE_6)
        
    {
        image.frame=CGRectMake(10, 11, 30, 30);
        
    }
    else{
        image.frame=CGRectMake(10, 10, 25, 25);
        
    }
    
    image.image = [UIImage imageNamed:@"img_fork.png"];
    [cell.contentView addSubview:image];
    
    
    UILabel *lblTitels = [[UILabel alloc]init];
    if (IS_IPHONE_6Plus) {
        //        lblTitels.frame=CGRectMake(50, 7, 150, 30);
        lblTitels.frame=CGRectMake(50, 12, 150, 30);
        
    }else if (IS_IPHONE_6)
        
    {
        lblTitels.frame=CGRectMake(50, 11, 150, 30);
        
    }
    else{
        lblTitels.frame=CGRectMake(50, 10, 130, 25);
        
    }
    lblTitels.text = [[ary_servelistfilter objectAtIndex:indexPath.row] valueForKey:@"DishName"];
    lblTitels.font=[UIFont fontWithName:kFont size:12];
    [cell.contentView addSubview:lblTitels];
    
    
    
    
    UIImageView *imagViewPlusminus  = [[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus) {
        //            imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-170, 10, 120, 25);
        imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-170, 12, 120, 30);
        
    }else if (IS_IPHONE_6)
        
    {
        imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-170, 11, 120, 30);
        
    }
    else{
        imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-140, 10, 100, 25);
        
    }
    imagViewPlusminus.image = [UIImage imageNamed:@"img_plusminus.png"];
    [cell.contentView addSubview:imagViewPlusminus];
    
    
    
    
    UILabel *labl = [[UILabel alloc]init];
    if (IS_IPHONE_6Plus) {
        //            labl.frame=CGRectMake(self.view.frame.size.width-150, 10, 80, 25);
        labl.frame=CGRectMake(self.view.frame.size.width-150, 12, 80, 30);
        
    }else if (IS_IPHONE_6)
        
    {
        labl.frame=CGRectMake(self.view.frame.size.width-150, 11, 80, 30);
        
    }
    else{
        labl.frame=CGRectMake(self.view.frame.size.width-130, 10, 80, 25);
        
    }
    
    labl.textAlignment = NSTextAlignmentCenter;
    labl.text = [[ary_servelistfilter objectAtIndex:indexPath.row] valueForKey:@"quantity"];
    
    [cell.contentView addSubview:labl];
    
    UIButton *btn_TableCell_CheckBox1 = [[UIButton alloc]init];
    btn_TableCell_CheckBox1.frame=CGRectMake(10,2,15,15);
    if (IS_IPHONE_6Plus)
    {
        //        img_lineimage.frame=CGRectMake(10, 49, WIDTH-40, 0.5);
        btn_TableCell_CheckBox1.frame=CGRectMake(0, 0, tableViewDishes.frame.size.width, 55);
        
    }
    else if (IS_IPHONE_6)
    {
        btn_TableCell_CheckBox1.frame=CGRectMake(0, 0, tableViewDishes.frame.size.width, 51);
    }
    else
    {
        btn_TableCell_CheckBox1.frame=CGRectMake(0, 0, tableViewDishes.frame.size.width, 44);
    }
    btn_TableCell_CheckBox1.backgroundColor=[UIColor clearColor];
    //    [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img.checkcorrect@2x.png"] forState:UIControlStateSelected];
    //    [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
    [btn_TableCell_CheckBox1 addTarget:self action:@selector(click_selectObjectAt:) forControlEvents:UIControlEventTouchUpInside];
    btn_TableCell_CheckBox1.tag = indexPath.row;
    [cell.contentView addSubview:btn_TableCell_CheckBox1];
    
    UIButton *btnMinus;
    UIButton *btnPlus;
    
    if ([cell.contentView viewWithTag:100] == nil)
    {
        btnMinus =[UIButton buttonWithType:UIButtonTypeCustom];
        
        if (IS_IPHONE_6Plus) {
            btnMinus.frame=CGRectMake(self.view.frame.size.width-180, 0, cell.contentView.frame.size.height+10, cell.contentView.frame.size.height);
            
        }
        else if (IS_IPHONE_6)
        {
            btnMinus.frame=CGRectMake(self.view.frame.size.width-180, 0, cell.contentView.frame.size.height+10, cell.contentView.frame.size.height);
            
        }
        else{
            btnMinus.frame=CGRectMake(self.view.frame.size.width-150, 0, cell.contentView.frame.size.height+10, cell.contentView.frame.size.height);
            
        }
        //    btnMinus.tag = indexPath.row;
        btnMinus.backgroundColor = [UIColor clearColor];
        [btnMinus addTarget:self action:@selector(click_BtnMinusnow:) forControlEvents:UIControlEventTouchUpInside];
        btnMinus.tag=100;
        btnMinus.layer.borderColor= [[UIColor blackColor]CGColor];
        //btnMinus.layer.borderWidth= 1.0f;
        btnMinus.clipsToBounds = YES;
        [cell.contentView addSubview:btnMinus];
    }
    
    if ([cell.contentView viewWithTag:101] == nil) {
        
        btnPlus = [UIButton buttonWithType:UIButtonTypeCustom];
        if (IS_IPHONE_6Plus) {
            //            btnPlus.frame=CGRectMake(self.view.frame.size.width-50, 10, 25, 25);
            btnPlus.frame=CGRectMake(self.view.frame.size.width-90, 0, cell.contentView.frame.size.height+10, cell.contentView.frame.size.height);
            
        }
        else if (IS_IPHONE_6)
        {
            btnPlus.frame=CGRectMake(self.view.frame.size.width-90, 0, cell.contentView.frame.size.height+10, cell.contentView.frame.size.height);
            
        }
        else
        {
            btnPlus.frame=CGRectMake(self.view.frame.size.width-75, 0, cell.contentView.frame.size.height+10, cell.contentView.frame.size.height);
            
        }
        btnPlus.tag = indexPath.row;
        btnPlus.backgroundColor = [UIColor clearColor];
        [btnPlus addTarget:self action:@selector(click_BtnPlusnow:) forControlEvents:UIControlEventTouchUpInside];
        btnPlus.tag=101;
        btnPlus.layer.borderColor= [[UIColor blackColor]CGColor];
        // btnPlus.layer.borderWidth= 1.0f;
        btnPlus.clipsToBounds = YES;
        [cell.contentView addSubview:btnPlus];
    }
    
    
    //    if (indexPath.row==selectedindex)
    //    {
    //        image.image = [UIImage imageNamed:@"img_forkblue.png"];
    //        btnPlus.hidden = NO;
    //        btnMinus.hidden= NO;
    //        labl.hidden= NO;
    //
    //
    //    }
    //    else{
    //        image.image = [UIImage imageNamed:@"img_fork.png"];
    //        btnPlus.hidden = YES;
    //        btnMinus.hidden= YES;
    //        labl.hidden= YES;
    //
    //
    //    }
    
    if([Arr_Temp containsObject:[ary_servelistfilter objectAtIndex:indexPath.row]])
    {
        imagViewPlusminus.hidden = NO;
        
        image.image = [UIImage imageNamed:@"img_forkblue.png"];
        btnPlus.hidden = NO;
        btnMinus.hidden= NO;
        labl.hidden= NO;
        
        
    }
    else{
        image.image = [UIImage imageNamed:@"img_fork.png"];
        btnPlus.hidden = YES;
        btnMinus.hidden= YES;
        labl.hidden= YES;
        
        imagViewPlusminus.hidden = YES;
        
    }
    
    
    //    //    UIButton *btnPlus = [[UIButton alloc]init];
    //    UIButton *btnPlus = nil;
    //    //     btnPlus.frame=CGRectMake(self.view.frame.size.width-50, 10, 25, 25);
    //    if (IS_IPHONE_6Plus) {
    //        //            btnPlus.frame=CGRectMake(self.view.frame.size.width-50, 10, 25, 25);
    //        btnPlus.frame=CGRectMake(self.view.frame.size.width-80, 12, 30, 30);
    //
    //    }
    //    else if (IS_IPHONE_6)
    //    {
    //        btnPlus.frame=CGRectMake(self.view.frame.size.width-80, 11, 30, 30);
    //
    //    }
    //    else
    //    {
    //        btnPlus.frame=CGRectMake(self.view.frame.size.width-65, 10, 25, 25);
    //
    //    }
    //    btnPlus.tag = indexPath.row;
    //    //        btnPlus.backgroundColor = [UIColor redColor];
    //    [btnPlus addTarget:self action:@selector(click_BtnPlus:) forControlEvents:UIControlEventTouchUpInside];
    //    //        [cell.contentView addSubview:btnPlus];
    
    
    
    UIImageView *img_lineimage=[[UIImageView alloc]init];
    img_lineimage.frame=CGRectMake(10, 49, WIDTH-50, 0.5);
    if (IS_IPHONE_6Plus) {
        //        img_lineimage.frame=CGRectMake(10, 49, WIDTH-40, 0.5);
        img_lineimage.frame=CGRectMake(10, 55, WIDTH-60, 0.5);
        
    }else if (IS_IPHONE_6)
        
    {
        img_lineimage.frame=CGRectMake(10, 51, WIDTH-50, 0.5);
        
    }
    else{
        img_lineimage.frame=CGRectMake(10, 44, WIDTH-40, 0.5);
        
    }
    [img_lineimage setUserInteractionEnabled:YES];
    img_lineimage.backgroundColor=[UIColor clearColor];
    img_lineimage.image=[UIImage imageNamed:@"line1.png"];
    [cell.contentView addSubview:img_lineimage];
    
    
    
    
    
    cell.contentView.tag = indexPath.row;
    
    
    //    if([Arr_Temp  containsObject:[arryFoodnames objectAtIndex:indexPath.row]])
    //    {
    //        [btn_TableCell_CheckBox1 setSelected:YES];
    //        image.image = [UIImage imageNamed:@"img_forkblue"];
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //        [cell.contentView addSubview:imagViewPlusminus];
    //        [cell.contentView addSubview:labl];
    //        [cell.contentView addSubview:btnMinus];
    //        [cell.contentView addSubview:btnPlus];
    //    }
    //    else{
    //        [btn_TableCell_CheckBox1 setSelected:NO];
    //    }
    //
    //
    //
    //    if ([cell viewWithTag:lbltag])
    //    {
    //        labl = (UILabel *)[cell viewWithTag:lbltag];
    //
    //    }
    //    else
    //    {
    //        UILabel *labl = [[UILabel alloc]init];
    //        if (IS_IPHONE_6Plus) {
    //            //            labl.frame=CGRectMake(self.view.frame.size.width-150, 10, 80, 25);
    //            labl.frame=CGRectMake(self.view.frame.size.width-150, 12, 80, 30);
    //
    //        }else if (IS_IPHONE_6)
    //
    //        {
    //            labl.frame=CGRectMake(self.view.frame.size.width-150, 11, 80, 30);
    //
    //        }
    //        else{
    //            labl.frame=CGRectMake(self.view.frame.size.width-130, 10, 80, 25);
    //
    //        }
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //        //labl.layer.borderWidth = 4.0;
    //        labl.textAlignment = NSTextAlignmentCenter;
    //        [cell.contentView addSubview:labl];
    //
    //    }
    //
    //    if ([cell viewWithTag:indexPath.row+1])
    //    {
    //        btnPlus = (UIButton*)[cell viewWithTag:indexPath.row+1];
    //    }
    //    else
    //    {
    //        btnPlus=[UIButton buttonWithType:UIButtonTypeCustom];
    //        btnPlus.tag = indexPath.row+1;
    //        if (IS_IPHONE_6Plus) {
    //            //            btnPlus.frame=CGRectMake(self.view.frame.size.width-50, 10, 25, 25);
    //            btnPlus.frame=CGRectMake(self.view.frame.size.width-80, 12, 30, 30);
    //
    //        }
    //        else if (IS_IPHONE_6)
    //        {
    //            btnPlus.frame=CGRectMake(self.view.frame.size.width-80, 11, 30, 30);
    //
    //        }
    //        else
    //        {
    //            btnPlus.frame=CGRectMake(self.view.frame.size.width-65, 10, 25, 25);
    //
    //        }
    //        //        btnPlus.tag = indexPath.row;
    //        //        btnPlus.backgroundColor = [UIColor redColor];
    //        [btnPlus addTarget:self action:@selector(click_BtnPlus:) forControlEvents:UIControlEventTouchUpInside];
    //        //btnPlus.layer.borderWidth = 4.0;
    //        [cell addSubview:btnPlus];
    //
    //    }
    
    return cell;
    
    
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    selectedindex= (int)indexPath.row;
    
    if (indexPath.row==selectedindex)
    {
        indexSelected = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
        
    }
    

}




-(void)AFServeNowlist
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    NSDictionary *params =@{
                            
                            @"uid"                              :   [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:Kservenowlist
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseServeNowlist:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFServeNowlist];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseServeNowlist :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    [ary_servelist removeAllObjects];
    [ary_servelistfilter removeAllObjects];
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DishsList"] count]; i++)
        {
            
            // [ary_dummy addObject:[[TheDict valueForKey:@"DishsList"] objectAtIndex:i]];
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
            
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
            
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
            [dict setValue:@"0"  forKey:@"quantity"];
            [ary_servelist addObject:dict];
            
            
            NSMutableDictionary *dict1=[[NSMutableDictionary alloc] init];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Delivery_Address"]] forKey:@"Delivery_Address"];
            
            [dict1 setValue:@"0"  forKey:@"quantity"];
            [ary_servelistfilter addObject:dict1];
            
            
            
            
            if ([ary_servelistfilter count]>5)
            {
                imgViewSerchdish.hidden= NO;
                img_Dish.hidden = NO;
                btn_Search.hidden = NO;
                textfield_SearchDish.hidden= NO;
            }
            
            
            
        }
        
        // new code
        Arr_Temp=[[NSMutableArray alloc] init];
        for(int i=0;i<ary_servelistfilter.count;i++)
        {
            [Arr_Temp addObject:@""];
        }

        
        [tableViewDishes reloadData];
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
    
    
}

-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
