//
//  MyMenuVC.m
//  Not86
//
//  Created by Interwld on 9/1/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "MyMenuVC.h"
#import "AppDelegate.h"
#import "JWNavigationController.h"
#import "JWSlideMenuController.h"
#import "JWSlideMenuViewController.h"

@interface MyMenuVC ()<UIScrollViewDelegate,UITextFieldDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
{
    UIScrollView *scrollviewMenu;
    UITextField *txt_cheftype;
    UITextField *txt_others;
    NSMutableArray *collectionarrImages;
    NSMutableArray *collectionarrImagesDish;
    UICollectionViewFlowLayout *layout;
    UICollectionView *collView_serviceDirectory;
    NSData*imgData;
    UICollectionViewFlowLayout *layout2;
    UICollectionView *collView_serviceDirectory2;
    UIView *view_Meal;
    UIView *view_SignalDish;
    UIButton *btn_Signaldish;
    UIButton *btn_Meal;
    AppDelegate *delegate;
    UITextField *txt_MealTitle;
    UITextView * txt_view;
    UIButton *btn_imgupload ;
    NSString *str_pickingImage;
    UIImageView *img_backround;
    
}


@end

@implementation MyMenuVC

-(void)addHeaderView
{
    UIImageView *headerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 45)];
    [headerImageView setUserInteractionEnabled:YES];
    headerImageView.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:headerImageView];
    
    UILabel  * lbl_Title = [[UILabel alloc]initWithFrame:CGRectMake(50,5, 220,30)];
    lbl_Title.text = @"My Menu";
    lbl_Title.backgroundColor=[UIColor clearColor];
    lbl_Title.textColor=[UIColor whiteColor];
    lbl_Title.textAlignment=NSTextAlignmentLeft;
    lbl_Title.font = [UIFont fontWithName:kFont size:18];
    [headerImageView addSubview:lbl_Title];
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 10, 35, 35)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-logo.png"];
    [headerImageView addSubview:img_logo];
}


#pragma mark - UIView Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addHeaderView];
    [self IntegrateBodyDesign];
    view_SignalDish.hidden=YES;
    str_pickingImage = [NSString new];
    collectionarrImages=[NSMutableArray arrayWithObjects:@"img_halal@2x.png",@"img_Univer@2x.png",@"img_Organic@2x.png", nil];
    collectionarrImagesDish=[NSMutableArray arrayWithObjects:@"img_halal@2x.png",@"img_Univer@2x.png",@"img_Organic@2x.png", nil];

    // Do any additional setup after loading the view.
}


-(void)IntegrateBodyDesign{
    
    
    scrollviewMenu=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 50, WIDTH, HEIGHT-80)];
    [scrollviewMenu setShowsVerticalScrollIndicator:NO];
    scrollviewMenu.delegate = self;
    scrollviewMenu.scrollEnabled = YES;
    scrollviewMenu.showsVerticalScrollIndicator = NO;
    [scrollviewMenu setUserInteractionEnabled:YES];
    scrollviewMenu.backgroundColor = [UIColor clearColor];
    [scrollviewMenu setContentSize:CGSizeMake(0,HEIGHT)];
    [self.view addSubview:scrollviewMenu];

    
     btn_Signaldish = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        btn_Signaldish.frame = CGRectMake(60, 15, 150,25);
        
    }
    else if (IS_IPHONE_6){
        btn_Signaldish.frame = CGRectMake(55, 15, 135,25);
    }
    else  {
        btn_Signaldish.frame = CGRectMake(50, 15, 100,25);
        
    }
    btn_Signaldish.backgroundColor = [UIColor lightGrayColor];
    [btn_Signaldish setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Signaldish setTitle:@"Single Dish" forState:UIControlStateNormal];
    btn_Signaldish.titleLabel.font = [UIFont fontWithName:kFontBold size:10];
    [btn_Signaldish addTarget:self action:@selector(btn_signalDishClick:) forControlEvents:UIControlEventTouchUpInside];
    [scrollviewMenu addSubview:btn_Signaldish];
    
    
    
    
    
    btn_Meal = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        btn_Meal.frame = CGRectMake(210, 15, 150,25);
    }
    else if (IS_IPHONE_6){
        btn_Meal.frame = CGRectMake(190, 15, 135,25);
    }
    else  {
        btn_Meal.frame = CGRectMake(150, 15, 100,25);
    }
    btn_Meal.backgroundColor = [UIColor blackColor];
    [btn_Meal setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Meal setTitle:@"Meal" forState:UIControlStateNormal];
    btn_Meal.titleLabel.font = [UIFont fontWithName:kFontBold size:10];
    [btn_Meal addTarget:self action:@selector(Btn_MealClick:) forControlEvents:UIControlEventTouchUpInside];
    [scrollviewMenu addSubview:btn_Meal];
    
    
    
    
    //View for Meal.........
    
    
    
    
    view_Meal = [[UIView alloc]init];
    view_Meal.frame=CGRectMake(0,CGRectGetMaxY(btn_Signaldish.frame),WIDTH,800);
    view_Meal.backgroundColor=[UIColor clearColor];
    [scrollviewMenu  addSubview: view_Meal];
       
    


    
    UIImageView *img_Backgroundimage=[[UIImageView alloc]init];
        if (IS_IPHONE_6Plus){
        img_Backgroundimage.frame = CGRectMake(0, CGRectGetMaxY(btn_Signaldish.frame)+10, WIDTH, 70);
    }
    else if (IS_IPHONE_6){
        img_Backgroundimage.frame = CGRectMake(0, CGRectGetMaxY(btn_Signaldish.frame)+10, WIDTH, 80);
    }
    else  {
        img_Backgroundimage.frame = CGRectMake(0, CGRectGetMaxY(btn_Signaldish.frame)+10, WIDTH, 80);
    }
    
    [img_Backgroundimage setUserInteractionEnabled:YES];
    img_Backgroundimage.backgroundColor=[UIColor clearColor];
    img_Backgroundimage.image=[UIImage imageNamed:@"bg1.png"];
    [view_Meal addSubview:img_Backgroundimage];
    
    
    UIImageView *Arrow_left=[[UIImageView alloc]init];
    
    
    
    
    if (IS_IPHONE_6Plus){
        Arrow_left.frame=CGRectMake(15, 20, 10, 15);
        
    }
    else if (IS_IPHONE_6){
        Arrow_left.frame=CGRectMake(5, 25, 10, 15);
    }
    else  {
        Arrow_left.frame=CGRectMake(5, 25, 10, 15);
        
    }
    
    [Arrow_left setUserInteractionEnabled:YES];
    Arrow_left.backgroundColor=[UIColor clearColor];
    Arrow_left.image=[UIImage imageNamed:@"arrow_left.png"];
    [img_Backgroundimage addSubview:Arrow_left];
    
    UIButton *btn_Arrowleft = [[UIButton alloc] init];
    
    
    
    if (IS_IPHONE_6Plus){
        btn_Arrowleft.frame = CGRectMake(15, 20, 10, 15);
        
    }
    else if (IS_IPHONE_6){
        btn_Arrowleft.frame = CGRectMake(5, 25, 10, 15);
        
    }
    else  {
        btn_Arrowleft.frame = CGRectMake(5, 25, 10, 15);
        
    }
    
    btn_Arrowleft.backgroundColor = [UIColor clearColor];
    [btn_Arrowleft setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    //    [btn_Arrowleft addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_Backgroundimage addSubview:btn_Arrowleft];
    
    
    layout=[[UICollectionViewFlowLayout alloc] init];
    collView_serviceDirectory = [[UICollectionView alloc] initWithFrame:CGRectMake(30,0,WIDTH-65,70)
                                                   collectionViewLayout:layout];
    
    
    
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_serviceDirectory setDataSource:self];
    [collView_serviceDirectory setDelegate:self];
    collView_serviceDirectory.scrollEnabled = YES;
    collView_serviceDirectory.showsVerticalScrollIndicator = NO;
    collView_serviceDirectory.showsHorizontalScrollIndicator = NO;
    collView_serviceDirectory.pagingEnabled = NO;
    [collView_serviceDirectory registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_serviceDirectory setBackgroundColor:[UIColor clearColor]];
    layout.minimumInteritemSpacing = 2;
    layout.minimumLineSpacing = 0;
    collView_serviceDirectory.userInteractionEnabled = YES;
    [img_Backgroundimage addSubview:collView_serviceDirectory];
    
    
    UIImageView *Arrow_right=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus){
        Arrow_right.frame=CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+1, 25, 10, 15);
        
    }
    else if (IS_IPHONE_6){
        Arrow_right.frame=CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
        
    }
    else  {
        Arrow_right.frame=CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
        
    }
    [Arrow_right setUserInteractionEnabled:YES];
    Arrow_right.backgroundColor=[UIColor clearColor];
    Arrow_right.image=[UIImage imageNamed:@"arrow_right.png"];
    [img_Backgroundimage addSubview:Arrow_right];
    
    
    UIButton *btn_Arrowright = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        btn_Arrowright.frame = CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+1, 25, 10, 15);
        
    }
    else if (IS_IPHONE_6){
        btn_Arrowright.frame = CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
        
    }
    else  {
        btn_Arrowright.frame = CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
        
    }
    btn_Arrowright.backgroundColor = [UIColor clearColor];
    //    [btn_Arrowleft setImage:[UIImage imageNamed:@"arrow_left.png"] forState:UIControlStateNormal];
    [btn_Arrowright setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    //    [btn_Arrowleft addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_Backgroundimage addSubview:btn_Arrowright];
    
    
    
    UILabel  * lbl_Mealimage = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus){
        lbl_Mealimage.frame = CGRectMake(100,120, 220,40);
        
    }
    else if (IS_IPHONE_6){
        lbl_Mealimage.frame = CGRectMake(100,120, 220,40);
        
    }
    else  {
        lbl_Mealimage.frame = CGRectMake(80,120, 220,40);
        
    }
    lbl_Mealimage.text = @"Upload Meal image ";
    lbl_Mealimage.backgroundColor=[UIColor clearColor];
    lbl_Mealimage.textColor=[UIColor blackColor];
    lbl_Mealimage.textAlignment=NSTextAlignmentLeft;
    lbl_Mealimage.font = [UIFont fontWithName:kFontBold size:13];
    [view_Meal addSubview:lbl_Mealimage];
    
    
    img_backround=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus){
        img_backround.frame=CGRectMake(80, CGRectGetMaxY(lbl_Mealimage.frame)+1, 240, 180);
        
    }
    else if (IS_IPHONE_6){
        img_backround.frame=CGRectMake(80, CGRectGetMaxY(lbl_Mealimage.frame)+1, 210, 180);
        
    }
    else  {
        img_backround.frame=CGRectMake(60, CGRectGetMaxY(lbl_Mealimage.frame)+1, 180, 180);
        
    }
   img_backround.image=[UIImage imageNamed:@"img_bg@2x.png"];
    [img_backround setUserInteractionEnabled:YES];
    img_backround.backgroundColor=[UIColor clearColor];
    [view_Meal addSubview:img_backround];
    
    
    UIImageView *img_Dropimage=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus){
        img_Dropimage.frame=CGRectMake(210, 150, 15, 15);
        
    }
    else if (IS_IPHONE_6){
        img_Dropimage.frame=CGRectMake(180, 150, 15, 15);
        
    }
    else  {
        img_Dropimage.frame=CGRectMake(150, 150, 15, 15);
        
        
    }
    img_Dropimage.image=[UIImage imageNamed:@"icon 2.png"];
    [img_Dropimage setUserInteractionEnabled:YES];
    img_Dropimage.backgroundColor=[UIColor clearColor];
    [img_backround addSubview:img_Dropimage];
    
    
    btn_imgupload = [[UIButton alloc] init];
    if (IS_IPHONE_6Plus){
        btn_imgupload.frame=CGRectMake(0, 0, 15, 15);
        
    }
    else if (IS_IPHONE_6){
        btn_imgupload.frame=CGRectMake(0, 0, 15, 15);
        
    }
    else  {
        btn_imgupload.frame=CGRectMake(0, 0, 15, 15);
      
    }
    btn_imgupload.layer.cornerRadius=4.0f;
    btn_imgupload.backgroundColor=[UIColor redColor];
    [btn_imgupload addTarget:self action:@selector(btn_imgupload:) forControlEvents:UIControlEventTouchUpInside ];
    [img_Dropimage addSubview:btn_imgupload];

    
    UILabel  * lbl_Weidth = [[UILabel alloc]init];
    if (IS_IPHONE_6Plus){
        lbl_Weidth.frame=CGRectMake(CGRectGetMaxX(img_backround.frame)+3,CGRectGetMaxY(lbl_Mealimage.frame)+80, 100,120);
    }
    else if (IS_IPHONE_6){
        lbl_Weidth.frame=CGRectMake(CGRectGetMaxX(img_backround.frame)+2,CGRectGetMaxY(lbl_Mealimage.frame)+80, 100,120);
    }
    else  {
        lbl_Weidth.frame=CGRectMake(CGRectGetMaxX(img_backround.frame)+2,CGRectGetMaxY(lbl_Mealimage.frame)+80, 100,120);
        
    }
    
    lbl_Weidth.text = @"Width:150px\nHeight:150px\nSize:5mb\n.jpg..png";
    lbl_Weidth.backgroundColor=[UIColor clearColor];
    lbl_Weidth.textColor=[UIColor lightGrayColor];
    lbl_Weidth.textAlignment=NSTextAlignmentLeft;
    lbl_Weidth.numberOfLines=0;
    lbl_Weidth.font = [UIFont fontWithName:kFontBold size:10];
    [view_Meal addSubview:lbl_Weidth];
    
    
    UIImageView *img_backgroundimages=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus){
        img_backgroundimages.frame=CGRectMake(5, CGRectGetMaxY(img_backround.frame)+2, WIDTH-4, 400);
    }
    else if (IS_IPHONE_6){
        img_backgroundimages.frame=CGRectMake(5, CGRectGetMaxY(img_backround.frame)+2, WIDTH-4, 400);
    }
    else  {
        img_backgroundimages.frame=CGRectMake(5, CGRectGetMaxY(img_backround.frame)+2, WIDTH-4, 350);
        
    }
    img_backgroundimages.image=[UIImage imageNamed:@"img_backgound.png"];
    
    [img_backgroundimages setUserInteractionEnabled:YES];
    img_backgroundimages.backgroundColor=[UIColor clearColor];
    [view_Meal addSubview:img_backgroundimages];
    
    txt_MealTitle = [[UITextField alloc] init];
    if (IS_IPHONE_6Plus){
        txt_MealTitle.frame=CGRectMake(30, 10, 250, 30);
    }
    else if (IS_IPHONE_6){
        txt_MealTitle.frame=CGRectMake(30, 10, 250, 30);
    }
    else  {
        txt_MealTitle.frame=CGRectMake(20, 10, 250, 30);
    }
    txt_MealTitle.borderStyle = UITextBorderStyleNone;
    txt_MealTitle.font = [UIFont fontWithName:kFont size:13];
    txt_MealTitle.placeholder = @"Meal Title";
    [txt_MealTitle setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_MealTitle setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_MealTitle.leftViewMode = UITextFieldViewModeAlways;
    txt_MealTitle.userInteractionEnabled=YES;
    txt_MealTitle.textAlignment = NSTextAlignmentLeft;
    txt_MealTitle.backgroundColor = [UIColor clearColor];
    txt_MealTitle.keyboardType = UIKeyboardTypeAlphabet;
    txt_MealTitle.delegate = self;
    [img_backgroundimages addSubview:txt_MealTitle];
    
    UIImageView *line_img=[[UIImageView alloc]init];
    
    
    if (IS_IPHONE_6Plus){
        line_img.frame=CGRectMake(30, CGRectGetMaxY(txt_MealTitle.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6){
        line_img.frame=CGRectMake(30, CGRectGetMaxY(txt_MealTitle.frame)+1, WIDTH-60, 0.5);
    }
    else  {
        line_img.frame=CGRectMake(20, CGRectGetMaxY(txt_MealTitle.frame)+1, WIDTH-40, 0.5);
        
    }
    [line_img setUserInteractionEnabled:YES];
    line_img.backgroundColor=[UIColor clearColor];
    line_img.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:line_img];
    
    
    UILabel  * lbl_Maxchar = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus){
        lbl_Maxchar.frame=CGRectMake(330,CGRectGetMaxY(txt_MealTitle.frame)+4, 100,5);
        
    }
    else if (IS_IPHONE_6){
        lbl_Maxchar.frame=CGRectMake(300,CGRectGetMaxY(txt_MealTitle.frame)+4, 100,5);
        
    }
    else  {
        lbl_Maxchar.frame=CGRectMake(240,CGRectGetMaxY(txt_MealTitle.frame)+4, 100,5);
        
        
    }
    lbl_Maxchar.text = @"(Max. 50 char)";
    lbl_Maxchar.backgroundColor=[UIColor clearColor];
    lbl_Maxchar.textColor=[UIColor lightGrayColor];
    lbl_Maxchar.textAlignment=NSTextAlignmentLeft;
    lbl_Maxchar.numberOfLines=0;
    lbl_Maxchar.font = [UIFont fontWithName:kFontBold size:7];
    [img_backgroundimages addSubview:lbl_Maxchar];
    
    
    
    
    
    UITextField *txt_Category= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus){
        txt_Category.frame=CGRectMake(30, CGRectGetMaxY(txt_MealTitle.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6){
        txt_Category.frame=CGRectMake(30, CGRectGetMaxY(txt_MealTitle.frame)+5, 200, 30);
    }
    else  {
        txt_Category.frame=CGRectMake(20, CGRectGetMaxY(txt_MealTitle.frame)+5, 200, 30);
    }
    txt_Category.borderStyle = UITextBorderStyleNone;
    txt_Category.font = [UIFont fontWithName:kFont size:13];
    txt_Category.placeholder = @"Category";
    [txt_Category setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Category setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Category.leftViewMode = UITextFieldViewModeAlways;
    txt_Category.userInteractionEnabled=YES;
    txt_Category.textAlignment = NSTextAlignmentLeft;
    txt_Category.backgroundColor = [UIColor clearColor];
    txt_Category.keyboardType = UIKeyboardTypeAlphabet;
    txt_Category.delegate = self;
    [img_backgroundimages addSubview:txt_Category];
    
    UIImageView *img_lineimg=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus){
        img_lineimg.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6){
        img_lineimg.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+1, WIDTH-60, 0.5);
    }
    else  {
        img_lineimg.frame=CGRectMake(20, CGRectGetMaxY(txt_Category.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_lineimg setUserInteractionEnabled:YES];
    img_lineimg.backgroundColor=[UIColor clearColor];
    img_lineimg.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_lineimg];
    
    
    
    UIButton *img_dropbox = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        img_dropbox.frame=CGRectMake(370, CGRectGetMaxY(txt_MealTitle.frame)+20, 15, 10);
    }
    else if (IS_IPHONE_6){
        img_dropbox.frame=CGRectMake(325, CGRectGetMaxY(txt_MealTitle.frame)+20, 15, 10);
    }
    else  {
        img_dropbox.frame=CGRectMake(275, CGRectGetMaxY(txt_MealTitle.frame)+20, 15, 10);
        
    }
    img_dropbox.backgroundColor = [UIColor clearColor];
    [img_dropbox setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [img_dropbox setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [img_dropbox addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimages addSubview:img_dropbox];
    
    
    
    UITextField *txt_Cuisine= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus){
        txt_Cuisine.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6){
        txt_Cuisine.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+5, 200, 30);
    }
    else  {
        txt_Cuisine.frame=CGRectMake(20, CGRectGetMaxY(txt_Category.frame)+5, 200, 30);
    }
    txt_Cuisine.borderStyle = UITextBorderStyleNone;
    txt_Cuisine.font = [UIFont fontWithName:kFont size:13];
    txt_Cuisine.placeholder = @"Cuisine";
    [txt_Cuisine setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Cuisine setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Cuisine.leftViewMode = UITextFieldViewModeAlways;
    txt_Cuisine.userInteractionEnabled=YES;
    txt_Cuisine.textAlignment = NSTextAlignmentLeft;
    txt_Cuisine.backgroundColor = [UIColor clearColor];
    txt_Cuisine.keyboardType = UIKeyboardTypeAlphabet;
    txt_Cuisine.delegate = self;
    [img_backgroundimages addSubview:txt_Cuisine];
    
    UIImageView *img_line=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus){
        img_line.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6){
        img_line.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    }
    else  {
        img_line.frame=CGRectMake(20, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_line setUserInteractionEnabled:YES];
    img_line.backgroundColor=[UIColor clearColor];
    img_line.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_line];
    
    
    
    UIButton *img_dropbox1mg = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        img_dropbox1mg.frame=CGRectMake(370, CGRectGetMaxY(txt_Category.frame)+20, 15, 10);
    }
    else if (IS_IPHONE_6){
        img_dropbox1mg.frame=CGRectMake(325, CGRectGetMaxY(txt_Category.frame)+20, 15, 10);
    }
    else  {
        img_dropbox1mg.frame=CGRectMake(275, CGRectGetMaxY(txt_Category.frame)+20, 15, 10);
        
    }
    img_dropbox1mg.backgroundColor = [UIColor clearColor];
    [img_dropbox1mg setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [img_dropbox1mg setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [img_dropbox1mg addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimages addSubview:img_dropbox1mg];
    
    UITextField *txt_Course= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus){
        txt_Course.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6){
        txt_Course.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+5, 200, 30);
    }
    else  {
        txt_Course.frame=CGRectMake(20, CGRectGetMaxY(txt_Cuisine.frame)+5, 200, 30);
    }
    txt_Course.borderStyle = UITextBorderStyleNone;
    txt_Course.font = [UIFont fontWithName:kFont size:13];
    txt_Course.placeholder = @"Courses";
    [txt_Course setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Course setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Course.leftViewMode = UITextFieldViewModeAlways;
    txt_Course.userInteractionEnabled=YES;
    txt_Course.textAlignment = NSTextAlignmentLeft;
    txt_Course.backgroundColor = [UIColor clearColor];
    txt_Course.keyboardType = UIKeyboardTypeAlphabet;
    txt_Course.delegate = self;
    [img_backgroundimages addSubview:txt_Course];
    
    UIImageView *img_lineimge=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus){
        img_lineimge.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6){
        img_lineimge.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+1, WIDTH-60, 0.5);
    }
    else  {
        img_lineimge.frame=CGRectMake(20, CGRectGetMaxY(txt_Course.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_lineimge setUserInteractionEnabled:YES];
    img_lineimge.backgroundColor=[UIColor clearColor];
    img_lineimge.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_lineimge];
    
    
    
    UIButton *img_dropbox1mgs = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        img_dropbox1mgs.frame=CGRectMake(370, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
    }
    else if (IS_IPHONE_6){
        img_dropbox1mgs.frame=CGRectMake(325, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
    }
    else  {
        img_dropbox1mgs.frame=CGRectMake(275, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
        
    }
    img_dropbox1mgs.backgroundColor = [UIColor clearColor];
    [img_dropbox1mgs setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [img_dropbox1mgs setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [img_dropbox1mgs addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimages addSubview:img_dropbox1mgs];
    
    
    
    
    UITextField *txt_keyword= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus){
        txt_keyword.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6){
        txt_keyword.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+5, 200, 30);
    }
    else  {
        txt_keyword.frame=CGRectMake(20, CGRectGetMaxY(txt_Course.frame)+5, 200, 30);
    }
    txt_keyword.borderStyle = UITextBorderStyleNone;
    txt_keyword.font = [UIFont fontWithName:kFont size:13];
    txt_keyword.placeholder = @"Keyword";
    [txt_keyword setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_keyword setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_keyword.leftViewMode = UITextFieldViewModeAlways;
    txt_keyword.userInteractionEnabled=YES;
    txt_keyword.textAlignment = NSTextAlignmentLeft;
    txt_keyword.backgroundColor = [UIColor clearColor];
    txt_keyword.keyboardType = UIKeyboardTypeAlphabet;
    txt_keyword.delegate = self;
    [img_backgroundimages addSubview:txt_keyword];
    
    UIImageView *img_lineimges=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus){
        img_lineimges.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6){
        img_lineimges.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+1, WIDTH-60, 0.5);
    }
    else  {
        img_lineimges.frame=CGRectMake(20, CGRectGetMaxY(txt_keyword.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_lineimges setUserInteractionEnabled:YES];
    img_lineimges.backgroundColor=[UIColor clearColor];
    img_lineimges.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_lineimges];
    
    
    
    UILabel  * lbl_uptolimit = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus){
        lbl_uptolimit.frame=CGRectMake(350, CGRectGetMaxY(txt_Course.frame)+20, 35, 15);
        
    }
    else if (IS_IPHONE_6){
        lbl_uptolimit.frame=CGRectMake(310, CGRectGetMaxY(txt_Course.frame)+20, 35, 15);
        
    }
    else  {
        lbl_uptolimit.frame=CGRectMake(260, CGRectGetMaxY(txt_Course.frame)+20, 35, 15);
        
        
    }
    lbl_uptolimit.text = @"Up to 5";
    lbl_uptolimit.backgroundColor=[UIColor clearColor];
    lbl_uptolimit.textColor=[UIColor lightGrayColor];
    lbl_uptolimit.textAlignment=NSTextAlignmentLeft;
    lbl_uptolimit.numberOfLines=0;
    lbl_uptolimit.font = [UIFont fontWithName:kFontBold size:9];
    [img_backgroundimages addSubview:lbl_uptolimit];
    
    //    UIButton *btn_uptolimit = [[UIButton alloc] init];
    //
    //    if (IS_IPHONE_6Plus){
    //        btn_uptolimit.frame=CGRectMake(200, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    //    }
    //    else if (IS_IPHONE_6){
    //        btn_uptolimit.frame=CGRectMake(325, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
    //    }
    //    else  {
    //        btn_uptolimit.frame=CGRectMake(250, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    //
    //    }
    //    btn_uptolimit.backgroundColor = [UIColor clearColor];
    ////    [btn_uptolimit setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    //    [btn_uptolimit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    //    [btn_uptolimit addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    //    [img_backgroundimages addSubview:btn_uptolimit];
    //
    
    UITextField *txt_Price= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus){
        txt_Price.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6){
        txt_Price.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+5, 200, 30);
    }
    else  {
        txt_Price.frame=CGRectMake(20, CGRectGetMaxY(txt_keyword.frame)+5, 200, 30);
    }
    txt_Price.borderStyle = UITextBorderStyleNone;
    txt_Price.font = [UIFont fontWithName:kFont size:13];
    txt_Price.placeholder = @"Price";
    [txt_Price setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Price setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Price.leftViewMode = UITextFieldViewModeAlways;
    txt_Price.userInteractionEnabled=YES;
    txt_Price.textAlignment = NSTextAlignmentLeft;
    txt_Price.backgroundColor = [UIColor clearColor];
    txt_Price.keyboardType = UIKeyboardTypeAlphabet;
    txt_Price.delegate = self;
    [img_backgroundimages addSubview:txt_Price];
    
    UIImageView *img_linesimge=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus){
        img_linesimge.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6){
        img_linesimge.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+1, WIDTH-60, 0.5);
    }
    else  {
        img_linesimge.frame=CGRectMake(20, CGRectGetMaxY(txt_Price.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_linesimge setUserInteractionEnabled:YES];
    img_linesimge.backgroundColor=[UIColor clearColor];
    img_linesimge.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_linesimge];
    
    
    
    UILabel  * lbl_PerMeal = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus){
        lbl_PerMeal.frame=CGRectMake(345, CGRectGetMaxY(txt_keyword.frame)+20, 40, 15);
        
    }
    else if (IS_IPHONE_6){
        lbl_PerMeal.frame=CGRectMake(300, CGRectGetMaxY(txt_keyword.frame)+20, 40, 15);
        
    }
    else  {
        lbl_PerMeal.frame=CGRectMake(250, CGRectGetMaxY(txt_keyword.frame)+20, 40, 15);
        
        
    }
    lbl_PerMeal.text = @"Per Meal";
    lbl_PerMeal.backgroundColor=[UIColor clearColor];
    lbl_PerMeal.textColor=[UIColor lightGrayColor];
    lbl_PerMeal.textAlignment=NSTextAlignmentLeft;
    lbl_PerMeal.numberOfLines=0;
    lbl_PerMeal.font = [UIFont fontWithName:kFontBold size:9];
    [img_backgroundimages addSubview:lbl_PerMeal];
    
    
    
    
    
    UILabel  * lbl_Description = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus){
        lbl_Description.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+10, 100, 25);
        
    }
    else if (IS_IPHONE_6){
        lbl_Description.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+10, 100, 25);
        
    }
    else  {
        lbl_Description.frame=CGRectMake(20, CGRectGetMaxY(txt_Price.frame)+10, 100, 25);
        
        
    }
    lbl_Description.text = @"Description";
    lbl_Description.backgroundColor=[UIColor clearColor];
    lbl_Description.textColor=[UIColor blackColor];
    lbl_Description.textAlignment=NSTextAlignmentLeft;
    lbl_Description.numberOfLines=0;
    lbl_Description.font = [UIFont fontWithName:kFont size:14];
    [img_backgroundimages addSubview:lbl_Description];
    
    
    
    txt_view=[[UITextView alloc]init];
    if (IS_IPHONE_6Plus){
        txt_view.frame=CGRectMake(30, CGRectGetMaxY(lbl_Description.frame)+5, WIDTH-60, 100);
        
    }
    else if (IS_IPHONE_6){
        txt_view.frame=CGRectMake(30, CGRectGetMaxY(lbl_Description.frame)+5, WIDTH-60, 100);
        
    }
    else  {
        txt_view.frame=CGRectMake(20, CGRectGetMaxY(lbl_Description.frame)+5, WIDTH-40, 100);
        
        
    }
    [txt_view setReturnKeyType:UIReturnKeyDone];
    txt_view.scrollEnabled=YES;
    [txt_view setDelegate:self];
    [txt_view setReturnKeyType:UIReturnKeyDone];
    [txt_view setFont:[UIFont fontWithName:kFont size:10]];
    [txt_view setTextColor:[UIColor lightTextColor]];
    txt_view.userInteractionEnabled=YES;
    txt_view.backgroundColor=[UIColor whiteColor];
    txt_view.delegate=self;
    txt_view.textColor=[UIColor blackColor];
    txt_view.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    txt_view.layer.borderWidth=1.0f;
    txt_view.clipsToBounds=YES;
    [img_backgroundimages addSubview:txt_view];
    
    UILabel  * lblmax = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus){
        lblmax.frame=CGRectMake(320, CGRectGetMaxY(txt_view.frame)+1, 70, 15);
        
    }
    else if (IS_IPHONE_6){
        lblmax.frame=CGRectMake(280, CGRectGetMaxY(txt_view.frame)+1, 70, 15);
        
    }
    else  {
        lblmax.frame=CGRectMake(250, CGRectGetMaxY(txt_view.frame)+1, 70, 15);
        
        
    }
    
    lblmax.text = @"(500 char. max.)";
    lblmax.backgroundColor=[UIColor clearColor];
    lblmax.textColor=[UIColor lightGrayColor];
    lblmax.numberOfLines = 0;
    lblmax.font = [UIFont fontWithName:kFont size:8];
    [img_backgroundimages addSubview:lblmax];
    
    
    
    
    
    UIButton *btn_Save = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        btn_Save.frame=CGRectMake(50, HEIGHT-50, WIDTH-80, 40);
    }
    else if (IS_IPHONE_6){
        btn_Save.frame=CGRectMake(50, HEIGHT-50, WIDTH-80, 40);
    }
    else  {
        btn_Save.frame=CGRectMake(30, HEIGHT-50, WIDTH-60, 40);
        
    }
    btn_Save.backgroundColor = [UIColor clearColor];
    [btn_Save setImage:[UIImage imageNamed:@"img_savebutton.png"] forState:UIControlStateNormal];
    [btn_Save setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Save addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [self.view addSubview:btn_Save];
    
    
    
    
    //View for Signal Dish.........
    
    view_SignalDish = [[UIView alloc]init];
    view_SignalDish.frame=CGRectMake(0,CGRectGetMaxY(btn_Signaldish.frame),WIDTH,800);
    view_SignalDish.backgroundColor=[UIColor clearColor];
    [scrollviewMenu  addSubview: view_SignalDish];

    
        
    
        UIImageView *img_BackgroundimageDish=[[UIImageView alloc]init];
                if (IS_IPHONE_6Plus){
            img_BackgroundimageDish.frame = CGRectMake(0, CGRectGetMaxY(btn_Signaldish.frame)+10, WIDTH, 80);
        }
        else if (IS_IPHONE_6){
            img_BackgroundimageDish.frame = CGRectMake(0, CGRectGetMaxY(btn_Signaldish.frame)+10, WIDTH, 80);
        }
        else  {
            img_BackgroundimageDish.frame = CGRectMake(0, CGRectGetMaxY(btn_Signaldish.frame)+10, WIDTH, 80);
        }
        
        [img_BackgroundimageDish setUserInteractionEnabled:YES];
        img_BackgroundimageDish.backgroundColor=[UIColor clearColor];
        img_BackgroundimageDish.image=[UIImage imageNamed:@"bg1.png"];
        [view_SignalDish addSubview:img_BackgroundimageDish];
        
        
        UIImageView *Arrow_leftDish=[[UIImageView alloc]init];
        
        
        
        
        if (IS_IPHONE_6Plus){
            Arrow_leftDish.frame=CGRectMake(15, 20, 10, 15);
            
        }
        else if (IS_IPHONE_6){
            Arrow_leftDish.frame=CGRectMake(5, 25, 10, 15);
        }
        else  {
            Arrow_leftDish.frame=CGRectMake(5, 25, 10, 15);
            
        }
        
        [Arrow_leftDish setUserInteractionEnabled:YES];
        Arrow_leftDish.backgroundColor=[UIColor clearColor];
        Arrow_leftDish.image=[UIImage imageNamed:@"arrow_left.png"];
        [img_BackgroundimageDish addSubview:Arrow_leftDish];
        
        UIButton *btn_ArrowleftDish = [[UIButton alloc] init];
        
        
        
        if (IS_IPHONE_6Plus){
            btn_ArrowleftDish.frame = CGRectMake(15, 20, 10, 15);
            
        }
        else if (IS_IPHONE_6){
            btn_ArrowleftDish.frame = CGRectMake(5, 25, 10, 15);
            
        }
        else  {
            btn_ArrowleftDish.frame = CGRectMake(5, 25, 10, 15);
            
        }
        
        btn_ArrowleftDish.backgroundColor = [UIColor clearColor];
        [btn_ArrowleftDish setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
        //    [btn_Arrowleft addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
        [img_BackgroundimageDish addSubview:btn_ArrowleftDish];
        
        
        layout=[[UICollectionViewFlowLayout alloc] init];
        collView_serviceDirectory2 = [[UICollectionView alloc] initWithFrame:CGRectMake(30,0,WIDTH-65,70)
                                                       collectionViewLayout:layout];
        
        
        
        [layout2 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        [collView_serviceDirectory2 setDataSource:self];
        [collView_serviceDirectory2 setDelegate:self];
        collView_serviceDirectory2.scrollEnabled = YES;
        collView_serviceDirectory2.showsVerticalScrollIndicator = NO;
        collView_serviceDirectory2.showsHorizontalScrollIndicator = NO;
        collView_serviceDirectory2.pagingEnabled = NO;
        [collView_serviceDirectory2 registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [collView_serviceDirectory2 setBackgroundColor:[UIColor clearColor]];
        layout2.minimumInteritemSpacing = 2;
        layout2.minimumLineSpacing = 0;
        collView_serviceDirectory2.userInteractionEnabled = YES;
        [img_BackgroundimageDish addSubview:collView_serviceDirectory2];
        
        
        
        
        
        
        
        
        UIImageView *Arrow_rightDish=[[UIImageView alloc]init];
        
        if (IS_IPHONE_6Plus){
            Arrow_rightDish.frame=CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+1, 25, 10, 15);
            
        }
        else if (IS_IPHONE_6){
            Arrow_rightDish.frame=CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
            
        }
        else  {
            Arrow_rightDish.frame=CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
            
        }
        [Arrow_rightDish setUserInteractionEnabled:YES];
        Arrow_rightDish.backgroundColor=[UIColor clearColor];
        Arrow_rightDish.image=[UIImage imageNamed:@"arrow_right.png"];
        [img_BackgroundimageDish addSubview:Arrow_rightDish];
        
        
        UIButton *btn_ArrowrightDish = [[UIButton alloc] init];
        
        if (IS_IPHONE_6Plus){
            btn_ArrowrightDish.frame = CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+1, 25, 10, 15);
            
        }
        else if (IS_IPHONE_6){
            btn_ArrowrightDish.frame = CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
            
        }
        else  {
            btn_ArrowrightDish.frame = CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
            
        }
        btn_ArrowrightDish.backgroundColor = [UIColor clearColor];
        //    [btn_ArrowrightDish setImage:[UIImage imageNamed:@"arrow_left.png"] forState:UIControlStateNormal];
        [btn_ArrowrightDish setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        //    [btn_ArrowrightDish addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
        [img_BackgroundimageDish addSubview:btn_ArrowrightDish];
        
        
        
        UILabel  * lbl_MealimageDish = [[UILabel alloc]init];
        
        if (IS_IPHONE_6Plus){
            lbl_MealimageDish.frame = CGRectMake(100,120, 220,40);
            
        }
        else if (IS_IPHONE_6){
            lbl_MealimageDish.frame = CGRectMake(100,120, 220,40);
            
        }
        else  {
            lbl_MealimageDish.frame = CGRectMake(80,120, 220,40);
            
        }
        lbl_MealimageDish.text = @"Upload Dish image ";
        lbl_MealimageDish.backgroundColor=[UIColor clearColor];
        lbl_MealimageDish.textColor=[UIColor blackColor];
        lbl_MealimageDish.textAlignment=NSTextAlignmentLeft;
        lbl_MealimageDish.font = [UIFont fontWithName:kFontBold size:13];
        [view_SignalDish addSubview:lbl_MealimageDish];
        
        
        UIImageView *img_backroundDish=[[UIImageView alloc]init];
        
        
        if (IS_IPHONE_6Plus){
            img_backroundDish.frame=CGRectMake(80, CGRectGetMaxY(lbl_Mealimage.frame)+1, 240, 180);
            
        }
        else if (IS_IPHONE_6){
            img_backroundDish.frame=CGRectMake(80, CGRectGetMaxY(lbl_Mealimage.frame)+1, 210, 180);
            
        }
        else  {
            img_backroundDish.frame=CGRectMake(60, CGRectGetMaxY(lbl_Mealimage.frame)+1, 180, 180);
            
        }
        img_backroundDish.image=[UIImage imageNamed:@"img_bg@2x.png"];
        
        [img_backroundDish setUserInteractionEnabled:YES];
        img_backroundDish.backgroundColor=[UIColor clearColor];
        [view_SignalDish addSubview:img_backroundDish];
        
        
        UIImageView *img_DropimageDish=[[UIImageView alloc]init];
        
        
        if (IS_IPHONE_6Plus){
            img_DropimageDish.frame=CGRectMake(210, 150, 15, 15);
            
        }
        else if (IS_IPHONE_6){
            img_DropimageDish.frame=CGRectMake(180, 150, 15, 15);
            
        }
        else  {
            img_DropimageDish.frame=CGRectMake(180, 150, 15, 15);
            
            
        }
        img_DropimageDish.image=[UIImage imageNamed:@"icon 2.png"];
        
        [img_DropimageDish setUserInteractionEnabled:YES];
        img_DropimageDish.backgroundColor=[UIColor clearColor];
        [img_backround addSubview:img_DropimageDish];
        
        UILabel  * lbl_WeidthDish = [[UILabel alloc]init];
        
        
        if (IS_IPHONE_6Plus){
            lbl_WeidthDish.frame=CGRectMake(CGRectGetMaxX(img_backround.frame)+3,CGRectGetMaxY(lbl_Mealimage.frame)+80, 100,120);
        }
        else if (IS_IPHONE_6){
            lbl_WeidthDish.frame=CGRectMake(CGRectGetMaxX(img_backround.frame)+2,CGRectGetMaxY(lbl_Mealimage.frame)+90, 90,110);
        }
        else  {
            lbl_WeidthDish.frame=CGRectMake(CGRectGetMaxX(img_backround.frame)+2,CGRectGetMaxY(lbl_Mealimage.frame)+50, 100,120);
            
        }
        
        lbl_WeidthDish.text = @"Width:150px\nHeight:150px\nSize:5mb\n.jpg..png";
        lbl_WeidthDish.backgroundColor=[UIColor clearColor];
        lbl_WeidthDish.textColor=[UIColor lightGrayColor];
        lbl_WeidthDish.textAlignment=NSTextAlignmentLeft;
        lbl_WeidthDish.numberOfLines=0;
        lbl_WeidthDish.font = [UIFont fontWithName:kFontBold size:10];
        [img_DropimageDish addSubview:lbl_WeidthDish];
        
        
        UIImageView *img_backgroundimagesDish=[[UIImageView alloc]init];
        
        
        if (IS_IPHONE_6Plus){
            img_backgroundimagesDish.frame=CGRectMake(5, CGRectGetMaxY(img_backround.frame)+2, WIDTH-4, 400);
        }
        else if (IS_IPHONE_6){
            img_backgroundimagesDish.frame=CGRectMake(5, CGRectGetMaxY(img_backround.frame)+2, WIDTH-4, 400);
        }
        else  {
            img_backgroundimagesDish.frame=CGRectMake(5, CGRectGetMaxY(img_backround.frame)+2, WIDTH-4, 350);
            
        }
        img_backgroundimagesDish.image=[UIImage imageNamed:@"img_bg@2x.png"];
        
        [img_backgroundimagesDish setUserInteractionEnabled:YES];
        img_backgroundimagesDish.backgroundColor=[UIColor clearColor];
        [view_SignalDish addSubview:img_backgroundimagesDish];
        
        UITextField *txt_MealTitleDish = [[UITextField alloc] init];
        if (IS_IPHONE_6Plus){
            txt_MealTitleDish.frame=CGRectMake(30, 10, 250, 30);
        }
        else if (IS_IPHONE_6){
            txt_MealTitleDish.frame=CGRectMake(30, 10, 250, 30);
        }
        else  {
            txt_MealTitleDish.frame=CGRectMake(30, 10, 250, 30);
        }
        txt_MealTitleDish.borderStyle = UITextBorderStyleNone;
        txt_MealTitleDish.font = [UIFont fontWithName:kFont size:13];
        txt_MealTitleDish.placeholder = @"Dish Title";
        [txt_MealTitleDish setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
        [txt_MealTitleDish setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
        txt_MealTitleDish.leftViewMode = UITextFieldViewModeAlways;
        txt_MealTitleDish.userInteractionEnabled=YES;
        txt_MealTitleDish.textAlignment = NSTextAlignmentLeft;
        txt_MealTitleDish.backgroundColor = [UIColor clearColor];
        txt_MealTitleDish.keyboardType = UIKeyboardTypeAlphabet;
        txt_MealTitleDish.delegate = self;
        [img_backgroundimagesDish addSubview:txt_MealTitleDish];
        
        UIImageView *line_imgDish=[[UIImageView alloc]init];
        
        
        if (IS_IPHONE_6Plus){
            line_imgDish.frame=CGRectMake(30, CGRectGetMaxY(txt_MealTitle.frame)+1, WIDTH-60, 0.5);
        }
        else if (IS_IPHONE_6){
            line_imgDish.frame=CGRectMake(30, CGRectGetMaxY(txt_MealTitle.frame)+1, WIDTH-60, 0.5);
        }
        else  {
            line_imgDish.frame=CGRectMake(20, CGRectGetMaxY(txt_MealTitle.frame)+1, WIDTH-40, 0.5);
            
        }
        [line_imgDish setUserInteractionEnabled:YES];
        line_imgDish.backgroundColor=[UIColor clearColor];
        line_imgDish.image=[UIImage imageNamed:@"line-2.png"];
        [img_backgroundimagesDish addSubview:line_imgDish];
        
        
        UILabel  * lbl_MaxcharDish = [[UILabel alloc]init];
        
        if (IS_IPHONE_6Plus){
            lbl_MaxcharDish.frame=CGRectMake(330,CGRectGetMaxY(txt_MealTitle.frame)+4, 100,5);
            
        }
        else if (IS_IPHONE_6){
            lbl_MaxcharDish.frame=CGRectMake(300,CGRectGetMaxY(txt_MealTitle.frame)+4, 100,5);
            
        }
        else  {
            lbl_MaxcharDish.frame=CGRectMake(240,CGRectGetMaxY(txt_MealTitle.frame)+4, 100,5);
            
            
        }
        lbl_MaxcharDish.text = @"(Max. 50 char)";
        lbl_MaxcharDish.backgroundColor=[UIColor clearColor];
        lbl_MaxcharDish.textColor=[UIColor lightGrayColor];
        lbl_MaxcharDish.textAlignment=NSTextAlignmentLeft;
        lbl_MaxcharDish.numberOfLines=0;
        lbl_MaxcharDish.font = [UIFont fontWithName:kFontBold size:7];
        [img_backgroundimagesDish addSubview:lbl_MaxcharDish];
        
        
        
        
        
        UITextField *txt_CategoryDish= [[UITextField alloc] init];
        if (IS_IPHONE_6Plus){
            txt_CategoryDish.frame=CGRectMake(30, CGRectGetMaxY(txt_MealTitle.frame)+5, 200, 30);
        }
        else if (IS_IPHONE_6){
            txt_CategoryDish.frame=CGRectMake(30, CGRectGetMaxY(txt_MealTitle.frame)+5, 200, 30);
        }
        else  {
            txt_CategoryDish.frame=CGRectMake(20, CGRectGetMaxY(txt_MealTitle.frame)+5, 200, 30);
        }
        txt_CategoryDish.borderStyle = UITextBorderStyleNone;
        txt_CategoryDish.font = [UIFont fontWithName:kFont size:13];
        txt_CategoryDish.placeholder = @"Category";
        [txt_CategoryDish setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
        [txt_CategoryDish setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
        txt_CategoryDish.leftViewMode = UITextFieldViewModeAlways;
        txt_CategoryDish.userInteractionEnabled=YES;
        txt_CategoryDish.textAlignment = NSTextAlignmentLeft;
        txt_CategoryDish.backgroundColor = [UIColor clearColor];
        txt_CategoryDish.keyboardType = UIKeyboardTypeAlphabet;
        txt_CategoryDish.delegate = self;
        [img_backgroundimagesDish addSubview:txt_CategoryDish];
        
        UIImageView *img_lineimgDish=[[UIImageView alloc]init];
        
        if (IS_IPHONE_6Plus){
            img_lineimgDish.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+1, WIDTH-60, 0.5);
        }
        else if (IS_IPHONE_6){
            img_lineimgDish.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+1, WIDTH-60, 0.5);
        }
        else  {
            img_lineimgDish.frame=CGRectMake(20, CGRectGetMaxY(txt_Category.frame)+1, WIDTH-40, 0.5);
            
        }
        [img_lineimgDish setUserInteractionEnabled:YES];
        img_lineimgDish.backgroundColor=[UIColor clearColor];
        img_lineimgDish.image=[UIImage imageNamed:@"line-2.png"];
        [img_backgroundimagesDish addSubview:img_lineimgDish];
        
        
        
        UIButton *img_dropboxDish = [[UIButton alloc] init];
        
        if (IS_IPHONE_6Plus){
            img_dropboxDish.frame=CGRectMake(370, CGRectGetMaxY(txt_MealTitle.frame)+20, 15, 10);
        }
        else if (IS_IPHONE_6){
            img_dropboxDish.frame=CGRectMake(325, CGRectGetMaxY(txt_MealTitle.frame)+20, 15, 10);
        }
        else  {
            img_dropboxDish.frame=CGRectMake(275, CGRectGetMaxY(txt_MealTitle.frame)+20, 15, 10);
            
        }
        img_dropboxDish.backgroundColor = [UIColor clearColor];
        [img_dropboxDish setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
        [img_dropboxDish setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //    [img_dropboxDish addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
        [img_backgroundimagesDish addSubview:img_dropboxDish];
        
        
        
        UITextField *txt_CuisineDish= [[UITextField alloc] init];
        if (IS_IPHONE_6Plus){
            txt_CuisineDish.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+5, 200, 30);
        }
        else if (IS_IPHONE_6){
            txt_CuisineDish.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+5, 200, 30);
        }
        else  {
            txt_CuisineDish.frame=CGRectMake(20, CGRectGetMaxY(txt_Category.frame)+5, 200, 30);
        }
        txt_CuisineDish.borderStyle = UITextBorderStyleNone;
        txt_CuisineDish.font = [UIFont fontWithName:kFont size:13];
        txt_CuisineDish.placeholder = @"Cuisine";
        [txt_CuisineDish setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
        [txt_CuisineDish setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
        txt_CuisineDish.leftViewMode = UITextFieldViewModeAlways;
        txt_CuisineDish.userInteractionEnabled=YES;
        txt_CuisineDish.textAlignment = NSTextAlignmentLeft;
        txt_CuisineDish.backgroundColor = [UIColor clearColor];
        txt_CuisineDish.keyboardType = UIKeyboardTypeAlphabet;
        txt_CuisineDish.delegate = self;
        [img_backgroundimagesDish addSubview:txt_CuisineDish];
        
        UIImageView *img_lineDish=[[UIImageView alloc]init];
        
        if (IS_IPHONE_6Plus){
            img_lineDish.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
        }
        else if (IS_IPHONE_6){
            img_lineDish.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
        }
        else  {
            img_lineDish.frame=CGRectMake(20, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-40, 0.5);
            
        }
        [img_lineDish setUserInteractionEnabled:YES];
        img_lineDish.backgroundColor=[UIColor clearColor];
        img_lineDish.image=[UIImage imageNamed:@"line-2.png"];
        [img_backgroundimagesDish addSubview:img_lineDish];
        
        
        
        UIButton *img_dropbox1mgDish = [[UIButton alloc] init];
        
        if (IS_IPHONE_6Plus){
            img_dropbox1mgDish.frame=CGRectMake(370, CGRectGetMaxY(txt_Category.frame)+20, 15, 10);
        }
        else if (IS_IPHONE_6){
            img_dropbox1mgDish.frame=CGRectMake(325, CGRectGetMaxY(txt_Category.frame)+20, 15, 10);
        }
        else  {
            img_dropbox1mgDish.frame=CGRectMake(275, CGRectGetMaxY(txt_Category.frame)+20, 15, 10);
            
        }
        img_dropbox1mgDish.backgroundColor = [UIColor clearColor];
        [img_dropbox1mgDish setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
        [img_dropbox1mgDish setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //    [img_dropbox1mg addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
        [img_backgroundimagesDish addSubview:img_dropbox1mgDish];
        
        UITextField *txt_CourseDish= [[UITextField alloc] init];
        if (IS_IPHONE_6Plus){
            txt_CourseDish.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+5, 200, 30);
        }
        else if (IS_IPHONE_6){
            txt_CourseDish.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+5, 200, 30);
        }
        else  {
            txt_CourseDish.frame=CGRectMake(20, CGRectGetMaxY(txt_Cuisine.frame)+5, 200, 30);
        }
        txt_CourseDish.borderStyle = UITextBorderStyleNone;
        txt_CourseDish.font = [UIFont fontWithName:kFont size:13];
        txt_CourseDish.placeholder = @"Course";
        [txt_CourseDish setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
        [txt_CourseDish setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
        txt_CourseDish.leftViewMode = UITextFieldViewModeAlways;
        txt_CourseDish.userInteractionEnabled=YES;
        txt_CourseDish.textAlignment = NSTextAlignmentLeft;
        txt_CourseDish.backgroundColor = [UIColor clearColor];
        txt_CourseDish.keyboardType = UIKeyboardTypeAlphabet;
        txt_CourseDish.delegate = self;
        [img_backgroundimagesDish addSubview:txt_CourseDish];
        
        UIImageView *img_lineimgeDish=[[UIImageView alloc]init];
        
        if (IS_IPHONE_6Plus){
            img_lineimgeDish.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+1, WIDTH-60, 0.5);
        }
        else if (IS_IPHONE_6){
            img_lineimgeDish.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+1, WIDTH-60, 0.5);
        }
        else  {
            img_lineimgeDish.frame=CGRectMake(20, CGRectGetMaxY(txt_Course.frame)+1, WIDTH-40, 0.5);
            
        }
        [img_lineimgeDish setUserInteractionEnabled:YES];
        img_lineimgeDish.backgroundColor=[UIColor clearColor];
        img_lineimgeDish.image=[UIImage imageNamed:@"line-2.png"];
        [img_backgroundimagesDish addSubview:img_lineimgeDish];
        
        
        
        UIButton *img_dropbox1mgsDish = [[UIButton alloc] init];
        
        if (IS_IPHONE_6Plus){
            img_dropbox1mgsDish.frame=CGRectMake(370, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
        }
        else if (IS_IPHONE_6){
            img_dropbox1mgsDish.frame=CGRectMake(325, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
        }
        else  {
            img_dropbox1mgsDish.frame=CGRectMake(275, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
            
        }
        img_dropbox1mgsDish.backgroundColor = [UIColor clearColor];
        [img_dropbox1mgsDish setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
        [img_dropbox1mgsDish setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //    [img_dropbox1mgsDish addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
        [img_backgroundimagesDish addSubview:img_dropbox1mgsDish];
        
        
        
        
        UITextField *txt_keywordDish= [[UITextField alloc] init];
        if (IS_IPHONE_6Plus){
            txt_keywordDish.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+5, 200, 30);
        }
        else if (IS_IPHONE_6){
            txt_keywordDish.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+5, 200, 30);
        }
        else  {
            txt_keywordDish.frame=CGRectMake(20, CGRectGetMaxY(txt_Course.frame)+5, 200, 30);
        }
        txt_keywordDish.borderStyle = UITextBorderStyleNone;
        txt_keywordDish.font = [UIFont fontWithName:kFont size:13];
        txt_keywordDish.placeholder = @"Keyword(Eg. Tacos, Fries)";
        [txt_keywordDish setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
        [txt_keywordDish setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
        txt_keywordDish.leftViewMode = UITextFieldViewModeAlways;
        txt_keywordDish.userInteractionEnabled=YES;
        txt_keywordDish.textAlignment = NSTextAlignmentLeft;
        txt_keywordDish.backgroundColor = [UIColor clearColor];
        txt_keywordDish.keyboardType = UIKeyboardTypeAlphabet;
        txt_keywordDish.delegate = self;
        [img_backgroundimagesDish addSubview:txt_keywordDish];
        
        UIImageView *img_lineimgesDish=[[UIImageView alloc]init];
        
        if (IS_IPHONE_6Plus){
            img_lineimgesDish.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+1, WIDTH-60, 0.5);
        }
        else if (IS_IPHONE_6){
            img_lineimgesDish.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+1, WIDTH-60, 0.5);
        }
        else  {
            img_lineimgesDish.frame=CGRectMake(20, CGRectGetMaxY(txt_keyword.frame)+1, WIDTH-40, 0.5);
            
        }
        [img_lineimgesDish setUserInteractionEnabled:YES];
        img_lineimgesDish.backgroundColor=[UIColor clearColor];
        img_lineimgesDish.image=[UIImage imageNamed:@"line-2.png"];
        [img_backgroundimagesDish addSubview:img_lineimgesDish];
        
        
        
        UILabel  * lbl_uptolimitDish = [[UILabel alloc]init];
        
        if (IS_IPHONE_6Plus){
            lbl_uptolimitDish.frame=CGRectMake(350, CGRectGetMaxY(txt_Course.frame)+20, 35, 15);
            
        }
        else if (IS_IPHONE_6){
            lbl_uptolimitDish.frame=CGRectMake(310, CGRectGetMaxY(txt_Course.frame)+20, 35, 15);
            
        }
        else  {
            lbl_uptolimitDish.frame=CGRectMake(260, CGRectGetMaxY(txt_Course.frame)+20, 35, 15);
            
            
        }
        lbl_uptolimitDish.text = @"Up to 5";
        lbl_uptolimitDish.backgroundColor=[UIColor clearColor];
        lbl_uptolimitDish.textColor=[UIColor lightGrayColor];
        lbl_uptolimitDish.textAlignment=NSTextAlignmentLeft;
        lbl_uptolimitDish.numberOfLines=0;
        lbl_uptolimitDish.font = [UIFont fontWithName:kFontBold size:9];
        [img_backgroundimagesDish addSubview:lbl_uptolimitDish];
        
        //    UIButton *btn_uptolimit = [[UIButton alloc] init];
        //
        //    if (IS_IPHONE_6Plus){
        //        btn_uptolimit.frame=CGRectMake(200, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
        //    }
        //    else if (IS_IPHONE_6){
        //        btn_uptolimit.frame=CGRectMake(325, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
        //    }
        //    else  {
        //        btn_uptolimit.frame=CGRectMake(250, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
        //
        //    }
        //    btn_uptolimit.backgroundColor = [UIColor clearColor];
        ////    [btn_uptolimit setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
        //    [btn_uptolimit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //    //    [btn_uptolimit addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
        //    [img_backgroundimages addSubview:btn_uptolimit];
        //
        
        UITextField *txt_PriceDish= [[UITextField alloc] init];
        if (IS_IPHONE_6Plus){
            txt_PriceDish.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+5, 200, 30);
        }
        else if (IS_IPHONE_6){
            txt_PriceDish.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+5, 200, 30);
        }
        else  {
            txt_PriceDish.frame=CGRectMake(20, CGRectGetMaxY(txt_keyword.frame)+5, 200, 30);
        }
        txt_PriceDish.borderStyle = UITextBorderStyleNone;
        txt_PriceDish.font = [UIFont fontWithName:kFont size:13];
        txt_PriceDish.placeholder = @"Price";
        [txt_PriceDish setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
        [txt_PriceDish setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
        txt_PriceDish.leftViewMode = UITextFieldViewModeAlways;
        txt_PriceDish.userInteractionEnabled=YES;
        txt_PriceDish.textAlignment = NSTextAlignmentLeft;
        txt_PriceDish.backgroundColor = [UIColor clearColor];
        txt_PriceDish.keyboardType = UIKeyboardTypeAlphabet;
        txt_PriceDish.delegate = self;
        [img_backgroundimagesDish addSubview:txt_PriceDish];
        
        UIImageView *img_linesimgeDish=[[UIImageView alloc]init];
        
        if (IS_IPHONE_6Plus){
            img_linesimgeDish.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+1, WIDTH-60, 0.5);
        }
        else if (IS_IPHONE_6){
            img_linesimgeDish.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+1, WIDTH-60, 0.5);
        }
        else  {
            img_linesimgeDish.frame=CGRectMake(20, CGRectGetMaxY(txt_Price.frame)+1, WIDTH-40, 0.5);
            
        }
        [img_linesimgeDish setUserInteractionEnabled:YES];
        img_linesimgeDish.backgroundColor=[UIColor clearColor];
        img_linesimgeDish.image=[UIImage imageNamed:@"line-2.png"];
        [img_backgroundimagesDish addSubview:img_linesimgeDish];
        
        
        
        UILabel  * lbl_PerMealDish = [[UILabel alloc]init];
        
        if (IS_IPHONE_6Plus){
            lbl_PerMealDish.frame=CGRectMake(345, CGRectGetMaxY(txt_keyword.frame)+20, 40, 15);
            
        }
        else if (IS_IPHONE_6){
            lbl_PerMealDish.frame=CGRectMake(290, CGRectGetMaxY(txt_keyword.frame)+20, 60, 15);
            
        }
        else  {
            lbl_PerMealDish.frame=CGRectMake(250, CGRectGetMaxY(txt_keyword.frame)+20, 40, 15);
            
            
        }
        lbl_PerMealDish.text = @"Per Serving";
        lbl_PerMealDish.backgroundColor=[UIColor clearColor];
        lbl_PerMealDish.textColor=[UIColor lightGrayColor];
        lbl_PerMealDish.textAlignment=NSTextAlignmentLeft;
        lbl_PerMealDish.numberOfLines=0;
        lbl_PerMealDish.font = [UIFont fontWithName:kFontBold size:9];
        [img_backgroundimagesDish addSubview:lbl_PerMealDish];
        
        
        
        
        
        UILabel  * lbl_DescriptionDish = [[UILabel alloc]init];
        
        if (IS_IPHONE_6Plus){
            lbl_DescriptionDish.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+10, 100, 25);
            
        }
        else if (IS_IPHONE_6){
            lbl_DescriptionDish.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+10, 100, 25);
            
        }
        else  {
            lbl_DescriptionDish.frame=CGRectMake(20, CGRectGetMaxY(txt_Price.frame)+10, 100, 25);
            
            
        }
        lbl_DescriptionDish.text = @"Description";
        lbl_DescriptionDish.backgroundColor=[UIColor clearColor];
        lbl_DescriptionDish.textColor=[UIColor blackColor];
        lbl_DescriptionDish.textAlignment=NSTextAlignmentLeft;
        lbl_DescriptionDish.numberOfLines=0;
        lbl_DescriptionDish.font = [UIFont fontWithName:kFont size:14];
        [img_backgroundimagesDish addSubview:lbl_DescriptionDish];
        
        
        
        UITextView * txt_viewDish=[[UITextView alloc]init];
        if (IS_IPHONE_6Plus){
            txt_viewDish.frame=CGRectMake(30, CGRectGetMaxY(lbl_Description.frame)+5, WIDTH-60, 100);
            
        }
        else if (IS_IPHONE_6){
            txt_viewDish.frame=CGRectMake(30, CGRectGetMaxY(lbl_Description.frame)+5, WIDTH-60, 100);
            
        }
        else  {
            txt_viewDish.frame=CGRectMake(20, CGRectGetMaxY(lbl_Description.frame)+5, WIDTH-40, 100);
            
            
        }
        [txt_viewDish setReturnKeyType:UIReturnKeyDone];
        txt_viewDish.scrollEnabled=YES;
        [txt_viewDish setDelegate:self];
        [txt_viewDish setReturnKeyType:UIReturnKeyDone];
        [txt_viewDish setFont:[UIFont fontWithName:kFont size:10]];
        [txt_viewDish setTextColor:[UIColor lightTextColor]];
        txt_viewDish.userInteractionEnabled=YES;
        txt_viewDish.backgroundColor=[UIColor whiteColor];
        txt_viewDish.delegate=self;
        txt_viewDish.textColor=[UIColor blackColor];
        txt_viewDish.layer.borderColor=[[UIColor lightGrayColor]CGColor];
        txt_viewDish.layer.borderWidth=1.0f;
        txt_viewDish.clipsToBounds=YES;
        [img_backgroundimagesDish addSubview:txt_viewDish];
        
        UILabel  * lblmaxDish = [[UILabel alloc]init];
        
        if (IS_IPHONE_6Plus){
            lblmaxDish.frame=CGRectMake(320, CGRectGetMaxY(txt_view.frame)+1, 70, 15);
            
        }
        else if (IS_IPHONE_6){
            lblmaxDish.frame=CGRectMake(280, CGRectGetMaxY(txt_view.frame)+1, 70, 15);
            
        }
        else  {
            lblmaxDish.frame=CGRectMake(250, CGRectGetMaxY(txt_view.frame)+1, 70, 15);
            
            
        }
        
        lblmaxDish.text = @"(500 char. max.)";
        lblmaxDish.backgroundColor=[UIColor clearColor];
        lblmaxDish.textColor=[UIColor lightGrayColor];
        lblmaxDish.numberOfLines = 0;
        lblmaxDish.font = [UIFont fontWithName:kFont size:8];
        [img_backgroundimagesDish addSubview:lblmaxDish];
        
    
}
-(void)Next_btnClick{
    
//    MenuScreenEditMealVC *menuScreenEdit = [[MenuScreenEditMealVC alloc]init];
//    [self presentViewController:menuScreenEdit animated:NO completion:nil];
//    //    [self.navigationController pushViewController:menuScreenEdit animated:NO];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    //   [self AFChefMyProfileDishimages];
    [self AFChefMyProfileDish];
}


#pragma mark UiCollectionView Delegate Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView==collView_serviceDirectory) {
        return [collectionarrImages count];

    } else {
        return [collectionarrImagesDish count];
    }
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView1 dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    

    
    if (collectionView1==collView_serviceDirectory) {
        
        UIImageView *img_backGnd = [[UIImageView alloc]init];
        
        
        if (IS_IPHONE_6Plus){
            img_backGnd.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        else if (IS_IPHONE_6){
            img_backGnd.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        else  {
            img_backGnd.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        img_backGnd.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:img_backGnd];
        
        
        UIImageView *img_Images = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus){
            img_Images.frame = CGRectMake(28, 0, 55,55);
            
        }
        else if (IS_IPHONE_6){
            img_Images.frame = CGRectMake(28, 0,55,55);
            
        }
        else  {
            img_Images.frame = CGRectMake(25, 0, 40,40);
            
        }
        
        [img_Images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[collectionarrImages objectAtIndex:indexPath.row]]]];
        [img_Images setUserInteractionEnabled:YES];
        [img_Images setContentMode:UIViewContentModeScaleAspectFill];
        [img_Images setClipsToBounds:YES];
        [img_Images setUserInteractionEnabled:YES];
        [img_backGnd addSubview:img_Images];
        
        
        UILabel  * lbl_headings = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus){
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(img_Images.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        else if (IS_IPHONE_6){
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(img_Images.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        else  {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(img_Images.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        
        if (indexPath.row == 0) {
            lbl_headings.text = @"Halal";
        }else if (indexPath.row==1){
            lbl_headings.text=@"Kasher";
        }else{
            lbl_headings.text = @"Organic";
        }
        lbl_headings.backgroundColor=[UIColor clearColor];
        lbl_headings.textColor=[UIColor blackColor];
        lbl_headings.textAlignment=NSTextAlignmentCenter;
        lbl_headings.font = [UIFont fontWithName:kFont size:9];
        [img_backGnd addSubview:lbl_headings];
        
        UIImageView *img_Tikmark=[[UIImageView alloc]init];
        if (indexPath.row ==2)
        {
            img_Tikmark.image=[UIImage imageNamed:@""];
            
        }
        else{
            if (IS_IPHONE_6Plus){
                img_Tikmark.frame=CGRectMake(CGRectGetMaxX(img_Images.frame)-10, CGRectGetMaxY(img_Images.frame)+2, 12, 10);
            }
            else if (IS_IPHONE_6){
                img_Tikmark.frame=CGRectMake(CGRectGetMaxX(img_Images.frame)-10, CGRectGetMaxY(img_Images.frame)+2, 12, 10);
            }
            else  {
                img_Tikmark.frame=CGRectMake(CGRectGetMaxX(lbl_headings.frame)-20, CGRectGetMaxY(img_Images.frame)+2, 12, 10);
            }
            
            
            img_Tikmark.image=[UIImage imageNamed:@"img_correct@2x.png"];
            
        }
        [img_Tikmark setUserInteractionEnabled:YES];
        img_Tikmark.backgroundColor=[UIColor clearColor];
        
        [img_backGnd addSubview:img_Tikmark];
        

    } else {
        UIImageView *img_backGnd = [[UIImageView alloc]init];
        
        
        if (IS_IPHONE_6Plus){
            img_backGnd.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        else if (IS_IPHONE_6){
            img_backGnd.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        else  {
            img_backGnd.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        img_backGnd.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:img_backGnd];
        
        
        UIImageView *img_Images = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus){
            img_Images.frame = CGRectMake(28, 0, 55,55);
            
        }
        else if (IS_IPHONE_6){
            img_Images.frame = CGRectMake(28, 0,55,55);
            
        }
        else  {
            img_Images.frame = CGRectMake(25, 0, 40,40);
            
        }
        
        [img_Images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[collectionarrImages objectAtIndex:indexPath.row]]]];
        [img_Images setUserInteractionEnabled:YES];
        [img_Images setContentMode:UIViewContentModeScaleAspectFill];
        [img_Images setClipsToBounds:YES];
        [img_Images setUserInteractionEnabled:YES];
        [img_backGnd addSubview:img_Images];
        
        
        UILabel  * lbl_headings = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus){
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(img_Images.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        else if (IS_IPHONE_6){
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(img_Images.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        else  {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(img_Images.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        
        if (indexPath.row == 0) {
            lbl_headings.text = @"Halal";
        }else if (indexPath.row==1){
            lbl_headings.text=@"Kasher";
        }else{
            lbl_headings.text = @"Organic";
        }
        lbl_headings.backgroundColor=[UIColor clearColor];
        lbl_headings.textColor=[UIColor blackColor];
        lbl_headings.textAlignment=NSTextAlignmentCenter;
        lbl_headings.font = [UIFont fontWithName:kFont size:9];
        [img_backGnd addSubview:lbl_headings];
        
        UIImageView *img_Tikmark=[[UIImageView alloc]init];
        if (indexPath.row ==2)
        {
            img_Tikmark.image=[UIImage imageNamed:@""];
            
        }
        else{
            if (IS_IPHONE_6Plus){
                img_Tikmark.frame=CGRectMake(CGRectGetMaxX(img_Images.frame)-10, CGRectGetMaxY(img_Images.frame)+2, 12, 10);
            }
            else if (IS_IPHONE_6){
                img_Tikmark.frame=CGRectMake(CGRectGetMaxX(img_Images.frame)-10, CGRectGetMaxY(img_Images.frame)+2, 12, 10);
            }
            else  {
                img_Tikmark.frame=CGRectMake(CGRectGetMaxX(lbl_headings.frame)-20, CGRectGetMaxY(img_Images.frame)+2, 12, 10);
            }
            
            
            img_Tikmark.image=[UIImage imageNamed:@"img_correct@2x.png"];
            
        }
        [img_Tikmark setUserInteractionEnabled:YES];
        img_Tikmark.backgroundColor=[UIColor clearColor];
        
        [img_backGnd addSubview:img_Tikmark];
        

    }
    

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView ==collView_serviceDirectory) {
        if (IS_IPHONE_6Plus){
            return CGSizeMake((WIDTH-46)/3, 70);
        }
        else if (IS_IPHONE_6){
            return CGSizeMake((WIDTH-46)/3, 70);
        }
        else  {
            return CGSizeMake((WIDTH-46)/3, 60);
        }

    } else {
        if (IS_IPHONE_6Plus){
            return CGSizeMake((WIDTH-46)/3, 70);
        }
        else if (IS_IPHONE_6){
            return CGSizeMake((WIDTH-46)/3, 70);
        }
        else  {
            return CGSizeMake((WIDTH-46)/3, 60);
        }
        

    }
    
        return CGSizeMake(0, 0);

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}

#pragma uibutton click events

-(void)Btn_MealClick:(UIButton *)sender
{
    view_Meal.hidden=NO;
    view_SignalDish.hidden=YES;
    btn_Meal.backgroundColor = [UIColor blackColor];
    btn_Signaldish.backgroundColor = [UIColor lightGrayColor];


}

-(void)btn_signalDishClick:(UIButton *)sender
{
    view_Meal.hidden=YES;
    view_SignalDish.hidden=NO;
    btn_Meal.backgroundColor = [UIColor lightGrayColor];
    btn_Signaldish.backgroundColor = [UIColor blackColor];


}
-(void)btn_imgupload:(UIButton *) sender
{
    str_pickingImage = @"img_backround";
    
    UIActionSheet *actionSheet_popupQuery = [[UIActionSheet alloc] initWithTitle:@"Select A Image From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Camera", nil];
    actionSheet_popupQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet_popupQuery showInView:self.view];
    
}
#pragma mark - ACTION SHEET DELEGATE

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 0)
    {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            UIImagePickerController*imgPicker = [[UIImagePickerController alloc] init];
            UIColor* color = [UIColor colorWithRed:46.0/255 green:127.0/255 blue:244.0/255 alpha:1];
            [imgPicker.navigationBar setTintColor:color];
            imgPicker.delegate = self;
            imgPicker.allowsEditing = YES;
            imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            [self presentViewController:imgPicker animated:NO completion:Nil];
        }
    }
    else if(buttonIndex == 1)
    {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController*imgPicker = [[UIImagePickerController alloc] init];
            UIColor* color = [UIColor colorWithRed:46.0/255 green:127.0/255 blue:244.0/255 alpha:1];
            [imgPicker.navigationBar setTintColor:color];
            imgPicker.delegate = self;
            imgPicker.allowsEditing = YES;
            imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:imgPicker animated:NO completion:Nil];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Not86" message:@"Device Does Not Support Camera" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //  [UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
            [alert show];
        }
    }
}

#pragma mark - IMAGEPICKER DELEGATE METHODS
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *imageOriginal =  [info objectForKey:UIImagePickerControllerEditedImage];
    
    if ( [[NSString stringWithFormat:@"%@",str_pickingImage]isEqualToString:@"img_backround"])
    {
        imgData =  UIImageJPEGRepresentation(imageOriginal,0.5);
        img_backround.image = imageOriginal;
        
    }
       
    [picker dismissViewControllerAnimated:YES completion:Nil];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:Nil];
}






# pragma mark AFChefMyProfileDishimages method

-(void)AFChefMyProfileDish
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            
                            @"uid"                                   :   @"563",
                            @"title"                                 :   txt_MealTitle.text,
                            @"description"                           :   txt_view.text,
                            @"cuisine_category_id"                   :   @"80",
                            @"dish_cuisine_id"                       :   @"84||85||86||87",
                            @"course_id"                             :   @"563",
                            @"dish_tags"                             :   @"563",
                            @"dish_price"                            :   @"563",
                            @"dietary_restrictions"                  :   @"563",
                            @"type"                                  :   @"Dish",
                            @"dish_id"                               :   @"563",
                            @"dish_image"                            :   @"563",

                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    
    
    
    NSMutableURLRequest *request = nil;
    
    if ( img_backround!= nil)
    {
        request = [httpClient multipartFormRequestWithMethod:@"POST" path:kChefProfileMealDish parameters:params constructingBodyWithBlock:^(id <AFMultipartFormData>formData)
                   {
                       NSTimeInterval timeInterval = [NSDate timeIntervalSinceReferenceDate];
                       
                       [formData appendPartWithFileData: imgData name:@"img_backround" fileName:[NSString stringWithFormat:@"%lf-image.png",timeInterval] mimeType:@"image/jpeg"];
                       
                       
                       
                   }];
    }
    else
    {
        request = [httpClient requestWithMethod:@"POST"
                                           path:kChefProfileMealDish
                                     parameters:params];
    }

//    
//    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
//                                                            path:kChefProfileMealDish
//                                                      parameters:params];
//    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefProfileDish:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFChefMyProfileDish];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefProfileDish :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
//        for (int i=0; i<[[[TheDict valueForKey:@"DishsImagesList"] objectAtIndex:0] count]; i++)
//        {
//            [ary_Menuimages addObject:[[TheDict valueForKey:@"DishsImagesList"] objectAtIndex:0]];
//            
//        }
//        NSMutableDictionary *dict_exp=[[NSMutableDictionary alloc] init];
//        [dict_exp setValue:@"" forKey:@"DishUID"];
//        [dict_exp setValue:@"" forKey:@"DishID"];
//        [dict_exp setValue:@"" forKey:@"DishName"];
//        [dict_exp setValue:@"" forKey:@"DishImage"];
//        [ary_Menuimages addObject:dict_exp];
//        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
}










- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
