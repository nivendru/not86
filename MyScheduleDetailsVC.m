//
//  MyScheduleDetailsVC.m
//  Not86
//
//  Created by Interworld on 04/12/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "MyScheduleDetailsVC.h"
#import "EditMyScheduleVC.h"
#import "RepeatMyScheduleVC.h"


@interface MyScheduleDetailsVC ()<UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate,UITableViewDataSource, UICollectionViewDataSource,UICollectionViewDelegate>
{
    UIImageView *headerImageView;
    UITableView *table_DetailSchedule;
    
    NSMutableArray *ary_MainCount;
     NSMutableArray *ary_DishList;
    NSMutableArray *ary_DietaryList;
    NSMutableArray *ary_CuisinesList;
    NSMutableArray *ary_CollectionCount;
    RepeatMyScheduleVC *reapetVCobj;
    AppDelegate *delegate;
    NSString*str_date;
    
    EditMyScheduleVC *vcObj;
    UILabel *serving_timeings;
    
    
}
@end

@implementation MyScheduleDetailsVC
@synthesize str_ServeDate, str_ServeId, str_ServeTime;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    ary_MainCount = [NSMutableArray new];
    ary_DietaryList = [NSMutableArray new];
    ary_CuisinesList = [NSMutableArray new];
    ary_DishList = [NSMutableArray new];
     ary_CollectionCount = [NSMutableArray new];
    str_date = [NSString new];
    
    
    [self addHeaderView];
    [self IntegrateBodyDesign];
}

-(void)viewWillAppear:(BOOL)animated
{
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [ary_MainCount removeAllObjects];
    [table_DetailSchedule reloadData];
    
    [self AFDetailScheduleList];
    
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate* date = [dateFormatter dateFromString:str_ServeDate];
    
    [dateFormatter setDateFormat:@"dd MMM"];
    NSString* myNiceLookingDate = [dateFormatter stringFromDate:date];
    str_date = [NSString stringWithFormat:@"%@  %@",myNiceLookingDate, str_ServeTime];
    serving_timeings.text = str_date;
}

-(void)addHeaderView
{
    headerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 45)];
    [headerImageView setUserInteractionEnabled:YES];
    headerImageView.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:headerImageView];
    
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [icon_back addTarget:self action:@selector(click_on_back_arrow:) forControlEvents:UIControlEventTouchUpInside];
    [headerImageView   addSubview:icon_back];
    
    
    UILabel  * lbl_Title = [[UILabel alloc]initWithFrame:CGRectMake(50,5, WIDTH-110,35)];
    lbl_Title.text = @"My Schedule";
    lbl_Title.backgroundColor=[UIColor clearColor];
    lbl_Title.textColor=[UIColor whiteColor];
    lbl_Title.textAlignment=NSTextAlignmentLeft;
    lbl_Title.font = [UIFont fontWithName:kFont size:18];
    [headerImageView addSubview:lbl_Title];
    
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 5, 35, 35)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-logo.png"];
    [headerImageView addSubview:img_logo];
    
}


-(void)IntegrateBodyDesign
{
   
    UIImageView *img_calender = [[UIImageView alloc]init];
    img_calender.frame = CGRectMake(7,CGRectGetMaxY(headerImageView.frame)+10, WIDTH-12, 50);
    [img_calender  setImage:[UIImage imageNamed:@"bg1"]];
//    img_calender.backgroundColor = [UIColor whiteColor];
    [img_calender  setUserInteractionEnabled:YES];
//    img_calender.layer.borderWidth = 1.0;
    [ self.view addSubview:img_calender ];
    
    
    UIButton *btn_Back_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_Back_arrow.frame = CGRectMake(12,15, 13, 20);
    [btn_Back_arrow addTarget:self action:@selector(btn_Back_arrow_Click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_Back_arrow setImage:[UIImage imageNamed:@"icon-back@2x"] forState:UIControlStateNormal];
    [img_calender   addSubview:btn_Back_arrow];
    
    
    
    UIImageView *servingimges = [[UIImageView alloc]init];
    servingimges.frame =  CGRectMake(CGRectGetMaxX(btn_Back_arrow.frame)+15,10, 30, 30);
//    [servingimges setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_serving_img objectAtIndex:indexPath.row]]]];
    [servingimges setImage:[UIImage imageNamed:@"img_Timehandle.png"]];
    servingimges.backgroundColor =[UIColor clearColor];
    [servingimges setUserInteractionEnabled:YES];
    [img_calender addSubview:servingimges];
    
   
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate* date = [dateFormatter dateFromString:str_ServeDate];

   [dateFormatter setDateFormat:@"dd MMM"];
    NSString* myNiceLookingDate = [dateFormatter stringFromDate:date];
    
    serving_timeings = [[UILabel alloc]init];
    serving_timeings.frame = CGRectMake(CGRectGetMaxX(servingimges.frame)+10, 10, WIDTH-110, 30);
    str_date = [NSString stringWithFormat:@"%@  %@",myNiceLookingDate, str_ServeTime];
    serving_timeings.text = str_date;
    serving_timeings .font = [UIFont fontWithName:kFontBold size:13];
    serving_timeings .textColor = [UIColor blackColor];
    serving_timeings .backgroundColor = [UIColor clearColor];
    [img_calender addSubview:serving_timeings ];

    
    
    table_DetailSchedule = [[UITableView alloc] init ];
    table_DetailSchedule.frame  = CGRectMake(10,CGRectGetMaxY(img_calender.frame)+10,WIDTH-20,HEIGHT-190);
    [table_DetailSchedule setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_DetailSchedule.delegate = self;
    table_DetailSchedule.dataSource = self;
    table_DetailSchedule.showsVerticalScrollIndicator = NO;
//    table_DetailSchedule.layer.borderWidth = 1.0;
    table_DetailSchedule.backgroundColor = [UIColor clearColor];
    [self.view addSubview:table_DetailSchedule];
    
    UIButton   *btn_Repeat = [[UIButton alloc] init];
    btn_Repeat.frame = CGRectMake(30, CGRectGetMaxY(table_DetailSchedule.frame)+15, (WIDTH-72)/2.0, 45);
    btn_Repeat.layer.cornerRadius=4.0f;
    [btn_Repeat setTitle:@"Repeat" forState:UIControlStateNormal];
    [btn_Repeat setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Repeat setBackgroundColor:[UIColor colorWithRed:18.0/255.0 green:44.0/255.0 blue:86.0/255.0 alpha:1.0f]];
    btn_Repeat.titleLabel.font=[UIFont fontWithName:kFont size:16];
    [btn_Repeat setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Repeat addTarget:self action:@selector(btn_Repeat_Method:) forControlEvents:UIControlEventTouchUpInside ];
    [self.view addSubview:btn_Repeat];
    
    
    UIButton   *btn_Edit = [[UIButton alloc] init];
    btn_Edit.frame = CGRectMake(CGRectGetMaxX(btn_Repeat.frame)+15, CGRectGetMaxY(table_DetailSchedule.frame)+15, (WIDTH-72)/2.0, 45);
    btn_Edit.layer.cornerRadius=4.0f;
    [btn_Edit setTitle:@"Edit" forState:UIControlStateNormal];
    [btn_Edit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Edit setBackgroundColor:[UIColor colorWithRed:39.0/255.0 green:37.0/255.0 blue:48.0/255.0 alpha:1.0f]];
    btn_Edit.titleLabel.font=[UIFont fontWithName:kFont size:16];
    [btn_Edit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Edit addTarget:self action:@selector(btn_Edit_Method:) forControlEvents:UIControlEventTouchUpInside ];
    [self.view addSubview:btn_Edit];
    
}

#pragma mark  ---click_events

-(void)click_on_back_arrow:(UIButton *)sender
{
    NSLog(@"click_on_back_arrow");
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)btn_Repeat_Method:(UIButton *)sender
{
    NSLog(@"btn_Repeat_Method clcked");
    reapetVCobj = [RepeatMyScheduleVC new];
    reapetVCobj.str_ReapetSchedleDate = str_date;
    reapetVCobj.str_ServeID = str_ServeId;
    reapetVCobj.str_Times = str_ServeTime;
    [self presentViewController:reapetVCobj animated:NO completion:nil];
    
}

-(void)btn_Edit_Method:(UIButton *)sender
{
    NSLog(@"btn_Edit_Method clicked");
    vcObj = [EditMyScheduleVC new];
    vcObj.ary_DetailSchedule = ary_MainCount;
    vcObj.str_selecteddate = str_date;
    vcObj.str_ServeId = str_ServeId;
    [self presentViewController:vcObj animated:NO completion:nil];
}

-(void)btn_Back_arrow_Click:(UIButton *)sender
{
    NSLog(@"btn_Back_arrow_Click clicked");
    [self dismissViewControllerAnimated:NO completion:nil];
}


#pragma mark  ---TableViewDelegate


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ary_MainCount count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
        for (UIImageView *view in cell.contentView.subviews)
        {
            [view removeFromSuperview];
        }
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-5, 150);
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    img_cellBackGnd.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd setUserInteractionEnabled:YES];
//    img_cellBackGnd.layer.borderWidth = 1.0;
    [cell.contentView addSubview:img_cellBackGnd];
    
    
    
    UIImageView *img_dish = [[UIImageView alloc] init];
    NSString *ImagePath1 =[NSString stringWithFormat:@"%@",[[[ary_MainCount objectAtIndex:indexPath.row]valueForKey:@"DishlistDetail"]valueForKey:@"dishimage"]];
    [img_dish setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@"img-user@2x"]];
    [img_cellBackGnd addSubview:img_dish];
        
    UILabel *dish_name = [[UILabel alloc]init];
    //        dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
//    dish_name.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"DishName"];
//    dish_name.font = [UIFont fontWithName:kFontBold size:8];
//    dish_name.text = @"Dish Name";
    dish_name.text =[NSString stringWithFormat:@"%@",[[[ary_MainCount objectAtIndex:indexPath.row]valueForKey:@"DishlistDetail"]valueForKey:@"dishname"] ];
    dish_name.textColor = [UIColor blackColor];
    
    dish_name.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:dish_name];
    
//    
//   UICollectionViewFlowLayout  *layout=[[UICollectionViewFlowLayout alloc] init];
//    UICollectionView *collView_serviceDirectory = [[UICollectionView alloc] initWithFrame:CGRectMake(30,0,WIDTH-65,70)
//                                                   collectionViewLayout:layout];
//    
//    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
//    [collView_serviceDirectory setDataSource:self];
//    [collView_serviceDirectory setDelegate:self];
//    collView_serviceDirectory.scrollEnabled = YES;
//    collView_serviceDirectory.showsVerticalScrollIndicator = NO;
//    collView_serviceDirectory.showsHorizontalScrollIndicator = NO;
//    collView_serviceDirectory.pagingEnabled = NO;
//    [collView_serviceDirectory registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
//    [collView_serviceDirectory setBackgroundColor:[UIColor clearColor]];
//    layout.minimumInteritemSpacing = 0;
//    layout.minimumLineSpacing = 0;
//    collView_serviceDirectory.layer.borderWidth = 1.0;
//    collView_serviceDirectory.userInteractionEnabled = YES;
//    [img_cellBackGnd addSubview:collView_serviceDirectory];
//
    
    UIScrollView*delivery_Scroll;
    delivery_Scroll = [[UIScrollView alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+7, CGRectGetMaxY(dish_name.frame)+55, 180, 30);
    }
    else  if (IS_IPHONE_6)
    {
        
        delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+7, CGRectGetMaxY(dish_name.frame)+55, 180, 30);
    }
    else  if (IS_IPHONE_5)
    {
        
        delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+2, CGRectGetMaxY(dish_name.frame)+55, 140, 30);
    }
    else
    {
        delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+7, CGRectGetMaxY(dish_name.frame)+40, 120, 30);
    }
    
    delivery_Scroll.backgroundColor = [UIColor whiteColor];
    delivery_Scroll.bounces=YES;
    //delivery_Scroll.layer.borderWidth = 1.0;
    delivery_Scroll.layer.borderColor = [UIColor blackColor].CGColor;
    delivery_Scroll.showsVerticalScrollIndicator = NO;
    delivery_Scroll.showsHorizontalScrollIndicator = NO;
    
    //delivery_Scroll.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [delivery_Scroll setScrollEnabled:YES];
    delivery_Scroll.userInteractionEnabled = YES;
    [cell.contentView addSubview:delivery_Scroll];


    
    UILabel *doller_rate = [[UILabel alloc]init];
    //   doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);         doller_rate.text = @"$14.90";
    doller_rate.font = [UIFont fontWithName:kFontBold size:16];
    doller_rate.text = [NSString stringWithFormat:@"$%@",[[[ary_MainCount objectAtIndex:indexPath.row]valueForKey:@"DishlistDetail"]valueForKey:@"dishprice"]];
    doller_rate.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    doller_rate.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:doller_rate];
    
    
    
    UIImageView *img_line = [[UIImageView alloc] init];
    //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
    [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
    [img_cellBackGnd addSubview:img_line];
    
    
    UIImageView *icon_take = [[UIImageView alloc] init];
    //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
    if ([[[ary_MainCount objectAtIndex:indexPath.row]valueForKey:@"Take-out"] isEqualToString:@"1"])
    {
        [icon_take setImage:[UIImage imageNamed:@"take-icon@2x.png"]];

    }
    else
    {
        [icon_take setImage:[UIImage imageNamed:@""]];

    }
//    [icon_take setImage:[UIImage imageNamed:@"take-icon@2x.png"]];
    [img_cellBackGnd addSubview:icon_take];
    
    
    UIImageView *icon_deliver = [[UIImageView alloc] init];
    //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
    if ([[[ary_MainCount objectAtIndex:indexPath.row]valueForKey:@"Delivery"] isEqualToString:@"1"])
    {
        [icon_deliver setImage:[UIImage imageNamed:@"deliver-icon@2x.png"]];

    }
    else
    {
        [icon_deliver setImage:[UIImage imageNamed:@""]];
    }
    [img_cellBackGnd addSubview:icon_deliver];
    
    
    UIImageView *icon_Dine = [[UIImageView alloc] init];
    //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
    if ([[[ary_MainCount objectAtIndex:indexPath.row]valueForKey:@"Dine-in"] isEqualToString:@"1"])
    {
        [icon_Dine setImage:[UIImage imageNamed:@"dine-in-img@2x"]];
    }
    else
    {
        [icon_Dine setImage:[UIImage imageNamed:@""]] ;
    }
//    [icon_Dine setImage:[UIImage imageNamed:@"dine-in-img@2x"]];
    [img_cellBackGnd addSubview:icon_Dine];
    
    
    
    UIImageView *img_btn_seving_Now = [[UIImageView alloc] init];
    //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
    if ([[[[ary_MainCount objectAtIndex:indexPath.row]valueForKey:@"DishlistDetail"]valueForKey:@"Serving_Type"]isEqualToString:@"serve_later"])
    {
         [img_btn_seving_Now setImage:[UIImage imageNamed:@"icon-serving-time@2x"]];
    }
    else if ([[[[ary_MainCount objectAtIndex:indexPath.row]valueForKey:@"DishlistDetail"]valueForKey:@"Serving_Type"]isEqualToString:@"on_request"])
    {
         [img_btn_seving_Now setImage:[UIImage imageNamed:@"iicon-on_reuest@2x"]];
    }
    else
    {
        [img_btn_seving_Now setImage:[UIImage imageNamed:@"now-icon@2x.png"]];
    }
//    [img_btn_seving_Now setImage:[UIImage imageNamed:@"now-icon@2x.png"]];
    [img_cellBackGnd addSubview:img_btn_seving_Now];
    
    
//    UIButton *img_btn_seving_Now = [UIButton buttonWithType:UIButtonTypeCustom];
//    //        img_btn_seving_Now.text = [[array_items_name objectAtIndex:indexPath.row] valueForKey:@"likes"];
////    [img_btn_seving_Now addTarget:self action:@selector(click_on_seving_now_btn:) forControlEvents:UIControlEventTouchUpInside];
//    [img_btn_seving_Now setImage:[UIImage imageNamed:@"now-icon@2x.png"] forState:UIControlStateNormal];
//    [img_cellBackGnd   addSubview:img_btn_seving_Now];
    
    
    UIButton *img_btn_chef_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    //    img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
    //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
//    [img_btn_chef_menu addTarget:self action:@selector(click_on_icon_chef_menu_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_btn_chef_menu setImage:[UIImage imageNamed:@"chef-menu-icon@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:img_btn_chef_menu];
    
    
    
    UILabel *quantity = [[UILabel alloc]init];
    //  quantity.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
    quantity.font = [UIFont fontWithName:kFontBold size:10];
    quantity.text = [NSString stringWithFormat:@"%@", [[[ary_MainCount objectAtIndex:indexPath.row]valueForKey:@"DishlistDetail"]valueForKey:@"quantity"]];
    //    likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
    quantity.textColor = [UIColor whiteColor];
    quantity.textAlignment = NSTextAlignmentCenter;
    quantity.backgroundColor = [UIColor blackColor];
    [img_btn_chef_menu addSubview:quantity];
    
    
    UILabel *total_Servings = [[UILabel alloc]init];
//    total_Servings.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)+2,CGRectGetMaxY(img_line.frame)+5,100, 12);
    total_Servings.text = @"Total Servings";
    //    likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
    total_Servings.font = [UIFont fontWithName:kFontBold size:11];
    //    total_Servings.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    total_Servings.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:total_Servings];
    
    
    UILabel *Serving_NO = [[UILabel alloc]init];
//    Serving_NO.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)+2,CGRectGetMaxY(total_Servings.frame),100, 12);  Total_Serving
     Serving_NO.font = [UIFont fontWithName:kFont size:11];
    Serving_NO.text = [NSString stringWithFormat:@"%@", [[[ary_MainCount objectAtIndex:indexPath.row]valueForKey:@"DishlistDetail"]valueForKey:@"Total_Serving"]];
    
    //    likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
   
    //    Serving_NO.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    Serving_NO.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:Serving_NO];
    
    
    UIButton *icon_thumb = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+5, 20, 20);
    //icon_thumb .backgroundColor = [UIColor clearColor];
//    [icon_thumb addTarget:self action:@selector(click_on_icon_thumb_btn:) forControlEvents:UIControlEventTouchUpInside];
    [icon_thumb setImage:[UIImage imageNamed:@"thumb-icon@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_thumb];
    
    
    UILabel *likes = [[UILabel alloc]init];
    //  likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
     likes.font = [UIFont fontWithName:kFont size:10];
        likes.text = [NSString stringWithFormat:@"%@%%", [[[ary_MainCount objectAtIndex:indexPath.row]valueForKey:@"DishlistDetail"]valueForKey:@"likes"]];
//    likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
    likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    likes.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:likes];
    


    
    
    if (IS_IPHONE_6Plus)
    {
        
        img_dish.frame = CGRectMake(7,12, 80,  80 );
        //    btn_dish.frame=CGRectMake(0, 0, 80, 80);
        //    img_favorite.frame = CGRectMake(3,62, 30, 30);
        //    img_round_red.frame = CGRectMake(62, 3, 30,30);
        //    round_red_val .frame = CGRectMake(0,0,30, 30);
        dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,12,WIDTH-125, 20);
        delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+7, CGRectGetMaxY(dish_name.frame)+40, 180, 30);

       // collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+7, CGRectGetMaxY(dish_name.frame)+30, 120, 30);
        doller_rate.frame = CGRectMake(WIDTH-75,75,200, 15);
        img_line.frame = CGRectMake(10,CGRectGetMaxX(img_dish.frame)+15, WIDTH-40, 0.5 );
        icon_take.frame = CGRectMake(5, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        icon_deliver.frame = CGRectMake(CGRectGetMaxX(icon_take.frame)+2,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        icon_Dine.frame = CGRectMake(CGRectGetMaxX(icon_deliver.frame)+2,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_seving_Now.frame = CGRectMake(150,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_chef_menu.frame = CGRectMake(CGRectGetMaxX(img_btn_seving_Now.frame)+2,CGRectGetMaxY(img_line.frame)+12,  30, 20);
        quantity.frame = CGRectMake(5,5,20, 10);
        total_Servings.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)+2,CGRectGetMaxY(img_line.frame)+8,80, 12);
        Serving_NO.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)+2,CGRectGetMaxY(total_Servings.frame),80, 12);
        icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+10, 20, 20);
        likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,CGRectGetMaxY(img_line.frame)+12,100, 20);
        dish_name.font = [UIFont fontWithName:kFontBold size:15];
        
    }
    else if (IS_IPHONE_6)
    {

        
        img_dish.frame = CGRectMake(7,12, 80,  80 );
        //    btn_dish.frame=CGRectMake(0, 0, 80, 80);
        //    img_favorite.frame = CGRectMake(3,62, 30, 30);
        //    img_round_red.frame = CGRectMake(62, 3, 30,30);
        //    round_red_val .frame = CGRectMake(0,0,30, 30);
        dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,12,WIDTH-125, 20);
        delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+7, CGRectGetMaxY(dish_name.frame)+40, 180, 30);

       // collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+7, CGRectGetMaxY(dish_name.frame)+30, 120, 30);
        doller_rate.frame = CGRectMake(WIDTH-75,75,200, 15);
        img_line.frame = CGRectMake(10,CGRectGetMaxX(img_dish.frame)+15, WIDTH-40, 0.5 );
        icon_take.frame = CGRectMake(5, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        icon_deliver.frame = CGRectMake(CGRectGetMaxX(icon_take.frame)+2,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        icon_Dine.frame = CGRectMake(CGRectGetMaxX(icon_deliver.frame)+2,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_seving_Now.frame = CGRectMake(125,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_chef_menu.frame = CGRectMake(CGRectGetMaxX(img_btn_seving_Now.frame)+2,CGRectGetMaxY(img_line.frame)+12,  30, 20);
        quantity.frame = CGRectMake(5,5,20, 10);
        total_Servings.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)+2,CGRectGetMaxY(img_line.frame)+8,80, 12);
        Serving_NO.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)+2,CGRectGetMaxY(total_Servings.frame),80, 12);
        icon_thumb.frame = CGRectMake(290,CGRectGetMaxY(img_line.frame)+10, 20, 20);
        likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,CGRectGetMaxY(img_line.frame)+12,100, 20);
        dish_name.font = [UIFont fontWithName:kFontBold size:15];
        
    }
    else if (IS_IPHONE_5)
    {
        img_dish.frame = CGRectMake(7,12, 80,  80 );
        //    btn_dish.frame=CGRectMake(0, 0, 80, 80);
        //    img_favorite.frame = CGRectMake(3,62, 30, 30);
        //    img_round_red.frame = CGRectMake(62, 3, 30,30);
        //    round_red_val .frame = CGRectMake(0,0,30, 30);
        dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,12,WIDTH-125, 20);
        delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+7, CGRectGetMaxY(dish_name.frame)+40, 140, 30);

       // collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+7, CGRectGetMaxY(dish_name.frame)+30, 120, 30);
        doller_rate.frame = CGRectMake(WIDTH-75,75,200, 15);
        img_line.frame = CGRectMake(10,CGRectGetMaxX(img_dish.frame)+15, WIDTH-40, 0.5 );
        icon_take.frame = CGRectMake(5, CGRectGetMaxY(img_line.frame)+5, 28, 30);
        icon_deliver.frame = CGRectMake(CGRectGetMaxX(icon_take.frame)+2,CGRectGetMaxY(img_line.frame)+5,  28, 30);
        icon_Dine.frame = CGRectMake(CGRectGetMaxX(icon_deliver.frame)+2,CGRectGetMaxY(img_line.frame)+5,  28, 30);
        img_btn_seving_Now.frame = CGRectMake(100,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_chef_menu.frame = CGRectMake(CGRectGetMaxX(img_btn_seving_Now.frame)+2,CGRectGetMaxY(img_line.frame)+12,  30, 20);
        quantity.frame = CGRectMake(5,5,20, 10);
        total_Servings.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)+2,CGRectGetMaxY(img_line.frame)+8,80, 12);
        Serving_NO.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)+2,CGRectGetMaxY(total_Servings.frame),80, 12);
        icon_thumb.frame = CGRectMake(248,CGRectGetMaxY(img_line.frame)+10, 20, 20);
        likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,CGRectGetMaxY(img_line.frame)+12,100, 20);
        
        dish_name.font = [UIFont fontWithName:kFontBold size:15];
    }
    else
    {
        
        img_dish.frame = CGRectMake(7,12, 80,  80 );
        //    btn_dish.frame=CGRectMake(0, 0, 80, 80);
        //    img_favorite.frame = CGRectMake(3,62, 30, 30);
        //    img_round_red.frame = CGRectMake(62, 3, 30,30);
        //    round_red_val .frame = CGRectMake(0,0,30, 30);
        dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,12,WIDTH-125, 20);
        delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+7, CGRectGetMaxY(dish_name.frame)+30, 120, 30);

       // collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+7, CGRectGetMaxY(dish_name.frame)+30, 120, 30);
        doller_rate.frame = CGRectMake(WIDTH-75,75,200, 15);
        img_line.frame = CGRectMake(10,CGRectGetMaxX(img_dish.frame)+15, WIDTH-40, 0.5 );
        icon_take.frame = CGRectMake(5, CGRectGetMaxY(img_line.frame)+5, 28, 30);
        icon_deliver.frame = CGRectMake(CGRectGetMaxX(icon_take.frame)+2,CGRectGetMaxY(img_line.frame)+5,  28, 30);
        icon_Dine.frame = CGRectMake(CGRectGetMaxX(icon_deliver.frame)+2,CGRectGetMaxY(img_line.frame)+5,  28, 30);
        img_btn_seving_Now.frame = CGRectMake(100,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_chef_menu.frame = CGRectMake(CGRectGetMaxX(img_btn_seving_Now.frame)+2,CGRectGetMaxY(img_line.frame)+12,  30, 20);
        quantity.frame = CGRectMake(5,5,20, 10);
        total_Servings.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)+2,CGRectGetMaxY(img_line.frame)+8,80, 12);
        Serving_NO.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)+2,CGRectGetMaxY(total_Servings.frame),80, 12);
        icon_thumb.frame = CGRectMake(248,CGRectGetMaxY(img_line.frame)+10, 20, 20);
        likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,CGRectGetMaxY(img_line.frame)+12,100, 20);
        dish_name.font = [UIFont fontWithName:kFontBold size:15];
    }
    

    
    // collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+7, CGRectGetMaxY(dish_name.frame)+30, 120, 30);
//    collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+7, CGRectGetMaxY(dish_name.frame)+30, 120, 30);
//    collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+7, CGRectGetMaxY(dish_name.frame)+30, 120, 30);
//    collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+7, CGRectGetMaxY(dish_name.frame)+30, 120, 30);
       //[(NSDictionary *) [[ary_maintosave  objectAtIndex:0] valueForKey:@"Delivery_Address"] count]
    
    int totalPage;
    
    totalPage = (int)[[[[ary_MainCount objectAtIndex:indexPath.row] valueForKey:@"DishlistDetail"] valueForKey:@"field_dish_dietary_restrictions"] count];
    [delivery_Scroll setContentSize:CGSizeMake(40*totalPage, delivery_Scroll.frame.size.height)];
    
    
    
    for (int j = 0; j<totalPage; j++)
    {
        
        
        UIImageView *img_non_veg = [[UIImageView alloc] init];
        img_non_veg.frame = CGRectMake(20*j,0, 18, 18);
        
        NSString *url_Img = [NSString stringWithFormat: @"%@", [[[[[ary_MainCount objectAtIndex:indexPath.row] valueForKey:@"DishlistDetail"] valueForKey:@"field_dish_dietary_restrictions"] objectAtIndex:j] valueForKey:@"resImage"]];
        
        [img_non_veg setImageWithURL:[NSURL URLWithString:url_Img] placeholderImage:nil];

        UIImage *scaleImage = [self imageWithImage: img_non_veg.image convertToSize:CGSizeMake(75, 75)];
        img_non_veg.image = scaleImage;
        
       
        img_non_veg.backgroundColor=[UIColor clearColor];
        [delivery_Scroll addSubview:img_non_veg];
        
        
        //        UIButton*get_Deliver_Address_Btn;
        //        get_Deliver_Address_Btn = [[UIButton alloc]init];
        //        get_Deliver_Address_Btn.frame = CGRectMake(0,25*j, delivery_Scroll.frame.size.width, 25);
        //        [get_Deliver_Address_Btn addTarget:self action:@selector(get_Deliver_Address_BtnMethod:) forControlEvents:UIControlEventTouchUpInside];
        //        get_Deliver_Address_Btn.tag = j;
        //        get_Deliver_Address_Btn.titleLabel.text = [NSString stringWithFormat:@"%d",indexPath.row];
        //        [get_Deliver_Address_Btn setBackgroundColor:[UIColor clearColor]];
        //        //   get_Deliver_Address_Btn.layer.borderWidth = 1.0;
        //        [delivery_Scroll addSubview: get_Deliver_Address_Btn];
    }
    
    
    return cell;
    
}


#pragma  mark ---CollectionViewDelegate&Datasources


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
//    return [ary_CuisinesList count]+1;
      return 4;
    //    return [ary_DishRestrictions count];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
  
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    if (indexPath == 0)
    {
        
    }
        UIImageView *img_backGnd = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        //[img_backGnd setImage:[UIImage imageNamed:@"img_BackGnd@2x.png"]];
        img_backGnd.backgroundColor = [UIColor clearColor];
        img_backGnd.layer.borderWidth = 1.0;
        [cell.contentView addSubview:img_backGnd];
        
        
        UIImageView *img_non_veg = [[UIImageView alloc] init];
        //        NSString *ImagePath1 =  [[NSString stringWithFormat:@"%@",[[array_dietary_img objectAtIndex:indexPath.row] valueForKey:@"resImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //    [img_non_veg setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@"img_merchantPlaceholder@2x.png"]];
        img_non_veg.frame = CGRectMake(1,1, 28, 28 );
        img_non_veg.backgroundColor=[UIColor grayColor];
        [img_backGnd addSubview:img_non_veg];
        
        
        UIImageView  *img_swapImg = [[UIImageView alloc]initWithFrame: CGRectMake(0,-2,50,20)];
        [img_swapImg setImage:[UIImage imageNamed:@"bg_nonveg@2x.png"]];
        img_swapImg .backgroundColor = [UIColor clearColor];
        [img_swapImg setUserInteractionEnabled:YES];
        [img_backGnd addSubview:img_swapImg];
        
        img_swapImg.hidden=NO;

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(30, 30);
    
}


# pragma mark DetailScheduleListServices

-(void) AFDetailScheduleList
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //    NSDictionary *params =@{
    //                            @"user_id"                          :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"UserId"],
    //                            @"type"                             :   @"",
    //                            @"value"                            :   @"",
    //
    //                            };
    
    NSDictionary *params =@{
                            @"uid"                          : [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"serve_date"                   : str_ServeDate,
                            @"serve_id"                     : str_ServeId
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kChefProfileMyScheduleDetailList
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseDetailScheduleList:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFDetailScheduleList];
                                         }
                                     }];
    [operation start];
    
}

-(void) ResponseDetailScheduleList :(NSDictionary * )TheDict
{
    [ary_MainCount removeAllObjects];
    [ary_CuisinesList removeAllObjects];
    [ary_DietaryList removeAllObjects];
    [ary_CollectionCount removeAllObjects];
    [ary_DishList removeAllObjects];
    NSLog(@"Dict is %@", TheDict);
    
//    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
//    {
//for (int i=0; i<[[TheDict valueForKey:@"DishlistDetail"]count]; i++)
//        {
//            
//            [ary_MainCount addObject:[[TheDict valueForKey:@"DishlistDetail"]objectAtIndex:i]];
//
//        }
//        
//        
//for (int i=0; i<[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ]count]; i++)
//        {
//            
//        [ary_DishList addObject:[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ] objectAtIndex:i]];
//        }
//        
//for (int i=0; i<[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ]valueForKey:@"cuisencetegory"]count]; i++)
//        {
//            
//            [ary_CuisinesList addObject:[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ]valueForKey:@"cuisencetegory"] objectAtIndex:i]];
//            
//        }
//        
//for (int i=0; i<[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ]valueForKey:@"field_dish_dietary_restrictions"]count]; i++)
//        {
//            
//            [ary_DietaryList addObject:[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ]valueForKey:@"field_dish_dietary_restrictions"] objectAtIndex:i]];
//            
//        }
//        
//         NSLog(@"Array of ary_MainCount %@",ary_MainCount );
//        NSLog(@"Array of ary_DishList %@",ary_DishList );
//         NSLog(@"Array of  ary_DietaryList %@",ary_DietaryList );
//         NSLog(@"Array of ary_CuisinesList %@",ary_CuisinesList );
    
//    }
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        for (int i=0; i<[[TheDict valueForKey:@"DishlistDetail"]count]; i++)
        {
            
        [ary_MainCount addObject:[[TheDict valueForKey:@"DishlistDetail"]objectAtIndex:i]];
            
        }

         NSLog(@"ary_MainCount is: %@", ary_MainCount);
        
         NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
for (int i=0; i<[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ]count]; i++)
    {
       
        [dict setValue:[NSString  stringWithFormat:@"%@",[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail"]objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
        
        [dict setValue:[NSString  stringWithFormat:@"%@",[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ] objectAtIndex:i] valueForKey:@"Total_Serving"]]  forKey:@"Total_Serving"];
        
        [dict setValue:[NSString  stringWithFormat:@"%@",[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ] objectAtIndex:i] valueForKey:@"chefid"]]  forKey:@"chefid"];
        
        [dict setObject:[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ] objectAtIndex:i] valueForKey:@"cuisencetegory"] forKey:@"cuisencetegory"];
        
        [dict setValue:[NSString  stringWithFormat:@"%@",[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ] objectAtIndex:i] valueForKey:@"dietry_res_other"]] forKey:@"dietry_res_other"];
        
        [dict setValue:[NSString  stringWithFormat:@"%@",[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ] objectAtIndex:i] valueForKey:@"dishcourse"]] forKey:@"dishcourse"];
        
        [dict setValue:[NSString  stringWithFormat:@"%@",[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ] objectAtIndex:i] valueForKey:@"dishid"]] forKey:@"dishid"];
        
        [dict setValue:[NSString  stringWithFormat:@"%@",[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ] objectAtIndex:i] valueForKey:@"dishimage"]] forKey:@"dishimage"];
        
        [dict setValue:[NSString  stringWithFormat:@"%@",[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ] objectAtIndex:i] valueForKey:@"dishimagebanner"]] forKey:@"dishimagebanner"];
        
        [dict setValue:[NSString  stringWithFormat:@"%@",[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ] objectAtIndex:i] valueForKey:@"dishname"]] forKey:@"dishname"];
    
        [dict setValue:[NSString  stringWithFormat:@"%@",[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ] objectAtIndex:i] valueForKey:@"dishprice"]] forKey:@"dishprice"];
        
        [dict setValue:[NSString  stringWithFormat:@"%@",[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ] objectAtIndex:i] valueForKey:@"dishtype"]] forKey:@"dishtype"];
        
        [dict setValue:[NSString  stringWithFormat:@"%@",[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ] objectAtIndex:i] valueForKey:@"distance"]] forKey:@"distance"];
        
        [dict setObject:[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ] objectAtIndex:i] valueForKey:@"field_dish_dietary_restrictions"] forKey:@"field_dish_dietary_restrictions"];
        
        [dict setValue:[NSString  stringWithFormat:@"%@",[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ] objectAtIndex:i] valueForKey:@"foodtype"]] forKey:@"foodtype"];
        
        [dict setValue:[NSString  stringWithFormat:@"%@",[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ] objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
        
        [dict setValue:[NSString  stringWithFormat:@"%@",[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ] objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
        
        [dict setValue:[NSString  stringWithFormat:@"%@",[[[[TheDict valueForKey:@"DishlistDetail"]valueForKey:@"DishlistDetail" ] objectAtIndex:i] valueForKey:@"quantity"]] forKey:@"quantity"];
        
    }
        
//        for ( int i = 0; i < [[dict valueForKey:@"field_dish_dietary_restrictions"] count]; i++)
//        {
//    
//    [ary_DietaryList addObject: [[dict valueForKey:@"field_dish_dietary_restrictions"] objectAtIndex:i]];
//            
//    [ary_CuisinesList addObject: [[dict valueForKey:@"cuisencetegory"] objectAtIndex:i]];
//            
//    ary_CollectionCount = [[ary_DietaryList arrayByAddingObjectsFromArray:ary_CuisinesList]mutableCopy ];
//    
//        }
        
        
        
        
        
        
        
        
        
         NSLog(@"ary_CollectionCount is: %@", ary_CollectionCount);
//         NSLog(@"ary_DishList is: %@", ary_CuisinesList);
        
//        for (int i=0; i<[[TheDict valueForKey:@"DishlistDetail"]count]; i++)
//        {
//            NSMutableDictionary *dict1=[[NSMutableDictionary alloc] init];
//            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishlistDetail"] objectAtIndex:i] valueForKey:@"Delivery"]] forKey:@"Delivery"];
//            
//            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishlistDetail"] objectAtIndex:i] valueForKey:@"Dine-in"]]  forKey:@"Dine-in"];
//            
//            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishlistDetail"] objectAtIndex:i] valueForKey:@"DishlistDetail"]]  forKey:@"DishlistDetail"];
//            
//            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishlistDetail"] objectAtIndex:i] valueForKey:@"ID"]] forKey:@"ID"];
//            
//            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishlistDetail"] objectAtIndex:i] valueForKey:@"ServID"]] forKey:@"ServID"];
//            
//            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishlistDetail"] objectAtIndex:i] valueForKey:@"Take-out"]] forKey:@"Take-out"];
//            
//            [ary_DietaryList addObject:dict1];
//        }
//        
//        NSLog(@"ary_MainCount is: %@", ary_DietaryList);
//        
//        for (int i=0; i<[[TheDict valueForKey:@"DishlistDetail"]count]; i++)
//        {
//            NSMutableDictionary *dict2=[[NSMutableDictionary alloc] init];
//            [dict2 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishlistDetail"] objectAtIndex:i] valueForKey:@"Delivery"]] forKey:@"Delivery"];
//            
//            [dict2 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishlistDetail"] objectAtIndex:i] valueForKey:@"Dine-in"]]  forKey:@"Dine-in"];
//            
//            [dict2 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishlistDetail"] objectAtIndex:i] valueForKey:@"DishlistDetail"]]  forKey:@"DishlistDetail"];
//            
//            [dict2 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishlistDetail"] objectAtIndex:i] valueForKey:@"ID"]] forKey:@"ID"];
//            
//            [dict2 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishlistDetail"] objectAtIndex:i] valueForKey:@"ServID"]] forKey:@"ServID"];
//            
//            [dict2 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishlistDetail"] objectAtIndex:i] valueForKey:@"Take-out"]] forKey:@"Take-out"];
//            
//            [ary_CuisinesList addObject:dict2];
//        }
//        
//        NSLog(@"ary_MainCount is: %@", ary_CuisinesList);
        
    }
    
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {

        NSLog(@"No item found on this date");
        
    }
    
    [table_DetailSchedule reloadData];
}


- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
