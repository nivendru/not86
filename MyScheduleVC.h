//
//  MyScheduleVC.h
//  Not86
//
//  Created by Interwld on 9/1/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"
#import "CKCalendarView.h"
@interface MyScheduleVC : JWSlideMenuViewController
@property(nonatomic, strong) CKCalendarView *calendar;
@property(nonatomic, strong) UILabel *dateLabel;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property(nonatomic, strong) NSDate *minimumDate;
@property(nonatomic, strong) NSArray *disabledDates;
@property(nonatomic,strong)NSString*str_comefrom;

@end
