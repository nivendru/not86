//
//  Header.h
//  Not86
//
//  Created by Interwld on 8/4/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#ifndef Not86_Header_h
#define Not86_Header_h



#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

//#define kFontbold @"century gothic bold"
//Helvetica LT Black.ttf



//#define KFontBlack @"HelveticaNeue-CondensedBlack"
#define KFontSizeHTML UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? @"22px" : @"20px"
#define kColorGreen [UIColor colorWithRed:107.0/255.0 green:196.0/255.0 blue:13.0/255.0 alpha:1.0]
#define kColorGrayTextfield [UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:36.0/255.0 alpha:1.0]

#define HEIGHT    self.view.frame.size.height
#define WIDTH     self.view.frame.size.width
#define HEADER_HEIGHT 40

#define kAppid                       @"1880711225486754"


//#define kBaseUrl                    @"http://sgappstore.com/not86/"
#define kBaseUrl                    @"http://not86live.elasticbeanstalk.com/"


#define kUserLogin                     @"webservices/user-login.json"

#define kFacebookLogin                 @"webservices/fb-login.json"
#define kUserRegistration              @"w1/user-register.json"
#define kForgetPassword                @"webservices/forget_password.json"
#define KChefSignUp                    @"webservices/user-register.json"
#define kUserProfileInfo               @"webservices/view-profile.json"
#define kUserProfileAddress            @"webservices/cart-address-list.json"
#define kUserSearchFoodNow             @"webservices/food-now.json"
#define kUserProfileFavorites          @"webservices/favorites-chef.json"
#define kUserNotificationsList         @"webservices/notification-list.json"
#define kUserNotificationsSettings     @"webservices/settings-status.json"

#define kUserDiningCart                @"webservices/cart-list.json"
#define kUserDishLists                 @"webservices/alldishes_list.json"
#define kUserUserFavoritesDish         @"webservices/favorite-dish-chef.json"

#define kUserDishDetails               @"webservices/dish-detail.json"
#define kUserAddtoCart                 @"webservices/add-to-cart.json"
#define AFChefSignUpFirst              @"webservices/add-chef-profile.json"
#define AFChefSigUpHcountry            @"webservices/get_countrylist.json"
#define AFChefSigUpHcity               @"webservices/get_citylist.json"
#define AFChefSigUpCuisineCategoryList @"webservices/cuisine-category.json"
#define AFChefSigUpCuisineList         @"webservices/cuisine-list.json"
#define kForgetPassword                @"webservices/forget_password.json"

#define AFChefServeNowServeLater       @"webservices/servenow-servelater.json"


#define kChefProfileType                  @"webservices/chef-type-category.json"
#define kChefProfileInUserMenu            @"webservices/dishes-list.json"
#define kChefProfileReviews               @"webservices/chef-review-byuser.json"
#define kChefProfileMyScheduleDetailList  @"webservices/schedule-detail-list.json"
#define kChefProfileDishImages            @"webservices/dish-imageslist.json"
#define kChefProfileMealDish              @"webservices/add-dish.json"

#define kUserAllMessagesList              @"webservices/list_messages.json"


#define KFoodAlery                        @"webservices/food-alergies.json"
#define kChefTermsandConditions           @"ws/pages-contents.php?nid"
#define kChefProfileInUser                @"webservices/view-profile.json"


#define kCenterEventList                  @"w1/event-centre-listing.json"
#define KeventDetail                      @"w1/event-detail.json"
#define Knewsbloglist                     @"w1/news-blogs.json"
#define KLocateus                         @"w1/locateus-list.json"
#define klogout                           @"w1/logout-user.json"
#define KEventOpportunitiesList           @"w1/event-opportunity.json"
#define kviewprofile                      @"w1/view-profile.json"
#define KCenterOpportunitiesList          @"w1/centre-opportunity.json"
#define kNotificationlist                 @"w1/notifications-list.json"
#define kdeletenotification               @"w1/delete-notification.json"
#define kmissionandvision                 @"w1/mission-vison.json"
#define KCenterDetail                     @"w1/centre-detail.json"
#define ksocialuser                       @"w1/socialuser-check.json"
#define kaddvolunteer                     @"w1/add-volunteer.json"
#define kScanQR                           @"w1/scan-eventqrcode.json"
#define kcenterlist                       @"w1/centre-listing.json"
#define kslecectevent                     @"w1/submit-opportunity.json"
#define kslectcenter                      @"w1/submit-opportunity.json"
#define kparticipate                      @"w1/participants-user.json"
#define Keditvolunteer                    @"w1/edit-volunteer.json"
#define kChefProfileInUser                @"webservices/view-profile.json"
#define kchatsendmessage                  @"webservices/sendmessage.json"
#define kchatingmessages                  @"webservices/userlast-chatlist.json"
#define kNotificationSettings             @"webservices/notification-list.json"
#define kaccount_section                  @"webservices/chef-order-account.json"
#define kfoodnow_later                    @"webservices/food-now.json"
#define kwritereviews                     @"webservices/rating-review.json"
#define ksingledishandmeal                @"webservices/add-dish.json"
#define kCourses                          @"webservices/dishCourse-list.json"
#define kDietary_restryctions             @"webservices/dietary-restriction-category.json"

#define Kdishlist                   @"webservices/dishes-list.json"
#define Kreviews                    @"webservices/chef-review-byuser.json"
#define Kschedulelistcook           @"webservices/schedule-list.json"
#define Kservenowlist               @"webservices/dishes-list.json"
#define KfoodnowList                @"webservices/foodnow_foodlater_onschedule_Count.json"

#define korderonrequest             @"webservices/userorder-onrequestlist.json"
#define korderinprogress            @"webservices/userorder-inprogresslist.json"

#define KAddaddress                @"webservices/add-delivery-address.json"
#define Konrequestamount           @"webservices/chef_onrequest_amount.json"

#define Kfavoritedishchef         @"webservices/favorite-dish-chef.json"
#define Kupdatecart              @"webservices/update-to-cart.json"
#define KCheckOut                @"webservices/payment.json"
#define KRequestapproval         @"webservices/approval-request-cart.json"
#define kChefAccountSaleSummary  @"webservices/api-sale-summary.json"
#define kChefSaleTracker         @"webservices/api-sale-tracker.json"
#define kChefDeleteFullSchedule  @"webservices/api-schedule-delete.json"
#define kChefDeleteDishFromSchedule  @"webservices/api-schedule-delete-dish.json"
#define  kAddCertainDatesSchedule @"webservices/certains-dates.json"
#define kChefScheduleList          @"webservices/schedule-list.json"

#define Khomescreenlist           @"webservices/quantity-alertorderlist.json"
#define KQualityalert             @"webservices/serving_qunatityrlert.json"
#define KCheforderlist           @"webservices/chef-order-list.json"
#define KDeletedish             @"webservices/delete-dish-meal.json"
#define Kordercompleteuser      @"webservices/userorders-completed-list.json"
#define Kordercanceleuser      @"webservices/userorders-cancelled-list.jsonn"
#define Kordercompleteuser      @"webservices/userorders-completed-list.json"
#define Kdeletenotification     @"webservices/notification-delete.json"

#endif