//
//  ChefSignup4.m
//  Not86
//
//  Created by Interwld on 8/12/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ChefSignup4.h"
#import "ChefSignup5.h"
#import "AppDelegate.h"
#import "Define.h"

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"

@interface ChefSignup4 ()<UITextFieldDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIImageView *img_topbar;
    UIImageView *img_BackgroundImg;
    UIScrollView *scrollview4;
    UITextField *txt_popular;
    UITableView *cuisines_table;
    UITableView *diatary_table;
    UITableView *popular_table;
    NSMutableArray *arrCuisines;
    NSMutableArray *arrrestrictions;
    UIView *alertviewBg;
    AppDelegate *delegate;
    UIButton *dropbox_btn;
    UIImageView *drop_img;
    NSMutableArray *aryPopularlist;
    NSString*str_cuisenCatId;
    NSMutableArray*ary_countrylist;
    NSMutableArray *Arr_temp;
    NSMutableArray *Arr_tempdiet;
    UITextField *txt_others;
    UIImageView *lineimage;
    CGFloat	animatedDistance;
    NSMutableArray*ary_tempselection;
    NSMutableString *str_selectcat ;
    
    
}

@end

@implementation ChefSignup4
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;



- (void)viewDidLoad {
    [super viewDidLoad];
    [self IntegrateHeaderDesign];
    [self IntegrateBodyDesign];
    //    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    aryPopularlist = [NSMutableArray new];
    arrCuisines=[NSMutableArray new];
    str_cuisenCatId = [NSString new];
    ary_countrylist = [NSMutableArray new];
    ary_tempselection= [NSMutableArray new];
    str_selectcat = [[NSMutableString alloc] init];
    
    
    arrrestrictions=[NSMutableArray new];
    Arr_temp=[[NSMutableArray alloc]init];
    Arr_tempdiet=[[NSMutableArray alloc]init];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self popularList];
    
    str_cuisenCatId = @"80";
    [self cuisineList];
    
    [self countryList];
    
    
    
}
-(void)IntegrateHeaderDesign{
    
    
    img_topbar=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,45)];
    [img_topbar setUserInteractionEnabled:YES];
    img_topbar.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:img_topbar];
    
    
    UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(12, 16, 15, 15)];
    [img_back setUserInteractionEnabled:YES];
    img_back.backgroundColor=[UIColor clearColor];
    img_back.image=[UIImage imageNamed:@"arrow@2x.png"];
    [img_topbar addSubview:img_back];
    
    UILabel  * lbl_heading = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(img_back.frame)+20,0, 220,45)];
    lbl_heading.text = @"Chef Application";
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:kFont size:15];
    [img_topbar addSubview:lbl_heading];
    
    UIButton *btn_back = [[UIButton alloc] init];
    btn_back.frame = CGRectMake(0, 0, 60,35);
    btn_back.backgroundColor = [UIColor clearColor];
    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_topbar addSubview:btn_back];
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 7, 30, 30)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-login-logo@2x.png"];
    [img_topbar addSubview:img_logo];
    
    scrollview4=[[UIScrollView alloc]init];
    
    [scrollview4 setShowsVerticalScrollIndicator:NO];
    scrollview4.delegate = self;
    scrollview4.scrollEnabled = YES;
    scrollview4.showsVerticalScrollIndicator = NO;
    [scrollview4 setUserInteractionEnabled:YES];
    scrollview4.backgroundColor = [UIColor colorWithRed:241/255.0f green:244/255.0f blue:242/255.0f alpha:1];
    [scrollview4 setContentSize:CGSizeMake(0,HEIGHT+270)];
    [self.view addSubview:scrollview4];
    
    
    
}
-(void)Back_btnClick{
    //    [self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
    
    
}


-(void)IntegrateBodyDesign{
    
    UIImageView *img_bg=[[UIImageView alloc]init];
    img_bg.frame = CGRectMake(-13, -13, WIDTH+35, 470);
    [img_bg setUserInteractionEnabled:YES];
    img_bg.backgroundColor=[UIColor clearColor];
    img_bg.image=[UIImage imageNamed:@"bg1-img@2x.png"];
    [img_bg setUserInteractionEnabled:YES];
    [scrollview4 addSubview:img_bg];
    
    
    UILabel  *lbl_food = [[UILabel alloc]init];
    lbl_food.text = @"About your Food";
    lbl_food.backgroundColor=[UIColor clearColor];
    lbl_food.textColor=[UIColor blackColor];
    lbl_food.numberOfLines = 1;
    lbl_food.font = [UIFont fontWithName:kFontBold size:15];
    [img_bg addSubview:lbl_food];
    
    UILabel  *lbl_enjoycooking = [[UILabel alloc]init];
    lbl_enjoycooking.text = @"Cuisines that you enjoy cooking\n(choose up to 5)";
    lbl_enjoycooking.backgroundColor=[UIColor clearColor];
    lbl_enjoycooking.textColor=[UIColor colorWithRed:37/255.0f green:37/255.0f blue:37/255.0f alpha:1];
    lbl_enjoycooking.numberOfLines = 0;
    lbl_enjoycooking.font = [UIFont fontWithName:kFont size:11];
    [img_bg addSubview:lbl_enjoycooking];
    
    txt_popular=[[UITextField alloc]init];
    txt_popular.borderStyle = UITextBorderStyleNone;
    txt_popular.font = [UIFont fontWithName:kFont size:13];
    txt_popular.placeholder = @"Popular";
    [txt_popular setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_popular setValue:[UIColor colorWithRed:37/255.0f green:37/255.0f blue:37/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_popular.leftViewMode = UITextFieldViewModeAlways;
    txt_popular.userInteractionEnabled=YES;
    txt_popular.textAlignment = NSTextAlignmentLeft;
    txt_popular.backgroundColor = [UIColor clearColor];
    txt_popular.keyboardType = UIKeyboardTypeAlphabet;
    txt_popular.delegate = self;
    [img_bg addSubview:txt_popular];
    
    UIImageView *line_img=[[UIImageView alloc]init];
    [line_img setUserInteractionEnabled:YES];
    line_img.backgroundColor=[UIColor clearColor];
    line_img.image=[UIImage imageNamed:@"line1.png"];
    [img_bg addSubview:line_img];
    
    
    drop_img=[[UIImageView alloc]init];
    drop_img.backgroundColor=[UIColor clearColor];
    [drop_img setUserInteractionEnabled:YES];
    drop_img.image=[UIImage imageNamed:@"drop down.png"];
    [img_bg addSubview:drop_img];
    
    dropbox_btn=[[UIButton alloc]init];
    dropbox_btn.backgroundColor = [UIColor clearColor];
    [dropbox_btn addTarget:self action:@selector(drop_btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [dropbox_btn setUserInteractionEnabled:YES];
    [img_bg addSubview:dropbox_btn];
    
    
    UILabel  *lbl_cuisines = [[UILabel alloc]init];
    lbl_cuisines.text = @"Cuisines";
    lbl_cuisines.backgroundColor=[UIColor clearColor];
    lbl_cuisines.textColor=[UIColor blackColor];
    lbl_cuisines.numberOfLines = 0;
    lbl_cuisines.font = [UIFont fontWithName:kFont size:12];
    [img_bg addSubview:lbl_cuisines];
    
    UIImageView *image=[[UIImageView alloc]init];
    [image setUserInteractionEnabled:YES];
    image.backgroundColor=[UIColor clearColor];
    image.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    image.layer.borderWidth=1.0f;
    image.clipsToBounds=YES;
    image.image=[UIImage imageNamed:@"img_tablebox@2x.png"];
    [img_bg addSubview:image];
    
    
    
#pragma mark Tableview
    
    popular_table= [[UITableView alloc] init];
    [popular_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    popular_table.delegate = self;
    popular_table.dataSource = self;
    [popular_table setUserInteractionEnabled:YES];
    popular_table.layer.borderColor = [[UIColor blackColor]CGColor];
    popular_table.layer.borderWidth = 1.0f;
    popular_table.clipsToBounds = YES;
    popular_table.showsVerticalScrollIndicator = NO;
    popular_table.backgroundColor = [UIColor whiteColor];
    [img_bg addSubview:popular_table];
    
    cuisines_table= [[UITableView alloc] init];
    [cuisines_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    cuisines_table.delegate = self;
    cuisines_table.dataSource = self;
    cuisines_table.showsVerticalScrollIndicator = NO;
    cuisines_table.backgroundColor = [UIColor clearColor];
    [image addSubview:cuisines_table];
    
    txt_others=[[UITextField alloc]init];
    txt_others.borderStyle = UITextBorderStyleNone;
    txt_others.font = [UIFont fontWithName:kFont size:13];
    txt_others.leftViewMode = UITextFieldViewModeAlways;
    txt_others.userInteractionEnabled=YES;
    txt_others.textColor=[UIColor blackColor];
    txt_others.textAlignment = NSTextAlignmentLeft;
    txt_others.backgroundColor = [UIColor clearColor];
    txt_others.keyboardType = UIKeyboardTypeAlphabet;
    txt_others.delegate = self;
    [img_bg addSubview:txt_others];
    
    
    lineimage=[[UIImageView alloc]init];
    [lineimage setUserInteractionEnabled:YES];
    lineimage.backgroundColor=[UIColor clearColor];
    lineimage.image=[UIImage imageNamed:@"line1.png"];
    [img_bg addSubview:lineimage];
    
    txt_others.hidden = YES;
    lineimage.hidden = YES;
    
    
    
    //    arrCuisines=[[NSMutableArray alloc]initWithObjects:@"Biscults and cookies",@"Burgers",@"Bread",@"Cakes & Pies",@"Coffee & Tea",@"Fresh juices",@"Hotdogs",@"Pizzas",@"Sandwiches",@"Other homemade drinks", nil];
    
    
    //    arrrestrictions=[[NSMutableArray alloc]initWithObjects:@"None",@"Dairy Free",@"Gluten Free",@"Halal",@"Kosher",@"Low Sodium",@"Organic",@"Vegetarian",@"Vegan",@"Others(please specify)", nil];
    
    UIImageView *img_bg2 =[[UIImageView alloc]init];
    //    img_bg2.frame = CGRectMake(-13, CGRectGetMaxY(img_bg.frame)-38, WIDTH+35, 375);
    [img_bg2 setUserInteractionEnabled:YES];
    img_bg2.backgroundColor=[UIColor clearColor];
    img_bg2.image=[UIImage imageNamed:@"bg1-img@2x.png"];
    [scrollview4 addSubview:img_bg2];
    
    UILabel  *lbl_Restrictions = [[UILabel alloc]init];
    
    lbl_Restrictions.text = @"I declare that I will cook according to\nthe following dietary restrictions";
    lbl_Restrictions.backgroundColor=[UIColor clearColor];
    lbl_Restrictions.textColor=[UIColor colorWithRed:37/255.0f green:37/255.0f blue:37/255.0f alpha:1];
    lbl_Restrictions.numberOfLines = 0;
    lbl_Restrictions.font = [UIFont fontWithName:kFont size:11];
    [img_bg2 addSubview:lbl_Restrictions];
    
    diatary_table= [[UITableView alloc] init];
    [diatary_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    diatary_table.delegate = self;
    diatary_table.dataSource = self;
    diatary_table.showsVerticalScrollIndicator = NO;
    diatary_table.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    diatary_table.layer.borderWidth = 1.0f;
    diatary_table.clipsToBounds = YES;
    
    diatary_table.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:diatary_table];
    
    
    
    
    UIImageView *img_bg3 = [[UIImageView alloc]init];
    img_bg3.frame =  CGRectMake(-13, 660, WIDTH+35, 80);
    [img_bg3 setUserInteractionEnabled:YES];
    img_bg3.backgroundColor = [UIColor colorWithRed:241/255.0f green:244/255.0f blue:242/255.0f alpha:1];
    [scrollview4 addSubview:img_bg3];
    
    UILabel  * page4 = [[UILabel alloc]init];
    
    page4.text = @"page 4/6";
    page4.backgroundColor=[UIColor clearColor];
    page4.textColor=[UIColor colorWithRed:37/255.0f green:37/255.0f blue:37/255.0f alpha:1];
    page4.numberOfLines = 0;
    page4.textAlignment = NSTextAlignmentCenter;
    page4.font = [UIFont fontWithName:kFont size:13];
    [img_bg3  addSubview:page4];
    
    UIButton *Nextview = [[UIButton alloc] init];
    [Nextview setImage:[UIImage imageNamed:@"text buuton.png"] forState:UIControlStateNormal];
    [Nextview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [Nextview addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    Nextview.layer.cornerRadius=4.0f;
    [img_bg3  addSubview:Nextview];
    
    if (IS_IPHONE_6Plus)
    {
        scrollview4.frame = CGRectMake(0, 45, WIDTH, HEIGHT);
        img_bg.frame = CGRectMake(-5, -8, WIDTH+14, 500);
        lbl_food.frame=CGRectMake(self.view.frame.size.width/2-50,20, 300,30);
        lbl_enjoycooking.frame = CGRectMake(35,44, 300,50);
        txt_popular.frame=CGRectMake(35,CGRectGetMaxY(lbl_enjoycooking.frame)+1, 150, 38);
        line_img.frame=CGRectMake(35, CGRectGetMaxY(txt_popular.frame)-10, self.view.frame.size.width-55, 0.5);
        drop_img.frame = CGRectMake(self.view.frame.size.width-50, CGRectGetMaxY(lbl_enjoycooking.frame)+15, 20, 10);
        dropbox_btn.frame = CGRectMake(35, CGRectGetMaxY(lbl_enjoycooking.frame)+1, WIDTH-55, 38);
        popular_table.frame  = CGRectMake(35, CGRectGetMaxY(txt_popular.frame), WIDTH-55, 120);
        lbl_cuisines.frame = CGRectMake(self.view.frame.size.width/2-40,CGRectGetMaxY(txt_popular.frame), 300,30);
        image.frame=CGRectMake(35, CGRectGetMaxY(lbl_cuisines.frame)+3, self.view.frame.size.width-50, 250);
        cuisines_table.frame  = CGRectMake(0,0,400,250);
        txt_others.frame=CGRectMake(50, CGRectGetMaxY(image.frame)+4, 340, 30);
        lineimage.frame=CGRectMake(60, CGRectGetMaxY(txt_others.frame)+1, self.view.frame.size.width-100, 0.5);
        img_bg2.frame = CGRectMake(-5, CGRectGetMaxY(img_bg.frame)-20, WIDTH+14, 375);
        lbl_Restrictions.frame = CGRectMake(35,10,300,50);
        diatary_table.frame  = CGRectMake(35, CGRectGetMaxY(lbl_Restrictions.frame)+5,360,260);
        //        img_bg3.frame =  CGRectMake(0, HEIGHT-100, WIDTH, 100);
        img_bg3.frame=CGRectMake(0, CGRectGetMaxY(img_bg2.frame)+10, WIDTH, 100);
        page4.frame = CGRectMake(WIDTH/2-205,0, WIDTH,30);
        Nextview.frame = CGRectMake(18, 40, WIDTH-36,50);
        lbl_Restrictions.font = [UIFont fontWithName:kFont size:16];
        [scrollview4 setContentSize:CGSizeMake(0,CGRectGetMaxY(img_bg2.frame)+150)];
        
        
        
        
    }
    else if (IS_IPHONE_6){
        scrollview4.frame = CGRectMake(0, 45, WIDTH, HEIGHT);
        img_bg.frame = CGRectMake(-5, -8, WIDTH+12, 520);
        // img_bg.frame = CGRectMake(-13, -13, WIDTH+35, 470);
        lbl_food.frame=CGRectMake(self.view.frame.size.width/2-60,15, 300,30);
        lbl_enjoycooking.frame = CGRectMake(35,44, 300,50);
        txt_popular.frame=CGRectMake(35,CGRectGetMaxY(lbl_enjoycooking.frame)+1, 150, 38);
        line_img.frame=CGRectMake(35, CGRectGetMaxY(txt_popular.frame)-10, self.view.frame.size.width-55, 0.5);
        drop_img.frame = CGRectMake(self.view.frame.size.width-50, CGRectGetMaxY(lbl_enjoycooking.frame)+15, 20, 10);
        dropbox_btn.frame = CGRectMake(35, CGRectGetMaxY(lbl_enjoycooking.frame)+1, WIDTH-55, 38);
        popular_table.frame  = CGRectMake(35, CGRectGetMaxY(txt_popular.frame), WIDTH-55, 120);
        lbl_cuisines.frame = CGRectMake(self.view.frame.size.width/2-30,CGRectGetMaxY(txt_popular.frame)+10, 300,30);
        image.frame=CGRectMake(35, CGRectGetMaxY(lbl_cuisines.frame)+3, self.view.frame.size.width-60, 250);
        cuisines_table.frame  = CGRectMake(0,0,400,250);
        txt_others.frame=CGRectMake(50, CGRectGetMaxY(image.frame)+4, 340, 30);
        lineimage.frame=CGRectMake(60, CGRectGetMaxY(txt_others.frame)+1, self.view.frame.size.width-100, 0.5);
        lbl_Restrictions.frame = CGRectMake(38,7, 300,50);
        img_bg2.frame = CGRectMake(-5, CGRectGetMaxY(img_bg.frame)-18, WIDTH+12, 375);
        diatary_table.frame  = CGRectMake(35, CGRectGetMaxY(lbl_Restrictions.frame)+5,350,260);
        img_bg3.frame=CGRectMake(0, CGRectGetMaxY(img_bg2.frame)+10, WIDTH, 100);
        //        img_bg3.frame=CGRectMake(0, HEIGHT-100, WIDTH, 100);
        page4.frame =CGRectMake(0,4, WIDTH,30);
        Nextview.frame = CGRectMake(18, 40, WIDTH-36,50);
        lbl_Restrictions.font = [UIFont fontWithName:kFont size:15];
        [scrollview4 setContentSize:CGSizeMake(0,CGRectGetMaxY(img_bg2.frame)+150)];
        
    }
    else if(IS_IPHONE_5)
    {
        scrollview4.frame = CGRectMake(0, 45, WIDTH, HEIGHT);
        img_bg.frame = CGRectMake(-5, -7, WIDTH+12, 500);
        // img_bg.frame = CGRectMake(-13, -13, WIDTH+35, 470);
        lbl_food.frame=CGRectMake(self.view.frame.size.width/2-60,10, 300,30);
        lbl_enjoycooking.frame = CGRectMake(35,44, 300,50);
        txt_popular.frame=CGRectMake(35,CGRectGetMaxY(lbl_enjoycooking.frame)+1, 150, 38);
        line_img.frame=CGRectMake(35, CGRectGetMaxY(txt_popular.frame)-10, self.view.frame.size.width-55, 0.5);
        drop_img.frame = CGRectMake(self.view.frame.size.width-40, CGRectGetMaxY(lbl_enjoycooking.frame)+15, 15, 10);
        dropbox_btn.frame = CGRectMake(35, CGRectGetMaxY(lbl_enjoycooking.frame)+1, WIDTH-55, 33);
        popular_table.frame  = CGRectMake(35, CGRectGetMaxY(txt_popular.frame), WIDTH-55, 120);
        lbl_cuisines.frame = CGRectMake(self.view.frame.size.width/2-30,CGRectGetMaxY(txt_popular.frame)+10, 300,30);
        image.frame=CGRectMake(35, CGRectGetMaxY(lbl_cuisines.frame)+3, self.view.frame.size.width-50, 250);
        cuisines_table.frame  = CGRectMake(0,0,400,250);
        txt_others.frame=CGRectMake(50, CGRectGetMaxY(image.frame)+4, 340, 30);
        lineimage.frame=CGRectMake(60, CGRectGetMaxY(txt_others.frame)+1, self.view.frame.size.width-100, 0.5);
        lbl_Restrictions.frame = CGRectMake(38,7, 300,50);
        img_bg2.frame = CGRectMake(-5, CGRectGetMaxY(img_bg.frame)-19, WIDTH+12, 375);
        diatary_table.frame  = CGRectMake(35, CGRectGetMaxY(lbl_Restrictions.frame)+5,270,250);
        img_bg3.frame=CGRectMake(0, CGRectGetMaxY(img_bg2.frame)+10, WIDTH, 100);
        //        img_bg3.frame=CGRectMake(0, HEIGHT-80, WIDTH, 100);
        page4.frame =CGRectMake(0,0, WIDTH,30);
        Nextview.frame = CGRectMake(18, 35, WIDTH-36,40);
        lbl_Restrictions.font = [UIFont fontWithName:kFont size:14];
        [scrollview4 setContentSize:CGSizeMake(0,CGRectGetMaxY(img_bg2.frame)+140)];
        
    }
    else
    {
        scrollview4.frame = CGRectMake(0, 45, WIDTH, HEIGHT);
        img_bg.frame = CGRectMake(-5, -7, WIDTH+12, 500);
        // img_bg.frame = CGRectMake(-13, -13, WIDTH+35, 470);
        lbl_food.frame=CGRectMake(self.view.frame.size.width/2-60,10, 300,30);
        lbl_enjoycooking.frame = CGRectMake(35,44, 300,50);
        txt_popular.frame=CGRectMake(35,CGRectGetMaxY(lbl_enjoycooking.frame)+1, 150, 38);
        line_img.frame=CGRectMake(35, CGRectGetMaxY(txt_popular.frame)-10, self.view.frame.size.width-55, 0.5);
        drop_img.frame = CGRectMake(self.view.frame.size.width-40, CGRectGetMaxY(lbl_enjoycooking.frame)+15, 15, 10);
        dropbox_btn.frame = CGRectMake(35, CGRectGetMaxY(lbl_enjoycooking.frame)+1, WIDTH-55, 38);
        popular_table.frame  = CGRectMake(35, CGRectGetMaxY(txt_popular.frame), WIDTH-55, 120);
        //        next.frame = CGRectMake(self.view.frame.size.width-30, CGRectGetMaxY(lbl_enjoycooking.frame)+15, 15, 10);
        lbl_cuisines.frame = CGRectMake(self.view.frame.size.width/2-30,CGRectGetMaxY(txt_popular.frame)+10, 300,30);
        image.frame=CGRectMake(35, CGRectGetMaxY(lbl_cuisines.frame)+3, self.view.frame.size.width-50, 250);
        cuisines_table.frame  = CGRectMake(0,0,400,250);
        txt_others.frame=CGRectMake(50, CGRectGetMaxY(image.frame)+4, 340, 30);
        lineimage.frame=CGRectMake(60, CGRectGetMaxY(txt_others.frame)+1, self.view.frame.size.width-100, 0.5);
        
        lbl_Restrictions.frame = CGRectMake(38,7, 300,50);
        img_bg2.frame = CGRectMake(-5, CGRectGetMaxY(img_bg.frame)-19, WIDTH+12, 375);
        diatary_table.frame  = CGRectMake(35, CGRectGetMaxY(lbl_Restrictions.frame)+5,250,220);
        img_bg3.frame=CGRectMake(0, CGRectGetMaxY(img_bg2.frame)+10, WIDTH, 100);
        //        img_bg3.frame=CGRectMake(0, HEIGHT-80, WIDTH, 100);
        page4.frame =CGRectMake(0,0, WIDTH,30);
        Nextview.frame = CGRectMake(18, 35, WIDTH-36,40);
        lbl_Restrictions.font = [UIFont fontWithName:kFont size:14];
        [scrollview4 setContentSize:CGSizeMake(0,CGRectGetMaxY(img_bg2.frame)+140)];
        
    }
    
    [popular_table setHidden:YES];
    //    [scrollview4 setContentSize:CGSizeMake(0,1000)];
    
    
}


#pragma mark UITableview methods



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView== cuisines_table)
    {
        return [arrCuisines count];
        
    }
    else if(tableView==diatary_table){
        return [ary_countrylist count];
        
    }else{
        return [aryPopularlist count];
    }
    return 0;
    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"LazyTableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor=[UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    
    int selectedindex= (int)indexPath.row;
    NSLog(@"%d",selectedindex);
    
    UILabel  *lbl_Restrictions = [[UILabel alloc]init];
    lbl_Restrictions.frame =CGRectMake(35,-3,150,25);
    
    if (tableView== cuisines_table)
    {
        UIButton *btn_TableCell_CheckBox1 = [[UIButton alloc]initWithFrame:CGRectMake(10,2,15,15)];
        btn_TableCell_CheckBox1.backgroundColor=[UIColor clearColor];
        [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img.checkcorrect@2x.png"] forState:UIControlStateSelected];
        [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
        btn_TableCell_CheckBox1.tag = indexPath.row;
        [cell.contentView addSubview:btn_TableCell_CheckBox1];
        if([Arr_temp containsObject:[arrCuisines objectAtIndex:indexPath.row]])
        {
            [btn_TableCell_CheckBox1 setSelected:YES];
        }
        else{
            [btn_TableCell_CheckBox1 setSelected:NO];
        }
        [btn_TableCell_CheckBox1 addTarget:self action:@selector(click_selectObjectAt:) forControlEvents:UIControlEventTouchUpInside];
        
        lbl_Restrictions.text = [[arrCuisines objectAtIndex:indexPath.row] valueForKey:@"CuisineName"];
        
        
    }
    else if(tableView==diatary_table){
        UIButton *btn_TableCell_CheckBox2 = [[UIButton alloc]initWithFrame:CGRectMake(10,2,15,15)];
        btn_TableCell_CheckBox2.backgroundColor=[UIColor clearColor];
        [btn_TableCell_CheckBox2 setImage:[UIImage imageNamed:@"img.checkcorrect@2x.png"] forState:UIControlStateSelected];
        [btn_TableCell_CheckBox2 setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
        btn_TableCell_CheckBox2.tag = indexPath.row;
        [cell.contentView addSubview:btn_TableCell_CheckBox2];
        if([Arr_tempdiet containsObject:[ary_countrylist objectAtIndex:indexPath.row]])
        {
            [btn_TableCell_CheckBox2 setSelected:YES];
        }
        else{
            [btn_TableCell_CheckBox2 setSelected:NO];
        }
        [btn_TableCell_CheckBox2 addTarget:self action:@selector(click_selectObjectAt1:) forControlEvents:UIControlEventTouchUpInside];
        
        lbl_Restrictions.text = [[ary_countrylist objectAtIndex:indexPath.row] valueForKey:@"DietaryName"];
        
    }
    else
    {
        
        UIButton *btn_TableCell_CheckBox2 = [[UIButton alloc]initWithFrame:CGRectMake(10,2,15,15)];
        btn_TableCell_CheckBox2.backgroundColor=[UIColor clearColor];
        [btn_TableCell_CheckBox2 setImage:[UIImage imageNamed:@"img.checkcorrect@2x.png"] forState:UIControlStateSelected];
        [btn_TableCell_CheckBox2 setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
        btn_TableCell_CheckBox2.tag = indexPath.row;
        [cell.contentView addSubview:btn_TableCell_CheckBox2];
        if([ary_tempselection containsObject:[aryPopularlist objectAtIndex:indexPath.row]])
        {
            [btn_TableCell_CheckBox2 setSelected:YES];
        }
        else{
            [btn_TableCell_CheckBox2 setSelected:NO];
        }
        [btn_TableCell_CheckBox2 addTarget:self action:@selector(click_selectObjectAt2:) forControlEvents:UIControlEventTouchUpInside];
        
        lbl_Restrictions.frame =CGRectMake(40,-3,150,25);
        lbl_Restrictions.text=[[aryPopularlist objectAtIndex:indexPath.row] valueForKey:@"CuisineCatName"];
    }
    lbl_Restrictions.backgroundColor=[UIColor clearColor];
    lbl_Restrictions.textColor=[UIColor blackColor];
    lbl_Restrictions.font = [UIFont fontWithName:kFont size:12];
    [cell.contentView addSubview:lbl_Restrictions];
    
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView== cuisines_table)
    {
        return 25;
        
    }
    else if(tableView == diatary_table){
        return 25;
        
    }else{
        return 25;
    }
    return 0;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView== cuisines_table)
    {
        
    }
    else if(tableView==diatary_table){
        
    }else{
        txt_popular.text=[[aryPopularlist objectAtIndex:indexPath.row] valueForKey:@"CuisineCatName"];
        str_cuisenCatId =[[aryPopularlist objectAtIndex:indexPath.row] valueForKey:@"CuisineCatID"];
        
        [str_selectcat  appendString:[NSString stringWithFormat:@"%@",[[aryPopularlist objectAtIndex:indexPath.row] valueForKey:@"CuisineCatID"]]];
        
        
        [self cuisineList];
        [popular_table setHidden:YES];
    }
    
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}

#pragma mark Button actions


-(void)Next_btnClick
{
    //    ChefSignup5 *chefsignup5=[[ChefSignup5 alloc]init];
    //    [self presentViewController:chefsignup5 animated:NO completion:nil];
    //    [self.navigationController pushViewController:chefsignup5 animated:NO];
    
    
    NSLog(@"click_Loginbtn");
    
    if ([txt_popular.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Please enter categories list"];
        
    }
    else  if (Arr_temp.count ==0)
        
    {
        [self popup_Alertview:@"Please enter cuisine id"];
    }
    else{
        [self SignupFourth];
        
    }
}

-(void)check_btnClick:(id)sender
{
    
    
    
}
-(void) click_selectObjectAt:(UIButton *) sender
{
    
    if([Arr_temp containsObject:[arrCuisines objectAtIndex:sender.tag]])
    {
        if ([[[arrCuisines objectAtIndex:sender.tag] valueForKey:@"CuisineName"]isEqualToString:@"Other Homemade drinks"])
        {
            txt_others.hidden = YES;
            lineimage.hidden = YES;
            [txt_others resignFirstResponder];
            
            
        }
        [Arr_temp removeObject:[arrCuisines objectAtIndex:sender.tag]];
    }
    else
    {
        if (Arr_temp.count >4)
        {
            [self popup_Alertview:@"Select upto five cuisines"];
            
        }
        else{
            if ([[[arrCuisines objectAtIndex:sender.tag] valueForKey:@"CuisineName"]isEqualToString:@"Other Homemade drinks"])
            {
                txt_others.hidden = NO;
                lineimage.hidden = NO;
                [txt_others becomeFirstResponder];
                
            }
            [Arr_temp addObject:[arrCuisines objectAtIndex:sender.tag]];
            
        }
    }
    
    [cuisines_table reloadData];
    
}


-(void) click_selectObjectAt2:(UIButton *) sender
{
    if([ary_tempselection containsObject:[aryPopularlist objectAtIndex:sender.tag]])
    {
        [Arr_tempdiet removeObject:[aryPopularlist objectAtIndex:sender.tag]];
        
        txt_popular.text=[[aryPopularlist objectAtIndex:sender.tag] valueForKey:@"CuisineCatName"];
        str_cuisenCatId =[[aryPopularlist objectAtIndex:sender.tag] valueForKey:@"CuisineCatID"];
        
        
        for (int i=0; i<[ary_tempselection count]; i++)
        {
            
            
            if (i==[ary_tempselection count]-1)
            {
                [str_selectcat  appendString:[NSString stringWithFormat:@"%@",[[ary_tempselection objectAtIndex:i] valueForKey:@"CuisineCatID"]]];
            }
            else
            {
                [str_selectcat  appendString:[NSString stringWithFormat:@"%@||",[[ary_tempselection objectAtIndex:i] valueForKey:@"CuisineCatID"]]];
            }
            
            
        }
        
        
        [self cuisineList];
    }
    else
    {
        [ary_tempselection addObject:[aryPopularlist objectAtIndex:sender.tag]];
        
        
        txt_popular.text=[[aryPopularlist objectAtIndex:sender.tag] valueForKey:@"CuisineCatName"];
        str_cuisenCatId =[[aryPopularlist objectAtIndex:sender.tag] valueForKey:@"CuisineCatID"];
        
        
        for (int i=0; i<[ary_tempselection count]; i++)
        {
            
            
            if (i==[ary_tempselection count]-1)
            {
                [str_selectcat  appendString:[NSString stringWithFormat:@"%@",[[ary_tempselection objectAtIndex:i] valueForKey:@"CuisineCatID"]]];
            }
            else
            {
                [str_selectcat  appendString:[NSString stringWithFormat:@"%@||",[[ary_tempselection objectAtIndex:i] valueForKey:@"CuisineCatID"]]];
            }
            
            
        }
        
        [self cuisineList];
        
    }
    [diatary_table reloadData];
    
}
-(void) click_selectObjectAt1:(UIButton *) sender
{
    if ([[[ary_countrylist objectAtIndex:sender.tag] valueForKey:@"DietaryName"]isEqualToString:@"None"])
    {
        [Arr_tempdiet removeAllObjects];
        [Arr_tempdiet addObject:[ary_countrylist objectAtIndex:sender.tag]];
        [diatary_table reloadData];
        
    }
    else if ([[[ary_countrylist objectAtIndex:sender.tag] valueForKey:@"DietaryID"]isEqualToString:@"196"])
    {
        
        if([Arr_tempdiet containsObject:[ary_countrylist objectAtIndex:sender.tag]])
        {
            [Arr_tempdiet removeObject:[ary_countrylist objectAtIndex:sender.tag]];
            txt_others.hidden = YES;
            lineimage.hidden = YES;
            [txt_others resignFirstResponder];
        }
        else
        {
            [Arr_tempdiet addObject:[ary_countrylist objectAtIndex:sender.tag]];
            txt_others.hidden = NO;
            lineimage.hidden = NO;
            [txt_others becomeFirstResponder];
            
        }
        [diatary_table reloadData];
        
        
    }
    else{
        if([Arr_tempdiet containsObject:[ary_countrylist objectAtIndex:sender.tag]])
        {
            [Arr_tempdiet removeObject:[ary_countrylist objectAtIndex:sender.tag]];
        }
        else
        {
            [Arr_tempdiet addObject:[ary_countrylist objectAtIndex:sender.tag]];
            
            
        }
        
        [diatary_table reloadData];
        
    }
    
    //   else if ([[[ary_countrylist objectAtIndex:sender.tag] valueForKey:@"DietaryName"]isEqualToString:@"Vegan"]||[[[ary_countrylist objectAtIndex:sender.tag] valueForKey:@"DietaryName"]isEqualToString:@"Vegetarian"]||[[[ary_countrylist objectAtIndex:sender.tag] valueForKey:@"DietaryName"]isEqualToString:@"Kosher"])
    //    {
    //
    //        int selectedindex = (int)sender.tag;
    //
    //        if([Arr_tempdiet containsObject:[ary_countrylist objectAtIndex:sender.tag]])
    //        {
    //            [Arr_tempdiet removeObject:[ary_countrylist objectAtIndex:sender.tag]];
    //        }
    //        else
    //        {
    //            [Arr_tempdiet addObject:[ary_countrylist objectAtIndex:sender.tag]];
    //        }
    //
    //        [Arr_tempdiet removeObject:[ary_countrylist objectAtIndex:0]];
    //
    //
    //        [diatary_table reloadData];
    //
    //
    //    }
    
    
}

-(void)drop_btnClick:(id)sender
{
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        popular_table.hidden=YES;
        
    }
    else
    {
        [sender setSelected:NO];
        popular_table.hidden=NO;
    }
    
    [self popularList];
    
}
-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFontBold size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFontBold size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFontBold size:16.0f]];
}
-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}




# pragma mark ChefSignupFourth method


-(void)SignupFourth
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    
    
    
    NSMutableString *str_cuisenid = [[NSMutableString alloc] init];
    
    for (int i=0; i<[Arr_temp count]; i++)
    {
        
        
        if (i==[Arr_temp count]-1)
        {
            [str_cuisenid  appendString:[NSString stringWithFormat:@"%@",[[Arr_temp objectAtIndex:i] valueForKey:@"CuisineID"]]];
        }
        else
        {
            [str_cuisenid  appendString:[NSString stringWithFormat:@"%@||",[[Arr_temp objectAtIndex:i] valueForKey:@"CuisineID"]]];
        }
        
        
    }
    
    NSMutableString *str_dietryids = [[NSMutableString alloc] init];
    
    for (int i=0; i<[Arr_tempdiet count]; i++)
    {
        
        
        if (i==[Arr_tempdiet count]-1)
        {
            [str_dietryids  appendString:[NSString stringWithFormat:@"%@",[[Arr_tempdiet objectAtIndex:i] valueForKey:@"DietaryID"]]];
        }
        else
        {
            [str_dietryids  appendString:[NSString stringWithFormat:@"%@||",[[Arr_tempdiet objectAtIndex:i] valueForKey:@"DietaryID"]]];
        }
        
        
    }
    
    NSLog(@"str_dietryids%@",str_dietryids);
    NSLog(@"str_dietryids%@",str_cuisenid);
    
    // [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"]
    
    NSDictionary *params =@{
                            @"cuisine_category_id"               :  str_cuisenCatId,
                            @"dietary_restrictions"              :  str_dietryids,
                            @"field_other_dietary_restrictions"  : txt_others.text,
                            @"uid"                               :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"registration_page"                 :  @"4",
                            
                            @"role_type"                         :  @"chef_profile",
                            @"device_udid"                       :  UniqueAppID,
                            @"device_token"                      :  @"Dev",
                            @"cusines_id"                        :  str_cuisenid,
                            @"device_type"                       :  @"1"
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSignUpFirst
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpFourth:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self SignupFourth];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpFourth :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        ChefSignup5 *chefsignup5=[[ChefSignup5 alloc]init];
        [self presentViewController:chefsignup5 animated:NO completion:nil];
        
        //         [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        txt_popular.text=@"";
        
    }
    
}




# pragma mark Popular method

-(void)popularList
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpCuisineCategoryList
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpPopular:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self popularList];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpPopular :(NSDictionary * )TheDict
{
    [aryPopularlist removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"CuisineCategoryList"] count]; i++)
        {
            [aryPopularlist addObject:[[TheDict valueForKey:@"CuisineCategoryList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        txt_popular.text=@"";
        
        
    }
    
    [popular_table reloadData];
    
    
}



# pragma mark  Cuisine list method

-(void)cuisineList
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    NSDictionary *params =@{
                            
                            
                            @"cuisine_category_id"            :  str_selectcat,
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpCuisineList
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpCuisineList:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self cuisineList];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpCuisineList :(NSDictionary * )TheDict
{
    [arrCuisines removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"CuisineList"] count]; i++)
        {
            [arrCuisines addObject:[[TheDict valueForKey:@"CuisineList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        txt_popular.text=@"";
        
        
    }
    
    [cuisines_table reloadData];
    
    
}

# pragma mark Hcountry method

-(void)countryList
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpHcountry
                                                      parameters:nil];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpHcountry:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self countryList];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpHcountry :(NSDictionary * )TheDict
{
    [ary_countrylist removeAllObjects];
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DietarytList"] count]; i++)
        {
            [ary_countrylist addObject:[[TheDict valueForKey:@"DietarytList"] objectAtIndex:i]];
            
        }
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        
        
    }
    
    [diatary_table reloadData];
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
