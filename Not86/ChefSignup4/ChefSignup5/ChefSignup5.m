//
//  ChefSignup5.m
//  Not86
//
//  Created by Interwld on 8/12/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ChefSignup5.h"
#import "ChefSignup6.h"
#import "AppDelegate.h"
#import "Define.h"

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"

@interface ChefSignup5 ()<UIScrollViewDelegate,UITextFieldDelegate,UITextViewDelegate,UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIImageView *img_topbar;
    UIImageView *img_BackgroundImg;
    UIScrollView *scrollview5;
    UITextField *txt_cheftype;
    UITextField *txt_others;
    UITextView *txtview1;
    UITextField *txt_statequatification;
    AppDelegate *delegate;
    UIView *alertviewBg;
    
    UITableView *cheftype_table;
    NSMutableArray *arr_cheftype;
    NSString *str_status;
    UIImageView *button_img;
    UIImageView *button_imge;
    UIImageView *buttonimge;
    UIImageView *buttonimges;
    NSString *str_cooking;
    NSString *str_LearnCook;
    UIImageView *buttonimg;
    UIImageView *buttonimage;
    UIImageView *img_selftaught;
    UIImageView *img_Lreaned;
    UIImageView *img_others;
    UITextField *txt_otherslearncook;
    int DEF;
    NSMutableArray *ary_temp2;
    UIButton *btn_Lreaned;
    UIButton *btn_selftaught;
    UIButton *btn_others;
    UIButton *btnyrs1;
    UIButton *btn_yrs;
    UIButton *btnyrs;
    UIButton *btn_Yes;
    UILabel  *lblYrs1;
    NSMutableString*str_CookType;
    UIImageView *lineimg;
    UILabel  *lbl_cookingexp;
    UILabel  *lbl_Yes;
    UILabel  *lbl_Yrs;
    UILabel  *lblYrs;
    UILabel  *lbl_cookingqual;
    UILabel  *lblYes;
    UIButton *btnYes;
    UIButton *btnNo;
    UILabel  *lbl_learncook;
    UILabel  * lbl_selftaught;
    UILabel  * lbl_learned;
    UILabel  * lbl_others;
    
    UILabel  *lblNo;
    UILabel  *lbl_statequalification;
    UIImageView *imgline;
    UIImageView *img_bg3;
    UILabel  * page4;
    UIButton *Nextview;
    
    int ABC;
    NSMutableArray *ary_temp;
    CGFloat	animatedDistance;
    
    NSString*str_cheftype;
    
    UIImageView *img_line;
    
    UIImageView *img_backgroundimag;
    
    UIImageView *img_backgroundimage;
    UIImageView *img_backgroundimage2;
    UIImageView *img_backgroundimage3;
    UILabel  * lbl_recommand;
    UILabel  * lblmax;
    
}

@end

@implementation ChefSignup5
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self IntegrateHeaderDesign];
    [self IntegrateBodyDesign];
    //    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    str_cooking=@"";
    str_status=@"";
    str_LearnCook=@"";
    
    str_cheftype = [NSString new];
    img_backgroundimag.hidden=YES;
    
    img_backgroundimage2.hidden=YES;
    img_backgroundimage3.hidden=YES;
    
    
    ary_temp=[[NSMutableArray alloc]init];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
}
-(void)IntegrateHeaderDesign{
    
    
    
    img_topbar=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,45)];
    [img_topbar setUserInteractionEnabled:YES];
    img_topbar.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:img_topbar];
    
    
    UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(12, 16, 15, 15)];
    [img_back setUserInteractionEnabled:YES];
    img_back.backgroundColor=[UIColor clearColor];
    img_back.image=[UIImage imageNamed:@"arrow@2x.png"];
    [img_topbar addSubview:img_back];
    
    UILabel  * lbl_heading = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(img_back.frame)+20,0, 220,45)];
    lbl_heading.text = @"Chef Application";
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:kFont size:15];
    [img_topbar addSubview:lbl_heading];
    
    UIButton *btn_back = [[UIButton alloc] init];
    btn_back.frame = CGRectMake(0, 0, 60,35);
    btn_back.backgroundColor = [UIColor clearColor];
    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_topbar addSubview:btn_back];
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 7, 30, 30)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-login-logo@2x.png"];
    [img_topbar addSubview:img_logo];
    
    scrollview5=[[UIScrollView alloc]init];
    [scrollview5 setShowsVerticalScrollIndicator:NO];
    scrollview5.delegate = self;
    scrollview5.scrollEnabled = YES;
    scrollview5.showsVerticalScrollIndicator = NO;
    [scrollview5 setUserInteractionEnabled:YES];
    scrollview5.backgroundColor =  [UIColor colorWithRed:241/255.0f green:244/255.0f blue:242/255.0f alpha:1];
    [self.view addSubview:scrollview5];
    [scrollview5 setContentSize:CGSizeMake(0,HEIGHT+250)];
    
    
    
    
}
-(void)Back_btnClick{
    //    [self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
    
    
}


-(void)IntegrateBodyDesign
{
    UIImageView *img_bg=[[UIImageView alloc]init];
    img_bg.frame = CGRectMake(-13, -20, WIDTH+35, 700);
    [img_bg setUserInteractionEnabled:YES];
    img_bg.backgroundColor=[UIColor clearColor];
    img_bg.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [scrollview5 addSubview:img_bg];
    
    
    UILabel  *lbl_chef = [[UILabel alloc]init];
    lbl_chef.text = @"You as a Chef";
    lbl_chef.backgroundColor=[UIColor clearColor];
    lbl_chef.textColor=[UIColor blackColor];
    lbl_chef.numberOfLines = 1;
    lbl_chef.textAlignment = NSTextAlignmentCenter;
    lbl_chef.font = [UIFont fontWithName:kFontBold size:15];
    [img_bg addSubview:lbl_chef];
    
    
    txt_cheftype = [[UITextField alloc] init];
    txt_cheftype.borderStyle = UITextBorderStyleNone;
    txt_cheftype.textColor = [UIColor blackColor];
    txt_cheftype.font = [UIFont fontWithName:kFont size:13];
    txt_cheftype.placeholder = @"Chef Type";
    [txt_cheftype setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_cheftype setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    [img_bg addSubview:txt_cheftype];
    
    txt_cheftype.leftViewMode = UITextFieldViewModeAlways;
    txt_cheftype.userInteractionEnabled=YES;
    txt_cheftype.textAlignment = NSTextAlignmentLeft;
    txt_cheftype.backgroundColor = [UIColor clearColor];
    txt_cheftype.keyboardType = UIKeyboardTypeAlphabet;
    txt_cheftype.delegate = self;
    [img_bg addSubview:txt_cheftype];
    
    UIImageView *line_img=[[UIImageView alloc]init];
    [line_img setUserInteractionEnabled:YES];
    line_img.backgroundColor=[UIColor clearColor];
    line_img.image=[UIImage imageNamed:@"line1.png"];
    [img_bg addSubview:line_img];
    
    
    
    UIImageView *dropbox_img=[[UIImageView alloc]init];
    [dropbox_img setUserInteractionEnabled:YES];
    dropbox_img.backgroundColor=[UIColor clearColor];
    dropbox_img.image=[UIImage imageNamed:@"drop down.png"];
    [img_bg addSubview:dropbox_img];
    
    UIButton *dropbox_btn = [[UIButton alloc] init];
    dropbox_btn.backgroundColor = [UIColor clearColor];
    [dropbox_btn addTarget:self action:@selector(drop_btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg addSubview:dropbox_btn];
    
    
    
//    img_backgroundimag=[[UIImageView alloc]init];
//    [img_backgroundimag setUserInteractionEnabled:YES];
//    img_backgroundimag.backgroundColor=[UIColor redColor];
//    img_backgroundimage.image=[UIImage imageNamed:@"img.bg@2x.png"];
//   // img_backgroundimag.userInteractionEnabled = YES;
//    [img_bg addSubview:img_backgroundimag];
//    
    
    txt_others = [[UITextField alloc] init];
    txt_others.borderStyle = UITextBorderStyleNone;
    txt_others.textColor = [UIColor blackColor];
    txt_others.font = [UIFont fontWithName:kFont size:13];
    txt_others.placeholder = @"Others (please specify)";
    [txt_others setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_others setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    txt_others.leftViewMode = UITextFieldViewModeAlways;
    txt_others.userInteractionEnabled=YES;
    txt_others.textAlignment = NSTextAlignmentLeft;
    txt_others.backgroundColor = [UIColor clearColor];
    txt_others.keyboardType = UIKeyboardTypeAlphabet;
    txt_others.delegate = self;
    [img_bg addSubview:txt_others];
    
    img_line=[[UIImageView alloc]init];
    [img_line setUserInteractionEnabled:YES];
    img_line.backgroundColor=[UIColor clearColor];
    img_line.image=[UIImage imageNamed:@"line1.png"];
    [img_bg addSubview:img_line];
    
    txt_others.hidden = YES;
    img_line.hidden = YES;
    
    
    
    lbl_cookingexp = [[UILabel alloc]init];
    lbl_cookingexp.text = @"Years of Cooking Experience";
    lbl_cookingexp.backgroundColor=[UIColor clearColor];
    lbl_cookingexp.textColor=[UIColor blackColor];
    lbl_cookingexp.numberOfLines = 0;
    lbl_cookingexp.font = [UIFont fontWithName:kFontBold size:15];
    [img_bg addSubview:lbl_cookingexp];
    
    
    btn_Yes = [[UIButton alloc] init];
    //    btn_Yes.tag=1;
    [btn_Yes setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    btn_Yes.backgroundColor = [UIColor clearColor];
    [btn_Yes addTarget:self action:@selector(btn_FirstyearClick:) forControlEvents:UIControlEventTouchUpInside ];
    [img_bg addSubview:btn_Yes];
    
    
    
    lbl_Yes = [[UILabel alloc]init];
    lbl_Yes.text = @"1-5";
    lbl_Yes.backgroundColor=[UIColor clearColor];
    lbl_Yes.textColor=[UIColor blackColor];
    lbl_Yes.numberOfLines = 0;
    lbl_Yes.font = [UIFont fontWithName:kFont size:11];
    [img_bg addSubview:lbl_Yes];
    
    
    btn_yrs = [[UIButton alloc] init];
    btn_yrs.backgroundColor = [UIColor clearColor];
    //    btn_yrs.tag=2;
    [btn_yrs setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    
    [btn_yrs addTarget:self action:@selector(btn_sixyearClick:) forControlEvents:UIControlEventTouchUpInside ];
    [img_bg addSubview:btn_yrs];
    
    
    
    lbl_Yrs = [[UILabel alloc]init];
    lbl_Yrs.text = @"6-10";
    lbl_Yrs.backgroundColor=[UIColor clearColor];
    lbl_Yrs.textColor=[UIColor blackColor];
    lbl_Yrs.numberOfLines = 0;
    lbl_Yrs.font = [UIFont fontWithName:kFont size:11];
    [img_bg addSubview:lbl_Yrs];
    
    //
    
    btnyrs = [[UIButton alloc] init];
    //    btnyrs.tag=3;
    [btnyrs setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    btnyrs.backgroundColor = [UIColor clearColor];
    [btnyrs addTarget:self action:@selector(btn_ElevyearClick:) forControlEvents:UIControlEventTouchUpInside ];
    [img_bg addSubview:btnyrs];
    
    
    lblYrs = [[UILabel alloc]init];
    
    lblYrs.text = @"11-20";
    lblYrs.backgroundColor=[UIColor clearColor];
    lblYrs.textColor=[UIColor blackColor];
    lblYrs.numberOfLines = 0;
    lblYrs.font = [UIFont fontWithName:kFont size:11];
    [img_bg addSubview:lblYrs];
    
    btnyrs1 = [[UIButton alloc] init];
    //    btnyrs1.tag=4;
    [btnyrs1 setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    btnyrs1.backgroundColor = [UIColor clearColor];
    [btnyrs1 addTarget:self action:@selector(btn_moreyearClick:) forControlEvents:UIControlEventTouchUpInside ];
    [img_bg addSubview:btnyrs1];
    
    
    lblYrs1 = [[UILabel alloc]init];
    
    lblYrs1.text = @"more than 20";
    lblYrs1.backgroundColor=[UIColor clearColor];
    lblYrs1.textColor=[UIColor blackColor];
    lblYrs1.numberOfLines = 0;
    lblYrs1.font = [UIFont fontWithName:kFont size:11];
    [img_bg addSubview:lblYrs1];
    
    
    lbl_cookingqual = [[UILabel alloc]init];
    
    lbl_cookingqual.text = @"Do you have a formal cooking\nqualification?";
    lbl_cookingqual.backgroundColor=[UIColor clearColor];
    lbl_cookingqual.textColor=[UIColor blackColor];
    lbl_cookingqual.numberOfLines = 0;
    lbl_cookingqual.font = [UIFont fontWithName:kFont size:13];
    [img_bg addSubview:lbl_cookingqual];
    
    
    
    img_backgroundimage=[[UIImageView alloc]init];
    [img_backgroundimage setUserInteractionEnabled:YES];
    img_backgroundimage.backgroundColor=[UIColor whiteColor];
    //    img_backgroundimage.image=[UIImage imageNamed:@"img.bg@2x.png"];
    img_backgroundimage.userInteractionEnabled = YES;
    [img_bg addSubview:img_backgroundimage];
    
    
    
    
    
    
    buttonimg=[[UIImageView alloc]init];
    
    [buttonimg setUserInteractionEnabled:YES];
    buttonimg.backgroundColor=[UIColor clearColor];
    buttonimg.image=[UIImage imageNamed:@"button img 2.png"];
    [img_backgroundimage addSubview:buttonimg];
    
    btnYes = [[UIButton alloc] init];
    btnYes.backgroundColor = [UIColor clearColor];
    [btnYes addTarget:self action:@selector(btn_YesClick:) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimage addSubview:btnYes];
    
    
    
    lblYes = [[UILabel alloc]init];
    
    lblYes.text = @"Yes";
    lblYes.backgroundColor=[UIColor clearColor];
    lblYes.textColor=[UIColor blackColor];
    lblYes.numberOfLines = 0;
    lblYes.font = [UIFont fontWithName:kFont size:11];
    [img_backgroundimage addSubview:lblYes];
    
    buttonimage=[[UIImageView alloc]init];
    [buttonimage setUserInteractionEnabled:YES];
    buttonimage.backgroundColor=[UIColor clearColor];
    buttonimage.image=[UIImage imageNamed:@"button img 2.png"];
    [img_backgroundimage addSubview:buttonimage];
    
    
    btnNo = [[UIButton alloc] init];
    btnNo.backgroundColor = [UIColor clearColor];
    [btnNo addTarget:self action:@selector(btn_NoClick:) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimage addSubview:btnNo];
    
    
    lblNo = [[UILabel alloc]init];
    lblNo.text = @"No";
    lblNo.backgroundColor=[UIColor clearColor];
    lblNo.textColor=[UIColor blackColor];
    lblNo.numberOfLines = 0;
    lblNo.font = [UIFont fontWithName:kFont size:11];
    [img_backgroundimage addSubview:lblNo];
    
    
    img_backgroundimage2=[[UIImageView alloc]init];
    [img_backgroundimage2 setUserInteractionEnabled:YES];
    img_backgroundimage2.backgroundColor=[UIColor clearColor];
    //    img_backgroundimage2.image=[UIImage imageNamed:@"img.bg@2x.png"];
    img_backgroundimage2.userInteractionEnabled = YES;
    img_backgroundimage2.clipsToBounds=YES;
    [img_bg addSubview:img_backgroundimage2];
    
    
    
    
    lbl_statequalification = [[UILabel alloc]init];
    lbl_statequalification.text = @"State qualification";
    lbl_statequalification.backgroundColor=[UIColor clearColor];
    lbl_statequalification.textColor=[UIColor blackColor];
    lbl_statequalification.numberOfLines = 0;
    lbl_statequalification.font = [UIFont fontWithName:kFont size:13];
    [img_backgroundimage2 addSubview:lbl_statequalification];
    
    
    
    txt_statequatification = [[UITextField alloc] init];
    txt_statequatification.borderStyle = UITextBorderStyleNone;
    txt_statequatification.textColor = [UIColor blackColor];
    txt_statequatification.font = [UIFont fontWithName:kFont size:13];
    txt_statequatification.placeholder = @"";
    [txt_statequatification setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_statequatification setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    txt_statequatification.leftViewMode = UITextFieldViewModeAlways;
    txt_statequatification.userInteractionEnabled=YES;
    txt_statequatification.textAlignment = NSTextAlignmentLeft;
    txt_statequatification.backgroundColor = [UIColor clearColor];
    txt_statequatification.keyboardType = UIKeyboardTypeAlphabet;
    txt_statequatification.delegate = self;
    [img_backgroundimage2 addSubview:txt_statequatification];
    
    
    
    
    
    imgline=[[UIImageView alloc]init];
    
    [imgline setUserInteractionEnabled:YES];
    imgline.backgroundColor=[UIColor clearColor];
    imgline.image=[UIImage imageNamed:@"line1.png"];
    [img_backgroundimage2 addSubview:imgline];
    
    
    
    img_backgroundimage3=[[UIImageView alloc]init];
    [img_backgroundimage3 setUserInteractionEnabled:YES];
    img_backgroundimage3.backgroundColor=[UIColor clearColor];
    //    img_backgroundimage3.image=[UIImage imageNamed:@"img.bg@2x.png"];
    img_backgroundimage3.userInteractionEnabled = YES;
    [img_bg addSubview:img_backgroundimage3];
    
    
    
    lbl_learncook = [[UILabel alloc]init];
    lbl_learncook.text = @"How did you learn to cook ";
    lbl_learncook.backgroundColor=[UIColor clearColor];
    lbl_learncook.textColor=[UIColor blackColor];
    lbl_learncook.numberOfLines = 0;
    lbl_learncook.font = [UIFont fontWithName:kFont size:13];
    [img_backgroundimage3 addSubview:lbl_learncook];
    
    
    
    btn_selftaught = [[UIButton alloc] init];
    btn_selftaught.backgroundColor = [UIColor clearColor];
    btn_selftaught.tag=1;
    [btn_selftaught setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [btn_selftaught addTarget:self action:@selector(click_selectselfTaughtAt:) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimage3 addSubview:btn_selftaught];
    
    
    lbl_selftaught = [[UILabel alloc]init ];
    
    lbl_selftaught.text = @"Self Taught";
    lbl_selftaught.backgroundColor=[UIColor clearColor];
    lbl_selftaught.textColor=[UIColor blackColor];
    lbl_selftaught.numberOfLines = 0;
    lbl_selftaught.font = [UIFont fontWithName:kFont size:13];
    [img_backgroundimage3 addSubview:lbl_selftaught];
    
    
    
    
    btn_Lreaned = [[UIButton alloc] init];
    btn_Lreaned.tag=2;
    [btn_Lreaned setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    
    btn_Lreaned.backgroundColor = [UIColor clearColor];
    [btn_Lreaned addTarget:self action:@selector(click_selectLearnedAt:) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimage3 addSubview:btn_Lreaned];
    
    
    lbl_learned = [[UILabel alloc]init];
    
    lbl_learned.text = @"Learned from a friend or relative";
    lbl_learned.backgroundColor=[UIColor clearColor];
    lbl_learned.textColor=[UIColor blackColor];
    lbl_learned.numberOfLines = 0;
    lbl_learned.font = [UIFont fontWithName:kFont size:13];
    [img_backgroundimage3 addSubview:lbl_learned];
    
    
    btn_others = [[UIButton alloc] init];
    btn_others.tag=3;
    [btn_others setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    
    btn_others.backgroundColor = [UIColor clearColor];
    [btn_others addTarget:self action:@selector(click_selectothersAt:) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimage3 addSubview:btn_others];
    
    
    
    lbl_others= [[UILabel alloc]init];
    
    lbl_others.text = @"Others (please specify)";
    lbl_others.backgroundColor=[UIColor clearColor];
    lbl_others.textColor=[UIColor blackColor];
    lbl_others.numberOfLines = 0;
    lbl_others.font = [UIFont fontWithName:kFont size:12];
    [img_backgroundimage3 addSubview:lbl_others];
    
    
    
    txt_otherslearncook = [[UITextField alloc] init];
    txt_otherslearncook.borderStyle = UITextBorderStyleNone;
    txt_otherslearncook.textColor = [UIColor blackColor];
    txt_otherslearncook.font = [UIFont fontWithName:kFont size:13];
    txt_otherslearncook.placeholder = @"";
    [txt_otherslearncook setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_otherslearncook setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    txt_otherslearncook.leftViewMode = UITextFieldViewModeAlways;
    txt_otherslearncook.userInteractionEnabled=YES;
    txt_otherslearncook.textAlignment = NSTextAlignmentLeft;
    txt_otherslearncook.backgroundColor = [UIColor clearColor];
    txt_otherslearncook.keyboardType = UIKeyboardTypeAlphabet;
    txt_otherslearncook.delegate = self;
    [img_backgroundimage3 addSubview:txt_otherslearncook];
    txt_otherslearncook.hidden=YES;
    
    lineimg=[[UIImageView alloc]init];
    [lineimg setUserInteractionEnabled:YES];
    lineimg.backgroundColor=[UIColor clearColor];
    lineimg.image=[UIImage imageNamed:@"line1.png"];
    [img_backgroundimage3 addSubview:lineimg];
    lineimg.hidden=YES;
    
    
    
    lbl_recommand= [[UILabel alloc]init];
    
    lbl_recommand.text = @"Add a description of yourself as a chef\n(recommended)";
    lbl_recommand.backgroundColor=[UIColor clearColor];
    lbl_recommand.textColor=[UIColor blackColor];
    lbl_recommand.numberOfLines = 0;
    lbl_recommand.font = [UIFont fontWithName:kFont size:13];
    [img_bg addSubview:lbl_recommand];
    
    
    
    txtview1=[[UITextView alloc]init];
    [txtview1 setReturnKeyType:UIReturnKeyDone];
    txtview1.scrollEnabled=YES;
    [txtview1 setDelegate:self];
    txtview1.textColor = [UIColor blackColor];
    [txtview1 setReturnKeyType:UIReturnKeyDone];
    [txtview1 setText:@"This will appear on your public profile page and will provide further information to your potential clients about yourself"];
    [txtview1 setFont:[UIFont fontWithName:@"HelveticaNeue" size:10]];
    [txtview1 setTextColor:[UIColor lightGrayColor]];
    txtview1.userInteractionEnabled=YES;
    txtview1.font=[UIFont fontWithName:kFont size:10];
    txtview1.backgroundColor=[UIColor whiteColor];
    txtview1.delegate=self;
    txtview1.textColor=[UIColor blackColor];
    txtview1.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    txtview1.layer.borderWidth=1.0f;
    txtview1.clipsToBounds=YES;
    [img_bg addSubview:txtview1];
    
    lblmax = [[UILabel alloc]init];
    lblmax.text = @"(500 char.max)";
    lblmax.backgroundColor=[UIColor clearColor];
    lblmax.textColor=[UIColor blackColor];
    lblmax.numberOfLines = 0;
    lblmax.font = [UIFont fontWithName:kFont size:10];
    [img_bg addSubview:lblmax];
    
    img_bg3 = [[UIImageView alloc]init];
    img_bg3.frame =  CGRectMake(-13, 660, WIDTH+35, 80);
    [img_bg3 setUserInteractionEnabled:YES];
    img_bg3.backgroundColor = [UIColor colorWithRed:241/255.0f green:244/255.0f blue:242/255.0f alpha:1];
    [scrollview5 addSubview:img_bg3];
    
    
    page4 = [[UILabel alloc]init];
    page4.text = @"Page 5/6";
    page4.backgroundColor=[UIColor clearColor];
    page4.textColor=[UIColor blackColor];
    page4.numberOfLines = 1;
    page4.textAlignment = NSTextAlignmentCenter;
    page4.font = [UIFont fontWithName:kFont size:13];
    [img_bg3 addSubview:page4];
    
    Nextview = [[UIButton alloc] init];
    
    Nextview.backgroundColor = [UIColor clearColor];
    [Nextview setTitle:@"Next" forState:UIControlStateNormal];
    Nextview.layer.cornerRadius=4.0f;
    [Nextview setImage:[UIImage imageNamed:@"text buuton.png"] forState:UIControlStateNormal];
    [Nextview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [Nextview addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_bg3 addSubview:Nextview];
    
    
    arr_cheftype=[[NSMutableArray alloc]initWithObjects:@"Chef Home",@"Chef Restaurant",@"Chef Others", nil];
    
    
    cheftype_table= [[UITableView alloc] init];
    [cheftype_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    cheftype_table.delegate = self;
    cheftype_table.dataSource = self;
    cheftype_table.showsVerticalScrollIndicator = NO;
    cheftype_table.backgroundColor = [UIColor clearColor];
    cheftype_table.layer.borderColor = [[UIColor blackColor]CGColor];
    cheftype_table.layer.borderWidth = 1.0f;
    cheftype_table.clipsToBounds = YES;
    [img_bg addSubview:cheftype_table];
    [cheftype_table setHidden:YES];
    
    
    
    if (IS_IPHONE_6Plus)
    {
        img_bg.frame = CGRectMake(0, -12, WIDTH+14, 620);
        scrollview5.frame=CGRectMake(0, 45, WIDTH, HEIGHT);
        lbl_chef.frame=CGRectMake(0,20, self.view.frame.size.width,30);
        NSLog(@"%f",self.view.frame.size.width);
        txt_cheftype.frame=CGRectMake(30,CGRectGetMaxY(lbl_chef.frame)+1, self.view.frame.size.width-40, 38);
        line_img.frame=CGRectMake(30, CGRectGetMaxY(txt_cheftype.frame)-6, self.view.frame.size.width-60, 0.5);
        dropbox_img.frame = CGRectMake(self.view.frame.size.width-50, CGRectGetMaxY(lbl_chef.frame)+20, 15, 10);
        dropbox_btn.frame=CGRectMake(30,CGRectGetMaxY(lbl_chef.frame)+1, self.view.frame.size.width-40, 38);
        cheftype_table.frame=CGRectMake(30,CGRectGetMaxY(txt_cheftype.frame)+1, self.view.frame.size.width-60, 75);
        txt_others.frame=CGRectMake(30,CGRectGetMaxY(txt_cheftype.frame)+5, self.view.frame.size.width-40, 38);
        img_line.frame=CGRectMake(30, CGRectGetMaxY(txt_others.frame)-6, self.view.frame.size.width-60, 0.5);
        lbl_cookingexp.frame=CGRectMake(60,CGRectGetMaxY(txt_cheftype.frame)+10, 300,30);
        btn_Yes.frame = CGRectMake(30, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
        button_img.frame=CGRectMake(30, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
        lbl_Yes.frame = CGRectMake(60,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
        btn_yrs.frame = CGRectMake(90, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
        button_imge.frame=CGRectMake(90, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
        lbl_Yrs.frame = CGRectMake(120,CGRectGetMaxY(lbl_cookingexp.frame)+1, 30,20);
        btnyrs.frame = CGRectMake(150, CGRectGetMaxY(lbl_cookingexp.frame)+3, 15,15);
        buttonimge.frame=CGRectMake(150, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
        lblYrs.frame = CGRectMake(180,CGRectGetMaxY(lbl_cookingexp.frame)+2, 40,20);
        btnyrs1.frame = CGRectMake(220, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
        buttonimges.frame=CGRectMake(220, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
        lblYrs1.frame = CGRectMake(250,CGRectGetMaxY(lbl_cookingexp.frame)+2, 120,20);
        lbl_cookingqual.frame = CGRectMake(30,CGRectGetMaxY(lbl_Yes.frame)+20, 300,50);
        img_backgroundimage.frame=CGRectMake(30, CGRectGetMaxY(lbl_cookingqual.frame)+5, WIDTH, 30);
        btnYes.frame = CGRectMake(0, 10, 30,30);
        buttonimg.frame=CGRectMake(0, 10, 15, 15);
        lblYes.frame = CGRectMake(20,10, 30,15);
        btnNo.frame = CGRectMake(50, 10, 30,30);
        buttonimage.frame=CGRectMake(50, 10, 15, 15);
        lblNo.frame = CGRectMake(70,10, 30,15);
        img_backgroundimage2.frame=CGRectMake(30, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 60);
        lbl_statequalification.frame = CGRectMake(0,5, 150,25);
        txt_statequatification.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+1, 200, 30);
        imgline.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+24, self.view.frame.size.width-57, 0.5);
        img_backgroundimage3.frame=CGRectMake(30, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 120);
        lbl_learncook.frame = CGRectMake(0,5, 250,25);
        btn_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+7, 15,15);
        img_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+5, 15, 15);
        lbl_selftaught.frame = CGRectMake(20,CGRectGetMaxY(lbl_learncook.frame)+3, 300,20);
        btn_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
        img_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
        lbl_learned.frame= CGRectMake(20,CGRectGetMaxY(lbl_selftaught.frame)+3, 300,20);
        btn_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15,15);
        img_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15, 15);
        lbl_others.frame = CGRectMake(20,CGRectGetMaxY(lbl_learned.frame)+1, 300,20);
        txt_otherslearncook.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+3, 300, 20);
        lineimg.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+20, self.view.frame.size.width-77, 0.5);
        lbl_recommand.frame = CGRectMake(30,CGRectGetMaxY(img_backgroundimage.frame)+8, 300,50);
        txtview1.frame=CGRectMake(30, CGRectGetMaxY(lbl_recommand.frame)+15, self.view.frame.size.width-57, 100);
        lblmax.frame = CGRectMake(self.view.frame.size.width-105,CGRectGetMaxY(txtview1.frame)+1, 100,10);
        img_bg3.frame=CGRectMake(0, CGRectGetMaxY(txtview1.frame)+180, WIDTH, 100);
//        img_bg3.frame =  CGRectMake(0, HEIGHT-100, WIDTH, 100);
        page4.frame = CGRectMake(WIDTH/2-205,0, WIDTH,30);
        Nextview.frame = CGRectMake(18, 40, WIDTH-36,50);
        [scrollview5 setContentSize:CGSizeMake(0,CGRectGetMaxY(img_bg.frame)+170)];

    }
    else if (IS_IPHONE_6)
    {
        img_bg.frame = CGRectMake(0, -12, WIDTH+12, 620);
        scrollview5.frame=CGRectMake(0, 45, WIDTH, HEIGHT);
        lbl_chef.frame=CGRectMake(0,20, self.view.frame.size.width,30);
        NSLog(@"%f",self.view.frame.size.width);
        txt_cheftype.frame=CGRectMake(20,CGRectGetMaxY(lbl_chef.frame)+1, self.view.frame.size.width-40, 50);
        line_img.frame=CGRectMake(20, CGRectGetMaxY(txt_cheftype.frame)-6, self.view.frame.size.width-40, 0.5);
        dropbox_img.frame = CGRectMake(self.view.frame.size.width-40, CGRectGetMaxY(lbl_chef.frame)+30, 15, 10);
        dropbox_btn.frame=CGRectMake(20,CGRectGetMaxY(lbl_chef.frame)+1, self.view.frame.size.width-40, 38);
        cheftype_table.frame=CGRectMake(20,CGRectGetMaxY(txt_cheftype.frame), WIDTH-40, 100);
        txt_others.frame=CGRectMake(20,CGRectGetMaxY(txt_cheftype.frame)+5, self.view.frame.size.width-40, 38);
        img_line.frame=CGRectMake(20, CGRectGetMaxY(txt_others.frame)-5, self.view.frame.size.width-40, 0.5);
        lbl_cookingexp.frame=CGRectMake(55,CGRectGetMaxY(txt_cheftype.frame)+10, 300,30);
        btn_Yes.frame = CGRectMake(20, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
        button_img.frame=CGRectMake(20, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
        lbl_Yes.frame = CGRectMake(50,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
        btn_yrs.frame = CGRectMake(80, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
        button_imge.frame=CGRectMake(80, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
        lbl_Yrs.frame = CGRectMake(110,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
        btnyrs.frame = CGRectMake(150, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
        buttonimge.frame=CGRectMake(150, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
        lblYrs.frame = CGRectMake(170,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
        btnyrs1.frame = CGRectMake(210, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
        buttonimges.frame=CGRectMake(210, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
        lblYrs1.frame = CGRectMake(240,CGRectGetMaxY(lbl_cookingexp.frame)+5, 120,15);
        lbl_cookingqual.frame = CGRectMake(20,CGRectGetMaxY(lbl_Yes.frame)+20, 300,50);
        img_backgroundimage.frame=CGRectMake(20, CGRectGetMaxY(lbl_cookingqual.frame)+5, WIDTH, 30);
        btnYes.frame = CGRectMake(0, 10, 30,30);
        buttonimg.frame=CGRectMake(0, 10, 15, 15);
        lblYes.frame = CGRectMake(20,10, 30,15);
        btnNo.frame = CGRectMake(50, 10, 30,30);
        buttonimage.frame=CGRectMake(50, 10, 15, 15);
        lblNo.frame = CGRectMake(70,10, 30,15);
        img_backgroundimage2.frame=CGRectMake(20, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 60);
        lbl_statequalification.frame = CGRectMake(0,5, 150,25);
        txt_statequatification.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+1, 200, 30);
        imgline.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+24, self.view.frame.size.width-55, 0.5);
        img_backgroundimage3.frame=CGRectMake(20, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 120);
        lbl_learncook.frame = CGRectMake(0,5, 250,25);
        btn_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+7, 15,15);
        img_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+5, 15, 15);
        lbl_selftaught.frame = CGRectMake(20,CGRectGetMaxY(lbl_learncook.frame)+3, 300,20);
        btn_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
        img_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
        lbl_learned.frame= CGRectMake(20,CGRectGetMaxY(lbl_selftaught.frame)+3, 300,20);
        btn_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15,15);
        img_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15, 15);
        lbl_others.frame = CGRectMake(20,CGRectGetMaxY(lbl_learned.frame)+1, 300,20);
        txt_otherslearncook.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+3, 300, 20);
        lineimg.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+20, self.view.frame.size.width-75, 0.5);
        lbl_recommand.frame = CGRectMake(20,CGRectGetMaxY(img_backgroundimage.frame)+8, 300,50);
        txtview1.frame=CGRectMake(20, CGRectGetMaxY(lbl_recommand.frame)+15, self.view.frame.size.width-40, 100);
        lblmax.frame = CGRectMake(self.view.frame.size.width-95,CGRectGetMaxY(txtview1.frame)+1, 100,10);
        img_bg3.frame=CGRectMake(0, CGRectGetMaxY(txtview1.frame)+180, WIDTH, 100);
//        img_bg3.frame=CGRectMake(0, HEIGHT-100, WIDTH, 100);
        page4.frame =CGRectMake(0,4, WIDTH,30);
        Nextview.frame = CGRectMake(18, 40, WIDTH-36,50);
        [scrollview5 setContentSize:CGSizeMake(0,CGRectGetMaxY(img_bg.frame)+170)];

       

    
    }
    else
    {
        img_bg.frame = CGRectMake(-5, -12, WIDTH+12, 600);
        scrollview5.frame=CGRectMake(0, 45, WIDTH, HEIGHT);
        lbl_chef.frame=CGRectMake(0,20, self.view.frame.size.width,30);
        NSLog(@"%f",self.view.frame.size.width);
        txt_cheftype.frame=CGRectMake(25,CGRectGetMaxY(lbl_chef.frame), WIDTH-36, 50);
        line_img.frame=CGRectMake(25, CGRectGetMaxY(txt_cheftype.frame)-15, WIDTH-36, 0.5);
        dropbox_img.frame = CGRectMake(self.view.frame.size.width-30, CGRectGetMaxY(lbl_chef.frame)+20, 15, 10);
        dropbox_btn.frame=CGRectMake(25,CGRectGetMaxY(lbl_chef.frame)+1, self.view.frame.size.width-36, 38);
        cheftype_table.frame=CGRectMake(25,CGRectGetMaxY(txt_cheftype.frame)-10, self.view.frame.size.width-45, 100);
        img_backgroundimag.frame=CGRectMake(25,CGRectGetMaxY(txt_cheftype.frame)-5, WIDTH-36, 45);
        txt_others.frame=CGRectMake(0,0, self.view.frame.size.width-40, 38);
        img_line.frame=CGRectMake(0, CGRectGetMaxY(txt_others.frame)-6, WIDTH-36, 0.5);
        lbl_cookingexp.frame=CGRectMake(50,CGRectGetMaxY(txt_cheftype.frame), 300,30);
        btn_Yes.frame = CGRectMake(25, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
        button_img.frame=CGRectMake(25, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
        lbl_Yes.frame = CGRectMake(50,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
        btn_yrs.frame = CGRectMake(80, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
        button_imge.frame=CGRectMake(80, CGRectGetMaxY(lbl_cookingexp.frame)+5, 30, 15);
        lbl_Yrs.frame = CGRectMake(105,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
        btnyrs.frame = CGRectMake(145, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
        buttonimge.frame=CGRectMake(145, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
        lblYrs.frame = CGRectMake(170,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
        btnyrs1.frame = CGRectMake(210, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
        buttonimges.frame=CGRectMake(210, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
        lblYrs1.frame = CGRectMake(235,CGRectGetMaxY(lbl_cookingexp.frame)+5, 120,15);
        lbl_cookingqual.frame = CGRectMake(25,CGRectGetMaxY(lbl_Yes.frame)+20, 300,50);
        img_backgroundimage.frame=CGRectMake(25, CGRectGetMaxY(lbl_cookingqual.frame)+5, WIDTH, 30);
        btnYes.frame = CGRectMake(0, 10, 30,30);
        buttonimg.frame=CGRectMake(0, 10, 15, 15);
        lblYes.frame = CGRectMake(20,10, 30,15);
        btnNo.frame = CGRectMake(50, 10, 30,30);
        buttonimage.frame=CGRectMake(50, 10, 15, 15);
        lblNo.frame = CGRectMake(70,10, 30,15);
        img_backgroundimage2.frame=CGRectMake(25, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 60);
        lbl_statequalification.frame = CGRectMake(0,5, 150,25);
        txt_statequatification.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+1, 200, 30);
        imgline.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+24, self.view.frame.size.width-48, 0.5);
        img_backgroundimage3.frame=CGRectMake(25, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 120);
        lbl_learncook.frame = CGRectMake(0,5, 250,25);
        btn_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+7, 15,15);
        img_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+5, 15, 15);
        lbl_selftaught.frame = CGRectMake(20,CGRectGetMaxY(lbl_learncook.frame)+3, 300,20);
        btn_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
        img_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
        lbl_learned.frame= CGRectMake(20,CGRectGetMaxY(lbl_selftaught.frame)+3, 300,20);
        btn_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15,15);
        img_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15, 15);
        lbl_others.frame = CGRectMake(20,CGRectGetMaxY(lbl_learned.frame)+1, 300,20);
        txt_otherslearncook.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+3, 300, 20);
        lineimg.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+20, self.view.frame.size.width-68, 0.5);
        lbl_recommand.frame = CGRectMake(25,CGRectGetMaxY(img_backgroundimage.frame)+8, 300,50);
        txtview1.frame=CGRectMake(25, CGRectGetMaxY(lbl_recommand.frame)+15, self.view.frame.size.width-40, 100);
        lblmax.frame = CGRectMake(self.view.frame.size.width-95,CGRectGetMaxY(txtview1.frame)+1, 100,10);
        img_bg3.frame=CGRectMake(0, CGRectGetMaxY(txtview1.frame)+180, WIDTH, 100);
//        img_bg3.frame=CGRectMake(0, HEIGHT-80, WIDTH, 100);
        page4.frame =CGRectMake(0,0, WIDTH,30);
        Nextview.frame = CGRectMake(18, 35, WIDTH-36,40);
        [scrollview5 setContentSize:CGSizeMake(0,CGRectGetMaxY(img_bg.frame)+150)];

        
    }
    
//    [scrollview5 setContentSize:CGSizeMake(0,740)];
    
    
}

#pragma mark - textView delegates

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        if(txtview1.text.length == 0){
            txtview1.textColor = [UIColor grayColor];
            txtview1.text = @"This will appers on your public profile page and will\nprovide further informations to your potential clients\nabout yourself";
            [txtview1 resignFirstResponder];
        }
        else if (txtview1.text.length > 500)
        {
            return NO;
        }
        return NO;
    }
    
    return YES;
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    textView.text = @"";
    return YES;
}

#pragma mark UITableview methods



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView== cheftype_table)
    {
        return [arr_cheftype count];
        
    }
    
    return 0;
    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    UILabel  * lbl_txt = [[UILabel alloc]init];
    lbl_txt.frame=CGRectMake(10, 0, 140, 25);
    lbl_txt.text = [arr_cheftype objectAtIndex:indexPath.row];
    lbl_txt.backgroundColor=[UIColor clearColor];
    lbl_txt.textColor=[UIColor blackColor];
    lbl_txt.numberOfLines = 1;
    lbl_txt.textAlignment = NSTextAlignmentLeft;
    lbl_txt.font = [UIFont fontWithName:kFont size:13];
    [cell.contentView addSubview:lbl_txt];
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView== cheftype_table)
    {
        return 25;
        
    }
    
    return 0;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView== cheftype_table)
    {
        txt_cheftype.text =[arr_cheftype objectAtIndex:indexPath.row];
        
        
        if (indexPath.row ==0)
        {
            
            str_cheftype = @"28";
            img_backgroundimag.hidden=YES;
            txt_others.hidden = YES;
            img_line.hidden = YES;
            
            if(IS_IPHONE_6Plus)
            {
                lbl_cookingexp.frame=CGRectMake(60,CGRectGetMaxY(txt_cheftype.frame)+10, 300,30);
                
                btn_Yes.frame = CGRectMake(30, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                button_img.frame=CGRectMake(30, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lbl_Yes.frame = CGRectMake(50,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btn_yrs.frame = CGRectMake(80, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                button_imge.frame=CGRectMake(80, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                
                lbl_Yrs.frame = CGRectMake(100,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btnyrs.frame = CGRectMake(130, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                buttonimge.frame=CGRectMake(130, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                
                lblYrs.frame = CGRectMake(150,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btnyrs1.frame = CGRectMake(190, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                buttonimges.frame=CGRectMake(190, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lblYrs1.frame = CGRectMake(210,CGRectGetMaxY(lbl_cookingexp.frame)+5, 120,15);
                
                lbl_cookingqual.frame = CGRectMake(30,CGRectGetMaxY(lbl_Yes.frame)+20, 300,50);
                
                img_backgroundimage.frame=CGRectMake(30, CGRectGetMaxY(lbl_cookingqual.frame)+5, WIDTH, 30);
                
                btnYes.frame = CGRectMake(0, 10, 30,30);
                buttonimg.frame=CGRectMake(0, 10, 15, 15);
                lblYes.frame = CGRectMake(20,10, 30,15);
                btnNo.frame = CGRectMake(50, 10, 30,30);
                buttonimage.frame=CGRectMake(50, 10, 15, 15);
                
                lblNo.frame = CGRectMake(70,10, 30,15);
                
                img_backgroundimage2.frame=CGRectMake(30, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 60);
                img_backgroundimage2.hidden=YES;
                
                lbl_statequalification.frame = CGRectMake(0,5, 150,25);
                txt_statequatification.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+1, 200, 30);
                imgline.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+24, self.view.frame.size.width-57, 0.5);
                img_backgroundimage3.frame=CGRectMake(30, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 120);
                img_backgroundimage3.hidden=YES;
                
                lbl_learncook.frame = CGRectMake(0,5, 250,25);
                btn_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+7, 15,15);
                img_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+5, 15, 15);
                lbl_selftaught.frame = CGRectMake(20,CGRectGetMaxY(lbl_learncook.frame)+3, 300,20);
                btn_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
                img_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
                lbl_learned.frame= CGRectMake(20,CGRectGetMaxY(lbl_selftaught.frame)+3, 300,20);
                btn_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15,15);
                img_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15, 15);
                lbl_others.frame = CGRectMake(20,CGRectGetMaxY(lbl_learned.frame)+1, 300,20);
                txt_otherslearncook.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+3, 300, 20);
                lineimg.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+20, self.view.frame.size.width-77, 0.5);
                lbl_recommand.frame = CGRectMake(30,CGRectGetMaxY(img_backgroundimage.frame)+8, 300,50);
                txtview1.frame=CGRectMake(30, CGRectGetMaxY(lbl_recommand.frame)+15, self.view.frame.size.width-57, 100);
                lblmax.frame = CGRectMake(self.view.frame.size.width-105,CGRectGetMaxY(txtview1.frame)+1, 100,10);
                img_bg3.frame =  CGRectMake(0, HEIGHT-100, WIDTH, 100);
                page4.frame = CGRectMake(WIDTH/2-205,0, WIDTH,30);
                Nextview.frame = CGRectMake(18, 40, WIDTH-36,50);
            }
            else if(IS_IPHONE_6)
            {
                
                lbl_cookingexp.frame=CGRectMake(55,CGRectGetMaxY(txt_cheftype.frame)+10, 300,30);
                btn_Yes.frame = CGRectMake(20, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                button_img.frame=CGRectMake(20, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lbl_Yes.frame = CGRectMake(50,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btn_yrs.frame = CGRectMake(80, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                button_imge.frame=CGRectMake(80, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lbl_Yrs.frame = CGRectMake(110,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btnyrs.frame = CGRectMake(150, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                buttonimge.frame=CGRectMake(150, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lblYrs.frame = CGRectMake(170,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btnyrs1.frame = CGRectMake(210, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                buttonimges.frame=CGRectMake(210, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lblYrs1.frame = CGRectMake(240,CGRectGetMaxY(lbl_cookingexp.frame)+5, 120,15);
                lbl_cookingqual.frame = CGRectMake(20,CGRectGetMaxY(lbl_Yes.frame)+20, 300,50);
                img_backgroundimage.frame=CGRectMake(20, CGRectGetMaxY(lbl_cookingqual.frame)+5, WIDTH, 30);
                btnYes.frame = CGRectMake(0, 10, 30,30);
                buttonimg.frame=CGRectMake(0, 10, 15, 15);
                lblYes.frame = CGRectMake(20,10, 30,15);
                btnNo.frame = CGRectMake(50, 10, 30,30);
                buttonimage.frame=CGRectMake(50, 10, 15, 15);
                lblNo.frame = CGRectMake(70,10, 30,15);
                img_backgroundimage2.frame=CGRectMake(20, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 60);
                img_backgroundimage2.hidden=YES;
                lbl_statequalification.frame = CGRectMake(0,5, 150,25);
                txt_statequatification.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+1, 200, 30);
                imgline.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+24, self.view.frame.size.width-55, 0.5);
                img_backgroundimage3.frame=CGRectMake(20, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 120);
                img_backgroundimage3.hidden=YES;
                lbl_learncook.frame = CGRectMake(0,5, 250,25);
                btn_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+7, 15,15);
                img_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+5, 15, 15);
                lbl_selftaught.frame = CGRectMake(20,CGRectGetMaxY(lbl_learncook.frame)+3, 300,20);
                btn_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
                img_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
                lbl_learned.frame= CGRectMake(20,CGRectGetMaxY(lbl_selftaught.frame)+3, 300,20);
                btn_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15,15);
                img_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15, 15);
                lbl_others.frame = CGRectMake(20,CGRectGetMaxY(lbl_learned.frame)+1, 300,20);
                txt_otherslearncook.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+3, 300, 20);
                lineimg.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+20, self.view.frame.size.width-75, 0.5);
                lbl_recommand.frame = CGRectMake(20,CGRectGetMaxY(img_backgroundimage.frame)+8, 300,50);
                txtview1.frame=CGRectMake(20, CGRectGetMaxY(lbl_recommand.frame)+15, self.view.frame.size.width-40, 100);
                lblmax.frame = CGRectMake(self.view.frame.size.width-95,CGRectGetMaxY(txtview1.frame)+1, 100,10);
                img_bg3.frame=CGRectMake(0, CGRectGetMaxY(txtview1.frame)+190, WIDTH, 100);
                //        img_bg3.frame=CGRectMake(0, HEIGHT-100, WIDTH, 100);
                page4.frame =CGRectMake(0,4, WIDTH,30);
                Nextview.frame = CGRectMake(18, 40, WIDTH-36,50);
                
                
                
                
            }
            else
            {
                //                str_cheftype = @"28";
                //                img_backgroundimag.hidden=YES;
                //                txt_others.hidden = YES;
                //                img_line.hidden = YES;
                
                lbl_cookingexp.frame=CGRectMake(50,CGRectGetMaxY(txt_cheftype.frame), 300,30);
                btn_Yes.frame = CGRectMake(30, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                button_img.frame=CGRectMake(30, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lbl_Yes.frame = CGRectMake(60,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btn_yrs.frame = CGRectMake(90, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                button_imge.frame=CGRectMake(90, CGRectGetMaxY(lbl_cookingexp.frame)+5, 30, 15);
                lbl_Yrs.frame = CGRectMake(110,CGRectGetMaxY(lbl_cookingexp.frame)+1, 30,20);
                btnyrs.frame = CGRectMake(140, CGRectGetMaxY(lbl_cookingexp.frame)+3, 15,15);
                buttonimge.frame=CGRectMake(140, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lblYrs.frame = CGRectMake(160,CGRectGetMaxY(lbl_cookingexp.frame)+2, 40,20);
                btnyrs1.frame = CGRectMake(200, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                buttonimges.frame=CGRectMake(200, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lblYrs1.frame = CGRectMake(220,CGRectGetMaxY(lbl_cookingexp.frame)+2, 120,20);
                lbl_cookingqual.frame = CGRectMake(25,CGRectGetMaxY(lbl_Yes.frame)+20, 300,50);
                img_backgroundimage.frame=CGRectMake(25, CGRectGetMaxY(lbl_cookingqual.frame)+5, WIDTH, 30);
                btnYes.frame = CGRectMake(0, 10, 30,30);
                buttonimg.frame=CGRectMake(0, 10, 15, 15);
                lblYes.frame = CGRectMake(20,10, 30,15);
                btnNo.frame = CGRectMake(50, 10, 30,30);
                buttonimage.frame=CGRectMake(50, 10, 15, 15);
                lblNo.frame = CGRectMake(70,10, 30,15);
                img_backgroundimage2.frame=CGRectMake(25, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 60);
                img_backgroundimage2.hidden=YES;
                //      img_backgroundimage3.frame=CGRectMake(40, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 120);
                lbl_statequalification.frame = CGRectMake(0,5, 150,25);
                txt_statequatification.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+1, 200, 30);
                imgline.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+24, self.view.frame.size.width-48, 0.5);
                img_backgroundimage3.frame=CGRectMake(25, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 120);
                img_backgroundimage3.hidden=YES;
                lbl_learncook.frame = CGRectMake(0,5, 250,25);
                btn_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+7, 15,15);
                img_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+5, 15, 15);
                lbl_selftaught.frame = CGRectMake(20,CGRectGetMaxY(lbl_learncook.frame)+3, 300,20);
                btn_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
                img_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
                lbl_learned.frame= CGRectMake(20,CGRectGetMaxY(lbl_selftaught.frame)+3, 300,20);
                btn_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15,15);
                img_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15, 15);
                lbl_others.frame = CGRectMake(20,CGRectGetMaxY(lbl_learned.frame)+1, 300,20);
                txt_otherslearncook.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+3, 300, 20);
                lineimg.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+20, self.view.frame.size.width-68, 0.5);
                lbl_recommand.frame = CGRectMake(25,CGRectGetMaxY(img_backgroundimage.frame)+8, 300,50);
                txtview1.frame=CGRectMake(25, CGRectGetMaxY(lbl_recommand.frame)+15, self.view.frame.size.width-40, 100);
                lblmax.frame = CGRectMake(self.view.frame.size.width-95,CGRectGetMaxY(txtview1.frame)+1, 100,10);
                img_bg3.frame=CGRectMake(0, CGRectGetMaxY(txtview1.frame)+180, WIDTH, 100);
                //        img_bg3.frame=CGRectMake(0, HEIGHT-80, WIDTH, 100);
                page4.frame =CGRectMake(0,0, WIDTH,30);
                Nextview.frame = CGRectMake(18, 35, WIDTH-36,40);
                
              
                
                
            }
            
            
            
        }
        else if (indexPath.row == 1)
        {
            str_cheftype = @"28";
            img_backgroundimag.hidden=YES;
            txt_others.hidden = YES;
            img_line.hidden = YES;
            
            if(IS_IPHONE_6Plus)
            {
                lbl_cookingexp.frame=CGRectMake(60,CGRectGetMaxY(txt_cheftype.frame)+10, 300,30);
                
                btn_Yes.frame = CGRectMake(30, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                button_img.frame=CGRectMake(30, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lbl_Yes.frame = CGRectMake(50,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btn_yrs.frame = CGRectMake(80, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                button_imge.frame=CGRectMake(80, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                
                lbl_Yrs.frame = CGRectMake(100,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btnyrs.frame = CGRectMake(130, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                buttonimge.frame=CGRectMake(130, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                
                lblYrs.frame = CGRectMake(150,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btnyrs1.frame = CGRectMake(190, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                buttonimges.frame=CGRectMake(190, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lblYrs1.frame = CGRectMake(210,CGRectGetMaxY(lbl_cookingexp.frame)+5, 120,15);
                
                lbl_cookingqual.frame = CGRectMake(30,CGRectGetMaxY(lbl_Yes.frame)+20, 300,50);
                
                img_backgroundimage.frame=CGRectMake(30, CGRectGetMaxY(lbl_cookingqual.frame)+5, WIDTH, 30);
                
                btnYes.frame = CGRectMake(0, 10, 30,30);
                buttonimg.frame=CGRectMake(0, 10, 15, 15);
                lblYes.frame = CGRectMake(20,10, 30,15);
                btnNo.frame = CGRectMake(50, 10, 30,30);
                buttonimage.frame=CGRectMake(50, 10, 15, 15);
                
                lblNo.frame = CGRectMake(70,10, 30,15);
                
                img_backgroundimage2.frame=CGRectMake(30, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 60);
                img_backgroundimage2.hidden=YES;
                
                lbl_statequalification.frame = CGRectMake(0,5, 150,25);
                txt_statequatification.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+1, 200, 30);
                imgline.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+24, self.view.frame.size.width-57, 0.5);
                img_backgroundimage3.frame=CGRectMake(30, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 120);
                img_backgroundimage3.hidden=YES;
                
                lbl_learncook.frame = CGRectMake(0,5, 250,25);
                btn_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+7, 15,15);
                img_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+5, 15, 15);
                lbl_selftaught.frame = CGRectMake(20,CGRectGetMaxY(lbl_learncook.frame)+3, 300,20);
                btn_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
                img_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
                lbl_learned.frame= CGRectMake(20,CGRectGetMaxY(lbl_selftaught.frame)+3, 300,20);
                btn_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15,15);
                img_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15, 15);
                lbl_others.frame = CGRectMake(20,CGRectGetMaxY(lbl_learned.frame)+1, 300,20);
                txt_otherslearncook.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+3, 300, 20);
                lineimg.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+20, self.view.frame.size.width-77, 0.5);
                lbl_recommand.frame = CGRectMake(30,CGRectGetMaxY(img_backgroundimage.frame)+8, 300,50);
                txtview1.frame=CGRectMake(30, CGRectGetMaxY(lbl_recommand.frame)+15, self.view.frame.size.width-57, 100);
                lblmax.frame = CGRectMake(self.view.frame.size.width-105,CGRectGetMaxY(txtview1.frame)+1, 100,10);
                img_bg3.frame =  CGRectMake(0, HEIGHT-100, WIDTH, 100);
                page4.frame = CGRectMake(WIDTH/2-205,0, WIDTH,30);
                Nextview.frame = CGRectMake(18, 40, WIDTH-36,50);
            }
            else if(IS_IPHONE_6)
            {
                lbl_cookingexp.frame=CGRectMake(55,CGRectGetMaxY(txt_cheftype.frame)+10, 300,30);
                btn_Yes.frame = CGRectMake(20, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                button_img.frame=CGRectMake(20, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lbl_Yes.frame = CGRectMake(50,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btn_yrs.frame = CGRectMake(80, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                button_imge.frame=CGRectMake(80, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lbl_Yrs.frame = CGRectMake(110,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btnyrs.frame = CGRectMake(150, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                buttonimge.frame=CGRectMake(150, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lblYrs.frame = CGRectMake(170,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btnyrs1.frame = CGRectMake(210, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                buttonimges.frame=CGRectMake(210, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lblYrs1.frame = CGRectMake(240,CGRectGetMaxY(lbl_cookingexp.frame)+5, 120,15);
                lbl_cookingqual.frame = CGRectMake(20,CGRectGetMaxY(lbl_Yes.frame)+20, 300,50);
                img_backgroundimage.frame=CGRectMake(20, CGRectGetMaxY(lbl_cookingqual.frame)+5, WIDTH, 30);
                btnYes.frame = CGRectMake(0, 10, 30,30);
                buttonimg.frame=CGRectMake(0, 10, 15, 15);
                lblYes.frame = CGRectMake(20,10, 30,15);
                btnNo.frame = CGRectMake(50, 10, 30,30);
                buttonimage.frame=CGRectMake(50, 10, 15, 15);
                lblNo.frame = CGRectMake(70,10, 30,15);
                img_backgroundimage2.frame=CGRectMake(20, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 60);
                img_backgroundimage2.hidden=YES;
                lbl_statequalification.frame = CGRectMake(0,5, 150,25);
                txt_statequatification.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+1, 200, 30);
                imgline.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+24, self.view.frame.size.width-55, 0.5);
                img_backgroundimage3.frame=CGRectMake(20, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 120);
                img_backgroundimage3.hidden=YES;
                lbl_learncook.frame = CGRectMake(0,5, 250,25);
                btn_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+7, 15,15);
                img_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+5, 15, 15);
                lbl_selftaught.frame = CGRectMake(20,CGRectGetMaxY(lbl_learncook.frame)+3, 300,20);
                btn_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
                img_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
                lbl_learned.frame= CGRectMake(20,CGRectGetMaxY(lbl_selftaught.frame)+3, 300,20);
                btn_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15,15);
                img_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15, 15);
                lbl_others.frame = CGRectMake(20,CGRectGetMaxY(lbl_learned.frame)+1, 300,20);
                txt_otherslearncook.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+3, 300, 20);
                lineimg.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+20, self.view.frame.size.width-75, 0.5);
                lbl_recommand.frame = CGRectMake(20,CGRectGetMaxY(img_backgroundimage.frame)+8, 300,50);
                txtview1.frame=CGRectMake(20, CGRectGetMaxY(lbl_recommand.frame)+15, self.view.frame.size.width-40, 100);
                lblmax.frame = CGRectMake(self.view.frame.size.width-95,CGRectGetMaxY(txtview1.frame)+1, 100,10);
                img_bg3.frame=CGRectMake(0, CGRectGetMaxY(txtview1.frame)+190, WIDTH, 100);
                //        img_bg3.frame=CGRectMake(0, HEIGHT-100, WIDTH, 100);
                page4.frame =CGRectMake(0,4, WIDTH,30);
                Nextview.frame = CGRectMake(18, 40, WIDTH-36,50);
                
            }
            else
            {
                
                //            img_backgroundimag.hidden=YES;
                //            str_cheftype = @"29";
                //            txt_others.hidden = YES;
                //            img_line.hidden = YES;
                
                lbl_cookingexp.frame=CGRectMake(50,CGRectGetMaxY(txt_cheftype.frame), 300,30);
                btn_Yes.frame = CGRectMake(30, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                button_img.frame=CGRectMake(30, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lbl_Yes.frame = CGRectMake(60,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btn_yrs.frame = CGRectMake(90, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                button_imge.frame=CGRectMake(90, CGRectGetMaxY(lbl_cookingexp.frame)+5, 30, 15);
                lbl_Yrs.frame = CGRectMake(110,CGRectGetMaxY(lbl_cookingexp.frame)+1, 30,20);
                btnyrs.frame = CGRectMake(140, CGRectGetMaxY(lbl_cookingexp.frame)+3, 15,15);
                buttonimge.frame=CGRectMake(140, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lblYrs.frame = CGRectMake(160,CGRectGetMaxY(lbl_cookingexp.frame)+2, 40,20);
                btnyrs1.frame = CGRectMake(200, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                buttonimges.frame=CGRectMake(200, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lblYrs1.frame = CGRectMake(220,CGRectGetMaxY(lbl_cookingexp.frame)+2, 120,20);
                lbl_cookingqual.frame = CGRectMake(25,CGRectGetMaxY(lbl_Yes.frame)+20, 300,50);
                img_backgroundimage.frame=CGRectMake(25, CGRectGetMaxY(lbl_cookingqual.frame)+5, WIDTH, 30);
                btnYes.frame = CGRectMake(0, 10, 30,30);
                buttonimg.frame=CGRectMake(0, 10, 15, 15);
                lblYes.frame = CGRectMake(20,10, 30,15);
                btnNo.frame = CGRectMake(50, 10, 30,30);
                buttonimage.frame=CGRectMake(50, 10, 15, 15);
                lblNo.frame = CGRectMake(70,10, 30,15);
                img_backgroundimage2.frame=CGRectMake(25, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 60);
                img_backgroundimage2.hidden=YES;
                lbl_statequalification.frame = CGRectMake(0,5, 150,25);
                txt_statequatification.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+1, 200, 30);
                imgline.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+24, self.view.frame.size.width-48, 0.5);
                img_backgroundimage3.frame=CGRectMake(25, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 120);
                img_backgroundimage3.hidden=YES;
                lbl_learncook.frame = CGRectMake(0,5, 250,25);
                btn_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+7, 15,15);
                img_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+5, 15, 15);
                lbl_selftaught.frame = CGRectMake(20,CGRectGetMaxY(lbl_learncook.frame)+3, 300,20);
                btn_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
                img_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
                lbl_learned.frame= CGRectMake(20,CGRectGetMaxY(lbl_selftaught.frame)+3, 300,20);
                btn_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15,15);
                img_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15, 15);
                lbl_others.frame = CGRectMake(20,CGRectGetMaxY(lbl_learned.frame)+1, 300,20);
                txt_otherslearncook.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+3, 300, 20);
                lineimg.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+20, self.view.frame.size.width-68, 0.5);
                lbl_recommand.frame = CGRectMake(25,CGRectGetMaxY(img_backgroundimage.frame)+8, 300,50);
                txtview1.frame=CGRectMake(25, CGRectGetMaxY(lbl_recommand.frame)+15, self.view.frame.size.width-40, 100);
                lblmax.frame = CGRectMake(self.view.frame.size.width-95,CGRectGetMaxY(txtview1.frame)+1, 100,10);
                img_bg3.frame=CGRectMake(0, CGRectGetMaxY(txtview1.frame)+180, WIDTH, 100);
                //        img_bg3.frame=CGRectMake(0, HEIGHT-80, WIDTH, 100);
                page4.frame =CGRectMake(0,0, WIDTH,30);
                Nextview.frame = CGRectMake(18, 35, WIDTH-36,40);
                
            }
        }
        
        else
        {
            img_backgroundimag.hidden=NO;
            str_cheftype = @"30";
            txt_others.hidden = NO;
            img_line.hidden = NO;
            if(IS_IPHONE_6Plus)
            {
                
                lbl_cookingexp.frame=CGRectMake(60,CGRectGetMaxY(txt_others.frame)+10, 300,30);
                
                btn_Yes.frame = CGRectMake(30, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                button_img.frame=CGRectMake(30, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lbl_Yes.frame = CGRectMake(50,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btn_yrs.frame = CGRectMake(80, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                button_imge.frame=CGRectMake(80, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                
                lbl_Yrs.frame = CGRectMake(100,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btnyrs.frame = CGRectMake(130, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                buttonimge.frame=CGRectMake(130, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                
                lblYrs.frame = CGRectMake(150,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btnyrs1.frame = CGRectMake(190, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                buttonimges.frame=CGRectMake(190, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lblYrs1.frame = CGRectMake(210,CGRectGetMaxY(lbl_cookingexp.frame)+5, 120,15);
                
                lbl_cookingqual.frame = CGRectMake(30,CGRectGetMaxY(lbl_Yes.frame)+20, 300,50);
                
                img_backgroundimage.frame=CGRectMake(30, CGRectGetMaxY(lbl_cookingqual.frame)+5, WIDTH, 30);
                
                btnYes.frame = CGRectMake(0, 10, 30,30);
                buttonimg.frame=CGRectMake(0, 10, 15, 15);
                lblYes.frame = CGRectMake(20,10, 30,15);
                btnNo.frame = CGRectMake(50, 10, 30,30);
                buttonimage.frame=CGRectMake(50, 10, 15, 15);
                
                lblNo.frame = CGRectMake(70,10, 30,15);
                
                img_backgroundimage2.frame=CGRectMake(30, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 60);
                img_backgroundimage2.hidden=YES;
                
                lbl_statequalification.frame = CGRectMake(0,5, 150,25);
                txt_statequatification.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+1, 200, 30);
                imgline.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+24, self.view.frame.size.width-57, 0.5);
                img_backgroundimage3.frame=CGRectMake(30, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 120);
                img_backgroundimage3.hidden=YES;
                
                lbl_learncook.frame = CGRectMake(0,5, 250,25);
                btn_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+7, 15,15);
                img_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+5, 15, 15);
                lbl_selftaught.frame = CGRectMake(20,CGRectGetMaxY(lbl_learncook.frame)+3, 300,20);
                btn_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
                img_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
                lbl_learned.frame= CGRectMake(20,CGRectGetMaxY(lbl_selftaught.frame)+3, 300,20);
                btn_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15,15);
                img_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15, 15);
                lbl_others.frame = CGRectMake(20,CGRectGetMaxY(lbl_learned.frame)+1, 300,20);
                txt_otherslearncook.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+3, 300, 20);
                lineimg.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+20, self.view.frame.size.width-77, 0.5);
                lbl_recommand.frame = CGRectMake(25,CGRectGetMaxY(img_backgroundimage.frame)+8, 300,50);
                txtview1.frame=CGRectMake(25, CGRectGetMaxY(lbl_recommand.frame)+15, self.view.frame.size.width-57, 100);
                lblmax.frame = CGRectMake(self.view.frame.size.width-105,CGRectGetMaxY(txtview1.frame)+1, 100,10);
                img_bg3.frame =  CGRectMake(0, HEIGHT-100, WIDTH, 100);
                page4.frame = CGRectMake(WIDTH/2-205,0, WIDTH,30);
                Nextview.frame = CGRectMake(18, 40, WIDTH-36,50);
            }
            else if(IS_IPHONE_6)
            {
                lbl_cookingexp.frame=CGRectMake(55,CGRectGetMaxY(txt_others.frame)+10, 300,30);
                btn_Yes.frame = CGRectMake(20, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                button_img.frame=CGRectMake(20, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lbl_Yes.frame = CGRectMake(50,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btn_yrs.frame = CGRectMake(80, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                button_imge.frame=CGRectMake(80, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lbl_Yrs.frame = CGRectMake(110,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btnyrs.frame = CGRectMake(150, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                buttonimge.frame=CGRectMake(150, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lblYrs.frame = CGRectMake(170,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btnyrs1.frame = CGRectMake(210, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                buttonimges.frame=CGRectMake(210, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lblYrs1.frame = CGRectMake(240,CGRectGetMaxY(lbl_cookingexp.frame)+5, 120,15);
                lbl_cookingqual.frame = CGRectMake(20,CGRectGetMaxY(lbl_Yes.frame)+20, 300,50);
                img_backgroundimage.frame=CGRectMake(20, CGRectGetMaxY(lbl_cookingqual.frame)+5, WIDTH, 30);
                btnYes.frame = CGRectMake(0, 10, 30,30);
                buttonimg.frame=CGRectMake(0, 10, 15, 15);
                lblYes.frame = CGRectMake(20,10, 30,15);
                btnNo.frame = CGRectMake(50, 10, 30,30);
                buttonimage.frame=CGRectMake(50, 10, 15, 15);
                lblNo.frame = CGRectMake(70,10, 30,15);
                img_backgroundimage2.frame=CGRectMake(20, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 60);
                img_backgroundimage2.hidden=YES;
                lbl_statequalification.frame = CGRectMake(0,5, 150,25);
                txt_statequatification.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+1, 200, 30);
                imgline.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+24, self.view.frame.size.width-55, 0.5);
                img_backgroundimage3.frame=CGRectMake(20, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 120);
                img_backgroundimage3.hidden=YES;
                lbl_learncook.frame = CGRectMake(0,5, 250,25);
                btn_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+7, 15,15);
                img_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+5, 15, 15);
                lbl_selftaught.frame = CGRectMake(20,CGRectGetMaxY(lbl_learncook.frame)+3, 300,20);
                btn_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
                img_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
                lbl_learned.frame= CGRectMake(20,CGRectGetMaxY(lbl_selftaught.frame)+3, 300,20);
                btn_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15,15);
                img_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15, 15);
                lbl_others.frame = CGRectMake(20,CGRectGetMaxY(lbl_learned.frame)+1, 300,20);
                txt_otherslearncook.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+3, 300, 20);
                lineimg.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+20, self.view.frame.size.width-75, 0.5);
                lbl_recommand.frame = CGRectMake(20,CGRectGetMaxY(img_backgroundimage.frame)+8, 300,50);
                txtview1.frame=CGRectMake(20, CGRectGetMaxY(lbl_recommand.frame)+15, self.view.frame.size.width-40, 100);
                lblmax.frame = CGRectMake(self.view.frame.size.width-95,CGRectGetMaxY(txtview1.frame)+1, 100,10);
                img_bg3.frame=CGRectMake(0, CGRectGetMaxY(txtview1.frame)+150, WIDTH, 100);
                //        img_bg3.frame=CGRectMake(0, HEIGHT-100, WIDTH, 100);
                page4.frame =CGRectMake(0,4, WIDTH,30);
                Nextview.frame = CGRectMake(18, 40, WIDTH-36,50);
                
            }
            else
            {
                txt_others.frame =CGRectMake(25,CGRectGetMaxY(txt_cheftype.frame)+10, WIDTH-40,30);
                img_line.frame =CGRectMake(25,CGRectGetMaxY(txt_others.frame)+1, WIDTH-40,0.3);
                lbl_cookingexp.frame=CGRectMake(55,CGRectGetMaxY(txt_others.frame)+10, 300,30);
                btn_Yes.frame = CGRectMake(20, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                button_img.frame=CGRectMake(20, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lbl_Yes.frame = CGRectMake(50,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btn_yrs.frame = CGRectMake(80, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                button_imge.frame=CGRectMake(80, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lbl_Yrs.frame = CGRectMake(110,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btnyrs.frame = CGRectMake(150, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                buttonimge.frame=CGRectMake(150, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lblYrs.frame = CGRectMake(170,CGRectGetMaxY(lbl_cookingexp.frame)+5, 30,15);
                btnyrs1.frame = CGRectMake(210, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15,15);
                buttonimges.frame=CGRectMake(210, CGRectGetMaxY(lbl_cookingexp.frame)+5, 15, 15);
                lblYrs1.frame = CGRectMake(240,CGRectGetMaxY(lbl_cookingexp.frame)+5, 120,15);
                lbl_cookingqual.frame = CGRectMake(20,CGRectGetMaxY(lbl_Yes.frame)+20, 300,50);
                img_backgroundimage.frame=CGRectMake(20, CGRectGetMaxY(lbl_cookingqual.frame)+5, WIDTH, 30);
                btnYes.frame = CGRectMake(0, 10, 30,30);
                buttonimg.frame=CGRectMake(0, 10, 15, 15);
                lblYes.frame = CGRectMake(20,10, 30,15);
                btnNo.frame = CGRectMake(50, 10, 30,30);
                buttonimage.frame=CGRectMake(50, 10, 15, 15);
                lblNo.frame = CGRectMake(70,10, 30,15);
                img_backgroundimage2.frame=CGRectMake(20, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 60);
                img_backgroundimage2.hidden=YES;
                lbl_statequalification.frame = CGRectMake(0,5, 150,25);
                txt_statequatification.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+1, 200, 30);
                imgline.frame=CGRectMake(0, CGRectGetMaxY(lbl_statequalification.frame)+24, self.view.frame.size.width-55, 0.5);
                img_backgroundimage3.frame=CGRectMake(20, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 120);
                img_backgroundimage3.hidden=YES;
                lbl_learncook.frame = CGRectMake(0,5, 250,25);
                btn_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+7, 15,15);
                img_selftaught.frame = CGRectMake(0, CGRectGetMaxY(lbl_learncook.frame)+5, 15, 15);
                lbl_selftaught.frame = CGRectMake(20,CGRectGetMaxY(lbl_learncook.frame)+3, 300,20);
                btn_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
                img_Lreaned.frame = CGRectMake(0, CGRectGetMaxY(btn_selftaught.frame)+5, 15,15);
                lbl_learned.frame= CGRectMake(20,CGRectGetMaxY(lbl_selftaught.frame)+3, 300,20);
                btn_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15,15);
                img_others.frame = CGRectMake(0, CGRectGetMaxY(btn_Lreaned.frame)+5, 15, 15);
                lbl_others.frame = CGRectMake(20,CGRectGetMaxY(lbl_learned.frame)+1, 300,20);
                txt_otherslearncook.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+3, 300, 20);
                lineimg.frame=CGRectMake(20, CGRectGetMaxY(lbl_others.frame)+20, self.view.frame.size.width-75, 0.5);
                lbl_recommand.frame = CGRectMake(20,CGRectGetMaxY(img_backgroundimage.frame)+8, 300,50);
                txtview1.frame=CGRectMake(20, CGRectGetMaxY(lbl_recommand.frame)+15, self.view.frame.size.width-40, 100);
                lblmax.frame = CGRectMake(self.view.frame.size.width-95,CGRectGetMaxY(txtview1.frame)+1, 100,10);
                img_bg3.frame=CGRectMake(0, CGRectGetMaxY(txtview1.frame)+150, WIDTH, 100);
                //        img_bg3.frame=CGRectMake(0, HEIGHT-100, WIDTH, 100);
                page4.frame =CGRectMake(0,4, WIDTH,30);
                Nextview.frame = CGRectMake(18, 40, WIDTH-36,50);
                
                
                
            }
            
        }
    }
    
    
    [cheftype_table setHidden:YES];
    
    
    
}
-(void)keyboard_returnmethod
{
    [txt_cheftype resignFirstResponder];
    [txt_others resignFirstResponder];
    [txt_otherslearncook resignFirstResponder];
    [txt_statequatification resignFirstResponder];
    [txtview1 resignFirstResponder];
    
}

#pragma mark - ButtonActions

-(void)Next_btnClick
{
    //    ChefSignup6 *chefsignup6=[[ChefSignup6 alloc]init];
    //    [self presentViewController:chefsignup6 animated:NO completion:nil];
    //
    //    [self.navigationController pushViewController:chefsignup6 animated:NO];
    
    NSLog(@"click_Loginbtn");
    
    NSString*str_goon;
    
    if ([txt_cheftype.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Please enter Cheftype is required"];
        
    }
    else if ([[NSString stringWithFormat:@"%@",str_status] isEqualToString:@""]){
        [self popup_Alertview:@"please select years of Cooking Experience"];
    }
    else if ([[NSString stringWithFormat:@"%@",str_cooking] isEqualToString:@""])
    {
        [self popup_Alertview:@"Please select Formal Qualification"];
        
    }
    else if (txt_statequatification.text.length<=0)
    {
        if ([[NSString stringWithFormat:@"%@",str_cooking] isEqualToString:@"YES"])
        {
            if ([ary_temp count]==0)
            {
                [self popup_Alertview:@"Please select  State Qualification"];
            }
            else
            {
                str_goon = @"YES";
                
            }
            
        }
        else
        {
            if ([[NSString stringWithFormat:@"%@",str_LearnCook] isEqualToString:@""])
            {
                
                [self popup_Alertview:@"Please select Learn to Cook"];
                
                
            }
            else{
                str_goon = @"NO";
            }
            
        }
        
        if ([[NSString stringWithFormat:@"%@",str_goon]isEqualToString:@"NO"])
        {
            [self SignupFive];
        }
    }
     else if (txtview1.text.length<=0)
     {
          [self popup_Alertview:@"Please enter description"];
     }
    else
    {
        [self SignupFive];
    }
    
    
    
}
-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFontBold size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFontBold size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFontBold size:16.0f]];
}
-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}


-(void)btn_FirstyearClick:(UIButton *)sender
{
    [btn_yrs setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [btnyrs setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [btnyrs1 setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    
    
    
    if([sender isSelected])
    {
        [btn_Yes setSelected:YES];
        [sender setSelected:NO];
        str_status=@"1-5";
        [btn_Yes setImage:[UIImage imageNamed:@"button img .png"] forState:UIControlStateNormal];
        
        
        
        
    }
    else
    {
        
        
        [btn_Yes setSelected:NO];
        [sender setSelected:YES];
        [btn_Yes setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
        
    }
}


-(void)btn_sixyearClick:(UIButton *)sender
{
    [btn_Yes setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [btnyrs setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [btnyrs1 setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    
    if([sender isSelected])
    {
        [btn_yrs setSelected:YES];
        [sender setSelected:NO];
        str_status=@"6-10";
        [btn_yrs setImage:[UIImage imageNamed:@"button img .png"] forState:UIControlStateNormal];
        
    }
    else
    {
        
        
        [btn_yrs setSelected:NO];
        
        [sender setSelected:YES];
        [btn_yrs setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
        
    }
    
    
}
-(void)btn_ElevyearClick:(UIButton *)sender{
    [btn_Yes setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [btn_yrs setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [btnyrs1 setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    
    
    if([sender isSelected])
    {
        [btnyrs setSelected:YES];
        [sender setSelected:NO];
        str_status=@"11-20";
        [btnyrs setImage:[UIImage imageNamed:@"button img .png"] forState:UIControlStateNormal];
        
    }
    else
    {
        
        
        [btnyrs setSelected:NO];
        
        [sender setSelected:YES];
        [btnyrs setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
        
    }
    
    
}
-(void)btn_moreyearClick:(UIButton *)sender{
    [btn_Yes setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [btn_yrs setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [btnyrs setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    
    
    if([sender isSelected])
    {
        [btnyrs1 setSelected:YES];
        [sender setSelected:NO];
        
        str_status=@"more than 20";
        [btnyrs1 setImage:[UIImage imageNamed:@"button img .png"] forState:UIControlStateNormal];
        
    }
    else
    {
        
        
        [btnyrs1 setSelected:NO];
        
        [sender setSelected:YES];
        [btnyrs1 setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
        
    }
    
    
}

-(void) btn_YesClick:(UIButton *) sender
{
    buttonimage.image=[UIImage imageNamed:@"button img 2.png"];
    
    
    
    if([sender isSelected])
    {
        [sender setSelected:NO];
        [btn_Yes setSelected:YES];
        
        str_cooking=@"YES";
        buttonimg.image=[UIImage imageNamed:@"button img .png"];
        img_backgroundimage2.hidden=NO;
        
        if(IS_IPHONE_6Plus)
        {
            img_backgroundimage3.hidden=YES;
            img_backgroundimage2.frame=CGRectMake(30, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 60);
            lbl_recommand.frame = CGRectMake(30,CGRectGetMaxY(img_backgroundimage2.frame)+8, 300,50);
            txtview1.frame=CGRectMake(30, CGRectGetMaxY(lbl_recommand.frame)+15, self.view.frame.size.width-57, 100);
            lblmax.frame = CGRectMake(self.view.frame.size.width-105,CGRectGetMaxY(txtview1.frame)+1, 100,10);
        }
        else if(IS_IPHONE_6)
        {
            img_backgroundimage3.hidden=YES;
            img_backgroundimage2.frame=CGRectMake(20, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 60);
            lbl_recommand.frame = CGRectMake(20,CGRectGetMaxY(img_backgroundimage2.frame)+8, 300,50);
            txtview1.frame=CGRectMake(20, CGRectGetMaxY(lbl_recommand.frame)+15, self.view.frame.size.width-48, 100);
            lblmax.frame = CGRectMake(self.view.frame.size.width-95,CGRectGetMaxY(txtview1.frame)+1, 100,10);
        }
        else
        {
            img_backgroundimage3.hidden=YES;
            img_backgroundimage2.frame=CGRectMake(25, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 60);
            lbl_recommand.frame = CGRectMake(25,CGRectGetMaxY(img_backgroundimage2.frame)+8, 300,50);
            txtview1.frame=CGRectMake(25, CGRectGetMaxY(lbl_recommand.frame)+15, self.view.frame.size.width-40, 100);
            lblmax.frame = CGRectMake(self.view.frame.size.width-95,CGRectGetMaxY(txtview1.frame)+1, 100,10);
        }
        
        
        
        
    }
    else
    {
        
        [sender setSelected:YES];
        [btn_Yes setSelected:NO];
        buttonimg.image=[UIImage imageNamed:@"button img 2.png"];
        img_backgroundimage2.hidden=YES;
    }
    
}
-(void) btn_NoClick:(UIButton *) sender

{
    buttonimg.image=[UIImage imageNamed:@"button img 2.png"];
    
    
    if([sender isSelected])
    {
        [sender setSelected:NO];
        str_cooking=@"NO";
        
        buttonimage.image=[UIImage imageNamed:@"button img .png"];
        img_backgroundimage3.hidden=NO;
        
        if(IS_IPHONE_6Plus)
        {
            img_backgroundimage2.hidden=YES;
            img_backgroundimage3.frame=CGRectMake(30, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 120);
            lbl_recommand.frame = CGRectMake(30,CGRectGetMaxY(img_backgroundimage3.frame)+8, 300,50);
            txtview1.frame=CGRectMake(30, CGRectGetMaxY(lbl_recommand.frame)+15, self.view.frame.size.width-57, 100);
            lblmax.frame = CGRectMake(self.view.frame.size.width-105,CGRectGetMaxY(txtview1.frame)+1, 100,10);
        }
        else if(IS_IPHONE_6)
        {
            img_backgroundimage2.hidden=YES;
            img_backgroundimage3.frame=CGRectMake(20, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 120);
            lbl_recommand.frame = CGRectMake(20,CGRectGetMaxY(img_backgroundimage3.frame)+8, 300,50);
            txtview1.frame=CGRectMake(20, CGRectGetMaxY(lbl_recommand.frame)+15, self.view.frame.size.width-40, 100);
            lblmax.frame = CGRectMake(self.view.frame.size.width-95,CGRectGetMaxY(txtview1.frame)+1, 100,10);
        }
        else
        {
            img_backgroundimage2.hidden=YES;
            img_backgroundimage3.frame=CGRectMake(25, CGRectGetMaxY(img_backgroundimage.frame)+5, WIDTH, 120);
            lbl_recommand.frame = CGRectMake(25,CGRectGetMaxY(img_backgroundimage3.frame)+8, 300,50);
            txtview1.frame=CGRectMake(25, CGRectGetMaxY(lbl_recommand.frame)+15, self.view.frame.size.width-40, 100);
            lblmax.frame = CGRectMake(self.view.frame.size.width-95,CGRectGetMaxY(txtview1.frame)+1, 100,10);
        }
        
        
        
    }
    else
    {
        [sender setSelected:YES];
        buttonimage.image=[UIImage imageNamed:@"button img 2.png"];
        img_backgroundimage3.hidden=YES;
        
    }
    
}

-(void)click_selectselfTaughtAt:(UIButton *)sender
{
    
    
    [btn_Lreaned setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [btn_others setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    
    
    if([sender isSelected])
    {
        [sender setSelected:NO];
        
        str_LearnCook=@"1";
        [btn_selftaught setImage:[UIImage imageNamed:@"button img .png"] forState:UIControlStateNormal];
        
        
        
        
        
    }
    else
    {
        [sender setSelected:YES];
        [btn_selftaught setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
        
    }
    
    //    ABC=sender.tag;
    //
    //
    //
    //    if([ary_temp containsObject:[NSString stringWithFormat:@"%d",ABC]])
    //    {
    //        [ary_temp removeObject:[NSString stringWithFormat:@"%d",ABC]];
    //
    //        if (sender.tag==1)
    //        {
    //            [btn_selftaught setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    //
    //        }
    //        else if (sender.tag==2)
    //        {
    //            [btn_Lreaned setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    //
    //        }
    //        else if (sender.tag==3)
    //        {
    //            [btn_others setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    //        }
    //    }
    //    else
    //    {
    //        [ary_temp addObject:[NSString stringWithFormat:@"%d",ABC]];
    //        if (sender.tag==1)
    //        {
    //
    //            [btn_selftaught setImage:[UIImage imageNamed:@"button img .png"] forState:UIControlStateNormal];
    //        }
    //        else if (sender.tag==2)
    //        {
    //            [btn_Lreaned setImage:[UIImage imageNamed:@"button img .png"] forState:UIControlStateNormal];
    //
    //        }
    //        else if (sender.tag==3)
    //        {
    //            [btn_others setImage:[UIImage imageNamed:@"button img .png"] forState:UIControlStateNormal];
    //
    //        }
    //    }
    //
    //        str_LearnCook = [[NSMutableString alloc] init];
    //        for (int i=0; i<[ary_temp count];i++)
    //        {
    ////            if (i==[ary_temp count]-1)
    ////            {
    ////                [str_LearnCook  appendString:[NSString stringWithFormat:@"%@",[ary_temp objectAtIndex:i]]];
    ////            }
    ////            else
    ////            {
    ////                [str_LearnCook  appendString:[NSString stringWithFormat:@"%@||",[ary_temp objectAtIndex:i]]];
    ////            }
    //        }
    //
    //        NSLog(@"str_LearnCook :%@",str_LearnCook);
    //        NSLog(@"ary_temp :%@",ary_temp);
    //
    
}
-(void)click_selectLearnedAt:(UIButton *)sender
{
    
    [btn_selftaught setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [btn_others setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    
    
    if([sender isSelected])
    {
        [sender setSelected:NO];
        
        str_LearnCook=@"2";
        [btn_Lreaned setImage:[UIImage imageNamed:@"button img .png"] forState:UIControlStateNormal];
        
        
    }
    else
    {
        [sender setSelected:YES];
        [btn_Lreaned setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
        
    }
    
}



-(void)click_selectothersAt:(UIButton *)sender
{
    [btn_selftaught setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [btn_Lreaned setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    
    
    if([sender isSelected])
    {
        [sender setSelected:NO];
        
        str_LearnCook=@"3";
        [btn_others setImage:[UIImage imageNamed:@"button img .png"] forState:UIControlStateNormal];
        txt_otherslearncook.hidden=NO;
        lineimg.hidden=NO;
    }
    else
    {
        [sender setSelected:YES];
        [btn_others setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
        txt_otherslearncook.hidden=YES;
        lineimg.hidden=YES;
        
    }
    
}

-(void)drop_btnClick:(UIButton *)sender
{
    [self keyboard_returnmethod];
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        cheftype_table.hidden=YES;
        
    }
    else
    {
        [sender setSelected:NO];
        cheftype_table.hidden=NO;
    }
    
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}



# pragma mark ChefSignupFive method


-(void)SignupFive
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
        
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    //[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"]
    
    NSDictionary *params =@{
                            @"field_chef_type"                   :  str_cheftype,
                            @"uid"                               : [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"field_other_chef_type"             :  txt_others.text,
                            @"field_years_of_cooking"            :  str_status,
                            @"field_formal_cooking_qualificati"  :  str_cooking,
                            @"field_if_yes_state_qualification"  :  txt_statequatification.text,
                            @"field_how_did_you_learn_to_cook"   :  str_LearnCook,
                            @"registration_page"                 : @"5",
                            @"device_udid"                       :  UniqueAppID,
                            @"device_token"                      :  @"Dev",
                            @"device_type"                       :  @"1"
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSignUpFirst
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpFive:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self SignupFive];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpFive :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        //        for (int i=0; i<[[TheDict valueForKey:@"Country_CityList"] count]; i++)
        //        {
        //            [ary_countrylist addObject:[[TheDict valueForKey:@"Country_CityList"] objectAtIndex:i]];
        //
        //        }
        
        ChefSignup6 *chefsignup6=[[ChefSignup6 alloc]init];
        [self presentViewController:chefsignup6 animated:NO completion:nil];
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        txt_cheftype.text=@"";
        txt_others.text=@"";
        txt_statequatification.text=@"";
        str_cooking=@"";
        str_LearnCook=@"";
        
        
        
    }
    
}



//# pragma mark ChefProfileType method
//
//
//-(void)ChefProfileType
//{
//    
//    [self.view addSubview:delegate.activityIndicator];
//    [delegate.activityIndicator startAnimating];
//    
//    //=================================================================BASE URL
//    
//    NSURL *url = [NSURL URLWithString:kBaseUrl];
//    
//    //=================================================================USED PARAMETERS(ONLY TEXT)
//    
//    if (TARGET_IPHONE_SIMULATOR)
//    {
//        delegate.devicestr = @"";
//    }
//    
//    
//    UIDevice *myDevice=[UIDevice currentDevice];
//    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
//    
//    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
//    
//    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
//        str_device_token = @"dev-signup";
//        
//    }
//    
//    NSDictionary *params =@{
//                            @"field_chef_type"                   :  str_cheftype,
//                            @"uid"                               : [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
//                            @"field_other_chef_type"             :  txt_others.text,
//                            @"registration_page"                 : @"5",
//                            @"device_udid"                       :  UniqueAppID,
//                            @"device_token"                      :  @"Dev",
//                            @"device_type"                       :  @"1"
//                            
//                            };
//    
//    
//    //===========================================AFNETWORKING HEADER
//    
//    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
//    httpClient.parameterEncoding = AFFormURLParameterEncoding;
//    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
//    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
//    
//    //===============================SIMPLE REQUEST
//    
//    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
//                                                            path:kChefProfileType
//                                                      parameters:params];
//    
//    
//    
//    
//    //====================================================RESPONSE
//    
//    
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
//    
//    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
//        
//    }];
//    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSError *error = nil;
//        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
//        
//        [delegate.activityIndicator stopAnimating];
//        [self ResponseChefProfileType:JSON];
//    }
//     
//     //==================================================ERROR
//                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                         
//                                         
//                                         [delegate.activityIndicator stopAnimating];
//                                         
//                                         if([operation.response statusCode] == 406){
//                                             
//                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
//                                             return;
//                                         }
//                                         
//                                         if([operation.response statusCode] == 403){
//                                             NSLog(@"Upload Failed");
//                                             return;
//                                         }
//                                         if ([[operation error] code] == -1009) {
//                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
//                                                                                          message:@"Please check your internet connection"
//                                                                                         delegate:nil
//                                                                                cancelButtonTitle:@"OK"
//                                                                                otherButtonTitles:nil];
//                                             [av show];
//                                         }
//                                         else if ([[operation error] code] == -1001) {
//                                             
//                                             NSLog(@"Successfully Registered");
//                                             [self ChefProfileType];
//                                         }
//                                     }];
//    [operation start];
//    
//}
//-(void) ResponseChefProfileType :(NSDictionary * )TheDict
//{
//    NSLog(@"Login: %@",TheDict);
//    
//    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
//    {
//        //        for (int i=0; i<[[TheDict valueForKey:@"Country_CityList"] count]; i++)
//        //        {
//        //            [ary_countrylist addObject:[[TheDict valueForKey:@"Country_CityList"] objectAtIndex:i]];
//        //
//        //        }
//        
//        
//        
//        
//    }
//    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
//    {
//        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
//        txt_cheftype.text=@"";
//        txt_others.text=@"";
//        
//        
//        
//    }
//    
//}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
