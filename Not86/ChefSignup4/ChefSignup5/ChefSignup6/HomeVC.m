

//  HomeVC.m
//  Not86
//
//  Created by Admin on 29/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "HomeVC.h"
#import "SearchForMoreFiltersVC.h"
#import "SerchForFoodNowVC.h"
#import "DiningCartVC.h"
#import "UserProfileVC.h"
#import "UIImageView+AFNetworking.h"
#import "ChefHomeViewController.h"
#import "MenuScreenVC.h"
#import "MyScheduleScreenVC.h"
#import "MyProfileVC.h"
#import "MyHomescreenVC.h"
#import "MyScheduleVC.h"

#import "MyMenuVC.h"
#import "MyAccountVC.h"
#import "MessagesVC.h"
#import "SwitchtoUser.h"
#import "JWSlideMenuViewController.h"
#import "ChefSupportsVC.h"
#import "JWSlideMenuController.h"
#import "UserMessagesVC.h"
#import "NotificationsVC.h"
#import "MyordersVC.h"
#import "UserDishRivewsVC.h"
#import "ChefProfileVC.h"
#import "AddToCartFoodNowVC.h"
#import "ItemDetailVC.h"
#import "UserSearchVC.h"
#import "AppDelegate.h"
#import "Define.h"
#import "UserSupportVC.h"
#import "UserMessagesVC.h"

#import "MessageListVC.h"

#import "NotificationSettingsVC.h"
#import "ItemDetailVC.h"
#import "AddToCartFoodNowVC.h"


#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"



@interface HomeVC ()<UITextFieldDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDelegate,MKMapViewDelegate,CLLocationManagerDelegate>
{
    UIImageView *img_header;
    UIImageView *img_background;
    UITableView *img_table;
    NSMutableArray*ary_displaynames;
   // NSMutableArray * array_img;
    AppDelegate *delegate;
    NSMutableArray*ary_dishList;
    UICollectionViewFlowLayout *layout;
    UICollectionView *collView_serviceDirectory;
    int selectedindex;
    NSIndexPath *indexSelected;
    NSMutableArray *ary_DishRestrictions;
    NSMutableArray *ary_Serving_Type;
    
    
    //location
    CLLocation *myLocation;
    CLGeocoder *geocoder;
    UILabel *lbl_CurrentLoc;
    
    CLLocationManager *locationManager;
    CLGeocoder *myGeoCoder;
    BOOL bool_LocationSelect;
    NSString *str_Latitude;
    NSString *str_Longitude;
    CLPlacemark *place;
    NSString *str_street;


    
    
}

@end




@implementation HomeVC


- (void)viewDidLoad {
    [super viewDidLoad];
    ary_dishList=[NSMutableArray new];
    ary_DishRestrictions=[NSMutableArray new];
    ary_Serving_Type=[NSMutableArray new];
    
    
      // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBodyDesign];
    
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    //[self.navigationController.slideMenuController  HideHomeButton];

    [self.navigationController.slideMenuController UnhideHomeButton];
    
    
    
    
    [delegate fetchUsersCurrentLocation];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER)
    {
        [locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    [locationManager startUpdatingLocation];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    [locationManager startUpdatingLocation];
    
    myLocation = [[CLLocation alloc]init];
    myLocation = [locationManager location];
    
    CLLocationCoordinate2D coordinate;
    coordinate.longitude = locationManager.location.coordinate.longitude;
    coordinate.latitude = locationManager.location.coordinate.latitude;
    
    str_Longitude =[NSString stringWithFormat:@"%f",coordinate.longitude];
    str_Latitude =[NSString stringWithFormat:@"%f",coordinate.latitude];
    
    NSLog(@"str_lat:%@ str_lon:%@",str_Latitude,str_Longitude);
    

    
//    [self AFUserFavoritesDish];
      [self AFUserDishLists];
    
}

-(void)integrateHeader
{
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu .frame = CGRectMake(10, 13,20,20);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu  addTarget:self action:@selector(btn_HomeScreenClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_menu setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
   // [img_header   addSubview:icon_menu ];
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 150, 45);
    lbl_User_Sign_Up.text = @"Home";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    UIButton *icon_serch = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_serch .frame = CGRectMake(WIDTH-65, 13, 20, 20);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_serch  addTarget:self action:@selector(btn_action_on_serch:) forControlEvents:UIControlEventTouchUpInside];
    [icon_serch setImage:[UIImage imageNamed:@"search-icon@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_serch ];
    
    
//    UIImageView *icon_user = [[UIImageView alloc]init];
//    icon_user.frame = CGRectMake(WIDTH-35, 9, 27, 27);
//    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
//    //   icon_user.backgroundColor = [UIColor redColor];
//    [icon_user setUserInteractionEnabled:YES];
//    [img_header addSubview:icon_user];
    
    UIButton *icon_logo = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_logo .frame = CGRectMake(WIDTH-35, 9, 27, 27);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_logo  addTarget:self action:@selector(click_on_logo_btn:) forControlEvents:UIControlEventTouchUpInside];
    [icon_logo setImage:[UIImage imageNamed:@"logo-img@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_logo ];
    

    
    
}


-(void)integrateBodyDesign
{
    
    
    img_background = [[UIImageView alloc]init];
    //  img_background .frame = CGRectMake(0, CGRectGetMaxY(img_header.frame), WIDTH, 210);
    [img_background setImage:[UIImage imageNamed:@"home-bg@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_background setUserInteractionEnabled:YES];
    [self.view addSubview:img_background];
    
    
    UIButton *icon_Food_Now = [UIButton buttonWithType:UIButtonTypeCustom];
    // icon_Food_Now .frame = CGRectMake(20, CGRectGetMaxY(img_header.frame)-20, 125, 60);
    //icon_Food_Now .backgroundColor = [UIColor clearColor];
    [icon_Food_Now  addTarget:self action:@selector(icon_Food_Now_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_Food_Now setImage:[UIImage imageNamed:@"img-food-now@2x.png"] forState:UIControlStateNormal];
    [img_background   addSubview:icon_Food_Now ];
    
    UIButton *icon_Food_Later = [UIButton buttonWithType:UIButtonTypeCustom];
    //  icon_Food_Later.frame = CGRectMake(175, CGRectGetMaxY(img_header.frame)-20, 125, 60);
    //icon_Food_Later .backgroundColor = [UIColor clearColor];
    [icon_Food_Later  addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_Food_Later setImage:[UIImage imageNamed:@"food-later-img@2x.png"] forState:UIControlStateNormal];
    [img_background   addSubview:icon_Food_Later];
    
    
    UILabel *dish_number = [[UILabel alloc]init];
    //   dish_number.frame = CGRectMake(27,CGRectGetMaxY(img_header.frame)+70,100, 15);
    dish_number.text = @"0";
    dish_number.font = [UIFont fontWithName:kFont size:20];
    dish_number.textColor = [UIColor whiteColor];
    dish_number.backgroundColor = [UIColor clearColor];
    dish_number.textAlignment = NSTextAlignmentCenter;
    [img_background  addSubview:dish_number];
    
    
    UILabel *favorite_dishes = [[UILabel alloc]init];
    //   favorite_dishes.frame = CGRectMake(10,CGRectGetMaxY(dish_number.frame)+10,100, 15);
    favorite_dishes.text = @"Favorite Items";
    favorite_dishes.font = [UIFont fontWithName:kFontBold size:15];
    favorite_dishes.textColor = [UIColor whiteColor];
    favorite_dishes.backgroundColor = [UIColor clearColor];
    [img_background  addSubview:favorite_dishes];
    
    UIButton *btn_favorite_dishes = [UIButton buttonWithType:UIButtonTypeCustom];
    //  btn_favorite_dishes .frame = CGRectMake(10, CGRectGetMaxY(img_header.frame)+63, 100, 60);
    btn_favorite_dishes  .backgroundColor = [UIColor clearColor];
    [btn_favorite_dishes   addTarget:self action:@selector(btn_favorite_dish_click:) forControlEvents:UIControlEventTouchUpInside];
    //    [btn_favorite_dishes  setImage:[UIImage imageNamed:@"img-food-later@2x.png"] forState:UIControlStateNormal];
    [img_background   addSubview:btn_favorite_dishes ];
    
    
    UIImageView *img_line1 = [[UIImageView alloc]init];
    //    img_line1 .frame =  CGRectMake(110,CGRectGetMaxY(img_header.frame)+60, 1, 30);
    [img_line1  setImage:[UIImage imageNamed:@"small-line@2x.png"]];
    [img_line1  setUserInteractionEnabled:YES];
    [img_background addSubview:img_line1 ];
    
    UILabel *cuisines_number = [[UILabel alloc]init];
    //  cuisines_number.frame = CGRectMake(140,CGRectGetMaxY(img_header.frame)+70,85, 15);
    cuisines_number.text = @"0";
    cuisines_number.font = [UIFont fontWithName:kFont size:20];
    cuisines_number.textColor = [UIColor whiteColor];
    cuisines_number.textAlignment = NSTextAlignmentCenter;

    cuisines_number.backgroundColor = [UIColor clearColor];
    [img_background addSubview:cuisines_number];
    
    
    UILabel *favorite_cuisines = [[UILabel alloc]init];
    //  favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,110, 15);
    favorite_cuisines.text = @"Favorite Cuisines";
    favorite_cuisines.font = [UIFont fontWithName:kFontBold size:13.5];
    favorite_cuisines.textColor = [UIColor whiteColor];
    favorite_cuisines.backgroundColor = [UIColor clearColor];
    [img_background  addSubview:favorite_cuisines];
    
    UIButton *btn_favorite_cuisines = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
    btn_favorite_cuisines  .backgroundColor = [UIColor clearColor];
    [btn_favorite_cuisines   addTarget:self action:@selector(btn_favorite_cuisines_click:) forControlEvents:UIControlEventTouchUpInside];
    //    [btn_favorite_dishes  setImage:[UIImage imageNamed:@"img-food-later@2x.png"] forState:UIControlStateNormal];
    [img_background   addSubview:btn_favorite_cuisines ];
    
    
    UIImageView *img_line2 = [[UIImageView alloc]init];
    //   img_line2.frame =  CGRectMake(230,CGRectGetMaxY(img_header.frame)+60, 1, 30);
    [img_line2 setImage:[UIImage imageNamed:@"small-line@2x.png"]];
    [img_line2 setUserInteractionEnabled:YES];
    [img_background addSubview:img_line2];
    
   
    
    
    UILabel *all_dishes = [[UILabel alloc]init];
    //   all_dishes.frame = CGRectMake(245,CGRectGetMaxY(all_dish_numbers.frame)+10,200, 15);
    all_dishes.text = @"All Items";
    all_dishes.font = [UIFont fontWithName:kFontBold size:15];
    all_dishes.textColor = [UIColor whiteColor];
    all_dishes.backgroundColor = [UIColor clearColor];
    [img_background  addSubview:all_dishes];
    
    
    UILabel *all_dish_numbers = [[UILabel alloc]init];
    all_dish_numbers.frame = CGRectMake(255,CGRectGetMaxY(img_header.frame)+70,200, 15);
    all_dish_numbers.text = @"0";
    all_dish_numbers.font = [UIFont fontWithName:kFont size:20];
    all_dish_numbers.textColor = [UIColor whiteColor];
    all_dish_numbers.backgroundColor = [UIColor clearColor];
    all_dish_numbers.textAlignment = NSTextAlignmentCenter;
    [img_background addSubview:all_dish_numbers];
    
    UIButton *btn_all_dishes = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_all_dishes .frame = CGRectMake(232, CGRectGetMaxY(img_header.frame)+60, 100, 63);
    btn_all_dishes  .backgroundColor = [UIColor clearColor];
    [btn_all_dishes   addTarget:self action:@selector(btn_all_dishes_click:) forControlEvents:UIControlEventTouchUpInside];
    //    [btn_favorite_dishes  setImage:[UIImage imageNamed:@"img-food-later@2x.png"] forState:UIControlStateNormal];
    [img_background   addSubview:btn_all_dishes ];

    
    UIImageView *img_line =[[UIImageView alloc]init];
    //   img_line.frame = CGRectMake(10, CGRectGetMaxY(favorite_dishes .frame)+13, WIDTH-25, 1);
    [img_line setUserInteractionEnabled:YES];
    img_line.backgroundColor=[UIColor clearColor];
    img_line.image=[UIImage imageNamed:@"img-white-line@2x.png"];
    [img_background addSubview:img_line];
    
    
    UILabel *serv_now = [[UILabel alloc]init];
    //    serv_now .frame = CGRectMake(95,CGRectGetMaxY(img_line.frame)+5,200, 28);
    serv_now .text = @"SERVING NOW";
    serv_now .font = [UIFont fontWithName:kFont size:20];
    serv_now .textColor = [UIColor whiteColor];
    serv_now .backgroundColor = [UIColor clearColor];
    [img_background  addSubview:serv_now ];
    
    UIButton *btn_sevrving_now = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_sevrving_now .frame = CGRectMake(0, CGRectGetMaxY(img_line.frame)+2,320, 37);
    btn_sevrving_now  .backgroundColor = [UIColor clearColor];
    [btn_sevrving_now   addTarget:self action:@selector(btn_serving_now_click:) forControlEvents:UIControlEventTouchUpInside];
    //    [btn_favorite_dishes  setImage:[UIImage imageNamed:@"img-food-later@2x.png"] forState:UIControlStateNormal];
    [img_background   addSubview:btn_sevrving_now ];
    
    
    UITextField *text_favorite_dishes = [[UITextField alloc]init];
    //   text_favorite_dishes.frame = CGRectMake(13, CGRectGetMaxY(img_background.frame)-40, WIDTH-80, 30);
    text_favorite_dishes.text = @"Favorite Items Serving Now";
    text_favorite_dishes.font = [UIFont fontWithName:kFontBold size:16];
    text_favorite_dishes.textColor = [UIColor blackColor];
    text_favorite_dishes.backgroundColor = [UIColor clearColor];
    [img_background addSubview:text_favorite_dishes];
    
    
    
    
    
#pragma mark Tableview
    
    img_table= [[UITableView alloc] init ];
    //   img_table.frame  = CGRectMake(13,290,296,370);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    img_table.backgroundColor = [UIColor clearColor];
    [self.view addSubview:img_table];
    
    
    
    if (IS_IPHONE_6Plus)
    {
        img_background .frame = CGRectMake(0, CGRectGetMaxY(img_header.frame), WIDTH, 220);
        icon_Food_Now .frame = CGRectMake(-25, CGRectGetMaxY(img_header.frame)-50, 264, 115);
        icon_Food_Later.frame = CGRectMake(175, CGRectGetMaxY(img_header.frame)-50, 264, 115);
        dish_number.frame = CGRectMake(10,CGRectGetMaxY(img_header.frame)+72,100, 20);
        favorite_dishes.frame = CGRectMake(13,CGRectGetMaxY(dish_number.frame)+10,150, 15);
        //favorite_dishes.frame = CGRectMake(10,CGRectGetMaxY(dish_number.frame)+10,100, 15);
        // favorite_dishes.frame = CGRectMake(10,CGRectGetMaxY(dish_number.frame)+10,100, 15);
        btn_favorite_dishes .frame = CGRectMake(0, CGRectGetMaxY(img_header.frame)+63, 100, 60);
        img_line1 .frame =  CGRectMake(130,CGRectGetMaxY(img_header.frame)+60, 1, 40);
        cuisines_number.frame = CGRectMake(150,CGRectGetMaxY(img_header.frame)+70,85, 20);
        //  cuisines_number.frame = CGRectMake(140,CGRectGetMaxY(img_header.frame)+70,85, 15);
        favorite_cuisines.frame = CGRectMake(140,CGRectGetMaxY(cuisines_number.frame)+12,180, 15);
        // favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,110, 15);
        // favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,110, 15);
        // favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,110, 15);
        //favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,110, 15);
        btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
        // btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
        //  btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
        img_line2.frame =  CGRectMake(260,CGRectGetMaxY(img_header.frame)+60, 1, 40);
        all_dish_numbers.frame = CGRectMake(290,CGRectGetMaxY(img_header.frame)+70,100, 20);
        all_dishes.frame = CGRectMake(307,CGRectGetMaxY(all_dish_numbers.frame)+12,200, 15);
        //all_dishes.frame = CGRectMake(245,CGRectGetMaxY(all_dish_numbers.frame)+10,200, 15);
        btn_all_dishes .frame = CGRectMake(232, CGRectGetMaxY(img_header.frame)+60, 150, 63);
        img_line.frame = CGRectMake(10, CGRectGetMaxY(favorite_dishes .frame)+13, WIDTH-25, 1);
        serv_now .frame = CGRectMake(120,CGRectGetMaxY(img_line.frame)+8,200, 28);
        btn_sevrving_now .frame = CGRectMake(0, CGRectGetMaxY(img_line.frame)+2,320, 37);
        text_favorite_dishes.frame = CGRectMake(15, CGRectGetMaxY(img_background.frame)-45, WIDTH-80, 34);
        //  text_favorite_dishes.frame = CGRectMake(13, CGRectGetMaxY(img_background.frame)-40, WIDTH-80, 30);
        img_table.frame  = CGRectMake(0,300,WIDTH,480);
        
        
    }
    else if (IS_IPHONE_6)
    {
        img_background .frame = CGRectMake(0, CGRectGetMaxY(img_header.frame), WIDTH, 220);
        icon_Food_Now .frame = CGRectMake(-34, CGRectGetMaxY(img_header.frame)-50, 264, 115);
        icon_Food_Later.frame = CGRectMake(147, CGRectGetMaxY(img_header.frame)-50, 264, 115);
        dish_number.frame = CGRectMake(20,CGRectGetMaxY(img_header.frame)+67,100, 20);
        favorite_dishes.frame = CGRectMake(30,CGRectGetMaxY(dish_number.frame)+10,150, 15);
        //favorite_dishes.frame = CGRectMake(10,CGRectGetMaxY(dish_number.frame)+10,100, 15);
        // favorite_dishes.frame = CGRectMake(10,CGRectGetMaxY(dish_number.frame)+10,100, 15);
        btn_favorite_dishes .frame = CGRectMake(0, CGRectGetMaxY(img_header.frame)+63, 100, 60);
        img_line1 .frame =  CGRectMake(130,CGRectGetMaxY(img_header.frame)+60, 1, 40);
        cuisines_number.frame = CGRectMake(150,CGRectGetMaxY(img_header.frame)+67,85,20);
        //  cuisines_number.frame = CGRectMake(140,CGRectGetMaxY(img_header.frame)+70,85, 15);
        favorite_cuisines.frame = CGRectMake(140,CGRectGetMaxY(cuisines_number.frame)+12,180, 15);
        // favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,110, 15);
        // favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,110, 15);
        // favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,110, 15);
        //favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,110, 15);
        btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
        // btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
        //  btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
        img_line2.frame =  CGRectMake(260,CGRectGetMaxY(img_header.frame)+60, 1, 40);
        all_dish_numbers.frame = CGRectMake(270,CGRectGetMaxY(img_header.frame)+66,110, 20);
        all_dishes.frame = CGRectMake(286,CGRectGetMaxY(all_dish_numbers.frame)+12,200, 15);
        //all_dishes.frame = CGRectMake(245,CGRectGetMaxY(all_dish_numbers.frame)+10,200, 15);
        btn_all_dishes .frame = CGRectMake(232, CGRectGetMaxY(img_header.frame)+60, 150, 63);
        img_line.frame = CGRectMake(10, CGRectGetMaxY(favorite_dishes .frame)+13, WIDTH-25, 1);
        serv_now .frame = CGRectMake(120,CGRectGetMaxY(img_line.frame)+8,200, 28);
        btn_sevrving_now .frame = CGRectMake(0, CGRectGetMaxY(img_line.frame)+2,320, 37);
        text_favorite_dishes.frame = CGRectMake(15, CGRectGetMaxY(img_background.frame)-45, WIDTH-80, 34);
        // text_favorite_dishes.frame = CGRectMake(13, CGRectGetMaxY(img_background.frame)-40, WIDTH-80, 30);
        img_table.frame  = CGRectMake(5,300,WIDTH-10,380);
        
    }
    else
    {
        img_background .frame = CGRectMake(0, CGRectGetMaxY(img_header.frame), WIDTH, 210);
        icon_Food_Now .frame = CGRectMake(20, CGRectGetMaxY(img_header.frame)-20, 125, 60);
        icon_Food_Later.frame = CGRectMake(175, CGRectGetMaxY(img_header.frame)-20, 125, 60);
        
        dish_number.frame = CGRectMake(10,CGRectGetMaxY(img_header.frame)+70,100, 15);
        favorite_dishes.frame = CGRectMake(10,CGRectGetMaxY(dish_number.frame)+10,150, 15);
        //        favorite_dishes.frame = CGRectMake(10,CGRectGetMaxY(dish_number.frame)+10,150, 15);
        //        favorite_dishes.frame = CGRectMake(10,CGRectGetMaxY(dish_number.frame)+10,150, 15);
        btn_favorite_dishes .frame = CGRectMake(10, CGRectGetMaxY(img_header.frame)+63, 100, 60);
        img_line1 .frame =  CGRectMake(114,CGRectGetMaxY(img_header.frame)+60, 1, 34);
        cuisines_number.frame = CGRectMake(130,CGRectGetMaxY(img_header.frame)+70,85, 15);
        cuisines_number.frame = CGRectMake(130,CGRectGetMaxY(img_header.frame)+70,95, 15);
        favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,180, 15);
        btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
        btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
        btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
        img_line2.frame =  CGRectMake(234,CGRectGetMaxY(img_header.frame)+60, 1, 34);
        all_dish_numbers.frame = CGRectMake(235,CGRectGetMaxY(img_header.frame)+70,90, 15);
        all_dishes.frame = CGRectMake(245,CGRectGetMaxY(all_dish_numbers.frame)+10,200, 15);
        all_dishes.frame = CGRectMake(245,CGRectGetMaxY(all_dish_numbers.frame)+10,200, 15);
        btn_all_dishes .frame = CGRectMake(232, CGRectGetMaxY(img_header.frame)+60, 100, 63);
        img_line.frame = CGRectMake(10, CGRectGetMaxY(favorite_dishes .frame)+13, WIDTH-25, 1);
        serv_now .frame = CGRectMake(95,CGRectGetMaxY(img_line.frame)+5,200, 28);
        btn_sevrving_now .frame = CGRectMake(0, CGRectGetMaxY(img_line.frame)+2,320, 37);
        text_favorite_dishes.frame = CGRectMake(15, CGRectGetMaxY(img_background.frame)-45, WIDTH-80, 34);
        // text_favorite_dishes.frame = CGRectMake(13, CGRectGetMaxY(img_background.frame)-40, WIDTH-80, 30);
        img_table.frame  = CGRectMake(5,290,WIDTH-5,IS_IPHONE_5?275:195);
        
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ary_dishList count];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 185;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH+5, 170);
        
    }
    else if (IS_IPHONE_6)
    {
        img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-5, 185);
        
    }
    else
    {    img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-10,170);
        
    }
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    img_cellBackGnd.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [cell.contentView addSubview:img_cellBackGnd];
    
    
    
    
    
    
    
    
    UIImageView *img_dish = [[UIImageView alloc] init];
    img_dish.backgroundColor=[UIColor clearColor];
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat: @"%@", [[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"DishImage"]]];
    
//    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//        if (data) {
//            UIImage *image = [UIImage imageWithData:data];
//            if (image) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                   // img_dish.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
    
                        NSString *url_Img = [NSString stringWithFormat: @"%@", [[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"DishImage"]];
                        img_dish.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img]]];
//
//                });
//            }
//        }
//    }];
//    [task resume];

    
    
//    UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"DishImage"]]];
//    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(image)];
//    img_dish.image = [UIImage imageWithData:imageData];
    

//    

    
//        NSString *ImagePath1 =[[NSString stringWithFormat:@"%@",[[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"DishImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        [img_dish setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@""]];
    img_dish.userInteractionEnabled=YES;
//    NSString *ImagePath1 =[[NSString stringWithFormat:@"%@",[[[ary_dishList objectAtIndex:0] objectAtIndex:indexPath.row] valueForKey:@"DishImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //    img_dish.frame = CGRectMake(7,7, 90,  95 );
    //[img_dish setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[ary_dishList objectAtIndex:indexPath.row]]]];
    [img_cellBackGnd addSubview:img_dish];
    
    
    UIButton *btn_favorite = [UIButton buttonWithType:UIButtonTypeCustom];
    //  btn_favorite.frame = CGRectMake(5,58, 35, 35);
    btn_favorite .backgroundColor = [UIColor clearColor];
    [btn_favorite addTarget:self action:@selector(btn_favorite_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_favorite setImage:[UIImage imageNamed:@"favorite-icon@2x.png"] forState:UIControlStateNormal];
    [img_dish   addSubview:btn_favorite];
    
    UIButton *btn_number_three = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_number_three.frame = CGRectMake(58,-5, 35, 35);
    //icon_cow .backgroundColor = [UIColor clearColor];
    [btn_number_three addTarget:self action:@selector(btn_number_three_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_number_three setImage:[UIImage imageNamed:@"red3-icon@2x.png"] forState:UIControlStateNormal];
    [img_dish   addSubview:btn_number_three];
    
    
    UIButton *icon_delete = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_delete.frame = CGRectMake(WIDTH-70,20, 40, 40);
    //icon_cow .backgroundColor = [UIColor clearColor];
    icon_delete.tag = indexPath.row;
    [icon_delete addTarget:self action:@selector(btn_add_to_cart_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_delete setImage:[UIImage imageNamed:@"img-cart@2x.png"] forState:UIControlStateNormal];
    [cell.contentView   addSubview:icon_delete];
    
    UILabel *dish_name = [[UILabel alloc]init];
    //   dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
//    dish_name.text = [NSString stringWithFormat:@"%@",[ary_displaynames objectAtIndex:indexPath.row]];
    dish_name.text = [[ary_dishList  objectAtIndex:indexPath.row] valueForKey:@"DishName"];
    dish_name.font = [UIFont fontWithName:kFontBold size:7];
    dish_name.textColor = [UIColor blackColor];
    dish_name.userInteractionEnabled=YES;
    dish_name.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:dish_name];
    
    
    UIButton *btn_DishName = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_number_three.frame = CGRectMake(58,-5, 35, 35);
    //icon_cow .backgroundColor = [UIColor clearColor];
    [btn_DishName addTarget:self action:@selector(btn_DishName:) forControlEvents:UIControlEventTouchUpInside];
    [dish_name   addSubview:btn_DishName];
    
    
    UIImageView *icon_location = [[UIImageView alloc] init];
    //    icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
    [icon_location setImage:[UIImage imageNamed:@"location-icon@2x.png"]];
    [img_cellBackGnd  addSubview:icon_location];
    
    UILabel *meters = [[UILabel alloc]init];
    //  meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
//    meters.text = @"5km";
    meters.text = [[ary_dishList  objectAtIndex:indexPath.row] valueForKey:@"kitchenDistance"];

    meters.font = [UIFont fontWithName:kFont size:15];
    meters.textColor = [UIColor blackColor];
    meters.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:meters];
    
    
    
   
    
//    if ([[NSString stringWithFormat:@"%@",[[ary_maintosave  objectAtIndex:indexPath.row] valueForKey:@"tableshow"]]isEqualToString:@"NO"])
//    {
//        delivery_Scroll.hidden = YES;
//        
//    }
//    else{
//        delivery_Scroll.hidden = NO;
//        
//    }
    


    
    
//    layout=[[UICollectionViewFlowLayout alloc] init];
//    if (IS_IPHONE_6Plus)
//    {
//
//
//        collView_serviceDirectory = [[UICollectionView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(img_dish.frame)+110,CGRectGetMaxY(icon_location.frame)+75,150,40)collectionViewLayout:layout];
//
//    }
//    else if (IS_IPHONE_6)
//    {
//        
//    collView_serviceDirectory = [[UICollectionView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(img_dish.frame)+110,CGRectGetMaxY(icon_location.frame)+85,150,40)collectionViewLayout:layout];
//
//    }
//    else
//    {
//    
//        
//    collView_serviceDirectory = [[UICollectionView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(img_dish.frame)+105,CGRectGetMaxY(icon_location.frame)+80,120,30)collectionViewLayout:layout];
//
//    }
//    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
//    [collView_serviceDirectory setDataSource:self];
//    [collView_serviceDirectory setDelegate:self];
//    collView_serviceDirectory.scrollEnabled = YES;
//    collView_serviceDirectory.showsVerticalScrollIndicator = NO;
//    collView_serviceDirectory.showsHorizontalScrollIndicator = NO;
//    collView_serviceDirectory.pagingEnabled = NO;
//    [collView_serviceDirectory registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
//    [collView_serviceDirectory setBackgroundColor:[UIColor clearColor]];
//    layout.minimumInteritemSpacing = 2;
//    layout.minimumLineSpacing = 0;
//    collView_serviceDirectory.userInteractionEnabled = YES;
//    [img_cellBackGnd addSubview:collView_serviceDirectory];

    
//
    UILabel *doller_rate = [[UILabel alloc]init];
    //   doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
//    doller_rate.text = @"$14.90";
    doller_rate.text = [NSString stringWithFormat:@"$%@",[[ary_dishList  objectAtIndex:indexPath.row] valueForKey:@"DishPrice"]];

    doller_rate.font = [UIFont fontWithName:kFontBold size:16];
    doller_rate.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    doller_rate.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:doller_rate];
    
    UIImageView *img_line = [[UIImageView alloc] init];
    //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
    [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
    [img_cellBackGnd addSubview:img_line];
    
    
    UIButton *icon_take = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
    //icon_take .backgroundColor = [UIColor clearColor];
    [icon_take addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_take setImage:[UIImage imageNamed:@"take-icon@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_take];
    
    UIButton *icon_deliver = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //icon_deliver .backgroundColor = [UIColor clearColor];
    [icon_deliver addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_deliver setImage:[UIImage imageNamed:@"deliver-icon@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_deliver];
    
    
    UIButton *img_btn_seving_Now = [UIButton buttonWithType:UIButtonTypeCustom];
    //   img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //img_btn_deliver_Now .backgroundColor = [UIColor clearColor];
    [img_btn_seving_Now addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [img_btn_seving_Now setImage:[UIImage imageNamed:@"now-icon@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:img_btn_seving_Now];
    
    
    UIButton *img_btn_chef_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    //    img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
    //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
    [img_btn_chef_menu addTarget:self action:@selector(btn_chef_menu_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_btn_chef_menu setImage:[UIImage imageNamed:@"img_servemenutime@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:img_btn_chef_menu];
    
    
  
    
    UILabel*label_view_chef_menudata;
    
    label_view_chef_menudata = [[UILabel alloc]init];
  //  label_view_chef_menudata .frame = CGRectMake(5,2,20, 12);
    label_view_chef_menudata .font = [UIFont fontWithName:kFont size:10];
    label_view_chef_menudata.text = [NSString stringWithFormat:@"%@",[[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"Total_Serving"]];
    
    label_view_chef_menudata .textColor = [UIColor whiteColor];
    [label_view_chef_menudata setUserInteractionEnabled:YES];
    label_view_chef_menudata .backgroundColor = [UIColor clearColor];
    label_view_chef_menudata.textAlignment = NSTextAlignmentCenter;
    [img_btn_chef_menu addSubview:label_view_chef_menudata ];
    

    
    
    UIButton *icon_thumb = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+5, 20, 20);
    //icon_thumb .backgroundColor = [UIColor clearColor];
    [icon_thumb addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_thumb setImage:[UIImage imageNamed:@"thumb-icon@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_thumb];
    
    UILabel *likes = [[UILabel alloc]init];
    //  likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
//    likes.text = @"87.4%";
    likes.text = [[ary_dishList  objectAtIndex:indexPath.row] valueForKey:@"likes"];

    likes.font = [UIFont fontWithName:kFont size:10];
    likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    likes.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:likes];
    
    if (IS_IPHONE_6Plus)
    {
        img_dish.frame = CGRectMake(10,7, 95, 105 );
        btn_favorite.frame = CGRectMake(5,58, 35, 35);
        btn_number_three.frame = CGRectMake(58,-5, 35, 35);
        
        icon_delete.frame = CGRectMake(WIDTH-50,40, 40, 40);
        
        dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 21);
        btn_DishName.frame=CGRectMake(0, 0, 200, 20);
        icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+10,45,200, 15);
      
        doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
        img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-45, 0.5 );
        icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        label_view_chef_menudata .frame = CGRectMake(5,5,40, 50);

        icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+5, 20, 20);
        likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+7,137,100, 10);
        
        dish_name.font = [UIFont fontWithName:kFontBold size:18];
        
    }
    else if (IS_IPHONE_6)
    {
        img_dish.frame = CGRectMake(7,7, 100,  115 );
        btn_favorite.frame = CGRectMake(5,58, 35, 35);
        btn_number_three.frame = CGRectMake(58,-5, 35, 35);
        icon_delete.frame = CGRectMake(WIDTH-62,40, 40, 40);
        dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 21);
        btn_DishName.frame=CGRectMake(0, 0, 200, 20);

        icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+10,40,200, 15);
       
        doller_rate.frame = CGRectMake(WIDTH-90,95,200, 15);
        img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+30, WIDTH-60, 0.5 );
        icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        label_view_chef_menudata .frame = CGRectMake(5,5,40, 50);

        icon_thumb.frame = CGRectMake(280,CGRectGetMaxY(img_line.frame)+5, 20, 20);
        likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,CGRectGetMaxY(img_line.frame)+12,100, 10);
        dish_name.font = [UIFont fontWithName:kFontBold size:18];
        
    }
    else
    {
        img_dish.frame = CGRectMake(7,7, 90,  95 );
        btn_favorite.frame = CGRectMake(5,58, 35, 35);
        btn_number_three.frame = CGRectMake(58,-5, 35, 35);
        icon_delete.frame = CGRectMake(WIDTH-65,40, 40, 40);
        dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 20);
        btn_DishName.frame=CGRectMake(0, 0, 200, 20);

        icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+10,40,200, 15);
        
        doller_rate.frame = CGRectMake(IS_IPHONE_5?WIDTH-80:WIDTH-80,85,200, 15);
        img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
        icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        label_view_chef_menudata .frame = CGRectMake(5,5,40, 50);

        icon_thumb.frame = CGRectMake(IS_IPHONE_5?230:230,CGRectGetMaxY(img_line.frame)+5, 20, 20);
        likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
        dish_name.font = [UIFont fontWithName:kFontBold size:IS_IPHONE_5?15:15];
        
    }
    
    UIScrollView*delivery_Scroll;
    delivery_Scroll = [[UIScrollView alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(icon_location.frame)+20,150,40);
    }
    else  if (IS_IPHONE_6)
    {
        
        delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(icon_location.frame)+20,150,40);
    }
    else  if (IS_IPHONE_5)
    {
        
        delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(icon_location.frame)+20,150,40);
    }
    else
    {
        delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(icon_location.frame)+20,150,40);
    }
    
    delivery_Scroll.backgroundColor = [UIColor whiteColor];
    delivery_Scroll.bounces=YES;
    //delivery_Scroll.layer.borderWidth = 1.0;
    delivery_Scroll.layer.borderColor = [UIColor blackColor].CGColor;
    delivery_Scroll.showsVerticalScrollIndicator = NO;
    delivery_Scroll.showsHorizontalScrollIndicator = NO;

    //delivery_Scroll.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [delivery_Scroll setScrollEnabled:YES];
    delivery_Scroll.userInteractionEnabled = YES;
    [cell.contentView addSubview:delivery_Scroll];
    //[(NSDictionary *) [[ary_maintosave  objectAtIndex:0] valueForKey:@"Delivery_Address"] count]
    
    int totalPage;
    
    totalPage = (int)[[[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"DishRestrictions"] count];
    [delivery_Scroll setContentSize:CGSizeMake(50*totalPage, delivery_Scroll.frame.size.height)];
    
    
    
    for (int j = 0; j<totalPage; j++)
    {
        
        
        UIImageView *img_non_veg = [[UIImageView alloc] init];
        img_non_veg.frame = CGRectMake(50*j,10, 25, 25);
        
//        NSString *ImagePath1 =[[NSString stringWithFormat:@"%@",[[[[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"DishRestrictions"] objectAtIndex:j] valueForKey:@"resImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        [img_non_veg setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@""]];
        
        NSString *url_Img = [NSString stringWithFormat: @"%@", [[[[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"DishRestrictions"] objectAtIndex:j] valueForKey:@"resImage"]];
        img_non_veg.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img]]];
        
        //    NSString *ImagePath1 =  [[NSString stringWithFormat:@"%@",[[array_items_name objectAtIndex:indexPath.row] valueForKey:@"resImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //    [img_non_veg setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@"img_merchantPlaceholder@2x.png"]];
        img_non_veg.backgroundColor=[UIColor clearColor];
        [delivery_Scroll addSubview:img_non_veg];
        
        
        //        UIButton*get_Deliver_Address_Btn;
        //        get_Deliver_Address_Btn = [[UIButton alloc]init];
        //        get_Deliver_Address_Btn.frame = CGRectMake(0,25*j, delivery_Scroll.frame.size.width, 25);
        //        [get_Deliver_Address_Btn addTarget:self action:@selector(get_Deliver_Address_BtnMethod:) forControlEvents:UIControlEventTouchUpInside];
        //        get_Deliver_Address_Btn.tag = j;
        //        get_Deliver_Address_Btn.titleLabel.text = [NSString stringWithFormat:@"%d",indexPath.row];
        //        [get_Deliver_Address_Btn setBackgroundColor:[UIColor clearColor]];
        //        //   get_Deliver_Address_Btn.layer.borderWidth = 1.0;
        //        [delivery_Scroll addSubview: get_Deliver_Address_Btn];
    }

    
    return cell;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    [self.navigationController pushViewController:VC animated:NO];
    
    ItemDetailVC*vc = [ItemDetailVC new];
    vc.str_dishID = [[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"DishID"];
    vc.str_type = @"0";
    [self presentViewController:vc animated:NO completion:nil];
}




#pragma mark UiCollectionView Delegate Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 4;
    //    return [ary_DishRestrictions count];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
       cell.backgroundColor=[UIColor clearColor];
    
          UIImageView *img_backGnd = [[UIImageView alloc]init];
          if (IS_IPHONE_6Plus)
          {
             img_backGnd.frame = CGRectMake(1,-5, 35, 35);
          }
          else if (IS_IPHONE_6)
          {
             img_backGnd.frame = CGRectMake(1,-5, 35, 35);
          }
          else
          {
           img_backGnd.frame = CGRectMake(1,-2, 33, 30);
           }
    
        [img_backGnd setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
        img_backGnd.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:img_backGnd];
    
        UIImageView *img_non_veg = [[UIImageView alloc] init];
        NSString *ImagePath1 =[[NSString stringWithFormat:@"%@",[[ary_DishRestrictions objectAtIndex:indexPath.row] valueForKey:@"resImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
       [img_non_veg setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@""]];
    //    NSString *ImagePath1 =  [[NSString stringWithFormat:@"%@",[[array_items_name objectAtIndex:indexPath.row] valueForKey:@"resImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //    [img_non_veg setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@"img_merchantPlaceholder@2x.png"]];
        img_non_veg.frame = CGRectMake(2,0, 30, 20 );
        img_non_veg.backgroundColor=[UIColor clearColor];
        [img_backGnd addSubview:img_non_veg];
//    
//        UIImageView  *img_swapImg = [[UIImageView alloc]initWithFrame: CGRectMake(0,-2,50,20)];
//        [img_swapImg setImage:[UIImage imageNamed:@"bg_nonveg@2x.png"]];
//        img_swapImg .backgroundColor = [UIColor clearColor];
//        [img_swapImg setUserInteractionEnabled:YES];
//         [img_backGnd addSubview:img_swapImg];
//        img_swapImg.hidden=YES;
//        if (indexPath.row==selectedindex)
//        {
//            img_swapImg.hidden=NO;
//            UILabel *chef_titiles = [[UILabel alloc]init];
//            chef_titiles .frame = CGRectMake(0,0,50, 15);
//            chef_titiles .text = [NSString stringWithFormat:@"%@",[[ary_DishRestrictions  objectAtIndex:indexPath.row] valueForKey:@"Restriction_name"]];
//            chef_titiles .font = [UIFont fontWithName:kFontBold size:9];
//            chef_titiles .textColor = [UIColor whiteColor];
//            chef_titiles .backgroundColor = [UIColor clearColor];
//            [img_swapImg addSubview:chef_titiles];
//                // img_MenuImg.hidden=NO;
//        }
//        else
//        {
//            img_swapImg.hidden=YES;
//            // img_MenuImg.hidden=YES;
//        }
    
    
    
    UIImageView  *icon_dietary = [[UIImageView alloc]init];
    icon_dietary.frame = CGRectMake(8,3,35,35);
    [icon_dietary setImage:[UIImage imageNamed:@"bg_nonveg@2x.png"]];
    icon_dietary .backgroundColor = [UIColor clearColor];
    [icon_dietary setUserInteractionEnabled:YES];
    [img_backGnd  addSubview:icon_dietary];
    
    if (indexPath.row==selectedindex)
    {
        icon_dietary.hidden=NO;
        
        UILabel *lable_non_veg = [[UILabel alloc]init];
        lable_non_veg .frame = CGRectMake(0,0,50, 15);
        lable_non_veg .text = [NSString stringWithFormat:@"%@",[[ary_DishRestrictions  objectAtIndex:indexPath.row] valueForKey:@"Restriction_name"]];
        lable_non_veg .font = [UIFont fontWithName:kFontBold size:9];
        lable_non_veg .textColor = [UIColor whiteColor];
        lable_non_veg .backgroundColor = [UIColor clearColor];
        [icon_dietary addSubview:lable_non_veg];

        
    }
    else
    {
        icon_dietary.hidden=YES;
        
    }
    
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (IS_IPHONE_6Plus)
    {
        return CGSizeMake(50, 25);
    }
    else if (IS_IPHONE_6)
    {
        return CGSizeMake(WIDTH/4-56, 25);
    }
    else
    {
        return CGSizeMake(WIDTH/4-44, 25);
    }
    
    return CGSizeMake(0, 0);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
        selectedindex= (int)indexPath.row;
        
//        if (indexPath.row==selectedindex)
//        {
//            indexSelected = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
//        }
//        [collectionView reloadData];
    
    
    if (indexPath.row==selectedindex)
    {
        
        indexSelected = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
    }
    
    [collView_serviceDirectory reloadData];
    

    
    
}



#pragma mark -integrateJWSlider
-(void)integrateJWSlider
{
    
    if ([[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Role"]]isEqualToString:@"1"])
    {//user
        
        JWSlideMenuController *slideMenu=[[JWSlideMenuController alloc] init];
        JWSlideMenuViewController *vc_Home=[[ChefHomeViewController alloc] init];
        [slideMenu addViewController:vc_Home  withTitle:@"Home" andImage:[UIImage imageNamed:@"img_home@2x.png"]];
        
        JWSlideMenuViewController *vc_Appointment=[MyScheduleVC new];
        [slideMenu addViewController:vc_Appointment withTitle:@"My\n Schedule" andImage:[UIImage imageNamed:@"img_schedule@2x.png"]];
        
        JWSlideMenuViewController *vc_MyVeh=[MyordersVC new];
        [slideMenu addViewController:vc_MyVeh withTitle:@"Orders" andImage:[UIImage imageNamed:@"img_order@2x.png"]];
        
        JWSlideMenuViewController *vc_Accessories=[MyMenuVC new];
        [slideMenu addViewController:vc_Accessories withTitle:@"My Menu" andImage:[UIImage imageNamed:@"img_Menusrceen@2x.png"]];
        
        JWSlideMenuViewController *vc_Notification=[MyAccountVC new];
        [slideMenu addViewController:vc_Notification withTitle:@"Account" andImage:[UIImage imageNamed:@"img_account@2x.png"]];
        
        JWSlideMenuViewController *vc_Promo=[MessagesVC new];
        [slideMenu addViewController:vc_Promo withTitle:@"Notification" andImage:[UIImage imageNamed:@"img_notification@2x.png"]];
        
        JWSlideMenuViewController *vc_Promo1=[MessageListVC new];
        [slideMenu addViewController:vc_Promo1 withTitle:@"Messages" andImage:[UIImage imageNamed:@"img_msg@2x.png"]];
        
        
        JWSlideMenuViewController *vc_Promo2=[SwitchtoUser new];
        [slideMenu addViewController:vc_Promo2 withTitle:@"Switch to\n User" andImage:[UIImage imageNamed:@"img_swtich@2x.png"]];
        
        [self presentViewController:slideMenu animated:NO completion:nil];
       
    }
    else
    {
        //chef
       
        JWSlideMenuController *slideMenu=[[JWSlideMenuController alloc] init];
        JWSlideMenuViewController *vc_Home=[[HomeVC alloc] init];
        [slideMenu addViewController:vc_Home  withTitle:@"Home" andImage:[UIImage imageNamed:@"img_home@2x.png"]];
        
        JWSlideMenuViewController *vc_Appointment=[SerchForFoodNowVC new];
        [slideMenu addViewController:vc_Appointment withTitle:@"Food Now\n Later" andImage:[UIImage imageNamed:@"img_schedule@2x.png"]];
        
        JWSlideMenuViewController *vc_MyVeh=[MyordersVC new];
        [slideMenu addViewController:vc_MyVeh withTitle:@"My Orders" andImage:[UIImage imageNamed:@"img_order@2x.png"]];
        
        JWSlideMenuViewController *vc_Notification=[DiningCartVC new];
        [slideMenu addViewController:vc_Notification withTitle:@"Dining\n Cart" andImage:[UIImage imageNamed:@"img_account@2x.png"]];
        
        JWSlideMenuViewController *vc_Promo=[ChefProfileVC new];
        [slideMenu addViewController:vc_Promo withTitle:@"Favorites" andImage:[UIImage imageNamed:@"icon-fav.png"]];
        
        JWSlideMenuViewController *vc_Promo1=[NotificationSettingsVC new];
        [slideMenu addViewController:vc_Promo1 withTitle:@"Notification" andImage:[UIImage imageNamed:@"img_notification@2x.png"]];
        JWSlideMenuViewController *vc_Promo2=[MessageListVC new];
        
        [slideMenu addViewController:vc_Promo2 withTitle:@"Messages" andImage:[UIImage imageNamed:@"img_msg@2x.png"]];
        JWSlideMenuViewController *vc_Promo3=[SwitchtoUser new];
        [slideMenu addViewController:vc_Promo3 withTitle:@"Switch to\n Chef" andImage:[UIImage imageNamed:@"img_swtich@2x.png"]];
        
        [self presentViewController:slideMenu animated:NO completion:nil];


        
    }
    
}
-(void)btn_HomeScreenClick:(id)sender
{
    NSLog(@"btn_HomeScreenClick");
    
    //[self showMenu];
    
   // [self integrateJWSlider];
    
}


#pragma mark Click Events

-(void)click_on_logo_btn:(UIButton *)sender
{
    NSLog(@"click_on_logo_btn");
    UserSupportVC *vc = [[UserSupportVC alloc]init];
    [self presentViewController:vc animated:NO completion:nil];
}

-(void)btn_DishName:(UIButton *)sender
{
    ItemDetailVC *VC = [[ItemDetailVC alloc] init];
    VC.str_dishID = [[ary_dishList objectAtIndex:0] valueForKey:@"DishID"];
    [self presentViewController:VC animated:NO completion:nil];
 
}
//-(void)btn_HomeScreenClick:(UIButton *)sender
//{
//    NSLog(@"btn_action_on_menu");
//    UserProfileVC*vc = [[UserProfileVC alloc]init];
//    [self presentViewController:vc animated:NO completion:nil];
////        [self.navigationController pushViewController:vc animated:NO];
//    
//}
-(void)btn_action_on_serch:(UIButton *)sender
{
    NSLog(@"btn_action_on_serch");
    UserSearchVC * vc = [[UserSearchVC alloc]init];
    [self presentViewController:vc animated:NO completion:nil];

    //[self.navigationController pushViewController:vc animated:NO];
}

-(void)btn_number_three_click:(UIButton *)sender
{
    NSLog(@"btn_number_three_click:");
}


-(void)icon_Food_Now_BtnClick:(UIButton *)sender
{
    NSLog(@"icon_Drop_Down Btn Click");
    //    SearchForMoreFiltersVC*vc = [[SearchForMoreFiltersVC alloc]init];
    //    [self.navigationController pushViewController:vc animated:NO];
      SerchForFoodNowVC *vc = [[SerchForFoodNowVC alloc]init];
      //[self presentViewController:vc animated:NO completion:nil];
   [self.navigationController pushViewController:vc];
    
}
-(void)icon_Food_Later_BtnClick:(UIButton *)sender
{
    NSLog(@"icon_Drop_Down Btn Click");
     SerchForFoodNowVC*vc  = [[SerchForFoodNowVC alloc]init];
   // [self presentViewController:vc animated:NO completion:nil];

     [self.navigationController pushViewController:vc];
    
}
-(void)btn_favorite_click:(UIButton *)sender
{
    NSLog(@"btn_favorite_click");
    [self AFUserFavoritesDish];
    
//    VC.str_dishID = [[ary_dishList objectAtIndex:0] valueForKey:@"DishID"];

}

-(void)btn_add_to_cart_click:(UIButton *)sender
{
    NSLog(@"btn_add_to_cart_click:");
    NSMutableArray*ary_dummy;
    ary_dummy = [NSMutableArray new];
    [ary_dummy addObject:[ary_dishList objectAtIndex:sender.tag]];
    
    AddToCartFoodNowVC*vc = [AddToCartFoodNowVC new];
    vc.ary_details = ary_dummy;
    vc.str_camefrom = @"HOME";
    [self presentViewController:vc animated:NO completion:nil];

    // [self.navigationController pushViewController:vc animated:NO];
}

-(void)btn_favorite_dish_click:(UIButton *)sender
{
    NSLog(@"btn_favorite_dish_click:");
}
-(void)btn_favorite_cuisines_click:(UIButton *)sender
{
    NSLog(@"btn_favorite_cuisines_click");
}
-(void)btn_all_dishes_click:(UIButton *)sender
{
    NSLog(@"btn_all_dishes_click");
    
    
    
}
-(void)btn_serving_now_click:(UIButton *)sender
{
    NSLog(@"btn_serving_now_click:");
}
-(void)btn_chef_menu_click:(UIButton *)sender
{
    NSLog(@"btn_chef_menu_click:");
}
// #pragma mark -integrateJWSlider
//-(void)integrateJWSlider
//{
//    JWSlideMenuController *slideMenu=[[JWSlideMenuController alloc] init];
//    JWSlideMenuViewController *vc_Home=[[HomeVC alloc] init];
//    [slideMenu addViewController:vc_Home  withTitle:@"Home" andImage:[UIImage imageNamed:@"img_home@2x.png"]];
//    
//    JWSlideMenuViewController *vc_Appointment=[SerchForFoodNowVC new];
//    [slideMenu addViewController:vc_Appointment withTitle:@"Food Now\n Later" andImage:[UIImage imageNamed:@"img_schedule@2x.png"]];
//    
//    JWSlideMenuViewController *vc_MyVeh=[MyordersVC new];
//    [slideMenu addViewController:vc_MyVeh withTitle:@"Orders" andImage:[UIImage imageNamed:@"img_order@2x.png"]];
//    
//    JWSlideMenuViewController *vc_Accessories=[DiningCartVC new];
//    [slideMenu addViewController:vc_Accessories withTitle:@"Dining\nCart" andImage:[UIImage imageNamed:@"img_Menusrceen@2x.png"]];
//    
//    JWSlideMenuViewController *vc_Notification=[UserProfileVC new];
//    [slideMenu addViewController:vc_Notification withTitle:@"Favorites" andImage:[UIImage imageNamed:@"img_account@2x.png"]];
//    
//    JWSlideMenuViewController *vc_Promo=[NotificationsVC new];
//    [slideMenu addViewController:vc_Promo withTitle:@"Notification" andImage:[UIImage imageNamed:@"img_notification@2x.png"]];
//    
//    JWSlideMenuViewController *vc_Promo1=[UserMessagesVC new];
//    [slideMenu addViewController:vc_Promo1 withTitle:@"Messages" andImage:[UIImage imageNamed:@"img_msg@2x.png"]];
//    
//    
//    JWSlideMenuViewController *vc_Promo2=[UserMessagesVC new];
//    [slideMenu addViewController:vc_Promo2 withTitle:@"Switch to\n User" andImage:[UIImage imageNamed:@"img_swtich@2x.png"]];
//    
//    [self presentViewController:slideMenu animated:NO completion:nil];
//    
//}
// -(void)btn_HomeScreenClick:(id)sender
//{
//    NSLog(@"btn_HomeScreenClick");
//    
//    //[self showMenu];
//    
////    [self integrateJWSlider];
//    
//}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}
#pragma userDishLists-functionality

-(void)AFUserDishLists
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
//    @"lat"                       :  str_Latitude,
//    @"long"                      :  str_Longitude,
    
    NSDictionary *params =@{
                            
                            @"lat"                                 :  @"13.552534",
                            @"long"                                :  @"137.225362",
//                            @"role_type"                         :  @"user_profile",
//                            @"device_udid"                       :  UniqueAppID,
//                            @"device_token"                      :  @"Dev",
//                            @"device_type"                       :  @"1"
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:kUserDishLists  parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseUserDishLists:JSON];
    }
     
     
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error){
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFUserDishLists];
                                         }
                                     }];
    [operation start];
    
}
-(void)ResponseUserDishLists:(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    [ary_dishList removeAllObjects];
    [ary_DishRestrictions removeAllObjects];
    [ary_Serving_Type removeAllObjects];
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        for (int i=0; i<[[TheDict valueForKey:@"DishsList"] count]; i++)
        {
         [ary_dishList addObject:[[TheDict valueForKey:@"DishsList"]  objectAtIndex:i]];
            
        }
        
        for (int j=0; j<[[[TheDict valueForKey:@"DishsList"] valueForKey:@"DishRestrictions"] count]; j++)
        {
         [ary_DishRestrictions addObject:[[[TheDict valueForKey:@"DishsList"] valueForKey:@"DishRestrictions"] objectAtIndex:j]];
            
        }
        
        for (int z=0; z<[[[TheDict valueForKey:@"DishsList"] valueForKey:@"DishRestrictions"] count]; z++)
        {
            [ary_Serving_Type addObject:[[[TheDict valueForKey:@"DishsList"] valueForKey:@"Serving_Type"] objectAtIndex:z]];
            
        }

     
        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        [img_table reloadData];

        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
           }
    
}



#pragma UserFavoritesDish-functionality

-(void)AFUserFavoritesDish
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            
                            @"uid"                               :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"dish_id"                           :  @"206",
                            @"favorite_type"                     :  @"Dish",
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:kUserUserFavoritesDish  parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseUserFavoritesDish:JSON];
    }
     
     
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error){
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFUserFavoritesDish];
                                         }
                                     }];
    [operation start];
    
}
-(void)ResponseUserFavoritesDish:(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
//        for (int i=0; i<[[TheDict valueForKey:@"DishsList"] count]; i++)
//        {
//            [ary_dishList addObject:[[TheDict valueForKey:@"DishsList"]  objectAtIndex:i]];
//            
//        }
//        
//        
//        for (int j=0; j<[[[TheDict valueForKey:@"DishsList"] valueForKey:@"DishRestrictions"] count]; j++)
//        {
//            [ary_DishRestrictions addObject:[[[TheDict valueForKey:@"DishsList"] valueForKey:@"DishRestrictions"] objectAtIndex:j]];
//            
//        }
//        
//        for (int z=0; z<[[[TheDict valueForKey:@"DishsList"] valueForKey:@"DishRestrictions"] count]; z++)
//        {
//            [ary_Serving_Type addObject:[[[TheDict valueForKey:@"DishsList"] valueForKey:@"Serving_Type"] objectAtIndex:z]];
//            
//        }
        
        
        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
//        [img_table reloadData];
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
    }
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
