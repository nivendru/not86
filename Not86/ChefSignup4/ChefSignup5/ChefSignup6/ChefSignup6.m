
//
//  ChefSignup6.m
//  Not86
//
//  Created by Interwld on 8/13/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ChefSignup6.h"
#import "ChefHomeViewController.h"
#import "AppDelegate.h"
#import "Define.h"
#import "LoginViewController.h"
#import "AboutUS.h"

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"

@interface ChefSignup6 ()<UIScrollViewDelegate,UITextViewDelegate,UITextFieldDelegate>{
    UIImageView *img_topbar;
    UIImageView *img_BackgroundImg;
    UIScrollView *scrollview6;
    UITextField *txt_Email;
    UIView *alertviewBg;
    AppDelegate *delegate;
    int ABC;
    NSMutableArray *ary_Month;
    UIButton *btn_Endofmonth;
    UIButton *btn_Middleofmonth;
    NSString  *str_payInmonth;
    UIButton *btn_Termsandconditions;
    UILabel *lbl_end_of_month;
    UILabel *lbl_middle_of_month;
    UILabel *lbl_end_of_month1 ;
    NSString*str_success;
    UITextView *txtview_terms_and_conditions;
    
    
}

@end

@implementation ChefSignup6

- (void)viewDidLoad {
    [super viewDidLoad];
    [self IntegrateHeaderDesign];
    [self IntegrateBodyDesign];
    
    str_payInmonth = [NSString new];
    
    str_success = [NSString new];
    
    //    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ary_Month=[[NSMutableArray alloc]init];
    
    
    
    if (IS_IPHONE_5) {
        
        scrollview6.scrollEnabled = YES;
    }
    else{
        scrollview6.scrollEnabled = NO;
        
    }// Do any additional setup after loading the view.
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
}
-(void)IntegrateHeaderDesign{
    
    
    
    img_topbar=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,45)];
    [img_topbar setUserInteractionEnabled:YES];
    img_topbar.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:img_topbar];
    
    
    UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(12, 16, 15, 15)];
    [img_back setUserInteractionEnabled:YES];
    img_back.backgroundColor=[UIColor clearColor];
    img_back.image=[UIImage imageNamed:@"arrow@2x.png"];
    [img_topbar addSubview:img_back];
    
    UILabel  * lbl_heading = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(img_back.frame)+20,0, 220,45)];
    lbl_heading.text = @"Chef Application";
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:kFont size:15];
    [img_topbar addSubview:lbl_heading];
    
    UIButton *btn_back = [[UIButton alloc] init];
    btn_back.frame = CGRectMake(0, 0, 60,35);
    btn_back.backgroundColor = [UIColor clearColor];
    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_topbar addSubview:btn_back];
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 7, 30, 30)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-login-logo@2x.png"];
    [img_topbar addSubview:img_logo];
    
    scrollview6 =[[UIScrollView alloc]init];
    scrollview6.frame = CGRectMake(0, 45, WIDTH, HEIGHT-80);
    [scrollview6 setShowsVerticalScrollIndicator:NO];
    scrollview6.delegate = self;
    scrollview6.scrollEnabled = YES;
    scrollview6.showsVerticalScrollIndicator = NO;
    [scrollview6 setUserInteractionEnabled:YES];
    //    scrollview6.backgroundColor=[UIColor clearColor];
    scrollview6.backgroundColor = [UIColor colorWithRed:241/255.0f green:244/255.0f blue:242/255.0f alpha:1];
    [self.view addSubview:scrollview6];
   // [scrollview6 setContentSize:CGSizeMake(0,HEIGHT)];
    
    
    
    
}
-(void)Back_btnClick{
    //    [self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
    
    
}
-(void)IntegrateBodyDesign{
    
    UILabel  *lbl_payment = [[UILabel alloc]init];
    //    lbl_payment.frame = CGRectMake(0,20, self.view.frame.size.width,30);
    lbl_payment.text = @"Payment Gateway Link";
    lbl_payment.backgroundColor=[UIColor clearColor];
    lbl_payment.textColor=[UIColor blackColor];
    lbl_payment.numberOfLines = 1;
    lbl_payment.textAlignment = NSTextAlignmentCenter;
    lbl_payment.font = [UIFont fontWithName:kFontBold size:15];
    [scrollview6 addSubview:lbl_payment];
    
    
    UIImageView *img_bg=[[UIImageView alloc]init];
    //    img_bg.frame = CGRectMake(-13, 40, WIDTH+35, 350);
    [img_bg setUserInteractionEnabled:YES];
    img_bg.backgroundColor=[UIColor clearColor];
    img_bg.image=[UIImage imageNamed:@"bg1-img@2x.png"];
    [scrollview6 addSubview:img_bg];
    
    
    UILabel  *lbl_payaccount = [[UILabel alloc]init];
    //    lbl_payaccount.frame = CGRectMake(40,CGRectGetMaxY(lbl_payment.frame)-40, self.view.frame.size.width-40,70);
    lbl_payaccount.text = @"All payments made to you by not86 for food sold\nby you on not86 will be credited directly into your\npaypal account.";
    lbl_payaccount.backgroundColor=[UIColor clearColor];
    lbl_payaccount.textColor=[UIColor blackColor];
    lbl_payaccount.numberOfLines = 5;
    lbl_payaccount.font = [UIFont fontWithName:kFont size:13];
    [img_bg addSubview:lbl_payaccount];
    
    UILabel  *lbl_payemail=[[UILabel alloc]init];
    
    lbl_payemail.text = @"Please enter your Paypal Email Address ";
    lbl_payemail.backgroundColor=[UIColor clearColor];
    lbl_payemail.textColor=[UIColor blackColor];
    lbl_payemail.numberOfLines = 2;
    lbl_payemail.font = [UIFont fontWithName:kFontBold size:15];
    [img_bg addSubview:lbl_payemail];
    
    txt_Email = [[UITextField alloc] init];
    //    txt_Email.frame=CGRectMake(65,CGRectGetMaxY(lbl_payemail.frame)+5, 250, 38);
    txt_Email.borderStyle = UITextBorderStyleNone;
    txt_Email.textColor = [UIColor grayColor];
    txt_Email.font = [UIFont fontWithName:kFont size:13];
    txt_Email.placeholder = @"Email address(chef@gmail.com)";
    [txt_Email setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Email setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Email.leftViewMode = UITextFieldViewModeAlways;
    txt_Email.userInteractionEnabled=YES;
    txt_Email.textAlignment = NSTextAlignmentLeft;
    txt_Email.backgroundColor = [UIColor clearColor];
    txt_Email.keyboardType = UIKeyboardTypeAlphabet;
    [img_bg addSubview:txt_Email];
    txt_Email.delegate = self;
    
    UIImageView *line=[[UIImageView alloc]init];
    //    line.frame=CGRectMake(65, CGRectGetMaxY(txt_Email.frame)-7, self.view.frame.size.width-100, 0.5);
    [line setUserInteractionEnabled:YES];
    line.backgroundColor=[UIColor clearColor];
    line.image=[UIImage imageNamed:@"line1.png"];
    [img_bg addSubview:line];
    
    
    UILabel  * lbl_option = [[UILabel alloc]init];
    //    lbl_option.frame=CGRectMake(self.view.frame.size.width-120,CGRectGetMaxY(line.frame)+3, 200,13);
    lbl_option.text = @"Verify at Paypal";
    lbl_option.backgroundColor=[UIColor clearColor];
    lbl_option.textColor=[UIColor colorWithRed:16/255.0f green:0/255.0f blue:87/255.0f alpha:1];
    lbl_option.font = [UIFont fontWithName:kFont size:11];
   // [img_bg addSubview:lbl_option];
    
    UIImageView *line1=[[UIImageView alloc]init];
    //    line1.frame=CGRectMake(self.view.frame.size.width-110, CGRectGetMaxY(lbl_option.frame)+1, 90, 0.5);
    [line1 setUserInteractionEnabled:YES];
    line1.backgroundColor=[UIColor colorWithRed:16/255.0f green:0/255.0f blue:87/255.0f alpha:1];
    //[img_bg addSubview:line1];
    
    
    
    UILabel  * lbl_paymentfrequency = [[UILabel alloc]init];
    //    lbl_paymentfrequency.frame=CGRectMake(45,CGRectGetMaxY(line1.frame)+3, self.view.frame.size.width-40,40);
    lbl_paymentfrequency.text = @"Please choose your preferred payment\nfrequency";
    lbl_paymentfrequency.backgroundColor=[UIColor clearColor];
    lbl_paymentfrequency.textColor=[UIColor blackColor];
    lbl_paymentfrequency.font = [UIFont fontWithName:kFontBold size:14];
    lbl_paymentfrequency.numberOfLines = 2;
    [img_bg addSubview:lbl_paymentfrequency];
    
    
    UILabel  * lbl_paymentrecieve = [[UILabel alloc]init];
    
    //    lbl_paymentrecieve.frame=CGRectMake(45,CGRectGetMaxY(lbl_paymentfrequency.frame)+3, self.view.frame.size.width-40,40);
    lbl_paymentrecieve.text = @"I wish to recieve my payment from not86 by";
    lbl_paymentrecieve.backgroundColor=[UIColor clearColor];
    lbl_paymentrecieve.textColor=[UIColor blackColor];
    lbl_paymentrecieve.font = [UIFont fontWithName:kFont size:13];
    [img_bg addSubview:lbl_paymentrecieve];
    
    
    UIImageView *back_img_for_month = [[UIImageView alloc]init];
    //    back_img_for_month.frame =  CGRectMake(55, CGRectGetMaxY(lbl_paymentrecieve.frame)+10, WIDTH-110, 40);
    [back_img_for_month setUserInteractionEnabled:YES];
    back_img_for_month.backgroundColor = [UIColor colorWithRed:222/255.0f green:221/255.0f blue:231/255.0f alpha:1];
    [img_bg  addSubview:back_img_for_month];
    
    lbl_end_of_month = [[UILabel alloc]init];
    //    lbl_end_of_month.frame = CGRectMake(20,-2, 200, 45);
    lbl_end_of_month.text = @"End of the Month";
    lbl_end_of_month.font = [UIFont fontWithName:kFont size:10];
    lbl_end_of_month.textColor = [UIColor blackColor];
    [lbl_end_of_month setUserInteractionEnabled:YES];
    lbl_end_of_month.backgroundColor = [UIColor clearColor];
    [back_img_for_month addSubview:lbl_end_of_month];
    
    btn_Endofmonth = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_Endofmonth .backgroundColor = [UIColor clearColor];
    btn_Endofmonth.userInteractionEnabled = YES;
    btn_Endofmonth.tag=1;
    [btn_Endofmonth addTarget:self action:@selector(click_Endofmonth:) forControlEvents:UIControlEventTouchUpInside];
    [lbl_end_of_month  addSubview:btn_Endofmonth];
    
    
    UIImageView * img_line_on_bg = [[UIImageView alloc]init];
    //    img_line_on_bg .frame = CGRectMake(CGRectGetMaxX(lbl_end_of_month.frame)-80, 5, 0.5, 30);
    [img_line_on_bg setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
    img_line_on_bg.backgroundColor = [UIColor clearColor];
    [img_line_on_bg setUserInteractionEnabled:YES];
    [back_img_for_month addSubview:img_line_on_bg];
    
    lbl_middle_of_month = [[UILabel alloc]init];
    //    lbl_middle_of_month.frame = CGRectMake(CGRectGetMaxX(img_line_on_bg.frame)+20,-10, 200, 45);
    lbl_middle_of_month.text = @"Middle of Month";
    lbl_middle_of_month.font = [UIFont fontWithName:kFont size:10];
    lbl_middle_of_month.textColor = [UIColor blackColor];
    lbl_middle_of_month.userInteractionEnabled = YES;
    lbl_middle_of_month.backgroundColor = [UIColor clearColor];
    [back_img_for_month addSubview:lbl_middle_of_month];
    
    lbl_end_of_month1 = [[UILabel alloc]init];
    //    lbl_end_of_month1 .frame = CGRectMake(CGRectGetMaxX(img_line_on_bg.frame)+20,03, 200, 45);
    lbl_end_of_month1 .text = @"and End of Month";
    lbl_end_of_month1 .font = [UIFont fontWithName:kFont size:10];
    lbl_end_of_month1 .textColor = [UIColor blackColor];
    lbl_end_of_month1.userInteractionEnabled = YES;
    lbl_end_of_month1 .backgroundColor = [UIColor clearColor];
    [back_img_for_month addSubview:lbl_end_of_month1];
    
    
    btn_Middleofmonth = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_Middleofmonth .backgroundColor = [UIColor clearColor];
    btn_Middleofmonth.userInteractionEnabled = YES;
    btn_Middleofmonth.tag=2;
    [btn_Middleofmonth addTarget:self action:@selector(click_Middleofmonth:) forControlEvents:UIControlEventTouchUpInside];
    [back_img_for_month  addSubview:btn_Middleofmonth];
    
    UIImageView *img_bg3 = [[UIImageView alloc]init];
    [img_bg3 setUserInteractionEnabled:YES];
    img_bg3.backgroundColor =[UIColor colorWithRed:241/255.0f green:244/255.0f blue:242/255.0f alpha:1];
    [self.view addSubview:img_bg3];
    
    //    UIImageView *text_terms_and_condition = [[UIImageView alloc]init];
    //    //    text_terms_and_condition.frame=CGRectMake(40,-5, 350, 100);
    //    [text_terms_and_condition setUserInteractionEnabled:YES];
    //    text_terms_and_condition.backgroundColor=[UIColor clearColor];
    //    text_terms_and_condition.image=[UIImage imageNamed:@"text-img@2x.png"];
    //    [img_bg3 addSubview:text_terms_and_condition];
    
    txtview_terms_and_conditions = [[UITextView alloc]init];
    txtview_terms_and_conditions.frame = CGRectMake(20,20,200,70);
    txtview_terms_and_conditions.scrollEnabled = YES;
    txtview_terms_and_conditions.userInteractionEnabled = NO;
    txtview_terms_and_conditions.font = [UIFont fontWithName:kFont size:16];
    txtview_terms_and_conditions.backgroundColor = [UIColor clearColor];
    txtview_terms_and_conditions.delegate = self;
    txtview_terms_and_conditions.userInteractionEnabled=YES;
    txtview_terms_and_conditions.textColor = [UIColor blackColor];
    txtview_terms_and_conditions.text =@"By applying to becomea not 86 chef,\n you have read and agree to the\n                                             governing\n                      the use of not86.";
    //    txtview_adddescription.text = [NSString stringWithFormat:@"%@",[[ary_displaynames objectAtIndex:0] valueForKey:@"About_Us"]];
    
    [img_bg3 addSubview:txtview_terms_and_conditions];
    
    UILabel *lbl_terms_and_conditions = [[UILabel alloc]init];
    //    lbl_terms_and_conditions .frame = CGRectMake(WIDTH/2-30,160, 220, 50);
    lbl_terms_and_conditions .text = @"Terms  and  Conditions ";
    //    lbl_terms_and_conditions .font = [UIFont fontWithName:kFont size:16];
    lbl_terms_and_conditions .textColor = [UIColor colorWithRed:20/255.0f green:55/255.0f blue:94/255.0f alpha:1];
    lbl_terms_and_conditions .backgroundColor = [UIColor clearColor];
    lbl_terms_and_conditions.textAlignment=NSTextAlignmentCenter;
    lbl_terms_and_conditions.numberOfLines = 5;
    lbl_terms_and_conditions.userInteractionEnabled=YES;
    [txtview_terms_and_conditions addSubview:lbl_terms_and_conditions];
    
    btn_Termsandconditions = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_Termsandconditions .backgroundColor = [UIColor clearColor];
    btn_Termsandconditions.userInteractionEnabled = YES;
    btn_Termsandconditions.tag=67;
    [btn_Termsandconditions addTarget:self action:@selector(click_Termsandconditions:) forControlEvents:UIControlEventTouchUpInside];
    [lbl_terms_and_conditions  addSubview:btn_Termsandconditions];
    
    
    
    UILabel *lbl_page = [[UILabel alloc]init];
    //    lbl_page .frame = CGRectMake(WIDTH/2-30,160, 100, 100);
    lbl_page .text = @"Page 6/6";
    lbl_page .font = [UIFont fontWithName:kFont size:13];
    lbl_page .textColor = [UIColor blackColor];
    lbl_page .backgroundColor = [UIColor clearColor];
    lbl_page.textAlignment=NSTextAlignmentCenter;
    lbl_page.numberOfLines = 5;
    [img_bg3 addSubview:lbl_page];
    
    
    UILabel *lbl_apply = [[UILabel alloc]init];
    lbl_apply .text = @"APPLY";
    lbl_apply .font = [UIFont fontWithName:kFont size:18];
    lbl_apply .textColor = [UIColor whiteColor];
    lbl_apply .backgroundColor = [UIColor colorWithRed:39/255.0f green:37/255.0f blue:48/255.0f alpha:1];
    lbl_apply.layer.cornerRadius = 4.0f;
    lbl_apply.clipsToBounds = YES;
    lbl_apply.textAlignment = NSTextAlignmentCenter;
    lbl_apply.userInteractionEnabled = YES;
    [img_bg3 addSubview:lbl_apply];
    
    
    UIButton *btn_apply = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_apply .backgroundColor = [UIColor clearColor];
    btn_apply.userInteractionEnabled = YES;
    [btn_apply addTarget:self action:@selector(btn_apply_click) forControlEvents:UIControlEventTouchUpInside];
    [img_bg3   addSubview:btn_apply];
    
    
    
    if (IS_IPHONE_6Plus)
    {
        scrollview6.frame = CGRectMake(0, 45, WIDTH, HEIGHT);
        lbl_payment.frame = CGRectMake(0,2, self.view.frame.size.width,30);
        img_bg.frame = CGRectMake(2, CGRectGetMaxY(lbl_payment.frame)+5, WIDTH-2, 410);
        lbl_payaccount.frame = CGRectMake(50,0, WIDTH-40,100);
        lbl_payemail.frame = CGRectMake(65,lbl_payaccount.frame.origin.y+lbl_payaccount.frame.size.height+5, self.view.frame.size.width-80,40);
        txt_Email.frame=CGRectMake(65,CGRectGetMaxY(lbl_payemail.frame)+5, 250, 38);
        line.frame=CGRectMake(65, CGRectGetMaxY(txt_Email.frame)-7, self.view.frame.size.width-100, 0.5);
        lbl_option.frame=CGRectMake(self.view.frame.size.width-120,CGRectGetMaxY(line.frame)+3, 200,13);
        line1.frame=CGRectMake(self.view.frame.size.width-120, CGRectGetMaxY(lbl_option.frame)+1, 90, 0.5);
        lbl_paymentfrequency.frame=CGRectMake(65,CGRectGetMaxY(line1.frame)+3, self.view.frame.size.width-100,60);
        lbl_paymentrecieve.frame=CGRectMake(65,CGRectGetMaxY(lbl_paymentfrequency.frame)+2, self.view.frame.size.width-40,20);
        back_img_for_month.frame =  CGRectMake(55, CGRectGetMaxY(lbl_paymentrecieve.frame)+10, WIDTH-110, 40);
        lbl_end_of_month.frame = CGRectMake(20,-2, 200, 45);
        btn_Endofmonth.frame=CGRectMake(-20, 0, 200, 45);
        img_line_on_bg .frame = CGRectMake(CGRectGetMaxX(lbl_end_of_month.frame)-80, 5, 0.5, 30);
        btn_Middleofmonth.frame=CGRectMake(CGRectGetMaxX(img_line_on_bg.frame)+1,0, 200, 45);
        lbl_middle_of_month.frame = CGRectMake(CGRectGetMaxX(img_line_on_bg.frame)+20,0, 200, 30);
        lbl_end_of_month1 .frame = CGRectMake(CGRectGetMaxX(img_line_on_bg.frame)+20,15, 200, 30);
        img_bg3.frame =  CGRectMake(0,CGRectGetMaxY(img_bg.frame)+40, WIDTH,350);
        txtview_terms_and_conditions.frame=CGRectMake(50,14, 310, 85);
        lbl_terms_and_conditions .frame =CGRectMake(0,37, 200, 40);
        btn_Termsandconditions.frame=CGRectMake(0, 0, 200, 40);
        lbl_page .frame = CGRectMake(WIDTH/2-205,CGRectGetMaxY(txtview_terms_and_conditions.frame)+55, WIDTH,30);
        lbl_apply .frame = CGRectMake(18,CGRectGetMaxY(lbl_page.frame)+5, WIDTH-36, 50);
        btn_apply.frame = CGRectMake(18,CGRectGetMaxY(lbl_page.frame)+5, WIDTH-36, 50);
        lbl_terms_and_conditions .font = [UIFont fontWithName:kFont size:16];
      //  [scrollview6 setContentSize:CGSizeMake(0,CGRectGetMaxY(img_bg.frame)+170)];
        lbl_end_of_month.font = [UIFont fontWithName:kFontBold size:12];
        lbl_middle_of_month.font = [UIFont fontWithName:kFont size:12];
        lbl_end_of_month1 .font = [UIFont fontWithName:kFont size:12];
        lbl_terms_and_conditions .font = [UIFont fontWithName:kFont size:16];

        
    }
    else if (IS_IPHONE_6)
    {
        scrollview6.frame = CGRectMake(0, 45, WIDTH, HEIGHT);
        lbl_payment.frame = CGRectMake(0,2, self.view.frame.size.width,30);
        img_bg.frame = CGRectMake(2, CGRectGetMaxY(lbl_payment.frame)+5, WIDTH-2, 390);
        lbl_payaccount.frame = CGRectMake(30,0, WIDTH-40,100);
        lbl_payemail.frame = CGRectMake(50,lbl_payaccount.frame.origin.y+lbl_payaccount.frame.size.height+5, self.view.frame.size.width-80,40);
        txt_Email.frame=CGRectMake(50,CGRectGetMaxY(lbl_payemail.frame)+5, 250, 38);
        line.frame=CGRectMake(50, CGRectGetMaxY(txt_Email.frame)-7, self.view.frame.size.width-90, 0.5);
        lbl_option.frame=CGRectMake(self.view.frame.size.width-130,CGRectGetMaxY(line.frame)+3, 200,13);
        line1.frame=CGRectMake(self.view.frame.size.width-130, CGRectGetMaxY(lbl_option.frame)+1, 90, 0.5);
        lbl_paymentfrequency.frame=CGRectMake(50,CGRectGetMaxY(line1.frame)+3, self.view.frame.size.width-50,80);
        lbl_paymentrecieve.frame=CGRectMake(50,CGRectGetMaxY(lbl_paymentfrequency.frame)-20, self.view.frame.size.width-40,40);
        back_img_for_month.frame =  CGRectMake(40, CGRectGetMaxY(lbl_paymentrecieve.frame)+10, WIDTH-80, 50);
        lbl_end_of_month.frame = CGRectMake(20,-2, 200, 45);
        btn_Endofmonth.frame=CGRectMake(-20, 0, 150, 45);
        img_line_on_bg .frame = CGRectMake(CGRectGetMaxX(lbl_end_of_month.frame)-80, 5, 0.5, 30);
        btn_Middleofmonth.frame=CGRectMake(CGRectGetMaxX(img_line_on_bg.frame)+1,0, 150, 45);
        lbl_middle_of_month.frame = CGRectMake(CGRectGetMaxX(img_line_on_bg.frame)+20,0, 200, 30);
        lbl_end_of_month1 .frame = CGRectMake(CGRectGetMaxX(img_line_on_bg.frame)+20,15, 200, 30);
        img_bg3.frame =  CGRectMake(0,CGRectGetMaxY(img_bg.frame)+35, WIDTH,350);
        txtview_terms_and_conditions.frame=CGRectMake(45,6, 310, 100);
        lbl_terms_and_conditions .frame =CGRectMake(0,37, 200, 40);
        btn_Termsandconditions.frame=CGRectMake(0, 0, 200, 40);
        lbl_page .frame = CGRectMake(WIDTH/2-205,CGRectGetMaxY(txtview_terms_and_conditions.frame)+3, WIDTH,30);
        lbl_apply .frame = CGRectMake(18,CGRectGetMaxY(lbl_page.frame)+5, WIDTH-36, 55);
        btn_apply.frame = CGRectMake(18,CGRectGetMaxY(lbl_page.frame)+5, WIDTH-36, 55);
        lbl_end_of_month.font = [UIFont fontWithName:kFontBold size:12];
        lbl_middle_of_month.font = [UIFont fontWithName:kFont size:12];
        lbl_end_of_month1 .font = [UIFont fontWithName:kFont size:12];
        lbl_terms_and_conditions .font = [UIFont fontWithName:kFont size:16];
     //   [scrollview6 setContentSize:CGSizeMake(0,CGRectGetMaxY(img_bg.frame)+180)];
        
        
        
        
    }
    else if(IS_IPHONE_5)
    {
        scrollview6.frame = CGRectMake(0, 45, WIDTH, HEIGHT);
        lbl_payment.frame = CGRectMake(0,2, self.view.frame.size.width,30);
        img_bg.frame = CGRectMake(2, CGRectGetMaxY(lbl_payment.frame)+2, WIDTH-2, 330);
        lbl_payaccount.frame = CGRectMake(20,-20, WIDTH-40,100);
        lbl_payemail.frame = CGRectMake(30,lbl_payaccount.frame.origin.y+lbl_payaccount.frame.size.height-15, self.view.frame.size.width-80,40);
        txt_Email.frame=CGRectMake(30,CGRectGetMaxY(lbl_payemail.frame)-5, 250, 38);
        line.frame=CGRectMake(30, CGRectGetMaxY(txt_Email.frame)-7, self.view.frame.size.width-90, 0.5);
        lbl_option.frame=CGRectMake(self.view.frame.size.width-130,CGRectGetMaxY(line.frame)+3, 200,13);
        line1.frame=CGRectMake(self.view.frame.size.width-130, CGRectGetMaxY(lbl_option.frame)+1, 90, 0.5);
        lbl_paymentfrequency.frame=CGRectMake(30,CGRectGetMaxY(line1.frame)+3, self.view.frame.size.width-50,80);
        lbl_paymentrecieve.frame=CGRectMake(30,CGRectGetMaxY(lbl_paymentfrequency.frame)-25, self.view.frame.size.width-40,40);
        back_img_for_month.frame =  CGRectMake(25, CGRectGetMaxY(lbl_paymentrecieve.frame)+10, WIDTH-50, 50);
        lbl_end_of_month.frame = CGRectMake(20,-2, 200, 45);
        btn_Endofmonth.frame=CGRectMake(0, 0, 200, 45);
        img_line_on_bg .frame = CGRectMake(CGRectGetMaxX(lbl_end_of_month.frame)-80, 5, 0.5, 30);
        lbl_middle_of_month.frame = CGRectMake(CGRectGetMaxX(img_line_on_bg.frame)+20,0, 200, 30);
        lbl_end_of_month1 .frame = CGRectMake(CGRectGetMaxX(img_line_on_bg.frame)+20,15, 200, 30);
        btn_Middleofmonth.frame=CGRectMake(CGRectGetMaxX(img_line_on_bg.frame)+1,0, 200, 45);
        
        img_bg3.frame =  CGRectMake(0,CGRectGetMaxY(img_bg.frame)+1, WIDTH,200);
        txtview_terms_and_conditions.frame=CGRectMake(5,6, 310, 95);
        lbl_terms_and_conditions .frame =CGRectMake(0,37, 200, 40);
        btn_Termsandconditions.frame=CGRectMake(0, 0, 200, 40);
        lbl_page .frame = CGRectMake(WIDTH/2-160,CGRectGetMaxY(txtview_terms_and_conditions.frame)+10, WIDTH,30);
        lbl_apply .frame = CGRectMake(18,CGRectGetMaxY(lbl_page.frame)+5, WIDTH-36, 50);
        btn_apply.frame = CGRectMake(18,CGRectGetMaxY(lbl_page.frame)+5, WIDTH-36, 50);
        lbl_payment.font = [UIFont fontWithName:kFontBold size:15];
        lbl_payaccount.font = [UIFont fontWithName:kFont size:11.5];
        lbl_payemail.font = [UIFont fontWithName:kFontBold size:12.5];
        lbl_option.font = [UIFont fontWithName:kFont size:11];
        lbl_paymentfrequency.font = [UIFont fontWithName:kFontBold size:12];
        lbl_paymentrecieve.font = [UIFont fontWithName:kFont size:11];
        lbl_end_of_month.font = [UIFont fontWithName:kFontBold size:11];
        lbl_middle_of_month.font = [UIFont fontWithName:kFont size:11];
        lbl_end_of_month1 .font = [UIFont fontWithName:kFont size:11];
        lbl_page .font = [UIFont fontWithName:kFont size:13];
        lbl_apply .font = [UIFont fontWithName:kFont size:18];
        lbl_terms_and_conditions .font = [UIFont fontWithName:kFont size:13];
  //      [scrollview6 setContentSize:CGSizeMake(0,CGRectGetMaxY(img_bg.frame)+170)];
        
        
    }
    else
    {
        scrollview6.frame = CGRectMake(0, 45, WIDTH, HEIGHT);
        lbl_payment.frame = CGRectMake(0,2, self.view.frame.size.width,30);
        img_bg.frame = CGRectMake(2, CGRectGetMaxY(lbl_payment.frame)+2, WIDTH-2, 330);
        lbl_payaccount.frame = CGRectMake(20,-20, WIDTH-40,100);
        lbl_payemail.frame = CGRectMake(30,lbl_payaccount.frame.origin.y+lbl_payaccount.frame.size.height-15, self.view.frame.size.width-80,40);
        txt_Email.frame=CGRectMake(30,CGRectGetMaxY(lbl_payemail.frame)-5, 250, 38);
        line.frame=CGRectMake(30, CGRectGetMaxY(txt_Email.frame)-7, self.view.frame.size.width-90, 0.5);
        lbl_option.frame=CGRectMake(self.view.frame.size.width-130,CGRectGetMaxY(line.frame)+3, 200,13);
        line1.frame=CGRectMake(self.view.frame.size.width-130, CGRectGetMaxY(lbl_option.frame)+1, 90, 0.5);
        lbl_paymentfrequency.frame=CGRectMake(30,CGRectGetMaxY(line1.frame)+3, self.view.frame.size.width-50,80);
        lbl_paymentrecieve.frame=CGRectMake(30,CGRectGetMaxY(lbl_paymentfrequency.frame)-25, self.view.frame.size.width-40,40);
        back_img_for_month.frame =  CGRectMake(25, CGRectGetMaxY(lbl_paymentrecieve.frame)+10, WIDTH-50, 50);
        lbl_end_of_month.frame = CGRectMake(20,-2, 200, 45);
        btn_Endofmonth.frame=CGRectMake(0, 0, 200, 45);
        img_line_on_bg .frame = CGRectMake(CGRectGetMaxX(lbl_end_of_month.frame)-80, 5, 0.5, 30);
        lbl_middle_of_month.frame = CGRectMake(CGRectGetMaxX(img_line_on_bg.frame)+20,0, 200, 30);
        lbl_end_of_month1 .frame = CGRectMake(CGRectGetMaxX(img_line_on_bg.frame)+20,15, 200, 30);
        btn_Middleofmonth.frame=CGRectMake(CGRectGetMaxX(img_line_on_bg.frame)+1,0, 200, 45);
        
        img_bg3.frame =  CGRectMake(0,CGRectGetMaxY(img_bg.frame)+1, WIDTH,150);
        txtview_terms_and_conditions.frame=CGRectMake(5,6, 310, 95);
        lbl_terms_and_conditions .frame =CGRectMake(0,37, 200, 40);
        btn_Termsandconditions.frame=CGRectMake(0, 0, 200, 40);
        lbl_page .frame = CGRectMake(WIDTH/2-160,CGRectGetMaxY(txtview_terms_and_conditions.frame)+10, WIDTH,30);
        lbl_apply .frame = CGRectMake(18,CGRectGetMaxY(lbl_page.frame)+5, WIDTH-36, 50);
        btn_apply.frame = CGRectMake(18,CGRectGetMaxY(lbl_page.frame)+5, WIDTH-36, 50);
        lbl_payment.font = [UIFont fontWithName:kFontBold size:15];
        lbl_payaccount.font = [UIFont fontWithName:kFont size:11.5];
        lbl_payemail.font = [UIFont fontWithName:kFontBold size:12.5];
        lbl_option.font = [UIFont fontWithName:kFont size:11];
        lbl_paymentfrequency.font = [UIFont fontWithName:kFontBold size:12];
        lbl_paymentrecieve.font = [UIFont fontWithName:kFont size:11];
        lbl_end_of_month.font = [UIFont fontWithName:kFontBold size:11];
        lbl_middle_of_month.font = [UIFont fontWithName:kFont size:11];
        lbl_end_of_month1 .font = [UIFont fontWithName:kFont size:11];
        lbl_page .font = [UIFont fontWithName:kFont size:13];
        lbl_apply .font = [UIFont fontWithName:kFont size:18];
        lbl_terms_and_conditions .font = [UIFont fontWithName:kFont size:13];
     //   [scrollview6 setContentSize:CGSizeMake(0,CGRectGetMaxY(img_bg.frame)+170)];
        
        
    }
    if (IS_IPHONE_6Plus)
    {
        [scrollview6 setContentSize:CGSizeMake(0,700)];
    }
    else if (IS_IPHONE_6)
    {
        [scrollview6 setContentSize:CGSizeMake(0,700)];
    }
    else
    {
        [scrollview6 setContentSize:CGSizeMake(0,650)];
    }
    
    
    
    
}



-(void)btn_apply_click
{
    //    ChefHomeViewController *chefhome=[[ChefHomeViewController alloc]init];
    //    [self.navigationController pushViewController:chefhome animated:NO];
    //    [self presentViewController:chefhome animated:NO completion:nil];
    NSLog(@"click_Loginbtn");
    
    if (txt_Email.text.length==0)
    {
        [self popup_Alertview:@"Please Enter Email Id"];
    }
    else if(txt_Email.text.length>0 && ![self ValidateEmail:txt_Email.text])
    {
        txt_Email.text=@"";
        [self popup_Alertview:@"Please Enter a Valid Email"];
        txt_Email.text = @"";
    }
    else{
        [self SignupSixth];
        
    }
}



-(void)click_Endofmonth:(UIButton *)sender
{
    lbl_middle_of_month.font=[UIFont fontWithName:kFont size:10];
    lbl_end_of_month1.font=[UIFont fontWithName:kFont size:10];
    
    if([sender isSelected])
    {
        [sender setSelected:NO];
        str_payInmonth=@"1";
        lbl_end_of_month.font = [UIFont fontWithName:kFontBold size:12];
    }
    else
    {
        
        [sender setSelected:YES];
        lbl_end_of_month.font = [UIFont fontWithName:kFont size:12];
    }
    
}
-(void)click_Middleofmonth:(UIButton *)sender
{
    lbl_end_of_month.font = [UIFont fontWithName:kFont size:12];
    
    if([sender isSelected])
    {
        [sender setSelected:NO];
        str_payInmonth=@"2";
        lbl_middle_of_month.font=[UIFont fontWithName:kFontBold size:10];
        lbl_end_of_month1.font=[UIFont fontWithName:kFontBold size:10];
    }
    else
    {
        
        [sender setSelected:YES];
        lbl_middle_of_month.font=[UIFont fontWithName:kFont size:10];
        lbl_end_of_month1.font=[UIFont fontWithName:kFont size:10];
    }
    
}




//-(void)click_selectMonthAt:(UIButton *)sender
//{
//    ABC=sender.tag;
//
//
//
//    if([ary_Month containsObject:[NSString stringWithFormat:@"%d",ABC]])
//    {
//        [ary_Month removeObject:[NSString stringWithFormat:@"%d",ABC]];
//
//        if (sender.tag==1)
//        {
//            lbl_end_of_month.font = [UIFont fontWithName:kFont size:10];
//
//        }
//        else if (sender.tag==2)
//        {
//            lbl_middle_of_month.font=[UIFont fontWithName:kFont size:10];
//            lbl_end_of_month1.font=[UIFont fontWithName:kFont size:10];
//
//        }
//
//    }
//    else
//    {
//        [ary_Month addObject:[NSString stringWithFormat:@"%d",ABC]];
//        if (sender.tag==1)
//        {
//            lbl_end_of_month.font = [UIFont fontWithName:kFontBold size:10];
//
//        }
//        else if (sender.tag==2)
//        {
//            lbl_middle_of_month.font=[UIFont fontWithName:kFontBold size:10];
//            lbl_end_of_month1.font=[UIFont fontWithName:kFontBold size:10];
//
//        }
//
//    }
//
//    str_payInmonth = [[NSMutableString alloc] init];
//    for (int i=0; i<[ary_Month count];i++)
//    {
//        if (i==[ary_Month count]-1)
//        {
//            [str_payInmonth  appendString:[NSString stringWithFormat:@"%@",[ary_Month objectAtIndex:i]]];
//        }
//        else
//        {
//            [str_payInmonth  appendString:[NSString stringWithFormat:@"%@||",[ary_Month objectAtIndex:i]]];
//        }
//    }
//
//    NSLog(@"str_LearnCook :%@",str_payInmonth);
//    NSLog(@"ary_temp :%@",ary_Month);
//
//
//}
-(void)click_Termsandconditions:(UIButton *)sender
{
    
    AboutUS*vc = [AboutUS new];
    vc.str_Title = @"Terms & Conditions";
    vc.str_URL = @"ws/pages-contents.php?nid=67";
    [self presentViewController:vc animated:NO completion:nil];
    
    //    [self.navigationController pushViewController:vc animated:NO];
    
    
    //    [self SignupTermsandConditions];
}





#pragma mark Validate Email

-(BOOL)ValidateEmail :(NSString *) strToEvaluate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL b = [emailTest evaluateWithObject:strToEvaluate];
    return b;
}

- (NSMutableArray *)validateEmailWithString:(NSString*)emails
{
    NSMutableArray *validEmails = [[NSMutableArray alloc] init];
    NSArray *emailArray = [emails componentsSeparatedByString:@","];
    for (NSString *email in emailArray)
    {
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        if ([emailTest evaluateWithObject:email])
            [validEmails addObject:email];
    }
    return validEmails;
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}
-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    if ([str_success isEqualToString:@"SUCESS"])
    {
        LoginViewController *chefhome=[[LoginViewController alloc]init];
        [self presentViewController:chefhome animated:NO completion:nil];
        
        
    }
    [alertviewBg removeFromSuperview];
}
# pragma mark ChefSignupFirst method



-(void)SignupSixth
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
   // [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"]
    NSDictionary *params =@{
                            @"field_paypal_email_address"        :   txt_Email.text,
                            @"uid"                               :    [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            
                            @"recieve_my_payment_from"           :   str_payInmonth,
                            @"registration_page"                 : @"6",
                            @"role_type"                         :  @"chef_profile",
                            @"device_udid"                       :  UniqueAppID,
                            @"device_token"                      :  @"Dev",
                            @"device_type"                       :  @"1"
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSignUpFirst
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpSixth:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self SignupSixth];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpSixth :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        if (![[[[TheDict valueForKey:@"profile_info"] valueForKey:@"notes"] objectAtIndex:0] isKindOfClass:[NSNull class]]||([[[TheDict valueForKey:@"profile_info"] valueForKey:@"notes"] objectAtIndex:0] != (NSArray*) [NSNull null]))
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[TheDict valueForKey:@"profile_info"]  forKey:@"UserInfo"];
            [defaults synchronize];
        }else
        {
            
           
        }
        str_success = @"SUCESS";
        
        [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
}



-(void)SignupTermsandConditions
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    //    NSDictionary *params =@{
    //                            @"role_type"                         :  @"chef_profile",
    //                            @"device_udid"                       :  UniqueAppID,
    //                            @"device_token"                      :  @"Dev",
    //                            @"device_type"                       :  @"1"
    //
    //                            };
    //
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kChefTermsandConditions
                                                      parameters:nil];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseTermsandConditions:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self SignupTermsandConditions];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseTermsandConditions :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
