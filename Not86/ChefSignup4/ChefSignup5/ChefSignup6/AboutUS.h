//
//  AboutUS.h
//  ConnexTaxi
//
//  Created by User on 26/08/15.
//  Copyright (c) 2015 abc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JWSlideMenuViewController.h"

@interface AboutUS : JWSlideMenuViewController

@property(nonatomic,assign)BOOL isPolicy;
@property(nonatomic,assign)BOOL isAboutUs;
@property(nonatomic,assign)BOOL isTermsCondi;
@property(nonatomic,strong)NSString *str_Title;
@property (strong,nonatomic)NSString *str_URL;
@property(nonatomic,strong)NSString *str_comefrom;


@end
