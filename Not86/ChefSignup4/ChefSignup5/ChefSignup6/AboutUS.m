//
//  AboutUS.m
//  ConnexTaxi
//
//  Created by User on 26/08/15.
//  Copyright (c) 2015 abc. All rights reserved.
//

#import "AboutUS.h"
#import "Define.h"
#import "AppDelegate.h"

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"

@interface AboutUS ()<UIWebViewDelegate,UITextViewDelegate>
{
    UIImageView *img_BackGround;
    UILabel*lbl_title;
    UILabel*lbl_time;
    
    AppDelegate *delegate;
    UIImageView *imageview_TopBar;
    
    UIView *view_WebView;
    UIWebView *webView_Detail;
    
    NSMutableArray *ary_aboutus;
    UIButton  *btn_BackHeader;
    
    
}

@end

@implementation AboutUS
@synthesize isPolicy,isAboutUs,isTermsCondi,str_Title,str_URL;

- (void)viewDidLoad {
    [super viewDidLoad];
    // self.view.userInteractionEnabled=YES;
    delegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    isAboutUs = YES;
    UIImageView *img_topbar;
    
    img_topbar=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,45)];
    [img_topbar setUserInteractionEnabled:YES];
    img_topbar.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:img_topbar];
    
    
    UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(12, 16, 15, 15)];
    [img_back setUserInteractionEnabled:YES];
    img_back.backgroundColor=[UIColor clearColor];
    img_back.image=[UIImage imageNamed:@"arrow@2x.png"];
    [img_topbar addSubview:img_back];
    
    UILabel  * lbl_heading = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(img_back.frame)+20,0, 220,45)];
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.text = str_Title;
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:kFont size:15];
    [img_topbar addSubview:lbl_heading];
    
    UIButton *btn_back = [[UIButton alloc] init];
    btn_back.frame = CGRectMake(0, 0, 60,35);
    btn_back.backgroundColor = [UIColor clearColor];
    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_topbar addSubview:btn_back];
    
    
    
    //
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(NavigateHome:)
    //                                                 name:@"ShowHomeScreenMsg"
    //                                               object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(NavigateToFeedback:)
    //                                                 name:@"NavigateToFeedback"
    //                                               object:nil];
    //
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(NavigateToHome:)
    //                                                 name:@"NavigateToHome"
    //                                               object:nil];
    //
    
    
}
-(void)Back_btnClick
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)showTime
{
    NSDate *currDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"hh:mm"];
    [dateFormatter setAMSymbol:@"am"];
    [dateFormatter setPMSymbol:@"pm"];
    [dateFormatter setTimeStyle: NSDateFormatterShortStyle];
    NSString*dateString;
    
    dateString = [dateFormatter stringFromDate:currDate];
    lbl_time.text=dateString;
    
    
}
-(void)click_BtnBack:(UIButton *)sender
{
    [self.navigationController popViewController];
}
-(void)viewDidDisappear:(BOOL)animated
{
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    [self integratedWebView];
}

-(void)integratedWebView
{
    
    
    img_BackGround=[[UIImageView alloc]init];
    img_BackGround.frame=CGRectMake(0, 50, WIDTH+10, HEIGHT);
    [img_BackGround setUserInteractionEnabled:YES];
    img_BackGround.backgroundColor=[UIColor clearColor];
    img_BackGround.image=[UIImage imageNamed:@"img.bg@2x.png"];
    img_BackGround.userInteractionEnabled = YES;
    [self.view addSubview:img_BackGround];
    
    view_WebView=[[UIView alloc]init];
    view_WebView.frame = CGRectMake(0,0,WIDTH,HEIGHT-(HEADER_HEIGHT+4));
    view_WebView.backgroundColor = [UIColor clearColor];
    [img_BackGround addSubview:view_WebView];
    
    webView_Detail=[[UIWebView alloc] init];
    webView_Detail.frame = CGRectMake(2,0,WIDTH-4,HEIGHT-(HEADER_HEIGHT+4));
    [webView_Detail setBackgroundColor:[UIColor clearColor]];
    webView_Detail.delegate = self;
    webView_Detail.userInteractionEnabled=TRUE;
    //    webview_Information.scrollView.zoomScale = 4.0;
    //  [webview_Information setScalesPageToFit:YES];
    [webView_Detail setOpaque:NO];
    webView_Detail.backgroundColor = [UIColor clearColor];
    webView_Detail.contentMode = UIViewContentModeScaleAspectFit;
    NSMutableString *html;
    
    html = [NSMutableString stringWithString:[NSString stringWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"http://sgappstore.com/not86/",str_URL]] encoding:NSUTF8StringEncoding error:nil]];
    NSMutableString *newhtml=[NSMutableString stringWithFormat:@"<body style=\"font-family:%@;color:clear;font-size:12;margin:10px;padding:10px;\">%@",kFont,html];
    [html appendString:@"</body></html>"];
    [webView_Detail loadHTMLString:[newhtml description] baseURL:nil];
    [webView_Detail setOpaque:NO];
    
    
    [view_WebView addSubview:webView_Detail];
    
    
    
    
    
    
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    
    
}
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    return NO;
    
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [delegate.activityIndicator stopAnimating];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
