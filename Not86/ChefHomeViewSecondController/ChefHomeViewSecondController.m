//
//  ChefHomeViewSecondController.m
//  Not86
//
//  Created by Interwld on 8/14/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ChefHomeViewSecondController.h"

@interface ChefHomeViewSecondController ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIImageView *img_topbar;
    UIImageView *img_BackgroundImg;
    UITableView *img_table;
    UIScrollView *scroll;
    
}


@end

@implementation ChefHomeViewSecondController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self IntegrateBodyDesign];
    [self IntegrateHeaderDesign];

    // Do any additional setup after loading the view.
}
-(void)IntegrateHeaderDesign{
    
    
    img_topbar=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,60)];
    [img_topbar setUserInteractionEnabled:YES];
    img_topbar.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:img_topbar];
    
    
    UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(10, 16, 33, 25)];
    [img_back setUserInteractionEnabled:YES];
    img_back.backgroundColor=[UIColor clearColor];
    img_back.image=[UIImage imageNamed:@"icon-menu.png"];
    [img_topbar addSubview:img_back];
    UILabel  * lbl_heading = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(img_back.frame)+20,0, 220,60)];
    lbl_heading.text = @"Home";
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:@"Arial" size:26];
    [img_topbar addSubview:lbl_heading];
    
    UIImageView *imgback=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-90, 13, 25, 28)];
    [imgback setUserInteractionEnabled:YES];
    imgback.backgroundColor=[UIColor clearColor];
    imgback.image=[UIImage imageNamed:@"icon-3.png"];
    [img_topbar addSubview:imgback];
    
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-50, 10, 40, 40)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-logo@2x.png"];
    [img_topbar addSubview:img_logo];
    
}
-(void)IntegrateBodyDesign{
    
    UIImageView *img_Background=[[UIImageView alloc]initWithFrame:CGRectMake(0, 60, HEIGHT, 160)];
    [img_Background setUserInteractionEnabled:YES];
    img_Background.backgroundColor=[UIColor clearColor];
    img_Background.image=[UIImage imageNamed:@"bg.png"];
    [self.view addSubview:img_Background];
    
    
    
    
    UIImageView *img_savenow=[[UIImageView alloc]initWithFrame:CGRectMake(25, 20, 160, 70)];
    [img_savenow setUserInteractionEnabled:YES];
    img_savenow.backgroundColor=[UIColor clearColor];
    img_savenow.image=[UIImage imageNamed:@"ServingNow_Till.png"];
    [img_Background addSubview:img_savenow];
    
    UIButton *btn_savenow= [[UIButton alloc] init];
    btn_savenow.frame = CGRectMake(25, 20, 160, 70);
    btn_savenow.backgroundColor = [UIColor clearColor];
    //    [btn_Menu setTitle:@"MENU" forState:UIControlStateNormal];
    
    //    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_Background addSubview:btn_savenow];
    
    

    
    
//    UILabel  * lblheading = [[UILabel alloc]initWithFrame:CGRectMake(40,50, 100,15)];
//    lblheading.text = @"Till:12:15:PM";
//    lblheading.backgroundColor=[UIColor clearColor];
//    lblheading.textColor=[UIColor whiteColor];
//    lblheading.textAlignment=NSTextAlignmentLeft;
//    lblheading.font = [UIFont fontWithName:@"Arial" size:13];
//    [img_savenow addSubview:lblheading];
//    
    
    
    UIImageView *img_savelater=[[UIImageView alloc]initWithFrame:CGRectMake(195, 20, 160, 70)];
    [img_savelater setUserInteractionEnabled:YES];
    img_savelater.backgroundColor=[UIColor clearColor];
    img_savelater.image=[UIImage imageNamed:@"img-save-later.png"];
    [img_Background addSubview:img_savelater];
    
    UIButton *btn_savelater = [[UIButton alloc]init];
    btn_savelater.frame = CGRectMake(195, 20, 160, 70);
    btn_savelater.backgroundColor = [UIColor clearColor];
    //    [btn_Menu setTitle:@"MENU" forState:UIControlStateNormal];
    
    //    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_Background addSubview:btn_savelater];
    

    
    UILabel  * lbl_heading = [[UILabel alloc]initWithFrame:CGRectMake(35,CGRectGetMaxY(img_savenow.frame)+20, 60,35)];
    lbl_heading.text = @"MENU";
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:@"Arial" size:18];
    [img_Background addSubview:lbl_heading];
    
    
    
    UIButton *btn_Menu= [[UIButton alloc] init];
    btn_Menu.frame = CGRectMake(35, CGRectGetMaxY(img_savenow.frame)+20, 60,35);
    btn_Menu.backgroundColor = [UIColor clearColor];
    //    [btn_Menu setTitle:@"MENU" forState:UIControlStateNormal];
    
    //    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_Background addSubview:btn_Menu];
    
    
    
    UIImageView *line_IMG=[[UIImageView alloc]init];
    line_IMG.frame=CGRectMake(110, CGRectGetMaxY(img_savenow.frame)+17, 2, 45);
    [line_IMG setUserInteractionEnabled:YES];
    line_IMG.backgroundColor=[UIColor grayColor];
    line_IMG.image=[UIImage imageNamed:@"line_image@2.png"];
    [img_Background addSubview:line_IMG];
    
    
    
    
    UILabel  * lbl_Schedule = [[UILabel alloc]initWithFrame:CGRectMake(135,CGRectGetMaxY(img_savenow.frame)+20, 100,35)];
    lbl_Schedule.text = @"SCHEDULE";
    lbl_Schedule.backgroundColor=[UIColor clearColor];
    lbl_Schedule.textColor=[UIColor whiteColor];
    lbl_Schedule.textAlignment=NSTextAlignmentLeft;
    lbl_Schedule.font = [UIFont fontWithName:@"Arial" size:18];
    [img_Background addSubview:lbl_Schedule];
    
    
    
    UIButton *btn_Schedule= [[UIButton alloc] init];
    btn_Schedule.frame = CGRectMake(135, CGRectGetMaxY(img_savenow.frame)+20, 100,35);
    btn_Schedule.backgroundColor = [UIColor clearColor];
    //    [btn_Menu setTitle:@"MENU" forState:UIControlStateNormal];
    
    //    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_Background addSubview:btn_Schedule];
    
    UIImageView *line_IMGE=[[UIImageView alloc]init];
    line_IMGE.frame=CGRectMake(250, CGRectGetMaxY(img_savenow.frame)+17, 2, 45);
    [line_IMGE setUserInteractionEnabled:YES];
    line_IMGE.backgroundColor=[UIColor grayColor];
    line_IMGE.image=[UIImage imageNamed:@"line_image@2.png"];
    [img_Background addSubview:line_IMGE];
    
    
    
    UILabel  * lbl_Orders = [[UILabel alloc]initWithFrame:CGRectMake(265,CGRectGetMaxY(img_savenow.frame)+20, 100,35)];
    lbl_Orders.text = @"ORDERS";
    lbl_Orders.backgroundColor=[UIColor clearColor];
    lbl_Orders.textColor=[UIColor whiteColor];
    lbl_Orders.textAlignment=NSTextAlignmentLeft;
    lbl_Orders.font = [UIFont fontWithName:@"Arial" size:18];
    [img_Background addSubview:lbl_Orders];
    
    
    
    UIButton *btn_Orders= [[UIButton alloc] init];
    btn_Orders.frame = CGRectMake(265, CGRectGetMaxY(img_savenow.frame)+20, 100,35);
    btn_Orders.backgroundColor = [UIColor clearColor];
    //    [btn_Menu setTitle:@"MENU" forState:UIControlStateNormal];
    
    //    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_Background addSubview:btn_Orders];
    
    
    
    
    UIImageView *line_Emptyimage=[[UIImageView alloc]init];
    line_Emptyimage.frame=CGRectMake(0, CGRectGetMaxY(img_Background.frame)+5, WIDTH-5, 150);
    [line_Emptyimage setUserInteractionEnabled:YES];
    line_Emptyimage.backgroundColor=[UIColor clearColor];
    line_Emptyimage.image=[UIImage imageNamed:@"bg1.png"];
   [self.view addSubview:line_Emptyimage];
    
    
    UILabel  * lbl_Quality = [[UILabel alloc]initWithFrame:CGRectMake(0,0, WIDTH,35)];
    lbl_Quality.text = @"Quantity Alert";
    lbl_Quality.backgroundColor=[UIColor clearColor];
    lbl_Quality.textColor=[UIColor blackColor];
    lbl_Quality.textAlignment=NSTextAlignmentCenter;
    lbl_Quality.font = [UIFont fontWithName:@"Arial" size:20];
    [line_Emptyimage addSubview:lbl_Quality];
    
    UIButton *btn_Quality = [[UIButton alloc] init];
    btn_Quality.frame = CGRectMake(0,0, WIDTH-20,35);
    btn_Quality.backgroundColor = [UIColor clearColor];
    //    [btn_Arrowleft setImage:[UIImage imageNamed:@"arrow_left.png"] forState:UIControlStateNormal];
    [btn_Quality setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    //    [btn_Arrowleft addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [line_Emptyimage addSubview:btn_Quality];
    

    
    UIImageView *Emptyimage=[[UIImageView alloc]init];
    Emptyimage.frame=CGRectMake(330, 10, 10, 10);
    [Emptyimage setUserInteractionEnabled:YES];
    Emptyimage.backgroundColor=[UIColor clearColor];
    Emptyimage.image=[UIImage imageNamed:@"icon-del.png"];
    [lbl_Quality addSubview:Emptyimage];
    
    UIButton *btn_delete = [[UIButton alloc] init];
    btn_delete.frame = CGRectMake(330,10, 20,20);
    btn_delete.backgroundColor = [UIColor clearColor];
    //    [btn_Arrowleft setImage:[UIImage imageNamed:@"arrow_left.png"] forState:UIControlStateNormal];
    [btn_delete setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    //    [btn_Arrowleft addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [lbl_Quality addSubview:btn_delete];
    

    
    UIImageView *line=[[UIImageView alloc]init];
    line.frame=CGRectMake(20, CGRectGetMaxY(lbl_Quality.frame)+1, 320, 0.5);
    [line setUserInteractionEnabled:YES];
    line.backgroundColor=[UIColor clearColor];
    line.image=[UIImage imageNamed:@"line1.png"];
    [line_Emptyimage addSubview:line];
    
    
    UILabel  * lbl_ServingLow = [[UILabel alloc]initWithFrame:CGRectMake(240,CGRectGetMaxY(lbl_Quality.frame)+1, 110,35)];
    lbl_ServingLow.text = @"Serving Low";
    lbl_ServingLow.backgroundColor=[UIColor clearColor];
    lbl_ServingLow.textColor=[UIColor colorWithRed:157/255.0 green:17/255.0 blue:47/255.0 alpha:1];
    
    lbl_ServingLow.textAlignment=NSTextAlignmentCenter;
    lbl_ServingLow.font = [UIFont fontWithName:@"Arial" size:18];
    [line_Emptyimage addSubview:lbl_ServingLow];
    
    
    
    
    scroll = [[UIScrollView alloc]init];
    scroll.frame=CGRectMake(20,CGRectGetMaxY(line.frame)+5, 320, 100);
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor clearColor];
    [line_Emptyimage  addSubview:scroll];
    
    UIImageView *imgView_Photos = [[UIImageView alloc] init];
    imgView_Photos.frame = CGRectMake(20,0, 320, 100);
    [imgView_Photos setBackgroundColor:[UIColor clearColor]];
    [imgView_Photos setContentMode:UIViewContentModeScaleAspectFill];
    [imgView_Photos setClipsToBounds:YES];
    imgView_Photos.userInteractionEnabled = NO;
    [scroll addSubview:imgView_Photos];
    
    
    
    
    UIImageView *Arrow_left=[[UIImageView alloc]init];
    Arrow_left.frame=CGRectMake(4, 80, 15, 20);
    [Arrow_left setUserInteractionEnabled:YES];
    Arrow_left.backgroundColor=[UIColor clearColor];
    Arrow_left.image=[UIImage imageNamed:@"arrow_left.png"];
    [line_Emptyimage addSubview:Arrow_left];
    
    UIButton *btn_Arrowleft = [[UIButton alloc] init];
    btn_Arrowleft.frame = CGRectMake(0, 80, 20, 20);
    btn_Arrowleft.backgroundColor = [UIColor whiteColor];
    //    [btn_Arrowleft setImage:[UIImage imageNamed:@"arrow_left.png"] forState:UIControlStateNormal];
    [btn_Arrowleft setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    //    [btn_Arrowleft addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [imgView_Photos addSubview:btn_Arrowleft];
    
    
    UIImageView *img_Desh=[[UIImageView alloc]init];
    img_Desh.frame=CGRectMake(30, 60, 80, 80);
    [img_Desh setUserInteractionEnabled:YES];
    img_Desh.backgroundColor=[UIColor clearColor];
    img_Desh.image=[UIImage imageNamed:@"img-dish1@2x.png"];
    [line_Emptyimage addSubview:img_Desh];
    
    
    UILabel  * spicy_Fish = [[UILabel alloc]initWithFrame:CGRectMake(115,63, WIDTH-40,43)];
    spicy_Fish.text = @"Spicy Fish Ofah is running low\nfor Delivery.";
    spicy_Fish.backgroundColor=[UIColor clearColor];
    spicy_Fish.textColor=[UIColor blackColor];
    spicy_Fish.textAlignment=NSTextAlignmentLeft;
    spicy_Fish.font = [UIFont fontWithName:@"Arial" size:16];
    [line_Emptyimage addSubview:spicy_Fish];
    spicy_Fish.numberOfLines = 0;
    
    
    UILabel  * lbl_like = [[UILabel alloc]initWithFrame:CGRectMake(115,CGRectGetMaxY(spicy_Fish.frame)+2, WIDTH-40,35)];
    lbl_like.text = @"Would you like to top up";
    lbl_like.backgroundColor=[UIColor clearColor];
    lbl_like.textColor=[UIColor blackColor];
    lbl_like.textAlignment=NSTextAlignmentLeft;
    lbl_like.font = [UIFont fontWithName:@"Arial" size:16];
    [line_Emptyimage addSubview:lbl_like];
    lbl_like.numberOfLines = 0;
    
    
    UIImageView *Arrow_right=[[UIImageView alloc]init];
    Arrow_right.frame=CGRectMake(WIDTH-40, 80, 15, 20);
    [Arrow_right setUserInteractionEnabled:YES];
    Arrow_right.backgroundColor=[UIColor clearColor];
    Arrow_right.image=[UIImage imageNamed:@"arrow_right.png"];
    [line_Emptyimage addSubview:Arrow_right];
    
    
    UIButton *btn_Arrowright = [[UIButton alloc] init];
    btn_Arrowright.frame = CGRectMake(WIDTH-40, 80, 20, 20);
    btn_Arrowright.backgroundColor = [UIColor grayColor];
    //    [btn_Arrowleft setImage:[UIImage imageNamed:@"arrow_left.png"] forState:UIControlStateNormal];
    [btn_Arrowright setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    //    [btn_Arrowleft addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [imgView_Photos addSubview:btn_Arrowright];
    
    
    
    UILabel  * lbl_Todayorders = [[UILabel alloc]initWithFrame:CGRectMake(8,CGRectGetMaxY(line_Emptyimage.frame)+5, 250,35)];
    lbl_Todayorders.text = @"Today's Orders";
    lbl_Todayorders.backgroundColor=[UIColor clearColor];
    lbl_Todayorders.textColor=[UIColor blackColor];
    lbl_Todayorders.textAlignment=NSTextAlignmentLeft;
    lbl_Todayorders.font = [UIFont fontWithName:@"Arial" size:20];
    [self.view addSubview:lbl_Todayorders];
    lbl_Todayorders.numberOfLines = 0;
    
    
    //    UIImageView *lineEmptyimage=[[UIImageView alloc]init];
    //    lineEmptyimage.frame=CGRectMake(0, CGRectGetMaxY(lbl_Todayorders.frame)+5, WIDTH-8, 150);
    //    [lineEmptyimage setUserInteractionEnabled:YES];
    //    lineEmptyimage.backgroundColor=[UIColor clearColor];
    //    lineEmptyimage.image=[UIImage imageNamed:@"bg1.png"];
    //    [self.view addSubview:lineEmptyimage];
    //
    
    
    
#pragma mark Tableview
    
    img_table= [[UITableView alloc] init ];
    img_table.frame  = CGRectMake(5,CGRectGetMaxY(lbl_Todayorders.frame)+5,WIDTH-15,260);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    img_table.backgroundColor = [UIColor clearColor];
    [self.view addSubview:img_table];
    
    
    
    if (IS_IPHONE_6Plus)
    {
        
    }
    else if (IS_IPHONE_6)
    {
        
    }
    else
    {
        
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 210;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    img_cellBackGnd.frame =  CGRectMake(5,0, 360, 200);
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg1.png"]];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [cell.contentView addSubview:img_cellBackGnd];
    
    
    UILabel  * lbl_order = [[UILabel alloc]initWithFrame:CGRectMake(7,5, 105,35)];
    lbl_order.text = @"Order no.:";
    lbl_order.backgroundColor=[UIColor clearColor];
    lbl_order.textColor=[UIColor blackColor];
    lbl_order.textAlignment=NSTextAlignmentLeft;
    lbl_order.font = [UIFont fontWithName:@"Arial" size:18];
    [img_cellBackGnd addSubview:lbl_order];
    lbl_order.numberOfLines = 0;
    
    
    UILabel  * lbl_number = [[UILabel alloc]initWithFrame:CGRectMake(82,5, 85,35)];
    lbl_number.text = @"10847";
    
    lbl_number.backgroundColor=[UIColor clearColor];
    lbl_number.textColor=[UIColor colorWithRed:157/255.0 green:17/255.0 blue:47/255.0 alpha:1];
    lbl_number.textAlignment=NSTextAlignmentLeft;
    lbl_number.font = [UIFont fontWithName:@"Arial" size:18];
    [img_cellBackGnd addSubview:lbl_number];
    lbl_number.numberOfLines = 0;
    
    
    
    
    UILabel  * lbl_Duein = [[UILabel alloc]initWithFrame:CGRectMake(230,5, 70,35)];
    lbl_Duein.text = @"Due in";
    lbl_Duein.backgroundColor=[UIColor clearColor];
    lbl_Duein.textColor=[UIColor blackColor];
    lbl_Duein.textAlignment=NSTextAlignmentLeft;
    lbl_Duein.font = [UIFont fontWithName:@"Arial" size:18];
    [img_cellBackGnd addSubview:lbl_Duein];
    lbl_Duein.numberOfLines = 0;
    
    
    UILabel  * lblnumber = [[UILabel alloc]initWithFrame:CGRectMake(290,5, 70,35)];
    lblnumber.text = @"5 mins";
    lblnumber.backgroundColor=[UIColor clearColor];
    lblnumber.textColor=[UIColor colorWithRed:157/255.0 green:17/255.0 blue:47/255.0 alpha:1];
    lblnumber.textAlignment=NSTextAlignmentLeft;
    lblnumber.font = [UIFont fontWithName:@"Arial" size:18];
    [img_cellBackGnd addSubview:lblnumber];
    lblnumber.numberOfLines = 0;
    
    
    UILabel  * lbl_value = [[UILabel alloc]initWithFrame:CGRectMake(30,CGRectGetMaxY(lbl_order.frame)+3, 150,35)];
    lbl_value.text = @"Value:$50.80";
    lbl_value.backgroundColor=[UIColor clearColor];
    lbl_value.textColor=[UIColor blackColor];
    lbl_value.textAlignment=NSTextAlignmentLeft;
    lbl_value.font = [UIFont fontWithName:@"Arial" size:18];
    [img_cellBackGnd addSubview:lbl_value];
    lbl_value.numberOfLines = 0;
    
    UILabel  * lbl_Recieved= [[UILabel alloc]initWithFrame:CGRectMake(20,CGRectGetMaxY(lbl_value.frame)+3, 150,35)];
    lbl_Recieved.text = @"Received:";
    lbl_Recieved.backgroundColor=[UIColor clearColor];
    lbl_Recieved.textColor=[UIColor blackColor];
    lbl_Recieved.textAlignment=NSTextAlignmentLeft;
    lbl_Recieved.font = [UIFont fontWithName:@"Arial" size:18];
    [img_cellBackGnd addSubview:lbl_Recieved];
    lbl_Recieved.numberOfLines = 0;
    
    UILabel  * lbl_datetime= [[UILabel alloc]initWithFrame:CGRectMake(98,CGRectGetMaxY(lbl_value.frame)+3, 180,35)];
    lbl_datetime.text = @"16/07/2015, 4;00;pm";
    lbl_datetime.backgroundColor=[UIColor clearColor];
    lbl_datetime.textColor=[UIColor blackColor];
    lbl_datetime.textAlignment=NSTextAlignmentLeft;
    lbl_datetime.font = [UIFont fontWithName:@"Arial" size:18];
    [img_cellBackGnd addSubview:lbl_datetime];
    lbl_datetime.numberOfLines = 0;
    
    
    UIImageView *imr_icon = [[UIImageView alloc]init];
    imr_icon.frame =  CGRectMake(280,CGRectGetMaxY(lbl_value.frame)+3, 40, 30);
    [imr_icon setImage:[UIImage imageNamed:@"icon-6.png"]];
    [imr_icon setUserInteractionEnabled:YES];
    [img_cellBackGnd addSubview:imr_icon];
    
    
    UIImageView *line1=[[UIImageView alloc]init];
    line1.frame=CGRectMake(5, CGRectGetMaxY(imr_icon.frame)+1, 340, 0.5);
    [line1 setUserInteractionEnabled:YES];
    line1.backgroundColor=[UIColor clearColor];
    line1.image=[UIImage imageNamed:@"line1.png"];
    [img_cellBackGnd addSubview:line1];
    
    UIImageView *img_profile=[[UIImageView alloc]init];
    img_profile.frame=CGRectMake(5, CGRectGetMaxY(line1.frame)+2, 70, 70);
    [img_profile setUserInteractionEnabled:YES];
    img_profile.backgroundColor=[UIColor clearColor];
    img_profile.image=[UIImage imageNamed:@"profile-img.png"];
    [img_cellBackGnd addSubview:img_profile];
    
    UILabel  * lbl_Name= [[UILabel alloc]initWithFrame:CGRectMake(90,CGRectGetMaxY(line1.frame)+7, 150,35)];
    lbl_Name.text = @"Jane Doe";
    lbl_Name.backgroundColor=[UIColor clearColor];
    lbl_Name.textColor=[UIColor blackColor];
    lbl_Name.textAlignment=NSTextAlignmentLeft;
    lbl_Name.font = [UIFont fontWithName:@"Arial" size:16];
    [img_cellBackGnd addSubview:lbl_Name];
    lbl_Name.numberOfLines = 0;
    
    UIImageView *img_msg=[[UIImageView alloc]init];
    img_msg.frame=CGRectMake(280, CGRectGetMaxY(line1.frame)+5, 40, 25);
    [img_msg setUserInteractionEnabled:YES];
    img_msg.backgroundColor=[UIColor clearColor];
    img_msg.image=[UIImage imageNamed:@"icon-2.png"];
    [img_cellBackGnd addSubview:img_msg];
    
    
    
    
    
    
    
    return cell;
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
