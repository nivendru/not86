//
//  ChefServeLaterVC.m
//  Not86
//
//  Created by Interwld on 8/22/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ChefServeLaterVC.h"
#import "CKCalendarView.h"
#import "ChefServeLaterSecondVC.h"
#import "AppDelegate.h"
#import "ChefHomeViewController.h"

@interface ChefServeLaterVC ()<CKCalendarDelegate, UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate>
{
    UIImageView *imgViewTop;
    UIButton *btn_Schedule;
    UIButton *btn_OnRequest;
    
    NSArray  *arryFoodnames;
    NSArray  *arryFoodImages;
    
    UITableView *table_schedule;
    UITableView  *tableViewDishes;
    
    UIButton *btn_AddSchedule;
    
    UIScrollView *mainScrollView;
    UIView *onRequest_View;
    
    UIButton *next;
    UIButton *btn_Agree;
    UIButton *btn_Save;
    
    NSDate * date_Selectedfromtime;
    NSDate*date_Selectedtotime;
    int timetype;
    AppDelegate *delegate;
    NSMutableArray*ary_serverlaterlist;
    NSMutableArray*ary_servelaterfilter;
    
    UIView*alertviewBg;
    UIImageView *img_Dish;
    UIButton *btn_Search;
    UITextField *textfield_SearchDish;
    UIImageView *imgViewSerchdish;
    NSMutableArray*Arr_Temp;
    int selectedindex;
    NSIndexPath*indexSelected;
    UIImageView*img_swapImg;
    
    
    int search_leg;
    
    NSMutableArray*ary_servingtimes;
    NSMutableString*str_deletenotification;
    
    UIToolbar*keyboardToolbar_totime;
    NSDateFormatter*formattertotime;
    UIDatePicker*datePickerto;
    
    UIToolbar*keyboardToolbar_fromtime;
    NSDateFormatter*formatterfromtime;
    UIDatePicker*datePickerfrom;
    
    NSDate * date_SelectedDateFrom;
    NSDate * date_SelectedDateTo;
    UITextField*txt_amount;
    NSString*str_amountenter;
    NSMutableArray*ary_actualarrayfromWS;
    
    NSString*str_servingtimeselected;
    
    
    
}

@end

@implementation ChefServeLaterVC
@synthesize calendar,enabledDates;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    ary_actualarrayfromWS = [NSMutableArray new];
    
    ary_serverlaterlist= [NSMutableArray new];
    ary_servelaterfilter= [NSMutableArray new];
    str_deletenotification = [NSMutableString new];
    str_servingtimeselected= [NSMutableString new];
    str_amountenter= [NSString new];
    str_amountenter = @"0";
    str_servingtimeselected = @"NO";
    
    
    Arr_Temp = [NSMutableArray new];
    //    arryFoodImages = [[NSArray alloc]initWithObjects:@"chickedn.png",@"fish.png",@"fish.png",@"chickedn.png",@"chickedn.png", nil];
    //
    selectedindex = -1;
    indexSelected = nil;
    
    ary_servingtimes=[[NSMutableArray alloc]init];
    
    datePickerfrom = [[UIDatePicker alloc] init];
    datePickerfrom.datePickerMode = UIDatePickerModeTime;
    datePickerfrom.tag = 2;
    [datePickerfrom addTarget:self action:@selector(datePickerFromValueChanged) forControlEvents:UIControlEventValueChanged];
    
    
    NSDate *now = [NSDate date];
    NSCalendar *calendar1 = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar1 components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now];
    components.year = components.year  ;
    
    datePickerfrom.minimumDate = now;
    
    datePickerfrom.maximumDate = [formattertotime dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components.year+20, (long)components.month,(long)components.day]];
    
    
    
    datePickerto = [[UIDatePicker alloc] init];
    datePickerto.datePickerMode = UIDatePickerModeTime;
    datePickerto.tag=3;
    [datePickerto addTarget:self action:@selector(datePickerToValueChanged) forControlEvents:UIControlEventValueChanged];
    
    
    
    NSDate *now1 = [NSDate date];
    NSCalendar *calendar2 = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components1 = [calendar2 components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now1];
    components1.year = components1.year  ;
    
    datePickerto.minimumDate = now1;
    
    datePickerto.maximumDate = [formattertotime dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components1.year+20, (long)components1.month,(long)components1.day]];
    
    
    
    if (keyboardToolbar_fromtime == nil)
    {
        keyboardToolbar_fromtime = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar_fromtime setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_DoneDate:)];
        [keyboardToolbar_fromtime setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    date_SelectedDateFrom = datePickerfrom.date;
    date_SelectedDateTo = datePickerto.date;
    
    
    
    //     NSMutableDictionary *dict1=[[NSMutableDictionary alloc] init];
    //    [dict1 setValue:@""  forKey:@"starttime"];
    //    [dict1 setValue:@""  forKey:@"endtime"];
    //    [ary_servingtimes addObject:dict1];
    
    
    
    [self IntegrateHeaderDesign];
    [self IntegrateBodyDesign];
    
}
-(void)viewWillAppear:(BOOL)animated{
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self AFServelaterlist];
    
}



-(void)click_DoneDate:(id) sender
{
    if(timetype==1)
    {
        [self datePickerFromValueChanged];
    }
    else if(timetype==2)
    {
        [self datePickerToValueChanged];
    }
    else
    {
        
    }
    [self.view endEditing:YES];
}

-(void)datePickerFromValueChanged
{
    formatterfromtime = [[NSDateFormatter alloc] init];
    [formatterfromtime setDateFormat:@"hh:mm aa"];
    date_SelectedDateFrom = datePickerfrom.date;
    
    //    for (int i=0; i<[ary_servingtimes count]; i++) {
    //
    //        if (i == -2) {
    //
    //            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    //
    //            dict=[ary_servingtimes objectAtIndex:i];
    //
    //            formatterfromtime = [[NSDateFormatter alloc] init];
    //            [formatterfromtime setDateFormat:@"hh:mm aa"];
    //
    //            //                    NSDate * nowDate = [NSDate date];
    //            //                    NSDate * picDate = datePickerfrom.date;
    //            date_SelectedDateFrom = datePickerfrom.date;
    //            [dict setObject:[formatterfromtime stringFromDate:datePickerfrom.date] forKey:@"starttime"];
    //
    //            [table_schedule reloadData];
    //        }
    //
    //    }
    
    
}

-(void)datePickerToValueChanged
{
    formattertotime = [[NSDateFormatter alloc] init];
    [formattertotime setDateFormat:@"hh:mm aa"];
    date_SelectedDateTo = datePickerto.date;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)IntegrateHeaderDesign
{
    
    imgViewTop=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,45)];
    [imgViewTop setUserInteractionEnabled:YES];
    imgViewTop.image=[UIImage imageNamed:@"img-head.png"];
    [self.view addSubview:imgViewTop];
    //
    //        UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(10, 18, 25, 25)];
    //        [img_back setUserInteractionEnabled:YES];
    //        img_back.backgroundColor=[UIColor clearColor];
    //        img_back.image=[UIImage imageNamed:@"arrow.png"];
    //        [imgViewTop addSubview:img_back];
    //
    
    
    UIButton *btn_back = [[UIButton alloc] init];
    btn_back.frame = CGRectMake(15, 15, 15,17);
    UIImage *btnimage=[UIImage imageNamed:@"arrow@2x.png"];
    [btn_back setBackgroundImage:btnimage forState:UIControlStateNormal];
    btn_back.backgroundColor = [UIColor clearColor];
    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [imgViewTop addSubview:btn_back];
    
    
    
    
    UILabel  * lbl_headingTitle = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(btn_back.frame)+25,15, 100,17)];
    lbl_headingTitle.text = @"Serve Later";
    lbl_headingTitle.backgroundColor=[UIColor clearColor];
    lbl_headingTitle.textColor=[UIColor whiteColor];
    lbl_headingTitle.textAlignment=NSTextAlignmentLeft;
    lbl_headingTitle.font = [UIFont fontWithName:kFont size:17];
    [imgViewTop addSubview:lbl_headingTitle];
    
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 6, 33, 33)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-logo.png"];
    [imgViewTop addSubview:img_logo];
    
    // Do any additional setup after loading the view.
}

-(void)IntegrateBodyDesign
{
    
    btn_Schedule = [[UIButton alloc] init];
    if (IS_IPHONE_6Plus)
    {
        btn_Schedule.frame = CGRectMake(55, CGRectGetMaxY(imgViewTop.frame)+15, 150,35);
    }
    else if (IS_IPHONE_6)
    {
        btn_Schedule.frame = CGRectMake(45, CGRectGetMaxY(imgViewTop.frame)+15, 140,35);
    }
    else  if (IS_IPHONE_5)
    {
        btn_Schedule.frame = CGRectMake(40, CGRectGetMaxY(imgViewTop.frame)+10, WIDTH/3.0,30);
    }
    else
    {
        btn_Schedule.frame = CGRectMake(40, CGRectGetMaxY(imgViewTop.frame)+10, 120,30);
    }
    btn_Schedule.backgroundColor = [UIColor blackColor];
    [btn_Schedule setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Schedule setTitle:@"Schedule" forState:UIControlStateNormal];
    btn_Schedule.titleLabel.font = [UIFont fontWithName:kFontBold size:15];
    [btn_Schedule addTarget:self action:@selector(Schedule_btnClick:) forControlEvents:UIControlEventTouchUpInside ];
    [self.view addSubview:btn_Schedule];
    
    
    
    btn_OnRequest = [[UIButton alloc] init];
    if (IS_IPHONE_6Plus){
        btn_OnRequest.frame = CGRectMake(CGRectGetMaxX(btn_Schedule.frame), CGRectGetMaxY(imgViewTop.frame)+15, 150,35);
    }
    else if (IS_IPHONE_6){
        btn_OnRequest.frame = CGRectMake(CGRectGetMaxX(btn_Schedule.frame), CGRectGetMaxY(imgViewTop.frame)+15, 140,35);
    }
    else  {
        btn_OnRequest.frame = CGRectMake(CGRectGetMaxX(btn_Schedule.frame), CGRectGetMaxY(imgViewTop.frame)+10, 120,30);
    }
    btn_OnRequest.backgroundColor = [UIColor lightGrayColor];
    [btn_OnRequest setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_OnRequest setTitle:@"On Request" forState:UIControlStateNormal];
    btn_OnRequest.titleLabel.font = [UIFont fontWithName:kFontBold size:15];
    [btn_OnRequest addTarget:self action:@selector(onRequest_btnClick:) forControlEvents:UIControlEventTouchUpInside ];
    [self.view addSubview:btn_OnRequest];
    
    //    ============== Schedule View Design ===========
    
    mainScrollView = [[UIScrollView alloc]init];
    mainScrollView.frame=CGRectMake(0, CGRectGetMaxY(btn_OnRequest.frame)+10, WIDTH , HEIGHT - 95);
    if (IS_IPHONE_6Plus)
    {
        mainScrollView.frame=CGRectMake(0, CGRectGetMaxY(btn_OnRequest.frame)+10, WIDTH , HEIGHT - 95);
    }
    else if (IS_IPHONE_6)
    {
        mainScrollView.frame=CGRectMake(0, CGRectGetMaxY(btn_OnRequest.frame)+10, WIDTH , HEIGHT - 95);
    }
    else if (IS_IPHONE_5)
    {
        mainScrollView.frame=CGRectMake(0, CGRectGetMaxY(btn_OnRequest.frame)+10, WIDTH , HEIGHT - 95);
    }
    else
    {
        mainScrollView.frame=CGRectMake(0, CGRectGetMaxY(btn_OnRequest.frame)+10, WIDTH , HEIGHT - 95);
    }
    mainScrollView.backgroundColor =[UIColor colorWithRed:243/255.0f green:245/255.0f blue:244/255.0f alpha:1.0f];
    mainScrollView.bounces=NO;
    //            mainScrollView.layer.borderWidth = 1.0;
    mainScrollView.showsVerticalScrollIndicator = NO;
    [mainScrollView setScrollEnabled:YES];
    [self.view addSubview:mainScrollView];
    
    //    [mainScrollView setContentSize:CGSizeMake(WIDTH, 910.0)];
    if (IS_IPHONE_6Plus)
    {
        [mainScrollView setContentSize:CGSizeMake(WIDTH, 1100.0)];
    }
    else if (IS_IPHONE_6)
    {
        [mainScrollView setContentSize:CGSizeMake(WIDTH, 1050.0)];
    }
    else if (IS_IPHONE_5)
    {
        [mainScrollView setContentSize:CGSizeMake(WIDTH, 910.0)];
    }
    else
    {
        [mainScrollView setContentSize:CGSizeMake(WIDTH, 910.0)];
    }
    
    UILabel *calenderLabel = [[UILabel alloc] init];
    calenderLabel.frame=CGRectMake(0, 0, WIDTH, 20);
    if (IS_IPHONE_6Plus)
    {
        calenderLabel.frame=CGRectMake(0, 0, WIDTH, 20);
    }
    else if (IS_IPHONE_6)
    {
        calenderLabel.frame=CGRectMake(0, 0, WIDTH, 20);
    }
    else if (IS_IPHONE_5)
    {
        calenderLabel.frame=CGRectMake(0, 0, WIDTH, 20);
    }
    else
    {
        calenderLabel.frame=CGRectMake(0, 0, WIDTH, 20);
    }
    calenderLabel.textAlignment = NSTextAlignmentCenter;
    calenderLabel.font = [UIFont fontWithName:kFontBold size:13];
    calenderLabel.text = @"Click on a date to select your serving date";
    calenderLabel.backgroundColor = [UIColor clearColor];
    [mainScrollView addSubview:calenderLabel];
    
    
    UIView *calView = [[UIView alloc]init];
    calView.frame=CGRectMake(12, CGRectGetMaxY(calenderLabel.frame), WIDTH-24, 300);
    if (IS_IPHONE_6Plus)
    {
        calView.frame = CGRectMake(12, CGRectGetMaxY(calenderLabel.frame), WIDTH-24, 380);
    }
    else if (IS_IPHONE_6)
    {
        calView.frame = CGRectMake(12, CGRectGetMaxY(calenderLabel.frame), WIDTH-24, 350);
    }
    else if (IS_IPHONE_5)
    {
        calView.frame = CGRectMake(12, CGRectGetMaxY(calenderLabel.frame), WIDTH-24, 300);
    }
    else
    {
        calView.frame = CGRectMake(12, CGRectGetMaxY(calenderLabel.frame), WIDTH-24, 300);
    }
    calView.backgroundColor = [UIColor whiteColor];
    //    calView.layer.borderWidth=1.0;
    //    [self.view addSubview: notificationView];
    [mainScrollView addSubview: calView];
    
    
    calendar = [[CKCalendarView alloc] initWithStartDay:startSunday];
    calendar.frame = CGRectMake(0, 0, WIDTH-24, 300);
    if (IS_IPHONE_6Plus)
    {
        calendar.frame = CGRectMake(0, 0, WIDTH-24, 300);
    }
    else if (IS_IPHONE_6)
    {
        calendar.frame = CGRectMake(0, 0, WIDTH-24, 300);
    }
    else if (IS_IPHONE_5)
    {
        calendar.frame = CGRectMake(0, 0, WIDTH-24, 300);
    }
    else
    {
        calendar.frame = CGRectMake(0, 0, WIDTH-24, 300);
    }
    self.calendar = calendar;
    calendar.delegate = self;
    calendar.backgroundColor = [UIColor clearColor];
    calendar.titleColor = [UIColor blackColor];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    //     minimumDate = [dateFormatter dateFromString:dateString];
    //    minimumDate = [dateFormatter dateFromString:@"22/02/1990"];
    calendar.onlyShowCurrentMonth = NO;
    calendar.adaptHeightToNumberOfWeeksInMonth = NO;
    calendar.titleFont = [UIFont fontWithName:kFont size:17.0];
    calendar.layer.borderWidth = 1.0;
    calendar.clipsToBounds=YES;
    [calView addSubview:calendar];
    [calendar selectDate:[NSDate date] makeVisible:NO];
    //    ary_AvailableDates = self.enabledDates;
    [calendar reloadData];
    
    
    UILabel *servingTimeLabel = [[UILabel alloc] init];
    servingTimeLabel.frame=CGRectMake(0, CGRectGetMaxY(calView.frame)+10, WIDTH, 20);
    if (IS_IPHONE_6Plus)
    {
        servingTimeLabel.frame=CGRectMake(0, CGRectGetMaxY(calView.frame)+10, WIDTH, 20);
    }
    else if (IS_IPHONE_6)
    {
        servingTimeLabel.frame=CGRectMake(0, CGRectGetMaxY(calView.frame)+10, WIDTH, 20);
    }
    else if (IS_IPHONE_5)
    {
        servingTimeLabel.frame=CGRectMake(0, CGRectGetMaxY(calView.frame)+10, WIDTH, 20);
    }
    else
    {
        servingTimeLabel.frame=CGRectMake(0, CGRectGetMaxY(calView.frame)+10, WIDTH, 20);
    }
    servingTimeLabel.textAlignment = NSTextAlignmentCenter;
    servingTimeLabel.font = [UIFont fontWithName:kFontBold size:13];
    servingTimeLabel.text = @"Select your serving time";
    servingTimeLabel.backgroundColor = [UIColor clearColor];
    [mainScrollView addSubview:servingTimeLabel];
    
    
    table_schedule = [[UITableView alloc] init ];
    table_schedule.frame  = CGRectMake(0,CGRectGetMaxY(servingTimeLabel.frame)+5,WIDTH, 95);
    if (IS_IPHONE_6Plus)
    {
        table_schedule.frame  = CGRectMake(0,CGRectGetMaxY(servingTimeLabel.frame)+5,WIDTH, 110);
    }
    else if (IS_IPHONE_6)
    {
        table_schedule.frame  = CGRectMake(0,CGRectGetMaxY(servingTimeLabel.frame)+5,WIDTH, 110);
    }
    else if (IS_IPHONE_5)
    {
        table_schedule.frame  = CGRectMake(0,CGRectGetMaxY(servingTimeLabel.frame)+5,WIDTH, 95);
    }
    else
    {
        table_schedule.frame  = CGRectMake(0,CGRectGetMaxY(servingTimeLabel.frame)+5,WIDTH, 95);
    }
    [table_schedule setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_schedule.delegate = self;
    table_schedule.dataSource = self;
    table_schedule.showsVerticalScrollIndicator = NO;
    table_schedule.backgroundColor = [UIColor clearColor];
    //    table_schedule.layer.borderWidth = 1.0;
    [mainScrollView addSubview:table_schedule];
    
    
    btn_AddSchedule = [[UIButton alloc] init];
    btn_AddSchedule.frame = CGRectMake((WIDTH-50)/2.0, CGRectGetMaxY(table_schedule.frame)+10, 50,50);
    if (IS_IPHONE_6Plus)
    {
        btn_AddSchedule.frame = CGRectMake((WIDTH-55)/2.0, CGRectGetMaxY(table_schedule.frame)+10, 55,55);
    }
    else if (IS_IPHONE_6)
    {
        btn_AddSchedule.frame = CGRectMake((WIDTH-55)/2.0, CGRectGetMaxY(table_schedule.frame)+10, 55,55);
    }
    else if (IS_IPHONE_5)
    {
        btn_AddSchedule.frame = CGRectMake((WIDTH-50)/2.0, CGRectGetMaxY(table_schedule.frame)+10, 50,50);
    }
    else
    {
        btn_AddSchedule.frame = CGRectMake((WIDTH-50)/2.0, CGRectGetMaxY(table_schedule.frame)+10, 50,50);
    }
    UIImage *btnimage=[UIImage imageNamed:@"icon_add@2x"];
    [btn_AddSchedule setBackgroundImage:btnimage forState:UIControlStateNormal];
    btn_AddSchedule.backgroundColor = [UIColor clearColor];
    [btn_AddSchedule addTarget:self action:@selector(btn_AddSchedule_Method) forControlEvents:UIControlEventTouchUpInside ];
    [mainScrollView addSubview:btn_AddSchedule];
    
    
    UILabel *dishesLabel = [[UILabel alloc] init];
    dishesLabel.frame=CGRectMake(0, CGRectGetMaxY(btn_AddSchedule.frame)+10, WIDTH, 20);
    if (IS_IPHONE_6Plus)
    {
        dishesLabel.frame=CGRectMake(0, CGRectGetMaxY(btn_AddSchedule.frame)+10, WIDTH, 20);
    }
    else if (IS_IPHONE_6)
    {
        dishesLabel.frame=CGRectMake(0, CGRectGetMaxY(btn_AddSchedule.frame)+10, WIDTH, 20);
    }
    else if (IS_IPHONE_5)
    {
        dishesLabel.frame=CGRectMake(0, CGRectGetMaxY(btn_AddSchedule.frame)+10, WIDTH, 20);
    }
    else
    {
        dishesLabel.frame=CGRectMake(0, CGRectGetMaxY(btn_AddSchedule.frame)+10, WIDTH, 20);
    }
    dishesLabel.textAlignment = NSTextAlignmentCenter;
    dishesLabel.font = [UIFont fontWithName:kFontBold size:13];
    dishesLabel.text = @"Select your dishes to serve";
    dishesLabel.backgroundColor = [UIColor clearColor];
    [mainScrollView addSubview:dishesLabel];
    
    
    
    imgViewSerchdish=[[UIImageView alloc]init];
    //    imgViewSerchdish.frame=CGRectMake(0, CGRectGetMaxY(labl_Dishes.frame)+5,WIDTH+10,50);
    if (IS_IPHONE_6Plus)
    {
        //         imgViewSerchdish.frame=CGRectMake(0, CGRectGetMaxY(labl_Dishes.frame)+5,WIDTH+10,50);
        imgViewSerchdish.frame=CGRectMake(0, CGRectGetMaxY(dishesLabel.frame)+5,WIDTH+10,55);
    }
    else if (IS_IPHONE_6)
    {
        imgViewSerchdish.frame=CGRectMake(0, CGRectGetMaxY(dishesLabel.frame)+5,WIDTH+10,55);
    }
    else if (IS_IPHONE_5)
    {
        imgViewSerchdish.frame=CGRectMake(0, CGRectGetMaxY(dishesLabel.frame)+5,WIDTH+10,50);
    }
    else
    {
        imgViewSerchdish.frame=CGRectMake(0, CGRectGetMaxY(dishesLabel.frame)+5,WIDTH+10,45);
    }
    [imgViewSerchdish setUserInteractionEnabled:YES];
    imgViewSerchdish.image=[UIImage imageNamed:@"img_background@2x..png"];
    [mainScrollView addSubview:imgViewSerchdish];
    
    
    img_Dish=[[UIImageView alloc]init];
    //     img_Dish.frame=CGRectMake(20, 15,32,20);
    if (IS_IPHONE_6Plus)
    {
        //        img_Dish.frame=CGRectMake(20, 15,32,20);
        img_Dish.frame=CGRectMake(20, 15,35,25);
    }
    else if (IS_IPHONE_6)
    {
        img_Dish.frame=CGRectMake(20, 15,35,25);
    }
    else if (IS_IPHONE_5)
    {
        img_Dish.frame=CGRectMake(20, 15,32,20);
    }
    else
    {
        img_Dish.frame=CGRectMake(20, 13,32,20);
    }
    [img_Dish setUserInteractionEnabled:YES];
    //[img_Dish setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
    img_Dish.image = [UIImage imageNamed:@"img_dishplate.png"];
    [imgViewSerchdish addSubview:img_Dish];
    
    
    
    btn_Search = [[UIButton alloc]init];
    if (IS_IPHONE_6Plus) {
        
        //        btn_Search.frame = CGRectMake(self.view.frame.size.width-65, 15, 20, 20);
        btn_Search.frame = CGRectMake(self.view.frame.size.width-65, 15, 25, 25);
        
    }else if (IS_IPHONE_6)
    {
        btn_Search.frame = CGRectMake(self.view.frame.size.width-60, 15, 25, 25);
    }
    else if (IS_IPHONE_5)
    {
        btn_Search.frame = CGRectMake(self.view.frame.size.width-55, 15, 20, 20);
    }
    else
    {
        btn_Search.frame = CGRectMake(self.view.frame.size.width-55, 13, 20, 20);
    }
    
    [btn_Search setBackgroundImage:[UIImage imageNamed:@"img_search.png"] forState:UIControlStateNormal];
    [imgViewSerchdish addSubview:btn_Search];
    
    
    
    textfield_SearchDish = [[UITextField alloc]init];
    //    textfield_SearchDish.frame=CGRectMake(65, 10, self.view.frame.size.width-140, 35);
    if (IS_IPHONE_6Plus)
    {
        //        textfield_SearchDish.frame=CGRectMake(65, 10, self.view.frame.size.width-140, 35);
        textfield_SearchDish.frame=CGRectMake(70, 10, self.view.frame.size.width-150, 35);
    }
    else if (IS_IPHONE_6)
    {
        textfield_SearchDish.frame=CGRectMake(70, 10, self.view.frame.size.width-150, 35);
    }
    else if (IS_IPHONE_5)
    {
        textfield_SearchDish.frame=CGRectMake(65, 10, self.view.frame.size.width-140, 30);
    }
    else
    {
        textfield_SearchDish.frame=CGRectMake(65, 7, self.view.frame.size.width-140, 30);
    }
    textfield_SearchDish.placeholder = @"Search Dish...";
    textfield_SearchDish.font=[UIFont fontWithName:kFont size:13];
    [imgViewSerchdish addSubview:textfield_SearchDish];
    
    
    
    UIImageView *img_backgroundimage=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus) {
        //        img_backgroundimage.frame=CGRectMake(10,CGRectGetMaxY(imgViewSerchdish.frame)+5, WIDTH-20,250);
        img_backgroundimage.frame=CGRectMake(20,CGRectGetMaxY(imgViewSerchdish.frame)+20, WIDTH-40,280);
        
    }else if (IS_IPHONE_6)
        
    {
        img_backgroundimage.frame=CGRectMake(15,CGRectGetMaxY(imgViewSerchdish.frame)+15, WIDTH-30,260);
        
    }
    else if (IS_IPHONE_5)
    {
        img_backgroundimage.frame=CGRectMake(10,CGRectGetMaxY(imgViewSerchdish.frame)+5, WIDTH-15,225);
        
    }
    else
    {
        img_backgroundimage.frame=CGRectMake(10,CGRectGetMaxY(imgViewSerchdish.frame)+5, WIDTH-15,225);
    }
    [img_backgroundimage setUserInteractionEnabled:YES];
    //    img_backgroundimage.backgroundColor=[UIColor clearColor];
    [img_backgroundimage setContentMode:UIViewContentModeScaleAspectFill];
    img_backgroundimage.clipsToBounds = YES;
    img_backgroundimage.image=[UIImage imageNamed:@"bg1-img@2x"];
    [mainScrollView addSubview:img_backgroundimage];
    
    
    
    arryFoodnames = [[NSArray alloc]initWithObjects:@"Chilli Chicken",@"Spicy Nugget",@"Curry Chicken",@"Fish Otah",@"Peach Tea", nil];
    arryFoodImages = [[NSArray alloc]initWithObjects:@"img_fork.png",@"img_forkblue.png",@"img_forkblue.png",@"img_fork.png",@"img_fork.png", nil];
    
    
    
    tableViewDishes = [[UITableView alloc]init];
    if (IS_IPHONE_6Plus) {
        //        tableViewDishes.frame =CGRectMake(0,0, WIDTH-20,250);
        tableViewDishes.frame =CGRectMake(0,0, WIDTH-40,279);
        
    }else if (IS_IPHONE_6)
        
    {
        tableViewDishes.frame =CGRectMake(0,0, WIDTH-30,259);
    }
    else if (IS_IPHONE_5)
    {
        tableViewDishes.frame =CGRectMake(0,0, WIDTH-20,224);
    }
    else
    {
        tableViewDishes.frame =CGRectMake(0,0, WIDTH-20,224);
    }
    [tableViewDishes setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tableViewDishes.delegate = self;
    tableViewDishes.dataSource = self;
    tableViewDishes.showsVerticalScrollIndicator = NO;
    //        tableViewDishes.layer.borderWidth = 1.0;
    [img_backgroundimage addSubview:tableViewDishes];
    
    
    next = [[UIButton alloc] init];
    if (IS_IPHONE_6Plus) {
        //        next.frame = CGRectMake(40, self.view.frame.size.height-50, self.view.frame.size.width-80, 45);
        next.frame = CGRectMake(40, CGRectGetMaxY(img_backgroundimage.frame)+30, self.view.frame.size.width-80, 50);
        
    }else if (IS_IPHONE_6)
        
    {
        next.frame = CGRectMake(40, CGRectGetMaxY(img_backgroundimage.frame)+30, self.view.frame.size.width-80, 45);
        
    }
    else if (IS_IPHONE_5)
    {
        next.frame = CGRectMake(40, CGRectGetMaxY(img_backgroundimage.frame)+30, self.view.frame.size.width-80, 40);
    }
    else
    {
        next.frame = CGRectMake(40, CGRectGetMaxY(img_backgroundimage.frame)+30, self.view.frame.size.width-80, 35);
    }
    next.backgroundColor = [UIColor brownColor];
    next.layer.cornerRadius=4.0f;
    [next setImage:[UIImage imageNamed:@"text buuton.png"] forState:UIControlStateNormal];
    [next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [next addTarget:self action:@selector(next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [mainScrollView addSubview:next];
    
    
    onRequest_View = [[UIView alloc]init];
    onRequest_View.frame=CGRectMake(0, CGRectGetMaxY(btn_OnRequest.frame)+10, WIDTH , HEIGHT - 95);
    if (IS_IPHONE_6Plus)
    {
        
    }
    else if (IS_IPHONE_6)
    {
        
    }
    else if (IS_IPHONE_5)
    {
        
    }
    else
    {
        
    }
    onRequest_View.backgroundColor = [UIColor colorWithRed:243/255.0f green:245/255.0f blue:244/255.0f alpha:1.0f];
    //    onRequest_View.layer.borderWidth=1.0;
    //    [self.view addSubview: notificationView];
    [self.view addSubview: onRequest_View];
    onRequest_View.hidden = YES;
    
    
    UILabel *fisrtLabel = [[UILabel alloc] init];
    fisrtLabel.frame=CGRectMake(20, 7, 15, 20);
    if (IS_IPHONE_6Plus)
    {
        fisrtLabel.frame=CGRectMake(20, 7, 15, 20);
    }
    else if (IS_IPHONE_6)
    {
        fisrtLabel.frame=CGRectMake(20, 7, 15, 20);
    }
    else if (IS_IPHONE_5)
    {
        fisrtLabel.frame=CGRectMake(20, 7, 15, 20);
    }
    else
    {
        fisrtLabel.frame=CGRectMake(20, 7, 15, 20);
    }
    //    fisrtLabel.textAlignment = NSTextAlignmentCenter;
    fisrtLabel.font = [UIFont fontWithName:kFontBold size:13];
    fisrtLabel.text = @"1.";
    fisrtLabel.backgroundColor = [UIColor clearColor];
    [onRequest_View addSubview:fisrtLabel];
    
    
    UITextView *txtview_Text1 = [[UITextView alloc]init];
    txtview_Text1.frame = CGRectMake(CGRectGetMaxX(fisrtLabel.frame), 0, WIDTH-60 , 40);
    if (IS_IPHONE_6Plus)
    {
        txtview_Text1.frame = CGRectMake(CGRectGetMaxX(fisrtLabel.frame), 0, WIDTH-60 , 40);
    }
    else if (IS_IPHONE_6)
    {
        txtview_Text1.frame = CGRectMake(CGRectGetMaxX(fisrtLabel.frame), 0, WIDTH-60 , 40);
    }
    else if (IS_IPHONE_5)
    {
        txtview_Text1.frame = CGRectMake(CGRectGetMaxX(fisrtLabel.frame), 0, WIDTH-60 , 40);
    }
    else
    {
        txtview_Text1.frame = CGRectMake(CGRectGetMaxX(fisrtLabel.frame), 0, WIDTH-60 , 40);
    }
    txtview_Text1.scrollEnabled = YES;
    txtview_Text1.userInteractionEnabled = NO;
    txtview_Text1.font = [UIFont fontWithName:kFontBold size:13];
    txtview_Text1.backgroundColor = [UIColor clearColor];
    txtview_Text1.textColor = [UIColor blackColor];
    //        txtview_Text1.layer.borderWidth = 1.0;
    txtview_Text1.text =@"I will condider orders on request for later orders.";
    [onRequest_View addSubview:txtview_Text1];
    
    
    UILabel *secLabel = [[UILabel alloc] init];
    secLabel.frame=CGRectMake(20, CGRectGetMaxY(txtview_Text1.frame)+7, 15, 20);
    if (IS_IPHONE_6Plus)
    {
        secLabel.frame=CGRectMake(20, CGRectGetMaxY(txtview_Text1.frame)+7, 15, 20);
    }
    else if (IS_IPHONE_6)
    {
        secLabel.frame=CGRectMake(20, CGRectGetMaxY(txtview_Text1.frame)+7, 15, 20);
    }
    else if (IS_IPHONE_5)
    {
        secLabel.frame=CGRectMake(20, CGRectGetMaxY(txtview_Text1.frame)+7, 15, 20);
    }
    else
    {
        secLabel.frame=CGRectMake(20, CGRectGetMaxY(txtview_Text1.frame)+7, 15, 20);
    }
    //    secLabel.textAlignment = NSTextAlignmentCenter;
    secLabel.font = [UIFont fontWithName:kFontBold size:13];
    secLabel.text = @"2.";
    secLabel.backgroundColor = [UIColor clearColor];
    [onRequest_View addSubview:secLabel];
    
    UITextView *txtview_Text2 = [[UITextView alloc]init];
    txtview_Text2.frame = CGRectMake(CGRectGetMaxX(fisrtLabel.frame), CGRectGetMaxY(txtview_Text1.frame), WIDTH-60 , 25);
    if (IS_IPHONE_6Plus)
    {
        txtview_Text2.frame = CGRectMake(CGRectGetMaxX(fisrtLabel.frame), CGRectGetMaxY(txtview_Text1.frame), WIDTH-60 , 23);
    }
    else if (IS_IPHONE_6)
    {
        txtview_Text2.frame = CGRectMake(CGRectGetMaxX(fisrtLabel.frame), CGRectGetMaxY(txtview_Text1.frame), WIDTH-60 , 23);
    }
    else if (IS_IPHONE_5)
    {
        txtview_Text2.frame = CGRectMake(CGRectGetMaxX(fisrtLabel.frame), CGRectGetMaxY(txtview_Text1.frame), WIDTH-60 , 23);
    }
    else
    {
        txtview_Text2.frame = CGRectMake(CGRectGetMaxX(fisrtLabel.frame), CGRectGetMaxY(txtview_Text1.frame), WIDTH-60 , 23);
    }
    txtview_Text2.scrollEnabled = YES;
    txtview_Text2.userInteractionEnabled = NO;
    txtview_Text2.font = [UIFont fontWithName:kFontBold size:13];
    txtview_Text2.backgroundColor = [UIColor clearColor];
    txtview_Text2.textColor = [UIColor blackColor];
    //    txtview_Text2.layer.borderWidth = 1.0;
    txtview_Text2.text =@"I do not have a schedule right now.";
    [onRequest_View addSubview:txtview_Text2];
    
    
    UILabel *thirdLabel = [[UILabel alloc] init];
    thirdLabel.frame=CGRectMake(20, CGRectGetMaxY(txtview_Text2.frame)+7, 15, 20);
    if (IS_IPHONE_6Plus)
    {
        thirdLabel.frame=CGRectMake(20, CGRectGetMaxY(txtview_Text2.frame)+7, 15, 20);
    }
    else if (IS_IPHONE_6)
    {
        thirdLabel.frame=CGRectMake(20, CGRectGetMaxY(txtview_Text2.frame)+7, 15, 20);
    }
    else if (IS_IPHONE_5)
    {
        thirdLabel.frame=CGRectMake(20, CGRectGetMaxY(txtview_Text2.frame)+7, 15, 20);
    }
    else
    {
        thirdLabel.frame=CGRectMake(20, CGRectGetMaxY(txtview_Text2.frame)+7, 15, 20);
    }
    //    thirdLabel.textAlignment = NSTextAlignmentCenter;
    thirdLabel.font = [UIFont fontWithName:kFontBold size:13];
    thirdLabel.text = @"3.";
    thirdLabel.backgroundColor = [UIColor clearColor];
    [onRequest_View addSubview:thirdLabel];
    
    
    UITextView *txtview_Text3 = [[UITextView alloc]init];
    txtview_Text3.frame = CGRectMake(CGRectGetMaxX(fisrtLabel.frame), CGRectGetMaxY(txtview_Text2.frame), WIDTH-60 , 55);
    if (IS_IPHONE_6Plus)
    {
        txtview_Text3.frame = CGRectMake(CGRectGetMaxX(fisrtLabel.frame), CGRectGetMaxY(txtview_Text2.frame), WIDTH-60 , 55);
    }
    else if (IS_IPHONE_6)
    {
        txtview_Text3.frame = CGRectMake(CGRectGetMaxX(fisrtLabel.frame), CGRectGetMaxY(txtview_Text2.frame), WIDTH-60 , 55);
    }
    else if (IS_IPHONE_5)
    {
        txtview_Text3.frame = CGRectMake(CGRectGetMaxX(fisrtLabel.frame), CGRectGetMaxY(txtview_Text2.frame), WIDTH-60 , 55);
    }
    else
    {
        txtview_Text3.frame = CGRectMake(CGRectGetMaxX(fisrtLabel.frame), CGRectGetMaxY(txtview_Text2.frame), WIDTH-60 , 55);
    }
    txtview_Text3.scrollEnabled = YES;
    txtview_Text3.userInteractionEnabled = NO;
    txtview_Text3.font = [UIFont fontWithName:kFontBold size:13];
    txtview_Text3.backgroundColor = [UIColor clearColor];
    txtview_Text3.textColor = [UIColor blackColor];
    //    txtview_Text3.layer.borderWidth = 1.0;
    txtview_Text3.text =@"I will choose to cook as and when I get requests to cook based on the fiollowing minimum order value.";
    [onRequest_View addSubview:txtview_Text3];
    
    
    UIImageView *imgView=[[UIImageView alloc]init];
    imgView.frame=CGRectMake(0, CGRectGetMaxY(txtview_Text3.frame)+10,WIDTH,60);
    if (IS_IPHONE_6Plus)
    {
        imgView.frame=CGRectMake(0, CGRectGetMaxY(txtview_Text3.frame)+10,WIDTH,60);
    }
    else if (IS_IPHONE_6)
    {
        imgView.frame=CGRectMake(0, CGRectGetMaxY(txtview_Text3.frame)+10,WIDTH,60);
    }
    else if (IS_IPHONE_5)
    {
        imgView.frame=CGRectMake(0, CGRectGetMaxY(txtview_Text3.frame)+10,WIDTH,60);
    }
    else
    {
        imgView.frame=CGRectMake(0, CGRectGetMaxY(txtview_Text3.frame)+10,WIDTH,60);
    }
    [imgView setUserInteractionEnabled:YES];
    imgView.image=[UIImage imageNamed:@"img_background@2x..png"];
    [onRequest_View addSubview:imgView];
    
    
    UILabel *titleLbl = [[UILabel alloc]init];
    titleLbl.frame = CGRectMake(45, 0, 120, 55);
    if (IS_IPHONE_6Plus)
    {
        titleLbl.font = [UIFont fontWithName:kFontBold size:16];
        
        titleLbl.frame = CGRectMake(45, 10, 190, 30);
    }
    else if (IS_IPHONE_6)
    {
        titleLbl.font = [UIFont fontWithName:kFontBold size:16];
        
        titleLbl.frame = CGRectMake(45, 10, 190, 30);
    }
    else if (IS_IPHONE_5)
    {
        titleLbl.font = [UIFont fontWithName:kFontBold size:13];
        
        titleLbl.frame = CGRectMake(45, 0, 160, 55);
        titleLbl.numberOfLines = 0;
        
    }
    else
    {
        titleLbl.font = [UIFont fontWithName:kFontBold size:13];
        
        titleLbl.frame = CGRectMake(45, 0, 160, 55);
        titleLbl.numberOfLines = 0;
        
    }
    titleLbl.backgroundColor = [UIColor clearColor];
    titleLbl.text = @"Minimum order valve : $";
    titleLbl.textColor = [UIColor blackColor];
    titleLbl.lineBreakMode = NSLineBreakByWordWrapping;
    [imgView addSubview:titleLbl];
    
    
    
    txt_amount = [[UITextField alloc] init];
    if (IS_IPHONE_6Plus) {
        
        //        labl_Timer.frame = CGRectMake(self.view.frame.size.width-130, 10, 100,30);
        txt_amount.frame = CGRectMake(CGRectGetMaxX(titleLbl.frame)+2, 10, 100,30);
    }else if (IS_IPHONE_6)
    {
        txt_amount.frame = CGRectMake(CGRectGetMaxX(titleLbl.frame)+2, 10, 100,30);
    }
    else if (IS_IPHONE_5)
    {
        txt_amount.frame = CGRectMake(CGRectGetMaxX(titleLbl.frame)+2, 0, 100,55);
    }
    else
    {
        txt_amount.frame = CGRectMake(CGRectGetMaxX(titleLbl.frame)+2, 0, 100,55);
    }
    txt_amount.borderStyle = UITextBorderStyleNone;
    //    txt_EndTime.textColor = [UIColor grayColor];
    txt_amount.placeholder = @"_ _ _";
    //    txt_EndTime.placeholder = @"d   d      m  m      y    y    y    y";
    txt_amount.font = [UIFont fontWithName:kFont size:15];
    txt_amount.leftViewMode = UITextFieldViewModeAlways;
    txt_amount.userInteractionEnabled=YES;
    txt_amount.textAlignment = NSTextAlignmentLeft;
    txt_amount.backgroundColor = [UIColor clearColor];
    txt_amount.keyboardType = UIKeyboardTypeAlphabet;
    txt_amount.delegate = self;
    txt_amount.textColor =[UIColor blackColor];
    [imgView addSubview:txt_amount];
    
    
    btn_Agree = [[UIButton alloc]init];
    btn_Agree.frame = CGRectMake(18, CGRectGetMaxY(imgView.frame)+15, 20, 20);
    if (IS_IPHONE_6Plus)
    {
        btn_Agree.frame = CGRectMake(18, CGRectGetMaxY(imgView.frame)+15, 20, 20);
    }
    else if (IS_IPHONE_6)
    {
        btn_Agree.frame = CGRectMake(18, CGRectGetMaxY(imgView.frame)+15, 20, 20);
    }
    else if (IS_IPHONE_5)
    {
        btn_Agree.frame = CGRectMake(18, CGRectGetMaxY(imgView.frame)+15, 20, 20);
    }
    else
    {
        btn_Agree.frame = CGRectMake(18, CGRectGetMaxY(imgView.frame)+15, 20, 20);
    }
    [btn_Agree addTarget:self action:@selector(btn_Agree_Method:) forControlEvents:UIControlEventTouchUpInside ];
    [btn_Agree setBackgroundImage:[UIImage imageNamed:@"icon-un-selected-check@2x"] forState:UIControlStateNormal];
    [onRequest_View addSubview:btn_Agree];
    
    
    
    UITextView *txtview_Agree = [[UITextView alloc]init];
    //    txtview_Agree.frame = CGRectMake(CGRectGetMaxX(btn_Agree.frame)+5, CGRectGetMaxY(imgView.frame)+5, WIDTH-60 , 150);
    if (IS_IPHONE_6Plus)
    {
        txtview_Agree.frame = CGRectMake(CGRectGetMaxX(btn_Agree.frame)+5, CGRectGetMaxY(imgView.frame)+5, WIDTH-60 , 150);
    }
    else if (IS_IPHONE_6)
    {
        txtview_Agree.frame = CGRectMake(CGRectGetMaxX(btn_Agree.frame)+5, CGRectGetMaxY(imgView.frame)+5, WIDTH-60 , 150);
    }
    else if (IS_IPHONE_5)
    {
        txtview_Agree.frame = CGRectMake(CGRectGetMaxX(btn_Agree.frame)+5, CGRectGetMaxY(imgView.frame)+5, WIDTH-60 , 150);
    }
    else
    {
        txtview_Agree.frame = CGRectMake(CGRectGetMaxX(btn_Agree.frame)+5, CGRectGetMaxY(imgView.frame)+5, WIDTH-60 , 150);
    }
    txtview_Agree.scrollEnabled = YES;
    txtview_Agree.userInteractionEnabled = NO;
    txtview_Agree.font = [UIFont fontWithName:kFont size:14];
    txtview_Agree.backgroundColor = [UIColor clearColor];
    txtview_Agree.textColor = [UIColor blackColor];
    //    txtview_Agree.layer.borderWidth = 1.0;
    txtview_Agree.text =@"I understand that if I toggle to schedule and entr a schedule time to cook, I will not able to recieve orders on request until I clesr my schedule. Howesver, I will be able to serve now on a schedule at any time, as the orders on request status applies to later orders.";
    [onRequest_View addSubview:txtview_Agree];
    
    
    btn_Save = [[UIButton alloc] init];
    if (IS_IPHONE_6Plus) {
        btn_Save.frame = CGRectMake(40, CGRectGetMaxY(txtview_Agree.frame)+25, self.view.frame.size.width-80, 40);
        
    }else if (IS_IPHONE_6)
        
    {
        btn_Save.frame = CGRectMake(40, CGRectGetMaxY(txtview_Agree.frame)+25, self.view.frame.size.width-80, 40);
    }
    else if (IS_IPHONE_5)
    {
        btn_Save.frame = CGRectMake(40, CGRectGetMaxY(txtview_Agree.frame)+25, self.view.frame.size.width-80, 40);
    }
    else
    {
        btn_Save.frame = CGRectMake(40, CGRectGetMaxY(txtview_Agree.frame)+20, self.view.frame.size.width-80, 40);
    }
    btn_Save.layer.cornerRadius=4.0f;
    //    [btn_Save setImage:[UIImage imageNamed:@"text buuton.png"] forState:UIControlStateNormal];
    [btn_Save setTitle:@"SAVE" forState:UIControlStateNormal];
    [btn_Save setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Save setBackgroundColor:[UIColor colorWithRed:39.0/255.0 green:37.0/255.0 blue:48.0/255.0 alpha:1.0f]];
    btn_Save.titleLabel.font=[UIFont fontWithName:kFont size:16];
    [btn_Save setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Save addTarget:self action:@selector(btn_Save_Method) forControlEvents:UIControlEventTouchUpInside ];
    [onRequest_View addSubview:btn_Save];
    
    imgViewSerchdish.hidden= YES;
    img_Dish.hidden = YES;
    btn_Search.hidden = YES;
    textfield_SearchDish.hidden= YES;
    
}


#pragma mark --CalenderMethod--

- (void)localeDidChange {
    [self.calendar setLocale:[NSLocale currentLocale]];
}

- (BOOL)dateIsDisabled:(NSDate *)date
{
    for (NSDate *disabledDate in self.enabledDates)
    {
        if ([disabledDate isEqualToDate:date])
        {
            return YES;
        }
    }
    return NO;
}



#pragma mark ButtonSelectorMethod

-(void)Back_btnClick
{
    //    [self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

-(void)Schedule_btnClick: (UIButton *)sender
{
    NSLog(@"Schedule Button Clicked");
    mainScrollView.hidden = NO;
    onRequest_View.hidden = YES;
}


-(void)btn_AddSchedule_Method
{
    NSLog(@"AddSchedule Button Clicked");
    
    str_servingtimeselected = @"YES";
    
    
    NSMutableDictionary *dict1=[[NSMutableDictionary alloc] init];
    [dict1 setValue:@""  forKey:@"starttime"];
    [dict1 setValue:@""  forKey:@"endtime"];
    [ary_servingtimes addObject:dict1];
    [table_schedule reloadData];
    
}

-(void)onRequest_btnClick: (UIButton *)sender
{
    NSLog(@"onRequest Button Clicked");
    mainScrollView.hidden = YES;
    onRequest_View.hidden = NO;
}
//

-(void)click_BtnMinusnow: (UIButton *)sender
{
    NSLog(@"click_BtnMinus Button Clicked");
    
    int tag=(int)[[sender superview] tag];
    //    int value=(int)[[[ary_servelist objectAtIndex:tag] valueForKey:@"quantity"] integerValue]-1;
    //    if (value <= 0) value = 1;
    //    [ary_servelist replaceObjectAtIndex:tag withObject:[@(value) stringValue]];
    
    NSMutableArray*dummy;
    dummy = [NSMutableArray new];
    
    
    NSString* selectedid =[[ary_servelaterfilter objectAtIndex:tag]valueForKey:@"DishID"];
    
    for (int i=0; i<[ary_servelaterfilter count]; i++)
    {
        if ([selectedid isEqualToString:[[ary_servelaterfilter objectAtIndex:i]valueForKey:@"DishID"]])
        {
            
            NSMutableDictionary *dict1=[[NSMutableDictionary alloc] init];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
            //            [dummy addObject:[[ary_servelist objectAtIndex:i] valueForKey:@"Serving_Type"]];
            //            [dict setValue:dummy forKey:@"Serving_Type"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
            NSString *str_facycount1=[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"quantity"]];
            int int_fancy1=[str_facycount1 integerValue];
            int_fancy1=int_fancy1-1;
            
            if (int_fancy1<0)
            {
                [dict1 setValue:@"0"  forKey:@"quantity"];
                
            }
            else{
                [dict1 setValue:[NSString stringWithFormat:@"%d", int_fancy1]  forKey:@"quantity"];
                
            }
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"Delivery_Address"]] forKey:@"Delivery_Address"];
            [dict1 setValue:@" "  forKey:@"starttime"];
            [dict1 setValue:@" "  forKey:@"endtime"];
            [ary_servelaterfilter replaceObjectAtIndex:i withObject:dict1];
            
#pragma replacing temp array also to match
            [Arr_Temp replaceObjectAtIndex:i withObject:[ary_servelaterfilter objectAtIndex:i]];
            
        }
    }
    
    [tableViewDishes reloadData];
}

-(void)click_BtnPlusnow: (UIButton *)sender
{
    NSLog(@"click_BtnPlus Button Clicked");
    
    selectedindex = [sender tag];
    
    
    int tag=(int)[[sender superview] tag];
    NSString* selectedid =[[ary_servelaterfilter objectAtIndex:tag]valueForKey:@"DishID"];
    
    for (int i=0; i<[ary_servelaterfilter count]; i++)
    {
        if ([selectedid isEqualToString:[[ary_servelaterfilter objectAtIndex:i]valueForKey:@"DishID"]])
        {
            
            NSMutableDictionary *dict1=[[NSMutableDictionary alloc] init];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
            NSString *str_facycount1=[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"quantity"]];
            int int_fancy1=[str_facycount1 integerValue];
            int_fancy1=int_fancy1+1;
            [dict1 setValue:[NSString stringWithFormat:@"%d", int_fancy1]  forKey:@"quantity"];
            [dict1 setValue:@" "  forKey:@"starttime"];
            [dict1 setValue:@" "  forKey:@"endtime"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"Delivery_Address"]] forKey:@"Delivery_Address"];
            [ary_servelaterfilter replaceObjectAtIndex:i withObject:dict1];
            
#pragma replacing temp array also to match
            [Arr_Temp replaceObjectAtIndex:i withObject:[ary_servelaterfilter objectAtIndex:i]];
            
        }
    }
    
    [tableViewDishes reloadData];
    
}

-(void)next_btnClick
{
    NSLog(@"next Button Clicked");
    NSString*str_start;
    NSString*str_end;
    
    
    for (int i=0; i<[ary_servingtimes count]; i++)
    {
        if ([[[ary_servingtimes objectAtIndex:i] valueForKey:@"starttime"]isEqualToString:@""])
        {
            str_start = @"NO";
            break;
            
        }
        else if ([[[ary_servingtimes objectAtIndex:i] valueForKey:@"endtime"]isEqualToString:@""])
        {
            str_end = @"NO";
            break;
            
        }
    }
    
    if ([str_servingtimeselected isEqualToString:@"NO"])
    {
        
        [self popup_Alertview:@"plaese select serve time"];
        
    }
    else if ([str_start isEqualToString:@"NO"])
    {
        [self popup_Alertview:@"plaese select start time"];
        return;
        
    }
    else if ([str_end isEqualToString:@"NO"])
    {
        [self popup_Alertview:@"plaese select end time"];
        return;
        
    }

    
//#pragma new code to check
//    for (int i=0; i<ary_servingtimes.count; i++)
//    {
//        
//        NSString *strttime=[[ary_servingtimes objectAtIndex:i] valueForKey:@"starttime"];
//        NSString *endtime=[[ary_servingtimes objectAtIndex:i] valueForKey:@"endtime"];
//        NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
//        [formatter1 setDateFormat:@"hh:mm aa"];
//        
//        NSDate *date2= [formatter1 dateFromString:endtime];
//        NSDate *date1 = [formatter1 dateFromString:strttime];
//        
//        NSComparisonResult result = [date1 compare:date2];
//        if(result == NSOrderedDescending)
//        {
//            NSLog(@"date2 is earlier than date1");
//            [self popup_Alertview:@"To time must be later than start time"];
//            
//        }
//        
//    }
    
    
    
    
    
    
    //    else if (selectedindex<0)
    //    {
    //        [self popup_Alertview:@"plaese select atleast a dish"];
    //
    //    }
    //    else if ([ary_servelistfilter count]>0)
    //    {
    //        NSString*str_quantity;
    //
    //        for (int i=0; i<[ary_servelistfilter count]; i++)
    //        {
    //            if ([[[ary_servelistfilter objectAtIndex:i]valueForKey:@"quantity"] intValue]<=0)
    //            {
    //                str_quantity= @"NO";
    //                [self popup_Alertview:@"Plaese select quantity for selected dishes"];
    //                break;
    //            }
    //            else{
    //                str_quantity= @"YES";
    //            }
    //        }
    //
    //        if ([str_quantity isEqualToString:@"YES"])
    //        {
    //            ChefServeTypeVC *serveTypeVc = [[ChefServeTypeVC alloc]init];
    //            NSMutableArray*ary_mainlisttoshow;
    //            ary_mainlisttoshow = [NSMutableArray new];
    //
    //
    //            for (int i=0; i<[ary_servelistfilter count]; i++)
    //            {
    //                if ([[[ary_servelistfilter objectAtIndex:i]valueForKey:@"quantity"] intValue]>0)
    //                {
    //                    [ary_mainlisttoshow addObject:[ary_servelistfilter objectAtIndex:i]];
    //                }
    //            }
    //
    //
    //            [formatter setDateFormat:@"hh:mm aa"];
    //            NSString *str=[formatter stringFromDate:datePicker.date];
    //            [txt_EndTime setText:str];
    //
    //            serveTypeVc.ary_servelisting = ary_mainlisttoshow;
    //            serveTypeVc.str_time = txt_EndTime.text;
    //
    //            // [self.navigationController pushViewController:serveTypeVc];
    //
    //            [self presentViewController:serveTypeVc animated:NO completion:nil];
    //
    //        }
    //
    //    }
    
    else
    {
        ChefServeLaterSecondVC *serveTypeVc = [[ChefServeLaterSecondVC alloc]init];
        NSMutableArray*ary_mainlisttoshow;
        ary_mainlisttoshow = [NSMutableArray new];
        
        
        for (int i=0; i<[ary_servelaterfilter count]; i++)
        {
            if ([[[ary_servelaterfilter objectAtIndex:i]valueForKey:@"quantity"] intValue]>0)
            {
                [ary_mainlisttoshow addObject:[ary_servelaterfilter objectAtIndex:i]];
            }
        }
        
        if([ary_mainlisttoshow count]==0)
        {
            [self popup_Alertview:@"plaese select atleast a quantity of dish"];
            return;
        }
        
        serveTypeVc.ary_servelisting = ary_mainlisttoshow;
        [self presentViewController:serveTypeVc animated:NO completion:nil];
    }
    
    
    
    //    [self.navigationController pushViewController:serveTypeVc];
    
}


-(void)btn_Save_Method
{
    NSLog(@"Save Button Clicked");
    
    if (txt_amount.text.length<=0)
    {
        [self popup_Alertview:@"plaese enter order valve"];
        
    }
    else if ([str_amountenter isEqualToString:@"0"])
    {
        [self popup_Alertview:@"plaese agree terms "];
    }
    else{
        [self AFOnrequestamount];
        
    }
}

-(void)btn_Agree_Method: (UIButton *)sender

{
    if ([sender isSelected])
    {
        str_amountenter = @"1";
        
        [sender setSelected:NO];
        [btn_Agree setBackgroundImage:[UIImage imageNamed:@"icon-selected-check@2x"] forState:UIControlStateNormal];
        
    }
    else{
        str_amountenter = @"0";
        
        [btn_Agree setBackgroundImage:[UIImage imageNamed:@"icon-un-selected-check@2x"] forState:UIControlStateNormal];
        
        [sender setSelected:YES];
    }
    NSLog(@"Agree Button Clicked");
}

-(void)click_selectObjectAt: (UIButton *)sender
{
    NSLog(@"qwerty  selected");
    
    selectedindex= (int)sender.tag;
    
    if (sender.tag==selectedindex)
    {
        indexSelected = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        
    }
    
    if([Arr_Temp containsObject:[ary_servelaterfilter objectAtIndex:sender.tag]])
    {
        //[Arr_Temp removeObject:[ary_servelaterfilter objectAtIndex:sender.tag]];
        [Arr_Temp replaceObjectAtIndex:sender.tag withObject:@""];
        
        
        NSString* selectedid =[[ary_servelaterfilter objectAtIndex:sender.tag]valueForKey:@"DishID"];
        
        for (int i=0; i<[ary_servelaterfilter count]; i++)
        {
            if ([selectedid isEqualToString:[[ary_servelaterfilter objectAtIndex:i]valueForKey:@"DishID"]])
            {
                
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
                
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
                
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
                
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
                [dict setValue:@"0"  forKey:@"quantity"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelaterfilter objectAtIndex:i] valueForKey:@"Delivery_Address"]] forKey:@"Delivery_Address"];
                [dict setValue:@" "  forKey:@"starttime"];
                [dict setValue:@" "  forKey:@"endtime"];
                
                [ary_servelaterfilter replaceObjectAtIndex:i withObject:dict];
                
                
                
                
            }
        }
        
        [tableViewDishes reloadData];
    }
    
    else
    {
        // new code
        
        [Arr_Temp replaceObjectAtIndex:sender.tag withObject:[ary_servelaterfilter objectAtIndex:sender.tag]];
    }
    
    [tableViewDishes reloadData];
}

#pragma mark - CKCalendarDelegate
- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)myDate
{
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd/MM/yyyy"];
    
   // NSString *str_SelectedDate = [format stringFromDate:myDate];
    
}

#pragma mark TableviewDelegate&DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tableViewDishes)
    {
        return [ary_servelaterfilter count];
    }
    else if (tableView == table_schedule)
    {
        return [ary_servingtimes count];
        
    }
    
    return 1.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    else
    {
        for (UIView *view in cell.contentView.subviews)
            [view removeFromSuperview];
    }
    
    if (tableView == tableViewDishes)
    {
        UIImageView *image = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus) {
            //        image.frame=CGRectMake(10, 5, 30, 30);
            image.frame=CGRectMake(10, 12, 30, 30);
            
        }else if (IS_IPHONE_6)
            
        {
            image.frame=CGRectMake(10, 11, 30, 30);
            
        }
        else{
            image.frame=CGRectMake(10, 10, 25, 25);
            
        }
        
        image.image = [UIImage imageNamed:@"img_fork.png"];
        [cell.contentView addSubview:image];
        
        
        UILabel *lblTitels = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus) {
            //        lblTitels.frame=CGRectMake(50, 7, 150, 30);
            lblTitels.frame=CGRectMake(50, 12, 150, 30);
            
        }else if (IS_IPHONE_6)
            
        {
            lblTitels.frame=CGRectMake(50, 11, 150, 30);
            
        }
        else{
            lblTitels.frame=CGRectMake(50, 10, 130, 25);
            
        }
        lblTitels.text = [[ary_servelaterfilter objectAtIndex:indexPath.row] valueForKey:@"DishName"];
        lblTitels.font=[UIFont fontWithName:kFont size:12];
        [cell.contentView addSubview:lblTitels];
        
        
        
        
        UIImageView *imagViewPlusminus  = [[UIImageView alloc]init];
        
        if (IS_IPHONE_6Plus) {
            //            imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-170, 10, 120, 25);
            imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-170, 12, 120, 30);
            
        }else if (IS_IPHONE_6)
            
        {
            imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-170, 11, 120, 30);
            
        }
        else{
            imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-140, 10, 100, 25);
            
        }
        imagViewPlusminus.image = [UIImage imageNamed:@"img_plusminus.png"];
        [cell.contentView addSubview:imagViewPlusminus];
        
        
        
        
        UILabel *labl = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus) {
            //            labl.frame=CGRectMake(self.view.frame.size.width-150, 10, 80, 25);
            labl.frame=CGRectMake(self.view.frame.size.width-150, 12, 80, 30);
            
        }else if (IS_IPHONE_6)
            
        {
            labl.frame=CGRectMake(self.view.frame.size.width-150, 11, 80, 30);
            
        }
        else{
            labl.frame=CGRectMake(self.view.frame.size.width-130, 10, 80, 25);
            
        }
        
        labl.textAlignment = NSTextAlignmentCenter;
        labl.text = [[ary_servelaterfilter objectAtIndex:indexPath.row] valueForKey:@"quantity"];
        
        [cell.contentView addSubview:labl];
        
        UIButton *btn_TableCell_CheckBox1 = [[UIButton alloc]init];
        btn_TableCell_CheckBox1.frame=CGRectMake(10,2,15,15);
        if (IS_IPHONE_6Plus)
        {
            //        img_lineimage.frame=CGRectMake(10, 49, WIDTH-40, 0.5);
            btn_TableCell_CheckBox1.frame=CGRectMake(0, 0, tableViewDishes.frame.size.width, 55);
            
        }
        else if (IS_IPHONE_6)
        {
            btn_TableCell_CheckBox1.frame=CGRectMake(0, 0, tableViewDishes.frame.size.width, 51);
        }
        else
        {
            btn_TableCell_CheckBox1.frame=CGRectMake(0, 0, tableViewDishes.frame.size.width, 44);
        }
        btn_TableCell_CheckBox1.backgroundColor=[UIColor clearColor];
        //    [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img.checkcorrect@2x.png"] forState:UIControlStateSelected];
        //    [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
        [btn_TableCell_CheckBox1 addTarget:self action:@selector(click_selectObjectAt:) forControlEvents:UIControlEventTouchUpInside];
        btn_TableCell_CheckBox1.tag = indexPath.row;
        [cell.contentView addSubview:btn_TableCell_CheckBox1];
        
        UIButton *btnMinus;
        UIButton *btnPlus;
        
        if ([cell.contentView viewWithTag:100] == nil)
        {
            btnMinus =[UIButton buttonWithType:UIButtonTypeCustom];
            
            if (IS_IPHONE_6Plus) {
                btnMinus.frame=CGRectMake(self.view.frame.size.width-170, 12, 30, 30);
                
            }
            else if (IS_IPHONE_6)
            {
                btnMinus.frame=CGRectMake(self.view.frame.size.width-170, 11, 30, 30);
                
            }
            else{
                btnMinus.frame=CGRectMake(self.view.frame.size.width-140, 10, 25, 25);
                
            }
            //    btnMinus.tag = indexPath.row;
            btnMinus.backgroundColor = [UIColor clearColor];
            [btnMinus addTarget:self action:@selector(click_BtnMinusnow:) forControlEvents:UIControlEventTouchUpInside];
            btnMinus.tag=100;
            btnMinus.layer.borderColor= [[UIColor blackColor]CGColor];
            //btnMinus.layer.borderWidth= 1.0f;
            btnMinus.clipsToBounds = YES;
            [cell.contentView addSubview:btnMinus];
        }
        
        if ([cell.contentView viewWithTag:101] == nil) {
            
            btnPlus = [UIButton buttonWithType:UIButtonTypeCustom];
            if (IS_IPHONE_6Plus) {
                //            btnPlus.frame=CGRectMake(self.view.frame.size.width-50, 10, 25, 25);
                btnPlus.frame=CGRectMake(self.view.frame.size.width-80, 12, 30, 30);
                
            }
            else if (IS_IPHONE_6)
            {
                btnPlus.frame=CGRectMake(self.view.frame.size.width-80, 11, 30, 30);
                
            }
            else
            {
                btnPlus.frame=CGRectMake(self.view.frame.size.width-65, 10, 25, 25);
                
            }
            btnPlus.tag = indexPath.row;
            btnPlus.backgroundColor = [UIColor clearColor];
            [btnPlus addTarget:self action:@selector(click_BtnPlusnow:) forControlEvents:UIControlEventTouchUpInside];
            btnPlus.tag=101;
            btnPlus.layer.borderColor= [[UIColor blackColor]CGColor];
            //btnPlus.layer.borderWidth= 1.0f;
            btnPlus.clipsToBounds = YES;
            [cell.contentView addSubview:btnPlus];
        }
        
        
        //    if (indexPath.row==selectedindex)
        //    {
        //        image.image = [UIImage imageNamed:@"img_forkblue.png"];
        //        btnPlus.hidden = NO;
        //        btnMinus.hidden= NO;
        //        labl.hidden= NO;
        //
        //
        //    }
        //    else{
        //        image.image = [UIImage imageNamed:@"img_fork.png"];
        //        btnPlus.hidden = YES;
        //        btnMinus.hidden= YES;
        //        labl.hidden= YES;
        //
        //
        //    }
        
        if([Arr_Temp containsObject:[ary_servelaterfilter objectAtIndex:indexPath.row]])
        {
            image.image = [UIImage imageNamed:@"img_forkblue.png"];
            btnPlus.hidden = NO;
            btnMinus.hidden= NO;
            labl.hidden= NO;
            imagViewPlusminus.hidden = NO;
            
        }
        else{
            image.image = [UIImage imageNamed:@"img_fork.png"];
            btnPlus.hidden = YES;
            btnMinus.hidden= YES;
            labl.hidden= YES;
            imagViewPlusminus.hidden = YES;
            
            
        }
        
        
        //    //    UIButton *btnPlus = [[UIButton alloc]init];
        //    UIButton *btnPlus = nil;
        //    //     btnPlus.frame=CGRectMake(self.view.frame.size.width-50, 10, 25, 25);
        //    if (IS_IPHONE_6Plus) {
        //        //            btnPlus.frame=CGRectMake(self.view.frame.size.width-50, 10, 25, 25);
        //        btnPlus.frame=CGRectMake(self.view.frame.size.width-80, 12, 30, 30);
        //
        //    }
        //    else if (IS_IPHONE_6)
        //    {
        //        btnPlus.frame=CGRectMake(self.view.frame.size.width-80, 11, 30, 30);
        //
        //    }
        //    else
        //    {
        //        btnPlus.frame=CGRectMake(self.view.frame.size.width-65, 10, 25, 25);
        //
        //    }
        //    btnPlus.tag = indexPath.row;
        //    //        btnPlus.backgroundColor = [UIColor redColor];
        //    [btnPlus addTarget:self action:@selector(click_BtnPlus:) forControlEvents:UIControlEventTouchUpInside];
        //    //        [cell.contentView addSubview:btnPlus];
        
        
        
        UIImageView *img_lineimage=[[UIImageView alloc]init];
        img_lineimage.frame=CGRectMake(10, 49, WIDTH-50, 0.5);
        if (IS_IPHONE_6Plus) {
            //        img_lineimage.frame=CGRectMake(10, 49, WIDTH-40, 0.5);
            img_lineimage.frame=CGRectMake(10, 55, WIDTH-60, 0.5);
            
        }else if (IS_IPHONE_6)
            
        {
            img_lineimage.frame=CGRectMake(10, 51, WIDTH-50, 0.5);
            
        }
        else{
            img_lineimage.frame=CGRectMake(10, 44, WIDTH-40, 0.5);
            
        }
        [img_lineimage setUserInteractionEnabled:YES];
        img_lineimage.backgroundColor=[UIColor clearColor];
        img_lineimage.image=[UIImage imageNamed:@"line1.png"];
        [cell.contentView addSubview:img_lineimage];
        
        
        
        
        
        cell.contentView.tag = indexPath.row;
        
    }
    
    else if (tableView == table_schedule)
    {
        
        
        UIView *cellView = [[UIView alloc]init];
        cellView.backgroundColor = [UIColor whiteColor];
        cellView.userInteractionEnabled = YES;
        cellView.tag = indexPath.row;
        [cell.contentView addSubview:cellView];
        
        UIImageView *imgViewTimer=[[UIImageView alloc]init];
        [imgViewTimer setUserInteractionEnabled:YES];
        imgViewTimer.image=[UIImage imageNamed:@"img_background@2x..png"];
        imgViewTimer.backgroundColor = [UIColor clearColor];
        //[cellView addSubview:imgViewTimer];
        
        
        UIImageView *timerImage1=[[UIImageView alloc]init ];
        [timerImage1 setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
        timerImage1.image = [UIImage imageNamed:@"icon-time.png"];
        [timerImage1 setUserInteractionEnabled:YES];
        [cellView addSubview:timerImage1];
        
        
        
        
        
        UILabel  * labl_NowTo = [[UILabel alloc]init];
        
        labl_NowTo.text = @"to";
        labl_NowTo.backgroundColor = [UIColor clearColor];
        labl_NowTo.textColor=[UIColor darkTextColor];
        //        labl_NowTo.textAlignment = NSTextAlignmentCenter;
        labl_NowTo.font = [UIFont fontWithName:kFont size:15];
        [cellView addSubview:labl_NowTo];
        
        
        UIImageView *timerImage2=[[UIImageView alloc]init];
        
        
        [timerImage2 setUserInteractionEnabled:YES];
        //    [timerImage2 setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
        timerImage2.image = [UIImage imageNamed:@"icon-time.png"];
        [cellView addSubview:timerImage2];
        
        
        UITextField*txt_fromtime;
        UIView* paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        txt_fromtime=[[UITextField alloc]init];
        [txt_fromtime setDelegate:self];
        [txt_fromtime setSecureTextEntry:NO];
        txt_fromtime.leftViewMode=UITextFieldViewModeAlways;
        txt_fromtime.placeholder=@"Start time";
        txt_fromtime.leftView = paddingView3;
        txt_fromtime.tag=-2;
        txt_fromtime.textColor=[UIColor blackColor];
        txt_fromtime.autocorrectionType = UITextAutocorrectionTypeNo;
        txt_fromtime.keyboardType = UIKeyboardTypeDefault;
        txt_fromtime.returnKeyType = UIReturnKeyDone;
        txt_fromtime.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        txt_fromtime.delegate = self;
        txt_fromtime.userInteractionEnabled=YES;
        txt_fromtime.clearButtonMode = UITextFieldViewModeWhileEditing;
        txt_fromtime.backgroundColor = [UIColor clearColor];
        txt_fromtime.text = [[ary_servingtimes objectAtIndex:indexPath.row]valueForKey:@"starttime"];
        [txt_fromtime setValue:[UIColor colorWithRed:85/255.0f green:85/255.0f blue:85/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
        [txt_fromtime setValue:[UIFont fontWithName:kFont size:14.0f] forKeyPath:@"_placeholderLabel.font"];
        txt_fromtime.font=[UIFont fontWithName:kFont size:14.0];
        [cellView addSubview:txt_fromtime];
        
        
        UITextField*txt_totime;
        UIView* paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        txt_totime=[[UITextField alloc]init];
        [txt_totime setDelegate:self];
        [txt_totime setSecureTextEntry:NO];
        txt_totime.leftViewMode=UITextFieldViewModeAlways;
        txt_totime.placeholder=@"To time";
        txt_totime.leftView = paddingView;
        txt_totime.tag = -3;
        txt_totime.textColor=[UIColor blackColor];
        txt_totime.autocorrectionType = UITextAutocorrectionTypeNo;
        txt_totime.keyboardType = UIKeyboardTypeDefault;
        txt_totime.returnKeyType = UIReturnKeyDone;
        txt_totime.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        txt_totime.delegate = self;
        txt_totime.userInteractionEnabled=YES;
        txt_totime.clearButtonMode = UITextFieldViewModeWhileEditing;
        txt_totime.backgroundColor = [UIColor clearColor];
        txt_totime.text = [[ary_servingtimes objectAtIndex:indexPath.row]valueForKey:@"endtime"];
        [txt_totime setValue:[UIColor colorWithRed:85/255.0f green:85/255.0f blue:85/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
        [txt_totime setValue:[UIFont fontWithName:kFont size:14.0f] forKeyPath:@"_placeholderLabel.font"];
        txt_totime.font=[UIFont fontWithName:kFont size:14.0];
        [cellView addSubview:txt_totime];
        
        
        
        
        
        UIView *SwipeView = [[UIView alloc]init];
        SwipeView.tag = -1001;
        SwipeView.backgroundColor=[UIColor clearColor];
        [SwipeView setUserInteractionEnabled:YES];
        [cell.contentView addSubview:SwipeView];
        
        
        UIButton *btn_Cancel  =[UIButton buttonWithType:UIButtonTypeCustom];
        [btn_Cancel setTag:indexPath.row];
        [btn_Cancel setImage:[UIImage imageNamed:@"cross-img@2x.png"] forState:UIControlStateNormal];
        btn_Cancel.backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.5];
        [btn_Cancel addTarget:self action:@selector(click_removeservetimingBtn:) forControlEvents:UIControlEventTouchUpInside];
        btn_Cancel.backgroundColor=[UIColor clearColor];
        [SwipeView addSubview:btn_Cancel];
        
        
        UISwipeGestureRecognizer* swipeGestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(cellSwipedLeft:)];
        [swipeGestureLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
        [cell addGestureRecognizer:swipeGestureLeft];
        
        UISwipeGestureRecognizer* swipeGestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(cellSwipedRight:)];
        [swipeGestureRight setDirection:UISwipeGestureRecognizerDirectionRight];
        [cell addGestureRecognizer:swipeGestureRight];
        
        //        img_swapImg = [[UIImageView alloc]initWithFrame: CGRectMake(0,0,900,129)];
        //        [img_swapImg setImage:[UIImage imageNamed:@"img_msgDelete@2x.png"]];
        //        [img_swapImg setUserInteractionEnabled:YES];
        //        [SwipeView addSubview:img_swapImg];
        
        
        
        if (IS_IPHONE_6) {
            SwipeView.frame = CGRectMake(0,0,320,52);
            btn_Cancel.frame=CGRectMake(450,0,30,cell.frame.size.height);
        }
        else if (IS_IPHONE_6Plus) {
            SwipeView.frame = CGRectMake(0,0,345,52);
            btn_Cancel.frame=CGRectMake(450,0,30,cell.frame.size.height);
            timerImage1.frame=CGRectMake(20, 15,25,25);
        }
        else
        {
            SwipeView.frame = CGRectMake(0,0,380,52);
            btn_Cancel.frame=CGRectMake(320,0,30,cell.frame.size.height);
        }
        
        
        //        if (IS_IPHONE_6Plus) {
        //            cellView.frame = CGRectMake(0, 0,WIDTH,56);
        //            imgViewTimer.frame = CGRectMake(0, 0,WIDTH,56);
        //            txt_fromtime.frame = CGRectMake(70,12, 85,30);
        //
        //            labl_NowTo.frame = CGRectMake(200, 12, 25,30);
        //            timerImage2.frame = CGRectMake(265, 15,25,25);
        //            txt_totime.frame = CGRectMake(315, 12, 100,30);
        //
        //
        //
        //        }else if (IS_IPHONE_6)
        //
        //        {
        //            cellView.frame = CGRectMake(0, 0,WIDTH,52);
        //            imgViewTimer.frame = CGRectMake(0, 0,WIDTH,52);
        //            timerImage1.frame=CGRectMake(20, 15, 25, 25);
        //            txt_fromtime.frame = CGRectMake(60,12, 85,30);
        //            labl_NowTo.frame = CGRectMake(180, 12, 25,30);
        //            timerImage2.frame = CGRectMake(225, 15,25,25);
        //            txt_totime.frame = CGRectMake(270, 12, 100,30);
        //        }
        //        else if (IS_IPHONE_5)
        //        {
        //            cellView.frame = CGRectMake(0, 0,WIDTH,45);
        //            imgViewTimer.frame = CGRectMake(0, 0,WIDTH,45);
        //            timerImage1.frame=CGRectMake(20, 15,20,20);
        //            txt_fromtime.frame = CGRectMake(55,10, 85,30);
        //            labl_NowTo.frame = CGRectMake(145,10, 25,30);
        //            timerImage2.frame = CGRectMake(185, 15,20,20);
        //            txt_totime.frame = CGRectMake(220, 10, 80,30);
        //        }
        //        else
        //        {
        //            cellView.frame = CGRectMake(0, 0,WIDTH,45);
        //            imgViewTimer.frame = CGRectMake(0, 0,WIDTH,45);
        //            timerImage1.frame=CGRectMake(20, 15,20,20);
        //            txt_fromtime.frame = CGRectMake(55,0, 85,20);
        //            labl_NowTo.frame = CGRectMake(145,10, 25,30);
        //            timerImage2.frame = CGRectMake(185, 15,20,20);
        //            txt_totime.frame = CGRectMake(220, 0, 80,20);
        //
        //        }
        
        cellView.frame = CGRectMake(0, 3,WIDTH,46);
        imgViewTimer.frame = CGRectMake(0, 3,WIDTH,46);
        timerImage1.frame=CGRectMake(20, 13,20,20);
        txt_fromtime.frame = CGRectMake(55,8, 85,30);
        labl_NowTo.frame = CGRectMake(145,8, 25,30);
        timerImage2.frame = CGRectMake(185,13,20,20);
        txt_totime.frame = CGRectMake(220,8, 80,30);
        
        
        [img_swapImg setHidden:YES];
        [btn_Cancel setHidden:YES];
        
        
        if ([indexPath isEqual:indexSelected])
        {
            
            CGRect rect=SwipeView.frame;
            rect.origin.x=-35;
            SwipeView.frame=rect;
            if (IS_IPHONE_6Plus) {
                //SwipeView.frame = CGRectMake(0,0,380,52);
                img_swapImg.frame = CGRectMake(0,0, IS_IPHONE_5?520:450, 52);
                btn_Cancel.frame=CGRectMake(320,0,30,cell.frame.size.height);
                
            }else if (IS_IPHONE_6)
                
            {
                //SwipeView.frame = CGRectMake(0,0,380,52);
                img_swapImg.frame = CGRectMake(0,0, IS_IPHONE_5?520:450, 52);
                btn_Cancel.frame=CGRectMake(320,0,30,cell.frame.size.height);
            }
            else if (IS_IPHONE_5)
            {
                // SwipeView.frame = CGRectMake(0,0,380,52);
                img_swapImg.frame = CGRectMake(0,0, IS_IPHONE_5?520:450, 65);
                btn_Cancel.frame=CGRectMake(320,0,30,cell.frame.size.height);
            }
            else
            {
                // SwipeView.frame = CGRectMake(0,0,380,52);
                img_swapImg.frame = CGRectMake(0,0, IS_IPHONE_5?520:450, 65);
                btn_Cancel.frame=CGRectMake(320,0,30,cell.frame.size.height);
                
            }
            
            //        img_swapImg = [[UIImageView alloc]initWithFrame: CGRectMake(0,0,900,129)];
            //        [img_swapImg setImage:[UIImage imageNamed:@"img_msgDelete@2x.png"]];
            //        [img_swapImg setUserInteractionEnabled:YES];
            //        [SwipeView addSubview:img_swapImg];

        
            
            [btn_Cancel setImage:[UIImage imageNamed:@"cross-img@2x.png"] forState:UIControlStateNormal];
            [btn_Cancel setImageEdgeInsets:UIEdgeInsetsMake(20, 8, 20, 8)];
            [img_swapImg setHidden:NO];
            [btn_Cancel setHidden:NO];
            [img_swapImg setUserInteractionEnabled:YES];
            
        }
        
        
        
        txt_fromtime.inputAccessoryView = keyboardToolbar_fromtime;
        txt_totime.inputAccessoryView = keyboardToolbar_fromtime;
        txt_fromtime.inputView = datePickerfrom;
        txt_totime.inputView = datePickerto;
        
    }
    
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == tableViewDishes)
    {
        if (IS_IPHONE_6Plus)
        {
            return 50;
        }
        else if (IS_IPHONE_6)
        {
            return 50;
        }
        else if (IS_IPHONE_5)
        {
            return 50;
        }
        else
        {
            return 50;
        }
    }
    else if (tableView == table_schedule)
    {
        return 55;
    }
    else{
        if (IS_IPHONE_6Plus) {
            return 52;
            
        }else if (IS_IPHONE_6)
            
        {
            return 52.0;
        }
        else if (IS_IPHONE_5)
        {
            return 52;
        }
        else
        {
            return 52;
        }
        
    }
    
    return 45;
}

-(void) timefromValueChanged:(UIButton *) sender
{
    
}
-(void) timetoValueChanged:(UIButton *) sender
{
    
    
    
    
}

-(void) click_removeservetimingBtn:(UIButton *) sender
{
    [ary_servingtimes removeObjectAtIndex:sender.tag];
    
    indexSelected = nil;
    [table_schedule reloadData];
    
    //    str_deletenotification = @"1";
    //    [self popup_Alertviewwithboth:@"Would you like to delete this notification?"];
    
    
}
#pragma mark -------------------------Swipe Function Start----------------------------------


- (void)cellSwipedLeft:(UIGestureRecognizer *)gestureRecognizer
{
    NSLog(@"Swipe left");
    table_schedule.userInteractionEnabled=YES;
    img_swapImg.userInteractionEnabled = YES;
    
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        UITableViewCell *cell = (UITableViewCell *)gestureRecognizer.view;
        NSIndexPath* indexPath = [table_schedule indexPathForCell:cell];
        
        if ([indexPath isEqual:indexSelected])
        {
            //[self showButtonForIndex:indexPath animated:YES];
            
        }
        else
        {
            
            //[self hideButtonForIndex:indexSelected animated:YES];
            [self showButtonForIndex:indexPath animated:YES];
            
            
           
        }
         [table_schedule reloadData];
    }
}


- (void)cellSwipedRight:(UIGestureRecognizer *)gestureRecognizer {
    
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        UITableViewCell *cell = (UITableViewCell *)gestureRecognizer.view;
        NSIndexPath* indexPath = [table_schedule indexPathForCell:cell];
        if ([indexPath isEqual:indexSelected])
        {
            [self hideButtonForIndex:indexSelected animated:YES];
            
            [table_schedule reloadData];
        }
    }
}


-(void) hideButtonForIndex:(NSIndexPath *) index animated:(BOOL) animated
{
    UITableViewCell *cell = [table_schedule cellForRowAtIndexPath:index];
    UIView *swipeview = [cell.contentView viewWithTag:-1001];
    CGRect rect = swipeview.frame;
    rect.origin.x = 0;
    if (animated)
    {
        [UIView animateWithDuration:0.3 animations:^{
            swipeview.frame = rect;
        }];
    }
    else
    {
        swipeview.frame = rect;
    }
    indexSelected = nil;
}


-(void) showButtonForIndex:(NSIndexPath *) index animated:(BOOL) animated{
    
    
    UITableViewCell *cell = [table_schedule cellForRowAtIndexPath:index];
    UIView *swipeview = [cell.contentView viewWithTag:-1001];
    swipeview.backgroundColor=[UIColor clearColor];
    
    CGRect rect = swipeview.frame;
    
    rect.origin.x = -100;
    
    
    if (animated)
    {
        [UIView animateWithDuration:0.3 animations:^{
            swipeview.frame = rect;
        }];
    }
    else{
        swipeview.frame = rect;
    }
    
    indexSelected = index;
}

#pragma mark -------------------------Swipe Function End----------------------------------



#pragma mark ---textFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField ==textfield_SearchDish)
    {
        if(textField.text)
        {
            search_leg = range.location+1;
        }
        else
        {
            search_leg=0;
        }
        
        
        if (ary_servelaterfilter)
        {
            [ary_servelaterfilter removeAllObjects];
        }
        
        
        for (NSMutableDictionary *dict in  ary_serverlaterlist)
        {
            
            NSString *search = [dict valueForKey:@"DishName"];
            
            if ([search length]>search_leg)
            {
                search = [search substringToIndex:search_leg];
            }
            
            NSString *substring = [NSString stringWithString:textField.text];
            substring = [substring stringByReplacingCharactersInRange:range withString:string];
            BOOL nameMatches = [[substring lowercaseString] hasPrefix:[string lowercaseString]];
            NSRange r = [search rangeOfString:substring options:NSCaseInsensitiveSearch];
            
            if(r.location != NSNotFound || nameMatches)
            {
                if (r.length > 0)
                {
                    [ary_servelaterfilter addObject:dict];
                }
            }
            
        }
        
        
        
        [tableViewDishes reloadData];
    }
    
    if (textField==txt_amount)
    {
        NSString *stringVal = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered =[[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        if (stringVal.length<4 && [string isEqualToString:filtered])
        {
            return YES;
        }
        else{
            return NO;
        }
        return [string isEqualToString:filtered];
        
    }
    
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField isEqual:textfield_SearchDish])
    {
        if ([textField.text isEqualToString:@""])
        {
            [ary_servelaterfilter removeAllObjects];
            
            for (int i=0; i<[ary_serverlaterlist count]; i++)
            {
                [ary_servelaterfilter addObject:[ary_serverlaterlist objectAtIndex:i]];
            }
            
        }
        else{
            if ([ary_servelaterfilter count]==0)
            {
                
                //[SVProgressHUD showErrorWithStatus:@"No result found"];
                textfield_SearchDish.text = @"";
            }
            
        }
        
        
    }
    else{
        
        
       
        
        
        if (textField.tag==-2) {
            
            for (int i=0; i<[ary_servingtimes count]; i++) {
                
                if (i == textField.superview.tag) {
                    
                    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                    
                    dict=[ary_servingtimes objectAtIndex:i];
                    
                    formatterfromtime = [[NSDateFormatter alloc] init];
                    [formatterfromtime setDateFormat:@"hh:mm aa"];
                    
                    //                    NSDate * nowDate = [NSDate date];
                    //                    NSDate * picDate = datePickerfrom.date;
                    date_SelectedDateFrom = datePickerfrom.date;
                    [dict setObject:[formatterfromtime stringFromDate:datePickerfrom.date] forKey:@"starttime"];
                    
                    [table_schedule reloadData];
                }
                
            }
            
            
        }
        
        if (textField.tag==-3) {
            
            for (int i=0; i<[ary_servingtimes count]; i++) {
                
                if (i == textField.superview.tag) {
                    
                    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                    
                    dict=[ary_servingtimes objectAtIndex:i];
                    
                    formattertotime = [[NSDateFormatter alloc] init];
                    [formattertotime setDateFormat:@"hh:mm aa"];
                    
                    
                    date_SelectedDateTo = datePickerto.date;
                    
                    [dict setObject:[formattertotime stringFromDate:datePickerto.date] forKey:@"endtime"];
                    
                    [table_schedule reloadData];
                }
                
            }
        }
        
    }
    

    
    [tableViewDishes reloadData];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([textField isEqual:textfield_SearchDish])
    {
        [tableViewDishes setHidden:NO];
        
    }
    else{
        
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    if ([textField isEqual:textfield_SearchDish])
    {
        [textfield_SearchDish resignFirstResponder];
        
    }
    else{
        
    }
    
    
    return YES;
}




-(void)AFServelaterlist
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    NSDictionary *params =@{
                            
                            @"uid"                              :   [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:Kservenowlist
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseServelaterlist:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFServelaterlist];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseServelaterlist :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    [ary_servelaterfilter removeAllObjects];
    [ary_serverlaterlist removeAllObjects];
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DishsList"] count]; i++)
        {
            [ary_actualarrayfromWS addObject:[[TheDict valueForKey:@"DishsList"] objectAtIndex:i]];
            
            // [ary_dummy addObject:[[TheDict valueForKey:@"DishsList"] objectAtIndex:i]];
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
            
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
            
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
            [dict setValue:@"0"  forKey:@"quantity"];
            [dict setValue:@""  forKey:@"starttime"];
            [dict setValue:@""  forKey:@"endtime"];
            [ary_serverlaterlist addObject:dict];
            
            
            NSMutableDictionary *dict1=[[NSMutableDictionary alloc] init];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Delivery_Address"]] forKey:@"Delivery_Address"];
            
            [dict1 setValue:@"0"  forKey:@"quantity"];
            [dict1 setValue:@""  forKey:@"starttime"];
            [dict1 setValue:@""  forKey:@"endtime"];
            [ary_servelaterfilter addObject:dict1];
            
            
            
            if ([ary_servelaterfilter count]>5)
            {
                imgViewSerchdish.hidden= NO;
                img_Dish.hidden = NO;
                btn_Search.hidden = NO;
                textfield_SearchDish.hidden= NO;
            }
            
            
            
        }
        
        
        
        [tableViewDishes reloadData];
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
    // new code
    Arr_Temp=[[NSMutableArray alloc] init];
    for(int i=0;i<ary_servelaterfilter.count;i++)
    {
        [Arr_Temp addObject:@""];
    }
    
    
}

-(void)AFOnrequestamount
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    
    NSDictionary *params =@{
                            @"uid"                         :   [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"onrequest_amount"            :   txt_amount.text,
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:Konrequestamount
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseAFOnrequestamount:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFOnrequestamount];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseAFOnrequestamount :(NSDictionary * )TheDict
{
    NSLog(@"amount: %@",TheDict);
    
    
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        ChefHomeViewController*vc = [ChefHomeViewController new];
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
    
    
}



-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
