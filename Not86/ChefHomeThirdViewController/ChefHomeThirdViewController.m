////
////  ChefHomeThirdViewController.m
////  Not86
////
////  Created by Interwld on 8/14/15.
////  Copyright (c) 2015 com.interworld. All rights reserved.
////
//
//#import "ChefHomeThirdViewController.h"
//
//@interface ChefHomeThirdViewController ()
//
//@end
//
//@implementation ChefHomeThirdViewController
//
//- (void)viewDidLoad {
//}
//-(void)customHeaderViewInitilization
//    {
//        imgViewTop=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,60)];
//        [imgViewTop setUserInteractionEnabled:YES];
//        imgViewTop.image=[UIImage imageNamed:@"img-head.png"];
//        [self.view addSubview:imgViewTop];
//        
//        UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(10, 18, 25, 25)];
//        [img_back setUserInteractionEnabled:YES];
//        img_back.backgroundColor=[UIColor clearColor];
//        img_back.image=[UIImage imageNamed:@"arrow.png"];
//        [imgViewTop addSubview:img_back];
//        
//        UILabel  * lbl_headingTitle = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(img_back.frame)+20,0, 220,60)];
//        lbl_headingTitle.text = @"Serve Now";
//        lbl_headingTitle.backgroundColor=[UIColor clearColor];
//        lbl_headingTitle.textColor=[UIColor whiteColor];
//        lbl_headingTitle.textAlignment=NSTextAlignmentLeft;
//        lbl_headingTitle.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewTop addSubview:lbl_headingTitle];
//        
//        
//        UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-50, 15, 35, 35)];
//        [img_logo setUserInteractionEnabled:YES];
//        img_logo.backgroundColor=[UIColor clearColor];
//        img_logo.image=[UIImage imageNamed:@"img-logo.png"];
//        [imgViewTop addSubview:img_logo];
//        
//    }
//    -(void)customInitilization
//    {
//        [self.view setBackgroundColor:[UIColor colorWithRed:245/255.0f green:247/255.0f blue:246/255.0f alpha:1.0f]];
//        
//        UILabel  * labl_serveType = [[UILabel alloc]initWithFrame:CGRectMake(0,imgViewTop.frame.origin.y+imgViewTop.frame.size.height+20, self.view.frame.size.width,25)];
//        labl_serveType.text = @"Serving Type";
//        labl_serveType.textColor=[UIColor grayColor];
//        
//        labl_serveType.backgroundColor=[UIColor clearColor];
//        labl_serveType.textAlignment=NSTextAlignmentCenter;
//        labl_serveType.font = [UIFont fontWithName:kFont size:20];
//        [self.view addSubview:labl_serveType];
//        
//        
//        UIImageView *imgViewServeTypeHedder=[[UIImageView alloc]initWithFrame:CGRectMake(0, labl_serveType.frame.origin.y+labl_serveType.frame.size.height+20,WIDTH,80)];
//        [imgViewServeTypeHedder setUserInteractionEnabled:YES];
//        [imgViewServeTypeHedder setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
//        [self.view addSubview:imgViewServeTypeHedder];
//        
//        UIButton *btn_DineIn = [[UIButton alloc] init];
//        btn_DineIn.frame = CGRectMake(20, 10, self.view.frame.size.width-40, 50);
//        [btn_DineIn setImage:[UIImage imageNamed:@"text buuton.png"] forState:UIControlStateNormal];
//        [imgViewServeTypeHedder addSubview:btn_DineIn];
//        
//        UILabel  * labl_DineIn = [[UILabel alloc]initWithFrame:CGRectMake(0,btn_DineIn.frame.origin.y+btn_DineIn.frame.size.height+10, self.view.frame.size.width,25)];
//        labl_DineIn.text = @"Dine-in";
//        labl_DineIn.textColor=[UIColor darkTextColor];
//        labl_DineIn.textAlignment = NSTextAlignmentCenter;
//        labl_DineIn.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewServeTypeHedder addSubview:labl_DineIn];
//        
//        UIButton *btn_Takeout = [[UIButton alloc] init];
//        btn_Takeout.frame = CGRectMake(self.view.frame.size.width/2-25, 10, 50, 50);
//        [btn_Takeout setImage:[UIImage imageNamed:@"text buuton.png"] forState:UIControlStateNormal];
//        [imgViewServeTypeHedder addSubview:btn_Takeout];
//        
//        UILabel  * labl_TakeOut = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-25,btn_Takeout.frame.origin.y+btn_Takeout.frame.size.height+10, self.view.frame.size.width,25)];
//        labl_TakeOut.text = @"Take-out";
//        labl_TakeOut.textColor=[UIColor darkTextColor];
//        labl_TakeOut.textAlignment = NSTextAlignmentCenter;
//        labl_TakeOut.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewServeTypeHedder addSubview:labl_TakeOut];
//        
//        UIButton *btn_Delivery = [[UIButton alloc] init];
//        btn_Delivery.frame = CGRectMake(self.view.frame.size.width-80, 10, 50, 50);
//        [btn_Delivery setImage:[UIImage imageNamed:@"text buuton.png"] forState:UIControlStateNormal];
//        [imgViewServeTypeHedder addSubview:btn_Delivery];
//        
//        UILabel  * labl_Delivery = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width-80,btn_Delivery.frame.origin.y+btn_Delivery.frame.size.height+20, self.view.frame.size.width,25)];
//        labl_Delivery.text = @"Delivery";
//        labl_Delivery.textColor=[UIColor darkTextColor];
//        labl_Delivery.textAlignment = NSTextAlignmentCenter;
//        labl_Delivery.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewServeTypeHedder addSubview:labl_Delivery];
//        
//        UITextField *textfield_CheckBox = [[UITextField alloc]initWithFrame:CGRectMake(self.view.frame.size.width-100, 10, 10, 10)];
//        textfield_CheckBox.layer.borderWidth = 3.0f;
//        textfield_CheckBox.layer.borderColor = (__bridge CGColorRef)([UIColor blackColor]);
//        [self.view addSubview:textfield_CheckBox];
//        
//        
//        
//        UILabel  * labl_SelectServeTime = [[UILabel alloc]initWithFrame:CGRectMake(textfield_CheckBox.frame.origin.y+30,textfield_CheckBox.frame.origin.y, 100,25)];
//        labl_SelectServeTime.text = @"apply to all";
//        labl_SelectServeTime.textColor=[UIColor darkTextColor];
//        labl_SelectServeTime.textAlignment = NSTextAlignmentCenter;
//        labl_SelectServeTime.font = [UIFont fontWithName:@"Arial" size:20];
//        [self.view addSubview:labl_SelectServeTime];
//        
//        
//        UIImageView *imgViewBGSpicyNugget=[[UIImageView alloc]initWithFrame:CGRectMake(0, labl_serveType.frame.origin.y+labl_serveType.frame.size.height+20,WIDTH,50)];
//        [imgViewBGSpicyNugget setUserInteractionEnabled:YES];
//        [imgViewBGSpicyNugget setBackgroundColor:[UIColor colorWithRed:211/255.0f green:228/255.0f blue:228/255.0f alpha:1.0f]];
//        [self.view addSubview:imgViewServeTypeHedder];
//        
//        UILabel  * labl_SpicyNugget = [[UILabel alloc]initWithFrame:CGRectMake(0,10, 200,25)];
//        labl_SpicyNugget.text = @"Spicy Nugget       X4";
//        labl_SpicyNugget.textColor=[UIColor darkTextColor];
//        labl_SpicyNugget.textAlignment = NSTextAlignmentCenter;
//        labl_SpicyNugget.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewBGSpicyNugget addSubview:labl_SpicyNugget];
//        
//        UIButton *btn_DropDown = [[UIButton alloc] init];
//        btn_DropDown.frame = CGRectMake(self.view.frame.size.width-80, 10, 50, 50);
//        [btn_DropDown setImage:[UIImage imageNamed:@"text buuton.png"] forState:UIControlStateNormal];
//        [imgViewBGSpicyNugget addSubview:btn_DropDown];
//        
//        
//        UILabel  * labl_SelectServeTime2 = [[UILabel alloc]initWithFrame:CGRectMake(0,imgViewBGSpicyNugget.frame.origin.y+imgViewBGSpicyNugget.frame.size.height+20, self.view.frame.size.width,25)];
//        labl_SelectServeTime2.text = @"Serving Type";
//        labl_SelectServeTime2.textColor=[UIColor darkTextColor];
//        labl_SelectServeTime2.textAlignment = NSTextAlignmentCenter;
//        labl_SelectServeTime2.font = [UIFont fontWithName:@"Arial" size:20];
//        [self.view addSubview:labl_SelectServeTime2];
//        
//        
//        
//        UIImageView *imgViewServeTypeHedder2=[[UIImageView alloc]initWithFrame:CGRectMake(0, labl_SelectServeTime2.frame.origin.y+labl_serveType.frame.size.height+20,WIDTH,80)];
//        [imgViewServeTypeHedder2 setUserInteractionEnabled:YES];
//        [imgViewServeTypeHedder2 setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
//        [self.view addSubview:imgViewServeTypeHedder2];
//        
//        UIButton *btn_DineIn2 = [[UIButton alloc] init];
//        btn_DineIn2.frame = CGRectMake(20, 10, self.view.frame.size.width-40, 50);
//        [btn_DineIn2 setImage:[UIImage imageNamed:@"text buuton.png"] forState:UIControlStateNormal];
//        [imgViewServeTypeHedder2 addSubview:btn_DineIn2];
//        
//        UILabel  * labl_DineIn2 = [[UILabel alloc]initWithFrame:CGRectMake(0,btn_DineIn.frame.origin.y+btn_DineIn.frame.size.height+10, self.view.frame.size.width,25)];
//        labl_DineIn2.text = @"Dine-in";
//        labl_DineIn2.textColor=[UIColor darkTextColor];
//        labl_DineIn2.textAlignment = NSTextAlignmentCenter;
//        labl_DineIn2.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewServeTypeHedder2 addSubview:labl_DineIn2];
//        
//        UIButton *btn_Takeout2 = [[UIButton alloc] init];
//        btn_Takeout2.frame = CGRectMake(self.view.frame.size.width/2-25, 10, 50, 50);
//        [btn_Takeout2 setImage:[UIImage imageNamed:@"text buuton.png"] forState:UIControlStateNormal];
//        [imgViewServeTypeHedder2 addSubview:btn_Takeout2];
//        
//        UILabel  * labl_TakeOut2 = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-25,btn_Takeout.frame.origin.y+btn_Takeout.frame.size.height+10, self.view.frame.size.width,25)];
//        labl_TakeOut2.text = @"Take-out";
//        labl_TakeOut2.textColor=[UIColor darkTextColor];
//        labl_TakeOut2.textAlignment = NSTextAlignmentCenter;
//        labl_TakeOut2.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewServeTypeHedder2 addSubview:labl_TakeOut2];
//        
//        UIButton *btn_Delivery2 = [[UIButton alloc] init];
//        btn_Delivery2.frame = CGRectMake(self.view.frame.size.width-80, 10, 50, 50);
//        [btn_Delivery2 setImage:[UIImage imageNamed:@"text buuton.png"] forState:UIControlStateNormal];
//        [imgViewServeTypeHedder2 addSubview:btn_Delivery2];
//        
//        UILabel  * labl_Delivery2 = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width-80,btn_Delivery.frame.origin.y+btn_Delivery.frame.size.height+20, self.view.frame.size.width,25)];
//        labl_Delivery2.text = @"Delivery";
//        labl_Delivery2.textColor=[UIColor darkTextColor];
//        labl_Delivery2.textAlignment = NSTextAlignmentCenter;
//        labl_Delivery2.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewServeTypeHedder2 addSubview:labl_Delivery2];
//        
//        
//        
//        UILabel  * labl_DeliverHeder = [[UILabel alloc]initWithFrame:CGRectMake(0,imgViewServeTypeHedder2.frame.origin.y+imgViewServeTypeHedder2.frame.size.height+20, self.view.frame.size.width,25)];
//        labl_DeliverHeder.text = @"Delivery";
//        labl_DeliverHeder.textColor=[UIColor darkTextColor];
//        labl_DeliverHeder.textAlignment = NSTextAlignmentCenter;
//        labl_DeliverHeder.font = [UIFont fontWithName:@"Arial" size:20];
//        [self.view addSubview:labl_DeliverHeder];
//        
//        
//        UIImageView *imgViewServeTypeHedder3=[[UIImageView alloc]initWithFrame:CGRectMake(0, labl_SelectServeTime2.frame.origin.y+labl_serveType.frame.size.height+20,WIDTH,300)];
//        [imgViewServeTypeHedder3 setUserInteractionEnabled:YES];
//        [imgViewServeTypeHedder3 setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
//        [self.view addSubview:imgViewServeTypeHedder3];
//        
//        UILabel  * labl_DeliveryCompanyName = [[UILabel alloc]initWithFrame:CGRectMake(20,10, self.view.frame.size.width-40,25)];
//        labl_DeliveryCompanyName.text = @"Delivery company Name";
//        labl_DeliveryCompanyName.textColor=[UIColor darkTextColor];
//        labl_DeliveryCompanyName.textAlignment = NSTextAlignmentCenter;
//        labl_DeliveryCompanyName.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewServeTypeHedder3 addSubview:labl_DeliveryCompanyName];
//        
//        
//        UITextField *textfield_DeliveryName = [[UITextField alloc]initWithFrame:CGRectMake(20, labl_DeliveryCompanyName.frame.origin.y+35, self.view.frame.size.width-40, 22)];
//        textfield_DeliveryName.placeholder = @"Timo's Delivery";
//        [imgViewServeTypeHedder3 addSubview:textfield_DeliveryName];
//        
//        
//        UIImageView *line1=[[UIImageView alloc]initWithFrame:CGRectMake(20, textfield_DeliveryName.frame.origin.y+22,WIDTH-40,300)];
//        [line1 setUserInteractionEnabled:YES];
//        [line1 setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
//        [imgViewServeTypeHedder3 addSubview:line1];
//        
//        
//        UILabel  * labl_DeliveryCompanyPhone= [[UILabel alloc]initWithFrame:CGRectMake(20,line1.frame.origin.y+10, self.view.frame.size.width-40,25)];
//        labl_DeliveryCompanyPhone.text = @"Delivery company Name";
//        labl_DeliveryCompanyPhone.textColor=[UIColor darkTextColor];
//        labl_DeliveryCompanyPhone.textAlignment = NSTextAlignmentCenter;
//        labl_DeliveryCompanyPhone.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewServeTypeHedder3 addSubview:labl_DeliveryCompanyPhone];
//        
//        
//        UITextField *textfield_StdCode = [[UITextField alloc]initWithFrame:CGRectMake(20, labl_DeliveryCompanyPhone.frame.origin.y+35, 25, 25)];
//        textfield_StdCode.placeholder = @"+01";
//        [imgViewServeTypeHedder3 addSubview:textfield_StdCode];
//        
//        UITextField *textfield_PhoneNum = [[UITextField alloc]initWithFrame:CGRectMake(textfield_StdCode.frame.origin.x+45, labl_DeliveryCompanyPhone.frame.origin.y+35, 25, 25)];
//        textfield_PhoneNum.placeholder = @"890 374 273";
//        [imgViewServeTypeHedder3 addSubview:textfield_PhoneNum];
//        
//        
//        UIImageView *lineStdCode =[[UIImageView alloc]initWithFrame:CGRectMake(45, textfield_StdCode.frame.origin.y+22,WIDTH-90,300)];
//        [lineStdCode setUserInteractionEnabled:YES];
//        [lineStdCode setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
//        [imgViewServeTypeHedder3 addSubview:lineStdCode];
//        
//        UIImageView *linePhnum =[[UIImageView alloc]initWithFrame:CGRectMake(45, textfield_StdCode.frame.origin.y+22,WIDTH-90,300)];
//        [linePhnum setUserInteractionEnabled:YES];
//        [linePhnum setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
//        [imgViewServeTypeHedder3 addSubview:linePhnum];
//        
//        
//        
//        UILabel  * labl_DeliveryPersonName = [[UILabel alloc]initWithFrame:CGRectMake(20,linePhnum.frame.origin.y+10, self.view.frame.size.width-40,25)];
//        labl_DeliveryPersonName.text = @"Delivery person Name";
//        labl_DeliveryPersonName.textColor=[UIColor darkTextColor];
//        labl_DeliveryPersonName.textAlignment = NSTextAlignmentCenter;
//        labl_DeliveryPersonName.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewServeTypeHedder3 addSubview:labl_DeliveryPersonName];
//        
//        
//        UITextField *textfield_DeliveryPersonName = [[UITextField alloc]initWithFrame:CGRectMake(20, labl_DeliveryPersonName.frame.origin.y+35, self.view.frame.size.width-40, 22)];
//        textfield_DeliveryPersonName.placeholder = @"Kendrik timotty";
//        [imgViewServeTypeHedder3 addSubview:textfield_DeliveryPersonName];
//        
//        
//        UIImageView *linePersonName =[[UIImageView alloc]initWithFrame:CGRectMake(20, textfield_DeliveryName.frame.origin.y+22,WIDTH-40,300)];
//        [linePersonName setUserInteractionEnabled:YES];
//        [linePersonName setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
//        [imgViewServeTypeHedder3 addSubview:linePersonName];
//        
//        
//        // delivery Person Mobile
//        UILabel  * labl_DeliveryPersonMobile= [[UILabel alloc]initWithFrame:CGRectMake(20,line1.frame.origin.y+10, self.view.frame.size.width-40,25)];
//        labl_DeliveryPersonMobile.text = @"Delivery Person Mobile";
//        labl_DeliveryPersonMobile.textColor=[UIColor darkTextColor];
//        labl_DeliveryPersonMobile.textAlignment = NSTextAlignmentCenter;
//        labl_DeliveryPersonMobile.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewServeTypeHedder3 addSubview:labl_DeliveryPersonMobile];
//        
//        
//        UITextField *textfield_DeliveryPersonStdCode = [[UITextField alloc]initWithFrame:CGRectMake(20, labl_DeliveryCompanyPhone.frame.origin.y+35, 25, 25)];
//        textfield_StdCode.placeholder = @"+01";
//        [imgViewServeTypeHedder3 addSubview:textfield_StdCode];
//        
//        UITextField *textfield_DeliveryPersonPhoneNum = [[UITextField alloc]initWithFrame:CGRectMake(textfield_StdCode.frame.origin.x+45, labl_DeliveryCompanyPhone.frame.origin.y+35, 25, 25)];
//        textfield_PhoneNum.placeholder = @"890 374 273";
//        [imgViewServeTypeHedder3 addSubview:textfield_PhoneNum];
//        
//        
//        UIImageView *lineDeliveryPersonStdCode =[[UIImageView alloc]initWithFrame:CGRectMake(45, textfield_StdCode.frame.origin.y+22,WIDTH-90,300)];
//        [lineDeliveryPersonStdCode setUserInteractionEnabled:YES];
//        [lineDeliveryPersonStdCode setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
//        [imgViewServeTypeHedder3 addSubview:lineDeliveryPersonStdCode];
//        
//        UIImageView *lineDeliveryPersonPhnum =[[UIImageView alloc]initWithFrame:CGRectMake(45, textfield_StdCode.frame.origin.y+22,WIDTH-90,300)];
//        [lineDeliveryPersonPhnum setUserInteractionEnabled:YES];
//        [lineDeliveryPersonPhnum setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
//        [imgViewServeTypeHedder3 addSubview:lineDeliveryPersonPhnum];
//        
//        
//        UILabel  * labl_DeliveryType= [[UILabel alloc]initWithFrame:CGRectMake(20,lineDeliveryPersonPhnum.frame.origin.y+10, self.view.frame.size.width-40,25)];
//        labl_DeliveryType.text = @"Standard Delivery Charge";
//        labl_DeliveryType.textColor=[UIColor darkTextColor];
//        labl_DeliveryType.textAlignment = NSTextAlignmentCenter;
//        labl_DeliveryType.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewServeTypeHedder3 addSubview:labl_DeliveryType];
//        
//        
//        UIButton *btn_MileCharge = [[UIButton alloc]initWithFrame:CGRectMake(20, labl_DeliveryType.frame.origin.y+35, 10, 10)];
//        btn_MileCharge.backgroundColor = [UIColor yellowColor];
//        [btn_MileCharge setBackgroundImage:[UIImage imageNamed:@"dishes.png"] forState:UIControlStateNormal];
//        [imgViewServeTypeHedder3 addSubview:btn_MileCharge];
//        
//        UIButton *btn_FlatCharge = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-10, labl_DeliveryType.frame.origin.y+35, 10, 10)];
//        btn_FlatCharge.backgroundColor = [UIColor yellowColor];
//        [btn_FlatCharge setBackgroundImage:[UIImage imageNamed:@"dishes.png"] forState:UIControlStateNormal];
//        [imgViewServeTypeHedder3 addSubview:btn_FlatCharge];
//        
//        
//        UILabel  * labl_MileCharge= [[UILabel alloc]initWithFrame:CGRectMake(btn_MileCharge.frame.origin.x+10,btn_MileCharge.frame.origin.y, 100,25)];
//        labl_DeliveryType.text = @"Mile Charge";
//        labl_DeliveryType.textColor=[UIColor darkTextColor];
//        labl_DeliveryType.textAlignment = NSTextAlignmentCenter;
//        labl_DeliveryType.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewServeTypeHedder3 addSubview:labl_DeliveryType];
//        
//        
//        UILabel  * labl_FlatCharge= [[UILabel alloc]initWithFrame:CGRectMake(btn_FlatCharge.frame.origin.x+10,lineDeliveryPersonPhnum.frame.origin.y+10, self.view.frame.size.width-40,25)];
//        labl_DeliveryType.text = @"Flat Charge";
//        labl_DeliveryType.textColor=[UIColor darkTextColor];
//        labl_DeliveryType.textAlignment = NSTextAlignmentCenter;
//        labl_DeliveryType.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewServeTypeHedder3 addSubview:labl_DeliveryType];
//        
//        
//        
//        UILabel  * labl_FirstMileCharge= [[UILabel alloc]initWithFrame:CGRectMake(20,labl_FlatCharge.frame.origin.y+10, 100,25)];
//        labl_FirstMileCharge.text = @"First mile : $";
//        labl_FirstMileCharge.textColor=[UIColor darkTextColor];
//        labl_FirstMileCharge.textAlignment = NSTextAlignmentCenter;
//        labl_FirstMileCharge.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewServeTypeHedder3 addSubview:labl_FirstMileCharge];
//        
//        UITextField *textfield_FirstMileCharge = [[UITextField alloc]initWithFrame:CGRectMake(labl_FirstMileCharge.frame.origin.x+110, labl_FirstMileCharge.frame.origin.y+35, 100, 25)];
//        textfield_FirstMileCharge.placeholder = @"10 $";
//        [imgViewServeTypeHedder3 addSubview:textfield_FirstMileCharge];
//        
//        
//        UILabel  * labl_Additionalmile= [[UILabel alloc]initWithFrame:CGRectMake(20,labl_FirstMileCharge.frame.origin.y+10, 100,25)];
//        labl_Additionalmile.text = @"Each Additional mile : $";
//        labl_Additionalmile.textColor=[UIColor darkTextColor];
//        labl_Additionalmile.textAlignment = NSTextAlignmentCenter;
//        labl_Additionalmile.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewServeTypeHedder3 addSubview:labl_Additionalmile];
//        
//        UITextField *textfield_Additionalmile = [[UITextField alloc]initWithFrame:CGRectMake(125, labl_Additionalmile.frame.origin.y+35, 100, 25)];
//        textfield_Additionalmile.placeholder = @"5 $";
//        [imgViewServeTypeHedder3 addSubview:textfield_Additionalmile];
//        
//        
//        
//        UILabel  * labl_FlatChargecharge = [[UILabel alloc]initWithFrame:CGRectMake(20,labl_Additionalmile.frame.origin.y+10, 100,25)];
//        labl_FlatChargecharge.text = @"Flat Charge: $";
//        labl_FlatChargecharge.textColor=[UIColor darkTextColor];
//        labl_FlatChargecharge.textAlignment = NSTextAlignmentCenter;
//        labl_FlatChargecharge.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewServeTypeHedder3 addSubview:labl_FlatChargecharge];
//        
//        
//        UITextField *textfield_FlatChargecharge = [[UITextField alloc]initWithFrame:CGRectMake(labl_FlatChargecharge.frame.origin.x+120, labl_DeliveryCompanyPhone.frame.origin.y+35, 100, 25)];
//        textfield_FlatChargecharge.placeholder = @"5 $";
//        [imgViewServeTypeHedder3 addSubview:textfield_FlatChargecharge];
//        
//        
//        
//        
//        UITextField *textfield_ApplyToAllTextBox = [[UITextField alloc]initWithFrame:CGRectMake(100, textfield_FlatChargecharge.frame.origin.y+35, 25, 25)];
//        textfield_ApplyToAllTextBox.layer.borderWidth = 3.0f;
//        textfield_ApplyToAllTextBox.layer.borderColor = (__bridge CGColorRef)([UIColor blackColor]);
//        [imgViewServeTypeHedder3 addSubview:textfield_ApplyToAllTextBox];
//        
//        UILabel  * labl_ApplyToAll = [[UILabel alloc]initWithFrame:CGRectMake(textfield_ApplyToAllTextBox.frame.origin.x+35,textfield_ApplyToAllTextBox.frame.origin.y+10, 100,25)];
//        labl_ApplyToAll.text = @"Flat Charge: $";
//        labl_ApplyToAll.textColor=[UIColor darkTextColor];
//        labl_ApplyToAll.textAlignment = NSTextAlignmentCenter;
//        labl_ApplyToAll.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewServeTypeHedder3 addSubview:labl_FlatChargecharge];
//        
//        
//        
//        UITextField *textfield_DefaultTextBox = [[UITextField alloc]initWithFrame:CGRectMake(self.view.frame.size.width-150, textfield_FlatChargecharge.frame.origin.y+35, 25, 25)];
//        textfield_DefaultTextBox.layer.borderWidth = 3.0f;
//        textfield_DefaultTextBox.layer.borderColor = (__bridge CGColorRef)([UIColor blackColor]);
//        [imgViewServeTypeHedder3 addSubview:textfield_DefaultTextBox];
//        
//        UILabel  * labl_DefaultTextBox = [[UILabel alloc]initWithFrame:CGRectMake(textfield_DefaultTextBox.frame.origin.x+35,textfield_DefaultTextBox.frame.origin.y+10, 100,25)];
//        labl_DefaultTextBox.text = @"Flat Charge: $";
//        labl_DefaultTextBox.textColor=[UIColor darkTextColor];
//        labl_DefaultTextBox.textAlignment = NSTextAlignmentCenter;
//        labl_DefaultTextBox.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewServeTypeHedder3 addSubview:labl_FlatChargecharge];
//        
//        
//        
//        
//        UIImageView *imgViewBGCurryChicken =[[UIImageView alloc]initWithFrame:CGRectMake(0, imgViewServeTypeHedder3.frame.origin.y+imgViewServeTypeHedder3.frame.size.height+20,WIDTH,50)];
//        [imgViewBGCurryChicken setUserInteractionEnabled:YES];
//        [imgViewBGCurryChicken setBackgroundColor:[UIColor colorWithRed:211/255.0f green:228/255.0f blue:228/255.0f alpha:1.0f]];
//        [self.view addSubview:imgViewBGCurryChicken];
//        
//        UILabel  * labl_CurryChicken = [[UILabel alloc]initWithFrame:CGRectMake(0,10, 200,25)];
//        labl_CurryChicken.text = @"Curry Chicken       X4";
//        labl_CurryChicken.textColor=[UIColor darkTextColor];
//        labl_CurryChicken.textAlignment = NSTextAlignmentCenter;
//        labl_CurryChicken.font = [UIFont fontWithName:@"Arial" size:20];
//        [imgViewBGCurryChicken addSubview:labl_CurryChicken];
//        
//        UIButton *btn_DropDownCurryChicken = [[UIButton alloc] init];
//        btn_DropDownCurryChicken.frame = CGRectMake(self.view.frame.size.width-80, 10, 50, 50);
//        [btn_DropDownCurryChicken setImage:[UIImage imageNamed:@"text buuton.png"] forState:UIControlStateNormal];
//        [imgViewBGCurryChicken addSubview:btn_DropDownCurryChicken];
//        
//        
//        
//        
//        
//        UIButton *next = [[UIButton alloc] init];
//        next.frame = CGRectMake(20, self.view.frame.size.height-50, self.view.frame.size.width-40, 40);
//        next.backgroundColor = [UIColor brownColor];
//        next.layer.cornerRadius=4.0f;
//        [next setTitle:@"Serve Now" forState:UIControlStateNormal];
//        [next setImage:[UIImage imageNamed:@"text buuton.png"] forState:UIControlStateNormal];
//        [next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [next addTarget:self action:@selector(next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
//        [self.view addSubview:next];
//        
//        /*
//         tableViewDishes = [[UITableView alloc]initWithFrame:CGRectMake(0,imgViewSerchdish.frame.origin.y+imgViewSerchdish.frame.size.height+20, self.view.frame.size.width,250)];
//         tableViewDishes.delegate = self;
//         tableViewDishes.dataSource = self;
//         [self.view addSubview:tableViewDishes];  */
//        
//
//    [super viewDidLoad];
//    // Do any additional setup after loading the view.
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
///*
//#pragma mark - Navigation
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}
//*/
//
//@end
