//
//  ChefServeLaterSecondVC.h
//  Not86
//
//  Created by Interworld on 20/11/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CKCalendarView.h"

@interface ChefServeLaterSecondVC : UIViewController
@property(nonatomic, strong) CKCalendarView *calendar;
@property(nonatomic, strong) NSMutableArray *enabledDates;
@property(nonatomic,strong)NSMutableArray*ary_servelisting;
@property(nonatomic,strong)NSMutableArray*ary_mainfromWS;

@property(nonatomic,strong)NSString*str_time;
@end
