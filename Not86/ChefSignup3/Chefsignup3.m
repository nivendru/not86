//
//  Chefsignup3.m
//  Not86
//
//  Created by Interwld on 8/12/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "Chefsignup3.h"
#import "chefsignup4.h"
#import "AppDelegate.h"
#import "Define.h"

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"


@interface Chefsignup3 ()<UIScrollViewDelegate,UITextViewDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>{
    UIImageView *img_topbar;
    UIImageView *img_BackgroundImg;
    UIScrollView *scrollview3;
    UIImageView *dinein_picture;
    UIImageView *picture1;
    UITextField *txt_seats;
    AppDelegate *delegate;
    NSString *str_status;
    UIImageView *button_img;
    UIImageView*buttonimg;
    UIView *alertviewBg;
    NSData*imgData;
    NSString*str_pickingImage;
    
    int ABC;
    NSMutableArray *ary_temp;
    UIButton *btn_Delivery;
    UIButton *btn_Takeout;
    UIButton *btn_Dinein;
    NSMutableString*str_ServiceType;
    CGFloat	animatedDistance;
    UIImageView *img_backgroundimage2;
    UILabel  *lbl_dineinSeats;
    
    
    
    
}


@end

@implementation Chefsignup3
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self IntegrateHeaderDesign];
    [self IntegrateBodyDesign];
    str_pickingImage = [NSString new];
    ary_temp=[[NSMutableArray alloc]init];
    
    
    str_status=[[NSString alloc]init];
    str_status=@"";
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
}
-(void)IntegrateHeaderDesign{
    
    
    img_topbar=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,45)];
    [img_topbar setUserInteractionEnabled:YES];
    img_topbar.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:img_topbar];
    
    
    UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(12, 16, 15, 15)];
    [img_back setUserInteractionEnabled:YES];
    img_back.backgroundColor=[UIColor clearColor];
    img_back.image=[UIImage imageNamed:@"arrow@2x.png"];
    [img_topbar addSubview:img_back];
    
    UILabel  * lbl_heading = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(img_back.frame)+20,0, 220,45)];
    lbl_heading.text = @"Chef Application";
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:kFont size:15];
    [img_topbar addSubview:lbl_heading];
    
    UIButton *btn_back = [[UIButton alloc] init];
    btn_back.frame = CGRectMake(0, 0, 60,35);
    btn_back.backgroundColor = [UIColor clearColor];
    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_topbar addSubview:btn_back];
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 7, 30, 30)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-login-logo@2x.png"];
    [img_topbar addSubview:img_logo];
    
    scrollview3=[[UIScrollView alloc]init];
    scrollview3.frame =CGRectMake(0, 45, WIDTH, HEIGHT-80);
    [scrollview3 setShowsVerticalScrollIndicator:NO];
    scrollview3.delegate = self;
    scrollview3.scrollEnabled = YES;
    scrollview3.showsVerticalScrollIndicator = NO;
    [scrollview3 setUserInteractionEnabled:YES];
    scrollview3.backgroundColor = [UIColor colorWithRed:241/255.0f green:244/255.0f blue:242/255.0f alpha:1];
    [self.view addSubview:scrollview3];
    [scrollview3 setContentSize:CGSizeMake(0,HEIGHT)];
    
    
    
}
-(void)Back_btnClick{
    //    [self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
    
    
}
-(void)IntegrateBodyDesign

{
    
    
    UIImageView *img_backgroundimage=[[UIImageView alloc]init];
    [img_backgroundimage setUserInteractionEnabled:YES];
    img_backgroundimage.backgroundColor=[UIColor clearColor];
    img_backgroundimage.image=[UIImage imageNamed:@"bg1-img@2x.png"];
    [scrollview3 addSubview:img_backgroundimage];
    
    UILabel  * lbl_propose = [[UILabel alloc]init];
    lbl_propose.text = @"How do you propose to serve?";
    lbl_propose.backgroundColor=[UIColor clearColor];
    lbl_propose.textColor=[UIColor blackColor];
    lbl_propose.numberOfLines = 0;
    lbl_propose.font = [UIFont fontWithName:kFontBold size:15];
    [img_backgroundimage addSubview:lbl_propose];
    
    btn_Dinein = [[UIButton alloc] init];
    btn_Dinein.backgroundColor = [UIColor clearColor];
    btn_Dinein.tag=54;
    [btn_Dinein setImage:[UIImage imageNamed:@"img 2.png"] forState:UIControlStateNormal];
    [btn_Dinein addTarget:self action:@selector(click_selectObjectAt:) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimage addSubview:btn_Dinein];
    
    
    
    
    UILabel  * lbl_Dinein = [[UILabel alloc]init];
    lbl_Dinein.text = @"Dine-in -";
    lbl_Dinein.backgroundColor=[UIColor clearColor];
    lbl_Dinein.textColor=[UIColor blackColor];
    lbl_Dinein.numberOfLines = 0;
    lbl_Dinein.font = [UIFont fontWithName:kFont size:12];
    [img_backgroundimage addSubview:lbl_Dinein];
    
    
    UILabel  * lbl_customereats = [[UILabel alloc]init];
    lbl_customereats.text = @" Customer eats at your dining table";
    lbl_customereats.backgroundColor=[UIColor clearColor];
    lbl_customereats.textColor=[UIColor blackColor];
    lbl_customereats.textAlignment = NSTextAlignmentLeft;
    lbl_customereats.numberOfLines = 0;
    lbl_customereats.font = [UIFont fontWithName:kFont size:10];
    [img_backgroundimage addSubview:lbl_customereats];
    
    
    
    btn_Takeout = [[UIButton alloc] init];
    btn_Takeout.backgroundColor = [UIColor clearColor];
    btn_Takeout.tag=55;
    [btn_Takeout setImage:[UIImage imageNamed:@"img 2.png"] forState:UIControlStateNormal];
    [btn_Takeout addTarget:self action:@selector(click_selectObjectAt:) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimage addSubview:btn_Takeout];
    
    
    
    UILabel  * lbl_Takeout = [[UILabel alloc]init];
    lbl_Takeout.text = @"Take-out - ";
    lbl_Takeout.backgroundColor=[UIColor clearColor];
    lbl_Takeout.textColor=[UIColor blackColor];
    lbl_Takeout.numberOfLines = 0;
    lbl_Takeout.font = [UIFont fontWithName:kFont size:12];
    [img_backgroundimage addSubview:lbl_Takeout];
    
    UILabel  * lbl_customer = [[UILabel alloc]init];
    lbl_customer.text = @"Customer collects food from you";
    lbl_customer.backgroundColor=[UIColor clearColor];
    lbl_customer.textColor=[UIColor blackColor];
    lbl_customer.textAlignment = NSTextAlignmentLeft;
    
    lbl_customer.numberOfLines = 0;
    lbl_customer.font = [UIFont fontWithName:kFont size:10];
    [img_backgroundimage addSubview:lbl_customer];
    
    
    
    btn_Delivery = [[UIButton alloc] init];
    btn_Delivery.backgroundColor = [UIColor clearColor];
    [btn_Delivery setImage:[UIImage imageNamed:@"img 2.png"] forState:UIControlStateNormal];
    btn_Delivery.tag=56;
    [btn_Delivery addTarget:self action:@selector(click_selectObjectAt:) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimage addSubview:btn_Delivery];
    
    
    
    UILabel  * lbl_Delivery = [[UILabel alloc]init];
    lbl_Delivery.text = @"Delivery -";
    lbl_Delivery.backgroundColor=[UIColor clearColor];
    lbl_Delivery.textColor=[UIColor blackColor];
    lbl_Delivery.numberOfLines = 0;
    lbl_Delivery.font = [UIFont fontWithName:kFont size:12];
    [img_backgroundimage addSubview:lbl_Delivery];
    
    UILabel  * lbl_customeraddress = [[UILabel alloc]init];
    lbl_customeraddress.text = @"You send the food to the\ncustomer's address";
    lbl_customeraddress.backgroundColor=[UIColor clearColor];
    lbl_customeraddress.textColor=[UIColor blackColor];
    lbl_customeraddress.numberOfLines = 0;
    lbl_customeraddress.textAlignment = NSTextAlignmentLeft;
    
    lbl_customeraddress.font = [UIFont fontWithName:kFont size:10];
    [img_backgroundimage addSubview:lbl_customeraddress];
    
    
    img_backgroundimage2=[[UIImageView alloc]init];
    [img_backgroundimage2 setUserInteractionEnabled:YES];
    img_backgroundimage2.backgroundColor=[UIColor clearColor];
    img_backgroundimage2.image=[UIImage imageNamed:@"bg1-img@2x.png"];
    [scrollview3 addSubview:img_backgroundimage2];
    img_backgroundimage2.hidden = YES;
    
    
    UILabel  * lbl_dine = [[UILabel alloc]init];
    lbl_dine.text = @"If Dine In";
    lbl_dine.backgroundColor=[UIColor clearColor];
    lbl_dine.textColor=[UIColor blackColor];
    lbl_dine.numberOfLines = 0;
    lbl_dine.font = [UIFont fontWithName:kFontBold size:15];
    [img_backgroundimage2 addSubview:lbl_dine];
    
    UILabel  * lbl_text = [[UILabel alloc]init];
    lbl_text.text = @"(Please attach a picture your dining table\nand dining room with plates and cutlery set\nup for service) ";
    lbl_text.backgroundColor=[UIColor clearColor];
    lbl_text.textColor=[UIColor blackColor];
    lbl_text.numberOfLines = 0;
    //    lbl_text.font = [UIFont fontWithName:kFontBold size:15];
    if (IS_IPHONE_6Plus)
    {
        lbl_customereats.font = [UIFont fontWithName:kFont size:12];
        lbl_customer.font = [UIFont fontWithName:kFont size:12];
        lbl_customeraddress.font = [UIFont fontWithName:kFont size:12];
        lbl_text.font = [UIFont fontWithName:kFont size:16];
    }else if (IS_IPHONE_6)
    {
        lbl_customereats.font = [UIFont fontWithName:kFont size:12];
        lbl_customer.font = [UIFont fontWithName:kFont size:12];
        lbl_customeraddress.font = [UIFont fontWithName:kFont size:12];
        lbl_text.font = [UIFont fontWithName:kFont size:14];
    }else
    {
        lbl_text.font = [UIFont fontWithName:kFont size:12];
    }
    [img_backgroundimage2 addSubview:lbl_text];
    lbl_text.numberOfLines = 5;
    
    UILabel  * lbl_Whs = [[UILabel alloc]init];
    lbl_Whs.text = @"Width:900px\nHeight:450px\nSize:3mb";
    lbl_Whs.backgroundColor=[UIColor clearColor];
    lbl_Whs.textColor=[UIColor blackColor];
    lbl_Whs.numberOfLines = 0;
    lbl_Whs.font = [UIFont fontWithName:kFont size:11];
    [img_backgroundimage2 addSubview:lbl_Whs];
    
    dinein_picture=[[UIImageView alloc]init];
    dinein_picture.backgroundColor=[UIColor whiteColor];
    dinein_picture.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    dinein_picture.layer.borderWidth=1.0f;
    [dinein_picture setContentMode:UIViewContentModeScaleAspectFill];
    dinein_picture.clipsToBounds = YES;
    [img_backgroundimage2 addSubview:dinein_picture];
    
    
    picture1=[[UIImageView alloc]init];
    picture1.image=[UIImage imageNamed:@"icon 2.png"];
    picture1.backgroundColor=[UIColor whiteColor];
    //    picture1.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    //    picture1.layer.borderWidth=1.0f;
    //    picture1.clipsToBounds=YES;
    picture1.userInteractionEnabled = YES;
    
    [img_backgroundimage2 addSubview:picture1];
    
    UIButton*btn_dive;
    btn_dive = [[UIButton alloc] init];
    btn_dive.backgroundColor = [UIColor clearColor];
    [btn_dive addTarget:self action:@selector(profileimg_clickbtn:) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimage2 addSubview:btn_dive];
    
    UILabel  *lbl_Sdinein = [[UILabel alloc]init];
    lbl_Sdinein.text = @"Specify no.of seats that is available for Dine-in";
    lbl_Sdinein.backgroundColor=[UIColor clearColor];
    lbl_Sdinein.textColor=[UIColor blackColor];
    lbl_Sdinein.numberOfLines = 0;
    
    if (IS_IPHONE_6Plus)
    {
        lbl_Whs.font = [UIFont fontWithName:kFont size:13];
        lbl_Sdinein.font = [UIFont fontWithName:kFont size:16];
    }else if (IS_IPHONE_6)
    {
        lbl_Whs.font = [UIFont fontWithName:kFont size:12];
        lbl_Sdinein.font = [UIFont fontWithName:kFont size:15];
    }else
    {
        lbl_Sdinein.font = [UIFont fontWithName:kFont size:12];
    }
    [img_backgroundimage2 addSubview:lbl_Sdinein];
    
    txt_seats = [[UITextField alloc] init];
    //    txt_seats.frame=CGRectMake(180,CGRectGetMaxY(lbl_Sdinein.frame)+5, 150, 38);
    txt_seats.borderStyle = UITextBorderStyleNone;
    txt_seats.textColor = [UIColor blackColor];
    txt_seats.font = [UIFont fontWithName:kFontBold size:16];
    //    txt_seats.placeholder = @"Dine-In Seats";
    [txt_seats setValue:[UIFont fontWithName:kFontBold size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_seats setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    txt_seats.leftViewMode = UITextFieldViewModeAlways;
    txt_seats.userInteractionEnabled=YES;
    txt_seats.textAlignment = NSTextAlignmentLeft;
    txt_seats.backgroundColor = [UIColor clearColor];
    txt_seats.keyboardType = UIKeyboardTypeNumberPad;
    txt_seats.delegate = self;
    [img_backgroundimage2 addSubview:txt_seats];
    
    UIToolbar*keyboardToolbar_Phone;
    
    if (keyboardToolbar_Phone == nil)
    {
        keyboardToolbar_Phone = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, IS_IPHONE_5?45:45)];
        [keyboardToolbar_Phone setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_Done)];
        [keyboardToolbar_Phone setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    txt_seats.inputAccessoryView = keyboardToolbar_Phone;
    
    lbl_dineinSeats = [[UILabel alloc]init];
    lbl_dineinSeats.text = @"Dine-In Seats";
    if (IS_IPHONE_6Plus)
    {
        lbl_dineinSeats.font = [UIFont fontWithName:kFont size:16];
        
    }else if (IS_IPHONE_6)
    {
        lbl_dineinSeats.font = [UIFont fontWithName:kFont size:16];
        
    }else
    {
        lbl_dineinSeats.font = [UIFont fontWithName:kFont size:15];
    }
    
    lbl_dineinSeats.backgroundColor=[UIColor clearColor];
    lbl_dineinSeats.textColor=[UIColor blackColor];
    //    lbl_dineinSeats.font = [UIFont fontWithName:kFont size:15];
    [img_backgroundimage2 addSubview:lbl_dineinSeats];
    
    UIImageView *lineimg=[[UIImageView alloc]init];
    [lineimg setUserInteractionEnabled:YES];
    lineimg.backgroundColor=[UIColor clearColor];
    lineimg.image=[UIImage imageNamed:@"line1.png"];
    [img_backgroundimage2 addSubview:lineimg];
    
    UILabel *lbl_parking=[[UILabel alloc]init];
    lbl_parking.text = @"Is parking available?";
    lbl_parking.backgroundColor=[UIColor clearColor];
    lbl_parking.textColor=[UIColor blackColor];
    lbl_parking.numberOfLines = 0;
    lbl_parking.font = [UIFont fontWithName:kFont size:12];
    [img_backgroundimage2 addSubview:lbl_parking];
    
    
    button_img=[[UIImageView alloc]init];
    [button_img setUserInteractionEnabled:YES];
    button_img.backgroundColor=[UIColor clearColor];
    button_img.image=[UIImage imageNamed:@"button img 2.png"];
    [img_backgroundimage2 addSubview:button_img];
    
    UILabel  *lbl_Yes = [[UILabel alloc]init];
    lbl_Yes.text = @"Yes";
    lbl_Yes.backgroundColor=[UIColor clearColor];
    lbl_Yes.textColor=[UIColor blackColor];
    lbl_Yes.numberOfLines = 0;
    lbl_Yes.font = [UIFont fontWithName:kFont size:12];
    [img_backgroundimage2 addSubview:lbl_Yes];
    
    
    UIButton *btn_Yes = [[UIButton alloc] init];
    btn_Yes.backgroundColor = [UIColor clearColor];
    [btn_Yes addTarget:self action:@selector(btnYes_Parking:) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimage2 addSubview:btn_Yes];
    
    
    
    buttonimg=[[UIImageView alloc]init];
    [buttonimg setUserInteractionEnabled:YES];
    buttonimg.backgroundColor=[UIColor clearColor];
    buttonimg.image=[UIImage imageNamed:@"button img 2.png"];
    [img_backgroundimage2 addSubview:buttonimg];
    
    UILabel  *lbl_No = [[UILabel alloc]init];
    lbl_No.text = @"No";
    lbl_No.backgroundColor=[UIColor clearColor];
    lbl_No.textColor=[UIColor blackColor];
    lbl_No.numberOfLines = 0;
    lbl_No.font = [UIFont fontWithName:kFont size:12];
    [img_backgroundimage2 addSubview:lbl_No];
    
    UIButton *btn_No = [[UIButton alloc] init];
    btn_No.backgroundColor = [UIColor clearColor];
    [btn_No addTarget:self action:@selector(btnNo_Parking:) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimage2 addSubview:btn_No];
    
    
    
    UIImageView *image=[[UIImageView alloc]init];
    [image setUserInteractionEnabled:YES];
    image.backgroundColor=[UIColor colorWithRed:241/255.0f green:244/255.0f blue:242/255.0f alpha:1];
    [scrollview3 addSubview:image];
    
    
    
    UILabel  * page3 = [[UILabel alloc]init];
    page3.text = @"Page 3/6";
    page3.backgroundColor=[UIColor clearColor];
    page3.textAlignment = NSTextAlignmentCenter;
    page3.textColor=[UIColor blackColor];
    
    page3.textAlignment=NSTextAlignmentCenter;
    
    page3.numberOfLines = 0;
    page3.font = [UIFont fontWithName:kFont size:13];
    [image addSubview:page3];
    
    UIButton *next = [[UIButton alloc] init];
    next.layer.cornerRadius=4.0f;
    //    [next setTitle:@"Next" forState:UIControlStateNormal];
    [next setImage:[UIImage imageNamed:@"text buuton.png"] forState:UIControlStateNormal];
    [next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [next addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [image addSubview:next];
    
    if (IS_IPHONE_6)
    {
        
        scrollview3.frame =CGRectMake(0, 45, WIDTH, HEIGHT);
        img_backgroundimage.frame=CGRectMake(-5, -1,WIDTH+12, 150);
        lbl_propose.frame=CGRectMake(80,10, 330,30);
        btn_Dinein.frame = CGRectMake(25, CGRectGetMaxY(lbl_propose.frame)+5, 20,20);
        lbl_Dinein.frame=CGRectMake(55,CGRectGetMaxY(lbl_propose.frame)+5, 70,20);
        lbl_customereats.frame=CGRectMake(CGRectGetMaxX(lbl_Dinein.frame)-20, CGRectGetMaxY(lbl_propose.frame)+5, 250, 20);
        btn_Takeout.frame = CGRectMake(25, CGRectGetMaxY(btn_Dinein.frame)+5, 20,20);
        lbl_Takeout.frame=CGRectMake(55,CGRectGetMaxY(lbl_Dinein.frame)+5, 70,20);
        lbl_customer.frame=CGRectMake(CGRectGetMaxX(lbl_Takeout.frame)-11, CGRectGetMaxY(lbl_Dinein.frame)+5, 250, 20);
        btn_Delivery.frame = CGRectMake(25, CGRectGetMaxY(btn_Takeout.frame)+5, 20,20);
        lbl_Delivery.frame=CGRectMake(55,CGRectGetMaxY(lbl_Takeout.frame)+5, 70,20);
        lbl_customeraddress.frame=CGRectMake(CGRectGetMaxX(lbl_Delivery.frame)-15, CGRectGetMaxY(lbl_Takeout.frame)+2, 250, 40);
        img_backgroundimage2.frame=CGRectMake(-5, CGRectGetMaxY(img_backgroundimage.frame)+3,WIDTH+12, 390);
        lbl_dine.frame=CGRectMake(150,10, 350,30);
        lbl_text.frame=CGRectMake(25,CGRectGetMaxY(lbl_dine.frame)+5, 380,60);
        lbl_Whs.frame=CGRectMake(275,CGRectGetMaxY(lbl_text.frame)+70, 150,70);
        dinein_picture.frame=CGRectMake(25, CGRectGetMaxY(lbl_text.frame)+3, 240, 120);
        picture1.frame=CGRectMake(230, CGRectGetMaxY(lbl_text.frame)+85, 30, 30);
        btn_dive.frame=CGRectMake(230, CGRectGetMaxY(lbl_text.frame)+85, 30, 30);
        lbl_Sdinein.frame=CGRectMake(25,CGRectGetMaxY(dinein_picture.frame)+5, WIDTH-20,30);
        txt_seats.frame=CGRectMake(25,CGRectGetMaxY(lbl_Sdinein.frame)+5, 220, 38);
        lbl_dineinSeats.frame=CGRectMake(CGRectGetMaxX(txt_seats.frame), CGRectGetMaxY(lbl_Sdinein.frame)+5, 100, 38);
        lineimg.frame=CGRectMake(25, CGRectGetMaxY(txt_seats.frame)-8, 322, 0.5);
        // lineimg.frame=CGRectMake(10, CGRectGetMaxY(txt_seats.frame)+1, 300, 0.5);
        lbl_parking.frame=CGRectMake(25, CGRectGetMaxY(lineimg.frame)+5, 170, 30);
        btn_Yes.frame = CGRectMake(25, CGRectGetMaxY(lbl_parking.frame)+5, 15,15);
        button_img.frame=CGRectMake(25, CGRectGetMaxY(lbl_parking.frame)+5, 15, 15);
        lbl_Yes.frame=CGRectMake(50,CGRectGetMaxY(lbl_parking.frame)+5, 30,15);
        btn_No.frame = CGRectMake(100, CGRectGetMaxY(lbl_parking.frame)+5, 15,15);
        lbl_No.frame=CGRectMake(125,CGRectGetMaxY(lbl_parking.frame)+5, 30,15);
        buttonimg.frame=CGRectMake(100, CGRectGetMaxY(lbl_parking.frame)+5, 15, 15);
        image.frame=CGRectMake(0, CGRectGetMaxY(img_backgroundimage2.frame)+3, WIDTH, 100);
//        image.frame=CGRectMake(0, HEIGHT-100, WIDTH, 100);
        page3.frame =CGRectMake(0,4, WIDTH,30);
        next.frame = CGRectMake(18, 40, WIDTH-36,50);
        lbl_parking.font = [UIFont fontWithName:kFont size:13];
        lbl_Yes.font = [UIFont fontWithName:kFont size:13];
        lbl_No.font = [UIFont fontWithName:kFont size:13];
        [scrollview3 setContentSize:CGSizeMake(0,CGRectGetMaxY(img_backgroundimage2.frame)+150)];

    }
    else if (IS_IPHONE_6Plus)
    {
        scrollview3.frame =CGRectMake(0, 45, WIDTH, HEIGHT);
        img_backgroundimage.frame=CGRectMake(-5, 0,WIDTH+12, 150);
        lbl_propose.frame=CGRectMake(90,10, 330,30);
        btn_Dinein.frame = CGRectMake(25, CGRectGetMaxY(lbl_propose.frame)+5, 20,20);
        lbl_Dinein.frame=CGRectMake(55,CGRectGetMaxY(lbl_propose.frame)+5, 70,20);
        lbl_customereats.frame=CGRectMake(CGRectGetMaxX(lbl_Dinein.frame)-20, CGRectGetMaxY(lbl_propose.frame)+5, 250, 20);
        btn_Takeout.frame = CGRectMake(25, CGRectGetMaxY(btn_Dinein.frame)+5, 20,20);
        lbl_Takeout.frame=CGRectMake(55,CGRectGetMaxY(lbl_Dinein.frame)+5, 70,20);
        lbl_customer.frame=CGRectMake(CGRectGetMaxX(lbl_Takeout.frame)-11, CGRectGetMaxY(lbl_Dinein.frame)+5, 250, 20);
        btn_Delivery.frame = CGRectMake(25, CGRectGetMaxY(btn_Takeout.frame)+5, 20,20);
        lbl_Delivery.frame=CGRectMake(55,CGRectGetMaxY(lbl_Takeout.frame)+5, 70,20);
        lbl_customeraddress.frame=CGRectMake(CGRectGetMaxX(lbl_Delivery.frame)-15, CGRectGetMaxY(lbl_Takeout.frame)+2, 250, 40);
        img_backgroundimage2.frame=CGRectMake(-5, CGRectGetMaxY(img_backgroundimage.frame)+3,WIDTH+12, 420);
        lbl_dine.frame=CGRectMake(160,10, 350,30);
        lbl_text.frame=CGRectMake(25,CGRectGetMaxY(lbl_dine.frame)+5, 380,60);
        lbl_Whs.frame=CGRectMake(275,CGRectGetMaxY(lbl_text.frame)+70, 150,70);
        dinein_picture.frame=CGRectMake(25, CGRectGetMaxY(lbl_text.frame)+10, 240, 120);
        picture1.frame=CGRectMake(225, CGRectGetMaxY(lbl_text.frame)+89, 30, 30);
        btn_dive.frame=CGRectMake(225, CGRectGetMaxY(lbl_text.frame)+89, 30, 30);
        lbl_Sdinein.frame=CGRectMake(25,CGRectGetMaxY(dinein_picture.frame)+5, WIDTH-20,30);
        txt_seats.frame=CGRectMake(25,CGRectGetMaxY(lbl_Sdinein.frame)+5, 245, 38);
        lbl_dineinSeats.frame=CGRectMake(CGRectGetMaxX(txt_seats.frame), CGRectGetMaxY(lbl_Sdinein.frame)+5, 100, 38);
        lineimg.frame=CGRectMake(25, CGRectGetMaxY(txt_seats.frame)-8, 342, 0.5);
        // lineimg.frame=CGRectMake(10, CGRectGetMaxY(txt_seats.frame)+1, 300, 0.5);
        lbl_parking.frame=CGRectMake(25, CGRectGetMaxY(lineimg.frame)+5, 170, 30);
        btn_Yes.frame = CGRectMake(25, CGRectGetMaxY(lbl_parking.frame)+5, 15,15);
        button_img.frame=CGRectMake(25, CGRectGetMaxY(lbl_parking.frame)+5, 15, 15);
        lbl_Yes.frame=CGRectMake(50,CGRectGetMaxY(lbl_parking.frame)+5, 30,15);
        btn_No.frame = CGRectMake(100, CGRectGetMaxY(lbl_parking.frame)+5, 15,15);
        lbl_No.frame=CGRectMake(125,CGRectGetMaxY(lbl_parking.frame)+5, 30,15);
        buttonimg.frame=CGRectMake(100, CGRectGetMaxY(lbl_parking.frame)+5, 15, 15);
        image.frame=CGRectMake(0, CGRectGetMaxY(img_backgroundimage2.frame)+3, WIDTH, 100);
//        image.frame=CGRectMake(0, HEIGHT-100, WIDTH, 100);
        page3.frame =CGRectMake(0,4, WIDTH,30);
        next.frame = CGRectMake(18, 40, WIDTH-36,50);
        lbl_parking.font = [UIFont fontWithName:kFont size:13];
        lbl_Yes.font = [UIFont fontWithName:kFont size:13];
        lbl_No.font = [UIFont fontWithName:kFont size:13];
        [scrollview3 setContentSize:CGSizeMake(0,CGRectGetMaxY(img_backgroundimage2.frame)+150)];

    }
    else
    {
        scrollview3.frame =CGRectMake(0, 45, WIDTH, HEIGHT);
        img_backgroundimage.frame=CGRectMake(-5, -1,WIDTH+12, 150);
        lbl_propose.frame=CGRectMake(50,7, WIDTH-30,35);
        btn_Dinein.frame = CGRectMake(30, CGRectGetMaxY(lbl_propose.frame)+5, 20,20);
        lbl_Dinein.frame=CGRectMake(60,CGRectGetMaxY(lbl_propose.frame)+3,70,20);
        lbl_customereats.frame=CGRectMake(CGRectGetMaxX(lbl_Dinein.frame)-20, CGRectGetMaxY(lbl_propose.frame)+3, 250, 20);
        btn_Takeout.frame = CGRectMake(30, CGRectGetMaxY(btn_Dinein.frame)+5, 20,20);
        lbl_Takeout.frame=CGRectMake(60,CGRectGetMaxY(lbl_Dinein.frame)+5, 70,20);
        lbl_customer.frame=CGRectMake(CGRectGetMaxX(lbl_Takeout.frame)-11, CGRectGetMaxY(lbl_Dinein.frame)+5, 250, 20);
        btn_Delivery.frame = CGRectMake(30, CGRectGetMaxY(btn_Takeout.frame)+5, 20,20);
        lbl_Delivery.frame=CGRectMake(60,CGRectGetMaxY(lbl_Takeout.frame)-2, 70,40);
        lbl_customeraddress.frame=CGRectMake(CGRectGetMaxX(lbl_Delivery.frame)-15, CGRectGetMaxY(lbl_Takeout.frame)+2, 250, 40);
        img_backgroundimage2.frame=CGRectMake(-5, CGRectGetMaxY(img_backgroundimage.frame)+3,WIDTH+12, 390);
        lbl_dine.frame=CGRectMake(120,10, 300,30);
        lbl_text.frame=CGRectMake(30,CGRectGetMaxY(lbl_dine.frame), WIDTH-8,80);
        lbl_Whs.frame=CGRectMake(235,CGRectGetMaxY(lbl_text.frame)+70, 150,70);
        dinein_picture.frame=CGRectMake(30, CGRectGetMaxY(lbl_text.frame)+3, 200, 120);
        picture1.frame=CGRectMake(190, CGRectGetMaxY(lbl_text.frame)+85, 30, 30);
        btn_dive.frame=CGRectMake(190, CGRectGetMaxY(lbl_text.frame)+85, 30, 30);
        lbl_Sdinein.frame=CGRectMake(30,CGRectGetMaxY(dinein_picture.frame)+5, 320,30);
        txt_seats.frame=CGRectMake(25,CGRectGetMaxY(lbl_Sdinein.frame)+5, 168, 38);
        lbl_dineinSeats.frame=CGRectMake(CGRectGetMaxX(txt_seats.frame), CGRectGetMaxY(lbl_Sdinein.frame)+5, 100, 38);
        lineimg.frame=CGRectMake(30, CGRectGetMaxY(txt_seats.frame)-6, 260, 0.5);
        // lineimg.frame=CGRectMake(10, CGRectGetMaxY(txt_seats.frame)+1, 300, 0.5);
        lbl_parking.frame=CGRectMake(30, CGRectGetMaxY(txt_seats.frame)+5, 170, 30);
        btn_Yes.frame = CGRectMake(30, CGRectGetMaxY(lbl_parking.frame)+5, 15,15);
        button_img.frame=CGRectMake(30, CGRectGetMaxY(lbl_parking.frame)+5, 15, 15);
        lbl_Yes.frame=CGRectMake(55,CGRectGetMaxY(lbl_parking.frame)+5, 30,15);
        btn_No.frame = CGRectMake(90, CGRectGetMaxY(lbl_parking.frame)+5, 15,15);
        buttonimg.frame=CGRectMake(90, CGRectGetMaxY(lbl_parking.frame)+5, 15, 15);
        lbl_No.frame=CGRectMake(110,CGRectGetMaxY(lbl_parking.frame)+5, 30,15);
        image.frame=CGRectMake(0, CGRectGetMaxY(img_backgroundimage2.frame)+3, WIDTH, 100);
//        image.frame=CGRectMake(0, HEIGHT-80, WIDTH, 100);
        page3.frame =CGRectMake(0,0, WIDTH,30);
        next.frame = CGRectMake(18, 35, WIDTH-36,40);
//        [scrollview3 setContentSize:CGSizeMake(0,700)];
        [scrollview3 setContentSize:CGSizeMake(0,CGRectGetMaxY(img_backgroundimage2.frame)+130)];


        
    }
    
    // CGRectGetMaxY(lbl_No.frame)+120
//    [scrollview3 setContentSize:CGSizeMake(0,700)];
}

#pragma mark Button actions

-(void)Next_btnClick
{
    //    ChefSignup4 *chefsignup4=[[ChefSignup4 alloc]init];
    //    [self presentViewController:chefsignup4 animated:NO completion:nil];
    //    [self.navigationController pushViewController:chefsignup4 animated:NO];
    
    NSLog(@"click_Loginbtn");
    
    if (img_backgroundimage2.hidden == NO)
    {
        if (ary_temp.count==0)
        {
            [self popup_Alertview:@"please select propose of serve"];
        }
        
        else if (imgData == nil)
        {
            [self popup_Alertview:@"Please upload image for kitchen image"];
        }
        
        else  if ([txt_seats.text isEqualToString:@""])
        {
            [self popup_Alertview:@"Please enter Dine-in Seats"];
            
        }        else if ([[NSString stringWithFormat:@"%@",str_status] isEqualToString:@""])
        {
            [self popup_Alertview:@"please select parking available"];
        }
        
        else
        {
            [self SignupThird];
        }
        
    }
    else{
        if (ary_temp.count==0)
        {
            [self popup_Alertview:@"please enter propose of serve"];
        }
        
        else
        {
            [self SignupThird];
        }
    }
    
    
}

-(void) btnYes_Parking:(UIButton *) sender
{
    buttonimg.image=[UIImage imageNamed:@"button img 2.png"];
    
    if([sender isSelected])
    {
        [sender setSelected:NO];
        str_status=@"YES";
        button_img.image=[UIImage imageNamed:@"button img .png"];
    }
    else
    {
        
        [sender setSelected:YES];
        button_img.image=[UIImage imageNamed:@"button img 2.png"];
    }
    
}
-(void) btnNo_Parking:(UIButton *) sender

{
    button_img.image=[UIImage imageNamed:@"button img 2.png"];
    
    if([sender isSelected])
    {
        [sender setSelected:NO];
        str_status=@"NO";
        buttonimg.image=[UIImage imageNamed:@"button img .png"];
    }
    else
    {
        [sender setSelected:YES];
        buttonimg.image=[UIImage imageNamed:@"button img 2.png"];
    }
    
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField==txt_seats)
    {
        NSString *stringVal = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered =[[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        if (stringVal.length<9 && [string isEqualToString:filtered])
        {
            return YES;
        }
        else{
            return NO;
        }
        return [string isEqualToString:filtered];
    }
    return YES;
}
-(void)click_Done
{
    [txt_seats resignFirstResponder];
    
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([textField isEqual:txt_seats])
    {
        lbl_dineinSeats.hidden = YES;
    }
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == txt_seats) {
        txt_seats.textAlignment=NSTextAlignmentLeft;
        if (txt_seats.text.length<=0)
        {
            lbl_dineinSeats.hidden = NO;
        }
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}





-(void)click_selectObjectAt:(UIButton *)sender
{
    ABC=sender.tag;
    
    
    
    if([ary_temp containsObject:[NSString stringWithFormat:@"%d",ABC]])
    {
        [ary_temp removeObject:[NSString stringWithFormat:@"%d",ABC]];
        
        if (sender.tag==54)
        {
            [btn_Dinein setImage:[UIImage imageNamed:@"img 2.png"] forState:UIControlStateNormal];
            img_backgroundimage2.hidden = YES;
            
        }
        else if (sender.tag==55)
        {
            [btn_Takeout setImage:[UIImage imageNamed:@"img 2.png"] forState:UIControlStateNormal];
        }
        else if (sender.tag==56)
        {
            [btn_Delivery setImage:[UIImage imageNamed:@"img 2.png"] forState:UIControlStateNormal];
        }
    }
    else
    {
        [ary_temp addObject:[NSString stringWithFormat:@"%d",ABC]];
        if (sender.tag==54)
        {
            
            [btn_Dinein setImage:[UIImage imageNamed:@"img 1.png"] forState:UIControlStateNormal];
            img_backgroundimage2.hidden = NO;
        }
        else if (sender.tag==55)
        {
            [btn_Takeout setImage:[UIImage imageNamed:@"img 1.png"] forState:UIControlStateNormal];
            
        }
        else if (sender.tag==56)
        {
            [btn_Delivery setImage:[UIImage imageNamed:@"img 1.png"] forState:UIControlStateNormal];
            
        }
    }
    
    str_ServiceType = [[NSMutableString alloc] init];
    for (int i=0; i<[ary_temp count];i++)
    {
        if (i==[ary_temp count]-1)
        {
            [str_ServiceType  appendString:[NSString stringWithFormat:@"%@",[ary_temp objectAtIndex:i]]];
        }
        else
        {
            [str_ServiceType  appendString:[NSString stringWithFormat:@"%@||",[ary_temp objectAtIndex:i]]];
        }
    }
    
    //    '56||54||55',
    NSLog(@"str_CAt_ids :%@",str_ServiceType);
    NSLog(@"ary_temp :%@",ary_temp);
    
    
}



-(void)profileimg_clickbtn:(UIButton *) sender
{
    str_pickingImage = @"DineinTable";
    
    UIActionSheet *actionSheet_popupQuery = [[UIActionSheet alloc] initWithTitle:@"Select A Image From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Camera", nil];
    actionSheet_popupQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet_popupQuery showInView:self.view];
    
}

#pragma mark - ACTION SHEET DELEGATE

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 0)
    {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            UIImagePickerController*imgPicker = [[UIImagePickerController alloc] init];
            UIColor* color = [UIColor colorWithRed:46.0/255 green:127.0/255 blue:244.0/255 alpha:1];
            [imgPicker.navigationBar setTintColor:color];
            imgPicker.delegate = self;
            imgPicker.allowsEditing = YES;
            imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            [self presentViewController:imgPicker animated:NO completion:Nil];
        }
    }
    else if(buttonIndex == 1)
    {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController*imgPicker = [[UIImagePickerController alloc] init];
            UIColor* color = [UIColor colorWithRed:46.0/255 green:127.0/255 blue:244.0/255 alpha:1];
            [imgPicker.navigationBar setTintColor:color];
            imgPicker.delegate = self;
            imgPicker.allowsEditing = YES;
            imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:imgPicker animated:NO completion:Nil];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Not86" message:@"Device Does Not Support Camera" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
}

#pragma mark - IMAGEPICKER DELEGATE METHODS
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *imageOriginal =  [info objectForKey:UIImagePickerControllerEditedImage];
    
    if ( [[NSString stringWithFormat:@"%@",str_pickingImage]isEqualToString:@"DineinTable"])
    {
        imgData =  UIImageJPEGRepresentation(imageOriginal,0.5);
        dinein_picture.image = imageOriginal;
        
    }
    
    [picker dismissViewControllerAnimated:YES completion:Nil];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:Nil];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}
-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFontBold size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFontBold size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFontBold size:16.0f]];
}
-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}




# pragma mark ChefSignupThird method

-(void)SignupThird
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    NSDictionary *params;
    
    params =@{
              @"field_dine_in_seats"               :  txt_seats.text,
              @"uid"                                               :[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
              @"field_availlable_parking"          :  str_status,
              @"str_ServiceType"                   : str_ServiceType,
              @"registration_page"                 : @"3",

              @"role_type"                         :  @"chef_profile",
              @"device_udid"                       :  UniqueAppID,
              @"device_token"                      :  @"Dev",
              @"device_type"                       :  @"1"
              
              };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = nil;
    
    if (dinein_picture!= nil)
    {
        //        request = [httpClient multipartFormRequestWithMethod:@"POST" path:AFChefSignUpFirst parameters:params constructingBodyWithBlock:^(id <AFMultipartFormData>formData)
        //                   {
        //                       NSTimeInterval timeInterval = [NSDate timeIntervalSinceReferenceDate];
        //
        //                       [formData appendPartWithFileData: imgData name:@"dinein_img" fileName:[NSString stringWithFormat:@"%lf-image.png",timeInterval] mimeType:@"image/jpeg"];
        //
        //
        //                   }];
        
        request = [httpClient requestWithMethod:@"POST"
                                           path:AFChefSignUpFirst
                                     parameters:params];
        
    }
    else
    {
        request = [httpClient requestWithMethod:@"POST"
                                           path:AFChefSignUpFirst
                                     parameters:params];
    }
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpFirst:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self SignupThird];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpFirst :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        ChefSignup4 *chefsignup4=[[ChefSignup4 alloc]init];
        [self presentViewController:chefsignup4 animated:NO completion:nil];
        //    [self.navigationController pushViewController:chefsignup4 animated:NO];
        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        txt_seats.text=@"";
        
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
