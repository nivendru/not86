//
//  ChefEditProfileVC.m
//  Not86
//
//  Created by Admin on 18/09/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ChefMyProfileVC.h"
#import "AppDelegate.h"
#import "Define.h"
#import "ChefEditProfileVC.h"
#import "JWSlideMenuViewController.h"
#import "JWSlideMenuController.h"



#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"

@interface ChefMyProfileVC ()<UIScrollViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UITextViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate>
{
    UIImageView * img_header;
    UIScrollView * scroll;
    UIImageView *  img_bg;
    
    UICollectionViewFlowLayout *  layout;
    UICollectionView * collView_for_icons;
    NSMutableArray * array_for_icons;
    NSMutableArray * array_icons_name;
    
    UITextField * txt_full_name;
    UITextField * txt_socilid_number;
    
    UITextField * txt_serving_type;
    UITextField * txt_cooking_ex;
    
    int selectedindex;
    NSIndexPath*indexSelected;
    
    
    
    UIImageView * img_strip;
    UIView * view_personal;
    
    UIButton * btn_on_check_box;
    int ABC;
    NSMutableArray * array_temp;
    UIButton * click_on_dine_in;
    
    UIButton * btn_on_check_box_for_take_out;
    UIButton * btn_on_check_box_for_delivery;
    UIButton * btn_Dinein;
    UIButton * btn_Takeout;
    UIButton * btn_Delivery;
    UIButton * radio_button;
    UIButton * radio_button2;
    UIButton * radio_button3;
    UIButton * radio_button4;
    
    UIToolbar * keyboardToolbar_Date;
    
    
    NSMutableArray * ary_chef_info_titles;
    NSMutableArray * array_img;
    NSMutableArray * array_chef_details;
    
    UIImageView * img_strip1 ;
    UIView * view_for_kitchen;
    UITableView * table_for_kitchen_info;
    NSMutableArray * array_icon_kitchen;
    NSMutableArray * array_head_names_in_kitchen;
    NSMutableArray * array_kitchen_name;
    NSMutableArray * array_icon_storages_hygiene;
    UICollectionViewFlowLayout *  layout2;
    UICollectionView * collView_for_kitchen_imgs;
    NSMutableArray * array_kitchen_imgs;
    UITableView * table_for_food_storage;
    NSMutableArray * array_head_names_in_food_storage;
    
    
    UIImageView *  img_strip2;
    UIView * view_for_dine_in;
    
    UIImageView * img_strip3;
    UIView * view_for_menu;
    UITableView * table_for_menu;
    NSMutableArray *  array_dietary_icons;
    NSMutableArray * array_head_names_in_dietary;
    NSMutableArray * array_text_in_dietary_cell;
    UICollectionViewFlowLayout *  layout3;
    UICollectionView * collView_for_menu_items;
    NSMutableArray * array_menu_items;
    
    UIView *  view_for_schedule;
    UITableView * table_for_schedule;
    NSMutableArray * array_serving_img;
    NSMutableArray * array_serving_time;
    NSMutableArray * array_serving_items;
    UIView * view_for_schedule_items;
    UITableView * table_for_selected_schedule_deate;
    NSMutableArray * array_icon_left_arrow;
    NSMutableArray* array_serving_date_time;
    UITableView * table_for_dish_items;
    NSMutableArray*ary_displaynames;
    NSMutableArray * array_dish_img;
    NSMutableArray * arraY_serving_val;
    
    
    UIView * view_for_on_request;
    
    UICollectionViewFlowLayout *layout4;
    UICollectionView *collView_serviceDirectory;
    
    UIView *view_for_reviews;
    
    UIView * view_for_delivery;
    
    UIView * view_for_note;
    
    UITextView * txt_view_for_chef_description;
    
    UITextView * txt_view_for_storage_description;
    UITextView * txt_view_for_hygiene_description;
    UIButton * radio_button_for_parking;
    UIButton * radio_button_for_parking_no;
    
    UIButton * radio_button_for_delivery_charge;
    UIButton * radio_button_for_delivery_charge2;
    UIButton * btn_check_box_for_setas_default;
    
    UITextView * txtview_note;
    
    AppDelegate * delegate;
    //    UIButton *chef_img;
    UIImageView * chef_img;
    NSString*str_pickingImage;
    UIButton * profileimg_button;
    
    
    NSData*imgData;
    
    UITextField *txt_first_name;
    UITextField *txt_middle_name;
    UITextField *txt_last_name;
    UITextField *txt_cuntry_tel_code;
    UITextField *txt_mobile_number;
    UITextField *txt_email_address;
    UITextField *txt_date_of_birth;
    UITextField *txt_social_id_number;
    
    UITextField *txt_chef_type;
    UITextField *text_parking_val;
    UITextField *txt_no_of_seats_val;
    
    UITextField *txt_home_address;
    UITextField *txt_cuntry;
    UITextField *txt_city;
    UITextField *txt_pin_code;
    UITextField *txt_cooking_qualification;
    
    UITextField *txt_paypal_account;
    UITextField *txt_payment_receiverd_month;
    UITextField *txt_password;
    UITextField *txt_conform_password;
    
    UITextField *txt_kitchen_name;
    UITextField *txt_kitchen_address;
    UITextField *txt_cuntry_name;
    UITextField *txt_city_name;
    UITextField *txt_cuntry_pin_code;
    
    UITableView * table_for_delevery;
    
    
    NSMutableArray * array_delivery_company_name;
    NSMutableArray * array_company_phone;
    NSMutableArray * array_del_person_name;
    NSMutableArray * array_del_persone_numer;
    NSMutableArray * array_first_mile;
    NSMutableArray * array_additional_mile;
    NSMutableArray * array_flat_charge;
    
    
    UIImageView * img_radio_btn_on_yes;
    UIImageView * img_radio_btn_on_no;
    
    NSString * str_status;
    
    UIImageView *   img_radio_btn_on_onetofive;
    UIImageView *   img_radio_btn_on_sixtoten;
    UIImageView *  img_radio_btn_on_leventotwo;
    UIImageView *  img_radio_btn_on_moretwenty;
    
    
    UIButton *  btn_Yes ;
    UILabel * lbl_Yes ;
    UIButton *  btn_yrs ;
    UILabel * lbl_Yrs;
    UIButton *  btnyrs;
    UILabel * lblYrs;
    UIButton * btnyrs1;
    UILabel *  lblYrs1;
    
    
    UILabel * lbl_one_to_five;
    UILabel *  lbl_six_to_ten;
    UILabel * lbl_leven_to_twenty;
    UILabel * lbl_more_than_twenty;
    
    UITableView * table_for_chef_type;
    NSMutableArray * array_chef_type;
    UITableView * table_for_cuntrycode;
    
    
    CGFloat	animatedDistance;
    
    NSData     * imageData;
    
    NSMutableArray * arrayCountry;
    
    NSMutableArray * ary_citylist;
    
    UITableView * table_for_cuntry_name;
    
    UITableView * table_for_city_name;
    NSMutableArray * ary_countrycodelist;
    
    NSDateFormatter *formatter;
    
    UIDatePicker *datePicker;
    UIDatePicker *pickerView;
    
    NSString*str_DOBfinal;
    
    
    NSMutableArray*ary_delivaryaddress;
    NSMutableArray*array_text_in_cuisens_cell;
    
    
    NSMutableArray*ary_dishlist;
    NSMutableArray*ary_reviews;
    UILabel *label_overall_rating_value;
    UILabel *taste_value;
    UILabel *appeal_value;
    UILabel *value_value;
    UILabel *label_favorite_value;
    UILabel *label_total_reviews_value;
    
    NSMutableArray *ary_AvailableDates;
    NSDate *minimumDate;
    NSString*str_SelectedDate;
    NSMutableArray*ary_scedulelist;
    
    NSMutableArray*ary_servingtype;
    
}

@end

@implementation ChefMyProfileVC


static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;
@synthesize calendar,enabledDates;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    indexSelected = nil;
    selectedindex=-1;
    
    selectedindex= (int)0;
    indexSelected = [NSIndexPath indexPathForRow:0 inSection:0];
    [collView_for_icons reloadData];
    ary_AvailableDates = [NSMutableArray new];
    str_SelectedDate = [NSString new];
    ary_scedulelist = [NSMutableArray new];
    
    ary_servingtype = [NSMutableArray new];
    str_status=[[NSString alloc]init];
    str_status=@"";
    arrayCountry = [[NSMutableArray alloc]init];
    ary_citylist =[[NSMutableArray alloc]init];
    ary_countrycodelist = [[NSMutableArray alloc]init];
    ary_delivaryaddress = [NSMutableArray new];
    ary_dishlist = [NSMutableArray new];
    ary_reviews = [NSMutableArray new];
    
    
    array_kitchen_imgs = [NSMutableArray new];
    array_text_in_dietary_cell = [NSMutableArray new];
    array_text_in_cuisens_cell= [NSMutableArray new];
    
    str_pickingImage = [NSString new];
    // Do any additional setup after loading the view.
    
    array_temp = [NSMutableArray new];
    
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    
    
    array_for_icons = [[NSMutableArray alloc]initWithObjects:@"icon-personal-info-w@2x.png",@"icon-kichan-w@2x.png",@"icon-dine-in-w@2x.png",@"icon-menu-w@2x.png",@"calender-w-icon@2x.png",@"reviews-w-icon@2x.png",@"img-delivery-w@2x.png",@"icon-note@2x.png",nil];
    
    array_icons_name = [[NSMutableArray alloc]initWithObjects:@"Personal Info",@"   Kitchen",@"   Dine-in",@"     Menu",@"   Schedule",@"   Reviews",@"Delivery Info",@"    Note", nil];
    
    // personal info array declaration
    
    ary_chef_info_titles =[[NSMutableArray alloc]initWithObjects:@"Username",@"Full Name",@"Chef Type",@"Date of Birth",@"Social Security no./National ID",@"Email address",@"Mobile no.",@"Home Address",@"Serving Type",@"Cooking Qualification",@"Cooking Experience", @"Paypal Account",@"Payment Received",nil];
    array_img = [[NSMutableArray alloc]initWithObjects:@"user1-icon@2x.png",@"user1-icon@2x.png",@"food-icon@2x.png",@"icon-date-of-birth@2x.png",@"icon-social@2x.png",@"icon-email@2x.png",@"icon-phone2x.png",@"icon-Home@2x.png",@"seving-type-icon@2x.png",@"cooking-Q-icon@2x.png",@"cooking-Ex-icon@2x.png",@"icon-paypal@2x.png",@"icon-paypal@2x.png",nil];
    array_chef_details = [[NSMutableArray alloc]initWithObjects:@"Charles1990",@"Charles dan",@"Home Chef",@"29 july 1974",@"s123456789",@"Charlesdan@gmail.com",@"+01 643 124 5681",@"Smith St, Suntec City, Paris 743844",@"Dine-in,Takaway,Deliver",@"Diploma in Cooking Industry" ,@"6-10 Years",@"Charles@paypal.com",@"End of Month",nil];
    
    // kitchen info array declaration
    
    array_icon_kitchen = [[NSMutableArray alloc]initWithObjects:@"icon-kichan1@2x.png",@"icon-kichan1@2x.png", nil];
    array_head_names_in_kitchen = [[NSMutableArray alloc]initWithObjects:@"Kitchen Name",@"Kitchen Address",nil];
    array_kitchen_name = [[NSMutableArray alloc]initWithObjects:@"Charles X Restaruant",@"Cartel St, Sams City, Paris\n743659", nil];
    //  array_kitchen_imgs = [[NSMutableArray alloc]initWithObjects:@"img-kitchen1@2x.png",@"img-kitchen2@2x.png",@"img-kitchen3@2x.png",@"img-kitchen4@2x.png",@"img-kitchen5@2x.png",@"img-kitchen6@2x.png",@"img-kitchen7@2x.png",@"img-kitchen8@2x.png",@"img-kitchen1@2x.png",@"img-kitchen1@2x.png",nil];
    array_icon_storages_hygiene = [[NSMutableArray alloc]initWithObjects:@"icon-storage@2x.png",@"icon-hygiene@2x.png", nil];
    array_head_names_in_food_storage = [[NSMutableArray alloc]initWithObjects:@"Food Storage",@"Kitchen Hygiene Standards", nil];
    
    // array for menu
    array_dietary_icons = [[NSMutableArray alloc]initWithObjects:@"icon-order-food@2x.png",@"icon-allo_food@2x.png",nil];
    array_head_names_in_dietary = [[NSMutableArray alloc]initWithObjects:@"Cuisines Cooked",@"Dietary Restrictions followed",nil];
    //array_text_in_dietary_cell = [[NSMutableArray alloc]initWithObjects:@"Chinese, Mexican, Italian, Korean",@"Vegetarian, Kosher", nil];
    //collections view array
    array_menu_items = [[NSMutableArray alloc]initWithObjects:@"img-item1@2x.png",@"img-item2@2x.png",@"img-item3@x.png",@"img-item4@2x.png",@"img-item5@2x.png",@"img-item6@2x.png",@"img-item7@2x.png",@"img-item8@2x.png",@"img-item9@2x.png",@"img-item10@2x.png",@"img-add@2x.png", nil];
    
    //array declaration for schedule
    
    array_serving_img = [[NSMutableArray alloc]initWithObjects:@"seving-type-icon@2x.png", @"seving-type-icon@2x.png",@"seving-type-icon@2x.png",nil];
    array_serving_time = [[NSMutableArray alloc]initWithObjects:@"10:00AM-12:00PM",@"02:00PM-05:00PM",@"06:00PM-10:00PM", nil];
    array_serving_items =[[NSMutableArray alloc]initWithObjects:@"Chicken 65,Chill Chicken",@"Chicken 65,Chill Chicken",@"Chicken 65,Chill Chicken", nil];
    //array for serving date and time
    array_icon_left_arrow = [[NSMutableArray alloc]initWithObjects:@"left-arrow-img@2x.png", nil];
    array_serving_date_time = [[NSMutableArray alloc]initWithObjects:@"22 july 12.00pm to 5.30pm", nil];
    //arraydeclaration for dish items
    ary_displaynames =[[NSMutableArray alloc]initWithObjects:@"Steamed Thai otah",@"Rasberry custored", nil];
    array_dish_img = [[NSMutableArray alloc]initWithObjects:@"dish1-img@2x.png",@"dish1-img@2x.png", nil];
    arraY_serving_val = [[NSMutableArray alloc]initWithObjects:@"30",@"30", nil];
    
    //======array_for_delivery_info
    
    array_delivery_company_name = [[NSMutableArray alloc]initWithObjects:@"Timothy's Delivery",@"thom's delivery",@"jon's delivery",@"Timothy's Delivery",nil];
    array_company_phone = [[NSMutableArray alloc]initWithObjects:@"12345678",@"87456123",@"12354678",@"15236478", nil];
    array_del_person_name = [[NSMutableArray alloc]initWithObjects:@"jon",@"tom",@"robert",@"smith",nil];
    array_del_persone_numer = [[NSMutableArray alloc]initWithObjects:@"233454566",@"233454566",@"233454566",@"233454566", nil];
    array_first_mile = [[NSMutableArray alloc]initWithObjects:@"$10",@"$5",@"$7",@"$9", nil];
    array_additional_mile = [[NSMutableArray alloc]initWithObjects:@"$4",@"@6",@"$10",@"$5", nil];
    array_flat_charge = [[NSMutableArray alloc]initWithObjects:@"$5",@"$6",@"$7",@"$8",nil];
    
    array_chef_type = [[NSMutableArray alloc]initWithObjects:@"Home Chef",@"Restaurant Chef",@"Others",nil];
    
    
    [self integrateBody];
    [self integrateHeader];
    
}
#pragma mark - UIAction
//-(void)doneClicked{
//    [self.txt_MobileNo resignFirstResponder];
//}
//txt_mobile_number

-(void)user_img_click:(UIGestureRecognizer *)recognizer
{
    NSLog(@"-------- tap Image --------%li",  (long)[recognizer view].tag);
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Add Photo From " delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera", @"Photo", nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    actionSheet.backgroundColor = [UIColor clearColor];
    actionSheet.frame=CGRectMake(8, 90, 304, 216);
    
    // actionSheet.alpha=0.90;
    actionSheet.tag = 1000;
    [actionSheet showInView:self.view];
    
    
    
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    [self.navigationController.slideMenuController  UnhideHomeButton];
    
    [ary_delivaryaddress removeAllObjects];
    [array_kitchen_imgs removeAllObjects];
    [array_text_in_dietary_cell removeAllObjects];
    [array_text_in_cuisens_cell removeAllObjects];
    
    
    ary_delivaryaddress =[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Delivery_Address"];
    array_kitchen_imgs=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Kitchen_info"] valueForKey:@"KitchenImages"];
    array_text_in_dietary_cell=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Menu"] valueForKey:@"Dietary_RestrictionsArray"];
    array_text_in_cuisens_cell=[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Menu"] valueForKey:@"FavoriteCuisinesArray"];
    ary_servingtype=[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Serving_Type"];
    
    [self countryList];
    
    [self Dishlist];
    [self Reviews];
    
    
    // [self AFcuntry_list];
    //  [self ChefEditeProfile];
    
    
}



-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    
    UIButton *icon_back_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back_arrow.frame = CGRectMake(10,13,25,20);
    icon_back_arrow .backgroundColor = [UIColor clearColor];
    [icon_back_arrow addTarget:self action:@selector(btn_back_arrow_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_back_arrow setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_back_arrow];
    
    
    UILabel *lbl_my_profile = [[UILabel alloc]init];
    lbl_my_profile.frame = CGRectMake(CGRectGetMaxX(icon_back_arrow.frame)+30,0, 150, 45);
    lbl_my_profile.text = @"My Profile";
    lbl_my_profile.font = [UIFont fontWithName:kFont size:20];
    lbl_my_profile.textColor = [UIColor whiteColor];
    lbl_my_profile.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_my_profile];
    
    UIImageView *icon_logo = [[UIImageView alloc]init];
    icon_logo.frame = CGRectMake(WIDTH-40, 9, 30, 30);
    [icon_logo setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_logo setUserInteractionEnabled:YES];
    [img_header addSubview:icon_logo];
    
}
-(void)integrateBody
{
    
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.bounces = NO;
    scroll.delegate = self;
    scroll.backgroundColor =  [UIColor clearColor];
    scroll.frame = CGRectMake(0,0, WIDTH, 700);
    scroll.scrollEnabled = YES;
    //    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    img_bg = [[UIImageView alloc]init];
    img_bg .frame = CGRectMake(0,25, WIDTH, 210);
    //    NSString *ImagePath =  [[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"ProfileImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //    [img_bg setImageWithURL:[NSURL URLWithString:ImagePath] placeholderImage:[UIImage imageNamed:@"placeholder@2x.png"]];
    [img_bg  setImage:[UIImage imageNamed:@"bg2-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg  setUserInteractionEnabled:YES];
    [ scroll addSubview:img_bg ];
    
    //    chef_img = [UIButton buttonWithType:UIButtonTypeCustom];
    //    chef_img.frame = CGRectMake(140,30,80,80);
    //    chef_img.backgroundColor = [UIColor clearColor];
    //    [chef_img addTarget:self action:@selector(btn_img_user_click:) forControlEvents:UIControlEventTouchUpInside];
    //   // [chef_img setImage:[UIImage imageNamed:@"img-chef@2x.png"] forState:UIControlStateNormal];
    //    [img_bg  addSubview:chef_img];
    
    chef_img = [[UIImageView alloc]init];
    chef_img.frame = CGRectMake(140,30,80,80);
    NSString *ImagePath1 =  [[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"ProfileImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [chef_img setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@"placeholder@2x.png"]];
    [chef_img setUserInteractionEnabled:YES];
    chef_img.layer.cornerRadius = 80/2;
    chef_img.clipsToBounds = YES;
    chef_img.backgroundColor = [UIColor clearColor];
    
    [chef_img setImage:[UIImage imageNamed:@"img-chef@2x.png"]];
    [img_bg addSubview:chef_img];
    
    UILabel *lbl_chef_serve = [[UILabel alloc]init];
    lbl_chef_serve.frame = CGRectMake(140,60, 80,20);
    lbl_chef_serve.text = [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"user_serve_type"];
    lbl_chef_serve.font = [UIFont fontWithName:kFontBold size:10];
    lbl_chef_serve.textColor = [UIColor colorWithRed:0/255.0f green:222/255.0f blue:235/255.0f alpha:1];
    lbl_chef_serve.backgroundColor = [UIColor blackColor];
    lbl_chef_serve.textAlignment = NSTextAlignmentCenter;
    [img_bg addSubview:lbl_chef_serve];
    
    
    
    
    profileimg_button = [[UIButton alloc] init];
    profileimg_button.backgroundColor=[UIColor clearColor];
    [profileimg_button addTarget:self action:@selector(profilepic_clickbtn:) forControlEvents:UIControlEventTouchUpInside ];
    [chef_img addSubview:profileimg_button];
    
    
    
    
    
    UILabel *lbl_chef_name = [[UILabel alloc]init];
    lbl_chef_name.frame = CGRectMake(140,CGRectGetMaxY(chef_img.frame), 200,45);
    lbl_chef_name.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"UserName"];
    lbl_chef_name.font = [UIFont fontWithName:kFontBold size:15];
    lbl_chef_name.textColor = [UIColor whiteColor];
    lbl_chef_name.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_chef_name];
    
    UIButton *img_cm = [UIButton buttonWithType:UIButtonTypeCustom];
    img_cm.frame = CGRectMake(330,45,20,30);
    img_cm.backgroundColor = [UIColor clearColor];
    [img_cm addTarget:self action:@selector(btn_on_edite_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_cm setImage:[UIImage imageNamed:@"icon-edit@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:img_cm];
    
    
    
#pragma collection view
    
    layout = [[UICollectionViewFlowLayout alloc] init];
    collView_for_icons = [[UICollectionView alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(chef_img.frame)+45,WIDTH,65)
                                            collectionViewLayout:layout];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_icons setDataSource:self];
    [collView_for_icons setDelegate:self];
    collView_for_icons.scrollEnabled = YES;
    collView_for_icons.showsVerticalScrollIndicator = NO;
    collView_for_icons.showsHorizontalScrollIndicator = NO;
    collView_for_icons.pagingEnabled = YES;
    [collView_for_icons registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_icons setBackgroundColor:[UIColor clearColor]];
    layout.minimumInteritemSpacing = 12;
    layout.minimumLineSpacing = 0;
    collView_for_icons.userInteractionEnabled = YES;
    [img_bg  addSubview: collView_for_icons];
    
#pragma BODY FOR INFO
    
    img_strip = [[UIImageView alloc]init];
    img_strip.frame = CGRectMake(0,CGRectGetMaxY(collView_for_icons.frame)+2,78, 3);
    [img_strip setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    //img_strip.backgroundColor = [UIColor redColor];
    [img_strip setUserInteractionEnabled:YES];
    //[img_bg addSubview:img_strip];
    
    //view for info
    
    view_personal = [[UIView alloc]init];
    view_personal.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,1350);
    view_personal.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_personal];
    
    UIImageView *img_bg1 = [[UIImageView alloc]init];
    img_bg1.frame = CGRectMake(0,-10, WIDTH+3, 1350);
    [img_bg1  setImage:[UIImage imageNamed:@"img-whtcell-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg1  setUserInteractionEnabled:YES];
    [view_personal addSubview:img_bg1 ];
    
    txt_view_for_chef_description =[[UITextView alloc]init];
    txt_view_for_chef_description .frame = CGRectMake(10,20, WIDTH-80, 130);
    txt_view_for_chef_description.scrollEnabled=YES;
    txt_view_for_chef_description.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"About_Us"];
    //txt_view_for_user_description.numberOfLines = 5
    txt_view_for_chef_description.userInteractionEnabled=NO;
    txt_view_for_chef_description.font=[UIFont fontWithName:kFont size:14];
    // txt_view_for_chef_description.backgroundColor=[UIColor whiteColor];
    txt_view_for_chef_description.delegate=self;
    txt_view_for_chef_description.textColor=[UIColor blackColor];
    //    txt_view_for_chef_description.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    //    txt_view_for_chef_description.layer.borderWidth=1.0f;
    //    txt_view_for_chef_description.clipsToBounds=YES;
    [img_bg1 addSubview:txt_view_for_chef_description];
    
    UIImageView *img_linee = [[UIImageView alloc]init];
    img_linee.frame = CGRectMake(10,CGRectGetMidY(txt_view_for_chef_description.frame), WIDTH-20, 0.5);
    [img_linee  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_linee  setUserInteractionEnabled:YES];
    [img_bg1 addSubview:img_linee];
    
    UIImageView * icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(20,35,30,33);
    [icon_user setUserInteractionEnabled:NO];
    icon_user.image=[UIImage imageNamed:@"user1-icon@2x.png"];
    [img_bg1 addSubview:icon_user];
    
    UILabel *lbl_user_name = [[UILabel alloc]init];
    lbl_user_name.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,20,200,45);
    lbl_user_name.text = @"Username";
    lbl_user_name.font = [UIFont fontWithName:kFontBold size:12];
    lbl_user_name.textColor = [UIColor blackColor];
    lbl_user_name.backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:lbl_user_name];
    
    txt_first_name = [[UITextField alloc] init];
    txt_first_name.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_user_name.frame),300,45);
    txt_first_name .borderStyle = UITextBorderStyleNone;
    txt_first_name .textColor = [UIColor blackColor];
    txt_first_name .font = [UIFont fontWithName:kFont size:12];
    txt_first_name .placeholder = @"Name";
    [txt_first_name  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_first_name  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_first_name .leftView = padding;
    txt_first_name .leftViewMode = UITextFieldViewModeAlways;
    txt_first_name .userInteractionEnabled=NO;
    txt_first_name .textAlignment = NSTextAlignmentLeft;
    txt_first_name .backgroundColor = [UIColor clearColor];
    txt_first_name .keyboardType = UIKeyboardTypeAlphabet;
    txt_first_name .delegate = self;
    txt_first_name.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"FirstName"];
    
    [img_bg1 addSubview:txt_first_name ];
    
    UIImageView *img_line = [[UIImageView alloc]init];
    img_line.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMidY(txt_first_name.frame)+10, WIDTH-80, 0.5);
    [img_line  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line  setUserInteractionEnabled:YES];
    [img_bg1 addSubview:img_line];
    
    
    UIImageView * icon_user2 = [[UIImageView alloc]init];
    icon_user2.frame = CGRectMake(20,35,30,33);
    [icon_user2 setUserInteractionEnabled:NO];
    icon_user2.image=[UIImage imageNamed:@"user1-icon@2x.png"];
    [img_bg1 addSubview:icon_user2];
    
    
    UILabel *lbl_full_name = [[UILabel alloc]init];
    lbl_full_name.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,20,200,45);
    lbl_full_name.text = @"Full Name";
    lbl_full_name.font = [UIFont fontWithName:kFontBold size:12];
    lbl_full_name.textColor = [UIColor blackColor];
    lbl_full_name.backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:lbl_full_name];
    
    
    
    txt_full_name = [[UITextField alloc] init];
    txt_full_name.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_full_name.frame),300,45);
    txt_full_name .borderStyle = UITextBorderStyleNone;
    txt_full_name .textColor = [UIColor blackColor];
    txt_full_name .font = [UIFont fontWithName:kFont size:12];
    txt_full_name .placeholder = @"Name";
    [txt_full_name  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_full_name  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_full_name .leftView = padding2;
    txt_full_name .leftViewMode = UITextFieldViewModeAlways;
    txt_full_name .userInteractionEnabled=NO;
    txt_full_name .textAlignment = NSTextAlignmentLeft;
    txt_full_name .backgroundColor = [UIColor clearColor];
    txt_full_name .keyboardType = UIKeyboardTypeAlphabet;
    txt_full_name .delegate = self;
    txt_full_name.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Full_Name"];
    
    [img_bg1 addSubview:txt_full_name ];
    
    UIImageView *img_line2 = [[UIImageView alloc]init];
    img_line2.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMidY(txt_middle_name.frame)+10, WIDTH-80, 0.5);
    [img_line2 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line2  setUserInteractionEnabled:NO];
    [img_bg1 addSubview:img_line2];
    
    
    UIImageView * icon_for_chef_type = [[UIImageView alloc]init];
    icon_for_chef_type.frame = CGRectMake(20,CGRectGetMaxY(img_line2.frame)+20,30,33);
    [icon_for_chef_type setUserInteractionEnabled:YES];
    icon_for_chef_type.image=[UIImage imageNamed:@"food-icon@2x.png"];
    [img_bg1 addSubview:icon_for_chef_type];
    
    
    UILabel *lbl_chef_type = [[UILabel alloc]init];
    lbl_chef_type.frame = CGRectMake(CGRectGetMaxX(icon_for_chef_type.frame)+10,CGRectGetMidY(img_line2.frame),200,45);
    lbl_chef_type.text = @"Chef Type";
    lbl_chef_type.font = [UIFont fontWithName:kFontBold size:12];
    lbl_chef_type.textColor = [UIColor blackColor];
    lbl_chef_type.backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:lbl_chef_type];
    
    
    txt_chef_type = [[UITextField alloc] init];
    txt_chef_type.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_chef_type.frame),300,45);
    txt_chef_type .borderStyle = UITextBorderStyleNone;
    txt_chef_type .textColor = [UIColor blackColor];
    txt_chef_type .font = [UIFont fontWithName:kFont size:12];
    txt_chef_type .placeholder = @"Home Chef";
    [txt_chef_type  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_chef_type  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_chef_type .leftView = padding4;
    txt_chef_type .leftViewMode = UITextFieldViewModeAlways;
    txt_chef_type .userInteractionEnabled=NO;
    txt_chef_type .textAlignment = NSTextAlignmentLeft;
    txt_chef_type .backgroundColor = [UIColor clearColor];
    txt_chef_type .keyboardType = UIKeyboardTypeAlphabet;
    txt_chef_type .delegate = self;
    txt_chef_type.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Chef_Type"];
    
    [img_bg1 addSubview:txt_chef_type ];
    
    
    
    UIImageView *img_line3 = [[UIImageView alloc]init];
    img_line3.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMidY(txt_middle_name.frame)+10, WIDTH-80, 0.5);
    [img_line3 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line3  setUserInteractionEnabled:YES];
    [img_bg1 addSubview:img_line3];
    
    
    UIImageView * icon_date_of_birth = [[UIImageView alloc]init];
    icon_date_of_birth.frame = CGRectMake(20,CGRectGetMaxY(img_line3.frame)+20,30,33);
    [icon_date_of_birth setUserInteractionEnabled:YES];
    icon_date_of_birth.image=[UIImage imageNamed:@"icon-date-of-birth@2x.png"];
    [img_bg1 addSubview:icon_date_of_birth];
    
    UILabel *lbl_date_of_birth = [[UILabel alloc]init];
    lbl_date_of_birth.frame = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line3.frame),200,45);
    lbl_date_of_birth.text = @"Date of Birth";
    lbl_date_of_birth.font = [UIFont fontWithName:kFontBold size:12];
    lbl_date_of_birth.textColor = [UIColor blackColor];
    lbl_date_of_birth.backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:lbl_date_of_birth];
    
    
    txt_date_of_birth = [[UITextField alloc] init];
    txt_date_of_birth.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_date_of_birth.frame),300,45);
    txt_date_of_birth .borderStyle = UITextBorderStyleNone;
    txt_date_of_birth .textColor = [UIColor blackColor];
    txt_date_of_birth .font = [UIFont fontWithName:kFont size:12];
    txt_date_of_birth .placeholder = @"Date of Birth";
    [txt_date_of_birth  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_date_of_birth  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding5 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_date_of_birth .leftView = padding5;
    txt_date_of_birth .leftViewMode = UITextFieldViewModeAlways;
    txt_date_of_birth .userInteractionEnabled=NO;
    txt_date_of_birth .textAlignment = NSTextAlignmentLeft;
    txt_date_of_birth .backgroundColor = [UIColor clearColor];
    txt_date_of_birth .keyboardType = UIKeyboardTypeAlphabet;
    txt_date_of_birth.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"DateOfBirth"];
    
    txt_date_of_birth .delegate = self;
    [img_bg1 addSubview:txt_date_of_birth ];
    
    
    
    UIImageView *img_line4 = [[UIImageView alloc]init];
    img_line4.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMidY(txt_middle_name.frame)+10, WIDTH-80, 0.5);
    [img_line4 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line4  setUserInteractionEnabled:YES];
    [img_bg1 addSubview:img_line4];
    
    
    
    UIImageView * icon_social_id = [[UIImageView alloc]init];
    icon_social_id.frame = CGRectMake(20,CGRectGetMaxY(img_line4.frame)+20,30,33);
    [icon_social_id setUserInteractionEnabled:YES];
    icon_social_id.image=[UIImage imageNamed:@"icon-social@2x.png"];
    [img_bg1 addSubview:icon_social_id];
    
    UILabel *lbl_social_id = [[UILabel alloc]init];
    lbl_social_id.frame = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line3.frame),200,45);
    lbl_social_id.text = @"Social Security no./National ID";
    lbl_social_id.font = [UIFont fontWithName:kFontBold size:12];
    lbl_social_id.textColor = [UIColor blackColor];
    lbl_social_id.backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:lbl_social_id];
    
    
    txt_socilid_number = [[UITextField alloc] init];
    txt_socilid_number.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_social_id.frame),300,45);
    txt_socilid_number .borderStyle = UITextBorderStyleNone;
    txt_socilid_number .textColor = [UIColor blackColor];
    txt_socilid_number .font = [UIFont fontWithName:kFont size:12];
    txt_socilid_number .placeholder = @"Social number";
    [txt_socilid_number  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_socilid_number  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_socilid_number .leftView = padding6;
    txt_socilid_number .leftViewMode = UITextFieldViewModeAlways;
    txt_socilid_number .userInteractionEnabled=NO;
    txt_socilid_number .textAlignment = NSTextAlignmentLeft;
    txt_socilid_number .backgroundColor = [UIColor clearColor];
    txt_socilid_number .keyboardType = UIKeyboardTypeAlphabet;
    txt_socilid_number .delegate = self;
    txt_socilid_number.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"ational_id"];
    
    [img_bg1 addSubview:txt_socilid_number ];
    
    
    
    UIImageView *img_line5 = [[UIImageView alloc]init];
    img_line5.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMidY(txt_socilid_number.frame)+10, WIDTH-80, 0.5);
    [img_line5 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line5  setUserInteractionEnabled:YES];
    [img_bg1 addSubview:img_line5];
    
    
    UIImageView * icon_message = [[UIImageView alloc]init];
    icon_message.frame = CGRectMake(20,CGRectGetMaxY(img_line5.frame)+20,30,33);
    [icon_message setUserInteractionEnabled:YES];
    icon_message.image=[UIImage imageNamed:@"icon-msg@2x.png"];
    [img_bg1 addSubview:icon_message];
    
    UILabel *lbl_email = [[UILabel alloc]init];
    lbl_email.frame = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line5.frame),200,45);
    lbl_email.text = @"Email address";
    lbl_email.font = [UIFont fontWithName:kFontBold size:12];
    lbl_email.textColor = [UIColor blackColor];
    lbl_email.backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:lbl_email];
    
    
    txt_email_address = [[UITextField alloc] init];
    txt_email_address.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_email.frame),300,45);
    txt_email_address .borderStyle = UITextBorderStyleNone;
    txt_email_address .textColor = [UIColor blackColor];
    txt_email_address .font = [UIFont fontWithName:kFont size:12];
    txt_email_address .placeholder = @"Emailaddress";
    [txt_email_address  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_email_address  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding7 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_email_address .leftView = padding7;
    txt_email_address .leftViewMode = UITextFieldViewModeAlways;
    txt_email_address .userInteractionEnabled=NO;
    txt_email_address .textAlignment = NSTextAlignmentLeft;
    txt_email_address .backgroundColor = [UIColor clearColor];
    txt_email_address .keyboardType = UIKeyboardTypeAlphabet;
    txt_email_address .delegate = self;
    txt_email_address.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Email_Address"];
    
    [img_bg1 addSubview:txt_email_address ];
    
    
    
    UIImageView *img_line6 = [[UIImageView alloc]init];
    img_line6.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMidY(txt_socilid_number.frame)+10, WIDTH-80, 0.5);
    [img_line6 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line6  setUserInteractionEnabled:YES];
    [img_bg1 addSubview:img_line6];
    
    
    UIImageView * icon_phone = [[UIImageView alloc]init];
    icon_phone.frame = CGRectMake(20,CGRectGetMaxY(img_line6.frame)+20,30,33);
    [icon_phone setUserInteractionEnabled:YES];
    icon_phone.image=[UIImage imageNamed:@"icon-phone2x.png"];
    [img_bg1 addSubview:icon_phone];
    
    UILabel *lbl_mobile_no = [[UILabel alloc]init];
    lbl_mobile_no.frame = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line6.frame),200,45);
    lbl_mobile_no.text = @"Mobile no.";
    lbl_mobile_no.font = [UIFont fontWithName:kFontBold size:12];
    lbl_mobile_no.textColor = [UIColor blackColor];
    lbl_mobile_no.backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:lbl_mobile_no];
    
    
    txt_mobile_number = [[UITextField alloc] init];
    txt_mobile_number.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_email.frame),300,45);
    txt_mobile_number .borderStyle = UITextBorderStyleNone;
    txt_mobile_number .textColor = [UIColor blackColor];
    txt_mobile_number .font = [UIFont fontWithName:kFont size:12];
    txt_mobile_number .placeholder = @"1234565";
    [txt_mobile_number  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_mobile_number  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding8 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_mobile_number .leftView = padding8;
    txt_mobile_number .leftViewMode = UITextFieldViewModeAlways;
    txt_mobile_number .userInteractionEnabled=NO;
    txt_mobile_number .textAlignment = NSTextAlignmentLeft;
    txt_mobile_number .backgroundColor = [UIColor clearColor];
    txt_mobile_number .keyboardType = UIKeyboardTypeAlphabet;
    txt_mobile_number .delegate = self;
    txt_mobile_number.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Mobile_Number"];
    
    [img_bg1 addSubview:txt_mobile_number ];
    
    
    
    UIImageView *img_line7 = [[UIImageView alloc]init];
    img_line7.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMidY(txt_mobile_number.frame)+10, WIDTH-80, 0.5);
    [img_line7 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line7  setUserInteractionEnabled:YES];
    [img_bg1 addSubview:img_line7];
    
    
    UIImageView * icon_home = [[UIImageView alloc]init];
    icon_home.frame = CGRectMake(20,CGRectGetMaxY(img_line6.frame)+20,30,33);
    [icon_home setUserInteractionEnabled:YES];
    icon_home.image=[UIImage imageNamed:@"icon-Home@2x.png"];
    [img_bg1 addSubview:icon_home];
    
    UILabel *lbl_home = [[UILabel alloc]init];
    lbl_home.frame = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line6.frame),200,45);
    lbl_home.text = @"Home Address";
    lbl_home.font = [UIFont fontWithName:kFontBold size:12];
    lbl_home.textColor = [UIColor blackColor];
    lbl_home.backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:lbl_home];
    
    
    txt_home_address = [[UITextField alloc] init];
    txt_home_address.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_email.frame),300,45);
    txt_home_address .borderStyle = UITextBorderStyleNone;
    txt_home_address .textColor = [UIColor blackColor];
    txt_home_address .font = [UIFont fontWithName:kFont size:12];
    txt_home_address .placeholder = @"1234565";
    [txt_home_address  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_home_address  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding9 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_home_address .leftView = padding9;
    txt_home_address .leftViewMode = UITextFieldViewModeAlways;
    txt_home_address .userInteractionEnabled=NO;
    txt_home_address .textAlignment = NSTextAlignmentLeft;
    txt_home_address .backgroundColor = [UIColor clearColor];
    txt_home_address .keyboardType = UIKeyboardTypeAlphabet;
    txt_home_address .delegate = self;
    txt_home_address.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Home_Address"];
    
    [img_bg1 addSubview:txt_home_address ];
    
    
    
    UIImageView *img_line8 = [[UIImageView alloc]init];
    img_line8.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMidY(txt_mobile_number.frame)+10, WIDTH-80, 0.5);
    [img_line8 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line8  setUserInteractionEnabled:YES];
    [img_bg1 addSubview:img_line8];
    
    
    UIImageView * icon_servingtype = [[UIImageView alloc]init];
    icon_servingtype.frame = CGRectMake(20,CGRectGetMaxY(img_line6.frame)+20,30,33);
    [icon_servingtype setUserInteractionEnabled:YES];
    icon_servingtype.image=[UIImage imageNamed:@"icon-phone2x.png"];
    [img_bg1 addSubview:icon_servingtype];
    
    UILabel *lbl_serving_type = [[UILabel alloc]init];
    lbl_serving_type.frame = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line6.frame),200,45);
    lbl_serving_type.text = @"Serving Type";
    lbl_serving_type.font = [UIFont fontWithName:kFontBold size:12];
    lbl_serving_type.textColor = [UIColor blackColor];
    lbl_serving_type.backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:lbl_serving_type];
    
    
    txt_serving_type = [[UITextField alloc] init];
    txt_serving_type.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_email.frame),300,45);
    txt_serving_type .borderStyle = UITextBorderStyleNone;
    txt_serving_type .textColor = [UIColor blackColor];
    txt_serving_type .font = [UIFont fontWithName:kFont size:12];
    txt_serving_type .placeholder = @"1234565";
    [txt_serving_type  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_serving_type  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding10 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_serving_type .leftView = padding10;
    txt_serving_type .leftViewMode = UITextFieldViewModeAlways;
    txt_serving_type .userInteractionEnabled=NO;
    txt_serving_type .textAlignment = NSTextAlignmentLeft;
    txt_serving_type .backgroundColor = [UIColor clearColor];
    txt_serving_type .keyboardType = UIKeyboardTypeAlphabet;
    txt_serving_type .delegate = self;
    [img_bg1 addSubview:txt_serving_type ];
    
    
    
    UIImageView *img_line9 = [[UIImageView alloc]init];
    img_line9.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMidY(txt_mobile_number.frame)+10, WIDTH-80, 0.5);
    [img_line9 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line9  setUserInteractionEnabled:YES];
    [img_bg1 addSubview:img_line9];
    
    
    
    UIImageView * icon_cooking_Qualification = [[UIImageView alloc]init];
    icon_cooking_Qualification.frame = CGRectMake(20,CGRectGetMaxY(img_line6.frame)+20,30,33);
    [icon_cooking_Qualification setUserInteractionEnabled:YES];
    icon_cooking_Qualification.image=[UIImage imageNamed:@"cooking-Q-icon@2x.png"];
    [img_bg1 addSubview:icon_cooking_Qualification];
    
    UILabel *lbl_coocking_qualification = [[UILabel alloc]init];
    lbl_coocking_qualification.frame = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line6.frame),200,45);
    lbl_coocking_qualification.text = @"Cooking Qualification";
    lbl_coocking_qualification.font = [UIFont fontWithName:kFontBold size:12];
    lbl_coocking_qualification.textColor = [UIColor blackColor];
    lbl_coocking_qualification.backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:lbl_coocking_qualification];
    
    
    txt_cooking_qualification = [[UITextField alloc] init];
    txt_cooking_qualification.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_email.frame),300,45);
    txt_cooking_qualification .borderStyle = UITextBorderStyleNone;
    txt_cooking_qualification .textColor = [UIColor blackColor];
    txt_cooking_qualification .font = [UIFont fontWithName:kFont size:12];
    txt_cooking_qualification .placeholder = @"Dine-in";
    [txt_cooking_qualification  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_cooking_qualification  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding11 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_cooking_qualification .leftView = padding11;
    txt_cooking_qualification .leftViewMode = UITextFieldViewModeAlways;
    txt_cooking_qualification .userInteractionEnabled=NO;
    txt_cooking_qualification .textAlignment = NSTextAlignmentLeft;
    txt_cooking_qualification .backgroundColor = [UIColor clearColor];
    txt_cooking_qualification .keyboardType = UIKeyboardTypeAlphabet;
    txt_cooking_qualification .delegate = self;
    txt_cooking_qualification.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Cooking_Qualification"];
    
    [img_bg1 addSubview:txt_cooking_qualification ];
    
    
    
    UIImageView *img_line10 = [[UIImageView alloc]init];
    img_line10.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMidY(txt_mobile_number.frame)+10, WIDTH-80, 0.5);
    [img_line10 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line10  setUserInteractionEnabled:YES];
    [img_bg1 addSubview:img_line10];
    
    
    UIImageView * icon_cooking_EX = [[UIImageView alloc]init];
    icon_cooking_EX.frame = CGRectMake(20,CGRectGetMaxY(img_line6.frame)+20,30,33);
    [icon_cooking_EX setUserInteractionEnabled:YES];
    icon_cooking_EX.image=[UIImage imageNamed:@"cooking-Ex-icon@2x.png"];
    [img_bg1 addSubview:icon_cooking_EX];
    
    UILabel *lbl_coocking_EX = [[UILabel alloc]init];
    lbl_coocking_EX.frame = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line6.frame),200,45);
    lbl_coocking_EX.text = @"Cooking Experience";
    lbl_coocking_EX.font = [UIFont fontWithName:kFontBold size:12];
    lbl_coocking_EX.textColor = [UIColor blackColor];
    lbl_coocking_EX.backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:lbl_coocking_EX];
    
    
    txt_cooking_ex = [[UITextField alloc] init];
    txt_cooking_ex.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_email.frame),300,45);
    txt_cooking_ex .borderStyle = UITextBorderStyleNone;
    txt_cooking_ex .textColor = [UIColor blackColor];
    txt_cooking_ex .font = [UIFont fontWithName:kFont size:12];
    txt_cooking_ex .placeholder = @"6-10 years";
    [txt_cooking_ex  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_cooking_ex  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding12 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_cooking_ex .leftView = padding12;
    txt_cooking_ex .leftViewMode = UITextFieldViewModeAlways;
    txt_cooking_ex .userInteractionEnabled=NO;
    txt_cooking_ex .textAlignment = NSTextAlignmentLeft;
    txt_cooking_ex .backgroundColor = [UIColor clearColor];
    txt_cooking_ex .keyboardType = UIKeyboardTypeAlphabet;
    txt_cooking_ex .delegate = self;
    txt_cooking_ex.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Cooking_Experience"];
    
    [img_bg1 addSubview:txt_cooking_ex ];
    
    
    
    UIImageView *img_line11 = [[UIImageView alloc]init];
    img_line11.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMidY(txt_mobile_number.frame)+10, WIDTH-80, 0.5);
    [img_line11 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line11  setUserInteractionEnabled:YES];
    [img_bg1 addSubview:img_line11];
    
    
    
    
    
    UIImageView * icon_paypal = [[UIImageView alloc]init];
    icon_paypal.frame = CGRectMake(20,CGRectGetMaxY(img_line6.frame)+20,30,33);
    [icon_paypal setUserInteractionEnabled:YES];
    icon_paypal.image=[UIImage imageNamed:@"icon-paypal@2x.png"];
    [img_bg1 addSubview:icon_paypal];
    
    UILabel *lbl_paypal_ac = [[UILabel alloc]init];
    lbl_paypal_ac.frame = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line6.frame),200,45);
    lbl_paypal_ac.text = @"Paypal Account";
    lbl_paypal_ac.font = [UIFont fontWithName:kFontBold size:12];
    lbl_paypal_ac.textColor = [UIColor blackColor];
    lbl_paypal_ac.backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:lbl_paypal_ac];
    
    
    txt_paypal_account = [[UITextField alloc] init];
    txt_paypal_account.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_email.frame),300,45);
    txt_paypal_account .borderStyle = UITextBorderStyleNone;
    txt_paypal_account .textColor = [UIColor blackColor];
    txt_paypal_account .font = [UIFont fontWithName:kFont size:12];
    txt_paypal_account .placeholder = @"Charless@gmail.com";
    [txt_paypal_account  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_paypal_account  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding13 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_paypal_account .leftView = padding13;
    txt_paypal_account .leftViewMode = UITextFieldViewModeAlways;
    txt_paypal_account .userInteractionEnabled=NO;
    txt_paypal_account .textAlignment = NSTextAlignmentLeft;
    txt_paypal_account .backgroundColor = [UIColor clearColor];
    txt_paypal_account .keyboardType = UIKeyboardTypeAlphabet;
    txt_paypal_account .delegate = self;
    txt_paypal_account.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Paypal_Account"];
    
    [img_bg1 addSubview:txt_paypal_account ];
    
    
    
    UIImageView *img_line12 = [[UIImageView alloc]init];
    img_line12.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMidY(txt_mobile_number.frame)+10, WIDTH-80, 0.5);
    [img_line12 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line12  setUserInteractionEnabled:YES];
    [img_bg1 addSubview:img_line12];
    
    
    
    
    UIImageView * icon_paypal2 = [[UIImageView alloc]init];
    icon_paypal2.frame = CGRectMake(20,CGRectGetMaxY(img_line6.frame)+20,30,33);
    [icon_paypal2 setUserInteractionEnabled:YES];
    icon_paypal2.image=[UIImage imageNamed:@"icon-paypal@2x.png"];
    [img_bg1 addSubview:icon_paypal2];
    
    UILabel *lbl_payment_received = [[UILabel alloc]init];
    lbl_payment_received.frame = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line6.frame),200,45);
    lbl_payment_received.text = @"Payment Received";
    lbl_payment_received.font = [UIFont fontWithName:kFontBold size:12];
    lbl_payment_received.textColor = [UIColor blackColor];
    lbl_payment_received.backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:lbl_payment_received];
    
    
    txt_payment_receiverd_month = [[UITextField alloc] init];
    txt_payment_receiverd_month.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_email.frame),300,45);
    txt_payment_receiverd_month .borderStyle = UITextBorderStyleNone;
    txt_payment_receiverd_month .textColor = [UIColor blackColor];
    txt_payment_receiverd_month .font = [UIFont fontWithName:kFont size:12];
    txt_payment_receiverd_month .placeholder = @"End of Month";
    [txt_payment_receiverd_month  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_payment_receiverd_month  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding14 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_payment_receiverd_month .leftView = padding14;
    txt_payment_receiverd_month .leftViewMode = UITextFieldViewModeAlways;
    txt_payment_receiverd_month .userInteractionEnabled=NO;
    txt_payment_receiverd_month .textAlignment = NSTextAlignmentLeft;
    txt_payment_receiverd_month .backgroundColor = [UIColor clearColor];
    txt_payment_receiverd_month .keyboardType = UIKeyboardTypeAlphabet;
    txt_payment_receiverd_month .delegate = self;
    txt_payment_receiverd_month.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Payment_Recieved"];
    
    [img_bg1 addSubview:txt_payment_receiverd_month ];
    
    
    
    //    UIImageView *img_line13 = [[UIImageView alloc]init];
    //    img_line13.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMidY(txt_mobile_number.frame)+10, WIDTH-80, 0.5);
    //    [img_line13 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //    [img_line13  setUserInteractionEnabled:YES];
    //    [img_bg1 addSubview:img_line13];
    
    
    //    UIImageView * icon_paypal = [[UIImageView alloc]init];
    //    icon_paypal.frame = CGRectMake(24,CGRectGetMaxY(icon_cooking_ex.frame)+20,30,30);
    //    [icon_paypal setUserInteractionEnabled:YES];
    //    icon_paypal.image=[UIImage imageNamed:@"icon-paypal@2x.png"];
    //    [img_bg1 addSubview:icon_paypal];
    //
    
#pragma BODY OF KITCHEN
    
    img_strip1 = [[UIImageView alloc]init];
    img_strip1.frame = CGRectMake(CGRectGetMaxX(img_strip.frame),CGRectGetMaxY(collView_for_icons.frame)+2,77, 3);
    [img_strip1 setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    //img_strip1.backgroundColor = [UIColor redColor];
    [img_strip1 setUserInteractionEnabled:YES];
    //[img_bg addSubview:img_strip1];
    img_strip1.hidden = YES;
    
    //view for kitchen
    
    view_for_kitchen = [[UIView alloc]init];
    view_for_kitchen.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,900);
    view_for_kitchen.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_for_kitchen];
    view_for_kitchen.hidden = YES;
    
    
    UIImageView *img_bg2 = [[UIImageView alloc]init];
    img_bg2.frame = CGRectMake(0,10, WIDTH+2, 250);
    [img_bg2  setImage:[UIImage imageNamed:@"img-whtcell-bg@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [img_bg2  setUserInteractionEnabled:YES];
    [view_for_kitchen addSubview:img_bg2 ];
    
    
    
    UIImageView * icon_kichen = [[UIImageView alloc]init];
    icon_kichen.frame = CGRectMake(20,20,40,40);
    [icon_kichen setUserInteractionEnabled:YES];
    icon_kichen.image=[UIImage imageNamed:@"icon-kichan1@2x.png"];
    [img_bg2 addSubview:icon_kichen];
    
    UILabel *lbl_kitchen_name = [[UILabel alloc]init];
    lbl_kitchen_name.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+10,10,200,45);
    lbl_kitchen_name.text = @"Kitchen Name";
    lbl_kitchen_name.font = [UIFont fontWithName:kFontBold size:12];
    lbl_kitchen_name.textColor = [UIColor blackColor];
    lbl_kitchen_name.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:lbl_kitchen_name];
    
    txt_kitchen_name = [[UITextField alloc] init];
    txt_kitchen_name.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+5,CGRectGetMidY(lbl_kitchen_name.frame),300,45);
    txt_kitchen_name .borderStyle = UITextBorderStyleNone;
    txt_kitchen_name .textColor = [UIColor blackColor];
    txt_kitchen_name .font = [UIFont fontWithName:kFont size:12];
    txt_kitchen_name .placeholder = @"Charles X Restarunt";
    [txt_kitchen_name  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_kitchen_name  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding19 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_kitchen_name .leftView = padding19;
    txt_kitchen_name .leftViewMode = UITextFieldViewModeAlways;
    txt_kitchen_name .userInteractionEnabled=NO;
    txt_kitchen_name .textAlignment = NSTextAlignmentLeft;
    txt_kitchen_name .backgroundColor = [UIColor clearColor];
    txt_kitchen_name .keyboardType = UIKeyboardTypeAlphabet;
    txt_kitchen_name .delegate = self;
    txt_kitchen_name.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Kitchen_info"] valueForKey:@"Kitchen_Name"];
    [img_bg2 addSubview:txt_kitchen_name ];
    
    UIImageView *img_line19 = [[UIImageView alloc]init];
    img_line19.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+10,CGRectGetMidY(txt_kitchen_name.frame)+10, WIDTH-100, 0.5);
    [img_line19  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line19  setUserInteractionEnabled:YES];
    [img_bg2 addSubview:img_line19];
    
    UIImageView * icon_kichen2 = [[UIImageView alloc]init];
    icon_kichen2.frame = CGRectMake(20,CGRectGetMaxY(icon_kichen.frame)+20,40,40);
    [icon_kichen2 setUserInteractionEnabled:YES];
    icon_kichen2.image=[UIImage imageNamed:@"icon-kichan1@2x.png"];
    [img_bg2 addSubview:icon_kichen2];
    
    UILabel *lbl_kitchen_address = [[UILabel alloc]init];
    lbl_kitchen_address.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+10,CGRectGetMaxY(img_line19.frame)+10,200,45);
    lbl_kitchen_address.text = @"Kitchen Address";
    lbl_kitchen_address.font = [UIFont fontWithName:kFontBold size:12];
    lbl_kitchen_address.textColor = [UIColor blackColor];
    lbl_kitchen_address.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:lbl_kitchen_address];
    
    txt_kitchen_address = [[UITextField alloc] init];
    txt_kitchen_address.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+5,CGRectGetMidY(lbl_kitchen_address.frame),300,45);
    txt_kitchen_address .borderStyle = UITextBorderStyleNone;
    txt_kitchen_address .textColor = [UIColor blackColor];
    txt_kitchen_address .font = [UIFont fontWithName:kFont size:12];
    txt_kitchen_address .placeholder = @"Smith St";
    [txt_kitchen_address  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_kitchen_address  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding20 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_kitchen_address .leftView = padding20;
    txt_kitchen_address .leftViewMode = UITextFieldViewModeAlways;
    txt_kitchen_address .userInteractionEnabled=NO;
    txt_kitchen_address .textAlignment = NSTextAlignmentLeft;
    txt_kitchen_address .backgroundColor = [UIColor clearColor];
    txt_kitchen_address .keyboardType = UIKeyboardTypeAlphabet;
    txt_kitchen_address .delegate = self;
    txt_kitchen_address.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Kitchen_info"] valueForKey:@"Kitchen_Address"];
    [img_bg2 addSubview:txt_kitchen_address];
    UIImageView *img_bg_for_kitchen_imgs = [[UIImageView alloc]init];
    img_bg_for_kitchen_imgs.frame = CGRectMake(0,CGRectGetMaxY(img_bg2.frame)+5, WIDTH+2, 250);
    [img_bg_for_kitchen_imgs  setImage:[UIImage imageNamed:@"img-whtcell-bg@2x.png"]];
    //img_bg_for_kitchen_imgs.backgroundColor = [UIColor redColor];
    [img_bg_for_kitchen_imgs  setUserInteractionEnabled:YES];
    [view_for_kitchen addSubview:img_bg_for_kitchen_imgs];
    
    UIImageView * img_video = [[UIImageView alloc]init];
    img_video.frame = CGRectMake(20,10,330,100);
    [img_video setUserInteractionEnabled:YES];
    
    NSString *ImagePath3 =  [[NSString stringWithFormat:@"%@",[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Dine-In"] valueForKey:@"Dine_In_Pictures"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [img_video setImageWithURL:[NSURL URLWithString:ImagePath3] placeholderImage:[UIImage imageNamed:@"placeholder@2x.png"]];
    
    // img_video.image=[UIImage imageNamed:@"img-dine-area@2x.png"];
    [img_bg_for_kitchen_imgs addSubview:img_video];
    
    layout2 = [[UICollectionViewFlowLayout alloc] init];
    if(IS_IPHONE_6Plus)
    {
        collView_for_kitchen_imgs = [[UICollectionView alloc] initWithFrame:CGRectMake(20,CGRectGetMaxY(img_video.frame)+60,370,225)
                                                       collectionViewLayout:layout2];
    }
    else if(IS_IPHONE_6)
    {
        collView_for_kitchen_imgs = [[UICollectionView alloc] initWithFrame:CGRectMake(20,CGRectGetMaxY(img_video.frame)+5,335,210)
                                                       collectionViewLayout:layout2];
    }
    else if(IS_IPHONE_5)
    {
        collView_for_kitchen_imgs = [[UICollectionView alloc] initWithFrame:CGRectMake(20,CGRectGetMaxY(img_video.frame)+5,280,225)
                                                       collectionViewLayout:layout2];
    }
    else
    {
        collView_for_kitchen_imgs = [[UICollectionView alloc] initWithFrame:CGRectMake(20,CGRectGetMaxY(img_video.frame)+5,335,225)
                                                       collectionViewLayout:layout2];
    }
    [layout2 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_kitchen_imgs setDataSource:self];
    [collView_for_kitchen_imgs setDelegate:self];
    collView_for_kitchen_imgs.scrollEnabled = YES;
    collView_for_kitchen_imgs.showsVerticalScrollIndicator = YES;
    collView_for_kitchen_imgs.showsHorizontalScrollIndicator = NO;
    collView_for_kitchen_imgs.pagingEnabled = YES;
    [collView_for_kitchen_imgs registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_kitchen_imgs setBackgroundColor:[UIColor clearColor]];
    layout2.minimumInteritemSpacing = 5;
    layout2.minimumLineSpacing = 0;
    collView_for_kitchen_imgs.userInteractionEnabled = YES;
    [img_bg_for_kitchen_imgs  addSubview: collView_for_kitchen_imgs];
    
    
    
    UIImageView *img_bg_for_food_storage = [[UIImageView alloc]init];
    img_bg_for_food_storage.frame = CGRectMake(0,CGRectGetMaxY(img_bg_for_kitchen_imgs.frame)+5, WIDTH+2, 350);
    [img_bg_for_food_storage  setImage:[UIImage imageNamed:@"img-whtcell-bg@2x.png"]];
    //img_bg_for_food_storage.backgroundColor = [UIColor redColor];
    [img_bg_for_food_storage  setUserInteractionEnabled:YES];
    [view_for_kitchen addSubview:img_bg_for_food_storage];
    
    
    UIImageView * icon_frize = [[UIImageView alloc]init];
    icon_frize.frame = CGRectMake(20,20,30,40);
    [icon_frize setUserInteractionEnabled:YES];
    icon_frize.image=[UIImage imageNamed:@"icon-storage@2x.png"];
    [img_bg_for_food_storage addSubview:icon_frize];
    
    UILabel *lbl_food_storage = [[UILabel alloc]init];
    lbl_food_storage.frame = CGRectMake(CGRectGetMaxX(icon_frize.frame)+20,5,200,45);
    lbl_food_storage.text = @"Food Storage";
    lbl_food_storage.font = [UIFont fontWithName:kFontBold size:15];
    lbl_food_storage.textColor = [UIColor blackColor];
    lbl_food_storage.backgroundColor = [UIColor clearColor];
    [img_bg_for_food_storage addSubview:lbl_food_storage];
    
    
    txt_view_for_storage_description =[[UITextView alloc]init];
    txt_view_for_storage_description .frame = CGRectMake(CGRectGetMaxX(icon_frize.frame)+20,CGRectGetMaxY(lbl_food_storage.frame), WIDTH-100, 100);
    txt_view_for_storage_description.scrollEnabled=YES;
    txt_view_for_storage_description.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Kitchen_info"] valueForKey:@"Food_Storage"];
    
    //txt_view_for_user_description.numberOfLines = 5
    txt_view_for_storage_description.userInteractionEnabled=YES;
    txt_view_for_storage_description.font=[UIFont fontWithName:kFont size:14];
    txt_view_for_storage_description.backgroundColor=[UIColor whiteColor];
    txt_view_for_storage_description.delegate=self;
    txt_view_for_storage_description.textColor=[UIColor lightGrayColor];
    txt_view_for_storage_description.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    txt_view_for_storage_description.layer.borderWidth=1.0f;
    txt_view_for_storage_description.clipsToBounds=YES;
    [img_bg_for_food_storage addSubview:txt_view_for_storage_description];
    //text_on_popup_bg.numberOfLines = 4;
    
    UILabel *lbl_max_wordes_in_storage = [[UILabel alloc]init];
    lbl_max_wordes_in_storage.frame = CGRectMake(260,CGRectGetMaxY( txt_view_for_storage_description.frame)-15, 100, 45);
    lbl_max_wordes_in_storage.text = @"(500 char.max.)";
    lbl_max_wordes_in_storage.font = [UIFont fontWithName:kFont size:11];
    lbl_max_wordes_in_storage.textColor = [UIColor blackColor];
    lbl_max_wordes_in_storage.backgroundColor = [UIColor clearColor];
    [img_bg_for_food_storage addSubview:lbl_max_wordes_in_storage];
    
    UIImageView * icon_hygienes = [[UIImageView alloc]init];
    icon_hygienes.frame = CGRectMake(20,CGRectGetMaxY(lbl_max_wordes_in_storage.frame),38,40);
    [icon_hygienes setUserInteractionEnabled:YES];
    icon_hygienes.image=[UIImage imageNamed:@"icon-hygiene@2x.png"];
    [img_bg_for_food_storage addSubview:icon_hygienes];
    
    UILabel *lbl_food_hygiene = [[UILabel alloc]init];
    lbl_food_hygiene.frame = CGRectMake(CGRectGetMaxX(icon_hygienes.frame)+20,CGRectGetMidY(lbl_max_wordes_in_storage.frame)+5,200,45);
    lbl_food_hygiene.text = @"Kitchen Hygiene Standards";
    lbl_food_hygiene.font = [UIFont fontWithName:kFontBold size:15];
    lbl_food_hygiene.textColor = [UIColor blackColor];
    lbl_food_hygiene.backgroundColor = [UIColor clearColor];
    [img_bg_for_food_storage addSubview:lbl_food_hygiene];
    
    txt_view_for_hygiene_description =[[UITextView alloc]init];
    txt_view_for_hygiene_description .frame = CGRectMake(CGRectGetMaxX(icon_frize.frame)+20,CGRectGetMaxY(lbl_food_hygiene.frame), WIDTH-100, 100);
    txt_view_for_hygiene_description.scrollEnabled=YES;
    txt_view_for_hygiene_description.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Kitchen_info"] valueForKey:@"Kitchen_Hygien_Standard"];
    //txt_view_for_user_description.numberOfLines = 5
    txt_view_for_hygiene_description.userInteractionEnabled=YES;
    txt_view_for_hygiene_description.font=[UIFont fontWithName:kFont size:14];
    txt_view_for_hygiene_description.backgroundColor=[UIColor whiteColor];
    txt_view_for_hygiene_description.delegate=self;
    txt_view_for_hygiene_description.textColor=[UIColor lightGrayColor];
    txt_view_for_hygiene_description.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    txt_view_for_hygiene_description.layer.borderWidth=1.0f;
    txt_view_for_hygiene_description.clipsToBounds=YES;
    [img_bg_for_food_storage addSubview:txt_view_for_hygiene_description];
    //text_on_popup_bg.numberOfLines = 4;
    
    UILabel *lbl_max_wordes_in_hygiene = [[UILabel alloc]init];
    lbl_max_wordes_in_hygiene.frame = CGRectMake(260,CGRectGetMaxY( txt_view_for_hygiene_description.frame)-15, 100, 45);
    lbl_max_wordes_in_hygiene.text = @"(500 char.max.)";
    lbl_max_wordes_in_hygiene.font = [UIFont fontWithName:kFont size:11];
    lbl_max_wordes_in_hygiene.textColor = [UIColor blackColor];
    lbl_max_wordes_in_hygiene.backgroundColor = [UIColor clearColor];
    [img_bg_for_food_storage addSubview:lbl_max_wordes_in_hygiene];
    
    
#pragma BODY FOR DINE_IN
    
    img_strip2 = [[UIImageView alloc]init];
    img_strip2.frame = CGRectMake(CGRectGetMaxX(img_strip1.frame)+7,CGRectGetMaxY(collView_for_icons.frame)+2,77, 3);
    [img_strip2 setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    //img_strip2.backgroundColor = [UIColor redColor];
    [img_strip2 setUserInteractionEnabled:YES];
    // [img_bg addSubview:img_strip2];
    img_strip2.hidden = YES;
    
    //view for dine-in
    
    view_for_dine_in = [[UIView alloc]init];
    view_for_dine_in.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_dine_in.backgroundColor=[UIColor clearColor];
    [view_for_dine_in setUserInteractionEnabled:YES];
    [scroll  addSubview: view_for_dine_in];
    view_for_dine_in.hidden = YES;
    
    UIImageView *img_bg_for_din_in_area = [[UIImageView alloc]init];
    img_bg_for_din_in_area.frame = CGRectMake(0,10, WIDTH+2,270);
    [img_bg_for_din_in_area  setImage:[UIImage imageNamed:@"img-whtcell-bg@2x.png"]];
    //img_bg_for_food_storage.backgroundColor = [UIColor redColor];
    [img_bg_for_din_in_area  setUserInteractionEnabled:YES];
    [view_for_dine_in addSubview:img_bg_for_din_in_area];
    
    UILabel *lbl_dine_in_area = [[UILabel alloc]init];
    lbl_dine_in_area.frame = CGRectMake(20,0, 200,45);
    lbl_dine_in_area.text = @"Dine-in Area";
    lbl_dine_in_area.font = [UIFont fontWithName:kFontBold size:15];
    lbl_dine_in_area.textColor = [UIColor blackColor];
    lbl_dine_in_area.backgroundColor = [UIColor clearColor];
    [img_bg_for_din_in_area addSubview:lbl_dine_in_area];
    
    UIImageView *img_din_in_area = [[UIImageView alloc]init];
    img_din_in_area.frame = CGRectMake(15,CGRectGetMaxY(lbl_dine_in_area.frame)-5, WIDTH-33,100);
    [img_din_in_area  setImage:[UIImage imageNamed:@"img-dine-area@2x.png"]];
    //img_bg_for_food_storage.backgroundColor = [UIColor redColor];
    [img_din_in_area  setUserInteractionEnabled:YES];
    [img_bg_for_din_in_area addSubview:img_din_in_area];
    
    
    
    //    UIButton *icon_cross = [UIButton buttonWithType:UIButtonTypeCustom];
    //    icon_cross.frame = CGRectMake(300,5,35,35);
    //    icon_cross .backgroundColor = [UIColor clearColor];
    //    [icon_cross addTarget:self action:@selector(click_on_white_cross_btn:) forControlEvents:UIControlEventTouchUpInside];
    //    [icon_cross setImage:[UIImage imageNamed:@"icon-w-cross@2x.png"] forState:UIControlStateNormal];
    //    [img_din_in_area   addSubview:icon_cross];
    //
    
    //    UIButton *icon_edite_img = [UIButton buttonWithType:UIButtonTypeCustom];
    //    icon_edite_img.frame = CGRectMake(300,60,40,40);
    //    icon_edite_img .backgroundColor = [UIColor clearColor];
    //    [icon_edite_img addTarget:self action:@selector(click_on_white_edite_btn:) forControlEvents:UIControlEventTouchUpInside];
    //    [icon_edite_img setImage:[UIImage imageNamed:@"icon-w-edit@2x.png"] forState:UIControlStateNormal];
    //    [img_din_in_area   addSubview:icon_edite_img];
    //
    
    UIImageView *icon_no_of_seats = [[UIImageView alloc]init];
    icon_no_of_seats.frame = CGRectMake(25,CGRectGetMaxY(img_din_in_area.frame)+25, 35,20);
    [icon_no_of_seats  setImage:[UIImage imageNamed:@"icon-No-of-seats@2x.png"]];
    //img_bg_for_food_storage.backgroundColor = [UIColor redColor];
    [icon_no_of_seats  setUserInteractionEnabled:YES];
    [img_bg_for_din_in_area addSubview:icon_no_of_seats];
    
    UILabel *lbl_no_of_seats = [[UILabel alloc]init];
    lbl_no_of_seats.frame = CGRectMake(CGRectGetMaxX(icon_no_of_seats.frame)+10,CGRectGetMaxY(img_din_in_area.frame)+5, 200,45);
    lbl_no_of_seats.text = @"NO. of Seats";
    lbl_no_of_seats.font = [UIFont fontWithName:kFontBold size:12];
    lbl_no_of_seats.textColor = [UIColor blackColor];
    lbl_no_of_seats.backgroundColor = [UIColor clearColor];
    [img_bg_for_din_in_area addSubview:lbl_no_of_seats];
    
    //    UILabel *lbl_no_of_seats_val = [[UILabel alloc]init];
    //    lbl_no_of_seats_val.frame = CGRectMake(CGRectGetMaxX(icon_no_of_seats.frame)+10,CGRectGetMaxY(lbl_no_of_seats.frame)-25, 200,45);
    //    lbl_no_of_seats_val.text = @"10";
    //    lbl_no_of_seats_val.font = [UIFont fontWithName:kFont size:12];
    //    lbl_no_of_seats_val.textColor = [UIColor blackColor];
    //    lbl_no_of_seats_val.backgroundColor = [UIColor clearColor];
    //    [img_bg_for_din_in_area addSubview:lbl_no_of_seats_val];
    
    
    
    txt_no_of_seats_val = [[UITextField alloc] init];
    txt_no_of_seats_val.frame = CGRectMake(CGRectGetMaxX(icon_no_of_seats.frame)+10,CGRectGetMaxY(lbl_no_of_seats.frame)-25, 200,45);
    txt_no_of_seats_val .borderStyle = UITextBorderStyleNone;
    txt_no_of_seats_val .textColor = [UIColor blackColor];
    txt_no_of_seats_val .font = [UIFont fontWithName:kFont size:12];
    txt_no_of_seats_val .placeholder = @"10";
    [txt_no_of_seats_val  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_no_of_seats_val  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding35 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_no_of_seats_val .leftView = padding35;
    txt_no_of_seats_val .leftViewMode = UITextFieldViewModeAlways;
    txt_no_of_seats_val .userInteractionEnabled=NO;
    txt_no_of_seats_val .textAlignment = NSTextAlignmentLeft;
    txt_no_of_seats_val .backgroundColor = [UIColor clearColor];
    txt_no_of_seats_val .keyboardType = UIKeyboardTypeAlphabet;
    txt_no_of_seats_val .delegate = self;
    txt_no_of_seats_val.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Dine-In"] valueForKey:@"Number_Of_Seats"];
    
    [img_bg_for_din_in_area addSubview:txt_no_of_seats_val ];
    
    
    UIImageView *img_line24 = [[UIImageView alloc]init];
    img_line24.frame =  CGRectMake(20,CGRectGetMaxY(txt_no_of_seats_val.frame)-10, 280, 0.5);
    img_line24.backgroundColor =[UIColor grayColor];
    [img_line24 setUserInteractionEnabled:YES];
    [img_bg_for_din_in_area addSubview:img_line24];
    
    UIImageView *icon_parking = [[UIImageView alloc]init];
    icon_parking.frame = CGRectMake(25,CGRectGetMaxY(img_line24.frame)+10, 35,35);
    [icon_parking  setImage:[UIImage imageNamed:@"icon-parking@2x.png"]];
    //img_bg_for_food_storage.backgroundColor = [UIColor redColor];
    [icon_parking  setUserInteractionEnabled:YES];
    [img_bg_for_din_in_area addSubview:icon_parking];
    
    UILabel *lbl_parking = [[UILabel alloc]init];
    lbl_parking.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(img_line24.frame)-2, 200,45);
    lbl_parking.text = @"Parking Available";
    lbl_parking.font = [UIFont fontWithName:kFontBold size:12];
    lbl_parking.textColor = [UIColor blackColor];
    lbl_parking.backgroundColor = [UIColor clearColor];
    [img_bg_for_din_in_area addSubview:lbl_parking];
    
    
    
    
    //    img_radio_btn_on_yes  =[[UIImageView alloc]init];
    //    img_radio_btn_on_yes.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-10,20,20);
    //    [img_radio_btn_on_yes setUserInteractionEnabled:YES];
    //    img_radio_btn_on_yes.backgroundColor=[UIColor clearColor];
    //    img_radio_btn_on_yes.image=[UIImage imageNamed:@"icon-un-selec-radio@2x.png"];
    //    [img_bg_for_din_in_area addSubview:img_radio_btn_on_yes];
    
    
    
    
    UILabel *lbl_parking_yes = [[UILabel alloc]init];
    lbl_parking_yes.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-22, 40,45);
    lbl_parking_yes.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Dine-In"] valueForKey:@"Parking_Available"];
    lbl_parking_yes.font = [UIFont fontWithName:kFont size:15];
    lbl_parking_yes.textColor = [UIColor blackColor];
    lbl_parking_yes.backgroundColor = [UIColor clearColor];
    [img_bg_for_din_in_area addSubview:lbl_parking_yes];
    
    
    //    btn_Yes = [[UIButton alloc] init];
    //    btn_Yes.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-10,20,20);
    //    btn_Yes.backgroundColor = [UIColor clearColor];
    //    [btn_Yes addTarget:self action:@selector(btnYes_Parking:) forControlEvents:UIControlEventTouchUpInside ];
    //    [img_bg_for_din_in_area addSubview:btn_Yes];
    //
    //
    //
    //    img_radio_btn_on_no = [[UIImageView alloc]init];
    //    img_radio_btn_on_no.frame = CGRectMake(CGRectGetMaxX(lbl_parking_yes.frame)+10,CGRectGetMaxY(lbl_parking.frame)-10,20,20);
    //    [img_radio_btn_on_no setUserInteractionEnabled:YES];
    //    img_radio_btn_on_no.backgroundColor=[UIColor clearColor];
    //    img_radio_btn_on_no.image=[UIImage imageNamed:@"icon-un-selec-radio@2x.png"];
    //    [img_bg_for_din_in_area addSubview:img_radio_btn_on_no];
    //
    //
    //    UILabel *lbl_parking_no = [[UILabel alloc]init];
    //    lbl_parking_no.frame = CGRectMake(CGRectGetMaxX(img_radio_btn_on_no.frame)+10,CGRectGetMaxY(lbl_parking.frame)-22, 40,45);
    //    lbl_parking_no.text = @"No";
    //    lbl_parking_no.font = [UIFont fontWithName:kFont size:15];
    //    lbl_parking_no.textColor = [UIColor blackColor];
    //    lbl_parking_no.backgroundColor = [UIColor clearColor];
    //    [img_bg_for_din_in_area addSubview:lbl_parking_no];
    //
    //
    //    UIButton *btn_No = [[UIButton alloc] init];
    //    btn_No.frame = CGRectMake(CGRectGetMaxX(lbl_parking_yes.frame)+10,CGRectGetMaxY(lbl_parking.frame)-10,20,20);
    //    btn_No.backgroundColor = [UIColor clearColor];
    //    [btn_No addTarget:self action:@selector(btnNo_Parking:) forControlEvents:UIControlEventTouchUpInside ];
    //    [img_bg_for_din_in_area addSubview:btn_No];
    //
    
    
    UIButton *btn_update = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_update.frame = CGRectMake(30,CGRectGetMaxY(img_bg_for_din_in_area.frame)+20,WIDTH-60,40);
    btn_update .backgroundColor = [UIColor clearColor];
    [btn_update addTarget:self action:@selector(click_on_update_btn:) forControlEvents:UIControlEventTouchUpInside];
    [btn_update setImage:[UIImage imageNamed:@"img-black-btn@2x.png"] forState:UIControlStateNormal];
    [view_for_dine_in   addSubview:btn_update];
    
    UILabel *lbl_update = [[UILabel alloc]init];
    lbl_update.frame = CGRectMake(130,0, 100,45);
    lbl_update.text = @"UPDATE";
    lbl_update.font = [UIFont fontWithName:kFont size:15];
    lbl_update.textColor = [UIColor whiteColor];
    lbl_update.backgroundColor = [UIColor clearColor];
    [btn_update addSubview:lbl_update];
    
    
    
    
    
#pragma MENU BODY
    
    img_strip3 = [[UIImageView alloc]init];
    img_strip3.frame = CGRectMake(CGRectGetMaxX(img_strip2.frame)+3,CGRectGetMaxY(collView_for_icons.frame)+2,80, 3);
    [img_strip3 setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    //img_strip3.backgroundColor = [UIColor redColor];
    [img_strip3 setUserInteractionEnabled:YES];
    // [img_bg addSubview:img_strip3];
    img_strip3.hidden = YES;
    
    //view for menu
    
    view_for_menu = [[UIView alloc]init];
    view_for_menu.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_menu.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_for_menu];
    view_for_menu.hidden = YES;
    
    UIImageView *img_bg_for_dietary = [[UIImageView alloc]init];
    img_bg_for_dietary.frame = CGRectMake(0,10, WIDTH+2,100);
    [img_bg_for_dietary  setImage:[UIImage imageNamed:@"img-whtcell-bg@2x.png"]];
    //img_bg_for_food_storage.backgroundColor = [UIColor redColor];
    [img_bg_for_dietary  setUserInteractionEnabled:YES];
    [view_for_menu addSubview:img_bg_for_dietary];
    
    
#pragma mark table for food storage
    
    table_for_menu = [[UITableView alloc] init ];
    table_for_menu.frame  = CGRectMake(8,4,305,120);
    [table_for_menu setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_menu.delegate = self;
    table_for_menu.dataSource = self;
    table_for_menu.showsVerticalScrollIndicator = NO;
    table_for_menu.backgroundColor = [UIColor clearColor];
    [img_bg_for_dietary addSubview:table_for_menu];
    
    
    UIImageView *img_bg_for_menu_items = [[UIImageView alloc]init];
    img_bg_for_menu_items.frame = CGRectMake(0,CGRectGetMaxY(img_bg_for_dietary.frame)+35, WIDTH+2,200);
    [img_bg_for_menu_items  setImage:[UIImage imageNamed:@"img-whtcell-bg@2x.png"]];
    img_bg_for_menu_items.backgroundColor = [UIColor clearColor];
    [img_bg_for_menu_items  setUserInteractionEnabled:YES];
    [view_for_menu addSubview:img_bg_for_menu_items];
    
    layout3 = [[UICollectionViewFlowLayout alloc] init];
    if (IS_IPHONE_6Plus)
    {
        collView_for_menu_items = [[UICollectionView alloc] initWithFrame:CGRectMake(10,5,370,250)
                                                     collectionViewLayout:layout3];
        
    }
    else if (IS_IPHONE_6)
    {
        collView_for_menu_items = [[UICollectionView alloc] initWithFrame:CGRectMake(10,5,345,230)
                                                     collectionViewLayout:layout3];
        
    }
    else if (IS_IPHONE_5)
    {
        collView_for_menu_items = [[UICollectionView alloc] initWithFrame:CGRectMake(10,5,300,230)
                                                     collectionViewLayout:layout3];
        
    }
    else
    {
        collView_for_menu_items = [[UICollectionView alloc] initWithFrame:CGRectMake(10,5,240,230)
                                                     collectionViewLayout:layout3];
        
    }
    
    [layout3 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_menu_items setDataSource:self];
    [collView_for_menu_items setDelegate:self];
    collView_for_menu_items.scrollEnabled = YES;
    collView_for_menu_items.showsVerticalScrollIndicator = YES;
    collView_for_menu_items.showsHorizontalScrollIndicator = NO;
    collView_for_menu_items.pagingEnabled = YES;
    [collView_for_menu_items registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_menu_items setBackgroundColor:[UIColor clearColor]];
    layout3.minimumInteritemSpacing = 5;
    layout3.minimumLineSpacing = 0;
    collView_for_menu_items.userInteractionEnabled = YES;
    [img_bg_for_menu_items  addSubview: collView_for_menu_items];
    
    
    
    
    
#pragma mark BODY FOR SCHEDULE
    
    view_for_schedule = [[UIView alloc]init];
    view_for_schedule.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_schedule.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_for_schedule];
    view_for_schedule.hidden = YES;
    
    UIImageView *img_calender = [[UIImageView alloc]init];
    img_calender.frame = CGRectMake(0,10, WIDTH, 300);
    [img_calender  setImage:[UIImage imageNamed:@"img-whtcell-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_calender  setUserInteractionEnabled:YES];
    [view_for_schedule addSubview:img_calender ];
    
    UIImageView *img_bg_for_schedule = [[UIImageView alloc]init];
    img_bg_for_schedule.frame = CGRectMake(0,CGRectGetMaxY(img_calender.frame), WIDTH, 155);
    [img_bg_for_schedule  setImage:[UIImage imageNamed:@"img-whtcell-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_schedule  setUserInteractionEnabled:YES];
    [view_for_schedule addSubview:img_bg_for_schedule ];
    
    
    self.enabledDates=[[NSMutableArray alloc]init];
    
    NSString *str_StartDate = @"22/02/1990";
    NSString *str_EndDate = @"22/02/2990";
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd/MM/yyyy"];
    
    NSDate *now = [[NSDate alloc] init];
    NSString *dateString = [format stringFromDate:now];
    
    // NSString *str_CurrentDate = [formatter stringFromDate:[NSDate date]];
    NSDate *dateCurrent = [format dateFromString:dateString];
    NSDate *startDate = [format dateFromString:str_StartDate];
    
    if ([startDate compare: dateCurrent]==NSOrderedAscending)// start date less then current
    {
        startDate = dateCurrent;
    }
    
    NSDate *endDate = [format dateFromString:str_EndDate];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    
    for (int i = 0; i <= components.day; ++i) {
        NSDateComponents *newComponents = [NSDateComponents new];
        newComponents.day = i;
        
        NSDate *date = [gregorianCalendar dateByAddingComponents:newComponents
                                                          toDate:startDate
                                                         options:0];
        NSString *str_AvailableDate = [format stringFromDate:date];
        [self.enabledDates addObject:str_AvailableDate];
    }
    
    [self.enabledDates addObject:[format stringFromDate:endDate]];
    
    NSString *date_Available = startDate;
    
    //   calendar = [[CKCalendarView alloc] initWithStartDay:startSunday frame:CGRectMake(2, 0, calView.frame.size.width-4, calView.frame.size.height-45)];
    //   calendar = [[CKCalendarView alloc] initWithStartDay:startSunday frame:CGRectMake(20, 0, self.view.frame.size.width-40,200)];
    
    calendar = [[CKCalendarView alloc] initWithStartDay:startSunday];
    calendar.frame = CGRectMake(10, 0, img_bg_for_schedule.frame.size.width-20, img_bg_for_schedule.frame.size.height-35);
    self.calendar = calendar;
    calendar.delegate = self;
    calendar.backgroundColor = [UIColor clearColor];
    calendar.titleColor = [UIColor blackColor];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    //     minimumDate = [dateFormatter dateFromString:dateString];
    minimumDate = [dateFormatter dateFromString:@"22/02/1990"];
    calendar.onlyShowCurrentMonth = NO;
    calendar.adaptHeightToNumberOfWeeksInMonth = NO;
    calendar.titleFont = [UIFont fontWithName:kFont size:17.0];
    [img_calender addSubview:calendar];
    [calendar selectDate:[NSDate date] makeVisible:NO];
    ary_AvailableDates = self.enabledDates;
    [calendar reloadData];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localeDidChange) name:NSCurrentLocaleDidChangeNotification object:nil];
    
    
    
    
#pragma table_for_schedule
    
    table_for_schedule = [[UITableView alloc] init ];
    table_for_schedule.frame  = CGRectMake(10,0,300,155);
    [table_for_schedule setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_schedule.delegate = self;
    table_for_schedule.dataSource = self;
    table_for_schedule.showsVerticalScrollIndicator = NO;
    table_for_schedule.backgroundColor = [UIColor clearColor];
    [img_bg_for_schedule addSubview:table_for_schedule];
    
    UIButton *btn_on_schedule_img = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_schedule_img.frame = CGRectMake(20,CGRectGetMaxY(img_bg_for_schedule.frame)+10, WIDTH-40, 45);
    btn_on_schedule_img .backgroundColor = [UIColor clearColor];
    [btn_on_schedule_img addTarget:self action:@selector(click_on_add_schedule_btn:) forControlEvents:UIControlEventTouchUpInside];
    [btn_on_schedule_img setImage:[UIImage imageNamed:@"img-add-sche@2x.png"] forState:UIControlStateNormal];
    [view_for_schedule   addSubview:btn_on_schedule_img];
    
    
    
    // view for schedule items
    view_for_schedule_items = [[UIView alloc]init];
    view_for_schedule_items.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_schedule_items.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_for_schedule_items];
    view_for_schedule_items.hidden = YES;
    
    UIImageView *img_bg_for_selected_schedule_date = [[UIImageView alloc]init];
    img_bg_for_selected_schedule_date.frame = CGRectMake(0,05, WIDTH, 55);
    [img_bg_for_selected_schedule_date  setImage:[UIImage imageNamed:@"img-whtcell-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_selected_schedule_date  setUserInteractionEnabled:YES];
    [view_for_schedule_items addSubview:img_bg_for_selected_schedule_date ];
    
#pragma table_for_schedule
    
    table_for_selected_schedule_deate = [[UITableView alloc] init ];
    table_for_selected_schedule_deate.frame  = CGRectMake(10,0,300,55);
    [table_for_selected_schedule_deate setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_selected_schedule_deate.delegate = self;
    table_for_selected_schedule_deate.dataSource = self;
    table_for_selected_schedule_deate.showsVerticalScrollIndicator = NO;
    table_for_selected_schedule_deate.backgroundColor = [UIColor clearColor];
    [img_bg_for_selected_schedule_date addSubview:table_for_selected_schedule_deate];
    
#pragma table_for_dish_items
    
    table_for_dish_items = [[UITableView alloc] init ];
    table_for_dish_items.frame  = CGRectMake(10,CGRectGetMaxY(img_bg_for_selected_schedule_date.frame)+10,300,300);
    [table_for_dish_items setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_dish_items.delegate = self;
    table_for_dish_items.dataSource = self;
    table_for_dish_items.showsVerticalScrollIndicator = NO;
    table_for_dish_items.backgroundColor = [UIColor clearColor];
    [view_for_schedule_items addSubview:table_for_dish_items];
    
    
    UIButton *btn_img_edit = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_edit.frame = CGRectMake(10,CGRectGetMaxY(table_for_dish_items.frame)+10,140,40);
    btn_img_edit .backgroundColor = [UIColor clearColor];
    [btn_img_edit addTarget:self action:@selector(click_on_edite_btn:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_edit setImage:[UIImage imageNamed:@"img-edit@2x.png"] forState:UIControlStateNormal];
    [view_for_schedule_items   addSubview:btn_img_edit];
    
    UIButton *btn_img_delete = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_delete.frame = CGRectMake(CGRectGetMaxX(btn_img_edit.frame)+20,CGRectGetMaxY(table_for_dish_items.frame)+10,140,40);
    btn_img_delete .backgroundColor = [UIColor clearColor];
    [btn_img_delete addTarget:self action:@selector(click_on_delete_btn:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_delete setImage:[UIImage imageNamed:@"img-del@2x.png"] forState:UIControlStateNormal];
    [view_for_schedule_items   addSubview:btn_img_delete];
    
    
    //    layout4=[[UICollectionViewFlowLayout alloc] init];
    //    collView_serviceDirectory = [[UICollectionView alloc] initWithFrame:CGRectMake(30,0,WIDTH-65,70)
    //                                                   collectionViewLayout:layout4];
    //
    //    [layout4 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    //    [collView_serviceDirectory setDataSource:self];
    //    [collView_serviceDirectory setDelegate:self];
    //    collView_serviceDirectory.scrollEnabled = YES;
    //    collView_serviceDirectory.showsVerticalScrollIndicator = NO;
    //    collView_serviceDirectory.showsHorizontalScrollIndicator = NO;
    //    collView_serviceDirectory.pagingEnabled = NO;
    //    [collView_serviceDirectory registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    //    [collView_serviceDirectory setBackgroundColor:[UIColor clearColor]];
    //    layout4.minimumInteritemSpacing = 2;
    //    layout4.minimumLineSpacing = 0;
    //    collView_serviceDirectory.userInteractionEnabled = YES;
    //    [img_cellBackGnd addSubview:collView_serviceDirectory];
    
    
#pragma BODY FOR ON-REQUEST
    
    // view for on_request
    
    view_for_on_request = [[UIView alloc]init];
    view_for_on_request.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_on_request.backgroundColor=[UIColor clearColor];
    //  [scroll  addSubview: view_for_on_request];
    view_for_on_request.hidden = YES;
    
    
    UILabel *lbl_currently_accept_orders = [[UILabel alloc]init ];
    lbl_currently_accept_orders.frame = CGRectMake(90,02, 300, 45);
    lbl_currently_accept_orders.text = @"Currently accepting orders on request";
    lbl_currently_accept_orders.font = [UIFont fontWithName:kFontBold size:15];
    lbl_currently_accept_orders.textColor = [UIColor blackColor];
    lbl_currently_accept_orders.backgroundColor = [UIColor clearColor];
    [view_for_on_request addSubview:lbl_currently_accept_orders];
    
    UILabel *lbl_orders_accept = [[UILabel alloc]init ];
    lbl_orders_accept.frame = CGRectMake(90,CGRectGetMaxY(lbl_currently_accept_orders.frame)+5, 300, 45);
    lbl_orders_accept.text = @"% of Orders Accepted";
    lbl_orders_accept.font = [UIFont fontWithName:kFontBold size:15];
    lbl_orders_accept.textColor = [UIColor blackColor];
    lbl_orders_accept.backgroundColor = [UIColor clearColor];
    [view_for_on_request addSubview:lbl_orders_accept];
    
    UIImageView *img_green_percentage = [[UIImageView alloc]init];
    img_green_percentage .frame = CGRectMake(20,CGRectGetMaxY(lbl_orders_accept.frame),260,15);
    [img_green_percentage  setImage:[UIImage imageNamed:@"green-percentage@2x.png"]];
    img_green_percentage .backgroundColor = [UIColor clearColor];
    [img_green_percentage  setUserInteractionEnabled:YES];
    [view_for_on_request addSubview:img_green_percentage];
    
    UILabel *lbl_percentage_val = [[UILabel alloc]init ];
    lbl_percentage_val.frame = CGRectMake(CGRectGetMaxX(img_green_percentage.frame)+10,CGRectGetMaxY(lbl_orders_accept.frame)-15, 300, 45);
    lbl_percentage_val.text = @"56/100%";
    lbl_percentage_val.font = [UIFont fontWithName:kFontBold size:12];
    lbl_percentage_val.textColor = [UIColor blackColor];
    lbl_percentage_val.backgroundColor = [UIColor clearColor];
    [view_for_on_request addSubview:lbl_percentage_val];
    
    UILabel *lbl_avg_respond_time = [[UILabel alloc]init ];
    lbl_avg_respond_time.frame = CGRectMake(20,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
    lbl_avg_respond_time.text = @"Average Response Time:";
    lbl_avg_respond_time.font = [UIFont fontWithName:kFont size:15];
    lbl_avg_respond_time.textColor = [UIColor blackColor];
    lbl_avg_respond_time.backgroundColor = [UIColor clearColor];
    [view_for_on_request addSubview:lbl_avg_respond_time];
    
    UILabel *avg_respond_time_val = [[UILabel alloc]init ];
    avg_respond_time_val.frame = CGRectMake(CGRectGetMidX(lbl_avg_respond_time.frame)+35,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
    avg_respond_time_val.text = @"45 mins";
    avg_respond_time_val.font = [UIFont fontWithName:kFontBold size:15];
    avg_respond_time_val.textColor = [UIColor blackColor];
    avg_respond_time_val.backgroundColor = [UIColor clearColor];
    [view_for_on_request addSubview:avg_respond_time_val];
    
    
#pragma View_reviews
    
    view_for_reviews = [[UIView alloc]init];
    view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_reviews.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_for_reviews];
    view_for_reviews.hidden = YES;
    
    
    UIImageView *img_bg_in_reviews = [[UIImageView alloc]init];
    img_bg_in_reviews.frame = CGRectMake(10,20, WIDTH-20, 250);
    [img_bg_in_reviews  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    [img_bg_in_reviews  setUserInteractionEnabled:YES];
    [ view_for_reviews addSubview:img_bg_in_reviews ];
    
    UILabel *label_favorite_by_user = [[UILabel alloc]init];
    label_favorite_by_user.frame = CGRectMake(20,10, 200,30);
    label_favorite_by_user.text = @"Chef Favorite by Users";
    label_favorite_by_user.font = [UIFont fontWithName:kFont size:15];
    label_favorite_by_user.textColor = [UIColor blackColor];
    label_favorite_by_user.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_favorite_by_user];
    
    label_favorite_value = [[UILabel alloc]init];
    label_favorite_value.frame = CGRectMake(CGRectGetMaxX(label_favorite_by_user.frame)+3,-2, 295,60);
    
    label_favorite_value.font = [UIFont fontWithName:kFontBold size:20];
    label_favorite_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:   1];
    label_favorite_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_favorite_value];
    
    UIImageView *img_line_in_reviews = [[UIImageView alloc]init];
    img_line_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_favorite_by_user.frame)+10, WIDTH-50, 0.5);
    [img_line_in_reviews  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line_in_reviews  setUserInteractionEnabled:YES];
    [img_bg_in_reviews addSubview:img_line_in_reviews];
    
    UILabel *label_total_reviews = [[UILabel alloc]init];
    label_total_reviews.frame = CGRectMake(20,CGRectGetMaxY(img_line_in_reviews.frame)+10, 200,30);
    label_total_reviews.text = @"Total Reviews";
    label_total_reviews.font = [UIFont fontWithName:kFont size:15];
    label_total_reviews.textColor = [UIColor blackColor];
    label_total_reviews.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_total_reviews];
    
    
    label_total_reviews_value = [[UILabel alloc]init];
    label_total_reviews_value.frame = CGRectMake(CGRectGetMaxX(label_total_reviews.frame)-5,CGRectGetMaxY(img_line_in_reviews.frame)+10, 150,30);
    
    label_total_reviews_value.font = [UIFont fontWithName:kFontBold size:20];
    label_total_reviews_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    label_total_reviews_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_total_reviews_value];
    
    
    UIButton *icon_red_right_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_red_right_arrow.frame = CGRectMake(CGRectGetMaxX(label_total_reviews_value.frame)-100,CGRectGetMaxY( img_line_in_reviews.frame)+15,20,20);
    icon_red_right_arrow.backgroundColor = [UIColor clearColor];
    [icon_red_right_arrow addTarget:self action:@selector(click_on_right_arrow:) forControlEvents:UIControlEventTouchUpInside];
    [icon_red_right_arrow setImage:[UIImage imageNamed:@"red-right-arrow@2x.png"] forState:UIControlStateNormal];
    [img_bg_in_reviews  addSubview:icon_red_right_arrow];
    
    UIButton *btn_on_right_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_right_arrow.frame = CGRectMake(4,CGRectGetMaxY(img_line_in_reviews.frame),290,50);
    btn_on_right_arrow.backgroundColor = [UIColor clearColor];
    [btn_on_right_arrow addTarget:self action:@selector(click_on_right_arrow:) forControlEvents:UIControlEventTouchUpInside];
    // [icon_red_right_arrow setImage:[UIImage imageNamed:@"red-right-arrow@2x.png"] forState:UIControlStateNormal];
    [img_bg_in_reviews  addSubview:btn_on_right_arrow];
    
    
    
    
    UIImageView *img_line2_in_reviews = [[UIImageView alloc]init];
    img_line2_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_total_reviews.frame)+10, WIDTH-50, 0.5);
    [img_line2_in_reviews  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line2_in_reviews  setUserInteractionEnabled:YES];
    [img_bg_in_reviews addSubview:img_line2_in_reviews];
    
    UILabel *label_overall_rating = [[UILabel alloc]init];
    label_overall_rating.frame = CGRectMake(20,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 200,30);
    label_overall_rating.text = @"Overall Ratings";
    label_overall_rating.font = [UIFont fontWithName:kFont size:15];
    label_overall_rating.textColor = [UIColor blackColor];
    label_overall_rating.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_overall_rating];
    
    
    UIImageView *img_thumb = [[UIImageView alloc]init];
    img_thumb .frame = CGRectMake(CGRectGetMaxX(label_overall_rating.frame)-15,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 25,25);
    [img_thumb   setImage:[UIImage imageNamed:@"thumb-icon@2x.png"]];
    [img_thumb   setUserInteractionEnabled:YES];
    [img_bg_in_reviews addSubview:img_thumb ];
    
    label_overall_rating_value = [[UILabel alloc]init];
    label_overall_rating_value.frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+5,CGRectGetMaxY(img_line2_in_reviews.frame)-5, 150,60);
    label_overall_rating_value.font = [UIFont fontWithName:kFontBold size:17];
    label_overall_rating_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    label_overall_rating_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_overall_rating_value];
    
    
    UIImageView *img_line3_in_reviews = [[UIImageView alloc]init];
    img_line3_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_overall_rating.frame)+10, WIDTH-50, 0.5);
    [img_line3_in_reviews  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line3_in_reviews  setUserInteractionEnabled:YES];
    [img_bg_in_reviews addSubview:img_line3_in_reviews];
    
    UILabel *label_food_ratings = [[UILabel alloc]init];
    label_food_ratings.frame = CGRectMake(20,CGRectGetMaxY(img_line3_in_reviews.frame)+10, 250,30);
    label_food_ratings.text = @"Food Ratings";
    label_food_ratings.font = [UIFont fontWithName:kFont size:15];
    label_food_ratings.textColor = [UIColor blackColor];
    label_food_ratings.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_food_ratings];
    
    UILabel *label_taste = [[UILabel alloc]init];
    label_taste.frame = CGRectMake(10,CGRectGetMaxY(label_food_ratings.frame)+10, 50,30);
    label_taste.text = @"Taste:";
    label_taste.font = [UIFont fontWithName:kFont size:13];
    label_taste.textColor = [UIColor blackColor];
    label_taste.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_taste];
    
    taste_value = [[UILabel alloc]init];
    taste_value.frame = CGRectMake(CGRectGetMaxX(label_taste.frame)-10,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
    taste_value.font = [UIFont fontWithName:kFont size:13];
    taste_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];    taste_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:taste_value];
    
    UILabel *label_appeal = [[UILabel alloc]init];
    label_appeal.frame = CGRectMake(CGRectGetMaxX(taste_value.frame)-15,CGRectGetMaxY(label_food_ratings.frame)+10,70,30);
    label_appeal.text = @"Appeal:";
    label_appeal.font = [UIFont fontWithName:kFont size:13];
    label_appeal.textColor = [UIColor blackColor];
    label_appeal.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_appeal];
    
    appeal_value = [[UILabel alloc]init];
    appeal_value.frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-15,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
    appeal_value.font = [UIFont fontWithName:@"Arial" size:13];
    appeal_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    appeal_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:appeal_value];
    
    UILabel *label_value = [[UILabel alloc]init];
    label_value.frame = CGRectMake(CGRectGetMaxX(appeal_value.frame)-20,CGRectGetMaxY(label_food_ratings.frame)+10, 50,30);
    label_value.text = @"Value:";
    label_value.font = [UIFont fontWithName:kFont size:13];
    label_value.textColor = [UIColor blackColor];
    label_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_value];
    
    value_value = [[UILabel alloc]init];
    value_value.frame = CGRectMake(CGRectGetMaxX(label_value.frame)-5,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
    value_value.font = [UIFont fontWithName:kFont size:13];
    value_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];    value_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:value_value];
    
    
#pragma BODY FOR DELIVERY
    
    view_for_delivery = [[UIView alloc]init];
    view_for_delivery.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,700);
    view_for_delivery.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_for_delivery];
    view_for_delivery.hidden = YES;
    
    table_for_delevery = [[UITableView alloc] init ];
    table_for_delevery.frame  = CGRectMake(10,10,WIDTH-20,400);
    [table_for_delevery setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_delevery.delegate = self;
    table_for_delevery.dataSource = self;
    table_for_delevery.showsVerticalScrollIndicator = NO;
    table_for_delevery.backgroundColor = [UIColor clearColor];
    [view_for_delivery addSubview:table_for_delevery];
    
    
    
    UIButton *icon_add = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_add.frame = CGRectMake(110,CGRectGetMidY(table_for_delevery.frame)+200,150,150);
    icon_add .backgroundColor = [UIColor clearColor];
    [icon_add addTarget:self action:@selector(click_on_pluse_btn:) forControlEvents:UIControlEventTouchUpInside];
    [icon_add setImage:[UIImage imageNamed:@"img-pluse@2x.png"] forState:UIControlStateNormal];
    [view_for_delivery   addSubview:icon_add];
    
    
    UIButton *btn_update_in_delivery = [UIButton buttonWithType:UIButtonTypeCustom];
    // btn_update_in_delivery.frame = CGRectMake(30,CGRectGetMaxY(img_bg_for_din_in_area.frame)+20,WIDTH-60,40);
    btn_update_in_delivery .backgroundColor = [UIColor clearColor];
    [btn_update_in_delivery addTarget:self action:@selector(click_on_update_indelivery_btn:) forControlEvents:UIControlEventTouchUpInside];
    [btn_update_in_delivery setImage:[UIImage imageNamed:@"img-black-btn@2x.png"] forState:UIControlStateNormal];
    //  [view_for_delivery   addSubview:btn_update_in_delivery];
    
    UILabel *lbl_update_in_delivery = [[UILabel alloc]init];
    lbl_update_in_delivery.frame = CGRectMake(130,0, 100,45);
    lbl_update_in_delivery.text = @"UPDATE";
    lbl_update_in_delivery.font = [UIFont fontWithName:kFont size:15];
    lbl_update_in_delivery.textColor = [UIColor whiteColor];
    lbl_update_in_delivery.backgroundColor = [UIColor clearColor];
    //[btn_update_in_delivery addSubview:lbl_update_in_delivery];
    
    
    
    
#pragma BODY FOR NOTE
    
    view_for_note = [[UIView alloc]init];
    view_for_note.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_note.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_for_note];
    view_for_note.hidden = YES;
    
    UIImageView *img_bg_in_note = [[UIImageView alloc]init];
    img_bg_in_note.frame = CGRectMake(10,20, WIDTH-20, 240);
    [img_bg_in_note  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    [img_bg_in_note  setUserInteractionEnabled:YES];
    [ view_for_note addSubview:img_bg_in_note];
    
    
    
    txtview_note =[[UITextView alloc]init];
    txtview_note .frame = CGRectMake(10,10,330,150);
    txtview_note.scrollEnabled=YES;
  //  txtview_note.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"notes"] objectAtIndex:0];
    //txt_view_for_user_description.numberOfLines = 5
    txtview_note.userInteractionEnabled=NO;
    txtview_note.font=[UIFont fontWithName:kFont size:14];
    txtview_note.backgroundColor=[UIColor whiteColor];
    txtview_note.delegate=self;
    txtview_note.textColor=[UIColor lightGrayColor];
    txtview_note.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    txtview_note.layer.borderWidth=1.0f;
    txtview_note.clipsToBounds=YES;
    [img_bg_in_note addSubview:txtview_note];
    //text_on_popup_bg.numberOfLines = 4;
    
    UILabel *lbl_max_wordes_in_note = [[UILabel alloc]init];
    lbl_max_wordes_in_note.frame = CGRectMake(260,CGRectGetMaxY( txtview_note.frame)-15, 100, 45);
    lbl_max_wordes_in_note.text = @"(500 char.max.)";
    lbl_max_wordes_in_note.font = [UIFont fontWithName:kFont size:11];
    lbl_max_wordes_in_note.textColor = [UIColor blackColor];
    lbl_max_wordes_in_note.backgroundColor = [UIColor clearColor];
    [img_bg_in_note addSubview:lbl_max_wordes_in_note];
    
    
    UIButton *btn_update_in_note = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_update_in_note.frame = CGRectMake(30,CGRectGetMaxY(img_bg_for_din_in_area.frame)+20,WIDTH-60,40);
    btn_update_in_note .backgroundColor = [UIColor clearColor];
    [btn_update_in_note addTarget:self action:@selector(click_on_update_innote_btn:) forControlEvents:UIControlEventTouchUpInside];
    [btn_update_in_note setImage:[UIImage imageNamed:@"img-black-btn@2x.png"] forState:UIControlStateNormal];
    [view_for_note   addSubview:btn_update_in_note];
    
    UILabel *lbl_update_in_note = [[UILabel alloc]init];
    lbl_update_in_note.frame = CGRectMake(130,0, 100,45);
    lbl_update_in_note.text = @"UPDATE";
    lbl_update_in_note.font = [UIFont fontWithName:kFont size:15];
    lbl_update_in_note.textColor = [UIColor whiteColor];
    lbl_update_in_note.backgroundColor = [UIColor clearColor];
    [btn_update_in_note addSubview:lbl_update_in_note];
    
    if (IS_IPHONE_6Plus)
    {
        scroll.frame = CGRectMake(0,0, WIDTH, HEIGHT);
        
        img_bg .frame = CGRectMake(0,25, WIDTH, 210);
        chef_img.frame = CGRectMake(160,30,90,90);
        lbl_chef_serve.frame = CGRectMake(130,90, 100,20);
        
        lbl_chef_name.frame = CGRectMake(184,CGRectGetMaxY(chef_img.frame), 200,45);
        lbl_chef_name.font = [UIFont fontWithName:kFontBold size:15];
        img_cm.frame = CGRectMake(WIDTH-45,40,25,22);
        img_strip.frame = CGRectMake(0,CGRectGetMaxY(collView_for_icons.frame)+2,78, 3);
        
        view_personal.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,1350);
        img_bg1.frame = CGRectMake(0,-10, WIDTH+3, 875);
        txt_view_for_chef_description .frame = CGRectMake(10,30, WIDTH-80, 100);
        img_linee.frame = CGRectMake(15,CGRectGetMaxY(txt_view_for_chef_description.frame), WIDTH-30, 0.5);
        icon_user.frame = CGRectMake(20,CGRectGetMaxY(img_linee.frame)+10,30,33);
        lbl_user_name.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMaxY(img_linee.frame),200,45);
        lbl_user_name.font = [UIFont fontWithName:kFontBold size:12];
        txt_first_name.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_user_name.frame)-5,300,45);
        
        
        
        img_line.frame = CGRectMake(15,CGRectGetMidY(txt_first_name.frame)+10, WIDTH-30, 0.5);
        icon_user2.frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)+10,30,33);
        
        lbl_full_name.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMaxY(img_line.frame),200,45);
        lbl_full_name.font = [UIFont fontWithName:kFontBold size:12];
        txt_full_name.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_full_name.frame)-5,300,45);
        img_line2.frame = CGRectMake(15,CGRectGetMidY(txt_full_name.frame)+10, WIDTH-30, 0.5);
        
        icon_for_chef_type.frame = CGRectMake(20,CGRectGetMaxY(img_line2.frame)+10,30,33);
        lbl_chef_type.frame = CGRectMake(CGRectGetMaxX(icon_for_chef_type.frame)+10,CGRectGetMidY(img_line2.frame),200,45);
        lbl_chef_type.font = [UIFont fontWithName:kFontBold size:12];
        txt_chef_type.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_chef_type.frame),300,45);
        
        img_line3.frame = CGRectMake(15,CGRectGetMidY(txt_chef_type.frame)+10, WIDTH-30, 0.5);
        icon_date_of_birth.frame = CGRectMake(20,CGRectGetMaxY(img_line3.frame)+10,30,33);
        lbl_date_of_birth.frame = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line3.frame),200,45);
        lbl_date_of_birth.font = [UIFont fontWithName:kFontBold size:12];
        txt_date_of_birth.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_date_of_birth.frame),300,45);
        img_line4.frame = CGRectMake(15,CGRectGetMidY(txt_date_of_birth.frame)+10, WIDTH-30, 0.5);
        
        
        icon_social_id.frame = CGRectMake(20,CGRectGetMaxY(img_line4.frame)+10,30,33);
        lbl_social_id.frame = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line4.frame),200,45);
        lbl_social_id.font = [UIFont fontWithName:kFontBold size:12];
        txt_socilid_number.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_social_id.frame),300,45);
        img_line5.frame =  CGRectMake(15,CGRectGetMidY(txt_socilid_number.frame)+10, WIDTH-30, 0.5);
        
        icon_message.frame = CGRectMake(20,CGRectGetMaxY(img_line5.frame)+10,30,33);
        lbl_email.frame =  CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line5.frame),145,35);
        lbl_email.font  = [UIFont fontWithName:kFontBold size:12];
        txt_email_address.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_email.frame),300,45);
        img_line6.frame  =  CGRectMake(15,CGRectGetMidY(txt_email_address.frame)+10, WIDTH-30, 0.5);
        
        
        icon_phone.frame = CGRectMake(20,CGRectGetMaxY(img_line6.frame)+10,30,33);
        lbl_mobile_no.frame = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line6.frame),200,45);
        lbl_mobile_no.font =  [UIFont fontWithName:kFontBold size:12];
        txt_mobile_number.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_mobile_no.frame),300,45);
        img_line7.frame =  CGRectMake(15,CGRectGetMidY(txt_mobile_number.frame)+10, WIDTH-30, 0.5);
        
        
        
        
        
        icon_home.frame = CGRectMake(20,CGRectGetMaxY(img_line7.frame)+10,30,33);
        lbl_home.frame = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line7.frame),200,45);
        lbl_home.font =  [UIFont fontWithName:kFontBold size:12];
        txt_home_address.frame =  CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_home.frame),300,45);
        img_line8.frame  =  CGRectMake(15,CGRectGetMidY(txt_home_address.frame)+10, WIDTH-30, 0.5);
        
        icon_servingtype.frame = CGRectMake(20,CGRectGetMaxY(img_line8.frame)+10,30,33);
        lbl_serving_type.frame = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line8.frame),200,45);
        lbl_serving_type.font =  [UIFont fontWithName:kFontBold size:12];
        txt_serving_type.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_serving_type.frame),300,45);
        img_line9.frame = CGRectMake(15,CGRectGetMidY(txt_serving_type.frame)+10, WIDTH-30, 0.5);
        
        icon_cooking_Qualification.frame = CGRectMake(20,CGRectGetMaxY(img_line9.frame)+10,30,33);
        lbl_coocking_qualification.frame = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line9.frame),200,45);
        lbl_coocking_qualification.font =  [UIFont fontWithName:kFontBold size:12];
        txt_cooking_qualification.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_coocking_qualification.frame),300,45);
        img_line10.frame = CGRectMake(15,CGRectGetMidY(txt_cooking_qualification.frame)+10, WIDTH-30, 0.5);
        
        icon_cooking_EX.frame = CGRectMake(20,CGRectGetMaxY(img_line10.frame)+10,30,33);
        lbl_coocking_EX.frame = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line10.frame),200,45);
        lbl_coocking_EX.font =  [UIFont fontWithName:kFontBold size:12];
        txt_cooking_ex.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_coocking_EX.frame),300,45);
        img_line11.frame = CGRectMake(15,CGRectGetMidY(txt_cooking_ex.frame)+10, WIDTH-30, 0.5);
        
        
        icon_paypal.frame = CGRectMake(20,CGRectGetMaxY(img_line11.frame)+10,30,33);
        lbl_paypal_ac.frame = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line11.frame),200,45);
        lbl_paypal_ac.font =  [UIFont fontWithName:kFontBold size:12];
        txt_paypal_account.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_paypal_ac.frame),300,45);
        img_line12.frame = CGRectMake(15,CGRectGetMidY(txt_paypal_account.frame)+10, WIDTH-30, 0.5);
        
        icon_paypal2.frame = CGRectMake(20,CGRectGetMaxY(img_line12.frame)+10,30,33);
        lbl_payment_received.frame  = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line12.frame),200,45);
        lbl_payment_received.font =  [UIFont fontWithName:kFontBold size:12];
        txt_payment_receiverd_month.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_payment_received.frame),300,45);
        //   img_line13.frame = CGRectMake(15,CGRectGetMidY(txt_payment_receiverd_month.frame)+10, WIDTH-30, 0.5);
        
        
        
        
        
        
        
        
        
        view_for_kitchen.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,900);
        img_bg2.frame = CGRectMake(0,10, WIDTH+2, 150);
        icon_kichen.frame = CGRectMake(20,20,40,40);
        lbl_kitchen_name.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+10,10,200,45);
        lbl_kitchen_name.font = [UIFont fontWithName:kFontBold size:12];
        txt_kitchen_name.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+5,CGRectGetMidY(lbl_kitchen_name.frame),300,45);
        img_line19.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+10,CGRectGetMidY(txt_kitchen_name.frame)+10, WIDTH-100, 0.5);
        icon_kichen2.frame = CGRectMake(20,CGRectGetMaxY(icon_kichen.frame)+20,40,40);
        lbl_kitchen_address.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+10,CGRectGetMaxY(img_line19.frame)+10,200,45);
        lbl_kitchen_address.font = [UIFont fontWithName:kFontBold size:12];
        txt_kitchen_address.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+5,CGRectGetMidY(lbl_kitchen_address.frame),300,45);
        img_bg_for_kitchen_imgs.frame = CGRectMake(0,CGRectGetMaxY(img_bg2.frame)+5, WIDTH+2, 425);
        img_video.frame = CGRectMake(20,10,370,150);
        
        
        
        img_bg_for_food_storage.frame = CGRectMake(0,CGRectGetMaxY(img_bg_for_kitchen_imgs.frame)+5, WIDTH+2, 350);
        icon_frize.frame = CGRectMake(20,20,30,40);
        lbl_food_storage.frame = CGRectMake(CGRectGetMaxX(icon_frize.frame)+20,5,200,45);
        lbl_food_storage.font = [UIFont fontWithName:kFontBold size:15];
        txt_view_for_storage_description .frame = CGRectMake(CGRectGetMaxX(icon_frize.frame)+20,CGRectGetMaxY(lbl_food_storage.frame), WIDTH-100, 100);
        lbl_max_wordes_in_storage.frame = CGRectMake(260,CGRectGetMaxY( txt_view_for_storage_description.frame)-15, 100, 45);
        lbl_max_wordes_in_storage.font = [UIFont fontWithName:kFont size:11];
        icon_hygienes.frame = CGRectMake(20,CGRectGetMaxY(lbl_max_wordes_in_storage.frame),38,40);
        lbl_food_hygiene.frame = CGRectMake(CGRectGetMaxX(icon_hygienes.frame)+20,CGRectGetMidY(lbl_max_wordes_in_storage.frame)+5,200,45);
        lbl_food_hygiene.font = [UIFont fontWithName:kFontBold size:15];
        txt_view_for_hygiene_description .frame = CGRectMake(CGRectGetMaxX(icon_frize.frame)+20,CGRectGetMaxY(lbl_food_hygiene.frame), WIDTH-100, 100);
        lbl_max_wordes_in_hygiene.frame = CGRectMake(260,CGRectGetMaxY( txt_view_for_hygiene_description.frame)-15, 100, 45);
        lbl_max_wordes_in_hygiene.font = [UIFont fontWithName:kFont size:11];
        img_strip2.frame = CGRectMake(CGRectGetMaxX(img_strip1.frame)+7,CGRectGetMaxY(collView_for_icons.frame)+2,77, 3);
        
        
        
        view_for_dine_in.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_for_din_in_area.frame = CGRectMake(0,10, WIDTH+2,270);
        lbl_dine_in_area.frame = CGRectMake(20,0, 200,45);
        lbl_dine_in_area.font = [UIFont fontWithName:kFontBold size:15];
        img_din_in_area.frame = CGRectMake(15,CGRectGetMaxY(lbl_dine_in_area.frame)-5, WIDTH-33,100);
        
        
        icon_no_of_seats.frame = CGRectMake(25,CGRectGetMaxY(img_din_in_area.frame)+25, 35,20);
        lbl_no_of_seats.frame = CGRectMake(CGRectGetMaxX(icon_no_of_seats.frame)+10,CGRectGetMaxY(img_din_in_area.frame)+5, 200,45);
        lbl_no_of_seats.font = [UIFont fontWithName:kFontBold size:12];
        txt_no_of_seats_val.frame = CGRectMake(CGRectGetMaxX(icon_no_of_seats.frame)+10,CGRectGetMaxY(lbl_no_of_seats.frame)-25, 200,45);
        txt_no_of_seats_val.font = [UIFont fontWithName:kFont size:12];
        img_line24.frame =  CGRectMake(20,CGRectGetMaxY(txt_no_of_seats_val.frame)-10, 360, 0.5);
        icon_parking.frame = CGRectMake(25,CGRectGetMaxY(img_line24.frame)+10, 35,35);
        lbl_parking.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(img_line24.frame)-2, 200,45);
        lbl_parking.font = [UIFont fontWithName:kFontBold size:12];
        
        
        
        img_radio_btn_on_yes.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-10,20,20);
        lbl_parking_yes.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-22, 40,45);
        btn_Yes.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-10,20,20);
        text_parking_val.frame = CGRectMake(CGRectGetMaxX(radio_button_for_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-22, 40,45);
        text_parking_val.font = [UIFont fontWithName:kFont size:15];
        img_radio_btn_on_no.frame = CGRectMake(CGRectGetMaxX(lbl_parking_yes.frame)+10,CGRectGetMaxY(lbl_parking.frame)-10,20,20);
        
        btn_update.frame = CGRectMake(30,CGRectGetMaxY(img_bg_for_din_in_area.frame)+20,WIDTH-60,40);
        lbl_update.frame = CGRectMake(130,0, 100,45);
        lbl_update.font = [UIFont fontWithName:kFont size:15];
        img_strip3.frame = CGRectMake(CGRectGetMaxX(img_strip2.frame)+3,CGRectGetMaxY(collView_for_icons.frame)+2,80, 3);
        
        view_for_menu.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_for_dietary.frame = CGRectMake(0,10, WIDTH+2,150);
        table_for_menu.frame  = CGRectMake(8,4,WIDTH-10,145);
        img_bg_for_menu_items.frame = CGRectMake(0,CGRectGetMaxY(img_bg_for_dietary.frame)+35, WIDTH+2,270);
        
        
        
        view_for_schedule.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_calender.frame = CGRectMake(0,10, WIDTH, 200);
        img_bg_for_schedule.frame = CGRectMake(0,CGRectGetMaxY(img_calender.frame), WIDTH, 155);
        table_for_schedule.frame  = CGRectMake(10,0,WIDTH,155);
        btn_on_schedule_img.frame = CGRectMake(20,CGRectGetMaxY(img_bg_for_schedule.frame)+10, WIDTH-40, 45);
        
        
        view_for_schedule_items.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_for_selected_schedule_date.frame = CGRectMake(0,05, WIDTH, 55);
        
        calendar.frame = CGRectMake(10, 0, img_bg_for_schedule.frame.size.width-20, img_bg_for_schedule.frame.size.height-35);
        table_for_selected_schedule_deate.frame  = CGRectMake(10,0,300,55);
        table_for_dish_items.frame  = CGRectMake(10,CGRectGetMaxY(img_bg_for_selected_schedule_date.frame)+10,WIDTH-20,400);
        btn_img_edit.frame = CGRectMake(55,CGRectGetMaxY(table_for_dish_items.frame)+10,140,40);
        btn_img_delete.frame = CGRectMake(CGRectGetMaxX(btn_img_edit.frame)+20,CGRectGetMaxY(table_for_dish_items.frame)+10,140,40);
        
        
        view_for_on_request.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        lbl_currently_accept_orders.frame = CGRectMake(90,02, 300, 45);
        lbl_currently_accept_orders.font = [UIFont fontWithName:kFontBold size:15];
        lbl_orders_accept.frame = CGRectMake(90,CGRectGetMaxY(lbl_currently_accept_orders.frame)+5, 300, 45);
        lbl_orders_accept.font = [UIFont fontWithName:kFontBold size:15];
        img_green_percentage .frame = CGRectMake(20,CGRectGetMaxY(lbl_orders_accept.frame),260,15);
        lbl_percentage_val.frame = CGRectMake(CGRectGetMaxX(img_green_percentage.frame)+10,CGRectGetMaxY(lbl_orders_accept.frame)-15, 300, 45);
        lbl_percentage_val.font = [UIFont fontWithName:kFontBold size:12];
        lbl_avg_respond_time.frame = CGRectMake(20,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
        lbl_avg_respond_time.font = [UIFont fontWithName:kFont size:15];
        avg_respond_time_val.frame = CGRectMake(CGRectGetMidX(lbl_avg_respond_time.frame)+35,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
        avg_respond_time_val.font = [UIFont fontWithName:kFontBold size:15];
        
        
        view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_in_reviews.frame = CGRectMake(10,20, WIDTH-20, 250);
        label_favorite_by_user.frame = CGRectMake(20,10, 200,30);
        label_favorite_by_user.font = [UIFont fontWithName:kFont size:15];
        label_favorite_value.frame = CGRectMake(CGRectGetMaxX(label_favorite_by_user.frame)+70,-2, 295,60);
        label_favorite_value.font = [UIFont fontWithName:kFontBold size:20];
        img_line_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_favorite_by_user.frame)+10, WIDTH-50, 0.5);
        label_total_reviews.frame = CGRectMake(20,CGRectGetMaxY(img_line_in_reviews.frame)+10, 200,30);
        label_total_reviews.font = [UIFont fontWithName:kFont size:15];
        label_total_reviews_value.frame = CGRectMake(CGRectGetMaxX(label_total_reviews.frame)+70,CGRectGetMaxY(img_line_in_reviews.frame)+10, 150,30);
        label_total_reviews_value.font = [UIFont fontWithName:kFontBold size:20];
        
        icon_red_right_arrow.frame = CGRectMake(CGRectGetMaxX(label_total_reviews_value.frame)-100,CGRectGetMaxY( img_line_in_reviews.frame)+15,20,20);
        btn_on_right_arrow.frame = CGRectMake(4,CGRectGetMaxY(img_line_in_reviews.frame),290,50);
        img_line2_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_total_reviews.frame)+10, WIDTH-50, 0.5);
        label_overall_rating.frame = CGRectMake(20,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 200,30);
        label_overall_rating.font = [UIFont fontWithName:kFont size:15];
        img_thumb .frame = CGRectMake(CGRectGetMaxX(label_overall_rating.frame)+60,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 25,25);
        label_overall_rating_value.frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+5,CGRectGetMaxY(img_line2_in_reviews.frame)-5, 150,60);
        label_overall_rating_value.font = [UIFont fontWithName:kFontBold size:17];
        img_line3_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_overall_rating.frame)+10, WIDTH-50, 0.5);
        label_food_ratings.frame = CGRectMake(20,CGRectGetMaxY(img_line3_in_reviews.frame)+10, 250,30);
        label_food_ratings.font = [UIFont fontWithName:kFont size:15];
        label_taste.frame = CGRectMake(50,CGRectGetMaxY(label_food_ratings.frame)+10, 50,30);
        label_taste.font = [UIFont fontWithName:kFont size:13];
        taste_value.frame = CGRectMake(CGRectGetMaxX(label_taste.frame)-10,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
        label_appeal.frame = CGRectMake(CGRectGetMaxX(taste_value.frame)-15,CGRectGetMaxY(label_food_ratings.frame)+10,70,30);
        label_appeal.font = [UIFont fontWithName:kFont size:13];
        appeal_value.frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-15,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
        appeal_value.font = [UIFont fontWithName:@"Arial" size:13];
        label_value.frame = CGRectMake(CGRectGetMaxX(appeal_value.frame)-20,CGRectGetMaxY(label_food_ratings.frame)+10, 50,30);
        label_value.font = [UIFont fontWithName:kFont size:13];
        value_value.frame = CGRectMake(CGRectGetMaxX(label_value.frame)-5,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
        value_value.font = [UIFont fontWithName:kFont size:13];
        
        
        view_for_delivery.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,700);
        table_for_delevery.frame  = CGRectMake(10,10,WIDTH-20,400);
        
        icon_add.frame = CGRectMake(120,CGRectGetMidY(table_for_delevery.frame)+200,150,150);
        btn_update_in_delivery.frame = CGRectMake(30,CGRectGetMidY(icon_add.frame),WIDTH-60,150);
        lbl_update_in_delivery.frame = CGRectMake(140,55,200,40);
        lbl_update_in_delivery.font = [UIFont fontWithName:kFont size:15];
        
        
        view_for_note.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_in_note.frame = CGRectMake(10,20, WIDTH-20, 240);
        txtview_note .frame = CGRectMake(30,10,330,150);
        lbl_max_wordes_in_note.frame = CGRectMake(260,CGRectGetMaxY( txtview_note.frame)-15, 100, 45);
        lbl_max_wordes_in_note.font = [UIFont fontWithName:kFont size:11];
        
        btn_update_in_note.frame = CGRectMake(30,CGRectGetMaxY(img_bg_in_note.frame),WIDTH-60,150);
        lbl_update_in_note.frame = CGRectMake(140,53,200,40);
        lbl_update_in_note.font = [UIFont fontWithName:kFont size:15];
        
    }
    else if(IS_IPHONE_6)
    {
        scroll.frame = CGRectMake(0,0, WIDTH, HEIGHT);
        
        img_bg .frame = CGRectMake(0,25, WIDTH, 210);
        chef_img.frame = CGRectMake(140,30,80,80);
        lbl_chef_serve.frame = CGRectMake(130,90, 100,20);
        
        lbl_chef_name.frame = CGRectMake(150,CGRectGetMaxY(chef_img.frame), 200,45);
        lbl_chef_name.font = [UIFont fontWithName:kFontBold size:15];
        img_cm.frame = CGRectMake(WIDTH-45,40,25,23);
        img_strip.frame = CGRectMake(0,CGRectGetMaxY(collView_for_icons.frame)+2,78, 3);
        
        view_personal.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,1350);
        img_bg1.frame = CGRectMake(0,-10, WIDTH+3, 900);
        txt_view_for_chef_description .frame = CGRectMake(10,30,WIDTH-20, 100);
        img_linee.frame = CGRectMake(15,CGRectGetMaxY(txt_view_for_chef_description.frame), WIDTH-30, 0.5);
        icon_user.frame = CGRectMake(20,CGRectGetMaxY(img_linee.frame)+10,30,33);
        lbl_user_name.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMaxY(img_linee.frame)-5,200,45);
        
        
        lbl_user_name.font = [UIFont fontWithName:kFontBold size:12];
        txt_first_name.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_user_name.frame),300,45);
        img_line.frame = CGRectMake(15,CGRectGetMidY(txt_first_name.frame)+10, WIDTH-30, 0.5);
        icon_user2.frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)+10,30,33);
        
        lbl_full_name.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMaxY(img_line.frame)-5,200,45);
        lbl_full_name.font = [UIFont fontWithName:kFontBold size:12];
        txt_full_name.frame =CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_full_name.frame),300,45);
        img_line2.frame = CGRectMake(15,CGRectGetMidY(txt_full_name.frame)+10, WIDTH-30, 0.5);
        
        icon_for_chef_type.frame = CGRectMake(20,CGRectGetMaxY(img_line2.frame)+10,30,33);
        lbl_chef_type.frame = CGRectMake(CGRectGetMaxX(icon_for_chef_type.frame)+10,CGRectGetMidY(img_line2.frame),200,45);
        lbl_chef_type.font = [UIFont fontWithName:kFontBold size:12];
        txt_chef_type.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_chef_type.frame),300,45);
        img_line3.frame = CGRectMake(15,CGRectGetMidY(txt_chef_type.frame)+10, WIDTH-30, 0.5);
        
        icon_date_of_birth.frame = CGRectMake(20,CGRectGetMaxY(img_line3.frame)+10,30,33);
        lbl_date_of_birth.frame = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line3.frame),200,45);
        lbl_date_of_birth.font = [UIFont fontWithName:kFontBold size:12];
        txt_date_of_birth.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_date_of_birth.frame),300,45);
        img_line4.frame = CGRectMake(15,CGRectGetMidY(txt_date_of_birth.frame)+10, WIDTH-30, 0.5);
        
        
        icon_social_id.frame = CGRectMake(20,CGRectGetMaxY(img_line4.frame)+10,30,33);
        lbl_social_id.frame = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line4.frame),200,45);
        lbl_social_id.font = [UIFont fontWithName:kFontBold size:12];
        txt_socilid_number.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_social_id.frame),300,45);
        img_line5.frame = CGRectMake(15,CGRectGetMidY(txt_socilid_number.frame)+10, WIDTH-30, 0.5);
        
        
        icon_message.frame  = CGRectMake(20,CGRectGetMaxY(img_line5.frame)+10,20,25);
        lbl_email.frame  = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line5.frame),150,35);
        lbl_email.backgroundColor = [UIColor clearColor];
        lbl_email.font =[UIFont fontWithName:kFontBold size:12];
        txt_email_address.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_email.frame),300,40);
        img_line6.frame =  CGRectMake(15,CGRectGetMidY(txt_email_address.frame)+10, WIDTH-30, 0.5);
        
        
        icon_phone.frame =  CGRectMake(20,CGRectGetMaxY(img_line6.frame)+10,30,33);
        lbl_mobile_no.frame = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line6.frame),200,45);
        lbl_mobile_no.font =  [UIFont fontWithName:kFontBold size:12];
        txt_mobile_number.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_mobile_no.frame),300,45);
        img_line7.frame = CGRectMake(15,CGRectGetMidY(txt_mobile_number.frame)+10, WIDTH-30, 0.5);
        
        
        
        
        icon_home.frame = CGRectMake(20,CGRectGetMaxY(img_line7.frame)+10,30,33);
        lbl_home.frame = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line7.frame),200,45);
        lbl_home.font =  [UIFont fontWithName:kFontBold size:12];
        txt_home_address.frame =  CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_home.frame),WIDTH-50,45);
        txt_home_address.backgroundColor = [UIColor clearColor];
        img_line8.frame  =  CGRectMake(15,CGRectGetMidY(txt_home_address.frame)+10, WIDTH-30, 0.5);
        
        icon_servingtype.frame = CGRectMake(20,CGRectGetMaxY(img_line8.frame)+10,30,25);
        lbl_serving_type.frame = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line8.frame),200,45);
        lbl_serving_type.font =  [UIFont fontWithName:kFontBold size:12];
        txt_serving_type.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_serving_type.frame),300,45);
        img_line9.frame = CGRectMake(15,CGRectGetMidY(txt_serving_type.frame)+10, WIDTH-30, 0.5);
        
        icon_cooking_Qualification.frame = CGRectMake(20,CGRectGetMaxY(img_line9.frame)+10,30,33);
        lbl_coocking_qualification.frame = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line9.frame),200,45);
        lbl_coocking_qualification.font =  [UIFont fontWithName:kFontBold size:12];
        txt_cooking_qualification.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_coocking_qualification.frame),300,45);
        img_line10.frame = CGRectMake(15,CGRectGetMidY(txt_cooking_qualification.frame)+10, WIDTH-30, 0.5);
        
        icon_cooking_EX.frame = CGRectMake(20,CGRectGetMaxY(img_line10.frame)+10,30,33);
        lbl_coocking_EX.frame = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line10.frame),200,45);
        lbl_coocking_EX.font =  [UIFont fontWithName:kFontBold size:12];
        txt_cooking_ex.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_coocking_EX.frame),300,45);
        img_line11.frame = CGRectMake(15,CGRectGetMidY(txt_cooking_ex.frame)+10, WIDTH-30, 0.5);
        
        
        icon_paypal.frame = CGRectMake(20,CGRectGetMaxY(img_line11.frame)+10,30,33);
        lbl_paypal_ac.frame = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line11.frame),200,45);
        lbl_paypal_ac.font =  [UIFont fontWithName:kFontBold size:12];
        txt_paypal_account.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_paypal_ac.frame),300,45);
        img_line12.frame = CGRectMake(15,CGRectGetMidY(txt_paypal_account.frame)+10, WIDTH-30, 0.5);
        
        icon_paypal2.frame = CGRectMake(20,CGRectGetMaxY(img_line12.frame)+10,30,33);
        lbl_payment_received.frame  = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line12.frame),200,45);
        lbl_payment_received.font =  [UIFont fontWithName:kFontBold size:12];
        txt_payment_receiverd_month.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_payment_received.frame),300,45);
        // img_line13.frame = CGRectMake(15,CGRectGetMidY(txt_payment_receiverd_month.frame)+10, WIDTH-30, 0.5);
        
        
        
        view_for_kitchen.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,900);
        img_bg2.frame = CGRectMake(0,10, WIDTH+2, 150);
        icon_kichen.frame = CGRectMake(20,20,40,40);
        lbl_kitchen_name.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+10,10,200,45);
        lbl_kitchen_name.font = [UIFont fontWithName:kFontBold size:12];
        txt_kitchen_name.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+5,CGRectGetMidY(lbl_kitchen_name.frame),300,45);
        img_line19.frame = CGRectMake(15,CGRectGetMidY(txt_kitchen_name.frame)+15, WIDTH-30, 0.5);
        icon_kichen2.frame = CGRectMake(20,CGRectGetMaxY(icon_kichen.frame)+20,40,40);
        lbl_kitchen_address.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+10,CGRectGetMaxY(img_line19.frame)+10,200,45);
        lbl_kitchen_address.font = [UIFont fontWithName:kFontBold size:12];
        txt_kitchen_address.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+5,CGRectGetMidY(lbl_kitchen_address.frame),300,45);
        
        img_bg_for_kitchen_imgs.frame = CGRectMake(0,CGRectGetMaxY(img_bg2.frame)+5, WIDTH+2, 350);
        img_video.frame = CGRectMake(20,10,335,100);
        img_bg_for_food_storage.frame = CGRectMake(0,CGRectGetMaxY(img_bg_for_kitchen_imgs.frame)+5, WIDTH+2, 350);
        icon_frize.frame = CGRectMake(20,20,30,40);
        lbl_food_storage.frame = CGRectMake(CGRectGetMaxX(icon_frize.frame)+20,5,200,45);
        lbl_food_storage.font = [UIFont fontWithName:kFontBold size:15];
        txt_view_for_storage_description .frame = CGRectMake(CGRectGetMaxX(icon_frize.frame)+20,CGRectGetMaxY(lbl_food_storage.frame), WIDTH-100, 100);
        lbl_max_wordes_in_storage.frame = CGRectMake(260,CGRectGetMaxY( txt_view_for_storage_description.frame)-15, 100, 45);
        lbl_max_wordes_in_storage.font = [UIFont fontWithName:kFont size:11];
        icon_hygienes.frame = CGRectMake(20,CGRectGetMaxY(lbl_max_wordes_in_storage.frame),38,40);
        lbl_food_hygiene.frame = CGRectMake(CGRectGetMaxX(icon_hygienes.frame)+20,CGRectGetMidY(lbl_max_wordes_in_storage.frame)+5,200,45);
        lbl_food_hygiene.font = [UIFont fontWithName:kFontBold size:15];
        txt_view_for_hygiene_description .frame = CGRectMake(CGRectGetMaxX(icon_frize.frame)+20,CGRectGetMaxY(lbl_food_hygiene.frame), WIDTH-100, 100);
        lbl_max_wordes_in_hygiene.frame = CGRectMake(260,CGRectGetMaxY( txt_view_for_hygiene_description.frame)-15, 100, 45);
        lbl_max_wordes_in_hygiene.font = [UIFont fontWithName:kFont size:11];
        img_strip2.frame = CGRectMake(CGRectGetMaxX(img_strip1.frame)+7,CGRectGetMaxY(collView_for_icons.frame)+2,77, 3);
        
        
        
        view_for_dine_in.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_for_din_in_area.frame = CGRectMake(0,10, WIDTH+2,270);
        lbl_dine_in_area.frame = CGRectMake(20,0, 200,45);
        lbl_dine_in_area.font = [UIFont fontWithName:kFontBold size:15];
        img_din_in_area.frame = CGRectMake(15,CGRectGetMaxY(lbl_dine_in_area.frame)-5, WIDTH-33,100);
        
        
        icon_no_of_seats.frame = CGRectMake(25,CGRectGetMaxY(img_din_in_area.frame)+25, 35,20);
        lbl_no_of_seats.frame = CGRectMake(CGRectGetMaxX(icon_no_of_seats.frame)+10,CGRectGetMaxY(img_din_in_area.frame)+5, 200,45);
        lbl_no_of_seats.font = [UIFont fontWithName:kFontBold size:12];
        txt_no_of_seats_val.frame = CGRectMake(CGRectGetMaxX(icon_no_of_seats.frame)+10,CGRectGetMaxY(lbl_no_of_seats.frame)-25, 200,45);
        txt_no_of_seats_val.font = [UIFont fontWithName:kFont size:12];
        img_line24.frame =  CGRectMake(20,CGRectGetMaxY(txt_no_of_seats_val.frame)-10, 280, 0.5);
        icon_parking.frame = CGRectMake(25,CGRectGetMaxY(img_line24.frame)+10, 35,35);
        lbl_parking.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(img_line24.frame)-2, 200,45);
        lbl_parking.font = [UIFont fontWithName:kFontBold size:12];
        
        
        
        
        img_radio_btn_on_yes.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-10,20,20);
        btn_Yes.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-10,20,20);
        lbl_parking_yes.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-22, 40,45);
        text_parking_val.frame = CGRectMake(CGRectGetMaxX(radio_button_for_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-22, 40,45);
        text_parking_val.font = [UIFont fontWithName:kFont size:15];
        img_radio_btn_on_no.frame = CGRectMake(CGRectGetMaxX(lbl_parking_yes.frame)+10,CGRectGetMaxY(lbl_parking.frame)-10,20,20);
        btn_update.frame = CGRectMake(30,CGRectGetMaxY(img_bg_for_din_in_area.frame)+20,WIDTH-60,40);
        lbl_update.frame = CGRectMake(130,0, 100,45);
        lbl_update.font = [UIFont fontWithName:kFont size:15];
        img_strip3.frame = CGRectMake(CGRectGetMaxX(img_strip2.frame)+3,CGRectGetMaxY(collView_for_icons.frame)+2,80, 3);
        view_for_menu.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_for_dietary.frame = CGRectMake(0,10, WIDTH+2,128);
        table_for_menu.frame  = CGRectMake(8,4,WIDTH-18,116);
        img_bg_for_menu_items.frame = CGRectMake(0,CGRectGetMaxY(img_bg_for_dietary.frame)+35, WIDTH+2,250);
        
        
        
        view_for_schedule.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_calender.frame = CGRectMake(0,10, WIDTH, 400);
        img_bg_for_schedule.frame = CGRectMake(0,CGRectGetMaxY(img_calender.frame), WIDTH, 250);
        calendar.frame = CGRectMake(10, 0, img_bg_for_schedule.frame.size.width-20, img_bg_for_schedule.frame.size.height-35);
        
        table_for_schedule.frame  = CGRectMake(10,0,WIDTH-20,155);
        btn_on_schedule_img.frame = CGRectMake(20,CGRectGetMaxY(img_bg_for_schedule.frame)+10, WIDTH-40, 45);
        
        
        view_for_schedule_items.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_for_selected_schedule_date.frame = CGRectMake(0,05, WIDTH, 55);
        table_for_selected_schedule_deate.frame  = CGRectMake(10,0,300,55);
        table_for_dish_items.frame  = CGRectMake(10,CGRectGetMaxY(img_bg_for_selected_schedule_date.frame)+10,WIDTH-20,350);
        btn_img_edit.frame = CGRectMake(40,CGRectGetMaxY(table_for_dish_items.frame)+10,140,40);
        btn_img_delete.frame = CGRectMake(CGRectGetMaxX(btn_img_edit.frame)+20,CGRectGetMaxY(table_for_dish_items.frame)+10,140,40);
        
        
        view_for_on_request.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        lbl_currently_accept_orders.frame = CGRectMake(90,02, 300, 45);
        lbl_currently_accept_orders.font = [UIFont fontWithName:kFontBold size:15];
        lbl_orders_accept.frame = CGRectMake(90,CGRectGetMaxY(lbl_currently_accept_orders.frame)+5, 300, 45);
        lbl_orders_accept.font = [UIFont fontWithName:kFontBold size:15];
        img_green_percentage .frame = CGRectMake(20,CGRectGetMaxY(lbl_orders_accept.frame),260,15);
        lbl_percentage_val.frame = CGRectMake(CGRectGetMaxX(img_green_percentage.frame)+10,CGRectGetMaxY(lbl_orders_accept.frame)-15, 300, 45);
        lbl_percentage_val.font = [UIFont fontWithName:kFontBold size:12];
        lbl_avg_respond_time.frame = CGRectMake(20,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
        lbl_avg_respond_time.font = [UIFont fontWithName:kFont size:15];
        avg_respond_time_val.frame = CGRectMake(CGRectGetMidX(lbl_avg_respond_time.frame)+35,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
        avg_respond_time_val.font = [UIFont fontWithName:kFontBold size:15];
        
        
        view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_in_reviews.frame = CGRectMake(10,20, WIDTH-20, 250);
        label_favorite_by_user.frame = CGRectMake(20,10, 200,30);
        label_favorite_by_user.font = [UIFont fontWithName:kFont size:15];
        label_favorite_value.frame = CGRectMake(CGRectGetMaxX(label_favorite_by_user.frame)+50,-2, 295,60);
        label_favorite_value.font = [UIFont fontWithName:kFontBold size:20];
        img_line_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_favorite_by_user.frame)+10, WIDTH-50, 0.5);
        label_total_reviews.frame = CGRectMake(20,CGRectGetMaxY(img_line_in_reviews.frame)+10, 200,30);
        label_total_reviews.font = [UIFont fontWithName:kFont size:15];
        label_total_reviews_value.frame = CGRectMake(CGRectGetMaxX(label_total_reviews.frame)+35,CGRectGetMaxY(img_line_in_reviews.frame)+10, 150,30);
        label_total_reviews_value.font = [UIFont fontWithName:kFontBold size:20];
        
        icon_red_right_arrow.frame = CGRectMake(CGRectGetMaxX(label_total_reviews_value.frame)-100,CGRectGetMaxY( img_line_in_reviews.frame)+15,20,20);
        btn_on_right_arrow.frame = CGRectMake(4,CGRectGetMaxY(img_line_in_reviews.frame),290,50);
        img_line2_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_total_reviews.frame)+10, WIDTH-50, 0.5);
        label_overall_rating.frame = CGRectMake(20,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 200,30);
        label_overall_rating.font = [UIFont fontWithName:kFont size:15];
        img_thumb .frame = CGRectMake(CGRectGetMaxX(label_overall_rating.frame)+20,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 25,25);
        label_overall_rating_value.frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+5,CGRectGetMaxY(img_line2_in_reviews.frame)-5, 150,60);
        label_overall_rating_value.font = [UIFont fontWithName:kFontBold size:17];
        img_line3_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_overall_rating.frame)+10, WIDTH-50, 0.5);
        label_food_ratings.frame = CGRectMake(20,CGRectGetMaxY(img_line3_in_reviews.frame)+10, 250,30);
        label_food_ratings.font = [UIFont fontWithName:kFont size:15];
        label_taste.frame = CGRectMake(20,CGRectGetMaxY(label_food_ratings.frame)+10, 50,30);
        label_taste.font = [UIFont fontWithName:kFont size:13];
        taste_value.frame = CGRectMake(CGRectGetMaxX(label_taste.frame)-10,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
        label_appeal.frame = CGRectMake(CGRectGetMaxX(taste_value.frame),CGRectGetMaxY(label_food_ratings.frame)+10,70,30);
        label_appeal.font = [UIFont fontWithName:kFont size:13];
        appeal_value.frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-15,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
        appeal_value.font = [UIFont fontWithName:@"Arial" size:13];
        label_value.frame = CGRectMake(CGRectGetMaxX(appeal_value.frame),CGRectGetMaxY(label_food_ratings.frame)+10, 50,30);
        label_value.font = [UIFont fontWithName:kFont size:13];
        value_value.frame = CGRectMake(CGRectGetMaxX(label_value.frame)-5,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
        value_value.font = [UIFont fontWithName:kFont size:13];
        
        
        view_for_delivery.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,700);
        table_for_delevery.frame  = CGRectMake(10,10,WIDTH-20,400);
        
        icon_add.frame = CGRectMake(110,CGRectGetMidY(table_for_delevery.frame)+200,150,150);
        btn_update_in_delivery.frame = CGRectMake(30,CGRectGetMidY(icon_add.frame),WIDTH-60,150);
        lbl_update_in_delivery.frame = CGRectMake(120,55,200,40);
        lbl_update_in_delivery.font = [UIFont fontWithName:kFont size:15];
        
        
        view_for_note.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_in_note.frame = CGRectMake(10,20, WIDTH-20, 240);
        txtview_note .frame = CGRectMake(10,10,330,150);
        lbl_max_wordes_in_note.frame = CGRectMake(260,CGRectGetMaxY( txtview_note.frame)-15, 100, 45);
        lbl_max_wordes_in_note.font = [UIFont fontWithName:kFont size:11];
        
        btn_update_in_note.frame = CGRectMake(30,CGRectGetMaxY(img_bg_in_note.frame),WIDTH-60,150);
        lbl_update_in_note.frame = CGRectMake(120,53,200,40);
        lbl_update_in_note.font = [UIFont fontWithName:kFont size:15];
        
    }
    else
    {
        scroll.frame = CGRectMake(0,0, WIDTH, HEIGHT);
        
        img_bg .frame = CGRectMake(0,25, WIDTH, 210);
        chef_img.frame = CGRectMake(120,30,80,80);
        lbl_chef_serve.frame = CGRectMake(120,60, 90,20);
        
        lbl_chef_name.frame = CGRectMake(135,CGRectGetMaxY(chef_img.frame), 200,45);
        lbl_chef_name.font = [UIFont fontWithName:kFontBold size:15];
        img_cm.frame = CGRectMake(WIDTH-45,40,25,23);
        img_strip.frame = CGRectMake(0,CGRectGetMaxY(collView_for_icons.frame)+2,78, 3);
        
        view_personal.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,1350);
        img_bg1.frame = CGRectMake(0,-10, WIDTH+3, 900);
        txt_view_for_chef_description .frame = CGRectMake(10,25,WIDTH-40, 120);
        img_linee.frame = CGRectMake(15,CGRectGetMaxY(txt_view_for_chef_description.frame), WIDTH-30, 0.5);
        icon_user.frame = CGRectMake(20,CGRectGetMaxY(img_linee.frame)+10,30,33);
        lbl_user_name.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMaxY(img_linee.frame)-5,200,45);
        
        
        lbl_user_name.font = [UIFont fontWithName:kFontBold size:12];
        txt_first_name.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_user_name.frame),300,45);
        img_line.frame = CGRectMake(15,CGRectGetMidY(txt_first_name.frame)+10, WIDTH-30, 0.5);
        icon_user2.frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)+10,30,33);
        
        lbl_full_name.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10,CGRectGetMaxY(img_line.frame)-5,200,45);
        lbl_full_name.font = [UIFont fontWithName:kFontBold size:12];
        txt_full_name.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_full_name.frame),300,45);
        img_line2.frame = CGRectMake(15,CGRectGetMidY(txt_full_name.frame)+10, WIDTH-30, 0.5);
        icon_for_chef_type.frame = CGRectMake(20,CGRectGetMaxY(img_line2.frame)+10,30,33);
        lbl_chef_type.frame = CGRectMake(CGRectGetMaxX(icon_for_chef_type.frame)+10,CGRectGetMidY(img_line2.frame),200,45);
        lbl_chef_type.font = [UIFont fontWithName:kFontBold size:12];
        
        txt_chef_type.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_chef_type.frame),300,45);
        img_line3.frame = CGRectMake(15,CGRectGetMidY(txt_chef_type.frame)+10, WIDTH-30, 0.5);
        
        icon_date_of_birth.frame = CGRectMake(20,CGRectGetMaxY(img_line3.frame)+10,30,33);
        lbl_date_of_birth.frame = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line3.frame),200,45);
        lbl_date_of_birth.font = [UIFont fontWithName:kFontBold size:12];
        txt_date_of_birth.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_date_of_birth.frame),300,45);
        img_line4.frame = CGRectMake(15,CGRectGetMidY(txt_date_of_birth.frame)+10, WIDTH-30, 0.5);
        
        icon_social_id.frame = CGRectMake(20,CGRectGetMaxY(img_line4.frame)+10,30,33);
        lbl_social_id.frame = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line4.frame),200,45);
        lbl_social_id.font = [UIFont fontWithName:kFontBold size:12];
        txt_socilid_number.frame =  CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_social_id.frame),300,45);
        img_line5.frame = CGRectMake(15,CGRectGetMidY(txt_socilid_number.frame)+10, WIDTH-30, 0.5);
        
        
        icon_message.frame = CGRectMake(20,CGRectGetMaxY(img_line5.frame)+10,30,33);
        lbl_email.frame = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line5.frame),200,35);
        lbl_email.font =  [UIFont fontWithName:kFontBold size:12];
        txt_email_address.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_email.frame),300,45);
        img_line6.frame = CGRectMake(15,CGRectGetMidY(txt_email_address.frame)+10, WIDTH-30, 0.5);
        
        
        icon_phone.frame = CGRectMake(20,CGRectGetMaxY(img_line6.frame)+10,30,33);
        lbl_mobile_no.frame = CGRectMake(CGRectGetMaxX(icon_date_of_birth.frame)+10,CGRectGetMidY(img_line6.frame),200,45);
        lbl_mobile_no.font = [UIFont fontWithName:kFontBold size:12];
        txt_mobile_number.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_mobile_no.frame),300,45);
        img_line7.frame = CGRectMake(15,CGRectGetMidY(txt_mobile_number.frame)+10, WIDTH-30, 0.5);
        
        
        
        icon_home.frame = CGRectMake(20,CGRectGetMaxY(img_line7.frame)+10,30,33);
        lbl_home.frame = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line7.frame),200,45);
        lbl_home.font =  [UIFont fontWithName:kFontBold size:12];
        txt_home_address.frame =  CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_home.frame),300,45);
        img_line8.frame  =  CGRectMake(15,CGRectGetMidY(txt_home_address.frame)+10, WIDTH-30, 0.5);
        
        icon_servingtype.frame = CGRectMake(20,CGRectGetMaxY(img_line8.frame)+10,30,33);
        lbl_serving_type.frame = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line8.frame),200,45);
        lbl_serving_type.font =  [UIFont fontWithName:kFontBold size:12];
        txt_serving_type.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_serving_type.frame),300,45);
        img_line9.frame = CGRectMake(15,CGRectGetMidY(txt_serving_type.frame)+10, WIDTH-30, 0.5);
        
        icon_cooking_Qualification.frame = CGRectMake(20,CGRectGetMaxY(img_line9.frame)+10,30,33);
        lbl_coocking_qualification.frame = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line9.frame),200,45);
        lbl_coocking_qualification.font =  [UIFont fontWithName:kFontBold size:12];
        txt_cooking_qualification.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_coocking_qualification.frame),300,45);
        img_line10.frame = CGRectMake(15,CGRectGetMidY(txt_cooking_qualification.frame)+10, WIDTH-30, 0.5);
        
        icon_cooking_EX.frame = CGRectMake(20,CGRectGetMaxY(img_line10.frame)+10,30,33);
        lbl_coocking_EX.frame = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line10.frame),200,45);
        lbl_coocking_EX.font =  [UIFont fontWithName:kFontBold size:12];
        txt_cooking_ex.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_coocking_EX.frame),300,45);
        img_line11.frame = CGRectMake(15,CGRectGetMidY(txt_cooking_ex.frame)+10, WIDTH-30, 0.5);
        
        
        icon_paypal.frame = CGRectMake(20,CGRectGetMaxY(img_line11.frame)+10,30,33);
        lbl_paypal_ac.frame = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line11.frame),200,45);
        lbl_paypal_ac.font =  [UIFont fontWithName:kFontBold size:12];
        txt_paypal_account.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_paypal_ac.frame),300,45);
        img_line12.frame = CGRectMake(15,CGRectGetMidY(txt_paypal_account.frame)+10, WIDTH-30, 0.5);
        
        icon_paypal2.frame = CGRectMake(20,CGRectGetMaxY(img_line12.frame)+10,30,33);
        lbl_payment_received.frame  = CGRectMake(CGRectGetMaxX(icon_social_id.frame)+10,CGRectGetMidY(img_line12.frame),200,45);
        lbl_payment_received.font =  [UIFont fontWithName:kFontBold size:12];
        txt_payment_receiverd_month.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+5,CGRectGetMidY(lbl_payment_received.frame),300,45);
        //        img_line13.frame = CGRectMake(15,CGRectGetMidY(txt_payment_receiverd_month.frame)+10, WIDTH-30, 0.5);
        
        
        
        
        
        
        view_for_kitchen.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,900);
        img_bg2.frame = CGRectMake(0,10, WIDTH+2, 250);
        icon_kichen.frame = CGRectMake(20,20,40,40);
        lbl_kitchen_name.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+10,10,200,45);
        lbl_kitchen_name.font = [UIFont fontWithName:kFontBold size:12];
        txt_kitchen_name.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+5,CGRectGetMidY(lbl_kitchen_name.frame),300,45);
        img_line19.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+10,CGRectGetMidY(txt_kitchen_name.frame)+10, WIDTH-100, 0.5);
        icon_kichen2.frame = CGRectMake(20,CGRectGetMaxY(icon_kichen.frame)+20,40,40);
        lbl_kitchen_address.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+10,CGRectGetMaxY(img_line19.frame)+10,200,45);
        lbl_kitchen_address.font = [UIFont fontWithName:kFontBold size:12];
        txt_kitchen_address.frame = CGRectMake(CGRectGetMaxX(icon_kichen.frame)+5,CGRectGetMidY(lbl_kitchen_address.frame),300,45);
        
        img_bg_for_kitchen_imgs.frame = CGRectMake(0,CGRectGetMaxY(img_bg2.frame)+5, WIDTH+2, 370);
        img_video.frame = CGRectMake(20,10,280,100);
        img_bg_for_food_storage.frame = CGRectMake(0,CGRectGetMaxY(img_bg_for_kitchen_imgs.frame)+5, WIDTH+2, 350);
        icon_frize.frame = CGRectMake(20,20,30,40);
        lbl_food_storage.frame = CGRectMake(CGRectGetMaxX(icon_frize.frame)+20,5,200,45);
        lbl_food_storage.font = [UIFont fontWithName:kFontBold size:15];
        txt_view_for_storage_description .frame = CGRectMake(CGRectGetMaxX(icon_frize.frame)+20,CGRectGetMaxY(lbl_food_storage.frame), WIDTH-100, 100);
        lbl_max_wordes_in_storage.frame = CGRectMake(200,CGRectGetMaxY( txt_view_for_storage_description.frame)-15, 100, 45);
        lbl_max_wordes_in_storage.font = [UIFont fontWithName:kFont size:11];
        icon_hygienes.frame = CGRectMake(20,CGRectGetMaxY(lbl_max_wordes_in_storage.frame),38,40);
        lbl_food_hygiene.frame = CGRectMake(CGRectGetMaxX(icon_hygienes.frame)+20,CGRectGetMidY(lbl_max_wordes_in_storage.frame)+5,200,45);
        lbl_food_hygiene.font = [UIFont fontWithName:kFontBold size:15];
        txt_view_for_hygiene_description .frame = CGRectMake(CGRectGetMaxX(icon_frize.frame)+20,CGRectGetMaxY(lbl_food_hygiene.frame), WIDTH-100, 100);
        lbl_max_wordes_in_hygiene.frame = CGRectMake(200,CGRectGetMaxY( txt_view_for_hygiene_description.frame)-15, 100, 45);
        lbl_max_wordes_in_hygiene.font = [UIFont fontWithName:kFont size:11];
        img_strip2.frame = CGRectMake(CGRectGetMaxX(img_strip1.frame)+7,CGRectGetMaxY(collView_for_icons.frame)+2,77, 3);
        
        
        
        view_for_dine_in.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_for_din_in_area.frame = CGRectMake(0,10, WIDTH+2,270);
        lbl_dine_in_area.frame = CGRectMake(20,0, 200,45);
        lbl_dine_in_area.font = [UIFont fontWithName:kFontBold size:15];
        img_din_in_area.frame = CGRectMake(15,CGRectGetMaxY(lbl_dine_in_area.frame)-5, WIDTH-33,100);
        
        icon_no_of_seats.frame = CGRectMake(25,CGRectGetMaxY(img_din_in_area.frame)+25, 35,20);
        lbl_no_of_seats.frame = CGRectMake(CGRectGetMaxX(icon_no_of_seats.frame)+10,CGRectGetMaxY(img_din_in_area.frame)+5, 200,45);
        lbl_no_of_seats.font = [UIFont fontWithName:kFontBold size:12];
        txt_no_of_seats_val.frame = CGRectMake(CGRectGetMaxX(icon_no_of_seats.frame)+10,CGRectGetMaxY(lbl_no_of_seats.frame)-25, 200,45);
        txt_no_of_seats_val.font = [UIFont fontWithName:kFont size:12];
        img_line24.frame =  CGRectMake(20,CGRectGetMaxY(txt_no_of_seats_val.frame)-10, 280, 0.5);
        icon_parking.frame = CGRectMake(25,CGRectGetMaxY(img_line24.frame)+10, 35,35);
        lbl_parking.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(img_line24.frame)-2, 200,45);
        lbl_parking.font = [UIFont fontWithName:kFontBold size:12];
        
        
        img_radio_btn_on_yes.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-10,20,20);
        btn_Yes.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-10,20,20);
        lbl_parking_yes.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-22, 40,45);
        
        
        
        text_parking_val.frame = CGRectMake(CGRectGetMaxX(radio_button_for_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-22, 40,45);
        text_parking_val.font = [UIFont fontWithName:kFont size:15];
        
        img_radio_btn_on_no.frame = CGRectMake(CGRectGetMaxX(lbl_parking_yes.frame)+10,CGRectGetMaxY(lbl_parking.frame)-10,20,20);
        btn_update.frame = CGRectMake(30,CGRectGetMaxY(img_bg_for_din_in_area.frame)+20,WIDTH-60,40);
        lbl_update.frame = CGRectMake(100,0, 100,45);
        lbl_update.font = [UIFont fontWithName:kFont size:15];
        img_strip3.frame = CGRectMake(CGRectGetMaxX(img_strip2.frame)+3,CGRectGetMaxY(collView_for_icons.frame)+2,80, 3);
        view_for_menu.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_for_dietary.frame = CGRectMake(0,10, WIDTH+2,130);
        table_for_menu.frame  = CGRectMake(8,4,305,116);
        img_bg_for_menu_items.frame = CGRectMake(0,CGRectGetMaxY(img_bg_for_dietary.frame)+35, WIDTH+2,250);
        
        
        
        view_for_schedule.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_calender.frame = CGRectMake(0,10, WIDTH, 300);
        img_bg_for_schedule.frame = CGRectMake(0,CGRectGetMaxY(img_calender.frame), WIDTH, 250);
        calendar.frame = CGRectMake(10, 0, img_bg_for_schedule.frame.size.width-20, img_bg_for_schedule.frame.size.height-35);
        
        table_for_schedule.frame  = CGRectMake(10,0,300,155);
        btn_on_schedule_img.frame = CGRectMake(20,CGRectGetMaxY(img_bg_for_schedule.frame)+10, WIDTH-40, 45);
        
        
        view_for_schedule_items.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_for_selected_schedule_date.frame = CGRectMake(0,05, WIDTH, 55);
        table_for_selected_schedule_deate.frame  = CGRectMake(10,0,300,55);
        table_for_dish_items.frame  = CGRectMake(10,CGRectGetMaxY(img_bg_for_selected_schedule_date.frame)+10,WIDTH-20,300);
        btn_img_edit.frame = CGRectMake(10,CGRectGetMaxY(table_for_dish_items.frame)+10,140,40);
        btn_img_delete.frame = CGRectMake(CGRectGetMaxX(btn_img_edit.frame)+20,CGRectGetMaxY(table_for_dish_items.frame)+10,140,40);
        
        
        view_for_on_request.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        lbl_currently_accept_orders.frame = CGRectMake(90,02, 300, 45);
        lbl_currently_accept_orders.font = [UIFont fontWithName:kFontBold size:15];
        lbl_orders_accept.frame = CGRectMake(90,CGRectGetMaxY(lbl_currently_accept_orders.frame)+5, 300, 45);
        lbl_orders_accept.font = [UIFont fontWithName:kFontBold size:15];
        img_green_percentage .frame = CGRectMake(20,CGRectGetMaxY(lbl_orders_accept.frame),260,15);
        lbl_percentage_val.frame = CGRectMake(CGRectGetMaxX(img_green_percentage.frame)+10,CGRectGetMaxY(lbl_orders_accept.frame)-15, 300, 45);
        lbl_percentage_val.font = [UIFont fontWithName:kFontBold size:12];
        lbl_avg_respond_time.frame = CGRectMake(20,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
        lbl_avg_respond_time.font = [UIFont fontWithName:kFont size:15];
        avg_respond_time_val.frame = CGRectMake(CGRectGetMidX(lbl_avg_respond_time.frame)+35,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
        avg_respond_time_val.font = [UIFont fontWithName:kFontBold size:15];
        
        
        view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_in_reviews.frame = CGRectMake(10,20, WIDTH-20, 250);
        label_favorite_by_user.frame = CGRectMake(20,10, 200,30);
        label_favorite_by_user.font = [UIFont fontWithName:kFont size:15];
        label_favorite_value.frame = CGRectMake(CGRectGetMaxX(label_favorite_by_user.frame)+3,-2, 295,60);
        label_favorite_value.font = [UIFont fontWithName:kFontBold size:20];
        img_line_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_favorite_by_user.frame)+10, WIDTH-50, 0.5);
        label_total_reviews.frame = CGRectMake(20,CGRectGetMaxY(img_line_in_reviews.frame)+10, 200,30);
        label_total_reviews.font = [UIFont fontWithName:kFont size:15];
        label_total_reviews_value.frame = CGRectMake(CGRectGetMaxX(label_total_reviews.frame)-5,CGRectGetMaxY(img_line_in_reviews.frame)+10, 150,30);
        label_total_reviews_value.font = [UIFont fontWithName:kFontBold size:20];
        
        icon_red_right_arrow.frame = CGRectMake(CGRectGetMaxX(label_total_reviews_value.frame)-100,CGRectGetMaxY( img_line_in_reviews.frame)+15,20,20);
        btn_on_right_arrow.frame = CGRectMake(4,CGRectGetMaxY(img_line_in_reviews.frame),290,50);
        img_line2_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_total_reviews.frame)+10, WIDTH-50, 0.5);
        label_overall_rating.frame = CGRectMake(20,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 200,30);
        label_overall_rating.font = [UIFont fontWithName:kFont size:15];
        img_thumb .frame = CGRectMake(CGRectGetMaxX(label_overall_rating.frame)-15,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 25,25);
        label_overall_rating_value.frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+5,CGRectGetMaxY(img_line2_in_reviews.frame)-5, 150,60);
        label_overall_rating_value.font = [UIFont fontWithName:kFontBold size:17];
        img_line3_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_overall_rating.frame)+10, WIDTH-50, 0.5);
        label_food_ratings.frame = CGRectMake(20,CGRectGetMaxY(img_line3_in_reviews.frame)+10, 250,30);
        label_food_ratings.font = [UIFont fontWithName:kFont size:15];
        label_taste.frame = CGRectMake(10,CGRectGetMaxY(label_food_ratings.frame)+10, 50,30);
        label_taste.font = [UIFont fontWithName:kFont size:13];
        taste_value.frame = CGRectMake(CGRectGetMaxX(label_taste.frame)-10,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
        label_appeal.frame = CGRectMake(CGRectGetMaxX(taste_value.frame)-15,CGRectGetMaxY(label_food_ratings.frame)+10,70,30);
        label_appeal.font = [UIFont fontWithName:kFont size:13];
        appeal_value.frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-15,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
        appeal_value.font = [UIFont fontWithName:@"Arial" size:13];
        label_value.frame = CGRectMake(CGRectGetMaxX(appeal_value.frame)-20,CGRectGetMaxY(label_food_ratings.frame)+10, 50,30);
        label_value.font = [UIFont fontWithName:kFont size:13];
        value_value.frame = CGRectMake(CGRectGetMaxX(label_value.frame)-5,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
        value_value.font = [UIFont fontWithName:kFont size:13];
        
        
        view_for_delivery.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,700);
        table_for_delevery.frame  = CGRectMake(10,10,WIDTH-20,400);
        
        icon_add.frame = CGRectMake(100,450,90,90);
        btn_update_in_delivery.frame = CGRectMake(30,CGRectGetMidX(icon_add.frame)+20,WIDTH-60,50);
        lbl_update_in_delivery.frame = CGRectMake(90,5,200,40);
        lbl_update_in_delivery.font = [UIFont fontWithName:kFont size:15];
        
        
        view_for_note.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_in_note.frame = CGRectMake(10,20, WIDTH-20, 240);
        txtview_note .frame = CGRectMake(10,10,277,150);
        lbl_max_wordes_in_note.frame = CGRectMake(195,CGRectGetMaxY( txtview_note.frame)-15, 100, 45);
        lbl_max_wordes_in_note.font = [UIFont fontWithName:kFont size:11];
        
        btn_update_in_note.frame = CGRectMake(30,CGRectGetMaxY(img_bg_in_note.frame)+10,WIDTH-60,50);
        lbl_update_in_note.frame = CGRectMake(90,5,200,40);
        lbl_update_in_note.font = [UIFont fontWithName:kFont size:15];
        
        
        //        scroll.frame = CGRectMake(0,0, WIDTH, HEIGHT);
        
        //        lbl_dine_in.font = [UIFont fontWithName:kFont size:13];
        //        text_in_dine_in.font = [UIFont fontWithName:kFont size:10];
        //        lbl_take_out.font = [UIFont fontWithName:kFont size:13];
        //
        //        text_in_take_out.font = [UIFont fontWithName:kFont size:10];
        //        lbl_deliver.font = [UIFont fontWithName:kFont size:13];
        //        text_in_delivery.font = [UIFont fontWithName:kFont size:10];
        //        lbl_cooking_qualifications.font = [UIFont fontWithName:kFontBold size:10];
        
        
    }
    
    if (IS_IPHONE_6Plus)
    {
        [scroll setContentSize:CGSizeMake(0,1085)];
    }
    else if (IS_IPHONE_6)
    {
        [scroll setContentSize:CGSizeMake(0,1085)];
    }
    else
    {
        [scroll setContentSize:CGSizeMake(0,1085)];
    }
    
    
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == table_for_kitchen_info)
    {
        return [array_icon_kitchen count];
    }
    else if (tableView == table_for_food_storage)
    {
        return [array_head_names_in_food_storage count];
    }
    else if (tableView == table_for_menu)
    {
        return 2;
    }
    else if(tableView == table_for_schedule)
    {
        return [ary_scedulelist count];
    }
    else if (tableView == table_for_selected_schedule_deate)
    {
        return [array_serving_date_time count];
    }
    else if (tableView == table_for_dish_items)
    {
        return [ary_displaynames count];
        
    }
    else if ( tableView == table_for_delevery)
    {
        return [ary_delivaryaddress count];
        
    }
    else if (tableView == table_for_chef_type)
    {
        return [array_chef_type count];
    }
    else if (tableView == table_for_cuntrycode)
    {
        return  [ary_countrycodelist count];
    }
    else if (tableView == table_for_cuntry_name)
    {
        
        return [arrayCountry count];
    }
    else if (tableView == table_for_city_name)
    {
        return [ary_citylist count];
        
    }
    
    
    
    
    
    
    return  0;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == table_for_kitchen_info)
    {
        return 1;
    }
    else if (tableView == table_for_food_storage)
    {
        return 1;
    }
    else if (tableView == table_for_menu)
    {
        return 1;
    }
    else if(tableView == table_for_schedule)
    {
        return 1;
    }
    else if (tableView == table_for_selected_schedule_deate)
    {
        return 1;
    }
    else if (tableView == table_for_dish_items)
    {
        return 1;
    }
    else if ( tableView == table_for_delevery)
    {
        return [array_delivery_company_name count];
        
    }
    else if (tableView == table_for_chef_type)
    {
        return 1;
    }
    else if (tableView == table_for_cuntrycode)
    {
        return 1;
    }
    else if (tableView == table_for_cuntry_name)
    {
        return 1;
    }
    else if (tableView == table_for_city_name)
    {
        return 1;
    }
    
    
    
    
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table_for_kitchen_info)
    {
        return 70;
    }
    else if (tableView == table_for_food_storage)
    {
        
        return 70;
    }
    else if (tableView == table_for_menu)
    {
        return 69;
    }
    else if (tableView == table_for_schedule)
    {
        return 50;
    }
    else if (tableView == table_for_selected_schedule_deate)
    {
        return 50;
    }
    else if (tableView == table_for_dish_items)
    {
        
        return 170;
    }
    else if ( tableView == table_for_delevery)
    {
        return 500;
        
    }
    else if (tableView == table_for_chef_type)
    {
        return 27;
    }
    else if (tableView == table_for_cuntrycode)
    {
        return 20;
    }
    else if (tableView == table_for_cuntry_name)
    {
        return 20;
    }
    else if (tableView == table_for_city_name)
    {
        return 20;
    }
    
    
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    if (tableView == table_for_kitchen_info)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        img_cellBackGnd.frame =  CGRectMake(0,2, 310, 70);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"img-whtcell-bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor whiteColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UIImageView *kitchen_icon_in_cell = [[UIImageView alloc]init];
        kitchen_icon_in_cell.frame =  CGRectMake(20,10, 40, 40);
        [kitchen_icon_in_cell setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_icon_kitchen objectAtIndex:indexPath.row]]]];
        kitchen_icon_in_cell.backgroundColor =[UIColor clearColor];
        [kitchen_icon_in_cell setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:kitchen_icon_in_cell];
        
        UILabel *lbl_head_in_kitchen_cell = [[UILabel alloc]init];
        lbl_head_in_kitchen_cell .frame = CGRectMake(CGRectGetMaxX(kitchen_icon_in_cell.frame)+15,10,200, 15);
        lbl_head_in_kitchen_cell .text = [NSString stringWithFormat:@"%@",[array_head_names_in_kitchen objectAtIndex:indexPath.row]];
        lbl_head_in_kitchen_cell .font = [UIFont fontWithName:kFontBold size:15];
        lbl_head_in_kitchen_cell .textColor = [UIColor blackColor];
        lbl_head_in_kitchen_cell .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_head_in_kitchen_cell ];
        
        UILabel *lbl_kitchen_details = [[UILabel alloc]init];
        lbl_kitchen_details .frame = CGRectMake(CGRectGetMaxX(kitchen_icon_in_cell.frame)+15,CGRectGetMaxY(lbl_head_in_kitchen_cell.frame)+10,200, 15);
        lbl_kitchen_details .text = [NSString stringWithFormat:@"%@",[array_kitchen_name objectAtIndex:indexPath.row]];
        lbl_kitchen_details .font = [UIFont fontWithName:kFont size:12];
        lbl_kitchen_details .textColor = [UIColor blackColor];
        lbl_kitchen_details.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_kitchen_details];
        
        UIImageView *img_line = [[UIImageView alloc]init];
        img_line.frame =  CGRectMake(7,67, 280, 0.5);
        img_line.backgroundColor =[UIColor grayColor];
        [img_line setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line];
        
        
    }
    else if (tableView == table_for_food_storage)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        img_cellBackGnd.frame =  CGRectMake(0,2, 310, 70);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"img-whtcell-bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor whiteColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UIImageView *kitchen_storage_icons = [[UIImageView alloc]init];
        kitchen_storage_icons.frame =  CGRectMake(20,10, 30, 35);
        [kitchen_storage_icons setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_icon_storages_hygiene objectAtIndex:indexPath.row]]]];
        kitchen_storage_icons.backgroundColor =[UIColor clearColor];
        [kitchen_storage_icons setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:kitchen_storage_icons];
        
        UILabel *lbl_head_in_kitchen_storage_cell = [[UILabel alloc]init];
        lbl_head_in_kitchen_storage_cell .frame = CGRectMake(CGRectGetMaxX(kitchen_storage_icons.frame)+15,10,200, 15);
        lbl_head_in_kitchen_storage_cell .text = [NSString stringWithFormat:@"%@",[array_head_names_in_kitchen objectAtIndex:indexPath.row]];
        lbl_head_in_kitchen_storage_cell .font = [UIFont fontWithName:kFontBold size:15];
        lbl_head_in_kitchen_storage_cell .textColor = [UIColor blackColor];
        lbl_head_in_kitchen_storage_cell .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_head_in_kitchen_storage_cell ];
        
        UITextView * txtview_adddescription = [[UITextView alloc]init];
        txtview_adddescription.frame = CGRectMake(CGRectGetMaxX(kitchen_storage_icons.frame)+15,CGRectGetMaxY(lbl_head_in_kitchen_storage_cell.frame)-5, 240,40);
        txtview_adddescription.scrollEnabled = YES;
        txtview_adddescription.userInteractionEnabled = NO;
        txtview_adddescription.font = [UIFont fontWithName:kFont size:12];
        txtview_adddescription.backgroundColor = [UIColor clearColor];
        txtview_adddescription.delegate = self;
        txtview_adddescription.textColor = [UIColor blackColor];
        txtview_adddescription.text =@"LOrem ipsumdotor sit amet.ne dicit virits corpor a per,ut illum liberavisse vet.At mei vidit conse cteture.in vitae definitionem per.tate vetear cliquado ei vim.";
        [img_cellBackGnd addSubview:txtview_adddescription];
        
        UIImageView *img_line = [[UIImageView alloc]init];
        img_line.frame =  CGRectMake(7,CGRectGetMaxY(txtview_adddescription.frame)+10, 280, 0.5);
        img_line.backgroundColor =[UIColor grayColor];
        [img_line setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line];
        
        
    }
    else if (tableView == table_for_menu)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        img_cellBackGnd.frame =  CGRectMake(0,2,WIDTH+80, 57);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"img-whtcell-bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UIImageView *dietary_icons = [[UIImageView alloc]init];
        dietary_icons.frame =  CGRectMake(20,10, 30, 30);
        // [dietary_icons setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_dietary_icons objectAtIndex:indexPath.row]]]];
        dietary_icons.backgroundColor =[UIColor clearColor];
        [dietary_icons setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:dietary_icons];
        
        UILabel *lbl_head_in_dietary_cell = [[UILabel alloc]init];
        lbl_head_in_dietary_cell .frame = CGRectMake(CGRectGetMaxX(dietary_icons.frame)+15,10,250, 14);
        lbl_head_in_dietary_cell .font = [UIFont fontWithName:kFontBold size:10];
        lbl_head_in_dietary_cell .textColor = [UIColor blackColor];
        lbl_head_in_dietary_cell .backgroundColor = [UIColor clearColor];
        lbl_head_in_dietary_cell.numberOfLines=2;
        
        [img_cellBackGnd addSubview:lbl_head_in_dietary_cell];
        
        UILabel *text_in_dietary_cell = [[UILabel alloc]init];
        text_in_dietary_cell .frame = CGRectMake(CGRectGetMaxX(dietary_icons.frame)+15,CGRectGetMaxY(lbl_head_in_dietary_cell.frame)+5,250, 30);
        
        
        NSMutableString *str_dietid = [[NSMutableString alloc] init];
        NSMutableString *str_cuisenid = [[NSMutableString alloc] init];
        
        if (indexPath.row==0)
        {
            for (int i=0; i<[array_text_in_dietary_cell count]; i++)
            {
                
                
                if (i==[array_text_in_dietary_cell count]-1)
                {
                    [str_dietid  appendString:[NSString stringWithFormat:@"%@",[[array_text_in_dietary_cell objectAtIndex:i] valueForKey:@"name"]]];
                }
                else
                {
                    [str_dietid  appendString:[NSString stringWithFormat:@"%@,",[[array_text_in_dietary_cell objectAtIndex:i] valueForKey:@"name"]]];
                }
                
                
            }
            lbl_head_in_dietary_cell .text = [NSString stringWithFormat:@"%@",@"Cuisines Cooked"];
            [dietary_icons setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"icon-order-food@2x.png"]]];
            text_in_dietary_cell .text =str_dietid;
            
        }
        else{
            
            
            
            for (int i=0; i<[array_text_in_cuisens_cell count]; i++)
            {
                
                
                if (i==[array_text_in_cuisens_cell count]-1)
                {
                    [str_cuisenid  appendString:[NSString stringWithFormat:@"%@",[[array_text_in_cuisens_cell objectAtIndex:i] valueForKey:@"name"]]];
                }
                else
                {
                    [str_cuisenid  appendString:[NSString stringWithFormat:@"%@,",[[array_text_in_cuisens_cell objectAtIndex:i] valueForKey:@"name"]]];
                }
                
                
            }
            lbl_head_in_dietary_cell .text = [NSString stringWithFormat:@"%@",@"Dietary Restrictions followed"];
            
            [dietary_icons setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"icon-allo_food@2x.png"]]];
            
            text_in_dietary_cell .text =str_cuisenid;
            
            
        }
        
        
        
        // text_in_dietary_cell .text = [NSString stringWithFormat:@"%@",[array_text_in_dietary_cell objectAtIndex:indexPath.row]];
        text_in_dietary_cell .font = [UIFont fontWithName:kFont size:10];
        text_in_dietary_cell .textColor = [UIColor blackColor];
        text_in_dietary_cell .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:text_in_dietary_cell];
        
        UIImageView *img_line = [[UIImageView alloc]init];
        img_line.frame =  CGRectMake(7,CGRectGetMaxY(text_in_dietary_cell.frame)+15, 280, 0.7);
        img_line.backgroundColor =[UIColor grayColor];
        [img_line setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line];
        
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(-10,0, WIDTH+3, 74);
            dietary_icons.frame =  CGRectMake(20,10, 30, 30);
            
            lbl_head_in_dietary_cell .frame = CGRectMake(CGRectGetMaxX(dietary_icons.frame)+15,10,250, 14);
            text_in_dietary_cell .frame = CGRectMake(CGRectGetMaxX(dietary_icons.frame)+15,CGRectGetMaxY(lbl_head_in_dietary_cell.frame)+5,250, 30);
            img_line.frame =  CGRectMake(7,71,WIDTH-16, 1);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(-8,0,WIDTH, 62);
            dietary_icons.frame =  CGRectMake(20,10, 30, 30);
            
            lbl_head_in_dietary_cell .frame = CGRectMake(CGRectGetMaxX(dietary_icons.frame)+15,10,250, 14);
            text_in_dietary_cell .frame = CGRectMake(CGRectGetMaxX(dietary_icons.frame)+15,CGRectGetMaxY(lbl_head_in_dietary_cell.frame)+5,250, 30);
            img_line.frame =  CGRectMake(7,61, 360, 1);
            
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(-10,0, 340, 62);
            dietary_icons.frame =  CGRectMake(20,10, 30, 30);
            lbl_head_in_dietary_cell .frame = CGRectMake(CGRectGetMaxX(dietary_icons.frame)+15,10,250, 14);
            text_in_dietary_cell .frame = CGRectMake(CGRectGetMaxX(dietary_icons.frame)+15,CGRectGetMaxY(lbl_head_in_dietary_cell.frame)+5,250, 30);
            img_line.frame =  CGRectMake(7,61, 307, 1);
            
        }
        
        
    }
    
    else if (tableView == table_for_schedule)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        //    img_cellBackGnd.frame =  CGRectMake(0,2, 320, 50);
        //    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor whiteColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UIImageView *img_line = [[UIImageView alloc]init];
        //    img_line.frame =  CGRectMake(7,49, 280, 0.5);
        img_line.backgroundColor =[UIColor grayColor];
        [img_line setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line];
        
        UIImageView *servingimges = [[UIImageView alloc]init];
        //   servingimges.frame =  CGRectMake(10,10, 30, 30);
        // NSString *ImagePath =  [[NSString stringWithFormat:@"%@",[ary_scedulelist objectAtIndex:indexPath.row]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        // [servingimges setImageWithURL:[NSURL URLWithString:ImagePath] placeholderImage:[UIImage imageNamed:@"placeholder@2x.png"]];
        [servingimges setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"seving-type-icon@2x.png"]]];
        servingimges.backgroundColor =[UIColor clearColor];
        [servingimges setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:servingimges];
        
        UILabel *serving_timeings = [[UILabel alloc]init];
        //    serving_timeings .frame = CGRectMake(50,10,200, 15);
        serving_timeings .text = [NSString stringWithFormat:@"%@ - %@",[[array_serving_time objectAtIndex:indexPath.row] valueForKey:@"DisplayFromTime"],[[array_serving_time objectAtIndex:indexPath.row] valueForKey:@"DisplayToTime"]];
        serving_timeings .font = [UIFont fontWithName:kFontBold size:12];
        serving_timeings .textColor = [UIColor blackColor];
        serving_timeings .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:serving_timeings ];
        
        UILabel *serving_items = [[UILabel alloc]init];
        //    serving_items .frame = CGRectMake(50,30,200, 15);
        serving_items .text = [NSString stringWithFormat:@"%@",[[array_serving_time objectAtIndex:indexPath.row] valueForKey:@"DishName"]];
        serving_items .font = [UIFont fontWithName:kFont size:10];
        serving_items .textColor = [UIColor blackColor];
        serving_items.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:serving_items ];
        
        
        UIButton *icon_right_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
        //    icon_right_arrow.frame = CGRectMake(260,10, 20, 20);
        //icon_thumb .backgroundColor = [UIColor clearColor];
        //  [icon_right_arrow addTarget:self action:@selector(icon_right_arrow_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [icon_right_arrow setImage:[UIImage imageNamed:@"right-arrow-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_right_arrow];
        
        //         UIButton *btn_on_scedule_cell = [UIButton buttonWithType:UIButtonTypeCustom];
        //          btn_on_scedule_cell.frame = CGRectMake(5,5, 200, 20);
        //         btn_on_scedule_cell .backgroundColor = [UIColor clearColor];
        //         [btn_on_scedule_cell addTarget:self action:@selector(icon_right_arrow_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
        //       //[btn_on_scedule_cell setImage:[UIImage imageNamed:@"right-arrow-icon@2x.png"] forState:UIControlStateNormal];
        //         [img_cellBackGnd   addSubview:btn_on_scedule_cell];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(5,2, WIDTH-20, 50);
            img_line.frame =  CGRectMake(7,49, WIDTH-20, 0.5);
            servingimges.frame =  CGRectMake(10,10, 30, 30);
            serving_timeings .frame = CGRectMake(50,10,200, 15);
            serving_items .frame = CGRectMake(50,30,300, 15);
            icon_right_arrow.frame = CGRectMake(WIDTH-68,15, 20, 20);
            //  btn_on_scedule_cell.frame = CGRectMake(0,0, 300, 49);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, WIDTH-20, 50);
            img_line.frame =  CGRectMake(7,49, WIDTH-40, 0.5);
            servingimges.frame =  CGRectMake(10,10, 30, 30);
            serving_timeings .frame = CGRectMake(50,10,200, 15);
            serving_items .frame = CGRectMake(50,30,300, 15);
            icon_right_arrow.frame = CGRectMake(WIDTH-65,15, 20, 20);
            //  btn_on_scedule_cell.frame = CGRectMake(0,0, 300, 49);
            
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, 320, 50);
            img_line.frame =  CGRectMake(7,49, 280, 0.5);
            servingimges.frame =  CGRectMake(10,10, 30, 30);
            serving_timeings .frame = CGRectMake(50,10,200, 15);
            serving_items .frame = CGRectMake(50,30,200, 15);
            icon_right_arrow.frame = CGRectMake(260,15, 20, 20);
            //  btn_on_scedule_cell.frame = CGRectMake(0,0, 300,49);
            
        }
        
        
    }
    else if (tableView == table_for_selected_schedule_deate)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        //    img_cellBackGnd.frame =  CGRectMake(0,2, 320, 50);
        //    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor whiteColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UIImageView *img_line = [[UIImageView alloc]init];
        //    img_line.frame =  CGRectMake(7,49, 280, 0.5);
        img_line.backgroundColor =[UIColor grayColor];
        [img_line setUserInteractionEnabled:YES];
        //  [img_cellBackGnd addSubview:img_line];
        
        UIButton *icon_left_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
        // icon_left_arrow.frame = CGRectMake(260,10, 20, 20);
        //icon_left_arrow .backgroundColor = [UIColor clearColor];
        // [icon_left_arrow addTarget:self action:@selector(icon_right_arrow_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [icon_left_arrow setImage:[UIImage imageNamed:@"left-arrow-img@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_left_arrow];
        
        
        UIImageView *servingimges = [[UIImageView alloc]init];
        //   servingimges.frame =  CGRectMake(10,10, 30, 30);
        [servingimges setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_serving_img objectAtIndex:indexPath.row]]]];
        servingimges.backgroundColor =[UIColor clearColor];
        [servingimges setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:servingimges];
        
        UILabel *serving_date_time = [[UILabel alloc]init];
        //    serving_date_time .frame = CGRectMake(50,10,200, 15);
        serving_date_time .text = [NSString stringWithFormat:@"%@",[array_serving_date_time objectAtIndex:indexPath.row]];
        serving_date_time .font = [UIFont fontWithName:kFontBold size:12];
        serving_date_time .textColor = [UIColor blackColor];
        serving_date_time .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:serving_date_time ];
        
        UIButton *btn_on_scedule_cell = [UIButton buttonWithType:UIButtonTypeCustom];
        btn_on_scedule_cell.frame = CGRectMake(5,5, 200, 20);
        btn_on_scedule_cell .backgroundColor = [UIColor clearColor];
        [btn_on_scedule_cell addTarget:self action:@selector(click_on_left_arrow:) forControlEvents:UIControlEventTouchUpInside];
        //[btn_on_scedule_cell setImage:[UIImage imageNamed:@"right-arrow-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:btn_on_scedule_cell];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(5,2, WIDTH-20, 50);
            img_line.frame =  CGRectMake(7,49, WIDTH-20, 0.5);
            
            icon_left_arrow.frame = CGRectMake(05,15, 15, 15);
            servingimges.frame =  CGRectMake(CGRectGetMaxX(icon_left_arrow.frame)+10,10, 30, 30);
            serving_date_time .frame = CGRectMake(70,15,200, 15);
            
            btn_on_scedule_cell.frame = CGRectMake(0,0, 300, 49);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, WIDTH-40, 50);
            img_line.frame =  CGRectMake(7,49, WIDTH-40, 0.5);
            
            icon_left_arrow.frame = CGRectMake(05,15, 15, 15);
            servingimges.frame =  CGRectMake(CGRectGetMaxX(icon_left_arrow.frame)+10,10, 30, 30);
            serving_date_time .frame = CGRectMake(70,15,200, 15);
            
            btn_on_scedule_cell.frame = CGRectMake(0,0, 300, 49);
            
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, 320, 50);
            img_line.frame =  CGRectMake(7,49, 280, 0.5);
            
            icon_left_arrow.frame = CGRectMake(05,15, 15, 15);
            servingimges.frame =  CGRectMake(CGRectGetMaxX(icon_left_arrow.frame)+10,10, 30, 30);
            serving_date_time .frame = CGRectMake(70,15,200, 15);
            
            btn_on_scedule_cell.frame = CGRectMake(0,0, 300,49);
            
        }
        
    }
    
    else if (tableView == table_for_dish_items)
    {
        
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH+5, 170);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-5, 170);
            
        }
        else
        {    img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-10,170);
            
        }
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        
        UIImageView *img_dish = [[UIImageView alloc] init];
        //    img_dish.frame = CGRectMake(7,7, 90,  95 );
        [img_dish setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_dish_img objectAtIndex:indexPath.row]]]];
        [img_cellBackGnd addSubview:img_dish];
        
        
        UILabel *dish_name = [[UILabel alloc]init];
        //   dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
        dish_name.text = [NSString stringWithFormat:@"%@",[ary_displaynames objectAtIndex:indexPath.row]];
        dish_name.font = [UIFont fontWithName:kFontBold size:8];
        dish_name.textColor = [UIColor blackColor];
        dish_name.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:dish_name];
        
        
        UIImageView *icon_location = [[UIImageView alloc] init];
        //    icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        [icon_location setImage:[UIImage imageNamed:@"location-icon@2x.png"]];
        [img_cellBackGnd  addSubview:icon_location];
        
        UILabel *meters = [[UILabel alloc]init];
        //  meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        meters.text = @"5km";
        meters.font = [UIFont fontWithName:kFont size:15];
        meters.textColor = [UIColor blackColor];
        meters.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:meters];
        
        UIButton *icon_server = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
        //icon_server .backgroundColor = [UIColor clearColor];
        [icon_server  addTarget:self action:@selector(click_on_seve_icon:) forControlEvents:UIControlEventTouchUpInside];
        [icon_server setImage:[UIImage imageNamed:@"serving-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_server];
        
        UIButton *icon_halal = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
        //icon_halal .backgroundColor = [UIColor clearColor];
        [icon_halal addTarget:self action:@selector(click_on_halal:) forControlEvents:UIControlEventTouchUpInside];
        [icon_halal setImage:[UIImage imageNamed:@"halal-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_halal];
        
        UIImageView *img_non_veg = [[UIImageView alloc] init];
        //  img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
        [img_non_veg setImage:[UIImage imageNamed:@"non-veg bt@2x.png"]];
        [img_cellBackGnd addSubview:img_non_veg];
        
        UIButton *icon_cow = [UIButton buttonWithType:UIButtonTypeCustom];
        //  icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_cow addTarget:self action:@selector(click_on_cow_icon:) forControlEvents:UIControlEventTouchUpInside];
        [icon_cow setImage:[UIImage imageNamed:@"cow-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_cow];
        
        UIButton *icon_fronce = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_fronce addTarget:self action:@selector(click_on_icon_fronce:) forControlEvents:UIControlEventTouchUpInside];
        [icon_fronce setImage:[UIImage imageNamed:@"fronce-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_fronce];
        
        UILabel *doller_rate = [[UILabel alloc]init];
        //   doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
        doller_rate.text = @"$14.90";
        doller_rate.font = [UIFont fontWithName:kFontBold size:16];
        doller_rate.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        doller_rate.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:doller_rate];
        
        UIImageView *img_line = [[UIImageView alloc] init];
        //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
        [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
        [img_cellBackGnd addSubview:img_line];
        
        
        UIButton *icon_take = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        //icon_take .backgroundColor = [UIColor clearColor];
        [icon_take addTarget:self action:@selector(clik_on_take_out:) forControlEvents:UIControlEventTouchUpInside];
        [icon_take setImage:[UIImage imageNamed:@"take-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_take];
        
        UIButton *icon_deliver = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //icon_deliver .backgroundColor = [UIColor clearColor];
        [icon_deliver addTarget:self action:@selector(click_on_delivery:) forControlEvents:UIControlEventTouchUpInside];
        [icon_deliver setImage:[UIImage imageNamed:@"deliver-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_deliver];
        
        
        UIButton *img_btn_seving_Now = [UIButton buttonWithType:UIButtonTypeCustom];
        //   img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //img_btn_deliver_Now .backgroundColor = [UIColor clearColor];
        [img_btn_seving_Now addTarget:self action:@selector(click_on_serving_now_icon:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_seving_Now setImage:[UIImage imageNamed:@"icon-serving-time@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_seving_Now];
        
        
        UIButton *img_btn_chef_menu = [UIButton buttonWithType:UIButtonTypeCustom];
        //    img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
        [img_btn_chef_menu addTarget:self action:@selector(btn_chef_menu_click:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_chef_menu setImage:[UIImage imageNamed:@"chef-menu-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_chef_menu];
        
        UILabel *lbl_total_servings = [[UILabel alloc]init];
        lbl_total_servings.frame = CGRectMake(WIDTH-70,85,200, 15);
        lbl_total_servings.text = @"Total Servings";
        lbl_total_servings.font = [UIFont fontWithName:kFontBold size:11];
        lbl_total_servings.textColor = [UIColor blackColor];
        lbl_total_servings.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_total_servings];
        
        UILabel *lbl_total_servings_val = [[UILabel alloc]init];
        lbl_total_servings_val.frame = CGRectMake(WIDTH-70,85,200, 15);
        lbl_total_servings_val.text = [NSString stringWithFormat:@"%@",[arraY_serving_val objectAtIndex:indexPath.row]];
        lbl_total_servings_val.font = [UIFont fontWithName:kFontBold size:11];
        lbl_total_servings_val.textColor = [UIColor blackColor];
        lbl_total_servings_val.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_total_servings_val];
        
        
        UIButton *icon_thumb = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+5, 20, 20);
        //icon_thumb .backgroundColor = [UIColor clearColor];
        [icon_thumb addTarget:self action:@selector(click_on_thumb_icon:) forControlEvents:UIControlEventTouchUpInside];
        [icon_thumb setImage:[UIImage imageNamed:@"thumb-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_thumb];
        
        UILabel *likes = [[UILabel alloc]init];
        //  likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
        likes.text = @"87.4%";
        likes.font = [UIFont fontWithName:kFont size:10];
        likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        likes.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:likes];
        
        if (IS_IPHONE_6Plus)
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            lbl_total_servings.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)-5,CGRectGetMaxY(img_line.frame)+9,200, 15);
            lbl_total_servings_val.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)-5,CGRectGetMaxY(img_line.frame)+23,200, 15);
            icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+9, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+9,128,100, 10);
            
            dish_name.font = [UIFont fontWithName:kFontBold size:18];
            
        }
        else if (IS_IPHONE_6)
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(WIDTH-90,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            lbl_total_servings.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)-5,CGRectGetMaxY(img_line.frame)+9,200, 15);
            lbl_total_servings_val.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)-5,CGRectGetMaxY(img_line.frame)+23,200, 15);
            icon_thumb.frame = CGRectMake(290,CGRectGetMaxY(img_line.frame)+5, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
            
            
            dish_name.font = [UIFont fontWithName:kFontBold size:18];
            
        }
        else
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(IS_IPHONE_5?WIDTH-80:WIDTH-80,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(15, CGRectGetMaxY(img_line.frame)+5, 25, 25);
            icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  25, 25);
            img_btn_seving_Now.frame = CGRectMake(90,CGRectGetMaxY(img_line.frame)+10,  20, 20);
            img_btn_chef_menu.frame = CGRectMake(100,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            lbl_total_servings.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)-5,CGRectGetMaxY(img_line.frame)+9,200, 15);
            lbl_total_servings_val.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)-5,CGRectGetMaxY(img_line.frame)+23,200, 15);
            icon_thumb.frame = CGRectMake(IS_IPHONE_5?235:230,CGRectGetMaxY(img_line.frame)+9, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,132,100, 10);
            
            dish_name.font = [UIFont fontWithName:kFontBold size:IS_IPHONE_5?15:15];
            
        }
        
    }
    
    else if ( tableView == table_for_delevery)
    {
        UIImageView *img_bg_for_delevery = [[UIImageView alloc]init];
        img_bg_for_delevery.frame = CGRectMake(-5,0, WIDTH, 500);
        [img_bg_for_delevery  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        [img_bg_for_delevery  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_delevery];
        
        
        UILabel *labl_delevery_company_name = [[UILabel alloc]init];
        labl_delevery_company_name.frame = CGRectMake(40,20, 300,40);
        //labl_delevery_company_name.text = @"Delivery Company Name";
        labl_delevery_company_name.text = [NSString stringWithFormat:@"%@ Delivary",[[ary_delivaryaddress objectAtIndex:indexPath.row] valueForKey:@"Company_name"]];
        labl_delevery_company_name.font = [UIFont fontWithName:kFontBold size:15];
        labl_delevery_company_name.textColor = [UIColor blackColor];
        labl_delevery_company_name.backgroundColor = [UIColor clearColor];
        [img_bg_for_delevery addSubview:labl_delevery_company_name];
        
        // array_delivery_company_name
        
        UITextField *txt_delevery_company = [[UITextField alloc] init];
        txt_delevery_company.frame = CGRectMake(35,CGRectGetMidY(labl_delevery_company_name.frame),300,45);
        txt_delevery_company .borderStyle = UITextBorderStyleNone;
        txt_delevery_company .textColor = [UIColor blackColor];
        txt_delevery_company .font = [UIFont fontWithName:kFont size:12];
        txt_delevery_company .placeholder = @"";
        [txt_delevery_company  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
        [txt_delevery_company  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *padding24 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
        txt_delevery_company .leftView = padding24;
        txt_delevery_company .leftViewMode = UITextFieldViewModeAlways;
        txt_delevery_company .userInteractionEnabled=YES;
        txt_delevery_company .textAlignment = NSTextAlignmentLeft;
        txt_delevery_company .backgroundColor = [UIColor clearColor];
        txt_delevery_company .keyboardType = UIKeyboardTypeAlphabet;
        txt_delevery_company .delegate = self;
        txt_delevery_company.text = [NSString stringWithFormat:@"%@",[[ary_delivaryaddress objectAtIndex:indexPath.row] valueForKey:@"Company_name"]];
        
        [img_bg_for_delevery addSubview:txt_delevery_company ];
        
        
        UIImageView *img_line25 = [[UIImageView alloc]init];
        img_line25.frame = CGRectMake(40,CGRectGetMidY(txt_delevery_company.frame)+15, WIDTH-80, 0.5);
        [img_line25  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
        [img_line25  setUserInteractionEnabled:YES];
        [img_bg_for_delevery addSubview:img_line25];
        
        
        UILabel *labl_delevery_company_phone_no = [[UILabel alloc]init];
        labl_delevery_company_phone_no.frame = CGRectMake(40,CGRectGetMaxY(img_line25.frame), 300,40);
        labl_delevery_company_phone_no.text = @"Delivery Company Name";
        labl_delevery_company_phone_no.font = [UIFont fontWithName:kFontBold size:15];
        labl_delevery_company_phone_no.textColor = [UIColor blackColor];
        labl_delevery_company_phone_no.backgroundColor = [UIColor clearColor];
        [img_bg_for_delevery addSubview:labl_delevery_company_phone_no];
        
        
        UITextField *txt_tel_code = [[UITextField alloc] init];
        txt_tel_code.frame = CGRectMake(35,CGRectGetMidY(labl_delevery_company_phone_no.frame),80,45);
        txt_tel_code .borderStyle = UITextBorderStyleNone;
        txt_tel_code .textColor = [UIColor blackColor];
        txt_tel_code .font = [UIFont fontWithName:kFont size:12];
        txt_tel_code .placeholder = @"+01";
        [txt_tel_code  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
        [txt_tel_code  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *padding25 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
        txt_tel_code .leftView = padding25;
        txt_tel_code .leftViewMode = UITextFieldViewModeAlways;
        txt_tel_code .userInteractionEnabled=YES;
        txt_tel_code .textAlignment = NSTextAlignmentLeft;
        txt_tel_code .backgroundColor = [UIColor clearColor];
        txt_tel_code .keyboardType = UIKeyboardTypeAlphabet;
        txt_tel_code .delegate = self;
        txt_tel_code.text = [NSString stringWithFormat:@"%@",[[ary_delivaryaddress objectAtIndex:indexPath.row] valueForKey:@"Mobile_code_number"]];
        
        [img_bg_for_delevery addSubview:txt_tel_code];
        
        UIButton *icon_dropdown_for_tel_code =[UIButton buttonWithType:UIButtonTypeCustom];
        icon_dropdown_for_tel_code.frame=CGRectMake(CGRectGetMaxX(txt_tel_code.frame)-50,CGRectGetMidY(labl_delevery_company_phone_no.frame)+15, 20, 15);
        //icon_dropdown.backgroundColor = [UIColor clearColor];
        // [icon_dropdown addTarget:self action:@selector(icon_drop_down_click:) forControlEvents:UIControlEventTouchUpInside];
        icon_dropdown_for_tel_code.userInteractionEnabled = YES;
        [icon_dropdown_for_tel_code setImage:[UIImage imageNamed:@"dropdown@2x.png"] forState:UIControlStateNormal];
        [img_bg_for_delevery  addSubview:icon_dropdown_for_tel_code];
        
        UIButton *btn_on_tel_code_dropdown =[UIButton buttonWithType:UIButtonTypeCustom];
        btn_on_tel_code_dropdown.frame=CGRectMake(40,CGRectGetMidY(labl_delevery_company_phone_no.frame)+8,70,20);
        btn_on_tel_code_dropdown.backgroundColor = [UIColor clearColor];
        [btn_on_tel_code_dropdown addTarget:self action:@selector(click_on_tel_code_drop_down:) forControlEvents:UIControlEventTouchUpInside];
        btn_on_tel_code_dropdown.userInteractionEnabled = YES;
        //  [btn_on_dropdown setImage:[UIImage imageNamed:@"dropdown@2x.png"] forState:UIControlStateNormal];
        [img_bg_for_delevery  addSubview:btn_on_tel_code_dropdown];
        
        
        UIImageView *img_line26 = [[UIImageView alloc]init];
        img_line26.frame = CGRectMake(40,CGRectGetMidY(txt_tel_code.frame)+10, 50, 0.5);
        [img_line26  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
        [img_line26  setUserInteractionEnabled:YES];
        [img_bg_for_delevery addSubview:img_line26];
        
        
        
        UITextField *txt_mobile_number2 = [[UITextField alloc] init];
        txt_mobile_number2.frame = CGRectMake(CGRectGetMidX(txt_tel_code.frame)+35,CGRectGetMidY(labl_delevery_company_phone_no.frame),80,45);
        txt_mobile_number2 .borderStyle = UITextBorderStyleNone;
        txt_mobile_number2 .textColor = [UIColor blackColor];
        txt_mobile_number2 .font = [UIFont fontWithName:kFont size:12];
        txt_mobile_number2 .placeholder = @"890 374 273";
        [txt_mobile_number2  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
        [txt_mobile_number2  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *padding26 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
        txt_mobile_number2 .leftView = padding26;
        txt_mobile_number2 .leftViewMode = UITextFieldViewModeAlways;
        txt_mobile_number2 .userInteractionEnabled=YES;
        txt_mobile_number2 .textAlignment = NSTextAlignmentLeft;
        txt_mobile_number2 .backgroundColor = [UIColor clearColor];
        txt_mobile_number2 .keyboardType = UIKeyboardTypeAlphabet;
        txt_mobile_number2 .delegate = self;
        txt_mobile_number2.text = [NSString stringWithFormat:@"%@",[[ary_delivaryaddress objectAtIndex:indexPath.row] valueForKey:@"Mobile_number"]];
        
        [img_bg_for_delevery addSubview:txt_mobile_number2];
        
        
        UIImageView *img_line27 = [[UIImageView alloc]init];
        img_line27.frame = CGRectMake(CGRectGetMaxX(img_line26.frame)+15,CGRectGetMidY(txt_mobile_number2.frame)+10, 230, 0.5);
        [img_line27  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
        [img_line27  setUserInteractionEnabled:YES];
        [img_bg_for_delevery addSubview:img_line27];
        
        
        UILabel *labl_delevery_person_name = [[UILabel alloc]init];
        labl_delevery_person_name.frame = CGRectMake(40,CGRectGetMaxY(img_line27.frame), 300,40);
        labl_delevery_person_name.text = @"Delivery Person Name";
        labl_delevery_person_name.font = [UIFont fontWithName:kFontBold size:15];
        labl_delevery_person_name.textColor = [UIColor blackColor];
        labl_delevery_person_name.backgroundColor = [UIColor clearColor];
        [img_bg_for_delevery addSubview:labl_delevery_person_name];
        
        
        UITextField *txt_persone_name = [[UITextField alloc] init];
        txt_persone_name.frame = CGRectMake(35,CGRectGetMidY(labl_delevery_person_name.frame),300,45);
        txt_persone_name .borderStyle = UITextBorderStyleNone;
        txt_persone_name .textColor = [UIColor blackColor];
        txt_persone_name .font = [UIFont fontWithName:kFont size:12];
        txt_persone_name .placeholder = @"";
        [txt_persone_name  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
        [txt_persone_name  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *padding29 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
        txt_persone_name .leftView = padding29;
        txt_persone_name .leftViewMode = UITextFieldViewModeAlways;
        txt_persone_name .userInteractionEnabled=YES;
        txt_persone_name .textAlignment = NSTextAlignmentLeft;
        txt_persone_name .backgroundColor = [UIColor clearColor];
        txt_persone_name .keyboardType = UIKeyboardTypeAlphabet;
        txt_persone_name .delegate = self;
        txt_persone_name.text = [NSString stringWithFormat:@"%@",[[ary_delivaryaddress objectAtIndex:indexPath.row] valueForKey:@"Delivery_person_name"]];
        
        [img_bg_for_delevery addSubview:txt_persone_name];
        
        
        UIImageView *img_line28 = [[UIImageView alloc]init];
        img_line28.frame = CGRectMake(40,CGRectGetMidY(txt_persone_name.frame)+10, 295, 0.5);
        [img_line28  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
        [img_line28  setUserInteractionEnabled:YES];
        [img_bg_for_delevery addSubview:img_line28];
        
        
        UILabel *labl_optional = [[UILabel alloc]init];
        labl_optional.frame = CGRectMake(270,CGRectGetMinY(img_line28.frame)-10,300,40);
        labl_optional.text = @"(optional)";
        labl_optional.font = [UIFont fontWithName:kFontBold size:12];
        labl_optional.textColor = [UIColor lightGrayColor];
        labl_optional.backgroundColor = [UIColor clearColor];
        [img_bg_for_delevery addSubview:labl_optional];
        
        UILabel *labl_persone_mobile_number = [[UILabel alloc]init];
        labl_persone_mobile_number.frame = CGRectMake(40,CGRectGetMinY(img_line28.frame),300,40);
        labl_persone_mobile_number.text = @"Delivery Person Mobile";
        labl_persone_mobile_number.font = [UIFont fontWithName:kFontBold size:15];
        labl_persone_mobile_number.textColor = [UIColor blackColor];
        labl_persone_mobile_number.backgroundColor = [UIColor clearColor];
        [img_bg_for_delevery addSubview:labl_persone_mobile_number];
        
        
        UITextField *txt_tel_code2 = [[UITextField alloc] init];
        txt_tel_code2.frame = CGRectMake(35,CGRectGetMidY(labl_persone_mobile_number.frame),80,45);
        txt_tel_code2 .borderStyle = UITextBorderStyleNone;
        txt_tel_code2 .textColor = [UIColor blackColor];
        txt_tel_code2 .font = [UIFont fontWithName:kFont size:12];
        txt_tel_code2 .placeholder = @"+01";
        [txt_tel_code2  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
        [txt_tel_code2  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *padding27 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
        txt_tel_code2 .leftView = padding27;
        txt_tel_code2 .leftViewMode = UITextFieldViewModeAlways;
        txt_tel_code2 .userInteractionEnabled=YES;
        txt_tel_code2 .textAlignment = NSTextAlignmentLeft;
        txt_tel_code2 .backgroundColor = [UIColor clearColor];
        txt_tel_code2 .keyboardType = UIKeyboardTypeAlphabet;
        txt_tel_code2 .delegate = self;
        txt_tel_code2.text = [NSString stringWithFormat:@"%@",[[ary_delivaryaddress objectAtIndex:indexPath.row] valueForKey:@"Mobile_code_number"]];
        
        [img_bg_for_delevery addSubview:txt_tel_code2];
        
        UIButton *icon_dropdown_for_tel_code2 =[UIButton buttonWithType:UIButtonTypeCustom];
        icon_dropdown_for_tel_code2.frame=CGRectMake(CGRectGetMaxX(txt_tel_code2.frame)-50,CGRectGetMidY(labl_persone_mobile_number.frame)+15, 20, 15);
        //icon_dropdown.backgroundColor = [UIColor clearColor];
        // [icon_dropdown addTarget:self action:@selector(icon_drop_down_click:) forControlEvents:UIControlEventTouchUpInside];
        icon_dropdown_for_tel_code2.userInteractionEnabled = YES;
        [icon_dropdown_for_tel_code2 setImage:[UIImage imageNamed:@"dropdown@2x.png"] forState:UIControlStateNormal];
        [img_bg_for_delevery  addSubview:icon_dropdown_for_tel_code2];
        
        UIButton *btn_on_tel_code_dropdown2 =[UIButton buttonWithType:UIButtonTypeCustom];
        btn_on_tel_code_dropdown2.frame=CGRectMake(40,CGRectGetMidY(labl_persone_mobile_number.frame)+8,70,20);
        btn_on_tel_code_dropdown2.backgroundColor = [UIColor clearColor];
        [btn_on_tel_code_dropdown2 addTarget:self action:@selector(click_on_tel_code_drop_down2:) forControlEvents:UIControlEventTouchUpInside];
        btn_on_tel_code_dropdown2.userInteractionEnabled = YES;
        //  [btn_on_dropdown setImage:[UIImage imageNamed:@"dropdown@2x.png"] forState:UIControlStateNormal];
        [img_bg_for_delevery  addSubview:btn_on_tel_code_dropdown2];
        
        
        UIImageView *img_line29 = [[UIImageView alloc]init];
        img_line29.frame = CGRectMake(40,CGRectGetMidY(txt_tel_code2.frame)+10, 50, 0.5);
        [img_line29  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
        [img_line29  setUserInteractionEnabled:YES];
        [img_bg_for_delevery addSubview:img_line29];
        
        
        UITextField *txt_person_mobile_number = [[UITextField alloc] init];
        txt_person_mobile_number.frame = CGRectMake(CGRectGetMidX(txt_tel_code2.frame)+35,CGRectGetMidY(labl_persone_mobile_number.frame),80,45);
        txt_person_mobile_number .borderStyle = UITextBorderStyleNone;
        txt_person_mobile_number .textColor = [UIColor blackColor];
        txt_person_mobile_number .font = [UIFont fontWithName:kFont size:12];
        txt_person_mobile_number .placeholder = @"";
        [txt_person_mobile_number  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
        [txt_person_mobile_number  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *padding28 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
        txt_person_mobile_number .leftView = padding28;
        txt_person_mobile_number .leftViewMode = UITextFieldViewModeAlways;
        txt_person_mobile_number .userInteractionEnabled=YES;
        txt_person_mobile_number .textAlignment = NSTextAlignmentLeft;
        txt_person_mobile_number .backgroundColor = [UIColor clearColor];
        txt_person_mobile_number .keyboardType = UIKeyboardTypeAlphabet;
        txt_person_mobile_number .delegate = self;
        txt_person_mobile_number.text = [NSString stringWithFormat:@"%@",[[ary_delivaryaddress objectAtIndex:indexPath.row] valueForKey:@"Mobile_number"]];
        
        [img_bg_for_delevery addSubview:txt_person_mobile_number];
        
        
        UIImageView *img_line30 = [[UIImageView alloc]init];
        img_line30.frame = CGRectMake(CGRectGetMaxX(img_line29.frame)+15,CGRectGetMidY(txt_person_mobile_number.frame)+10, 230, 0.5);
        [img_line30  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
        [img_line30  setUserInteractionEnabled:YES];
        [img_bg_for_delevery addSubview:img_line30];
        
        
        UILabel *labl_optional_number = [[UILabel alloc]init];
        labl_optional_number.frame = CGRectMake(270,CGRectGetMinY(img_line30.frame)-10,300,40);
        labl_optional_number.text = @"(optional)";
        labl_optional_number.font = [UIFont fontWithName:kFontBold size:12];
        labl_optional_number.textColor = [UIColor lightGrayColor];
        labl_optional_number.backgroundColor = [UIColor clearColor];
        [img_bg_for_delevery addSubview:labl_optional_number];
        
        UILabel *labl_standerd_delivery_charge = [[UILabel alloc]init];
        labl_standerd_delivery_charge.frame = CGRectMake(40,CGRectGetMaxY(img_line30.frame)+15,300,40);
        labl_standerd_delivery_charge.text = @"Standard Delivery charge";
        labl_standerd_delivery_charge.font = [UIFont fontWithName:kFontBold size:15];
        labl_standerd_delivery_charge.textColor = [UIColor blackColor];
        labl_standerd_delivery_charge.backgroundColor = [UIColor clearColor];
        [img_bg_for_delevery addSubview:labl_standerd_delivery_charge];
        
        
        
        radio_button_for_delivery_charge  =  [UIButton buttonWithType:UIButtonTypeCustom];
        radio_button_for_delivery_charge.frame = CGRectMake(40,CGRectGetMidY(labl_standerd_delivery_charge.frame)+25,20,20);
        [radio_button_for_delivery_charge setImage:[UIImage imageNamed:@"icon-sele-radio@2x.png"] forState:UIControlStateSelected];
        [radio_button_for_delivery_charge setImage:[UIImage imageNamed:@"icon-un-selec-radio@2x.png"] forState:UIControlStateNormal];
        radio_button_for_delivery_charge.tag = 54;
        [radio_button_for_delivery_charge addTarget:self action:@selector(click_on_radio_btn_for_delivery_charge:) forControlEvents:UIControlEventTouchUpInside];
        [img_bg_for_delevery addSubview:  radio_button_for_delivery_charge];
        
        
        UILabel *labl_mile_charge = [[UILabel alloc]init];
        labl_mile_charge.frame = CGRectMake(CGRectGetMaxX(radio_button_for_delivery_charge.frame)+10,CGRectGetMaxY(labl_standerd_delivery_charge.frame)-5,200,40);
        labl_mile_charge.text = @"Mile Charge";
        labl_mile_charge.font = [UIFont fontWithName:kFont size:15];
        labl_mile_charge.textColor = [UIColor blackColor];
        labl_mile_charge.backgroundColor = [UIColor clearColor];
        [img_bg_for_delevery addSubview:labl_mile_charge];
        
        
        radio_button_for_delivery_charge2  =  [UIButton buttonWithType:UIButtonTypeCustom];
        radio_button_for_delivery_charge2.frame = CGRectMake(CGRectGetMinX(labl_mile_charge.frame)+100,CGRectGetMidY(labl_standerd_delivery_charge.frame)+25,20,20);
        [radio_button_for_delivery_charge2 setImage:[UIImage imageNamed:@"icon-sele-radio@2x.png"] forState:UIControlStateSelected];
        [radio_button_for_delivery_charge2 setImage:[UIImage imageNamed:@"icon-un-selec-radio@2x.png"] forState:UIControlStateNormal];
        radio_button_for_delivery_charge2.tag = 54;
        [radio_button_for_delivery_charge2 addTarget:self action:@selector(click_on_radio_btn_for_delivery_charge2:) forControlEvents:UIControlEventTouchUpInside];
        [img_bg_for_delevery addSubview:  radio_button_for_delivery_charge2];
        
        
        
        UILabel *labl_flat_charge = [[UILabel alloc]init];
        labl_flat_charge.frame = CGRectMake(CGRectGetMaxX(radio_button_for_delivery_charge2.frame)+10,CGRectGetMaxY(labl_standerd_delivery_charge.frame)-5,300,40);
        labl_flat_charge.text = @"Flate Charge";
        labl_flat_charge.font = [UIFont fontWithName:kFont size:15];
        labl_flat_charge.textColor = [UIColor blackColor];
        labl_flat_charge.backgroundColor = [UIColor clearColor];
        [img_bg_for_delevery addSubview:labl_flat_charge];
        
        UILabel *labl_first_mile = [[UILabel alloc]init];
        labl_first_mile.frame = CGRectMake(40,CGRectGetMaxY(labl_flat_charge.frame)-5,150,40);
        labl_first_mile.text = @"First mile: $";
        labl_first_mile.font = [UIFont fontWithName:kFont size:15];
        labl_first_mile.textColor = [UIColor blackColor];
        labl_first_mile.backgroundColor = [UIColor clearColor];
        [img_bg_for_delevery addSubview:labl_first_mile];
        
        UITextField *txt_first_mile_charge = [[UITextField alloc] init];
        txt_first_mile_charge.frame = CGRectMake(CGRectGetMidX(labl_first_mile.frame)+5,CGRectGetMaxY(labl_flat_charge.frame)-9,80,45);
        txt_first_mile_charge .borderStyle = UITextBorderStyleNone;
        txt_first_mile_charge .textColor = [UIColor blackColor];
        txt_first_mile_charge .font = [UIFont fontWithName:kFont size:15];
        txt_first_mile_charge .placeholder = @"10";
        [txt_first_mile_charge  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
        [txt_first_mile_charge  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *padding30 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
        txt_first_mile_charge .leftView = padding30;
        txt_first_mile_charge .leftViewMode = UITextFieldViewModeAlways;
        txt_first_mile_charge .userInteractionEnabled=YES;
        txt_first_mile_charge .textAlignment = NSTextAlignmentLeft;
        txt_first_mile_charge .backgroundColor = [UIColor clearColor];
        txt_first_mile_charge .keyboardType = UIKeyboardTypeAlphabet;
        txt_first_mile_charge .delegate = self;
        txt_first_mile_charge.text = [NSString stringWithFormat:@"%@",[[ary_delivaryaddress objectAtIndex:indexPath.row] valueForKey:@"First_mile"]];
        
        [img_bg_for_delevery addSubview:txt_first_mile_charge];
        
        
        
        UIImageView *img_line31 = [[UIImageView alloc]init];
        img_line31.frame = CGRectMake(CGRectGetMidX(labl_first_mile.frame)+10,CGRectGetMidY(txt_first_mile_charge.frame)+10, 50, 0.5);
        [img_line31  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
        [img_line31  setUserInteractionEnabled:YES];
        [img_bg_for_delevery addSubview:img_line31];
        
        UILabel *labl_each_aditional_mile = [[UILabel alloc]init];
        labl_each_aditional_mile.frame = CGRectMake(40,CGRectGetMaxY(img_line31.frame)-5,200,40);
        labl_each_aditional_mile.text = @"Each additionl mile: $";
        labl_each_aditional_mile.font = [UIFont fontWithName:kFont size:15];
        labl_each_aditional_mile.textColor = [UIColor blackColor];
        labl_each_aditional_mile.backgroundColor = [UIColor clearColor];
        [img_bg_for_delevery addSubview:labl_each_aditional_mile];
        
        UITextField *txt_each_mile_charge = [[UITextField alloc] init];
        txt_each_mile_charge.frame = CGRectMake(CGRectGetMidX(labl_each_aditional_mile.frame)+55,CGRectGetMaxY(img_line31.frame)-5,80,45);
        txt_each_mile_charge .borderStyle = UITextBorderStyleNone;
        txt_each_mile_charge .textColor = [UIColor blackColor];
        txt_each_mile_charge .font = [UIFont fontWithName:kFont size:15];
        txt_each_mile_charge .placeholder = @"5";
        [txt_each_mile_charge  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
        [txt_each_mile_charge  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *padding31 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
        txt_each_mile_charge .leftView = padding31;
        txt_each_mile_charge .leftViewMode = UITextFieldViewModeAlways;
        txt_each_mile_charge .userInteractionEnabled=YES;
        txt_each_mile_charge .textAlignment = NSTextAlignmentLeft;
        txt_each_mile_charge .backgroundColor = [UIColor clearColor];
        txt_each_mile_charge .keyboardType = UIKeyboardTypeAlphabet;
        txt_each_mile_charge .delegate = self;
        txt_each_mile_charge.text = [NSString stringWithFormat:@"%@",[[ary_delivaryaddress objectAtIndex:indexPath.row] valueForKey:@"Flat_charge"]];
        
        [img_bg_for_delevery addSubview:txt_each_mile_charge];
        
        
        
        UIImageView *img_line32 = [[UIImageView alloc]init];
        img_line32.frame = CGRectMake(CGRectGetMidX(labl_each_aditional_mile.frame)+55,CGRectGetMidY(txt_each_mile_charge.frame)+10, 50, 0.5);
        [img_line32  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
        [img_line32  setUserInteractionEnabled:YES];
        [img_bg_for_delevery addSubview:img_line32];
        
        
        UILabel *labl_flat = [[UILabel alloc]init];
        labl_flat.frame = CGRectMake(40,CGRectGetMaxY(img_line32.frame)-5,200,40);
        labl_flat.text = @"Flat Charge: $";
        labl_flat.font = [UIFont fontWithName:kFont size:15];
        labl_flat.textColor = [UIColor blackColor];
        labl_flat.backgroundColor = [UIColor clearColor];
        [img_bg_for_delevery addSubview:labl_flat];
        
        UITextField *txt_flat_charge = [[UITextField alloc] init];
        txt_flat_charge.frame = CGRectMake(CGRectGetMidX(labl_flat.frame)+10,CGRectGetMaxY(img_line32.frame)-8,80,45);
        txt_flat_charge .borderStyle = UITextBorderStyleNone;
        txt_flat_charge .textColor = [UIColor blackColor];
        txt_flat_charge .font = [UIFont fontWithName:kFont size:15];
        txt_flat_charge .placeholder = @"5";
        [txt_flat_charge  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
        [txt_flat_charge  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *padding32 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
        txt_flat_charge .leftView = padding32;
        txt_flat_charge .leftViewMode = UITextFieldViewModeAlways;
        txt_flat_charge .userInteractionEnabled=YES;
        txt_flat_charge .textAlignment = NSTextAlignmentLeft;
        txt_flat_charge .backgroundColor = [UIColor clearColor];
        txt_flat_charge .keyboardType = UIKeyboardTypeAlphabet;
        txt_flat_charge .delegate = self;
        txt_flat_charge.text = [NSString stringWithFormat:@"%@",[[ary_delivaryaddress objectAtIndex:indexPath.row] valueForKey:@"Flat_charge"]];
        
        [img_bg_for_delevery addSubview:txt_flat_charge];
        
        
        
        UIImageView *img_line33 = [[UIImageView alloc]init];
        img_line33.frame = CGRectMake(CGRectGetMidX(labl_flat.frame)+10,CGRectGetMidY(txt_flat_charge.frame)+10, 50, 0.5);
        [img_line33  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
        [img_line33  setUserInteractionEnabled:YES];
        [img_bg_for_delevery addSubview:img_line33];
        
        
        
        btn_check_box_for_setas_default  =  [UIButton buttonWithType:UIButtonTypeCustom];
        btn_check_box_for_setas_default.frame = CGRectMake(200,CGRectGetMidY(img_line33.frame)+25,20,20);
        [btn_check_box_for_setas_default setImage:[UIImage imageNamed:@"img-check-select@2x.png"] forState:UIControlStateSelected];
        [btn_check_box_for_setas_default setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
        btn_check_box_for_setas_default.tag = 54;
        [btn_check_box_for_setas_default addTarget:self action:@selector(click_on_check_box_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_bg_for_delevery addSubview:  btn_check_box_for_setas_default];
        
        UILabel *labl_set_as_default = [[UILabel alloc]init];
        labl_set_as_default.frame = CGRectMake(CGRectGetMidX(btn_check_box_for_setas_default.frame)+20,CGRectGetMidY(img_line33.frame)+13,200,40);
        labl_set_as_default.text = @"Set as Default";
        labl_set_as_default.font = [UIFont fontWithName:kFont size:15];
        labl_set_as_default.textColor = [UIColor blackColor];
        labl_set_as_default.backgroundColor = [UIColor clearColor];
        [img_bg_for_delevery addSubview:labl_set_as_default];
        
        
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_delevery.frame = CGRectMake(-5,0, WIDTH, 500);
            labl_delevery_company_name.frame = CGRectMake(40,20, 300,40);
            txt_delevery_company.frame = CGRectMake(35,CGRectGetMidY(labl_delevery_company_name.frame),300,45);
            img_line25.frame = CGRectMake(40,CGRectGetMidY(txt_delevery_company.frame)+15, WIDTH-80, 0.5);
            labl_delevery_company_phone_no.frame = CGRectMake(40,CGRectGetMaxY(img_line25.frame), 300,40);
            txt_tel_code.frame = CGRectMake(35,CGRectGetMidY(labl_delevery_company_phone_no.frame),80,45);
            icon_dropdown_for_tel_code.frame=CGRectMake(CGRectGetMaxX(txt_tel_code.frame)-50,CGRectGetMidY(labl_delevery_company_phone_no.frame)+15, 20, 15);
            btn_on_tel_code_dropdown.frame=CGRectMake(40,CGRectGetMidY(labl_delevery_company_phone_no.frame)+8,70,20);
            img_line26.frame = CGRectMake(40,CGRectGetMidY(txt_tel_code.frame)+10, 50, 0.5);
            txt_mobile_number2.frame = CGRectMake(CGRectGetMidX(txt_tel_code.frame)+35,CGRectGetMidY(labl_delevery_company_phone_no.frame),80,45);
            img_line27.frame = CGRectMake(CGRectGetMaxX(img_line26.frame)+15,CGRectGetMidY(txt_mobile_number2.frame)+10, 230, 0.5);
            labl_delevery_person_name.frame = CGRectMake(40,CGRectGetMaxY(img_line27.frame), 300,40);
            txt_persone_name.frame = CGRectMake(35,CGRectGetMidY(labl_delevery_person_name.frame),300,45);
            img_line28.frame = CGRectMake(40,CGRectGetMidY(txt_persone_name.frame)+10, 295, 0.5);
            labl_optional.frame = CGRectMake(270,CGRectGetMinY(img_line28.frame)-10,300,40);
            labl_persone_mobile_number.frame = CGRectMake(40,CGRectGetMinY(img_line28.frame),300,40);
            txt_tel_code2.frame = CGRectMake(35,CGRectGetMidY(labl_persone_mobile_number.frame),80,45);
            icon_dropdown_for_tel_code2.frame=CGRectMake(CGRectGetMaxX(txt_tel_code2.frame)-50,CGRectGetMidY(labl_persone_mobile_number.frame)+15, 20, 15);
            btn_on_tel_code_dropdown2.frame=CGRectMake(40,CGRectGetMidY(labl_persone_mobile_number.frame)+8,70,20);
            img_line29.frame = CGRectMake(40,CGRectGetMidY(txt_tel_code2.frame)+10, 50, 0.5);
            txt_person_mobile_number.frame = CGRectMake(CGRectGetMidX(txt_tel_code2.frame)+35,CGRectGetMidY(labl_persone_mobile_number.frame),80,45);
            img_line30.frame = CGRectMake(CGRectGetMaxX(img_line29.frame)+15,CGRectGetMidY(txt_person_mobile_number.frame)+10, 230, 0.5);
            labl_optional_number.frame = CGRectMake(270,CGRectGetMinY(img_line30.frame)-10,300,40);
            labl_standerd_delivery_charge.frame = CGRectMake(40,CGRectGetMaxY(img_line30.frame)+15,300,40);
            radio_button_for_delivery_charge.frame = CGRectMake(40,CGRectGetMidY(labl_standerd_delivery_charge.frame)+25,20,20);
            labl_mile_charge.frame = CGRectMake(CGRectGetMaxX(radio_button_for_delivery_charge.frame)+10,CGRectGetMaxY(labl_standerd_delivery_charge.frame)-5,200,40);
            radio_button_for_delivery_charge2.frame = CGRectMake(CGRectGetMinX(labl_mile_charge.frame)+100,CGRectGetMidY(labl_standerd_delivery_charge.frame)+25,20,20);
            labl_flat_charge.frame = CGRectMake(CGRectGetMaxX(radio_button_for_delivery_charge2.frame)+10,CGRectGetMaxY(labl_standerd_delivery_charge.frame)-5,300,40);
            labl_first_mile.frame = CGRectMake(40,CGRectGetMaxY(labl_flat_charge.frame)-5,150,40);
            txt_first_mile_charge.frame = CGRectMake(CGRectGetMidX(labl_first_mile.frame)+5,CGRectGetMaxY(labl_flat_charge.frame)-9,80,45);
            img_line31.frame = CGRectMake(CGRectGetMidX(labl_first_mile.frame)+10,CGRectGetMidY(txt_first_mile_charge.frame)+10, 50, 0.5);
            labl_each_aditional_mile.frame = CGRectMake(40,CGRectGetMaxY(img_line31.frame)-5,200,40);
            txt_each_mile_charge.frame = CGRectMake(CGRectGetMidX(labl_each_aditional_mile.frame)+55,CGRectGetMaxY(img_line31.frame)-5,80,45);
            img_line32.frame = CGRectMake(CGRectGetMidX(labl_each_aditional_mile.frame)+55,CGRectGetMidY(txt_each_mile_charge.frame)+10, 50, 0.5);
            labl_flat.frame = CGRectMake(40,CGRectGetMaxY(img_line32.frame)-5,200,40);
            txt_flat_charge.frame = CGRectMake(CGRectGetMidX(labl_flat.frame)+10,CGRectGetMaxY(img_line32.frame)-8,80,45);
            img_line33.frame = CGRectMake(CGRectGetMidX(labl_flat.frame)+10,CGRectGetMidY(txt_flat_charge.frame)+10, 50, 0.5);
            btn_check_box_for_setas_default.frame = CGRectMake(200,CGRectGetMidY(img_line33.frame)+25,20,20);
            labl_set_as_default.frame = CGRectMake(CGRectGetMidX(btn_check_box_for_setas_default.frame)+20,CGRectGetMidY(img_line33.frame)+13,200,40);
            
        }
        else if (IS_IPHONE_6)
        {
            img_bg_for_delevery.frame = CGRectMake(-5,0, WIDTH, 500);
            labl_delevery_company_name.frame = CGRectMake(40,20, 300,40);
            txt_delevery_company.frame = CGRectMake(35,CGRectGetMidY(labl_delevery_company_name.frame),300,45);
            img_line25.frame = CGRectMake(40,CGRectGetMidY(txt_delevery_company.frame)+15, WIDTH-80, 0.5);
            labl_delevery_company_phone_no.frame = CGRectMake(40,CGRectGetMaxY(img_line25.frame), 300,40);
            txt_tel_code.frame = CGRectMake(35,CGRectGetMidY(labl_delevery_company_phone_no.frame),80,45);
            icon_dropdown_for_tel_code.frame=CGRectMake(CGRectGetMaxX(txt_tel_code.frame)-50,CGRectGetMidY(labl_delevery_company_phone_no.frame)+15, 20, 15);
            btn_on_tel_code_dropdown.frame=CGRectMake(40,CGRectGetMidY(labl_delevery_company_phone_no.frame)+8,70,20);
            img_line26.frame = CGRectMake(40,CGRectGetMidY(txt_tel_code.frame)+10, 50, 0.5);
            txt_mobile_number2.frame = CGRectMake(CGRectGetMidX(txt_tel_code.frame)+35,CGRectGetMidY(labl_delevery_company_phone_no.frame),80,45);
            img_line27.frame = CGRectMake(CGRectGetMaxX(img_line26.frame)+15,CGRectGetMidY(txt_mobile_number2.frame)+10, 230, 0.5);
            labl_delevery_person_name.frame = CGRectMake(40,CGRectGetMaxY(img_line27.frame), 300,40);
            txt_persone_name.frame = CGRectMake(35,CGRectGetMidY(labl_delevery_person_name.frame),300,45);
            img_line28.frame = CGRectMake(40,CGRectGetMidY(txt_persone_name.frame)+10, 295, 0.5);
            labl_optional.frame = CGRectMake(270,CGRectGetMinY(img_line28.frame)-10,300,40);
            labl_persone_mobile_number.frame = CGRectMake(40,CGRectGetMinY(img_line28.frame),300,40);
            txt_tel_code2.frame = CGRectMake(35,CGRectGetMidY(labl_persone_mobile_number.frame),80,45);
            icon_dropdown_for_tel_code2.frame=CGRectMake(CGRectGetMaxX(txt_tel_code2.frame)-50,CGRectGetMidY(labl_persone_mobile_number.frame)+15, 20, 15);
            btn_on_tel_code_dropdown2.frame=CGRectMake(40,CGRectGetMidY(labl_persone_mobile_number.frame)+8,70,20);
            img_line29.frame = CGRectMake(40,CGRectGetMidY(txt_tel_code2.frame)+10, 50, 0.5);
            txt_person_mobile_number.frame = CGRectMake(CGRectGetMidX(txt_tel_code2.frame)+35,CGRectGetMidY(labl_persone_mobile_number.frame),80,45);
            img_line30.frame = CGRectMake(CGRectGetMaxX(img_line29.frame)+15,CGRectGetMidY(txt_person_mobile_number.frame)+10, 230, 0.5);
            labl_optional_number.frame = CGRectMake(270,CGRectGetMinY(img_line30.frame)-10,300,40);
            labl_standerd_delivery_charge.frame = CGRectMake(40,CGRectGetMaxY(img_line30.frame)+15,300,40);
            radio_button_for_delivery_charge.frame = CGRectMake(40,CGRectGetMidY(labl_standerd_delivery_charge.frame)+25,20,20);
            labl_mile_charge.frame = CGRectMake(CGRectGetMaxX(radio_button_for_delivery_charge.frame)+10,CGRectGetMaxY(labl_standerd_delivery_charge.frame)-5,200,40);
            radio_button_for_delivery_charge2.frame = CGRectMake(CGRectGetMinX(labl_mile_charge.frame)+100,CGRectGetMidY(labl_standerd_delivery_charge.frame)+25,20,20);
            labl_flat_charge.frame = CGRectMake(CGRectGetMaxX(radio_button_for_delivery_charge2.frame)+10,CGRectGetMaxY(labl_standerd_delivery_charge.frame)-5,300,40);
            labl_first_mile.frame = CGRectMake(40,CGRectGetMaxY(labl_flat_charge.frame)-5,150,40);
            txt_first_mile_charge.frame = CGRectMake(CGRectGetMidX(labl_first_mile.frame)+5,CGRectGetMaxY(labl_flat_charge.frame)-9,80,45);
            img_line31.frame = CGRectMake(CGRectGetMidX(labl_first_mile.frame)+10,CGRectGetMidY(txt_first_mile_charge.frame)+10, 50, 0.5);
            labl_each_aditional_mile.frame = CGRectMake(40,CGRectGetMaxY(img_line31.frame)-5,200,40);
            txt_each_mile_charge.frame = CGRectMake(CGRectGetMidX(labl_each_aditional_mile.frame)+55,CGRectGetMaxY(img_line31.frame)-5,80,45);
            img_line32.frame = CGRectMake(CGRectGetMidX(labl_each_aditional_mile.frame)+55,CGRectGetMidY(txt_each_mile_charge.frame)+10, 50, 0.5);
            labl_flat.frame = CGRectMake(40,CGRectGetMaxY(img_line32.frame)-5,200,40);
            txt_flat_charge.frame = CGRectMake(CGRectGetMidX(labl_flat.frame)+10,CGRectGetMaxY(img_line32.frame)-8,80,45);
            img_line33.frame = CGRectMake(CGRectGetMidX(labl_flat.frame)+10,CGRectGetMidY(txt_flat_charge.frame)+10, 50, 0.5);
            btn_check_box_for_setas_default.frame = CGRectMake(200,CGRectGetMidY(img_line33.frame)+25,20,20);
            labl_set_as_default.frame = CGRectMake(CGRectGetMidX(btn_check_box_for_setas_default.frame)+20,CGRectGetMidY(img_line33.frame)+13,200,40);
            
        }
        else
        {
            img_bg_for_delevery.frame = CGRectMake(0,0, WIDTH, 500);
            labl_delevery_company_name.frame = CGRectMake(10,20, 300,40);
            txt_delevery_company.frame = CGRectMake(5,CGRectGetMidY(labl_delevery_company_name.frame),300,45);
            img_line25.frame = CGRectMake(10,CGRectGetMidY(txt_delevery_company.frame)+15, WIDTH-40, 0.5);
            labl_delevery_company_phone_no.frame = CGRectMake(10,CGRectGetMaxY(img_line25.frame), 300,40);
            txt_tel_code.frame = CGRectMake(5,CGRectGetMidY(labl_delevery_company_phone_no.frame),80,45);
            icon_dropdown_for_tel_code.frame=CGRectMake(CGRectGetMaxX(txt_tel_code.frame)-50,CGRectGetMidY(labl_delevery_company_phone_no.frame)+15, 20, 15);
            btn_on_tel_code_dropdown.frame=CGRectMake(10,CGRectGetMidY(labl_delevery_company_phone_no.frame)+8,70,20);
            img_line26.frame = CGRectMake(10,CGRectGetMidY(txt_tel_code.frame)+10, 50, 0.5);
            txt_mobile_number2.frame = CGRectMake(CGRectGetMidX(txt_tel_code.frame)+35,CGRectGetMidY(labl_delevery_company_phone_no.frame),80,45);
            img_line27.frame = CGRectMake(CGRectGetMaxX(img_line26.frame)+15,CGRectGetMidY(txt_mobile_number2.frame)+10, 215, 0.5);
            labl_delevery_person_name.frame = CGRectMake(10,CGRectGetMaxY(img_line27.frame), 300,40);
            txt_persone_name.frame = CGRectMake(5,CGRectGetMidY(labl_delevery_person_name.frame),300,45);
            img_line28.frame = CGRectMake(10,CGRectGetMidY(txt_persone_name.frame)+10, WIDTH-40, 0.5);
            labl_optional.frame = CGRectMake(230,CGRectGetMinY(img_line28.frame)-10,300,40);
            labl_persone_mobile_number.frame = CGRectMake(10,CGRectGetMinY(img_line28.frame),300,40);
            txt_tel_code2.frame = CGRectMake(5,CGRectGetMidY(labl_persone_mobile_number.frame),80,45);
            icon_dropdown_for_tel_code2.frame=CGRectMake(CGRectGetMaxX(txt_tel_code2.frame)-50,CGRectGetMidY(labl_persone_mobile_number.frame)+15, 20, 15);
            btn_on_tel_code_dropdown2.frame=CGRectMake(10,CGRectGetMidY(labl_persone_mobile_number.frame)+8,70,20);
            img_line29.frame = CGRectMake(10,CGRectGetMidY(txt_tel_code2.frame)+10, 50, 0.5);
            txt_person_mobile_number.frame = CGRectMake(CGRectGetMidX(txt_tel_code2.frame)+35,CGRectGetMidY(labl_persone_mobile_number.frame),80,45);
            img_line30.frame = CGRectMake(CGRectGetMaxX(img_line29.frame)+15,CGRectGetMidY(txt_person_mobile_number.frame)+10, 215, 0.5);
            labl_optional_number.frame = CGRectMake(230,CGRectGetMinY(img_line30.frame)-10,300,40);
            labl_standerd_delivery_charge.frame = CGRectMake(10,CGRectGetMaxY(img_line30.frame)+15,300,40);
            radio_button_for_delivery_charge.frame = CGRectMake(10,CGRectGetMidY(labl_standerd_delivery_charge.frame)+25,20,20);
            labl_mile_charge.frame = CGRectMake(CGRectGetMaxX(radio_button_for_delivery_charge.frame)+10,CGRectGetMaxY(labl_standerd_delivery_charge.frame)-5,200,40);
            radio_button_for_delivery_charge2.frame = CGRectMake(CGRectGetMinX(labl_mile_charge.frame)+100,CGRectGetMidY(labl_standerd_delivery_charge.frame)+25,20,20);
            labl_flat_charge.frame = CGRectMake(CGRectGetMaxX(radio_button_for_delivery_charge2.frame)+10,CGRectGetMaxY(labl_standerd_delivery_charge.frame)-5,300,40);
            labl_first_mile.frame = CGRectMake(10,CGRectGetMaxY(labl_flat_charge.frame)-5,150,40);
            txt_first_mile_charge.frame = CGRectMake(CGRectGetMidX(labl_first_mile.frame)+5,CGRectGetMaxY(labl_flat_charge.frame)-9,80,45);
            img_line31.frame = CGRectMake(CGRectGetMidX(labl_first_mile.frame)+10,CGRectGetMidY(txt_first_mile_charge.frame)+10, 50, 0.5);
            labl_each_aditional_mile.frame = CGRectMake(10,CGRectGetMaxY(img_line31.frame)-5,200,40);
            txt_each_mile_charge.frame = CGRectMake(CGRectGetMidX(labl_each_aditional_mile.frame)+55,CGRectGetMaxY(img_line31.frame)-5,80,45);
            img_line32.frame = CGRectMake(CGRectGetMidX(labl_each_aditional_mile.frame)+55,CGRectGetMidY(txt_each_mile_charge.frame)+10, 50, 0.5);
            labl_flat.frame = CGRectMake(10,CGRectGetMaxY(img_line32.frame)-5,200,40);
            txt_flat_charge.frame = CGRectMake(CGRectGetMidX(labl_flat.frame)+10,CGRectGetMaxY(img_line32.frame)-8,80,45);
            img_line33.frame = CGRectMake(CGRectGetMidX(labl_flat.frame)+10,CGRectGetMidY(txt_flat_charge.frame)+10, 50, 0.5);
            btn_check_box_for_setas_default.frame = CGRectMake(150,CGRectGetMidY(img_line33.frame)+25,20,20);
            labl_set_as_default.frame = CGRectMake(CGRectGetMidX(btn_check_box_for_setas_default.frame)+20,CGRectGetMidY(img_line33.frame)+13,200,40);
            
        }
        
        
        
    }
    else if (tableView == table_for_chef_type)
    {
        UIImageView *img_bg_for_delevery = [[UIImageView alloc]init];
        img_bg_for_delevery.frame = CGRectMake(-5,0, WIDTH,30);
        [img_bg_for_delevery  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        [img_bg_for_delevery  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_delevery];
        
        UILabel *labl_chef_type = [[UILabel alloc]init];
        labl_chef_type.frame = CGRectMake(10,5, 170,15);
        //labl_delevery_company_name.text = @"Delivery Company Name";
        labl_chef_type.text = [NSString stringWithFormat:@"%@",[array_chef_type objectAtIndex:indexPath.row]];
        labl_chef_type.font = [UIFont fontWithName:kFont size:13];
        labl_chef_type.textColor = [UIColor blackColor];
        labl_chef_type.backgroundColor = [UIColor clearColor];
        [img_bg_for_delevery addSubview:labl_chef_type];
        
    }
    else if (tableView == table_for_cuntrycode)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        img_cellBackGnd.frame =  CGRectMake(0,0, 120, 25);
        //    [img_cellBackGnd setImage:[UIImage imageNamed:@"img-white-bg1@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor whiteColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        img_cellBackGnd.tag=100;
        [cell.contentView addSubview:img_cellBackGnd];
        
        UIImageView *img_bg1 = [[UIImageView alloc]init];
        img_bg1.frame =  CGRectMake(0,0, 120, 25);
        img_bg1.backgroundColor = [UIColor whiteColor];
        [img_bg1 setUserInteractionEnabled:YES];
        img_bg1.tag=100;
        [img_cellBackGnd addSubview:img_bg1];
        
        
        UILabel *cuntry_code = [[UILabel alloc]init];
        cuntry_code.frame = CGRectMake(0, 2, 120, 21);
        cuntry_code.text = [[ary_countrycodelist objectAtIndex:indexPath.row] valueForKey:@"code"];
        cuntry_code.font = [UIFont fontWithName:kFont size:15];
        cuntry_code.textColor = [UIColor blackColor];
        cuntry_code.backgroundColor = [UIColor whiteColor];
        cuntry_code.textAlignment=NSTextAlignmentLeft;
        cuntry_code.tag=200;
        [img_bg1 addSubview:cuntry_code];
    }
    
    else if (tableView == table_for_cuntry_name)
    {
        UILabel  * lbl_countrylist = [[UILabel alloc]init];
        lbl_countrylist.frame=CGRectMake(3, 0, 100, 30);
        lbl_countrylist.backgroundColor=[UIColor clearColor];
        lbl_countrylist.textColor=[UIColor blackColor];
        lbl_countrylist.font = [UIFont fontWithName:kFont size:15];
        lbl_countrylist.text=[[arrayCountry objectAtIndex:indexPath.row] valueForKey:@"Country"];
        [cell.contentView addSubview:lbl_countrylist];
        
    }
    
    else if (tableView == table_for_city_name)
    {
        UILabel  * lbl_list = [[UILabel alloc]init];
        lbl_list.frame=CGRectMake(5, 0, 150, 30);
        lbl_list.backgroundColor=[UIColor clearColor];
        lbl_list.textColor=[UIColor blackColor];
        lbl_list.font = [UIFont fontWithName:kFont size:15];
        lbl_list.text=[[ary_citylist objectAtIndex:indexPath.row] valueForKey:@"City"];
        [cell.contentView addSubview:lbl_list];
        
        
    }
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    lbl_food_now.text =[array_head_names objectAtIndex:indexPath.row];
    //    [table_on_drop_down setHidden:YES];
    if (tableView ==  table_for_schedule)
    {
        if (indexPath.row == 0)
        {
            view_for_schedule.hidden = YES;
            view_for_schedule_items.hidden = NO;
        }
        else if (indexPath.row == 1)
        {
            view_for_schedule.hidden = YES;
            view_for_schedule_items.hidden = NO;
            
        }
        else if (indexPath.row == 2)
        {
            
            view_for_schedule.hidden = YES;
            view_for_schedule_items.hidden = NO;
        }
        
    }
    //    else if (tableView == table_for_selected_schedule_deate)
    //    {
    //        if (indexPath.row == 0)
    //        {
    //            view_for_schedule.hidden = NO;
    //            view_for_schedule_items.hidden = YES;
    //
    //        }
    //    }
    
    if(tableView == table_for_chef_type)
    {
        //        if (indexPath.row == 3)
        //        {
        //            view_serch_for_food_now.hidden = NO;
        //            view_serch_for_food_later.hidden = YES;
        //
        //        }
        //
        txt_chef_type.text =[array_chef_type objectAtIndex:indexPath.row];
        [table_for_chef_type setHidden:YES];
    }
    if(tableView == table_for_cuntrycode)
    {
        NSLog(@"didSelectRowAtIndexPath");
        
        txt_cuntry_tel_code.text = [[ary_countrycodelist objectAtIndex:indexPath.row] valueForKey:@"code"];
        
        CGSize constraint1 = CGSizeMake(210 ,10000.0f);
        CGSize chatText_size1 = [txt_cuntry_tel_code.text  sizeWithFont:[UIFont fontWithName:kFont size:15.0f] constrainedToSize:constraint1 lineBreakMode:NSLineBreakByWordWrapping];
        
        
        
        table_for_cuntrycode.hidden = YES;
    }
    
    else if (tableView == table_for_cuntry_name)
    {
        txt_cuntry.font = [UIFont fontWithName:kFont size:15];
        
        txt_cuntry.text =[[arrayCountry objectAtIndex:indexPath.row] valueForKey:@"Country"];
        
        
        [table_for_cuntry_name setHidden:YES];
        [self AFcity_list];
        
        
        
    }
    else if (tableView == table_for_city_name)
    {
        txt_city.text =  [[ary_citylist objectAtIndex:indexPath.row] valueForKey:@"City"];
        [table_for_city_name setHidden:YES];
    }
    
    
    
    
}



#pragma mark UiCollectionView Delegate Methods for dish

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(collectionView == collView_for_icons)
    {
        return [array_for_icons count];
    }
    else if (collectionView == collView_for_kitchen_imgs)
    {
        return [array_kitchen_imgs count];
    }
    else if (collectionView == collView_for_menu_items)
    {
        return [ary_dishlist count];
    }
    
    return 0;
    
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if(collectionView == collView_for_icons)
    {
        return 1;
    }
    else if (collectionView == collView_for_kitchen_imgs)
    {
        return 1;
    }
    else if (collectionView == collView_for_menu_items)
    {
        return 1;
    }
    
    return 1;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView1 dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    
    if (collectionView1 == collView_for_icons)
    {
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,0,80,75);
        //[cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        UIImageView *chef_pro_icons= [[UIImageView alloc]init];
        
        if (indexPath.row ==7)
        {
            chef_pro_icons .frame = CGRectMake(20,13,25,35);
            
        }
        else{
            chef_pro_icons .frame = CGRectMake(20,15,35,30);
            
            
        }
        [chef_pro_icons setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_for_icons objectAtIndex:indexPath.row]]]];
        //[img_dish  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        chef_pro_icons .backgroundColor = [UIColor clearColor];
        [chef_pro_icons  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:chef_pro_icons];
        
        UILabel *icons_name = [[UILabel alloc]init];
        icons_name.frame = CGRectMake(10,CGRectGetMaxY(chef_pro_icons.frame),200, 12);
        icons_name.text = [NSString stringWithFormat:@"%@",[array_icons_name objectAtIndex:indexPath.row]];
        icons_name.textColor = [UIColor whiteColor];
        icons_name.backgroundColor = [UIColor clearColor];
        [cell_for_collection_view addSubview:icons_name];
        
        UIImageView* img_stripcol = [[UIImageView alloc]init];
        img_stripcol.frame = CGRectMake(0,CGRectGetMaxY(icons_name.frame),80, 2);
        // [img_stripcol setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
        img_stripcol.backgroundColor = [UIColor whiteColor];
        [img_stripcol setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:img_stripcol];
        
        if (indexPath.row==selectedindex)
        {
            img_stripcol.hidden = NO;
            chef_pro_icons.alpha = 2.0;
            icons_name.font = [UIFont fontWithName:kFontBold size:10];
            
            
        }
        else{
            img_stripcol.hidden = YES;
            chef_pro_icons.alpha = 0.8;
            icons_name.font = [UIFont fontWithName:kFont size:10];
            
            
        }
        
        //        UIButton *btn_on_icons = [UIButton buttonWithType:UIButtonTypeCustom];
        //        btn_on_icons .frame = CGRectMake(0,0,30,30);
        //        btn_on_icons .backgroundColor = [UIColor clearColor];
        //        [btn_on_icons  addTarget:self action:@selector(btn_on_icons_click:) forControlEvents:UIControlEventTouchUpInside];
        //        //[btn_on_icons  setImage:[UIImage imageNamed:@"icon-w-location@2x.png"] forState:UIControlStateNormal];
        //        [cell_for_collection_view  addSubview:btn_on_icons ];
    }
    else if (collectionView1 == collView_for_kitchen_imgs)
    {
        
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,0,70,70);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor whiteColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        UIImageView *kitchen_images = [[UIImageView alloc]init];
        kitchen_images .frame = CGRectMake(0,0,70,70);
        NSString *ImagePath =  [[NSString stringWithFormat:@"%@",[array_kitchen_imgs objectAtIndex:indexPath.row]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [kitchen_images setImageWithURL:[NSURL URLWithString:ImagePath] placeholderImage:[UIImage imageNamed:@"placeholder@2x.png"]];
        
        // [kitchen_images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_kitchen_imgs objectAtIndex:indexPath.row]]]];
        //[img_dish  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        kitchen_images .backgroundColor = [UIColor clearColor];
        [kitchen_images  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:kitchen_images];
        
        if (IS_IPHONE_6Plus)
        {
            cell_for_collection_view .frame = CGRectMake(0,5,70,70);
            kitchen_images .frame = CGRectMake(0,0,70,70);
            
        }
        else if (IS_IPHONE_6)
        {
            cell_for_collection_view .frame = CGRectMake(0,5,75,70);
            kitchen_images .frame = CGRectMake(0,0,75,70);
            
        }
        else
        {
            cell_for_collection_view .frame = CGRectMake(5,5,59,63);
            kitchen_images .frame = CGRectMake(0,0,60,60);
            
        }
        
    }
    else if (collectionView1 == collView_for_menu_items)
    {
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,5,80,103);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        UIImageView *kitchen_images = [[UIImageView alloc]init];
        kitchen_images .frame = CGRectMake(02,20,75,70);
        NSString *ImagePath =  [[NSString stringWithFormat:@"%@",[[ary_dishlist objectAtIndex:indexPath.row] valueForKey:@"DishImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [kitchen_images setImageWithURL:[NSURL URLWithString:ImagePath] placeholderImage:[UIImage imageNamed:@"placeholder@2x.png"]];
        
        //[kitchen_images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[[ary_dishlist objectAtIndex:indexPath.row] valueForKey:@"DishImage"]]]];
        //[img_dish  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        kitchen_images .backgroundColor = [UIColor clearColor];
        [kitchen_images  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:kitchen_images];
        
        if (IS_IPHONE_6Plus)
        {
            cell_for_collection_view .frame = CGRectMake(20,5,70,70);
            kitchen_images .frame = CGRectMake(0,0,70,70);
            
        }
        else if (IS_IPHONE_6)
        {
            cell_for_collection_view .frame = CGRectMake(0,5,75,70);
            kitchen_images .frame = CGRectMake(0,0,75,70);
            
        }
        else
        {
            cell_for_collection_view .frame = CGRectMake(0,0,70,70);
            kitchen_images .frame = CGRectMake(0,0,70,70);
            
        }
        
        
        
    }
    
    
    
    
    
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView ==collView_for_kitchen_imgs)
    {
        
        if (IS_IPHONE_6Plus)
        {
            return CGSizeMake((370/4),75);
        }
        else if (IS_IPHONE_6)
        {
            return CGSizeMake((WIDTH/4)-10,75);
        }
        else
        {
            return CGSizeMake((260/4),75);
        }
        
        
        
    }
    else if (collectionView == collView_for_menu_items)
    {
        
        if (IS_IPHONE_6Plus)
        {
            return CGSizeMake((370/4),60);
        }
        else if (IS_IPHONE_6)
        {
            return CGSizeMake((WIDTH/4)-5,60);
        }
        else
        {
            return CGSizeMake((WIDTH/4)-5,60);
        }
        
        
    }
    
    return CGSizeMake((WIDTH/4), 90);
    
    
    
    
    
    
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == collView_for_icons)
    {
        
        
        
        if (indexPath.row ==0)
        {
            img_strip.hidden = NO;
            img_strip1.hidden =YES;
            img_strip2.hidden = YES;
            img_strip3.hidden = YES;
            
            
            view_personal.hidden = NO;
            view_for_kitchen.hidden = YES;
            view_for_dine_in.hidden = YES;
            view_for_menu.hidden = YES;
            view_for_schedule.hidden = YES;
            view_for_schedule_items.hidden = YES;
            view_for_reviews.hidden = YES;
            view_for_delivery.hidden = YES;
            view_for_note.hidden = YES;
            
            if (IS_IPHONE_6Plus)
            {
                [scroll setContentSize:CGSizeMake(0,1200)];
            }
            else if (IS_IPHONE_6)
            {
                [scroll setContentSize:CGSizeMake(0,1200)];
            }
            else
            {
                [scroll setContentSize:CGSizeMake(0,1200)];
            }
            
        }
        else if (indexPath.row == 1)
        {
            img_strip.hidden = YES;
            img_strip1.hidden =NO;
            img_strip2.hidden = YES;
            img_strip3.hidden = YES;
            
            
            view_personal.hidden = YES;
            view_for_kitchen.hidden = NO;
            view_for_dine_in.hidden = YES;
            view_for_menu .hidden = YES;
            view_for_schedule.hidden = YES;
            view_for_schedule_items.hidden = YES;
            view_for_reviews.hidden = YES;
            view_for_delivery.hidden = YES;
            view_for_note.hidden = YES;
            if (IS_IPHONE_6Plus)
            {
                [scroll setContentSize:CGSizeMake(0,1300)];
            }
            else if (IS_IPHONE_6)
            {
                [scroll setContentSize:CGSizeMake(0,1300)];
            }
            else
            {
                [scroll setContentSize:CGSizeMake(0,1300)];
            }
        }
        else if (indexPath.row == 2)
        {
            img_strip.hidden = YES;
            img_strip1.hidden =YES;
            img_strip2.hidden = NO;
            img_strip3.hidden = YES;
            
            view_personal.hidden = YES;
            view_for_kitchen.hidden = YES;
            view_for_dine_in.hidden = NO;
            view_for_menu.hidden = YES;
            view_for_schedule.hidden = YES;
            view_for_schedule_items.hidden = YES;
            view_for_reviews.hidden = YES;
            view_for_delivery.hidden = YES;
            view_for_note.hidden = YES;
            if (IS_IPHONE_6Plus)
            {
                [scroll setContentSize:CGSizeMake(0,700)];
            }
            else if (IS_IPHONE_6)
            {
                [scroll setContentSize:CGSizeMake(0,700)];
            }
            else
            {
                [scroll setContentSize:CGSizeMake(0,700)];
            }
            
        }
        else if (indexPath.row == 3)
        {
            
            img_strip.hidden = YES;
            img_strip1.hidden =YES;
            img_strip2.hidden = YES;
            img_strip3.hidden = NO;
            
            view_personal.hidden = YES;
            view_for_kitchen.hidden = YES;
            view_for_dine_in.hidden = YES;
            view_for_menu.hidden = NO;
            view_for_schedule.hidden = YES;
            view_for_schedule_items.hidden = YES;
            view_for_reviews.hidden = YES;
            view_for_delivery.hidden = YES;
            view_for_note.hidden = YES;
            if (IS_IPHONE_6Plus)
            {
                [scroll setContentSize:CGSizeMake(0,900)];
            }
            else if (IS_IPHONE_6)
            {
                [scroll setContentSize:CGSizeMake(0,900)];
            }
            else
            {
                [scroll setContentSize:CGSizeMake(0,800)];
            }
            
        }
        else if (indexPath.row == 4)
        {
            img_strip.hidden = YES;
            img_strip1.hidden =YES;
            img_strip2.hidden = YES;
            img_strip3.hidden = NO;
            
            view_personal.hidden = YES;
            view_for_kitchen.hidden = YES;
            view_for_dine_in.hidden = YES;
            view_for_menu.hidden = YES;
            view_for_schedule.hidden = NO;
            //   view_for_schedule_items.hidden = YES;
            view_for_reviews.hidden = YES;
            view_for_delivery.hidden = YES;
            view_for_note.hidden = YES;
            
            
            view_for_schedule_items.hidden = YES;
            if (IS_IPHONE_6Plus)
            {
                [scroll setContentSize:CGSizeMake(0,1100)];
            }
            else if (IS_IPHONE_6)
            {
                [scroll setContentSize:CGSizeMake(0,1100)];
            }
            else
            {
                [scroll setContentSize:CGSizeMake(0,1000)];
            }
            
        }
        else if (indexPath.row == 5)
        {
            img_strip.hidden = YES;
            img_strip1.hidden =YES;
            img_strip2.hidden = YES;
            img_strip3.hidden = NO;
            
            view_personal.hidden = YES;
            view_for_kitchen.hidden = YES;
            view_for_dine_in.hidden = YES;
            view_for_menu.hidden = YES;
            view_for_schedule.hidden = YES;
            view_for_schedule_items.hidden = YES;
            view_for_reviews.hidden = NO;
            view_for_delivery.hidden = YES;
            view_for_note.hidden = YES;
            if (IS_IPHONE_6Plus)
            {
                [scroll setContentSize:CGSizeMake(0,700)];
            }
            else if (IS_IPHONE_6)
            {
                [scroll setContentSize:CGSizeMake(0,700)];
            }
            else
            {
                [scroll setContentSize:CGSizeMake(0,600)];
            }
        }
        else if (indexPath.row == 6)
        {
            img_strip.hidden = YES;
            img_strip1.hidden =YES;
            img_strip2.hidden = YES;
            img_strip3.hidden = NO;
            
            view_personal.hidden = YES;
            view_for_kitchen.hidden = YES;
            view_for_dine_in.hidden = YES;
            view_for_menu.hidden = YES;
            view_for_schedule.hidden = YES;
            view_for_schedule_items.hidden = YES;
            view_for_reviews.hidden = YES;
            view_for_delivery.hidden = NO;
            view_for_note.hidden = YES;
            
            if (IS_IPHONE_6Plus)
            {
                [scroll setContentSize:CGSizeMake(0,1100)];
            }
            else if (IS_IPHONE_6)
            {
                [scroll setContentSize:CGSizeMake(0,1100)];
            }
            else
            {
                [scroll setContentSize:CGSizeMake(0,1000)];
            }
            
            
        }
        else if (indexPath.row == 7)
        {
            img_strip.hidden = YES;
            img_strip1.hidden =YES;
            img_strip2.hidden = YES;
            img_strip3.hidden = NO;
            
            view_personal.hidden = YES;
            view_for_kitchen.hidden = YES;
            view_for_dine_in.hidden = YES;
            view_for_menu.hidden = YES;
            view_for_schedule.hidden = YES;
            view_for_schedule_items.hidden = YES;
            view_for_reviews.hidden = YES;
            view_for_delivery.hidden = YES;
            view_for_note.hidden = NO;
            if (IS_IPHONE_6Plus)
            {
                [scroll setContentSize:CGSizeMake(0,800)];
            }
            else if (IS_IPHONE_6)
            {
                [scroll setContentSize:CGSizeMake(0,800)];
            }
            else
            {
                [scroll setContentSize:CGSizeMake(0,600)];
            }
            
        }
        
        selectedindex= (int)indexPath.row;
        
        if (indexPath.row==selectedindex)
        {
            indexSelected = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
            
        }
        
        [collView_for_icons reloadData];
        
    }
}


#pragma  click events

-(void)profilepic_clickbtn:(UIButton *) sender
{
    str_pickingImage = @"Profile";
    
    UIActionSheet *actionSheet_popupQuery = [[UIActionSheet alloc] initWithTitle:@"Select A Image From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Camera", nil];
    actionSheet_popupQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet_popupQuery showInView:self.view];
    
}

- (void)localeDidChange {
    [self.calendar setLocale:[NSLocale currentLocale]];
}

- (BOOL)dateIsDisabled:(NSDate *)date
{
    for (NSDate *disabledDate in self.enabledDates)
    {
        if ([disabledDate isEqualToDate:date])
        {
            return YES;
        }
    }
    return NO;
}
- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)myDate
{
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    str_SelectedDate = [format stringFromDate:myDate];
    
    [self schedulelist];
    
    
}


#pragma mark - ACTION SHEET DELEGATE

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 0)
    {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            UIImagePickerController*imgPicker = [[UIImagePickerController alloc] init];
            UIColor* color = [UIColor colorWithRed:46.0/255 green:127.0/255 blue:244.0/255 alpha:1];
            [imgPicker.navigationBar setTintColor:color];
            imgPicker.delegate = self;
            imgPicker.allowsEditing = YES;
            imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            [self presentViewController:imgPicker animated:NO completion:Nil];
        }
    }
    else if(buttonIndex == 1)
    {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController*imgPicker = [[UIImagePickerController alloc] init];
            UIColor* color = [UIColor colorWithRed:46.0/255 green:127.0/255 blue:244.0/255 alpha:1];
            [imgPicker.navigationBar setTintColor:color];
            imgPicker.delegate = self;
            imgPicker.allowsEditing = YES;
            imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:imgPicker animated:NO completion:Nil];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Not86" message:@"Device Does Not Support Camera" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
}

#pragma mark - IMAGEPICKER DELEGATE METHODS
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *imageOriginal =  [info objectForKey:UIImagePickerControllerEditedImage];
    
    
    imgData =  UIImageJPEGRepresentation(imageOriginal,0.5);
    chef_img.layer.cornerRadius = 80/2;
    chef_img.image = imageOriginal;
    
    
    
    
    
    
    [picker dismissViewControllerAnimated:YES completion:Nil];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:Nil];
}


#pragma mark - Date Picker
- (void)datePickerValueChanged:(id)sender
{
    [formatter setDateFormat:@"dd/MM/yyyy"];
    //    @"d  d   m  m   y  y  y  y"
    NSString *str=[formatter stringFromDate:datePicker.date];
    
    
    if(IS_IPHONE_6Plus)
    {
        str_DOBfinal = [NSString stringWithFormat:@"  %@     %@          %@     %@        %@      %@     %@      %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
        
    }
    else if(IS_IPHONE_6)
    {
        str_DOBfinal = [NSString stringWithFormat:@"  %@    %@        %@     %@        %@     %@    %@      %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
    }
    else if (IS_IPHONE_5)
    {
        str_DOBfinal = [NSString stringWithFormat:@" %@    %@        %@     %@      %@     %@    %@    %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
    }
    else
    {
        str_DOBfinal = [NSString stringWithFormat:@" %@   %@      %@    %@      %@   %@   %@   %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
    }
    
    //    str_DOBfinal = [NSString stringWithFormat:@"  %@     %@          %@     %@        %@      %@     %@      %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
    //    NSLog(@"fdyhsag %@",str_DOBfinal);
    
    txt_date_of_birth.font = [UIFont fontWithName:kFont size:14];
    txt_date_of_birth.textColor = [UIColor blackColor];
    
    NSLog(@"gdj%@", [str substringToIndex:2]);
    
    [txt_date_of_birth setText:str_DOBfinal];
    
}

-(void) click_DoneDate:(id) sender
{
    [formatter setDateFormat:@"dd/MM/yyyy"];
    //    @"d  d   m  m   y  y  y  y"
    NSString *str=[formatter stringFromDate:datePicker.date];
    
    if(IS_IPHONE_6Plus)
    {
        str_DOBfinal = [NSString stringWithFormat:@"  %@     %@          %@     %@        %@      %@     %@      %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
        
    }
    else if(IS_IPHONE_6)
    {
        str_DOBfinal = [NSString stringWithFormat:@"  %@    %@        %@     %@        %@     %@    %@      %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
    }
    else if (IS_IPHONE_5)
    {
        str_DOBfinal = [NSString stringWithFormat:@" %@    %@        %@     %@      %@     %@    %@    %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
    }
    else
    {
        str_DOBfinal = [NSString stringWithFormat:@" %@   %@      %@    %@      %@   %@   %@   %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
    }
    
    //    str_DOBfinal = [NSString stringWithFormat:@"  %@     %@          %@     %@        %@      %@     %@      %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
    //    NSLog(@"fdyhsag %@",str_DOBfinal);
    
    txt_date_of_birth.font = [UIFont fontWithName:kFont size:14];
    txt_date_of_birth.textColor = [UIColor blackColor];
    
    NSLog(@"gdj%@", [str substringToIndex:2]);
    
    [txt_date_of_birth setText:str_DOBfinal];
    [self.view endEditing:YES];
}






-(void)btn_back_arrow_click:(UIButton *)sender
{
    NSLog(@"btn_back_arrow_click:");
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
}
-(void)btn_img_user_click:(UIButton *)sender
{
    NSLog(@"btn_img_user_click:");
}

-(void)btn_on_edite_click:(UIButton *)sender
{
    NSLog(@"btn_img_pencil_click:");
    
    ChefEditProfileVC * obj = [ChefEditProfileVC new];
    [self presentViewController:obj animated:NO completion:nil];
    
    
}
-(void)btn_on_icons_click:(UIButton *)sender
{
    NSLog(@"btn_on_icons_click:");
}
//-(void)icon_right_arrow_BtnClick:(UIButton * )sender
//{
//    NSLog(@"icon_right_arrow_BtnClick:");
//    view_for_schedule.hidden = YES;
//    view_for_schedule_items.hidden = NO;
//}
-(void)click_on_add_schedule_btn:(UIButton *)sender
{
    NSLog(@"click_on_add_schedule_btn:");
}
-(void)click_on_left_arrow:(UIButton *)sender
{
    NSLog(@"click_on_left_arrow:");
    view_for_schedule.hidden = NO;
    view_for_schedule_items.hidden = YES;
}
//click actions for dish items
-(void)click_on_seve_icon:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
}
-(void)click_on_halal:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
}
-(void)click_on_cow_icon:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
}
-(void)click_on_icon_fronce:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
}
-(void)clik_on_take_out:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
}
-(void)click_on_delivery:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
}
-(void)click_on_serving_now_icon:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
}
-(void)btn_chef_menu_click:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
}
-(void)click_on_thumb_icon:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
}
-(void)click_on_edite_btn:(UIButton *)sender
{
    NSLog(@"click_on_edite_btn");
}
-(void)click_on_delete_btn:(UIButton *)sender
{
    NSLog(@"click_on_delete_btn");
}
-(void)click_on_right_arrow:(UIButton *)sender
{
    NSLog(@"click_on_right_arrow");
}
-(void)click_on_write_note_btn:(UIButton *)sender
{
    NSLog(@"click_on_write_note_btn");
}

#pragma click events for edit chef profile

-(void)click_on_drop_down:(UIButton *)sender
{
    NSLog(@"click_on_drop_down");
    table_for_chef_type.hidden = NO;
}
-(void)click_on_calender_down:(UIButton *)sender
{
    NSLog(@"click_on_calender_down");
}
-(void)click_on_cuntry_codedrop_down:(UIButton *)sender
{
    NSLog(@"click_on_cuntry_codedrop_down");
    
    table_for_cuntrycode.hidden=YES;
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        table_for_cuntrycode.hidden=NO;
        
        
    }
    else
    {
        [sender setSelected:NO];
        table_for_cuntrycode.hidden=YES;
    }
    
    
}
-(void)click_on_cuntry_drop_down:(UIButton *)sender
{
    NSLog(@"click_on_cuntry_drop_down");
    table_for_cuntry_name.hidden=YES;
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        table_for_cuntry_name.hidden=NO;
        
        
    }
    else
    {
        [sender setSelected:NO];
        table_for_cuntry_name.hidden=YES;
    }
    
    
}
-(void)click_on_city_drop_down:(UIButton *)sender
{
    NSLog(@"click_on_city_drop_down");
    
    table_for_city_name.hidden=YES;
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        table_for_city_name.hidden=NO;
        
        
    }
    else
    {
        [sender setSelected:NO];
        table_for_city_name.hidden=YES;
    }
    
    
    
    
}
-(void)btn_on_check_box_click:(UIButton *)sender
{
    NSLog(@"btn_singel_dish_click:");
    
    ABC=(int)sender.tag;
    
    if([ array_temp containsObject:[NSString stringWithFormat:@"%d",ABC]])
    {
        [ array_temp removeObject:[NSString stringWithFormat:@"%d",ABC]];
        
        if (sender.tag==54)
        {
            
            [click_on_dine_in setImage:[UIImage imageNamed:@"icon-un-selected-check@2x.png"] forState:UIControlStateNormal];
            
        }
    }
    else
    {
        [ array_temp addObject:[NSString stringWithFormat:@"%d",ABC]];
        if (sender.tag==54)
        {
            [ click_on_dine_in setImage:[UIImage imageNamed:@"icon-selected-check@2x.png"] forState:UIControlStateNormal];
            
            
        }
    }
    
}

-(void)click_on_radio_btn:(UIButton *)sender
{
    NSLog(@"click_on_radio_btn:");
}
-(void)click_on_drop_down4:(UIButton *)sender
{
    NSLog(@"click_on_drop_down4");
    
    
}
-(void)click_on_drop_down5:(UIButton *)sender
{
    NSLog(@"click_on_drop_down4");
}
-(void)click_on_white_cross_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_white_edite_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_radio_btn_in_parking_yes:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_radio_btn_in_praking_no:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_update_btn:(UIButton *)sender
{
    NSLog(@"click_on_update_btn");
}
-(void)click_on_tel_code_drop_down:(UIButton *)sender
{
    NSLog(@"click_on_tel_code_drop_down");
}
-(void)click_on_tel_code_drop_down2:(UIButton *)sender
{
    NSLog( @"click_on_tel_code_drop_down2");
}
-(void)click_on_radio_btn_for_delivery_charge:(UIButton *)sender
{
    NSLog(@"click_on_radio_btn_for_delivery_charge");
}
-(void)click_on_radio_btn_for_delivery_charge2:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_check_box_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_pluse_btn:(UIButton *)sender
{
    NSLog(@"click_on_pluse_btn");
}
-(void)click_on_update_indelivery_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_update_innote_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void) btnYes_Parking:(UIButton *) sender
{
    img_radio_btn_on_no.image=[UIImage imageNamed:@"icon-un-selec-radio@2x.png"];
    
    if([sender isSelected])
    {
        [sender setSelected:NO];
        str_status=@"YES";
        img_radio_btn_on_yes.image=[UIImage imageNamed:@"icon-sele-radio@2x.png"];
    }
    else
    {
        
        [sender setSelected:YES];
        img_radio_btn_on_yes.image=[UIImage imageNamed:@"icon-un-selec-radio@2x.png"];
    }
    
}
-(void) btnNo_Parking:(UIButton *) sender

{
    img_radio_btn_on_yes.image=[UIImage imageNamed:@"icon-un-selec-radio@2x.png"];
    
    if([sender isSelected])
    {
        [sender setSelected:NO];
        str_status=@"NO";
        img_radio_btn_on_no.image=[UIImage imageNamed:@"icon-sele-radio@2x.png"];
    }
    else
    {
        [sender setSelected:YES];
        img_radio_btn_on_no.image=[UIImage imageNamed:@"icon-un-selec-radio@2x.png"];
    }
    
}



-(void)btn_FirstyearClick:(UIButton *)sender
{
    
    
    [radio_button2 setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [radio_button3 setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [radio_button4 setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    
    
    
    if([sender isSelected])
    {
        [radio_button setSelected:YES];
        [sender setSelected:NO];
        str_status=@"1-5";
        [radio_button setImage:[UIImage imageNamed:@"button img .png"] forState:UIControlStateNormal];
        
    }
    else
    {
        [radio_button setSelected:NO];
        [sender setSelected:YES];
        [radio_button setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
        
    }
}
-(void)btn_sixyearClick:(UIButton *)sender
{
    [radio_button setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [radio_button3 setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [radio_button4 setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    
    if([sender isSelected])
    {
        [radio_button2 setSelected:YES];
        [sender setSelected:NO];
        str_status=@"6-10";
        [radio_button2 setImage:[UIImage imageNamed:@"button img .png"] forState:UIControlStateNormal];
        
    }
    else
    {
        
        
        [radio_button2 setSelected:NO];
        
        [sender setSelected:YES];
        [radio_button2 setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
        
    }
    
    
}
-(void)btn_ElevyearClick:(UIButton *)sender{
    [radio_button setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [radio_button setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [radio_button4 setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    
    
    if([sender isSelected])
    {
        [radio_button3 setSelected:YES];
        [sender setSelected:NO];
        str_status=@"11-20";
        [radio_button3 setImage:[UIImage imageNamed:@"button img .png"] forState:UIControlStateNormal];
        
    }
    else
    {
        
        
        [radio_button3 setSelected:NO];
        
        [sender setSelected:YES];
        [radio_button3 setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
        
    }
    
    
}
-(void)btn_moreyearClick:(UIButton *)sender
{
    [radio_button setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [radio_button2 setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    [radio_button3 setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
    
    
    if([sender isSelected])
    {
        [radio_button4 setSelected:YES];
        [sender setSelected:NO];
        
        str_status=@"more than 20";
        [radio_button4 setImage:[UIImage imageNamed:@"button img .png"] forState:UIControlStateNormal];
        
    }
    else
    {
        
        
        [radio_button4 setSelected:NO];
        
        [sender setSelected:YES];
        [radio_button4 setImage:[UIImage imageNamed:@"button img 2.png"] forState:UIControlStateNormal];
        
    }
    
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}







// #pragma ChefEditeProfile

//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
//{
//
//    UIImage *imageOriginal =  [info objectForKey:UIImagePickerControllerEditedImage];
//
//    if ( [[NSString stringWithFormat:@"%@",str_pickingImage]isEqualToString:@"Profile"])
//    {
//        imgData =  UIImageJPEGRepresentation(imageOriginal,0.5);
//        chef_img.image = imageOriginal;
//
//    }
//
//    [picker dismissViewControllerAnimated:YES completion:Nil];
//
//}
//
#pragma edite_profile functionality

-(void)ChefEditeProfile
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    // [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"]
    NSDictionary *params =@{
                            
                            @"uid"                                          : [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"registration_page"                           : @"0",
                            //                            @"field_first_name"                           : txt_first_name.text,
                            //                            @"field_middle_name"                          : txt_middle_name.text,
                            //                            @"field_last_name"                            : txt_last_name.text,
                            //
                            //                            @"field_chef_type"                            : txt_chef_type.text,
                            //                            @"field_date_of_birth"                        : txt_date_of_birth.text,
                            //                            @"field_national_id"                          : txt_social_id_number.text,
                            //                            @"field_email_address"                        : txt_email_address.text,
                            //
                            //                            @"field_mobile_code_number"                   : txt_cuntry_tel_code.text,
                            //                            @"field_mobile_number"                        : txt_mobile_number.text,
                            //
                            //                            @"home_address"                               :  txt_home_address.text,
                            //                            @"hcountry_name"                              :  txt_cuntry.text,
                            //                            @"hcity_name"                                 :  txt_city.text,
                            //                            @"hpostal_code"                               :  txt_pin_code.text,
                            //
                            //                            @"field_choose_serving_type"                  :  @"",
                            //                            @"field_formal_cooking_qualificati"           :  txt_cooking_qualification.text,
                            //
                            //
                            //                            @"field_years_of_cooking"                     :@"",
                            //                            @"field_if_yes_state_qualification"           :@"",
                            
                            
                            //                            @"field_paypal_email_address"                 : txt_paypal_account.text,
                            //                            @"recieve_my_payment_from"                    : txt_payment_receiverd_month.text,
                            //  @"field_about_yourself"                       :@"",
                            //
                            //
                            //
                            //                            //============================ paramiters for kitchrninfo  ================
                            //
                            //                            @"field_kitchen_name"                         :  txt_kitchen_name.text,
                            //                            @"kitchen_address"                            :  txt_kitchen_address.text,
                            //                            @"kcountry_name"                              :  txt_cuntry_name.text,
                            //                            @"kcity_name"                                 :  txt_city_name.text,
                            //                            @"kpostal_code"                               :  txt_cuntry_pin_code.text,
                            ////                            @"video_url"                                  :@"",
                            ////                            @"kitchen_img"                                :@"",
                            ////
                            //                            @"field_store_food_description"               : txt_view_for_storage_description.text,
                            //                            @"field_hygiene_standard_descripti"           : txt_view_for_hygiene_description.text,
                            //
                            //
                            //
                            //
                            //                            //============================ parameters for Dine-in-area ==================
                            //
                            //                          //  @"dinein_img"                                 :@"",
                            //                            @"field_dine_in_seats"                        : txt_no_of_seats_val.text,
                            //                            @"field_availlable_parking"                   : text_parking_val.text,
                            //
                            
                            //==============================parameters for delivery info ===================
                            
                            
                            //============================parameters for note ============================
                            //  @""                                              : dinein_img.text,
                            
                            
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    //    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:@"webservices/add-user-profile.json"  parameters:params];
    
    
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSignUpFirst
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseUserProfileFavorites:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error){
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                       [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self ChefEditeProfile];
                                         }
                                     }];
    [operation start];
    
}
-(void)ResponseUserProfileFavorites:(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[TheDict valueForKey:@"profile_info"]  forKey:@"UserInfo"];
        [defaults synchronize];
        
        NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"ProfileImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [chef_img setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@"img_profile@2x.png"]];
        [chef_img setContentMode:UIViewContentModeScaleAspectFill];
        [chef_img setClipsToBounds:YES];
        chef_img.layer.cornerRadius= 90/2;
        
        txt_first_name.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"FirstName"];
        
        NSLog(@"hfv %@",[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"FirstName"]);
        
        NSLog(@"hf bfhv %@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"]);
        
        
        txt_middle_name.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"MiddleName"];
        txt_last_name.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"LastName"];
        txt_chef_type.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Chef_Type"];
        
        txt_date_of_birth.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"DateOfBirth"];
        txt_social_id_number.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"ational_id"];
        txt_email_address.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Email_Address"];
        txt_cuntry_tel_code.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Mobile_Code_Number"];
        txt_mobile_number.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Mobile_Number"];
        txt_home_address.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"HomeStreet"];
        txt_city.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"HomeCity"];
        txt_cuntry.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"HomeCountry"];
        txt_pin_code.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"HomePostalCode"];
        
        txt_cooking_qualification.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Cooking_Qualification"];
        //  txt_first_name.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Cooking_Experience"];
        
        
        txt_paypal_account.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Paypal_Account"];
        
        txt_payment_receiverd_month.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Payment_Recieved"];
        
        
        
        
        txt_view_for_chef_description.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"About_Us"];
        
        //============================= KITCHEN INFO ============================================
        
        txt_kitchen_name.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Kitchen_info"] valueForKey:@"Kitchen_Name"];
        txt_kitchen_address.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Kitchen_info"] valueForKey:@"Kitchen_street_Address"];
        txt_city_name.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Kitchen_info"] valueForKey:@"Kitchen_city"];
        txt_cuntry_name.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Kitchen_info"] valueForKey:@"Kitchen_country"];
        txt_cuntry_pin_code.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Kitchen_info"] valueForKey:@"Kitchen_postal_code"];
        
        //  txt_kitchen_name.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Kitchen_info"] valueForKey:@"KitchenVideo"];
        // txt_kitchen_name.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Kitchen_info"] valueForKey:@"Videothumb_url"];
        
        txt_kitchen_name.text =[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"KitchenImages"];
        
        
        
        txt_view_for_storage_description.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Kitchen_info"] valueForKey:@"Food_Storage"];
        txt_view_for_hygiene_description.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Kitchen_info"] valueForKey:@"Kitchen_Hygien_Standard."];
        
        //======================================== Dine-in-area =====================================================
        
        
        //    txt_kitchen_name.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Kitchen_info"] valueForKey:@"Dine_In_Pictures"];
        txt_no_of_seats_val.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Kitchen_info"] valueForKey:@"Number_Of_Seats"];
        //    txt_kitchen_name.text =[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Kitchen_info"] valueForKey:@"Parking_Available"];
        
        //========================================== Delivery_Info =================================
        
        
        
        
        
        //        if ([str_favorite_type isEqualToString:@"Dish"])
        //        {
        //
        //
        //
        //            NSArray *array = [TheDict objectForKey:@"Item_Information"];
        //
        //            NSLog(@"array %@",array);
        //
        //            for (NSDictionary *dict in array) {
        //                [ary_itemsinformation addObject:dict];
        //
        //            }
        //
        //            NSLog(@"ary_itemsinformation %@",array);
        //
        //
        //
        //
        //
        //
        //            //  [ary_itemsinformation addObject:[TheDict valueForKey:@"Item_Information"]];
        //            //            for (int i=0; i<[[TheDict valueForKey:@"Item_Information"] count]; i++)
        //            //            {
        //            //
        //            //                [ary_itemsinformation addObject:[[TheDict valueForKey:@"Item_Information"]objectAtIndex:i]];
        //            //            }
        //
        //
        //            //            for (int j=0; j<[[[TheDict valueForKey:@"Item_Information"] valueForKey:@"DietaryRestrictions"] count]; j++)
        //            //            {
        //            ////                               [array_items_name addObject:[[[TheDict valueForKey:@"Item_Information"] valueForKey:@"DietaryRestrictions"] objectAtIndex:j]];
        //            //            }
        //
        //
        //
        //            [table_items_in_favorites reloadData];
        //
        //        }
        //        else{
        //
        //            NSArray *array = [TheDict objectForKey:@"Chef_Information"];
        //
        //            NSLog(@"array %@",array);
        //
        //            for (NSDictionary *dict in array) {
        //                [ary_Chefinformation addObject:dict];
        //
        //            }
        //
        //            NSLog(@"ary_Chefinformation %@",array);
        //
        //
        //            //            for (int i=0; i<[[TheDict valueForKey:@"Chef_Information"] count]; i++)
        //            //            {
        //            //
        //            //               [ary_Chefinformation addObject:[[TheDict valueForKey:@"Chef_Information"]objectAtIndex:i]];
        //            //            }
        //
        //            //           [ary_Chefinformation addObject:[TheDict valueForKey:@"Chef_Information"]];
        //            //            for (int j=0; j<[[[TheDict valueForKey:@"Chef_Information"] valueForKey:@"DietaryRestrictions"] count]; j++)
        //            //            {
        //            //                [array_chef_name addObject:[[[TheDict valueForKey:@"Chef_Information"] valueForKey:@"DietaryRestrictions"] objectAtIndex:j]];
        //            //
        //            //            }
        //
        //            [table_for_chef_in_favorites reloadData];
        //
        //
        //        }
        //
        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
        
        //    }
        //    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
        //    {
        //        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        //
        //
    }
    
}

# pragma mark Hcountry method

-(void)countryList
{
    
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpHcountry
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [self ResponseChefSignUpHcountry:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self countryList];
                                         }
                                     }];
    [operation start];
    
}

-(void) ResponseChefSignUpHcountry :(NSDictionary * )TheDict
{
    [arrayCountry removeAllObjects];
    [ary_countrycodelist removeAllObjects];
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        
        
        for (int i=0; i<[[TheDict valueForKey:@"CountryMobileCode"] count]; i++)
        {
            [ary_countrycodelist addObject:[[TheDict valueForKey:@"CountryMobileCode"] objectAtIndex:i]];
            
        }
        for (int i=0; i<[[TheDict valueForKey:@"Country_CityList"] count]; i++)
        {
            [arrayCountry addObject:[[TheDict valueForKey:@"Country_CityList"] objectAtIndex:i]];
            
        }
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        txt_cuntry_tel_code.text=@"";
        txt_cuntry_name.text =@"";
        
        
    }
    
    NSMutableString*str_servetype;
    str_servetype = [NSMutableString new];
    
    for (int i=0; i<[ary_servingtype count]; i++)
    {
        
        
        if (i==[ary_servingtype count]-1)
        {
            [str_servetype  appendString:[NSString stringWithFormat:@"%@",[[ary_servingtype objectAtIndex:i] valueForKey:@"Serve_name"]]];
        }
        else
        {
            [str_servetype  appendString:[NSString stringWithFormat:@"%@,",[[ary_servingtype objectAtIndex:i] valueForKey:@"Serve_name"]]];
        }
        
        
    }
    
    txt_serving_type.text = str_servetype;
    
    
    [table_for_cuntrycode reloadData];
    [table_for_cuntry_name reloadData];
    
}



# pragma mark Dishlist method

-(void)Dishlist
{
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    NSDictionary *params =@{
                            
                            
                            @"page_number"            :  @"1",
                            @"uid"            : [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:Kdishlist
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [self ResponseDishlist:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
         
         if([operation.response statusCode] == 406){
             
             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
             return;
         }
         
         if([operation.response statusCode] == 403){
             NSLog(@"Upload Failed");
             return;
         }
         if ([[operation error] code] == -1009) {
             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                          message:@"Please check your internet connection"
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
             [av show];
         }
         else if ([[operation error] code] == -1001) {
             
             NSLog(@"Successfully Registered");
             [self Dishlist];
         }
     }];
    [operation start];
    
}

-(void) ResponseDishlist :(NSDictionary * )TheDict
{
    [ary_dishlist removeAllObjects];
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        for (int i=0; i<[[TheDict valueForKey:@"DishsList"] count]; i++)
        {
            [ary_dishlist addObject:[[TheDict valueForKey:@"DishsList"] objectAtIndex:i]];
            
        }
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        
    }
    
    [collView_for_menu_items reloadData];
    
}


# pragma mark Dishlist method

-(void)Reviews
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    NSDictionary *params =@{
                            
                            
                            @"chef_id"            : [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"uid"            :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:Kreviews
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [self ResponseReviews:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self Reviews];
                                         }
                                     }];
    [operation start];
    
}

-(void) ResponseReviews :(NSDictionary * )TheDict
{
    [ary_reviews removeAllObjects];
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        [ary_reviews addObject:[TheDict valueForKey:@"ChefDetails"]];
        
        
        
        
        label_favorite_value.text = [NSString stringWithFormat:@"%@",[[ary_reviews objectAtIndex:0] valueForKey:@"favorite_by_user"]];
        label_total_reviews_value.text = [NSString stringWithFormat:@"%@",[[ary_reviews objectAtIndex:0] valueForKey:@"total_revies"]];
        
        label_overall_rating_value.text = [NSString stringWithFormat:@"%@",[[ary_reviews objectAtIndex:0] valueForKey:@"overall_rating"]];
        taste_value.text = [NSString stringWithFormat:@"%@ ",[[ary_reviews objectAtIndex:0] valueForKey:@"taste_rating"]];
        appeal_value.text = [NSString stringWithFormat:@"%@",[[ary_reviews objectAtIndex:0] valueForKey:@"appeal_rating"]];
        value_value.text = [NSString stringWithFormat:@"%@",[[ary_reviews objectAtIndex:0] valueForKey:@"value_rating"]];
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        
    }
    
    [collView_for_menu_items reloadData];
    
}



# pragma mark Dishlist method

-(void)schedulelist
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    self.view.userInteractionEnabled = NO;
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    NSDictionary *params =@{
                            
                            
                            @"date"            :  str_SelectedDate,
                            @"uid"            :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"] ,
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:Kschedulelistcook
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        self.view.userInteractionEnabled = YES;
        
        [self Responseschedulelist:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         self.view.userInteractionEnabled = YES;
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self schedulelist];
                                         }
                                     }];
    [operation start];
    
}

-(void) Responseschedulelist :(NSDictionary * )TheDict
{
    [ary_scedulelist removeAllObjects];
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        
        for (int i=0; i<[[TheDict valueForKey:@"DishDate_Time"] count]; i++)
        {
            [ary_scedulelist addObject:[[TheDict valueForKey:@"DishDate_Time"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        
    }
    
    [table_for_schedule reloadData];
    
}





//-(void)click_Done
//{
//    [Mobilenumber resignFirstResponder];
//    [txt_portal resignFirstResponder];
//
//}
//






# pragma mark Hcity method

-(void)AFcity_list
{
    
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            
                            
                            @"country_name"            :  txt_cuntry.text,
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpHcity
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [self ResponseCity_list:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFcity_list];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseCity_list :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    [ary_citylist removeAllObjects];
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        for (int i=0; i<[[TheDict valueForKey:@"City_List"] count]; i++)
        {
            [ary_citylist addObject:[[TheDict valueForKey:@"City_List"] objectAtIndex:i]];
        }
        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        txt_city.text=@"";
        
        
    }
    
    [table_for_city_name reloadData];
}










/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
