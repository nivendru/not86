//
//  ChefMyProfileVC.h
//  Not86
//
//  Created by Interwld on 9/5/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"
#import "CKCalendarView.h"

@interface ChefMyProfileVC : JWSlideMenuViewController<CKCalendarDelegate>
@property(nonatomic, strong) CKCalendarView *calendar;
@property(nonatomic, strong) NSMutableArray *enabledDates;
@end
