//
//  ChefServeTypeVC.m
//  Not86
//
//  Created by Pavan on 25/05/2015 SAKA.
//  Copyright (c) 1937 Saka com.interworld. All rights reserved.
//

#import "ChefServeLaterSecondVC.h"
#import "CKCalendarView.h"


typedef void (^CompletionBlock)(BOOL success);

@interface ChefServeLaterSecondVC ()<UITextFieldDelegate, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource, CKCalendarDelegate, UIGestureRecognizerDelegate >
{
    
    UIImageView *imgViewTop;
    UIScrollView *scroll;
    
    UIButton *btn_Dine_In;
    UIButton *btn_Takeout;
    UIButton *btn_Delivery;
    UIButton *btn_SelectAll;
    UIButton *btn_ServeNow;
    
    UITableView *table_DishList;
    UITableView *table_Preview;
    
    int selectedSection;
    
    UIButton *section_Btn;
    UIImageView *imgCheck1;
    UIImageView *imgCheck2;
    UIImageView *imgCheck3;

    
    UIImageView *imgTik1;
    UIImageView *imgTik2;
    UIImageView *imgTik3;
    
    UIButton *repeat_Btn;
    UITableView *table_Selected_Date;
    UIButton *removeCell_Btn;
    UITableView *table_Repeated;
    NSArray *repeat_Arr;
    UILabel *repeatLbl;
    
    UIImageView *img_DelieveryBg;
    UIView *view_for_delivery;
    UITableView *table_for_delevery;
    
    NSMutableArray *array_delivery_company_name;
    NSMutableArray *array_company_phone;
    NSMutableArray *array_del_person_name;
    NSMutableArray *array_del_persone_numer;
    UIImageView *img_bg_for_delevery;
    
    UITableView *table_for_contry_code1;
    UITableView *table_for_contry_code2;
    NSMutableArray *arrayCountry;
    
    NSMutableArray *ary_Delivery_Address;
    
    NSString *str_Delivery_Address;
    
    UIButton *btn_on_tel_code_dropdown;
    UIButton *icon_dropdown_for_tel_code2;
    UIButton *btn_on_tel_code_dropdown2;
    UIButton *radio_button_for_delivery_charge;
    UIButton *radio_button_for_delivery_charge2;
    UIButton *btn_check_box_for_Apply_All;
    UIButton *btn_check_box_for_setas_default;
    
    UITextField *txt_tel_code;
    UITextField *txt_tel_code2;
    UIButton *btn_Save_Deliverly_Address;
    UIView *DeliveryPopUpBg;
    NSString*str_dineinbody;
    NSString*str_takoutbody;
    NSString*str_divarybody;
    
    
    NSMutableArray*ary_delivaries;
    NSMutableArray*ary_temp;
    int selectedindex;
    NSIndexPath*indexpath;
    
    AppDelegate*delegate;
    NSMutableArray*ary_mainarytosave;
    
    NSString*str_dishID;
    NSString*str_quantity;
    NSString *str_servingtypes;
    
    int str_selectedsectiontocompare;
    
    NSMutableArray*ary_maintosave;
    
    
    NSMutableArray*ary_delivary;
    
    //delivary
    
    UITextField *txt_delevery_company;
    UITextField *txt_mobile_number2;
    UITextField *txt_persone_name;
    UITextField *txt_person_mobile_number;
    UITextField *txt_first_mile_charge;
    UITextField *txt_each_mile_charge;
    UITextField *txt_flat_charge;
    
    NSString*str_delivarypopdefault;
    NSString*str_delivarypopselectall;
    UIView*alertviewBg;
    UIButton*bth_cross;
    
    
    

}

@end

@implementation ChefServeLaterSecondVC
@synthesize calendar,enabledDates,ary_servelisting,str_time,ary_mainfromWS;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self IntegrateBodyDesign];
    [self IntegrateHeaderDesign];
    
    str_dineinbody = [NSString new];
    str_takoutbody = [NSString new];
    str_divarybody = [NSString new];
    str_delivarypopdefault =[NSString new];
    str_delivarypopselectall =[NSString new];
    
    str_delivarypopdefault = @"0";
    str_delivarypopselectall = @"0";
    
    str_dineinbody = @"0";
    str_takoutbody = @"0";
    str_divarybody = @"0";
    
    selectedSection = 0;

    selectedindex= -1;
    str_selectedsectiontocompare = 0;
    indexpath= nil;
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ary_delivary= [NSMutableArray new];
    ary_delivary =[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Delivery_Address"];
    
    ary_maintosave= [NSMutableArray new];
    
    
    ary_delivaries = [[NSMutableArray alloc]initWithObjects:@"img-dine-in@2x",@"img-take-out@2x",@"img-delever@2x", nil];
    ary_temp= [NSMutableArray new];
    ary_mainarytosave= [NSMutableArray new];
    selectedSection = 0;

    
    repeat_Arr = [[NSArray alloc]initWithObjects:@"Don’t repeat (default)",@"Every day this week", @"Every day this month",@"Same day every week",@"Certain dates only", nil];
    
    array_delivery_company_name = [[NSMutableArray alloc]initWithObjects:@"Timothy's Delivery",@"thom's delivery",@"jon's delivery",@"Timothy's Delivery",nil];
    
    ary_Delivery_Address  = [[NSMutableArray alloc]initWithObjects:@"Timothy's Delivery",@"thom's delivery",@"jon's delivery",@"Timothy's Delivery",@"Add Other Address",nil];
    
    array_company_phone = [[NSMutableArray alloc]initWithObjects:@"12345678",@"87456123",@"12354678",@"15236478", nil];
    array_del_person_name = [[NSMutableArray alloc]initWithObjects:@"jon",@"tom",@"robert",@"smith",nil];
    array_del_persone_numer = [[NSMutableArray alloc]initWithObjects:@"233454566",@"233454566",@"233454566",@"233454566", nil];
    
    str_Delivery_Address = [NSString new];
    
    arrayCountry = [NSMutableArray new];
    
    
    
    for (int i=0; i<[ary_servelisting count]; i++)
    {
        NSMutableDictionary *dict1=[[NSMutableDictionary alloc] init];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",@"0"] forKey:@"delivery"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",@"0"] forKey:@"dinein"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",@"0"] forKey:@"takeout"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",@"0"] forKey:@"delivaryid"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"quantity"]]  forKey:@"quantity"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelisting objectAtIndex:i] valueForKey:@"Delivery_Address"]] forKey:@"Delivery_Address"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",@"0"] forKey:@"delivaryidselected"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",@"NO"] forKey:@"tableshow"];
        
        [ary_maintosave addObject:dict1];
    }
    

    [self countryList];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}
-(void)IntegrateHeaderDesign{
    
    imgViewTop=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,45)];
    [imgViewTop setUserInteractionEnabled:YES];
    imgViewTop.image=[UIImage imageNamed:@"img-head.png"];
    [self.view addSubview:imgViewTop];
    //
    //        UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(10, 18, 25, 25)];
    //        [img_back setUserInteractionEnabled:YES];
    //        img_back.backgroundColor=[UIColor clearColor];
    //        img_back.image=[UIImage imageNamed:@"arrow.png"];
    //        [imgViewTop addSubview:img_back];
    //
    
    
    UIButton *btn_back = [[UIButton alloc] init];
    btn_back.frame = CGRectMake(15, 15, 15,17);
    UIImage *btnimage=[UIImage imageNamed:@"arrow@2x.png"];
    [btn_back setBackgroundImage:btnimage forState:UIControlStateNormal];
    btn_back.backgroundColor = [UIColor clearColor];
    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [imgViewTop addSubview:btn_back];
    

    
    UILabel  * lbl_headingTitle = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(btn_back.frame)+25,15, 100,17)];
    lbl_headingTitle.text = @"Serve Later";
    lbl_headingTitle.backgroundColor=[UIColor clearColor];
    lbl_headingTitle.textColor=[UIColor whiteColor];
    lbl_headingTitle.textAlignment=NSTextAlignmentLeft;
    lbl_headingTitle.font = [UIFont fontWithName:kFont size:17];
    [imgViewTop addSubview:lbl_headingTitle];
    
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 6, 33, 33)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-logo.png"];
    [imgViewTop addSubview:img_logo];
    
    
    
    // Do any additional setup after loading the view.
    
}
-(void)IntegrateBodyDesign
{
    
        [self.view setBackgroundColor:[UIColor colorWithRed:243/255.0f green:245/255.0f blue:244/255.0f alpha:1.0f]];
    
    scroll = [[UIScrollView alloc]init];
    scroll.frame=CGRectMake(0, 45, WIDTH, HEIGHT-125);
    if (IS_IPHONE_6Plus)
    {
        scroll.frame=CGRectMake(0, 45, WIDTH, HEIGHT-135);
    }
    else if (IS_IPHONE_6)
    {
        scroll.frame=CGRectMake(0, 45, WIDTH, HEIGHT-125);
    }
    else if (IS_IPHONE_5)
    {
        scroll.frame=CGRectMake(0, 45, WIDTH, HEIGHT-125);
    }
    else
    {
        scroll.frame=CGRectMake(0, 45, WIDTH, HEIGHT-120);
    }
    scroll.backgroundColor = [UIColor colorWithRed:243/255.0f green:245/255.0f blue:244/255.0f alpha:1.0f];
    scroll.bounces=NO;
//        scroll.layer.borderWidth = 1.0;
    scroll.layer.borderColor = [UIColor blueColor].CGColor;
    scroll.showsVerticalScrollIndicator = YES;
    [scroll setScrollEnabled:YES];
    [self.view addSubview:scroll];
    
    if (IS_IPHONE_6Plus)
    {
        [scroll setContentSize:CGSizeMake(WIDTH, 1360.00)];
    }
    else if (IS_IPHONE_6)
    {
        [scroll setContentSize:CGSizeMake(WIDTH, 1300.00)];
    }
    else if (IS_IPHONE_5)
    {
        [scroll setContentSize:CGSizeMake(WIDTH, 1200.00)];
    }
    else
    {
        [scroll setContentSize:CGSizeMake(WIDTH, 1200.00)];
    }
    
    
    UILabel  * labl_serveingTime = [[UILabel alloc]init ];
    if (IS_IPHONE_6Plus)
    {
        labl_serveingTime.frame=CGRectMake(0,imgViewTop.frame.size.height+20, self.view.frame.size.width,32);
    }
    else if (IS_IPHONE_6)
    {
        labl_serveingTime.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,30);
    }
    else if (IS_IPHONE_5)
    {
        labl_serveingTime.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,25);
    }
    else
    {
        labl_serveingTime.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,20);
    }
    labl_serveingTime.text = @"Serving Time: 03:45 PM - 05:45 PM";
    labl_serveingTime.font = [UIFont fontWithName:kFont size:16];
    

    NSMutableAttributedString *text =
    [[NSMutableAttributedString alloc]
     initWithAttributedString: labl_serveingTime.attributedText];
    [text addAttribute:NSFontAttributeName
                 value:[UIFont fontWithName:kFontBold size:16]
                 range:NSMakeRange(12, 21)];
    [labl_serveingTime setAttributedText: text];
    
    labl_serveingTime.backgroundColor=[UIColor clearColor];
    labl_serveingTime.textAlignment=NSTextAlignmentCenter;
    [scroll addSubview:labl_serveingTime];

    
    
    
    UILabel  * labl_serveNowDate = [[UILabel alloc]init ];
    if (IS_IPHONE_6Plus)
    {
        labl_serveNowDate.frame=CGRectMake(0,CGRectGetMaxY(labl_serveingTime.frame)+10, self.view.frame.size.width,32);
    }
    else if (IS_IPHONE_6)
    {
        labl_serveNowDate.frame=CGRectMake(0,CGRectGetMaxY(labl_serveingTime.frame)+10, self.view.frame.size.width,30);
    }
    else if (IS_IPHONE_5)
    {
        labl_serveNowDate.frame=CGRectMake(0,CGRectGetMaxY(labl_serveingTime.frame)+10, self.view.frame.size.width,25);
    }
    else
    {
        labl_serveNowDate.frame=CGRectMake(0,CGRectGetMaxY(labl_serveingTime.frame)+10, self.view.frame.size.width,20);
    }
    labl_serveNowDate.text = @"Serving Type";
    labl_serveNowDate.font = [UIFont fontWithName:kFontBold size:16];
    labl_serveNowDate.backgroundColor=[UIColor clearColor];
    labl_serveNowDate.textAlignment=NSTextAlignmentCenter;
    [scroll addSubview:labl_serveNowDate];
    
    
    UIImageView *imgViewTimer=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        imgViewTimer.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH+10,130);
    }
    else if (IS_IPHONE_6)
    {
        imgViewTimer.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH+10,120);
    }
    else if (IS_IPHONE_5)
    {
        imgViewTimer.frame=CGRectMake(0, CGRectGetMaxY(labl_serveNowDate.frame),WIDTH,100);
    }
    else
    {
        imgViewTimer.frame=CGRectMake(0, CGRectGetMaxY(labl_serveNowDate.frame),WIDTH,100);
    }
    [imgViewTimer setUserInteractionEnabled:YES];
    imgViewTimer.image=[UIImage imageNamed:@"img_background@2x."];
    [scroll addSubview:imgViewTimer];
    
    
    UIImageView *imgView1=[[UIImageView alloc]init];
    imgView1.frame=CGRectMake(0, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH/3.0, imgViewTimer.frame.size.height-10);
    //    if (IS_IPHONE_6Plus)
    //    {
    //        imgView1.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH+10,130);
    //    }
    //    else if (IS_IPHONE_6)
    //    {
    //        imgView1.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH+10,120);
    //    }
    //    else if (IS_IPHONE_5)
    //    {
    //        imgView1.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame),WIDTH+10,100);
    //    }
    //    else
    //    {
    //        imgView1.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame),WIDTH+10,100);
    //    }
    [imgView1 setUserInteractionEnabled:YES];
    //    imgView1.layer.borderWidth = 1.0;
    [scroll addSubview:imgView1];
    
    
    btn_Dine_In = [[UIButton alloc] init];
    btn_Dine_In.frame = CGRectMake(CGRectGetMinX(imgView1.frame)+35, CGRectGetMinY(imgView1.frame)+20, CGRectGetWidth(imgView1.frame)-50, CGRectGetHeight(imgView1.frame)-30);
    [btn_Dine_In setBackgroundImage:[UIImage imageNamed:@"img-dine-in@2x"] forState:UIControlStateNormal];
    btn_Dine_In.backgroundColor = [UIColor clearColor];
    [btn_Dine_In addTarget:self action:@selector(DineIn_btnClick:) forControlEvents:UIControlEventTouchUpInside ];
    [scroll addSubview:btn_Dine_In];
    
    imgTik1 = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        imgTik1.frame = CGRectMake(43, 42, 42, 30);
    }
    else if (IS_IPHONE_6)
    {
        imgTik1.frame = CGRectMake(37, 37, 38, 27);
    }
    else if (IS_IPHONE_5)
    {
        imgTik1.frame = CGRectMake(30, 27, 30, 23);
    }
    else
    {
        imgTik1.frame = CGRectMake(30, 27, 30, 23);
    }
    imgTik1.backgroundColor = [UIColor clearColor];
    imgTik1.image = [UIImage imageNamed:@""];
    //        imgTik1.layer.borderWidth = 1.0;
    [btn_Dine_In addSubview:imgTik1];
    
    
    
    
    UIImageView *imgView2=[[UIImageView alloc]init];
    imgView2.frame=CGRectMake(WIDTH/3.0, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH/3.0,imgViewTimer.frame.size.height-10);
    [imgView2 setUserInteractionEnabled:YES];
    //    imgView2.layer.borderWidth = 1.0;
    [scroll addSubview:imgView2];
    
    
    btn_Takeout = [[UIButton alloc] init];
    btn_Takeout.frame  = CGRectMake(CGRectGetMinX(imgView2.frame)+25, CGRectGetMinY(imgView2.frame)+18, CGRectGetWidth(imgView2.frame)-50, CGRectGetHeight(imgView2.frame)-30);
    [btn_Takeout setBackgroundImage:[UIImage imageNamed:@"img-take-out@2x"] forState:UIControlStateNormal];
    btn_Takeout.backgroundColor = [UIColor clearColor];
    [btn_Takeout addTarget:self action:@selector(Takeout_btnClick:) forControlEvents:UIControlEventTouchUpInside ];
    [scroll addSubview:btn_Takeout];
    
    
    imgTik2 = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        imgTik2.frame = CGRectMake(43, 43, 42, 30);
    }
    else if (IS_IPHONE_6)
    {
        imgTik2.frame = CGRectMake(37, 38, 38, 27);
    }
    else if (IS_IPHONE_5)
    {
        imgTik2.frame = CGRectMake(30, 28, 30, 23);
    }
    else
    {
        imgTik2.frame = CGRectMake(30, 28, 30, 23);
    }
    imgTik2.backgroundColor = [UIColor clearColor];
    imgTik2.image = [UIImage imageNamed:@""];
    //        imgTik2.layer.borderWidth = 1.0;
    [btn_Takeout addSubview:imgTik2];
    
    
    
    UIImageView *imgView3=[[UIImageView alloc]init];
    imgView3.frame=CGRectMake(2*WIDTH/3.0, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH/3.0,imgViewTimer.frame.size.height-10);
    [imgView3 setUserInteractionEnabled:YES];
    [scroll addSubview:imgView3];
    
    
    
    btn_Delivery = [[UIButton alloc] init];
    btn_Delivery.frame  = CGRectMake(CGRectGetMinX(imgView3.frame)+15, CGRectGetMinY(imgView3.frame)+20, CGRectGetWidth(imgView3.frame)-50, CGRectGetHeight(imgView3.frame)-30);
    [btn_Delivery setBackgroundImage:[UIImage imageNamed:@"img-delever@2x"] forState:UIControlStateNormal];
    btn_Delivery.backgroundColor = [UIColor clearColor];
    [btn_Delivery addTarget:self action:@selector(Delivery_btnClick:) forControlEvents:UIControlEventTouchUpInside ];
    [scroll addSubview:btn_Delivery];
    
    
    imgTik3 = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        imgTik3.frame = CGRectMake(43, 42, 42, 30);
    }
    else if (IS_IPHONE_6)
    {
        imgTik3.frame = CGRectMake(37, 37, 38, 27);
    }
    else if (IS_IPHONE_5)
    {
        imgTik3.frame = CGRectMake(30, 27, 30, 23);
    }
    else
    {
        imgTik3.frame = CGRectMake(30, 27, 30, 23);
    }
    imgTik3.backgroundColor = [UIColor clearColor];
    imgTik3.image = [UIImage imageNamed:@""];
    //imgTik3.layer.borderWidth = 1.0;
    [btn_Delivery addSubview:imgTik3];
    
    
    btn_SelectAll = [[UIButton alloc] init];
    btn_SelectAll.frame = CGRectMake(2*WIDTH/3.0, CGRectGetMaxY(imgViewTimer.frame)+2, 20 , 20);
    [btn_SelectAll setBackgroundImage:[UIImage imageNamed:@"img-check@2x"] forState:UIControlStateNormal];
    btn_SelectAll.backgroundColor = [UIColor clearColor];
    [btn_SelectAll addTarget:self action:@selector(SelectAll_btnClick:) forControlEvents:UIControlEventTouchUpInside ];
    [scroll addSubview:btn_SelectAll];
    
    //img_checkok@2x
    
    UILabel  * labl_ApplyAll = [[UILabel alloc]init ];
    labl_ApplyAll.frame=CGRectMake(CGRectGetMaxX(btn_SelectAll.frame)+5, CGRectGetMaxY(imgViewTimer.frame)+2, WIDTH/3.0-25 , 20);
    //    if (IS_IPHONE_6Plus)
    //    {
    //        labl_ApplyAll.frame=CGRectMake(0,imgViewTop.frame.size.height+20, self.view.frame.size.width,32);
    //    }
    //    else if (IS_IPHONE_6)
    //    {
    //        labl_ApplyAll.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,30);
    //    }
    //    else if (IS_IPHONE_5)
    //    {
    //        labl_ApplyAll.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,25);
    //    }
    //    else
    //    {
    //        labl_ApplyAll.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,20);
    //    }
    labl_ApplyAll.text = @"Apply To All";
    labl_ApplyAll.font = [UIFont fontWithName:kFont size:12];
    labl_ApplyAll.backgroundColor=[UIColor clearColor];
    //    labl_ApplyAll.textAlignment=NSTextAlignmentCenter;
    [scroll addSubview:labl_ApplyAll];
    
    
    table_DishList = [[UITableView alloc]init ];
    table_DishList.frame  = CGRectMake(0,CGRectGetMaxY(labl_ApplyAll.frame)+12, WIDTH,280);
    [ table_DishList setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_DishList.delegate = self;
    table_DishList.dataSource = self;
    table_DishList.showsVerticalScrollIndicator = NO;
    //    table_DishList.layer.borderWidth = 1.0;
    table_DishList.rowHeight = 230.0;
    table_DishList.backgroundColor = [UIColor colorWithRed:243/255.0f green:245/255.0f blue:244/255.0f alpha:1.0f];
    [scroll addSubview: table_DishList];
    
    
    UILabel  * labl_Preview = [[UILabel alloc]init];
    labl_Preview.frame=CGRectMake(5, CGRectGetMaxY(table_DishList.frame)+5, WIDTH-10 , 25);
    //    if (IS_IPHONE_6Plus)
    //    {
    //        labl_Preview.frame=CGRectMake(0,imgViewTop.frame.size.height+20, self.view.frame.size.width,32);
    //    }
    //    else if (IS_IPHONE_6)
    //    {
    //        labl_Preview.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,30);
    //    }
    //    else if (IS_IPHONE_5)
    //    {
    //        labl_Preview.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,25);
    //    }
    //    else
    //    {
    //        labl_Preview.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,20);
    //    }
    labl_Preview.text = @"Preview";
    labl_Preview.font = [UIFont fontWithName:kFontBold size:16];
    labl_Preview.backgroundColor=[UIColor clearColor];
    labl_Preview.textAlignment=NSTextAlignmentCenter;
    [scroll addSubview:labl_Preview];
    
    
    UIImageView *imgPreview=[[UIImageView alloc]init];
    imgPreview.frame=CGRectMake( 10, CGRectGetMaxY(labl_Preview.frame)+5, WIDTH-20 , 180 );
    //    if (IS_IPHONE_6Plus)
    //    {
    //        imgPreview.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH+10,130);
    //    }
    //    else if (IS_IPHONE_6)
    //    {
    //        imgPreview.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH+10,120);
    //    }
    //    else if (IS_IPHONE_5)
    //    {
    //        imgPreview.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame),WIDTH+10,100);
    //    }
    //    else
    //    {
    //        imgPreview.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame),WIDTH+10,100);
    //    }
    [imgPreview setUserInteractionEnabled:YES];
    imgPreview.image = [UIImage imageNamed:@"bg-img@2x"];
    //    imgPreview.layer.borderWidth = 1.0;
    [scroll addSubview:imgPreview];

    
    
    UILabel  * lbl_Preview1 = [[UILabel alloc]init];
    lbl_Preview1.frame=CGRectMake(25, 10, 100 , 25);
    //    if (IS_IPHONE_6Plus)
    //    {
    //        lbl_Preview1.frame=CGRectMake(0,imgViewTop.frame.size.height+20, self.view.frame.size.width,32);
    //    }
    //    else if (IS_IPHONE_6)
    //    {
    //        lbl_Preview1.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,30);
    //    }
    //    else if (IS_IPHONE_5)
    //    {
    //        lbl_Preview1.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,25);
    //    }
    //    else
    //    {
    //        lbl_Preview1.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,20);
    //    }
    lbl_Preview1.text = @"Serving Date:";
    lbl_Preview1.font = [UIFont fontWithName:kFont size:14];
    lbl_Preview1.backgroundColor=[UIColor clearColor];
    //    lbl_Preview1.textAlignment=NSTextAlignmentCenter;
    [imgPreview addSubview:lbl_Preview1];
    
    
    
    UILabel  * lbl_Preview2 = [[UILabel alloc]init];
    lbl_Preview2.frame=CGRectMake(125, 10, 110 , 25);
    //    if (IS_IPHONE_6Plus)
    //    {
    //        lbl_Preview2.frame=CGRectMake(0,imgViewTop.frame.size.height+20, self.view.frame.size.width,32);
    //    }
    //    else if (IS_IPHONE_6)
    //    {
    //        lbl_Preview2.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,30);
    //    }
    //    else if (IS_IPHONE_5)
    //    {
    //        lbl_Preview2.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,25);
    //    }
    //    else
    //    {
    //        lbl_Preview2.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,20);
    //    }
    lbl_Preview2.text = @"4:00 PM";
    lbl_Preview2.font = [UIFont fontWithName:kFontBold size:14];
    lbl_Preview2.backgroundColor=[UIColor clearColor];
    //    lbl_Preview2.textAlignment=NSTextAlignmentCenter;
    [imgPreview addSubview:lbl_Preview2];
    
    
    
    UILabel  * lbl_Preview3 = [[UILabel alloc]init];
    lbl_Preview3.frame=CGRectMake(25, 35, 100 , 25);
    //    if (IS_IPHONE_6Plus)
    //    {
    //        lbl_Preview3.frame=CGRectMake(0,imgViewTop.frame.size.height+20, self.view.frame.size.width,32);
    //    }
    //    else if (IS_IPHONE_6)
    //    {
    //        lbl_Preview3.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,30);
    //    }
    //    else if (IS_IPHONE_5)
    //    {
    //        lbl_Preview3.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,25);
    //    }
    //    else
    //    {
    //        lbl_Preview3.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,20);
    //    }
    lbl_Preview3.text = @"Serving Date:";
    lbl_Preview3.font = [UIFont fontWithName:kFont size:14];
    lbl_Preview3.backgroundColor=[UIColor clearColor];
    //    lbl_Preview3.textAlignment=NSTextAlignmentCenter;
    [imgPreview addSubview:lbl_Preview3];
    
    
    UILabel  * lbl_Preview4 = [[UILabel alloc]init];
    lbl_Preview4.frame=CGRectMake(125, 35, 120 , 25);
    //    if (IS_IPHONE_6Plus)
    //    {
    //        lbl_Preview4.frame=CGRectMake(0,imgViewTop.frame.size.height+20, self.view.frame.size.width,32);
    //    }
    //    else if (IS_IPHONE_6)
    //    {
    //        lbl_Preview4.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,30);
    //    }
    //    else if (IS_IPHONE_5)
    //    {
    //        lbl_Preview4.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,25);
    //    }
    //    else
    //    {
    //        lbl_Preview4.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,20);
    //    }
    lbl_Preview4.text = @"4:00 PM";
    lbl_Preview4.font = [UIFont fontWithName:kFontBold size:14];
    lbl_Preview4.backgroundColor=[UIColor clearColor];
    //    lbl_Preview4.textAlignment=NSTextAlignmentCenter;
    [imgPreview addSubview:lbl_Preview4];
    
    
    UIImageView *imgLine=[[UIImageView alloc]init];
    imgLine.frame=CGRectMake( 10, 65, WIDTH-40 , 0.5 );
    //    if (IS_IPHONE_6Plus)
    //    {
    //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH+10,130);
    //    }
    //    else if (IS_IPHONE_6)
    //    {
    //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH+10,120);
    //    }
    //    else if (IS_IPHONE_5)
    //    {
    //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame),WIDTH+10,100);
    //    }
    //    else
    //    {
    //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame),WIDTH+10,100);
    //    }
    [imgLine setUserInteractionEnabled:YES];
    imgLine.image = [UIImage imageNamed:@"line-img@2x"];
    //    imgLine.layer.borderWidth = 1.0;
    [imgPreview addSubview:imgLine];
    
    
    
    table_Preview = [[UITableView alloc]init ];
    table_Preview.frame  = CGRectMake(10,70,WIDTH-40,100);
    [ table_Preview setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_Preview.delegate = self;
    table_Preview.dataSource = self;
    table_Preview.showsVerticalScrollIndicator = NO;
    //    table_Preview.layer.borderWidth = 1.0;
    table_Preview.rowHeight = 50.0;
    table_Preview.backgroundColor = [UIColor clearColor];
    [imgPreview addSubview: table_Preview];
    
    
    
    
    UIImageView *repeat_BgImg = [[UIImageView alloc]init];
    repeat_BgImg.frame = CGRectMake(0, CGRectGetMaxY(imgPreview.frame)+5,WIDTH ,53);
    repeat_BgImg.image = [UIImage imageNamed:@"img_background@2x."];
    repeat_BgImg.userInteractionEnabled = YES;
    //        repeat_BgImg.layer.borderWidth = 1.0;
    [scroll addSubview:repeat_BgImg];
    
    
    repeat_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    repeat_Btn.frame = CGRectMake(0, 0,WIDTH ,47);
    [repeat_Btn addTarget:self action:@selector(repeat_BtnMethod:) forControlEvents:UIControlEventTouchUpInside];
    //    repeat_Btn.tag = 1;  CGRectMake(0, CGRectGetMaxY(imgPreview.frame)+5,WIDTH ,47);
    repeat_Btn.backgroundColor = [UIColor clearColor];
    [repeat_BgImg addSubview: repeat_Btn];
    
    
    repeatLbl = [[UILabel alloc]init];
    repeatLbl.frame = CGRectMake(35, 0, 220, 47);
    repeatLbl.backgroundColor = [UIColor clearColor];
    repeatLbl.text = @"Repeat";
    //    repeatLbl.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    //    repeatLbl.textAlignment = NSTextAlignmentCenter;
    repeatLbl.font = [UIFont fontWithName:kFontBold size:15];
    repeatLbl.lineBreakMode = NSLineBreakByWordWrapping;
    repeatLbl.numberOfLines = 0;
    [repeat_BgImg addSubview:repeatLbl];
    
    
    UIImageView *dd_BgImg = [[UIImageView alloc]init];
    dd_BgImg.frame = CGRectMake(WIDTH-50, 20, 20 ,10);
    dd_BgImg.backgroundColor = [UIColor clearColor];
    dd_BgImg.image = [UIImage imageNamed:@"drop-down-icon@2x"];
    [repeat_BgImg addSubview:dd_BgImg];
    
    
    table_Repeated = [[UITableView alloc]init ];
    table_Repeated.frame  = CGRectMake(30,CGRectGetMinY(repeat_BgImg.frame)-150,WIDTH-60,150);
    [ table_Repeated setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_Repeated.delegate = self;
    table_Repeated.dataSource = self;
    table_Repeated.showsVerticalScrollIndicator = NO;
//    table_Repeated.layer.borderWidth = 1.0;
    table_Repeated.rowHeight = 30.0;
    table_Repeated.backgroundColor = [UIColor clearColor];
    [scroll addSubview: table_Repeated];
    table_Repeated.hidden = YES;
    
    
//    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideDropDown)];
//    [gestureRecognizer setCancelsTouchesInView:NO];
//    [self.view addGestureRecognizer:gestureRecognizer];
    
    
    UIView *calView = [[UIView alloc]init];
    calView.frame=CGRectMake(12, CGRectGetMaxY(repeat_BgImg.frame)+5, WIDTH-24, 300);
    if (IS_IPHONE_6Plus)
    {
        calView.frame=CGRectMake(12, CGRectGetMaxY(repeat_BgImg.frame)+5, WIDTH-24, 380);
    }
    else if (IS_IPHONE_6)
    {
       calView.frame=CGRectMake(12, CGRectGetMaxY(repeat_BgImg.frame)+5, WIDTH-24, 350);
    }
    else if (IS_IPHONE_5)
    {
        calView.frame=CGRectMake(12, CGRectGetMaxY(repeat_BgImg.frame)+5, WIDTH-24, 300);
    }
    else
    {
        calView.frame=CGRectMake(12, CGRectGetMaxY(repeat_BgImg.frame)+5, WIDTH-24, 300);
    }
    calView.backgroundColor = [UIColor whiteColor];
//    calView.layer.borderWidth=1.0;
    //    [self.view addSubview: notificationView];
    [scroll addSubview: calView];
    
    
    calendar = [[CKCalendarView alloc] initWithStartDay:startSunday];
//    calendar.frame = CGRectMake(12, CGRectGetMaxY(repeat_BgImg.frame)+5, WIDTH-24, 300);
    if (IS_IPHONE_6Plus)
    {
        calendar.frame = CGRectMake(0, 0, WIDTH-24, 380);
    }
    else if (IS_IPHONE_6)
    {
        calendar.frame = CGRectMake(0, 0, WIDTH-24, 350);
    }
    else if (IS_IPHONE_5)
    {
        calendar.frame = CGRectMake(0, 0, WIDTH-24, 300);
    }
    else
    {
        calendar.frame = CGRectMake(0, 0, WIDTH-24, 300);
    }
    self.calendar = calendar;
    calendar.delegate = self;
    calendar.backgroundColor = [UIColor clearColor];
    calendar.titleColor = [UIColor blackColor];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    //     minimumDate = [dateFormatter dateFromString:dateString];
    //    minimumDate = [dateFormatter dateFromString:@"22/02/1990"];
    calendar.onlyShowCurrentMonth = NO;
    calendar.adaptHeightToNumberOfWeeksInMonth = NO;
    calendar.titleFont = [UIFont fontWithName:kFont size:17.0];
    //    calendar.layer.borderWidth = 1.0;
    [calView addSubview:calendar];
    [calendar selectDate:[NSDate date] makeVisible:NO];
    //    ary_AvailableDates = self.enabledDates;
    [calendar reloadData];
    
    
    UILabel  * labl_selDate = [[UILabel alloc]init ];
    if (IS_IPHONE_6Plus)
    {
        labl_selDate.frame=CGRectMake(0,CGRectGetMaxY(calView.frame)+5, self.view.frame.size.width,32);
    }
    else if (IS_IPHONE_6)
    {
        labl_selDate.frame=CGRectMake(0,CGRectGetMaxY(calView.frame)+5, self.view.frame.size.width,30);
    }
    else if (IS_IPHONE_5)
    {
        labl_selDate.frame=CGRectMake(0,CGRectGetMaxY(calView.frame)+5, self.view.frame.size.width,25);
    }
    else
    {
        labl_selDate.frame=CGRectMake(0,CGRectGetMaxY(calView.frame)+5, self.view.frame.size.width,20);
    }
    labl_selDate.text = @"Date Selected";
    labl_selDate.font = [UIFont fontWithName:kFontBold size:16];
    labl_selDate.backgroundColor=[UIColor clearColor];
    labl_selDate.textAlignment=NSTextAlignmentCenter;
    [scroll addSubview:labl_selDate];
    
    
    UIImageView *imgSelectedDate=[[UIImageView alloc]init];
    imgSelectedDate.frame=CGRectMake( 10, CGRectGetMaxY(labl_selDate.frame)+5, WIDTH-20 , 100 );
    //    if (IS_IPHONE_6Plus)
    //    {
    //        imgSelectedDate.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH+10,130);
    //    }
    //    else if (IS_IPHONE_6)
    //    {
    //        imgSelectedDate.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH+10,120);
    //    }
    //    else if (IS_IPHONE_5)
    //    {
    //        imgSelectedDate.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame),WIDTH+10,100);
    //    }
    //    else
    //    {
    //        imgSelectedDate.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame),WIDTH+10,100);
    //    }
    [imgSelectedDate setUserInteractionEnabled:YES];
    imgSelectedDate.image = [UIImage imageNamed:@"bg-img@2x"];
    //    imgSelectedDate.layer.borderWidth = 1.0;
    [scroll addSubview:imgSelectedDate];
    
    
   table_Selected_Date = [[UITableView alloc]init ];
    table_Selected_Date.frame  = CGRectMake(10,3,WIDTH-40,90);
    [ table_Selected_Date setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_Selected_Date.delegate = self;
    table_Selected_Date.dataSource = self;
    table_Selected_Date.showsVerticalScrollIndicator = NO;
//        table_Selected_Date.layer.borderWidth = 1.0;
    table_Selected_Date.rowHeight = 45.0;
//    if (IS_IPHONE_6Plus)
//    {
//        table_Selected_Date.frame  = CGRectMake(10,3,WIDTH-40,110);
//        table_Selected_Date.rowHeight = 55.0;
//    }
//    else  if (IS_IPHONE_6)
//    {
//        table_Selected_Date.frame  = CGRectMake(10,3,WIDTH-40,110);
//        table_Selected_Date.rowHeight = 55.0;
//    }
//    else  if (IS_IPHONE_5)
//    {
//        table_Selected_Date.frame  = CGRectMake(10,3,WIDTH-40,90);
//        table_Selected_Date.rowHeight = 45.0;
//    }
//    else
//    {
//        table_Selected_Date.frame  = CGRectMake(10,3,WIDTH-40,90);
//        table_Selected_Date.rowHeight = 45.0;
//    }
    table_Selected_Date.backgroundColor = [UIColor clearColor];
    [imgSelectedDate addSubview: table_Selected_Date];
    
    
    btn_ServeNow = [[UIButton alloc] init];
//    btn_ServeNow.frame = CGRectMake(40, CGRectGetMaxY(imgPreview.frame)+20, WIDTH-80, 40);
        if (IS_IPHONE_6Plus) {
            //        btn_ServeNow.frame = CGRectMake(40, self.view.frame.size.height-50, self.view.frame.size.width-80, 45);
            btn_ServeNow.frame = CGRectMake(40, self.view.frame.size.height-65, self.view.frame.size.width-80, 50);
    
        }else if (IS_IPHONE_6)
    
        {
            btn_ServeNow.frame = CGRectMake(40, self.view.frame.size.height-60, self.view.frame.size.width-80, 45);
    
        }
        else if (IS_IPHONE_5)
        {
            btn_ServeNow.frame = CGRectMake(40, self.view.frame.size.height-60, self.view.frame.size.width-80, 40);
        }
        else
        {
            btn_ServeNow.frame = CGRectMake(40, self.view.frame.size.height-50, self.view.frame.size.width-80, 35);
        }
//    btn_ServeNow.backgroundColor = [UIColor brownColor];
    btn_ServeNow.layer.cornerRadius=4.0f;
    [btn_ServeNow setTitle:@"SERVE LATER" forState:UIControlStateNormal];
    [btn_ServeNow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_ServeNow setBackgroundColor:[UIColor colorWithRed:39.0/255.0 green:37.0/255.0 blue:48.0/255.0 alpha:1.0f]];
    btn_ServeNow.titleLabel.font=[UIFont fontWithName:kFont size:16];
    [btn_ServeNow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_ServeNow addTarget:self action:@selector(ServeNow_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [self.view addSubview:btn_ServeNow];
    
}
#pragma mark popup

//    ============== Delevery Design bg-img@2x

-(void)deliveryDesign
{
    [DeliveryPopUpBg removeFromSuperview];
    DeliveryPopUpBg=[[UIView alloc] init];
    DeliveryPopUpBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    DeliveryPopUpBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    DeliveryPopUpBg.userInteractionEnabled=TRUE;
    [self.view addSubview:DeliveryPopUpBg];
    
    
    img_bg_for_delevery = [[UIImageView alloc]init];
    img_bg_for_delevery.frame = CGRectMake(0,40, WIDTH, 500);
    if (IS_IPHONE_6Plus)
    {
        img_bg_for_delevery.frame = CGRectMake(10,(HEIGHT-500)/2.0, WIDTH-20, 500);
    }
    else if (IS_IPHONE_6)
    {
        img_bg_for_delevery.frame = CGRectMake(10,(HEIGHT-500)/2.0, WIDTH-20, 500);
    }
    else if (IS_IPHONE_5)
    {
        img_bg_for_delevery.frame = CGRectMake(0,45, WIDTH, 500);
    }
    else
    {
        img_bg_for_delevery.frame = CGRectMake(0,40, WIDTH, 450);
    }
    [img_bg_for_delevery  setImage:[UIImage imageNamed:@"img-whtcell-bg@2x"]];
//    img_bg_for_delevery.layer.borderWidth  = 1.0;
    [img_bg_for_delevery  setUserInteractionEnabled:YES];
    [DeliveryPopUpBg addSubview:img_bg_for_delevery];
//    img_bg_for_delevery.hidden = YES;
    
    
    bth_cross =[UIButton buttonWithType:UIButtonTypeCustom];
    bth_cross.frame=CGRectMake(img_bg_for_delevery.frame.size.width-60,12,30,30);
    bth_cross.backgroundColor = [UIColor clearColor];
    [bth_cross addTarget:self action:@selector(cross_Method:) forControlEvents:UIControlEventTouchUpInside];
    bth_cross.userInteractionEnabled = YES;
    [bth_cross setImage:[UIImage imageNamed:@"cross-img@2x.png"] forState:UIControlStateNormal];
    [img_bg_for_delevery  addSubview:bth_cross];
    
    
    UILabel *labl_delevery_company_name = [[UILabel alloc]init];
    labl_delevery_company_name.frame = CGRectMake(30,20, 300,40);
    if (IS_IPHONE_6Plus)
    {
        labl_delevery_company_name.frame = CGRectMake(30,20, 300,40);
    }
    else if (IS_IPHONE_6)
    {
        labl_delevery_company_name.frame = CGRectMake(30,20, 300,40);
    }
    else if (IS_IPHONE_5)
    {
        labl_delevery_company_name.frame = CGRectMake(30,20, 300,40);
    }
    else
    {
        labl_delevery_company_name.frame = CGRectMake(30,20, 300,35);
    }
    labl_delevery_company_name.text = @"Delivery Company Name";
//    labl_delevery_company_name.text = [NSString stringWithFormat:@"%@",[array_delivery_company_name objectAtIndex:indexPath.row]];
    labl_delevery_company_name.font = [UIFont fontWithName:kFontBold size:15];
    labl_delevery_company_name.textColor = [UIColor blackColor];
    labl_delevery_company_name.backgroundColor = [UIColor clearColor];
    [img_bg_for_delevery addSubview:labl_delevery_company_name];
    
    
    // array_delivery_company_name
    
    
    txt_delevery_company = [[UITextField alloc] init];
    txt_delevery_company.frame = CGRectMake(25,CGRectGetMidY(labl_delevery_company_name.frame),300,45);
    if (IS_IPHONE_6Plus)
    {
        txt_delevery_company.frame = CGRectMake(25,CGRectGetMidY(labl_delevery_company_name.frame),300,45);
    }
    else if (IS_IPHONE_6)
    {
        txt_delevery_company.frame = CGRectMake(25,CGRectGetMidY(labl_delevery_company_name.frame),300,45);
    }
    else if (IS_IPHONE_5)
    {
        txt_delevery_company.frame = CGRectMake(25,CGRectGetMidY(labl_delevery_company_name.frame),300,45);
    }
    else
    {
        txt_delevery_company.frame = CGRectMake(25,CGRectGetMidY(labl_delevery_company_name.frame),300,40);
    }
    txt_delevery_company .borderStyle = UITextBorderStyleNone;
    txt_delevery_company .textColor = [UIColor blackColor];
    txt_delevery_company .font = [UIFont fontWithName:kFont size:12];
    txt_delevery_company .placeholder = @"Delivery Company Name";
    [txt_delevery_company  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_delevery_company  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding24 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_delevery_company .leftView = padding24;
    txt_delevery_company .leftViewMode = UITextFieldViewModeAlways;
    txt_delevery_company .userInteractionEnabled=YES;
    txt_delevery_company .textAlignment = NSTextAlignmentLeft;
    txt_delevery_company .backgroundColor = [UIColor clearColor];
    txt_delevery_company .keyboardType = UIKeyboardTypeAlphabet;
    txt_delevery_company .delegate = self;
    [img_bg_for_delevery addSubview:txt_delevery_company ];
    
    
    UIImageView *img_line25 = [[UIImageView alloc]init];
    img_line25.frame = CGRectMake(30,CGRectGetMidY(txt_delevery_company.frame)+15, WIDTH-60, 0.5);
    if (IS_IPHONE_6Plus)
    {
        img_line25.frame = CGRectMake(30,CGRectGetMidY(txt_delevery_company.frame)+15, WIDTH-80, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        img_line25.frame = CGRectMake(30,CGRectGetMidY(txt_delevery_company.frame)+15, WIDTH-80, 0.5);
    }
    else if (IS_IPHONE_5)
    {
        img_line25.frame = CGRectMake(30,CGRectGetMidY(txt_delevery_company.frame)+15, WIDTH-60, 0.5);
    }
    else
    {
        img_line25.frame = CGRectMake(30,CGRectGetMidY(txt_delevery_company.frame)+15, WIDTH-60, 0.5);
    }
    [img_line25  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line25  setUserInteractionEnabled:YES];
    [img_bg_for_delevery addSubview:img_line25];
    
    
    UILabel *labl_delevery_company_phone_no = [[UILabel alloc]init];
    labl_delevery_company_phone_no.frame = CGRectMake(30,CGRectGetMaxY(img_line25.frame), 300,40);
    if (IS_IPHONE_6Plus)
    {
        labl_delevery_company_phone_no.frame = CGRectMake(30,CGRectGetMaxY(img_line25.frame), 300,40);
    }
    else if (IS_IPHONE_6)
    {
        labl_delevery_company_phone_no.frame = CGRectMake(30,CGRectGetMaxY(img_line25.frame), 300,40);
    }
    else if (IS_IPHONE_5)
    {
        labl_delevery_company_phone_no.frame = CGRectMake(30,CGRectGetMaxY(img_line25.frame), 300,40);
    }
    else
    {
        labl_delevery_company_phone_no.frame = CGRectMake(30,CGRectGetMaxY(img_line25.frame), 300,35);
    }
    labl_delevery_company_phone_no.text = @"Delivery Company Phone";
    labl_delevery_company_phone_no.font = [UIFont fontWithName:kFontBold size:15];
    labl_delevery_company_phone_no.textColor = [UIColor blackColor];
    labl_delevery_company_phone_no.backgroundColor = [UIColor clearColor];
    [img_bg_for_delevery addSubview:labl_delevery_company_phone_no];
    
    
    txt_tel_code = [[UITextField alloc] init];
    txt_tel_code.frame = CGRectMake(25,CGRectGetMidY(labl_delevery_company_phone_no.frame),45,45);
    if (IS_IPHONE_6Plus)
    {
        txt_tel_code.frame = CGRectMake(25,CGRectGetMidY(labl_delevery_company_phone_no.frame),45,45);
    }
    else if (IS_IPHONE_6)
    {
        txt_tel_code.frame = CGRectMake(25,CGRectGetMidY(labl_delevery_company_phone_no.frame),45,45);
    }
    else if (IS_IPHONE_5)
    {
        txt_tel_code.frame = CGRectMake(25,CGRectGetMidY(labl_delevery_company_phone_no.frame),45,45);
    }
    else
    {
        txt_tel_code.frame = CGRectMake(25,CGRectGetMidY(labl_delevery_company_phone_no.frame),45,40);
    }
    txt_tel_code .borderStyle = UITextBorderStyleNone;
    txt_tel_code .textColor = [UIColor blackColor];
    txt_tel_code .font = [UIFont fontWithName:kFont size:12];
    txt_tel_code .placeholder = @"code";
    [txt_tel_code  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_tel_code  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding25 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_tel_code .leftView = padding25;
    txt_tel_code .leftViewMode = UITextFieldViewModeAlways;
    txt_tel_code .userInteractionEnabled=YES;
    txt_tel_code .textAlignment = NSTextAlignmentLeft;
    txt_tel_code .backgroundColor = [UIColor clearColor];
    txt_tel_code .keyboardType = UIKeyboardTypeAlphabet;
    txt_tel_code .delegate = self;
    [img_bg_for_delevery addSubview:txt_tel_code];
    
    
    UIButton *icon_dropdown_for_tel_code =[UIButton buttonWithType:UIButtonTypeCustom];
    icon_dropdown_for_tel_code.frame=CGRectMake(CGRectGetMaxX(txt_tel_code.frame),CGRectGetMidY(labl_delevery_company_phone_no.frame)+15, 20, 15);
    if (IS_IPHONE_6Plus)
    {
        icon_dropdown_for_tel_code.frame=CGRectMake(CGRectGetMaxX(txt_tel_code.frame),CGRectGetMidY(labl_delevery_company_phone_no.frame)+15, 20, 15);
    }
    else if (IS_IPHONE_6)
    {
        icon_dropdown_for_tel_code.frame=CGRectMake(CGRectGetMaxX(txt_tel_code.frame),CGRectGetMidY(labl_delevery_company_phone_no.frame)+15, 20, 15);
    }
    else if (IS_IPHONE_5)
    {
        icon_dropdown_for_tel_code.frame=CGRectMake(CGRectGetMaxX(txt_tel_code.frame),CGRectGetMidY(labl_delevery_company_phone_no.frame)+15, 20, 15);
    }
    else
    {
        icon_dropdown_for_tel_code.frame=CGRectMake(CGRectGetMaxX(txt_tel_code.frame),CGRectGetMidY(labl_delevery_company_phone_no.frame)+15, 20, 15);
    }
    //icon_dropdown.backgroundColor = [UIColor clearColor];
    // [icon_dropdown addTarget:self action:@selector(icon_drop_down_click:) forControlEvents:UIControlEventTouchUpInside];
    icon_dropdown_for_tel_code.userInteractionEnabled = YES;
    [icon_dropdown_for_tel_code setImage:[UIImage imageNamed:@"dropdown@2x.png"] forState:UIControlStateNormal];
    [img_bg_for_delevery  addSubview:icon_dropdown_for_tel_code];
    
    
    btn_on_tel_code_dropdown =[UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_tel_code_dropdown.frame=CGRectMake(30,CGRectGetMidY(labl_delevery_company_phone_no.frame)+12,65,25);
    if (IS_IPHONE_6Plus)
    {
        btn_on_tel_code_dropdown.frame=CGRectMake(30,CGRectGetMidY(labl_delevery_company_phone_no.frame)+12,65,25);
    }
    else if (IS_IPHONE_6)
    {
        btn_on_tel_code_dropdown.frame=CGRectMake(30,CGRectGetMidY(labl_delevery_company_phone_no.frame)+12,65,25);
    }
    else if (IS_IPHONE_5)
    {
        btn_on_tel_code_dropdown.frame=CGRectMake(30,CGRectGetMidY(labl_delevery_company_phone_no.frame)+12,65,25);
    }
    else
    {
        btn_on_tel_code_dropdown.frame=CGRectMake(30,CGRectGetMidY(labl_delevery_company_phone_no.frame)+12,65,22);
    }
    btn_on_tel_code_dropdown.backgroundColor = [UIColor clearColor];
    [btn_on_tel_code_dropdown addTarget:self action:@selector(tel_code_dropdownMethod:) forControlEvents:UIControlEventTouchUpInside];
    btn_on_tel_code_dropdown.userInteractionEnabled = YES;
    //  [btn_on_dropdown setImage:[UIImage imageNamed:@"dropdown@2x.png"] forState:UIControlStateNormal];
    [img_bg_for_delevery  addSubview:btn_on_tel_code_dropdown];
    
    
    UIImageView *img_line26 = [[UIImageView alloc]init];
    img_line26.frame = CGRectMake(30,CGRectGetMidY(txt_tel_code.frame)+15, 65, 0.5);
    if (IS_IPHONE_6Plus)
    {
        img_line26.frame = CGRectMake(30,CGRectGetMidY(txt_tel_code.frame)+15, 65, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        img_line26.frame = CGRectMake(30,CGRectGetMidY(txt_tel_code.frame)+15, 65, 0.5);
    }
    else if (IS_IPHONE_5)
    {
        img_line26.frame = CGRectMake(30,CGRectGetMidY(txt_tel_code.frame)+15, 65, 0.5);
    }
    else
    {
        img_line26.frame = CGRectMake(30,CGRectGetMidY(txt_tel_code.frame)+15, 65, 0.5);
    }
    [img_line26  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line26  setUserInteractionEnabled:YES];
    [img_bg_for_delevery addSubview:img_line26];
    
    
    
    table_for_contry_code1 = [[UITableView alloc] init ];
    table_for_contry_code1.frame  = CGRectMake(30,CGRectGetMaxY(img_line26.frame), 65 ,100);
    if (IS_IPHONE_6Plus)
    {
        table_for_contry_code1.frame  = CGRectMake(30,CGRectGetMaxY(img_line26.frame), 65 ,100);
    }
    else if (IS_IPHONE_6)
    {
        table_for_contry_code1.frame  = CGRectMake(30,CGRectGetMaxY(img_line26.frame), 65 ,100);
    }
    else if (IS_IPHONE_5)
    {
        table_for_contry_code1.frame  = CGRectMake(30,CGRectGetMaxY(img_line26.frame), 65 ,100);
    }
    else
    {
        table_for_contry_code1.frame  = CGRectMake(30,CGRectGetMaxY(img_line26.frame), 65 ,100);
    }
    [table_for_contry_code1 setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_contry_code1.delegate = self;
    table_for_contry_code1.dataSource = self;
    table_for_contry_code1.showsVerticalScrollIndicator = NO;
    table_for_contry_code1.rowHeight = 25.0;
    table_for_contry_code1.layer.borderWidth = 1.0;
    table_for_contry_code1.layer.cornerRadius = 2.0;
   table_for_contry_code1.backgroundColor = [UIColor whiteColor];
    [img_bg_for_delevery addSubview:table_for_contry_code1];
    table_for_contry_code1.hidden =YES;
    
    
    
    txt_mobile_number2 = [[UITextField alloc] init];
    txt_mobile_number2.frame = CGRectMake(CGRectGetMaxX(btn_on_tel_code_dropdown.frame)+15,CGRectGetMidY(labl_delevery_company_phone_no.frame),80,45);
    if (IS_IPHONE_6Plus)
    {
        txt_mobile_number2.frame = CGRectMake(CGRectGetMaxX(btn_on_tel_code_dropdown.frame)+15,CGRectGetMidY(labl_delevery_company_phone_no.frame),80,45);
    }
    else if (IS_IPHONE_6)
    {
        txt_mobile_number2.frame = CGRectMake(CGRectGetMaxX(btn_on_tel_code_dropdown.frame)+15,CGRectGetMidY(labl_delevery_company_phone_no.frame),80,45);
    }
    else if (IS_IPHONE_5)
    {
        txt_mobile_number2.frame = CGRectMake(CGRectGetMaxX(btn_on_tel_code_dropdown.frame)+15,CGRectGetMidY(labl_delevery_company_phone_no.frame),80,45);
    }
    else
    {
        txt_mobile_number2.frame = CGRectMake(CGRectGetMaxX(btn_on_tel_code_dropdown.frame)+15,CGRectGetMidY(labl_delevery_company_phone_no.frame),80,40);
    }
    txt_mobile_number2 .borderStyle = UITextBorderStyleNone;
    txt_mobile_number2 .textColor = [UIColor blackColor];
    txt_mobile_number2 .font = [UIFont fontWithName:kFont size:12];
    txt_mobile_number2 .placeholder = @"Delivery Company Phone";
    [txt_mobile_number2  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_mobile_number2  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding26 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_mobile_number2 .leftView = padding26;
    txt_mobile_number2 .leftViewMode = UITextFieldViewModeAlways;
    txt_mobile_number2 .userInteractionEnabled=YES;
    txt_mobile_number2 .textAlignment = NSTextAlignmentLeft;
    txt_mobile_number2 .backgroundColor = [UIColor clearColor];
    txt_mobile_number2 .keyboardType = UIKeyboardTypeAlphabet;
    txt_mobile_number2 .delegate = self;
    [img_bg_for_delevery addSubview:txt_mobile_number2];
    
    
    UIImageView *img_line27 = [[UIImageView alloc]init];
    img_line27.frame = CGRectMake(CGRectGetMaxX(img_line26.frame)+15,CGRectGetMidY(txt_mobile_number2.frame)+15, WIDTH-140, 0.5);
    if (IS_IPHONE_6Plus)
    {
         img_line27.frame = CGRectMake(CGRectGetMaxX(img_line26.frame)+15,CGRectGetMidY(txt_mobile_number2.frame)+15, WIDTH-160, 0.5);
    }
    else if (IS_IPHONE_6)
    {
         img_line27.frame = CGRectMake(CGRectGetMaxX(img_line26.frame)+15,CGRectGetMidY(txt_mobile_number2.frame)+15, WIDTH-160, 0.5);
    }
    else if (IS_IPHONE_5)
    {
         img_line27.frame = CGRectMake(CGRectGetMaxX(img_line26.frame)+15,CGRectGetMidY(txt_mobile_number2.frame)+15, WIDTH-140, 0.5);
    }
    else
    {
         img_line27.frame = CGRectMake(CGRectGetMaxX(img_line26.frame)+15,CGRectGetMidY(txt_mobile_number2.frame)+15, WIDTH-140, 0.5);
    }
    [img_line27  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line27  setUserInteractionEnabled:YES];
    [img_bg_for_delevery addSubview:img_line27];
    
    
    UILabel *labl_delevery_person_name = [[UILabel alloc]init];
    labl_delevery_person_name.frame = CGRectMake(30,CGRectGetMaxY(img_line27.frame), 300,40);
    if (IS_IPHONE_6Plus)
    {
        labl_delevery_person_name.frame = CGRectMake(30,CGRectGetMaxY(img_line27.frame), 300,40);
    }
    else if (IS_IPHONE_6)
    {
        labl_delevery_person_name.frame = CGRectMake(30,CGRectGetMaxY(img_line27.frame), 300,40);
    }
    else if (IS_IPHONE_5)
    {
        labl_delevery_person_name.frame = CGRectMake(30,CGRectGetMaxY(img_line27.frame), 300,40);
    }
    else
    {
        labl_delevery_person_name.frame = CGRectMake(30,CGRectGetMaxY(img_line27.frame), 300,35);
    }
    labl_delevery_person_name.text = @"Delivery Person Name";
    labl_delevery_person_name.font = [UIFont fontWithName:kFontBold size:15];
    labl_delevery_person_name.textColor = [UIColor blackColor];
    labl_delevery_person_name.backgroundColor = [UIColor clearColor];
    [img_bg_for_delevery addSubview:labl_delevery_person_name];
    
    
    txt_persone_name = [[UITextField alloc] init];
    txt_persone_name.frame = CGRectMake(25,CGRectGetMidY(labl_delevery_person_name.frame),300,45);
    if (IS_IPHONE_6Plus)
    {
        txt_persone_name.frame = CGRectMake(25,CGRectGetMidY(labl_delevery_person_name.frame),300,45);
    }
    else if (IS_IPHONE_6)
    {
        txt_persone_name.frame = CGRectMake(25,CGRectGetMidY(labl_delevery_person_name.frame),300,45);
    }
    else if (IS_IPHONE_5)
    {
        txt_persone_name.frame = CGRectMake(25,CGRectGetMidY(labl_delevery_person_name.frame),300,45);
    }
    else
    {
        txt_persone_name.frame = CGRectMake(25,CGRectGetMidY(labl_delevery_person_name.frame),300,40);
    }
    txt_persone_name .borderStyle = UITextBorderStyleNone;
    txt_persone_name .textColor = [UIColor blackColor];
    txt_persone_name .font = [UIFont fontWithName:kFont size:12];
    txt_persone_name .placeholder = @"Delivery Person Name";
    [txt_persone_name  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_persone_name  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding29 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_persone_name .leftView = padding29;
    txt_persone_name .leftViewMode = UITextFieldViewModeAlways;
    txt_persone_name .userInteractionEnabled=YES;
    txt_persone_name .textAlignment = NSTextAlignmentLeft;
    txt_persone_name .backgroundColor = [UIColor clearColor];
    txt_persone_name .keyboardType = UIKeyboardTypeAlphabet;
    txt_persone_name .delegate = self;
    [img_bg_for_delevery addSubview:txt_persone_name];
    
    
    UIImageView *img_line28 = [[UIImageView alloc]init];
    img_line28.frame = CGRectMake(30,CGRectGetMidY(txt_persone_name.frame)+15, WIDTH-60, 0.5);
    if (IS_IPHONE_6Plus)
    {
        img_line28.frame = CGRectMake(30,CGRectGetMidY(txt_persone_name.frame)+15, WIDTH-80, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        img_line28.frame = CGRectMake(30,CGRectGetMidY(txt_persone_name.frame)+15, WIDTH-80, 0.5);
    }
    else if (IS_IPHONE_5)
    {
        img_line28.frame = CGRectMake(30,CGRectGetMidY(txt_persone_name.frame)+15, WIDTH-60, 0.5);
    }
    else
    {
        img_line28.frame = CGRectMake(30,CGRectGetMidY(txt_persone_name.frame)+15, WIDTH-60, 0.5);
    }
    [img_line28  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line28  setUserInteractionEnabled:YES];
    [img_bg_for_delevery addSubview:img_line28];
    
    
    UILabel *labl_optional = [[UILabel alloc]init];
    labl_optional.frame = CGRectMake(230,CGRectGetMinY(img_line28.frame)-10,100,40);
    if (IS_IPHONE_6Plus)
    {
        labl_optional.frame = CGRectMake(300,CGRectGetMinY(img_line28.frame)-10,100,40);
    }
    else if (IS_IPHONE_6)
    {
        labl_optional.frame = CGRectMake(265,CGRectGetMinY(img_line28.frame)-10,100,40);
    }
    else if (IS_IPHONE_5)
    {
        labl_optional.frame = CGRectMake(230,CGRectGetMinY(img_line28.frame)-10,100,40);
    }
    else
    {
        labl_optional.frame = CGRectMake(230,CGRectGetMinY(img_line28.frame)-10,100,35);
    }
    labl_optional.text = @"(optional)";
    labl_optional.font = [UIFont fontWithName:kFontBold size:12];
    labl_optional.textColor = [UIColor lightGrayColor];
    labl_optional.backgroundColor = [UIColor clearColor];
    [img_bg_for_delevery addSubview:labl_optional];
    
    
    UILabel *labl_persone_mobile_number = [[UILabel alloc]init];
    labl_persone_mobile_number.frame = CGRectMake(30,CGRectGetMinY(img_line28.frame),300,40);
    if (IS_IPHONE_6Plus)
    {
        labl_persone_mobile_number.frame = CGRectMake(30,CGRectGetMinY(img_line28.frame),300,40);
    }
    else if (IS_IPHONE_6)
    {
        labl_persone_mobile_number.frame = CGRectMake(30,CGRectGetMinY(img_line28.frame),300,40);
    }
    else if (IS_IPHONE_5)
    {
        labl_persone_mobile_number.frame = CGRectMake(30,CGRectGetMinY(img_line28.frame),300,40);
    }
    else
    {
        labl_persone_mobile_number.frame = CGRectMake(30,CGRectGetMinY(img_line28.frame),300,35);
    }
    labl_persone_mobile_number.text = @"Delivery Person Mobile";
    labl_persone_mobile_number.font = [UIFont fontWithName:kFontBold size:15];
    labl_persone_mobile_number.textColor = [UIColor blackColor];
    labl_persone_mobile_number.backgroundColor = [UIColor clearColor];
    [img_bg_for_delevery addSubview:labl_persone_mobile_number];
    
    
    txt_tel_code2 = [[UITextField alloc] init];
    txt_tel_code2.frame = CGRectMake(25,CGRectGetMidY(labl_persone_mobile_number.frame),45,45);
    if (IS_IPHONE_6Plus)
    {
        txt_tel_code2.frame = CGRectMake(25,CGRectGetMidY(labl_persone_mobile_number.frame),45,45);
    }
    else if (IS_IPHONE_6)
    {
        txt_tel_code2.frame = CGRectMake(25,CGRectGetMidY(labl_persone_mobile_number.frame),45,45);
    }
    else if (IS_IPHONE_5)
    {
        txt_tel_code2.frame = CGRectMake(25,CGRectGetMidY(labl_persone_mobile_number.frame),45,45);
    }
    else
    {
        txt_tel_code2.frame = CGRectMake(25,CGRectGetMidY(labl_persone_mobile_number.frame),45,40);
    }
    txt_tel_code2 .borderStyle = UITextBorderStyleNone;
    txt_tel_code2 .textColor = [UIColor blackColor];
    txt_tel_code2 .font = [UIFont fontWithName:kFont size:12];
    txt_tel_code2 .placeholder = @"code";
    [txt_tel_code2  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_tel_code2  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding27 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_tel_code2 .leftView = padding27;
    txt_tel_code2 .leftViewMode = UITextFieldViewModeAlways;
    txt_tel_code2 .userInteractionEnabled=YES;
    txt_tel_code2 .textAlignment = NSTextAlignmentLeft;
    txt_tel_code2 .backgroundColor = [UIColor clearColor];
    txt_tel_code2 .keyboardType = UIKeyboardTypeAlphabet;
    txt_tel_code2 .delegate = self;
    [img_bg_for_delevery addSubview:txt_tel_code2];
    
    
   icon_dropdown_for_tel_code2 =[UIButton buttonWithType:UIButtonTypeCustom];
    icon_dropdown_for_tel_code2.frame=CGRectMake(CGRectGetMaxX(txt_tel_code2.frame),CGRectGetMidY(labl_persone_mobile_number.frame)+15, 20, 15);
    if (IS_IPHONE_6Plus)
    {
        icon_dropdown_for_tel_code2.frame=CGRectMake(CGRectGetMaxX(txt_tel_code2.frame),CGRectGetMidY(labl_persone_mobile_number.frame)+15, 20, 15);
    }
    else if (IS_IPHONE_6)
    {
        icon_dropdown_for_tel_code2.frame=CGRectMake(CGRectGetMaxX(txt_tel_code2.frame),CGRectGetMidY(labl_persone_mobile_number.frame)+15, 20, 15);
    }
    else if (IS_IPHONE_5)
    {
        icon_dropdown_for_tel_code2.frame=CGRectMake(CGRectGetMaxX(txt_tel_code2.frame),CGRectGetMidY(labl_persone_mobile_number.frame)+15, 20, 15);
    }
    else
    {
        icon_dropdown_for_tel_code2.frame=CGRectMake(CGRectGetMaxX(txt_tel_code2.frame),CGRectGetMidY(labl_persone_mobile_number.frame)+15, 20, 15);
    }
    //icon_dropdown.backgroundColor = [UIColor clearColor];
    // [icon_dropdown addTarget:self action:@selector(icon_drop_down_click:) forControlEvents:UIControlEventTouchUpInside];
    icon_dropdown_for_tel_code2.userInteractionEnabled = YES;
    [icon_dropdown_for_tel_code2 setImage:[UIImage imageNamed:@"dropdown@2x.png"] forState:UIControlStateNormal];
    [img_bg_for_delevery  addSubview:icon_dropdown_for_tel_code2];
    
    
    btn_on_tel_code_dropdown2 =[UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_tel_code_dropdown2.frame=CGRectMake(30,CGRectGetMidY(labl_persone_mobile_number.frame)+12,65,25);
    if (IS_IPHONE_6Plus)
    {
        btn_on_tel_code_dropdown2.frame=CGRectMake(30,CGRectGetMidY(labl_persone_mobile_number.frame)+12,65,25);
    }
    else if (IS_IPHONE_6)
    {
        btn_on_tel_code_dropdown2.frame=CGRectMake(30,CGRectGetMidY(labl_persone_mobile_number.frame)+12,65,25);
    }
    else if (IS_IPHONE_5)
    {
        btn_on_tel_code_dropdown2.frame=CGRectMake(30,CGRectGetMidY(labl_persone_mobile_number.frame)+12,65,25);
    }
    else
    {
        btn_on_tel_code_dropdown2.frame=CGRectMake(30,CGRectGetMidY(labl_persone_mobile_number.frame)+12,65,22);
    }
    btn_on_tel_code_dropdown2.backgroundColor = [UIColor clearColor];
    [btn_on_tel_code_dropdown2 addTarget:self action:@selector(tel_code_dropdown2Method:) forControlEvents:UIControlEventTouchUpInside];
    btn_on_tel_code_dropdown2.userInteractionEnabled = YES;
    //  [btn_on_dropdown setImage:[UIImage imageNamed:@"dropdown@2x.png"] forState:UIControlStateNormal];
    [img_bg_for_delevery  addSubview:btn_on_tel_code_dropdown2];
    
    
    UIImageView *img_line29 = [[UIImageView alloc]init];
    img_line29.frame = CGRectMake(30,CGRectGetMidY(txt_tel_code2.frame)+15, 65, 0.5);
    if (IS_IPHONE_6Plus)
    {
        img_line29.frame = CGRectMake(30,CGRectGetMidY(txt_tel_code2.frame)+15, 65, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        img_line29.frame = CGRectMake(30,CGRectGetMidY(txt_tel_code2.frame)+15, 65, 0.5);
    }
    else if (IS_IPHONE_5)
    {
        img_line29.frame = CGRectMake(30,CGRectGetMidY(txt_tel_code2.frame)+15, 65, 0.5);
    }
    else
    {
        img_line29.frame = CGRectMake(30,CGRectGetMidY(txt_tel_code2.frame)+15, 65, 0.5);
    }
    [img_line29  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line29  setUserInteractionEnabled:YES];
    [img_bg_for_delevery addSubview:img_line29];
    
    
    table_for_contry_code2 = [[UITableView alloc] init ];
    table_for_contry_code2.frame  = CGRectMake(30,CGRectGetMaxY(img_line29.frame), 65 ,100);
    if (IS_IPHONE_6Plus)
    {
        table_for_contry_code2.frame  = CGRectMake(30,CGRectGetMaxY(img_line29.frame), 65 ,100);
    }
    else if (IS_IPHONE_6)
    {
        table_for_contry_code2.frame  = CGRectMake(30,CGRectGetMaxY(img_line29.frame), 65 ,100);
    }
    else if (IS_IPHONE_5)
    {
        table_for_contry_code2.frame  = CGRectMake(30,CGRectGetMaxY(img_line29.frame), 65 ,100);
    }
    else
    {
        table_for_contry_code2.frame  = CGRectMake(30,CGRectGetMaxY(img_line29.frame), 65 ,100);
    }
    [table_for_contry_code2 setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_contry_code2.delegate = self;
    table_for_contry_code2.dataSource = self;
//    table_for_contry_code2.userInteractionEnabled = NO;
    table_for_contry_code2.showsVerticalScrollIndicator = NO;
    table_for_contry_code2.rowHeight = 25.0;
    table_for_contry_code2.layer.borderWidth = 1.0;
    table_for_contry_code2.layer.cornerRadius = 2.0;
        table_for_contry_code2.backgroundColor = [UIColor whiteColor];
    [img_bg_for_delevery addSubview:table_for_contry_code2];
    table_for_contry_code2.hidden =YES;
    
    
   txt_person_mobile_number = [[UITextField alloc] init];
    txt_person_mobile_number.frame = CGRectMake(CGRectGetMaxX(btn_on_tel_code_dropdown2.frame)+10,CGRectGetMidY(labl_persone_mobile_number.frame),80,45);
    if (IS_IPHONE_6Plus)
    {
        txt_person_mobile_number.frame = CGRectMake(CGRectGetMaxX(btn_on_tel_code_dropdown2.frame)+10,CGRectGetMidY(labl_persone_mobile_number.frame),80,45);
    }
    else if (IS_IPHONE_6)
    {
        txt_person_mobile_number.frame = CGRectMake(CGRectGetMaxX(btn_on_tel_code_dropdown2.frame)+10,CGRectGetMidY(labl_persone_mobile_number.frame),80,45);
        
    }
    else if (IS_IPHONE_5)
    {
        txt_person_mobile_number.frame = CGRectMake(CGRectGetMaxX(btn_on_tel_code_dropdown2.frame)+10,CGRectGetMidY(labl_persone_mobile_number.frame),80,45);
    }
    else
    {
        txt_person_mobile_number.frame = CGRectMake(CGRectGetMaxX(btn_on_tel_code_dropdown2.frame)+10,CGRectGetMidY(labl_persone_mobile_number.frame),80,40);
    }
    txt_person_mobile_number .borderStyle = UITextBorderStyleNone;
    txt_person_mobile_number .textColor = [UIColor blackColor];
    txt_person_mobile_number .font = [UIFont fontWithName:kFont size:12];
    txt_person_mobile_number .placeholder = @"Delivery Person Mobile";
    [txt_person_mobile_number  setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
    [txt_person_mobile_number  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding28 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_person_mobile_number .leftView = padding28;
    txt_person_mobile_number .leftViewMode = UITextFieldViewModeAlways;
    txt_person_mobile_number .userInteractionEnabled=YES;
    txt_person_mobile_number .textAlignment = NSTextAlignmentLeft;
    txt_person_mobile_number .backgroundColor = [UIColor clearColor];
    txt_person_mobile_number .keyboardType = UIKeyboardTypeAlphabet;
    txt_person_mobile_number .delegate = self;
    [img_bg_for_delevery addSubview:txt_person_mobile_number];
    
    
    UIImageView *img_line30 = [[UIImageView alloc]init];
    img_line30.frame = CGRectMake(CGRectGetMaxX(img_line29.frame)+15,CGRectGetMidY(txt_person_mobile_number.frame)+15, WIDTH-140, 0.5);
    if (IS_IPHONE_6Plus)
    {
        img_line30.frame = CGRectMake(CGRectGetMaxX(img_line29.frame)+15,CGRectGetMidY(txt_person_mobile_number.frame)+15, WIDTH-160, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        img_line30.frame = CGRectMake(CGRectGetMaxX(img_line29.frame)+15,CGRectGetMidY(txt_person_mobile_number.frame)+15, WIDTH-160, 0.5);
    }
    else if (IS_IPHONE_5)
    {
        img_line30.frame = CGRectMake(CGRectGetMaxX(img_line29.frame)+15,CGRectGetMidY(txt_person_mobile_number.frame)+15, WIDTH-140, 0.5);
    }
    else
    {
        img_line30.frame = CGRectMake(CGRectGetMaxX(img_line29.frame)+15,CGRectGetMidY(txt_person_mobile_number.frame)+15, WIDTH-140, 0.5);
    }
    [img_line30  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line30  setUserInteractionEnabled:YES];
    [img_bg_for_delevery addSubview:img_line30];
    
    
    UILabel *labl_optional_number = [[UILabel alloc]init];
    labl_optional_number.frame = CGRectMake(230,CGRectGetMinY(img_line30.frame)-10,100,40);
    if (IS_IPHONE_6Plus)
    {
        labl_optional_number.frame = CGRectMake(300,CGRectGetMinY(img_line30.frame)-10,100,40);
    }
    else if (IS_IPHONE_6)
    {
        labl_optional_number.frame = CGRectMake(265,CGRectGetMinY(img_line30.frame)-10,100,40);
    }
    else if (IS_IPHONE_5)
    {
        labl_optional_number.frame = CGRectMake(230,CGRectGetMinY(img_line30.frame)-10,100,40);
    }
    else
    {
        labl_optional_number.frame = CGRectMake(230,CGRectGetMinY(img_line30.frame)-10,100,35);
    }
    labl_optional_number.text = @"(optional)";
    labl_optional_number.font = [UIFont fontWithName:kFontBold size:12];
    labl_optional_number.textColor = [UIColor lightGrayColor];
    labl_optional_number.backgroundColor = [UIColor clearColor];
    [img_bg_for_delevery addSubview:labl_optional_number];
    
    
    UILabel *labl_standerd_delivery_charge = [[UILabel alloc]init];
    labl_standerd_delivery_charge.frame = CGRectMake(30,CGRectGetMaxY(img_line30.frame)+5,300,30);
    if (IS_IPHONE_6Plus)
    {
        labl_standerd_delivery_charge.frame = CGRectMake(30,CGRectGetMaxY(img_line30.frame)+5,300,30);
    }
    else if (IS_IPHONE_6)
    {
        labl_standerd_delivery_charge.frame = CGRectMake(30,CGRectGetMaxY(img_line30.frame)+5,300,30);
    }
    else if (IS_IPHONE_5)
    {
        labl_standerd_delivery_charge.frame = CGRectMake(30,CGRectGetMaxY(img_line30.frame)+5,300,30);
    }
    else
    {
        labl_standerd_delivery_charge.frame = CGRectMake(30,CGRectGetMaxY(img_line30.frame)+5,300,30);
    }
    labl_standerd_delivery_charge.text = @"Standard Delivery charge";
    labl_standerd_delivery_charge.font = [UIFont fontWithName:kFontBold size:15];
    labl_standerd_delivery_charge.textColor = [UIColor blackColor];
    labl_standerd_delivery_charge.backgroundColor = [UIColor clearColor];
    [img_bg_for_delevery addSubview:labl_standerd_delivery_charge];
    
    
    
    radio_button_for_delivery_charge  =  [UIButton buttonWithType:UIButtonTypeCustom];
    radio_button_for_delivery_charge.frame = CGRectMake(30,CGRectGetMidY(labl_standerd_delivery_charge.frame)+20,20,20);
    if (IS_IPHONE_6Plus)
    {
        radio_button_for_delivery_charge.frame = CGRectMake(30,CGRectGetMidY(labl_standerd_delivery_charge.frame)+20,20,20);
    }
    else if (IS_IPHONE_6)
    {
        radio_button_for_delivery_charge.frame = CGRectMake(30,CGRectGetMidY(labl_standerd_delivery_charge.frame)+20,20,20);
    }
    else if (IS_IPHONE_5)
    {
        radio_button_for_delivery_charge.frame = CGRectMake(30,CGRectGetMidY(labl_standerd_delivery_charge.frame)+20,20,20);
    }
    else
    {
        radio_button_for_delivery_charge.frame = CGRectMake(30,CGRectGetMidY(labl_standerd_delivery_charge.frame)+15,20,20);
    }
    [radio_button_for_delivery_charge setImage:[UIImage imageNamed:@"icon-sele-radio@2x.png"] forState:UIControlStateSelected];
    [radio_button_for_delivery_charge setImage:[UIImage imageNamed:@"icon-un-selec-radio@2x.png"] forState:UIControlStateNormal];
//    radio_button_for_delivery_charge.tag = 54;
    [radio_button_for_delivery_charge addTarget:self action:@selector(click_on_radio_btn_for_delivery_charge:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_delevery addSubview:  radio_button_for_delivery_charge];
    
    
    UILabel *labl_mile_charge = [[UILabel alloc]init];
    labl_mile_charge.frame = CGRectMake(CGRectGetMaxX(radio_button_for_delivery_charge.frame)+10,CGRectGetMaxY(labl_standerd_delivery_charge.frame)-2,200,35);
    if (IS_IPHONE_6Plus)
    {
        labl_mile_charge.frame = CGRectMake(CGRectGetMaxX(radio_button_for_delivery_charge.frame)+10,CGRectGetMaxY(labl_standerd_delivery_charge.frame)-5,200,40);
    }
    else if (IS_IPHONE_6)
    {
        labl_mile_charge.frame = CGRectMake(CGRectGetMaxX(radio_button_for_delivery_charge.frame)+10,CGRectGetMaxY(labl_standerd_delivery_charge.frame)-5,200,40);
    }
    else if (IS_IPHONE_5)
    {
        labl_mile_charge.frame = CGRectMake(CGRectGetMaxX(radio_button_for_delivery_charge.frame)+10,CGRectGetMaxY(labl_standerd_delivery_charge.frame)-5,200,35);
    }
    else
    {
        labl_mile_charge.frame = CGRectMake(CGRectGetMaxX(radio_button_for_delivery_charge.frame)+10,CGRectGetMaxY(labl_standerd_delivery_charge.frame)-6,200,30);
    }
    labl_mile_charge.text = @"Mile Charge";
    labl_mile_charge.font = [UIFont fontWithName:kFont size:15];
    labl_mile_charge.textColor = [UIColor blackColor];
    labl_mile_charge.backgroundColor = [UIColor clearColor];
    [img_bg_for_delevery addSubview:labl_mile_charge];
    
    
    radio_button_for_delivery_charge2  =  [UIButton buttonWithType:UIButtonTypeCustom];
    radio_button_for_delivery_charge2.frame = CGRectMake(CGRectGetMinX(labl_mile_charge.frame)+100,CGRectGetMidY(labl_standerd_delivery_charge.frame)+20,20,20);
    if (IS_IPHONE_6Plus)
    {
        radio_button_for_delivery_charge2.frame = CGRectMake(CGRectGetMinX(labl_mile_charge.frame)+100,CGRectGetMidY(labl_standerd_delivery_charge.frame)+20,20,20);

    }
    else if (IS_IPHONE_6)
    {
        radio_button_for_delivery_charge2.frame = CGRectMake(CGRectGetMinX(labl_mile_charge.frame)+100,CGRectGetMidY(labl_standerd_delivery_charge.frame)+20,20,20);

    }
    else if (IS_IPHONE_5)
    {
        radio_button_for_delivery_charge2.frame = CGRectMake(CGRectGetMinX(labl_mile_charge.frame)+100,CGRectGetMidY(labl_standerd_delivery_charge.frame)+20,20,20);

    }
    else
    {
        radio_button_for_delivery_charge2.frame = CGRectMake(CGRectGetMinX(labl_mile_charge.frame)+100,CGRectGetMidY(labl_standerd_delivery_charge.frame)+15,20,20);

    }
    [radio_button_for_delivery_charge2 setImage:[UIImage imageNamed:@"icon-sele-radio@2x.png"] forState:UIControlStateSelected];
    [radio_button_for_delivery_charge2 setImage:[UIImage imageNamed:@"icon-un-selec-radio@2x.png"] forState:UIControlStateNormal];
//    radio_button_for_delivery_charge2.tag = 54;
    [radio_button_for_delivery_charge2 addTarget:self action:@selector(click_on_radio_btn_for_delivery_charge2:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_delevery addSubview:  radio_button_for_delivery_charge2];
    
    
    
    UILabel *labl_flat_charge = [[UILabel alloc]init];
    labl_flat_charge.frame = CGRectMake(CGRectGetMaxX(radio_button_for_delivery_charge2.frame)+10,CGRectGetMaxY(labl_standerd_delivery_charge.frame)-5,300,40);
    if (IS_IPHONE_6Plus)
    {
        labl_flat_charge.frame = CGRectMake(CGRectGetMaxX(radio_button_for_delivery_charge2.frame)+10,CGRectGetMaxY(labl_standerd_delivery_charge.frame)-5,300,40);

    }
    else if (IS_IPHONE_6)
    {
        labl_flat_charge.frame = CGRectMake(CGRectGetMaxX(radio_button_for_delivery_charge2.frame)+10,CGRectGetMaxY(labl_standerd_delivery_charge.frame)-5,300,40);

    }
    else if (IS_IPHONE_5)
    {
        labl_flat_charge.frame = CGRectMake(CGRectGetMaxX(radio_button_for_delivery_charge2.frame)+10,CGRectGetMaxY(labl_standerd_delivery_charge.frame)-5,300,40);

    }
    else
    {
        labl_flat_charge.frame = CGRectMake(CGRectGetMaxX(radio_button_for_delivery_charge2.frame)+10,CGRectGetMaxY(labl_standerd_delivery_charge.frame)-6,300,30);

    }
    labl_flat_charge.text = @"Flate Charge";
    labl_flat_charge.font = [UIFont fontWithName:kFont size:15];
    labl_flat_charge.textColor = [UIColor blackColor];
    labl_flat_charge.backgroundColor = [UIColor clearColor];
    [img_bg_for_delevery addSubview:labl_flat_charge];
    
    
    UILabel *labl_first_mile = [[UILabel alloc]init];
    labl_first_mile.frame = CGRectMake(30,CGRectGetMaxY(labl_flat_charge.frame)-5,150,40);
    if (IS_IPHONE_6Plus)
    {
        labl_first_mile.frame = CGRectMake(30,CGRectGetMaxY(labl_flat_charge.frame)-5,150,40);
    }
    else if (IS_IPHONE_6)
    {
        labl_first_mile.frame = CGRectMake(30,CGRectGetMaxY(labl_flat_charge.frame)-5,150,40);
    }
    else if (IS_IPHONE_5)
    {
        labl_first_mile.frame = CGRectMake(30,CGRectGetMaxY(labl_flat_charge.frame)-5,150,40);
    }
    else
    {
        labl_first_mile.frame = CGRectMake(30,CGRectGetMaxY(labl_flat_charge.frame)-2,150,35);
    }
    labl_first_mile.text = @"First mile: $";
    labl_first_mile.font = [UIFont fontWithName:kFont size:15];
    labl_first_mile.textColor = [UIColor blackColor];
    labl_first_mile.backgroundColor = [UIColor clearColor];
    [img_bg_for_delevery addSubview:labl_first_mile];
    
    
   txt_first_mile_charge = [[UITextField alloc] init];
    txt_first_mile_charge.frame = CGRectMake(CGRectGetMidX(labl_first_mile.frame)+5,CGRectGetMaxY(labl_flat_charge.frame)-9,80,45);
    if (IS_IPHONE_6Plus)
    {
        txt_first_mile_charge.frame = CGRectMake(CGRectGetMidX(labl_first_mile.frame)+5,CGRectGetMaxY(labl_flat_charge.frame)-9,80,45);
    }
    else if (IS_IPHONE_6)
    {
        txt_first_mile_charge.frame = CGRectMake(CGRectGetMidX(labl_first_mile.frame)+5,CGRectGetMaxY(labl_flat_charge.frame)-9,80,45);
    }
    else if (IS_IPHONE_5)
    {
        txt_first_mile_charge.frame = CGRectMake(CGRectGetMidX(labl_first_mile.frame)+5,CGRectGetMaxY(labl_flat_charge.frame)-9,80,45);
    }
    else
    {
        txt_first_mile_charge.frame = CGRectMake(CGRectGetMidX(labl_first_mile.frame)+5,CGRectGetMaxY(labl_flat_charge.frame)-7,80,40);
    }
    txt_first_mile_charge .borderStyle = UITextBorderStyleNone;
    txt_first_mile_charge .textColor = [UIColor blackColor];
    txt_first_mile_charge .font = [UIFont fontWithName:kFont size:15];
    txt_first_mile_charge .placeholder = @"";
    [txt_first_mile_charge  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_first_mile_charge  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding30 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_first_mile_charge .leftView = padding30;
    txt_first_mile_charge .leftViewMode = UITextFieldViewModeAlways;
    txt_first_mile_charge .userInteractionEnabled=YES;
    txt_first_mile_charge .textAlignment = NSTextAlignmentLeft;
    txt_first_mile_charge .backgroundColor = [UIColor clearColor];
    txt_first_mile_charge .keyboardType = UIKeyboardTypeAlphabet;
    txt_first_mile_charge .delegate = self;
    [img_bg_for_delevery addSubview:txt_first_mile_charge];
    
    
    
    UIImageView *img_line31 = [[UIImageView alloc]init];
    img_line31.frame = CGRectMake(CGRectGetMidX(labl_first_mile.frame)+10,CGRectGetMidY(txt_first_mile_charge.frame)+10, 50, 0.5);
    if (IS_IPHONE_6Plus)
    {
        img_line31.frame = CGRectMake(CGRectGetMidX(labl_first_mile.frame)+10,CGRectGetMidY(txt_first_mile_charge.frame)+10, 50, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        img_line31.frame = CGRectMake(CGRectGetMidX(labl_first_mile.frame)+10,CGRectGetMidY(txt_first_mile_charge.frame)+10, 50, 0.5);
    }
    else if (IS_IPHONE_5)
    {
        img_line31.frame = CGRectMake(CGRectGetMidX(labl_first_mile.frame)+10,CGRectGetMidY(txt_first_mile_charge.frame)+10, 50, 0.5);
    }
    else
    {
        img_line31.frame = CGRectMake(CGRectGetMidX(labl_first_mile.frame)+10,CGRectGetMidY(txt_first_mile_charge.frame)+5, 50, 0.5);
    }
    [img_line31  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line31  setUserInteractionEnabled:YES];
    [img_bg_for_delevery addSubview:img_line31];
    
    UILabel *labl_each_aditional_mile = [[UILabel alloc]init];
    labl_each_aditional_mile.frame = CGRectMake(30,CGRectGetMaxY(img_line31.frame)-5,200,40);
    if (IS_IPHONE_6Plus)
    {
        labl_each_aditional_mile.frame = CGRectMake(30,CGRectGetMaxY(img_line31.frame)-5,200,40);
    }
    else if (IS_IPHONE_6)
    {
        labl_each_aditional_mile.frame = CGRectMake(30,CGRectGetMaxY(img_line31.frame)-5,200,40);
    }
    else if (IS_IPHONE_5)
    {
        labl_each_aditional_mile.frame = CGRectMake(30,CGRectGetMaxY(img_line31.frame)-5,200,40);
    }
    else
    {
        labl_each_aditional_mile.frame = CGRectMake(30,CGRectGetMaxY(img_line31.frame)-2,200,35);
    }
    labl_each_aditional_mile.text = @"Each additionl mile: $";
    labl_each_aditional_mile.font = [UIFont fontWithName:kFont size:15];
    labl_each_aditional_mile.textColor = [UIColor blackColor];
    labl_each_aditional_mile.backgroundColor = [UIColor clearColor];
    [img_bg_for_delevery addSubview:labl_each_aditional_mile];
    
    
    txt_each_mile_charge = [[UITextField alloc] init];
    txt_each_mile_charge.frame = CGRectMake(CGRectGetMidX(labl_each_aditional_mile.frame)+55,CGRectGetMaxY(img_line31.frame)-5,80,45);
    if (IS_IPHONE_6Plus)
    {
        txt_each_mile_charge.frame = CGRectMake(CGRectGetMidX(labl_each_aditional_mile.frame)+55,CGRectGetMaxY(img_line31.frame)-5,80,45);
    }
    else if (IS_IPHONE_6)
    {
        txt_each_mile_charge.frame = CGRectMake(CGRectGetMidX(labl_each_aditional_mile.frame)+55,CGRectGetMaxY(img_line31.frame)-5,80,45);
    }
    else if (IS_IPHONE_5)
    {
        txt_each_mile_charge.frame = CGRectMake(CGRectGetMidX(labl_each_aditional_mile.frame)+55,CGRectGetMaxY(img_line31.frame)-5,80,45);
    }
    else
    {
        txt_each_mile_charge.frame = CGRectMake(CGRectGetMidX(labl_each_aditional_mile.frame)+55,CGRectGetMaxY(img_line31.frame)-2,80,40);
    }
    txt_each_mile_charge .borderStyle = UITextBorderStyleNone;
    txt_each_mile_charge .textColor = [UIColor blackColor];
    txt_each_mile_charge .font = [UIFont fontWithName:kFont size:15];
    txt_each_mile_charge .placeholder = @"";
    [txt_each_mile_charge  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_each_mile_charge  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding31 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_each_mile_charge .leftView = padding31;
    txt_each_mile_charge .leftViewMode = UITextFieldViewModeAlways;
    txt_each_mile_charge .userInteractionEnabled=YES;
    txt_each_mile_charge .textAlignment = NSTextAlignmentLeft;
    txt_each_mile_charge .backgroundColor = [UIColor clearColor];
    txt_each_mile_charge .keyboardType = UIKeyboardTypeAlphabet;
    txt_each_mile_charge .delegate = self;
    [img_bg_for_delevery addSubview:txt_each_mile_charge];
    
    
    
    UIImageView *img_line32 = [[UIImageView alloc]init];
    img_line32.frame = CGRectMake(CGRectGetMidX(labl_each_aditional_mile.frame)+55,CGRectGetMidY(txt_each_mile_charge.frame)+10, 50, 0.5);
    if (IS_IPHONE_6Plus)
    {
        img_line32.frame = CGRectMake(CGRectGetMidX(labl_each_aditional_mile.frame)+55,CGRectGetMidY(txt_each_mile_charge.frame)+10, 50, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        img_line32.frame = CGRectMake(CGRectGetMidX(labl_each_aditional_mile.frame)+55,CGRectGetMidY(txt_each_mile_charge.frame)+10, 50, 0.5);
    }
    else if (IS_IPHONE_5)
    {
        img_line32.frame = CGRectMake(CGRectGetMidX(labl_each_aditional_mile.frame)+55,CGRectGetMidY(txt_each_mile_charge.frame)+10, 50, 0.5);
    }
    else
    {
        img_line32.frame = CGRectMake(CGRectGetMidX(labl_each_aditional_mile.frame)+55,CGRectGetMidY(txt_each_mile_charge.frame)+5, 50, 0.5);
    }
    [img_line32  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line32  setUserInteractionEnabled:YES];
    [img_bg_for_delevery addSubview:img_line32];
    
    
    UILabel *labl_flat = [[UILabel alloc]init];
    labl_flat.frame = CGRectMake(30,CGRectGetMaxY(img_line32.frame)-5,200,40);
    if (IS_IPHONE_6Plus)
    {
        labl_flat.frame = CGRectMake(30,CGRectGetMaxY(img_line32.frame)-5,200,40);
    }
    else if (IS_IPHONE_6)
    {
        labl_flat.frame = CGRectMake(30,CGRectGetMaxY(img_line32.frame)-5,200,40);
    }
    else if (IS_IPHONE_5)
    {
        labl_flat.frame = CGRectMake(30,CGRectGetMaxY(img_line32.frame)-5,200,40);
    }
    else
    {
        labl_flat.frame = CGRectMake(30,CGRectGetMaxY(img_line32.frame)-2,200,35);
    }
    labl_flat.text = @"Flat Charge: $";
    labl_flat.font = [UIFont fontWithName:kFont size:15];
    labl_flat.textColor = [UIColor blackColor];
    labl_flat.backgroundColor = [UIColor clearColor];
    [img_bg_for_delevery addSubview:labl_flat];
    
    
    txt_flat_charge = [[UITextField alloc] init];
    txt_flat_charge.frame = CGRectMake(CGRectGetMidX(labl_flat.frame)+10,CGRectGetMaxY(img_line32.frame)-8,80,45);
    if (IS_IPHONE_6Plus)
    {
        txt_flat_charge.frame = CGRectMake(CGRectGetMidX(labl_flat.frame)+10,CGRectGetMaxY(img_line32.frame)-8,80,45);
    }
    else if (IS_IPHONE_6)
    {
        txt_flat_charge.frame = CGRectMake(CGRectGetMidX(labl_flat.frame)+10,CGRectGetMaxY(img_line32.frame)-8,80,45);
    }
    else if (IS_IPHONE_5)
    {
        txt_flat_charge.frame = CGRectMake(CGRectGetMidX(labl_flat.frame)+10,CGRectGetMaxY(img_line32.frame)-8,80,45);
    }
    else
    {
        txt_flat_charge.frame = CGRectMake(CGRectGetMidX(labl_flat.frame)+10,CGRectGetMaxY(img_line32.frame)-6,80,40);
    }
    txt_flat_charge .borderStyle = UITextBorderStyleNone;
    txt_flat_charge .textColor = [UIColor blackColor];
    txt_flat_charge .font = [UIFont fontWithName:kFont size:15];
    txt_flat_charge .placeholder = @"";
    [txt_flat_charge  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_flat_charge  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding32 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_flat_charge .leftView = padding32;
    txt_flat_charge .leftViewMode = UITextFieldViewModeAlways;
    txt_flat_charge .userInteractionEnabled=YES;
    txt_flat_charge .textAlignment = NSTextAlignmentLeft;
    txt_flat_charge .backgroundColor = [UIColor clearColor];
    txt_flat_charge .keyboardType = UIKeyboardTypeAlphabet;
    txt_flat_charge .delegate = self;
    [img_bg_for_delevery addSubview:txt_flat_charge];
    
    
    
    UIImageView *img_line33 = [[UIImageView alloc]init];
    img_line33.frame = CGRectMake(CGRectGetMidX(labl_flat.frame)+10,CGRectGetMidY(txt_flat_charge.frame)+10, 50, 0.5);
    if (IS_IPHONE_6Plus)
    {
        img_line33.frame = CGRectMake(CGRectGetMidX(labl_flat.frame)+10,CGRectGetMidY(txt_flat_charge.frame)+10, 50, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        img_line33.frame = CGRectMake(CGRectGetMidX(labl_flat.frame)+10,CGRectGetMidY(txt_flat_charge.frame)+10, 50, 0.5);
    }
    else if (IS_IPHONE_5)
    {
        img_line33.frame = CGRectMake(CGRectGetMidX(labl_flat.frame)+10,CGRectGetMidY(txt_flat_charge.frame)+10, 50, 0.5);
    }
    else
    {
        img_line33.frame = CGRectMake(CGRectGetMidX(labl_flat.frame)+10,CGRectGetMidY(txt_flat_charge.frame)+5, 50, 0.5);
    }
    [img_line33  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line33  setUserInteractionEnabled:YES];
    [img_bg_for_delevery addSubview:img_line33];
    
    
    
    btn_check_box_for_Apply_All  =  [UIButton buttonWithType:UIButtonTypeCustom];
    btn_check_box_for_Apply_All.frame = CGRectMake(100,CGRectGetMidY(img_line33.frame)+15,17,17);
    if (IS_IPHONE_6Plus)
    {
        btn_check_box_for_Apply_All.frame = CGRectMake(170,CGRectGetMidY(img_line33.frame)+15,17,17);
    }
    else if (IS_IPHONE_6)
    {
        btn_check_box_for_Apply_All.frame = CGRectMake(140,CGRectGetMidY(img_line33.frame)+15,17,17);
    }
    else if (IS_IPHONE_5)
    {
        btn_check_box_for_Apply_All.frame = CGRectMake(100,CGRectGetMidY(img_line33.frame)+15,17,17);
    }
    else
    {
        btn_check_box_for_Apply_All.frame = CGRectMake(100,CGRectGetMidY(img_line33.frame)+15,17,17);
    }
    [btn_check_box_for_Apply_All setImage:[UIImage imageNamed:@"img-check-select@2x.png"] forState:UIControlStateSelected];
    [btn_check_box_for_Apply_All setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
//    btn_check_box_for_Apply_All.tag = 54;
    [btn_check_box_for_Apply_All addTarget:self action:@selector(check_box_ApplyAll_Method:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_delevery addSubview:  btn_check_box_for_Apply_All];
    
    
    UILabel *labl_Apply_All = [[UILabel alloc]init];
    labl_Apply_All.frame = CGRectMake(CGRectGetMidX(btn_check_box_for_Apply_All.frame)+13,CGRectGetMidY(img_line33.frame)+3,140,40);
    if (IS_IPHONE_6Plus)
    {
        labl_Apply_All.frame = CGRectMake(CGRectGetMidX(btn_check_box_for_Apply_All.frame)+13,CGRectGetMidY(img_line33.frame)+3,140,40);
    }
    else if (IS_IPHONE_6)
    {
        labl_Apply_All.frame = CGRectMake(CGRectGetMidX(btn_check_box_for_Apply_All.frame)+13,CGRectGetMidY(img_line33.frame)+3,140,40);
    }
    else if (IS_IPHONE_5)
    {
        labl_Apply_All.frame = CGRectMake(CGRectGetMidX(btn_check_box_for_Apply_All.frame)+13,CGRectGetMidY(img_line33.frame)+3,140,40);
    }
    else
    {
        labl_Apply_All.frame = CGRectMake(CGRectGetMidX(btn_check_box_for_Apply_All.frame)+13,CGRectGetMidY(img_line33.frame)+3,140,40);
    }
    labl_Apply_All.text = @"Apply to All";
    labl_Apply_All.font = [UIFont fontWithName:kFontBold size:11];
    labl_Apply_All.textColor = [UIColor blackColor];
    labl_Apply_All.backgroundColor = [UIColor clearColor];
    [img_bg_for_delevery addSubview:labl_Apply_All];

    
    btn_check_box_for_setas_default  =  [UIButton buttonWithType:UIButtonTypeCustom];
    btn_check_box_for_setas_default.frame = CGRectMake(200,CGRectGetMidY(img_line33.frame)+15,16,16);
    if (IS_IPHONE_6Plus)
    {
        btn_check_box_for_setas_default.frame = CGRectMake(270,CGRectGetMidY(img_line33.frame)+15,16,16);
    }
    else if (IS_IPHONE_6)
    {
        btn_check_box_for_setas_default.frame = CGRectMake(230,CGRectGetMidY(img_line33.frame)+15,16,16);
    }
    else if (IS_IPHONE_5)
    {
        btn_check_box_for_setas_default.frame = CGRectMake(200,CGRectGetMidY(img_line33.frame)+15,16,16);
    }
    else
    {
        btn_check_box_for_setas_default.frame = CGRectMake(200,CGRectGetMidY(img_line33.frame)+15,16,16);
    }
    [btn_check_box_for_setas_default setImage:[UIImage imageNamed:@"img-check-select@2x.png"] forState:UIControlStateSelected];
    [btn_check_box_for_setas_default setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
//    btn_check_box_for_setas_default.tag = 54;
    [btn_check_box_for_setas_default addTarget:self action:@selector(click_on_check_box_setAsDefault_btnMethod:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_delevery addSubview:  btn_check_box_for_setas_default];
    
    
    UILabel *labl_set_as_default = [[UILabel alloc]init];
    labl_set_as_default.frame = CGRectMake(CGRectGetMidX(btn_check_box_for_setas_default.frame)+13,CGRectGetMidY(img_line33.frame)+3,140,40);
    if (IS_IPHONE_6Plus)
    {
        labl_set_as_default.frame = CGRectMake(CGRectGetMidX(btn_check_box_for_setas_default.frame)+13,CGRectGetMidY(img_line33.frame)+3,140,40);
    }
    else if (IS_IPHONE_6)
    {
        labl_set_as_default.frame = CGRectMake(CGRectGetMidX(btn_check_box_for_setas_default.frame)+13,CGRectGetMidY(img_line33.frame)+3,140,40);
    }
    else if (IS_IPHONE_5)
    {
        labl_set_as_default.frame = CGRectMake(CGRectGetMidX(btn_check_box_for_setas_default.frame)+13,CGRectGetMidY(img_line33.frame)+3,140,40);
    }
    else
    {
        labl_set_as_default.frame = CGRectMake(CGRectGetMidX(btn_check_box_for_setas_default.frame)+13,CGRectGetMidY(img_line33.frame)+3,140,40);
    }
    labl_set_as_default.text = @"Set as Default";
    labl_set_as_default.font = [UIFont fontWithName:kFontBold size:11];
    labl_set_as_default.textColor = [UIColor blackColor];
    labl_set_as_default.backgroundColor = [UIColor clearColor];
    [img_bg_for_delevery addSubview:labl_set_as_default];

    
    btn_Save_Deliverly_Address  =  [UIButton buttonWithType:UIButtonTypeCustom];
    btn_Save_Deliverly_Address.frame = CGRectMake(100, CGRectGetMaxY(btn_check_box_for_setas_default.frame)+15, WIDTH-200, 30);
    if (IS_IPHONE_6Plus)
    {
         btn_Save_Deliverly_Address.frame = CGRectMake(90, CGRectGetMaxY(btn_check_box_for_setas_default.frame)+15, WIDTH-200, 30);
    }
    else if (IS_IPHONE_6)
    {
        btn_Save_Deliverly_Address.frame = CGRectMake(90, CGRectGetMaxY(btn_check_box_for_setas_default.frame)+15, WIDTH-200, 30);
    }
    else if (IS_IPHONE_5)
    {
        btn_Save_Deliverly_Address.frame = CGRectMake(100, CGRectGetMaxY(btn_check_box_for_setas_default.frame)+15, WIDTH-200, 30);
    }
    else
    {
        btn_Save_Deliverly_Address.frame = CGRectMake(100, CGRectGetMaxY(btn_check_box_for_setas_default.frame)+15, WIDTH-200, 25);
    }
    [btn_Save_Deliverly_Address setTitle:@"Add Address" forState:UIControlStateNormal];
    [btn_Save_Deliverly_Address setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Save_Deliverly_Address setBackgroundColor:[UIColor colorWithRed:39.0/255.0 green:37.0/255.0 blue:48.0/255.0 alpha:1.0f]];
    btn_Save_Deliverly_Address.layer.cornerRadius = 2.0;
    btn_Save_Deliverly_Address.titleLabel.font=[UIFont fontWithName:kFont size:14];
    [btn_Save_Deliverly_Address addTarget:self action:@selector(save_Deliverly_Address_Method:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_delevery addSubview: btn_Save_Deliverly_Address];
    
}


#pragma mark ButtonSelectorMethod

- (void)hideDropDown
{
    //    if ((popUpTabel != nil) && (popUpView != nil))
    //    {
    //        [popUpView removeFromSuperview];
    //    }
    [table_Repeated removeFromSuperview];
}


-(void)save_Deliverly_Address_Method: (UIButton *)sender
{
    
    NSLog(@"save_Deliverly_Address button clicked");
    
    if (txt_delevery_company.text.length<=0)
    {
        [self popup_Alertview:@"please enter delivary company name"];
        
    }else if (txt_tel_code.text.length<=0)
    {
         [self popup_Alertview:@"please enter country code"];
    }
    else if (txt_mobile_number2.text.length<=0)
    {
         [self popup_Alertview:@"please enter mobile number"];
    }
    else if (txt_persone_name.text.length<=0)
    {
         [self popup_Alertview:@"please enter person name"];
    }
    else if (txt_tel_code2.text.length<=0)
    {
         [self popup_Alertview:@"please enter country code"];
    }
    else if (txt_person_mobile_number.text.length<=0)
    {
         [self popup_Alertview:@"please enter person mobile number"];
    }
    else if (txt_first_mile_charge.text.length<=0)
    {
         [self popup_Alertview:@"please enter first mile charge"];
    }
      else if (txt_each_mile_charge.text.length<=0)
    {
         [self popup_Alertview:@"please enter additional charge"];
    }
      else if (txt_flat_charge.text.length<=0)
      {
          [self popup_Alertview:@"please enter flat charge"];
      }

    else{
        [DeliveryPopUpBg removeFromSuperview];
        [self AFAddresslist];
        
    }

    
    
//    img_bg_for_delevery.hidden = YES;
    
}
-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}




-(void)Back_btnClick
{
    //    [self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

-(void)DineIn_btnClick:(UIButton *)sender
{
    if([sender isSelected])
    {
        [sender setSelected:NO];
        str_dineinbody = @"0";
        [btn_Dine_In setBackgroundImage:[UIImage imageNamed:@"img-dine-in@2x"] forState:UIControlStateNormal];
        imgTik1.image = [UIImage imageNamed:@""];
        
    }
    else
    {
        
        [btn_Dine_In setBackgroundImage:[UIImage imageNamed:@"img-dine-in-bl@2x"] forState:UIControlStateNormal];
        imgTik1.image = [UIImage imageNamed:@"icon-right-tik@2x"];
        str_dineinbody = @"1";
        [sender setSelected:YES];
        
    }
    
    
}
-(void)Takeout_btnClick:(UIButton *)sender
{
    
    NSLog(@"TakeOut Button Clicked");
    if([sender isSelected])
    {
        [sender setSelected:NO];
        str_takoutbody = @"0";
        
        [btn_Takeout setBackgroundImage:[UIImage imageNamed:@"img-take-out@2x"] forState:UIControlStateNormal];
        imgTik2.image = [UIImage imageNamed:@""];
        
        
        
    }
    else
    {
        
        str_takoutbody = @"1";
        [btn_Takeout setBackgroundImage:[UIImage imageNamed:@"img-take-out-bl@2x"] forState:UIControlStateNormal];
        imgTik2.image = [UIImage imageNamed:@"icon-right-tik@2x"];
        
        [sender setSelected:YES];
        
    }
    
}

-(void)Delivery_btnClick:(UIButton *)sender
{
    
    NSLog(@"Delevery Button Clicked");
    //    SelectAll_btnClick
    
    if([sender isSelected])
    {
        [sender setSelected:NO];
        str_divarybody = @"0";
        
        [btn_Delivery setBackgroundImage:[UIImage imageNamed:@"img-delever@2x"] forState:UIControlStateNormal];
        imgTik3.image = [UIImage imageNamed:@""];
    }
    else
    {
        
        str_divarybody = @"1";
        
        [btn_Delivery setBackgroundImage:[UIImage imageNamed:@"img-delever-bl@2x"] forState:UIControlStateNormal];
        imgTik3.image = [UIImage imageNamed:@"icon-right-tik@2x"];
        [sender setSelected:YES];
        
    }
    
}

-(void)cross_Method:(UIButton *)sender
{
    [DeliveryPopUpBg removeFromSuperview];
    
}


-(void)SelectAll_btnClick:(UIButton *)sender
{
    
    NSLog(@"SelectAll Button Clicked");
    
    if([sender isSelected])
    {
        [sender setSelected:NO];
        
        [btn_SelectAll setBackgroundImage:[UIImage imageNamed:@"img-check@2x"] forState:UIControlStateNormal];
        
        for (int i=0; i<[ary_maintosave count]; i++)
        {
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[ary_maintosave objectAtIndex:i];
            [dict setObject:@"0" forKey:@"dinein"];
            [dict setObject:@"0" forKey:@"takeout"];
            [dict setObject:@"0" forKey:@"delivery"];
            dict=[ary_maintosave objectAtIndex:i];
        }
        
        [table_DishList reloadData];
        
        
        
          }
    else
    {
        [btn_SelectAll setBackgroundImage:[UIImage imageNamed:@"img_checkok@2x"] forState:UIControlStateNormal];
        
        if ([str_dineinbody isEqualToString:@"1"])
        {
            for (int i=0; i<[ary_maintosave count]; i++)
            {
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                dict=[ary_maintosave objectAtIndex:i];
                [dict setObject:@"1" forKey:@"dinein"];
                dict=[ary_maintosave objectAtIndex:i];
                
            }
            
            [table_DishList reloadData];
        }
        else{
            for (int i=0; i<[ary_maintosave count]; i++)
            {
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                dict=[ary_maintosave objectAtIndex:i];
                [dict setObject:@"0" forKey:@"dinein"];
                dict=[ary_maintosave objectAtIndex:i];
                
            }
            
            [table_DishList reloadData];
            
        }
        
        if ([str_takoutbody isEqualToString:@"1"])
        {
            for (int i=0; i<[ary_maintosave count]; i++)
            {
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                dict=[ary_maintosave objectAtIndex:i];
                [dict setObject:@"1" forKey:@"takeout"];
                dict=[ary_maintosave objectAtIndex:i];
                
            }
            
            [table_DishList reloadData];
        }
        else{
            for (int i=0; i<[ary_maintosave count]; i++)
            {
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                dict=[ary_maintosave objectAtIndex:i];
                [dict setObject:@"0" forKey:@"takeout"];
                dict=[ary_maintosave objectAtIndex:i];
                
            }
            
            [table_DishList reloadData];
            
        }
        
        
        if ([str_divarybody isEqualToString:@"1"])
        {
            for (int i=0; i<[ary_maintosave count]; i++)
            {
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                dict=[ary_maintosave objectAtIndex:i];
                [dict setObject:@"1" forKey:@"delivery"];
                dict=[ary_maintosave objectAtIndex:i];
                
            }
            
            [table_DishList reloadData];
        }else
        {
            for (int i=0; i<[ary_maintosave count]; i++)
            {
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                dict=[ary_maintosave objectAtIndex:i];
                [dict setObject:@"0" forKey:@"delivery"];
                dict=[ary_maintosave objectAtIndex:i];
                
            }
            
            [table_DishList reloadData];
        }
        
        [sender setSelected:YES];
        
    }
    
    
    
}

-(void)click_selectObjectAt1:(UIButton *)sender
{
    
    
    for (int i=0; i<[ary_maintosave count]; i++) {
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[ary_maintosave objectAtIndex:i];
        if (selectedSection ==i)
        {
            if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:selectedSection] valueForKey:@"dinein"]]isEqualToString:@"1"])
            {
                [dict setObject:@"0" forKey:@"dinein"];
                
            }
            else{
                [dict setObject:@"1" forKey:@"dinein"];
            }
            
        }
        else
        {
            
        }
        
    }
    
    [table_DishList reloadData];
    
    

    
}
-(void)click_selectObjectAt2:(UIButton *)sender
{
    
    for (int i=0; i<[ary_maintosave count]; i++) {
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[ary_maintosave objectAtIndex:i];
        if (selectedSection ==i)
        {
            if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:selectedSection] valueForKey:@"takeout"]]isEqualToString:@"1"])
            {
                [dict setObject:@"0" forKey:@"takeout"];
                
            }
            else{
                [dict setObject:@"1" forKey:@"takeout"];
            }
            
        }
        else
        {
            
        }
        
    }
    
    [table_DishList reloadData];
    
   
    NSLog(@"ary ;osu  hgjh %@",ary_maintosave);
    

    
    
}
-(void)click_selectObjectAt3:(UIButton *)sender
{
    
    for (int i=0; i<[ary_maintosave count]; i++) {
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[ary_maintosave objectAtIndex:i];
        if (selectedSection ==i)
        {
            if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:selectedSection] valueForKey:@"delivery"]]isEqualToString:@"1"])
            {
                [dict setObject:@"0" forKey:@"delivery"];
                
            }
            else{
                [dict setObject:@"1" forKey:@"delivery"];
            }
            
        }
        else
        {
            
        }
        
    }
    
    [table_DishList reloadData];
    
    

    
}



-(void)ServeNow_btnClick
{
    
    NSLog(@"ServeNow Button Clicked");
    
}

-(void)delibery_Btn_Method: (UIButton *)sender
{
//    
//    NSLog(@"delibery_Btn Button Clicked");
//    if (![sender isSelected])
//    {
//        [sender setSelected:YES];
//        delivery_Scroll.hidden=NO;
//        
//    }
//    else
//    {
//        [sender setSelected:NO];
//        delivery_Scroll.hidden=YES;
//    }
    
    if ([[NSString stringWithFormat:@"%@",[[ary_maintosave  objectAtIndex:sender.tag] valueForKey:@"tableshow"]]isEqualToString:@"NO"])
    {
        for (int i=0; i<[ary_maintosave count]; i++)
        {
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            
            dict=[ary_maintosave objectAtIndex:i];
            
            for (int i=0; i<[ary_maintosave count]; i++) {
                
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                dict=[ary_maintosave objectAtIndex:i];
                [dict setValue:[NSString  stringWithFormat:@"%@",@"YES"] forKey:@"tableshow"];
                
                [table_DishList reloadData];
                
            }
        }
        
        
    }
    else{
        for (int i=0; i<[ary_maintosave count]; i++)
        {
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            
            dict=[ary_maintosave objectAtIndex:i];
            
            for (int i=0; i<[ary_maintosave count]; i++) {
                
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                dict=[ary_maintosave objectAtIndex:i];
                [dict setValue:[NSString  stringWithFormat:@"%@",@"NO"] forKey:@"tableshow"];
                
                [table_DishList reloadData];
                
            }
        }
        
        
    }
    

}

-(void)apply_Btn_Method: (UIButton *)sender
{
    
    NSLog(@"apply_Btn Button Clicked");
    
    NSString*str_add = [NSString stringWithFormat:@"%@",[[ary_delivary objectAtIndex:sender.tag] valueForKey:@"Company_name"]];
    NSString*str_id = [NSString stringWithFormat:@"%@",[[ary_delivary objectAtIndex:sender.tag] valueForKey:@"DeliveryID"]];

    
    if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:sender.tag] valueForKey:@"delivaryidselected"]]isEqualToString:@"1"])
    {
        for (int i=0; i<[ary_maintosave count]; i++)
        {
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[ary_maintosave objectAtIndex:i];
            [dict setValue:@"" forKey:@"Delivery_Address"];
            [dict setValue:@"0" forKey:@"delivaryid"];
            [dict setValue:[NSString  stringWithFormat:@"%@",@"0"] forKey:@"delivaryidselected"];
            dict=[ary_maintosave objectAtIndex:i];
            
            [table_DishList reloadData];
        }
    }
    else
    {
        for (int i=0; i<[ary_maintosave count]; i++)
        {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[ary_maintosave objectAtIndex:i];
            [dict setValue:str_add forKey:@"Delivery_Address"];
            [dict setValue:str_id forKey:@"delivaryid"];
            [dict setValue:[NSString  stringWithFormat:@"%@",@"1"] forKey:@"delivaryidselected"];
            dict=[ary_maintosave objectAtIndex:i];
            
            [table_DishList reloadData];
        }
        
        
    }

    
}

-(void)repeat_BtnMethod: (UIButton *)sender
{
    
    NSLog(@"repeat_Btn Button Clicked");
//    table_Repeated.hidden=YES;
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        table_Repeated.hidden=NO;
        
    }
    else
    {
        [sender setSelected:NO];
        table_Repeated.hidden=YES;
    }
    
}

-(void)click_on_pluse_btn:(UIButton *)sender
{
    NSLog(@"pluse_btn Delevery View");
}

-(void)click_on_update_indelivery_btn:(UIButton *)sender
{
    NSLog(@"update_indelivery_btn Delevery View");
}

-(void)removeCell_Btn_Method: (UIButton *)sender
{
    NSLog(@"removeCell Button Clicked ");
}

-(void)tel_code_dropdownMethod: (UIButton *)sender
{
    NSLog(@")click_on_tel_code_drop_down Button ");
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        table_for_contry_code1.hidden = NO;
    }
    else
    {
        [sender setSelected:NO];
        table_for_contry_code1.hidden = YES;
    }
    
}

-(void)tel_code_dropdown2Method: (UIButton *)sender
{
    NSLog(@")click_on_tel_code_drop_down Button ");
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        table_for_contry_code2.hidden = NO;
        
    }
    else
    {
        [sender setSelected:NO];
        table_for_contry_code2.hidden = YES;
    }
    
}


-(void)click_on_radio_btn_for_delivery_charge: (UIButton *)sender
{
    NSLog(@"radio_button_for_delivery_charge Clicked ");
    [radio_button_for_delivery_charge setSelected:YES];
    [radio_button_for_delivery_charge2 setSelected:NO];
}

-(void)click_on_radio_btn_for_delivery_charge2: (UIButton *)sender
{
    NSLog(@"radio_button_for_delivery_charge Clicked ");
    [radio_button_for_delivery_charge setSelected:NO];
    [radio_button_for_delivery_charge2 setSelected:YES];
  
}

-(void)click_on_check_box_setAsDefault_btnMethod: (UIButton *)sender
{
    if([sender isSelected])
    {
        [sender setSelected:NO];
        [btn_check_box_for_setas_default  setSelected:NO];
        str_delivarypopdefault = @"0";
    }
    else
    {
        [btn_check_box_for_setas_default  setSelected:YES];
        str_delivarypopdefault = @"1";
        [sender setSelected:YES];
        
    }

}

-(void)check_box_ApplyAll_Method: (UIButton *)sender
{
    NSLog(@"check_box_ApplyAll Button Clicked ");

    
    if([sender isSelected])
    {
        [sender setSelected:NO];
        [btn_check_box_for_Apply_All  setSelected:NO];
        str_delivarypopselectall = @"0";
    }
    else
    {
        [btn_check_box_for_Apply_All  setSelected:YES];
        str_delivarypopselectall = @"1";
        [sender setSelected:YES];
        
    }

 
}

-(void)get_Deliver_Address_BtnMethod:(UIButton *)sender
{
//    NSLog(@")get_Deliver_Address Button Clicked");
//    str_Delivery_Address = [NSString stringWithFormat:@"%@", [[ary_Delivery_Address objectAtIndex:sender.tag] valueForKey:@"Company_name"]];
//    //companyNameLbl.text = str_Delivery_Address ;
//    //delivery_Scroll.hidden = YES;
//    img_bg_for_delevery.hidden = YES;
//    if ( [str_Delivery_Address isEqualToString:@"Add delivary address"])
//    {
////        img_bg_for_delevery.hidden = NO;
//        [self deliveryDesign];
//    }
//    else
//    {
//        [DeliveryPopUpBg removeFromSuperview];
////        img_bg_for_delevery.hidden = YES;
//    }
//    
//    
//    
//    int val = [sender.titleLabel.text intValue];
    int val = [sender.titleLabel.text intValue];
    NSLog(@"sender ta %ld",(long)sender.tag);
    
    
    int totalPage;
    
    totalPage = (int)[[[ary_mainfromWS objectAtIndex:val] valueForKey:@"addressdetails"] count]+1;
    
    if (sender.tag==totalPage-1)
    {
        
        [self deliveryDesign];
        
    }
    else
    {
        str_Delivery_Address = [NSString stringWithFormat:@"%@", [[[[ary_mainfromWS objectAtIndex:val] valueForKey:@"addressdetails"] objectAtIndex:sender.tag] valueForKey:@"address"]];
        NSString*str_id =[NSString stringWithFormat:@"%@", [[[[ary_mainfromWS objectAtIndex:val] valueForKey:@"addressdetails"] objectAtIndex:sender.tag] valueForKey:@"id"]];
        
        
        img_bg_for_delevery.hidden = YES;
        [DeliveryPopUpBg removeFromSuperview];
        
        for (int i=0; i<[ary_maintosave count]; i++) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[ary_maintosave objectAtIndex:i];
            
            if (sender.tag ==i)
            {
                [dict setValue:str_Delivery_Address forKey:@"Delivery_Address"];
                [dict setValue:str_id forKey:@"delivaryid"];
                [dict setValue:[NSString  stringWithFormat:@"%@",@"YES"] forKey:@"tableshow"];
            }
            else{
                [dict setValue:@"" forKey:@"Delivery_Address"];
                [dict setValue:@"0" forKey:@"delivaryid"];
            }
            
            [table_DishList reloadData];
            
        }
        
    }
    
    
}


#pragma mark --CalenderMethod--

- (void)localeDidChange {
    [self.calendar setLocale:[NSLocale currentLocale]];
}

- (BOOL)dateIsDisabled:(NSDate *)date
{
    for (NSDate *disabledDate in self.enabledDates)
    {
        if ([disabledDate isEqualToDate:date])
        {
            return YES;
        }
    }
    return NO;
}


#pragma mark - CKCalendarDelegate
- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)myDate
{
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd/MM/yyyy"];
    
    NSString *str_SelectedDate = [format stringFromDate:myDate];
    
}

#pragma mark TableviewDelegate&DataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == table_DishList)
    {
        return [ary_maintosave count];
    }
    else
    {
        return 1.0;
    }
    return 1.0;

    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView == table_DishList)
    {
        //        return 1;
        NSInteger i;
        i = 0;
        if (section == selectedSection)
        {
            i = 1.0;
        }
        return i;
    }
    else if (tableView == table_Preview)
    {
        return [ary_maintosave count];
    }

    else if (tableView == table_Selected_Date)
    {
        return 2;
    }
    else if (tableView == table_Repeated)
    {
       return [repeat_Arr count];
    }
    else if ( tableView == table_for_delevery)
    {
        return 2;
    }
    else if (tableView == table_for_contry_code1)
    {
        return [arrayCountry count];
    }
    else if (tableView == table_for_contry_code2)
    {
        return [arrayCountry count];
    }
    return 1;
    
}

-(CGFloat)tableView: (UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == table_DishList)
    {
        return 53.0;
    }
    return 0.0;
}

-(UIView *)tableView : (UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView*sectionView;
    sectionView = [[UIView alloc]init];
    sectionView.frame = CGRectMake(0, 0, WIDTH, table_DishList.sectionHeaderHeight);
    sectionView.backgroundColor = [UIColor whiteColor];
    
    
    if (tableView == table_DishList)
    {
        UIImageView *section_BgImg = [[UIImageView alloc]init];
        section_BgImg.frame = CGRectMake(0, 0,WIDTH ,53);
        section_BgImg.image = [UIImage imageNamed:@"img_background@2x."];
        //        section_BgImg.layer.borderWidth = 1.0;
        [sectionView addSubview:section_BgImg];
        
        
        //    UIImageView *selected_Img = [[UIImageView alloc]init];
        //    selected_Img.frame = CGRectMake(0, 0,WIDTH ,53);
        //    selected_Img.backgroundColor = [UIColor colorWithRed:215.0/255.0 green:232.0/255.0 blue:242.0/255.0 alpha:1.0];
        //        section_BgImg.layer.borderWidth = 1.0;
        //    [sectionView addSubview:selected_Img];
        
        
        section_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
        section_Btn.frame = CGRectMake(0, 0,WIDTH ,47);
        [section_Btn addTarget:self action:@selector(section_BtnMethod:) forControlEvents:UIControlEventTouchUpInside];
        section_Btn.tag = section;
        [sectionView addSubview: section_Btn];
        
        
        UILabel *firstLabel = [[UILabel alloc]init];
        firstLabel.frame = CGRectMake(35, 0, (WIDTH-70)/2.0, 47);
        firstLabel.backgroundColor = [UIColor clearColor];
        firstLabel.text = [[ary_maintosave  objectAtIndex:section] valueForKey:@"DishName"];
        //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
        //    firstLabel.textAlignment = NSTextAlignmentCenter;
        firstLabel.font = [UIFont fontWithName:kFontBold size:15];
        firstLabel.lineBreakMode = NSLineBreakByWordWrapping;
        firstLabel.numberOfLines = 0;
        [sectionView addSubview:firstLabel];
        
        
        UILabel *secondLabel = [[UILabel alloc]init];
        secondLabel.frame = CGRectMake((WIDTH-70)/2.0+45, 0, (WIDTH-70)/2.0-35, 47);
        secondLabel.text = [NSString stringWithFormat:@"x%@",[[ary_maintosave  objectAtIndex:section] valueForKey:@"quantity"]];
        //    secondLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"subject"];
        //    secondLabel.textAlignment = NSTextAlignmentCenter;
        //    secondLabel.backgroundColor = [UIColor clearColor];
        //    secondLabel.textColor = [UIColor blackColor];
        secondLabel.font = [UIFont fontWithName:kFont size:15];
        secondLabel.lineBreakMode = NSLineBreakByWordWrapping;
        secondLabel.numberOfLines = 0;
        [sectionView addSubview:secondLabel];
        
        
        UIImageView *dd_BgImg = [[UIImageView alloc]init];
        dd_BgImg.frame = CGRectMake(WIDTH-50, 20, 20 ,10);
        dd_BgImg.backgroundColor = [UIColor clearColor];
        dd_BgImg.image = [UIImage imageNamed:@"drop-down-icon@2x"];
        [sectionView addSubview:dd_BgImg];
        
    }
    return sectionView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    }
    else
    {
        for (UIView *view in cell.contentView.subviews)
            [view removeFromSuperview];
    }
    
    if (tableView == table_Preview)
    {
        UIView *cellView = [[UIView alloc]init];
        cellView.frame = CGRectMake(0, 0, table_Preview.frame.size.width, table_Preview.rowHeight);
        cellView.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:cellView];
        
        
        UILabel *titleLbl = [[UILabel alloc]init];
        //        titleLbl.frame = CGRectMake(15, 5, 120, 40);
        if (IS_IPHONE_6Plus)
        {
            titleLbl.frame = CGRectMake(25, 5, 145, 40);
        }
        else if (IS_IPHONE_6)
        {
            titleLbl.frame = CGRectMake(20, 5, 140, 40);
        }
        else if (IS_IPHONE_5)
        {
            titleLbl.frame = CGRectMake(15, 5, 120, 40);
        }
        else
        {
            titleLbl.frame = CGRectMake(15, 5, 120, 40);
        }
        titleLbl.backgroundColor = [UIColor clearColor];
        titleLbl.text = [[ary_maintosave  objectAtIndex:indexPath.row] valueForKey:@"DishName"];
        titleLbl.textColor = [UIColor blackColor];
        titleLbl.font = [UIFont fontWithName:kFontBold size:16];
        titleLbl.lineBreakMode = NSLineBreakByWordWrapping;
        titleLbl.numberOfLines = 0;
        [cellView addSubview:titleLbl];
        titleLbl = nil;
        
        UILabel *subTitleLbl = [[UILabel alloc]init];
        //        subTitleLbl.frame = CGRectMake(135, 5, 20 , 40);
        if (IS_IPHONE_6Plus)
        {
            subTitleLbl.frame = CGRectMake(170, 5, 25 , 40);
        }
        else if (IS_IPHONE_6)
        {
            subTitleLbl.frame = CGRectMake(160, 5, 25 , 40);
        }
        else if (IS_IPHONE_5)
        {
            subTitleLbl.frame = CGRectMake(135, 5, 20 , 40);
        }
        else
        {
            subTitleLbl.frame = CGRectMake(135, 5, 20 , 40);
        }
        subTitleLbl.backgroundColor = [UIColor clearColor];
        subTitleLbl.text =[NSString stringWithFormat:@"x%@",[[ary_maintosave  objectAtIndex:indexPath.row] valueForKey:@"quantity"]];
        subTitleLbl.textColor = [UIColor blackColor];
        subTitleLbl.textAlignment = NSTextAlignmentCenter;
        subTitleLbl.font = [UIFont fontWithName:kFont size:16];
        subTitleLbl.lineBreakMode = NSLineBreakByWordWrapping;
        subTitleLbl.numberOfLines = 0;
        [cellView addSubview:subTitleLbl];
        subTitleLbl = nil;
        
        
        UIImageView *img1 = [[UIImageView alloc]init];
        //        img1.frame = CGRectMake(165, 8, 35 , 32);
        if (IS_IPHONE_6Plus)
        {
            img1.frame = CGRectMake(230, 8, 35 , 32);
        }
        else if (IS_IPHONE_6)
        {
            img1.frame = CGRectMake(205, 8, 35 , 32);
        }
        else if (IS_IPHONE_5)
        {
            img1.frame = CGRectMake(165, 8, 35 , 32);
        }
        else
        {
            img1.frame = CGRectMake(165, 8, 35 , 32);
        }
        // img1.image = [UIImage imageNamed:@"img-dine-in@2x"];
        img1.backgroundColor = [UIColor clearColor];
        //img1.layer.borderWidth = 1.0;
        [cell.contentView addSubview:img1];
        
        UIImageView *img2 = [[UIImageView alloc]init];
        //        img2.frame = CGRectMake(202, 8, 35 , 32);
        if (IS_IPHONE_6Plus)
        {
            img2.frame = CGRectMake(267, 8, 35 , 32);
        }
        else if (IS_IPHONE_6)
        {
            img2.frame = CGRectMake(242, 8, 35 , 32);
        }
        else if (IS_IPHONE_5)
        {
            img2.frame = CGRectMake(202, 8, 35 , 32);
        }
        else
        {
            img2.frame = CGRectMake(202, 8, 35 , 32);
        }
        
        // img2.image = [UIImage imageNamed:@"img-take-out@2x"];
        img2.backgroundColor = [UIColor clearColor];
        // img2.layer.borderWidth = 1.0;
        [cell.contentView addSubview:img2];
        
        UIImageView *img3 = [[UIImageView alloc]init];
        //        img3.frame = CGRectMake(239, 8, 35 , 32);
        if (IS_IPHONE_6Plus)
        {
            img3.frame = CGRectMake(304, 8, 35 , 32);
        }
        else if (IS_IPHONE_6)
        {
            img3.frame = CGRectMake(279, 8, 35 , 32);
        }
        else if (IS_IPHONE_5)
        {
            img3.frame = CGRectMake(239, 8, 35 , 32);
        }
        else
        {
            img3.frame = CGRectMake(239, 8, 35 , 32);
        }
        
        // img3.image = [UIImage imageNamed:@"img-delever@2x"];
        img3.backgroundColor = [UIColor clearColor];
        //img3.layer.borderWidth = 1.0;
        [cell.contentView addSubview:img3];
        
        
        if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:indexPath.row] valueForKey:@"dinein"]]isEqualToString:@"1"])
        {
            img1.image = [UIImage imageNamed:@"img-dine-in-bl@2x"];
        }
        else{
            
            img1.image = [UIImage imageNamed:@"img-dine-in@2x"];
        }
        if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:indexPath.row] valueForKey:@"takeout"]]isEqualToString:@"1"])
        {
            img2.image = [UIImage imageNamed:@"img-take-out-bl@2x"];
            
        }
        else{
            
            img2.image = [UIImage imageNamed:@"img-take-out@2x"];
        }
        if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:indexPath.row] valueForKey:@"delivery"]]isEqualToString:@"1"])
        {
            img3.image = [UIImage imageNamed:@"img-delever-bl@2x"];
            
        }
        else{
            
            img3.image = [UIImage imageNamed:@"img-delever@2x"];
        }
        
    }
   
    
    else if (tableView == table_for_contry_code1)
    {
        
        UILabel *titleLbl = [[UILabel alloc]init];
        titleLbl.frame = CGRectMake(0, 0, 65, 23);
        titleLbl.backgroundColor = [UIColor whiteColor];
        titleLbl.text = [[arrayCountry objectAtIndex:indexPath.row]valueForKey:@"id"];
        titleLbl.textColor = [UIColor blackColor];
        titleLbl.font = [UIFont fontWithName:kFont size:13];
        titleLbl.lineBreakMode = NSLineBreakByWordWrapping;
        titleLbl.numberOfLines = 0;
        [cell.contentView addSubview:titleLbl];

//        cell.textLabel.text = [[arrayCountry objectAtIndex:indexPath.row]valueForKey:@"id"];
//        cell.textLabel.font = [UIFont fontWithName:kFont size:13];
//        cell.textLabel.textColor = [UIColor blackColor];
//        cell.backgroundColor = [UIColor whiteColor];
        
        UIImageView *imgLine=[[UIImageView alloc]init];
        imgLine.frame=CGRectMake( 0, 25, table_for_contry_code1.frame.size.width , 0.5 );
        //    if (IS_IPHONE_6Plus)
        //    {
        //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH+10,130);
        //    }
        //    else if (IS_IPHONE_6)
        //    {
        //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH+10,120);
        //    }
        //    else if (IS_IPHONE_5)
        //    {
        //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame),WIDTH+10,100);
        //    }
        //    else
        //    {
        //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame),WIDTH+10,100);
        //    }
        [imgLine setUserInteractionEnabled:YES];
        imgLine.image = [UIImage imageNamed:@"line-img@2x"];
        //    imgLine.layer.borderWidth = 1.0;
        [cell.contentView addSubview:imgLine];
    }
    
    else if (tableView == table_for_contry_code2)
    {
//        cell.textLabel.text = [[arrayCountry objectAtIndex:indexPath.row]valueForKey:@"id"];
//        cell.textLabel.font = [UIFont fontWithName:kFont size:13];
//        cell.textLabel.textColor = [UIColor blackColor];
//        cell.backgroundColor = [UIColor whiteColor];

        
        UILabel *titleLbl = [[UILabel alloc]init];
        titleLbl.frame = CGRectMake(0, 0, 65, 23);
        titleLbl.backgroundColor = [UIColor whiteColor];
        titleLbl.text = [[arrayCountry objectAtIndex:indexPath.row]valueForKey:@"id"];
        titleLbl.textColor = [UIColor blackColor];
        titleLbl.font = [UIFont fontWithName:kFont size:13];
        titleLbl.lineBreakMode = NSLineBreakByWordWrapping;
        titleLbl.numberOfLines = 0;
        [cell.contentView addSubview:titleLbl];

        
        UIImageView *imgLine=[[UIImageView alloc]init];
        imgLine.frame=CGRectMake( 0, 25, table_for_contry_code2.frame.size.width , 0.5 );
        //    if (IS_IPHONE_6Plus)
        //    {
        //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH+10,130);
        //    }
        //    else if (IS_IPHONE_6)
        //    {
        //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH+10,120);
        //    }
        //    else if (IS_IPHONE_5)
        //    {
        //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame),WIDTH+10,100);
        //    }
        //    else
        //    {
        //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame),WIDTH+10,100);
        //    }
        [imgLine setUserInteractionEnabled:YES];
        imgLine.image = [UIImage imageNamed:@"line-img@2x"];
        //    imgLine.layer.borderWidth = 1.0;
        [cell.contentView addSubview:imgLine];
    }
    
    else if (tableView == table_DishList)
    {
        UIView *cellView = [[UIView alloc]init];
        cellView.frame = CGRectMake(0, 0, table_DishList.frame.size.width, table_DishList.rowHeight);
        cellView.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:cellView];
        
        
        UILabel *titleLbl = [[UILabel alloc]init];
        titleLbl.frame = CGRectMake(0, 5, WIDTH , 20);
        titleLbl.backgroundColor = [UIColor clearColor];
        titleLbl.text = @"Serving Type";
        titleLbl.textColor = [UIColor blackColor];
        titleLbl.textAlignment = NSTextAlignmentCenter;
        titleLbl.font = [UIFont fontWithName:kFontBold size:16];
        titleLbl.lineBreakMode = NSLineBreakByWordWrapping;
        titleLbl.numberOfLines = 0;
        [cellView addSubview:titleLbl];
        titleLbl = nil;
        
        
        UIImageView *img1 = [[UIImageView alloc]init];
        img1.frame = CGRectMake(10, 30, WIDTH-20 , 80);
        img1.backgroundColor = [UIColor clearColor];
        img1.image = [UIImage imageNamed:@"bg-img@2x"];
        img1.userInteractionEnabled = YES;
        
        //        img1.layer.borderWidth = 1.0;
        [cell.contentView addSubview:img1];
        
        //        UICollectionViewFlowLayout*layout3;
        //
        //        layout3 = [[UICollectionViewFlowLayout alloc] init];
        //        coll_delivary = [[UICollectionView alloc] initWithFrame:CGRectMake(0,30,WIDTH-20,80)
        //                                           collectionViewLayout:layout3];
        //        [layout3 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        //        [coll_delivary setDataSource:self];
        //        [coll_delivary setDelegate:self];
        //        coll_delivary.scrollEnabled = YES;
        //        coll_delivary.showsVerticalScrollIndicator = YES;
        //        coll_delivary.showsHorizontalScrollIndicator = NO;
        //        coll_delivary.pagingEnabled = YES;
        //        [coll_delivary registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        //        [coll_delivary setBackgroundColor:[UIColor clearColor]];
        //        layout3.minimumInteritemSpacing = 0;
        //        layout3.minimumLineSpacing = 0;
        //        coll_delivary.userInteractionEnabled = YES;
        //        [cell.contentView  addSubview: coll_delivary];
        
        
        //        img1=nil;
        
        
        
//        UIImageView *imgCheck1;
//        UIImageView *imgCheck2;
//        UIImageView *imgCheck3;
        
        UIImageView *imgDine = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            imgDine.frame = CGRectMake(45, 15, 55 , 55);
        }
        else if (IS_IPHONE_6)
        {
            imgDine.frame = CGRectMake(40, 15, 55 , 55);
        }
        else if (IS_IPHONE_5)
        {
            imgDine.frame = CGRectMake(30, 15, 50 , 50);
        }
        else
        {
            imgDine.frame = CGRectMake(30, 15, 50 , 50);
        }
        //        imgDine.frame = CGRectMake(30, 15, 50 , 50);
        imgDine.backgroundColor = [UIColor clearColor];
        
        
        
        //        imgDine.layer.borderWidth = 1.0;
        imgDine.userInteractionEnabled = YES;
        
        [img1 addSubview:imgDine];
        
        
        imgCheck1 = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            imgCheck1.frame = CGRectMake(73, 40, 27 , 20);
        }
        else if (IS_IPHONE_6)
        {
            imgCheck1.frame = CGRectMake(68, 40, 27 , 20);
        }
        else if (IS_IPHONE_5)
        {
            imgCheck1.frame = CGRectMake(55, 38, 25 , 20);
        }
        else
        {
            imgCheck1.frame = CGRectMake(55, 38, 25 , 20);
        }
        imgCheck1.backgroundColor = [UIColor clearColor];
        //        imgCheck1.layer.borderWidth = 1.0;
        imgCheck1.userInteractionEnabled = YES;
        
        [img1 addSubview:imgCheck1];
        
        
        
        if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:selectedSection] valueForKey:@"dinein"]]isEqualToString:@"1"])
        {
            imgDine.image = [UIImage imageNamed:@"img-dine-in-bl@2x"];
            imgCheck1.image = [UIImage imageNamed:@"icon-right-tik@2x"];
        }
        else{
            
            imgDine.image = [UIImage imageNamed:@"img-dine-in@2x"];
            imgCheck1.image = [UIImage imageNamed:@""];
        }
        
        //            if (indexPath.row ==selectedSection)
        //            {
        //
        //                if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:indexPath.row] valueForKey:@"dinein"]]isEqualToString:@"1"])
        //                {
        //                    imgDine.image = [UIImage imageNamed:@"img-dine-in-bl@2x"];
        //                    imgCheck1.image = [UIImage imageNamed:@"icon-right-tik@2x"];
        //
        //                }
        //                else
        //                {
        //                    if ([str_dineinbody isEqualToString:@"1"])
        //                    {
        //                        imgDine.image = [UIImage imageNamed:@"img-dine-in-bl@2x"];
        //                        imgCheck1.image = [UIImage imageNamed:@"icon-right-tik@2x"];
        //
        //                    }else
        //                    {
        //
        //                        imgDine.image = [UIImage imageNamed:@"img-dine-in@2x"];
        //                        imgCheck1.image = [UIImage imageNamed:@""];
        //
        //                    }
        //                }
        //
        //
        //            }
        //            else
        //            {
        //
        //
        //                if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:indexPath.row] valueForKey:@"dinein"]]isEqualToString:@"1"])
        //                {
        //                    if ([str_dineinbody isEqualToString:@"1"])
        //                    {
        //                        imgDine.image = [UIImage imageNamed:@"img-dine-in-bl@2x"];
        //                        imgCheck1.image = [UIImage imageNamed:@"icon-right-tik@2x"];
        //
        //                    }else
        //                    {
        //                        imgDine.image = [UIImage imageNamed:@"img-dine-in@2x"];
        //                        imgCheck1.image = [UIImage imageNamed:@""];
        //
        //                    }
        //                }
        //                else
        //                {
        //                    imgDine.image = [UIImage imageNamed:@"img-dine-in@2x"];
        //                    imgCheck1.image = [UIImage imageNamed:@""];
        //
        //
        //                }
        //            }
        
        //        }
        //        else{
        //            if (indexPath.row ==selectedSection)
        //            {
        //                imgDine.image = [UIImage imageNamed:@"img-dine-in@2x"];
        //                imgCheck1.image = [UIImage imageNamed:@""];
        //
        //
        //            }
        //            else
        //            {
        //                if ([str_dineinbody isEqualToString:@"1"])
        //                {
        //                    imgDine.image = [UIImage imageNamed:@"img-dine-in-bl@2x"];
        //                    imgCheck1.image = [UIImage imageNamed:@"icon-right-tik@2x"];
        //
        //                }else
        //                {
        //                    imgDine.image = [UIImage imageNamed:@"img-dine-in@2x"];
        //                    imgCheck1.image = [UIImage imageNamed:@""];
        //
        //
        //                }
        //
        //
        //            }
        //        }
        //
        
        
        //        if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:indexPath.row] valueForKey:@"dinein"]]isEqualToString:@"1"])
        //        {
        //            if (indexPath.row ==selectedSection)
        //            {
        //                imgDine.image = [UIImage imageNamed:@"img-dine-in-bl@2x"];
        //                imgCheck1.image = [UIImage imageNamed:@"icon-right-tik@2x"];
        //
        //
        //            }
        //            else
        //            {
        //                if ([str_dineinbody isEqualToString:@"1"])
        //                {
        //                    imgDine.image = [UIImage imageNamed:@"img-dine-in-bl@2x"];
        //                    imgCheck1.image = [UIImage imageNamed:@"icon-right-tik@2x"];
        //
        //                }else
        //                {
        //                    imgDine.image = [UIImage imageNamed:@"img-dine-in@2x"];
        //                    imgCheck1.image = [UIImage imageNamed:@""];
        //
        //                }
        //
        //            }
        //
        //        }
        //        else{
        //            if (indexPath.row ==selectedSection)
        //            {
        //                imgDine.image = [UIImage imageNamed:@"img-dine-in@2x"];
        //                imgCheck1.image = [UIImage imageNamed:@""];
        //
        //
        //            }
        //            else
        //            {
        //                if ([str_dineinbody isEqualToString:@"1"])
        //                {
        //                    imgDine.image = [UIImage imageNamed:@"img-dine-in-bl@2x"];
        //                    imgCheck1.image = [UIImage imageNamed:@"icon-right-tik@2x"];
        //
        //                }else
        //                {
        //                    imgDine.image = [UIImage imageNamed:@"img-dine-in@2x"];
        //                    imgCheck1.image = [UIImage imageNamed:@""];
        //
        //
        //                }
        //
        //
        //            }
        //        }
        
        // UIButton *btn_TableCell_1 = [[UIButton alloc]init];
        UIButton *btn_TableCell_1 =[UIButton buttonWithType:UIButtonTypeCustom];
        
        if (IS_IPHONE_6Plus)
        {
            //        img_lineimage.frame=CGRectMake(10, 49, WIDTH-40, 0.5);
            btn_TableCell_1.frame=CGRectMake(45, 15, 55 , 55);
            
        }
        else if (IS_IPHONE_6)
        {
            btn_TableCell_1.frame=CGRectMake(40, 15, 55 , 55);
        }
        else
        {
            btn_TableCell_1.frame=CGRectMake(30, 15, 50 , 50);
        }
        btn_TableCell_1.backgroundColor=[UIColor clearColor];
        //    [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img.checkcorrect@2x.png"] forState:UIControlStateSelected];
        //    [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
        btn_TableCell_1.tag = indexPath.row;
        [btn_TableCell_1 addTarget:self action:@selector(click_selectObjectAt1:) forControlEvents:UIControlEventTouchUpInside];
        
        [img1 addSubview:btn_TableCell_1];
        
        //
        //
        //
        UIImageView *imgTake = [[UIImageView alloc]init];
        //        imgTake.frame = CGRectMake(125, 10, 50 , 55);
        if (IS_IPHONE_6Plus)
        {
            imgTake.frame = CGRectMake(170, 10, 55 , 60);
        }
        else if (IS_IPHONE_6)
        {
            imgTake.frame = CGRectMake(150, 10, 55 , 60);
        }
        else if (IS_IPHONE_5)
        {
            imgTake.frame = CGRectMake(125, 10, 50 , 55);
        }
        else
        {
            imgTake.frame = CGRectMake(125, 10, 50 , 55);
        }
        imgTake.backgroundColor = [UIColor clearColor];
        
        
        //        imgTake.layer.borderWidth = 1.0;
        imgTake.userInteractionEnabled = YES;
        
        [img1 addSubview:imgTake];
        
        
        imgCheck2 = [[UIImageView alloc]init];
        //        imgCheck2.frame = CGRectMake(150, 38, 25 , 20);
        if (IS_IPHONE_6Plus)
        {
            imgCheck2.frame = CGRectMake(197, 40, 27 , 20);
        }
        else if (IS_IPHONE_6)
        {
            imgCheck2.frame = CGRectMake(177, 40, 27 , 20);
        }
        else if (IS_IPHONE_5)
        {
            imgCheck2.frame = CGRectMake(150, 38, 25 , 20);
        }
        else
        {
            imgCheck2.frame = CGRectMake(150, 38, 25 , 20);
        }
        imgCheck2.backgroundColor = [UIColor clearColor];
        //        imgCheck2.layer.borderWidth = 1.0;
        imgCheck2.userInteractionEnabled = YES;
        
        [img1 addSubview:imgCheck2];
        
        if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:selectedSection] valueForKey:@"takeout"]]isEqualToString:@"1"])
        {
            imgTake.image = [UIImage imageNamed:@"img-take-out-bl@2x"];
            imgCheck2.image = [UIImage imageNamed:@"icon-right-tik@2x"];
            
        }
        else{
            
            imgTake.image = [UIImage imageNamed:@"img-take-out@2x"];
            imgCheck2.image = [UIImage imageNamed:@""];
        }
        
        
        //        if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:indexPath.row] valueForKey:@"takeout"]]isEqualToString:@"1"])
        //        {
        //
        //            if (indexPath.row ==selectedSection)
        //            {
        //                imgTake.image = [UIImage imageNamed:@"img-take-out-bl@2x"];
        //                imgCheck2.image = [UIImage imageNamed:@"icon-right-tik@2x"];
        //
        //            }
        //            else
        //            {
        //
        //                if ([str_dineinbody isEqualToString:@"1"])
        //                {
        //                    imgTake.image = [UIImage imageNamed:@"img-take-out-bl@2x"];
        //                    imgCheck2.image = [UIImage imageNamed:@"icon-right-tik@2x"];
        //
        //                }else
        //                {
        //                    imgTake.image = [UIImage imageNamed:@"img-take-out@2x"];
        //                    imgCheck2.image = [UIImage imageNamed:@""];
        //
        //                }
        //
        //
        //            }
        //
        //        }
        //        else{
        //
        //            if (indexPath.row ==selectedSection)
        //            {
        //                imgTake.image = [UIImage imageNamed:@"img-take-out@2x"];
        //                imgCheck2.image = [UIImage imageNamed:@""];
        //
        //            }
        //            else
        //            {
        //
        //                if ([str_takoutbody isEqualToString:@"1"])
        //                {
        //                    imgTake.image = [UIImage imageNamed:@"img-take-out-bl@2x"];
        //                    imgCheck2.image = [UIImage imageNamed:@"icon-right-tik@2x"];
        //
        //                }else
        //                {
        //                    imgTake.image = [UIImage imageNamed:@"img-take-out@2x"];
        //                    imgCheck2.image = [UIImage imageNamed:@""];
        //
        //
        //                }
        //
        //
        //
        //
        //            }
        //
        //
        //
        //        }
        //
        
        
        UIButton *btn_TableCell_2 = [[UIButton alloc]init];
        if (IS_IPHONE_6Plus)
        {
            btn_TableCell_2.frame = CGRectMake(170, 10, 55 , 60);
        }
        else if (IS_IPHONE_6)
        {
            btn_TableCell_2.frame = CGRectMake(150, 10, 55 , 60);
        }
        else if (IS_IPHONE_5)
        {
            btn_TableCell_2.frame = CGRectMake(125, 10, 50 , 55);
        }
        else
        {
            btn_TableCell_2.frame = CGRectMake(125, 10, 50 , 55);
        }
        btn_TableCell_2.backgroundColor=[UIColor clearColor];
        //    [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img.checkcorrect@2x.png"] forState:UIControlStateSelected];
        //    [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
        btn_TableCell_2.tag = indexPath.row;
        [btn_TableCell_2 addTarget:self action:@selector(click_selectObjectAt2:) forControlEvents:UIControlEventTouchUpInside];
        
        [img1 addSubview:btn_TableCell_2];
        //
        //
        //
        UIImageView *imgDelivery = [[UIImageView alloc]init];
        //        imgDelivery.frame = CGRectMake(220, 12, 50 , 55);
        if (IS_IPHONE_6Plus)
        {
            imgDelivery.frame = CGRectMake(283, 12, 55 , 60);
        }
        else if (IS_IPHONE_6)
        {
            imgDelivery.frame = CGRectMake(260, 12, 55 , 60);
        }
        else if (IS_IPHONE_5)
        {
            imgDelivery.frame = CGRectMake(220, 12, 50 , 55);
        }
        else
        {
            imgDelivery.frame = CGRectMake(220, 12, 50 , 55);
        }
        
        imgDelivery.backgroundColor = [UIColor clearColor];
        //        imgDelivery.layer.borderWidth = 1.0;
        imgDelivery.userInteractionEnabled = YES;
        
        [img1 addSubview:imgDelivery];
        
        
        imgCheck3 = [[UIImageView alloc]init];
        //        imgCheck3.frame = CGRectMake(245, 38, 25 , 20);
        if (IS_IPHONE_6Plus)
        {
            imgCheck3.frame = CGRectMake(310, 40, 27 , 20);
        }
        else if (IS_IPHONE_6)
        {
            imgCheck3.frame = CGRectMake(287, 40, 27 , 20);
        }
        else if (IS_IPHONE_5)
        {
            imgCheck3.frame = CGRectMake(245, 38, 25 , 20);
        }
        else
        {
            imgCheck3.frame = CGRectMake(245, 38, 25 , 20);
        }
        imgCheck3.backgroundColor = [UIColor clearColor];
        //        imgCheck3.layer.borderWidth = 1.0;
        imgCheck3.userInteractionEnabled = YES;
        [img1 addSubview:imgCheck3];
        
        if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:selectedSection] valueForKey:@"delivery"]]isEqualToString:@"1"])
        {
            imgDelivery.image = [UIImage imageNamed:@"img-delever-bl@2x"];
            imgCheck3.image = [UIImage imageNamed:@"icon-right-tik@2x"];
            
        }
        else{
            
            imgDelivery.image = [UIImage imageNamed:@"img-delever@2x"];
            imgCheck3.image = [UIImage imageNamed:@""];
        }
        
        //        if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:indexPath.row] valueForKey:@"delivery"]]isEqualToString:@"1"])
        //        {
        //
        //            if (indexPath.row ==selectedSection)
        //            {
        //                imgDelivery.image = [UIImage imageNamed:@"img-delever-bl@2x"];
        //                imgCheck3.image = [UIImage imageNamed:@"icon-right-tik@2x"];
        //
        //            }
        //            else
        //            {
        //
        //
        //                if ([str_dineinbody isEqualToString:@"1"])
        //                {
        //                    imgDelivery.image = [UIImage imageNamed:@"img-delever-bl@2x"];
        //                    imgCheck3.image = [UIImage imageNamed:@"icon-right-tik@2x"];
        //
        //
        //
        //                }else
        //                {
        //                    imgDelivery.image = [UIImage imageNamed:@"img-delever@2x"];
        //                    imgCheck3.image = [UIImage imageNamed:@""];
        //
        //                }
        //
        //
        //            }
        //
        //        }
        //        else{
        //            if (indexPath.row ==selectedSection)
        //            {
        //                imgDelivery.image = [UIImage imageNamed:@"img-delever@2x"];
        //                imgCheck3.image = [UIImage imageNamed:@""];
        //
        //            }
        //            else
        //            {
        //
        //                if ([str_takoutbody isEqualToString:@"1"])
        //                {
        //                    imgDelivery.image = [UIImage imageNamed:@"img-delever-bl@2x"];
        //                    imgCheck3.image = [UIImage imageNamed:@"icon-right-tik@2x"];
        //
        //                }else
        //                {
        //                    imgDelivery.image = [UIImage imageNamed:@"img-delever@2x"];
        //                    imgCheck3.image = [UIImage imageNamed:@""];
        //
        //
        //                }
        //
        //
        //            }
        //
        //
        //
        //
        //        }
        
        UIButton *btn_TableCell_3 = [[UIButton alloc]init];
        if (IS_IPHONE_6Plus)
        {
            btn_TableCell_3.frame = CGRectMake(283, 12, 55 , 60);
        }
        else if (IS_IPHONE_6)
        {
            btn_TableCell_3.frame = CGRectMake(260, 12, 55 , 60);
        }
        else if (IS_IPHONE_5)
        {
            btn_TableCell_3.frame = CGRectMake(220, 12, 50 , 55);
        }
        else
        {
            btn_TableCell_3.frame = CGRectMake(220, 12, 50 , 55);
        }
        btn_TableCell_3.backgroundColor=[UIColor clearColor];
        btn_TableCell_3.tag = indexPath.row;
        
        //    [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img.checkcorrect@2x.png"] forState:UIControlStateSelected];
        //    [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
        [btn_TableCell_3 addTarget:self action:@selector(click_selectObjectAt3:) forControlEvents:UIControlEventTouchUpInside];
        [img1 addSubview:btn_TableCell_3];
        
        
        UILabel *subTitleLbl = [[UILabel alloc]init];
        subTitleLbl.frame = CGRectMake(0, 115, WIDTH , 20);
        subTitleLbl.backgroundColor = [UIColor clearColor];
        subTitleLbl.text =@"Delivery";
        subTitleLbl.textColor = [UIColor blackColor];
        subTitleLbl.textAlignment = NSTextAlignmentCenter;
        subTitleLbl.font = [UIFont fontWithName:kFontBold size:16];
        subTitleLbl.lineBreakMode = NSLineBreakByWordWrapping;
        subTitleLbl.numberOfLines = 0;
        [cellView addSubview:subTitleLbl];
        
        
        UIImageView *img2 = [[UIImageView alloc]init];
        img2.frame = CGRectMake(10, 140, WIDTH-20 , 80);
        img2.backgroundColor = [UIColor clearColor];
        img2.image = [UIImage imageNamed:@"bg-img@2x"];
        img2.userInteractionEnabled=YES;
        //        img2.layer.borderWidth = 1.0;
        [cell.contentView addSubview:img2];
        //        img2=nil;
        
        UILabel *nameLbl = [[UILabel alloc]init];
        //        nameLbl .frame = CGRectMake(25, 10, 45 , 30);
        if (IS_IPHONE_6Plus)
        {
            nameLbl .frame = CGRectMake(45, 10, 45 , 30);
        }
        else if (IS_IPHONE_6)
        {
            nameLbl .frame = CGRectMake(40, 10, 45 , 30);
        }
        else if (IS_IPHONE_5)
        {
            nameLbl .frame = CGRectMake(25, 10, 45 , 30);
        }
        else
        {
            nameLbl .frame = CGRectMake(25, 10, 45 , 30);
        }
        nameLbl .backgroundColor = [UIColor clearColor];
        nameLbl .text =@"Name";
        nameLbl .textColor = [UIColor blackColor];
        //        nameLbl .textAlignment = NSTextAlignmentCenter;
        nameLbl .font = [UIFont fontWithName:kFont size:13];
        nameLbl .lineBreakMode = NSLineBreakByWordWrapping;
        nameLbl .numberOfLines = 0;
        [img2 addSubview:nameLbl];
        
        //        UIImageView *imgLblBg = [[UIImageView alloc]init];
        //        imgLblBg.frame = CGRectMake(75, 10, 200 , 30);
        //        imgLblBg.backgroundColor = [UIColor clearColor];
        //        imgLblBg.image = [UIImage imageNamed:@"dietary-table-img@2x"];
        //        //        imgLblBg.layer.borderWidth = 1.0;
        //        [img2 addSubview:imgLblBg];
        
        
        
        
        UIScrollView*delivery_Scroll;
        delivery_Scroll = [[UIScrollView alloc]init];
        // delivery_Scroll.frame=CGRectMake(80, 25, 210, 125);
        if (IS_IPHONE_6Plus)
        {
            delivery_Scroll.frame=CGRectMake(105, 25, 250, 125);
        }
        else  if (IS_IPHONE_6)
        {
            
            delivery_Scroll.frame=CGRectMake(100, 25, 227, 125);
        }
        else  if (IS_IPHONE_5)
        {
            
            delivery_Scroll.frame=CGRectMake(80, 25, 210, 125);
        }
        else
        {
            delivery_Scroll.frame=CGRectMake(80, 25, 210, 125);
        }
        
        delivery_Scroll.backgroundColor = [UIColor colorWithRed:243/255.0f green:245/255.0f blue:244/255.0f alpha:1.0f];
        delivery_Scroll.bounces=YES;
        delivery_Scroll.layer.borderWidth = 1.0;
        delivery_Scroll.layer.borderColor = [UIColor blackColor].CGColor;
        delivery_Scroll.showsVerticalScrollIndicator = YES;
        delivery_Scroll.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [delivery_Scroll setScrollEnabled:YES];
        delivery_Scroll.userInteractionEnabled = YES;
        [cell.contentView addSubview:delivery_Scroll];
        //[(NSDictionary *) [[ary_maintosave  objectAtIndex:0] valueForKey:@"Delivery_Address"] count]
        
        
        int totalPage;
        
        totalPage = (int)[[[ary_mainfromWS objectAtIndex:indexPath.row] valueForKey:@"addressdetails"] count]+1;
        [delivery_Scroll setContentSize:CGSizeMake(delivery_Scroll.frame.size.width, 25*totalPage)];
        
        
        for (int j = 0; j<totalPage; j++)
        {
            UILabel*deliver_Address_Lbl;
            deliver_Address_Lbl = [[UILabel alloc]init];
            deliver_Address_Lbl.frame = CGRectMake(0,25*j, delivery_Scroll.frame.size.width, 25);
            if (j==totalPage-1)
            {
                deliver_Address_Lbl.text = @"Add delivary address";
                
            }
            else
            {
                deliver_Address_Lbl.text = [[[[ary_mainfromWS objectAtIndex:indexPath.row] valueForKey:@"addressdetails"] objectAtIndex:j] valueForKey:@"address"];
                
            }

            deliver_Address_Lbl.textAlignment = NSTextAlignmentCenter;
            deliver_Address_Lbl.font = [UIFont fontWithName:kFont size:12];
            deliver_Address_Lbl.userInteractionEnabled = YES;
            
            [delivery_Scroll addSubview: deliver_Address_Lbl];
            
            
            UIImageView *imgLine=[[UIImageView alloc]init];
            imgLine.frame=CGRectMake( 0, 25*j, delivery_Scroll.frame.size.width , 0.5);
            [imgLine setUserInteractionEnabled:YES];
            imgLine.image = [UIImage imageNamed:@"line-img@2x"];
            [delivery_Scroll addSubview:imgLine];
            
            UIButton*get_Deliver_Address_Btn;
            get_Deliver_Address_Btn = [[UIButton alloc]init];
            get_Deliver_Address_Btn.frame = CGRectMake(0,25*j, delivery_Scroll.frame.size.width, 25);
            [get_Deliver_Address_Btn addTarget:self action:@selector(get_Deliver_Address_BtnMethod:) forControlEvents:UIControlEventTouchUpInside];
            get_Deliver_Address_Btn.tag = j;
            [get_Deliver_Address_Btn setBackgroundColor:[UIColor clearColor]];
            get_Deliver_Address_Btn.titleLabel.text = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
            //   get_Deliver_Address_Btn.layer.borderWidth = 1.0;
            [delivery_Scroll addSubview: get_Deliver_Address_Btn];
        }
        
        
        
        UIButton*delibery_Btn;
        
        delibery_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
        //        delibery_Btn.frame = CGRectMake(70, 10, 210 , 30);
        if (IS_IPHONE_6Plus)
        {
            delibery_Btn.frame = CGRectMake(95, 10, 250 , 30);
        }
        else if (IS_IPHONE_6)
        {
            delibery_Btn.frame = CGRectMake(90, 10, 227 , 30);
        }
        else if (IS_IPHONE_5)
        {
            delibery_Btn.frame = CGRectMake(70, 10, 210 , 30);
        }
        else
        {
            delibery_Btn.frame = CGRectMake(70, 10, 210 , 30);
        }
        [delibery_Btn addTarget:self action:@selector(delibery_Btn_Method:) forControlEvents:UIControlEventTouchUpInside];
        delibery_Btn.tag = indexPath.row;
        [delibery_Btn setBackgroundImage:[UIImage imageNamed:@"dietary-table-img@2x"] forState:UIControlStateNormal];
        [img2 addSubview: delibery_Btn];
        
        
        
        if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:indexPath.row] valueForKey:@"delivery"]]isEqualToString:@"1"])
        {
            
            img2.hidden = NO;
            nameLbl.hidden = NO;
            delivery_Scroll.hidden = NO;
            delibery_Btn.hidden = NO;
            subTitleLbl.hidden = NO;
            
            
            if ([[NSString stringWithFormat:@"%@",[[ary_maintosave  objectAtIndex:indexPath.row] valueForKey:@"tableshow"]]isEqualToString:@"NO"])
            {
                delivery_Scroll.hidden = YES;
                
            }
            else{
                delivery_Scroll.hidden = NO;
                
            }
            
            
        }
        else{
            img2.hidden = YES;
            nameLbl.hidden = YES;
            delivery_Scroll.hidden = YES;
            delibery_Btn.hidden = YES;
            subTitleLbl.hidden = YES;
            
        }
        
        UILabel*companyNameLbl;
        companyNameLbl = [[UILabel alloc]init];
        //        companyNameLbl .frame = CGRectMake(80, 10, 160 , 30);
        if (IS_IPHONE_6Plus)
        {
            companyNameLbl .frame = CGRectMake(105, 10, 180 , 30);
        }
        else if (IS_IPHONE_6)
        {
            companyNameLbl .frame = CGRectMake(100, 10, 175 , 30);
        }
        else if (IS_IPHONE_5)
        {
            companyNameLbl .frame = CGRectMake(80, 10, 160 , 30);
        }
        else
        {
            companyNameLbl .frame = CGRectMake(80, 10, 160 , 30);
        }
        companyNameLbl .backgroundColor = [UIColor clearColor];
        companyNameLbl .text =[[ary_maintosave objectAtIndex:indexPath.row] valueForKey:@"Delivery_Address"];
        companyNameLbl .textColor = [UIColor blackColor];
        //        nameLbl .textAlignment = NSTextAlignmentCenter;
        companyNameLbl .font = [UIFont fontWithName:kFont size:13];
        companyNameLbl.lineBreakMode = NSLineBreakByWordWrapping;
        companyNameLbl.numberOfLines = 0;
        [img2 addSubview:companyNameLbl];
        //
        
        UIButton*apply_Btn;
        
        apply_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
        //        apply_Btn.frame = CGRectMake(190, 52, 15 , 15);
        if (IS_IPHONE_6Plus)
        {
            apply_Btn.frame = CGRectMake(245, 50, 17 , 17);
        }
        else if (IS_IPHONE_6)
        {
            apply_Btn.frame = CGRectMake(232, 52, 16 , 16);
        }
        else if (IS_IPHONE_5)
        {
            apply_Btn.frame = CGRectMake(190, 52, 15 , 15);
        }
        else
        {
            apply_Btn.frame = CGRectMake(190, 52, 15 , 15);
        }
        
        apply_Btn.tag = indexPath.row;
        
        if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:indexPath.row] valueForKey:@"delivaryidselected"]]isEqualToString:@"1"])
        {
            [apply_Btn setBackgroundImage:[UIImage imageNamed:@"img-check-select@2x"] forState:UIControlStateNormal];
            
        }
        else
        {
            [apply_Btn setBackgroundImage:[UIImage imageNamed:@"img-check@2x"] forState:UIControlStateNormal];
            
        }
        [apply_Btn addTarget:self action:@selector(apply_Btn_Method:) forControlEvents:UIControlEventTouchUpInside];
        [img2 addSubview: apply_Btn];
        
        UILabel *apply_Lbl = [[UILabel alloc]init];
        //        apply_Lbl.frame = CGRectMake(210, 52, 80 , 15);
        if (IS_IPHONE_6Plus)
        {
            apply_Lbl.frame = CGRectMake(270, 50, 80 , 17);
        }
        else if (IS_IPHONE_6)
        {
            apply_Lbl.frame = CGRectMake(255, 52, 80 , 16);
        }
        else if (IS_IPHONE_5)
        {
            apply_Lbl.frame = CGRectMake(210, 52, 80 , 15);
        }
        else
        {
            apply_Lbl.frame = CGRectMake(210, 52, 80 , 15);
        }
        apply_Lbl.backgroundColor = [UIColor clearColor];
        apply_Lbl.text =@"Apply to All";
        apply_Lbl.textColor = [UIColor blackColor];
        //        apply_Lbl.textAlignment = NSTextAlignmentCenter;
        apply_Lbl.font = [UIFont fontWithName:kFontBold size:11];
        apply_Lbl.lineBreakMode = NSLineBreakByWordWrapping;
        apply_Lbl.numberOfLines = 0;
        [img2 addSubview:apply_Lbl];
        
        cell.backgroundColor = [UIColor colorWithRed:234.0/255.0 green:244.0/255.0  blue:248.0/255.0 alpha:1];
    }
    else if (tableView == table_Selected_Date)
    {
//        cell.textLabel.text = @"Chal Maakhuch";
        
        UIView *cellView = [[UIView alloc]init];
        cellView.frame = CGRectMake(0, 0, table_Preview.frame.size.width, table_Preview.rowHeight);
        cellView.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:cellView];
        
        
        UILabel *titleLbl = [[UILabel alloc]init];
        //        titleLbl.frame = CGRectMake(15, 5, 120, 40);
        if (IS_IPHONE_6Plus)
        {
            titleLbl.frame = CGRectMake(25, 2, 240, 40);
        }
        else if (IS_IPHONE_6)
        {
            titleLbl.frame = CGRectMake(20, 2, 240, 40);
        }
        else if (IS_IPHONE_5)
        {
            titleLbl.frame = CGRectMake(15, 2, 240, 40);
        }
        else
        {
            titleLbl.frame = CGRectMake(15, 2, 240, 40);
        }
        titleLbl.backgroundColor = [UIColor clearColor];
        titleLbl.text = @"adfsgfdgh";
        titleLbl.textColor = [UIColor blackColor];
        titleLbl.font = [UIFont fontWithName:kFontBold size:16];
        titleLbl.lineBreakMode = NSLineBreakByWordWrapping;
        titleLbl.numberOfLines = 0;
        [cellView addSubview:titleLbl];
        titleLbl = nil;
        
//        UIImageView *img1 = [[UIImageView alloc]init];
//        //        img1.frame = CGRectMake(165, 8, 35 , 32);
//        if (IS_IPHONE_6Plus)
//        {
//            img1.frame = CGRectMake(230, 8, 35 , 32);
//        }
//        else if (IS_IPHONE_6)
//        {
//            img1.frame = CGRectMake(205, 8, 35 , 32);
//        }
//        else if (IS_IPHONE_5)
//        {
//            img1.frame = CGRectMake(table_Selected_Date.frame.size.width-25, 14, 16 , 16);
//        }
//        else
//        {
//            img1.frame = CGRectMake(table_Selected_Date.frame.size.width-25, 14, 16 , 16);
//        }
//        img1.image = [UIImage imageNamed:@"cross-img@2x"];
//        img1.backgroundColor = [UIColor clearColor];
////        img1.layer.borderWidth = 1.0;
//        [cell.contentView addSubview:img1];
//        img1=nil;
        
        
        removeCell_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
        //        removeCell_Btn.frame = CGRectMake(190, 52, 15 , 15);
        if (IS_IPHONE_6Plus)
        {
            removeCell_Btn.frame = CGRectMake(table_Selected_Date.frame.size.width-25, 15, 16 , 16);
        }
        else if (IS_IPHONE_6)
        {
            removeCell_Btn.frame = CGRectMake(table_Selected_Date.frame.size.width-25, 15, 16 , 16);
        }
        else if (IS_IPHONE_5)
        {
            removeCell_Btn.frame = CGRectMake(table_Selected_Date.frame.size.width-25, 15, 16 , 16);
        }
        else
        {
            removeCell_Btn.frame = CGRectMake(table_Selected_Date.frame.size.width-25, 15, 16 , 16);
        }
        [removeCell_Btn addTarget:self action:@selector(removeCell_Btn_Method:) forControlEvents:UIControlEventTouchUpInside];
        [removeCell_Btn setBackgroundImage:[UIImage imageNamed:@"cross-img@2x"] forState:UIControlStateNormal];
        removeCell_Btn.tag = indexPath.row;
        [cell.contentView addSubview: removeCell_Btn];
        
        
        UIImageView *imgLine=[[UIImageView alloc]init];
        imgLine.frame=CGRectMake( 10, 46, WIDTH-50 , 0.5 );
        //    if (IS_IPHONE_6Plus)
        //    {
        //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH+10,130);
        //    }
        //    else if (IS_IPHONE_6)
        //    {
        //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH+10,120);
        //    }
        //    else if (IS_IPHONE_5)
        //    {
        //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame),WIDTH+10,100);
        //    }
        //    else
        //    {
        //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame),WIDTH+10,100);
        //    }
        [imgLine setUserInteractionEnabled:YES];
        imgLine.image = [UIImage imageNamed:@"line-img@2x"];
        //    imgLine.layer.borderWidth = 1.0;
        [cell.contentView addSubview:imgLine];
        
    }
    
    else if (tableView == table_Repeated)
    {
        
        cell.textLabel.text = [repeat_Arr objectAtIndex:indexPath.row];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.font = [UIFont fontWithName:kFont size:13];
        
        UIImageView *imgLine=[[UIImageView alloc]init];
        imgLine.frame=CGRectMake( 0, 29, WIDTH-60 , 0.5 );
        //    if (IS_IPHONE_6Plus)
        //    {
        //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH+10,130);
        //    }
        //    else if (IS_IPHONE_6)
        //    {
        //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame)+5,WIDTH+10,120);
        //    }
        //    else if (IS_IPHONE_5)
        //    {
        //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame),WIDTH+10,100);
        //    }
        //    else
        //    {
        //        imgLine.frame=CGRectMake(-2, CGRectGetMaxY(labl_serveNowDate.frame),WIDTH+10,100);
        //    }
        [imgLine setUserInteractionEnabled:YES];
        imgLine.image = [UIImage imageNamed:@"line-img@2x"];
        //    imgLine.layer.borderWidth = 1.0;
        [cell.contentView addSubview:imgLine];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table_Repeated)
    {
        repeatLbl.text =[repeat_Arr objectAtIndex:indexPath.row];
        [table_Repeated setHidden:YES];
    }
    
    else if (tableView == table_for_contry_code1)
    {
        txt_tel_code.text = [[arrayCountry objectAtIndex:indexPath.row]valueForKey:@"id"];
       table_for_contry_code1.hidden = YES;
    }
    
    else if (tableView == table_for_contry_code2)
    {

        txt_tel_code2.text = [[arrayCountry objectAtIndex:indexPath.row]valueForKey:@"id"];
        table_for_contry_code2.hidden = YES;
    }
    
}

# pragma mark Hcountry method

-(void)countryList
{
    
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpHcountry
                                                      parameters:nil];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [self ResponseChefSignUpHcountry:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self countryList];
                                         }
                                     }];
    [operation start];
    
}

-(void) ResponseChefSignUpHcountry :(NSDictionary * )TheDict
{
    [arrayCountry removeAllObjects];
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"CountryMobileCode"] count]; i++)
        {
            [arrayCountry addObject:[[TheDict valueForKey:@"CountryMobileCode"] objectAtIndex:i]];
            
        }
        [table_for_contry_code1 reloadData];
        [table_for_contry_code2 reloadData];
        
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        NSLog(@"Display error message");
        
    }
    
    [table_for_contry_code1 reloadData];
    [table_for_contry_code2 reloadData];
    
}


#pragma mark - ExpandAndCollapse--
-(void)section_BtnMethod: (UIButton *)sender
{
    
    
    if ([sender isSelected])
    {
        [sender setSelected:NO];
        
    }
    else{
        [sender setSelected:YES];
    }
    
    NSLog(@"Section Button Selected");
    
    if (sender.tag == selectedSection)
    {
        
        [self collapseCellsAtSection:sender.tag withCallback:^(BOOL success)
         {
         }];
    }
    else{
        if (selectedSection >= 0) {
            
            [self collapseCellsAtSection:selectedSection withCallback:^(BOOL success){
                
                if (success) {
                    
                    [self performSelector:@selector(expandCellsAtSection:) withObject:[NSNumber numberWithInt:sender.tag] afterDelay:0.5];
                    
                }
                
            }];
            
        }
        else{
            
            [self expandCellsAtSection:[NSNumber numberWithInt:sender.tag]];
        }
    }
    
    
}



-(void) collapseCellsAtSection:(int) section withCallback:(CompletionBlock)callback
{
    selectedSection = -1;
    [table_DishList beginUpdates];
    [table_DishList reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationFade];
    [table_DishList endUpdates];
    callback(YES);
}

-(void) expandCellsAtSection:(NSNumber *) section
{
    selectedSection = [section intValue];
    [table_DishList beginUpdates];
    [table_DishList reloadSections:[NSIndexSet indexSetWithIndex:[section intValue]] withRowAnimation:UITableViewRowAnimationFade];
    [table_DishList endUpdates];
    
}


-(void)AFAddresslist
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
  
    
    NSDictionary *params =@{
                            @"uid"                              :   [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"delivery_company_name"            :   txt_delevery_company.text,
                            @"delivery_company_phone_code"      :   txt_tel_code.text,
                            @"delivery_company_phone_number"    :   txt_mobile_number2.text,
                            @"delivery_person_name"             :   txt_persone_name.text,
                            @"delivery_person_phone_code"       :   txt_tel_code2.text,
                            @"delivery_person_phone_number"     :   txt_person_mobile_number.text,
                            @"delivery_charge_first_mile"       :   txt_first_mile_charge.text,
                            @"delivery_charge_additional_mile"  :   txt_each_mile_charge.text,
                            @"delivery_flat_charge"             :   txt_flat_charge.text,
                            @"delivery_setdefault"              :   [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:KAddaddress
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseAFAddresslist:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFAddresslist];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseAFAddresslist :(NSDictionary * )TheDict
{
    NSLog(@"address: %@",TheDict);
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
    
    
}




#pragma mark textFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
