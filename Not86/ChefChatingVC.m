//
//  UserChatingVC.m
//  Not86
//
//  Created by User on 12/10/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ChefChatingVC.h"
#import "AppDelegate.h"
#import "JWSlideMenuViewController.h"
#include "JWNavigationController.h"


#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"



@interface ChefChatingVC ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    UIImageView * img_header;
    UIScrollView * scroll;
    
    NSMutableArray * ary_message;
    NSMutableDictionary * dict_Section;
    
    UITableView * table_for_chat;
    
    UITextField * txt_message;
    UIView * alertviewBg;
    UIView * view_bottom;
    
    AppDelegate * delegate ;
    
}

@end

@implementation ChefChatingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self integrateHeader];
    [self integrateBodyDesign];
    
    ary_message = [NSMutableArray new];
    dict_Section = [[NSMutableDictionary alloc]init];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    
    // Do any additional setup after loading the view.
}



-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [icon_back addTarget:self action:@selector(click_on_back_arrow:) forControlEvents:UIControlEventTouchUpInside];
    [img_header   addSubview:icon_back];
    
    
    UILabel *lbl_support_on_header = [[UILabel alloc]init];
    lbl_support_on_header.frame = CGRectMake(CGRectGetMaxX(icon_back .frame)+30,0, 300, 45);
    lbl_support_on_header.text = @"Jonathan Timothy";
    lbl_support_on_header.font = [UIFont fontWithName:kFont size:20];
    lbl_support_on_header.textColor = [UIColor whiteColor];
    lbl_support_on_header.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_support_on_header];
    
    
    
    UIButton *btn_logo = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_logo.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    btn_logo .backgroundColor = [UIColor clearColor];
    [btn_logo setImage:[UIImage imageNamed:@"logo-img@2x.png"] forState:UIControlStateNormal];
    [btn_logo addTarget:self action:@selector(click_on_logo:) forControlEvents:UIControlEventTouchUpInside];
    [img_header   addSubview:btn_logo];
    
    
    
}
-(void)integrateBodyDesign
{
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.frame = CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT);
    scroll.backgroundColor = [UIColor clearColor];
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    
    //UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1
    
    UIImageView *white_bg = [[UIImageView alloc]init];
    white_bg.frame = CGRectMake(0,0,WIDTH,80);
    [white_bg setUserInteractionEnabled:YES];
    white_bg.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [scroll addSubview:white_bg];
    
    UILabel *lbl_order_no = [[UILabel alloc]init];
    lbl_order_no.frame = CGRectMake(20,5, 100, 45);
    lbl_order_no.text = @"Order no.:";
    lbl_order_no.font = [UIFont fontWithName:kFontBold size:20];
    lbl_order_no.textColor = [UIColor blackColor];
    lbl_order_no.backgroundColor = [UIColor clearColor];
    [white_bg addSubview:lbl_order_no];
    
    
    UILabel *lbl_order_val = [[UILabel alloc]init];
    lbl_order_val.frame = CGRectMake(CGRectGetMaxX(lbl_order_no.frame),5, 150, 45);
    lbl_order_val.text = @"10847";
    lbl_order_val.font = [UIFont fontWithName:kFontBold size:20];
    lbl_order_val.textColor = [UIColor blackColor];
    lbl_order_val.backgroundColor = [UIColor clearColor];
    [white_bg addSubview:lbl_order_val];
    
    UILabel *lbl_subject = [[UILabel alloc]init];
    lbl_subject.frame = CGRectMake(50,CGRectGetMidY(lbl_order_no.frame), 100, 45);
    lbl_subject.text = @"Subject:";
    lbl_subject.font = [UIFont fontWithName:kFont size:17];
    lbl_subject.textColor = [UIColor blackColor];
    lbl_subject.backgroundColor = [UIColor clearColor];
    [white_bg addSubview:lbl_subject];
    
    
    UILabel *lbl_subject_val = [[UILabel alloc]init];
    lbl_subject_val.frame = CGRectMake(CGRectGetMidX(lbl_subject.frame)+20,CGRectGetMidY(lbl_order_no.frame),300,45);
    lbl_subject_val.text = @"Lorem ipsum dolor sit";
    lbl_subject_val.font = [UIFont fontWithName:kFontBold size:17];
    lbl_subject_val.textColor = [UIColor blackColor];
    lbl_subject_val.backgroundColor = [UIColor clearColor];
    [white_bg addSubview:lbl_subject_val];
    
    
    UIButton *img_right_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
    img_right_arrow.frame = CGRectMake(WIDTH-40,20, 30, 30);
    img_right_arrow .backgroundColor = [UIColor clearColor];
    [img_right_arrow setImage:[UIImage imageNamed:@"right-arrow-icon@2x.png"]forState:UIControlStateNormal];
    // [img_right_arrow addTarget:self action:@selector(click_on_logo:) forControlEvents:UIControlEventTouchUpInside];
    [white_bg   addSubview:img_right_arrow];
    
    
    
#pragma mark Tableview
    
    table_for_chat = [[UITableView alloc] init ];
    table_for_chat.frame  = CGRectMake(0,CGRectGetMaxY(white_bg.frame)+48,WIDTH,480);
    [table_for_chat setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_chat.delegate = self;
    table_for_chat.dataSource = self;
    table_for_chat.showsVerticalScrollIndicator = NO;
    table_for_chat.backgroundColor = [UIColor clearColor];
    [self.view addSubview:table_for_chat];
    
    
    UIImageView *white_bg2 = [[UIImageView alloc]init];
    white_bg2.frame = CGRectMake(0,CGRectGetMaxY(table_for_chat.frame)-40,WIDTH,80);
    [white_bg2 setUserInteractionEnabled:YES];
    white_bg2.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [scroll addSubview:white_bg2];
    
    UIImageView *msg_line = [[UIImageView alloc]init];
    msg_line.frame = CGRectMake(20,30,300,5);
    [msg_line setUserInteractionEnabled:YES];
    msg_line.image=[UIImage imageNamed:@"img-msg-bg@2x.png"];
    [white_bg2 addSubview:msg_line];
    
    
    txt_message = [[UITextField alloc] init];
    txt_message  .frame=CGRectMake(30,13,290,20);
    txt_message  .borderStyle = UITextBorderStyleNone;
    txt_message  .textColor = [UIColor grayColor];
    txt_message  .font = [UIFont fontWithName:kFont size:14];
    txt_message  .placeholder = @"Message";
    [txt_message   setValue:[UIFont fontWithName:kFont size: 14] forKeyPath:@"_placeholderLabel.font"];
    [txt_message   setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding5 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 10)];
    txt_message  .leftView = padding5;
    txt_message  .leftViewMode = UITextFieldViewModeAlways;
    txt_message  .userInteractionEnabled=YES;
    txt_message  .textAlignment = NSTextAlignmentLeft;
    txt_message .backgroundColor = [UIColor clearColor];
    txt_message  .keyboardType = UIKeyboardTypeAlphabet;
    txt_message  .delegate = self;
    [white_bg2 addSubview:txt_message ];
    txt_message.enabled = YES;
    //self.txt_Phone_Code = txt_message;
    
    
    UIButton *btn_on_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_arrow.frame = CGRectMake(CGRectGetMaxX(msg_line.frame)+5,10, 30, 30);
    btn_on_arrow .backgroundColor = [UIColor clearColor];
    [btn_on_arrow setImage:[UIImage imageNamed:@"icon-msg-arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_arrow addTarget:self action:@selector(click_on_msg_arrow:) forControlEvents:UIControlEventTouchUpInside];
    [white_bg2   addSubview:btn_on_arrow];
    
    
    
    
    
    [scroll setContentSize:CGSizeMake(0,630)];
    
    
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[dict_Section valueForKey:[[[dict_Section allKeys] sortedArrayUsingSelector:@selector(compare:)] objectAtIndex:section]] count];
    // return 10;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[dict_Section allKeys] count];
    // return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
    
    CGFloat abc =0;
    if (tableView == table_for_chat)
    {
        
        NSDictionary *dict_message = [[dict_Section valueForKey:[[[dict_Section allKeys] sortedArrayUsingSelector:@selector(compare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        
        NSString *str_Message = [dict_message valueForKey:@"message"];
        
        NSString *decoded = [str_Message stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        CGSize constraint = CGSizeMake(210, 10000.0f);
        CGSize size = [decoded  sizeWithFont:[UIFont fontWithName:kFont size:12.0f] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
        
        if (size.height >70)
        {
            return size.height+55;
        }
        
        return size.height+85;
        
    }
    return abc;
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIImageView *headerView =[[UIImageView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    
    UIImageView *imgView_BG =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,WIDTH,20)];
    // imgView_BG.image = [UIImage imageNamed:@"img_Header@2x.png"];
    imgView_BG.backgroundColor = [UIColor clearColor];
    [headerView addSubview:imgView_BG];
    
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0,WIDTH,20)];
    headerLabel.backgroundColor=[UIColor clearColor];
    headerLabel.textColor=[UIColor whiteColor];
    headerLabel.font=[UIFont fontWithName:kFont size:13];
    
    
    NSString *str_Date = [NSString stringWithFormat:@"%@",[[[dict_Section allKeys] sortedArrayUsingSelector:@selector(compare:)] objectAtIndex:section]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"EEEE, dd/MM/yyyy"];
    NSDate *date_Message = [NSDate dateWithTimeIntervalSince1970:[str_Date intValue]];
    
    NSString *str_Time = [formatter stringFromDate:date_Message];
    //    headerLabel.text = str_Time;
    headerLabel.textAlignment = NSTextAlignmentCenter;
    [headerView addSubview:headerLabel];
    
    
    return headerView;
    
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"LazyTableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor=[UIColor clearColor];
        if (tableView == table_for_chat)
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else
    {
        for (UIView *view in cell.contentView.subviews)
            [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    
    if (tableView == table_for_chat)
    {
        NSDictionary *dict_message = [[dict_Section valueForKey:[[[dict_Section allKeys] sortedArrayUsingSelector:@selector(compare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        
        
        UIImageView  *imgView_PopUp= [[UIImageView alloc] init];
        imgView_PopUp.userInteractionEnabled = YES;
        imgView_PopUp.contentMode = UIViewContentModeScaleToFill;
        [imgView_PopUp setClipsToBounds:YES];
        [imgView_PopUp setBackgroundColor:[UIColor clearColor]];
        imgView_PopUp.layer.cornerRadius = 4.0;
        [cell.contentView addSubview:imgView_PopUp];
        
        
        UIImageView *img_Persons = [[UIImageView alloc]init];
        [img_Persons setUserInteractionEnabled:YES];
        img_Persons.layer.cornerRadius = 43/2;
        img_Persons.layer.borderWidth=2.0f;
        img_Persons.layer.borderColor = [UIColor clearColor].CGColor;
        img_Persons.clipsToBounds = YES;
        img_Persons.backgroundColor = [UIColor whiteColor];
        [cell.contentView addSubview:img_Persons];
        
        
        UILabel *lbl_personname=[[UILabel alloc] init];
        lbl_personname.backgroundColor=[UIColor clearColor];
        lbl_personname.textColor=[UIColor blackColor];
        lbl_personname.lineBreakMode = NSLineBreakByWordWrapping;
        lbl_personname.textAlignment = NSTextAlignmentCenter;
        lbl_personname.numberOfLines = 2;
        [lbl_personname setFont:[UIFont fontWithName:kFont size:11.0f]];
        [cell.contentView addSubview:lbl_personname];
        
        
        UILabel *lbl_Time = [[UILabel alloc] init];
        lbl_Time.textColor = [UIColor blackColor];
        lbl_Time.textAlignment = NSTextAlignmentRight;
        lbl_Time.backgroundColor = [UIColor clearColor];
        [lbl_Time setFont:[UIFont fontWithName:kFont size:11.0f]];
        [cell.contentView addSubview:lbl_Time];
        
        
        UILabel *lbl_Chat=[[UILabel alloc] init];
        lbl_Chat.backgroundColor=[UIColor clearColor];
        lbl_Chat.hidden = YES;
        lbl_Chat.textColor=[UIColor blackColor];
        lbl_Chat.numberOfLines =0;
        lbl_Chat.lineBreakMode = NSLineBreakByWordWrapping;
        lbl_Chat.textAlignment = NSTextAlignmentLeft;
        [lbl_Chat setFont:[UIFont fontWithName:kFont size:12.0f]];
        [imgView_PopUp addSubview:lbl_Chat];
        
        //user_uid
        UIButton *btn_Profile =[UIButton buttonWithType:UIButtonTypeCustom];
        [btn_Profile setBackgroundColor:[UIColor clearColor]];
        [btn_Profile addTarget:self action:@selector(click_BtnGoToProfile:) forControlEvents:UIControlEventTouchUpInside];
        //        [cell.contentView addSubview:btn_Profile];
        
        
        if ([[dict_message valueForKey:@"user_uid"] isEqualToString:[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"] ])
        {
            
            btn_Profile.tag = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"] intValue];
            
            imgView_PopUp.frame = CGRectMake(35,6,230, 49);
            img_Persons.frame = CGRectMake(CGRectGetMaxX(imgView_PopUp.frame)+4,8,43, 43);
            btn_Profile.frame=CGRectMake(260,0,60,60);
            lbl_personname.frame = CGRectMake(268,CGRectGetMaxY(img_Persons.frame)+1,50, 24);
            
            btn_Profile.tag = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"]intValue];
            
            
            if ([[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserDetail"] valueForKey:@"ProfilePhoto"] length]>0&&![[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserDetail"] valueForKey:@"ProfilePhoto"] isEqual:[NSNull null]])
            {
                
                
                NSString *ImagePath = [[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserDetail"] valueForKey:@"ProfilePhoto"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                [img_Persons setImageWithURL:[NSURL URLWithString:ImagePath] placeholderImage:[UIImage imageNamed:@"user-img@2x.png"]];
                
            }
            else
            {
                [img_Persons setImage:[UIImage imageNamed:@"user-img@2x.png"]];
            }
            
            imgView_PopUp.image = [UIImage imageNamed:@"img-chat1@2x.png"];
            
            
            double timestampval1 =  [[dict_message valueForKey:@"timestamp"] doubleValue];
            NSTimeInterval timestamp1 = (NSTimeInterval)timestampval1;
            NSDate *updatetimestamp1 = [NSDate dateWithTimeIntervalSince1970:timestamp1];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"hh:mm a";
            //            NSString *str_Time = [dateFormatter stringFromDate:updatetimestamp1];
            
            NSString *str_Message = [dict_message valueForKey:@"message"];
            
            NSString *decoded = [str_Message stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            decoded = [decoded stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            decoded = [decoded stringByReplacingOccurrencesOfString:@"+" withString:@" "];
            
            
            CGSize constraint = CGSizeMake(210 ,10000.0f);
            CGSize chatText_size = [decoded  sizeWithFont:[UIFont fontWithName:kFont size:12.0f] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
            
            lbl_Time.textAlignment = NSTextAlignmentLeft;
            //            lbl_Time.text = [[NSString stringWithFormat:@"%@",str_Time]uppercaseString];
            
            lbl_Time.text = [[NSString stringWithFormat:@"%@",[dict_message valueForKey:@"date_time"]]uppercaseString];
            lbl_Time.textColor = [UIColor redColor];
            lbl_personname.text = [NSString stringWithFormat:@"%@,",[dict_message valueForKey:@"user_name"]];
            
            
            
            
            
            //            CGSize constraint1 = CGSizeMake(210 ,10000.0f);
            //            CGSize chatText_size1 = [lbl_personname.text  sizeWithFont:[UIFont fontWithName:kFont size:12.0f] constrainedToSize:constraint1 lineBreakMode:NSLineBreakByWordWrapping];
            //
            //            if (chatText_size1.width >150)
            //            {
            //                lbl_personname.frame =CGRectMake(37,5, 150, 15);
            //                lbl_Time.frame = CGRectMake(CGRectGetMaxX(lbl_personname.frame),5, 70, 15);
            //            }
            //            else{
            //                lbl_personname.frame =CGRectMake(37,5, chatText_size1.width +10, 15);
            //                lbl_Time.frame = CGRectMake(CGRectGetMaxX(lbl_personname.frame),5, 70, 15);
            //            }
            //            lbl_Chat.frame = CGRectMake(10,CGRectGetMaxY(lbl_Time.frame)+3, 210, 20);
            //            lbl_Chat.text = [NSString stringWithFormat:@"%@",decoded];
            //            lbl_Chat.hidden = NO;
            //
            //
            //
            //
            //            if (chatText_size.height >55)
            //            {
            //                imgView_PopUp.frame = CGRectMake(35,6,230, chatText_size.height +16+30);
            //                lbl_personname.frame =CGRectMake(37,10, 100, 15);
            //                lbl_Time.frame = CGRectMake(CGRectGetMaxX(lbl_personname.frame),10, 70, 15);
            //                lbl_Chat.frame = CGRectMake(10,CGRectGetMaxY(lbl_Time.frame)+3, 210, chatText_size.height+20 );
            //
            //            }
            
            
            
            
            CGSize constraint1 = CGSizeMake(210 ,10000.0f);
            CGSize chatText_size1 = [lbl_personname.text  sizeWithFont:[UIFont fontWithName:kFont size:12.0f] constrainedToSize:constraint1 lineBreakMode:NSLineBreakByWordWrapping];
            
            if (chatText_size1.width >150)
            {
                lbl_personname.frame =CGRectMake(37,5, 150, 15);
                lbl_Time.frame = CGRectMake(CGRectGetMaxX(lbl_personname.frame),5, 130, 15);
            }
            else{
                lbl_personname.frame =CGRectMake(37,5, chatText_size1.width +10, 15);
                lbl_Time.frame = CGRectMake(CGRectGetMaxX(lbl_personname.frame),5, 130, 15);
            }
            lbl_Chat.frame = CGRectMake(10,CGRectGetMaxY(lbl_Time.frame)+3, 210, 20);
            lbl_Chat.text = [NSString stringWithFormat:@"%@",decoded];
            lbl_Chat.hidden = NO;
            
            
            
            
            if (chatText_size.height >55)
            {
                imgView_PopUp.frame = CGRectMake(35,6,230, chatText_size.height +16+30);
                lbl_personname.frame =CGRectMake(37,10, 100, 15);
                lbl_Time.frame = CGRectMake(CGRectGetMaxX(lbl_personname.frame),10, 130, 15);
                lbl_Chat.frame = CGRectMake(10,CGRectGetMaxY(lbl_Time.frame)+3, 210, chatText_size.height+20 );
                
            }
            
            
        }
        else
        {
            img_Persons.frame = CGRectMake(11,8,43, 43);
            imgView_PopUp.frame = CGRectMake(CGRectGetMaxX(img_Persons.frame)+4,6,230, 49);
            btn_Profile.frame=CGRectMake(10,0,60,60);
            
            btn_Profile.tag = [[dict_message valueForKey:@"user_uid"] intValue];
            
            if ([[dict_message valueForKey:@"senderimage"] length]>0&&![[dict_message valueForKey:@"senderimage"] isEqual:[NSNull null]])
            {
                
                
                NSString *ImagePath = [[NSString stringWithFormat:@"%@",[dict_message valueForKey:@"senderimage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                [img_Persons setImageWithURL:[NSURL URLWithString:ImagePath] placeholderImage:[UIImage imageNamed:@"chef-img@2x.png"]];
                
            }
            else
            {
                [img_Persons setImage:[UIImage imageNamed:@"chef-img@2x.png"]];
            }
            
            
            
            imgView_PopUp.image = [UIImage imageNamed:@"img-chat2@2x.png"];
            
            double timestampval1 =  [[dict_message valueForKey:@"timestamp"] doubleValue];
            NSTimeInterval timestamp1 = (NSTimeInterval)timestampval1;
            NSDate *updatetimestamp1 = [NSDate dateWithTimeIntervalSince1970:timestamp1];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"hh:mm a";
            NSString *str_Time = [dateFormatter stringFromDate:updatetimestamp1];
            
            lbl_Time.text = [[NSString stringWithFormat:@"%@",str_Time]uppercaseString];
            
            NSString *str_Message = [dict_message valueForKey:@"message"];
            
            NSString *decoded = [str_Message stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            decoded = [decoded stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            decoded = [decoded stringByReplacingOccurrencesOfString:@"+" withString:@" "];
            
            lbl_Time.textAlignment = NSTextAlignmentLeft;
            
            lbl_Time.text = [[NSString stringWithFormat:@"%@",str_Time]uppercaseString];
            
            lbl_Time.text = [[NSString stringWithFormat:@"%@",[dict_message valueForKey:@"date_time"]]uppercaseString];
            lbl_Time.textColor = [UIColor redColor];
            
            //            lbl_personname.frame =CGRectMake(37,5, 100, 15);
            lbl_personname.text = [NSString stringWithFormat:@"%@,",[dict_message valueForKey:@"user_name"]];
            
            
            //            CGSize constraint1 = CGSizeMake(210 ,10000.0f);
            //            CGSize chatText_size1 = [lbl_personname.text  sizeWithFont:[UIFont fontWithName:kFont size:12.0f] constrainedToSize:constraint1 lineBreakMode:NSLineBreakByWordWrapping];
            //
            //            if (chatText_size1.width >150)
            //            {
            //                lbl_personname.frame =CGRectMake(70,5, 100, 15);
            //                lbl_Time.frame = CGRectMake(CGRectGetMaxX(lbl_personname.frame),5, 70, 15);
            //            }
            //            else{
            //                lbl_personname.frame =CGRectMake(70,5, chatText_size1.width +10, 15);
            //                lbl_Time.frame = CGRectMake(CGRectGetMaxX(lbl_personname.frame),5, 70, 15);
            //            }
            //
            //
            //
            //
            //            lbl_Chat.frame = CGRectMake(10,CGRectGetMaxY(lbl_Time.frame)+3, 210, 20);
            //            lbl_Chat.text = [NSString stringWithFormat:@"%@",decoded];
            //            CGSize constraint = CGSizeMake(210 ,10000.0f);
            //            CGSize chatText_size = [decoded  sizeWithFont:[UIFont fontWithName:kFont size:12.0f] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
            //
            //            lbl_Chat.hidden = NO;
            //            if (chatText_size.height >55)
            //            {
            //                lbl_personname.frame =CGRectMake(70,5, 100, 15);
            //                lbl_Time.frame = CGRectMake(CGRectGetMaxX(lbl_personname.frame),5, 70, 15);
            //                imgView_PopUp.frame = CGRectMake(CGRectGetMaxX(img_Persons.frame)+2,6,230,  chatText_size.height +16+30);
            //                lbl_Chat.frame = CGRectMake(10,8, 210, chatText_size.height+20 );
            //
            //            }
            
            CGSize constraint1 = CGSizeMake(210 ,10000.0f);
            CGSize chatText_size1 = [lbl_personname.text  sizeWithFont:[UIFont fontWithName:kFont size:12.0f] constrainedToSize:constraint1 lineBreakMode:NSLineBreakByWordWrapping];
            
            if (chatText_size1.width >150)
            {
                lbl_personname.frame =CGRectMake(70,5, 100, 15);
                lbl_Time.frame = CGRectMake(CGRectGetMaxX(lbl_personname.frame),5, 130, 15);
            }
            else{
                lbl_personname.frame =CGRectMake(70,5, chatText_size1.width +10, 15);
                lbl_Time.frame = CGRectMake(CGRectGetMaxX(lbl_personname.frame),5, 130, 15);
            }
            
            
            
            
            lbl_Chat.frame = CGRectMake(10,CGRectGetMaxY(lbl_Time.frame)+3, 210, 20);
            lbl_Chat.text = [NSString stringWithFormat:@"%@",decoded];
            CGSize constraint = CGSizeMake(210 ,10000.0f);
            CGSize chatText_size = [decoded  sizeWithFont:[UIFont fontWithName:kFont size:12.0f] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
            
            lbl_Chat.hidden = NO;
            if (chatText_size.height >55)
            {
                lbl_personname.frame =CGRectMake(70,5, 100, 15);
                lbl_Time.frame = CGRectMake(CGRectGetMaxX(lbl_personname.frame),5, 130, 15);
                imgView_PopUp.frame = CGRectMake(CGRectGetMaxX(img_Persons.frame)+2,6,230,  chatText_size.height +16+30);
                lbl_Chat.frame = CGRectMake(10,8, 210, chatText_size.height+20 );
                
            }
            
            
            //            lbl_Time.frame = CGRectMake(CGRectGetMaxX(img_Persons.frame)+180,CGRectGetMaxY(imgView_PopUp.frame), 70, 20);
        }
        
    }
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    [self.navigationController pushViewController:VC animated:NO];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma click_events


-(void)click_on_back_arrow:(UIButton *)sender
{
    NSLog(@"click_on_back_arrow");
}
-(void)click_on_logo:(UIButton *)sender
{
    NSLog(@"click_on_logo");
}
//-(void)click_on_msg_arrow:(UIButton *)sender
//{
//    NSLog(@"click_on_msg_arrow");
//}

- (void)refresh:(UIRefreshControl *)refreshControl {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (refreshControl.tag == -500)
        {
            
            //[self AFChatHistory];
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //
            //set the title while refreshing
            refreshControl.attributedTitle = [[NSAttributedString alloc]initWithString:@"Refreshing ..."];
            //set the date and time of refreshing
            //            NSDateFormatter *formattedDate = [[NSDateFormatter alloc]init];
            //            [formattedDate setDateFormat:@"MMM d, h:mm a"];
            //            NSString *lastupdated = [NSString stringWithFormat:@"Last Updated on %@",[formattedDate stringFromDate:[NSDate date]]];
            //            refreshControl.attributedTitle = [[NSAttributedString alloc]initWithString:lastupdated];
            
            
            [refreshControl endRefreshing];
        });
    });
}

-(void)click_on_msg_arrow:(UIButton *)sender
{
    
    //    [messages addObject:txtFld_Message.text];
    //    [tableView_ChatDetail reloadData];
    
    
    
    if (txt_message.text.length == 0)
        
    {
        [self popup_Alertview:@"Please enter text."];
        
    }
    else if (txt_message.text.length>0 && ![self validateString:txt_message.text])
    {
        
        [self popup_Alertview:@"Please enter text."];
        
    }
    else{
        
        [self AFSendMessage];
        
    }
    
    
    txt_message.text=@"";
    
}


#pragma mark TEXTFIELD DELEGATE




- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [scroll setContentOffset:CGPointMake(0, 256)];
    view_bottom.frame = CGRectMake(0,HEIGHT-256, WIDTH, 40);
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [scroll setContentOffset:CGPointMake(0, 0)];
    view_bottom.frame = CGRectMake(0,HEIGHT-40, WIDTH, 40);
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [alertviewBg removeFromSuperview];
    
    
}



#pragma mark Alertview Popup

-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:196.0/255.0 green:0.0/255.0 blue:52.0/255.0 alpha:1.0];
    lab_alertViewTitle.text=@"Taxi";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,160);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,70);
    if (IS_IPHONE_6) {
        
        lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    }
    else if (IS_IPHONE_6Plus) {
        
        lab_alertViewbody.font = [UIFont fontWithName:kFont size:15.0f];
        
    }
    else
    {
        lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
        
    }
    imageview_div.frame= CGRectMake(0,120,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,120,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
    
    
}


#pragma mark Validate empty text

- (BOOL)validateString:(NSString *) str
{
    NSString *TextFieldString = str;
    NSCharacterSet *TextFieldwhitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *TextFieldtrimmed = [TextFieldString stringByTrimmingCharactersInSet:TextFieldwhitespace];
    
    BOOL isValid;
    
    if([TextFieldtrimmed length] == 0)
    {
        isValid=NO;
    }
    else
    {
        isValid=YES;
    }
    
    return isValid;
}


# pragma mark ****** Send Message *******


-(void) AFSendMessage
{
    
    //sender_uid*, receiver_uid*, product_id*, message*
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    //    [self.view addSubview:delegate.load];
    //    [delegate LoadAnimation];
    //
    
    
    NSDictionary *params;
    
    if (txt_message.text.length >0)
    {
        params =@{
                  @"sender_uid"             :   [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                  @"receiver_uid"           :  _str_ReceiverID,
                  @"nid"                    :  _str_nid,
                  @"message"                :   [[NSString stringWithFormat:@"%@",txt_message.text]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                  @"order_id"               :  _str_orderId
                  
                  };
        
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kchatsendmessage
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        
        //        [delegate.load removeFromSuperview];
        [self ResponseSendMessage:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error){
                                         
                                         [view_bottom setUserInteractionEnabled:YES];
                                         [self.view setUserInteractionEnabled:YES];
                                         
                                         // [delegate.load removeFromSuperview];
                                         
                                         if([operation.response statusCode] == 406){
                                             return;
                                         }
                                         if([operation.response statusCode] == 403){
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             
                                             [self popup_Alertview:@"Please check your internet connection."];
                                         }
                                         else if ([[operation error] code] == -1001)
                                         {
                                             [self AFSendMessage];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseSendMessage :(NSDictionary * )TheDict
{
    NSLog(@"chat: %@",TheDict);
    
    [ary_message removeAllObjects];
    
    if ([[TheDict valueForKey:@"error"] isEqualToString:@"0"])
    {
        
        for(int i = 0; i < [[TheDict valueForKey:@"instance_list"] count] ; i++)
        {
            [ary_message addObject:[[TheDict valueForKey:@"instance_list"] objectAtIndex:i] ];
        }
        
        
        NSMutableArray *ary_TempFilter=[[NSMutableArray alloc]init];
        
        NSSortDescriptor *aSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:YES comparator:^(id obj1, id obj2) {
            
            if ([obj1 doubleValue] > [obj2 doubleValue]) {
                return (NSComparisonResult)NSOrderedDescending;
            }
            if ([obj1 doubleValue] < [obj2 doubleValue]) {
                return (NSComparisonResult)NSOrderedAscending;
            }
            return (NSComparisonResult)NSOrderedSame;
        }];
        ary_TempFilter = [NSMutableArray arrayWithArray:[ary_message sortedArrayUsingDescriptors:[NSArray arrayWithObject:aSortDescriptor]]];
        
        
        
        if ([ary_TempFilter count]>0)
        {
            [ary_message removeAllObjects];
            
            for (int i=0; i<[ary_TempFilter count]; i++)
            {
                [ary_message addObject:[ary_TempFilter objectAtIndex:i]];
            }
            
            
        }
        
        [self initializeArray];
        
        //    [txtFld_Message resignFirstResponder];
        
        
        
    }
    else if ([[TheDict valueForKey:@"error"] isEqualToString:@"1"])
    {
        
        
    }
    
    
    
}


-(void)initializeArray
{
    BOOL found;
    
    id responseArray = ary_message;
    
    
    [dict_Section removeAllObjects];
    
    NSMutableArray *responseInfoArray = [responseArray mutableCopy];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"EEEE, dd/MM/yyyy"];//"YYYY-MM-dd\'T\'HH:mm:ssZZZZZ"
    
    for (NSMutableDictionary *dict in responseInfoArray)
    {
        
        NSString *str_TimeStemp = [dict valueForKey:@"timestamp"];
        
        //        NSDate *date_Message = [NSDate dateWithTimeIntervalSince1970:[str_TimeStemp intValue]];
        
        NSDate *date_Message = [NSDate dateWithTimeIntervalSince1970:[[dict valueForKey:@"timestamp"] intValue]];
        
        NSString *str_Time = [formatter stringFromDate:date_Message];
        
        
        found = NO;
        
        
        if (!found)
        {
            //  [ary_TempSection addObject:c];
        }
        
        for (NSString *str in [dict_Section allKeys])
        {
            if ([str isEqualToString:str_Time])
            {
                found = YES;
            }
        }
        
        if (!found)
        {
            [dict_Section setValue:[[NSMutableArray alloc] init]  forKey:str_Time];
        }
    }
    
    
    
    NSMutableArray *tempArray = [NSMutableArray array];
    
    // fast enumeration of the array
    for (NSString *dateString in [dict_Section allKeys])
    {
        NSDate *date1 = [formatter dateFromString:dateString];
        
        NSString *str_Date = [NSString stringWithFormat:@"%f",date1.timeIntervalSince1970];
        [tempArray addObject:str_Date];
    }
    
    
    [dict_Section removeAllObjects];
    
    for (int i=0; i<[tempArray count]; i++)
    {
        [dict_Section setValue:[[NSMutableArray alloc] init]  forKey:[tempArray objectAtIndex:i]];
    }
    
    
    
    [[dict_Section allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
    
    
    // Loop again and sort the country into their respective keys
    
    
    for (NSDictionary *dict in ary_message)
    {
        NSString *str_TimeStemp = [dict valueForKey:@"timestamp"];
        
        NSDate *date_Message = [NSDate dateWithTimeIntervalSince1970:[str_TimeStemp intValue]];
        
        //   NSDate *date_Message = [NSDate dateWithTimeIntervalSince1970:[[dict valueForKey:@"time"] intValue]];
        
        NSString *str_Time = [formatter stringFromDate:date_Message];
        
        NSDate *date1 = [formatter dateFromString:str_Time];
        
        NSString *str_Date = [NSString stringWithFormat:@"%f",date1.timeIntervalSince1970];
        
        [[dict_Section objectForKey:str_Date] addObject:dict];
    }
    
    NSLog(@"dict_Section%@",dict_Section);
    
    [table_for_chat reloadData];
    
    if ([dict_Section count] != 0)
    {
        int lastRow = [[dict_Section valueForKey:[[[dict_Section allKeys] sortedArrayUsingSelector:@selector(compare:)] objectAtIndex:[[[dict_Section allKeys] sortedArrayUsingSelector:@selector(compare:)] count]-1]] count]-1;
        
        NSIndexPath *indexpath=[NSIndexPath indexPathForRow:lastRow inSection:[[[dict_Section allKeys] sortedArrayUsingSelector:@selector(compare:)] count]-1];
        
        [table_for_chat scrollToRowAtIndexPath:indexpath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
    
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
