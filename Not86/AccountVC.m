//
//  AccountVC.m
//  Not86
//
//  Created by User on 26/10/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "AccountVC.h"
#import "JWNavigationController.h"
#import "JWSlideMenuViewController.h"
#import "AppDelegate.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"


@interface AccountVC ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIScrollViewDelegate>

{
    UITextField *text_date;
    UITextField *text_date2;
    
    UIImageView * img_header;
    UILabel * lbl_account;
    
    
    UITableView * table_on_drop_down;
    NSMutableArray * array_head_names;
    
    
    UIView * view_for_account;
    UITableView * table_for_ordrelist;
    NSMutableArray * array_kitchen_name;
    NSMutableArray * array_invoice_no;
    NSMutableArray * array_order_no;
    NSMutableArray * array_img;
    NSMutableArray * array_invoice_date;
    NSMutableArray * array_order_date;
    NSMutableArray * array_serving_date;
    NSMutableArray * array_subtotal;
    NSMutableArray * array_delivery_fee;
    NSMutableArray * array_service_fee;
    
    
    UIView * view_for_sales_summery;
    UITableView  * table_in_sales_view;
    NSMutableArray * array_total_rate;
    NSMutableArray * array_net_rate;
    
    
    UIView * view_for_sales_tracker;
    NSMutableArray * array_dish_name;
    NSMutableArray * array_per_serving;
    NSMutableArray * total_sale_val;
    NSMutableArray * array_total;
    NSMutableArray * array_qty;
    UITableView * table_in_sales_tracker;
    NSMutableArray * array_labl;
    NSMutableArray * array_total_earn;
    
    
    UILabel * lbl_sold_val;
    UILabel * lbl_doller_val;
    UILabel * lbl_doller_val_in_trackerview;
    UILabel * lbl_doller_val_in_saleview;
    AppDelegate *delegate;
    NSMutableArray * array_for_accounts;
    NSMutableArray * array_total_Items;
    NSMutableArray * array_Sale_Summary;
    NSMutableArray * array_Sale_Tracker;
    NSMutableArray *array_Dish_Detail;
    
    UIToolbar*keyboardToolbar_Date;
    NSDateFormatter*formatter;
    UIDatePicker*datePicker;
    
    UIToolbar*keyboardToolbar_Date1;
    NSDateFormatter*formatter1;
    UIDatePicker*datePicker1;
    
    UIToolbar*keyboardToolbar_Date3;
    NSDateFormatter*formatter3;
    UIDatePicker*datePicker3;
    
    UIToolbar*keyboardToolbar_Date4;
    NSDateFormatter*formatter4;
    UIDatePicker*datePicker4;
    
    
    UIToolbar*keyboardToolbar_Date5;
    NSDateFormatter*formatter5;
    UIDatePicker*datePicker5;
    
    UIToolbar*keyboardToolbar_Date6;
    NSDateFormatter*formatter6;
    UIDatePicker*datePicker6;
    
    
    
    NSString*str_order;
    
    UITextField *text_date_in_saleview;
    UITextField *text_date2_in_saleview;
    UITextField *text_date_in_trackerview;
    UITextField *text_date2_in_trackerview;
    
    UITextField *txt_search;
    UITextField *txt_search_in_saleview;
    UITextField *txt_search_in_trackerview;
    NSMutableArray*ary_accountsfilter;
    NSMutableArray*array_total_Itemsfilter;
    NSMutableArray*array_Sale_Summaryfilter;
    NSMutableArray*array_Sale_Trackerfilters;
    NSMutableArray*array_Dish_Detailfilters;
    
    int search_leg;
    int search_legsales;
    int search_legsalestrack;
    
    
    
    
    
}

@end

@implementation AccountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    array_for_accounts = [NSMutableArray new];
    ary_accountsfilter = [NSMutableArray new];
    
    array_total_Items = [NSMutableArray new];
    array_total_Itemsfilter = [NSMutableArray new];
    
    array_Sale_Summary = [NSMutableArray new];
    array_Sale_Summaryfilter = [NSMutableArray new];
    
    array_Sale_Tracker = [NSMutableArray new];
    array_Sale_Trackerfilters= [NSMutableArray new];
    
    array_Dish_Detail = [NSMutableArray new];
    array_Dish_Detailfilters = [NSMutableArray new];
    
    
    str_order = [NSString new];
    str_order = @"ASC";
    
    
    [self integratehead];
    [self integratebody];
    self.view.backgroundColor = [UIColor colorWithRed:244/255.0f green:246/255.0f blue:245/255.0f alpha:1];
    array_head_names = [[NSMutableArray alloc]initWithObjects:@"Account",@"Sales Summary",@"Sales Tracker",nil];
    
    array_kitchen_name = [[NSMutableArray alloc]initWithObjects:@"Doe's kitchen",@"jon's kitchen",@"Doe's kitchen",@"jon's kitchen",@"Doe's kitchen",nil];
    array_invoice_no = [[NSMutableArray alloc]initWithObjects:@"89883",@"89883",@"89883",@"89883",@"89883", nil];
    array_order_no = [[NSMutableArray alloc]initWithObjects:@"89883",@"89883",@"89883",@"89883",@"89883",nil];
    array_invoice_date = [[NSMutableArray alloc]initWithObjects:@"16/07/2015",@"16/07/2015",@"16/07/2015",@"16/07/2015",@"16/07/2015",nil];
    array_order_date = [[NSMutableArray alloc]initWithObjects:@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",nil];
    array_serving_date = [[NSMutableArray alloc]initWithObjects:@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM", nil];
    array_img = [[NSMutableArray alloc]initWithObjects:@"img-user@2x.png",@"img-user@2x.png",@"img-user@2x.png",@"img-user@2x.png",@"img-user@2x.png",nil];
    
    array_subtotal = [[NSMutableArray alloc]initWithObjects:@"$14.00",@"$14.00",@"$14.00",@"$14.00",@"$14.00", nil];
    array_delivery_fee = [[NSMutableArray alloc]initWithObjects:@"$13.15",@"$13.15",@"$13.15",@"$13.15",@"$13.15", nil];
    array_service_fee  = [[NSMutableArray alloc]initWithObjects:@"$5.55",@"$5.55",@"$5.55",@"$5.55",@"$5.55", nil];
    
    
    array_total_rate = [[NSMutableArray alloc]initWithObjects:@"$42.00",@"$50.00",@"$60.70",@"$70.30",@"$50.45", nil];
    array_net_rate = [[NSMutableArray alloc]initWithObjects:@"$39.00",@"$39.00",@"$39.00",@"$39.00",@"$39.00", nil];
    
    
    //array_for_trackerview
    
    array_dish_name = [[NSMutableArray alloc]initWithObjects:@"Mexician Tacos",@"Fried Rice",@"Mexician Tacos",@"Fried Rice",@"Mexician Tacos",nil];
    array_per_serving = [[NSMutableArray alloc]initWithObjects:@"$1 per serving",@"$4 per serving",@"$1 per serving",@"$1 per serving",@"$1 per serving", nil];
    total_sale_val =[[NSMutableArray alloc]initWithObjects:@"20",@"10",@"20",@"20",@"20",nil];
    array_qty = [[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5", nil];
    array_labl = [[NSMutableArray alloc]initWithObjects:@"Dine-in",@"Take-out",@"Delivery:", nil];
    array_total_earn = [[NSMutableArray alloc]initWithObjects:@"$40.00",@"$40.00",@"$40.00",@"$40.00",@"$40.00",nil];
    
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self account_sections];
    [self AFSaleSummary];
    [self  AFSaleTracker];
}

-(void)integratehead
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH,45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu.frame = CGRectMake(10,13,20,20);
    icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
    [icon_menu addTarget:self action:@selector(btn_menu_click:) forControlEvents:UIControlEventTouchUpInside];
    // [self.view   addSubview:icon_menu];
    
    
    lbl_account = [[UILabel alloc]init];
    lbl_account.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, WIDTH-210, 45);
    lbl_account.text = @"Account";
    lbl_account.font = [UIFont fontWithName:kFont size:20];
    lbl_account.textColor = [UIColor whiteColor];
    lbl_account.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_account];
    
    UIButton *icon_drop_down = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_drop_down.frame = CGRectMake(CGRectGetMaxX(lbl_account.frame),15,20,20);
    icon_drop_down .backgroundColor = [UIColor clearColor];
    [icon_drop_down addTarget:self action:@selector(btn_drop_down:) forControlEvents:UIControlEventTouchUpInside];
    [icon_drop_down setImage:[UIImage imageNamed:@"white-dropd-@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_drop_down];
    
    UIButton *btn_on_food_later_in_head = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_food_later_in_head.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0,WIDTH-190,45);
    btn_on_food_later_in_head .backgroundColor = [UIColor clearColor];
    [btn_on_food_later_in_head addTarget:self action:@selector(btn_on_account_label_click:) forControlEvents:UIControlEventTouchUpInside];
    //[icon_drop_down setImage:[UIImage imageNamed:@"drop-down@2x.png"] forState:UIControlStateNormal];
    [self.view   addSubview:btn_on_food_later_in_head];
    
    
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 11, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
    
}
-(void)integratebody
{
    
    view_for_account = [[UIView alloc]init];
    view_for_account.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+380);
    [view_for_account setUserInteractionEnabled:YES];
    view_for_account.backgroundColor=[UIColor colorWithRed:244/255.0f green:246/255.0f blue:245/255.0f alpha:1];
    [self.view  addSubview:  view_for_account];
    
    UIImageView * white_bg = [[UIImageView alloc]init];
    white_bg.frame = CGRectMake(10,CGRectGetMinY(img_header.frame)+5,WIDTH-20,50);
    [white_bg setUserInteractionEnabled:YES];
    white_bg.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_account addSubview:white_bg];
    
    UIImageView *icon_up_and_down_arrow = [[UIImageView alloc]init];
    icon_up_and_down_arrow.frame = CGRectMake(5,10, 30, 30);
    [icon_up_and_down_arrow setImage:[UIImage imageNamed:@"img-up-down-arrow@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [icon_up_and_down_arrow setUserInteractionEnabled:YES];
    [white_bg addSubview:icon_up_and_down_arrow];
    
    
    
    UILabel * lbl_time = [[UILabel alloc]init];
    lbl_time.frame = CGRectMake(CGRectGetMidX(icon_up_and_down_arrow.frame)+30,0,100, 45);
    lbl_time.text = @"Time";
    lbl_time.font = [UIFont fontWithName:kFont size:20];
    lbl_time.textColor = [UIColor blackColor];
    lbl_time.backgroundColor = [UIColor clearColor];
    [lbl_time setUserInteractionEnabled:YES];
    [white_bg addSubview:lbl_time];
    
    UIButton *btn_sortacont = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_sortacont.frame = CGRectMake(5,0, 150, 50);
    btn_sortacont .userInteractionEnabled=YES;
    btn_sortacont .backgroundColor = [UIColor clearColor];
    [btn_sortacont addTarget:self action:@selector(btn_sortbyaccount_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [white_bg   addSubview:btn_sortacont];
    
    
    UIImageView *icon_line = [[UIImageView alloc]init];
    icon_line.frame = CGRectMake(CGRectGetMidX(lbl_time.frame)+10,10,1, 30);
    [icon_line setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [icon_line setUserInteractionEnabled:YES];
    [white_bg addSubview:icon_line];
    
    
    UIImageView *img_serch_bar = [[UIImageView alloc]init];
    img_serch_bar.frame = CGRectMake(CGRectGetMidX(icon_line.frame)+10,10,170, 30);
    [img_serch_bar setImage:[UIImage imageNamed:@"search-bg@2x.png"]];
    // icon_user.backgroundColor = [UIColor redColor];
    [img_serch_bar setUserInteractionEnabled:YES];
    [white_bg addSubview:img_serch_bar];
    
    
    txt_search = [[UITextField alloc] init];
    txt_search.frame = CGRectMake(0,0, 150, 30);
    txt_search .borderStyle = UITextBorderStyleNone;
    txt_search .textColor = [UIColor grayColor];
    txt_search .font = [UIFont fontWithName:kFont size:15];
    txt_search .placeholder = @"Search...";
    [txt_search  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_search  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_search .leftView = padding3;
    txt_search .leftViewMode = UITextFieldViewModeAlways;
    txt_search .userInteractionEnabled=YES;
    txt_search .textAlignment = NSTextAlignmentLeft;
    txt_search .backgroundColor = [UIColor clearColor];
    txt_search .keyboardType = UIKeyboardTypeAlphabet;
    txt_search .delegate = self;
    [img_serch_bar addSubview:txt_search ];
    
    
    UIImageView *icon_search = [[UIImageView alloc]init];
    icon_search.frame = CGRectMake(150,8, 15, 15);
    [icon_search setImage:[UIImage imageNamed:@"icon-search@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_search setUserInteractionEnabled:YES];
    [img_serch_bar addSubview:icon_search];
    
    UIButton *btn_on_search_bar = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_search_bar.frame = CGRectMake(150,0, 30, 30);
    btn_on_search_bar .userInteractionEnabled=YES;
    btn_on_search_bar .backgroundColor = [UIColor clearColor];
    [btn_on_search_bar addTarget:self action:@selector(btn_on_search_bar_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [img_serch_bar   addSubview:btn_on_search_bar];
    
    UIImageView * white_bg2 = [[UIImageView alloc]init];
    white_bg2.frame = CGRectMake(10,CGRectGetMaxY(white_bg.frame)+5,WIDTH-20,50);
    [white_bg2 setUserInteractionEnabled:YES];
    white_bg2.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_account addSubview:white_bg2];
    
    
    UIImageView * img_calender = [[UIImageView alloc]init];
    img_calender.frame = CGRectMake(20,10,30,30);
    [img_calender setUserInteractionEnabled:YES];
    img_calender.image=[UIImage imageNamed:@"img-calender@2x.png"];
    [white_bg2 addSubview:img_calender];
    
    
    text_date = [[UITextField alloc] init];
    text_date .frame = CGRectMake(CGRectGetMidX(img_calender.frame)+18,5,100,45);
    text_date .borderStyle = UITextBorderStyleNone;
    text_date .textColor = [UIColor blackColor];
    text_date .font = [UIFont fontWithName:kFont size:13];
    text_date .placeholder = @"From";
    [text_date  setValue:[UIFont fontWithName:kFont size:16] forKeyPath:@"_placeholderLabel.font"];
    [text_date  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    text_date .leftView = padding;
    text_date .leftViewMode = UITextFieldViewModeAlways;
    text_date .userInteractionEnabled=YES;
    text_date .textAlignment = NSTextAlignmentLeft;
    text_date .backgroundColor = [UIColor clearColor];
    text_date .delegate = self;
    [white_bg2 addSubview:text_date];
    
    if (keyboardToolbar_Date == nil)
    {
        keyboardToolbar_Date = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar_Date setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_DoneDate:)];
        [keyboardToolbar_Date setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    text_date.inputAccessoryView = keyboardToolbar_Date;
    text_date.backgroundColor=[UIColor clearColor];
    
    formatter = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    //datePicker.minimumDate = [formatter dateFromString:@"1900-01-01"];
    
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now];
    components.year = components.year  ;
    
    
    
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    datePicker.minimumDate = [formatter dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components.year-20, (long)components.month,(long)components.day]];
    
    datePicker.maximumDate = now;
    text_date.inputView = datePicker;
    
    
    
    
    UIImageView *icon_line2 = [[UIImageView alloc]init];
    icon_line2.frame = CGRectMake(CGRectGetMidX(text_date.frame)+50,10,1, 30);
    [icon_line2 setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [icon_line2 setUserInteractionEnabled:YES];
    [white_bg2 addSubview:icon_line2];
    
    
    UIImageView * img_calender2 = [[UIImageView alloc]init];
    img_calender2.frame = CGRectMake(CGRectGetMidX(icon_line2.frame)+20,10,30,30);
    [img_calender2 setUserInteractionEnabled:YES];
    img_calender2.image=[UIImage imageNamed:@"img-calender@2x.png"];
    [white_bg2 addSubview:img_calender2];
    
    
    text_date2 = [[UITextField alloc] init];
    text_date2 .frame = CGRectMake(CGRectGetMidX(img_calender2.frame)+18,5,100,45);
    text_date2 .borderStyle = UITextBorderStyleNone;
    text_date2 .textColor = [UIColor blackColor];
    text_date2 .font = [UIFont fontWithName:kFont size:13];
    text_date2 .placeholder = @"To";
    [text_date2  setValue:[UIFont fontWithName:kFont size:16] forKeyPath:@"_placeholderLabel.font"];
    [text_date2  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    text_date2 .leftView = padding2;
    text_date2 .leftViewMode = UITextFieldViewModeAlways;
    text_date2 .userInteractionEnabled=YES;
    text_date2 .textAlignment = NSTextAlignmentLeft;
    text_date2 .backgroundColor = [UIColor clearColor];
    text_date2 .keyboardType = UIKeyboardTypeAlphabet;
    text_date2 .delegate = self;
    [white_bg2 addSubview:text_date2];
    
    
    
    if (keyboardToolbar_Date1 == nil)
    {
        keyboardToolbar_Date1 = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar_Date1 setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_DoneDate1:)];
        [keyboardToolbar_Date1 setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    text_date2.inputAccessoryView = keyboardToolbar_Date1;
    text_date2.backgroundColor=[UIColor clearColor];
    
    formatter1 = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter1 setDateFormat:@"dd-MM-yyyy"];
    //datePicker.minimumDate = [formatter dateFromString:@"1900-01-01"];
    
    
    NSDate *now1 = [NSDate date];
    NSCalendar *calendar1 = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components1 = [calendar1 components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now1];
    components.year = components1.year  ;
    
    datePicker1 = [[UIDatePicker alloc] init];
    datePicker1.datePickerMode = UIDatePickerModeDate;
    [datePicker1 addTarget:self action:@selector(datePickerValueChanged1:) forControlEvents:UIControlEventValueChanged];
    datePicker1.minimumDate = [formatter1 dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components1.year-20, (long)components1.month,(long)components1.day]];
    
    datePicker1.maximumDate = now;
    
    text_date2.inputView = datePicker1;
    
    
    
    //    UILabel * lbl_date2 = [[UILabel alloc]init];
    //    lbl_date2.frame = CGRectMake(CGRectGetMidX(img_calender2.frame)+18,5,100,45);
    //    lbl_date2.text = @"17-05-2015";
    //    lbl_date2.font = [UIFont fontWithName:kFont size:15];
    //    lbl_date2.textColor = [UIColor blackColor];
    //    lbl_date2.backgroundColor = [UIColor clearColor];
    //    [white_bg2 addSubview:lbl_date2];
    //
    
    
    lbl_doller_val = [[UILabel alloc]init];
    lbl_doller_val.frame = CGRectMake(WIDTH/2-50,CGRectGetMaxY(white_bg2.frame),100, 45);
    //    lbl_doller_val.text = @"$423.30";
    //    lbl_doller_val.text = str_TotalAmount;
    lbl_doller_val.font = [UIFont fontWithName:kFontBold size:25];
    lbl_doller_val.textColor = [UIColor blackColor];
    lbl_doller_val.backgroundColor = [UIColor clearColor];
    [view_for_account addSubview:lbl_doller_val];
    
    
#pragma mark Tableview
    
    table_for_ordrelist = [[UITableView alloc] init ];
    table_for_ordrelist.frame  = CGRectMake(10,CGRectGetMaxY(lbl_doller_val.frame),WIDTH-20,350);
    [table_for_ordrelist setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_ordrelist.delegate = self;
    table_for_ordrelist.dataSource = self;
    table_for_ordrelist.showsVerticalScrollIndicator = NO;
    table_for_ordrelist.backgroundColor = [UIColor clearColor];
    // table_for_ordrelist.layer.borderWidth = 1.0;
    [view_for_account addSubview:table_for_ordrelist];
    
    
    
    
    
    
    
    view_for_sales_summery = [[UIView alloc]init];
    view_for_sales_summery.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+380);
    [view_for_sales_summery setUserInteractionEnabled:YES];
    view_for_sales_summery.backgroundColor=[UIColor colorWithRed:244/255.0f green:246/255.0f blue:245/255.0f alpha:1];
    [self.view  addSubview:  view_for_sales_summery];
    view_for_sales_summery.hidden = YES;
    
    
    
    UIImageView * white_bg_for_saleview = [[UIImageView alloc]init];
    white_bg_for_saleview.frame = CGRectMake(10,CGRectGetMinY(img_header.frame)+5,WIDTH-20,50);
    [white_bg_for_saleview setUserInteractionEnabled:YES];
    white_bg_for_saleview.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_sales_summery addSubview:white_bg_for_saleview];
    
    UIImageView *icon_up_and_down_arrow_in_salesview = [[UIImageView alloc]init];
    icon_up_and_down_arrow_in_salesview.frame = CGRectMake(5,10, 30, 30);
    [icon_up_and_down_arrow_in_salesview setImage:[UIImage imageNamed:@"img-up-down-arrow@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [icon_up_and_down_arrow_in_salesview setUserInteractionEnabled:YES];
    [white_bg_for_saleview addSubview:icon_up_and_down_arrow_in_salesview];
    
    UILabel * lbl_time_in_saleview = [[UILabel alloc]init];
    lbl_time_in_saleview.frame = CGRectMake(CGRectGetMidX(icon_up_and_down_arrow.frame)+30,0,100, 45);
    lbl_time_in_saleview.text = @"Time";
    lbl_time_in_saleview.font = [UIFont fontWithName:kFont size:20];
    lbl_time_in_saleview.textColor = [UIColor blackColor];
    lbl_time_in_saleview.backgroundColor = [UIColor clearColor];
    [white_bg_for_saleview addSubview:lbl_time_in_saleview];
    
    UIButton *btn_sortacontsales = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_sortacontsales.frame = CGRectMake(5,0, 150, 50);
    btn_sortacontsales .userInteractionEnabled=YES;
    btn_sortacontsales .backgroundColor = [UIColor clearColor];
    [btn_sortacontsales addTarget:self action:@selector(btn_sortbyaccountsummary_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [white_bg_for_saleview   addSubview:btn_sortacontsales];
    
    
    UIImageView *icon_line_in_saleview = [[UIImageView alloc]init];
    icon_line_in_saleview.frame = CGRectMake(CGRectGetMidX(lbl_time.frame)+10,10,1, 30);
    [icon_line_in_saleview setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [icon_line_in_saleview setUserInteractionEnabled:YES];
    [white_bg_for_saleview addSubview:icon_line_in_saleview];
    
    
    UIImageView *img_serch_bar_in_saleview = [[UIImageView alloc]init];
    img_serch_bar_in_saleview.frame = CGRectMake(CGRectGetMidX(icon_line.frame)+10,10,170, 30);
    [img_serch_bar_in_saleview setImage:[UIImage imageNamed:@"search-bg@2x.png"]];
    // icon_user.backgroundColor = [UIColor redColor];
    [img_serch_bar_in_saleview setUserInteractionEnabled:YES];
    [white_bg_for_saleview addSubview:img_serch_bar_in_saleview];
    
    
    txt_search_in_saleview = [[UITextField alloc] init];
    txt_search_in_saleview.frame = CGRectMake(0,0, 150, 30);
    txt_search_in_saleview .borderStyle = UITextBorderStyleNone;
    txt_search_in_saleview .textColor = [UIColor grayColor];
    txt_search_in_saleview .font = [UIFont fontWithName:kFont size:15];
    txt_search_in_saleview .placeholder = @"Search...";
    [txt_search_in_saleview  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_search_in_saleview  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_search_in_saleview .leftView = padding4;
    txt_search_in_saleview .leftViewMode = UITextFieldViewModeAlways;
    txt_search_in_saleview .userInteractionEnabled=YES;
    txt_search_in_saleview .textAlignment = NSTextAlignmentLeft;
    txt_search_in_saleview .backgroundColor = [UIColor clearColor];
    txt_search_in_saleview .keyboardType = UIKeyboardTypeAlphabet;
    txt_search_in_saleview .delegate = self;
    [img_serch_bar_in_saleview addSubview:txt_search_in_saleview ];
    
    
    UIImageView *icon_search_in_saleview = [[UIImageView alloc]init];
    icon_search_in_saleview.frame = CGRectMake(150,8, 15, 15);
    [icon_search_in_saleview setImage:[UIImage imageNamed:@"icon-search@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_search_in_saleview setUserInteractionEnabled:YES];
    [img_serch_bar_in_saleview addSubview:icon_search_in_saleview];
    
    UIButton *btn_on_search_bar_in_saleview = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_search_bar_in_saleview.frame = CGRectMake(150,0, 30, 30);
    btn_on_search_bar_in_saleview .userInteractionEnabled=YES;
    btn_on_search_bar_in_saleview .backgroundColor = [UIColor clearColor];
    [btn_on_search_bar_in_saleview addTarget:self action:@selector(btn_on_search_bar_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [img_serch_bar_in_saleview   addSubview:btn_on_search_bar_in_saleview];
    
    UIImageView * white_bg2_in_saleview = [[UIImageView alloc]init];
    white_bg2_in_saleview.frame = CGRectMake(10,CGRectGetMaxY(white_bg.frame)+5,WIDTH-20,50);
    [white_bg2_in_saleview setUserInteractionEnabled:YES];
    white_bg2_in_saleview.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_sales_summery addSubview:white_bg2_in_saleview];
    
    
    UIImageView * img_calender_in_saleview = [[UIImageView alloc]init];
    img_calender_in_saleview.frame = CGRectMake(20,10,30,30);
    [img_calender_in_saleview setUserInteractionEnabled:YES];
    img_calender_in_saleview.image=[UIImage imageNamed:@"img-calender@2x.png"];
    [white_bg2_in_saleview addSubview:img_calender_in_saleview];
    
    
    text_date_in_saleview = [[UITextField alloc] init];
    text_date_in_saleview .frame = CGRectMake(CGRectGetMidX(img_calender.frame)+18,5,100,45);
    text_date_in_saleview .borderStyle = UITextBorderStyleNone;
    text_date_in_saleview .textColor = [UIColor blackColor];
    text_date_in_saleview .font = [UIFont fontWithName:kFont size:13];
    text_date_in_saleview .placeholder = @"From";
    [text_date_in_saleview  setValue:[UIFont fontWithName:kFont size:16] forKeyPath:@"_placeholderLabel.font"];
    [text_date_in_saleview  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding5 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    text_date_in_saleview .leftView = padding5;
    text_date_in_saleview .leftViewMode = UITextFieldViewModeAlways;
    text_date_in_saleview .userInteractionEnabled=YES;
    text_date_in_saleview .textAlignment = NSTextAlignmentLeft;
    text_date_in_saleview .backgroundColor = [UIColor clearColor];
    text_date_in_saleview .keyboardType = UIKeyboardTypeAlphabet;
    text_date_in_saleview .delegate = self;
    [white_bg2_in_saleview addSubview:text_date_in_saleview];
    
    
    
    UIImageView *icon_line2_in_saleview = [[UIImageView alloc]init];
    icon_line2_in_saleview.frame = CGRectMake(CGRectGetMidX(text_date.frame)+50,10,1, 30);
    [icon_line2_in_saleview setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [icon_line2_in_saleview setUserInteractionEnabled:YES];
    [white_bg2_in_saleview addSubview:icon_line2_in_saleview];
    
    
    UIImageView * img_calender2_in_saleview = [[UIImageView alloc]init];
    img_calender2_in_saleview.frame = CGRectMake(CGRectGetMidX(icon_line2.frame)+20,10,30,30);
    [img_calender2_in_saleview setUserInteractionEnabled:YES];
    img_calender2_in_saleview.image=[UIImage imageNamed:@"img-calender@2x.png"];
    [white_bg2_in_saleview addSubview:img_calender2_in_saleview];
    
    
    text_date2_in_saleview = [[UITextField alloc] init];
    text_date2_in_saleview .frame = CGRectMake(CGRectGetMidX(img_calender2.frame)+18,5,100,45);
    text_date2_in_saleview .borderStyle = UITextBorderStyleNone;
    text_date2_in_saleview .textColor = [UIColor blackColor];
    text_date2_in_saleview .font = [UIFont fontWithName:kFont size:13];
    text_date2_in_saleview .placeholder = @"To";
    [text_date2_in_saleview  setValue:[UIFont fontWithName:kFont size:16] forKeyPath:@"_placeholderLabel.font"];
    [text_date2_in_saleview  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    text_date2_in_saleview .leftView = padding6;
    text_date2_in_saleview .leftViewMode = UITextFieldViewModeAlways;
    text_date2_in_saleview .userInteractionEnabled=YES;
    text_date2_in_saleview .textAlignment = NSTextAlignmentLeft;
    text_date2_in_saleview .backgroundColor = [UIColor clearColor];
    text_date2_in_saleview .keyboardType = UIKeyboardTypeAlphabet;
    text_date2_in_saleview .delegate = self;
    [white_bg2_in_saleview addSubview:text_date2_in_saleview];
    
    
    
    if (keyboardToolbar_Date3 == nil)
    {
        keyboardToolbar_Date3 = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar_Date3 setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_DoneDate1sales:)];
        [keyboardToolbar_Date3 setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    text_date_in_saleview.inputAccessoryView = keyboardToolbar_Date3;
    text_date_in_saleview.backgroundColor=[UIColor clearColor];
    
    formatter3 = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter3 setDateFormat:@"dd-MM-yyyy"];
    //datePicker.minimumDate = [formatter dateFromString:@"1900-01-01"];
    
    NSDate *now3 = [NSDate date];
    NSCalendar *calendar3 = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components3 = [calendar3 components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now3];
    components3.year = components3.year  ;
    
    
    
    
    datePicker3 = [[UIDatePicker alloc] init];
    datePicker3.datePickerMode = UIDatePickerModeDate;
    [datePicker3 addTarget:self action:@selector(click_DoneDate1sales:) forControlEvents:UIControlEventValueChanged];
    datePicker3.minimumDate = [formatter3 dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components3.year-20, (long)components3.month,(long)components3.day]];
    
    datePicker5.maximumDate = now3;
    text_date_in_saleview.inputView = datePicker3;
    
    
    if (keyboardToolbar_Date4 == nil)
    {
        keyboardToolbar_Date4 = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar_Date4 setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_DoneDate2sales:)];
        [keyboardToolbar_Date4 setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    text_date2_in_saleview.inputAccessoryView = keyboardToolbar_Date4;
    text_date2_in_saleview.backgroundColor=[UIColor clearColor];
    
    formatter4 = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter4 setDateFormat:@"dd-MM-yyyy"];
    //datePicker.minimumDate = [formatter dateFromString:@"1900-01-01"];
    
    
    NSDate *now4 = [NSDate date];
    NSCalendar *calendar4 = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components4 = [calendar4 components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now4];
    components4.year = components4.year  ;
    
    datePicker4 = [[UIDatePicker alloc] init];
    datePicker4.datePickerMode = UIDatePickerModeDate;
    [datePicker4 addTarget:self action:@selector(click_DoneDate2sales:) forControlEvents:UIControlEventValueChanged];
    datePicker4.minimumDate = [formatter4 dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components4.year-20, (long)components4.month,(long)components4.day]];
    
    datePicker4.maximumDate = now4;
    
    text_date2_in_saleview.inputView = datePicker6;
    
    
    
    
    //    UILabel * lbl_date2 = [[UILabel alloc]init];
    //    lbl_date2.frame = CGRectMake(CGRectGetMidX(img_calender2.frame)+18,5,100,45);
    //    lbl_date2.text = @"17-05-2015";
    //    lbl_date2.font = [UIFont fontWithName:kFont size:15];
    //    lbl_date2.textColor = [UIColor blackColor];
    //    lbl_date2.backgroundColor = [UIColor clearColor];
    //    [white_bg2 addSubview:lbl_date2];
    //
    
    
    
    lbl_doller_val_in_saleview = [[UILabel alloc]init];
    lbl_doller_val_in_saleview.frame = CGRectMake(WIDTH/2-50,CGRectGetMaxY(white_bg2.frame),100, 45);
    lbl_doller_val_in_saleview.text = @"";
    lbl_doller_val_in_saleview.font = [UIFont fontWithName:kFontBold size:25];
    lbl_doller_val_in_saleview.textColor = [UIColor blackColor];
    lbl_doller_val_in_saleview.backgroundColor = [UIColor clearColor];
    [view_for_sales_summery addSubview:lbl_doller_val_in_saleview];
    
    
    UIImageView * img_whit_bg = [[UIImageView alloc]init];
    img_whit_bg.frame = CGRectMake(5,CGRectGetMaxY(lbl_doller_val_in_saleview.frame)+10,WIDTH-10,360);
    [img_whit_bg setUserInteractionEnabled:YES];
    img_whit_bg.image=[UIImage imageNamed:@"img-whtcell-bg.png"];
    [view_for_sales_summery addSubview:img_whit_bg];
    
    
    UILabel * lbl_order_id = [[UILabel alloc]init];
    lbl_order_id.frame = CGRectMake(15,5,100, 45);
    lbl_order_id.text = @"Order ID";
    lbl_order_id.font = [UIFont fontWithName:kFontBold size:18];
    lbl_order_id.textColor = [UIColor blackColor];
    lbl_order_id.backgroundColor = [UIColor clearColor];
    [img_whit_bg addSubview:lbl_order_id];
    
    
    UIImageView * img_drop_down = [[UIImageView alloc]init];
    img_drop_down.frame = CGRectMake(CGRectGetMaxX(lbl_order_id.frame)-8,25,15,10);
    [img_drop_down setUserInteractionEnabled:YES];
    img_drop_down.image=[UIImage imageNamed:@"drop-down-icon@2x.png"];
    [img_whit_bg addSubview:img_drop_down];
    
    UIImageView * img_line = [[UIImageView alloc]init];
    img_line.frame = CGRectMake(CGRectGetMaxX(img_drop_down.frame)+10,7,1,50);
    [img_line setUserInteractionEnabled:YES];
    img_line.image=[UIImage imageNamed:@"img_small line@2x.png"];
    [img_whit_bg addSubview:img_line];
    
    UILabel * lbl_total_sal = [[UILabel alloc]init];
    lbl_total_sal.frame = CGRectMake(CGRectGetMaxX(img_line.frame)+5,5,100, 45);
    lbl_total_sal.text = @"Total\nSales";
    [lbl_total_sal setNumberOfLines:2];
    lbl_total_sal.font = [UIFont fontWithName:kFontBold size:18];
    lbl_total_sal.textColor = [UIColor blackColor];
    lbl_total_sal.backgroundColor = [UIColor clearColor];
    [img_whit_bg addSubview:lbl_total_sal];
    
    
    UIImageView * img_drop_down2 = [[UIImageView alloc]init];
    img_drop_down2.frame = CGRectMake(CGRectGetMidX(lbl_total_sal.frame)+5,25,15,10);
    [img_drop_down2 setUserInteractionEnabled:YES];
    img_drop_down2.image=[UIImage imageNamed:@"drop-down-icon@2x.png"];
    [img_whit_bg addSubview:img_drop_down2];
    
    UIImageView * img_line2 = [[UIImageView alloc]init];
    img_line2.frame = CGRectMake(CGRectGetMaxX(img_drop_down2.frame)+10,7,1,50);
    [img_line2 setUserInteractionEnabled:YES];
    img_line2.image=[UIImage imageNamed:@"img_small line@2x.png"];
    [img_whit_bg addSubview:img_line2];
    
    UILabel * lbl_net_sal = [[UILabel alloc]init];
    lbl_net_sal.frame = CGRectMake(CGRectGetMaxX(img_line2.frame)+2,5,100, 45);
    lbl_net_sal.text = @"Nett\nSales";
    [lbl_net_sal setNumberOfLines:2];
    lbl_net_sal.font = [UIFont fontWithName:kFontBold size:18];
    lbl_net_sal.textColor = [UIColor blackColor];
    lbl_net_sal.backgroundColor = [UIColor clearColor];
    [img_whit_bg addSubview:lbl_net_sal];
    
    
    UIImageView * img_drop_down3 = [[UIImageView alloc]init];
    img_drop_down3.frame = CGRectMake(CGRectGetMidX(lbl_net_sal.frame)+5,25,15,10);
    [img_drop_down3 setUserInteractionEnabled:YES];
    img_drop_down3.image=[UIImage imageNamed:@"drop-down-icon@2x.png"];
    [img_whit_bg addSubview:img_drop_down3];
    
    
    UIImageView * img_line3 = [[UIImageView alloc]init];
    img_line3.frame = CGRectMake(CGRectGetMaxX(img_drop_down3.frame)+10,7,1,50);
    [img_line3 setUserInteractionEnabled:YES];
    img_line3.image=[UIImage imageNamed:@"img_small line@2x.png"];
    [img_whit_bg addSubview:img_line3];
    
    UILabel * lbl_pdf = [[UILabel alloc]init];
    lbl_pdf.frame = CGRectMake(CGRectGetMaxX(img_line3.frame)+8,5,100, 45);
    lbl_pdf.text = @"Pdf";
    [lbl_pdf setNumberOfLines:2];
    lbl_pdf.font = [UIFont fontWithName:kFontBold size:18];
    lbl_pdf.textColor = [UIColor blackColor];
    lbl_pdf.backgroundColor = [UIColor clearColor];
    [img_whit_bg addSubview:lbl_pdf];
    
    UIImageView * img_line4 = [[UIImageView alloc]init];
    img_line4.frame = CGRectMake(5,CGRectGetMaxY(lbl_pdf.frame)+7,WIDTH-20,1);
    [img_line4 setUserInteractionEnabled:YES];
    img_line4.image=[UIImage imageNamed:@"img-line@2x.png"];
    [img_whit_bg addSubview:img_line4];
    
    
#pragma mark Tableview
    
    table_in_sales_view = [[UITableView alloc] init ];
    table_in_sales_view .frame  = CGRectMake(8,CGRectGetMaxY(img_line4.frame),WIDTH-29,280);
    [table_in_sales_view  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_in_sales_view .delegate = self;
    table_in_sales_view .dataSource = self;
    table_in_sales_view .showsVerticalScrollIndicator = NO;
    table_in_sales_view .backgroundColor = [UIColor clearColor];
    [img_whit_bg addSubview: table_in_sales_view ];
    
    
    
    
    view_for_sales_tracker = [[UIView alloc]init];
    view_for_sales_tracker.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+380);
    [view_for_sales_tracker setUserInteractionEnabled:YES];
    view_for_sales_tracker.backgroundColor=[UIColor colorWithRed:244/255.0f green:246/255.0f blue:245/255.0f alpha:1];
    [self.view  addSubview:  view_for_sales_tracker];
    view_for_sales_tracker.hidden = YES;
    
    
    
    UIImageView * white_bg_for_trackerview = [[UIImageView alloc]init];
    white_bg_for_trackerview.frame = CGRectMake(10,CGRectGetMinY(img_header.frame)+5,WIDTH-20,50);
    [white_bg_for_trackerview setUserInteractionEnabled:YES];
    white_bg_for_trackerview.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_sales_tracker addSubview:white_bg_for_trackerview];
    
    UIImageView *icon_up_and_down_arrow_in_trackerview = [[UIImageView alloc]init];
    icon_up_and_down_arrow_in_trackerview.frame = CGRectMake(5,10, 30, 30);
    [icon_up_and_down_arrow_in_trackerview setImage:[UIImage imageNamed:@"img-up-down-arrow@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [icon_up_and_down_arrow_in_trackerview setUserInteractionEnabled:YES];
    [white_bg_for_trackerview addSubview:icon_up_and_down_arrow_in_trackerview];
    
    UILabel * lbl_time_in_trackerview = [[UILabel alloc]init];
    lbl_time_in_trackerview.frame = CGRectMake(CGRectGetMidX(icon_up_and_down_arrow.frame)+30,0,100, 45);
    lbl_time_in_trackerview.text = @"Time";
    lbl_time_in_trackerview.font = [UIFont fontWithName:kFont size:20];
    lbl_time_in_trackerview.textColor = [UIColor blackColor];
    lbl_time_in_trackerview.backgroundColor = [UIColor clearColor];
    [white_bg_for_trackerview addSubview:lbl_time_in_trackerview];
    
    UIButton *btn_sortacontsalessu = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_sortacontsalessu.frame = CGRectMake(5,0, 150, 50);
    btn_sortacontsalessu .userInteractionEnabled=YES;
    btn_sortacontsalessu .backgroundColor = [UIColor clearColor];
    [btn_sortacontsalessu addTarget:self action:@selector(btn_sortbyaccounttrace_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [white_bg_for_trackerview   addSubview:btn_sortacontsales];
    
    
    UIImageView *icon_line_in_trackerview = [[UIImageView alloc]init];
    icon_line_in_trackerview.frame = CGRectMake(CGRectGetMidX(lbl_time.frame)+10,10,1, 30);
    [icon_line_in_trackerview setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [icon_line_in_trackerview setUserInteractionEnabled:YES];
    [white_bg_for_trackerview addSubview:icon_line_in_trackerview];
    
    
    UIImageView *img_serch_bar_in_trackerview = [[UIImageView alloc]init];
    img_serch_bar_in_trackerview.frame = CGRectMake(CGRectGetMidX(icon_line.frame)+10,10,170, 30);
    [img_serch_bar_in_trackerview setImage:[UIImage imageNamed:@"search-bg@2x.png"]];
    // icon_user.backgroundColor = [UIColor redColor];
    [img_serch_bar_in_trackerview setUserInteractionEnabled:YES];
    [white_bg_for_trackerview addSubview:img_serch_bar_in_trackerview];
    
    
    txt_search_in_trackerview = [[UITextField alloc] init];
    txt_search_in_trackerview.frame = CGRectMake(0,0, 150, 30);
    txt_search_in_trackerview .borderStyle = UITextBorderStyleNone;
    txt_search_in_trackerview .textColor = [UIColor grayColor];
    txt_search_in_trackerview .font = [UIFont fontWithName:kFont size:15];
    txt_search_in_trackerview .placeholder = @"Search...";
    [txt_search_in_trackerview  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_search_in_trackerview  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding7 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_search_in_trackerview .leftView = padding7;
    txt_search_in_trackerview .leftViewMode = UITextFieldViewModeAlways;
    txt_search_in_trackerview .userInteractionEnabled=YES;
    txt_search_in_trackerview .textAlignment = NSTextAlignmentLeft;
    txt_search_in_trackerview .backgroundColor = [UIColor clearColor];
    txt_search_in_trackerview .keyboardType = UIKeyboardTypeAlphabet;
    txt_search_in_trackerview .delegate = self;
    [img_serch_bar_in_trackerview addSubview:txt_search_in_trackerview ];
    
    
    UIImageView *icon_search_in_trackerview = [[UIImageView alloc]init];
    icon_search_in_trackerview.frame = CGRectMake(150,8, 15, 15);
    [icon_search_in_trackerview setImage:[UIImage imageNamed:@"icon-search@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_search_in_trackerview setUserInteractionEnabled:YES];
    [img_serch_bar_in_trackerview addSubview:icon_search_in_trackerview];
    
    UIButton *btn_on_search_bar_in_trackerview = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_search_bar_in_trackerview.frame = CGRectMake(150,0, 30, 30);
    btn_on_search_bar_in_trackerview .userInteractionEnabled=YES;
    btn_on_search_bar_in_trackerview .backgroundColor = [UIColor clearColor];
    [btn_on_search_bar_in_trackerview addTarget:self action:@selector(btn_on_search_bar_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [img_serch_bar_in_trackerview   addSubview:btn_on_search_bar_in_trackerview];
    
    UIImageView * white_bg2_in_trackerview = [[UIImageView alloc]init];
    white_bg2_in_trackerview.frame = CGRectMake(10,CGRectGetMaxY(white_bg.frame)+5,WIDTH-20,50);
    [white_bg2_in_trackerview setUserInteractionEnabled:YES];
    white_bg2_in_trackerview.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_sales_tracker addSubview:white_bg2_in_trackerview];
    
    
    UIImageView * img_calender_in_trackerview = [[UIImageView alloc]init];
    img_calender_in_trackerview.frame = CGRectMake(20,10,30,30);
    [img_calender_in_trackerview setUserInteractionEnabled:YES];
    img_calender_in_trackerview.image=[UIImage imageNamed:@"img-calender@2x.png"];
    [white_bg2_in_trackerview addSubview:img_calender_in_trackerview];
    
    
    text_date_in_trackerview = [[UITextField alloc] init];
    text_date_in_trackerview .frame = CGRectMake(CGRectGetMidX(img_calender.frame)+18,5,100,45);
    text_date_in_trackerview .borderStyle = UITextBorderStyleNone;
    text_date_in_trackerview .textColor = [UIColor blackColor];
    text_date_in_trackerview .font = [UIFont fontWithName:kFont size:13];
    text_date_in_trackerview .placeholder = @"From";
    [text_date_in_trackerview  setValue:[UIFont fontWithName:kFont size:16] forKeyPath:@"_placeholderLabel.font"];
    [text_date_in_trackerview  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding8 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    text_date_in_trackerview .leftView = padding8;
    text_date_in_trackerview .leftViewMode = UITextFieldViewModeAlways;
    text_date_in_trackerview .userInteractionEnabled=YES;
    text_date_in_trackerview .textAlignment = NSTextAlignmentLeft;
    text_date_in_trackerview .backgroundColor = [UIColor clearColor];
    text_date_in_trackerview .keyboardType = UIKeyboardTypeAlphabet;
    text_date_in_trackerview .delegate = self;
    [white_bg2_in_trackerview addSubview:text_date_in_trackerview];
    
    
    
    UIImageView *icon_line2_in_trackerview = [[UIImageView alloc]init];
    icon_line2_in_trackerview.frame = CGRectMake(CGRectGetMidX(text_date.frame)+50,10,1, 30);
    [icon_line2_in_trackerview setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [icon_line2_in_trackerview setUserInteractionEnabled:YES];
    [white_bg2_in_trackerview addSubview:icon_line2_in_trackerview];
    
    
    UIImageView * img_calender2_in_trackerview = [[UIImageView alloc]init];
    img_calender2_in_trackerview.frame = CGRectMake(CGRectGetMidX(icon_line2.frame)+20,10,30,30);
    [img_calender2_in_trackerview setUserInteractionEnabled:YES];
    img_calender2_in_trackerview.image=[UIImage imageNamed:@"img-calender@2x.png"];
    [white_bg2_in_trackerview addSubview:img_calender2_in_trackerview];
    
    
    text_date2_in_trackerview = [[UITextField alloc] init];
    text_date2_in_trackerview .frame = CGRectMake(CGRectGetMidX(img_calender2.frame)+18,5,100,45);
    text_date2_in_trackerview .borderStyle = UITextBorderStyleNone;
    text_date2_in_trackerview .textColor = [UIColor blackColor];
    text_date2_in_trackerview .font = [UIFont fontWithName:kFont size:13];
    text_date2_in_trackerview .placeholder = @"To";
    [text_date2_in_trackerview  setValue:[UIFont fontWithName:kFont size:16] forKeyPath:@"_placeholderLabel.font"];
    [text_date2_in_trackerview  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding9 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    text_date2_in_trackerview .leftView = padding9;
    text_date2_in_trackerview .leftViewMode = UITextFieldViewModeAlways;
    text_date2_in_trackerview .userInteractionEnabled=YES;
    text_date2_in_trackerview .textAlignment = NSTextAlignmentLeft;
    text_date2_in_trackerview .backgroundColor = [UIColor clearColor];
    text_date2_in_trackerview .keyboardType = UIKeyboardTypeAlphabet;
    text_date2_in_trackerview .delegate = self;
    [white_bg2_in_trackerview addSubview:text_date2_in_trackerview];
    
    
    
    if (keyboardToolbar_Date5 == nil)
    {
        keyboardToolbar_Date5 = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar_Date5 setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(datePickerValueChangedtrace1:)];
        [keyboardToolbar_Date5 setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    text_date_in_trackerview.inputAccessoryView = keyboardToolbar_Date5;
    text_date_in_trackerview.backgroundColor=[UIColor clearColor];
    
    formatter5 = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter5 setDateFormat:@"dd-MM-yyyy"];
    //datePicker.minimumDate = [formatter dateFromString:@"1900-01-01"];
    
    NSDate *now5 = [NSDate date];
    NSCalendar *calendar5 = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components5 = [calendar5 components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now5];
    components5.year = components5.year  ;
    
    
    
    
    datePicker5 = [[UIDatePicker alloc] init];
    datePicker5.datePickerMode = UIDatePickerModeDate;
    [datePicker5 addTarget:self action:@selector(datePickerValueChangedtrace1:) forControlEvents:UIControlEventValueChanged];
    datePicker5.minimumDate = [formatter5 dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components5.year-20, (long)components5.month,(long)components5.day]];
    
    datePicker5.maximumDate = now5;
    text_date_in_trackerview.inputView = datePicker5;
    
    
    if (keyboardToolbar_Date6 == nil)
    {
        keyboardToolbar_Date6 = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar_Date6 setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(datePickerValueChangedtrace2:)];
        [keyboardToolbar_Date6 setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    text_date2_in_trackerview.inputAccessoryView = keyboardToolbar_Date6;
    text_date2_in_trackerview.backgroundColor=[UIColor clearColor];
    
    formatter6 = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter6 setDateFormat:@"dd-MM-yyyy"];
    //datePicker.minimumDate = [formatter dateFromString:@"1900-01-01"];
    
    
    NSDate *now6 = [NSDate date];
    NSCalendar *calendar6 = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components6 = [calendar6 components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now6];
    components6.year = components6.year  ;
    
    datePicker6 = [[UIDatePicker alloc] init];
    datePicker6.datePickerMode = UIDatePickerModeDate;
    [datePicker6 addTarget:self action:@selector(datePickerValueChangedtrace2:) forControlEvents:UIControlEventValueChanged];
    datePicker6.minimumDate = [formatter6 dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components6.year-20, (long)components6.month,(long)components6.day]];
    
    datePicker6.maximumDate = now6;
    
    text_date2_in_trackerview.inputView = datePicker6;
    
    
    
    
    //    UILabel * lbl_date2 = [[UILabel alloc]init];
    //    lbl_date2.frame = CGRectMake(CGRectGetMidX(img_calender2.frame)+18,5,100,45);
    //    lbl_date2.text = @"17-05-2015";
    //    lbl_date2.font = [UIFont fontWithName:kFont size:15];
    //    lbl_date2.textColor = [UIColor blackColor];
    //    lbl_date2.backgroundColor = [UIColor clearColor];
    //    [white_bg2 addSubview:lbl_date2];
    //
    
    lbl_sold_val = [[UILabel alloc]init];
    lbl_sold_val.frame = CGRectMake(70,CGRectGetMaxY(white_bg2.frame),150, 45);
    lbl_sold_val.text = @"";
    lbl_sold_val.font = [UIFont fontWithName:kFontBold size:25];
    lbl_sold_val.textColor = [UIColor blackColor];
    lbl_sold_val.backgroundColor = [UIColor clearColor];
    [view_for_sales_tracker addSubview:lbl_sold_val];
    
    
    lbl_doller_val_in_trackerview = [[UILabel alloc]init];
    lbl_doller_val_in_trackerview.frame = CGRectMake(CGRectGetMaxX(lbl_sold_val.frame)-10,CGRectGetMaxY(white_bg2.frame),100, 45);
    lbl_doller_val_in_trackerview.text = @"";
    lbl_doller_val_in_trackerview.font = [UIFont fontWithName:kFontBold size:25];
    lbl_doller_val_in_trackerview.textColor = [UIColor blackColor];
    lbl_doller_val_in_trackerview.backgroundColor = [UIColor clearColor];
    [view_for_sales_tracker addSubview:lbl_doller_val_in_trackerview];
    
    
    
    
    table_in_sales_tracker = [[UITableView alloc] init ];
    table_in_sales_tracker .frame  = CGRectMake(8,CGRectGetMaxY(lbl_sold_val.frame),WIDTH-29,455);
    [table_in_sales_tracker  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_in_sales_tracker .delegate = self;
    table_in_sales_tracker .dataSource = self;
    table_in_sales_tracker .showsVerticalScrollIndicator = NO;
    table_in_sales_tracker .backgroundColor = [UIColor clearColor];
    [view_for_sales_tracker addSubview: table_in_sales_tracker ];
    
    
    
    
    
    
    
#pragma mark Tableview
    
    table_on_drop_down = [[UITableView alloc] init ];
    table_on_drop_down .frame  = CGRectMake(60,45,WIDTH-170,90);
    [table_on_drop_down  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_on_drop_down .delegate = self;
    table_on_drop_down .dataSource = self;
    table_on_drop_down .showsVerticalScrollIndicator = NO;
    table_on_drop_down .backgroundColor = [UIColor clearColor];
    table_on_drop_down.hidden = YES;
    [self.view addSubview: table_on_drop_down ];
    
    if (IS_IPHONE_6Plus)
    {
        view_for_account.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+380);
        white_bg.frame = CGRectMake(10,CGRectGetMinY(img_header.frame)+5,WIDTH-20,50);
        icon_up_and_down_arrow.frame = CGRectMake(5,10, 30, 30);
        lbl_time.frame = CGRectMake(CGRectGetMidX(icon_up_and_down_arrow.frame)+30,0,100, 45);
        btn_sortacont.frame = CGRectMake(5,0, 150, 50);
        
        icon_line.frame = CGRectMake(CGRectGetMidX(lbl_time.frame)+10,10,1, 30);
        img_serch_bar.frame = CGRectMake(CGRectGetMidX(icon_line.frame)+10,10,260, 30);
        txt_search.frame = CGRectMake(0,0, 150, 30);
        icon_search.frame = CGRectMake(230,8, 15, 15);
        btn_on_search_bar.frame = CGRectMake(230,0, 30, 30);
        white_bg2.frame = CGRectMake(10,CGRectGetMaxY(white_bg.frame)+5,WIDTH-20,50);
        img_calender.frame = CGRectMake(20,10,30,30);
        text_date .frame = CGRectMake(CGRectGetMidX(img_calender.frame)+18,5,100,45);
        icon_line2.frame = CGRectMake(CGRectGetMidX(text_date.frame)+80,10,1, 30);
        img_calender2.frame = CGRectMake(CGRectGetMidX(icon_line2.frame)+30,10,30,30);
        text_date2 .frame = CGRectMake(CGRectGetMidX(img_calender2.frame)+18,5,100,45);
        lbl_doller_val.frame = CGRectMake(WIDTH/2-50,CGRectGetMaxY(white_bg2.frame),100, 45);
        table_for_ordrelist.frame  = CGRectMake(10,CGRectGetMaxY(lbl_doller_val.frame),WIDTH-20,520);
        
        view_for_sales_summery.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+380);
        white_bg_for_saleview.frame = CGRectMake(10,CGRectGetMinY(img_header.frame)+5,WIDTH-20,50);
        icon_up_and_down_arrow_in_salesview.frame = CGRectMake(5,10, 30, 30);
        lbl_time_in_saleview.frame = CGRectMake(CGRectGetMidX(icon_up_and_down_arrow.frame)+30,0,100, 45);
        btn_sortacontsales.frame = CGRectMake(5,0, 150, 50);
        
        icon_line_in_saleview.frame = CGRectMake(CGRectGetMidX(lbl_time.frame)+10,10,1, 30);
        img_serch_bar_in_saleview.frame = CGRectMake(CGRectGetMidX(icon_line.frame)+10,10,260, 30);
        txt_search_in_saleview.frame = CGRectMake(0,0, 150, 30);
        icon_search_in_saleview.frame = CGRectMake(230,8, 15, 15);
        btn_on_search_bar_in_saleview.frame = CGRectMake(230,0, 30, 30);
        
        white_bg2_in_saleview.frame = CGRectMake(10,CGRectGetMaxY(white_bg.frame)+5,WIDTH-20,50);
        img_calender_in_saleview.frame = CGRectMake(20,10,30,30);
        text_date_in_saleview .frame = CGRectMake(CGRectGetMidX(img_calender.frame)+18,5,100,45);
        icon_line2_in_saleview.frame = CGRectMake(CGRectGetMidX(text_date.frame)+80,10,1, 30);
        img_calender2_in_saleview.frame = CGRectMake(CGRectGetMidX(icon_line2.frame)+30,10,30,30);
        text_date2_in_saleview .frame = CGRectMake(CGRectGetMidX(img_calender2.frame)+18,5,100,45);
        
        lbl_doller_val_in_saleview.frame = CGRectMake(WIDTH/2-50,CGRectGetMaxY(white_bg2.frame),100, 45);
        img_whit_bg.frame = CGRectMake(5,CGRectGetMaxY(lbl_doller_val_in_saleview.frame)+10,WIDTH-10,360);
        lbl_order_id.frame = CGRectMake(15,5,100, 45);
        img_drop_down.frame = CGRectMake(CGRectGetMaxX(lbl_order_id.frame)-8,25,15,10);
        img_line.frame = CGRectMake(CGRectGetMaxX(img_drop_down.frame)+10,7,1,50);
        lbl_total_sal.frame = CGRectMake(CGRectGetMaxX(img_line.frame)+5,5,100, 45);
        img_drop_down2.frame = CGRectMake(CGRectGetMidX(lbl_total_sal.frame)+5,25,15,10);
        img_line2.frame = CGRectMake(CGRectGetMaxX(img_drop_down2.frame)+10,7,1,50);
        lbl_net_sal.frame = CGRectMake(CGRectGetMaxX(img_line2.frame)+2,5,100, 45);
        img_drop_down3.frame = CGRectMake(CGRectGetMidX(lbl_net_sal.frame)+5,25,15,10);
        img_line3.frame = CGRectMake(CGRectGetMaxX(img_drop_down3.frame)+10,7,1,50);
        lbl_pdf.frame = CGRectMake(CGRectGetMaxX(img_line3.frame)+8,5,100, 45);
        img_line4.frame = CGRectMake(5,CGRectGetMaxY(lbl_pdf.frame)+7,WIDTH-20,1);
        table_in_sales_view .frame  = CGRectMake(8,CGRectGetMaxY(img_line4.frame),WIDTH-29,280);
        
        view_for_sales_tracker.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+380);
        white_bg_for_trackerview.frame = CGRectMake(10,CGRectGetMinY(img_header.frame)+5,WIDTH-20,50);
        icon_up_and_down_arrow_in_trackerview.frame = CGRectMake(5,10, 30, 30);
        lbl_time_in_trackerview.frame = CGRectMake(CGRectGetMidX(icon_up_and_down_arrow.frame)+30,0,100, 45);
        btn_sortacontsalessu.frame = CGRectMake(5,0, 150, 50);
        
        icon_line_in_trackerview.frame = CGRectMake(CGRectGetMidX(lbl_time.frame)+10,10,1, 30);
        img_serch_bar_in_trackerview.frame = CGRectMake(CGRectGetMidX(icon_line.frame)+10,10,260, 30);
        txt_search_in_trackerview.frame = CGRectMake(0,0, 150, 30);
        icon_search_in_trackerview.frame = CGRectMake(230,8, 15, 15);
        btn_on_search_bar_in_trackerview.frame = CGRectMake(230,0, 30, 30);
        
        white_bg2_in_trackerview.frame = CGRectMake(10,CGRectGetMaxY(white_bg.frame)+5,WIDTH-20,50);
        img_calender_in_trackerview.frame = CGRectMake(20,10,30,30);
        text_date_in_trackerview .frame = CGRectMake(CGRectGetMidX(img_calender.frame)+18,5,100,45);
        icon_line2_in_trackerview.frame = CGRectMake(CGRectGetMidX(text_date.frame)+80,10,1, 30);
        img_calender2_in_trackerview.frame = CGRectMake(CGRectGetMidX(icon_line2.frame)+30,10,30,30);
        text_date2_in_trackerview .frame = CGRectMake(CGRectGetMidX(img_calender2.frame)+18,5,100,45);
        
        lbl_sold_val.frame = CGRectMake(70,CGRectGetMaxY(white_bg2.frame),150, 45);
        lbl_doller_val_in_trackerview.frame = CGRectMake(CGRectGetMaxX(lbl_sold_val.frame)-10,CGRectGetMaxY(white_bg2.frame),100, 45);
        table_in_sales_tracker .frame  = CGRectMake(8,CGRectGetMaxY(lbl_sold_val.frame),WIDTH-20,455);
        table_on_drop_down .frame  = CGRectMake(60,45,150,100);
        
    }
    else if (IS_IPHONE_6)
    {
        view_for_account.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+380);
        white_bg.frame = CGRectMake(10,CGRectGetMinY(img_header.frame)+5,WIDTH-20,50);
        icon_up_and_down_arrow.frame = CGRectMake(5,10, 30, 30);
        lbl_time.frame = CGRectMake(CGRectGetMidX(icon_up_and_down_arrow.frame)+30,0,100, 45);
        btn_sortacont.frame = CGRectMake(5,0, 150, 50);
        
        icon_line.frame = CGRectMake(CGRectGetMidX(lbl_time.frame)+10,10,1, 30);
        img_serch_bar.frame = CGRectMake(CGRectGetMidX(icon_line.frame)+10,10,230, 30);
        txt_search.frame = CGRectMake(0,0, 150, 30);
        icon_search.frame = CGRectMake(200,8, 15, 15);
        btn_on_search_bar.frame = CGRectMake(200,0, 30, 30);
        white_bg2.frame = CGRectMake(10,CGRectGetMaxY(white_bg.frame)+5,WIDTH-20,50);
        img_calender.frame = CGRectMake(20,10,30,30);
        text_date .frame = CGRectMake(CGRectGetMidX(img_calender.frame)+18,5,100,45);
        icon_line2.frame = CGRectMake(CGRectGetMidX(text_date.frame)+50,10,1, 30);
        img_calender2.frame = CGRectMake(CGRectGetMidX(icon_line2.frame)+20,10,30,30);
        text_date2 .frame = CGRectMake(CGRectGetMidX(img_calender2.frame)+18,5,100,45);
        lbl_doller_val.frame = CGRectMake(WIDTH/2-50,CGRectGetMaxY(white_bg2.frame),100, 45);
        table_for_ordrelist.frame  = CGRectMake(10,CGRectGetMaxY(lbl_doller_val.frame),WIDTH-20,450);
        
        view_for_sales_summery.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+380);
        white_bg_for_saleview.frame = CGRectMake(10,CGRectGetMinY(img_header.frame)+5,WIDTH-20,50);
        icon_up_and_down_arrow_in_salesview.frame = CGRectMake(5,10, 30, 30);
        lbl_time_in_saleview.frame = CGRectMake(CGRectGetMidX(icon_up_and_down_arrow.frame)+30,0,100, 45);
        btn_sortacontsales.frame = CGRectMake(5,0, 150, 50);
        
        icon_line_in_saleview.frame = CGRectMake(CGRectGetMidX(lbl_time.frame)+10,10,1, 30);
        img_serch_bar_in_saleview.frame = CGRectMake(CGRectGetMidX(icon_line.frame)+10,10,230, 30);
        txt_search_in_saleview.frame = CGRectMake(0,0, 150, 30);
        icon_search_in_saleview.frame = CGRectMake(200,8, 15, 15);
        btn_on_search_bar_in_saleview.frame = CGRectMake(200,0, 30, 30);
        
        white_bg2_in_saleview.frame = CGRectMake(10,CGRectGetMaxY(white_bg.frame)+5,WIDTH-20,50);
        img_calender_in_saleview.frame = CGRectMake(20,10,30,30);
        text_date_in_saleview .frame = CGRectMake(CGRectGetMidX(img_calender.frame)+18,5,100,45);
        icon_line2_in_saleview.frame = CGRectMake(CGRectGetMidX(text_date.frame)+50,10,1, 30);
        img_calender2_in_saleview.frame = CGRectMake(CGRectGetMidX(icon_line2.frame)+20,10,30,30);
        text_date2_in_saleview .frame = CGRectMake(CGRectGetMidX(img_calender2.frame)+18,5,100,45);
        
        lbl_doller_val_in_saleview.frame = CGRectMake(WIDTH/2-50,CGRectGetMaxY(white_bg2.frame),100, 45);
        img_whit_bg.frame = CGRectMake(5,CGRectGetMaxY(lbl_doller_val_in_saleview.frame)+10,WIDTH-10,360);
        lbl_order_id.frame = CGRectMake(15,5,100, 45);
        img_drop_down.frame = CGRectMake(CGRectGetMaxX(lbl_order_id.frame)-8,25,15,10);
        img_line.frame = CGRectMake(CGRectGetMaxX(img_drop_down.frame)+10,7,1,50);
        lbl_total_sal.frame = CGRectMake(CGRectGetMaxX(img_line.frame)+5,5,100, 45);
        img_drop_down2.frame = CGRectMake(CGRectGetMidX(lbl_total_sal.frame)+5,25,15,10);
        img_line2.frame = CGRectMake(CGRectGetMaxX(img_drop_down2.frame)+10,7,1,50);
        lbl_net_sal.frame = CGRectMake(CGRectGetMaxX(img_line2.frame)+2,5,100, 45);
        img_drop_down3.frame = CGRectMake(CGRectGetMidX(lbl_net_sal.frame)+5,25,15,10);
        img_line3.frame = CGRectMake(CGRectGetMaxX(img_drop_down3.frame)+10,7,1,50);
        lbl_pdf.frame = CGRectMake(CGRectGetMaxX(img_line3.frame)+8,5,100, 45);
        img_line4.frame = CGRectMake(5,CGRectGetMaxY(lbl_pdf.frame)+7,WIDTH-22,1);
        table_in_sales_view .frame  = CGRectMake(8,CGRectGetMaxY(img_line4.frame),WIDTH-29,280);
        
        view_for_sales_tracker.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+380);
        white_bg_for_trackerview.frame = CGRectMake(10,CGRectGetMinY(img_header.frame)+5,WIDTH-20,50);
        icon_up_and_down_arrow_in_trackerview.frame = CGRectMake(5,10, 30, 30);
        lbl_time_in_trackerview.frame = CGRectMake(CGRectGetMidX(icon_up_and_down_arrow.frame)+30,0,100, 45);
        btn_sortacontsalessu.frame = CGRectMake(5,0, 150, 50);
        
        icon_line_in_trackerview.frame = CGRectMake(CGRectGetMidX(lbl_time.frame)+10,10,1, 30);
        img_serch_bar_in_trackerview.frame = CGRectMake(CGRectGetMidX(icon_line.frame)+10,10,230, 30);
        txt_search_in_trackerview.frame = CGRectMake(0,0, 150, 30);
        icon_search_in_trackerview.frame = CGRectMake(200,8, 15, 15);
        btn_on_search_bar_in_trackerview.frame = CGRectMake(200,0, 30, 30);
        
        white_bg2_in_trackerview.frame = CGRectMake(10,CGRectGetMaxY(white_bg.frame)+5,WIDTH-20,50);
        img_calender_in_trackerview.frame = CGRectMake(20,10,30,30);
        text_date_in_trackerview .frame = CGRectMake(CGRectGetMidX(img_calender.frame)+18,5,100,45);
        icon_line2_in_trackerview.frame = CGRectMake(CGRectGetMidX(text_date.frame)+50,10,1, 30);
        img_calender2_in_trackerview.frame = CGRectMake(CGRectGetMidX(icon_line2.frame)+20,10,30,30);
        text_date2_in_trackerview .frame = CGRectMake(CGRectGetMidX(img_calender2.frame)+18,5,100,45);
        
        lbl_sold_val.frame = CGRectMake(40,CGRectGetMaxY(white_bg2.frame),150, 45);
        lbl_doller_val_in_trackerview.frame = CGRectMake(CGRectGetMaxX(lbl_sold_val.frame)-10,CGRectGetMaxY(white_bg2.frame),150, 45);
        table_in_sales_tracker .frame  = CGRectMake(8,CGRectGetMaxY(lbl_sold_val.frame),WIDTH-29,455);
        table_on_drop_down .frame  = CGRectMake(60,45,150,100);
        
        lbl_sold_val.font = [UIFont fontWithName:kFontBold size:20];
        lbl_doller_val_in_trackerview.font = [UIFont fontWithName:kFontBold size:20];
    }
    else if (IS_IPHONE_5)
    {
        view_for_account.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+380);
        white_bg.frame = CGRectMake(10,CGRectGetMinY(img_header.frame)+5,WIDTH-20,50);
        icon_up_and_down_arrow.frame = CGRectMake(5,10, 30, 30);
        lbl_time.frame = CGRectMake(CGRectGetMidX(icon_up_and_down_arrow.frame)+30,0,100, 45);
        btn_sortacont.frame = CGRectMake(5,0, 150, 50);
        
        icon_line.frame = CGRectMake(CGRectGetMidX(lbl_time.frame)+10,10,1, 30);
        img_serch_bar.frame = CGRectMake(CGRectGetMidX(icon_line.frame)+10,10,170, 30);
        txt_search.frame = CGRectMake(0,0, 150, 30);
        icon_search.frame = CGRectMake(150,8, 15, 15);
        btn_on_search_bar.frame = CGRectMake(150,0, 30, 30);
        white_bg2.frame = CGRectMake(10,CGRectGetMaxY(white_bg.frame)+5,WIDTH-20,50);
        img_calender.frame = CGRectMake(20,10,30,30);
        text_date .frame = CGRectMake(CGRectGetMidX(img_calender.frame)+18,5,100,45);
        icon_line2.frame = CGRectMake(CGRectGetMidX(text_date.frame)+50,10,1, 30);
        img_calender2.frame = CGRectMake(CGRectGetMidX(icon_line2.frame)+20,10,30,30);
        text_date2 .frame = CGRectMake(CGRectGetMidX(img_calender2.frame)+18,5,100,45);
        lbl_doller_val.frame = CGRectMake(WIDTH/2-50,CGRectGetMaxY(white_bg2.frame),100, 45);
        table_for_ordrelist.frame  = CGRectMake(10,CGRectGetMaxY(lbl_doller_val.frame),WIDTH-20,350);
        
        view_for_sales_summery.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+380);
        white_bg_for_saleview.frame = CGRectMake(10,CGRectGetMinY(img_header.frame)+5,WIDTH-20,50);
        icon_up_and_down_arrow_in_salesview.frame = CGRectMake(5,10, 30, 30);
        lbl_time_in_saleview.frame = CGRectMake(CGRectGetMidX(icon_up_and_down_arrow.frame)+30,0,100, 45);
        btn_sortacontsales.frame = CGRectMake(5,0, 150, 50);
        
        icon_line_in_saleview.frame = CGRectMake(CGRectGetMidX(lbl_time.frame)+10,10,1, 30);
        img_serch_bar_in_saleview.frame = CGRectMake(CGRectGetMidX(icon_line.frame)+10,10,170, 30);
        txt_search_in_saleview.frame = CGRectMake(0,0, 150, 30);
        icon_search_in_saleview.frame = CGRectMake(150,8, 15, 15);
        btn_on_search_bar_in_saleview.frame = CGRectMake(150,0, 30, 30);
        
        white_bg2_in_saleview.frame = CGRectMake(10,CGRectGetMaxY(white_bg.frame)+5,WIDTH-20,50);
        img_calender_in_saleview.frame = CGRectMake(20,10,30,30);
        text_date_in_saleview .frame = CGRectMake(CGRectGetMidX(img_calender.frame)+18,5,100,45);
        icon_line2_in_saleview.frame = CGRectMake(CGRectGetMidX(text_date.frame)+50,10,1, 30);
        img_calender2_in_saleview.frame = CGRectMake(CGRectGetMidX(icon_line2.frame)+20,10,30,30);
        text_date2_in_saleview .frame = CGRectMake(CGRectGetMidX(img_calender2.frame)+18,5,100,45);
        
        lbl_doller_val_in_saleview.frame = CGRectMake(WIDTH/2-50,CGRectGetMaxY(white_bg2.frame),100, 45);
        img_whit_bg.frame = CGRectMake(5,CGRectGetMaxY(lbl_doller_val_in_saleview.frame)+10,WIDTH-10,360);
        lbl_order_id.frame = CGRectMake(15,5,100, 45);
        img_drop_down.frame = CGRectMake(CGRectGetMaxX(lbl_order_id.frame)-8,25,15,10);
        img_line.frame = CGRectMake(CGRectGetMaxX(img_drop_down.frame)+10,7,1,50);
        lbl_total_sal.frame = CGRectMake(CGRectGetMaxX(img_line.frame)+5,5,100, 45);
        img_drop_down2.frame = CGRectMake(CGRectGetMidX(lbl_total_sal.frame)+5,25,15,10);
        img_line2.frame = CGRectMake(CGRectGetMaxX(img_drop_down2.frame)+10,7,1,50);
        lbl_net_sal.frame = CGRectMake(CGRectGetMaxX(img_line2.frame)+2,5,100, 45);
        img_drop_down3.frame = CGRectMake(CGRectGetMidX(lbl_net_sal.frame)+5,25,15,10);
        img_line3.frame = CGRectMake(CGRectGetMaxX(img_drop_down3.frame)+10,7,1,50);
        lbl_pdf.frame = CGRectMake(CGRectGetMaxX(img_line3.frame)+8,5,100, 45);
        img_line4.frame = CGRectMake(5,CGRectGetMaxY(lbl_pdf.frame)+7,WIDTH-20,1);
        table_in_sales_view .frame  = CGRectMake(8,CGRectGetMaxY(img_line4.frame),WIDTH-29,280);
        
        view_for_sales_tracker.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+380);
        white_bg_for_trackerview.frame = CGRectMake(10,CGRectGetMinY(img_header.frame)+5,WIDTH-20,50);
        icon_up_and_down_arrow_in_trackerview.frame = CGRectMake(5,10, 30, 30);
        lbl_time_in_trackerview.frame = CGRectMake(CGRectGetMidX(icon_up_and_down_arrow.frame)+30,0,100, 45);
        btn_sortacontsalessu.frame = CGRectMake(5,0, 150, 50);
        
        icon_line_in_trackerview.frame = CGRectMake(CGRectGetMidX(lbl_time.frame)+10,10,1, 30);
        img_serch_bar_in_trackerview.frame = CGRectMake(CGRectGetMidX(icon_line.frame)+10,10,170, 30);
        txt_search_in_trackerview.frame = CGRectMake(0,0, 150, 30);
        icon_search_in_trackerview.frame = CGRectMake(150,8, 15, 15);
        btn_on_search_bar_in_trackerview.frame = CGRectMake(150,0, 30, 30);
        
        white_bg2_in_trackerview.frame = CGRectMake(10,CGRectGetMaxY(white_bg.frame)+5,WIDTH-20,50);
        img_calender_in_trackerview.frame = CGRectMake(20,10,30,30);
        text_date_in_trackerview .frame = CGRectMake(CGRectGetMidX(img_calender.frame)+18,5,100,45);
        icon_line2_in_trackerview.frame = CGRectMake(CGRectGetMidX(text_date.frame)+50,10,1, 30);
        img_calender2_in_trackerview.frame = CGRectMake(CGRectGetMidX(icon_line2.frame)+20,10,30,30);
        text_date2_in_trackerview .frame = CGRectMake(CGRectGetMidX(img_calender2.frame)+18,5,100,45);
        
        lbl_sold_val.frame = CGRectMake(70,CGRectGetMaxY(white_bg2.frame),150, 45);
        lbl_doller_val_in_trackerview.frame = CGRectMake(CGRectGetMaxX(lbl_sold_val.frame)-10,CGRectGetMaxY(white_bg2.frame),100, 45);
        table_in_sales_tracker .frame  = CGRectMake(8,CGRectGetMaxY(lbl_sold_val.frame),WIDTH-20,455);
        table_on_drop_down .frame  = CGRectMake(60,45,150,100);
    }
    else
    {
        view_for_account.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+380);
        white_bg.frame = CGRectMake(10,CGRectGetMinY(img_header.frame)+5,WIDTH-20,50);
        icon_up_and_down_arrow.frame = CGRectMake(5,10, 30, 30);
        lbl_time.frame = CGRectMake(CGRectGetMidX(icon_up_and_down_arrow.frame)+30,0,100, 45);
        
        icon_line.frame = CGRectMake(CGRectGetMidX(lbl_time.frame)+10,10,1, 30);
        img_serch_bar.frame = CGRectMake(CGRectGetMidX(icon_line.frame)+10,10,170, 30);
        txt_search.frame = CGRectMake(0,0, 150, 30);
        icon_search.frame = CGRectMake(150,8, 15, 15);
        btn_on_search_bar.frame = CGRectMake(150,0, 30, 30);
        white_bg2.frame = CGRectMake(10,CGRectGetMaxY(white_bg.frame)+5,WIDTH-20,50);
        img_calender.frame = CGRectMake(20,10,30,30);
        text_date .frame = CGRectMake(CGRectGetMidX(img_calender.frame)+18,5,100,45);
        icon_line2.frame = CGRectMake(CGRectGetMidX(text_date.frame)+50,10,1, 30);
        img_calender2.frame = CGRectMake(CGRectGetMidX(icon_line2.frame)+20,10,30,30);
        text_date2 .frame = CGRectMake(CGRectGetMidX(img_calender2.frame)+18,5,100,45);
        lbl_doller_val.frame = CGRectMake(WIDTH/2-50,CGRectGetMaxY(white_bg2.frame),100, 45);
        table_for_ordrelist.frame  = CGRectMake(10,CGRectGetMaxY(lbl_doller_val.frame),WIDTH-20,350);
        
        view_for_sales_summery.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+380);
        white_bg_for_saleview.frame = CGRectMake(10,CGRectGetMinY(img_header.frame)+5,WIDTH-20,50);
        icon_up_and_down_arrow_in_salesview.frame = CGRectMake(5,10, 30, 30);
        lbl_time_in_saleview.frame = CGRectMake(CGRectGetMidX(icon_up_and_down_arrow.frame)+30,0,100, 45);
        icon_line_in_saleview.frame = CGRectMake(CGRectGetMidX(lbl_time.frame)+10,10,1, 30);
        img_serch_bar_in_saleview.frame = CGRectMake(CGRectGetMidX(icon_line.frame)+10,10,170, 30);
        txt_search_in_saleview.frame = CGRectMake(0,0, 150, 30);
        icon_search_in_saleview.frame = CGRectMake(150,8, 15, 15);
        btn_on_search_bar_in_saleview.frame = CGRectMake(150,0, 30, 30);
        
        white_bg2_in_saleview.frame = CGRectMake(10,CGRectGetMaxY(white_bg.frame)+5,WIDTH-20,50);
        img_calender_in_saleview.frame = CGRectMake(20,10,30,30);
        text_date_in_saleview .frame = CGRectMake(CGRectGetMidX(img_calender.frame)+18,5,100,45);
        icon_line2_in_saleview.frame = CGRectMake(CGRectGetMidX(text_date.frame)+50,10,1, 30);
        img_calender2_in_saleview.frame = CGRectMake(CGRectGetMidX(icon_line2.frame)+20,10,30,30);
        text_date2_in_saleview .frame = CGRectMake(CGRectGetMidX(img_calender2.frame)+18,5,100,45);
        
        lbl_doller_val_in_saleview.frame = CGRectMake(WIDTH/2-50,CGRectGetMaxY(white_bg2.frame),100, 45);
        img_whit_bg.frame = CGRectMake(5,CGRectGetMaxY(lbl_doller_val_in_saleview.frame)+10,WIDTH-10,360);
        lbl_order_id.frame = CGRectMake(15,5,100, 45);
        btn_sortacontsales.frame = CGRectMake(5,0, 150, 50);
        
        img_drop_down.frame = CGRectMake(CGRectGetMaxX(lbl_order_id.frame)-8,25,15,10);
        img_line.frame = CGRectMake(CGRectGetMaxX(img_drop_down.frame)+10,7,1,50);
        lbl_total_sal.frame = CGRectMake(CGRectGetMaxX(img_line.frame)+5,5,100, 45);
        img_drop_down2.frame = CGRectMake(CGRectGetMidX(lbl_total_sal.frame)+5,25,15,10);
        img_line2.frame = CGRectMake(CGRectGetMaxX(img_drop_down2.frame)+10,7,1,50);
        lbl_net_sal.frame = CGRectMake(CGRectGetMaxX(img_line2.frame)+2,5,100, 45);
        img_drop_down3.frame = CGRectMake(CGRectGetMidX(lbl_net_sal.frame)+5,25,15,10);
        img_line3.frame = CGRectMake(CGRectGetMaxX(img_drop_down3.frame)+10,7,1,50);
        lbl_pdf.frame = CGRectMake(CGRectGetMaxX(img_line3.frame)+8,5,100, 45);
        img_line4.frame = CGRectMake(5,CGRectGetMaxY(lbl_pdf.frame)+7,WIDTH-20,1);
        table_in_sales_view .frame  = CGRectMake(8,CGRectGetMaxY(img_line4.frame),WIDTH-29,280);
        
        view_for_sales_tracker.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+380);
        white_bg_for_trackerview.frame = CGRectMake(10,CGRectGetMinY(img_header.frame)+5,WIDTH-20,50);
        icon_up_and_down_arrow_in_trackerview.frame = CGRectMake(5,10, 30, 30);
        lbl_time_in_trackerview.frame = CGRectMake(CGRectGetMidX(icon_up_and_down_arrow.frame)+30,0,100, 45);
        btn_sortacontsalessu.frame = CGRectMake(5,0, 150, 50);
        
        icon_line_in_trackerview.frame = CGRectMake(CGRectGetMidX(lbl_time.frame)+10,10,1, 30);
        img_serch_bar_in_trackerview.frame = CGRectMake(CGRectGetMidX(icon_line.frame)+10,10,170, 30);
        txt_search_in_trackerview.frame = CGRectMake(0,0, 150, 30);
        icon_search_in_trackerview.frame = CGRectMake(150,8, 15, 15);
        btn_on_search_bar_in_trackerview.frame = CGRectMake(150,0, 30, 30);
        
        white_bg2_in_trackerview.frame = CGRectMake(10,CGRectGetMaxY(white_bg.frame)+5,WIDTH-20,50);
        img_calender_in_trackerview.frame = CGRectMake(20,10,30,30);
        text_date_in_trackerview .frame = CGRectMake(CGRectGetMidX(img_calender.frame)+18,5,100,45);
        icon_line2_in_trackerview.frame = CGRectMake(CGRectGetMidX(text_date.frame)+50,10,1, 30);
        img_calender2_in_trackerview.frame = CGRectMake(CGRectGetMidX(icon_line2.frame)+20,10,30,30);
        text_date2_in_trackerview .frame = CGRectMake(CGRectGetMidX(img_calender2.frame)+18,5,100,45);
        
        lbl_sold_val.frame = CGRectMake(70,CGRectGetMaxY(white_bg2.frame),150, 45);
        lbl_doller_val_in_trackerview.frame = CGRectMake(CGRectGetMaxX(lbl_sold_val.frame)-10,CGRectGetMaxY(white_bg2.frame),100, 45);
        table_in_sales_tracker .frame  = CGRectMake(8,CGRectGetMaxY(lbl_sold_val.frame),WIDTH-20,455);
        table_on_drop_down .frame  = CGRectMake(60,45,150,100);
    }
    
}
#pragma tableview_deligates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == table_on_drop_down)
    {
        return [array_head_names count];
    }
    else if (tableView == table_for_ordrelist)
    {
        return [ary_accountsfilter count];
    }
    else if (tableView ==  table_in_sales_view)
    {
        return [array_Sale_Summaryfilter count];
    }
    else if (tableView == table_in_sales_tracker)
    {
        return [array_Sale_Trackerfilters count];
        //        return  2;
    }
    return 1;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table_on_drop_down)
    {
        return 30;
    }
    else if (tableView == table_for_ordrelist)
    {
        return 410;
    }
    else if (tableView == table_in_sales_view)
    {
        return 71;
    }
    else if (tableView == table_in_sales_tracker)
    {
        return 300;
    }
    return 1;
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    for (UIView *view in cell.contentView.subviews)
    {
        [view removeFromSuperview];
    }
    cell.backgroundColor = [UIColor clearColor];
    
    
    //    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    //    if (IS_IPHONE_6Plus)
    //    {
    //        img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-15,150);
    //
    //    }
    //    else if (IS_IPHONE_6)
    //    {
    //        img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-15,150);
    //
    //    }
    //    else
    //    {    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-15,150);
    //
    //    }
    //    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    //    img_cellBackGnd.backgroundColor = [UIColor clearColor];
    //    [img_cellBackGnd setUserInteractionEnabled:YES];
    //    [cell.contentView addSubview:img_cellBackGnd];
    
    if (tableView == table_on_drop_down)
    {
        UIImageView * img_bg_for_first_tbl = [[UIImageView alloc]init];
        img_bg_for_first_tbl.frame =  CGRectMake(0,5, 150, 40);
        img_bg_for_first_tbl.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_bg_for_first_tbl setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_first_tbl];
        
        
        UILabel * lbl_food_now_r_later = [[UILabel alloc]init];
        lbl_food_now_r_later .frame = CGRectMake(5,10,200, 15);
        lbl_food_now_r_later .text = [NSString stringWithFormat:@"%@",[array_head_names objectAtIndex:indexPath.row]];
        lbl_food_now_r_later .font = [UIFont fontWithName:kFontBold size:15];
        lbl_food_now_r_later .textColor = [UIColor blackColor];
        lbl_food_now_r_later .backgroundColor = [UIColor clearColor];
        [img_bg_for_first_tbl addSubview: lbl_food_now_r_later ];
        
    }
    else if (tableView == table_for_ordrelist)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-15,400);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-15,400);
            
        }
        else
        {    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-15,400);
            
        }
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [img_cellBackGnd setContentMode:UIViewContentModeScaleAspectFill];
        img_cellBackGnd.clipsToBounds = YES;
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        UILabel * lbl_kitchen_name = [[UILabel alloc]init];
        lbl_kitchen_name .frame = CGRectMake(20,20,200, 15);
        lbl_kitchen_name .text = [NSString stringWithFormat:@"Kitchen Name:"];
        lbl_kitchen_name .font = [UIFont fontWithName:kFontBold size:18];
        lbl_kitchen_name .textColor = [UIColor blackColor];
        lbl_kitchen_name .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_kitchen_name ];
        
        
        UILabel * lbl_array_kitchen_name = [[UILabel alloc]init];
        lbl_array_kitchen_name .frame = CGRectMake(CGRectGetMidX(lbl_kitchen_name.frame)+35,21,200, 15);
        lbl_array_kitchen_name .text = [NSString stringWithFormat:@"%@",[[ary_accountsfilter objectAtIndex:indexPath.row]valueForKey:@"Kitchen_Name"] ];
        lbl_array_kitchen_name .font = [UIFont fontWithName:kFontBold size:17];
        lbl_array_kitchen_name .textColor = [UIColor blackColor];
        lbl_array_kitchen_name .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_array_kitchen_name ];
        
        
        
        UILabel * lbl_invoice_no = [[UILabel alloc]init];
        lbl_invoice_no .frame = CGRectMake(20,CGRectGetMaxY(lbl_kitchen_name.frame)+5,200, 15);
        lbl_invoice_no .text = [NSString stringWithFormat:@"Invoice no.:"];
        lbl_invoice_no .font = [UIFont fontWithName:kFontBold size:18];
        lbl_invoice_no .textColor = [UIColor blackColor];
        lbl_invoice_no .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_invoice_no ];
        
        UILabel * lbl_array_invoice_val = [[UILabel alloc]init];
        lbl_array_invoice_val .frame = CGRectMake(CGRectGetMidX(lbl_invoice_no.frame)+10,CGRectGetMaxY(lbl_kitchen_name.frame)+6,200, 15);
        lbl_array_invoice_val .text = [NSString stringWithFormat:@"%@",[[ary_accountsfilter objectAtIndex:indexPath.row]valueForKey:@"InvoiceNumber"]];
        lbl_array_invoice_val .font = [UIFont fontWithName:kFontBold size:17];
        lbl_array_invoice_val .textColor = [UIColor blackColor];
        lbl_array_invoice_val .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_array_invoice_val ];
        
        UILabel * lbl_order_no = [[UILabel alloc]init];
        lbl_order_no .frame = CGRectMake(20,CGRectGetMaxY(lbl_invoice_no.frame)+5,200, 15);
        lbl_order_no .text = [NSString stringWithFormat:@"Order no.:"];
        lbl_order_no .font = [UIFont fontWithName:kFontBold size:18];
        lbl_order_no .textColor = [UIColor blackColor];
        lbl_order_no .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_order_no ];
        
        UILabel * lbl_array_order_val = [[UILabel alloc]init];
        lbl_array_order_val .frame = CGRectMake(CGRectGetMidX(lbl_order_no.frame)-5,CGRectGetMaxY(lbl_array_invoice_val.frame)+6,200, 15);
        lbl_array_order_val .text = [NSString stringWithFormat:@"%@",[[array_for_accounts objectAtIndex:indexPath.row]valueForKey:@"OrderID"]];
        lbl_array_order_val .font = [UIFont fontWithName:kFontBold size:17];
        lbl_array_order_val .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        lbl_array_order_val .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_array_order_val ];
        
        UILabel * lbl_invoice_date = [[UILabel alloc]init];
        lbl_invoice_date .frame = CGRectMake(20,CGRectGetMaxY(lbl_order_no.frame)+5,200, 20);
        lbl_invoice_date .text = [NSString stringWithFormat:@"Invoice Date:"];
        lbl_invoice_date .font = [UIFont fontWithName:kFont size:14];
        lbl_invoice_date .textColor = [UIColor blackColor];
        lbl_invoice_date .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_invoice_date];
        
        
        UILabel * lbl_array_invoice_date_val = [[UILabel alloc]init];
        lbl_array_invoice_date_val .frame = CGRectMake(CGRectGetMidX(lbl_invoice_date.frame)-5,CGRectGetMaxY(lbl_order_no.frame)+6,200, 20);
        lbl_array_invoice_date_val .text = [NSString stringWithFormat:@"%@",[[ary_accountsfilter objectAtIndex:indexPath.row]valueForKey:@"InvoiceDate"]];
        lbl_array_invoice_date_val .font = [UIFont fontWithName:kFont size:14];
        lbl_array_invoice_date_val .textColor = [UIColor blackColor];
        lbl_array_invoice_date_val .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_array_invoice_date_val ];
        
        
        UILabel * lbl_order_date_time = [[UILabel alloc]init];
        lbl_order_date_time .frame = CGRectMake(20,CGRectGetMaxY(lbl_invoice_date.frame)+5,200, 20);
        lbl_order_date_time .text = [NSString stringWithFormat:@"Order Date/Time:"];
        lbl_order_date_time .font = [UIFont fontWithName:kFont size:14];
        lbl_order_date_time .textColor = [UIColor blackColor];
        lbl_order_date_time .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_order_date_time];
        
        
        UILabel * lbl_array_order_date_val = [[UILabel alloc]init];
        lbl_array_order_date_val .frame = CGRectMake(CGRectGetMidX(lbl_order_date_time.frame)+20,CGRectGetMaxY(lbl_array_invoice_date_val.frame)+5,200, 20);
        lbl_array_order_date_val .text = [NSString stringWithFormat:@"%@",[[ary_accountsfilter objectAtIndex:indexPath.row]valueForKey:@"OrderDate"]];
        lbl_array_order_date_val .font = [UIFont fontWithName:kFont size:14];
        lbl_array_order_date_val .textColor = [UIColor blackColor];
        lbl_array_order_date_val .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_array_order_date_val ];
        
        
        UILabel * lbl_serving_date_time = [[UILabel alloc]init];
        lbl_serving_date_time .frame = CGRectMake(20,CGRectGetMaxY(lbl_order_date_time.frame)+5,200, 20);
        lbl_serving_date_time .text = [NSString stringWithFormat:@"Serving Date/Time:"];
        lbl_serving_date_time .font = [UIFont fontWithName:kFont size:14];
        lbl_serving_date_time .textColor = [UIColor blackColor];
        lbl_serving_date_time .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_serving_date_time];
        
        
        UILabel * lbl_array_serving_date_val = [[UILabel alloc]init];
        lbl_array_serving_date_val .frame = CGRectMake(CGRectGetMidX(lbl_serving_date_time.frame)+29,CGRectGetMaxY(lbl_order_date_time.frame)+5,200,20);
        lbl_array_serving_date_val .text = [NSString stringWithFormat:@"%@",[[ary_accountsfilter objectAtIndex:indexPath.row]valueForKey:@"Servdatetime"]];
        lbl_array_serving_date_val .font = [UIFont fontWithName:kFont size:14];
        lbl_array_serving_date_val .textColor = [UIColor blackColor];
        lbl_array_serving_date_val .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_array_serving_date_val ];
        
        
        UIImageView *img_chef = [[UIImageView alloc] init];
        img_chef.frame = CGRectMake(CGRectGetMaxX(lbl_invoice_date.frame)+70,10,50,50 );
        NSString *ImagePath1 =[[NSString stringWithFormat:@"%@",[[ary_accountsfilter objectAtIndex:indexPath.row]valueForKey:@"UserImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [img_chef setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@"img-user@2x"]];
        //        [[array_for_accounts objectAtIndex:indexPath.row]valueForKey:@"Kitchen_Name"]
        //        [img_chef setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_img objectAtIndex:indexPath.row]]]];
        img_chef.layer.cornerRadius = img_chef.frame.size.width/2.0;
        img_chef.clipsToBounds = YES;
        [img_cellBackGnd addSubview:img_chef];
        
        
        UIImageView * img_pdf = [[UIImageView alloc]init];
        img_pdf.frame = CGRectMake(CGRectGetMaxX(lbl_invoice_date.frame)+80,CGRectGetMaxY(img_chef.frame)+20,25,25);
        [img_pdf setUserInteractionEnabled:YES];
        img_pdf.image=[UIImage imageNamed:@"icon-pdf@2x.png"];
        [img_cellBackGnd addSubview:img_pdf];
        
        
        
        
        UIImageView * img_line = [[UIImageView alloc]init];
        img_line.frame =  CGRectMake(20,CGRectGetMaxY(lbl_serving_date_time.frame)+10,315,1);
        img_line.image=[UIImage imageNamed:@"img-line@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_line setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_line];
        
        
        UILabel * lbl_dish_meal = [[UILabel alloc]init];
        lbl_dish_meal .frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)+10,100, 20);
        lbl_dish_meal .text = [NSString stringWithFormat:@"Dishe/Meals"];
        lbl_dish_meal .font = [UIFont fontWithName:kFont size:13];
        lbl_dish_meal .textColor = [UIColor blackColor];
        lbl_dish_meal .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_dish_meal];
        
        
        UIImageView * img_line2 = [[UIImageView alloc]init];
        img_line2.frame =  CGRectMake(20,CGRectGetMaxY(lbl_dish_meal.frame),75,1);
        img_line2.image=[UIImage imageNamed:@"img-line@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_line2 setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_line2];
        
        
        UILabel * lbl_price_per_serving = [[UILabel alloc]init];
        lbl_price_per_serving .frame = CGRectMake(CGRectGetMaxX(lbl_dish_meal.frame)+10,CGRectGetMaxY(img_line.frame)+10,150, 20);
        lbl_price_per_serving .text = [NSString stringWithFormat:@"Price per Serving"];
        lbl_price_per_serving .font = [UIFont fontWithName:kFont size:13];
        lbl_price_per_serving .textColor = [UIColor blackColor];
        lbl_price_per_serving .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_price_per_serving];
        
        
        UIImageView * img_line3 = [[UIImageView alloc]init];
        img_line3.frame =  CGRectMake(CGRectGetMaxX(img_line2.frame)+35,CGRectGetMaxY(lbl_dish_meal.frame),105,1);
        img_line3.image=[UIImage imageNamed:@"img-line@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_line3 setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_line3];
        
        
        UILabel * lbl_qty = [[UILabel alloc]init];
        lbl_qty .frame = CGRectMake(CGRectGetMaxX(lbl_price_per_serving.frame)+10,CGRectGetMaxY(img_line.frame)+10,50, 20);
        lbl_qty .text = [NSString stringWithFormat:@"Qty"];
        lbl_qty .font = [UIFont fontWithName:kFont size:13];
        lbl_qty .textColor = [UIColor blackColor];
        lbl_qty .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_qty];
        
        
        
        UIImageView * img_line4 = [[UIImageView alloc]init];
        img_line4.frame =  CGRectMake(CGRectGetMaxX(img_line3.frame)+55,CGRectGetMaxY(lbl_dish_meal.frame),30,1);
        img_line4.image=[UIImage imageNamed:@"img-line@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_line4 setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_line4];
        
        
        UIScrollView *  scroll = [[UIScrollView alloc]init];
        [scroll setShowsVerticalScrollIndicator:NO];
        [scroll setUserInteractionEnabled:YES];
        scroll.delegate = self;
        scroll.backgroundColor = [UIColor clearColor];
        scroll.frame = CGRectMake(03, CGRectGetMaxY(lbl_dish_meal.frame)+3, WIDTH-16, 100);
        scroll.scrollEnabled = YES;
        scroll.pagingEnabled = YES;
        //scroll.layer.borderWidth = 1.0;
        scroll.layer.borderColor = [UIColor clearColor].CGColor;
        [img_cellBackGnd addSubview:scroll];
        
        for (int i=0; i<[array_total_Itemsfilter count]; i++)
        {
            UILabel * lbl_soup = [[UILabel alloc]init];
            lbl_soup .frame = CGRectMake(5,20*i,155, 20);
            lbl_soup .text = [NSString stringWithFormat:@"%@",[[array_total_Itemsfilter objectAtIndex:i] valueForKey:@"Item_Name"]];
            lbl_soup .font = [UIFont fontWithName:kFont size:14];
            lbl_soup .textColor = [UIColor blackColor];
            lbl_soup .backgroundColor = [UIColor clearColor];
            [scroll addSubview: lbl_soup];
            
            
            UILabel * lbl_array_price_for_serving = [[UILabel alloc]init];
            lbl_array_price_for_serving .frame = CGRectMake(CGRectGetMidX(lbl_soup.frame)+75,20*i,200, 20);
            lbl_array_price_for_serving .text = [NSString stringWithFormat:@"%@",[[array_total_Itemsfilter objectAtIndex:i] valueForKey:@"Price"]];
            lbl_array_price_for_serving .font = [UIFont fontWithName:kFont size:14];
            lbl_array_price_for_serving .textColor = [UIColor blackColor];
            lbl_array_price_for_serving.textAlignment = NSTextAlignmentCenter;
            lbl_array_price_for_serving .backgroundColor = [UIColor clearColor];
            [scroll addSubview: lbl_array_price_for_serving ];
            
            
            UILabel * lbl_array_qty = [[UILabel alloc]init];
            lbl_array_qty .frame = CGRectMake(CGRectGetMidX(lbl_array_price_for_serving.frame)+50,20*i,200, 20);
            lbl_array_qty .text = [NSString stringWithFormat:@"%@",[[array_total_Itemsfilter objectAtIndex:i] valueForKey:@"Qty"]];
            lbl_array_qty .font = [UIFont fontWithName:kFont size:14];
            lbl_array_qty .textColor = [UIColor blackColor];
            lbl_array_qty .backgroundColor = [UIColor clearColor];
            lbl_array_qty.textAlignment = NSTextAlignmentCenter;
            [scroll addSubview: lbl_array_qty ];
            
            if (IS_IPHONE_6Plus)
            {
                lbl_soup .frame = CGRectMake(0,20*i,120, 20);
                lbl_array_price_for_serving .frame = CGRectMake(CGRectGetMaxX(lbl_soup.frame)+7,20*i,190, 20);
                lbl_array_qty .frame = CGRectMake(CGRectGetMidX(lbl_array_price_for_serving.frame)+27,20*i,100, 20);
                
            }
            else if (IS_IPHONE_6)
            {
                lbl_soup .frame = CGRectMake(0,20*i,120, 20);
                lbl_array_price_for_serving .frame = CGRectMake(CGRectGetMaxX(lbl_soup.frame)+7,20*i,120, 20);
                lbl_array_qty .frame = CGRectMake(CGRectGetMaxX(lbl_array_price_for_serving.frame)+7,20*i,100, 20);
                
            }
            else
            {
                lbl_soup .frame = CGRectMake(0,20*i,150, 20);
                lbl_array_price_for_serving .frame = CGRectMake(CGRectGetMidX(lbl_soup.frame)+75,20*i,200, 20);
                lbl_array_qty .frame = CGRectMake(CGRectGetMidX(lbl_array_price_for_serving.frame)+20,20*i,200, 20);
                
            }
            
            
            
        }
        
        
        UILabel * lbl_sub_total = [[UILabel alloc]init];
        lbl_sub_total .frame = CGRectMake(190,CGRectGetMaxY(scroll.frame)+5,100, 20);
        lbl_sub_total .text = [NSString stringWithFormat:@"Subtotal:"];
        lbl_sub_total .font = [UIFont fontWithName:kFont size:16];
        lbl_sub_total .textColor = [UIColor blackColor];
        lbl_sub_total .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_sub_total];
        
        
        
        UILabel * lbl_subtotal_val = [[UILabel alloc]init];
        lbl_subtotal_val .frame = CGRectMake(CGRectGetMidX(lbl_sub_total.frame)+25,CGRectGetMaxY(scroll.frame)+5,200, 20);
        lbl_subtotal_val .text = [NSString stringWithFormat:@"%@",[[ary_accountsfilter objectAtIndex:indexPath.row] valueForKey:@"SubTotal"]];
        lbl_subtotal_val .font = [UIFont fontWithName:kFontBold size:14];
        lbl_subtotal_val .textColor = [UIColor blackColor];
        lbl_subtotal_val .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_subtotal_val ];
        
        
        
        UILabel * lbl_delivery_fee = [[UILabel alloc]init];
        lbl_delivery_fee .frame = CGRectMake(163,CGRectGetMaxY(lbl_sub_total.frame),100, 20);
        lbl_delivery_fee .text = [NSString stringWithFormat:@"Delivery fee:"];
        lbl_delivery_fee .font = [UIFont fontWithName:kFont size:16];
        lbl_delivery_fee .textColor = [UIColor blackColor];
        lbl_delivery_fee .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_delivery_fee];
        
        UILabel * lbl_delivery_fee_val = [[UILabel alloc]init];
        lbl_delivery_fee_val .frame = CGRectMake(CGRectGetMidX(lbl_delivery_fee.frame)+50,CGRectGetMaxY(lbl_sub_total.frame),200, 20);
        lbl_delivery_fee_val .text = [NSString stringWithFormat:@"%@",[[ary_accountsfilter objectAtIndex:indexPath.row] valueForKey:@"DeliveryFee"]];
        lbl_delivery_fee_val .font = [UIFont fontWithName:kFontBold size:14];
        lbl_delivery_fee_val .textColor = [UIColor blackColor];
        lbl_delivery_fee_val .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_delivery_fee_val];
        
        
        
        UILabel * lbl_service_fee = [[UILabel alloc]init];
        lbl_service_fee .frame = CGRectMake(84,CGRectGetMaxY(lbl_delivery_fee.frame),180, 20);
        lbl_service_fee .text = [NSString stringWithFormat:@"%@",@"not86 service fee (7.5%):"];
        lbl_service_fee .font = [UIFont fontWithName:kFont size:16];
        lbl_service_fee .textColor = [UIColor blackColor];
        lbl_service_fee .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_service_fee];
        
        
        UILabel * lbl_service_fee_val = [[UILabel alloc]init];
        lbl_service_fee_val .frame = CGRectMake(CGRectGetMidX(lbl_service_fee.frame)+90,CGRectGetMaxY(lbl_delivery_fee.frame),200, 20);
        lbl_service_fee_val .text = [NSString stringWithFormat:@"%@",[[ary_accountsfilter objectAtIndex:indexPath.row] valueForKey:@"Not86ServiceFee"]];
        lbl_service_fee_val .font = [UIFont fontWithName:kFontBold size:14];
        lbl_service_fee_val .textColor = [UIColor blackColor];
        lbl_service_fee_val .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_service_fee_val];
        
        if (IS_IPHONE_6Plus)
        {
            
            lbl_kitchen_name .frame = CGRectMake(20,20,200, 15);
            lbl_array_kitchen_name .frame = CGRectMake(CGRectGetMidX(lbl_kitchen_name.frame)+35,21,200, 15);
            lbl_invoice_no .frame = CGRectMake(20,CGRectGetMaxY(lbl_kitchen_name.frame)+5,200, 15);
            lbl_array_invoice_val .frame = CGRectMake(CGRectGetMidX(lbl_invoice_no.frame)+10,CGRectGetMaxY(lbl_kitchen_name.frame)+6,200, 15);
            lbl_order_no .frame = CGRectMake(20,CGRectGetMaxY(lbl_invoice_no.frame)+5,200, 15);
            lbl_array_order_val .frame = CGRectMake(CGRectGetMidX(lbl_order_no.frame)-5,CGRectGetMaxY(lbl_array_invoice_val.frame)+6,200, 15);
            lbl_invoice_date .frame = CGRectMake(20,CGRectGetMaxY(lbl_order_no.frame)+5,200, 20);
            lbl_array_invoice_date_val .frame = CGRectMake(CGRectGetMidX(lbl_invoice_date.frame)-5,CGRectGetMaxY(lbl_order_no.frame)+6,200, 20);
            lbl_order_date_time .frame = CGRectMake(20,CGRectGetMaxY(lbl_invoice_date.frame)+5,200, 20);
            lbl_array_order_date_val .frame = CGRectMake(CGRectGetMidX(lbl_order_date_time.frame)+20,CGRectGetMaxY(lbl_array_invoice_date_val.frame)+5,200, 20);
            lbl_serving_date_time .frame = CGRectMake(20,CGRectGetMaxY(lbl_order_date_time.frame)+5,200, 20);
            lbl_array_serving_date_val .frame = CGRectMake(CGRectGetMidX(lbl_serving_date_time.frame)+29,CGRectGetMaxY(lbl_order_date_time.frame)+5,200,20);
            img_chef.frame = CGRectMake(CGRectGetMaxX(lbl_invoice_date.frame)+100,10,50,50 );
            img_pdf.frame = CGRectMake(CGRectGetMaxX(lbl_invoice_date.frame)+115,CGRectGetMaxY(img_chef.frame)+20,25,25);
            img_line.frame =  CGRectMake(20,CGRectGetMaxY(lbl_serving_date_time.frame)+10,360,1);
            lbl_dish_meal .frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)+10,100, 20);
            img_line2.frame =  CGRectMake(20,CGRectGetMaxY(lbl_dish_meal.frame),75,1);
            lbl_price_per_serving .frame = CGRectMake(CGRectGetMaxX(lbl_dish_meal.frame)+35,CGRectGetMaxY(img_line.frame)+10,150, 20);
            img_line3.frame =  CGRectMake(CGRectGetMaxX(img_line2.frame)+62,CGRectGetMaxY(lbl_dish_meal.frame),105,1);
            lbl_qty .frame = CGRectMake(CGRectGetMaxX(lbl_price_per_serving.frame)+10,CGRectGetMaxY(img_line.frame)+10,50, 20);
            img_line4.frame =  CGRectMake(CGRectGetMaxX(img_line3.frame)+55,CGRectGetMaxY(lbl_dish_meal.frame),30,1);
            scroll.frame = CGRectMake(03, CGRectGetMaxY(lbl_dish_meal.frame)+3, WIDTH-19, 100);
            //  lbl_soup .frame = CGRectMake(20,20*i,100, 20);
            //  lbl_array_price_for_serving .frame = CGRectMake(CGRectGetMidX(lbl_soup.frame)+75,20*i,200, 20);
            //  lbl_array_qty .frame = CGRectMake(CGRectGetMidX(lbl_array_price_for_serving.frame)+50,20*i,200, 20);
            lbl_sub_total .frame = CGRectMake(245,CGRectGetMaxY(scroll.frame)+5,100, 20);
            lbl_subtotal_val .frame = CGRectMake(CGRectGetMidX(lbl_sub_total.frame)+25,CGRectGetMaxY(scroll.frame)+5,200, 20);
            lbl_delivery_fee .frame = CGRectMake(218,CGRectGetMaxY(lbl_sub_total.frame),100, 20);
            lbl_delivery_fee_val .frame = CGRectMake(CGRectGetMidX(lbl_delivery_fee.frame)+50,CGRectGetMaxY(lbl_sub_total.frame),200, 20);
            lbl_service_fee .frame = CGRectMake(140,CGRectGetMaxY(lbl_delivery_fee.frame),180, 20);
            lbl_service_fee_val .frame = CGRectMake(CGRectGetMidX(lbl_service_fee.frame)+90,CGRectGetMaxY(lbl_delivery_fee.frame),200, 20);
            
            lbl_kitchen_name .font = [UIFont fontWithName:kFontBold size:18];
            lbl_array_kitchen_name .font = [UIFont fontWithName:kFontBold size:17];
            lbl_invoice_no .font = [UIFont fontWithName:kFontBold size:18];
            lbl_array_invoice_val .font = [UIFont fontWithName:kFontBold size:17];
            lbl_order_no .font = [UIFont fontWithName:kFontBold size:18];
            lbl_array_order_val .font = [UIFont fontWithName:kFontBold size:17];
            lbl_invoice_date .font = [UIFont fontWithName:kFont size:14];
            lbl_array_invoice_date_val .font = [UIFont fontWithName:kFont size:14];
            lbl_order_date_time .font = [UIFont fontWithName:kFont size:14];
            lbl_array_order_date_val .font = [UIFont fontWithName:kFont size:14];
            lbl_serving_date_time .font = [UIFont fontWithName:kFont size:14];
            lbl_array_serving_date_val .font = [UIFont fontWithName:kFont size:14];
            lbl_dish_meal .font = [UIFont fontWithName:kFont size:13];
            lbl_price_per_serving .font = [UIFont fontWithName:kFont size:13];
            lbl_qty .font = [UIFont fontWithName:kFont size:13];
            
            lbl_sub_total .font = [UIFont fontWithName:kFont size:16];
            lbl_subtotal_val .font = [UIFont fontWithName:kFontBold size:14];
            lbl_delivery_fee .font = [UIFont fontWithName:kFont size:16];
            lbl_delivery_fee_val .font = [UIFont fontWithName:kFontBold size:14];
            lbl_service_fee .font = [UIFont fontWithName:kFont size:16];
            lbl_service_fee_val .font = [UIFont fontWithName:kFontBold size:14];
            
            img_chef.layer.cornerRadius = img_chef.frame.size.width/2.0;
            
            
            
        }
        else if (IS_IPHONE_6)
        {
            lbl_kitchen_name .frame = CGRectMake(20,20,140, 20);
            lbl_array_kitchen_name .frame = CGRectMake(CGRectGetMaxX(lbl_kitchen_name.frame),21,140, 20);
            lbl_invoice_no .frame = CGRectMake(20,CGRectGetMaxY(lbl_kitchen_name.frame)+5,140, 15);
            lbl_array_invoice_val .frame = CGRectMake(CGRectGetMaxX(lbl_invoice_no.frame)+10,CGRectGetMaxY(lbl_kitchen_name.frame)+6,200, 15);
            lbl_order_no .frame = CGRectMake(20,CGRectGetMaxY(lbl_invoice_no.frame)+5,200, 15);
            lbl_array_order_val .frame = CGRectMake(CGRectGetMidX(lbl_order_no.frame)-5,CGRectGetMaxY(lbl_array_invoice_val.frame)+6,200, 15);
            lbl_invoice_date .frame = CGRectMake(20,CGRectGetMaxY(lbl_order_no.frame)+5,200, 20);
            lbl_array_invoice_date_val .frame = CGRectMake(CGRectGetMidX(lbl_invoice_date.frame)-5,CGRectGetMaxY(lbl_order_no.frame)+6,200, 20);
            lbl_order_date_time .frame = CGRectMake(20,CGRectGetMaxY(lbl_invoice_date.frame)+5,200, 20);
            lbl_array_order_date_val .frame = CGRectMake(CGRectGetMidX(lbl_order_date_time.frame)+20,CGRectGetMaxY(lbl_array_invoice_date_val.frame)+5,200, 20);
            lbl_serving_date_time .frame = CGRectMake(20,CGRectGetMaxY(lbl_order_date_time.frame)+5,200, 20);
            lbl_array_serving_date_val .frame = CGRectMake(CGRectGetMidX(lbl_serving_date_time.frame)+29,CGRectGetMaxY(lbl_order_date_time.frame)+5,200,20);
            img_chef.frame = CGRectMake(CGRectGetMaxX(lbl_array_kitchen_name.frame),10,40,40 );
            img_pdf.frame = CGRectMake(CGRectGetMaxX(lbl_invoice_date.frame)+80,CGRectGetMaxY(img_chef.frame)+20,25,25);
            img_line.frame =  CGRectMake(20,CGRectGetMaxY(lbl_serving_date_time.frame)+10,315,1);
            lbl_dish_meal .frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)+10,100, 20);
            img_line2.frame =  CGRectMake(20,CGRectGetMaxY(lbl_dish_meal.frame),75,1);
            lbl_price_per_serving .frame = CGRectMake(CGRectGetMaxX(lbl_dish_meal.frame)+10,CGRectGetMaxY(img_line.frame)+10,150, 20);
            img_line3.frame =  CGRectMake(CGRectGetMaxX(img_line2.frame)+35,CGRectGetMaxY(lbl_dish_meal.frame),105,1);
            lbl_qty .frame = CGRectMake(CGRectGetMaxX(lbl_price_per_serving.frame)+10,CGRectGetMaxY(img_line.frame)+10,50, 20);
            img_line4.frame =  CGRectMake(CGRectGetMaxX(img_line3.frame)+55,CGRectGetMaxY(lbl_dish_meal.frame),30,1);
            scroll.frame = CGRectMake(03, CGRectGetMaxY(lbl_dish_meal.frame)+3, WIDTH-16, 100);
            //    lbl_soup .frame = CGRectMake(20,20*i,100, 20);
            //    lbl_array_price_for_serving .frame = CGRectMake(CGRectGetMidX(lbl_soup.frame)+75,20*i,200, 20);
            //   lbl_array_qty .frame = CGRectMake(CGRectGetMidX(lbl_array_price_for_serving.frame)+50,20*i,200, 20);
            lbl_sub_total .frame = CGRectMake(190,CGRectGetMaxY(scroll.frame)+5,100, 20);
            lbl_subtotal_val .frame = CGRectMake(CGRectGetMidX(lbl_sub_total.frame)+25,CGRectGetMaxY(scroll.frame)+5,200, 20);
            lbl_delivery_fee .frame = CGRectMake(163,CGRectGetMaxY(lbl_sub_total.frame),100, 20);
            lbl_delivery_fee_val .frame = CGRectMake(CGRectGetMidX(lbl_delivery_fee.frame)+50,CGRectGetMaxY(lbl_sub_total.frame),200, 20);
            lbl_service_fee .frame = CGRectMake(64,CGRectGetMaxY(lbl_delivery_fee.frame),200, 20);
            lbl_service_fee_val .frame = CGRectMake(CGRectGetMaxX(lbl_service_fee.frame)+9,CGRectGetMaxY(lbl_delivery_fee.frame),80, 20);
            
            lbl_kitchen_name .font = [UIFont fontWithName:kFontBold size:16];
            lbl_array_kitchen_name .font = [UIFont fontWithName:kFontBold size:15];
            lbl_invoice_no .font = [UIFont fontWithName:kFontBold size:16];
            lbl_array_invoice_val .font = [UIFont fontWithName:kFontBold size:15];
            lbl_order_no .font = [UIFont fontWithName:kFontBold size:16];
            lbl_array_order_val .font = [UIFont fontWithName:kFontBold size:15];
            lbl_invoice_date .font = [UIFont fontWithName:kFont size:14];
            lbl_array_invoice_date_val .font = [UIFont fontWithName:kFont size:14];
            lbl_order_date_time .font = [UIFont fontWithName:kFont size:14];
            lbl_array_order_date_val .font = [UIFont fontWithName:kFont size:14];
            lbl_serving_date_time .font = [UIFont fontWithName:kFont size:14];
            lbl_array_serving_date_val .font = [UIFont fontWithName:kFont size:14];
            lbl_dish_meal .font = [UIFont fontWithName:kFont size:13];
            lbl_price_per_serving .font = [UIFont fontWithName:kFont size:13];
            lbl_qty .font = [UIFont fontWithName:kFont size:13];
            
            lbl_sub_total .font = [UIFont fontWithName:kFont size:16];
            lbl_subtotal_val .font = [UIFont fontWithName:kFontBold size:14];
            lbl_delivery_fee .font = [UIFont fontWithName:kFont size:16];
            lbl_delivery_fee_val .font = [UIFont fontWithName:kFontBold size:14];
            lbl_service_fee .font = [UIFont fontWithName:kFont size:16];
            lbl_service_fee_val .font = [UIFont fontWithName:kFontBold size:14];
            img_chef.layer.cornerRadius = img_chef.frame.size.width/2.0;
            
            
        }
        else if (IS_IPHONE_5)
        {
            lbl_kitchen_name .frame = CGRectMake(20,20,140, 15);
            lbl_array_kitchen_name .frame = CGRectMake(CGRectGetMaxX(lbl_kitchen_name.frame),21,150, 15);
            lbl_invoice_no .frame = CGRectMake(20,CGRectGetMaxY(lbl_kitchen_name.frame)+5,200, 15);
            lbl_array_invoice_val .frame = CGRectMake(CGRectGetMidX(lbl_invoice_no.frame)-10,CGRectGetMaxY(lbl_kitchen_name.frame)+6,200, 15);
            lbl_order_no .frame = CGRectMake(20,CGRectGetMaxY(lbl_invoice_no.frame)+5,200, 15);
            lbl_array_order_val .frame = CGRectMake(CGRectGetMidX(lbl_order_no.frame)-19,CGRectGetMaxY(lbl_array_invoice_val.frame)+6,200, 15);
            lbl_invoice_date .frame = CGRectMake(20,CGRectGetMaxY(lbl_order_no.frame)+5,200, 20);
            lbl_array_invoice_date_val .frame = CGRectMake(CGRectGetMidX(lbl_invoice_date.frame)-20,CGRectGetMaxY(lbl_order_no.frame)+6,200, 20);
            lbl_order_date_time .frame = CGRectMake(20,CGRectGetMaxY(lbl_invoice_date.frame)+5,200, 20);
            lbl_array_order_date_val .frame = CGRectMake(CGRectGetMidX(lbl_order_date_time.frame),CGRectGetMaxY(lbl_array_invoice_date_val.frame)+5,200, 20);
            lbl_serving_date_time .frame = CGRectMake(20,CGRectGetMaxY(lbl_order_date_time.frame)+5,200, 20);
            lbl_array_serving_date_val .frame = CGRectMake(CGRectGetMidX(lbl_serving_date_time.frame),CGRectGetMaxY(lbl_order_date_time.frame)+5,200,20);
            img_chef.frame = CGRectMake(CGRectGetMaxX(lbl_invoice_date.frame)+10,10,50,50 );
            img_pdf.frame = CGRectMake(CGRectGetMaxX(lbl_invoice_date.frame)+25,CGRectGetMaxY(img_chef.frame)+20,25,25);
            img_line.frame =  CGRectMake(20,CGRectGetMaxY(lbl_serving_date_time.frame)+10,270,1);
            lbl_dish_meal .frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)+10,100, 20);
            img_line2.frame =  CGRectMake(20,CGRectGetMaxY(lbl_dish_meal.frame),55,1);
            lbl_price_per_serving .frame = CGRectMake(CGRectGetMaxX(lbl_dish_meal.frame)+10,CGRectGetMaxY(img_line.frame)+10,150, 20);
            img_line3.frame =  CGRectMake(CGRectGetMaxX(img_line2.frame)+55,CGRectGetMaxY(lbl_dish_meal.frame),77,1);
            lbl_qty .frame = CGRectMake(CGRectGetMaxX(lbl_price_per_serving.frame)-25,CGRectGetMaxY(img_line.frame)+10,50, 20);
            img_line4.frame =  CGRectMake(CGRectGetMaxX(img_line3.frame)+47,CGRectGetMaxY(lbl_dish_meal.frame),30,1);
            scroll.frame = CGRectMake(03, CGRectGetMaxY(lbl_dish_meal.frame)+3, WIDTH-30, 100);
            //   lbl_soup .frame = CGRectMake(20,20*i,100, 20);
            //    lbl_array_price_for_serving .frame = CGRectMake(CGRectGetMidX(lbl_soup.frame)+75,20*i,200, 20);
            //    lbl_array_qty .frame = CGRectMake(CGRectGetMidX(lbl_array_price_for_serving.frame)+50,20*i,200, 20);
            lbl_sub_total .frame = CGRectMake(190,CGRectGetMaxY(scroll.frame)+5,100, 20);
            lbl_subtotal_val .frame = CGRectMake(CGRectGetMidX(lbl_sub_total.frame)+10,CGRectGetMaxY(scroll.frame)+5,200, 20);
            lbl_delivery_fee .frame = CGRectMake(170,CGRectGetMaxY(lbl_sub_total.frame),100, 20);
            lbl_delivery_fee_val .frame = CGRectMake(CGRectGetMidX(lbl_delivery_fee.frame)+30,CGRectGetMaxY(lbl_sub_total.frame),200, 20);
            lbl_service_fee .frame = CGRectMake(115,CGRectGetMaxY(lbl_delivery_fee.frame),180, 20);
            lbl_service_fee_val .frame = CGRectMake(CGRectGetMidX(lbl_service_fee.frame)+50,CGRectGetMaxY(lbl_delivery_fee.frame),200, 20);
            
            lbl_kitchen_name .font = [UIFont fontWithName:kFontBold size:15];
            lbl_array_kitchen_name .font = [UIFont fontWithName:kFontBold size:14];
            lbl_invoice_no .font = [UIFont fontWithName:kFontBold size:15];
            lbl_array_invoice_val .font = [UIFont fontWithName:kFontBold size:14];
            lbl_order_no .font = [UIFont fontWithName:kFontBold size:15];
            lbl_array_order_val .font = [UIFont fontWithName:kFontBold size:14];
            lbl_invoice_date .font = [UIFont fontWithName:kFont size:11];
            lbl_array_invoice_date_val .font = [UIFont fontWithName:kFont size:11];
            lbl_order_date_time .font = [UIFont fontWithName:kFont size:11];
            lbl_array_order_date_val .font = [UIFont fontWithName:kFont size:11];
            lbl_serving_date_time .font = [UIFont fontWithName:kFont size:11];
            lbl_array_serving_date_val .font = [UIFont fontWithName:kFont size:11];
            lbl_dish_meal .font = [UIFont fontWithName:kFont size:10];
            lbl_price_per_serving .font = [UIFont fontWithName:kFont size:10];
            lbl_qty .font = [UIFont fontWithName:kFont size:10];
            
            lbl_sub_total .font = [UIFont fontWithName:kFont size:13];
            lbl_subtotal_val .font = [UIFont fontWithName:kFontBold size:11];
            lbl_delivery_fee .font = [UIFont fontWithName:kFont size:13];
            lbl_delivery_fee_val .font = [UIFont fontWithName:kFontBold size:11];
            lbl_service_fee .font = [UIFont fontWithName:kFont size:12];
            lbl_service_fee_val .font = [UIFont fontWithName:kFontBold size:11];
            
            
        }
        else
        {
            lbl_kitchen_name .frame = CGRectMake(20,20,200, 15);
            lbl_array_kitchen_name .frame = CGRectMake(CGRectGetMidX(lbl_kitchen_name.frame)+10,21,200, 15);
            lbl_invoice_no .frame = CGRectMake(20,CGRectGetMaxY(lbl_kitchen_name.frame)+5,200, 15);
            lbl_array_invoice_val .frame = CGRectMake(CGRectGetMidX(lbl_invoice_no.frame)-10,CGRectGetMaxY(lbl_kitchen_name.frame)+6,200, 15);
            lbl_order_no .frame = CGRectMake(20,CGRectGetMaxY(lbl_invoice_no.frame)+5,200, 15);
            lbl_array_order_val .frame = CGRectMake(CGRectGetMidX(lbl_order_no.frame)-19,CGRectGetMaxY(lbl_array_invoice_val.frame)+6,200, 15);
            lbl_invoice_date .frame = CGRectMake(20,CGRectGetMaxY(lbl_order_no.frame)+5,200, 20);
            lbl_array_invoice_date_val .frame = CGRectMake(CGRectGetMidX(lbl_invoice_date.frame)-20,CGRectGetMaxY(lbl_order_no.frame)+6,200, 20);
            lbl_order_date_time .frame = CGRectMake(20,CGRectGetMaxY(lbl_invoice_date.frame)+5,200, 20);
            lbl_array_order_date_val .frame = CGRectMake(CGRectGetMidX(lbl_order_date_time.frame),CGRectGetMaxY(lbl_array_invoice_date_val.frame)+5,200, 20);
            lbl_serving_date_time .frame = CGRectMake(20,CGRectGetMaxY(lbl_order_date_time.frame)+5,200, 20);
            lbl_array_serving_date_val .frame = CGRectMake(CGRectGetMidX(lbl_serving_date_time.frame),CGRectGetMaxY(lbl_order_date_time.frame)+5,200,20);
            img_chef.frame = CGRectMake(CGRectGetMaxX(lbl_invoice_date.frame)+10,10,50,50 );
            img_pdf.frame = CGRectMake(CGRectGetMaxX(lbl_invoice_date.frame)+25,CGRectGetMaxY(img_chef.frame)+20,25,25);
            img_line.frame =  CGRectMake(20,CGRectGetMaxY(lbl_serving_date_time.frame)+10,270,1);
            lbl_dish_meal .frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)+10,100, 20);
            img_line2.frame =  CGRectMake(20,CGRectGetMaxY(lbl_dish_meal.frame),55,1);
            lbl_price_per_serving .frame = CGRectMake(CGRectGetMaxX(lbl_dish_meal.frame)+10,CGRectGetMaxY(img_line.frame)+10,150, 20);
            img_line3.frame =  CGRectMake(CGRectGetMaxX(img_line2.frame)+55,CGRectGetMaxY(lbl_dish_meal.frame),77,1);
            lbl_qty .frame = CGRectMake(CGRectGetMaxX(lbl_price_per_serving.frame)-25,CGRectGetMaxY(img_line.frame)+10,50, 20);
            img_line4.frame =  CGRectMake(CGRectGetMaxX(img_line3.frame)+47,CGRectGetMaxY(lbl_dish_meal.frame),30,1);
            scroll.frame = CGRectMake(03, CGRectGetMaxY(lbl_dish_meal.frame)+3, WIDTH-30, 100);
            //   lbl_soup .frame = CGRectMake(20,20*i,100, 20);
            //    lbl_array_price_for_serving .frame = CGRectMake(CGRectGetMidX(lbl_soup.frame)+75,20*i,200, 20);
            //    lbl_array_qty .frame = CGRectMake(CGRectGetMidX(lbl_array_price_for_serving.frame)+50,20*i,200, 20);
            lbl_sub_total .frame = CGRectMake(190,CGRectGetMaxY(scroll.frame)+5,100, 20);
            lbl_subtotal_val .frame = CGRectMake(CGRectGetMidX(lbl_sub_total.frame)+10,CGRectGetMaxY(scroll.frame)+5,200, 20);
            lbl_delivery_fee .frame = CGRectMake(170,CGRectGetMaxY(lbl_sub_total.frame),100, 20);
            lbl_delivery_fee_val .frame = CGRectMake(CGRectGetMidX(lbl_delivery_fee.frame)+30,CGRectGetMaxY(lbl_sub_total.frame),200, 20);
            lbl_service_fee .frame = CGRectMake(115,CGRectGetMaxY(lbl_delivery_fee.frame),180, 20);
            lbl_service_fee_val .frame = CGRectMake(CGRectGetMidX(lbl_service_fee.frame)+50,CGRectGetMaxY(lbl_delivery_fee.frame),200, 20);
            
            lbl_kitchen_name .font = [UIFont fontWithName:kFontBold size:15];
            lbl_array_kitchen_name .font = [UIFont fontWithName:kFontBold size:14];
            lbl_invoice_no .font = [UIFont fontWithName:kFontBold size:15];
            lbl_array_invoice_val .font = [UIFont fontWithName:kFontBold size:14];
            lbl_order_no .font = [UIFont fontWithName:kFontBold size:15];
            lbl_array_order_val .font = [UIFont fontWithName:kFontBold size:14];
            lbl_invoice_date .font = [UIFont fontWithName:kFont size:11];
            lbl_array_invoice_date_val .font = [UIFont fontWithName:kFont size:11];
            lbl_order_date_time .font = [UIFont fontWithName:kFont size:11];
            lbl_array_order_date_val .font = [UIFont fontWithName:kFont size:11];
            lbl_serving_date_time .font = [UIFont fontWithName:kFont size:11];
            lbl_array_serving_date_val .font = [UIFont fontWithName:kFont size:11];
            lbl_dish_meal .font = [UIFont fontWithName:kFont size:10];
            lbl_price_per_serving .font = [UIFont fontWithName:kFont size:10];
            lbl_qty .font = [UIFont fontWithName:kFont size:10];
            
            lbl_sub_total .font = [UIFont fontWithName:kFont size:13];
            lbl_subtotal_val .font = [UIFont fontWithName:kFontBold size:11];
            lbl_delivery_fee .font = [UIFont fontWithName:kFont size:13];
            lbl_delivery_fee_val .font = [UIFont fontWithName:kFontBold size:11];
            lbl_service_fee .font = [UIFont fontWithName:kFont size:12];
            lbl_service_fee_val .font = [UIFont fontWithName:kFontBold size:11];
            
            
        }
        
        
        
        [scroll setContentSize:CGSizeMake(0,[array_total_Itemsfilter count]*20)];
        
        
    }
    
    else if (tableView == table_in_sales_view)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        img_cellBackGnd.frame =  CGRectMake(-3,-2, WIDTH-15,75);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        UILabel * lbl_array_order_val = [[UILabel alloc]init];
        lbl_array_order_val .frame = CGRectMake(20,20,100, 20);
        lbl_array_order_val .text = [NSString stringWithFormat:@"%@",[[array_Sale_Summaryfilter objectAtIndex:indexPath.row] valueForKey:@"orderId"]];
        lbl_array_order_val .font = [UIFont fontWithName:kFontBold size:14];
        lbl_array_order_val .textColor = [UIColor blackColor];
        lbl_array_order_val .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_array_order_val];
        
        
        UIImageView * img_vertical_line = [[UIImageView alloc]init];
        img_vertical_line.frame = CGRectMake(CGRectGetMaxX(lbl_array_order_val.frame)+7,0,1,75);
        [img_vertical_line setUserInteractionEnabled:YES];
        img_vertical_line.image=[UIImage imageNamed:@"img_small line@2x.png"];
        [img_cellBackGnd addSubview:img_vertical_line];
        
        UILabel * lbl_array_total_sale = [[UILabel alloc]init];
        lbl_array_total_sale .frame = CGRectMake(CGRectGetMaxX(img_vertical_line.frame)+10,20,100, 20);
        lbl_array_total_sale .text = [NSString stringWithFormat:@"%@",[[array_Sale_Summaryfilter objectAtIndex:indexPath.row] valueForKey:@"totalSale"]];
        lbl_array_total_sale .font = [UIFont fontWithName:kFontBold size:14];
        lbl_array_total_sale .textColor = [UIColor blackColor];
        lbl_array_total_sale .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_array_total_sale];
        
        
        UIImageView * img_vertical_line2 = [[UIImageView alloc]init];
        img_vertical_line2.frame = CGRectMake(CGRectGetMidX(lbl_array_total_sale.frame)+25,0,1,75);
        [img_vertical_line2 setUserInteractionEnabled:YES];
        img_vertical_line2.image=[UIImage imageNamed:@"img_small line@2x.png"];
        [img_cellBackGnd addSubview:img_vertical_line2];
        
        
        UILabel * lbl_array_netrate_val = [[UILabel alloc]init];
        lbl_array_netrate_val .frame = CGRectMake(CGRectGetMaxX(img_vertical_line2.frame)+10,20,100, 20);
        lbl_array_netrate_val .text = [NSString stringWithFormat:@"%@",[[array_Sale_Summaryfilter objectAtIndex:indexPath.row] valueForKey:@"netSale"]];
        lbl_array_netrate_val .font = [UIFont fontWithName:kFontBold size:14];
        lbl_array_netrate_val .textColor = [UIColor blackColor];
        lbl_array_netrate_val .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_array_netrate_val];
        
        
        UIImageView * img_vertical_line3 = [[UIImageView alloc]init];
        img_vertical_line3.frame = CGRectMake(CGRectGetMidX(lbl_array_netrate_val.frame)+22,0,1,75);
        [img_vertical_line3 setUserInteractionEnabled:YES];
        img_vertical_line3.image=[UIImage imageNamed:@"img_small line@2x.png"];
        [img_cellBackGnd addSubview:img_vertical_line3];
        
        
        UIImageView * img_pdf = [[UIImageView alloc]init];
        img_pdf.frame = CGRectMake(CGRectGetMaxX(img_vertical_line3.frame)+10,20,25,25);
        [img_pdf setUserInteractionEnabled:YES];
        img_pdf.image=[UIImage imageNamed:@"icon-pdf@2x.png"];
        [img_cellBackGnd addSubview:img_pdf];
        
        
        
        
        UIImageView * img_line = [[UIImageView alloc]init];
        img_line.frame = CGRectMake(5,71,WIDTH-20,1);
        [img_line setUserInteractionEnabled:YES];
        img_line.image=[UIImage imageNamed:@"img-line@2x.png"];
        [img_cellBackGnd addSubview:img_line];
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(-3,-2, WIDTH-15,75);
            lbl_array_order_val .frame = CGRectMake(20,20,100, 20);
            img_vertical_line.frame = CGRectMake(CGRectGetMaxX(lbl_array_order_val.frame)+7,0,1,75);
            lbl_array_total_sale .frame = CGRectMake(CGRectGetMaxX(img_vertical_line.frame)+10,20,100, 20);
            img_vertical_line2.frame = CGRectMake(CGRectGetMidX(lbl_array_total_sale.frame)+25,0,1,75);
            lbl_array_netrate_val .frame = CGRectMake(CGRectGetMaxX(img_vertical_line2.frame)+10,20,100, 20);
            img_vertical_line3.frame = CGRectMake(CGRectGetMidX(lbl_array_netrate_val.frame)+22,0,1,75);
            img_pdf.frame = CGRectMake(CGRectGetMaxX(img_vertical_line3.frame)+10,20,25,25);
            img_line.frame = CGRectMake(5,71,WIDTH-20,1);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(-3,-2, WIDTH-15,75);
            lbl_array_order_val .frame = CGRectMake(20,20,100, 20);
            img_vertical_line.frame = CGRectMake(CGRectGetMaxX(lbl_array_order_val.frame)+7,0,1,75);
            lbl_array_total_sale .frame = CGRectMake(CGRectGetMaxX(img_vertical_line.frame)+10,20,100, 20);
            img_vertical_line2.frame = CGRectMake(CGRectGetMidX(lbl_array_total_sale.frame)+25,0,1,75);
            lbl_array_netrate_val .frame = CGRectMake(CGRectGetMaxX(img_vertical_line2.frame)+10,20,100, 20);
            img_vertical_line3.frame = CGRectMake(CGRectGetMidX(lbl_array_netrate_val.frame)+22,0,1,75);
            img_pdf.frame = CGRectMake(CGRectGetMaxX(img_vertical_line3.frame)+10,20,25,25);
            img_line.frame = CGRectMake(5,71,WIDTH-20,1);
            
        }
        else if (IS_IPHONE_5)
        {
            img_cellBackGnd.frame =  CGRectMake(-3,-2, WIDTH-15,75);
            lbl_array_order_val .frame = CGRectMake(20,20,100, 20);
            img_vertical_line.frame = CGRectMake(CGRectGetMaxX(lbl_array_order_val.frame)+7,0,1,75);
            lbl_array_total_sale .frame = CGRectMake(CGRectGetMaxX(img_vertical_line.frame)+10,20,100, 20);
            img_vertical_line2.frame = CGRectMake(CGRectGetMidX(lbl_array_total_sale.frame)+25,0,1,75);
            lbl_array_netrate_val .frame = CGRectMake(CGRectGetMaxX(img_vertical_line2.frame)+10,20,100, 20);
            img_vertical_line3.frame = CGRectMake(CGRectGetMidX(lbl_array_netrate_val.frame)+22,0,1,75);
            img_pdf.frame = CGRectMake(CGRectGetMaxX(img_vertical_line3.frame)+10,20,25,25);
            img_line.frame = CGRectMake(5,71,WIDTH-20,1);
            
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(-3,-2, WIDTH-15,75);
            lbl_array_order_val .frame = CGRectMake(20,20,100, 20);
            img_vertical_line.frame = CGRectMake(CGRectGetMaxX(lbl_array_order_val.frame)+7,0,1,75);
            lbl_array_total_sale .frame = CGRectMake(CGRectGetMaxX(img_vertical_line.frame)+10,20,100, 20);
            img_vertical_line2.frame = CGRectMake(CGRectGetMidX(lbl_array_total_sale.frame)+25,0,1,75);
            lbl_array_netrate_val .frame = CGRectMake(CGRectGetMaxX(img_vertical_line2.frame)+10,20,100, 20);
            img_vertical_line3.frame = CGRectMake(CGRectGetMidX(lbl_array_netrate_val.frame)+22,0,1,75);
            img_pdf.frame = CGRectMake(CGRectGetMaxX(img_vertical_line3.frame)+10,20,25,25);
            img_line.frame = CGRectMake(5,71,WIDTH-20,1);
            
        }
        
    }
    else if (tableView == table_in_sales_tracker)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-15,300);
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-15,300);
            
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-15,300);
        }
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        UILabel * lbl_array_dish_names = [[UILabel alloc]init];
        lbl_array_dish_names .frame = CGRectMake(20,20,170, 20);
        lbl_array_dish_names .text = [NSString stringWithFormat:@"%@",[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"title" ]];
        lbl_array_dish_names .font = [UIFont fontWithName:kFontBold size:18];
        lbl_array_dish_names .textColor = [UIColor blackColor];
        lbl_array_dish_names .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_array_dish_names];
        
        
        
        
        UILabel * lbl_array_serving_fee = [[UILabel alloc]init];
        lbl_array_serving_fee .frame = CGRectMake(20,CGRectGetMaxY(lbl_array_dish_names.frame),170, 20);
        lbl_array_serving_fee .text = [NSString stringWithFormat:@"$ %@ per serving",[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"price" ]];
        lbl_array_serving_fee .font = [UIFont fontWithName:kFont size:13];
        lbl_array_serving_fee .textColor = [UIColor blackColor];
        lbl_array_serving_fee .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_array_serving_fee];
        
        
        UILabel * lbl_total_sal= [[UILabel alloc]init];
        lbl_total_sal .frame = CGRectMake(210,CGRectGetMaxY(lbl_array_serving_fee.frame),100, 20);
        lbl_total_sal .text = [NSString stringWithFormat:@"Total Sale:"];
        lbl_total_sal .font = [UIFont fontWithName:kFont size:16];
        lbl_total_sal .textColor = [UIColor blackColor];
        lbl_total_sal .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_total_sal];
        
        UILabel * lbl_array_totalsale_val = [[UILabel alloc]init];
        lbl_array_totalsale_val .frame = CGRectMake(CGRectGetMaxX(lbl_total_sal.frame)-10,CGRectGetMaxY(lbl_array_serving_fee.frame),170, 20);
        lbl_array_totalsale_val .text = [NSString stringWithFormat:@"%@",[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"tot_saleCount" ]];
        lbl_array_totalsale_val .font = [UIFont fontWithName:kFont size:16];
        lbl_array_totalsale_val .textColor = [UIColor blackColor];
        lbl_array_totalsale_val .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_array_totalsale_val];
        
        
        UIImageView * img_line = [[UIImageView alloc]init];
        img_line.frame = CGRectMake(10,CGRectGetMaxY(lbl_total_sal.frame),WIDTH-35,1);
        [img_line setUserInteractionEnabled:YES];
        img_line.image=[UIImage imageNamed:@"img-line@2x.png"];
        [img_cellBackGnd addSubview:img_line];
        
        
        UILabel * lbl_item = [[UILabel alloc]init];
        lbl_item .frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)+10,100, 20);
        lbl_item .text = [NSString stringWithFormat:@"Item"];
        lbl_item .font = [UIFont fontWithName:kFont size:13];
        lbl_item .textColor = [UIColor blackColor];
        lbl_item .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_item];
        
        
        UIImageView * img_line2 = [[UIImageView alloc]init];
        img_line2.frame =  CGRectMake(20,CGRectGetMaxY(lbl_item.frame),30,1);
        img_line2.image=[UIImage imageNamed:@"img-line@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_line2 setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_line2];
        
        
        UILabel * lbl_qty = [[UILabel alloc]init];
        lbl_qty .frame = CGRectMake(CGRectGetMaxX(lbl_item.frame)+95,CGRectGetMaxY(img_line.frame)+10,50, 20);
        lbl_qty .text = [NSString stringWithFormat:@"Qty"];
        lbl_qty .font = [UIFont fontWithName:kFont size:13];
        lbl_qty .textColor = [UIColor blackColor];
        lbl_qty .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_qty];
        
        
        UIImageView * img_line3 = [[UIImageView alloc]init];
        img_line3.frame =  CGRectMake(CGRectGetMaxX(img_line2.frame)+163,CGRectGetMaxY(lbl_item.frame),25,1);
        img_line3.image=[UIImage imageNamed:@"img-line@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_line3 setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_line3];
        
        
        UILabel * lbl_subtotal = [[UILabel alloc]init];
        lbl_subtotal .frame = CGRectMake(CGRectGetMaxX(lbl_qty.frame),CGRectGetMaxY(img_line.frame)+10,150, 20);
        lbl_subtotal .text = [NSString stringWithFormat:@"Subtotal"];
        lbl_subtotal .font = [UIFont fontWithName:kFont size:13];
        lbl_subtotal .textColor = [UIColor blackColor];
        lbl_subtotal .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_subtotal];
        
        
        
        UIImageView * img_line4 = [[UIImageView alloc]init];
        img_line4.frame =  CGRectMake(CGRectGetMaxX(img_line3.frame)+30,CGRectGetMaxY(lbl_item.frame),49,1);
        img_line4.image=[UIImage imageNamed:@"img-line@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_line4 setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_line4];
        
        
        //        UIScrollView * scroll = [[UIScrollView alloc]init];
        //        [scroll setShowsVerticalScrollIndicator:NO];
        //        [scroll setUserInteractionEnabled:YES];
        //        scroll.delegate = self;
        //        scroll.backgroundColor = [UIColor clearColor];
        //        scroll.frame = CGRectMake(03, CGRectGetMaxY(lbl_item.frame)+3, WIDTH-16, 80);
        //        scroll.scrollEnabled = YES;
        //        scroll.pagingEnabled = YES;
        //         scroll.layer.borderWidth = 1.0;
        //        [img_cellBackGnd addSubview:scroll];
        //
        //                for (int i=0; i<3; i++)
        //        {
        //            UILabel * lbl_array_totalsale_val = [[UILabel alloc]init];
        //            lbl_array_totalsale_val .frame = CGRectMake(20,10,170, 20);
        //
        //            //serveType
        ////          =================================conditions=======================================
        //            int total =(int)[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] count];
        //
        //            NSString *str_NotAvail = @"N.A";
        //
        //            if (total==1)
        //            {
        //
        //            if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0] valueForKey:@"serveType"]]isEqualToString:@"Dine In"])
        //            {
        //
        //
        //                lbl_array_totalsale_val .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveQuantity"]];
        //            }
        //            else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0] valueForKey:@"serveType"]]isEqualToString:@"Takeout"])
        //            {
        //                lbl_array_totalsale_val .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveQuantity"]];
        //            }
        //            else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0] valueForKey:@"serveType"]]isEqualToString:@"Delivery"])
        //            {
        //                    lbl_array_totalsale_val .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveQuantity"]];
        //            }
        //
        //            else{
        //                lbl_array_totalsale_val .text= str_NotAvail;
        //
        //            }
        //
        ////    ========================================  END  repeat for count=============================================
        //            }else if (total ==2)
        //            {
        //
        //                if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1] valueForKey:@"serveType"]]isEqualToString:@"Dine In"])
        //                {
        //                    lbl_array_totalsale_val .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1]valueForKey:@"serveQuantity"]];
        //                }
        //                else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1] valueForKey:@"serveType"]]isEqualToString:@"Takeout"])
        //                {
        //                    lbl_array_totalsale_val .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1]valueForKey:@"serveQuantity"]];
        //                }
        //                else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1] valueForKey:@"serveType"]]isEqualToString:@"Delivery"])
        //                {
        //                    lbl_array_totalsale_val .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1]valueForKey:@"serveQuantity"]];
        //                }
        //
        //                else{
        //                    lbl_array_totalsale_val .text= str_NotAvail;
        //
        //                }
        //
        //            }
        //
        //            else if (total ==3)
        //            {
        //                if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:2] valueForKey:@"serveType"]]isEqualToString:@"Dine In"])
        //                {
        //                    lbl_array_totalsale_val .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:2]valueForKey:@"serveQuantity"]];
        //                }
        //                else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:2] valueForKey:@"serveType"]]isEqualToString:@"Takeout"])
        //                {
        //                    lbl_array_totalsale_val .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:2]valueForKey:@"serveQuantity"]];
        //                }
        //                else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:2] valueForKey:@"serveType"]]isEqualToString:@"Delivery"])
        //                {
        //                    lbl_array_totalsale_val .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:2]valueForKey:@"serveQuantity"]];
        //                }
        //
        //                else{
        //                    lbl_array_totalsale_val .text= str_NotAvail;
        //
        //                }
        //
        //
        //            }
        //
        ////            if ([[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] containsObject:@"Dine In"])
        ////            {
        ////                lbl_array_totalsale_val .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Tracker objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:i]valueForKey:@"serveQuantity"]];
        ////            }
        ////            else
        ////            {
        ////                 lbl_array_totalsale_val .text= str_NotAvail;
        ////
        ////
        ////            }
        //            //lbl_array_totalsale_val .text = [NSString stringWithFormat:@"%@",[array_labl objectAtIndex:indexPath.row]];
        //            lbl_array_totalsale_val .font = [UIFont fontWithName:kFont size:16];
        //            lbl_array_totalsale_val .textColor = [UIColor blackColor];
        //            lbl_array_totalsale_val .backgroundColor = [UIColor clearColor];
        //            [scroll addSubview: lbl_array_totalsale_val];
        //
        ////
        //       }
        
        
        
        
        
        UILabel * lbl_Dine_In= [[UILabel alloc]init];
        lbl_Dine_In .frame = CGRectMake(20,CGRectGetMaxY(img_line4.frame)+10,150, 20);
        lbl_Dine_In .text = [NSString stringWithFormat:@"Dine In"];
        lbl_Dine_In .font = [UIFont fontWithName:kFont size:17];
        lbl_Dine_In .textColor = [UIColor blackColor];
        lbl_Dine_In .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_Dine_In];
        
        
        UILabel * lbl_Dine_Qty= [[UILabel alloc]init];
        lbl_Dine_Qty .frame = CGRectMake(180,CGRectGetMaxY(img_line4.frame)+10,60, 20);
        lbl_Dine_Qty .text= @"dsgdt";
        lbl_Dine_Qty .text = [NSString stringWithFormat:@"Dine In"];
        lbl_Dine_Qty .font = [UIFont fontWithName:kFont size:17];
        lbl_Dine_Qty .textColor = [UIColor blackColor];
        lbl_Dine_Qty .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_Dine_Qty];
        
        
        UILabel * lbl_Dine_SubTotal= [[UILabel alloc]init];
        lbl_Dine_SubTotal .frame = CGRectMake(240,CGRectGetMaxY(img_line4.frame)+10,60, 20);
        lbl_Dine_SubTotal .text = [NSString stringWithFormat:@"Dine In"];
        lbl_Dine_SubTotal .font = [UIFont fontWithName:kFont size:17];
        lbl_Dine_SubTotal .textColor = [UIColor blackColor];
        lbl_Dine_SubTotal .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_Dine_SubTotal];
        
        
        UILabel * lbl_Take= [[UILabel alloc]init];
        lbl_Take .frame = CGRectMake(20,CGRectGetMaxY(lbl_Dine_SubTotal.frame),150, 20);
        lbl_Take .text = [NSString stringWithFormat:@"Take Out"];
        lbl_Take .font = [UIFont fontWithName:kFont size:17];
        lbl_Take .textColor = [UIColor blackColor];
        lbl_Take .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_Take];
        
        
        
        UILabel * lbl_Tak_Qty= [[UILabel alloc]init];
        lbl_Tak_Qty .frame = CGRectMake(180,CGRectGetMaxY(lbl_Dine_SubTotal.frame),60, 20);
        lbl_Tak_Qty .text = [NSString stringWithFormat:@"Dine In"];
        lbl_Tak_Qty .font = [UIFont fontWithName:kFont size:17];
        lbl_Tak_Qty .textColor = [UIColor blackColor];
        lbl_Tak_Qty .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_Tak_Qty];
        
        
        UILabel * lbl_Tak_Subtotal= [[UILabel alloc]init];
        lbl_Tak_Subtotal .frame = CGRectMake(240,CGRectGetMaxY(lbl_Dine_SubTotal.frame),60, 20);
        lbl_Tak_Subtotal .text = [NSString stringWithFormat:@"Dine In"];
        lbl_Tak_Subtotal .font = [UIFont fontWithName:kFont size:17];
        lbl_Tak_Subtotal .textColor = [UIColor blackColor];
        lbl_Tak_Subtotal .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_Tak_Subtotal];
        
        
        UILabel * lbl_Delivery= [[UILabel alloc]init];
        lbl_Delivery .frame = CGRectMake(20,CGRectGetMaxY(lbl_Tak_Subtotal.frame),150, 20);
        lbl_Delivery .text = [NSString stringWithFormat:@"Delivery"];
        lbl_Delivery .font = [UIFont fontWithName:kFont size:17];
        lbl_Delivery .textColor = [UIColor blackColor];
        lbl_Delivery .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_Delivery];
        
        
        UILabel * lbl_Delivery_Qty= [[UILabel alloc]init];
        lbl_Delivery_Qty .frame = CGRectMake(180,CGRectGetMaxY(lbl_Tak_Subtotal.frame),60, 20);
        lbl_Delivery_Qty .text = [NSString stringWithFormat:@"Dine In"];
        lbl_Delivery_Qty .font = [UIFont fontWithName:kFont size:17];
        lbl_Delivery_Qty .textColor = [UIColor blackColor];
        lbl_Delivery_Qty .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_Delivery_Qty];
        
        
        UILabel * lbl_Delivery_Subtotal= [[UILabel alloc]init];
        lbl_Delivery_Subtotal .frame = CGRectMake(240,CGRectGetMaxY(lbl_Tak_Subtotal.frame),60, 20);
        lbl_Delivery_Subtotal .text = [NSString stringWithFormat:@"Dine In"];
        lbl_Delivery_Subtotal .font = [UIFont fontWithName:kFont size:17];
        lbl_Delivery_Subtotal .textColor = [UIColor blackColor];
        lbl_Delivery_Subtotal .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_Delivery_Subtotal];
        
        
        
        UILabel * lbl_total_erning= [[UILabel alloc]init];
        lbl_total_erning .frame = CGRectMake(CGRectGetMidX(lbl_qty.frame)-70,CGRectGetMaxY(lbl_Delivery_Subtotal.frame)+10,150, 20);
        lbl_total_erning .text = [NSString stringWithFormat:@"Total Earning:"];
        lbl_total_erning .font = [UIFont fontWithName:kFont size:17];
        lbl_total_erning .textColor = [UIColor blackColor];
        lbl_total_erning .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_total_erning];
        
        
        
        UILabel * lbl_array_totalearn_val = [[UILabel alloc]init];
        lbl_array_totalearn_val .frame = CGRectMake(CGRectGetMaxX(lbl_total_erning.frame)-35,CGRectGetMaxY(lbl_Delivery_Subtotal.frame)+10,170, 20);
        lbl_array_totalearn_val .text = [NSString stringWithFormat:@"%@",[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"tot_sale"]];
        lbl_array_totalearn_val .font = [UIFont fontWithName:kFontBold size:17];
        lbl_array_totalearn_val .textColor = [UIColor blackColor];
        lbl_array_totalearn_val .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview: lbl_array_totalearn_val];
        //
        
        int total =(int)[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] count];
        
        NSString *str_NotAvail = @"N.A";
        
        if (total==1)
        {
            
            if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0] valueForKey:@"serveType"]]isEqualToString:@"Dine In"])
            {
                lbl_Dine_In.text = @"Dine In";
                lbl_Dine_Qty .text= [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveQuantity"]];
                lbl_Dine_SubTotal .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveSubTotal"]];
                lbl_Take .text = @"Take Out";
                lbl_Tak_Qty .text = str_NotAvail;
                lbl_Tak_Subtotal .text = str_NotAvail;
                lbl_Delivery .text = @"Delivery";
                lbl_Delivery_Qty .text =  str_NotAvail;
                lbl_Delivery_Subtotal .text =  str_NotAvail;
            }
            
            else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0] valueForKey:@"serveType"]]isEqualToString:@"Takeout"])
            {
                lbl_Dine_In.text = @"Dine In";
                lbl_Dine_Qty .text= str_NotAvail;;
                lbl_Dine_SubTotal .text = str_NotAvail;;
                
                lbl_Take .text = @"Take Out";
                lbl_Tak_Qty .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveQuantity"]];
                lbl_Tak_Subtotal .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveSubTotal"]];
                
                lbl_Delivery .text = @"Delivery";
                lbl_Delivery_Qty .text =  str_NotAvail;
                lbl_Delivery_Subtotal .text =  str_NotAvail;
            }
            
            else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0] valueForKey:@"serveType"]]isEqualToString:@"Delivery"])
            {
                lbl_Dine_In.text = @"Dine In";
                lbl_Dine_Qty .text= str_NotAvail;
                lbl_Dine_SubTotal .text = str_NotAvail;
                
                lbl_Take .text = @"Take Out";
                lbl_Tak_Qty .text = str_NotAvail;
                lbl_Tak_Subtotal .text = str_NotAvail;
                
                lbl_Delivery .text = @"Delivery";
                lbl_Delivery_Qty .text =  [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveQuantity"]];
                lbl_Delivery_Subtotal .text =  [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveSubTotal"]];
            }
            
            else{
                lbl_array_totalsale_val .text= str_NotAvail;
                
            }
        }
        else if (total == 2)
        {
            
            if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0] valueForKey:@"serveType"]]isEqualToString:@"Dine In"])
            {
                lbl_Dine_In.text = @"Dine In";
                lbl_Dine_Qty .text= [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveQuantity"]];
                lbl_Dine_SubTotal .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveSubTotal"]];
                lbl_Take .text = @"Take Out";
                lbl_Tak_Qty .text = str_NotAvail;
                lbl_Tak_Subtotal .text = str_NotAvail;
                lbl_Delivery .text = @"Delivery";
                lbl_Delivery_Qty .text =  str_NotAvail;
                lbl_Delivery_Subtotal .text =  str_NotAvail;
            }
            
            else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0] valueForKey:@"serveType"]]isEqualToString:@"Takeout"])
            {
                lbl_Dine_In.text = @"Dine In";
                lbl_Dine_Qty .text= str_NotAvail;
                lbl_Dine_SubTotal .text = str_NotAvail;
                
                lbl_Take .text = @"Take Out";
                lbl_Tak_Qty .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveQuantity"]];
                lbl_Tak_Subtotal .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveSubTotal"]];
                
                lbl_Delivery .text = @"Delivery";
                lbl_Delivery_Qty .text =  str_NotAvail;
                lbl_Delivery_Subtotal .text =  str_NotAvail;
            }
            
            else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0] valueForKey:@"serveType"]]isEqualToString:@"Delivery"])
            {
                lbl_Dine_In.text = @"Dine In";
                lbl_Dine_Qty .text= str_NotAvail;
                lbl_Dine_SubTotal .text = str_NotAvail;
                
                lbl_Take .text = @"Take Out";
                lbl_Tak_Qty .text = str_NotAvail;
                lbl_Tak_Subtotal .text = str_NotAvail;
                
                lbl_Delivery .text = @"Delivery";
                lbl_Delivery_Qty .text =  [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveQuantity"]];
                lbl_Delivery_Subtotal .text =  [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveSubTotal"]];
            }
            else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1] valueForKey:@"serveType"]]isEqualToString:@"Dine In"])
            {
                lbl_Dine_In.text = @"Dine In";
                lbl_Dine_Qty .text= [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1]valueForKey:@"serveQuantity"]];
                lbl_Dine_SubTotal .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1]valueForKey:@"serveSubTotal"]];
                lbl_Take .text = @"Take Out";
                lbl_Tak_Qty .text = str_NotAvail;
                lbl_Tak_Subtotal .text = str_NotAvail;
                lbl_Delivery .text = @"Delivery";
                lbl_Delivery_Qty .text =  str_NotAvail;
                lbl_Delivery_Subtotal .text =  str_NotAvail;
            }
            
            else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1] valueForKey:@"serveType"]]isEqualToString:@"Takeout"])
            {
                lbl_Dine_In.text = @"Dine In";
                lbl_Dine_Qty .text= str_NotAvail;
                lbl_Dine_SubTotal .text = str_NotAvail;
                
                lbl_Take .text = @"Take Out";
                lbl_Tak_Qty .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1]valueForKey:@"serveQuantity"]];
                lbl_Tak_Subtotal .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1]valueForKey:@"serveSubTotal"]];
                
                lbl_Delivery .text = @"Delivery";
                lbl_Delivery_Qty .text =  str_NotAvail;
                lbl_Delivery_Subtotal .text =  str_NotAvail;
            }
            
            else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1] valueForKey:@"serveType"]]isEqualToString:@"Delivery"])
            {
                lbl_Dine_In.text = @"Dine In";
                lbl_Dine_Qty .text= str_NotAvail;
                lbl_Dine_SubTotal .text = str_NotAvail;
                
                lbl_Take .text = @"Take Out";
                lbl_Tak_Qty .text = str_NotAvail;
                lbl_Tak_Subtotal .text = str_NotAvail;
                
                lbl_Delivery .text = @"Delivery";
                lbl_Delivery_Qty .text =  [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1]valueForKey:@"serveQuantity"]];
                lbl_Delivery_Subtotal .text =  [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1]valueForKey:@"serveSubTotal"]];
            }
            
            else{
                lbl_array_totalsale_val .text= str_NotAvail;
                
            }
            
        }
        else if (total == 3)
        {
            if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0] valueForKey:@"serveType"]]isEqualToString:@"Dine In"])
            {
                lbl_Dine_In.text = @"Dine In";
                lbl_Dine_Qty .text= [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveQuantity"]];
                lbl_Dine_SubTotal .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveSubTotal"]];
                lbl_Take .text = @"Take Out";
                lbl_Tak_Qty .text = str_NotAvail;
                lbl_Tak_Subtotal .text = str_NotAvail;
                lbl_Delivery .text = @"Delivery";
                lbl_Delivery_Qty .text =  str_NotAvail;
                lbl_Delivery_Subtotal .text =  str_NotAvail;
            }
            
            else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0] valueForKey:@"serveType"]]isEqualToString:@"Takeout"])
            {
                lbl_Dine_In.text = @"Dine In";
                lbl_Dine_Qty .text= str_NotAvail;
                lbl_Dine_SubTotal .text = str_NotAvail;
                
                lbl_Take .text = @"Take Out";
                lbl_Tak_Qty .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveQuantity"]];
                lbl_Tak_Subtotal .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveSubTotal"]];
                
                lbl_Delivery .text = @"Delivery";
                lbl_Delivery_Qty .text =  str_NotAvail;
                lbl_Delivery_Subtotal .text =  str_NotAvail;
            }
            
            else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0] valueForKey:@"serveType"]]isEqualToString:@"Delivery"])
            {
                lbl_Dine_In.text = @"Dine In";
                lbl_Dine_Qty .text= str_NotAvail;
                lbl_Dine_SubTotal .text = str_NotAvail;
                
                lbl_Take .text = @"Take Out";
                lbl_Tak_Qty .text = str_NotAvail;
                lbl_Tak_Subtotal .text = str_NotAvail;
                
                lbl_Delivery .text = @"Delivery";
                lbl_Delivery_Qty .text =  [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveQuantity"]];
                lbl_Delivery_Subtotal .text =  [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:0]valueForKey:@"serveSubTotal"]];
            }
            else  if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1] valueForKey:@"serveType"]]isEqualToString:@"Dine In"])
            {
                lbl_Dine_In.text = @"Dine In";
                lbl_Dine_Qty .text= [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1]valueForKey:@"serveQuantity"]];
                lbl_Dine_SubTotal .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1]valueForKey:@"serveSubTotal"]];
                lbl_Take .text = @"Take Out";
                lbl_Tak_Qty .text = str_NotAvail;
                lbl_Tak_Subtotal .text = str_NotAvail;
                lbl_Delivery .text = @"Delivery";
                lbl_Delivery_Qty .text =  str_NotAvail;
                lbl_Delivery_Subtotal .text =  str_NotAvail;
            }
            
            else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1] valueForKey:@"serveType"]]isEqualToString:@"Takeout"])
            {
                lbl_Dine_In.text = @"Dine In";
                lbl_Dine_Qty .text= str_NotAvail;
                lbl_Dine_SubTotal .text = str_NotAvail;
                
                lbl_Take .text = @"Take Out";
                lbl_Tak_Qty .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1]valueForKey:@"serveQuantity"]];
                lbl_Tak_Subtotal .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1]valueForKey:@"serveSubTotal"]];
                
                lbl_Delivery .text = @"Delivery";
                lbl_Delivery_Qty .text =  str_NotAvail;
                lbl_Delivery_Subtotal .text =  str_NotAvail;
            }
            
            else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1] valueForKey:@"serveType"]]isEqualToString:@"Delivery"])
            {
                lbl_Dine_In.text = @"Dine In";
                lbl_Dine_Qty .text= str_NotAvail;
                lbl_Dine_SubTotal .text = str_NotAvail;
                
                lbl_Take .text = @"Take Out";
                lbl_Tak_Qty .text = str_NotAvail;
                lbl_Tak_Subtotal .text = str_NotAvail;
                
                lbl_Delivery .text = @"Delivery";
                lbl_Delivery_Qty .text =  [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1]valueForKey:@"serveQuantity"]];
                lbl_Delivery_Subtotal .text =  [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:1]valueForKey:@"serveSubTotal"]];
            }
            
            else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:2] valueForKey:@"serveType"]]isEqualToString:@"Dine In"])
            {
                lbl_Dine_In.text = @"Dine In";
                lbl_Dine_Qty .text= [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:2]valueForKey:@"serveQuantity"]];
                lbl_Dine_SubTotal .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:2]valueForKey:@"serveSubTotal"]];
                lbl_Take .text = @"Take Out";
                lbl_Tak_Qty .text = str_NotAvail;
                lbl_Tak_Subtotal .text = str_NotAvail;
                lbl_Delivery .text = @"Delivery";
                lbl_Delivery_Qty .text =  str_NotAvail;
                lbl_Delivery_Subtotal .text =  str_NotAvail;
            }
            
            else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:2] valueForKey:@"serveType"]]isEqualToString:@"Takeout"])
            {
                lbl_Dine_In.text = @"Dine In";
                lbl_Dine_Qty .text= str_NotAvail;;
                lbl_Dine_SubTotal .text = str_NotAvail;
                
                lbl_Take .text = @"Take Out";
                lbl_Tak_Qty .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:2]valueForKey:@"serveQuantity"]];
                lbl_Tak_Subtotal .text = [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:2]valueForKey:@"serveSubTotal"]];
                
                lbl_Delivery .text = @"Delivery";
                lbl_Delivery_Qty .text =  str_NotAvail;
                lbl_Delivery_Subtotal .text =  str_NotAvail;
            }
            
            else if ([[NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:2] valueForKey:@"serveType"]]isEqualToString:@"Delivery"])
            {
                lbl_Dine_In.text = @"Dine In";
                lbl_Dine_Qty .text= str_NotAvail;
                lbl_Dine_SubTotal .text = str_NotAvail;
                
                lbl_Take .text = @"Take Out";
                lbl_Tak_Qty .text = str_NotAvail;
                lbl_Tak_Subtotal .text = str_NotAvail;
                
                lbl_Delivery .text = @"Delivery";
                lbl_Delivery_Qty .text =  [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:2]valueForKey:@"serveQuantity"]];
                lbl_Delivery_Subtotal .text =  [NSString stringWithFormat:@"%@",[[[[array_Sale_Trackerfilters objectAtIndex:indexPath.row]valueForKey:@"DishDetail"] objectAtIndex:2]valueForKey:@"serveSubTotal"]];
            }
            
            else{
                lbl_array_totalsale_val .text= str_NotAvail;
                
            }
            
        }
        
        
        
        
        //        if (IS_IPHONE_6Plus)
        //        {
        //            lbl_array_dish_names .frame = CGRectMake(20,20,170, 20);
        //            lbl_array_serving_fee .frame = CGRectMake(20,CGRectGetMaxY(lbl_array_dish_names.frame),170, 20);
        //            lbl_total_sal .frame = CGRectMake(260,CGRectGetMaxY(lbl_array_serving_fee.frame),100, 20);
        //            lbl_array_totalsale_val .frame = CGRectMake(CGRectGetMaxX(lbl_total_sal.frame)-10,CGRectGetMaxY(lbl_array_serving_fee.frame),170, 20);
        //            img_line.frame = CGRectMake(10,CGRectGetMaxY(lbl_total_sal.frame),WIDTH-35,1);
        //            lbl_item .frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)+10,100, 20);
        //            img_line2.frame =  CGRectMake(20,CGRectGetMaxY(lbl_item.frame),30,1);
        //            lbl_qty .frame = CGRectMake(CGRectGetMaxX(lbl_item.frame)+135,CGRectGetMaxY(img_line.frame)+10,50, 20);
        //            img_line3.frame =  CGRectMake(CGRectGetMaxX(img_line2.frame)+207,CGRectGetMaxY(lbl_item.frame),25,1);
        //            lbl_subtotal .frame = CGRectMake(CGRectGetMaxX(lbl_qty.frame),CGRectGetMaxY(img_line.frame)+10,150, 20);
        //            img_line4.frame =  CGRectMake(CGRectGetMaxX(img_line3.frame)+30,CGRectGetMaxY(lbl_item.frame),49,1);
        //
        //            scroll.frame = CGRectMake(03, CGRectGetMaxY(lbl_item.frame)+3, WIDTH-16, 80);
        //            lbl_total_erning .frame = CGRectMake(CGRectGetMidX(lbl_qty.frame)-70,CGRectGetMaxY(scroll.frame)+10,150, 20);
        //            lbl_array_totalearn_val .frame = CGRectMake(CGRectGetMaxX(lbl_total_erning.frame)-35,CGRectGetMaxY(scroll.frame)+10,170, 20);
        //        }
        //        else if (IS_IPHONE_6)
        //        {
        //            lbl_array_dish_names .frame = CGRectMake(20,20,170, 20);
        //            lbl_array_serving_fee .frame = CGRectMake(20,CGRectGetMaxY(lbl_array_dish_names.frame),170, 20);
        //            lbl_total_sal .frame = CGRectMake(210,CGRectGetMaxY(lbl_array_serving_fee.frame),100, 20);
        //            lbl_array_totalsale_val .frame = CGRectMake(CGRectGetMaxX(lbl_total_sal.frame)-10,CGRectGetMaxY(lbl_array_serving_fee.frame),170, 20);
        //            img_line.frame = CGRectMake(10,CGRectGetMaxY(lbl_total_sal.frame),WIDTH-35,1);
        //            lbl_item .frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)+10,100, 20);
        //            img_line2.frame =  CGRectMake(20,CGRectGetMaxY(lbl_item.frame),30,1);
        //            lbl_qty .frame = CGRectMake(CGRectGetMaxX(lbl_item.frame)+95,CGRectGetMaxY(img_line.frame)+10,50, 20);
        //            img_line3.frame =  CGRectMake(CGRectGetMaxX(img_line2.frame)+163,CGRectGetMaxY(lbl_item.frame),25,1);
        //            lbl_subtotal .frame = CGRectMake(CGRectGetMaxX(lbl_qty.frame),CGRectGetMaxY(img_line.frame)+10,150, 20);
        //            img_line4.frame =  CGRectMake(CGRectGetMaxX(img_line3.frame)+30,CGRectGetMaxY(lbl_item.frame),49,1);
        //
        //            scroll.frame = CGRectMake(03, CGRectGetMaxY(lbl_item.frame)+3, WIDTH-16, 80);
        //            lbl_total_erning .frame = CGRectMake(CGRectGetMidX(lbl_qty.frame)-70,CGRectGetMaxY(scroll.frame)+10,150, 20);
        //            lbl_array_totalearn_val .frame = CGRectMake(CGRectGetMaxX(lbl_total_erning.frame)-35,CGRectGetMaxY(scroll.frame)+10,170, 20);
        //        }
        //        else if (IS_IPHONE_5)
        //        {
        //            lbl_array_dish_names .frame = CGRectMake(20,20,170, 20);
        //            lbl_array_serving_fee .frame = CGRectMake(20,CGRectGetMaxY(lbl_array_dish_names.frame),170, 20);
        //            lbl_total_sal .frame = CGRectMake(180,CGRectGetMaxY(lbl_array_serving_fee.frame),100, 20);
        //            lbl_array_totalsale_val .frame = CGRectMake(CGRectGetMaxX(lbl_total_sal.frame)-35,CGRectGetMaxY(lbl_array_serving_fee.frame),170, 20);
        //            img_line.frame = CGRectMake(10,CGRectGetMaxY(lbl_total_sal.frame),WIDTH-50,1);
        //            lbl_item .frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)+10,100, 20);
        //            img_line2.frame =  CGRectMake(20,CGRectGetMaxY(lbl_item.frame),30,1);
        //            lbl_qty .frame = CGRectMake(CGRectGetMaxX(lbl_item.frame)+60,CGRectGetMaxY(img_line.frame)+10,50, 20);
        //            img_line3.frame =  CGRectMake(CGRectGetMaxX(img_line2.frame)+130,CGRectGetMaxY(lbl_item.frame),22,1);
        //            lbl_subtotal .frame = CGRectMake(CGRectGetMaxX(lbl_qty.frame),CGRectGetMaxY(img_line.frame)+10,150, 20);
        //            img_line4.frame =  CGRectMake(CGRectGetMaxX(img_line3.frame)+30,CGRectGetMaxY(lbl_item.frame),49,1);
        //
        //            scroll.frame = CGRectMake(03, CGRectGetMaxY(lbl_item.frame)+3, WIDTH-35, 80);
        //            lbl_total_erning .frame = CGRectMake(CGRectGetMidX(lbl_qty.frame)-50,CGRectGetMaxY(scroll.frame)+10,150, 20);
        //            lbl_array_totalearn_val .frame = CGRectMake(CGRectGetMaxX(lbl_total_erning.frame)-65,CGRectGetMaxY(scroll.frame)+12,170, 20);
        //
        //            lbl_array_dish_names .font = [UIFont fontWithName:kFontBold size:14];
        //            lbl_array_serving_fee .font = [UIFont fontWithName:kFont size:11];
        //
        //            lbl_total_sal .font = [UIFont fontWithName:kFont size:13];
        //            lbl_array_totalsale_val .font = [UIFont fontWithName:kFont size:13];
        //            lbl_item .font = [UIFont fontWithName:kFont size:11];
        //            lbl_qty .font = [UIFont fontWithName:kFont size:11];
        //            lbl_subtotal .font = [UIFont fontWithName:kFont size:11];
        //            lbl_total_erning .font = [UIFont fontWithName:kFontBold size:13];
        //            lbl_array_totalearn_val .font = [UIFont fontWithName:kFontBold size:13];
        //
        //        }
        //        else
        //        {
        //            lbl_array_dish_names .frame = CGRectMake(20,20,170, 20);
        //            lbl_array_serving_fee .frame = CGRectMake(20,CGRectGetMaxY(lbl_array_dish_names.frame),170, 20);
        //            lbl_total_sal .frame = CGRectMake(210,CGRectGetMaxY(lbl_array_serving_fee.frame),100, 20);
        //            lbl_array_totalsale_val .frame = CGRectMake(CGRectGetMaxX(lbl_total_sal.frame)-10,CGRectGetMaxY(lbl_array_serving_fee.frame),170, 20);
        //            img_line.frame = CGRectMake(10,CGRectGetMaxY(lbl_total_sal.frame),WIDTH-35,1);
        //            lbl_item .frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)+10,100, 20);
        //            img_line2.frame =  CGRectMake(20,CGRectGetMaxY(lbl_item.frame),30,1);
        //            lbl_qty .frame = CGRectMake(CGRectGetMaxX(lbl_item.frame)+95,CGRectGetMaxY(img_line.frame)+10,50, 20);
        //            img_line3.frame =  CGRectMake(CGRectGetMaxX(img_line2.frame)+163,CGRectGetMaxY(lbl_item.frame),25,1);
        //            lbl_subtotal .frame = CGRectMake(CGRectGetMaxX(lbl_qty.frame),CGRectGetMaxY(img_line.frame)+10,150, 20);
        //            img_line4.frame =  CGRectMake(CGRectGetMaxX(img_line3.frame)+30,CGRectGetMaxY(lbl_item.frame),49,1);
        //
        //            scroll.frame = CGRectMake(03, CGRectGetMaxY(lbl_item.frame)+3, WIDTH-16, 80);
        //            lbl_total_erning .frame = CGRectMake(CGRectGetMidX(lbl_qty.frame)-70,CGRectGetMaxY(scroll.frame)+10,150, 20);
        //            lbl_array_totalearn_val .frame = CGRectMake(CGRectGetMaxX(lbl_total_erning.frame)-35,CGRectGetMaxY(scroll.frame)+10,170, 20);
        //        }
        
        
        
        
        
        
    }
    
    
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    [self.navigationController pushViewController:VC animated:NO];
    if(tableView == table_on_drop_down)
    {
        
        str_order = @"ASC";
        
        if (indexPath.row == 0)
        {
            view_for_account.hidden = NO;
            view_for_sales_summery.hidden = YES;
            view_for_sales_tracker.hidden = YES;
            
        }
        else if (indexPath.row == 1)
        {
            view_for_account.hidden =YES;
            view_for_sales_summery.hidden = NO;
            view_for_sales_tracker.hidden = YES;
            
        }
        else
        {
            view_for_account.hidden =YES;
            view_for_sales_summery.hidden = YES;
            view_for_sales_tracker.hidden = NO;
            
        }
        
        lbl_account.text =[array_head_names objectAtIndex:indexPath.row];
        [table_on_drop_down setHidden:YES];
    }
    
    
}

#pragma click_events


- (void)datePickerValueChanged1:(id)sender
{
    [formatter1 setDateFormat:@"dd/MM/yyyy"];
    NSString *str=[formatter1 stringFromDate:datePicker1.date];
    text_date2.font = [UIFont fontWithName:kFont size:14];
    text_date2.textColor = [UIColor blackColor];
    [text_date2 setText:str];
    
    if (text_date.text.length>0 && text_date2.text.length>0)
    {
        [self account_sections];
    }
}
- (void)datePickerValueChanged:(id)sender
{
    [formatter setDateFormat:@"dd/MM/yyyy"];
    NSString *str=[formatter stringFromDate:datePicker.date];
    text_date.font = [UIFont fontWithName:kFont size:14];
    text_date.textColor = [UIColor blackColor];
    [text_date setText:str];
    
    if (text_date.text.length>0 && text_date2.text.length>0)
    {
        [self account_sections];
    }
    
}
-(void) click_DoneDate:(id) sender
{
    [formatter setDateFormat:@"dd/MM/yyyy"];
    NSString *str=[formatter stringFromDate:datePicker.date];
    text_date.font = [UIFont fontWithName:kFont size:14];
    text_date.textColor = [UIColor blackColor];
    [text_date setText:str];
    [text_date resignFirstResponder];
    
    if (text_date.text.length>0 && text_date2.text.length>0)
    {
        [self account_sections];
    }
    
    
    
}
-(void) click_DoneDate1:(id) sender
{
    [formatter1 setDateFormat:@"dd/MM/yyyy"];
    NSString *str=[formatter1 stringFromDate:datePicker1.date];
    text_date2.font = [UIFont fontWithName:kFont size:14];
    text_date2.textColor = [UIColor blackColor];
    [text_date2 setText:str];
    [text_date2 resignFirstResponder];
    
    if (text_date.text.length>0 && text_date2.text.length>0)
    {
        [self account_sections];
    }
    
}

-(void) click_DoneDate2sales:(id) sender
{
    [formatter setDateFormat:@"dd/MM/yyyy"];
    NSString *str=[formatter stringFromDate:datePicker4.date];
    text_date2_in_saleview.font = [UIFont fontWithName:kFont size:14];
    text_date2_in_saleview.textColor = [UIColor blackColor];
    [text_date2_in_saleview setText:str];
    [text_date2_in_saleview resignFirstResponder];
    if (text_date_in_saleview.text.length>0 && text_date2_in_saleview.text.length>0)
    {
        [self AFSaleSummary];
    }
    
}
-(void) click_DoneDate1sales:(id) sender
{
    [formatter3 setDateFormat:@"dd/MM/yyyy"];
    NSString *str=[formatter3 stringFromDate:datePicker3.date];
    text_date_in_saleview.font = [UIFont fontWithName:kFont size:14];
    text_date_in_saleview.textColor = [UIColor blackColor];
    [text_date_in_saleview setText:str];
    [text_date_in_saleview resignFirstResponder];
    
    if (text_date_in_saleview.text.length>0 && text_date2_in_saleview.text.length>0)
    {
        [self AFSaleSummary];
    }
    
}
-(void) datePickerValueChangedtrace1:(id) sender
{
    [formatter5 setDateFormat:@"dd/MM/yyyy"];
    NSString *str=[formatter5 stringFromDate:datePicker5.date];
    text_date_in_trackerview.font = [UIFont fontWithName:kFont size:14];
    text_date_in_trackerview.textColor = [UIColor blackColor];
    [text_date_in_trackerview setText:str];
    [text_date_in_trackerview resignFirstResponder];
    if (text_date_in_trackerview.text.length>0 && text_date2_in_trackerview.text.length>0)
    {
        [self AFSaleTracker];
    }
    
}
-(void) datePickerValueChangedtrace2:(id) sender
{
    [formatter6 setDateFormat:@"dd/MM/yyyy"];
    NSString *str=[formatter6 stringFromDate:datePicker6.date];
    text_date2_in_trackerview.font = [UIFont fontWithName:kFont size:14];
    text_date2_in_trackerview.textColor = [UIColor blackColor];
    [text_date2_in_trackerview setText:str];
    [text_date2_in_trackerview resignFirstResponder];
    if (text_date_in_trackerview.text.length>0 && text_date2_in_trackerview.text.length>0)
    {
        [self AFSaleTracker];
    }
    
}

-(void)btn_sortbyaccounttrace_click:(UIButton *)sender
{
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        str_order = @"DESC";
        
        [self AFSaleTracker];
        
        
    }
    else
    {
        [sender setSelected:NO];
        str_order = @"ASC";
        [self AFSaleTracker];
        
    }
    
}
-(void)btn_sortbyaccount_click:(UIButton *)sender
{
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        str_order = @"DESC";
        
        [self account_sections];
        
        
    }
    else
    {
        [sender setSelected:NO];
        str_order = @"ASC";
        [self account_sections];
        
    }
    
}
-(void)btn_sortbyaccountsummary_click:(UIButton *)sender
{
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        str_order = @"DESC";
        
        [self AFSaleSummary];
        
        
    }
    else
    {
        [sender setSelected:NO];
        str_order = @"ASC";
        [self AFSaleSummary];
        
    }
    
}


-(void)btn_menu_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click:");
}
-(void)btn_drop_down:(UIButton *)sender
{
    NSLog(@"btn_drop_down:");
}
-(void)btn_on_account_label_click:(UIButton *)sender
{
    NSLog(@"btn_on_food_later_label_click:");
    table_on_drop_down.hidden=YES;
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        table_on_drop_down.hidden=NO;
        
        
    }
    else
    {
        [sender setSelected:NO];
        table_on_drop_down.hidden=YES;
    }
    
    
    
    
}
-(void)btn_on_search_bar_click:(UIButton *)sender
{
    NSLog(@"btn_on_search_bar_click:");
}

#pragma mark ---textFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField ==txt_search)
    {
        if(textField.text)
        {
            search_leg = range.location+1;
        }
        else
        {
            search_leg=0;
        }
        
        
        if (ary_accountsfilter)
        {
            [ary_accountsfilter removeAllObjects];
        }
        
        
        for (NSMutableDictionary *dict in  array_for_accounts)
        {
            
            NSString *search = [dict valueForKey:@"Kitchen_Name"];
            
            if ([search length]>search_leg)
            {
                search = [search substringToIndex:search_leg];
            }
            
            NSString *substring = [NSString stringWithString:textField.text];
            substring = [substring stringByReplacingCharactersInRange:range withString:string];
            BOOL nameMatches = [[substring lowercaseString] hasPrefix:[string lowercaseString]];
            NSRange r = [search rangeOfString:substring options:NSCaseInsensitiveSearch];
            
            if(r.location != NSNotFound || nameMatches)
            {
                if (r.length > 0)
                {
                    [ary_accountsfilter addObject:dict];
                }
            }
            
        }
        
        [table_for_ordrelist reloadData];
        
    }
    else if (textField ==txt_search_in_saleview)
    {
        if(textField.text)
        {
            search_legsales = range.location+1;
        }
        else
        {
            search_legsales=0;
        }
        
        
        if (array_Sale_Summaryfilter)
        {
            [array_Sale_Summaryfilter removeAllObjects];
        }
        
        
        for (NSMutableDictionary *dict in  array_Sale_Summary)
        {
            
            NSString *search = [dict valueForKey:@"orderId"];
            
            if ([search length]>search_legsales)
            {
                search = [search substringToIndex:search_legsales];
            }
            
            NSString *substring = [NSString stringWithString:textField.text];
            substring = [substring stringByReplacingCharactersInRange:range withString:string];
            BOOL nameMatches = [[substring lowercaseString] hasPrefix:[string lowercaseString]];
            NSRange r = [search rangeOfString:substring options:NSCaseInsensitiveSearch];
            
            if(r.location != NSNotFound || nameMatches)
            {
                if (r.length > 0)
                {
                    [array_Sale_Summaryfilter addObject:dict];
                }
            }
            
        }
        
        [table_in_sales_view reloadData];
        
    }
    else if (textField ==txt_search_in_trackerview)
    {
        if(textField.text)
        {
            search_legsalestrack = range.location+1;
        }
        else
        {
            search_legsalestrack=0;
        }
        
        
        if (array_Sale_Trackerfilters)
        {
            [array_Sale_Trackerfilters removeAllObjects];
        }
        
        
        for (NSMutableDictionary *dict in  array_Sale_Tracker)
        {
            
            NSString *search = [dict valueForKey:@"title"];
            
            if ([search length]>search_legsalestrack)
            {
                search = [search substringToIndex:search_legsalestrack];
            }
            
            NSString *substring = [NSString stringWithString:textField.text];
            substring = [substring stringByReplacingCharactersInRange:range withString:string];
            BOOL nameMatches = [[substring lowercaseString] hasPrefix:[string lowercaseString]];
            NSRange r = [search rangeOfString:substring options:NSCaseInsensitiveSearch];
            
            if(r.location != NSNotFound || nameMatches)
            {
                if (r.length > 0)
                {
                    [array_Sale_Trackerfilters addObject:dict];
                }
            }
            
        }
        
        [table_in_sales_tracker reloadData];
        
    }
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == txt_search)
    {
        if ([textField.text isEqualToString:@""])
        {
            [ary_accountsfilter removeAllObjects];
            
            for (int i=0; i<[array_for_accounts count]; i++)
            {
                [ary_accountsfilter addObject:[array_for_accounts objectAtIndex:i]];
            }
            
        }
        
    }
    else if (textField ==txt_search_in_saleview)
    {
        if ([textField.text isEqualToString:@""])
        {
            [array_Sale_Summaryfilter removeAllObjects];
            
            for (int i=0; i<[array_Sale_Summary count]; i++)
            {
                [array_Sale_Summaryfilter addObject:[array_Sale_Summary objectAtIndex:i]];
            }
            
        }
    }
    else if (textField ==txt_search_in_trackerview)
    {
        if ([textField.text isEqualToString:@""])
        {
            [array_Sale_Trackerfilters removeAllObjects];
            
            for (int i=0; i<[array_Sale_Tracker count]; i++)
            {
                [array_Sale_Trackerfilters addObject:[array_Sale_Tracker objectAtIndex:i]];
            }
            
        }
    }
    
    else
    {
        if ([ary_accountsfilter count]==0)
        {
            
            //[SVProgressHUD showErrorWithStatus:@"No result found"];
            txt_search.text = @"";
        }
        else if ([array_Sale_Summary count]==0)
        {
            txt_search_in_saleview.text= @"";
            
        }
        else if ([array_Sale_Tracker count]==0)
        {
            txt_search_in_trackerview.text = @"";
        }
        
    }
    
    [table_for_ordrelist reloadData];
    [table_in_sales_view reloadData];
    [table_in_sales_tracker reloadData];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [table_for_ordrelist setHidden:NO];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    [txt_search resignFirstResponder];
    
    return YES;
}



#pragma mark servixcesfunctionality

-(void)account_sections
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    NSDictionary *params =@{
                            
                            
                            @"uid"            : [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER  kChefAccountSaleSummary
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kaccount_section
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpCuisineList:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self account_sections];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpCuisineList :(NSDictionary * )TheDict
{
    [array_for_accounts removeAllObjects];
    [array_total_Items removeAllObjects];
    [ary_accountsfilter removeAllObjects];
    
    
    NSLog(@"Response Dict: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"OrderData"] count]; i++)
        {
            [array_for_accounts addObject:[[TheDict valueForKey:@"OrderData"] objectAtIndex:i]];
            [ary_accountsfilter addObject:[[TheDict valueForKey:@"OrderData"] objectAtIndex:i]];
            
            
            
        }
        
        int int_GrandTotal = 0;
        for (int i=0; i<[ary_accountsfilter count]; i++)
        {
            int_GrandTotal = int_GrandTotal + [[[ary_accountsfilter objectAtIndex:i] valueForKey:@"SubTotal"] intValue];
        }
        
        lbl_doller_val.text = [NSString stringWithFormat:@"$ %i",int_GrandTotal];
        
        NSLog(@"array_for_accounts is: %@",ary_accountsfilter);
        
        for (int i = 0; i < [[[TheDict valueForKey:@"OrderData"]valueForKey:@"TotalitemsList"] count]; i++)
        {
            [array_total_Itemsfilter addObject:[[[ary_accountsfilter objectAtIndex:i]valueForKey:@"TotalitemsList"] objectAtIndex:i]];
        }
        NSLog(@"array_total_Items is: %@",array_total_Items);
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        NSLog(@"Error 1 check response");
        
    }
    
    [table_for_ordrelist reloadData];
    
    //    [self AlergyList];
}


-(void)AFSaleSummary
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    [formatter setDateFormat:@"yyyy-MM-dd"];
    // [txt_DateofBirth setText:[formatter stringFromDate:datePicker.date]];
    
    NSString*str_date1 =[formatter stringFromDate:datePicker3.date];
    NSString*str_date2 =[formatter stringFromDate:datePicker4.date];
    
    
    NSDictionary *params =@{
                            
                            
                            @"uid"            :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"from_date"      :  str_date1,
                            @"to_date"        :  str_date2,
                            @"sale_order"     :  str_order,
                            
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kChefAccountSaleSummary
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseSaleSummaryList:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFSaleSummary];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseSaleSummaryList :(NSDictionary * )TheDict
{
    
    [array_Sale_Summary removeAllObjects];
    [array_Sale_Summaryfilter removeAllObjects];
    
    
    NSLog(@"Response Dict: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"SaleArr"] count]; i++)
        {
            [array_Sale_Summary addObject:[[TheDict valueForKey:@"SaleArr"] objectAtIndex:i]];
            [array_Sale_Summaryfilter addObject:[[TheDict valueForKey:@"SaleArr"] objectAtIndex:i]];
            
        }
        
        NSLog(@"array_Sale_Summary is: %@",array_Sale_Summary);
        
        lbl_doller_val_in_saleview.text = [NSString stringWithFormat:@"$ %@",[TheDict valueForKey:@"totalSale"]];
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        NSLog(@"Error 1 check response");
        
    }
    
    [table_in_sales_view reloadData];
    
    //    [self AlergyList];
}


-(void)AFSaleTracker
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString*str_date1 =[formatter stringFromDate:datePicker5.date];
    NSString*str_date2 =[formatter stringFromDate:datePicker6.date];
    
    NSDictionary *params =@{
                            
                            
                            @"uid"            :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"from_date"      :  str_date1,
                            @"to_date"        :  str_date2,
                            @"price_order"     :  str_order,
                            @"search_text"     :  txt_search_in_trackerview.text,
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kChefSaleTracker
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseSaleTrackerList:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFSaleTracker];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseSaleTrackerList :(NSDictionary * )TheDict
{
    [array_Dish_Detail removeAllObjects];
    [array_Sale_Tracker removeAllObjects];
    [array_Sale_Trackerfilters removeAllObjects];
    
    
    NSLog(@"Response Dict: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"SaleArr"] count]; i++)
        {
            [array_Sale_Tracker addObject:[[TheDict valueForKey:@"SaleArr"] objectAtIndex:i]];
            [array_Sale_Trackerfilters addObject:[[TheDict valueForKey:@"SaleArr"] objectAtIndex:i]];
            
        }
        
        
        NSLog(@"array_Sale_Tracker is: %@",array_Sale_Trackerfilters);
        
        float int_GrandTotal1 = 0;
        int int_GrandTotal2 = 0;
        for (int i=0; i<[array_Sale_Trackerfilters count]; i++)
        {
            int_GrandTotal1 = int_GrandTotal1 + [[[array_Sale_Trackerfilters objectAtIndex:i] valueForKey:@"tot_sale"] floatValue];
            int_GrandTotal2 = int_GrandTotal2 + [[[array_Sale_Trackerfilters objectAtIndex:i] valueForKey:@"tot_saleCount"] intValue];
        }
        
        lbl_doller_val_in_trackerview.text = [NSString stringWithFormat:@"$ %f",int_GrandTotal1];
        lbl_sold_val.text = [NSString stringWithFormat:@"%i Sold",int_GrandTotal2];
        
        
        //        for (int i = 0; i < [array_Sale_Tracker count]; i++)
        //        {
        //
        //            int total = (int)[[[array_Sale_Tracker objectAtIndex:i]valueForKey:@"DishDetail"] count];
        //
        //            for (int j=0; j<total; j++)
        //            {
        //            [array_Dish_Detail addObject:[[[[TheDict valueForKey:@"SaleArr"] objectAtIndex:j]valueForKey:@"DishDetail"] objectAtIndex:i]];
        //
        //            }
        //        }
        //
        //        NSLog(@"array_Dish_Detail is: %@",array_Dish_Detail);
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        NSLog(@"Error 1 check response");
        
    }
    
    [table_in_sales_tracker reloadData];
    
    //    [self AlergyList];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
