//
//  ChefServeNowVC.m
//  Not86
//
//  Created by Pavan on 25/05/2015 SAKA.
//  Copyright (c) 1937 Saka com.interworld. All rights reserved.
//

#import "ChefServeNowVC.h"
#import "ChefServeTypeVC.h"
//#import "tableDishesCell.h"

#import "AppDelegate.h"



@interface ChefServeNowVC ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIScrollViewDelegate,UISearchBarDelegate>
{
    UIImageView *imgViewTop;
    UITableView *tableViewDishes;
    NSMutableArray *arryFoodnames;
    //    NSArray *arryFoodImages;
    AppDelegate *delegate;
    
    UIButton *btn_EndTime;
    UIButton *btn_Search;
    UILabel  * labl_Timer;
    UITextField *txt_EndTime;
    UIToolbar *keyboardToolbar_Date;
    NSDateFormatter *formatter;
    UIDatePicker  *datePicker;
    
    //    UIButton *btnMinus;
    //    UIButton *btnPlus;
    int minimum_Quantity;
    //     NSString *str_newQuantity;
    //    UILabel *labl;
    
    NSMutableArray *Arr_Temp;
    int selectedindex;
    
    NSMutableDictionary *tempDictionary;
    NSMutableArray*ary_servelist;
    NSIndexPath*indexSelected;
    UITextField *textfield_SearchDish;
    NSMutableArray*ary_servelistfilter;
    int search_leg;
    UIImageView *imgViewSerchdish;
    UIImageView *img_Dish;
    
    UIView*alertviewBg;
    
    
}

@end

@implementation ChefServeNowVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    ary_servelist = [[NSMutableArray alloc]init];
    
    ary_servelistfilter = [[NSMutableArray alloc]init];
    
    
    arryFoodnames = [[NSMutableArray alloc]initWithObjects:@"Chilli Chicken",@"Spicy Nugget",@"Curry Chicken",@"Fish Otah",@"Peach Tea",@"Cold Coffee",@"Hamburger", nil];
    
    Arr_Temp = [NSMutableArray new];
    //    arryFoodImages = [[NSArray alloc]initWithObjects:@"chickedn.png",@"fish.png",@"fish.png",@"chickedn.png",@"chickedn.png", nil];
    //
    selectedindex = -1;
    indexSelected = nil;
    
    tempDictionary = [[NSMutableDictionary alloc] init];
    
    [self customHeaderViewInitilization];
    [self customInitilization];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    //[self AFChefServeNow];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self AFServeNowlist];
    
}

-(void)customHeaderViewInitilization
{
    imgViewTop=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,45)];
    [imgViewTop setUserInteractionEnabled:YES];
    imgViewTop.image=[UIImage imageNamed:@"img-head.png"];
    [self.view addSubview:imgViewTop];
    //
    //        UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(10, 18, 25, 25)];
    //        [img_back setUserInteractionEnabled:YES];
    //        img_back.backgroundColor=[UIColor clearColor];
    //        img_back.image=[UIImage imageNamed:@"arrow.png"];
    //        [imgViewTop addSubview:img_back];
    //
    
    
    UIButton *btn_back = [[UIButton alloc] init];
    btn_back.frame = CGRectMake(15, 15, 15,17);
    UIImage *btnimage=[UIImage imageNamed:@"arrow@2x.png"];
    [btn_back setBackgroundImage:btnimage forState:UIControlStateNormal];
    btn_back.backgroundColor = [UIColor clearColor];
    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [imgViewTop addSubview:btn_back];
    
    
    
    UILabel  * lbl_headingTitle = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(btn_back.frame)+25,15, 100,17)];
    lbl_headingTitle.text = @"Serve Now";
    lbl_headingTitle.backgroundColor=[UIColor clearColor];
    lbl_headingTitle.textColor=[UIColor whiteColor];
    lbl_headingTitle.textAlignment=NSTextAlignmentLeft;
    lbl_headingTitle.font = [UIFont fontWithName:kFont size:17];
    [imgViewTop addSubview:lbl_headingTitle];
    
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 5, 35, 35)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-logo.png"];
    [imgViewTop addSubview:img_logo];
    
}

-(void)customInitilization
{
    //    [self.view setBackgroundColor:[UIColor colorWithRed:245/255.0f green:247/255.0f blue:246/255.0f alpha:1.0f]];
    
    [self.view setBackgroundColor:[UIColor colorWithRed:243/255.0f green:245/255.0f blue:244/255.0f alpha:1.0f]];
    
    UILabel  * labl_serveNowDate = [[UILabel alloc]init ];
    //    labl_serveNowDate.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,25);
    if (IS_IPHONE_6Plus)
    {
        //        labl_serveNowDate.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,25);
        labl_serveNowDate.frame=CGRectMake(0,imgViewTop.frame.size.height+20, self.view.frame.size.width,32);
    }
    else if (IS_IPHONE_6)
    {
        labl_serveNowDate.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,30);
    }
    else if (IS_IPHONE_5)
    {
        labl_serveNowDate.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,25);
    }
    else
    {
        labl_serveNowDate.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,20);
    }
    
    NSDate *currentDate = [[NSDate alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd/MM/yyyy";
    NSString *string = [dateFormatter stringFromDate:currentDate];
    
    //NSString *string = [NSString stringWithFormat:@"%ld.%ld.%ld", (long)day, (long)week, (long)year];
    
    //  labl_serveNowDate.text = @"Serve Now: 15/07/2015";
    //    labl_serveNowDate.textColor=[UIColor grayColor];
    labl_serveNowDate.font = [UIFont fontWithName:kFont size:16];
    labl_serveNowDate.text =[NSString stringWithFormat:@"Serve Now: %@",string];
    labl_serveNowDate.backgroundColor=[UIColor clearColor];
    labl_serveNowDate.textAlignment=NSTextAlignmentCenter;
    
    
    NSRange range1 = [labl_serveNowDate.text rangeOfString:@"Serve Now:"];
    NSRange range2 = [labl_serveNowDate.text rangeOfString:[NSString stringWithFormat:@" %@",string]];
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:labl_serveNowDate.text];
    
    [attributedText setAttributes:@{NSFontAttributeName:[UIFont fontWithName:kFont size:15]}
                            range:range1];
    [attributedText setAttributes:@{NSFontAttributeName:[UIFont fontWithName:kFontBold size:15]}
                            range:range2];
    
    labl_serveNowDate.attributedText = attributedText;
    
    
   // labl_serveNowDate.font = [UIFont fontWithName:kFontBold size:15];
    [self.view addSubview:labl_serveNowDate];
    
    
    UILabel  * labl_SelectServeTime = [[UILabel alloc]init];
    //     labl_SelectServeTime.frame=CGRectMake(0,CGRectGetMaxY(labl_serveNowDate.frame)+5, self.view.frame.size.width,25);
    if (IS_IPHONE_6Plus)
    {
        //        labl_SelectServeTime.frame=CGRectMake(0,CGRectGetMaxY(labl_serveNowDate.frame)+5, self.view.frame.size.width,25);
        labl_SelectServeTime.frame=CGRectMake(0,CGRectGetMaxY(labl_serveNowDate.frame)+5, self.view.frame.size.width,32);
    }
    else if (IS_IPHONE_6)
    {
        labl_SelectServeTime.frame=CGRectMake(0,CGRectGetMaxY(labl_serveNowDate.frame)+5, self.view.frame.size.width,30);
    }
    else if (IS_IPHONE_5)
    {
        labl_SelectServeTime.frame=CGRectMake(0,CGRectGetMaxY(labl_serveNowDate.frame)+5, self.view.frame.size.width,25);
    }
    else
    {
        labl_SelectServeTime.frame=CGRectMake(0,CGRectGetMaxY(labl_serveNowDate.frame)+5, self.view.frame.size.width,20);
    }
    labl_SelectServeTime.text = @"Select your serving end time";
    labl_SelectServeTime.textColor=[UIColor darkTextColor];
    labl_SelectServeTime.textAlignment = NSTextAlignmentCenter;
    labl_SelectServeTime.font = [UIFont fontWithName:kFontBold size:15];
    [self.view addSubview:labl_SelectServeTime];
    
    
    UIImageView *imgViewTimer=[[UIImageView alloc]init];
    //    imgViewTimer.frame=CGRectMake(0, CGRectGetMaxY(labl_SelectServeTime.frame)+5,WIDTH+10,50);
    if (IS_IPHONE_6Plus)
    {
        //      imgViewTimer.frame=CGRectMake(0, CGRectGetMaxY(labl_SelectServeTime.frame)+5,WIDTH+10,50);
        imgViewTimer.frame=CGRectMake(0, CGRectGetMaxY(labl_SelectServeTime.frame)+5,WIDTH+10,55);
    }
    else if (IS_IPHONE_6)
    {
        imgViewTimer.frame=CGRectMake(0, CGRectGetMaxY(labl_SelectServeTime.frame)+5,WIDTH+10,55);
    }
    else if (IS_IPHONE_5)
    {
        imgViewTimer.frame=CGRectMake(0, CGRectGetMaxY(labl_SelectServeTime.frame)+5,WIDTH+10,50);
    }
    else
    {
        imgViewTimer.frame=CGRectMake(0, CGRectGetMaxY(labl_SelectServeTime.frame)+5,WIDTH+10,45);
    }
    [imgViewTimer setUserInteractionEnabled:YES];
    imgViewTimer.image=[UIImage imageNamed:@"img_background@2x..png"];
    //    [imgViewTimer setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
    [self.view addSubview:imgViewTimer];
    
    
    UIImageView *timerImage1=[[UIImageView alloc]init ];
    //     timerImage1.frame=CGRectMake(20, 15,20,20);
    if (IS_IPHONE_6Plus)
    {
        //       timerImage1.frame=CGRectMake(20, 15,20,20);
        timerImage1.frame=CGRectMake(20, 15,25,25);
    }
    else if (IS_IPHONE_6)
    {
        timerImage1.frame=CGRectMake(20, 15,25,25);
    }
    else if (IS_IPHONE_5)
    {
        timerImage1.frame=CGRectMake(20, 15,20,20);
    }
    else
    {
        timerImage1.frame=CGRectMake(20, 13,20,20);
    }
    [timerImage1 setUserInteractionEnabled:YES];
    [timerImage1 setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
    timerImage1.image = [UIImage imageNamed:@"icon-time.png"];
    [imgViewTimer addSubview:timerImage1];
    
    
    
    UIImageView *timerImage2=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus) {
        
        //        timerImage2.frame =CGRectMake(self.view.frame.size.width-170, 15,20,20);
        timerImage2.frame = CGRectMake(self.view.frame.size.width-190, 15,25,25);
    }else if (IS_IPHONE_6)
    {
        timerImage2.frame = CGRectMake(self.view.frame.size.width-170, 15,25,25);
    }
    else if (IS_IPHONE_5)
    {
        timerImage2.frame = CGRectMake(self.view.frame.size.width-140, 15,20,20);
    }
    else
    {
        timerImage2.frame = CGRectMake(self.view.frame.size.width-140, 13,20,20);
    }
    
    [timerImage2 setUserInteractionEnabled:YES];
    //    [timerImage2 setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
    timerImage2.image = [UIImage imageNamed:@"icon-time.png"];
    [imgViewTimer addSubview:timerImage2];
    
    
    btn_EndTime = [[UIButton alloc]init];
    if (IS_IPHONE_6Plus) {
        
        //                btn_EndTime.frame = CGRectMake(self.view.frame.size.width-65, 15, 20, 20);
        btn_EndTime.frame = CGRectMake( 0, 0, 25, 25);
        
    }else if (IS_IPHONE_6)
    {
        btn_EndTime.frame = CGRectMake(0, 0,25,25);
    }
    else if (IS_IPHONE_5)
    {
        btn_EndTime.frame = CGRectMake( 0, 0, 20, 20);
    }
    else
    {
        btn_EndTime.frame = CGRectMake(0, 0,20,20);
    }
    [btn_EndTime addTarget:self action:@selector(EndTimeMethod:) forControlEvents:UIControlEventTouchUpInside];
    btn_EndTime.backgroundColor = [UIColor clearColor];
    [timerImage2 addSubview:btn_EndTime];
    
    
    UILabel  * labl_Now = [[UILabel alloc]init];
    if (IS_IPHONE_6Plus) {
        
        //        labl_Now.frame = CGRectMake(60,10, 50,30);
        labl_Now.frame = CGRectMake(60,12, 50,30);
    }else if (IS_IPHONE_6)
    {
        labl_Now.frame = CGRectMake(60,12, 50,30);
    }
    else if (IS_IPHONE_5)
    {
        labl_Now.frame = CGRectMake(60,10, 50,30);
    }
    else
    {
        labl_Now.frame = CGRectMake(60,7, 50,30);
    }
    
    labl_Now.text = @"Now";
    labl_Now.textColor=[UIColor darkTextColor];
    labl_Now.textAlignment = NSTextAlignmentCenter;
    labl_Now.font = [UIFont fontWithName:kFont size:15];
    [imgViewTimer addSubview:labl_Now];
    
    
    UILabel  * labl_NowTo = [[UILabel alloc]init];
    if (IS_IPHONE_6Plus) {
        
        //        labl_NowTo.frame = CGRectMake(90,10, 200,30);
        labl_NowTo.frame = CGRectMake(65,12, 200,30);
    }else if (IS_IPHONE_6)
    {
        labl_NowTo.frame = CGRectMake(45,12, 200,30);
    }
    else if (IS_IPHONE_5)
    {
        labl_NowTo.frame = CGRectMake(40,10, 200,30);
    }
    else{
        labl_NowTo.frame = CGRectMake(40,7, 200,30);
    }
    labl_NowTo.text = @"to";
    labl_NowTo.backgroundColor = [UIColor clearColor];
    labl_NowTo.textColor=[UIColor darkTextColor];
    labl_NowTo.textAlignment = NSTextAlignmentCenter;
    labl_NowTo.font = [UIFont fontWithName:kFont size:15];
    [imgViewTimer addSubview:labl_NowTo];
    
    
    //    labl_Timer = [[UILabel alloc]init];
    //    if (IS_IPHONE_6Plus) {
    //
    ////        labl_Timer.frame = CGRectMake(self.view.frame.size.width-130, 10, 100,30);
    //        labl_Timer.frame = CGRectMake(self.view.frame.size.width-150, 12, 100,30);
    //    }else if (IS_IPHONE_6)
    //    {
    //        labl_Timer.frame = CGRectMake(self.view.frame.size.width-130, 12, 100,30);
    //    }
    //    else if (IS_IPHONE_5)
    //    {
    //        labl_Timer.frame = CGRectMake(self.view.frame.size.width-110, 10, 100,30);
    //    }
    //    else
    //    {
    //        labl_Timer.frame = CGRectMake(self.view.frame.size.width-110, 7, 100,30);
    //    }
    //    labl_Timer.text = @"1:45 PM";
    //    labl_Timer.textColor=[UIColor darkTextColor];
    //    labl_Timer.textAlignment = NSTextAlignmentCenter;
    //    labl_Timer.font = [UIFont fontWithName:kFont size:15];
    //    [imgViewTimer addSubview:labl_Timer];
    
    
    txt_EndTime = [[UITextField alloc] init];
    if (IS_IPHONE_6Plus) {
        
        //        labl_Timer.frame = CGRectMake(self.view.frame.size.width-130, 10, 100,30);
        txt_EndTime.frame = CGRectMake(self.view.frame.size.width-150, 12, 100,30);
    }else if (IS_IPHONE_6)
    {
        txt_EndTime.frame = CGRectMake(self.view.frame.size.width-130, 12, 100,30);
    }
    else if (IS_IPHONE_5)
    {
        txt_EndTime.frame = CGRectMake(self.view.frame.size.width-110, 10, 100,30);
    }
    else
    {
        txt_EndTime.frame = CGRectMake(self.view.frame.size.width-110, 7, 100,30);
    }
    txt_EndTime.borderStyle = UITextBorderStyleNone;
    //    txt_EndTime.textColor = [UIColor grayColor];
    txt_EndTime.placeholder = @"1:45 PM";
    //    txt_EndTime.placeholder = @"d   d      m  m      y    y    y    y";
    txt_EndTime.font = [UIFont fontWithName:kFont size:15];
    txt_EndTime.leftViewMode = UITextFieldViewModeAlways;
    txt_EndTime.userInteractionEnabled=YES;
    txt_EndTime.textAlignment = NSTextAlignmentLeft;
    txt_EndTime.backgroundColor = [UIColor clearColor];
    txt_EndTime.keyboardType = UIKeyboardTypeAlphabet;
    txt_EndTime.delegate = self;
    txt_EndTime.textColor =[UIColor blackColor];
    [imgViewTimer addSubview:txt_EndTime];
    if (keyboardToolbar_Date == nil)
    {
        keyboardToolbar_Date = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar_Date setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_DoneDate:)];
        [keyboardToolbar_Date setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    txt_EndTime.inputAccessoryView = keyboardToolbar_Date;
    txt_EndTime.backgroundColor=[UIColor clearColor];
    
    
    formatter = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setDateFormat:@"hh:mm aa"];
    //datePicker.minimumDate = [formatter dateFromString:@"1900-01-01"];
    
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeTime;
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    //[datePicker setDate:[NSDate date]];
    txt_EndTime.inputView = datePicker;
    
    NSDate *now1 = [NSDate date];
    NSCalendar *calendar2 = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components1 = [calendar2 components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now1];
    components1.year = components1.year  ;
    
    datePicker.minimumDate = now1;
    
    
    UILabel  * labl_Dishes = [[UILabel alloc]init ];
    //     labl_Dishes.frame=CGRectMake(0,CGRectGetMaxY(imgViewTimer.frame)+5, self.view.frame.size.width,25);
    if (IS_IPHONE_6Plus)
    {
        //labl_Dishes.frame=CGRectMake(0,CGRectGetMaxY(imgViewTimer.frame)+5, self.view.frame.size.width,25);
        labl_Dishes.frame=CGRectMake(0,CGRectGetMaxY(imgViewTimer.frame)+5, self.view.frame.size.width,35);
    }
    else if (IS_IPHONE_6)
    {
        labl_Dishes.frame=CGRectMake(0,CGRectGetMaxY(imgViewTimer.frame)+5, self.view.frame.size.width,30);
    }
    else if (IS_IPHONE_5)
    {
        labl_Dishes.frame=CGRectMake(0,CGRectGetMaxY(imgViewTimer.frame)+5, self.view.frame.size.width,25);
    }
    else
    {
        labl_Dishes.frame=CGRectMake(0,CGRectGetMaxY(imgViewTimer.frame)+5, self.view.frame.size.width,20);
    }
    labl_Dishes.text = @"Select your dishes to serve";
    labl_Dishes.textColor=[UIColor darkTextColor];
    labl_Dishes.textAlignment = NSTextAlignmentCenter;
    labl_Dishes.font = [UIFont fontWithName:kFontBold size:15];
    [self.view addSubview:labl_Dishes];
    
    
    imgViewSerchdish=[[UIImageView alloc]init];
    //    imgViewSerchdish.frame=CGRectMake(0, CGRectGetMaxY(labl_Dishes.frame)+5,WIDTH+10,50);
    if (IS_IPHONE_6Plus)
    {
        //         imgViewSerchdish.frame=CGRectMake(0, CGRectGetMaxY(labl_Dishes.frame)+5,WIDTH+10,50);
        imgViewSerchdish.frame=CGRectMake(0, CGRectGetMaxY(labl_Dishes.frame)+5,WIDTH+10,55);
    }
    else if (IS_IPHONE_6)
    {
        imgViewSerchdish.frame=CGRectMake(0, CGRectGetMaxY(labl_Dishes.frame)+5,WIDTH+10,55);
    }
    else if (IS_IPHONE_5)
    {
        imgViewSerchdish.frame=CGRectMake(0, CGRectGetMaxY(labl_Dishes.frame)+5,WIDTH+10,50);
    }
    else
    {
        imgViewSerchdish.frame=CGRectMake(0, CGRectGetMaxY(labl_Dishes.frame)+5,WIDTH+10,45);
    }
    [imgViewSerchdish setUserInteractionEnabled:YES];
    imgViewSerchdish.image=[UIImage imageNamed:@"img_background@2x..png"];
    [self.view addSubview:imgViewSerchdish];
    
    
    
    img_Dish=[[UIImageView alloc]init];
    //     img_Dish.frame=CGRectMake(20, 15,32,20);
    if (IS_IPHONE_6Plus)
    {
        //        img_Dish.frame=CGRectMake(20, 15,32,20);
        img_Dish.frame=CGRectMake(20, 15,35,25);
    }
    else if (IS_IPHONE_6)
    {
        img_Dish.frame=CGRectMake(20, 15,35,25);
    }
    else if (IS_IPHONE_5)
    {
        img_Dish.frame=CGRectMake(20, 15,32,20);
    }
    else
    {
        img_Dish.frame=CGRectMake(20, 13,32,20);
    }
    [img_Dish setUserInteractionEnabled:YES];
    //[img_Dish setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
    img_Dish.image = [UIImage imageNamed:@"img_dishplate.png"];
    [imgViewSerchdish addSubview:img_Dish];
    
    
    btn_Search = [[UIButton alloc]init];
    if (IS_IPHONE_6Plus) {
        
        //        btn_Search.frame = CGRectMake(self.view.frame.size.width-65, 15, 20, 20);
        btn_Search.frame = CGRectMake(self.view.frame.size.width-65, 15, 25, 25);
        
    }else if (IS_IPHONE_6)
    {
        btn_Search.frame = CGRectMake(self.view.frame.size.width-60, 15, 25, 25);
    }
    else if (IS_IPHONE_5)
    {
        btn_Search.frame = CGRectMake(self.view.frame.size.width-55, 15, 20, 20);
    }
    else
    {
        btn_Search.frame = CGRectMake(self.view.frame.size.width-55, 13, 20, 20);
    }
    [btn_Search addTarget:self action:@selector(Search_Method:) forControlEvents:UIControlEventTouchUpInside];
    [btn_Search setBackgroundImage:[UIImage imageNamed:@"img_search.png"] forState:UIControlStateNormal];
    [imgViewSerchdish addSubview:btn_Search];
    
    
    
    textfield_SearchDish = [[UITextField alloc]init];
    //    textfield_SearchDish.frame=CGRectMake(65, 10, self.view.frame.size.width-140, 35);
    if (IS_IPHONE_6Plus)
    {
        //        textfield_SearchDish.frame=CGRectMake(65, 10, self.view.frame.size.width-140, 35);
        textfield_SearchDish.frame=CGRectMake(70, 10, self.view.frame.size.width-150, 35);
    }
    else if (IS_IPHONE_6)
    {
        textfield_SearchDish.frame=CGRectMake(70, 10, self.view.frame.size.width-150, 35);
    }
    else if (IS_IPHONE_5)
    {
        textfield_SearchDish.frame=CGRectMake(65, 10, self.view.frame.size.width-140, 30);
    }
    else
    {
        textfield_SearchDish.frame=CGRectMake(65, 7, self.view.frame.size.width-140, 30);
    }
    textfield_SearchDish.placeholder = @"Search Dish...";
    textfield_SearchDish.leftViewMode = UITextFieldViewModeAlways;
    textfield_SearchDish.userInteractionEnabled=YES;
    textfield_SearchDish.textAlignment = NSTextAlignmentLeft;
    textfield_SearchDish.backgroundColor = [UIColor clearColor];
    textfield_SearchDish.keyboardType = UIKeyboardTypeAlphabet;
    textfield_SearchDish.delegate = self;
    
    textfield_SearchDish.font=[UIFont fontWithName:kFont size:13];
    [imgViewSerchdish addSubview:textfield_SearchDish];
    
    imgViewSerchdish.hidden= YES;
    img_Dish.hidden = YES;
    btn_Search.hidden = YES;
    textfield_SearchDish.hidden= YES;
    
    
    UIImageView *img_backgroundimage=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus) {
        //        img_backgroundimage.frame=CGRectMake(10,CGRectGetMaxY(imgViewSerchdish.frame)+5, WIDTH-20,250);
        img_backgroundimage.frame=CGRectMake(20,CGRectGetMaxY(imgViewSerchdish.frame)+20, WIDTH-40,280);
        
    }else if (IS_IPHONE_6)
        
    {
        img_backgroundimage.frame=CGRectMake(15,CGRectGetMaxY(imgViewSerchdish.frame)+15, WIDTH-30,260);
        
    }
    else if (IS_IPHONE_5)
    {
        img_backgroundimage.frame=CGRectMake(10,CGRectGetMaxY(imgViewSerchdish.frame)+5, WIDTH-20,225);
        
    }
    else
    {
        img_backgroundimage.frame=CGRectMake(10,CGRectGetMaxY(imgViewSerchdish.frame)+5, WIDTH-20,180);
    }
    [img_backgroundimage setUserInteractionEnabled:YES];
    //    img_backgroundimage.backgroundColor=[UIColor clearColor];
    [img_backgroundimage setContentMode:UIViewContentModeScaleAspectFill];
    img_backgroundimage.clipsToBounds = YES;
    img_backgroundimage.image=[UIImage imageNamed:@"bg1-img@2x"];
    [self.view addSubview:img_backgroundimage];
    
    
    
    
    //    arryFoodImages = [[NSArray alloc]initWithObjects:@"img_fork.png",@"img_forkblue.png",@"img_forkblue.png",@"img_fork.png",@"img_fork.png", nil];
    
    tableViewDishes = [[UITableView alloc]init];
    
    if (IS_IPHONE_6Plus) {
        //        tableViewDishes.frame =CGRectMake(0,0, WIDTH-20,250);
        tableViewDishes.frame =CGRectMake(0,0, WIDTH-40,279);
        tableViewDishes.rowHeight = 56.0;
    }
    else if (IS_IPHONE_6)
    {
        tableViewDishes.frame =CGRectMake(0,0, WIDTH-30,259);
        tableViewDishes.rowHeight = 52.0;
    }
    else if (IS_IPHONE_5)
    {
        tableViewDishes.frame =CGRectMake(0,0, WIDTH-20,224);
        tableViewDishes.rowHeight = 45.0;
    }
    else
    {
        tableViewDishes.frame =CGRectMake(0,0, WIDTH-20,179);
        tableViewDishes.rowHeight = 45.0;
    }
    [tableViewDishes setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tableViewDishes.delegate = self;
    tableViewDishes.dataSource = self;
    tableViewDishes.showsVerticalScrollIndicator = NO;
    tableViewDishes.allowsMultipleSelection = YES;
    //    tableViewDishes.layer.borderWidth = 1.0;
    [img_backgroundimage addSubview:tableViewDishes];
    
    
    UIButton *next = [[UIButton alloc] init];
    if (IS_IPHONE_6Plus) {
        //        next.frame = CGRectMake(40, self.view.frame.size.height-50, self.view.frame.size.width-80, 45);
        next.frame = CGRectMake(40, self.view.frame.size.height-65, self.view.frame.size.width-80, 50);
        
    }else if (IS_IPHONE_6)
        
    {
        next.frame = CGRectMake(40, self.view.frame.size.height-60, self.view.frame.size.width-80, 45);
        
    }
    else if (IS_IPHONE_5)
    {
        next.frame = CGRectMake(40, self.view.frame.size.height-50, self.view.frame.size.width-80, 40);
    }
    else
    {
        next.frame = CGRectMake(40, self.view.frame.size.height-40, self.view.frame.size.width-80, 35);
    }
    next.backgroundColor = [UIColor brownColor];
    next.layer.cornerRadius=4.0f;
    [next setTitle:@"Next" forState:UIControlStateNormal];
    [next setImage:[UIImage imageNamed:@"text buuton.png"] forState:UIControlStateNormal];
    [next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [next addTarget:self action:@selector(next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [self.view addSubview:next];
    
}

#pragma mark ---ButtonSelectorMethod

-(void) click_DoneDate:(id) sender
{
    [formatter setDateFormat:@"hh:mm aa"];
    //[formatter setAMSymbol:@"AM"];
   // [formatter setPMSymbol:@"PM"];
    [formatter setTimeStyle: NSDateFormatterShortStyle];
    
    NSString *str=[formatter stringFromDate:datePicker.date];
    [txt_EndTime setText:str];
    [txt_EndTime resignFirstResponder];
    //    keyboardToolbar_Date.hidden = YES;
    //    datePicker.hidden = YES;
//#pragma new code
//    
//    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
//    [formatter1 setDateFormat:@"hh:mm aa"];
//    NSString *nowtime=[formatter1 stringFromDate:[NSDate date]];
//    NSDate *date2= [formatter1 dateFromString:txt_EndTime.text];
//    NSDate *date1 = [formatter1 dateFromString:nowtime];
//    
//    NSComparisonResult result = [date1 compare:date2];
//    if(result == NSOrderedDescending)
//    {
//        NSLog(@"date2 is earlier than date1");
//        [self popup_Alertview:@"please select end time later than now"];
//        txt_EndTime.text=@"";
//        
//    }
    
    
}

- (void)datePickerValueChanged:(id)sender
{
    [formatter setDateFormat:@"hh:mm aa"];
    NSString *str=[formatter stringFromDate:datePicker.date];
    [txt_EndTime setText:str];

}

-(void)Search_Method: (UIButton *)sender
{
    NSLog(@"Button Search_Method: Selected ");
    
    
}

-(void)EndTimeMethod: (UIButton *)sender
{
    NSLog(@"Button EndTime Selected");
}

-(void)click_BtnPlusnow: (UIButton *)sender
{
    //    int minimum_Quantity = 1;
    NSLog(@"Button Plus Selected");
    //    if (minimum_Quantity<20)
    //    {
    //        tableDishesCell *cell;
    //        cell.labl.text = [NSString stringWithFormat:@"%d",[cell.labl.text intValue]+1];
    //        str_newQuantity = [NSString stringWithFormat:@"%i", minimum_Quantity];
    ////        [tableViewDishes reloadData];
    //    }
    //    else{
    //        tableDishesCell *cell;
    //        cell.labl.text = [NSString stringWithFormat:@"%d",[cell.labl.text intValue]];
    //    str_newQuantity = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //     }
    
    //    int i = 1;
    //    if (i < 20)
    //    {
    //        i = i+1;
    //        labl.text = [NSString stringWithFormat:@"%i", i];
    //    }
    //    else
    //    {
    //        labl.text = [NSString stringWithFormat:@"%i", i];
    //    }
    selectedindex = [sender tag];
    
    //    for (int i = 0; i< [Arr_Temp count]; i++)
    //    {
    //        if (minimum_Quantity<20)
    //        {
    //            minimum_Quantity = minimum_Quantity + 1;
    //            labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //        }
    //        else
    //        {
    //            labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //        }
    //
    //    }
    //    if (minimum_Quantity<20)
    //    {
    //        minimum_Quantity = minimum_Quantity + 1;
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //    }
    //    else
    //    {
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //    }
    
    
    //    NSString *strLikes = @"1";
    //    int likeCount = [strLikes intValue] + 1;
    //    [_marrTest replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
    //
    //    NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:btnClicked.tag-1 inSection:0];
    //
    //    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:selectedIndexPath];
    //
    //    UILabel *requiredLabel = (UILabel*)[cell viewWithTag:1000];
    //
    //    NSString *str = requiredLabel.text;
    //
    //    //str = [str stringByAppendingFormat:@"selected %@", str];
    //    requiredLabel.text = @"";
    //    requiredLabel.text = [NSString stringWithFormat:@"%d likes",[[_marrTest objectAtIndex:btnClicked.tag-1] intValue]];
    //
    //    //do what ever you want with the label
    //
    //    if (minimum_Quantity<20)
    //    {
    //        minimum_Quantity = minimum_Quantity + 1;
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //    }
    //    else
    //    {
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //    }
    
    NSMutableArray*dummy;
    dummy = [NSMutableArray new];
    
    
    int tag=(int)[[sender superview] tag];
    NSString* selectedid =[[ary_servelistfilter objectAtIndex:tag]valueForKey:@"DishID"];
    
    for (int i=0; i<[ary_servelistfilter count]; i++)
    {
        if ([selectedid isEqualToString:[[ary_servelistfilter objectAtIndex:i]valueForKey:@"DishID"]])
        {
            //            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
            //
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"Description"]]  forKey:@"Description"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"ChefCategory"]]  forKey:@"ChefCategory"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishCategoryID"]]  forKey:@"DishCategoryID"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishCuisineID"]]  forKey:@"DishCuisineID"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishPrice"]]  forKey:@"DishPrice"];
            //
            //
            
            
            //            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
            ////            [dummy addObject:[[ary_servelist objectAtIndex:i] valueForKey:@"Serving_Type"]];
            ////            [dict setValue:dummy forKey:@"Serving_Type"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
            //
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
            //
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
            //            NSString *str_facycount=[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"quantity"]];
            //            int int_fancy=[str_facycount integerValue];
            //            int_fancy=int_fancy+1;
            //            [dict setValue:[NSString stringWithFormat:@"%d", int_fancy]  forKey:@"quantity"];
            //
            //
            //
            //
            //            [ary_servelist replaceObjectAtIndex:i withObject:dict];
            
            NSMutableDictionary *dict1=[[NSMutableDictionary alloc] init];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
            //            [dummy addObject:[[ary_servelist objectAtIndex:i] valueForKey:@"Serving_Type"]];
            //            [dict setValue:dummy forKey:@"Serving_Type"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
            NSString *str_facycount1=[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"quantity"]];
            int int_fancy1=[str_facycount1 integerValue];
            int_fancy1=int_fancy1+1;
            [dict1 setValue:[NSString stringWithFormat:@"%d", int_fancy1]  forKey:@"quantity"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Delivery_Address"]] forKey:@"Delivery_Address"];
            [ary_servelistfilter replaceObjectAtIndex:i withObject:dict1];
            
            
#pragma replacing temp array also to match
            
            [Arr_Temp replaceObjectAtIndex:i withObject:[ary_servelistfilter objectAtIndex:i]];
            
            
        }
    }
    
    //    int value=(int)[[[ary_servelist objectAtIndex:tag] valueForKey:@"quantity"] integerValue]+1;
    //    [ary_servelist replaceObjectAtIndex:tag withObject:[@(value) stringValue]];
    [tableViewDishes reloadData];
    
}
-(void)click_BtnMinusnow: (UIButton *)sender
{
    //    int minimum_Quantity = 1;
    NSLog(@"Button Minus Selected ");
    //    if (minimum_Quantity>1)
    //    {
    //        tableDishesCell *cell;
    //        cell.labl.text = [NSString stringWithFormat:@"%d",[cell.labl.text intValue]-1];
    //        str_newQuantity = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //                [tableViewDishes reloadData];
    //    }
    //    else
    //    {
    //        tableDishesCell *cell;
    //    cell.labl.text = [NSString stringWithFormat:@"%d",[cell.labl.text intValue]];
    //        str_newQuantity = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //    }
    
    //    int i = 1;
    //    if (i > 1)
    //    {
    //        i = i-1;
    //        labl.text = [NSString stringWithFormat:@"%i", i];
    //    }
    //    else
    //    {
    //        labl.text = [NSString stringWithFormat:@"%i", i];
    //    }
    //    selectedindex = [sender tag];
    //    if (minimum_Quantity>1)
    //    {
    //        minimum_Quantity = minimum_Quantity - 1;
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //    }
    //    else
    //    {
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //    }
    
    
    int tag=(int)[[sender superview] tag];
    //    int value=(int)[[[ary_servelist objectAtIndex:tag] valueForKey:@"quantity"] integerValue]-1;
    //    if (value <= 0) value = 1;
    //    [ary_servelist replaceObjectAtIndex:tag withObject:[@(value) stringValue]];
    
    NSMutableArray*dummy;
    dummy = [NSMutableArray new];
    
    
    NSString* selectedid =[[ary_servelistfilter objectAtIndex:tag]valueForKey:@"DishID"];
    
    for (int i=0; i<[ary_servelistfilter count]; i++)
    {
        if ([selectedid isEqualToString:[[ary_servelistfilter objectAtIndex:i]valueForKey:@"DishID"]])
        {
            //            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
            ////            [dummy addObject:[[ary_servelist objectAtIndex:i] valueForKey:@"Serving_Type"]];
            ////            [dict setValue:dummy forKey:@"Serving_Type"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
            //
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
            //
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
            //            [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
            //            NSString *str_facycount=[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"quantity"]];
            //            int int_fancy=[str_facycount integerValue];
            //            int_fancy=int_fancy-1;
            //
            //            if (int_fancy<0)
            //            {
            //                [dict setValue:@"0"  forKey:@"quantity"];
            //
            //            }
            //            else{
            //                [dict setValue:[NSString stringWithFormat:@"%d", int_fancy]  forKey:@"quantity"];
            //
            //            }
            //
            //
            //            [ary_servelist replaceObjectAtIndex:i withObject:dict];
            //
            
            
            
            NSMutableDictionary *dict1=[[NSMutableDictionary alloc] init];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
            //            [dummy addObject:[[ary_servelist objectAtIndex:i] valueForKey:@"Serving_Type"]];
            //            [dict setValue:dummy forKey:@"Serving_Type"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
            NSString *str_facycount1=[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"quantity"]];
            int int_fancy1=[str_facycount1 integerValue];
            int_fancy1=int_fancy1-1;
            
            if (int_fancy1<0)
            {
                [dict1 setValue:@"0"  forKey:@"quantity"];
                
            }
            else{
                [dict1 setValue:[NSString stringWithFormat:@"%d", int_fancy1]  forKey:@"quantity"];
                
            }
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Delivery_Address"]] forKey:@"Delivery_Address"];
            
            [ary_servelistfilter replaceObjectAtIndex:i withObject:dict1];
            
#pragma replacing temp array also to match
            [Arr_Temp replaceObjectAtIndex:i withObject:[ary_servelistfilter objectAtIndex:i]];
            
        }
    }
    
    [tableViewDishes reloadData];
    
}


-(void)click_selectObjectAt: (UIButton *)sender
{
    NSLog(@"qwerty  selected");
    //    selectedindex = [sender tag];
    //    if([Arr_Temp containsObject:[arryFoodnames objectAtIndex:sender.tag]])
    //    {
    //        //        [tempDictionary removeObjectForKey:[arryFoodnames objectAtIndex:sender.tag]];
    //        //        [Arr_Temp removeObject:tempDictionary];
    //
    //        [Arr_Temp removeObject:[arryFoodnames objectAtIndex:sender.tag]];
    //    }
    //
    //    else
    //    {
    //        //        [tempDictionary setObject:@"1" forKey:[arryFoodnames objectAtIndex:sender.tag]];
    //        //        [Arr_Temp addObject:tempDictionary];
    //        //        NSLog(@"Array Temp is %@", Arr_Temp);
    //        [Arr_Temp addObject:[arryFoodnames objectAtIndex:sender.tag]];
    //    }
    //
    //    [tableViewDishes reloadData];
    //
    
    NSMutableArray*dummy;
    dummy= [NSMutableArray new];
    
    selectedindex= (int)sender.tag;
    
    if (sender.tag==selectedindex)
    {
        indexSelected = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        
    }
    
    if([Arr_Temp containsObject:[ary_servelistfilter objectAtIndex:sender.tag]])
    {
        [Arr_Temp replaceObjectAtIndex:sender.tag withObject:@""];
       // [Arr_Temp removeObject:[ary_servelistfilter objectAtIndex:sender.tag]];
        //[Arr_Temp removeObjectAtIndex:sender.tag];
        
        NSString* selectedid =[[ary_servelistfilter objectAtIndex:sender.tag]valueForKey:@"DishID"];
        
        for (int i=0; i<[ary_servelistfilter count]; i++)
        {
            if ([selectedid isEqualToString:[[ary_servelistfilter objectAtIndex:i]valueForKey:@"DishID"]])
            {
                //                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                //                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
                //                [dict setValue:@"0"  forKey:@"quantity"];
                //                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
                //                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
                //                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"Description"]]  forKey:@"Description"];
                //                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"ChefCategory"]]  forKey:@"ChefCategory"];
                //                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishCategoryID"]]  forKey:@"DishCategoryID"];
                //                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishCuisineID"]]  forKey:@"DishCuisineID"];
                //                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelist objectAtIndex:i] valueForKey:@"DishPrice"]]  forKey:@"DishPrice"];
                //                [ary_servelist replaceObjectAtIndex:i withObject:dict];
                
                
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
                
                //                [dummy addObject:[[ary_servelist objectAtIndex:i] valueForKey:@"Serving_Type"]];
                //                [dict setValue:dummy forKey:@"Serving_Type"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
                
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
                
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
                [dict setValue:@"0"  forKey:@"quantity"];
                [dict setValue:[NSString  stringWithFormat:@"%@",[[ary_servelistfilter objectAtIndex:i] valueForKey:@"Delivery_Address"]] forKey:@"Delivery_Address"];
                
                NSLog(@"Crash here");
                [ary_servelistfilter replaceObjectAtIndex:i withObject:dict];
                
            }
        }
        
        [tableViewDishes reloadData];
    }
    
    else
    {
        // new code
        NSLog(@"Crash here 1");
        NSLog(@"%d",sender.tag);
        [Arr_Temp replaceObjectAtIndex:sender.tag withObject:[ary_servelistfilter objectAtIndex:sender.tag]];
    }
    
    [tableViewDishes reloadData];
}


-(void)Back_btnClick{
    //    [self.navigationController popViewController];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

-(void)next_btnClick
{
    //
    //    NSMutableArray*ary_mainlisttoshow;
    //    ary_mainlisttoshow = [NSMutableArray new];
    //
    //    for (int i=0; i<[ary_servelistfilter count]; i++)
    //    {
    //     if ([[[ary_servelistfilter objectAtIndex:i]valueForKey:@"quantity"] intValue]<=0)
    //     {
    //
    //     }
    //     else
    //     {
    //         [ary_mainlisttoshow addObject:[ary_servelistfilter objectAtIndex:i]];
    //     }
    //
    //    }
    
    
    
    if (txt_EndTime.text.length<=0)
    {
        [self popup_Alertview:@"plaese select serve time"];
        
    }
    else if (selectedindex<0)
    {
        [self popup_Alertview:@"plaese select atleast a dish"];
        
    }
    //    else if ([ary_servelistfilter count]>0)
    //    {
    //        NSString*str_quantity;
    //
    //        for (int i=0; i<[ary_servelistfilter count]; i++)
    //        {
    //            if ([[[ary_servelistfilter objectAtIndex:i]valueForKey:@"quantity"] intValue]<=0)
    //            {
    //                str_quantity= @"NO";
    //                [self popup_Alertview:@"Plaese select quantity for selected dishes"];
    //                break;
    //            }
    //            else{
    //                str_quantity= @"YES";
    //            }
    //        }
    //
    //        if ([str_quantity isEqualToString:@"YES"])
    //        {
    //            ChefServeTypeVC *serveTypeVc = [[ChefServeTypeVC alloc]init];
    //            NSMutableArray*ary_mainlisttoshow;
    //            ary_mainlisttoshow = [NSMutableArray new];
    //
    //
    //            for (int i=0; i<[ary_servelistfilter count]; i++)
    //            {
    //                if ([[[ary_servelistfilter objectAtIndex:i]valueForKey:@"quantity"] intValue]>0)
    //                {
    //                    [ary_mainlisttoshow addObject:[ary_servelistfilter objectAtIndex:i]];
    //                }
    //            }
    //
    //
    //            [formatter setDateFormat:@"HH:mm"];
    //            NSString *str=[formatter stringFromDate:datePicker.date];
    //            [txt_EndTime setText:str];
    //
    //            serveTypeVc.ary_servelisting = ary_mainlisttoshow;
    //            serveTypeVc.str_time = txt_EndTime.text;
    //
    //            // [self.navigationController pushViewController:serveTypeVc];
    //
    //            [self presentViewController:serveTypeVc animated:NO completion:nil];
    //
    //        }
    //
    //    }
    else
    {
        ChefServeTypeVC *serveTypeVc = [[ChefServeTypeVC alloc]init];
        NSMutableArray*ary_mainlisttoshow;
        ary_mainlisttoshow = [NSMutableArray new];
        
        
        
        
        for (int i=0; i<[ary_servelistfilter count]; i++)
        {
            if ([[[ary_servelistfilter objectAtIndex:i]valueForKey:@"quantity"] intValue]>0)
            {
                [ary_mainlisttoshow addObject:[ary_servelistfilter objectAtIndex:i]];
            }
    
        }
        
        if([ary_mainlisttoshow count]==0)
        {
            [self popup_Alertview:@"plaese select atleast a quantity of dish"];
            return;
        }
        
        
        [formatter setDateFormat:@"hh:mm aa"];
        NSString *str=[formatter stringFromDate:datePicker.date];
        [txt_EndTime setText:str];
        
        serveTypeVc.ary_servelisting = ary_mainlisttoshow;
        serveTypeVc.str_time = txt_EndTime.text;
        
        // [self.navigationController pushViewController:serveTypeVc];
        
        [self presentViewController:serveTypeVc animated:NO completion:nil];
    }
    
    
    
    //    [self.navigationController pushViewController:serveTypeVc];
}

#pragma mark ---textFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField ==textfield_SearchDish)
    {
        if(textField.text)
        {
            search_leg = range.location+1;
        }
        else
        {
            search_leg=0;
        }
        
        
        if (ary_servelistfilter)
        {
            [ary_servelistfilter removeAllObjects];
        }
        
        
        for (NSMutableDictionary *dict in  ary_servelist)
        {
            
            NSString *search = [dict valueForKey:@"DishName"];
            
            if ([search length]>search_leg)
            {
                search = [search substringToIndex:search_leg];
            }
            
            NSString *substring = [NSString stringWithString:textField.text];
            substring = [substring stringByReplacingCharactersInRange:range withString:string];
            BOOL nameMatches = [[substring lowercaseString] hasPrefix:[string lowercaseString]];
            NSRange r = [search rangeOfString:substring options:NSCaseInsensitiveSearch];
            
            if(r.location != NSNotFound || nameMatches)
            {
                if (r.length > 0)
                {
                    [ary_servelistfilter addObject:dict];
                }
            }
            
        }
    }
    [tableViewDishes reloadData];
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == textfield_SearchDish)
    {
        if ([textField.text isEqualToString:@""])
        {
            [ary_servelistfilter removeAllObjects];
            
            for (int i=0; i<[ary_servelist count]; i++)
            {
                [ary_servelistfilter addObject:[ary_servelist objectAtIndex:i]];
            }
            
        }
        
    }
    else
    {
        if ([ary_servelistfilter count]==0)
        {
            
            //[SVProgressHUD showErrorWithStatus:@"No result found"];
            textfield_SearchDish.text = @"";
        }
        
    }
    
    [tableViewDishes reloadData];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [tableViewDishes setHidden:NO];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    [textfield_SearchDish resignFirstResponder];
    
    return YES;
}

#pragma mark TableviewDelegate&DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ary_servelistfilter count];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    }
    
    else
    {
        for (UIView *view in cell.contentView.subviews)
            [view removeFromSuperview];
    }
    //
    //
    //
    //    UILabel *lblTitels = [[UILabel alloc]init];
    //    if (IS_IPHONE_6Plus) {
    //        //        lblTitels.frame=CGRectMake(50, 7, 150, 30);
    //        lblTitels.frame=CGRectMake(50, 12, 150, 30);
    //
    //    }else if (IS_IPHONE_6)
    //
    //    {
    //        lblTitels.frame=CGRectMake(50, 11, 150, 30);
    //
    //    }
    //    else{
    //        lblTitels.frame=CGRectMake(50, 10, 150, 25);
    //
    //    }
    //    lblTitels.text = [arryFoodnames objectAtIndex:indexPath.row];
    //    lblTitels.font=[UIFont fontWithName:kFont size:12];
    //    [cell.contentView addSubview:lblTitels];
    //
    //
    //
    //    UIImageView *image = [[UIImageView alloc]init];
    //    if (IS_IPHONE_6Plus) {
    //        //        image.frame=CGRectMake(10, 5, 30, 30);
    //        image.frame=CGRectMake(10, 12, 30, 30);
    //
    //    }else if (IS_IPHONE_6)
    //
    //    {
    //        image.frame=CGRectMake(10, 11, 30, 30);
    //
    //    }
    //    else{
    //        image.frame=CGRectMake(10, 10, 25, 25);
    //
    //    }
    //    image.image = [UIImage imageNamed:@"img_fork.png"];
    //    [cell.contentView addSubview:image];
    //
    //
    //
    //    UIImageView *imagViewPlusminus  = [[UIImageView alloc]init];
    //
    //    if (IS_IPHONE_6Plus) {
    //            //            imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-170, 10, 120, 25);
    //            imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-170, 12, 120, 30);
    //
    //    }else if (IS_IPHONE_6)
    //
    //    {
    //            imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-170, 11, 120, 30);
    //
    //    }
    //    else{
    //        imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-140, 10, 100, 25);
    //
    //    }
    //    imagViewPlusminus.image = [UIImage imageNamed:@"img_plusminus.png"];
    ////        [cell.contentView addSubview:imagViewPlusminus];
    //
    //
    //        labl = [[UILabel alloc]init];
    //        if (IS_IPHONE_6Plus) {
    //            //            labl.frame=CGRectMake(self.view.frame.size.width-150, 10, 80, 25);
    //            labl.frame=CGRectMake(self.view.frame.size.width-150, 12, 80, 30);
    //
    //        }else if (IS_IPHONE_6)
    //
    //        {
    //            labl.frame=CGRectMake(self.view.frame.size.width-150, 11, 80, 30);
    //
    //        }
    //        else{
    //            labl.frame=CGRectMake(self.view.frame.size.width-130, 10, 80, 25);
    //
    //        }
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //        labl.textAlignment = NSTextAlignmentCenter;
    //        //  labl.layer.borderWidth = 2.0f;
    //        labl.tag = indexPath.row;
    ////         [cell.contentView addSubview:labl];
    //
    //
    //        UIButton *btnMinus = [[UIButton alloc]init];
    //        if (IS_IPHONE_6Plus) {
    //            //            btnMinus.frame=CGRectMake(self.view.frame.size.width-150, 10, 25, 25);
    //            btnMinus.frame=CGRectMake(self.view.frame.size.width-170, 12, 30, 30);
    //
    //        }
    //        else if (IS_IPHONE_6)
    //        {
    //            btnMinus.frame=CGRectMake(self.view.frame.size.width-170, 11, 30, 30);
    //
    //        }
    //        else{
    //            btnMinus.frame=CGRectMake(self.view.frame.size.width-140, 10, 25, 25);
    //
    //        }
    //        btnMinus.tag = indexPath.row;
    //        //        btnMinus.backgroundColor = [UIColor redColor];
    //        [btnMinus addTarget:self action:@selector(click_BtnMinus:) forControlEvents:UIControlEventTouchUpInside];
    ////        [cell.contentView addSubview:btnMinus];
    //
    //
    //        UIButton *btnPlus = [[UIButton alloc]init];
    //        //     btnPlus.frame=CGRectMake(self.view.frame.size.width-50, 10, 25, 25);
    //        if (IS_IPHONE_6Plus) {
    //            //            btnPlus.frame=CGRectMake(self.view.frame.size.width-50, 10, 25, 25);
    //            btnPlus.frame=CGRectMake(self.view.frame.size.width-80, 12, 30, 30);
    //
    //        }
    //        else if (IS_IPHONE_6)
    //        {
    //            btnPlus.frame=CGRectMake(self.view.frame.size.width-80, 11, 30, 30);
    //
    //        }
    //        else
    //        {
    //            btnPlus.frame=CGRectMake(self.view.frame.size.width-65, 10, 25, 25);
    //
    //        }
    //        btnPlus.tag = indexPath.row;
    //        //        btnPlus.backgroundColor = [UIColor redColor];
    //        [btnPlus addTarget:self action:@selector(click_BtnPlus:) forControlEvents:UIControlEventTouchUpInside];
    ////        [cell.contentView addSubview:btnPlus];
    //
    //
    //
    //    UIImageView *img_lineimage=[[UIImageView alloc]init];
    //    img_lineimage.frame=CGRectMake(10, 49, WIDTH-50, 0.5);
    //    if (IS_IPHONE_6Plus) {
    //        //        img_lineimage.frame=CGRectMake(10, 49, WIDTH-40, 0.5);
    //        img_lineimage.frame=CGRectMake(10, 55, WIDTH-60, 0.5);
    //
    //    }else if (IS_IPHONE_6)
    //
    //    {
    //        img_lineimage.frame=CGRectMake(10, 51, WIDTH-50, 0.5);
    //
    //    }
    //    else{
    //        img_lineimage.frame=CGRectMake(10, 44, WIDTH-40, 0.5);
    //
    //    }
    //    [img_lineimage setUserInteractionEnabled:YES];
    //    img_lineimage.backgroundColor=[UIColor clearColor];
    //    img_lineimage.image=[UIImage imageNamed:@"line1.png"];
    //    [cell.contentView addSubview:img_lineimage];
    //
    //
    //
    //    UIButton *btn_TableCell_CheckBox1 = [[UIButton alloc]init];
    //    btn_TableCell_CheckBox1.frame=CGRectMake(10,2,15,15);
    //    if (IS_IPHONE_6Plus)
    //    {
    //        //        img_lineimage.frame=CGRectMake(10, 49, WIDTH-40, 0.5);
    //         btn_TableCell_CheckBox1.frame=CGRectMake(0, 0, tableViewDishes.frame.size.width, 55);
    //
    //    }
    //    else if (IS_IPHONE_6)
    //    {
    //        btn_TableCell_CheckBox1.frame=CGRectMake(0, 0, tableViewDishes.frame.size.width, 51);
    //    }
    //    else
    //    {
    //         btn_TableCell_CheckBox1.frame=CGRectMake(0, 0, tableViewDishes.frame.size.width, 44);
    //    }
    //    btn_TableCell_CheckBox1.backgroundColor=[UIColor clearColor];
    ////    [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img.checkcorrect@2x.png"] forState:UIControlStateSelected];
    ////    [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
    //    [btn_TableCell_CheckBox1 addTarget:self action:@selector(click_selectObjectAt:) forControlEvents:UIControlEventTouchUpInside];
    //    btn_TableCell_CheckBox1.tag = indexPath.row;
    //    [cell.contentView addSubview:btn_TableCell_CheckBox1];
    //
    //
    //    if([Arr_Temp  containsObject:[arryFoodnames objectAtIndex:indexPath.row]])
    //    {
    //        [btn_TableCell_CheckBox1 setSelected:YES];
    //        image.image = [UIImage imageNamed:@"img_forkblue"];
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //        [cell.contentView addSubview:imagViewPlusminus];
    //        [cell.contentView addSubview:labl];
    //        [cell.contentView addSubview:btnMinus];
    //        [cell.contentView addSubview:btnPlus];
    //    }
    //    else{
    //        [btn_TableCell_CheckBox1 setSelected:NO];
    //    }
    //
    //
    //    return cell;
    
    
    //
    //    NSString *kReuseIndentifier = [NSString stringWithFormat:@"Cell-%d",indexPath.row];
    //
    //    UITableViewCell *cell;
    //    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    //
    //    if (cell == nil) {
    //        //        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
    //        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
    //    }
    //    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    //        else
    //        {
    //            for (UIView *view in cell.contentView.subviews)
    //                [view removeFromSuperview];
    //        }
    //
    
    
    
    
    UIImageView *image = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus) {
        //        image.frame=CGRectMake(10, 5, 30, 30);
        image.frame=CGRectMake(10, 12, 30, 30);
        
    }else if (IS_IPHONE_6)
        
    {
        image.frame=CGRectMake(10, 11, 30, 30);
        
    }
    else{
        image.frame=CGRectMake(10, 10, 25, 25);
        
    }
    
    image.image = [UIImage imageNamed:@"img_fork.png"];
    [cell.contentView addSubview:image];
    
    
    UILabel *lblTitels = [[UILabel alloc]init];
    if (IS_IPHONE_6Plus) {
        //        lblTitels.frame=CGRectMake(50, 7, 150, 30);
        lblTitels.frame=CGRectMake(50, 12, 150, 30);
        
    }else if (IS_IPHONE_6)
        
    {
        lblTitels.frame=CGRectMake(50, 11, 150, 30);
        
    }
    else{
        lblTitels.frame=CGRectMake(50, 10, 130, 25);
        
    }
    lblTitels.text = [[ary_servelistfilter objectAtIndex:indexPath.row] valueForKey:@"DishName"];
    lblTitels.font=[UIFont fontWithName:kFont size:12];
    [cell.contentView addSubview:lblTitels];
    
    
    
    
    UIImageView *imagViewPlusminus  = [[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus) {
        //            imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-170, 10, 120, 25);
        imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-170, 12, 120, 30);
        
    }else if (IS_IPHONE_6)
        
    {
        imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-170, 11, 120, 30);
        
    }
    else{
        imagViewPlusminus.frame=CGRectMake(self.view.frame.size.width-140, 10, 100, 25);
        
    }
    imagViewPlusminus.image = [UIImage imageNamed:@"img_plusminus.png"];
    [cell.contentView addSubview:imagViewPlusminus];
    
    
    
    
    UILabel *labl = [[UILabel alloc]init];
    if (IS_IPHONE_6Plus) {
        //            labl.frame=CGRectMake(self.view.frame.size.width-150, 10, 80, 25);
        labl.frame=CGRectMake(self.view.frame.size.width-150, 12, 80, 30);
        
    }else if (IS_IPHONE_6)
        
    {
        labl.frame=CGRectMake(self.view.frame.size.width-150, 11, 80, 30);
        
    }
    else{
        labl.frame=CGRectMake(self.view.frame.size.width-130, 10, 80, 25);
        
    }
    
    labl.textAlignment = NSTextAlignmentCenter;
    labl.text = [[ary_servelistfilter objectAtIndex:indexPath.row] valueForKey:@"quantity"];
    
    [cell.contentView addSubview:labl];
    
    UIButton *btn_TableCell_CheckBox1 = [[UIButton alloc]init];
    btn_TableCell_CheckBox1.frame=CGRectMake(10,2,15,15);
    if (IS_IPHONE_6Plus)
    {
        //        img_lineimage.frame=CGRectMake(10, 49, WIDTH-40, 0.5);
        btn_TableCell_CheckBox1.frame=CGRectMake(0, 0, tableViewDishes.frame.size.width, 55);
        
    }
    else if (IS_IPHONE_6)
    {
        btn_TableCell_CheckBox1.frame=CGRectMake(0, 0, tableViewDishes.frame.size.width, 51);
    }
    else
    {
        btn_TableCell_CheckBox1.frame=CGRectMake(0, 0, tableViewDishes.frame.size.width, 44);
    }
    btn_TableCell_CheckBox1.backgroundColor=[UIColor clearColor];
    //    [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img.checkcorrect@2x.png"] forState:UIControlStateSelected];
    //    [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
    [btn_TableCell_CheckBox1 addTarget:self action:@selector(click_selectObjectAt:) forControlEvents:UIControlEventTouchUpInside];
    btn_TableCell_CheckBox1.tag = indexPath.row;
    [cell.contentView addSubview:btn_TableCell_CheckBox1];
    
    UIButton *btnMinus;
    UIButton *btnPlus;
    
    if ([cell.contentView viewWithTag:100] == nil)
    {
        btnMinus =[UIButton buttonWithType:UIButtonTypeCustom];
        
        if (IS_IPHONE_6Plus) {
            btnMinus.frame=CGRectMake(self.view.frame.size.width-180, 0, cell.contentView.frame.size.height+10, cell.contentView.frame.size.height);
            
        }
        else if (IS_IPHONE_6)
        {
            btnMinus.frame=CGRectMake(self.view.frame.size.width-180, 0, cell.contentView.frame.size.height+10, cell.contentView.frame.size.height);
            
        }
        else{
            btnMinus.frame=CGRectMake(self.view.frame.size.width-150, 0, cell.contentView.frame.size.height+10, cell.contentView.frame.size.height);
            
        }
        //    btnMinus.tag = indexPath.row;
        btnMinus.backgroundColor = [UIColor clearColor];
        [btnMinus addTarget:self action:@selector(click_BtnMinusnow:) forControlEvents:UIControlEventTouchUpInside];
        btnMinus.tag=100;
        btnMinus.layer.borderColor= [[UIColor blackColor]CGColor];
        //btnMinus.layer.borderWidth= 1.0f;
        btnMinus.clipsToBounds = YES;
        [cell.contentView addSubview:btnMinus];
    }
    
    if ([cell.contentView viewWithTag:101] == nil) {
        
        btnPlus = [UIButton buttonWithType:UIButtonTypeCustom];
        if (IS_IPHONE_6Plus) {
            //            btnPlus.frame=CGRectMake(self.view.frame.size.width-50, 10, 25, 25);
            btnPlus.frame=CGRectMake(self.view.frame.size.width-90, 0, cell.contentView.frame.size.height+10, cell.contentView.frame.size.height);
            
        }
        else if (IS_IPHONE_6)
        {
            btnPlus.frame=CGRectMake(self.view.frame.size.width-90, 0, cell.contentView.frame.size.height+10, cell.contentView.frame.size.height);
            
        }
        else
        {
            btnPlus.frame=CGRectMake(self.view.frame.size.width-75, 0, cell.contentView.frame.size.height+10, cell.contentView.frame.size.height);
            
        }
        btnPlus.tag = indexPath.row;
        btnPlus.backgroundColor = [UIColor clearColor];
        [btnPlus addTarget:self action:@selector(click_BtnPlusnow:) forControlEvents:UIControlEventTouchUpInside];
        btnPlus.tag=101;
        btnPlus.layer.borderColor= [[UIColor blackColor]CGColor];
        // btnPlus.layer.borderWidth= 1.0f;
        btnPlus.clipsToBounds = YES;
        [cell.contentView addSubview:btnPlus];
    }
    
    
    //    if (indexPath.row==selectedindex)
    //    {
    //        image.image = [UIImage imageNamed:@"img_forkblue.png"];
    //        btnPlus.hidden = NO;
    //        btnMinus.hidden= NO;
    //        labl.hidden= NO;
    //
    //
    //    }
    //    else{
    //        image.image = [UIImage imageNamed:@"img_fork.png"];
    //        btnPlus.hidden = YES;
    //        btnMinus.hidden= YES;
    //        labl.hidden= YES;
    //
    //
    //    }
    
    if([Arr_Temp containsObject:[ary_servelistfilter objectAtIndex:indexPath.row]])
    {
        imagViewPlusminus.hidden = NO;
        
        image.image = [UIImage imageNamed:@"img_forkblue.png"];
        btnPlus.hidden = NO;
        btnMinus.hidden= NO;
        labl.hidden= NO;
        
        
    }
    else{
        image.image = [UIImage imageNamed:@"img_fork.png"];
        btnPlus.hidden = YES;
        btnMinus.hidden= YES;
        labl.hidden= YES;
        
        imagViewPlusminus.hidden = YES;
        
    }
    
    
    //    //    UIButton *btnPlus = [[UIButton alloc]init];
    //    UIButton *btnPlus = nil;
    //    //     btnPlus.frame=CGRectMake(self.view.frame.size.width-50, 10, 25, 25);
    //    if (IS_IPHONE_6Plus) {
    //        //            btnPlus.frame=CGRectMake(self.view.frame.size.width-50, 10, 25, 25);
    //        btnPlus.frame=CGRectMake(self.view.frame.size.width-80, 12, 30, 30);
    //
    //    }
    //    else if (IS_IPHONE_6)
    //    {
    //        btnPlus.frame=CGRectMake(self.view.frame.size.width-80, 11, 30, 30);
    //
    //    }
    //    else
    //    {
    //        btnPlus.frame=CGRectMake(self.view.frame.size.width-65, 10, 25, 25);
    //
    //    }
    //    btnPlus.tag = indexPath.row;
    //    //        btnPlus.backgroundColor = [UIColor redColor];
    //    [btnPlus addTarget:self action:@selector(click_BtnPlus:) forControlEvents:UIControlEventTouchUpInside];
    //    //        [cell.contentView addSubview:btnPlus];
    
    
    
    UIImageView *img_lineimage=[[UIImageView alloc]init];
    img_lineimage.frame=CGRectMake(10, 49, WIDTH-50, 0.5);
    if (IS_IPHONE_6Plus) {
        //        img_lineimage.frame=CGRectMake(10, 49, WIDTH-40, 0.5);
        img_lineimage.frame=CGRectMake(10, 55, WIDTH-60, 0.5);
        
    }else if (IS_IPHONE_6)
        
    {
        img_lineimage.frame=CGRectMake(10, 51, WIDTH-50, 0.5);
        
    }
    else{
        img_lineimage.frame=CGRectMake(10, 44, WIDTH-40, 0.5);
        
    }
    [img_lineimage setUserInteractionEnabled:YES];
    img_lineimage.backgroundColor=[UIColor clearColor];
    img_lineimage.image=[UIImage imageNamed:@"line1.png"];
    [cell.contentView addSubview:img_lineimage];
    
    
    
    
    
    cell.contentView.tag = indexPath.row;
    
    
    //    if([Arr_Temp  containsObject:[arryFoodnames objectAtIndex:indexPath.row]])
    //    {
    //        [btn_TableCell_CheckBox1 setSelected:YES];
    //        image.image = [UIImage imageNamed:@"img_forkblue"];
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //        [cell.contentView addSubview:imagViewPlusminus];
    //        [cell.contentView addSubview:labl];
    //        [cell.contentView addSubview:btnMinus];
    //        [cell.contentView addSubview:btnPlus];
    //    }
    //    else{
    //        [btn_TableCell_CheckBox1 setSelected:NO];
    //    }
    //
    //
    //
    //    if ([cell viewWithTag:lbltag])
    //    {
    //        labl = (UILabel *)[cell viewWithTag:lbltag];
    //
    //    }
    //    else
    //    {
    //        UILabel *labl = [[UILabel alloc]init];
    //        if (IS_IPHONE_6Plus) {
    //            //            labl.frame=CGRectMake(self.view.frame.size.width-150, 10, 80, 25);
    //            labl.frame=CGRectMake(self.view.frame.size.width-150, 12, 80, 30);
    //
    //        }else if (IS_IPHONE_6)
    //
    //        {
    //            labl.frame=CGRectMake(self.view.frame.size.width-150, 11, 80, 30);
    //
    //        }
    //        else{
    //            labl.frame=CGRectMake(self.view.frame.size.width-130, 10, 80, 25);
    //
    //        }
    //        labl.text = [NSString stringWithFormat:@"%i", minimum_Quantity];
    //        //labl.layer.borderWidth = 4.0;
    //        labl.textAlignment = NSTextAlignmentCenter;
    //        [cell.contentView addSubview:labl];
    //
    //    }
    //
    //    if ([cell viewWithTag:indexPath.row+1])
    //    {
    //        btnPlus = (UIButton*)[cell viewWithTag:indexPath.row+1];
    //    }
    //    else
    //    {
    //        btnPlus=[UIButton buttonWithType:UIButtonTypeCustom];
    //        btnPlus.tag = indexPath.row+1;
    //        if (IS_IPHONE_6Plus) {
    //            //            btnPlus.frame=CGRectMake(self.view.frame.size.width-50, 10, 25, 25);
    //            btnPlus.frame=CGRectMake(self.view.frame.size.width-80, 12, 30, 30);
    //
    //        }
    //        else if (IS_IPHONE_6)
    //        {
    //            btnPlus.frame=CGRectMake(self.view.frame.size.width-80, 11, 30, 30);
    //
    //        }
    //        else
    //        {
    //            btnPlus.frame=CGRectMake(self.view.frame.size.width-65, 10, 25, 25);
    //
    //        }
    //        //        btnPlus.tag = indexPath.row;
    //        //        btnPlus.backgroundColor = [UIColor redColor];
    //        [btnPlus addTarget:self action:@selector(click_BtnPlus:) forControlEvents:UIControlEventTouchUpInside];
    //        //btnPlus.layer.borderWidth = 4.0;
    //        [cell addSubview:btnPlus];
    //
    //    }
    
    return cell;
    
    
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    selectedindex= (int)indexPath.row;
    
    if (indexPath.row==selectedindex)
    {
        indexSelected = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
        
    }
    
    
    //    if([Arr_Temp containsObject:[ary_servelist objectAtIndex:indexPath.row]])
    //    {
    //        [Arr_Temp removeObject:[ary_servelist objectAtIndex:indexPath.row]];
    //    }
    //
    //    else
    //    {
    //        [Arr_Temp addObject:[ary_servelist objectAtIndex:indexPath.row]];
    //    }
    //
    //    [tableViewDishes reloadData];
}



-(void)AFServeNowlist
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    NSDictionary *params =@{
                            
                            @"uid"                              :   [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:Kservenowlist
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseServeNowlist:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFServeNowlist];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseServeNowlist :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    [ary_servelist removeAllObjects];
    [ary_servelistfilter removeAllObjects];
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DishsList"] count]; i++)
        {
            
            // [ary_dummy addObject:[[TheDict valueForKey:@"DishsList"] objectAtIndex:i]];
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
            
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
            
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
            [dict setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
            [dict setValue:@"0"  forKey:@"quantity"];
            [ary_servelist addObject:dict];
            
            
            NSMutableDictionary *dict1=[[NSMutableDictionary alloc] init];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishID"]] forKey:@"DishID"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishName"]]  forKey:@"DishName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishImage"]]  forKey:@"DishImage"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"ChefCategory"]] forKey:@"ChefCategory"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"ChefName"]] forKey:@"ChefName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"CourseIDs"]] forKey:@"CourseIDs"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Description"]] forKey:@"Description"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishPrice"]] forKey:@"DishPrice"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishRestrictions"]] forKey:@"DishRestrictions"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"DishUID"]] forKey:@"DishUID"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"CourseName"]] forKey:@"CourseName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"KitchenDistance"]] forKey:@"KitchenDistance"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Serving_Type"]] forKey:@"Serving_Type"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Total_Serving"]] forKey:@"Total_Serving"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"cuisenCetegory"]] forKey:@"cuisenCetegory"];
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"delivery"]] forKey:@"delivery"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"dinein"]] forKey:@"dinein"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"dish_type"]] forKey:@"dish_type"];
            
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"kitchenName"]] forKey:@"kitchenName"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"likes"]] forKey:@"likes"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"takeout"]] forKey:@"takeout"];
            [dict1 setValue:[NSString  stringWithFormat:@"%@",[[[TheDict valueForKey:@"DishsList"] objectAtIndex:i] valueForKey:@"Delivery_Address"]] forKey:@"Delivery_Address"];
            
            [dict1 setValue:@"0"  forKey:@"quantity"];
            [ary_servelistfilter addObject:dict1];
            
            
            
            
            if ([ary_servelistfilter count]>5)
            {
                imgViewSerchdish.hidden= NO;
                img_Dish.hidden = NO;
                btn_Search.hidden = NO;
                textfield_SearchDish.hidden= NO;
            }
            
            
            
        }
        
        
        // new code
        Arr_Temp=[[NSMutableArray alloc] init];
        for(int i=0;i<ary_servelistfilter.count;i++)
        {
            [Arr_Temp addObject:@""];
        }
        
        [tableViewDishes reloadData];
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
    
    
}



-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
