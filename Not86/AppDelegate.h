//
//  AppDelegate.h
//  Not86
//
//  Created by Interwld on 7/29/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

/*harry/a12345678 : User
 mark/a12345678 : Chef*/

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <AVFoundation/AVFoundation.h>
@class GTMOAuth2Authentication;


@interface AppDelegate : UIResponder <UIApplicationDelegate,MKReverseGeocoderDelegate,CLLocationManagerDelegate>
{
     UIView *splashView;
   BOOL isFBLogin;
    UIActivityIndicatorView *activityIndicator;
    
    //location
    CLLocationManager *locationManager;
    CLGeocoder *myGeoCoder;
    AppDelegate*delegate;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController * navigationC;
@property (nonatomic,assign) BOOL isFBLogin;
@property (nonatomic, strong)NSString *devicestr;
@property (strong, nonatomic) NSString *str_Rotation;
@property (nonatomic, strong)NSString *login_type;



//location
@property (nonatomic, strong) CLLocation *mUserCurrentLocation;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (strong, nonatomic) CLGeocoder *myGeoCoder;
@property(nonatomic,strong)NSMutableString *str_Location1;
@property (strong, nonatomic) NSMutableArray *ary_Country;

@property (nonatomic,assign) BOOL isComeFromLocation;

@property(nonatomic,strong)UIActivityIndicatorView *activityIndicator;

//static NSString * const kClientId = @"1077951852722-01rce54v069m0i4l2l37jbc17ch1q2h2.apps.googleusercontent.com";
//static NSString * const kApiKey = @"AIzaSyD4Ks_y7OGn6vTtDdFRmvYCczajzy4oZvw";


-(void)callME;
-(void)clearApplicationCaches;

//location
-(void) updateCurrentLocation;
-(CLLocation *) getCurrentLocation;
-(void)CurrentLocationIdentifier;
-(void) AFLogout;
-(void)fetchUsersCurrentLocation;
@end

