//
//  ChefSignup.m
//  Not86
//
//  Created by Interwld on 8/11/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ChefSignup.h"
#import "ChefSignup2.h"
#import "AppDelegate.h"
#import "Define.h"

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"


@interface ChefSignup ()<UIScrollViewDelegate,UITextFieldDelegate,UITextViewDelegate,UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>
{
    UIImageView *img_topbar;
    UIImageView *img_BackgroundImg;
    UITextField *txt_FirstName;
    UITextField *txt_MiddleName;
    UITextField *txt_LastName;
    UITextField *Code;
    UITextField *Mobilenumber;
    UITextField *txt_Email;
    UITextField *txt_street;
    UITextField *txt_country;
    UITextField *txt_city;
    UITextField *txt_portal;
    UITextField *txt_DateofBirth;
    AppDelegate *delegate;
    UITableView *tableview_Hcountry;
    UITableView *tableview_Hcountrycode;
    UITableView *tableview_Hcity;
    
    NSMutableArray*ary_countrylist;
    
    NSMutableArray*ary_citylist;
    UIView *alertviewBg;
    UIToolbar *keyboardToolbar_postal;
    NSDateFormatter *formatter;
    UITextField *txt_registrationYear;
    NSMutableArray *ary_countrycodelist;
    CGFloat	animatedDistance;
    UIImageView *img_Font;
    UIImageView *img_Back;
    UIImageView *profileimg;
    UILabel  * lbl_Nationalid;
    
    NSData*imgData;
    NSData*frontData;
    NSData*backData;
    
    NSString*str_pickingImage;
    UIButton *profileimg_button;
    UIButton *fontimg_button;
    
    UIButton *backimg_button;
    UIButton *btn_imgicon;
    
    UIDatePicker *datePicker;
    UIToolbar *keyboardToolbar_Date;
    UIToolbar *keyboardToolbar_Date2;
    UIToolbar *keyboardToolbar_Date3;
    
    
    UILabel  * lbl_imgFont;
    UILabel  * lbl_imgBack;
    UIDatePicker *pickerView;
    
    NSString*str_DOBfinal;
    UIImageView *img_DateofBirth;
    
     NSString * str_countrycodetosend;
    
    UIButton * dropbox;
    UIImageView *line4;
    UIImageView *lin;
    UILabel  * lbl_jpgnpg;
    UIImageView *img_Drop;
    UIImageView *Line5;
    
}

@end

@implementation ChefSignup
@synthesize array_Facebook_Details;

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    str_countrycodetosend = [NSString new];
    str_countrycodetosend = @"1";
    
    str_DOBfinal = [NSString new];
    
    //    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    str_pickingImage = [NSString new];
    ary_countrylist = [NSMutableArray new];
    ary_citylist = [NSMutableArray new];
    ary_countrycodelist = [NSMutableArray new];
    
    [self IntegrateHeaderDesign];
    [self IntegrateBodyDesign];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self countryList];
    
}
-(void)IntegrateHeaderDesign{
    
    
    img_topbar=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,45)];
    [img_topbar setUserInteractionEnabled:YES];
    img_topbar.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:img_topbar];
    
    
    UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(12, 16, 15, 15)];
    [img_back setUserInteractionEnabled:YES];
    img_back.backgroundColor=[UIColor clearColor];
    img_back.image=[UIImage imageNamed:@"arrow@2x.png"];
    [img_topbar addSubview:img_back];
    
    UILabel  * lbl_heading = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(img_back.frame)+20,0, 220,45)];
    lbl_heading.text = @"Chef Application";
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:kFont size:15];
    [img_topbar addSubview:lbl_heading];
    
    UIButton *btn_back = [[UIButton alloc] init];
    btn_back.frame = CGRectMake(0, 0, 60,35);
    btn_back.backgroundColor = [UIColor clearColor];
    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_topbar addSubview:btn_back];
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 7, 30, 30)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-login-logo@2x.png"];
    [img_topbar addSubview:img_logo];
    
    scroll_view=[[UIScrollView alloc]init];
    [scroll_view setShowsVerticalScrollIndicator:NO];
    scroll_view.delegate = self;
    scroll_view.scrollEnabled = YES;
    scroll_view.showsVerticalScrollIndicator = NO;
    [scroll_view setUserInteractionEnabled:YES];
    //  scroll_view.backgroundColor=[UIColor brownColor];
    scroll_view.backgroundColor = [UIColor colorWithRed:241/255.0f green:244/255.0f blue:242/255.0f alpha:1];
    [self.view addSubview:scroll_view];
    
    
    
}
-(void)Back_btnClick{
    //    [self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}
-(void)IntegrateBodyDesign
{
    
    UIImageView *img_backgroundimage=[[UIImageView alloc]init];
    [img_backgroundimage setUserInteractionEnabled:YES];
    img_backgroundimage.backgroundColor=[UIColor clearColor];
    img_backgroundimage.image=[UIImage imageNamed:@"bg1-img@2x.png"];
    //    bg1-img@2x.png
    img_backgroundimage.userInteractionEnabled = YES;
    [scroll_view addSubview:img_backgroundimage];
    
    
    profileimg=[[UIImageView alloc]init];
    [profileimg setUserInteractionEnabled:YES];
    profileimg.backgroundColor=[UIColor clearColor];
    profileimg.image=[UIImage imageNamed:@"img-defaltpic@2x.png"];
    [img_backgroundimage addSubview:profileimg];
    
    profileimg_button = [[UIButton alloc] init];
    profileimg_button.layer.cornerRadius=4.0f;
    profileimg_button.backgroundColor=[UIColor clearColor];
    [profileimg_button addTarget:self action:@selector(profileimg_clickbtn:) forControlEvents:UIControlEventTouchUpInside ];
    [profileimg addSubview:profileimg_button];
    
    UILabel  * lbl_info = [[UILabel alloc]init];
    lbl_info.text = @"Personal Information";
    lbl_info.backgroundColor=[UIColor clearColor];
    lbl_info.textColor=[UIColor blackColor];
    lbl_info.font = [UIFont fontWithName:kFontBold size:15];
    [img_backgroundimage addSubview:lbl_info];
    
    
    txt_FirstName = [[UITextField alloc] init];
    txt_FirstName.borderStyle = UITextBorderStyleNone;
    txt_FirstName.textColor = [UIColor blackColor];
    txt_FirstName.font = [UIFont fontWithName:kFont size:13];
    txt_FirstName.placeholder = @"First Name";
    [txt_FirstName setValue:[UIFont fontWithName: kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_FirstName setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_FirstName.leftViewMode = UITextFieldViewModeAlways;
    txt_FirstName.userInteractionEnabled=YES;
    txt_FirstName.textAlignment = NSTextAlignmentLeft;
    txt_FirstName.backgroundColor = [UIColor clearColor];
    txt_FirstName.keyboardType = UIKeyboardTypeAlphabet;
    txt_FirstName.delegate = self;
    [img_backgroundimage addSubview:txt_FirstName];
    
    
    UIImageView *line=[[UIImageView alloc]init];
    [line setUserInteractionEnabled:YES];
    line.backgroundColor=[UIColor lightGrayColor];
    line.image=[UIImage imageNamed:@"line1.png"];
    [img_backgroundimage addSubview:line];
    
    txt_MiddleName = [[UITextField alloc] init];
    txt_MiddleName.borderStyle = UITextBorderStyleNone;
    txt_MiddleName.font = [UIFont fontWithName:kFont size:13];
    txt_MiddleName.placeholder = @"Middle Name";
    [txt_MiddleName setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_MiddleName setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_MiddleName.leftViewMode = UITextFieldViewModeAlways;
    txt_MiddleName.userInteractionEnabled=YES;
    txt_MiddleName.textAlignment = NSTextAlignmentLeft;
    txt_MiddleName.backgroundColor = [UIColor clearColor];
    txt_MiddleName.keyboardType = UIKeyboardTypeAlphabet;
    txt_MiddleName.delegate = self;
    txt_MiddleName.textColor =[UIColor blackColor];
    [img_backgroundimage addSubview:txt_MiddleName];
    
    
    UIImageView *line1=[[UIImageView alloc]init];
    [line1 setUserInteractionEnabled:YES];
    line1.backgroundColor=[UIColor clearColor];
    line1.image=[UIImage imageNamed:@"line1.png"];
    [img_backgroundimage addSubview:line1];
    
    UILabel  * lbl_option = [[UILabel alloc]init];
    lbl_option.text = @"(optional)";
    lbl_option.backgroundColor=[UIColor clearColor];
    lbl_option.textColor=[UIColor blackColor];
    lbl_option.font = [UIFont fontWithName:kFont size:10];
    [img_backgroundimage addSubview:lbl_option];
    
    txt_LastName = [[UITextField alloc] init];
    txt_LastName.borderStyle = UITextBorderStyleNone;
    txt_LastName.font = [UIFont fontWithName:kFont size:13];
    txt_LastName.placeholder = @"Last Name";
    [txt_LastName setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_LastName setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_LastName.leftViewMode = UITextFieldViewModeAlways;
    txt_LastName.userInteractionEnabled=YES;
    txt_LastName.textAlignment = NSTextAlignmentLeft;
    txt_LastName.backgroundColor = [UIColor clearColor];
    txt_LastName.keyboardType = UIKeyboardTypeAlphabet;
    txt_LastName.delegate = self;
    txt_LastName.textColor =[UIColor blackColor];
    [img_backgroundimage addSubview:txt_LastName];
    
    UIImageView *line2=[[UIImageView alloc]init];
    [line2 setUserInteractionEnabled:YES];
    line2.backgroundColor=[UIColor clearColor];
    line2.image=[UIImage imageNamed:@"line1.png"];
    [img_backgroundimage addSubview:line2];
    
    UILabel  * lbl_Date = [[UILabel alloc]init];
    lbl_Date.text = @"Date of Birth";
    lbl_Date.backgroundColor=[UIColor clearColor];
    lbl_Date.textColor=[UIColor blackColor];
    lbl_Date.font = [UIFont fontWithName:kFont size:13];
    [img_backgroundimage addSubview:lbl_Date];
    
    
    img_DateofBirth=[[UIImageView alloc]init];
    [img_DateofBirth setUserInteractionEnabled:YES];
    img_DateofBirth.backgroundColor=[UIColor clearColor];
    img_DateofBirth.image=[UIImage imageNamed:@"img_datebirth@2x.png"];
    img_DateofBirth.userInteractionEnabled=YES;
    [img_backgroundimage addSubview:img_DateofBirth];
    
    
    txt_DateofBirth = [[UITextField alloc] init];
    txt_DateofBirth.borderStyle = UITextBorderStyleNone;
    txt_DateofBirth.textColor = [UIColor grayColor];
    
    
    
    
    if(IS_IPHONE_6Plus)
    {
        txt_DateofBirth.placeholder = @" d    d        m    m        y    y     y     y";
        
        
        
    }
    else if(IS_IPHONE_6)
    {
        txt_DateofBirth.placeholder = @" d    d       m   m       y    y     y    y";
        
    }
    else if (IS_IPHONE_5)
    {
        txt_DateofBirth.placeholder = @" d  d      m  m     y   y   y   y";
        
        
    }
    else
    {
        txt_DateofBirth.placeholder = @" d  d     m  m    y   y   y   y";
        
        
    }

    //    txt_DateofBirth.placeholder = @"d   d      m  m      y    y    y    y";
    txt_DateofBirth.font = [UIFont fontWithName:kFont size:15];
    txt_DateofBirth.leftViewMode = UITextFieldViewModeAlways;
    txt_DateofBirth.userInteractionEnabled=YES;
    txt_DateofBirth.textAlignment = NSTextAlignmentLeft;
    txt_DateofBirth.backgroundColor = [UIColor clearColor];
    txt_DateofBirth.keyboardType = UIKeyboardTypeAlphabet;
    txt_DateofBirth.delegate = self;
    txt_DateofBirth.textColor =[UIColor blackColor];
    [img_DateofBirth addSubview:txt_DateofBirth];
    if (keyboardToolbar_Date == nil)
    {
        keyboardToolbar_Date = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar_Date setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_DoneDate:)];
        [keyboardToolbar_Date setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    txt_DateofBirth.inputAccessoryView = keyboardToolbar_Date;
    txt_DateofBirth.backgroundColor=[UIColor clearColor];
    
    
    formatter = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    //datePicker.minimumDate = [formatter dateFromString:@"1900-01-01"];
    
    
    NSDate *currentDate = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate];
    components.year = components.year  ;
    [components setYear:-19];
    NSDate *minDate=[calendar dateByAddingComponents:components toDate:currentDate  options:0];
    
    NSLog(@"Minimum date is :: %@",minDate);
    
    [components setYear:-150];
    
    NSDate *maxDate = [calendar dateByAddingComponents:components toDate:currentDate  options:0];
    
    
    NSLog(@"Maximum date is :: %@",maxDate);
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [datePicker setMinimumDate:maxDate];
    [datePicker setMaximumDate:minDate];
    [datePicker setDate:minDate];
    txt_DateofBirth.inputView = datePicker;
    
    
    
    
    
    UIImageView *line_img=[[UIImageView alloc]init];
    [line_img setUserInteractionEnabled:YES];
    line_img.backgroundColor=[UIColor clearColor];
    line_img.image=[UIImage imageNamed:@"line1.png"];
    [img_backgroundimage addSubview:line_img];
    
    
    
    
    img_Font=[[UIImageView alloc]init];
    [img_Font setUserInteractionEnabled:YES];
    img_Font.backgroundColor=[UIColor clearColor];
    img_Font.image=[UIImage imageNamed:@"img-back@2x.png"];
    [img_backgroundimage addSubview:img_Font];
    
    
    
    lbl_imgFont = [[UILabel alloc]init];
    lbl_imgFont.text = @"Front";
    lbl_imgFont.backgroundColor=[UIColor clearColor];
    [lbl_imgFont setUserInteractionEnabled:YES];
    lbl_imgFont.textColor=[UIColor lightGrayColor];
    lbl_imgFont.font = [UIFont fontWithName:kFont size:13];
    [img_Font addSubview:lbl_imgFont];
    
    fontimg_button = [[UIButton alloc] init];
    fontimg_button.layer.cornerRadius=4.0f;
    [fontimg_button setUserInteractionEnabled:YES];
    fontimg_button.backgroundColor=[UIColor clearColor];
    [fontimg_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [fontimg_button addTarget:self action:@selector(frontimg_clickbtn:) forControlEvents:UIControlEventTouchUpInside];
    [img_Font addSubview:fontimg_button];
    
    
    
    img_Back=[[UIImageView alloc]init];
    [img_Back setUserInteractionEnabled:YES];
    img_Back.backgroundColor=[UIColor clearColor];
    img_Back.image=[UIImage imageNamed:@"img-back@2x.png"];
    [img_backgroundimage addSubview:img_Back];
    
    lbl_imgBack = [[UILabel alloc]init];
    lbl_imgBack.text = @"Back";
    lbl_imgBack.backgroundColor=[UIColor clearColor];
    [lbl_imgBack setUserInteractionEnabled:YES];
    
    lbl_imgBack.textColor=[UIColor lightGrayColor];
    lbl_imgBack.font = [UIFont fontWithName:kFont size:13];
    [img_Back addSubview:lbl_imgBack];
    
    
    backimg_button = [[UIButton alloc] init];
    backimg_button.layer.cornerRadius=4.0f;
    [backimg_button setUserInteractionEnabled:YES];
    backimg_button.backgroundColor=[UIColor clearColor];
    [backimg_button addTarget:self action:@selector(backimg_clickbtn:) forControlEvents:UIControlEventTouchUpInside ];
    [img_Back addSubview:backimg_button];
    
    
    lbl_Nationalid = [[UILabel alloc] init];
    //    txt_Nationalid.borderStyle = UITextBorderStyleNone;
    lbl_Nationalid.textColor = [UIColor blackColor];
    lbl_Nationalid.text = @"Scan Social Security\nno./National ID";
    lbl_Nationalid.numberOfLines = 2;
    lbl_Nationalid.userInteractionEnabled=YES;
    lbl_Nationalid.font = [UIFont fontWithName:kFont size:12];
    
    lbl_Nationalid.textAlignment = NSTextAlignmentCenter;
    lbl_Nationalid.backgroundColor = [UIColor clearColor];
    [img_backgroundimage addSubview:lbl_Nationalid];
    
    UIImageView *img_icon=[[UIImageView alloc]init];
    [img_icon setUserInteractionEnabled:YES];
    img_icon.backgroundColor=[UIColor clearColor];
    img_icon.image=[UIImage imageNamed:@"img icon@2x .png"];
    [img_backgroundimage addSubview:img_icon];
    
    btn_imgicon = [[UIButton alloc] init];
    [btn_imgicon setUserInteractionEnabled:YES];
    btn_imgicon.backgroundColor=[UIColor clearColor];
    [btn_imgicon addTarget:self action:@selector(image_Clickbtn:) forControlEvents:UIControlEventTouchUpInside ];
    [img_icon addSubview:btn_imgicon];
    
    
    
    
    UIImageView *lin2=[[UIImageView alloc]init];
    [lin2 setUserInteractionEnabled:YES];
    lin2.backgroundColor=[UIColor clearColor];
    lin2.image=[UIImage imageNamed:@"line1.png"];
    [img_backgroundimage addSubview:lin2];
    
    lbl_jpgnpg = [[UILabel alloc]init];
    lbl_jpgnpg.text = @"[.jpg, .png]";
    lbl_jpgnpg.backgroundColor=[UIColor clearColor];
    lbl_jpgnpg.textColor=[UIColor blackColor];
    lbl_jpgnpg.font = [UIFont fontWithName:kFont size:9];
    [img_backgroundimage addSubview:lbl_jpgnpg];
    
    Code = [[UITextField alloc] init];
    Code.borderStyle = UITextBorderStyleNone;
    Code.textColor = [UIColor blackColor];
    Code.placeholder = @"+65";
    Code .font = [UIFont fontWithName:kFont size:13];
    [Code setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [Code setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    Code.leftViewMode = UITextFieldViewModeAlways;
    Code.userInteractionEnabled=YES;
    Code.textAlignment = NSTextAlignmentCenter;
    Code.backgroundColor = [UIColor clearColor];
    Code.keyboardType = UIKeyboardTypeAlphabet;
    Code.delegate = self;
    [img_backgroundimage addSubview:Code];
    
    img_Drop=[[UIImageView alloc]init];
    [img_Drop setUserInteractionEnabled:YES];
    img_Drop.backgroundColor=[UIColor clearColor];
    img_Drop.image=[UIImage imageNamed:@"drop down.png"];
    [img_backgroundimage addSubview:img_Drop];
    
    
    dropbox = [[UIButton alloc] init];
    dropbox.backgroundColor = [UIColor clearColor];
    //[dropbox setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [dropbox setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [dropbox addTarget:self action:@selector(dropbtn_Code:) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimage addSubview:dropbox];
    
    
    line4=[[UIImageView alloc]init];
    [line4 setUserInteractionEnabled:YES];
    line4.backgroundColor=[UIColor greenColor];
    line4.image=[UIImage imageNamed:@"line1.png"];
    [img_backgroundimage addSubview:line4];
    
    Mobilenumber = [[UITextField alloc] init];
    Mobilenumber.borderStyle = UITextBorderStyleNone;
    Mobilenumber.textColor = [UIColor blackColor];
    Mobilenumber.placeholder = @"Mobile no.";
    Mobilenumber.font = [UIFont fontWithName:kFont size:12];
    [Mobilenumber setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [Mobilenumber setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    Mobilenumber.leftViewMode = UITextFieldViewModeAlways;
    Mobilenumber.userInteractionEnabled=YES;
    Mobilenumber.textAlignment = NSTextAlignmentLeft;
    Mobilenumber.backgroundColor = [UIColor clearColor];
    Mobilenumber.keyboardType = UIKeyboardTypeNumberPad;
    Mobilenumber.delegate = self;
    [img_backgroundimage addSubview:Mobilenumber];
    
    
    
    if (keyboardToolbar_Date2 == nil)
    {
        keyboardToolbar_Date2 = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, IS_IPHONE_5?45:45)];
        [keyboardToolbar_Date2 setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_Done)];
        [keyboardToolbar_Date2 setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    Mobilenumber.inputAccessoryView = keyboardToolbar_Date2;
    
    lin=[[UIImageView alloc]init];
    [lin setUserInteractionEnabled:YES];
    lin.backgroundColor=[UIColor clearColor];
    lin.image=[UIImage imageNamed:@"line1.png"];
    [img_backgroundimage addSubview:lin];
    
    
    
    txt_Email = [[UITextField alloc] init];
    txt_Email.borderStyle = UITextBorderStyleNone;
    txt_Email.textColor = [UIColor blackColor];
    txt_Email.font = [UIFont fontWithName:kFont size:13];
    txt_Email.placeholder = @"Email Address(e.g chef@gmail.com)";
    [txt_Email setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Email setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Email.leftViewMode = UITextFieldViewModeAlways;
    txt_Email.userInteractionEnabled=YES;
    txt_Email.textAlignment = NSTextAlignmentLeft;
    txt_Email.backgroundColor = [UIColor clearColor];
    txt_Email.keyboardType = UIKeyboardTypeAlphabet;
    txt_Email.delegate = self;
    [img_backgroundimage addSubview:txt_Email];
    
    Line5=[[UIImageView alloc]init];
    [Line5 setUserInteractionEnabled:YES];
    Line5.backgroundColor=[UIColor clearColor];
    Line5.image=[UIImage imageNamed:@"line1.png"];
    [img_backgroundimage addSubview:Line5];
    
    UIImageView *img_backgroundimage2=[[UIImageView alloc]init];
    [img_backgroundimage2 setUserInteractionEnabled:YES];
    img_backgroundimage2.backgroundColor=[UIColor clearColor];
    [img_backgroundimage2 setContentMode:UIViewContentModeScaleAspectFill];
    [img_backgroundimage2 setClipsToBounds:YES];
    img_backgroundimage2.image=[UIImage imageNamed:@"bg1-img@2x.png"];
    img_backgroundimage2.userInteractionEnabled = YES;
    [scroll_view addSubview:img_backgroundimage2];
    
    
    
    
    UILabel  * home = [[UILabel alloc]init];
    home.text = @"Home Address";
    home.backgroundColor=[UIColor clearColor];
    home.textColor=[UIColor blackColor];
    home.font = [UIFont fontWithName:kFontBold size:15];
    [img_backgroundimage2 addSubview:home];
    
    txt_street = [[UITextField alloc] init];
    txt_street.borderStyle = UITextBorderStyleNone;
    txt_street.font = [UIFont fontWithName:kFont size:13];
    txt_street.placeholder = @"Street Address";
    [txt_street setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_street setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_street.leftViewMode = UITextFieldViewModeAlways;
    txt_street.userInteractionEnabled=YES;
    txt_street.textAlignment = NSTextAlignmentLeft;
    txt_street.backgroundColor = [UIColor clearColor];
    txt_street.keyboardType = UIKeyboardTypeAlphabet;
    txt_street.delegate = self;
    [img_backgroundimage2 addSubview:txt_street];
    
    UIImageView *line6=[[UIImageView alloc]init];
    [line6 setUserInteractionEnabled:YES];
    line6.backgroundColor=[UIColor clearColor];
    line6.image=[UIImage imageNamed:@"line1.png"];
    [img_backgroundimage2 addSubview:line6];
    
    txt_country = [[UITextField alloc] init];
    txt_country.borderStyle = UITextBorderStyleNone;
    txt_country.textColor = [UIColor blackColor];
    txt_country.font = [UIFont fontWithName:kFont size:13];
    txt_country.placeholder = @"Country";
    [txt_country setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_country setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_country.leftViewMode = UITextFieldViewModeAlways;
    txt_country.userInteractionEnabled=YES;
    txt_country.textAlignment = NSTextAlignmentLeft;
    txt_country.backgroundColor = [UIColor clearColor];
    txt_country.keyboardType = UIKeyboardTypeAlphabet;
    txt_country.delegate = self;
    txt_country.userInteractionEnabled = YES;
    [img_backgroundimage2 addSubview:txt_country];
    
    UIImageView *line7=[[UIImageView alloc]init];
    [line7 setUserInteractionEnabled:YES];
    line7.backgroundColor=[UIColor clearColor];
    line7.image=[UIImage imageNamed:@"line1.png"];
    line7.userInteractionEnabled = YES;
    [img_backgroundimage2 addSubview:line7];
    
    
    UIImageView *img_Dropcountry=[[UIImageView alloc]init];
    [img_Dropcountry setUserInteractionEnabled:YES];
    img_Dropcountry.backgroundColor=[UIColor clearColor];
    img_Dropcountry.image=[UIImage imageNamed:@"drop down.png"];
    img_Dropcountry.userInteractionEnabled = YES;
    [img_backgroundimage2 addSubview:img_Dropcountry];
    
    
    UIButton *brn_dropbox = [[UIButton alloc] init];
    brn_dropbox.backgroundColor = [UIColor clearColor];
    [brn_dropbox addTarget:self action:@selector(dropbtn_Country:) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimage2 addSubview:brn_dropbox];
    
    
    txt_city = [[UITextField alloc] init];
    txt_city.borderStyle = UITextBorderStyleNone;
    txt_city.textColor = [UIColor blackColor];
    txt_city.font = [UIFont fontWithName:kFont size:13];
    txt_city.placeholder = @"City";
    [txt_city setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_city setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_city.leftViewMode = UITextFieldViewModeAlways;
    txt_city.userInteractionEnabled=YES;
    txt_city.textAlignment = NSTextAlignmentLeft;
    txt_city.backgroundColor = [UIColor clearColor];
    txt_city.keyboardType = UIKeyboardTypeAlphabet;
    txt_city.delegate = self;
    [img_backgroundimage2 addSubview:txt_city];
    
    
    UIImageView *img_Dropcity=[[UIImageView alloc]init];
    [img_Dropcity setUserInteractionEnabled:YES];
    img_Dropcity.backgroundColor=[UIColor clearColor];
    img_Dropcity.image=[UIImage imageNamed:@"drop down.png"];
    [img_backgroundimage2 addSubview:img_Dropcity];
    
    
    UIButton *dropbox1 = [[UIButton alloc] init];
    dropbox1.backgroundColor = [UIColor clearColor];
    [dropbox1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [dropbox1 addTarget:self action:@selector(dropbtn_City:) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimage2 addSubview:dropbox1];
    
    UIImageView *line8=[[UIImageView alloc]init];
    [line8 setUserInteractionEnabled:YES];
    line8.backgroundColor=[UIColor clearColor];
    line8.image=[UIImage imageNamed:@"line1.png"];
    [img_backgroundimage2 addSubview:line8];
    
    
    
    
    txt_portal = [[UITextField alloc] init];
    txt_portal.borderStyle = UITextBorderStyleNone;
    txt_portal.textColor = [UIColor blackColor];
    txt_portal.placeholder = @"Postal";
   
    txt_portal.leftViewMode = UITextFieldViewModeAlways;
    txt_portal.userInteractionEnabled=YES;
    txt_portal.textAlignment = NSTextAlignmentLeft;
    txt_portal.backgroundColor = [UIColor clearColor];
    txt_portal.keyboardType = UIKeyboardTypeNumberPad;
    txt_portal.delegate = self;
    [img_backgroundimage2 addSubview:txt_portal];
    if (keyboardToolbar_Date3 == nil)
    {
        keyboardToolbar_Date3 = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, IS_IPHONE_5?45:45)];
        [keyboardToolbar_Date3 setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_Done)];
        [keyboardToolbar_Date3 setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    txt_portal.font= [UIFont fontWithName:kFont size:13];
    txt_portal.inputAccessoryView = keyboardToolbar_Date3;
    
    
    UIImageView *line9=[[UIImageView alloc]init];
    [line9 setUserInteractionEnabled:YES];
    line9.backgroundColor=[UIColor clearColor];
    line9.image=[UIImage imageNamed:@"line1.png"];
    [img_backgroundimage2 addSubview:line9];
    
    UIImageView *image=[[UIImageView alloc]init];
    [image setUserInteractionEnabled:YES];
    image.backgroundColor=[UIColor colorWithRed:241/255.0f green:244/255.0f blue:242/255.0f alpha:1];
    [scroll_view addSubview:image];
    
    UILabel  * page = [[UILabel alloc]init];
    page.text = @"Page 1/6";
    page.backgroundColor=[UIColor clearColor];
    page.textAlignment=NSTextAlignmentCenter;
    page.textColor=[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1];
    page.font = [UIFont fontWithName:kFont size:13];
    [image addSubview:page];
    
    UIButton *next = [[UIButton alloc] init];
    next.layer.cornerRadius=4.0f;
    [next setImage:[UIImage imageNamed:@"text buuton.png"] forState:UIControlStateNormal];
    [next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [next addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [image addSubview:next];
    
#pragma mark Tableview
    
    
    
    tableview_Hcountrycode = [[UITableView alloc]init];
    [tableview_Hcountrycode setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tableview_Hcountrycode.delegate = self;
    tableview_Hcountrycode.dataSource = self;
    tableview_Hcountrycode.showsVerticalScrollIndicator = NO;
    tableview_Hcountrycode.backgroundColor = [UIColor whiteColor];
    tableview_Hcountrycode.layer.borderColor = [[UIColor blackColor]CGColor];
    tableview_Hcountrycode.layer.borderWidth = 1.0f;
    tableview_Hcountrycode.clipsToBounds = YES;
    tableview_Hcountrycode.hidden =YES;
    [scroll_view addSubview:tableview_Hcountrycode];
    
    tableview_Hcountry = [[UITableView alloc]init];
    [tableview_Hcountry setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tableview_Hcountry.delegate = self;
    tableview_Hcountry.dataSource = self;
    tableview_Hcountry.showsVerticalScrollIndicator = NO;
    tableview_Hcountry.backgroundColor = [UIColor whiteColor];
    tableview_Hcountry.layer.borderColor = [[UIColor blackColor]CGColor];
    tableview_Hcountry.layer.borderWidth = 1.0f;
    tableview_Hcountry.clipsToBounds = YES;
    tableview_Hcountry.hidden =YES;
    [img_backgroundimage2 addSubview:tableview_Hcountry];
    
    tableview_Hcity = [[UITableView alloc]init];
    [tableview_Hcity setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tableview_Hcity.delegate = self;
    tableview_Hcity.dataSource = self;
    tableview_Hcity.showsVerticalScrollIndicator = NO;
    tableview_Hcity.backgroundColor = [UIColor whiteColor];
    tableview_Hcity.layer.borderColor = [[UIColor blackColor]CGColor];
    tableview_Hcity.layer.borderWidth = 1.0f;
    tableview_Hcity.clipsToBounds = YES;
    tableview_Hcity.hidden =YES;
    [img_backgroundimage2 addSubview:tableview_Hcity];
    
    
    
    
    
    
    //    ary_countrylist=[[NSMutableArray alloc]init];
    
    
    if (IS_IPHONE_6Plus)
    {
        scroll_view.frame=CGRectMake(0, 45, WIDTH, HEIGHT);
        img_backgroundimage.frame=CGRectMake(2, -10 ,WIDTH, 540);
        profileimg.frame=CGRectMake(140, 20, 120, 120);
        profileimg.layer.cornerRadius =120/2;
        profileimg.clipsToBounds = YES;
        profileimg_button.frame=CGRectMake(0, 10, 120, 120);
        lbl_info.frame =CGRectMake(125,134, 300,40);
        txt_FirstName.frame=CGRectMake(25,160, WIDTH-50, 38);
        line.frame=CGRectMake(25, CGRectGetMaxY(txt_FirstName.frame)-7, WIDTH-50, 0.5);
        txt_MiddleName.frame=CGRectMake(25,CGRectGetMaxY(txt_FirstName.frame)+4, WIDTH-50, 38);
        line1.frame=CGRectMake(25, CGRectGetMaxY(txt_MiddleName.frame)-7, WIDTH-50, 0.5);
        lbl_option.frame =CGRectMake(330,CGRectGetMaxY(txt_MiddleName.frame)-5, 100,10);
        txt_LastName.frame=CGRectMake(25, CGRectGetMaxY(txt_MiddleName.frame)+4, WIDTH-50, 38);
        line2.frame=CGRectMake(25, CGRectGetMaxY(txt_LastName.frame)-7, WIDTH-50, 0.5);
        lbl_Date.frame =CGRectMake(25,CGRectGetMaxY(txt_LastName.frame)+18, 100,20);
        img_DateofBirth.frame=CGRectMake(CGRectGetMaxX(lbl_Date.frame)+1,CGRectGetMaxY(txt_LastName.frame)+18, 250,20);
        txt_DateofBirth.frame=CGRectMake(0,0, 250,20);
        img_Font.frame=CGRectMake(25,CGRectGetMaxY(lbl_Date.frame)+10, 45, 45);
        lbl_imgFont.frame=CGRectMake(5,8, 35, 30);
        fontimg_button.frame=CGRectMake(0,0, 45, 45);
        img_Back.frame=CGRectMake(CGRectGetMaxX(img_Font.frame)+3,CGRectGetMaxY(lbl_Date.frame)+10, 45, 45);
        lbl_imgBack.frame=CGRectMake(5,10, 35, 20);
        backimg_button.frame=CGRectMake(0,0, 45, 45);
        lbl_Nationalid.frame=CGRectMake(CGRectGetMaxX(img_Back.frame)-10, CGRectGetMaxY(lbl_Date.frame)+19, 150, 45);
        img_icon.frame=CGRectMake(350, CGRectGetMaxY(lbl_Date.frame)+35, 30, 20);
        btn_imgicon.frame=CGRectMake(0, 0, 30, 20);
        lin2.frame=CGRectMake(25, CGRectGetMaxY(lbl_Date.frame)+60, WIDTH-50, 0.5);
        lbl_jpgnpg.frame =CGRectMake(340,CGRectGetMaxY(lin2.frame)+1, 60,10);
        Code.frame=CGRectMake(25,CGRectGetMaxY(lbl_jpgnpg.frame)+13, 35, 30);
        img_Drop.frame=CGRectMake(55, CGRectGetMaxY(lbl_jpgnpg.frame)+20, 15, 10);
        dropbox.frame = CGRectMake(25, CGRectGetMaxY(lbl_jpgnpg.frame)+13, 70, 30);
        tableview_Hcountrycode.frame=CGRectMake(25, CGRectGetMaxY(lbl_jpgnpg.frame)+13, 110, 100);
        line4.frame=CGRectMake(25, CGRectGetMaxY(Code.frame)-6, 50, 0.5);
        Mobilenumber.frame=CGRectMake(85,CGRectGetMaxY(lbl_jpgnpg.frame)+8, 200, 38);
        lin.frame=CGRectMake(85, CGRectGetMaxY(Mobilenumber.frame)-8, 300, 0.5);
        txt_Email.frame=CGRectMake(25,CGRectGetMaxY(Mobilenumber.frame)+15, WIDTH-80, 38);
        Line5.frame=CGRectMake(25, CGRectGetMaxY(txt_Email.frame)-7, WIDTH-50, 0.5);
        img_backgroundimage2.frame=CGRectMake(2, CGRectGetMaxY(img_backgroundimage.frame)+1, WIDTH-1, 250);
        home.frame=CGRectMake(150,20, 300,40);
        txt_street.frame=CGRectMake(25,CGRectGetMaxY(home.frame)+1, WIDTH-50, 38);
        line6.frame=CGRectMake(25, CGRectGetMaxY(txt_street.frame)-7, WIDTH-50, 0.5);
        txt_country.frame=CGRectMake(25,CGRectGetMaxY(txt_street.frame)+4, 150, 38);
        line7.frame=CGRectMake(25, CGRectGetMaxY(txt_country.frame)-7, WIDTH-50, 0.5);
        img_Dropcountry.frame=CGRectMake(370, CGRectGetMaxY(txt_street.frame)+15, 16, 10);
        brn_dropbox.frame = CGRectMake(25, CGRectGetMaxY(txt_street.frame)+4, 385, 38);
        tableview_Hcountry.frame  = CGRectMake(25,CGRectGetMaxY(txt_country.frame)-150,385,150);
        txt_city.frame=CGRectMake(25,CGRectGetMaxY(txt_country.frame)+4, 310, 38);
        line8.frame=CGRectMake(25,CGRectGetMaxY(txt_city.frame)-7, WIDTH-50, 0.5);
        img_Dropcity.frame=CGRectMake(370, CGRectGetMaxY(txt_country.frame)+15, 16, 10);
        dropbox1.frame = CGRectMake(25, CGRectGetMaxY(txt_country.frame)+4, 385, 38);
        tableview_Hcity.frame=CGRectMake(25, CGRectGetMaxY(txt_city.frame)-150, 385, 150);
        txt_portal.frame=CGRectMake(25,CGRectGetMaxY(txt_city.frame)+10, WIDTH-50, 38);
        line9.frame=CGRectMake(25, CGRectGetMaxY(txt_portal.frame)-7, WIDTH-50, 0.5);
        image.frame=CGRectMake(0, CGRectGetMaxY(img_backgroundimage2.frame)+3, WIDTH, 100);
        page.frame =CGRectMake(0,0, WIDTH,30);
        next.frame = CGRectMake(18, 40, WIDTH-36,50);
        
        [txt_portal setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
        [txt_portal setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
        [scroll_view setContentSize:CGSizeMake(0,CGRectGetMaxY(img_backgroundimage2.frame)+150)];
        
    }
    
    else if (IS_IPHONE_6)
    {
        scroll_view.frame=CGRectMake(0, 45, WIDTH, HEIGHT);
        img_backgroundimage.frame=CGRectMake(2, 2 ,WIDTH-1, 537);
        profileimg.frame=CGRectMake(125, 20, 120, 120);
        profileimg_button.frame=CGRectMake(0, 15, 120, 120);
        profileimg.layer.cornerRadius =120/2;
        profileimg.clipsToBounds = YES;
        lbl_info.frame =CGRectMake(120,130, 300,40);
        txt_FirstName.frame=CGRectMake(25,160, WIDTH-50, 38);
        line.frame=CGRectMake(25, CGRectGetMaxY(txt_FirstName.frame)-7, WIDTH-40, 0.5);
        txt_MiddleName.frame=CGRectMake(25,CGRectGetMaxY(txt_FirstName.frame)+4, WIDTH-40, 38);
        line1.frame=CGRectMake(25, CGRectGetMaxY(txt_MiddleName.frame)-7, WIDTH-40, 0.5);
        lbl_option.frame =CGRectMake(310,CGRectGetMaxY(txt_MiddleName.frame)-5, 100,10);
        txt_LastName.frame=CGRectMake(25, CGRectGetMaxY(txt_MiddleName.frame)+4, WIDTH-40, 38);
        line2.frame=CGRectMake(25, CGRectGetMaxY(txt_LastName.frame)-7, WIDTH-40, 0.5);
        lbl_Date.frame =CGRectMake(25,CGRectGetMaxY(txt_LastName.frame)+18, 80,20);
        img_DateofBirth.frame=CGRectMake(CGRectGetMaxX(lbl_Date.frame)+1,CGRectGetMaxY(txt_LastName.frame)+18, 230,20);
        txt_DateofBirth.frame=CGRectMake(0,0, 230,20);
        //        lbl_Date.frame =CGRectMake(25,CGRectGetMaxY(txt_LastName.frame)+18, 80,20);
        //        img_DateofBirth.frame=CGRectMake(CGRectGetMaxX(lbl_Date.frame)+1,CGRectGetMaxY(txt_LastName.frame)+18, 220,20);
        //        txt_DateofBirth.frame=CGRectMake(0,0, 180,20);
        img_Font.frame=CGRectMake(25,CGRectGetMaxY(lbl_Date.frame)+10, 45, 45);
        lbl_imgFont.frame=CGRectMake(5,8, 35, 30);
        fontimg_button.frame=CGRectMake(0,0, 45, 45);
        img_Back.frame=CGRectMake(CGRectGetMaxX(img_Font.frame)+3,CGRectGetMaxY(lbl_Date.frame)+10, 45, 45);
        lbl_imgBack.frame=CGRectMake(5,10, 35, 20);
        backimg_button.frame=CGRectMake(0,0, 45, 45);
        lbl_Nationalid.frame=CGRectMake(CGRectGetMaxX(img_Back.frame)-10, CGRectGetMaxY(lbl_Date.frame)+10, 150, 45);
        img_icon.frame=CGRectMake(320, CGRectGetMaxY(lbl_Date.frame)+35, 30, 20);
        btn_imgicon.frame=CGRectMake(0, 0, 30, 20);
        lin2.frame=CGRectMake(25, CGRectGetMaxY(lbl_Date.frame)+60, WIDTH-40, 0.5);
        lbl_jpgnpg.frame =CGRectMake(310,CGRectGetMaxY(lin2.frame)+1, 60,10);
        Code.frame=CGRectMake(25,CGRectGetMaxY(lbl_jpgnpg.frame)+13, 35, 30);
        img_Drop.frame=CGRectMake(58, CGRectGetMaxY(lbl_jpgnpg.frame)+20, 15, 10);
        dropbox.frame = CGRectMake(25, CGRectGetMaxY(lbl_jpgnpg.frame)+13, 70, 30);
        tableview_Hcountrycode.frame=CGRectMake(25, CGRectGetMaxY(Code.frame), 110, 100);
        line4.frame=CGRectMake(25, CGRectGetMaxY(Code.frame)-6, 50, 0.5);
        Mobilenumber.frame=CGRectMake(85,CGRectGetMaxY(lbl_jpgnpg.frame)+8, 200, 38);
        lin.frame=CGRectMake(85, CGRectGetMaxY(Mobilenumber.frame)-8, 275, 0.5);
        txt_Email.frame=CGRectMake(25,CGRectGetMaxY(Mobilenumber.frame)+15, WIDTH-70, 38);
        Line5.frame=CGRectMake(25, CGRectGetMaxY(txt_Email.frame)-7, WIDTH-40, 0.5);
        img_backgroundimage2.frame=CGRectMake(2, CGRectGetMaxY(img_backgroundimage.frame)-5, WIDTH-1, 260);
        home.frame=CGRectMake(140,20, 300,40);
        txt_street.frame=CGRectMake(25,CGRectGetMaxY(home.frame)+1, WIDTH-40, 38);
        line6.frame=CGRectMake(25, CGRectGetMaxY(txt_street.frame)-7, WIDTH-40, 0.5);
        txt_country.frame=CGRectMake(25,CGRectGetMaxY(txt_street.frame)+4, 150, 38);
        line7.frame=CGRectMake(25, CGRectGetMaxY(txt_country.frame)-7, WIDTH-40, 0.5);
        img_Dropcountry.frame=CGRectMake(335, CGRectGetMaxY(txt_street.frame)+15, 16, 10);
        brn_dropbox.frame = CGRectMake(25, CGRectGetMaxY(txt_street.frame)+4, 345, 38);
        tableview_Hcountry.frame  = CGRectMake(25,CGRectGetMaxY(txt_country.frame)-200,345,200);
        txt_city.frame=CGRectMake(25,CGRectGetMaxY(txt_country.frame)+4, 310, 38);
        line8.frame=CGRectMake(25,CGRectGetMaxY(txt_city.frame)-7, WIDTH-40, 0.5);
        img_Dropcity.frame=CGRectMake(335, CGRectGetMaxY(txt_country.frame)+15, 16, 10);
        dropbox1.frame = CGRectMake(25, CGRectGetMaxY(txt_country.frame)+4, 360, 38);
        tableview_Hcity.frame=CGRectMake(25, CGRectGetMaxY(txt_city.frame)-200, 345, 200);
        txt_portal.frame=CGRectMake(25,CGRectGetMaxY(txt_city.frame)+10, WIDTH-40, 38);
        line9.frame=CGRectMake(25, CGRectGetMaxY(txt_portal.frame)-7, WIDTH-40, 0.5);
        image.frame=CGRectMake(0, CGRectGetMaxY(img_backgroundimage2.frame)+3, WIDTH, 100);
        //        image.frame=CGRectMake(0, HEIGHT-100, WIDTH, 100);
        page.frame =CGRectMake(0,4, WIDTH,30);
        next.frame = CGRectMake(18, 40, WIDTH-36,50);
        
        [txt_portal setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
        [txt_portal setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
        [scroll_view setContentSize:CGSizeMake(0,CGRectGetMaxY(img_backgroundimage2.frame)+150)];
        
        
    }
    else if (IS_IPHONE_5)
    {
        
        scroll_view.frame=CGRectMake(0, 45, WIDTH, HEIGHT);
        img_backgroundimage.frame=CGRectMake(-4, -13 ,WIDTH+12, 500);
        profileimg.frame=CGRectMake(100, 15, 120, 120);
        profileimg_button.frame=CGRectMake(0, 10, 120, 120);
        profileimg.layer.cornerRadius =120/2;
        profileimg.clipsToBounds = YES;
        lbl_info.frame =CGRectMake(90,130, 300,40);
        txt_FirstName.frame=CGRectMake(28,165, WIDTH-50, 38);
        line.frame=CGRectMake(25, CGRectGetMaxY(txt_FirstName.frame)-7, WIDTH-50, 0.5);
        txt_MiddleName.frame=CGRectMake(25,CGRectGetMaxY(txt_FirstName.frame)+4, WIDTH-50, 38);
        line1.frame=CGRectMake(25, CGRectGetMaxY(txt_MiddleName.frame)-7, WIDTH-50, 0.5);
        lbl_option.frame =CGRectMake(245,CGRectGetMaxY(txt_MiddleName.frame)-5, 100,10);
        txt_LastName.frame=CGRectMake(25, CGRectGetMaxY(txt_MiddleName.frame)+4, WIDTH-50, 38);
        line2.frame=CGRectMake(25, CGRectGetMaxY(txt_LastName.frame)-7, WIDTH-50, 0.5);
        
        lbl_Date.frame =CGRectMake(25,CGRectGetMaxY(txt_LastName.frame)+18, 80,20);
        img_DateofBirth.frame=CGRectMake(CGRectGetMaxX(lbl_Date.frame)+1,CGRectGetMaxY(txt_LastName.frame)+18, 190,20);
        txt_DateofBirth.frame=CGRectMake(0,0, 210,20);
        
        //        lbl_Date.frame =CGRectMake(25,CGRectGetMaxY(txt_LastName.frame)+10, 100,20);
        //        img_DateofBirth.frame=CGRectMake(CGRectGetMaxX(lbl_Date.frame)+4,CGRectGetMaxY(txt_LastName.frame)+10, 150,20);
        //        txt_DateofBirth.frame=CGRectMake(0,0, 160,20);
        img_Font.frame=CGRectMake(25,CGRectGetMaxY(lbl_Date.frame)+13, 45, 45);
        lbl_imgFont.frame=CGRectMake(5,8, 35, 30);
        fontimg_button.frame=CGRectMake(0,0, 45, 45);
        img_Back.frame=CGRectMake(CGRectGetMaxX(img_Font.frame)+5,CGRectGetMaxY(lbl_Date.frame)+13, 45, 45);
        lbl_imgBack.frame=CGRectMake(5,10, 35, 20);
        backimg_button.frame=CGRectMake(0,0, 45, 45);
        lbl_Nationalid.frame=CGRectMake(CGRectGetMaxX(img_Back.frame)-10, CGRectGetMaxY(lbl_Date.frame)+10, 150, 45);
        img_icon.frame=CGRectMake(260, CGRectGetMaxY(lbl_Date.frame)+30, 30, 25);
        btn_imgicon.frame=CGRectMake(0, 0, 30, 25);
        lin2.frame=CGRectMake(25, CGRectGetMaxY(lbl_Date.frame)+60, WIDTH-50, 0.5);
        lbl_jpgnpg.frame =CGRectMake(250,CGRectGetMaxY(img_icon.frame)+4, 60,10);
        Code.frame=CGRectMake(25,CGRectGetMaxY(lin2.frame)+10, 35, 30);
        img_Drop.frame=CGRectMake(60, CGRectGetMaxY(lbl_jpgnpg.frame)+10, 15, 8);
        dropbox.frame = CGRectMake(25,CGRectGetMaxY(lin2.frame)+10, 65, 30);
        tableview_Hcountrycode.frame=CGRectMake(25, CGRectGetMaxY(Code.frame), 90, 100);
        line4.frame=CGRectMake(25, CGRectGetMaxY(Code.frame)-6, 50, 0.5);
        Mobilenumber.frame=CGRectMake(90,387, 200, 38);
        lin.frame=CGRectMake(90, CGRectGetMaxY(Mobilenumber.frame)-5, 205, 0.5);
        txt_Email.frame=CGRectMake(25,420, WIDTH-55, 38);
        Line5.frame=CGRectMake(25, CGRectGetMaxY(txt_Email.frame)-7, WIDTH-50, 0.5);
        img_backgroundimage2.frame=CGRectMake(-4, CGRectGetMaxY(img_backgroundimage.frame)-14, WIDTH+12, 250);
        home.frame=CGRectMake(105,20, 300,40);
        txt_street.frame=CGRectMake(25,CGRectGetMaxY(home.frame)+1, WIDTH-50, 38);
        line6.frame=CGRectMake(25, CGRectGetMaxY(txt_street.frame)-7, WIDTH-50, 0.5);
        txt_country.frame=CGRectMake(25,CGRectGetMaxY(txt_street.frame)+4, 150, 38);
        line7.frame=CGRectMake(25, CGRectGetMaxY(txt_country.frame)-7, WIDTH-50, 0.5);
        img_Dropcountry.frame=CGRectMake(270, CGRectGetMaxY(txt_street.frame)+15, 16, 10);
        brn_dropbox.frame = CGRectMake(25, CGRectGetMaxY(txt_street.frame)+4, WIDTH-50, 38);
        tableview_Hcountry.frame  = CGRectMake(25,CGRectGetMaxY(txt_country.frame)-100,WIDTH-50,100);
        txt_city.frame=CGRectMake(25,CGRectGetMaxY(txt_country.frame)+4, WIDTH-50, 38);
        line8.frame=CGRectMake(25,CGRectGetMaxY(txt_city.frame)-7, WIDTH-50, 0.5);
        img_Dropcity.frame=CGRectMake(270, CGRectGetMaxY(txt_country.frame)+15, 16, 10);
        dropbox1.frame = CGRectMake(25, CGRectGetMaxY(txt_country.frame)+4, 295, 38);
        tableview_Hcity.frame=CGRectMake(25, CGRectGetMaxY(txt_city.frame)-100, WIDTH-50, 100);
        txt_portal.frame=CGRectMake(25,CGRectGetMaxY(line8.frame)+15, WIDTH-50, 38);
        line9.frame=CGRectMake(25, CGRectGetMaxY(txt_portal.frame)+-7, WIDTH-50, 0.5);
        image.frame=CGRectMake(0, CGRectGetMaxY(img_backgroundimage2.frame)+3, WIDTH, 100);
        //        image.frame=CGRectMake(0, HEIGHT-80, WIDTH, 100);
        page.frame =CGRectMake(0,0, WIDTH,30);
        next.frame = CGRectMake(18, 35, WIDTH-36,40);
        
        [txt_portal setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
        [txt_portal setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
        [scroll_view setContentSize:CGSizeMake(0,860)];
    }
    else
    {
//        scroll_view.frame=CGRectMake(0, 45, WIDTH, HEIGHT);
//        img_backgroundimage.frame=CGRectMake(-4, -13 ,WIDTH+11, 500);
//        profileimg.frame=CGRectMake(100, 15, 120, 120);
//        profileimg_button.frame=CGRectMake(0, 10, 120, 120);
//        profileimg.layer.cornerRadius =120/2;
//        profileimg.clipsToBounds = YES;
//        lbl_info.frame =CGRectMake(90,130, 300,40);
//        txt_FirstName.frame=CGRectMake(25,160, WIDTH-50, 38);
//        line.frame=CGRectMake(25, CGRectGetMaxY(txt_FirstName.frame)-7, WIDTH-50, 0.5);
//        txt_MiddleName.frame=CGRectMake(25,CGRectGetMaxY(txt_FirstName.frame)+4, WIDTH-50, 38);
//        line1.frame=CGRectMake(25, CGRectGetMaxY(txt_MiddleName.frame)-7, WIDTH-50, 0.5);
//        lbl_option.frame =CGRectMake(245,CGRectGetMaxY(txt_MiddleName.frame)-5, 100,10);
//        txt_LastName.frame=CGRectMake(25, CGRectGetMaxY(txt_MiddleName.frame)+4, WIDTH-50, 38);
//        line2.frame=CGRectMake(25, CGRectGetMaxY(txt_LastName.frame)-7, WIDTH-50, 0.5);
//        lbl_Date.frame =CGRectMake(25,CGRectGetMaxY(txt_LastName.frame)+10, 80,20);
//        img_DateofBirth.frame=CGRectMake(CGRectGetMaxX(lbl_Date.frame)+10,CGRectGetMaxY(txt_LastName.frame)+10,180,20);
//        txt_DateofBirth.frame=CGRectMake(0,0, 200,20);
//        img_Font.frame=CGRectMake(25,CGRectGetMaxY(lbl_Date.frame)+10, 45, 45);
//        lbl_imgFont.frame=CGRectMake(5,8, 35, 30);
//        fontimg_button.frame=CGRectMake(0,0, 45, 45);
//        img_Back.frame=CGRectMake(CGRectGetMaxX(img_Font.frame)+3,CGRectGetMaxY(lbl_Date.frame)+10, 45, 45);
//        lbl_imgBack.frame=CGRectMake(5,10, 35, 20);
//        backimg_button.frame=CGRectMake(0,0, 45, 45);
//        lbl_Nationalid.frame=CGRectMake(CGRectGetMaxX(img_Back.frame)-10, CGRectGetMaxY(lbl_Date.frame)+10, 150, 45);
//        img_icon.frame=CGRectMake(260, CGRectGetMaxY(lbl_Date.frame)+30, 30, 25);
//        btn_imgicon.frame=CGRectMake(0, 0, 30, 25);
//        lin2.frame=CGRectMake(25, CGRectGetMaxY(lbl_Date.frame)+60, WIDTH-50, 0.5);
//        lbl_jpgnpg.frame =CGRectMake(250,CGRectGetMaxY(img_icon.frame)+4, 60,10);
//        Code.frame=CGRectMake(25,CGRectGetMaxY(lin2.frame)+10, 35, 30);
//        tableview_Hcountrycode.frame=CGRectMake(25, CGRectGetMaxY(txt_city.frame), 60, 100);
//        img_Drop.frame=CGRectMake(60, CGRectGetMaxY(lbl_jpgnpg.frame)+10, 15, 8);
//        dropbox.frame = CGRectMake(25, CGRectGetMaxY(lbl_jpgnpg.frame)+10, 35, 30);
//        line4.frame=CGRectMake(25, CGRectGetMaxY(Code.frame)-6, 50, 0.5);
//        Mobilenumber.frame=CGRectMake(90,378, 200, 38);
//        lin.frame=CGRectMake(90, CGRectGetMaxY(Mobilenumber.frame)-8, 205, 0.5);
//        txt_Email.frame=CGRectMake(25,420, WIDTH-55, 38);
//        Line5.frame=CGRectMake(25, CGRectGetMaxY(txt_Email.frame)-7, WIDTH-50, 0.5);
//        img_backgroundimage2.frame=CGRectMake(-4, CGRectGetMaxY(img_backgroundimage.frame)-14, WIDTH+11, 250);
//        home.frame=CGRectMake(105,470, 300,40);
//        txt_street.frame=CGRectMake(25,CGRectGetMaxY(home.frame)+1, WIDTH-50, 38);
//        line6.frame=CGRectMake(25, CGRectGetMaxY(txt_street.frame)-7, WIDTH-50, 0.5);
//        txt_country.frame=CGRectMake(25,CGRectGetMaxY(txt_street.frame)+4, 150, 38);
//        line7.frame=CGRectMake(25, CGRectGetMaxY(txt_country.frame)-7, WIDTH-50, 0.5);
//        img_Dropcountry.frame=CGRectMake(270, CGRectGetMaxY(txt_street.frame)+15, 16, 10);
//        brn_dropbox.frame = CGRectMake(25, CGRectGetMaxY(txt_street.frame)+4, 295, 38);
//        tableview_Hcountry.frame  = CGRectMake(25,CGRectGetMaxY(txt_country.frame),295,200);
//        txt_city.frame=CGRectMake(25,CGRectGetMaxY(txt_country.frame)+4, WIDTH-50, 38);
//        line8.frame=CGRectMake(25,CGRectGetMaxY(txt_city.frame)-7, WIDTH-50, 0.5);
//        img_Dropcity.frame=CGRectMake(270, CGRectGetMaxY(txt_country.frame)+15, 16, 10);
//        dropbox1.frame = CGRectMake(25, CGRectGetMaxY(txt_country.frame)+4, 295, 38);
//        tableview_Hcity.frame=CGRectMake(25, CGRectGetMaxY(txt_city.frame), 295, 200);
//        txt_portal.frame=CGRectMake(25,CGRectGetMaxY(line8.frame)+15, WIDTH-50, 38);
//        line9.frame=CGRectMake(25, CGRectGetMaxY(txt_portal.frame)+-7, WIDTH-50, 0.5);
//        image.frame=CGRectMake(0, CGRectGetMaxY(img_backgroundimage2.frame)+3, WIDTH, 100);
//        //        image.frame=CGRectMake(0, HEIGHT-80, WIDTH, 100);
//        page.frame =CGRectMake(0,0, WIDTH,30);
//        next.frame = CGRectMake(18, 35, WIDTH-36,40);
//        [scroll_view setContentSize:CGSizeMake(0,860)];
        
        scroll_view.frame=CGRectMake(0, 45, WIDTH, HEIGHT);
        img_backgroundimage.frame=CGRectMake(-4, -13 ,WIDTH+12, 500);
        profileimg.frame=CGRectMake(100, 15, 120, 120);
        profileimg_button.frame=CGRectMake(0, 10, 120, 120);
        profileimg.layer.cornerRadius =120/2;
        profileimg.clipsToBounds = YES;
        lbl_info.frame =CGRectMake(90,130, 300,40);
        txt_FirstName.frame=CGRectMake(28,165, WIDTH-50, 38);
        line.frame=CGRectMake(25, CGRectGetMaxY(txt_FirstName.frame)-7, WIDTH-50, 0.5);
        txt_MiddleName.frame=CGRectMake(25,CGRectGetMaxY(txt_FirstName.frame)+4, WIDTH-50, 38);
        line1.frame=CGRectMake(25, CGRectGetMaxY(txt_MiddleName.frame)-7, WIDTH-50, 0.5);
        lbl_option.frame =CGRectMake(245,CGRectGetMaxY(txt_MiddleName.frame)-5, 100,10);
        txt_LastName.frame=CGRectMake(25, CGRectGetMaxY(txt_MiddleName.frame)+4, WIDTH-50, 38);
        line2.frame=CGRectMake(25, CGRectGetMaxY(txt_LastName.frame)-7, WIDTH-50, 0.5);
        
        lbl_Date.frame =CGRectMake(25,CGRectGetMaxY(txt_LastName.frame)+18, 80,20);
        img_DateofBirth.frame=CGRectMake(CGRectGetMaxX(lbl_Date.frame)+10,CGRectGetMaxY(txt_LastName.frame)+10,180,20);
        txt_DateofBirth.frame=CGRectMake(0,0, 210,20);
        
        //        lbl_Date.frame =CGRectMake(25,CGRectGetMaxY(txt_LastName.frame)+10, 100,20);
        //        img_DateofBirth.frame=CGRectMake(CGRectGetMaxX(lbl_Date.frame)+4,CGRectGetMaxY(txt_LastName.frame)+10, 150,20);
        //        txt_DateofBirth.frame=CGRectMake(0,0, 160,20);
        img_Font.frame=CGRectMake(25,CGRectGetMaxY(lbl_Date.frame)+13, 45, 45);
        lbl_imgFont.frame=CGRectMake(5,8, 35, 30);
        fontimg_button.frame=CGRectMake(0,0, 45, 45);
        img_Back.frame=CGRectMake(CGRectGetMaxX(img_Font.frame)+5,CGRectGetMaxY(lbl_Date.frame)+13, 45, 45);
        lbl_imgBack.frame=CGRectMake(5,10, 35, 20);
        backimg_button.frame=CGRectMake(0,0, 45, 45);
        lbl_Nationalid.frame=CGRectMake(CGRectGetMaxX(img_Back.frame)-10, CGRectGetMaxY(lbl_Date.frame)+10, 150, 45);
        img_icon.frame=CGRectMake(260, CGRectGetMaxY(lbl_Date.frame)+30, 30, 25);
        btn_imgicon.frame=CGRectMake(0, 0, 30, 25);
        lin2.frame=CGRectMake(25, CGRectGetMaxY(lbl_Date.frame)+60, WIDTH-50, 0.5);
        lbl_jpgnpg.frame =CGRectMake(250,CGRectGetMaxY(img_icon.frame)+4, 60,10);
        Code.frame=CGRectMake(25,CGRectGetMaxY(lin2.frame)+10, 35, 30);
        img_Drop.frame=CGRectMake(60, CGRectGetMaxY(lbl_jpgnpg.frame)+10, 15, 8);
        dropbox.frame = CGRectMake(25,CGRectGetMaxY(lin2.frame)+10, 65, 30);
        tableview_Hcountrycode.frame=CGRectMake(25, CGRectGetMaxY(Code.frame), 130, 100);
        line4.frame=CGRectMake(25, CGRectGetMaxY(Code.frame)-6, 50, 0.5);
        Mobilenumber.frame=CGRectMake(90,387, 200, 38);
        lin.frame=CGRectMake(90, CGRectGetMaxY(Mobilenumber.frame)-5, 205, 0.5);
        txt_Email.frame=CGRectMake(25,420, WIDTH-55, 38);
        Line5.frame=CGRectMake(25, CGRectGetMaxY(txt_Email.frame)-7, WIDTH-50, 0.5);
        img_backgroundimage2.frame=CGRectMake(-4, CGRectGetMaxY(img_backgroundimage.frame)-14, WIDTH+12, 250);
        home.frame=CGRectMake(105,20, 300,40);
        txt_street.frame=CGRectMake(25,CGRectGetMaxY(home.frame)+1, WIDTH-50, 38);
        line6.frame=CGRectMake(25, CGRectGetMaxY(txt_street.frame)-7, WIDTH-50, 0.5);
        txt_country.frame=CGRectMake(25,CGRectGetMaxY(txt_street.frame)+4, 150, 38);
        line7.frame=CGRectMake(25, CGRectGetMaxY(txt_country.frame)-7, WIDTH-50, 0.5);
        img_Dropcountry.frame=CGRectMake(270, CGRectGetMaxY(txt_street.frame)+15, 16, 10);
        brn_dropbox.frame = CGRectMake(25, CGRectGetMaxY(txt_street.frame)+4, WIDTH-50, 38);
        tableview_Hcountry.frame  = CGRectMake(25,CGRectGetMaxY(txt_country.frame)-200,WIDTH-50,200);
        txt_city.frame=CGRectMake(25,CGRectGetMaxY(txt_country.frame)+4, WIDTH-50, 38);
        line8.frame=CGRectMake(25,CGRectGetMaxY(txt_city.frame)-7, WIDTH-50, 0.5);
        img_Dropcity.frame=CGRectMake(270, CGRectGetMaxY(txt_country.frame)+15, 16, 10);
        dropbox1.frame = CGRectMake(25, CGRectGetMaxY(txt_country.frame)+4, 295, 38);
        tableview_Hcity.frame=CGRectMake(25, CGRectGetMaxY(txt_city.frame)-200, WIDTH-50, 200);
        txt_portal.frame=CGRectMake(25,CGRectGetMaxY(line8.frame)+15, WIDTH-50, 38);
        line9.frame=CGRectMake(25, CGRectGetMaxY(txt_portal.frame)+-7, WIDTH-50, 0.5);
        image.frame=CGRectMake(0, CGRectGetMaxY(img_backgroundimage2.frame)+3, WIDTH, 100);
        //        image.frame=CGRectMake(0, HEIGHT-80, WIDTH, 100);
        page.frame =CGRectMake(0,0, WIDTH,30);
        next.frame = CGRectMake(18, 35, WIDTH-36,40);
        [txt_portal setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
        [txt_portal setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
        [scroll_view setContentSize:CGSizeMake(0,860)];

        
    }
    
    
    //    [scroll_view setContentSize:CGSizeMake(0,800)];
}



#pragma mark Button actions




-(void)Next_btnClick
{
    //
    //   ChefSignup2 *chefsignup2=[[ChefSignup2 alloc]init];
    ////    [self.navigationController pushViewController:chefsignup2 animated:NO];
    //    [self presentViewController:chefsignup2 animated:NO completion:nil];
    //
    
    NSLog(@"click_Loginbtn");
    NSDate *selectedDate =  self->datePicker.date;
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSYearCalendarUnit
                                       fromDate:selectedDate
                                       toDate:now
                                       options:0];
    NSInteger age = [ageComponents year];
    
    
    if ([txt_FirstName.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Please enter FirstName"];
        
    }
    else if ([txt_LastName.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Please enter Lastname"];
    }
    else if([txt_DateofBirth.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Please enter Date of Birth"];
    } else if (age < 18) {
        
        [self popup_Alertview:@"You must be 18 years of age"];
    }
    else if (imgData== nil)
    {
        [self popup_Alertview:@"Please select User Profile Pic"];
    }
    else if (frontData== nil)
    {
        [self popup_Alertview:@"Please select National Id"];
    }
    else if (backData == nil)
    {
        [self popup_Alertview:@"Please select National Id"];
        
    }
    else if ([Code.text isEqualToString:@""])
    {
        Code.text=@"";
        [self popup_Alertview:@"please enter Country code required if puted mobile number"];
    }
    else if([Mobilenumber.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Please enter Mobilenumber"];
    }
    else if ((Mobilenumber.text.length>0&&[Mobilenumber.text length]<5 ))
    {
        Mobilenumber.text=@"";
        [self popup_Alertview:@"Please enter valid mobile number"];
    }
    
    else if (txt_Email.text.length==0)
    {
        [self popup_Alertview:@"Please Enter Email Id"];
    }
    else if(txt_Email.text.length>0 && ![self ValidateEmail:txt_Email.text])
    {
        txt_Email.text=@"";
        [self popup_Alertview:@"Please Enter a Valid Email"];
    }
    else if([txt_street.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Please enter Home Address"];
    }
    else if([txt_country.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Please enter country"];
    }
    else if([txt_city.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Please enter city"];
    }
    else if([txt_portal.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Please enter postal"];
    }
    else if (txt_portal.text.length>6)
    {
        [self popup_Alertview:@"Please Enter postal code six digits"];
    }
    
    else
    {
        [self SignupFirst];
    }
}
#pragma mark - Date Picker
- (void)datePickerValueChanged:(id)sender
{
    [formatter setDateFormat:@"dd/MM/yyyy"];
    //    @"d  d   m  m   y  y  y  y"
    NSString *str=[formatter stringFromDate:datePicker.date];
    
    
    if(IS_IPHONE_6Plus)
    {
        str_DOBfinal = [NSString stringWithFormat:@"  %@     %@          %@     %@        %@      %@     %@      %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
        
    }
    else if(IS_IPHONE_6)
    {
        str_DOBfinal = [NSString stringWithFormat:@"  %@    %@        %@     %@        %@     %@    %@      %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
    }
    else if (IS_IPHONE_5)
    {
        str_DOBfinal = [NSString stringWithFormat:@" %@    %@       %@   %@      %@    %@    %@    %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
    }
    else
    {
        str_DOBfinal = [NSString stringWithFormat:@" %@   %@      %@    %@      %@   %@   %@   %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);

    }
    
    //    str_DOBfinal = [NSString stringWithFormat:@"  %@     %@          %@     %@        %@      %@     %@      %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
    //    NSLog(@"fdyhsag %@",str_DOBfinal);
    
    txt_DateofBirth.font = [UIFont fontWithName:kFont size:14];
    txt_DateofBirth.textColor = [UIColor blackColor];
    
    NSLog(@"gdj%@", [str substringToIndex:2]);
    
    [txt_DateofBirth setText:str_DOBfinal];
    
}

-(void) click_DoneDate:(id) sender
{
    [formatter setDateFormat:@"dd/MM/yyyy"];
    //    @"d  d   m  m   y  y  y  y"
    NSString *str=[formatter stringFromDate:datePicker.date];
    
    if(IS_IPHONE_6Plus)
    {
        str_DOBfinal = [NSString stringWithFormat:@"  %@     %@          %@     %@        %@      %@     %@      %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
        
    }
    else if(IS_IPHONE_6)
    {
        str_DOBfinal = [NSString stringWithFormat:@"  %@    %@        %@     %@        %@     %@    %@      %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
    }
    else if (IS_IPHONE_5)
    {
        str_DOBfinal = [NSString stringWithFormat:@" %@    %@       %@   %@      %@    %@    %@    %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
    }
    else
    {
        str_DOBfinal = [NSString stringWithFormat:@" %@   %@      %@    %@      %@   %@   %@   %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);

    }
    
    //    str_DOBfinal = [NSString stringWithFormat:@"  %@     %@          %@     %@        %@      %@     %@      %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
    //    NSLog(@"fdyhsag %@",str_DOBfinal);
    
    txt_DateofBirth.font = [UIFont fontWithName:kFont size:14];
    txt_DateofBirth.textColor = [UIColor blackColor];
    
    NSLog(@"gdj%@", [str substringToIndex:2]);
    
    [txt_DateofBirth setText:str_DOBfinal];
    [self.view endEditing:YES];
}

-(void)keyboard_reternmethod
{
    [txt_FirstName resignFirstResponder];
    [txt_LastName  resignFirstResponder];
    [txt_MiddleName resignFirstResponder];
    [txt_DateofBirth resignFirstResponder];
    [txt_country resignFirstResponder];
    [txt_Email resignFirstResponder];
    [txt_street resignFirstResponder];
    [txt_country resignFirstResponder];
    [txt_city resignFirstResponder];
    [txt_portal resignFirstResponder];
}

-(void)dropbtn_Code:(UIButton *) sender
{
    
    [self keyboard_reternmethod];
    
 //   [txt_FirstName resignFirstResponder];
    
    tableview_Hcountry.hidden=YES;
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        tableview_Hcountrycode.hidden=NO;
        
        
    }
    else
    {
        [sender setSelected:NO];
        tableview_Hcountrycode.hidden=YES;
    }
    
    
}
-(void)dropbtn_Country:(UIButton *) sender

{
    [self keyboard_reternmethod];
    tableview_Hcity.hidden=YES;
    
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        tableview_Hcountry.hidden=NO;
        
    }
    else
    {
        [sender setSelected:NO];
        tableview_Hcountry.hidden=YES;
    }
    
}



-(void)dropbtn_City:(UIButton *) sender

{
    
    if ([txt_country.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Plaese select country first"];
        
    }
    else{
        [self keyboard_reternmethod];
        tableview_Hcountry.hidden=YES;
        if (![sender isSelected])
        {
            [sender setSelected:YES];
            tableview_Hcity.hidden=NO;
            
            
        }
        else
        {
            [sender setSelected:NO];
            tableview_Hcity.hidden=YES;
        }

    }
    
}
-(void)profileimg_clickbtn:(UIButton *) sender
{
    str_pickingImage = @"Profile";
    
    UIActionSheet *actionSheet_popupQuery = [[UIActionSheet alloc] initWithTitle:@"Select A Image From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Camera", nil];
    actionSheet_popupQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet_popupQuery showInView:self.view];
    
}
-(void)frontimg_clickbtn:(UIButton *) sender
{
    str_pickingImage = @"Front";
    
    UIActionSheet *actionSheet_popupQuery = [[UIActionSheet alloc] initWithTitle:@"Select A Image From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Camera", nil];
    actionSheet_popupQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet_popupQuery showInView:self.view];
    
    
}
-(void)backimg_clickbtn:(UIButton *) sender
{
    str_pickingImage = @"Back";
    
    UIActionSheet *actionSheet_popupQuery = [[UIActionSheet alloc] initWithTitle:@"Select A Image From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Camera", nil];
    actionSheet_popupQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet_popupQuery showInView:self.view];
    
    
}
-(void)image_Clickbtn:(UIButton *) sender
{
    if (frontData==nil)
    {
        str_pickingImage = @"Front";
        
        UIActionSheet *actionSheet_popupQuery = [[UIActionSheet alloc] initWithTitle:@"Select A Image From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Camera", nil];
        actionSheet_popupQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        [actionSheet_popupQuery showInView:self.view];
    }
    else if(backData==nil){
        str_pickingImage = @"Back";
        
        UIActionSheet *actionSheet_popupQuery = [[UIActionSheet alloc] initWithTitle:@"Select A Image From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Camera", nil];
        actionSheet_popupQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        [actionSheet_popupQuery showInView:self.view];
        
        
    }
    
    
}

//-(void)click_BtnTakeTransp:(UIButton *)sender
//{
//
//    }
#pragma mark - ACTION SHEET DELEGATE

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 0)
    {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            UIImagePickerController*imgPicker = [[UIImagePickerController alloc] init];
            UIColor* color = [UIColor colorWithRed:46.0/255 green:127.0/255 blue:244.0/255 alpha:1];
            [imgPicker.navigationBar setTintColor:color];
            imgPicker.delegate = self;
            imgPicker.allowsEditing = YES;
            imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            [self presentViewController:imgPicker animated:NO completion:Nil];
        }
    }
    else if(buttonIndex == 1)
    {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController*imgPicker = [[UIImagePickerController alloc] init];
            UIColor* color = [UIColor colorWithRed:46.0/255 green:127.0/255 blue:244.0/255 alpha:1];
            [imgPicker.navigationBar setTintColor:color];
            imgPicker.delegate = self;
            imgPicker.allowsEditing = YES;
            imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:imgPicker animated:NO completion:Nil];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Not86" message:@"Device Does Not Support Camera" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //  [UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
            [alert show];
        }
    }
}

#pragma mark - IMAGEPICKER DELEGATE METHODS
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *imageOriginal =  [info objectForKey:UIImagePickerControllerEditedImage];
    
    if ( [[NSString stringWithFormat:@"%@",str_pickingImage]isEqualToString:@"Profile"])
    {
        imgData =  UIImageJPEGRepresentation(imageOriginal,0.5);
        profileimg.image = imageOriginal;
        
    }
    else if ([[NSString stringWithFormat:@"%@",str_pickingImage]isEqualToString:@"Front"])
    {
        frontData =  UIImageJPEGRepresentation(imageOriginal,0.5);
        img_Font.image = imageOriginal;
        lbl_imgFont.hidden = YES;
        
    }
    else
    {
        backData =  UIImageJPEGRepresentation(imageOriginal,0.5);
        img_Back.image = imageOriginal;
        lbl_imgBack.hidden = YES;
        
    }
    
    [picker dismissViewControllerAnimated:YES completion:Nil];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:Nil];
}





#pragma mark Validate Email

-(BOOL)ValidateEmail :(NSString *) strToEvaluate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL b = [emailTest evaluateWithObject:strToEvaluate];
    return b;
}
- (NSMutableArray *)validateEmailWithString:(NSString*)emails
{
    NSMutableArray *validEmails = [[NSMutableArray alloc] init];
    NSArray *emailArray = [emails componentsSeparatedByString:@","];
    for (NSString *email in emailArray)
    {
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        if ([emailTest evaluateWithObject:email])
            [validEmails addObject:email];
    }
    return validEmails;
}



#pragma mark TextField Delegate methods

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField==Mobilenumber)
    {
        NSString *stringVal = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered =[[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        if (stringVal.length<11 && [string isEqualToString:filtered])
        {
            return YES;
        }
        else{
            return NO;
        }
        return [string isEqualToString:filtered];
    }else if (textField == txt_portal)
    {
        NSString *stringVal = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered =[[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        if (stringVal.length<7 && [string isEqualToString:filtered])
        {
            return YES;
        }
        
        else{
            return NO;
        }
        return [string isEqualToString:filtered];
        
    }else if (textField==txt_Email)
    {
        textField.text = [txt_Email.text stringByReplacingCharactersInRange:range withString:[string lowercaseString]];
        return NO;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}




#pragma mark Alertview Popup


-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}

#pragma mark UITableview methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tableview_Hcountry)
    {
        return [ary_countrylist count];
        
    }
    else if(tableView == tableview_Hcity){
        return [ary_citylist count];
    }
    else if (tableView == tableview_Hcountrycode)
    {
        return [ary_countrycodelist count];
    }
    else{
        return 0;
    }
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"LazyTableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor=[UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    
    if (tableView==tableview_Hcountry)
    {
        UILabel  * lbl_list = [[UILabel alloc]init];
        lbl_list.frame=CGRectMake(5, 0, 150, 30);
        lbl_list.backgroundColor=[UIColor clearColor];
        lbl_list.textColor=[UIColor blackColor];
        lbl_list.font = [UIFont fontWithName:kFont size:15];
        lbl_list.text=[[ary_countrylist objectAtIndex:indexPath.row] valueForKey:@"Country"];
        [cell.contentView addSubview:lbl_list];
        
        
    }
    else if(tableView == tableview_Hcity)
    {
        UILabel  * lbl_list = [[UILabel alloc]init];
        lbl_list.frame=CGRectMake(5, 0, 150, 30);
        lbl_list.backgroundColor=[UIColor clearColor];
        lbl_list.textColor=[UIColor blackColor];
        lbl_list.font = [UIFont fontWithName:kFont size:15];
        lbl_list.text=[[ary_citylist objectAtIndex:indexPath.row] valueForKey:@"City"];
        [cell.contentView addSubview:lbl_list];
        
        
    }
    else if (tableView == tableview_Hcountrycode)
    {
        UILabel  * lbl_countrylist = [[UILabel alloc]init];
        lbl_countrylist.frame=CGRectMake(3, 0, 110, 30);
        lbl_countrylist.backgroundColor=[UIColor clearColor];
        lbl_countrylist.textColor=[UIColor blackColor];
        lbl_countrylist.font = [UIFont fontWithName:kFont size:13];
        lbl_countrylist.text=[[ary_countrycodelist objectAtIndex:indexPath.row] valueForKey:@"code"];
        [cell.contentView addSubview:lbl_countrylist];
        
        
    }
    else{
        
        
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if (tableView==tableview_Hcountry)
    {
        txt_country.font = [UIFont fontWithName:kFont size:15];
        
        txt_country.text =[[ary_countrylist objectAtIndex:indexPath.row] valueForKey:@"Country"];
        str_countrycodetosend =[[ary_countrylist objectAtIndex:indexPath.row] valueForKey:@"ID"];

        txt_city.text =@"";
        
        [tableview_Hcountry setHidden:YES];
        [self cityList];
        
        
    }else if (tableView == tableview_Hcity){
        txt_city.font = [UIFont fontWithName:kFont size:15];
        
        txt_city.text =[[ary_citylist objectAtIndex:indexPath.row] valueForKey:@"City"];
        [tableview_Hcity setHidden:YES];
        
        
    }
    else if (tableView == tableview_Hcountrycode)
    {
        Code.font = [UIFont fontWithName:kFont size:9];
        Code.text =[[ary_countrycodelist objectAtIndex:indexPath.row] valueForKey:@"code"];
        [tableview_Hcountrycode setHidden:YES];
        CGSize constraint1 = CGSizeMake(210 ,10000.0f);
        CGSize chatText_size1 = [Code.text  sizeWithFont:[UIFont fontWithName:kFont size:9.0f] constrainedToSize:constraint1 lineBreakMode:NSLineBreakByWordWrapping];
        
        if (IS_IPHONE_6Plus)
        {
//            Code.frame=CGRectMake(25,CGRectGetMaxY(lbl_jpgnpg.frame)+13, chatText_size1.width, 30);
//            img_Drop.frame=CGRectMake(chatText_size1.width+20, CGRectGetMaxY(lbl_jpgnpg.frame)+20, 15, 10);
//            dropbox.frame = CGRectMake(chatText_size1.width-20, CGRectGetMaxY(lbl_jpgnpg.frame)+13, 70, 30);
//            Mobilenumber.frame=CGRectMake(chatText_size1.width+40,CGRectGetMaxY(lbl_jpgnpg.frame)+8, WIDTH-chatText_size1.width+40, 38);
//            line4.frame=CGRectMake(25, CGRectGetMaxY(Code.frame)-6, 50, 0.5);
//            tableview_Hcountrycode.frame=CGRectMake(25, CGRectGetMaxY(lbl_jpgnpg.frame)+13, 120, 100);
//            lin.frame=CGRectMake(85, CGRectGetMaxY(Mobilenumber.frame)-8, 300, 0.5);
//            txt_Email.frame=CGRectMake(25,CGRectGetMaxY(Mobilenumber.frame)+5, WIDTH-55, 38);
//            Line5.frame=CGRectMake(25,CGRectGetMaxY(txt_Email.frame)+1, WIDTH-55, 38);
            
            Code.frame=CGRectMake(25,CGRectGetMaxY(lbl_jpgnpg.frame)+13, chatText_size1.width, 30);
            dropbox.frame = CGRectMake(25, CGRectGetMaxY(lbl_jpgnpg.frame)+13, chatText_size1.width+20, 30);
            img_Drop.frame=CGRectMake(CGRectGetMaxX(Code.frame)+2, CGRectGetMaxY(lbl_jpgnpg.frame)+20, 15, 10);
            Mobilenumber.frame=CGRectMake(CGRectGetMaxX(img_Drop.frame)+5,CGRectGetMaxY(lbl_jpgnpg.frame)+8, WIDTH-(Code.frame.size.width+70), 38);
            tableview_Hcountrycode.frame=CGRectMake(25, CGRectGetMaxY(Code.frame), 120, 120);
            line4.frame=CGRectMake(25, CGRectGetMaxY(Code.frame)-6, chatText_size1.width, 0.5);
            lin.frame=CGRectMake(CGRectGetMaxX(img_Drop.frame)+5, CGRectGetMaxY(Mobilenumber.frame)-8, WIDTH-(Code.frame.size.width+70), 0.5);
            txt_Email.frame=CGRectMake(25,CGRectGetMaxY(Mobilenumber.frame)+5, WIDTH-55, 38);
            Line5.frame=CGRectMake(25,CGRectGetMaxY(txt_Email.frame)+1, WIDTH-55, 1);
            

            
        }
        else if (IS_IPHONE_6)
        {
            
//            Code.frame=CGRectMake(25,CGRectGetMaxY(lbl_jpgnpg.frame)+13, chatText_size1.width, 30);
//            dropbox.frame = CGRectMake(chatText_size1.width-25, CGRectGetMaxY(lbl_jpgnpg.frame)+13, 70, 30);
//            img_Drop.frame=CGRectMake(chatText_size1.width+20, CGRectGetMaxY(lbl_jpgnpg.frame)+20, 15, 10);
//            Mobilenumber.frame=CGRectMake(chatText_size1.width+40,CGRectGetMaxY(lbl_jpgnpg.frame)+8, WIDTH-chatText_size1.width+40, 38);
//            tableview_Hcountrycode.frame=CGRectMake(25, CGRectGetMaxY(Code.frame), 120, 100);
//            line4.frame=CGRectMake(25, CGRectGetMaxY(Code.frame)-6, chatText_size1.width, 0.5);
//            lin.frame=CGRectMake(85, CGRectGetMaxY(Mobilenumber.frame)-8, 275, 0.5);
//           txt_Email.frame=CGRectMake(25,CGRectGetMaxY(Mobilenumber.frame)+5, WIDTH-55, 38);
//            Line5.frame=CGRectMake(25,CGRectGetMaxY(txt_Email.frame)+1, WIDTH-55, 38);
            
            Code.frame=CGRectMake(25,CGRectGetMaxY(lbl_jpgnpg.frame)+13, chatText_size1.width, 30);
            dropbox.frame = CGRectMake(25, CGRectGetMaxY(lbl_jpgnpg.frame)+13, chatText_size1.width+20, 30);
            img_Drop.frame=CGRectMake(CGRectGetMaxX(Code.frame)+2, CGRectGetMaxY(lbl_jpgnpg.frame)+20, 15, 10);
            Mobilenumber.frame=CGRectMake(CGRectGetMaxX(img_Drop.frame)+5,CGRectGetMaxY(lbl_jpgnpg.frame)+8, WIDTH-(Code.frame.size.width+70), 38);
            tableview_Hcountrycode.frame=CGRectMake(25, CGRectGetMaxY(Code.frame), 120, 120);
            line4.frame=CGRectMake(25, CGRectGetMaxY(Code.frame)-6, chatText_size1.width, 0.5);
            lin.frame=CGRectMake(CGRectGetMaxX(img_Drop.frame)+5, CGRectGetMaxY(Mobilenumber.frame)-8, WIDTH-(Code.frame.size.width+70), 0.5);
            txt_Email.frame=CGRectMake(25,CGRectGetMaxY(Mobilenumber.frame)+5, WIDTH-55, 38);
            Line5.frame=CGRectMake(25,CGRectGetMaxY(txt_Email.frame)+1, WIDTH-55, 1);
            

            
            
        }
        
       else if (IS_IPHONE_5)
           
       {
        
           Code.frame=CGRectMake(25,CGRectGetMaxY(lbl_jpgnpg.frame)+13, chatText_size1.width, 30);
           dropbox.frame = CGRectMake(25, CGRectGetMaxY(lbl_jpgnpg.frame)+13, chatText_size1.width+20, 30);
           img_Drop.frame=CGRectMake(CGRectGetMaxX(Code.frame)+2, CGRectGetMaxY(lbl_jpgnpg.frame)+20, 15, 10);
           Mobilenumber.frame=CGRectMake(CGRectGetMaxX(img_Drop.frame)+5,CGRectGetMaxY(lbl_jpgnpg.frame)+8, WIDTH-(Code.frame.size.width+70), 38);
           tableview_Hcountrycode.frame=CGRectMake(25, CGRectGetMaxY(Code.frame), 120, 120);
           line4.frame=CGRectMake(25, CGRectGetMaxY(Code.frame)-6, chatText_size1.width, 0.5);
           lin.frame=CGRectMake(CGRectGetMaxX(img_Drop.frame)+5, CGRectGetMaxY(Mobilenumber.frame)-8, WIDTH-(Code.frame.size.width+70), 0.5);
           txt_Email.frame=CGRectMake(25,CGRectGetMaxY(Mobilenumber.frame)+5, WIDTH-55, 38);
           Line5.frame=CGRectMake(25,CGRectGetMaxY(txt_Email.frame)+1, WIDTH-55, 1);
        
        
        
        
        
      }
     else
     {
         
         Code.frame=CGRectMake(25,CGRectGetMaxY(lbl_jpgnpg.frame)+13, chatText_size1.width, 30);
         dropbox.frame = CGRectMake(25, CGRectGetMaxY(lbl_jpgnpg.frame)+13, chatText_size1.width+20, 30);
         img_Drop.frame=CGRectMake(CGRectGetMaxX(Code.frame)+2, CGRectGetMaxY(lbl_jpgnpg.frame)+20, 15, 10);
         Mobilenumber.frame=CGRectMake(CGRectGetMaxX(img_Drop.frame)+5,CGRectGetMaxY(lbl_jpgnpg.frame)+8, WIDTH-(Code.frame.size.width+70), 38);
         tableview_Hcountrycode.frame=CGRectMake(25, CGRectGetMaxY(Code.frame), 120, 120);
         line4.frame=CGRectMake(25, CGRectGetMaxY(Code.frame)-6, chatText_size1.width, 0.5);
         lin.frame=CGRectMake(CGRectGetMaxX(img_Drop.frame)+5, CGRectGetMaxY(Mobilenumber.frame)-8, WIDTH-(Code.frame.size.width+70), 0.5);
         txt_Email.frame=CGRectMake(25,CGRectGetMaxY(Mobilenumber.frame)+5, WIDTH-55, 38);
         Line5.frame=CGRectMake(25,CGRectGetMaxY(txt_Email.frame)+1, WIDTH-55,1);
     }

        
    }
    else
    {
        
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tableview_Hcountry) {
        return 20;
    }
    else if (tableView == tableview_Hcity){
        return 20;
    }else{
        return 20;
    }
    return 0;
}



# pragma mark ChefSignupFirst method



-(void)SignupFirst
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
    }
    
    [formatter setDateFormat:@"dd-MM-yyyy"];
   // [txt_DateofBirth setText:[formatter stringFromDate:datePicker.date]];
    
    NSString*str_dob =[formatter stringFromDate:datePicker.date];
    
    NSDictionary *params =@{
                            @"field_first_name"                  :   txt_FirstName.text,
                            @"uid"                               :[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"field_middle_name"                 :   txt_MiddleName.text,
                            @"field_last_name"                   :   txt_LastName.text,
                            @"field_mobile_code_number"          :   Code.text,
                            @"field_mobile_number"               :   Mobilenumber.text,
                            @"field_email_address"               :  txt_Email.text,
                            @"field_date_of_birth"               :  str_dob,
                            @"home_address"                      :  txt_street.text,
                            @"hcountry_name"                     :  str_countrycodetosend,
                            @"hcity_name"                        :  txt_city.text,
                            @"hpostal_code"                      :  txt_portal.text,
                            @"registration_page"                 : @"1",
                            @"role_type"                         :  @"chef_profile",
                            @"device_udid"                       :  UniqueAppID,
                            @"device_token"                      :  @"Dev",
                            @"device_type"                       :  @"1"
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = nil;
    
    if (frontData!= nil && backData != nil && imgData!= nil)
    {
        request = [httpClient multipartFormRequestWithMethod:@"POST" path:AFChefSignUpFirst parameters:params constructingBodyWithBlock:^(id <AFMultipartFormData>formData)
                   {
                       NSTimeInterval timeInterval = [NSDate timeIntervalSinceReferenceDate];
                       
                       [formData appendPartWithFileData: imgData name:@"profile_img" fileName:[NSString stringWithFormat:@"%lf-image.png",timeInterval] mimeType:@"image/jpeg"];
                       
                       [formData appendPartWithFileData: frontData name:@"front_id_img" fileName:[NSString stringWithFormat:@"%lf-Frontimage.png",timeInterval] mimeType:@"image/jpeg"];
                       
                       [formData appendPartWithFileData: backData name:@"back_id_img" fileName:[NSString stringWithFormat:@"%lf-Backimage.png",timeInterval] mimeType:@"image/jpeg"];
                       
                   }];
    }
    else
    {
        request = [httpClient requestWithMethod:@"POST"
                                           path:AFChefSignUpFirst
                                     parameters:params];
    }
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpFirst:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self SignupFirst];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpFirst :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        
        ChefSignup2 *chefsignup2=[[ChefSignup2 alloc]init];
        chefsignup2.street_valve=txt_street.text;
        chefsignup2.country_name=txt_country.text;
        chefsignup2.city_name=txt_city.text;
        chefsignup2.portal_code=txt_portal.text;
        
        [self presentViewController:chefsignup2 animated:NO completion:nil];
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        txt_FirstName.text=@"";
        txt_MiddleName.text=@"";
        txt_LastName.text=@"";
        Code.text=@"";
        Mobilenumber.text=@"";
        txt_Email.text=@"";
        txt_street.text=@"";
        txt_country.text=@"";
        txt_city.text=@"";
        txt_portal.text=@"";
        
    }
    
}


# pragma mark Hcountry method

-(void)countryList
{
    
    //    [self.view addSubview:delegate.activityIndicator];
    //    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpHcountry
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpHcountry:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self countryList];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpHcountry :(NSDictionary * )TheDict
{
    [ary_countrylist removeAllObjects];
    [ary_countrycodelist removeAllObjects];
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
       
        
        for (int j=0; j<[[TheDict valueForKey:@"CountryMobileCode"] count]; j++) {
            [ary_countrycodelist addObject:[[TheDict valueForKey:@"CountryMobileCode"] objectAtIndex:j]];
            
        }
        
        
        for (int i=0; i<[[TheDict valueForKey:@"Country_CityList"] count]; i++)
        {
            [ary_countrylist addObject:[[TheDict valueForKey:@"Country_CityList"] objectAtIndex:i]];
            
        }
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        txt_country.text=@"";
        Code.text=@"";
        
        
    }
    
    [tableview_Hcountry reloadData];
    [tableview_Hcountrycode reloadData];
    
}

-(void)click_Done
{
    [Mobilenumber resignFirstResponder];
    [txt_portal resignFirstResponder];
    
}



# pragma mark Hcity method

-(void)cityList
{
    
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            
                            
                            @"country_name"            :  txt_country.text,
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpHcity
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [self ResponseChefSignUpHcity:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self cityList];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpHcity :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    [ary_citylist removeAllObjects];
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        for (int i=0; i<[[TheDict valueForKey:@"City_List"] count]; i++)
        {
            [ary_citylist addObject:[[TheDict valueForKey:@"City_List"] objectAtIndex:i]];
        }
        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        txt_city.text=@"";
        
        
    }
    
    [tableview_Hcity reloadData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
