//
//  ChefSignup2.m
//  Not86
//
//  Created by Interwld on 8/12/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ChefSignup2.h"
#import "Chefsignup3.h"
#import "AppDelegate.h"
#import "Define.h"
#import "ChefSignup.h"
#import <AssetsLibrary/AssetsLibrary.h>
//#import "CLImageEditor.h"
//#import "GKImagePicker.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"

@interface ChefSignup2 ()<UITextFieldDelegate,UITextViewDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
{
    UIScrollView *scrollview;
    UIImageView *img_topbar;
    UIImageView *img_BackgroundImg;
    UITextField *txt_Name;
    UITextField *txt_street;
    UITextField *txt_country;
    UITextField *txt_city;
    UITextField *txt_postolcode;
    AppDelegate *delegate;
    UITableView *tableview_Kcountry;
    UITableView *tableview_Kcity;
    UIView *alertviewBg;
    UIToolbar *keyboardToolbar_Date;
    CGFloat	animatedDistance;
    NSMutableArray*ary_countrylist;
    UITextView *txt_view;
    UITextView *txt_view1;
    
    NSMutableArray*ary_citylist;
    UIImageView *profileimg;
    UIImageView *img_Addpicture;
    NSData*imgData;
    NSData *img_Data;
    NSString*str_pickingImage;
    UIImageView *img_icon;
    
    UIImageView *img_backGnd;
    UICollectionViewFlowLayout *layout;
    UICollectionView *collView_serviceDirectory;
    UIImageView *img_backGnd2;
    
    int int_SelectedVideo;
    
    NSMutableArray *arr_SelectedIndex;
    NSMutableArray *arry_imageArry;
    NSMutableArray*arr_particularindex;
    NSMutableArray*arr_data;
    NSMutableArray*arry_AddProductImageArry;
    NSMutableArray*Arr_temp;
    
    
    
    
    
    UIImageView *img_productImg0;
    UIImageView *img_productImg1;
    UIImageView *img_productImg2;
    UIImageView *img_productImg3;
    UIImageView *img_productImg4;
    UIImageView *img_productImg5;
    UIImageView *img_productImg6;
    UIImageView *img_productImg7;
    UIImageView *img_productImg8;
    UIImageView *img_productImg9;
    UIImageView *img_productImg10;
    UIImageView *img_productImg11;
    
    int a0 ,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11;
    
    BOOL clear_data;
    BOOL selectvideo;
    int selectedindex;
    int selected_position;
    int int_ImageIndex;
    
    NSData*Data_video_thumb;
    NSData *data_Video;
    
    UILabel  * lbl_char;
    UILabel  * lbl_max;
    
}

@end

@implementation ChefSignup2
@synthesize street_valve;
@synthesize country_name;
@synthesize city_name;
@synthesize portal_code;

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    int_SelectedVideo=-1;
    ary_countrylist = [NSMutableArray new];
    ary_citylist = [NSMutableArray new];
    arry_AddProductImageArry = [NSMutableArray new];
    Arr_temp=[[NSMutableArray alloc]init];
    selectvideo=YES;
    selectedindex=0;
    selected_position=0;
    Data_video_thumb=[[NSData alloc]init];
    arr_SelectedIndex=[[NSMutableArray alloc]initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",nil];
    arr_particularindex=[[NSMutableArray alloc]init];
    arr_data=[[NSMutableArray alloc]init];
    arry_imageArry=[[NSMutableArray alloc]init];
    
    a0=0;
    a1=0;
    a2=0;
    a3=0;
    a4=0;
    a5=0;
    a6=0;
    a7=0;
    a8=0;
    a9=0;
    a10=0;
    // a11=0;
    
    UIImage *image=[UIImage new];
    image=[UIImage imageNamed:@"rectangle-img@2x.png"];
    clear_data=YES;
    
    for (int i=0; i<11; i++)
    {
        [arry_imageArry addObject:image];
        
        NSData *data=[[NSData alloc]init];
        [arr_data addObject:data];
        
        
    }
    
    for (int i=0; i<11; i++)
        
    {
        [arry_AddProductImageArry addObject:@""];
    }
    
    
    NSLog(@"arry :-  %@",arry_imageArry);
    
    // Do any additional setup after loading the view.
    
    [self IntegrateHeaderDesign];
    [self IntegrateBodyDesign];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [alertviewBg removeFromSuperview];
    
    
    if (clear_data==YES)
    {
        [arry_imageArry removeAllObjects];
        [arr_particularindex removeAllObjects];
        [Arr_temp removeAllObjects];
        a0=0;
        a1=0;
        a2=0;
        a3=0;
        a4=0;
        a5=0;
        a6=0;
        a7=0;
        a8=0;
        a9=0;
        a10=0;
        // a11=0;
        selectvideo = YES;
        UIImage *image=[UIImage new];
        image=[UIImage imageNamed:@"box-top@2x.png"];
        
        for (int i=0; i<11; i++)
        {
            [arry_imageArry addObject:image];
            NSData *data=[[NSData alloc]init];
            [arr_data addObject:data];
            
            
        }
        [collView_serviceDirectory reloadData];
        clear_data=NO;
        
    }
    else if(clear_data==NO)
    {
        
    }
    
}
-(void)IntegrateHeaderDesign{
    
    
    
    
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    [self countryList];
}
-(void)Back_btnClick{
    //    [self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
    
    
}

-(void)IntegrateBodyDesign
{
    
    img_topbar=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,45)];
    [img_topbar setUserInteractionEnabled:YES];
    img_topbar.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:img_topbar];
    
    
    UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(12, 16, 15, 15)];
    [img_back setUserInteractionEnabled:YES];
    img_back.backgroundColor=[UIColor clearColor];
    img_back.image=[UIImage imageNamed:@"arrow@2x.png"];
    [img_topbar addSubview:img_back];
    
    UILabel  * lbl_heading = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(img_back.frame)+20,0, 220,45)];
    lbl_heading.text = @"Chef Application";
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:kFont size:15];
    [img_topbar addSubview:lbl_heading];
    
    UIButton *btn_back = [[UIButton alloc] init];
    btn_back.frame = CGRectMake(0, 0, 60,35);
    btn_back.backgroundColor = [UIColor clearColor];
    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_topbar addSubview:btn_back];
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 7, 30, 30)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-login-logo@2x.png"];
    [img_topbar addSubview:img_logo];
    
    
    scrollview=[[UIScrollView alloc]init];
    [scrollview setShowsVerticalScrollIndicator:NO];
    scrollview.delegate = self;
    scrollview.scrollEnabled = YES;
    scrollview.showsVerticalScrollIndicator = NO;
    [scrollview setUserInteractionEnabled:YES];
    scrollview.backgroundColor = [UIColor colorWithRed:241/255.0f green:244/255.0f blue:242/255.0f alpha:1];
    [scrollview setContentSize:CGSizeMake(0,HEIGHT)];
    [self.view addSubview:scrollview];
    
    
    UIImageView *img_backgroundimage=[[UIImageView alloc]init];
    [img_backgroundimage setUserInteractionEnabled:YES];
    img_backgroundimage.backgroundColor=[UIColor clearColor];
    img_backgroundimage.image=[UIImage imageNamed:@"bg1-img@2x.png"];
    [scrollview addSubview:img_backgroundimage];
    
    
    
    UILabel  * lbl_info = [[UILabel alloc]init];
    lbl_info.text = @"Kitchen Information";
    lbl_info.backgroundColor=[UIColor clearColor];
    lbl_info.textColor=[UIColor blackColor];
    lbl_info.userInteractionEnabled=YES;
    
    lbl_info.font = [UIFont fontWithName:kFontBold size:14];
    [scrollview addSubview:lbl_info];
    
    txt_Name = [[UITextField alloc] init];
    txt_Name.borderStyle = UITextBorderStyleNone;
    txt_Name.font = [UIFont fontWithName:kFont size:13];
    txt_Name.placeholder = @"Name of your Kitchen";
    [txt_Name setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Name setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Name.leftViewMode = UITextFieldViewModeAlways;
    txt_Name.userInteractionEnabled=YES;
    txt_Name.textAlignment = NSTextAlignmentLeft;
    txt_Name.backgroundColor = [UIColor clearColor];
    txt_Name.keyboardType = UIKeyboardTypeAlphabet;
    txt_Name.delegate = self;
    [scrollview addSubview:txt_Name];
    
    
    UIImageView *line=[[UIImageView alloc]init];
    [line setUserInteractionEnabled:YES];
    line.backgroundColor=[UIColor clearColor];
    line.image=[UIImage imageNamed:@"line1.png"];
    [img_BackgroundImg addSubview:line];
    [scrollview addSubview:line];
    
    UILabel  * lbl_option = [[UILabel alloc]init] ;
    lbl_option.text = @"(ie Jeff's gourmet burgers, Gino's pizzas)";
    lbl_option.backgroundColor=[UIColor clearColor];
    lbl_option.textColor=[UIColor blackColor];
    lbl_option.userInteractionEnabled=YES;
    
    lbl_option.font = [UIFont fontWithName:kFont size:8];
    [scrollview addSubview:lbl_option];
    
    UILabel  * lbl_kitchen = [[UILabel alloc]init];
    lbl_kitchen.text = @"Kitchen Address";
    lbl_kitchen.backgroundColor=[UIColor clearColor];
    lbl_kitchen.textColor=[UIColor blackColor];
    lbl_kitchen.font = [UIFont fontWithName:kFontBold size:14];
    [scrollview addSubview:lbl_kitchen];
    
    UILabel  * lbl_small = [[UILabel alloc]init] ;
    lbl_small.text = @"(if different from home)";
    lbl_small.backgroundColor=[UIColor clearColor];
    lbl_small.textColor=[UIColor blackColor];
    lbl_small.font = [UIFont fontWithName:kFontBold size:10];
    [scrollview addSubview:lbl_small];
    
    txt_street = [[UITextField alloc] init];
    txt_street.borderStyle = UITextBorderStyleNone;
    txt_street.font = [UIFont fontWithName:kFont size:13];
    txt_street.placeholder = @"Street Address";
    [txt_street setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_street setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_street.leftViewMode = UITextFieldViewModeAlways;
    txt_street.userInteractionEnabled=YES;
    txt_street.textAlignment = NSTextAlignmentLeft;
    txt_street.backgroundColor = [UIColor clearColor];
    txt_street.keyboardType = UIKeyboardTypeAlphabet;
    txt_street.delegate=self;
    txt_street.text=street_valve;
    [scrollview addSubview:txt_street];
    
    UIImageView *line6=[[UIImageView alloc]init];
    [line6 setUserInteractionEnabled:YES];
    line6.backgroundColor=[UIColor clearColor];
    line6.image=[UIImage imageNamed:@"line1.png"];
    [scrollview addSubview:line6];
    
    txt_country = [[UITextField alloc] init];
    txt_country.borderStyle = UITextBorderStyleNone;
    txt_country.font = [UIFont fontWithName:kFont size:13];
    txt_country.placeholder = @"Country";
    [txt_country setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_country setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_country.leftViewMode = UITextFieldViewModeAlways;
    txt_country.userInteractionEnabled=YES;
    txt_country.textAlignment = NSTextAlignmentLeft;
    txt_country.backgroundColor = [UIColor clearColor];
    txt_country.keyboardType = UIKeyboardTypeAlphabet;
    txt_country.delegate = self;
    txt_country.text=country_name;
    [scrollview addSubview:txt_country];
    
    UIImageView *line7=[[UIImageView alloc]init];
    [line7 setUserInteractionEnabled:YES];
    line7.backgroundColor=[UIColor clearColor];
    line7.image=[UIImage imageNamed:@"line1.png"];
    [scrollview addSubview:line7];
    
    UIImageView *dropboximg_country=[[UIImageView alloc]init];
    [dropboximg_country setUserInteractionEnabled:YES];
    dropboximg_country.backgroundColor=[UIColor clearColor];
    dropboximg_country.image=[UIImage imageNamed:@"drop down.png"];
    [scrollview addSubview:dropboximg_country];
    
    
    UIButton *dropbox = [[UIButton alloc] init];
    dropbox.backgroundColor = [UIColor clearColor];
    //    [dropbox setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [dropbox setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [dropbox addTarget:self action:@selector(dropboxbtn_country:) forControlEvents:UIControlEventTouchUpInside ];
    [scrollview addSubview:dropbox];
    
    
    
    txt_city = [[UITextField alloc] init];
    txt_city.borderStyle = UITextBorderStyleNone;
    txt_city.font = [UIFont fontWithName:kFont size:13];
    txt_city.placeholder = @"City";
    [txt_city setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_city setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_city.leftViewMode = UITextFieldViewModeAlways;
    txt_city.userInteractionEnabled=YES;
    txt_city.textAlignment = NSTextAlignmentLeft;
    txt_city.backgroundColor = [UIColor clearColor];
    txt_city.keyboardType = UIKeyboardTypeAlphabet;
    txt_city.delegate = self;
    txt_city.text=city_name;
    [scrollview addSubview:txt_city];
    
    UIImageView *line8=[[UIImageView alloc]init];
    [line8 setUserInteractionEnabled:YES];
    line8.backgroundColor=[UIColor clearColor];
    line8.image=[UIImage imageNamed:@"line1.png"];
    [scrollview addSubview:line8];
    
    UIImageView *dropboximg_City=[[UIImageView alloc]init];
    [dropboximg_City setUserInteractionEnabled:YES];
    dropboximg_City.backgroundColor=[UIColor clearColor];
    dropboximg_City.image=[UIImage imageNamed:@"drop down.png"];
    [scrollview addSubview:dropboximg_City];
    
    
    UIButton *dropbox2 = [[UIButton alloc] init];
    dropbox2.backgroundColor = [UIColor clearColor];
    //    [dropbox2 setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [dropbox2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [dropbox2 addTarget:self action:@selector(dropboxbtn_city:) forControlEvents:UIControlEventTouchUpInside ];
    [scrollview addSubview:dropbox2];
    
    
    txt_postolcode = [[UITextField alloc] init];
    txt_postolcode.borderStyle = UITextBorderStyleNone;
    txt_postolcode.font = [UIFont fontWithName:kFont size:13];
    txt_postolcode.placeholder = @"Postal Code";
    [txt_postolcode setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_postolcode setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_postolcode.leftViewMode = UITextFieldViewModeAlways;
    txt_postolcode.userInteractionEnabled=YES;
    txt_postolcode.textAlignment = NSTextAlignmentLeft;
    txt_postolcode.backgroundColor = [UIColor clearColor];
    txt_postolcode.keyboardType = UIKeyboardTypeNumberPad;
    txt_postolcode.delegate = self;
    txt_postolcode.text=portal_code;
    [scrollview addSubview:txt_postolcode];
    
    if (keyboardToolbar_Date == nil)
    {
        keyboardToolbar_Date = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, IS_IPHONE_5?45:45)];
        [keyboardToolbar_Date setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_Done)];
        [keyboardToolbar_Date setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    txt_postolcode.inputAccessoryView = keyboardToolbar_Date;
    
    UIImageView *line9=[[UIImageView alloc]init];
    [line9 setUserInteractionEnabled:YES];
    line9.backgroundColor=[UIColor clearColor];
    line9.image=[UIImage imageNamed:@"line1.png"];
    [scrollview addSubview:line9];
    
    UILabel  * lbl_addpicture = [[UILabel alloc]init];
    lbl_addpicture.text = @"Add a picture of your cooking area\nand preparation area ";
    lbl_addpicture.numberOfLines = 0;
    lbl_addpicture.backgroundColor=[UIColor clearColor];
    lbl_addpicture.textColor=[UIColor blackColor];
    lbl_addpicture.font = [UIFont fontWithName:kFont size:14];
    [scrollview addSubview:lbl_addpicture];
    
    
    UILabel  * lbl_imagesvideo = [[UILabel alloc]init] ;
    lbl_imagesvideo.text = @"(upto 10 images or 1 video)";
    lbl_imagesvideo.backgroundColor=[UIColor clearColor];
    lbl_imagesvideo.textColor=[UIColor blackColor];
    lbl_imagesvideo.numberOfLines = 0;
    lbl_imagesvideo.font = [UIFont fontWithName:kFont size:10];
    [scrollview addSubview:lbl_imagesvideo];
    
    
    
    
    
    layout=[[UICollectionViewFlowLayout alloc] init];
    collView_serviceDirectory = [[UICollectionView alloc] initWithFrame:CGRectMake(30,0,WIDTH-65,70)
                                                   collectionViewLayout:layout];
    
    
    
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_serviceDirectory setDataSource:self];
    [collView_serviceDirectory setDelegate:self];
    collView_serviceDirectory.scrollEnabled = YES;
    collView_serviceDirectory.showsVerticalScrollIndicator = NO;
    collView_serviceDirectory.showsHorizontalScrollIndicator = NO;
    collView_serviceDirectory.pagingEnabled = NO;
    [collView_serviceDirectory registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_serviceDirectory setBackgroundColor:[UIColor clearColor]];
    layout.minimumInteritemSpacing = 2;
    layout.minimumLineSpacing = 0;
    collView_serviceDirectory.userInteractionEnabled = YES;
    [scrollview addSubview:collView_serviceDirectory];
    
    
    
    
    
    img_Addpicture=[[UIImageView alloc]init];
    [img_Addpicture setUserInteractionEnabled:YES];
    img_Addpicture.backgroundColor=[UIColor clearColor];
    img_Addpicture.image=[UIImage imageNamed:@"img-back@2x.png"];
    [img_Addpicture setContentMode:UIViewContentModeScaleAspectFill];
    img_Addpicture.clipsToBounds=YES;
    //[scrollview addSubview:img_Addpicture];
    
    
    UIButton *btn_Addpicture = [[UIButton alloc] init];
    btn_Addpicture.backgroundColor = [UIColor clearColor];
    //    [dropbox setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [btn_Addpicture setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Addpicture addTarget:self action:@selector(btn_Addpicture:) forControlEvents:UIControlEventTouchUpInside ];
    //[scrollview addSubview:btn_Addpicture];
    
    
    
    
    img_icon=[[UIImageView alloc]init];
    [img_icon setUserInteractionEnabled:YES];
    img_icon.backgroundColor=[UIColor clearColor];
    img_icon.image=[UIImage imageNamed:@"img icon@2x .png"];
    [scrollview addSubview:img_icon];
    
    
    UIButton *btn_imgicon = [[UIButton alloc] init];
    btn_imgicon.backgroundColor = [UIColor clearColor];
    [btn_imgicon setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_imgicon addTarget:self action:@selector(btn_imgicon:) forControlEvents:UIControlEventTouchUpInside];
    [img_icon addSubview:btn_imgicon];
    
    
    
    
    
    UIImageView *line_img=[[UIImageView alloc]init];
    [line_img setUserInteractionEnabled:YES];
    line_img.backgroundColor=[UIColor clearColor];
    line_img.image=[UIImage imageNamed:@"line1.png"];
    [scrollview addSubview:line_img];
    
    UILabel  * lbl_mp3video = [[UILabel alloc]init] ;
    lbl_mp3video.text = @"[.jpg, .png, .mov, .mp4]";
    lbl_mp3video.backgroundColor=[UIColor clearColor];
    lbl_mp3video.textColor=[UIColor blackColor];
    lbl_mp3video.numberOfLines = 0;
    lbl_mp3video.font = [UIFont fontWithName:kFont size:10];
    [scrollview addSubview:lbl_mp3video];
    
    
    UILabel  * lbl_store = [[UILabel alloc]init];
    lbl_store.text = @"Please describe how you store your food";
    lbl_store.backgroundColor=[UIColor clearColor];
    lbl_store.textColor=[UIColor blackColor];
    lbl_store.font = [UIFont fontWithName:kFont size:13];
    [scrollview addSubview:lbl_store];
    
    
    txt_view=[[UITextView alloc]init];
    txt_view.scrollEnabled=YES;
    txt_view.userInteractionEnabled=YES;
    txt_view.font=[UIFont fontWithName:kFont size:10];
    txt_view.backgroundColor=[UIColor whiteColor];
    txt_view.delegate=self;
    txt_view.textColor=[UIColor blackColor];
    txt_view.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    txt_view.layer.borderWidth=1.0f;
    txt_view.clipsToBounds=YES;
    [scrollview addSubview:txt_view];
    
    lbl_char = [[UILabel alloc]init];
    lbl_char.text = @"(500 char.max)";
    lbl_char.backgroundColor=[UIColor clearColor];
    lbl_char.textColor=[UIColor blackColor];
    lbl_char.numberOfLines = 0;
    lbl_char.font = [UIFont fontWithName:kFont size:10];
    [scrollview addSubview:lbl_char];
    
    
    
    
    UILabel  * lbl_description = [[UILabel alloc]init];
    lbl_description.text = @"Please describe how you maintain\nhygiene standards in your kitchen";
    lbl_description.backgroundColor=[UIColor whiteColor];
    lbl_description.textColor=[UIColor blackColor];
    lbl_description.numberOfLines = 0;
    lbl_description.font = [UIFont fontWithName:kFont size:13];
    [scrollview addSubview:lbl_description];
    
    txt_view1=[[UITextView alloc]init];
    txt_view1.scrollEnabled=YES;
    txt_view1.userInteractionEnabled=YES;
    txt_view1.font=[UIFont fontWithName:kFont size:10];
    txt_view1.backgroundColor=[UIColor whiteColor];
    txt_view1.delegate=self;
    txt_view1.textColor=[UIColor blackColor];
    txt_view1.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    txt_view1.layer.borderWidth=1.0f;
    txt_view1.clipsToBounds=YES;
    [scrollview addSubview:txt_view1];
    
    lbl_max = [[UILabel alloc]init] ;
    lbl_max.text = @"(500 char.max)";
    lbl_max.backgroundColor=[UIColor clearColor];
    lbl_max.textColor=[UIColor blackColor];
    lbl_max.numberOfLines = 0;
    lbl_max.font = [UIFont fontWithName:kFont size:10];
    [scrollview addSubview:lbl_max];
    
    UIImageView *image=[[UIImageView alloc]init];
    [image setUserInteractionEnabled:YES];
    image.backgroundColor=[UIColor colorWithRed:241/255.0f green:244/255.0f blue:242/255.0f alpha:1];
    [scrollview addSubview:image];
    
    
    
    UILabel  * page2 = [[UILabel alloc]init];
    page2.text = @"Page 2/6";
    page2.backgroundColor=[UIColor clearColor];
    page2.textAlignment=NSTextAlignmentCenter;
    
    page2.textColor=[UIColor blackColor];
    page2.numberOfLines = 0;
    page2.font = [UIFont fontWithName:kFont size:13];
    [image addSubview:page2];
    
    UIButton *next = [[UIButton alloc] init];
    next.layer.cornerRadius=4.0f;
    [next setImage:[UIImage imageNamed:@"text buuton.png"] forState:UIControlStateNormal];
    [next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [next addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    next.clipsToBounds = YES;
    [image addSubview:next];
    
    
    
    tableview_Kcountry = [[UITableView alloc]init];
    [tableview_Kcountry setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tableview_Kcountry.delegate = self;
    tableview_Kcountry.dataSource = self;
    tableview_Kcountry.showsVerticalScrollIndicator = NO;
    tableview_Kcountry.backgroundColor = [UIColor whiteColor];
    tableview_Kcountry.layer.borderColor = [[UIColor blackColor]CGColor];
    tableview_Kcountry.layer.borderWidth = 1.0f;
    tableview_Kcountry.clipsToBounds = YES;
    tableview_Kcountry.hidden =YES;
    [scrollview addSubview:tableview_Kcountry];
    
    tableview_Kcity = [[UITableView alloc]init];
    [tableview_Kcity setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tableview_Kcity.delegate = self;
    tableview_Kcity.dataSource = self;
    tableview_Kcity.showsVerticalScrollIndicator = NO;
    tableview_Kcity.backgroundColor = [UIColor whiteColor];
    tableview_Kcity.layer.borderColor = [[UIColor blackColor]CGColor];
    tableview_Kcity.layer.borderWidth = 1.0f;
    tableview_Kcity.clipsToBounds = YES;
    tableview_Kcity.hidden =YES;
    [scrollview addSubview:tableview_Kcity];
    
    
    
    if (IS_IPHONE_6Plus){
        scrollview.frame=CGRectMake(0, 45, WIDTH, HEIGHT);
        img_backgroundimage.frame=CGRectMake(-11, -26 ,WIDTH+29, 900);
        
        lbl_info.frame=CGRectMake(130,3, 300,40);
        txt_Name.frame=CGRectMake(25,40, WIDTH-50, 38);
        line.frame=CGRectMake(25, CGRectGetMaxY(txt_Name.frame)-8, WIDTH-50, 0.5);
        lbl_option.frame=CGRectMake(WIDTH-200,CGRectGetMaxY(txt_Name.frame)-4, 160,15);
        lbl_kitchen.frame=CGRectMake(90,CGRectGetMaxY(lbl_option.frame)+20, 140, 40);
        lbl_small.frame=CGRectMake(200,CGRectGetMaxY(lbl_option.frame)+20, 200,40);
        txt_street.frame=CGRectMake(25,CGRectGetMaxY(lbl_small.frame)+8,WIDTH-50, 38);
        line6.frame=CGRectMake(25, CGRectGetMaxY(txt_street.frame)-6, WIDTH-50, 0.5);
        txt_country.frame=CGRectMake(25,CGRectGetMaxY(txt_street.frame)+8, WIDTH-75, 38);
        line7.frame=CGRectMake(25, CGRectGetMaxY(txt_country.frame)-6, WIDTH-50, 0.5);
        dropboximg_country.frame = CGRectMake(CGRectGetMaxX(txt_country.frame)+4, CGRectGetMaxY(txt_street.frame)+20, 15, 10);
        
        dropbox.frame = CGRectMake(25, CGRectGetMaxY(txt_street.frame)+8, WIDTH-50, 38);
        tableview_Kcountry.frame=CGRectMake(20, CGRectGetMaxY(txt_country.frame)-25, WIDTH-70, 140);
        
        txt_city.frame=CGRectMake(25,CGRectGetMaxY(txt_country.frame)+8, WIDTH-75, 38);
        line8.frame=CGRectMake(25,CGRectGetMaxY(txt_city.frame)-6, WIDTH-50, 0.5);
        dropboximg_City.frame = CGRectMake(CGRectGetMaxX(txt_city.frame)+4, CGRectGetMaxY(txt_country.frame)+25, 15, 10);
        
        dropbox2.frame = CGRectMake(25, CGRectGetMaxY(txt_country.frame)+8, WIDTH-50, 38);
        tableview_Kcity.frame=CGRectMake(20, CGRectGetMaxY(txt_city.frame), WIDTH-70, 200);
        
        txt_postolcode.frame=CGRectMake(25,CGRectGetMaxY(txt_city.frame)+6, WIDTH-50, 38);
        line9.frame=CGRectMake(25, CGRectGetMaxY(txt_postolcode.frame)-6, WIDTH-50, 0.5);
        
        lbl_addpicture.frame=CGRectMake(25,CGRectGetMaxY(txt_postolcode.frame)+0,WIDTH-50,50);
        
        lbl_imagesvideo.frame=CGRectMake(25, CGRectGetMaxY(lbl_addpicture.frame)-10, 150, 15);
        img_Addpicture.frame=CGRectMake(25, CGRectGetMaxY(lbl_imagesvideo.frame)+2, 60, 25);
        collView_serviceDirectory.frame=CGRectMake(25, CGRectGetMaxY(lbl_imagesvideo.frame)+2, 240, 30);
        btn_Addpicture.frame=CGRectMake(25, CGRectGetMaxY(lbl_imagesvideo.frame)+2, 80, 25);
        
        img_icon.frame=CGRectMake(WIDTH-60,CGRectGetMaxY(lbl_imagesvideo.frame)+1, 30, 25);
        btn_imgicon.frame=CGRectMake(0,0, 30, 25);
        
        line_img.frame=CGRectMake(25, CGRectGetMaxY(img_Addpicture.frame)+4, WIDTH-50, 0.5);
        lbl_mp3video.frame=CGRectMake(WIDTH-135, CGRectGetMaxY(line_img.frame)+1, 130, 15);
        
        
        lbl_store.frame=CGRectMake(25,CGRectGetMaxY(lbl_mp3video.frame)+10, WIDTH-50,30);
        txt_view.frame=CGRectMake(25, CGRectGetMaxY(lbl_store.frame)+5, WIDTH-50, 140);
        lbl_char.frame=CGRectMake(WIDTH-100,CGRectGetMaxY(txt_view.frame)-1, 100,30);
        lbl_description.frame= CGRectMake(20,CGRectGetMaxY(lbl_char.frame)-10, WIDTH-50,45);
        txt_view1.frame=CGRectMake(20, CGRectGetMaxY(lbl_description.frame), WIDTH-50, 140);
        lbl_max.frame= CGRectMake(WIDTH-100,CGRectGetMaxY(txt_view1.frame)-1, 100,30);
        image.frame=CGRectMake(0, CGRectGetMaxY(img_backgroundimage.frame)+3, WIDTH, 100);
        //        image.frame=CGRectMake(0, HEIGHT-100, WIDTH, 100);
        page2.frame =CGRectMake(0,4, WIDTH,30);
        next.frame = CGRectMake(18, 40, WIDTH-36,50);
        [scrollview setContentSize:CGSizeMake(0,CGRectGetMaxY(image.frame)+70)];
        
    }
    else if (IS_IPHONE_6){
        scrollview.frame=CGRectMake(0, 45, WIDTH, HEIGHT);
        img_backgroundimage.frame=CGRectMake(-11, -26 ,WIDTH+29, 900);
        
        lbl_info.frame=CGRectMake(130,3, 300,40);
        txt_Name.frame=CGRectMake(25,40, WIDTH-50, 38);
        line.frame=CGRectMake(25, CGRectGetMaxY(txt_Name.frame)-8, WIDTH-50, 0.5);
        lbl_option.frame=CGRectMake(198,CGRectGetMaxY(txt_Name.frame)-4, 260,15);
        lbl_kitchen.frame=CGRectMake(90,CGRectGetMaxY(lbl_option.frame)+20, 140, 40);
        lbl_small.frame=CGRectMake(200,CGRectGetMaxY(lbl_option.frame)+20, 200,40);
        txt_street.frame=CGRectMake(25,CGRectGetMaxY(lbl_small.frame)+8,WIDTH-50, 38);
        line6.frame=CGRectMake(25, CGRectGetMaxY(txt_street.frame)-6, WIDTH-50, 0.5);
        txt_country.frame=CGRectMake(25,CGRectGetMaxY(txt_street.frame)+8, WIDTH-50, 38);
        line7.frame=CGRectMake(25, CGRectGetMaxY(txt_country.frame)-6, WIDTH-50, 0.5);
        dropboximg_country.frame = CGRectMake(330, CGRectGetMaxY(txt_street.frame)+25, 15, 10);
        
        dropbox.frame = CGRectMake(25, CGRectGetMaxY(txt_street.frame)+8, WIDTH-50, 38);
        tableview_Kcountry.frame=CGRectMake(20, CGRectGetMaxY(txt_country.frame), WIDTH-50, 200);
        
        txt_city.frame=CGRectMake(25,CGRectGetMaxY(txt_country.frame)+8, WIDTH-50, 38);
        line8.frame=CGRectMake(25,CGRectGetMaxY(txt_city.frame)-6, WIDTH-50, 0.5);
        dropboximg_City.frame = CGRectMake(330, CGRectGetMaxY(txt_country.frame)+25, 15, 10);
        
        dropbox2.frame = CGRectMake(25, CGRectGetMaxY(txt_country.frame)+8, WIDTH-50, 38);
        tableview_Kcity.frame=CGRectMake(20, CGRectGetMaxY(txt_city.frame), WIDTH-50, 200);
        
        txt_postolcode.frame=CGRectMake(25,CGRectGetMaxY(txt_city.frame)+6, WIDTH-50, 38);
        line9.frame=CGRectMake(25, CGRectGetMaxY(txt_postolcode.frame)-6, WIDTH-50, 0.5);
        
        lbl_addpicture.frame=CGRectMake(25,CGRectGetMaxY(txt_postolcode.frame)+0,WIDTH-50,50);
        
        lbl_imagesvideo.frame=CGRectMake(25, CGRectGetMaxY(lbl_addpicture.frame)-10, 150, 15);
        img_Addpicture.frame=CGRectMake(25, CGRectGetMaxY(lbl_imagesvideo.frame)+2, 40, 25);
        collView_serviceDirectory.frame=CGRectMake(25, CGRectGetMaxY(lbl_imagesvideo.frame)+2, 200, 50);
        btn_Addpicture.frame=CGRectMake(25, CGRectGetMaxY(lbl_imagesvideo.frame)+2, 80, 25);
        
        img_icon.frame=CGRectMake(310,CGRectGetMaxY(lbl_imagesvideo.frame)+1, 30, 25);
        btn_imgicon.frame=CGRectMake(0,0, 30, 25);
        
        line_img.frame=CGRectMake(25, CGRectGetMaxY(img_Addpicture.frame)+4, WIDTH-50, 0.5);
        lbl_mp3video.frame=CGRectMake(250, CGRectGetMaxY(line_img.frame)+1, WIDTH-50, 15);
        
        
        lbl_store.frame=CGRectMake(25,CGRectGetMaxY(lbl_mp3video.frame)+10, WIDTH-50,30);
        txt_view.frame=CGRectMake(25, CGRectGetMaxY(lbl_store.frame)+5, WIDTH-50, 140);
        lbl_char.frame=CGRectMake(WIDTH-120,CGRectGetMaxY(txt_view.frame)-1, 100,30);
        lbl_description.frame= CGRectMake(20,CGRectGetMaxY(lbl_char.frame)-10, WIDTH-50,45);
        txt_view1.frame=CGRectMake(20, CGRectGetMaxY(lbl_description.frame), WIDTH-50, 140);
        lbl_max.frame= CGRectMake(WIDTH-120,CGRectGetMaxY(txt_view1.frame)-1, 100,30);
        image.frame=CGRectMake(0, CGRectGetMaxY(img_backgroundimage.frame)+3, WIDTH, 100);
        //        image.frame=CGRectMake(0, HEIGHT-100, WIDTH, 100);
        page2.frame =CGRectMake(0,4, WIDTH,30);
        next.frame = CGRectMake(18, 40, WIDTH-36,50);
        [scrollview setContentSize:CGSizeMake(0,1190)];
        
    }
    else if(IS_IPHONE_5)
    {
        scrollview.frame=CGRectMake(0, 45, WIDTH, HEIGHT);
        
        img_backgroundimage.frame=CGRectMake(-2, -18 ,WIDTH+8, 950);
        lbl_info.frame=CGRectMake(90,5, 280,40);
        txt_Name.frame=CGRectMake(25,40, WIDTH-50, 38);
        line.frame=CGRectMake(25, CGRectGetMaxY(txt_Name.frame)-6,  WIDTH-50, 0.5);
        lbl_option.frame=CGRectMake(140,CGRectGetMaxY(txt_Name.frame)-3, WIDTH-50,13);
        lbl_kitchen.frame=CGRectMake(67,CGRectGetMaxY(lbl_option.frame)+20, 140, 40);
        lbl_small.frame=CGRectMake(180,CGRectGetMaxY(lbl_option.frame)+20, 200,40);
        txt_street.frame=CGRectMake(25,CGRectGetMaxY(lbl_small.frame)+10, WIDTH-50, 38);
        line6.frame=CGRectMake(25, CGRectGetMaxY(txt_street.frame)-6, WIDTH-50, 0.5);
        txt_country.frame=CGRectMake(25,CGRectGetMaxY(txt_street.frame)+3, WIDTH-70, 38);
        line7.frame=CGRectMake(25, CGRectGetMaxY(txt_country.frame)-6, WIDTH-50, 0.5);
        dropboximg_country.frame = CGRectMake(CGRectGetMaxX(txt_country.frame), CGRectGetMaxY(txt_street.frame)+18, 15, 10);
        
        dropbox.frame = CGRectMake(25, CGRectGetMaxY(txt_street.frame)+1, WIDTH-50, 38);
        tableview_Kcountry.frame=CGRectMake(20, CGRectGetMaxY(txt_country.frame)-200, WIDTH-50, 200);
        
        txt_city.frame=CGRectMake(25,CGRectGetMaxY(line7.frame)+1, WIDTH-70, 38);
        line8.frame=CGRectMake(25,CGRectGetMaxY(txt_city.frame)+1, WIDTH-50, 0.5);
        dropboximg_City.frame = CGRectMake(CGRectGetMaxX(txt_city.frame), CGRectGetMaxY(txt_country.frame)+18, 15, 10);
        dropbox2.frame=CGRectMake(25, CGRectGetMaxY(txt_country.frame)+1, WIDTH-50, 38);
        tableview_Kcity.frame=CGRectMake(20, CGRectGetMaxY(txt_city.frame)-200, WIDTH-50, 200);
        
        txt_postolcode.frame=CGRectMake(25,CGRectGetMaxY(line8.frame)+1, WIDTH-50, 38);
        line9.frame=CGRectMake(25, CGRectGetMaxY(txt_postolcode.frame)+1, WIDTH-50, 0.5);
        lbl_addpicture.frame=CGRectMake(25,CGRectGetMaxY(line9.frame)+10, WIDTH-50,50);
        
        lbl_imagesvideo.frame=CGRectMake(25, CGRectGetMaxY(lbl_addpicture.frame)+3, 150, 15);
        img_Addpicture.frame=CGRectMake(25, CGRectGetMaxY(lbl_imagesvideo.frame)+2, 60, 30);
        collView_serviceDirectory.frame=CGRectMake(25, CGRectGetMaxY(lbl_imagesvideo.frame)+2, 200, 30);
        
        btn_Addpicture.frame=CGRectMake(25, CGRectGetMaxY(lbl_imagesvideo.frame)+2, 130, 30);
        
        img_icon.frame=CGRectMake(260,CGRectGetMaxY(lbl_imagesvideo.frame)+5, 30, 25);
        btn_imgicon.frame=CGRectMake(0,0, 30, 25);
        
        line_img.frame=CGRectMake(25, CGRectGetMaxY(img_Addpicture.frame)+3, WIDTH-50, 0.5);
        lbl_mp3video.frame=CGRectMake(190, CGRectGetMaxY(line_img.frame)+1, WIDTH-50, 15);
        
        lbl_store.frame=CGRectMake(25,CGRectGetMaxY(lbl_mp3video.frame)+10, WIDTH-50,30);
        txt_view.frame=CGRectMake(25, CGRectGetMaxY(lbl_store.frame)+5, WIDTH-50, 140);
        lbl_char.frame=CGRectMake(220,CGRectGetMaxY(txt_view.frame)-1, WIDTH-52,30);
        lbl_description.frame= CGRectMake(25,CGRectGetMaxY(lbl_char.frame)+3, WIDTH-50,45);
        txt_view1.frame=CGRectMake(25, CGRectGetMaxY(lbl_description.frame)+15, WIDTH-50, 140);
        lbl_max.frame= CGRectMake(220,CGRectGetMaxY(txt_view1.frame)-1, WIDTH-50,30);
        image.frame=CGRectMake(0, CGRectGetMaxY(img_backgroundimage.frame)+3, WIDTH, 100);
        //        image.frame=CGRectMake(0, HEIGHT-80, WIDTH, 100);
        page2.frame =CGRectMake(0,0, WIDTH,30);
        
        next.frame = CGRectMake(18, 35, WIDTH-36,40);
        [scrollview setContentSize:CGSizeMake(0,1190)];
        
    }
    else
    {
        scrollview.frame=CGRectMake(0, 45, WIDTH, HEIGHT);
        
        img_backgroundimage.frame=CGRectMake(-2, -18 ,WIDTH+8, 950);
        lbl_info.frame=CGRectMake(90,5, 280,40);
        txt_Name.frame=CGRectMake(25,40, WIDTH-50, 38);
        line.frame=CGRectMake(25, CGRectGetMaxY(txt_Name.frame)-6,  WIDTH-50, 0.5);
        lbl_option.frame=CGRectMake(140,CGRectGetMaxY(txt_Name.frame)-3, WIDTH-50,13);
        lbl_kitchen.frame=CGRectMake(67,CGRectGetMaxY(lbl_option.frame)+20, 140, 40);
        lbl_small.frame=CGRectMake(180,CGRectGetMaxY(lbl_option.frame)+20, 200,40);
        txt_street.frame=CGRectMake(25,CGRectGetMaxY(lbl_small.frame)+5, WIDTH-50, 38);
        line6.frame=CGRectMake(25, CGRectGetMaxY(txt_street.frame)+1, WIDTH-50, 0.5);
        txt_country.frame=CGRectMake(25,CGRectGetMaxY(line6.frame)+1, WIDTH-50, 38);
        line7.frame=CGRectMake(25, CGRectGetMaxY(txt_country.frame)+1, WIDTH-50, 0.5);
        dropboximg_country.frame = CGRectMake(270, CGRectGetMaxY(txt_street.frame)+25, 15, 10);
        
        dropbox.frame = CGRectMake(25, CGRectGetMaxY(txt_street.frame)+1, WIDTH-50, 38);
        tableview_Kcountry.frame=CGRectMake(20, CGRectGetMaxY(txt_country.frame)-200, WIDTH-50, 200);
        
        txt_city.frame=CGRectMake(25,CGRectGetMaxY(line7.frame)+1, WIDTH-50, 38);
        line8.frame=CGRectMake(25,CGRectGetMaxY(txt_city.frame)+1, WIDTH-50, 0.5);
        dropboximg_City.frame = CGRectMake(270, CGRectGetMaxY(txt_country.frame)+25, 15, 10);
        dropbox2.frame=CGRectMake(25, CGRectGetMaxY(txt_country.frame)+1, WIDTH-50, 38);
        tableview_Kcity.frame=CGRectMake(20, CGRectGetMaxY(txt_city.frame)-200, WIDTH-50, 200);
        
        txt_postolcode.frame=CGRectMake(25,CGRectGetMaxY(line8.frame)+1, WIDTH-50, 38);
        line9.frame=CGRectMake(25, CGRectGetMaxY(txt_postolcode.frame)+1, WIDTH-50, 0.5);
        lbl_addpicture.frame=CGRectMake(25,CGRectGetMaxY(line9.frame)+10, WIDTH-50,50);
        
        lbl_imagesvideo.frame=CGRectMake(25, CGRectGetMaxY(lbl_addpicture.frame)+3, 150, 15);
        img_Addpicture.frame=CGRectMake(25, CGRectGetMaxY(lbl_imagesvideo.frame)+2, 120, 30);
        collView_serviceDirectory.frame=CGRectMake(25, CGRectGetMaxY(lbl_imagesvideo.frame)+2, 200, 30);
        
        btn_Addpicture.frame=CGRectMake(25, CGRectGetMaxY(lbl_imagesvideo.frame)+2, 130, 30);
        
        img_icon.frame=CGRectMake(260,CGRectGetMaxY(lbl_imagesvideo.frame)+5, 30, 25);
        btn_imgicon.frame=CGRectMake(0,0, 30, 25);
        
        line_img.frame=CGRectMake(25, CGRectGetMaxY(img_Addpicture.frame)+3, WIDTH-50, 0.5);
        lbl_mp3video.frame=CGRectMake(190, CGRectGetMaxY(line_img.frame)+1, WIDTH-50, 15);
        
        lbl_store.frame=CGRectMake(25,CGRectGetMaxY(lbl_mp3video.frame)+10, WIDTH-50,30);
        txt_view.frame=CGRectMake(25, CGRectGetMaxY(lbl_store.frame)+5, WIDTH-50, 140);
        lbl_char.frame=CGRectMake(220,CGRectGetMaxY(txt_view.frame)-1, WIDTH-52,30);
        lbl_description.frame= CGRectMake(25,CGRectGetMaxY(lbl_char.frame)+3, WIDTH-50,45);
        txt_view1.frame=CGRectMake(25, CGRectGetMaxY(lbl_description.frame)+15, WIDTH-50, 140);
        lbl_max.frame= CGRectMake(220,CGRectGetMaxY(txt_view1.frame)-1, WIDTH-50,30);
        image.frame=CGRectMake(0, CGRectGetMaxY(img_backgroundimage.frame)+3, WIDTH, 100);
        //        image.frame=CGRectMake(0, HEIGHT-80, WIDTH, 100);
        page2.frame =CGRectMake(0,0, WIDTH,30);
        
        next.frame = CGRectMake(18, 35, WIDTH-36,40);
        [scrollview setContentSize:CGSizeMake(0,1190)];
        
        
    }
    
    
    //    [scrollview setContentSize:CGSizeMake(0,CGRectGetMaxY(lbl_max.frame)+60)];
}
-(void)kyyboard_returnmethod
{
    [txt_Name resignFirstResponder];
    [txt_street resignFirstResponder];
    [txt_postolcode resignFirstResponder];
    [txt_view resignFirstResponder];
    [txt_view1 resignFirstResponder];
}




#pragma mark Button actions


-(void)Next_btnClick
{
    //  Chefsignup3 *chefsignup3=[[Chefsignup3 alloc]init];
    ////    [self.navigationController pushViewController:chefsignup3 animated:NO];
    //
    //    [self presentViewController:chefsignup3 animated:NO completion:nil];
    //
    
    NSLog(@"click_Loginbtn");
    
    //    if ([txt_Name.text isEqualToString:@""])
    //    {
    //        [self popup_Alertview:@"Please enter Kitchen Name"];
    //
    //    }
    if ([txt_street.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Please enter Kitchen Address"];
    }
    
    else if ([txt_country.text length]<6)
    {
        [self popup_Alertview:@"Please enter Country Name"];
    }
    else if([txt_city.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Please enter City Name"];
    }
    else if([txt_postolcode.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Please enter Postalcode"];
    }
    else if (txt_postolcode.text.length>6)
    {
        [self popup_Alertview:@"Please Enter Valid Postalcode"];
    }
    if ([Arr_temp count]==0)
    {
        [self popup_Alertview:@"Please Add a picture to your cooking area and preparation area"];
    }
    //    else if (imgData== nil)
    //    {
    //        [self popup_Alertview:@"Please Add a picture to your cooking area and preparation area"];
    //    }
    else if([txt_view.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Please enter store food description"];
    }else if([txt_view1.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Please enter hygeine standard description"];
    }
    
    else
    {
        
        NSString *strReplacedindex=[NSString stringWithFormat:@"%@",[Arr_temp objectAtIndex:0]];
        
        
        NSData *data1index=[[NSData alloc]init];
        NSData *data2=[[NSData alloc]init];
        if (int_SelectedVideo==[strReplacedindex integerValue])
        {
            
        }
        else
        {
            
            data1index=[arr_data objectAtIndex:0];
            data2=[arr_data objectAtIndex:[strReplacedindex integerValue]];
            [arr_data replaceObjectAtIndex:[strReplacedindex integerValue] withObject:data1index];
            [arr_data replaceObjectAtIndex:0 withObject:data2];
            
        }
        
        [self SignupSecond];
    }
    
    
    
}
-(void)dropboxbtn_country:(UIButton *) sender

{
    [self kyyboard_returnmethod];
    tableview_Kcity.hidden=YES;
    
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        tableview_Kcountry.hidden=NO;
        
    }
    else
    {
        [sender setSelected:NO];
        tableview_Kcountry.hidden=YES;
    }
    
}
-(void)dropboxbtn_city:(UIButton *) sender

{
    if ([txt_country.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Plaese select country first"];
        
    }
    else{
        
        [self kyyboard_returnmethod];
        tableview_Kcountry.hidden=YES;
        if (![sender isSelected])
        {
            [sender setSelected:YES];
            tableview_Kcity.hidden=NO;
            
            
            
        }
        else
        {
            [sender setSelected:NO];
            tableview_Kcity.hidden=YES;
        }
        
        
    }
    
}

-(void)btn_Addpicture:(UIButton *) sender

{
    str_pickingImage = @"Profile";
    
    UIActionSheet *actionSheet_popupQuery = [[UIActionSheet alloc] initWithTitle:@"Select A Image From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Camera", nil];
    actionSheet_popupQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet_popupQuery showInView:self.view];
    
    
    
}
-(void)btn_imgicon:(UIButton *) sender

{
    str_pickingImage = @"Kitchen";
    
    UIActionSheet *actionSheet_popupQuery = [[UIActionSheet alloc] initWithTitle:@"Select A Image From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Camera", nil];
    actionSheet_popupQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet_popupQuery showInView:self.view];
    
    
    
}

#pragma mark Textview Delegate


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    
    
    if ([textView isEqual:txt_view])
    {
        
        lbl_char.text = [NSString stringWithFormat:@"(%lu char.max)",500-[txt_view.text length]];
    }
    else
    {
        lbl_max.text = [NSString stringWithFormat:@"(%lu char.max)",500-[txt_view1.text length]];
        
    }
    
    if (txt_view.text.length>500)
    {
        return NO;
    }
    else if (txt_view1.text.length>500)
    {
        return NO;
        
    }
    
    
    return YES;
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    textView.text = @"";
    
    return YES;
}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    if([textView isEqual:txt_view])
    {
        [scrollview setContentOffset:CGPointMake(0,300) animated:YES];
    }else{
        [scrollview setContentOffset:CGPointMake(0,600) animated:YES];
        
        
    }
    
    //  [scrollview setContentOffset:CGPointMake(0,200) animated:YES];
}


- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    
    
    //    if([textView isEqual:txtview_adddescription])
    //    {
    //        if (textView.text.length==0)
    //        {
    //            [textView addSubview:label_placeHolder];
    //        }else{
    //            [dict_data setObject:txtview_adddescription.text forKey:@"Remarks"];
    //        }
    //    }
    
    if ([textView isEqual:txt_view])
    {
        
        lbl_char.text = [NSString stringWithFormat:@"(%lu char.max)",500-[txt_view.text length]];
    }
    else
    {
        lbl_max.text = [NSString stringWithFormat:@"(%lu char.max)",500-[txt_view1.text length]];
        
    }
    
    [scrollview setContentOffset:CGPointMake(0,0) animated:NO];
    
    
    
}




-(void) textViewDidChange:(UITextView *)textView
{
    
    
}
#pragma mark TextField Delegate methods


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField==txt_postolcode)
    {
        NSString *stringVal = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered =[[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        if (stringVal.length<7 && [string isEqualToString:filtered])
        {
            return YES;
        }
        else{
            return NO;
        }
        return [string isEqualToString:filtered];
        
    }
    
    return YES;
}
-(void)click_Done
{
    [txt_postolcode resignFirstResponder];
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [textField resignFirstResponder];
}




-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}




#pragma mark UITableview methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tableview_Kcountry) {
        return [ary_countrylist count];
        
    }else {
        return [ary_citylist count];
    }
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"LazyTableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor=[UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    
    if (tableView==tableview_Kcountry)
    {
        UILabel  * lbl_list = [[UILabel alloc]init];
        lbl_list.frame=CGRectMake(12, 0, 100, 30);
        lbl_list.backgroundColor=[UIColor clearColor];
        lbl_list.textColor=[UIColor blackColor];
        lbl_list.font = [UIFont fontWithName:kFont size:12];
        lbl_list.text=[[ary_countrylist objectAtIndex:indexPath.row] valueForKey:@"Country"];
        [cell.contentView addSubview:lbl_list];
        
        
    }else if(tableView == tableview_Kcity){
        UILabel  * lbl_list = [[UILabel alloc]init];
        lbl_list.frame=CGRectMake(12, 0, 100, 30);
        lbl_list.backgroundColor=[UIColor clearColor];
        lbl_list.textColor=[UIColor blackColor];
        lbl_list.font = [UIFont fontWithName:kFont size:12];
        lbl_list.text=[[ary_citylist objectAtIndex:indexPath.row] valueForKey:@"City"];
        [cell.contentView addSubview:lbl_list];
        
        
    }
    
    else{
        
        
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tableview_Kcountry) {
        
        txt_country.text =[[ary_countrylist objectAtIndex:indexPath.row] valueForKey:@"Country"];
        txt_city.text = @"";
        [tableview_Kcountry setHidden:YES];
        [self cityList];
        
        
    }else {
        txt_city.text =[[ary_citylist objectAtIndex:indexPath.row] valueForKey:@"City"];
        [tableview_Kcity setHidden:YES];
        
        
    }
    
}
#pragma mark UiCollectionView Delegate Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 11;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView1 dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    
    
    
    
    UIImageView *img_productImg = [[UIImageView alloc]initWithFrame:CGRectMake(0,1,57,28)];
    [cell.contentView addSubview:img_productImg];
    
    
    if (indexPath.row==0)
    {
        img_productImg0= [[UIImageView alloc]initWithFrame:CGRectMake(0,1,57,28)];
        [img_productImg0 setContentMode:UIViewContentModeScaleAspectFill];
        img_productImg0.layer.cornerRadius=3.0;
        img_productImg0.layer.borderWidth=2;
        img_productImg0.layer.borderColor=[UIColor lightTextColor].CGColor;
        [img_productImg0 setClipsToBounds:YES];
        [cell.contentView addSubview:img_productImg0];
        
        if (indexPath.row<[arry_imageArry count])
        {
            img_productImg0.image = [arry_imageArry objectAtIndex:indexPath.row];
        }
        else
        {
            img_productImg0.image = [UIImage imageNamed:@"img_placeholderProd@2x.png"];
        }
        
    }
    else if (indexPath.row==1)
    {
        
        img_productImg1= [[UIImageView alloc]initWithFrame:CGRectMake(0,1,57,28)];
        
        [img_productImg1 setContentMode:UIViewContentModeScaleAspectFill];
        img_productImg1.layer.cornerRadius=3.0;
        img_productImg1.layer.borderWidth=2;
        img_productImg1.layer.borderColor=[UIColor lightTextColor].CGColor;
        [img_productImg1 setClipsToBounds:YES];
        [cell.contentView addSubview:img_productImg1];
        
        if (indexPath.row<[arry_imageArry count])
        {
            img_productImg1.image = [arry_imageArry objectAtIndex:indexPath.row];
        }
        else
        {
            img_productImg1.image = [UIImage imageNamed:@"img_placeholderProd@2x.png"];
        }
        
        
    }
    else if (indexPath.row==2)
    {
        
        img_productImg2= [[UIImageView alloc]initWithFrame:CGRectMake(0,1,57,28)];
        
        [img_productImg2 setContentMode:UIViewContentModeScaleAspectFill];
        img_productImg2.layer.cornerRadius=3.0;
        img_productImg2.layer.borderWidth=2;
        img_productImg2.layer.borderColor=[UIColor lightTextColor].CGColor;
        [img_productImg2 setClipsToBounds:YES];
        [cell.contentView addSubview:img_productImg2];
        
        if (indexPath.row<[arry_imageArry count])
        {
            img_productImg2.image = [arry_imageArry objectAtIndex:indexPath.row];
        }
        else
        {
            img_productImg2.image = [UIImage imageNamed:@"img_placeholderProd@2x.png"];
        }
        
        
    }
    else if (indexPath.row==3)
    {
        
        img_productImg3= [[UIImageView alloc]initWithFrame:CGRectMake(0,1,57,28)];
        [img_productImg3 setContentMode:UIViewContentModeScaleAspectFill];
        img_productImg3.layer.cornerRadius=3.0;
        img_productImg3.layer.borderWidth=2;
        img_productImg3.layer.borderColor=[UIColor lightTextColor].CGColor;
        [img_productImg3 setClipsToBounds:YES];
        [cell.contentView addSubview:img_productImg3];
        
        if (indexPath.row<[arry_imageArry count])
        {
            img_productImg3.image = [arry_imageArry objectAtIndex:indexPath.row];
        }
        else
        {
            img_productImg3.image = [UIImage imageNamed:@"img_placeholderProd@2x.png"];
        }
        
        
    }
    
    else if (indexPath.row==4)
    {
        
        img_productImg4= [[UIImageView alloc]initWithFrame:CGRectMake(0,1,57,28)];
        
        [img_productImg4 setContentMode:UIViewContentModeScaleAspectFill];
        img_productImg4.layer.cornerRadius=3.0;
        img_productImg4.layer.borderWidth=2;
        img_productImg4.layer.borderColor=[UIColor lightTextColor].CGColor;
        [img_productImg4 setClipsToBounds:YES];
        [cell.contentView addSubview:img_productImg4];
        
        if (indexPath.row<[arry_imageArry count])
        {
            img_productImg4.image = [arry_imageArry objectAtIndex:indexPath.row];
        }
        else
        {
            img_productImg4.image = [UIImage imageNamed:@"img_placeholderProd@2x.png"];
        }
        
        
    }
    else if (indexPath.row==5)
    {
        
        img_productImg5= [[UIImageView alloc]initWithFrame:CGRectMake(0,1,57,28)];
        
        [img_productImg5 setContentMode:UIViewContentModeScaleAspectFill];
        img_productImg5.layer.cornerRadius=3.0;
        img_productImg5.layer.borderWidth=2;
        img_productImg5.layer.borderColor=[UIColor lightTextColor].CGColor;
        [img_productImg5 setClipsToBounds:YES];
        [cell.contentView addSubview:img_productImg5];
        
        if (indexPath.row<[arry_imageArry count])
        {
            img_productImg5.image = [arry_imageArry objectAtIndex:indexPath.row];
        }
        else
        {
            img_productImg5.image = [UIImage imageNamed:@"img_placeholderProd@2x.png"];
        }
        
        
    }
    else if (indexPath.row==6)
    {
        
        img_productImg6= [[UIImageView alloc]initWithFrame:CGRectMake(0,1,57,28)];
        
        [img_productImg6 setContentMode:UIViewContentModeScaleAspectFill];
        img_productImg6.layer.cornerRadius=3.0;
        img_productImg6.layer.borderWidth=2;
        img_productImg6.layer.borderColor=[UIColor lightTextColor].CGColor;
        [img_productImg6 setClipsToBounds:YES];
        [cell.contentView addSubview:img_productImg6];
        
        if (indexPath.row<[arry_imageArry count])
        {
            img_productImg6.image = [arry_imageArry objectAtIndex:indexPath.row];
        }
        else
        {
            img_productImg6.image = [UIImage imageNamed:@"img_placeholderProd@2x.png"];
        }
        
        
    }
    else if (indexPath.row==7)
    {
        
        img_productImg7= [[UIImageView alloc]initWithFrame:CGRectMake(0,1,57,28)];
        
        [img_productImg7 setContentMode:UIViewContentModeScaleAspectFill];
        img_productImg7.layer.cornerRadius=3.0;
        img_productImg7.layer.borderWidth=2;
        img_productImg7.layer.borderColor=[UIColor lightTextColor].CGColor;
        [img_productImg7 setClipsToBounds:YES];
        [cell.contentView addSubview:img_productImg7];
        
        if (indexPath.row<[arry_imageArry count])
        {
            img_productImg7.image = [arry_imageArry objectAtIndex:indexPath.row];
        }
        else
        {
            img_productImg7.image = [UIImage imageNamed:@"img_placeholderProd@2x.png"];
        }
        
        
    }
    else if (indexPath.row==8)
    {
        
        img_productImg8= [[UIImageView alloc]initWithFrame:CGRectMake(0,1,57,28)];
        
        [img_productImg8 setContentMode:UIViewContentModeScaleAspectFill];
        img_productImg8.layer.cornerRadius=3.0;
        img_productImg8.layer.borderWidth=2;
        img_productImg8.layer.borderColor=[UIColor lightTextColor].CGColor;
        [img_productImg8 setClipsToBounds:YES];
        [cell.contentView addSubview:img_productImg8];
        
        if (indexPath.row<[arry_imageArry count])
        {
            img_productImg8.image = [arry_imageArry objectAtIndex:indexPath.row];
        }
        else
        {
            img_productImg8.image = [UIImage imageNamed:@"img_placeholderProd@2x.png"];
        }
        
        
    }
    else if (indexPath.row==9)
    {
        
        //                if (IS_IPHONE_6Plus){
        //                    img_backGnd2.frame = CGRectMake(0, 0,((WIDTH-65)/3),50);
        //                }
        //                else if (IS_IPHONE_6){
        //                    img_backGnd2.frame = CGRectMake(0, 0,40,50);
        //                }
        //                else  {
        //                    img_backGnd2.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        //                }
        img_productImg9= [[UIImageView alloc]initWithFrame:CGRectMake(0,1,57,28)];
        
        [img_productImg9 setContentMode:UIViewContentModeScaleAspectFill];
        img_productImg9.layer.cornerRadius=3.0;
        img_productImg9.layer.borderWidth=2;
        img_productImg9.layer.borderColor=[UIColor lightTextColor].CGColor;
        [img_productImg9 setClipsToBounds:YES];
        [cell.contentView addSubview:img_productImg9];
        
        if (indexPath.row<[arry_imageArry count])
        {
            img_productImg9.image = [arry_imageArry objectAtIndex:indexPath.row];
        }
        else
        {
            img_productImg9.image = [UIImage imageNamed:@"img_placeholderProd@2x.png"];
        }
        
        
    }
    else if (indexPath.row==10)
    {
        
        //                if (IS_IPHONE_6Plus){
        //                    img_backGnd2.frame = CGRectMake(0, 0,((WIDTH-65)/3),50);
        //                }
        //                else if (IS_IPHONE_6){
        //                    img_backGnd2.frame = CGRectMake(0, 0,40,50);
        //                }
        //                else  {
        //                    img_backGnd2.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        //                }
        img_productImg10= [[UIImageView alloc]initWithFrame:CGRectMake(0,1,57,28)];
        
        [img_productImg10 setContentMode:UIViewContentModeScaleAspectFill];
        img_productImg10.layer.cornerRadius=3.0;
        img_productImg10.layer.borderWidth=2;
        img_productImg10.layer.borderColor=[UIColor lightTextColor].CGColor;
        [img_productImg10 setClipsToBounds:YES];
        [cell.contentView addSubview:img_productImg10];
        
        if (indexPath.row<[arry_imageArry count])
        {
            img_productImg10.image = [arry_imageArry objectAtIndex:indexPath.row];
        }
        else
        {
            img_productImg10.image = [UIImage imageNamed:@"img_placeholderProd@2x.png"];
        }
        
        
    }
    
    //    else if (indexPath.row==11)
    //    {
    //
    //        //                if (IS_IPHONE_6Plus){
    //        //                    img_backGnd2.frame = CGRectMake(0, 0,((WIDTH-65)/3),50);
    //        //                }
    //        //                else if (IS_IPHONE_6){
    //        //                    img_backGnd2.frame = CGRectMake(0, 0,40,50);
    //        //                }
    //        //                else  {
    //        //                    img_backGnd2.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
    //        //                }
    //        img_productImg11= [[UIImageView alloc]initWithFrame:CGRectMake(0,1,57,28)];
    //
    //        [img_productImg11 setContentMode:UIViewContentModeScaleAspectFill];
    //        img_productImg11.layer.cornerRadius=3.0;
    //        img_productImg11.layer.borderWidth=2;
    //        img_productImg11.layer.borderColor=[UIColor lightTextColor].CGColor;
    //        [img_productImg11 setClipsToBounds:YES];
    //        [cell.contentView addSubview:img_productImg11];
    //
    //        if (indexPath.row<[arry_imageArry count])
    //        {
    //            img_productImg11.image = [arry_imageArry objectAtIndex:indexPath.row];
    //        }
    //        else
    //        {
    //            img_productImg11.image = [UIImage imageNamed:@"img_placeholderProd@2x.png"];
    //        }
    //
    //
    //    }
    //
    else
    {
        
        [img_productImg setContentMode:UIViewContentModeScaleAspectFill];
        img_productImg.layer.cornerRadius=3.0;
        img_productImg.layer.borderWidth=2;
        img_productImg.layer.borderColor=[UIColor lightTextColor].CGColor;
        [img_productImg setClipsToBounds:YES];
        
        
        
        if (indexPath.row<[arry_imageArry count])
        {
            img_productImg.image = [arry_imageArry objectAtIndex:indexPath.row];
        }
        else
        {
            img_productImg.image = [UIImage imageNamed:@"img_placeholderProd@2x.png"];
        }
        
    }
    
    
    UIButton *btn_TableCell_CheckBox1 = [[UIButton alloc]initWithFrame:CGRectMake(0,0,50,28)];
    btn_TableCell_CheckBox1.backgroundColor=[UIColor clearColor];
    [btn_TableCell_CheckBox1 addTarget:self action:@selector(click_selectObjectAt:) forControlEvents:UIControlEventTouchUpInside];
    btn_TableCell_CheckBox1.tag = indexPath.row;
    //[cell.contentView addSubview:btn_TableCell_CheckBox1];
    
    selectedindex=indexPath.row;
    NSLog(@"%d",selectedindex);
    
    
    if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:indexPath.row]])
    {
        [btn_TableCell_CheckBox1 setSelected:YES];
    }
    else{
        [btn_TableCell_CheckBox1 setSelected:NO];
    }
    
    
    
    //    img_backGnd2 = [[UIImageView alloc]init];
    //
    //
    //    if (IS_IPHONE_6Plus){
    //        img_backGnd2.frame = CGRectMake(0, 0,((WIDTH-65)/3),50);
    //    }
    //    else if (IS_IPHONE_6){
    //        img_backGnd2.frame = CGRectMake(0, 0,40,50);
    //    }
    //    else  {
    //        img_backGnd2.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
    //    }
    //    img_backGnd2.backgroundColor = [UIColor clearColor];
    //    [cell.contentView addSubview:img_backGnd2];
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (IS_IPHONE_6Plus){
        return CGSizeMake((195)/3, 30);
    }
    else if (IS_IPHONE_6){
        return CGSizeMake((200)/3, 70);
    }
    else  {
        return CGSizeMake((200)/3, 30);
    }
    
    return CGSizeMake(0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (collectionView == collView_serviceDirectory)
    {
        selectedindex = indexPath.row;
        selected_position= indexPath.row;
        [self click_AddPhoto];
    }
    
    
}


-(void) click_selectObjectAt:(UIButton *) sender
{
    
    
    if (sender.tag==0 && a0==1)
    {
        
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:sender.tag]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                [Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:sender.tag]];
                
            }
            
            
            
        }
        
        
    }
    else if (sender.tag==1&& a1==1)
    {
        
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:sender.tag]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                
                [Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:sender.tag]];
                
            }
            
            
            
        }
        
        
        
    }
    else if (sender.tag==2&& a2==1)
    {
        
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:sender.tag]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                [Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:sender.tag]];
                
            }
            
            
            
        }
        
        
        
    }
    else if (sender.tag==3&& a3==1)
    {
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:sender.tag]])
        {
            [Arr_temp removeAllObjects];
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                [Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:sender.tag]];
                
            }
            
            
            
        }
        
        
    }
    else if (sender.tag==4&& a4==1)
    {
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:sender.tag]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                [Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:sender.tag]];
                
            }
            
            
            
        }
        
        
    }
    else if (sender.tag==5&& a5==1)
    {
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:sender.tag]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                [Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:sender.tag]];
                
            }
            
            
            
        }
        
        
    }
    
    else if (sender.tag==6&& a6==1)
    {
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:sender.tag]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                [Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:sender.tag]];
                
            }
            
            
            
        }
        
        
    }
    else if (sender.tag==7&& a7==1)
    {
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:sender.tag]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                [Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:sender.tag]];
                
            }
            
            
            
        }
        
        
    }
    
    else if (sender.tag==8&& a8==1)
    {
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:sender.tag]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                [Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:sender.tag]];
                
            }
            
            
            
        }
        
        
    }
    else if (sender.tag==9&& a9==1)
    {
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:sender.tag]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                [Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:sender.tag]];
                
            }
            
            
            
        }
        
        
    }
    else if (sender.tag==8&& a10==1)
    {
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:sender.tag]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                [Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:sender.tag]];
                
            }
            
            
            
        }
        
        
    }
    //    else if (sender.tag==9&& a11==1)
    //    {
    //        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:sender.tag]])
    //        {
    //            [Arr_temp removeAllObjects];
    //        }
    //        else
    //        {
    //
    //
    //            if ([arr_SelectedIndex count]>0)
    //            {
    //                [Arr_temp removeAllObjects];
    //                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:sender.tag]];
    //
    //            }
    //
    //
    //
    //        }
    //
    //
    //    }
    
    else
    {
        [self popup_Alertview:@"Please select the media file"];
        
    }
    
    
    
    //[collectionView_FashionCategory reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:sender.tag inSection:0]]  withRowAnimation:UITableViewRowAnimationFade];
    
    
    [collView_serviceDirectory reloadData];
    
    
    
}


-(void)click_AddPhoto
{
    UIActionSheet *sharingSheet = [[UIActionSheet alloc] initWithTitle:@"Please Select"
                                                              delegate:self
                                                     cancelButtonTitle:@"Cancel"
                                                destructiveButtonTitle:nil
                                                     otherButtonTitles:@"Video",
                                   @"Photo", nil];
    sharingSheet.tag = 100;
    [sharingSheet showInView:self.view];
    
    
}


#pragma mark - IMAGEPICKER DELEGATE METHODS
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
        NSURL * URL_Orgvideo = [info objectForKey:UIImagePickerControllerMediaURL];
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:URL_Orgvideo options:nil];
        AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        gen.appliesPreferredTrackTransform = YES;
        CMTime time = CMTimeMakeWithSeconds(0.0,600);
        NSError *error = nil;
        CMTime actualTime;
        CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
        UIImage *thumb = [[UIImage alloc] initWithCGImage:image];
        CGImageRelease(image);
        
        Data_video_thumb= UIImagePNGRepresentation(thumb);
        
        AVAsset *movie = [AVAsset assetWithURL:URL_Orgvideo];
        CMTime movieLength = movie.duration;
        int seconds = CMTimeGetSeconds(movieLength);
        
        if (seconds<=7)
        {
            if (int_SelectedVideo == selectedindex)
            {
                selectvideo=YES;
            }
            
            if (selectvideo==YES)
            {
                
                NSURL  *URL_Orgvideo=[info objectForKey:UIImagePickerControllerMediaURL];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd_HH-mm-ss"];
                
                
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"mergeVideo-%d.mp4",arc4random() % 1000]];
                NSURL *videoURL = [NSURL fileURLWithPath:myPathDocs];
                
                
                
                AVAsset *asset2 = [AVAsset assetWithURL:URL_Orgvideo];
                AVAssetExportSession *exportSession = [AVAssetExportSession exportSessionWithAsset:asset2
                                                                                        presetName:AVAssetExportPresetMediumQuality];
                //AVAssetExportPresetLowQuality
                exportSession.outputURL = videoURL;
                exportSession.outputFileType = AVFileTypeMPEG4;
                //   exportSession.outputFileType = AVFileTypeQuickTimeMovie;
                
                
                
                
                [exportSession exportAsynchronouslyWithCompletionHandler:^
                 {
                     if (AVAssetExportSessionStatusCompleted == exportSession.status)
                     {
                         NSLog(@"Export OK");
                         
                         data_Video=[[NSMutableData alloc]initWithContentsOfURL:videoURL];
                         
                     }
                     else if (AVAssetExportSessionStatusFailed == exportSession.status)
                     {
                         NSLog(@"Export failed: %@", [[exportSession error] localizedDescription]);
                         
                     }
                 }];
                
                
                
                int_SelectedVideo = selectedindex;
                [arry_imageArry replaceObjectAtIndex:selectedindex withObject:thumb];
                selectvideo=NO;
                
            }
            else
            {
                [self popup_Alertview:@"you had Already select a video"];
                
                
                
            }
            
            
            
            
        }
        else
        {
            [self popup_Alertview:@"List a video up to 7 seconds"];
        }
        
        
    }
    else
    {
        
        if (int_SelectedVideo == selectedindex)
        {
            selectvideo=YES;
        }
        
        NSURL * URL_Orgvideo = [info objectForKey:UIImagePickerControllerMediaURL];
        
        UIImage *imageOriginal;
        imageOriginal =  [info objectForKey:UIImagePickerControllerEditedImage];
        
        //     imageOriginal =  [info objectForKey:UIImagePickerControllerQualityType640x480];
        
        
        
        //        [self presentViewController:editor animated:YES completion:NULL];
        
        //    [self.navigationController pushViewController:editor];
        
        
        NSData *imageData =  UIImageJPEGRepresentation(imageOriginal,0.5);
        [arry_imageArry replaceObjectAtIndex:selectedindex withObject:imageOriginal];
        NSLog(@"%d",selectedindex);
        
        
        if (selectedindex==0)
        {
            img_productImg0.image=imageOriginal;
        }
        else if (selectedindex==1)
        {
            img_productImg1.image=imageOriginal;
        }
        else if (selectedindex==2)
        {
            img_productImg2.image=imageOriginal;
        }
        else if(selectedindex==3)
        {
            img_productImg3.image=imageOriginal;
        }
        else if (selectedindex==4)
        {
            img_productImg4.image=imageOriginal;
        }
        else if (selectedindex==5)
        {
            img_productImg5.image=imageOriginal;
        }
        else if (selectedindex==6)
        {
            img_productImg6.image=imageOriginal;
        }
        else if(selectedindex==7)
        {
            img_productImg7.image=imageOriginal;
        }
        else if (selectedindex==8)
        {
            img_productImg8.image=imageOriginal;
        }
        else if (selectedindex==9)
        {
            img_productImg9.image=imageOriginal;
        }
        else if (selectedindex==10)
        {
            img_productImg10.image=imageOriginal;
        }
        //        else if (selectedindex==11)
        //        {
        //            img_productImg11.image=imageOriginal;
        //        }
        
        
        [arr_data replaceObjectAtIndex:selectedindex withObject:imageData];
        
        [collView_serviceDirectory reloadData];
        
        
        
    }
    
    
    [arr_particularindex addObject:[NSString stringWithFormat:@"%d",selectedindex]];
    
    NSLog(@"%@",arr_particularindex);
    NSArray *copy = [arr_particularindex copy];
    NSInteger index = [copy count] - 1;
    for (id object in [copy reverseObjectEnumerator])
    {
        if ([arr_particularindex indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound) {
            [arr_particularindex removeObjectAtIndex:index];
        }
        index--;
    }
    NSLog(@"%@",arr_particularindex);
    
    if (selectedindex==0)
    {
        a0=1;
    }
    else if (selectedindex==1)
    {
        a1=1;
        
    }
    else if (selectedindex==2)
    {
        a2=1;
        
        
    }
    else if (selectedindex==3)
    {
        a3=1;
        
    }
    else if (selectedindex==4)
    {
        a4=1;
        
    }
    else if (selectedindex==5)
    {
        a5=1;
    }
    else if (selectedindex==6)
    {
        a6=1;
    }
    else if(selectedindex==7)
    {
        a7=1;
    }
    else if (selectedindex==8)
    {
        a8=1;
    }
    else if (selectedindex==9)
    {
        a9=1;
    }
    else if (selectedindex==10)
    {
        a10=1;
    }
    //    else if (selectedindex==11)
    //    {
    //        a11=1;
    //    }
    
    //[arry_FashionImageArry replaceObjectAtIndex:int_ImageIndex withObject:dict_Final];
    int_ImageIndex = -1;
    [picker dismissViewControllerAnimated:YES completion:Nil];
    [collView_serviceDirectory reloadData];
    
    
    if (selectedindex==0 && a0==1)
    {
        
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:selectedindex]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                //[Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:selectedindex]];
                
            }
            
            
            
        }
        
        
    }
    else if (selectedindex==1&& a1==1)
    {
        
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:selectedindex]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                
                // [Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:selectedindex]];
                
            }
            
            
            
        }
        
        
        
    }
    else if (selectedindex==2&& a2==1)
    {
        
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:selectedindex]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                // [Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:selectedindex]];
                
            }
            
            
            
        }
        
        
        
    }
    else if (selectedindex==3&& a3==1)
    {
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:selectedindex]])
        {
            [Arr_temp removeAllObjects];
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                // [Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:selectedindex]];
                
            }
            
            
            
        }
        
        
    }
    else if (selectedindex==4&& a4==1)
    {
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:selectedindex]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                //  [Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:selectedindex]];
                
            }
            
            
            
        }
        
        
    }
    else if (selectedindex==5&& a5==1)
    {
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:selectedindex]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                // [Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:selectedindex]];
                
            }
            
            
            
        }
        
        
    }
    
    else if (selectedindex==6&& a6==1)
    {
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:selectedindex]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                // [Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:selectedindex]];
                
            }
            
            
            
        }
        
        
    }
    else if (selectedindex==7&& a7==1)
    {
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:selectedindex]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                //[Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:selectedindex]];
                
            }
            
            
            
        }
        
        
    }
    
    else if (selectedindex==8&& a8==1)
    {
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:selectedindex]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                //[Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:selectedindex]];
                
            }
            
            
            
        }
        
        
    }
    else if (selectedindex==9&& a9==1)
    {
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:selectedindex]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                // [Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:selectedindex]];
                
            }
            
            
            
        }
        
        
    }
    else if (selectedindex==10&& a10==1)
    {
        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:selectedindex]])
        {
            [Arr_temp removeAllObjects];
        }
        else
        {
            
            
            if ([arr_SelectedIndex count]>0)
            {
                //[Arr_temp removeAllObjects];
                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:selectedindex]];
                
            }
            
            
            
        }
        
        
    }
    //    else if (selectedindex==11&& a11==1)
    //    {
    //        if([Arr_temp containsObject:[arr_SelectedIndex objectAtIndex:selectedindex]])
    //        {
    //            [Arr_temp removeAllObjects];
    //        }
    //        else
    //        {
    //
    //
    //            if ([arr_SelectedIndex count]>0)
    //            {
    //                // [Arr_temp removeAllObjects];
    //                [Arr_temp addObject:[arr_SelectedIndex objectAtIndex:selectedindex]];
    //
    //            }
    //
    //
    //
    //        }
    //
    //
    //    }
    
    else
    {
        [self popup_Alertview:@"Please select the media file"];
        
    }
    
    
    
    //[collectionView_FashionCategory reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:sender.tag inSection:0]]  withRowAnimation:UITableViewRowAnimationFade];
    
    
    [collView_serviceDirectory reloadData];
    
    
}




-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:Nil];
}



-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (actionSheet.tag == 100)
    {
        
        if (buttonIndex == 1)
        {
            //Photo button select ----> it will ask camera or library
            //Step1--> PHOTO
            UIActionSheet *sharingSheetphoto = [[UIActionSheet alloc] initWithTitle:@"Please Select"
                                                                           delegate:self
                                                                  cancelButtonTitle:@"Cancel"
                                                             destructiveButtonTitle:nil
                                                                  otherButtonTitles:@"Camera",
                                                @"Photo Library", nil];
            sharingSheetphoto.tag = 300;
            
            [sharingSheetphoto showInView:self.view];
            
            
        }
        else
        {
            if (buttonIndex == 0)
            {
                //Step01 -->selected Video from images and video option
                
                [self performSelector:@selector(showVideoOption:) withObject:nil afterDelay:3];
                
                [self popup_Alertview:@"List a video up to 7 seconds"];
                
            }
            
            
        }
        
        
        
    }
    if (actionSheet.tag == 200)
    {
        
        
        if (buttonIndex == 0)
        {
            
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                //Select a video from camera
            {
                
                UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                      message:@"Device has no camera"
                                                                     delegate:nil
                                                            cancelButtonTitle:@"OK"
                                                            otherButtonTitles: nil];
                
                [myAlertView show];
                
            }
            else{
                
                //                [self startCameraControllerFromViewController: self
                //                                                usingDelegate: self];
                
                //Record a video from Camera
                
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                
                picker.allowsEditing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                picker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeMovie, nil];
                [self presentViewController:picker animated:YES completion:NULL];
                clear_data=NO;
                
            }
            
            
        }
        
        
        else if (buttonIndex == 1)
        {
            //Select a video from library
            
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            picker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeMovie, nil];
            [self presentViewController:picker animated:YES completion:NULL];
            clear_data=NO;
        }
    }
    if (actionSheet.tag == 300)
    {
        
        if (buttonIndex == 0)
        {
            //PHOTO STEP :02 --->Select Camera
            
            //PHOTO STEP :02 A--->Select Camera -->No Camera Found
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                      message:@"Device has no camera"
                                                                     delegate:nil
                                                            cancelButtonTitle:@"OK"
                                                            otherButtonTitles: nil];
                [myAlertView show];
                
            }
            else
            {
                //PHOTO STEP :02 A--->Take Picture From Camera
                
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.allowsEditing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:picker animated:YES completion:NULL];
                clear_data=NO;
            }
        }
        else if (buttonIndex == 1)
        {
            //PHOTO STEP :02 B--->Selectphoto from library
            
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:picker animated:YES completion:NULL];
            clear_data=NO;
            
        }
    }
    
}


-(void)showVideoOption:(id)info
{
    UIActionSheet *sharingSheetvideo = [[UIActionSheet alloc] initWithTitle:@"Please Select"
                                                                   delegate:self
                                                          cancelButtonTitle:@"Cancel"
                                                     destructiveButtonTitle:nil
                                                          otherButtonTitles:@"Camera",
                                        @"Photo Library", nil];
    sharingSheetvideo.tag = 200;
    [sharingSheetvideo showInView:self.view];
    
}

# pragma mark ChefSignupSecond method


-(void)SignupSecond
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            
                            @"field_kitchen_name"                                :  txt_Name.text,
                            @"uid"                                               : [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            
                            @"kitchen_address"                                   :  txt_street.text,
                            @"kcountry_name"                                     :  txt_country.text,
                            @"kcity_name"                                        :  txt_city.text,
                            @"kpostal_code"                                      :  txt_postolcode.text,
                            @"field_store_food_description"                      :  txt_view.text,
                            @"field_hygiene_standard_descripti"                  :  txt_view1.text,
                            @"registration_page"                                   : @"2",
                            @"role_type"                                         :  @"chef_profile",
                            @"device_udid"                                       :  UniqueAppID,
                            @"device_token"                                      :  @"Dev",
                            @"device_type"                                       :  @"1"
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    //    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
    //                                                            path:AFChefSignUpFirst
    //                                                      parameters:params];
    
    
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:AFChefSignUpFirst parameters:params constructingBodyWithBlock:^(id <AFMultipartFormData>formData)
                                    {
                                        
                                        //    for (int i = 0; i<[arry_AddProductImageArry count]; i++)
                                        
                                        
                                        if ([arr_particularindex count]==0 && (int_SelectedVideo!=-1))
                                        {
                                            
                                            
                                            // [formData appendPartWithFileData: Data_video_thumb name:@"videothumb" fileName:[NSString stringWithFormat:@"image.png"] mimeType:@"image/jpeg"];
                                            
                                            
                                            [formData appendPartWithFileData:data_Video  name:@"video_url" fileName:[NSString stringWithFormat:@"video.mp4"] mimeType:@"video/mp4"];
                                        }
                                        
                                        
                                        for (int i = 0; i<[arr_particularindex count]; i++)
                                        {
                                            NSTimeInterval timeInterval = [NSDate timeIntervalSinceReferenceDate];
                                            
                                            
                                            int mm=[[arr_particularindex objectAtIndex:i] integerValue];
                                            NSLog(@"arr_particularindex    %d",mm);
                                            if (i==int_SelectedVideo)
                                            {
                                                
                                                //  [formData appendPartWithFileData: Data_video_thumb name:@"videothumb" fileName:[NSString stringWithFormat:@"%lf-image%d.png",timeInterval,i] mimeType:@"image/jpeg"];
                                                
                                                
                                                [formData appendPartWithFileData: [arr_data objectAtIndex:mm] name:[NSString stringWithFormat:@"kitchen_img%d",i+1] fileName:[NSString stringWithFormat:@"%lf-image%d.png",timeInterval,i+1] mimeType:@"image/jpeg"];
                                                
                                                
                                                [formData appendPartWithFileData:data_Video  name:@"video_url" fileName:[NSString stringWithFormat:@"%lf-video%d.mp4",timeInterval,i] mimeType:@"video/mp4"];
                                                
                                            }
                                            else
                                            {
                                                
                                                
                                                [formData appendPartWithFileData: [arr_data objectAtIndex:mm] name:[NSString stringWithFormat:@"kitchen_img%d",i+1] fileName:[NSString stringWithFormat:@"%lf-image%d.png",timeInterval,i+1] mimeType:@"image/jpeg"];
                                            }
                                            
                                            
                                            
                                            
                                        }
                                        
                                    }];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpSecond:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self SignupSecond];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpSecond :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        Chefsignup3 *chefsignup3=[[Chefsignup3 alloc]init];
        [self presentViewController:chefsignup3 animated:NO completion:nil];
        
        
        
        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        txt_Name.text=@"";
        txt_street.text=@"";
        txt_country.text=@"";
        txt_city.text=@"";
        txt_postolcode.text=@"";
        txt_view.text=@"";
        txt_view1.text=@"";
        
        
        
    }
    
}

# pragma mark Hcountry method

-(void)countryList
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpHcountry
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpHcountry:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self countryList];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpHcountry :(NSDictionary * )TheDict
{
    [ary_countrylist removeAllObjects];
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"Country_CityList"] count]; i++)
        {
            [ary_countrylist addObject:[[TheDict valueForKey:@"Country_CityList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        txt_country.text=@"";
        
        
    }
    
    [tableview_Kcountry reloadData];
    
    
    
    
}

//-(void)click_Done
//{
//    [Mobilenumber resignFirstResponder];
//
//}



# pragma mark Hcity method

-(void)cityList
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            
                            
                            @"country_name"            :  txt_country.text,
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpHcity
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpHcity:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self cityList];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpHcity :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    [ary_citylist removeAllObjects];
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        for (int i=0; i<[[TheDict valueForKey:@"City_List"] count]; i++)
        {
            [ary_citylist addObject:[[TheDict valueForKey:@"City_List"] objectAtIndex:i]];
        }
        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        txt_city.text=@"";
        
        
    }
    
    [tableview_Kcity reloadData];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
