//
//  ChefSignup2.h
//  Not86
//
//  Created by Interwld on 8/12/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <QuartzCore/QuartzCore.h>

@interface ChefSignup2 : UIViewController<UIScrollViewDelegate,UITextFieldDelegate>
{
    
}
@property(nonatomic,strong)NSString *street_valve;
@property(nonatomic,strong)NSString *country_name;
@property(nonatomic,strong)NSString *city_name;
@property(nonatomic,strong)NSString *portal_code;
@end