
//
//  LoginViewController.m
//  Not86
//
//  Created by Interwld on 8/3/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "Define.h"
#import "ForgetpasswordVC.h"
#import "JWSlideMenuViewController.h"
#import "JWSlideMenuController.h"
#import "HomeViewController1.h"

#import "MessagesVC.h"
#import "SignupViewController.h"
#import "ChefSignup.h"
#import "ChefSignup2.h"
#import "Chefsignup3.h"
#import "ChefHomeViewController.h"
#import "HomeVC.h"
#import "MenuScreenVC.h"
#import "MyScheduleScreenVC.h"
#import "MyProfileVC.h"
#import "MyHomescreenVC.h"
#import "MyScheduleVC.h"
#import "MyMenuVC.h"
#import "MyAccountVC.h"
#import "MessagesVC.h"
#import "SwitchtoUser.h"
#import "MessageListVC.h"
#import "SerchForFoodNowVC.h"
#import "DiningCartVC.h"
#import "ChefProfileVC.h"
#import "NotificationsVC.h"
#import "ChefMyProfileVC.h"
#import "ChefOrdersVC.h"
#import "AccountVC.h"
#import "ChefMessageListVC.h"
#import "NotificationSettingsVC.h"
#import "MyordersVC.h"
#import "ViewMenuVC.h"





#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#import "SSKeychain.h"
static NSString *kSSToolkitTestsServiceName =@"KIVSSToolkitTestService";


@interface LoginViewController ()<UITextViewDelegate,UITextFieldDelegate,UIScrollViewDelegate>
{
    UIImageView *img_BackgroundImg;
    UIImageView *img_topbar;
    AppDelegate *delegate;
    UITextField *txt_username;
    UITextField *txt_Password;
    UIView *view_popup;
    UIImageView *img_alertpop;
    UIView *alertviewBg;
    UIView *view_Forgetpassword;
    
    CGFloat	animatedDistance;
    UITextField *txt_Emailid;
    UIButton *btn_Cancel;
    UIButton *btn_Submitt;
    NSString *str_Rememberme;
    UIButton *btn_check;
    BOOL rememberMe;
    
}

@end

@implementation LoginViewController


static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

//static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
//static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
//static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
//static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 160;//216
//static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self IntegrateHeaderDesign];
    [self IntegrateBodyDesign];
    // [self IntegrateView];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    str_Rememberme=@"YES";
    
    [self popup_Forgetpassword];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    
    if([defaults valueForKey:@"loginKey"])
    {
        NSArray *accounts = [SSKeychain allAccounts];
        accounts = [SSKeychain accountsForService:kSSToolkitTestsServiceName];
        if(accounts.count>0)
        {
            txt_username.text=[[accounts lastObject] valueForKey:@"acct"];
            NSString *password = [SSKeychain passwordForService:kSSToolkitTestsServiceName account:txt_username.text];
            txt_Password.text=password;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)IntegrateHeaderDesign
{
    img_BackgroundImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    [img_BackgroundImg setUserInteractionEnabled:YES];
    img_BackgroundImg.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:img_BackgroundImg];
    
    
    
    img_topbar=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,45)];
    [img_topbar setUserInteractionEnabled:YES];
    img_topbar.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:img_topbar];
    
    
    UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(10, 15, 17, 15)];
    [img_back setUserInteractionEnabled:YES];
    img_back.backgroundColor=[UIColor clearColor];
    img_back.image=[UIImage imageNamed:@"arrow@2x.png"];
    [img_topbar addSubview:img_back];
    
    UIButton *btn_back = [[UIButton alloc] init];
    btn_back.frame = CGRectMake(0, 0, 60,35);
    btn_back.backgroundColor = [UIColor clearColor];
    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_topbar addSubview:btn_back];
    
    
    
    
    UILabel  * lbl_heading = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(img_back.frame)+20,-2, 220,45)];
    lbl_heading.text = @"Login";
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:kFont size:20];
    [img_topbar addSubview:lbl_heading];
    
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 10, 30, 30)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-login-logo@2x.png"];
    [img_topbar addSubview:img_logo];
    
    
    
}
-(void)IntegrateBodyDesign
{
    
    UIImageView *img_logo=[[UIImageView alloc]init];
    //    img_logo.frame=CGRectMake(20, CGRectGetMaxY(img_topbar.frame)+40, WIDTH-60, 90);
    if (IS_IPHONE_6Plus) {
        img_logo.frame=CGRectMake(45, CGRectGetMaxY(img_topbar.frame)+40, WIDTH-120,85);
    }
    else if (IS_IPHONE_6) {
        img_logo.frame=CGRectMake(40, CGRectGetMaxY(img_topbar.frame)+40, WIDTH-80, 90);
    }
    else
    {
        img_logo.frame=CGRectMake(30, CGRectGetMaxY(img_topbar.frame)+35, WIDTH-80, 75);
    }
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-logo-text@2x.png"];
    [img_BackgroundImg addSubview:img_logo];
    
    UIImageView *img_user=[[UIImageView alloc]init];
    //    img_user.frame=CGRectMake(20, CGRectGetMaxY(img_logo.frame)+70, WIDTH-40, 1);
    if (IS_IPHONE_6Plus) {
        img_user.frame=CGRectMake(26, CGRectGetMaxY(img_logo.frame)+73, WIDTH-60, 0.5);   }
    else if (IS_IPHONE_6) {
        img_user.frame=CGRectMake(24, CGRectGetMaxY(img_logo.frame)+73, WIDTH-60, 0.5);    }
    else
    {
        img_user.frame=CGRectMake(24, CGRectGetMaxY(img_logo.frame)+73, WIDTH-50, 0.5);    }
    
    img_user.backgroundColor=[UIColor lightGrayColor];
    [img_user setUserInteractionEnabled:YES];
    [img_BackgroundImg addSubview:img_user];
    
    txt_username = [[UITextField alloc] init];
    //    txt_username.frame=CGRectMake(20, CGRectGetMaxY(img_logo.frame)+35, WIDTH-80, 38);
    if (IS_IPHONE_6Plus) {
        txt_username.frame=CGRectMake(20, CGRectGetMaxY(img_logo.frame)+35, WIDTH-80, 38);
    }
    else if (IS_IPHONE_6) {
        txt_username.frame=CGRectMake(20, CGRectGetMaxY(img_logo.frame)+35, WIDTH-80, 38);
    }
    else
    {
        txt_username.frame=CGRectMake(20, CGRectGetMaxY(img_logo.frame)+35, WIDTH-80, 38);
    }
    
    
    txt_username.borderStyle = UITextBorderStyleNone;
    txt_username.font = [UIFont fontWithName:kFont size:18];
    txt_username.placeholder = @"Username";
    [txt_username setValue:[UIFont fontWithName:kFont size: 18] forKeyPath:@"_placeholderLabel.font"];
    [txt_username setValue:[UIColor colorWithRed:48/255.0f green:41/255.0f blue:42/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_username.leftView = padding1;
    txt_username.leftViewMode = UITextFieldViewModeAlways;
    txt_username.userInteractionEnabled=YES;
    txt_username.textColor = [UIColor colorWithRed:48/255.0f green:41/255.0f blue:42/255.0f alpha:1];
    txt_username.textAlignment = NSTextAlignmentLeft;
    txt_username.backgroundColor = [UIColor clearColor];
    txt_username.keyboardType = UIKeyboardTypeAlphabet;
    txt_username.delegate = self;
    [img_BackgroundImg addSubview:txt_username];
    
    UIImageView *img_pass=[[UIImageView alloc]init];
    //    img_pass.frame=CGRectMake(20, CGRectGetMaxY(img_user.frame)+60, WIDTH-40, 1);
    
    if (IS_IPHONE_6Plus) {
        img_pass.frame=CGRectMake(26, CGRectGetMaxY(img_user.frame)+60, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6) {
        img_pass.frame=CGRectMake(24, CGRectGetMaxY(img_user.frame)+63, WIDTH-60,0.5);
        
    }
    else
    {
        img_pass.frame=CGRectMake(24, CGRectGetMaxY(img_user.frame)+63, WIDTH-50, 0.5);
        
    }
    
    
    img_pass.backgroundColor=[UIColor lightGrayColor];
    [img_pass setUserInteractionEnabled:YES];
    [img_BackgroundImg addSubview:img_pass];
    
    
    txt_Password = [[UITextField alloc] init];
    //    txt_Password.frame=CGRectMake(20, CGRectGetMaxY(img_user.frame)+25, WIDTH-80, 38);
    if (IS_IPHONE_6Plus) {
        txt_Password.frame=CGRectMake(20, CGRectGetMaxY(img_user.frame)+23, WIDTH-80, 38);
    }
    else if (IS_IPHONE_6) {
        txt_Password.frame=CGRectMake(20, CGRectGetMaxY(img_user.frame)+25, WIDTH-80, 38);
    }
    else
    {
        txt_Password.frame=CGRectMake(20, CGRectGetMaxY(img_user.frame)+25, WIDTH-80, 38);
    }
    
    
    txt_Password.borderStyle = UITextBorderStyleNone;
    txt_Password.font = [UIFont fontWithName:kFont size:18];
    txt_Password.placeholder = @"Password";
    [txt_Password setValue:[UIFont fontWithName: kFont size: 18] forKeyPath:@"_placeholderLabel.font"];
    [txt_Password setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_Password.leftView = padding;
    txt_Password.leftViewMode = UITextFieldViewModeAlways;
    txt_Password.userInteractionEnabled=YES;
    txt_Password.textAlignment = NSTextAlignmentLeft;
    txt_Password.backgroundColor = [UIColor clearColor];
    txt_Password.keyboardType = UIKeyboardTypeAlphabet;
    txt_Password.textColor = [UIColor colorWithRed:48/255.0f green:41/255.0f blue:42/255.0f alpha:1];
    txt_Password.delegate = self;
    txt_Password.secureTextEntry=YES;
    [self.view addSubview:txt_Password];
    
    btn_check =[UIButton buttonWithType:UIButtonTypeCustom];
    //    btn_check.frame=CGRectMake(20, CGRectGetMaxY(img_pass.frame)+25,17,15);
    if (IS_IPHONE_6Plus) {
        btn_check.frame=CGRectMake(25, CGRectGetMaxY(img_pass.frame)+30,15,15);
    }
    else if (IS_IPHONE_6) {
        btn_check.frame=CGRectMake(25, CGRectGetMaxY(img_pass.frame)+30,15,15);
    }
    else
    {
        btn_check.frame=CGRectMake(25, CGRectGetMaxY(img_pass.frame)+30,20,20);
    }
    
    
    [btn_check setImage:[UIImage imageNamed:@"img-checkbox@2x.png"] forState:UIControlStateNormal];
    //[btn_check setImage:[UIImage imageNamed:@"img_checkok@2x.png"] forState:UIControlStateSelected];
    [btn_check addTarget:self action:@selector(click_BtnRemember:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn_check];
    
    
    
    UILabel  * lbl_remme = [[UILabel alloc]init];
    //    lbl_remme.frame=CGRectMake(CGRectGetMaxX(btn_check.frame)+15, CGRectGetMaxY(img_pass.frame)+20, WIDTH, 25);
    if (IS_IPHONE_6Plus) {
        lbl_remme.frame=CGRectMake(CGRectGetMaxX(btn_check.frame)+10, CGRectGetMaxY(img_pass.frame)+25, WIDTH, 25);
    }
    else if (IS_IPHONE_6) {
        lbl_remme.frame=CGRectMake(CGRectGetMaxX(btn_check.frame)+10, CGRectGetMaxY(img_pass.frame)+26, WIDTH, 25);
    }
    else
    {
        lbl_remme.frame=CGRectMake(CGRectGetMaxX(btn_check.frame)+10, CGRectGetMaxY(img_pass.frame)+27, WIDTH, 25);
    }
    
    
    lbl_remme.text = @"Remember me";
    lbl_remme.backgroundColor=[UIColor clearColor];
    lbl_remme.textColor=[UIColor blackColor];
    lbl_remme.textAlignment=NSTextAlignmentLeft;
    lbl_remme.font = [UIFont fontWithName:kFont size:14];
    [self.view addSubview:lbl_remme];
    
    
    
    
    UILabel  * lbl_forget = [[UILabel alloc]init];
    
    //    lbl_forget.frame= CGRectMake(WIDTH-150,CGRectGetMaxY(img_pass.frame)+25, WIDTH, 18);
    if (IS_IPHONE_6Plus) {
        lbl_forget.frame= CGRectMake(WIDTH-150,CGRectGetMaxY(img_pass.frame)+25, WIDTH, 25);
        lbl_forget.font = [UIFont fontWithName:kFont size:14];
    }
    else if (IS_IPHONE_6) {
        lbl_forget.frame= CGRectMake(WIDTH-145,CGRectGetMaxY(img_pass.frame)+26, WIDTH, 25);
        lbl_forget.font = [UIFont fontWithName:kFont size:13];
    }
    else
    {
        lbl_forget.frame= CGRectMake(WIDTH-140,CGRectGetMaxY(img_pass.frame)+27, WIDTH, 25);
        lbl_forget.font = [UIFont fontWithName:kFont size:14];
    }
    
    
    lbl_forget.text = @"Forgot Password?";
    lbl_forget.backgroundColor=[UIColor clearColor];
    lbl_forget.textColor=[UIColor blackColor];
    lbl_forget.textAlignment=NSTextAlignmentLeft;
    //    lbl_forget.font = [UIFont fontWithName:kFont size:16];
    [self.view addSubview:lbl_forget];
    
    
    
    
    UIButton *btn_forgot =[UIButton buttonWithType:UIButtonTypeCustom];
    //    btn_forgot.frame=CGRectMake(WIDTH-115, CGRectGetMaxY(img_pass.frame), 100, 40);
    if (IS_IPHONE_6Plus) {
        btn_forgot.frame=CGRectMake(WIDTH-115, CGRectGetMaxY(img_pass.frame)+28, 100, 40);
    }
    else if (IS_IPHONE_6) {
        btn_forgot.frame=CGRectMake(WIDTH-160, CGRectGetMaxY(img_pass.frame)+26, 150, 40);
    }
    else
    {
        btn_forgot.frame=CGRectMake(WIDTH-150, CGRectGetMaxY(img_pass.frame)+20, 120, 30);
    }
    
    
    [btn_forgot addTarget:self action:@selector(click_BtnForgotPass:) forControlEvents:UIControlEventTouchUpInside];
    btn_forgot.backgroundColor=[UIColor clearColor];
    [self.view addSubview:btn_forgot];
    
    
    UIImageView *img_btnlogin=[[UIImageView alloc]init];
    //    img_user.frame=CGRectMake(20, CGRectGetMaxY(img_logo.frame)+70, WIDTH-40, 1);
    if (IS_IPHONE_6Plus) {
        img_btnlogin.frame=CGRectMake(20, CGRectGetMaxY(lbl_forget.frame)+20, WIDTH-50, 55);
    }
    else if (IS_IPHONE_6) {
        img_btnlogin.frame=CGRectMake(20, CGRectGetMaxY(lbl_forget.frame)+20, WIDTH-50,55);
    }
    else
    {
        img_btnlogin.frame=CGRectMake(20, CGRectGetMaxY(lbl_forget.frame)+20, WIDTH-40, 50);
    }
    img_btnlogin.image=[UIImage imageNamed:@"img-login@2x.png"];
    //    img_btnlogin.backgroundColor=[UIColor lightGrayColor];
    [img_btnlogin setUserInteractionEnabled:YES];
    [self.view addSubview:img_btnlogin];
    
    
    
    
    UIButton *btn_login =[UIButton buttonWithType:UIButtonTypeCustom];
    //    [btn_login setImage:[UIImage imageNamed:@"img-login@2x.png"] forState:UIControlStateNormal];
    if (IS_IPHONE_6Plus) {
        btn_login.frame=CGRectMake(20, CGRectGetMaxY(lbl_forget.frame)+20, WIDTH-40, 70);
    }
    else if (IS_IPHONE_6) {
        btn_login.frame=CGRectMake(20, CGRectGetMaxY(lbl_forget.frame)+40, WIDTH-40, 60);
    }
    else
    {
        btn_login.frame=CGRectMake(20, CGRectGetMaxY(lbl_forget.frame)+20, WIDTH-40, 50);
    }
    [btn_login addTarget:self action:@selector(click_Loginbtn) forControlEvents:UIControlEventTouchUpInside];
    
    btn_login.backgroundColor=[UIColor clearColor];
    [self.view addSubview:btn_login];
    
    NSArray *accounts = [SSKeychain allAccounts];
    accounts = [SSKeychain accountsForService:kSSToolkitTestsServiceName];
    if(accounts.count>0)
    {
        txt_username.text=[[accounts lastObject] valueForKey:@"acct"];
        NSString *password = [SSKeychain passwordForService:kSSToolkitTestsServiceName account:txt_username.text];
        txt_Password.text=password;
        rememberMe=YES;
    }
    
    
    
}
-(void)Back_btnClick
{
    NSLog(@"Back_btnClick");
    //    [self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}


-(void)popup_Forgetpassword
{
    
    
    [view_Forgetpassword removeFromSuperview];
    view_Forgetpassword=[[UIView alloc] init];
    view_Forgetpassword.frame=CGRectMake(0 , 0, WIDTH, HEIGHT);
    view_Forgetpassword.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.7];
    view_Forgetpassword.userInteractionEnabled=TRUE;
    [self.view addSubview: view_Forgetpassword];
    
    UIImageView *alertView_Body =[[UIImageView alloc] init];
    if (IS_IPHONE_6Plus) {
        alertView_Body.frame=CGRectMake(50, 220, WIDTH-100, 180);
    }
    else if (IS_IPHONE_6) {
        alertView_Body.frame=CGRectMake(50, 220, WIDTH-100, 180);
    }
    else
    {
        alertView_Body.frame=CGRectMake(50, 200, WIDTH-100, 155);
    }
    
    
    //    [alertViewBody setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    alertView_Body.backgroundColor=[UIColor whiteColor];
    alertView_Body.userInteractionEnabled = YES;
    [ view_Forgetpassword addSubview:alertView_Body];
    
    
    
    UILabel  * lbl_forgetpassword = [[UILabel alloc]init];
    
    //    lbl_forget.frame= CGRectMake(WIDTH-150,CGRectGetMaxY(img_pass.frame)+25, WIDTH, 18);
    if (IS_IPHONE_6Plus) {
        lbl_forgetpassword.frame= CGRectMake(70,15, 230, 40);
    }
    else if (IS_IPHONE_6) {
        lbl_forgetpassword.frame= CGRectMake(50,15, 230, 40);
    }
    else
    {
        lbl_forgetpassword.frame= CGRectMake(25,15, 180, 30);
    }
    
    
    lbl_forgetpassword.text = @"Forgot Password";
    lbl_forgetpassword.backgroundColor=[UIColor clearColor];
    lbl_forgetpassword.textColor=[UIColor blackColor];
    lbl_forgetpassword.textAlignment=NSTextAlignmentLeft;
    lbl_forgetpassword.font = [UIFont fontWithName:kFontBold size:22];
    [alertView_Body addSubview:lbl_forgetpassword];
    
    
    
    txt_Emailid = [[UITextField alloc] init];
    if (IS_IPHONE_6Plus) {
        txt_Emailid.frame=CGRectMake(10, CGRectGetMaxY(lbl_forgetpassword.frame)+10, 295, 50);
    }
    else if (IS_IPHONE_6) {
        txt_Emailid.frame=CGRectMake(10, CGRectGetMaxY(lbl_forgetpassword.frame)+10, 255, 50);
    }
    else
    {
        txt_Emailid.frame=CGRectMake(10, CGRectGetMaxY(lbl_forgetpassword.frame)+10, 200, 40);
    }
    
    
    txt_Emailid.borderStyle = UITextBorderStyleNone;
    txt_Emailid.font = [UIFont fontWithName:kFont size:14];
    txt_Emailid.placeholder = @"      Email Address";
    [txt_Emailid setValue:[UIFont fontWithName: kFont size: 18] forKeyPath:@"_placeholderLabel.font"];
    [txt_Emailid setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Emailid.leftViewMode = UITextFieldViewModeAlways;
    txt_Emailid.userInteractionEnabled=YES;
    txt_Emailid.textAlignment = NSTextAlignmentLeft;
    txt_Emailid.backgroundColor = [UIColor lightGrayColor];
    txt_Emailid.keyboardType = UIKeyboardTypeAlphabet;
    txt_Emailid.textColor = [UIColor blackColor];
    txt_Emailid.delegate = self;
    [alertView_Body addSubview:txt_Emailid];
    
    
    
    UIImageView *img_Line=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus) {
        img_Line.frame=CGRectMake(10, CGRectGetMaxY(txt_Emailid.frame)+10, 295, 0.5);
    }
    else if (IS_IPHONE_6) {
        img_Line.frame=CGRectMake(10, CGRectGetMaxY(txt_Emailid.frame)+10, 255, 0.5);
    }
    else
    {
        img_Line.frame=CGRectMake(10, CGRectGetMaxY(txt_Emailid.frame)+10, 200, 0.5);
    }
    
    
    [img_Line setUserInteractionEnabled:YES];
    img_Line.backgroundColor=[UIColor lightGrayColor];
    img_Line.image=[UIImage imageNamed:@"line1.png"];
    [alertView_Body addSubview:img_Line];
    
    
    
    UILabel  * lbl_Cancel = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus) {
        lbl_Cancel.frame= CGRectMake(10, CGRectGetMaxY(img_Line.frame)+2, 147, 60);
    }
    else if (IS_IPHONE_6) {
        lbl_Cancel.frame= CGRectMake(10, CGRectGetMaxY(img_Line.frame)+2, 130, 50);
    }
    else
    {
        lbl_Cancel.frame= CGRectMake(10, CGRectGetMaxY(img_Line.frame)+2, 100, 40);
    }
    
    
    lbl_Cancel.text = @" Cancel";
    lbl_Cancel.backgroundColor=[UIColor clearColor];
    lbl_Cancel.textColor=[UIColor blackColor];
    lbl_Cancel.textAlignment=NSTextAlignmentCenter;
    lbl_Cancel.userInteractionEnabled=YES;
    lbl_Cancel.font = [UIFont fontWithName:kFontBold size:20];
    [alertView_Body addSubview:lbl_Cancel];
    
    
    
    btn_Cancel =[UIButton buttonWithType:UIButtonTypeCustom];
    //    [btn_login setImage:[UIImage imageNamed:@"img-login@2x.png"] forState:UIControlStateNormal];
    if (IS_IPHONE_6Plus) {
        btn_Cancel.frame=CGRectMake(10, CGRectGetMaxY(img_Line.frame)+2, 147, 60);
    }
    else if (IS_IPHONE_6) {
        btn_Cancel.frame=CGRectMake(10, CGRectGetMaxY(img_Line.frame)+2, 130, 50);
    }
    else
    {
        btn_Cancel.frame=CGRectMake(10, CGRectGetMaxY(img_Line.frame)+2, 100, 50);
    }
    [btn_Cancel addTarget:self action:@selector(click_Cancelbtn:) forControlEvents:UIControlEventTouchUpInside];
    
    btn_Cancel.backgroundColor=[UIColor clearColor];
    [alertView_Body addSubview:btn_Cancel];
    
    
    
    
    UIImageView *img_straightLine=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus) {
        img_straightLine.frame=CGRectMake(CGRectGetMaxX(lbl_Cancel.frame)+1, CGRectGetMaxY(img_Line.frame), 1, 50);
    }
    else if (IS_IPHONE_6) {
        img_straightLine.frame=CGRectMake(CGRectGetMaxX(lbl_Cancel.frame)+1, CGRectGetMaxY(img_Line.frame), 1, 50);
    }
    else
    {
        img_straightLine.frame=CGRectMake(CGRectGetMaxX(lbl_Cancel.frame)+1, CGRectGetMaxY(img_Line.frame), 1, 40);
    }
    
    
    [img_straightLine setUserInteractionEnabled:YES];
    img_straightLine.backgroundColor=[UIColor lightGrayColor];
    img_straightLine.image=[UIImage imageNamed:@"line_image@2.png"];
    [alertView_Body addSubview:img_straightLine];
    
    
    
    
    
    
    
    
    UILabel  * lbl_Submitt = [[UILabel alloc]init];
    
    //    lbl_forget.frame= CGRectMake(WIDTH-150,CGRectGetMaxY(img_pass.frame)+25, WIDTH, 18);
    if (IS_IPHONE_6Plus) {
        lbl_Submitt.frame= CGRectMake(170, CGRectGetMaxY(img_Line.frame)+2, 130, 60);
    }
    else if (IS_IPHONE_6) {
        lbl_Submitt.frame= CGRectMake(140, CGRectGetMaxY(img_Line.frame)+2, 130, 50);
    }
    else
    {
        lbl_Submitt.frame= CGRectMake(110, CGRectGetMaxY(img_Line.frame)+2, 100, 40);
        
    }
    
    
    lbl_Submitt.text = @" Send";
    lbl_Submitt.backgroundColor=[UIColor clearColor];
    lbl_Submitt.textColor=[UIColor blackColor];
    lbl_Submitt.textAlignment=NSTextAlignmentCenter;
    lbl_Submitt.userInteractionEnabled=YES;
    lbl_Submitt.font = [UIFont fontWithName:kFontBold size:20];
    [alertView_Body addSubview:lbl_Submitt];
    
    
    
    
    
    btn_Submitt =[UIButton buttonWithType:UIButtonTypeCustom];
    //    [btn_login setImage:[UIImage imageNamed:@"img-login@2x.png"] forState:UIControlStateNormal];
    if (IS_IPHONE_6Plus) {
        btn_Submitt.frame=CGRectMake(170, CGRectGetMaxY(img_Line.frame)+2, 130, 60);
    }
    else if (IS_IPHONE_6) {
        btn_Submitt.frame=CGRectMake(140, CGRectGetMaxY(img_Line.frame)+2, 130, 50);
    }
    else
    {
        btn_Submitt.frame=CGRectMake(110, CGRectGetMaxY(img_Line.frame)+2, 100, 50);
    }
    [btn_Submitt addTarget:self action:@selector(click_Sendbtn:) forControlEvents:UIControlEventTouchUpInside];
    
    btn_Submitt.backgroundColor=[UIColor clearColor];
    [alertView_Body addSubview:btn_Submitt];
    view_Forgetpassword.hidden = YES;
}

-(void)click_BtnRemember:(UIButton *)sender
{
    
    if (rememberMe==YES)
    {
        rememberMe=NO;
        // [img_chek_box setImage:[UIImage imageNamed:@"img_checkbox@2x.png"]];
        
        
        [btn_check setImage:[UIImage imageNamed:@"img-checkbox@2x.png"] forState:UIControlStateNormal];
        
        NSArray *accounts = [SSKeychain allAccounts];
        accounts = [SSKeychain accountsForService:kSSToolkitTestsServiceName];
        
        int count = (int)[accounts count];
        
        for (int i = 0; i < count ; i ++) {
            
            [SSKeychain deletePasswordForService:kSSToolkitTestsServiceName account:[[accounts lastObject] valueForKey:@"acct"]];
        }
        
    }
    else
    {
        
        
        [btn_check setImage:[UIImage imageNamed:@"img_checkok@2x.png"] forState:UIControlStateNormal];
        
        rememberMe=YES;
        [SSKeychain setPassword:txt_Password.text forService:kSSToolkitTestsServiceName account:txt_username.text];
    }
    
    
    
    [txt_username resignFirstResponder];
    [txt_Password resignFirstResponder];
    
    
    
    //    [btn_check setImage:[UIImage imageNamed:@"img-checkbox@2x.png"] forState:UIControlStateNormal];
    //
    //    if([sender isSelected])
    //    {
    //        [sender setSelected:NO];
    //        str_Rememberme=@"YES";
    //        [btn_check setImage:[UIImage imageNamed:@"img_checkok@2x.png"] forState:UIControlStateNormal];
    //    }
    //    else
    //    {
    //
    //        [sender setSelected:YES];
    //        [btn_check setImage:[UIImage imageNamed:@"img-checkbox@2x.png"] forState:UIControlStateNormal];
    //    }
    
    //        if ([sender isSelected]==YES)
    //        {
    //            [sender setSelected:NO];
    //
    //            NSArray *accounts = [SSKeychain allAccounts];
    //            accounts = [SSKeychain accountsForService:kSSToolkitTestsServiceName];
    //            int count = (int)[accounts count];
    //            for (int i = 0; i < count ; i ++) {
    //                [SSKeychain deletePasswordForService:kSSToolkitTestsServiceName account:[[accounts lastObject] valueForKey:@"acct"]];
    //            }
    //        }
    //        else
    //        {
    //            [sender setSelected:YES];
    //            [SSKeychain setPassword:txt_Password.text forService:kSSToolkitTestsServiceName account:txt_username.text];
    //        }
}


#pragma mark TextField Delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //    if(textField == txt_username)
    //    {
    //        if ([string isEqualToString:@" "])
    //        {
    //            return NO;
    //        }
    //    }
    
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    [self resignKeyboard];
    return YES;
}
-(void)resignKeyboard
{
    [txt_Emailid resignFirstResponder];
    
    
    
    
    
}


#pragma mark Validate Email

-(BOOL)ValidateEmail :(NSString *) strToEvaluate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL b = [emailTest evaluateWithObject:strToEvaluate];
    return b;
}
- (NSMutableArray *)validateEmailWithString:(NSString*)emails
{
    NSMutableArray *validEmails = [[NSMutableArray alloc] init];
    NSArray *emailArray = [emails componentsSeparatedByString:@","];
    for (NSString *email in emailArray)
    {
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        if ([emailTest evaluateWithObject:email])
            [validEmails addObject:email];
    }
    return validEmails;
}




-(void)click_BtnForgotPass:(UIButton *)sender
{
    
    
    //    ForgetpasswordVC *forgetpassword=[[ForgetpasswordVC alloc]init];
    //    [self.navigationController pushViewController:forgetpassword animated:YES];
    NSLog(@"click_BtnForgotPass");
    
    view_Forgetpassword.hidden = NO;
    
    //    [self AFForgotPass];
    
    
    
}

-(void)click_Loginbtn
{
    NSLog(@"click_Loginbtn");
    
    //    [self integrateJWSlider];
    
    
    if ([txt_username.text isEqualToString:@""])
    {
        
        [self popup_Alertview:@"Please enter Username"];
        
    }
    else if ([txt_Password.text isEqualToString:@""])
    {        [self popup_Alertview:@"Please enter Password"];
    }
    
    else if ([txt_Password.text length]<8)
    {
        txt_Password.text=@"";
        [self popup_Alertview:@"Password should be minimum of 8 characters"];
    }
    
    else
    {
        [self AFLogIn];
    }
}


-(void)click_Cancelbtn:(UIButton *)sender
{
    
    
    view_Forgetpassword.hidden = YES;
    
    
    
}
-(void)click_Sendbtn:(UIButton *)sender
{
    
    
    if ([txt_Emailid.text isEqualToString:@""])
    {
        
        [self popup_Alertview:@"Please enter Email Id"] ;
        
    }
    
    else
    {
        [self AFForgetPass];
    }
    
    
}
-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    
    //    UILabel *lbl_message = [[UILabel alloc]initWithFrame:CGRectMake(13, 10, 310, 14)];
    //    lbl_message.text = @"INVALID USERNAME OR PASSWORD.PLEASE TRY AGAIN. ";
    //    lbl_message.font = [UIFont fontWithName:kFontBold size:11.0f];
    //    lbl_message.textColor = [UIColor redColor];
    //    [alertViewBody addSubview:lbl_message];
    
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFontBold size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFontBold size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}
-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
    
}




# pragma mark login method

-(void) AFLogIn
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            @"username"                         :   txt_username.text,
                            @"password"                         :   txt_Password.text,
                            @"role_type"                        :   @"chef_profile",
                            @"device_udid"                      :   UniqueAppID,
                            @"device_token"                     :   @"Dev",
                            @"device_type"                      :   @"1",
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kUserLogin
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseLogin:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully login");
                                             [self AFLogIn];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseLogin :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
        [defaults1 removeObjectForKey:@"UserInfo"];
        [defaults1 synchronize];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[[TheDict mutableCopy] valueForKey:@"profile_info"]  forKey:@"UserInfo"];
        [defaults synchronize];
        
        
        [self integrateJWSlider];
        
        
        //        if ([[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"field_last_name"]isEqualToString:@""]||[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"field_mobile_number"]isEqualToString:@""]||[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"field_date_of_birth"]isEqualToString:@""])
        {
            ChefHomeViewController *chefhome=[[ChefHomeViewController alloc]init];
            //    [self.navigationController pushViewController:chefhome animated:NO];
            [self presentViewController:chefhome animated:NO completion:nil];
            
        }
        //        else
        //        {
        
        //        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        txt_username.text = @"";
        txt_Password.text = @"";
        
        
        //        [alertViewBody setHidden:NO];
        
        
    }
    
}
#pragma mark -integrateJWSlider
-(void)integrateJWSlider
{
    
    if ([[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Role"]]isEqualToString:@"1"])
    {
        
        //chef
        
        JWSlideMenuController *slideMenu=[[JWSlideMenuController alloc] init];
        JWSlideMenuViewController *vc_Home=[[ChefHomeViewController alloc] init];
        [slideMenu addViewController:vc_Home  withTitle:@"Home" andImage:[UIImage imageNamed:@"img_home@2x.png"]];
        
        JWSlideMenuViewController *vc_Appointment=[MyScheduleVC new];
        [slideMenu addViewController:vc_Appointment withTitle:@"My\n Schedule" andImage:[UIImage imageNamed:@"img_schedule@2x.png"]];
        
        JWSlideMenuViewController *vc_MyVeh=[ChefOrdersVC new];
        [slideMenu addViewController:vc_MyVeh withTitle:@"Orders" andImage:[UIImage imageNamed:@"img_order@2x.png"]];
        
        JWSlideMenuViewController *vc_Accessories=[ViewMenuVC new];
        [slideMenu addViewController:vc_Accessories withTitle:@"My Menu" andImage:[UIImage imageNamed:@"img_Menusrceen@2x.png"]];
        
        JWSlideMenuViewController *vc_Notification=[AccountVC new];
        [slideMenu addViewController:vc_Notification withTitle:@"Account" andImage:[UIImage imageNamed:@"img_account@2x.png"]];
        
        JWSlideMenuViewController *vc_Promo=[NotificationSettingsVC new];
        [slideMenu addViewController:vc_Promo withTitle:@"Notification" andImage:[UIImage imageNamed:@"img_notification@2x.png"]];
        
        JWSlideMenuViewController *vc_Promo1=[ChefMessageListVC new];
        [slideMenu addViewController:vc_Promo1 withTitle:@"Messages" andImage:[UIImage imageNamed:@"img_msg@2x.png"]];
        
        
        JWSlideMenuViewController *vc_Promo2=[SwitchtoUser new];
        [slideMenu addViewController:vc_Promo2 withTitle:@"Switch to\n User" andImage:[UIImage imageNamed:@"img_swtich@2x.png"]];
        
        [self presentViewController:slideMenu animated:NO completion:nil];
        
        
        
    }
    else {
        //user
        JWSlideMenuController *slideMenu=[[JWSlideMenuController alloc] init];
        JWSlideMenuViewController *vc_Home=[[HomeVC alloc] init];
        [slideMenu addViewController:vc_Home  withTitle:@"Home" andImage:[UIImage imageNamed:@"img_home@2x.png"]];
        
        JWSlideMenuViewController *vc_Appointment=[SerchForFoodNowVC new];
        [slideMenu addViewController:vc_Appointment withTitle:@"Food Now\n Later" andImage:[UIImage imageNamed:@"img_schedule@2x.png"]];
        
        JWSlideMenuViewController *vc_MyVeh=[MyordersVC new];
        [slideMenu addViewController:vc_MyVeh withTitle:@"My Orders" andImage:[UIImage imageNamed:@"img_order@2x.png"]];
        
        JWSlideMenuViewController *vc_Notification=[DiningCartVC new];
        [slideMenu addViewController:vc_Notification withTitle:@"Dining\n Cart" andImage:[UIImage imageNamed:@"img_account@2x.png"]];
        
        JWSlideMenuViewController *vc_Promo=[ChefProfileVC new];
        [slideMenu addViewController:vc_Promo withTitle:@"Favorites" andImage:[UIImage imageNamed:@"icon-fav.png"]];
        
        JWSlideMenuViewController *vc_Promo1=[NotificationsVC new];
        [slideMenu addViewController:vc_Promo1 withTitle:@"Notification" andImage:[UIImage imageNamed:@"img_notification@2x.png"]];
        JWSlideMenuViewController *vc_Promo2=[MessageListVC new];
        
        [slideMenu addViewController:vc_Promo2 withTitle:@"Messages" andImage:[UIImage imageNamed:@"img_msg@2x.png"]];
        JWSlideMenuViewController *vc_Promo3=[SwitchtoUser new];
        [slideMenu addViewController:vc_Promo3 withTitle:@"Switch to\n Chef" andImage:[UIImage imageNamed:@"img_swtich@2x.png"]];
        
        [self presentViewController:slideMenu animated:NO completion:nil];
    }
    
}
-(void) AFForgetPass
{
    
    [btn_Cancel setUserInteractionEnabled:NO];
    [btn_Submitt setUserInteractionEnabled:NO];
    
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    NSDictionary *params =@{
                            @"email"                  :   txt_Emailid.text,
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kForgetPassword
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseForgetPass:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully login");
                                             [self AFForgetPass];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseForgetPass :(NSDictionary * )TheDict
{
    NSLog(@"the dict %@",TheDict);
    
    [btn_Cancel setUserInteractionEnabled:YES];
    [btn_Submitt setUserInteractionEnabled:YES];
    
    
    if([[TheDict valueForKey:@"error"] intValue] == 0)
    {
        [self resignKeyboard];
        
        [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
    else{
        
        
        txt_Emailid.text = @"";
        [self resignKeyboard];
        [view_Forgetpassword removeFromSuperview];
        
        
        [self popup_Alertview:[TheDict valueForKey:@"Invalid username or password"]];
        
    }
    
    
}









//#pragma mark integrateJWSlider
//
//-(void)integrateJWSlider
//{
//
//    JWSlideMenuController *slideMenu = [[JWSlideMenuController alloc] init];
//
//
//    JWSlideMenuViewController  *vc_Home =  [[HomeViewController1 alloc]init];
//    [slideMenu addViewController:vc_Home withTitle:@"Home" andImage:[UIImage imageNamed:nil]];
//
//    JWSlideMenuViewController  *vc_Vehical = [[FoodNowVC alloc] init];
//    [slideMenu addViewController:vc_Vehical  withTitle:@"Food Now" andImage:[UIImage imageNamed:nil] ];
//
//    JWSlideMenuViewController  *vc_mycardairy  =  [[MyOrderVC alloc]init];
//    [slideMenu addViewController:vc_mycardairy withTitle:@"My Order" andImage:[UIImage imageNamed:nil]];
//
//
//    JWSlideMenuViewController  *vc_servicedirectory = [[DinningCartVC alloc] init];
//    [slideMenu addViewController:vc_servicedirectory  withTitle:@"Dinning Cart" andImage:[UIImage imageNamed:nil] ];
//
//    JWSlideMenuViewController  *vc_Artical =  [[FavoritesVC alloc]init];
//    [slideMenu addViewController:vc_Artical withTitle:@"Favorite" andImage:[UIImage imageNamed:nil]];
//
//
//
//    JWSlideMenuViewController  *vc_History = [[MessagesVC alloc] init];
//    [slideMenu addViewController:vc_History  withTitle:@"Messages" andImage:[UIImage imageNamed:nil] ];
//    
//    
//    [self presentViewController:slideMenu animated:YES completion:nil];
//}
//


@end
