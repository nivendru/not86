//
//  AppDelegate.m
//  Not86
//
//  Created by Interwld on 7/29/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "AppDelegate.h"
#import "SplashScreensViewController.h"
#import "ChefSignup6.h"
#import "ChefSignup.h"
#import "ChefHomeViewController.h"
#import "ChefHomeViewSecondController.h"
#import "MenuScreenVC.h"
#import "HomeViewController1.h"
#import "SignupViewController.h"
#import "ChefServeNowVC.h"
#import "MenuScreenVC.h"
#import "MyScheduleScreenVC.h"
#import "LoginViewController.h"
#import "SignupViewController.h"
#import "MyScheduleScreenVC.h"
#import "ChefSignup2.h"
#import "Chefsignup3.h"
#import "ChefMyProfileVC.h"
#import "ChefProfileVC.h"
#import "ChefSignup4.h"
#import "ChefSignup5.h"
#import "Chefsignup3.h"


#import "ChefProfileVC.h"
#import "ChefSignup.h"

#import "ChefSignup5.h"
#import "ChefSignup6.h"
#import "UserProfileVC.h"
#import "HomeVC.h"
#import "UserNotificationsVC.h"
#import "CheckOutVC.h"
#import "ChefReviwesVC.h"
#import "UserSupportVC.h"
#import "AddressDetailsVC.h"

#import "DiningCartVC.h"
#import "MoreFiltersInFoodForNowVC.h"
#import "LocationVC.h"
#import "UserMessagesVC.h"
#import "UserSearchVC.h"
#import "ViewDishVC.h"
#import "UserDishRivewsVC.h"
#import "ItemDetailVC.h"
#import "AddToCartFoodNowVC.h"
#import "NotificationsVC.h"
#import "UserNotificationsVC.h"
#import "ChefSettingsVC.h"
#import "MyProfileVC.h"
#import "UserEditProfileVC.h"
#import "MyMenuVC.h"
#import "OrderDetailsInProgressVC.h"



#import "ChefChatingVC.h"
#import "ChefSupportsVC.h"  
#import "ChefEditProfileVC.h"


#import "ChefRegistration6VC.h"
#import "ChefHomeThirdViewController.h"
#import "CKCalendarView.h"
#import "MenuScreenEditDishVC.h"
#import "MenuScreenAddDishVC.h"
#import "MenuScreenEditMealVC.h"


#import "OrderDetailsVC.h"


//USER CLASSES

#import "UserLogonVC.h"
#import "UserSignUpVC.h"
#import "UserRegistration1VC.h"
#import "UserRegistration2VC.h"
#import "UserLoginVC.h"
#import "HomeVC.h"
#import "SerchForFoodNowVC.h"
#import "MyordersVC.h"
#import "EditeUserProfileVC.h"




#import "WriteReviewVC.h"
#import "UserInfoVC.h"
#import "NotificationSettingsVC.h"
#import "MessageListVC.h"
#import "UserChatingVC.h"
#import "ViewMenuVC.h"
#import "DiningCartVC.h"

#import "ChefMessageListVC.h"
#import "ChefNotificationsVC.h"
#import "AccountVC.h"
#import "UserSupportVC.h"
#import "ChefSupportsVC.h"
#import "ProceedToPaymentVC.h"
#import "PersonalDetailsVC.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import <FacebookSDK/FacebookSDK.h> // Gaurav
#import "DineInCheckOutVC.h"
#import "ViewDishDetailsVC.h"
@interface AppDelegate ()<GPPDeepLinkDelegate>
{
 
    
}


@end

@implementation AppDelegate

@synthesize isFBLogin,activityIndicator;
@synthesize navigationC;
@synthesize devicestr;
@synthesize str_Rotation,login_type,locationManager,mUserCurrentLocation,myGeoCoder,ary_Country,str_Location1;

NSString *str_appState;
UIAlertView *alert_view;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
     [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
//    ChefEditProfileVC * SplashScreen = [ChefEditProfileVC new];
//        navigationC = [[UINavigationController alloc]
//                       initWithRootViewController:SplashScreen]; ChefHomeViewController
     SplashScreensViewController * SplashScreen = [SplashScreensViewController new];
            navigationC = [[UINavigationController alloc]
                           initWithRootViewController:SplashScreen];
    
    
  //  UserRegistration1VC * SplashScreen = [UserRegistration1VC new];
  //  navigationC = [[UINavigationController alloc]
 //                  initWithRootViewController:SplashScreen];
    navigationC.navigationBarHidden = YES;
    self.window.rootViewController=navigationC;
  
    self.window.backgroundColor = [UIColor whiteColor];
//    SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:UINavigationController frontViewController:frontNavigationController];
//    revealController.delegate = self;
    if (IS_OS_8_OR_LATER)
    {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
        
        
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert |UIRemoteNotificationTypeBadge |UIRemoteNotificationTypeSound |
         UIRemoteNotificationTypeNewsstandContentAvailability)];
        
        
    }
       //*************** For the Activity Indicator *******************
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    
    
    if (IS_IPHONE_6Plus)
    {
        self.activityIndicator.frame  = CGRectMake(386/2-30,730/2-50, 100, 100);

    }

    else if (IS_IPHONE_6)
    {
        self.activityIndicator.frame  = CGRectMake(375/2-50,667/2-50, 100, 100);

    }
    else if (IS_IPHONE_5)
    {
        self.activityIndicator.frame  = CGRectMake(320/2-50,568/2-50, 100, 100);

    }
    else
    {
        self.activityIndicator.frame  = CGRectMake(320/2-50,480/2-50, 100, 100);

    }
    self.activityIndicator.backgroundColor = [UIColor clearColor];
    [self.activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.window addSubview:self.activityIndicator];
    
    
    
    
    
    [self.window makeKeyAndVisible];
    
    
    
    //  For Starting the application
    
       return YES;
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    if ([self.login_type isEqualToString:@"google"]) {
        
        
        return [GPPURLHandler handleURL:url
                      sourceApplication:sourceApplication
                             annotation:annotation];
        
    }
    return 0;
    /*else{
        [FBAppCall handleOpenURL:url
                      sourceApplication:sourceApplication
                        fallbackHandler:^(FBAppCall *call) {
                            NSLog(@"In fallback handler");
                        }];
        
    }*/

}
//
#pragma mark - GPPDeepLinkDelegate

- (void)didReceiveDeepLink:(GPPDeepLink *)deepLink {
    // An example to handle the deep link data.
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Deep-link Data"
                          message:[deepLink deepLinkID]
                          delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
}


#pragma mark - GPPDeepLinkDelegate

-(void)clearApplicationCaches
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    [self clearApplicationCaches];
}





#pragma mark ------------------------- PUSH NOTIFICATION-----------------------START

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *str = [NSString stringWithFormat:@"%@",deviceToken];
    self.devicestr=	str;
    self.devicestr=[self.devicestr stringByReplacingOccurrencesOfString:@"<" withString:@""];
    self.devicestr=[self.devicestr stringByReplacingOccurrencesOfString:@">" withString:@""];
    
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    
    NSLog(@"notifications  %@",userInfo);
    NSString *text= [[userInfo valueForKey:@"aps"] valueForKey:@"alert"] ;
    NSString *type= [[userInfo valueForKey:@"aps"] valueForKey:@"type"] ;
    NSString *badgeno= [[userInfo valueForKey:@"aps"] valueForKey:@"badge"] ;
    int bad_no=[badgeno intValue];
    
    //here i am increasing cdhat count and notification count
    
    
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateNotification" object:userInfo userInfo:nil];
    
    //-----------------------APNS HANDLE----------------
    
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive)
    {
        application.applicationIconBadgeNumber=0;
        //        NSString *decoded = [text stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //        UIAlertView *alert_view = [[UIAlertView alloc] initWithTitle:@"Metta" message:decoded delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //
        //        [alert_view show];
        NSString *decoded = [NSString stringWithFormat:@"%@",[[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        alert_view = [[UIAlertView alloc] initWithTitle:@"Metta" message:decoded delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert_view show];
        
    }
    //=====================================================
    
    else if((state==UIApplicationStateBackground) || (state==UIApplicationStateInactive))
    {
        str_appState=@"inactive";
        
        // here i am increasing the badges
        application.applicationIconBadgeNumber=bad_no;
        NSString *decoded = [NSString stringWithFormat:@"%@",[[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        alert_view = [[UIAlertView alloc] initWithTitle:@"Metta" message:decoded delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert_view show];
    }
    

    
    
        
    
}
-(void)callME
{

}
#pragma mark UsersCurrentLocation

- (void)fetchUsersCurrentLocation
{
    
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
        // Use one or the other, not both. Depending on what you put in info.plist
        [locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    [locationManager startUpdatingLocation];
    
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    [locationManager startUpdatingLocation];
    
    mUserCurrentLocation = [[CLLocation alloc]init];
    mUserCurrentLocation = [self.locationManager location];
    
    
    CLLocationCoordinate2D coordinate;
    coordinate.longitude = locationManager.location.coordinate.longitude;
    coordinate.latitude = locationManager.location.coordinate.latitude;
    
    NSString *str_lon =[NSString stringWithFormat:@"%f",coordinate.longitude];
    NSString *str_lat =[NSString stringWithFormat:@"%f",coordinate.latitude];
    
    NSLog(@"str_lat:%@ str_lon:%@",str_lat,str_lon);
    
    
    [self updateCurrentLocation];
    
}



- (void)updateCurrentLocation
{
    self.locationManager = [[CLLocationManager alloc] init];
    
    
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
        // Use one or the other, not both. Depending on what you put in info.plist
        //[self.locationManager  requestWhenInUseAuthorization];
        [self.locationManager  requestAlwaysAuthorization];
    }
#endif
    
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    //    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
    //        [self.locationManager requestWhenInUseAuthorization];
    //    }
    
    [self.locationManager startUpdatingLocation];
    
    mUserCurrentLocation = [[CLLocation alloc]init];
    mUserCurrentLocation = [self.locationManager location];
    
    [self getCurrentLocation];
}

-(CLLocation *) getCurrentLocation
{
    return mUserCurrentLocation;
}

- (NSString *)deviceLocation
{
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude];
}
- (NSString *)deviceLat
{
    return [NSString stringWithFormat:@"%f", locationManager.location.coordinate.latitude];
}
- (NSString *)deviceLon {
    return [NSString stringWithFormat:@"%f", locationManager.location.coordinate.longitude];
}
- (NSString *)deviceAlt {
    return [NSString stringWithFormat:@"%f",locationManager.location.altitude];
}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
    printf("\nerror");
    //    UIAlertView *alert = [ [UIAlertView alloc] initWithTitle:@"Error"
    //                                                     message:@"Error while getting your current location."
    //                                                    delegate:self
    //                                           cancelButtonTitle:@"OK"
    //                                           otherButtonTitles:nil ];
    //
    //    [alert show];
}

#pragma mark - LocationManager Delegate Methods

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations objectAtIndex:0];
    NSLog(@"lat%f - lon%f", location.coordinate.latitude, location.coordinate.longitude);
    
    
//    str_lat = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
//    str_long = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    //   [self ShowImportLocation];
    
    [locationManager stopUpdatingLocation];
    
    if (!self.myGeoCoder)
    {
        self.myGeoCoder = [[CLGeocoder alloc] init];
    }
    [self.myGeoCoder reverseGeocodeLocation: locationManager.location completionHandler:
     
     ^(NSArray *placemarks, NSError *error)
     {
         //Get nearby address
         CLPlacemark *placemark=[placemarks objectAtIndex:0];
         //String to hold address
         NSString *locatedAt=[[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
         NSLog(@"located at %@",locatedAt);
         NSString *cityName=@"";
         cityName=[placemark.addressDictionary valueForKey:@"City"];
         NSLog(@"City name is %@",cityName);
         
         ary_Country = [[NSMutableArray alloc] init];
         NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
         [dict setValue:locatedAt forKey:@"Location"];
         
         [ary_Country addObject:dict];
         NSLog(@"ary_Location :: %@",ary_Country);
         NSLog(@"locatedAt :: %@",locatedAt);
         //Print the location to console
         delegate.str_Location1=[NSMutableString stringWithFormat:@"%@",locatedAt];
         NSLog(@" delegate.str_Location1 :: %@", delegate.str_Location1);
         
         
     }];
    
    
}

// this delegate is called when the app successfully finds your current location
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    // this creates a MKReverseGeocoder to find a placemark using the found coordinates
    MKReverseGeocoder *geoCoder = [[MKReverseGeocoder alloc] initWithCoordinate:newLocation.coordinate];
    geoCoder.delegate = self;
    [geoCoder start];
}

// this delegate method is called if an error occurs in locating your current location


// this delegate is called when the reverseGeocoder finds a placemark
- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark
{
    MKPlacemark * myPlacemark = placemark;
    // with the placemark you can now retrieve the city name
    //    NSString *city = [myPlacemark.addressDictionary objectForKey:(NSString*) kABPersonAddressCityKey];
}

// this delegate is called when the reversegeocoder fails to find a placemark
#pragma mark UsersCurrentLocation end


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
