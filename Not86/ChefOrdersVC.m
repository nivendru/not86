//
//  ChefOrdersVC.m
//  Not86
//
//  Created by Admin on 11/09/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ChefOrdersVC.h"
#import "JWSlideMenuController.h"
#import "JWNavigationController.h"
#import "JWSlideMenuViewController.h"
#import "AppDelegate.h"
#import "ChefChatingVC.h"
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"




@interface ChefOrdersVC ()<UIScrollViewDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegate>
{
    CGFloat	animatedDistance;
    UIImageView *  img_header;
    UIScrollView * scroll;
    UIView * view_for_todays;
    
    UIView * view_for_on_request;
    UITableView * tabl_for_on_request_orders;
    
    UIView * view_for_past_request;
    UITableView * tabl_for_past_orders;
    NSMutableArray * array_order_date_and_time;
    NSMutableArray * array_serving_date_and_time;
    NSMutableArray * array_delivery_state;
    
    
    
    
    UIView * view_for_tomorrow;
    UITableView * tabl_for_tomarrow;
    NSMutableArray * array_due_in_tomarrow;
    
    
    UIView * view_for_active;
    UITableView * tabl_for_active;
    
    UIView * view_for_cancelled;
    UITableView * tabl_for_cancelled;
    NSMutableArray * array_schedule_date_time;
    NSMutableArray * array_cancelled_date_time;
    NSMutableArray * array_cancellled_by;
    NSMutableArray * array_refund;
    NSMutableArray * array_refund_amount;
    NSMutableArray * array_cancelled_time;
    
    
    
    
    
    UIView * view_for_served;
    UITableView * tabl_for_served;
    
    
    
    UITableView *  tabl_for_today_orders;
    UILabel * lbl_today;
    UIImageView * img_bg_for_first_tbl;
    UILabel * lbl_todays;
    NSMutableArray *  array_head_names;
    UICollectionViewFlowLayout *  layout;
    UICollectionView * collView_alert;
    NSMutableArray * array_labl_serving_time;
    
    
    
    
    UITableView * tabl_for_orders;
    
    NSMutableArray * array_order_no;
    NSMutableArray * array_value;
    NSMutableArray * array_due_time;
    NSMutableArray * array_recived_date;
    NSMutableArray * array_seving_type;
    NSMutableArray * array_user_img;
    NSMutableArray * array_user_name;
    
    UILabel *lbl_quantity_available;
    UIImageView *img_line_under_quantity;
    UIButton *btn_on_quantity_available;
    
    //POPUP
    UIView * view_for_popup;
    
    
    NSMutableArray*ary_dishList;
    NSMutableArray*ary_qualityalert;
    AppDelegate*delegate;
    UIView*alertviewBg;
    NSString*str_sortby;
    NSString*str_day;
    
    UIButton *icon_menu ;
    UITextField *txt_search;
    int search_leg;
    NSMutableArray*ary_dishListfilter;
    
    
    
}

@end

@implementation ChefOrdersVC
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    // Do any additional setup after loading the view.
    
    
    str_sortby  = [NSString new];
    str_day = [NSString new];
    ary_dishList = [NSMutableArray new];
    ary_dishListfilter = [NSMutableArray new];
    
    ary_qualityalert =[NSMutableArray new];
    
    str_day= @"0";
    str_sortby = @"0";
    
    
    [self integrateHeader];
    [self integrateBodyDesign];
    [self popup_cancel_oeder];
    
    array_head_names = [[NSMutableArray alloc]initWithObjects:@"Today",@"On Request",@"Past Request",@"Tomorrow",@"Active",@"Cancelled",@"Served",nil];
    array_labl_serving_time = [[NSMutableArray alloc]initWithObjects:@"Serving Now",@"Serving Now",@"Serving Now",@"Serving Now", nil];
    //array for order numbers
    array_order_no = [[NSMutableArray alloc]initWithObjects: @"10847",@"10848",@"10849",@"10850",nil];
    array_value = [[NSMutableArray alloc]initWithObjects:@"$50.80",@"$50.80",@"$50.80",@"$50.80", nil];
    array_recived_date = [[NSMutableArray alloc]initWithObjects:@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM", nil];
    array_due_time = [[NSMutableArray alloc]initWithObjects:@"20 mins",@"30 mins",@"20 mins",@"30 mins", nil];
    array_seving_type = [[NSMutableArray alloc]initWithObjects:@"take-icon@2x.png",@"deliver-icon@2x.png",@"img-dinin@2x.png",@"img-dinin@2x.png", nil];
    array_user_img = [[NSMutableArray alloc]initWithObjects:@"img-user@2x.png",@"img-user@2x.png",@"img-user@2x.png",@"img-user@2x.png", nil];
    array_user_name = [[NSMutableArray alloc]initWithObjects:@"Jane Doe",@"Jane Doe", @"Jane Doe",@"Jane Doe",nil];
    
    //array forpast request
    array_order_date_and_time = [[NSMutableArray alloc]initWithObjects:@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM", nil];
    array_serving_date_and_time = [[NSMutableArray alloc]initWithObjects:@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM", nil];
    array_delivery_state = [[NSMutableArray alloc]initWithObjects:@"Accepted",@"Accepted",@"Accepted",@"Accepted",nil];
    
    //array for tomarrow
    array_due_in_tomarrow = [[NSMutableArray alloc]initWithObjects:@"1 day",@"1 day",@"2 day",@"2 day", nil];
    //array for cancelled
    
    array_schedule_date_time = [[NSMutableArray alloc]initWithObjects:@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",nil];
    array_cancelled_date_time = [[NSMutableArray alloc]initWithObjects:@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM", nil];
    array_cancellled_by = [[NSMutableArray alloc]initWithObjects:@"Chef",@"Diner",@"Chef",@"Diner",nil];
    array_refund = [[NSMutableArray alloc]initWithObjects:@"Yes",@"No",@"Yes",@"No", nil];
    array_refund_amount = [[NSMutableArray alloc]initWithObjects:@"$14.90",@"$13.30",@"$14.30",@"$12.30", nil];
    array_cancelled_time = [[NSMutableArray alloc]initWithObjects:@"5 mins ago",@"20 mins ago",@"15 mins ago",@"30 mins ago", nil];
}


-(BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    [self.navigationController.slideMenuController  UnhideHomeButton];
    
    str_day= @"0";
    str_sortby = @"0";
    
    
    
    if ([_str_comefrom isEqualToString:@"HOME"])
    {
        [self.navigationController.slideMenuController HideHomeButton];
        icon_menu.hidden = NO;
        
    }
    else{
        [self.navigationController.slideMenuController UnhideHomeButton];
        icon_menu.hidden = YES;
        
        
    }
    
    [self AFUserDishLists];
    
}

-(void)integrateHeader
{
    
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [ self.view addSubview:img_header];
    
    icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu .frame = CGRectMake(10, 13,25,20);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu  addTarget:self action:@selector(click_on_Back_Btn:)forControlEvents:UIControlEventTouchUpInside];
    [icon_menu setImage:[UIImage imageNamed:@"img_arrow@2x"] forState:UIControlStateNormal];
    //    icon_menu.layer.borderWidth = 1.0;
    [icon_menu setBackgroundColor:[UIColor clearColor]];
    [img_header   addSubview:icon_menu ];
    
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init ];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 150, 45);
    lbl_User_Sign_Up.text = @"Orders";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    UIImageView *icon_user = [[UIImageView alloc]init ];
    icon_user .frame = CGRectMake(WIDTH-40, 8, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"img_logo@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}

-(void)click_on_Back_Btn: (UIButton *)sender

{
    NSLog(@"click_on_Back_Btn Click");
    [self dismissViewControllerAnimated:NO completion:nil];
}
-(void)integrateBodyDesign
{
    
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    scroll.frame = CGRectMake(0, 45, WIDTH, HEIGHT);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    UIImageView *bg_for_sech_bar = [[UIImageView alloc]init];
    bg_for_sech_bar.frame = CGRectMake(5,5, WIDTH-10, 50);
    [bg_for_sech_bar setUserInteractionEnabled:YES];
    bg_for_sech_bar.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [scroll addSubview:bg_for_sech_bar];
    
    
    UIImageView *img_serch_bar = [[UIImageView alloc]init];
    img_serch_bar.frame = CGRectMake(10,10, WIDTH-30, 30);
    [img_serch_bar setImage:[UIImage imageNamed:@"search-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_serch_bar setUserInteractionEnabled:YES];
    [bg_for_sech_bar addSubview:img_serch_bar];
    
    
    txt_search = [[UITextField alloc] init];
    txt_search.frame = CGRectMake(10,-8, 300, 45);
    txt_search .borderStyle = UITextBorderStyleNone;
    txt_search .textColor = [UIColor grayColor];
    txt_search .font = [UIFont fontWithName:kFont size:15];
    txt_search .placeholder = @"Search...";
    [txt_search  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_search  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_search .leftView = padding3;
    txt_search .leftViewMode = UITextFieldViewModeAlways;
    txt_search .userInteractionEnabled=YES;
    txt_search .textAlignment = NSTextAlignmentLeft;
    txt_search .backgroundColor = [UIColor clearColor];
    txt_search .keyboardType = UIKeyboardTypeAlphabet;
    txt_search .delegate = self;
    [img_serch_bar addSubview:txt_search ];
    
    
    UIImageView *icon_search = [[UIImageView alloc]init];
    icon_search.frame = CGRectMake(310,8, 15, 15);
    [icon_search setImage:[UIImage imageNamed:@"icon-search@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_search setUserInteractionEnabled:YES];
    [img_serch_bar addSubview:icon_search];
    
    UIButton *btn_on_search_bar = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_search_bar.frame = CGRectMake(310,0, 30, 30);
    btn_on_search_bar .userInteractionEnabled=YES;
    btn_on_search_bar .backgroundColor = [UIColor clearColor];
    [btn_on_search_bar addTarget:self action:@selector(btn_on_search_bar_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [img_serch_bar   addSubview:btn_on_search_bar];
    
    UIImageView *bg_for_todays_time = [[UIImageView alloc]init];
    bg_for_todays_time.frame = CGRectMake(5,CGRectGetMaxY(bg_for_sech_bar.frame)+5, WIDTH-10, 50);
    [bg_for_todays_time setUserInteractionEnabled:YES];
    bg_for_todays_time.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [scroll addSubview:bg_for_todays_time];
    
    UIImageView *img_up_down_arrow = [[UIImageView alloc]init];
    img_up_down_arrow .frame = CGRectMake(40,13, 25, 25);
    [img_up_down_arrow  setImage:[UIImage imageNamed:@"img-up-down-arrow@2x.png"]];
    img_up_down_arrow .backgroundColor = [UIColor clearColor];
    [img_up_down_arrow  setUserInteractionEnabled:YES];
    [bg_for_todays_time addSubview:img_up_down_arrow ];
    
    
    UILabel *lbl_serving_type = [[UILabel alloc]init ];
    lbl_serving_type.frame = CGRectMake(CGRectGetMaxX(img_up_down_arrow.frame)+10,5, 100, 45);
    lbl_serving_type.text = @"Time";
    lbl_serving_type.font = [UIFont fontWithName:kFont size:15];
    lbl_serving_type.textColor = [UIColor blackColor];
    lbl_serving_type.backgroundColor = [UIColor clearColor];
    [bg_for_todays_time addSubview:lbl_serving_type];
    
    UIButton *btn_on_up_down_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_up_down_arrow.frame = CGRectMake(0,0,175,50);
    btn_on_up_down_arrow .userInteractionEnabled=YES;
    btn_on_up_down_arrow .backgroundColor = [UIColor clearColor];
    [btn_on_up_down_arrow addTarget:self action:@selector(click_on_btn_up_down_arrow:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [bg_for_todays_time   addSubview:btn_on_up_down_arrow];
    
    
    UIImageView *img_small_line = [[UIImageView alloc]init];
    img_small_line .frame = CGRectMake(CGRectGetMaxX(lbl_serving_type.frame),10,2,35);
    [img_small_line  setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
    img_small_line .backgroundColor = [UIColor clearColor];
    [img_small_line  setUserInteractionEnabled:YES];
    [bg_for_todays_time addSubview:img_small_line];
    
    lbl_today = [[UILabel alloc]init ];
    lbl_today.frame = CGRectMake(CGRectGetMaxX(img_small_line.frame)+40,5, 100, 45);
    lbl_today.text = @"Today";
    lbl_today.font = [UIFont fontWithName:kFont size:15];
    lbl_today.textColor = [UIColor blackColor];
    lbl_today.backgroundColor = [UIColor clearColor];
    [bg_for_todays_time addSubview:lbl_today];
    
    UIButton *icon_dropdown =[UIButton buttonWithType:UIButtonTypeCustom];
    icon_dropdown.frame=CGRectMake(CGRectGetMaxX(lbl_today.frame),25, 18, 10);
    icon_dropdown.backgroundColor = [UIColor clearColor];
    [icon_dropdown addTarget:self action:@selector(click_on_drop_down_btn:) forControlEvents:UIControlEventTouchUpInside];
    icon_dropdown.userInteractionEnabled = YES;
    [icon_dropdown setImage:[UIImage imageNamed:@"dropdown@2x.png"] forState:UIControlStateNormal];
    [ bg_for_todays_time   addSubview:icon_dropdown];
    
    UIButton *btn_on_drop_down = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_drop_down.frame = CGRectMake(CGRectGetMaxX(img_small_line.frame),0, 185, 50);
    btn_on_drop_down .userInteractionEnabled=YES;
    btn_on_drop_down .backgroundColor = [UIColor clearColor];
    [btn_on_drop_down addTarget:self action:@selector(click_on_drop_down_btn:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [bg_for_todays_time   addSubview:btn_on_drop_down];
    
    
    lbl_quantity_available = [[UILabel alloc]init ];
    lbl_quantity_available.frame = CGRectMake(180,CGRectGetMaxY(bg_for_todays_time.frame), 300, 45);
    lbl_quantity_available.text = @"View Quantity Available";
    lbl_quantity_available.font = [UIFont fontWithName:kFont size:15];
    lbl_quantity_available.textColor = [UIColor blackColor];
    lbl_quantity_available.backgroundColor = [UIColor clearColor];
    [scroll addSubview:lbl_quantity_available];
    
    img_line_under_quantity = [[UIImageView alloc]init];
    img_line_under_quantity .frame = CGRectMake(180,CGRectGetMaxY(lbl_quantity_available.frame)-10,175,1);
    [img_line_under_quantity  setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
    img_line_under_quantity .backgroundColor = [UIColor clearColor];
    [img_line_under_quantity  setUserInteractionEnabled:YES];
    [scroll addSubview:img_line_under_quantity];
    
    btn_on_quantity_available = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_quantity_available.frame = CGRectMake(180,CGRectGetMaxY(bg_for_todays_time.frame), 190, 40);
    btn_on_quantity_available .userInteractionEnabled=YES;
    btn_on_quantity_available .backgroundColor = [UIColor clearColor];
    [btn_on_quantity_available addTarget:self action:@selector(click_on_quantity_available_btn:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [scroll   addSubview:btn_on_quantity_available];
    
    view_for_todays = [[UIView alloc]init];
    view_for_todays.frame=CGRectMake(5,CGRectGetMaxY(lbl_quantity_available.frame),WIDTH-10,600);
    [view_for_todays setUserInteractionEnabled:YES];
    view_for_todays.backgroundColor=[UIColor clearColor];
    view_for_todays.hidden = NO;
    [scroll addSubview: view_for_todays];
    
    
    
    UIImageView *img_bg_for_quantity_alert = [[UIImageView alloc]init];
    img_bg_for_quantity_alert .frame = CGRectMake(0,10,WIDTH-7,180);
    [img_bg_for_quantity_alert  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    img_bg_for_quantity_alert .backgroundColor = [UIColor clearColor];
    [img_bg_for_quantity_alert  setUserInteractionEnabled:YES];
    [view_for_todays addSubview:img_bg_for_quantity_alert];
    
    UILabel *lbl_quantity_alert = [[UILabel alloc]init ];
    lbl_quantity_alert.frame = CGRectMake(110,02, 300, 45);
    lbl_quantity_alert.text = @"Quantity Alert";
    lbl_quantity_alert.font = [UIFont fontWithName:kFontBold size:17];
    lbl_quantity_alert.textColor = [UIColor blackColor];
    lbl_quantity_alert.backgroundColor = [UIColor clearColor];
    [img_bg_for_quantity_alert addSubview:lbl_quantity_alert];
    
    UIButton *icon_cross =[UIButton buttonWithType:UIButtonTypeCustom];
    icon_cross.frame=CGRectMake(CGRectGetMidX(lbl_quantity_alert.frame)+65,17, 15,15);
    icon_cross.backgroundColor = [UIColor clearColor];
    [icon_cross addTarget:self action:@selector(click_on_x_btn:) forControlEvents:UIControlEventTouchUpInside];
    icon_cross.userInteractionEnabled = YES;
    [icon_cross setImage:[UIImage imageNamed:@"cross-img@2x.png"] forState:UIControlStateNormal];
    [img_bg_for_quantity_alert  addSubview:icon_cross];
    
    UIButton *btn_on_cross = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_cross.frame = CGRectMake(CGRectGetMidX(lbl_quantity_alert.frame)+65,12, 30,30);
    btn_on_cross .userInteractionEnabled=YES;
    btn_on_cross .backgroundColor = [UIColor clearColor];
    [btn_on_cross addTarget:self action:@selector(click_on_cross_btn:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [img_bg_for_quantity_alert   addSubview:btn_on_cross];
    
    UIImageView *img_line2 = [[UIImageView alloc]init];
    img_line2 .frame = CGRectMake(20,CGRectGetMaxY(lbl_quantity_alert.frame)-5,WIDTH-50,0.5);
    [img_line2  setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
    img_line2 .backgroundColor = [UIColor clearColor];
    [img_line2  setUserInteractionEnabled:YES];
    [img_bg_for_quantity_alert addSubview:img_line2];
    
    
    
    layout = [[UICollectionViewFlowLayout alloc] init];
    collView_alert = [[UICollectionView alloc] initWithFrame:CGRectMake(25,CGRectGetMaxY(img_line2.frame)+5,WIDTH-63,100)
                                        collectionViewLayout:layout];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_alert setDataSource:self];
    [collView_alert setDelegate:self];
    collView_alert.scrollEnabled = YES;
    collView_alert.showsVerticalScrollIndicator = NO;
    collView_alert.showsHorizontalScrollIndicator = NO;
    collView_alert.pagingEnabled = YES;
    [collView_alert registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_alert setBackgroundColor:[UIColor clearColor]];
    layout.minimumInteritemSpacing = 12;
    layout.minimumLineSpacing = 0;
    collView_alert.userInteractionEnabled = YES;
    [img_bg_for_quantity_alert addSubview: collView_alert];
    
    UIButton *  btn_on_quantity_alert = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_quantity_alert.frame = CGRectMake(0,0, 190, 180);
    btn_on_quantity_alert .userInteractionEnabled=YES;
    btn_on_quantity_alert .backgroundColor = [UIColor clearColor];
    [btn_on_quantity_alert addTarget:self action:@selector(click_on_quantity_alert_btn:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [img_bg_for_quantity_alert   addSubview:btn_on_quantity_alert];
    
    
#pragma mark Tableview for served orders
    
    tabl_for_orders = [[UITableView alloc] init ];
    tabl_for_orders.frame  = CGRectMake(2,CGRectGetMaxY(img_bg_for_quantity_alert.frame),WIDTH-15,275);
    [tabl_for_orders setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tabl_for_orders.delegate = self;
    tabl_for_orders.dataSource = self;
    tabl_for_orders.showsVerticalScrollIndicator = NO;
    tabl_for_orders.backgroundColor = [UIColor clearColor];
    [view_for_todays addSubview:tabl_for_orders];
    
#pragma view for on request
    
    view_for_on_request = [[UIView alloc]init];
    view_for_on_request.frame=CGRectMake(5,CGRectGetMaxY(bg_for_todays_time.frame),WIDTH-10,600);
    [view_for_on_request setUserInteractionEnabled:YES];
    view_for_on_request.backgroundColor=[UIColor clearColor];
    view_for_on_request.hidden = YES;
    [scroll addSubview: view_for_on_request];
    
    UILabel *lbl_orders_accept = [[UILabel alloc]init ];
    lbl_orders_accept.frame = CGRectMake(90,02, 300, 45);
    lbl_orders_accept.text = @"% of Orders Accepted";
    lbl_orders_accept.font = [UIFont fontWithName:kFontBold size:15];
    lbl_orders_accept.textColor = [UIColor blackColor];
    lbl_orders_accept.backgroundColor = [UIColor clearColor];
    [view_for_on_request addSubview:lbl_orders_accept];
    
    UIImageView *img_green_percentage = [[UIImageView alloc]init];
    img_green_percentage .frame = CGRectMake(20,CGRectGetMaxY(lbl_orders_accept.frame),260,15);
    [img_green_percentage  setImage:[UIImage imageNamed:@"green-percentage@2x.png"]];
    img_green_percentage .backgroundColor = [UIColor clearColor];
    [img_green_percentage  setUserInteractionEnabled:YES];
    [view_for_on_request addSubview:img_green_percentage];
    
    UILabel *lbl_percentage_val = [[UILabel alloc]init ];
    lbl_percentage_val.frame = CGRectMake(CGRectGetMaxX(img_green_percentage.frame)+10,CGRectGetMaxY(lbl_orders_accept.frame)-15, 300, 45);
    lbl_percentage_val.text = @"56/100%";
    lbl_percentage_val.font = [UIFont fontWithName:kFont size:12];
    lbl_percentage_val.textColor = [UIColor blackColor];
    lbl_percentage_val.backgroundColor = [UIColor clearColor];
    [view_for_on_request addSubview:lbl_percentage_val];
    
    UILabel *lbl_avg_respond_time = [[UILabel alloc]init ];
    lbl_avg_respond_time.frame = CGRectMake(20,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
    lbl_avg_respond_time.text = @"Average Response Time:";
    lbl_avg_respond_time.font = [UIFont fontWithName:kFont size:15];
    lbl_avg_respond_time.textColor = [UIColor blackColor];
    lbl_avg_respond_time.backgroundColor = [UIColor clearColor];
    [view_for_on_request addSubview:lbl_avg_respond_time];
    
    UILabel *avg_respond_time_val = [[UILabel alloc]init ];
    avg_respond_time_val.frame = CGRectMake(CGRectGetMidX(lbl_avg_respond_time.frame)+35,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
    avg_respond_time_val.text = @"45 mins";
    avg_respond_time_val.font = [UIFont fontWithName:kFontBold size:15];
    avg_respond_time_val.textColor = [UIColor blackColor];
    avg_respond_time_val.backgroundColor = [UIColor clearColor];
    [view_for_on_request addSubview:avg_respond_time_val];
    
#pragma mark Tableview for on request
    
    
    tabl_for_on_request_orders = [[UITableView alloc] init ];
    tabl_for_on_request_orders.frame  = CGRectMake(2,CGRectGetMaxY(lbl_avg_respond_time.frame),WIDTH-15,475);
    [tabl_for_on_request_orders setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tabl_for_on_request_orders.delegate = self;
    tabl_for_on_request_orders.dataSource = self;
    tabl_for_on_request_orders.showsVerticalScrollIndicator = NO;
    tabl_for_on_request_orders.backgroundColor = [UIColor clearColor];
    [view_for_on_request addSubview:tabl_for_on_request_orders];
    
    
#pragma view for past request
    
    view_for_past_request = [[UIView alloc]init];
    view_for_past_request.frame=CGRectMake(5,CGRectGetMaxY(bg_for_todays_time.frame),WIDTH-10,600);
    [view_for_past_request setUserInteractionEnabled:YES];
    view_for_past_request.backgroundColor=[UIColor clearColor];
    view_for_past_request.hidden = YES;
    [scroll addSubview: view_for_past_request];
    
#pragma mark Tableview for past request
    
    tabl_for_past_orders = [[UITableView alloc] init ];
    tabl_for_past_orders.frame  = CGRectMake(2,10,WIDTH-15,475);
    [tabl_for_past_orders setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tabl_for_past_orders.delegate = self;
    tabl_for_past_orders.dataSource = self;
    tabl_for_past_orders.showsVerticalScrollIndicator = NO;
    tabl_for_past_orders.backgroundColor = [UIColor clearColor];
    [view_for_past_request addSubview:tabl_for_past_orders];
    
    
#pragma view for tomarrow
    
    view_for_tomorrow = [[UIView alloc]init];
    view_for_tomorrow.frame=CGRectMake(5,CGRectGetMaxY(lbl_quantity_available.frame),WIDTH-10,600);
    [view_for_tomorrow setUserInteractionEnabled:YES];
    view_for_tomorrow.backgroundColor=[UIColor clearColor];
    view_for_tomorrow.hidden = YES;
    [scroll addSubview: view_for_tomorrow];
    
#pragma mark Tableview for tomarrow
    
    tabl_for_tomarrow = [[UITableView alloc] init ];
    tabl_for_tomarrow.frame  = CGRectMake(2,10,WIDTH-15,475);
    [tabl_for_tomarrow setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tabl_for_tomarrow.delegate = self;
    tabl_for_tomarrow.dataSource = self;
    tabl_for_tomarrow.showsVerticalScrollIndicator = NO;
    tabl_for_tomarrow.backgroundColor = [UIColor clearColor];
    [view_for_tomorrow addSubview:tabl_for_tomarrow];
    
    
#pragma view for active
    
    view_for_active = [[UIView alloc]init];
    view_for_active.frame=CGRectMake(5,CGRectGetMaxY(lbl_quantity_available.frame),WIDTH-10,600);
    [view_for_active setUserInteractionEnabled:YES];
    view_for_active.backgroundColor=[UIColor clearColor];
    view_for_active.hidden = YES;
    [scroll addSubview: view_for_active];
    
#pragma mark Tableview for active
    
    tabl_for_active = [[UITableView alloc] init ];
    tabl_for_active.frame  = CGRectMake(2,10,WIDTH-15,475);
    [tabl_for_active setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tabl_for_active.delegate = self;
    tabl_for_active.dataSource = self;
    tabl_for_active.showsVerticalScrollIndicator = NO;
    tabl_for_active.backgroundColor = [UIColor clearColor];
    [view_for_active addSubview:tabl_for_active];
    
    
    
#pragma view for cancelled
    
    view_for_cancelled = [[UIView alloc]init];
    view_for_cancelled.frame=CGRectMake(5,CGRectGetMaxY(bg_for_todays_time.frame),WIDTH-10,600);
    [view_for_cancelled setUserInteractionEnabled:YES];
    view_for_cancelled.backgroundColor=[UIColor clearColor];
    view_for_cancelled.hidden = YES;
    [scroll addSubview: view_for_cancelled];
    
#pragma mark Tableview for cancelled
    
    tabl_for_cancelled = [[UITableView alloc] init ];
    tabl_for_cancelled.frame  = CGRectMake(2,10,WIDTH-15,475);
    [tabl_for_cancelled setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tabl_for_cancelled.delegate = self;
    tabl_for_cancelled.dataSource = self;
    tabl_for_cancelled.showsVerticalScrollIndicator = NO;
    tabl_for_cancelled.backgroundColor = [UIColor clearColor];
    [view_for_cancelled addSubview:tabl_for_cancelled];
    
    
#pragma view for served
    
    view_for_served = [[UIView alloc]init];
    view_for_served.frame=CGRectMake(5,CGRectGetMaxY(bg_for_todays_time.frame),WIDTH-10,600);
    [view_for_served setUserInteractionEnabled:YES];
    view_for_served.backgroundColor=[UIColor clearColor];
    view_for_served.hidden = YES;
    [scroll addSubview: view_for_served];
    
#pragma mark Tableview for served
    
    tabl_for_served = [[UITableView alloc] init ];
    tabl_for_served.frame  = CGRectMake(2,10,WIDTH-15,475);
    [tabl_for_served setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tabl_for_served.delegate = self;
    tabl_for_served.dataSource = self;
    tabl_for_served.showsVerticalScrollIndicator = NO;
    tabl_for_served.backgroundColor = [UIColor clearColor];
    [view_for_served addSubview:tabl_for_served];
    
    
    
#pragma mark Tableview for orders on drop down
    
    tabl_for_today_orders = [[UITableView alloc] init ];
    tabl_for_today_orders.frame  = CGRectMake(CGRectGetMaxX(img_small_line.frame)+5,CGRectGetMaxY(bg_for_todays_time.frame),185,150);
    [tabl_for_today_orders setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tabl_for_today_orders.delegate = self;
    tabl_for_today_orders.dataSource = self;
    tabl_for_today_orders.showsVerticalScrollIndicator = NO;
    tabl_for_today_orders.backgroundColor = [UIColor clearColor];
    [scroll addSubview:tabl_for_today_orders];
    tabl_for_today_orders.hidden =YES;
    
    if (IS_IPHONE_6Plus)
    {
        scroll.frame = CGRectMake(0, 45, WIDTH, HEIGHT+50);
        
        bg_for_sech_bar.frame = CGRectMake(5,5, WIDTH-10, 50);
        img_serch_bar.frame = CGRectMake(10,10, WIDTH-30, 30);
        txt_search.frame = CGRectMake(10,-8, 300, 45);
        icon_search.frame = CGRectMake(355,8, 15, 15);
        btn_on_search_bar.frame = CGRectMake(310,0, 30, 30);
        
        bg_for_todays_time.frame = CGRectMake(5,CGRectGetMaxY(bg_for_sech_bar.frame)+5, WIDTH-10, 50);
        img_up_down_arrow .frame = CGRectMake(40,13, 25, 25);
        lbl_serving_type.frame = CGRectMake(CGRectGetMaxX(img_up_down_arrow.frame)+10,5, 100, 45);
        btn_on_up_down_arrow.frame = CGRectMake(0,0,175,50);
        img_small_line .frame = CGRectMake(CGRectGetMaxX(lbl_serving_type.frame)+20,10,2,35);
        lbl_today.frame = CGRectMake(CGRectGetMaxX(img_small_line.frame)+50,5, 100, 45);
        icon_dropdown.frame=CGRectMake(CGRectGetMaxX(lbl_today.frame),25, 18, 10);
        btn_on_drop_down.frame = CGRectMake(CGRectGetMaxX(img_small_line.frame),0, 220, 50);
        
        lbl_quantity_available.frame = CGRectMake(230,CGRectGetMaxY(bg_for_todays_time.frame), 300, 45);
        img_line_under_quantity .frame = CGRectMake(230,CGRectGetMaxY(lbl_quantity_available.frame)-10,175,1);
        btn_on_quantity_available.frame = CGRectMake(230,CGRectGetMaxY(bg_for_todays_time.frame), 190, 40);
        
        view_for_todays.frame=CGRectMake(5,CGRectGetMaxY(lbl_quantity_available.frame),WIDTH-10,600);
        img_bg_for_quantity_alert .frame = CGRectMake(0,0,WIDTH-7,180);
        lbl_quantity_alert.frame = CGRectMake(140,02, 300, 45);
        icon_cross.frame=CGRectMake(CGRectGetMidX(lbl_quantity_alert.frame)+75,17, 15,15);
        btn_on_cross.frame = CGRectMake(CGRectGetMidX(lbl_quantity_alert.frame)+65,12, 30,30);
        img_line2 .frame = CGRectMake(20,CGRectGetMaxY(lbl_quantity_alert.frame)-5,WIDTH-50,0.5);
        btn_on_quantity_alert.frame = CGRectMake(0,40,400,130);
        // collView_alert = [[UICollectionView alloc] initWithFrame:CGRectMake(25,CGRectGetMaxY(img_line2.frame)+5,WIDTH-63,100)collectionViewLayout:layout];
        tabl_for_orders.frame  = CGRectMake(2,CGRectGetMaxY(img_bg_for_quantity_alert.frame),WIDTH-15,350);
        
        view_for_on_request.frame=CGRectMake(5,CGRectGetMaxY(bg_for_todays_time.frame),WIDTH-10,600);
        lbl_orders_accept.frame = CGRectMake(90,02, 300, 45);
        img_green_percentage .frame = CGRectMake(20,CGRectGetMaxY(lbl_orders_accept.frame),320,15);
        lbl_percentage_val.frame = CGRectMake(CGRectGetMaxX(img_green_percentage.frame)+10,CGRectGetMaxY(lbl_orders_accept.frame)-15, 300, 45);
        lbl_avg_respond_time.frame = CGRectMake(20,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
        avg_respond_time_val.frame = CGRectMake(CGRectGetMidX(lbl_avg_respond_time.frame)+35,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
        tabl_for_on_request_orders.frame  = CGRectMake(2,CGRectGetMaxY(lbl_avg_respond_time.frame),WIDTH-15,475);
        
        view_for_past_request.frame=CGRectMake(5,CGRectGetMaxY(bg_for_todays_time.frame),WIDTH-10,600);
        tabl_for_past_orders.frame  = CGRectMake(2,10,WIDTH-15,575);
        
        view_for_tomorrow.frame=CGRectMake(5,CGRectGetMaxY(lbl_quantity_available.frame),WIDTH-10,600);
        tabl_for_tomarrow.frame  = CGRectMake(2,10,WIDTH-15,500);
        
        view_for_active.frame=CGRectMake(5,CGRectGetMaxY(lbl_quantity_available.frame),WIDTH-10,600);
        tabl_for_active.frame  = CGRectMake(2,10,WIDTH-15,475);
        
        view_for_cancelled.frame=CGRectMake(5,CGRectGetMaxY(bg_for_todays_time.frame),WIDTH-10,600);
        tabl_for_cancelled.frame  = CGRectMake(2,10,WIDTH-15,475);
        
        view_for_served.frame=CGRectMake(5,CGRectGetMaxY(bg_for_todays_time.frame),WIDTH-10,600);
        tabl_for_served.frame  = CGRectMake(2,10,WIDTH-15,475);
        tabl_for_today_orders.frame  = CGRectMake(CGRectGetMaxX(img_small_line.frame)+5,CGRectGetMaxY(bg_for_todays_time.frame),185,150);
        
    }
    else if (IS_IPHONE_6)
    {
        scroll.frame = CGRectMake(0, 45, WIDTH, HEIGHT);
        
        bg_for_sech_bar.frame = CGRectMake(5,5, WIDTH-10, 50);
        img_serch_bar.frame = CGRectMake(10,10, WIDTH-30, 30);
        txt_search.frame = CGRectMake(10,-8, 300, 45);
        icon_search.frame = CGRectMake(310,8, 15, 15);
        btn_on_search_bar.frame = CGRectMake(310,0, 30, 30);
        
        
        bg_for_todays_time.frame = CGRectMake(5,CGRectGetMaxY(bg_for_sech_bar.frame)+5, WIDTH-10, 50);
        img_up_down_arrow .frame = CGRectMake(40,13, 25, 25);
        lbl_serving_type.frame = CGRectMake(CGRectGetMaxX(img_up_down_arrow.frame)+10,5, 100, 45);
        btn_on_up_down_arrow.frame = CGRectMake(0,0,175,50);
        img_small_line .frame = CGRectMake(CGRectGetMaxX(lbl_serving_type.frame),10,2,35);
        lbl_today.frame = CGRectMake(CGRectGetMaxX(img_small_line.frame)+40,5, 100, 45);
        icon_dropdown.frame=CGRectMake(CGRectGetMaxX(lbl_today.frame),25, 18, 10);
        btn_on_drop_down.frame = CGRectMake(CGRectGetMaxX(img_small_line.frame),0, 185, 50);
        
        
        lbl_quantity_available.frame = CGRectMake(180,CGRectGetMaxY(bg_for_todays_time.frame), 300, 45);
        img_line_under_quantity .frame = CGRectMake(180,CGRectGetMaxY(lbl_quantity_available.frame)-10,175,1);
        btn_on_quantity_available.frame = CGRectMake(180,CGRectGetMaxY(bg_for_todays_time.frame), 190, 40);
        
        view_for_todays.frame=CGRectMake(5,CGRectGetMaxY(lbl_quantity_available.frame),WIDTH-10,600);
        img_bg_for_quantity_alert .frame = CGRectMake(0,10,WIDTH-7,180);
        lbl_quantity_alert.frame = CGRectMake(110,02, 300, 45);
        icon_cross.frame=CGRectMake(CGRectGetMidX(lbl_quantity_alert.frame)+65,17, 15,15);
        btn_on_cross.frame = CGRectMake(CGRectGetMidX(lbl_quantity_alert.frame)+65,12, 30,30);
        img_line2 .frame = CGRectMake(20,CGRectGetMaxY(lbl_quantity_alert.frame)-5,WIDTH-50,0.5);
        btn_on_quantity_alert.frame = CGRectMake(0,40,360,130);
        // collView_alert = [[UICollectionView alloc] initWithFrame:CGRectMake(25,CGRectGetMaxY(img_line2.frame)+5,WIDTH-63,100)collectionViewLayout:layout];
        tabl_for_orders.frame  = CGRectMake(2,CGRectGetMaxY(img_bg_for_quantity_alert.frame),WIDTH-15,275);
        
        view_for_on_request.frame=CGRectMake(5,CGRectGetMaxY(bg_for_todays_time.frame),WIDTH-10,600);
        lbl_orders_accept.frame = CGRectMake(90,02, 300, 45);
        img_green_percentage .frame = CGRectMake(20,CGRectGetMaxY(lbl_orders_accept.frame),260,15);
        lbl_percentage_val.frame = CGRectMake(CGRectGetMaxX(img_green_percentage.frame)+10,CGRectGetMaxY(lbl_orders_accept.frame)-15, 300, 45);
        lbl_avg_respond_time.frame = CGRectMake(20,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
        avg_respond_time_val.frame = CGRectMake(CGRectGetMidX(lbl_avg_respond_time.frame)+35,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
        tabl_for_on_request_orders.frame  = CGRectMake(2,CGRectGetMaxY(lbl_avg_respond_time.frame),WIDTH-15,475);
        
        view_for_past_request.frame=CGRectMake(5,CGRectGetMaxY(bg_for_todays_time.frame),WIDTH-10,600);
        tabl_for_past_orders.frame  = CGRectMake(2,10,WIDTH-15,475);
        
        view_for_tomorrow.frame=CGRectMake(5,CGRectGetMaxY(lbl_quantity_available.frame),WIDTH-10,600);
        tabl_for_tomarrow.frame  = CGRectMake(2,10,WIDTH-15,475);
        
        view_for_active.frame=CGRectMake(5,CGRectGetMaxY(lbl_quantity_available.frame),WIDTH-10,600);
        tabl_for_active.frame  = CGRectMake(2,10,WIDTH-15,475);
        
        view_for_cancelled.frame=CGRectMake(5,CGRectGetMaxY(bg_for_todays_time.frame),WIDTH-10,600);
        tabl_for_cancelled.frame  = CGRectMake(2,10,WIDTH-15,475);
        
        view_for_served.frame=CGRectMake(5,CGRectGetMaxY(bg_for_todays_time.frame),WIDTH-10,600);
        tabl_for_served.frame  = CGRectMake(2,10,WIDTH-15,475);
        tabl_for_today_orders.frame  = CGRectMake(CGRectGetMaxX(img_small_line.frame)+5,CGRectGetMaxY(bg_for_todays_time.frame),185,150);
        
    }
    else
    {
        scroll.frame = CGRectMake(0, 45, WIDTH, HEIGHT-70);
        
        bg_for_sech_bar.frame = CGRectMake(5,5, WIDTH-10, 50);
        img_serch_bar.frame = CGRectMake(10,10, WIDTH-30, 30);
        txt_search.frame = CGRectMake(10,-8, 300, 45);
        icon_search.frame = CGRectMake(265,8, 15, 15);
        btn_on_search_bar.frame = CGRectMake(310,0, 30, 30);
        bg_for_todays_time.frame = CGRectMake(5,CGRectGetMaxY(bg_for_sech_bar.frame)+5, WIDTH-10, 50);
        img_up_down_arrow .frame = CGRectMake(20,13, 25, 25);
        lbl_serving_type.frame = CGRectMake(CGRectGetMaxX(img_up_down_arrow.frame)+5,5, 100, 45);
        btn_on_up_down_arrow.frame = CGRectMake(0,0,175,50);
        img_small_line .frame = CGRectMake(CGRectGetMaxX(lbl_serving_type.frame),10,2,35);
        lbl_today.frame = CGRectMake(CGRectGetMaxX(img_small_line.frame)+25,5, 100, 45);
        icon_dropdown.frame=CGRectMake(CGRectGetMaxX(lbl_today.frame)-5,25, 18, 10);
        btn_on_drop_down.frame = CGRectMake(CGRectGetMaxX(img_small_line.frame),0, 185, 50);
        
        lbl_quantity_available.frame = CGRectMake(150,CGRectGetMaxY(bg_for_todays_time.frame), 300, 45);
        img_line_under_quantity .frame = CGRectMake(150,CGRectGetMaxY(lbl_quantity_available.frame)-10,155,1);
        btn_on_quantity_available.frame = CGRectMake(150,CGRectGetMaxY(bg_for_todays_time.frame), 160, 40);
        lbl_quantity_available.font = [UIFont fontWithName:kFont size:13];
        
        view_for_todays.frame=CGRectMake(5,CGRectGetMaxY(lbl_quantity_available.frame),WIDTH-10,600);
        img_bg_for_quantity_alert .frame = CGRectMake(0,0,WIDTH-7,180);
        lbl_quantity_alert.frame = CGRectMake(110,02, 300, 45);
        icon_cross.frame=CGRectMake(CGRectGetMidX(lbl_quantity_alert.frame)+20,17, 15,15);
        btn_on_cross.frame = CGRectMake(CGRectGetMidX(lbl_quantity_alert.frame)+65,12, 30,30);
        img_line2 .frame = CGRectMake(20,CGRectGetMaxY(lbl_quantity_alert.frame)-5,WIDTH-50,0.5);
        btn_on_quantity_alert.frame = CGRectMake(0,40, 310, 130);
        //   collView_alert = [[UICollectionView alloc] initWithFrame:CGRectMake(25,CGRectGetMaxY(img_line2.frame)+5,WIDTH-63,100)collectionViewLayout:layout];
        tabl_for_orders.frame  = CGRectMake(2,CGRectGetMaxY(img_bg_for_quantity_alert.frame),WIDTH-15,275);
        
        view_for_on_request.frame=CGRectMake(5,CGRectGetMaxY(bg_for_todays_time.frame),WIDTH-10,600);
        lbl_orders_accept.frame = CGRectMake(90,02, 300, 45);
        img_green_percentage .frame = CGRectMake(10,CGRectGetMaxY(lbl_orders_accept.frame),240,15);
        lbl_percentage_val.frame = CGRectMake(CGRectGetMaxX(img_green_percentage.frame)+10,CGRectGetMaxY(lbl_orders_accept.frame)-15, 300, 45);
        lbl_avg_respond_time.frame = CGRectMake(20,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
        avg_respond_time_val.frame = CGRectMake(CGRectGetMidX(lbl_avg_respond_time.frame)+15,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
        tabl_for_on_request_orders.frame  = CGRectMake(2,CGRectGetMaxY(lbl_avg_respond_time.frame),WIDTH-15,475);
        
        view_for_past_request.frame=CGRectMake(5,CGRectGetMaxY(bg_for_todays_time.frame),WIDTH-10,600);
        tabl_for_past_orders.frame  = CGRectMake(2,10,WIDTH-15,475);
        
        view_for_tomorrow.frame=CGRectMake(5,CGRectGetMaxY(lbl_quantity_available.frame),WIDTH-10,600);
        tabl_for_tomarrow.frame  = CGRectMake(2,10,WIDTH-15,475);
        
        view_for_active.frame=CGRectMake(5,CGRectGetMaxY(lbl_quantity_available.frame),WIDTH-10,600);
        tabl_for_active.frame  = CGRectMake(2,10,WIDTH-15,475);
        
        view_for_cancelled.frame=CGRectMake(5,CGRectGetMaxY(bg_for_todays_time.frame),WIDTH-10,600);
        tabl_for_cancelled.frame  = CGRectMake(2,10,WIDTH-15,475);
        
        view_for_served.frame=CGRectMake(5,CGRectGetMaxY(bg_for_todays_time.frame),WIDTH-10,600);
        tabl_for_served.frame  = CGRectMake(2,10,WIDTH-15,475);
        tabl_for_today_orders.frame  = CGRectMake(CGRectGetMaxX(img_small_line.frame)+5,CGRectGetMaxY(bg_for_todays_time.frame),185,150);
        
        
        lbl_orders_accept.font = [UIFont fontWithName:kFontBold size:13];
        lbl_avg_respond_time.font = [UIFont fontWithName:kFont size:13];
        avg_respond_time_val.font = [UIFont fontWithName:kFontBold size:13];
        
        
    }
    
    
    
    
    [scroll setContentSize:CGSizeMake(0,750)];
    
    
    
}
#pragma mark UiCollectionView Delegate Methods for dish

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [array_labl_serving_time count];
    
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView1 dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    
    
    UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
    cell_for_collection_view .frame = CGRectMake(0,-5,WIDTH,103);
    [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    cell_for_collection_view .backgroundColor = [UIColor clearColor];
    [cell_for_collection_view  setUserInteractionEnabled:YES];
    [cell.contentView addSubview:cell_for_collection_view];
    
    UIImageView *img_dish = [[UIImageView alloc]init];
    img_dish .frame = CGRectMake(5,15,80,80);
    [img_dish  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
    img_dish .backgroundColor = [UIColor clearColor];
    [img_dish  setUserInteractionEnabled:YES];
    [cell_for_collection_view addSubview:img_dish];
    
    UILabel *lbl_serving_low = [[UILabel alloc]init ];
    lbl_serving_low.frame = CGRectMake(210,-11, 300, 45);
    lbl_serving_low.text = @"Serving Low";
    lbl_serving_low.font = [UIFont fontWithName:kFontBold size:17];
    lbl_serving_low.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    lbl_serving_low.backgroundColor = [UIColor clearColor];
    [cell_for_collection_view addSubview:lbl_serving_low];
    
    if (IS_IPHONE_6Plus)
    {
        cell_for_collection_view .frame = CGRectMake(0,-5,WIDTH,103);
        img_dish .frame = CGRectMake(5,15,80,80);
        lbl_serving_low.frame = CGRectMake(210,-11, 300, 45);
        
    }
    else if (IS_IPHONE_6)
    {
        cell_for_collection_view .frame = CGRectMake(0,-5,WIDTH,103);
        img_dish .frame = CGRectMake(5,15,80,80);
        lbl_serving_low.frame = CGRectMake(210,-11, 300, 45);
        
    }
    else
    {
        cell_for_collection_view .frame = CGRectMake(0,-5,WIDTH,103);
        img_dish .frame = CGRectMake(5,15,80,80);
        lbl_serving_low.frame = CGRectMake(210,-11, 300, 45);
        
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((WIDTH-63), 90);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
}


#pragma tableview delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tabl_for_today_orders )
        
    {
        return [array_head_names count];
    }
    else if (tableView ==  tabl_for_orders)
    {
        return [ary_dishListfilter count];
    }
    else if (tableView == tabl_for_on_request_orders)
    {
        return [ary_dishListfilter count];
    }
    else if (tableView == tabl_for_past_orders)
    {
        return [ary_dishListfilter count];
    }
    else if (tableView == tabl_for_tomarrow)
    {
        return [ary_dishListfilter count];
    }
    else if (tableView == tabl_for_active)
    {
        return [ary_dishListfilter count];
    }
    else if (tableView == tabl_for_cancelled)
    {
        return [array_order_no count];
    }
    else if (tableView == tabl_for_served)
    {
        return [ary_dishListfilter count];
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tabl_for_today_orders )
        
    {
        return 40;
    }
    else if (tableView ==  tabl_for_orders)
    {
        return 170;
    }
    else if (tableView == tabl_for_on_request_orders)
    {
        return 170;
    }
    else if (tableView == tabl_for_past_orders)
    {
        return 190;
    }
    else if (tableView == tabl_for_tomarrow)
    {
        return 170;
    }
    else if (tableView == tabl_for_active)
    {
        return 170;
    }
    else if (tableView == tabl_for_cancelled)
    {
        return 265;
    }
    else if (tableView == tabl_for_served)
    {
        return 170;
    }
    return 0;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    if (tableView == tabl_for_today_orders )
    {
        
        img_bg_for_first_tbl = [[UIImageView alloc]init];
        img_bg_for_first_tbl.frame =  CGRectMake(0,0, 220, 40);
        img_bg_for_first_tbl.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_bg_for_first_tbl setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_first_tbl];
        
        lbl_todays = [[UILabel alloc]init];
        lbl_todays .frame = CGRectMake(5,10,200, 20);
        lbl_todays .text = [NSString stringWithFormat:@"%@",[array_head_names objectAtIndex:indexPath.row]];
        lbl_todays .font = [UIFont fontWithName:kFontBold size:15];
        lbl_todays .textColor = [UIColor blackColor];
        lbl_todays .backgroundColor = [UIColor clearColor];
        [img_bg_for_first_tbl addSubview: lbl_todays ];
        
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(0,0, 185, 40);
            lbl_todays .frame = CGRectMake(5,10,200, 20);
            
        }
        else if (IS_IPHONE_6)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(0,0, 185, 40);
            lbl_todays .frame = CGRectMake(5,10,200, 20);
            
        }
        else
        {
            img_bg_for_first_tbl.frame =  CGRectMake(0,0, 160, 40);
            lbl_todays .frame = CGRectMake(5,10,200, 20);
            
        }
        
        
    }
    else if (tableView ==  tabl_for_orders )
    {
        UIImageView * img_bg_for_order_no = [[UIImageView alloc]init];
        img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
        img_bg_for_order_no.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_order_no.backgroundColor = [UIColor whiteColor];
        [img_bg_for_order_no setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_order_no];
        
        UILabel *lbl_order_number = [[UILabel alloc]init ];
        lbl_order_number.frame = CGRectMake(10,0, 100, 45);
        lbl_order_number.text = @"Order no.:";
        lbl_order_number.font = [UIFont fontWithName:kFont size:18];
        lbl_order_number.textColor = [UIColor blackColor];
        lbl_order_number.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_order_number];
        
        // [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1
        
        UILabel *  lbl_order_number_val = [[UILabel alloc]init];
        lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
        lbl_order_number_val .font = [UIFont fontWithName:kFontBold size:15];
        lbl_order_number_val .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        lbl_order_number_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_order_number_val ];
        
        UILabel *lbl_value = [[UILabel alloc]init ];
        lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
        lbl_value.text = @"Value:";
        lbl_value.font = [UIFont fontWithName:kFont size:15];
        lbl_value.textColor = [UIColor blackColor];
        lbl_value.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_value];
        
        UILabel * value = [[UILabel alloc]init];
        value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
        value .font = [UIFont fontWithName:kFontBold size:15];
        value .textColor = [UIColor blackColor];
        value .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:value ];
        
        UILabel *lbl_received_date = [[UILabel alloc]init ];
        lbl_received_date.frame = CGRectMake(40,CGRectGetMidY(lbl_value.frame), 80, 45);
        lbl_received_date.text = @"Received:";
        lbl_received_date.font = [UIFont fontWithName:kFont size:12];
        lbl_received_date.textColor = [UIColor blackColor];
        lbl_received_date.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_received_date];
        
        UILabel * received_date_val = [[UILabel alloc]init];
        received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_received_date.frame)-17,CGRectGetMaxY(value.frame),180,20);
        
        received_date_val .font = [UIFont fontWithName:kFontBold size:12];
        received_date_val .textColor = [UIColor blackColor];
        received_date_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:received_date_val];
        
        UILabel *lbl_due_in = [[UILabel alloc]init ];
        lbl_due_in.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+140,0, 80, 45);
        lbl_due_in.text = @"Due in";
        lbl_due_in.font = [UIFont fontWithName:kFont size:15];
        lbl_due_in.textColor = [UIColor blackColor];
        lbl_due_in.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_due_in];
        
        UILabel *  due_in_val = [[UILabel alloc]init];
        due_in_val .frame = CGRectMake(CGRectGetMaxX(lbl_due_in.frame)-30,13,180,20);
        
        due_in_val .font = [UIFont fontWithName:kFontBold size:15];
        due_in_val .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        due_in_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:due_in_val];
        
        UIImageView *  icon_serving_type = [[UIImageView alloc]init];
        icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+210,CGRectGetMaxY(due_in_val.frame)+24,25,25);
        
        icon_serving_type .backgroundColor = [UIColor clearColor];
        [icon_serving_type  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:icon_serving_type];
        
        
        UIImageView *img_line = [[UIImageView alloc]init];
        img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_received_date.frame)-4,WIDTH-40,0.5);
        [img_line  setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
        img_line .backgroundColor = [UIColor clearColor];
        [img_line  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:img_line];
        
        UIImageView * img_user = [[UIImageView alloc]init];
        img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
        img_user .backgroundColor = [UIColor clearColor];
        [img_user  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:img_user];
        
        UILabel * user_name = [[UILabel alloc]init];
        user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
        user_name .font = [UIFont fontWithName:kFontBold size:15];
        user_name .textColor = [UIColor blackColor];
        user_name .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:user_name];
        
        
        UIButton *icon_massage = [UIButton buttonWithType:UIButtonTypeCustom];
        icon_massage.frame = CGRectMake(CGRectGetMaxX(img_user.frame)+248,CGRectGetMaxY(img_line.frame)+25,25,20);
        icon_massage .backgroundColor = [UIColor clearColor];
        icon_massage.tag = indexPath.row;
        
        [icon_massage addTarget:self action:@selector(click_on_message_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_massage setImage:[UIImage imageNamed:@"icon-msg@2x.png"] forState:UIControlStateNormal];
        [img_bg_for_order_no   addSubview:icon_massage];
        
        UIButton * icon_semi_cercle = [UIButton buttonWithType:UIButtonTypeCustom];
        icon_semi_cercle.frame = CGRectMake(160,CGRectGetMaxY(user_name.frame)+3,50,30);
        icon_semi_cercle .backgroundColor = [UIColor clearColor];
        [icon_semi_cercle addTarget:self action:@selector(click_on_semi_cercle_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_semi_cercle setImage:[UIImage imageNamed:@"icon-semi-cercle@2x.png"] forState:UIControlStateNormal];
        [img_bg_for_order_no  addSubview:icon_semi_cercle];
        
        
        //        UIView * transperent_view_for_served=[[UIView alloc] init];
        //        [transperent_view_for_served removeFromSuperview];
        //        transperent_view_for_served.frame =  CGRectMake(0,0,WIDTH, 170);
        //        transperent_view_for_served.backgroundColor=[UIColor clearColor];
        //        transperent_view_for_served.userInteractionEnabled=TRUE;
        //        [img_bg_for_order_no addSubview:transperent_view_for_served];
        //
        //
        //
        //
        //        img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
        //        lbl_order_number.frame = CGRectMake(10,0, 100, 45);
        
        
        
        //     transperent_view_for_served.hidden = YES;
        
        
        
        lbl_order_number_val .text = [NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"orderno"]];
        value .text = [NSString stringWithFormat:@"$ %@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"value"]];
        received_date_val .text = [NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"recieved"]];
        
        if ([[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"duein"] intValue]<=0)
        {
            due_in_val .text = [NSString stringWithFormat:@"%@",@"Past"];
            
        }
        else{
            due_in_val .text = [NSString stringWithFormat:@"%@ min",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"duein"]];
            
        }
        
        NSString *url_Img = [NSString stringWithFormat: @"%@", [[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"userimage"]];
        img_user.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img]]];
        
        
        
        
        
        if ([[NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"serve_type"]]isEqualToString:@"dinein"])
        {
            [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"img-dinin@2x.png"]]];
            
        }
        else if ([[NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"serve_type"]]isEqualToString:@"takeout"])
        {
            [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"take-icon@2x.png"]]];
            
        }
        else{
            [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"deliver-icon@2x.png"]]];
            
        }
        
        //[icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_seving_type objectAtIndex:indexPath.row]]]];
        //[img_user  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_user_img objectAtIndex:indexPath.row]]]];
        
        user_name .text = [NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"username"]];
        
        
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
            lbl_order_number.frame = CGRectMake(10,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
            lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
            value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
            lbl_received_date.frame = CGRectMake(40,CGRectGetMidY(lbl_value.frame), 80, 45);
            received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_received_date.frame)-17,CGRectGetMaxY(value.frame),180,20);
            lbl_due_in.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+170,0, 80, 45);
            due_in_val .frame = CGRectMake(CGRectGetMaxX(lbl_due_in.frame)-30,13,180,20);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+250,CGRectGetMaxY(due_in_val.frame)+24,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_received_date.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            icon_massage.frame = CGRectMake(CGRectGetMaxX(img_user.frame)+288,CGRectGetMaxY(img_line.frame)+25,25,20);
            icon_semi_cercle.frame = CGRectMake(160,CGRectGetMaxY(user_name.frame)+3,50,30);
            //transperent_view_for_served.frame = CGRectMake(0,3,50,30);
            
        }
        else if (IS_IPHONE_6)
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
            lbl_order_number.frame = CGRectMake(10,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
            lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
            value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
            lbl_received_date.frame = CGRectMake(40,CGRectGetMidY(lbl_value.frame), 80, 45);
            received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_received_date.frame)-17,CGRectGetMaxY(value.frame),180,20);
            lbl_due_in.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+140,0, 80, 45);
            due_in_val .frame = CGRectMake(CGRectGetMaxX(lbl_due_in.frame)-30,13,180,20);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+210,CGRectGetMaxY(due_in_val.frame)+24,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_received_date.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            icon_massage.frame = CGRectMake(CGRectGetMaxX(img_user.frame)+248,CGRectGetMaxY(img_line.frame)+25,25,20);
            icon_semi_cercle.frame = CGRectMake(160,CGRectGetMaxY(user_name.frame)+3,50,30);
            // transperent_view_for_served.frame = CGRectMake(0,3,50,30);
            
        }
        else
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
            lbl_order_number.frame = CGRectMake(15,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-20,13,100,20);
            lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
            value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-25,CGRectGetMaxY(lbl_value.frame)-30,100,20);
            lbl_received_date.frame = CGRectMake(35,CGRectGetMidY(lbl_value.frame), 80, 45);
            received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_received_date.frame)-20,CGRectGetMaxY(value.frame),180,20);
            lbl_due_in.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+90,0, 80, 45);
            due_in_val .frame = CGRectMake(CGRectGetMaxX(lbl_due_in.frame)-35,13,180,20);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+150,CGRectGetMaxY(due_in_val.frame)+24,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_received_date.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            icon_massage.frame = CGRectMake(CGRectGetMaxX(img_user.frame)+193,CGRectGetMaxY(img_line.frame)+25,25,20);
            icon_semi_cercle.frame = CGRectMake(160,CGRectGetMaxY(user_name.frame)+3,50,30);
            // transperent_view_for_served.frame = CGRectMake(0,3,50,30);
            
            lbl_order_number.font = [UIFont fontWithName:kFont size:15];
            lbl_value.font = [UIFont fontWithName:kFont size:12];
            value .font = [UIFont fontWithName:kFontBold size:12];
            lbl_received_date.font = [UIFont fontWithName:kFont size:11];
            received_date_val .font = [UIFont fontWithName:kFontBold size:11];
            lbl_due_in.font = [UIFont fontWithName:kFont size:12];
            due_in_val .font = [UIFont fontWithName:kFontBold size:12];
            
        }
        
    }
    else if (tableView == tabl_for_on_request_orders)
    {
        UIImageView *  img_bg_for_order_no = [[UIImageView alloc]init];
        img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
        img_bg_for_order_no.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_order_no.backgroundColor = [UIColor whiteColor];
        [img_bg_for_order_no setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_order_no];
        
        UILabel *lbl_order_number = [[UILabel alloc]init ];
        lbl_order_number.frame = CGRectMake(10,0, 100, 45);
        lbl_order_number.text = @"Order no.:";
        lbl_order_number.font = [UIFont fontWithName:kFont size:18];
        lbl_order_number.textColor = [UIColor blackColor];
        lbl_order_number.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_order_number];
        
        // [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1
        
        UILabel * lbl_order_number_val = [[UILabel alloc]init];
        lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
        // lbl_order_number_val .text = [NSString stringWithFormat:@"%@",[array_order_no objectAtIndex:indexPath.row]];
        lbl_order_number_val .font = [UIFont fontWithName:kFontBold size:15];
        lbl_order_number_val .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        lbl_order_number_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_order_number_val ];
        
        UILabel *lbl_value = [[UILabel alloc]init ];
        lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
        lbl_value.text = @"Value:";
        lbl_value.font = [UIFont fontWithName:kFont size:15];
        lbl_value.textColor = [UIColor blackColor];
        lbl_value.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_value];
        
        UILabel * value = [[UILabel alloc]init];
        value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
        // value .text = [NSString stringWithFormat:@"%@",[array_value objectAtIndex:indexPath.row]];
        value .font = [UIFont fontWithName:kFontBold size:15];
        value .textColor = [UIColor blackColor];
        value .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:value ];
        
        UILabel *lbl_received_date = [[UILabel alloc]init ];
        lbl_received_date.frame = CGRectMake(40,CGRectGetMidY(lbl_value.frame), 80, 45);
        lbl_received_date.text = @"Received:";
        lbl_received_date.font = [UIFont fontWithName:kFont size:12];
        lbl_received_date.textColor = [UIColor blackColor];
        lbl_received_date.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_received_date];
        
        UILabel * received_date_val = [[UILabel alloc]init];
        received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_received_date.frame)-17,CGRectGetMaxY(value.frame),180,20);
        //received_date_val .text = [NSString stringWithFormat:@"%@",[array_recived_date objectAtIndex:indexPath.row]];
        received_date_val .font = [UIFont fontWithName:kFontBold size:12];
        received_date_val .textColor = [UIColor blackColor];
        received_date_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:received_date_val];
        
        UILabel *lbl_due_in = [[UILabel alloc]init ];
        lbl_due_in.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+160,0, 80, 45);
        lbl_due_in.text = @"Due in";
        lbl_due_in.font = [UIFont fontWithName:kFont size:15];
        lbl_due_in.textColor = [UIColor blackColor];
        lbl_due_in.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_due_in];
        
        UILabel *  due_in_val = [[UILabel alloc]init];
        due_in_val .frame = CGRectMake(CGRectGetMaxX(lbl_due_in.frame)-30,13,180,20);
        // due_in_val .text = [NSString stringWithFormat:@"%@",[array_due_time objectAtIndex:indexPath.row]];
        due_in_val .font = [UIFont fontWithName:kFontBold size:15];
        due_in_val .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        due_in_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:due_in_val];
        
        UIImageView * icon_serving_type = [[UIImageView alloc]init];
        icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+210,CGRectGetMaxY(due_in_val.frame)+24,25,25);
        //[icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_seving_type objectAtIndex:indexPath.row]]]];
        icon_serving_type .backgroundColor = [UIColor clearColor];
        [icon_serving_type  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:icon_serving_type];
        
        
        UIImageView *img_line = [[UIImageView alloc]init];
        img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_received_date.frame)-4,WIDTH-40,0.5);
        [img_line  setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
        img_line .backgroundColor = [UIColor clearColor];
        [img_line  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:img_line];
        
        UIImageView*  img_user = [[UIImageView alloc]init];
        img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
        //[img_user  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_user_img objectAtIndex:indexPath.row]]]];
        img_user .backgroundColor = [UIColor clearColor];
        [img_user  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:img_user];
        
        
        UILabel*user_name;
        user_name = [[UILabel alloc]init];
        user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
        // user_name .text = [NSString stringWithFormat:@"%@",[array_user_name objectAtIndex:indexPath.row]];
        user_name .font = [UIFont fontWithName:kFontBold size:15];
        user_name .textColor = [UIColor blackColor];
        user_name .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:user_name];
        
        
        
        lbl_order_number_val .text = [NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"orderno"]];
        value .text = [NSString stringWithFormat:@"$ %@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"value"]];
        received_date_val .text = [NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"recieved"]];
        
        if ([[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"duein"] intValue]<=0)
        {
            due_in_val .text = [NSString stringWithFormat:@"%@",@"Past"];
            
        }
        else{
            due_in_val .text = [NSString stringWithFormat:@"%@ min",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"duein"]];
            
        }
        
        NSString *url_Img = [NSString stringWithFormat: @"%@", [[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"userimage"]];
        img_user.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img]]];
        if ([[NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"serve_type"]]isEqualToString:@"dinein"])
        {
            [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"img-dinin@2x.png"]]];
            
        }
        else if ([[NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"serve_type"]]isEqualToString:@"takeout"])
        {
            [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"take-icon@2x.png"]]];
            
        }
        else{
            [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"deliver-icon@2x.png"]]];
            
        }
        //[icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_seving_type objectAtIndex:indexPath.row]]]];
        //[img_user  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_user_img objectAtIndex:indexPath.row]]]];
        
        user_name .text = [NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"username"]];
        
        
        UIButton *icon_massage = [UIButton buttonWithType:UIButtonTypeCustom];
        icon_massage.frame = CGRectMake(CGRectGetMaxX(img_user.frame)+248,CGRectGetMaxY(img_line.frame)+25,25,20);
        icon_massage .backgroundColor = [UIColor clearColor];
        icon_massage.tag = indexPath.row;
        
        [icon_massage addTarget:self action:@selector(click_on_message_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_massage setImage:[UIImage imageNamed:@"icon-msg@2x.png"] forState:UIControlStateNormal];
        [img_bg_for_order_no   addSubview:icon_massage];
        
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
            lbl_order_number.frame = CGRectMake(10,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
            lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
            value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
            lbl_received_date.frame = CGRectMake(40,CGRectGetMidY(lbl_value.frame), 80, 45);
            received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_received_date.frame)-17,CGRectGetMaxY(value.frame),180,20);
            lbl_due_in.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+160,0, 80, 45);
            due_in_val .frame = CGRectMake(CGRectGetMaxX(lbl_due_in.frame)-30,13,180,20);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+250,CGRectGetMaxY(due_in_val.frame)+24,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_received_date.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            icon_massage.frame = CGRectMake(CGRectGetMaxX(img_user.frame)+288,CGRectGetMaxY(img_line.frame)+25,25,20);
            
        }
        else if (IS_IPHONE_6)
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
            lbl_order_number.frame = CGRectMake(10,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
            lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
            value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
            lbl_received_date.frame = CGRectMake(40,CGRectGetMidY(lbl_value.frame), 80, 45);
            received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_received_date.frame)-17,CGRectGetMaxY(value.frame),180,20);
            lbl_due_in.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+140,0, 80, 45);
            due_in_val .frame = CGRectMake(CGRectGetMaxX(lbl_due_in.frame)-30,13,180,20);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+210,CGRectGetMaxY(due_in_val.frame)+24,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_received_date.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            icon_massage.frame = CGRectMake(CGRectGetMaxX(img_user.frame)+248,CGRectGetMaxY(img_line.frame)+25,25,20);
            
        }
        else
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
            lbl_order_number.frame = CGRectMake(15,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-20,13,100,20);
            lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
            value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-25,CGRectGetMaxY(lbl_value.frame)-30,100,20);
            lbl_received_date.frame = CGRectMake(35,CGRectGetMidY(lbl_value.frame), 80, 45);
            received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_received_date.frame)-20,CGRectGetMaxY(value.frame),180,20);
            lbl_due_in.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+90,0, 80, 45);
            due_in_val .frame = CGRectMake(CGRectGetMaxX(lbl_due_in.frame)-35,13,180,20);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+150,CGRectGetMaxY(due_in_val.frame)+24,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_received_date.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            icon_massage.frame = CGRectMake(CGRectGetMaxX(img_user.frame)+193,CGRectGetMaxY(img_line.frame)+25,25,20);
            
            lbl_order_number.font = [UIFont fontWithName:kFont size:15];
            lbl_value.font = [UIFont fontWithName:kFont size:12];
            value .font = [UIFont fontWithName:kFontBold size:12];
            lbl_received_date.font = [UIFont fontWithName:kFont size:11];
            received_date_val .font = [UIFont fontWithName:kFontBold size:11];
            lbl_due_in.font = [UIFont fontWithName:kFont size:12];
            due_in_val .font = [UIFont fontWithName:kFontBold size:12];
        }
        
        
    }
    
    else if (tableView == tabl_for_past_orders)
    {
        
        UIImageView *  img_bg_for_order_no = [[UIImageView alloc]init];
        img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 190);
        img_bg_for_order_no.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_order_no.backgroundColor = [UIColor whiteColor];
        [img_bg_for_order_no setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_order_no];
        
        UILabel *lbl_order_number = [[UILabel alloc]init ];
        lbl_order_number.frame = CGRectMake(10,0, 100, 45);
        lbl_order_number.text = @"Order no.:";
        lbl_order_number.font = [UIFont fontWithName:kFont size:18];
        lbl_order_number.textColor = [UIColor blackColor];
        lbl_order_number.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_order_number];
        
        // [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1
        
        UILabel * lbl_order_number_val = [[UILabel alloc]init];
        lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
        //lbl_order_number_val .text = [NSString stringWithFormat:@"%@",[array_order_no objectAtIndex:indexPath.row]];
        lbl_order_number_val .font = [UIFont fontWithName:kFontBold size:15];
        lbl_order_number_val .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        lbl_order_number_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_order_number_val ];
        
        UILabel *lbl_value = [[UILabel alloc]init ];
        lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
        lbl_value.text = @"Value:";
        lbl_value.font = [UIFont fontWithName:kFont size:15];
        lbl_value.textColor = [UIColor blackColor];
        lbl_value.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_value];
        
        UILabel * value = [[UILabel alloc]init];
        value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
        //  value .text = [NSString stringWithFormat:@"%@",[array_value objectAtIndex:indexPath.row]];
        value .font = [UIFont fontWithName:kFontBold size:15];
        value .textColor = [UIColor blackColor];
        value .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:value ];
        
        UILabel *lbl_order_date = [[UILabel alloc]init ];
        lbl_order_date.frame = CGRectMake(10,CGRectGetMidY(lbl_value.frame), 180, 45);
        lbl_order_date.text = @"Ordered Date/Time:";
        lbl_order_date.font = [UIFont fontWithName:kFont size:13];
        lbl_order_date.textColor = [UIColor blackColor];
        lbl_order_date.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_order_date];
        
        UILabel * order_date_val = [[UILabel alloc]init];
        order_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_date.frame)-50,CGRectGetMaxY(value.frame),180,20);
        //order_date_val .text = [NSString stringWithFormat:@"%@",[array_order_date_and_time objectAtIndex:indexPath.row]];
        order_date_val .font = [UIFont fontWithName:kFontBold size:12];
        order_date_val .textColor = [UIColor blackColor];
        order_date_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:order_date_val];
        
        UILabel *lbl_serving_date = [[UILabel alloc]init ];
        lbl_serving_date.frame = CGRectMake(18,CGRectGetMidY(lbl_order_date.frame), 180, 45);
        lbl_serving_date.text = @"Serving Date/Time:";
        lbl_serving_date.font = [UIFont fontWithName:kFont size:13];
        lbl_serving_date.textColor = [UIColor blackColor];
        lbl_serving_date.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_serving_date];
        
        UILabel * seving_date_val = [[UILabel alloc]init];
        seving_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_serving_date.frame)-58,CGRectGetMaxY(order_date_val.frame)+2,180,20);
        // seving_date_val .text = [NSString stringWithFormat:@"%@",[array_order_date_and_time objectAtIndex:indexPath.row]];
        seving_date_val .font = [UIFont fontWithName:kFontBold size:12];
        seving_date_val .textColor = [UIColor blackColor];
        seving_date_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:seving_date_val];
        
        UILabel * lbl_delivery_state = [[UILabel alloc]init];
        lbl_delivery_state .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+160,13,180,20);
        lbl_delivery_state .text = [NSString stringWithFormat:@"%@",[array_delivery_state objectAtIndex:indexPath.row]];
        lbl_delivery_state .font = [UIFont fontWithName:kFontBold size:17];
        lbl_delivery_state .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        lbl_delivery_state .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_delivery_state];
        
        UIImageView *  icon_serving_type = [[UIImageView alloc]init];
        icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+210,CGRectGetMaxY(lbl_delivery_state.frame)+45,25,25);
        // [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_seving_type objectAtIndex:indexPath.row]]]];
        icon_serving_type .backgroundColor = [UIColor clearColor];
        [icon_serving_type  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:icon_serving_type];
        
        
        UIImageView *img_line = [[UIImageView alloc]init];
        img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_serving_date.frame)-4,WIDTH-40,0.5);
        [img_line  setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
        img_line .backgroundColor = [UIColor clearColor];
        [img_line  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:img_line];
        
        UIImageView *  img_user = [[UIImageView alloc]init];
        img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
        //[img_user  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_user_img objectAtIndex:indexPath.row]]]];
        img_user .backgroundColor = [UIColor clearColor];
        [img_user  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:img_user];
        
        UILabel * user_name = [[UILabel alloc]init];
        user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
        // user_name .text = [NSString stringWithFormat:@"%@",[array_user_name objectAtIndex:indexPath.row]];
        user_name .font = [UIFont fontWithName:kFontBold size:15];
        user_name .textColor = [UIColor blackColor];
        user_name .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:user_name];
        
        
        lbl_order_number_val .text = [NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"orderno"]];
        value .text = [NSString stringWithFormat:@"$ %@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"value"]];
        
        //        if ([[[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"duein"] intValue]<=0)
        //        {
        //            due_in_val .text = [NSString stringWithFormat:@"%@",@"Past"];
        //
        //        }
        //        else{
        //            due_in_val .text = [NSString stringWithFormat:@"%@ min",[[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"duein"]];
        //
        //        }
        
        NSString *url_Img = [NSString stringWithFormat: @"%@", [[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"userimage"]];
        img_user.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img]]];
        if ([[NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"serve_type"]]isEqualToString:@"dinein"])
        {
            [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"img-dinin@2x.png"]]];
            
        }
        else if ([[NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"serve_type"]]isEqualToString:@"takeout"])
        {
            [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"take-icon@2x.png"]]];
            
        }
        else{
            [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"deliver-icon@2x.png"]]];
            
        }
        //[icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_seving_type objectAtIndex:indexPath.row]]]];
        //[img_user  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_user_img objectAtIndex:indexPath.row]]]];
        
        user_name .text = [NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"username"]];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 190);
            lbl_order_number.frame = CGRectMake(10,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
            lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
            value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
            lbl_order_date.frame = CGRectMake(10,CGRectGetMidY(lbl_value.frame), 180, 45);
            order_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_date.frame)-50,CGRectGetMaxY(value.frame),180,20);
            lbl_serving_date.frame = CGRectMake(18,CGRectGetMidY(lbl_order_date.frame), 180, 45);
            seving_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_serving_date.frame)-58,CGRectGetMaxY(order_date_val.frame)+2,180,20);
            lbl_delivery_state .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+200,13,180,20);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+250,CGRectGetMaxY(lbl_delivery_state.frame)+45,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_serving_date.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            
        }
        else if (IS_IPHONE_6)
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 190);
            lbl_order_number.frame = CGRectMake(10,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
            lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
            value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
            lbl_order_date.frame = CGRectMake(10,CGRectGetMidY(lbl_value.frame), 180, 45);
            order_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_date.frame)-50,CGRectGetMaxY(value.frame),180,20);
            lbl_serving_date.frame = CGRectMake(18,CGRectGetMidY(lbl_order_date.frame), 180, 45);
            seving_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_serving_date.frame)-58,CGRectGetMaxY(order_date_val.frame)+2,180,20);
            lbl_delivery_state .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+160,13,180,20);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+210,CGRectGetMaxY(lbl_delivery_state.frame)+45,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_serving_date.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            
        }
        else
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 190);
            lbl_order_number.frame = CGRectMake(10,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-20,13,100,20);
            lbl_value.frame = CGRectMake(48,CGRectGetMidY(lbl_order_number.frame),70, 45);
            value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-27,CGRectGetMaxY(lbl_value.frame)-32,100,20);
            lbl_order_date.frame = CGRectMake(10,CGRectGetMidY(lbl_value.frame), 180, 45);
            order_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_date.frame)-70,CGRectGetMaxY(value.frame)+2,180,20);
            lbl_serving_date.frame = CGRectMake(18,CGRectGetMidY(lbl_order_date.frame), 180, 45);
            seving_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_serving_date.frame)-78,CGRectGetMaxY(order_date_val.frame)+3,180,20);
            lbl_delivery_state .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+110,13,180,20);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+155,CGRectGetMaxY(lbl_delivery_state.frame)+45,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_serving_date.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            
            
            lbl_order_number.font = [UIFont fontWithName:kFont size:16];
            lbl_order_number_val .font = [UIFont fontWithName:kFontBold size:13];
            lbl_value.font = [UIFont fontWithName:kFont size:13];
            value .font = [UIFont fontWithName:kFontBold size:13];
            lbl_order_date.font = [UIFont fontWithName:kFont size:11];
            order_date_val .font = [UIFont fontWithName:kFontBold size:11];
            lbl_serving_date.font = [UIFont fontWithName:kFont size:11];
            seving_date_val .font = [UIFont fontWithName:kFontBold size:11];
            lbl_delivery_state .font = [UIFont fontWithName:kFontBold size:15];
            user_name .font = [UIFont fontWithName:kFontBold size:13];
            
            
        }
        
        
        
    }
    
    else if (tableView == tabl_for_tomarrow)
    {
        UIImageView *  img_bg_for_order_no = [[UIImageView alloc]init];
        img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
        img_bg_for_order_no.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_order_no.backgroundColor = [UIColor whiteColor];
        [img_bg_for_order_no setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_order_no];
        
        UILabel *lbl_order_number = [[UILabel alloc]init ];
        lbl_order_number.frame = CGRectMake(10,0, 100, 45);
        lbl_order_number.text = @"Order no.:";
        lbl_order_number.font = [UIFont fontWithName:kFont size:18];
        lbl_order_number.textColor = [UIColor blackColor];
        lbl_order_number.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_order_number];
        
        // [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1
        
        UILabel *  lbl_order_number_val = [[UILabel alloc]init];
        lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
        // lbl_order_number_val .text = [NSString stringWithFormat:@"%@",[array_order_no objectAtIndex:indexPath.row]];
        lbl_order_number_val .font = [UIFont fontWithName:kFontBold size:15];
        lbl_order_number_val .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        lbl_order_number_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_order_number_val ];
        
        UILabel *lbl_value = [[UILabel alloc]init ];
        lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
        lbl_value.text = @"Value:";
        lbl_value.font = [UIFont fontWithName:kFont size:15];
        lbl_value.textColor = [UIColor blackColor];
        lbl_value.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_value];
        
        UILabel * value = [[UILabel alloc]init];
        value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
        // value .text = [NSString stringWithFormat:@"%@",[array_value objectAtIndex:indexPath.row]];
        value .font = [UIFont fontWithName:kFontBold size:15];
        value .textColor = [UIColor blackColor];
        value .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:value ];
        
        UILabel *lbl_received_date = [[UILabel alloc]init ];
        lbl_received_date.frame = CGRectMake(40,CGRectGetMidY(lbl_value.frame), 80, 45);
        lbl_received_date.text = @"Received:";
        lbl_received_date.font = [UIFont fontWithName:kFont size:12];
        lbl_received_date.textColor = [UIColor blackColor];
        lbl_received_date.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_received_date];
        
        UILabel * received_date_val = [[UILabel alloc]init];
        received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_received_date.frame)-17,CGRectGetMaxY(value.frame),180,20);
        // received_date_val .text = [NSString stringWithFormat:@"%@",[array_recived_date objectAtIndex:indexPath.row]];
        received_date_val .font = [UIFont fontWithName:kFontBold size:12];
        received_date_val .textColor = [UIColor blackColor];
        received_date_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:received_date_val];
        
        UILabel *lbl_due_in = [[UILabel alloc]init ];
        lbl_due_in.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+140,0, 80, 45);
        lbl_due_in.text = @"Due in";
        lbl_due_in.font = [UIFont fontWithName:kFont size:15];
        lbl_due_in.textColor = [UIColor blackColor];
        lbl_due_in.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_due_in];
        
        UILabel * due_in_val = [[UILabel alloc]init];
        due_in_val .frame = CGRectMake(CGRectGetMaxX(lbl_due_in.frame)-30,13,180,20);
        // due_in_val .text = [NSString stringWithFormat:@"%@",[array_due_in_tomarrow objectAtIndex:indexPath.row]];
        due_in_val .font = [UIFont fontWithName:kFontBold size:17];
        due_in_val .textColor = [UIColor colorWithRed:11/255.0f green:0/255.0f blue:65/255.0f alpha:1];
        due_in_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:due_in_val];
        
        UIImageView *   icon_serving_type = [[UIImageView alloc]init];
        icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+210,CGRectGetMaxY(due_in_val.frame)+24,25,25);
        // [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_seving_type objectAtIndex:indexPath.row]]]];
        icon_serving_type .backgroundColor = [UIColor clearColor];
        [icon_serving_type  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:icon_serving_type];
        
        
        UIImageView *img_line = [[UIImageView alloc]init];
        img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_received_date.frame)-4,WIDTH-40,0.5);
        [img_line  setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
        img_line .backgroundColor = [UIColor clearColor];
        [img_line  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:img_line];
        
        UIImageView *   img_user = [[UIImageView alloc]init];
        img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
        // [img_user  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_user_img objectAtIndex:indexPath.row]]]];
        img_user .backgroundColor = [UIColor clearColor];
        [img_user  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:img_user];
        
        UILabel *  user_name = [[UILabel alloc]init];
        user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
        // user_name .text = [NSString stringWithFormat:@"%@",[array_user_name objectAtIndex:indexPath.row]];
        user_name .font = [UIFont fontWithName:kFontBold size:15];
        user_name .textColor = [UIColor blackColor];
        user_name .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:user_name];
        
        
        UIButton *icon_massage = [UIButton buttonWithType:UIButtonTypeCustom];
        icon_massage.frame = CGRectMake(CGRectGetMaxX(img_user.frame)+248,CGRectGetMaxY(img_line.frame)+25,25,20);
        icon_massage.tag = indexPath.row;
        
        icon_massage .backgroundColor = [UIColor clearColor];
        [icon_massage addTarget:self action:@selector(click_on_message_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_massage setImage:[UIImage imageNamed:@"icon-msg@2x.png"] forState:UIControlStateNormal];
        [img_bg_for_order_no   addSubview:icon_massage];
        
        
        lbl_order_number_val .text = [NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"orderno"]];
        value .text = [NSString stringWithFormat:@"$ %@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"value"]];
        received_date_val .text = [NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"recieved"]];
        
        if ([[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"duein"] intValue]<=0)
        {
            due_in_val .text = [NSString stringWithFormat:@"%@",@"Past"];
            
        }
        else{
            due_in_val .text = [NSString stringWithFormat:@"%@ min",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"duein"]];
            
        }
        
        NSString *url_Img = [NSString stringWithFormat: @"%@", [[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"userimage"]];
        img_user.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img]]];
        
        //[icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_seving_type objectAtIndex:indexPath.row]]]];
        //[img_user  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_user_img objectAtIndex:indexPath.row]]]];
        
        user_name .text = [NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"username"]];
        if ([[NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"serve_type"]]isEqualToString:@"dinein"])
        {
            [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"img-dinin@2x.png"]]];
            
        }
        else if ([[NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"serve_type"]]isEqualToString:@"takeout"])
        {
            [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"take-icon@2x.png"]]];
            
        }
        else{
            [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"deliver-icon@2x.png"]]];
            
        }
        
        
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
            lbl_order_number.frame = CGRectMake(10,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
            lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
            value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
            lbl_received_date.frame = CGRectMake(40,CGRectGetMidY(lbl_value.frame), 80, 45);
            received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_received_date.frame)-17,CGRectGetMaxY(value.frame),180,20);
            lbl_due_in.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+175,0, 80, 45);
            due_in_val .frame = CGRectMake(CGRectGetMaxX(lbl_due_in.frame)-30,13,180,20);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+245,CGRectGetMaxY(due_in_val.frame)+24,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_received_date.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            icon_massage.frame = CGRectMake(CGRectGetMaxX(img_user.frame)+283,CGRectGetMaxY(img_line.frame)+25,25,20);
            
        }
        else if (IS_IPHONE_6)
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
            lbl_order_number.frame = CGRectMake(10,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
            lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
            value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
            lbl_received_date.frame = CGRectMake(40,CGRectGetMidY(lbl_value.frame), 80, 45);
            received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_received_date.frame)-17,CGRectGetMaxY(value.frame),180,20);
            lbl_due_in.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+140,0, 80, 45);
            due_in_val .frame = CGRectMake(CGRectGetMaxX(lbl_due_in.frame)-30,13,180,20);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+210,CGRectGetMaxY(due_in_val.frame)+24,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_received_date.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            icon_massage.frame = CGRectMake(CGRectGetMaxX(img_user.frame)+248,CGRectGetMaxY(img_line.frame)+25,25,20);
            
        }
        else
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
            lbl_order_number.frame = CGRectMake(10,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
            lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
            value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
            lbl_received_date.frame = CGRectMake(40,CGRectGetMidY(lbl_value.frame), 80, 45);
            received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_received_date.frame)-17,CGRectGetMaxY(value.frame),180,20);
            lbl_due_in.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+100,0, 80, 45);
            due_in_val .frame = CGRectMake(CGRectGetMaxX(lbl_due_in.frame)-35,13,180,20);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+160,CGRectGetMaxY(due_in_val.frame)+24,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_received_date.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            icon_massage.frame = CGRectMake(CGRectGetMaxX(img_user.frame)+248,CGRectGetMaxY(img_line.frame)+25,25,20);
            
            lbl_due_in.font = [UIFont fontWithName:kFont size:13];
            due_in_val .font = [UIFont fontWithName:kFontBold size:14];
            
            
        }
        
        
    }
    else if (tableView == tabl_for_active)
    {
        UIImageView *  img_bg_for_order_no = [[UIImageView alloc]init];
        img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
        img_bg_for_order_no.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_order_no.backgroundColor = [UIColor whiteColor];
        [img_bg_for_order_no setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_order_no];
        
        UILabel *lbl_order_number = [[UILabel alloc]init ];
        lbl_order_number.frame = CGRectMake(10,0, 100, 45);
        lbl_order_number.text = @"Order no.:";
        lbl_order_number.font = [UIFont fontWithName:kFont size:18];
        lbl_order_number.textColor = [UIColor blackColor];
        lbl_order_number.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_order_number];
        
        // [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1
        
        UILabel * lbl_order_number_val = [[UILabel alloc]init];
        lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
        // lbl_order_number_val .text = [NSString stringWithFormat:@"%@",[array_order_no objectAtIndex:indexPath.row]];
        lbl_order_number_val .font = [UIFont fontWithName:kFontBold size:15];
        lbl_order_number_val .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        lbl_order_number_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_order_number_val ];
        
        UILabel *lbl_value = [[UILabel alloc]init ];
        lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
        lbl_value.text = @"Value:";
        lbl_value.font = [UIFont fontWithName:kFont size:15];
        lbl_value.textColor = [UIColor blackColor];
        lbl_value.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_value];
        
        UILabel * value = [[UILabel alloc]init];
        value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
        // value .text = [NSString stringWithFormat:@"%@",[array_value objectAtIndex:indexPath.row]];
        value .font = [UIFont fontWithName:kFontBold size:15];
        value .textColor = [UIColor blackColor];
        value .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:value ];
        
        UILabel *lbl_received_date = [[UILabel alloc]init ];
        lbl_received_date.frame = CGRectMake(40,CGRectGetMidY(lbl_value.frame), 80, 45);
        lbl_received_date.text = @"Received:";
        lbl_received_date.font = [UIFont fontWithName:kFont size:12];
        lbl_received_date.textColor = [UIColor blackColor];
        lbl_received_date.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_received_date];
        
        UILabel * received_date_val = [[UILabel alloc]init];
        received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_received_date.frame)-17,CGRectGetMaxY(value.frame),180,20);
        //  received_date_val .text = [NSString stringWithFormat:@"%@",[array_recived_date objectAtIndex:indexPath.row]];
        received_date_val .font = [UIFont fontWithName:kFontBold size:12];
        received_date_val .textColor = [UIColor blackColor];
        received_date_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:received_date_val];
        
        UILabel *lbl_due_in = [[UILabel alloc]init ];
        lbl_due_in.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+140,0, 80, 45);
        lbl_due_in.text = @"Due in";
        lbl_due_in.font = [UIFont fontWithName:kFont size:15];
        lbl_due_in.textColor = [UIColor blackColor];
        lbl_due_in.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_due_in];
        
        UILabel *   due_in_val = [[UILabel alloc]init];
        due_in_val .frame = CGRectMake(CGRectGetMaxX(lbl_due_in.frame)-30,13,180,20);
        // due_in_val .text = [NSString stringWithFormat:@"%@",[array_due_time objectAtIndex:indexPath.row]];
        due_in_val .font = [UIFont fontWithName:kFontBold size:15];
        due_in_val .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        due_in_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:due_in_val];
        
        UIImageView * icon_serving_type = [[UIImageView alloc]init];
        icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+210,CGRectGetMaxY(due_in_val.frame)+24,25,25);
        // [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_seving_type objectAtIndex:indexPath.row]]]];
        icon_serving_type .backgroundColor = [UIColor clearColor];
        [icon_serving_type  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:icon_serving_type];
        
        
        UIImageView *img_line = [[UIImageView alloc]init];
        img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_received_date.frame)-4,WIDTH-40,0.5);
        [img_line  setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
        img_line .backgroundColor = [UIColor clearColor];
        [img_line  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:img_line];
        
        UIImageView *  img_user = [[UIImageView alloc]init];
        img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
        // [img_user  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_user_img objectAtIndex:indexPath.row]]]];
        img_user .backgroundColor = [UIColor clearColor];
        [img_user  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:img_user];
        
        UILabel * user_name = [[UILabel alloc]init];
        user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
        // user_name .text = [NSString stringWithFormat:@"%@",[array_user_name objectAtIndex:indexPath.row]];
        user_name .font = [UIFont fontWithName:kFontBold size:15];
        user_name .textColor = [UIColor blackColor];
        user_name .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:user_name];
        
        
        
        lbl_order_number_val .text = [NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"orderno"]];
        value .text = [NSString stringWithFormat:@"$ %@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"value"]];
        received_date_val .text = [NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"recieved"]];
        
        if ([[[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"duein"] intValue]<=0)
        {
            due_in_val .text = [NSString stringWithFormat:@"%@",@"Past"];
            
        }
        else{
            due_in_val .text = [NSString stringWithFormat:@"%@ min",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"duein"]];
            
        }
        
        NSString *url_Img = [NSString stringWithFormat: @"%@", [[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"userimage"]];
        img_user.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img]]];
        
        //[icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_seving_type objectAtIndex:indexPath.row]]]];
        //[img_user  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_user_img objectAtIndex:indexPath.row]]]];
        
        user_name .text = [NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"username"]];
        if ([[NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"serve_type"]]isEqualToString:@"dinein"])
        {
            [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"img-dinin@2x.png"]]];
            
        }
        else if ([[NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"serve_type"]]isEqualToString:@"takeout"])
        {
            [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"take-icon@2x.png"]]];
            
        }
        else{
            [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"deliver-icon@2x.png"]]];
            
        }
        
        
        UIButton *icon_massage = [UIButton buttonWithType:UIButtonTypeCustom];
        icon_massage.frame = CGRectMake(CGRectGetMaxX(img_user.frame)+248,CGRectGetMaxY(img_line.frame)+25,25,20);
        icon_massage.tag = indexPath.row;
        icon_massage .backgroundColor = [UIColor clearColor];
        [icon_massage addTarget:self action:@selector(click_on_message_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_massage setImage:[UIImage imageNamed:@"icon-msg@2x.png"] forState:UIControlStateNormal];
        [img_bg_for_order_no   addSubview:icon_massage];
        
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
            lbl_order_number.frame = CGRectMake(10,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
            lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
            value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
            lbl_received_date.frame = CGRectMake(40,CGRectGetMidY(lbl_value.frame), 80, 45);
            received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_received_date.frame)-17,CGRectGetMaxY(value.frame),180,20);
            lbl_due_in.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+175,0, 80, 45);
            due_in_val .frame = CGRectMake(CGRectGetMaxX(lbl_due_in.frame)-30,13,180,20);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+245,CGRectGetMaxY(due_in_val.frame)+24,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_received_date.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            icon_massage.frame = CGRectMake(CGRectGetMaxX(img_user.frame)+283,CGRectGetMaxY(img_line.frame)+25,25,20);
            
        }
        else if (IS_IPHONE_6)
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
            lbl_order_number.frame = CGRectMake(10,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
            lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
            value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
            lbl_received_date.frame = CGRectMake(40,CGRectGetMidY(lbl_value.frame), 80, 45);
            received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_received_date.frame)-17,CGRectGetMaxY(value.frame),180,20);
            lbl_due_in.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+140,0, 80, 45);
            due_in_val .frame = CGRectMake(CGRectGetMaxX(lbl_due_in.frame)-30,13,180,20);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+210,CGRectGetMaxY(due_in_val.frame)+24,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_received_date.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            icon_massage.frame = CGRectMake(CGRectGetMaxX(img_user.frame)+248,CGRectGetMaxY(img_line.frame)+25,25,20);
            
        }
        else
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
            lbl_order_number.frame = CGRectMake(10,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
            lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
            value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
            lbl_received_date.frame = CGRectMake(40,CGRectGetMidY(lbl_value.frame), 80, 45);
            received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_received_date.frame)-17,CGRectGetMaxY(value.frame),180,20);
            lbl_due_in.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+90,0, 80, 45);
            due_in_val .frame = CGRectMake(CGRectGetMaxX(lbl_due_in.frame)-30,13,180,20);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+160,CGRectGetMaxY(due_in_val.frame)+24,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_received_date.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            icon_massage.frame = CGRectMake(CGRectGetMaxX(img_user.frame)+193,CGRectGetMaxY(img_line.frame)+25,25,20);
            
            lbl_due_in.font = [UIFont fontWithName:kFont size:13];
            due_in_val .font = [UIFont fontWithName:kFontBold size:13];
            
        }
        
        
        
    }
    else if (tableView == tabl_for_cancelled)
    {
        UIImageView *  img_bg_for_order_no = [[UIImageView alloc]init];
        img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 250);
        img_bg_for_order_no.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_order_no.backgroundColor = [UIColor whiteColor];
        [img_bg_for_order_no setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_order_no];
        
        UILabel *lbl_order_number = [[UILabel alloc]init ];
        lbl_order_number.frame = CGRectMake(10,0, 100, 45);
        lbl_order_number.text = @"Order no.:";
        lbl_order_number.font = [UIFont fontWithName:kFont size:18];
        lbl_order_number.textColor = [UIColor blackColor];
        lbl_order_number.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_order_number];
        
        // [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1
        
        UILabel * lbl_order_number_val = [[UILabel alloc]init];
        lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
        lbl_order_number_val .text = [NSString stringWithFormat:@"%@",[array_order_no objectAtIndex:indexPath.row]];
        lbl_order_number_val .font = [UIFont fontWithName:kFontBold size:15];
        lbl_order_number_val .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        lbl_order_number_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_order_number_val ];
        
        UILabel *lbl_cancelled = [[UILabel alloc]init ];
        lbl_cancelled.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+160,0,250, 45);
        lbl_cancelled.text = @"Cancelled";
        lbl_cancelled.font = [UIFont fontWithName:kFont size:15];
        lbl_cancelled.textColor = [UIColor blackColor];
        lbl_cancelled.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_cancelled];
        
        UILabel *  lbl_cancelled_val = [[UILabel alloc]init];
        lbl_cancelled_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+150,CGRectGetMaxY(lbl_cancelled.frame)-10,150,25);
        lbl_cancelled_val .text = [NSString stringWithFormat:@"%@",[array_cancelled_time objectAtIndex:indexPath.row]];
        lbl_cancelled_val .font = [UIFont fontWithName:kFontBold size:18];
        lbl_cancelled_val .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        lbl_cancelled_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_cancelled_val ];
        
        
        UILabel *lbl_scheduled_date_time = [[UILabel alloc]init ];
        lbl_scheduled_date_time.frame = CGRectMake(10,CGRectGetMidY(lbl_order_number.frame)+30,250, 45);
        lbl_scheduled_date_time.text = @"Scheduled Serving Date/Time:";
        lbl_scheduled_date_time.font = [UIFont fontWithName:kFont size:13];
        lbl_scheduled_date_time.textColor = [UIColor blackColor];
        lbl_scheduled_date_time.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_scheduled_date_time];
        
        
        UILabel * scheduled_date_val = [[UILabel alloc]init];
        scheduled_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_scheduled_date_time.frame)-55,CGRectGetMaxY(lbl_order_number_val.frame)+33,180,20);
        scheduled_date_val .text = [NSString stringWithFormat:@"%@",[array_schedule_date_time objectAtIndex:indexPath.row]];
        scheduled_date_val .font = [UIFont fontWithName:kFontBold size:13];
        scheduled_date_val .textColor = [UIColor blackColor];
        scheduled_date_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:scheduled_date_val];
        
        
        UILabel *lbl_cancelled_date_time = [[UILabel alloc]init ];
        lbl_cancelled_date_time.frame = CGRectMake(10,CGRectGetMidY(lbl_scheduled_date_time.frame),250, 45);
        lbl_cancelled_date_time.text = @"Cancelled Date/Time:";
        lbl_cancelled_date_time.font = [UIFont fontWithName:kFont size:13];
        lbl_cancelled_date_time.textColor = [UIColor blackColor];
        lbl_cancelled_date_time.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_cancelled_date_time];
        
        UILabel *  cancelled_date_val = [[UILabel alloc]init];
        cancelled_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_scheduled_date_time.frame)-110,CGRectGetMaxY(scheduled_date_val.frame)+3,180,20);
        cancelled_date_val .text = [NSString stringWithFormat:@"%@",[array_cancelled_date_time objectAtIndex:indexPath.row]];
        cancelled_date_val .font = [UIFont fontWithName:kFontBold size:13];
        cancelled_date_val .textColor = [UIColor blackColor];
        cancelled_date_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:cancelled_date_val];
        
        UILabel *lbl_cancelled_by = [[UILabel alloc]init ];
        lbl_cancelled_by.frame = CGRectMake(10,CGRectGetMidY(lbl_cancelled_date_time.frame),100, 45);
        lbl_cancelled_by.text = @"Cancelled By:";
        lbl_cancelled_by.font = [UIFont fontWithName:kFont size:13];
        lbl_cancelled_by.textColor = [UIColor blackColor];
        lbl_cancelled_by.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_cancelled_by];
        
        UILabel *  cancelled_by_val = [[UILabel alloc]init];
        cancelled_by_val .frame = CGRectMake(CGRectGetMaxX(lbl_cancelled_by.frame)-8,CGRectGetMaxY(cancelled_date_val.frame)+1,180,20);
        cancelled_by_val .text = [NSString stringWithFormat:@"%@",[array_cancellled_by objectAtIndex:indexPath.row]];
        cancelled_by_val .font = [UIFont fontWithName:kFontBold size:13];
        cancelled_by_val .textColor = [UIColor blackColor];
        cancelled_by_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:cancelled_by_val];
        
        UILabel *lbl_refund = [[UILabel alloc]init ];
        lbl_refund.frame = CGRectMake(11,CGRectGetMidY(lbl_cancelled_by.frame),100, 45);
        lbl_refund.text = @"Refund:";
        lbl_refund.font = [UIFont fontWithName:kFont size:13];
        lbl_refund.textColor = [UIColor blackColor];
        lbl_refund.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_refund];
        
        UILabel *  refund_val = [[UILabel alloc]init];
        refund_val .frame = CGRectMake(CGRectGetMaxX(lbl_refund.frame)-50,CGRectGetMaxY(cancelled_by_val.frame)+3,180,20);
        refund_val .text = [NSString stringWithFormat:@"%@",[array_refund objectAtIndex:indexPath.row]];
        refund_val .font = [UIFont fontWithName:kFontBold size:13];
        refund_val .textColor = [UIColor blackColor];
        refund_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:refund_val];
        
        UILabel *lbl_refund_amount = [[UILabel alloc]init ];
        lbl_refund_amount.frame = CGRectMake(11,CGRectGetMidY(lbl_refund.frame),100, 45);
        lbl_refund_amount.text = @"Refund Amount";
        lbl_refund_amount.font = [UIFont fontWithName:kFont size:13];
        lbl_refund_amount.textColor = [UIColor blackColor];
        lbl_refund_amount.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_refund_amount];
        
        UIImageView * icon_serving_type = [[UIImageView alloc]init];
        icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_refund.frame)+210,CGRectGetMaxY(lbl_cancelled.frame)+24,25,25);
        [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_seving_type objectAtIndex:indexPath.row]]]];
        icon_serving_type .backgroundColor = [UIColor clearColor];
        [icon_serving_type  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:icon_serving_type];
        
        
        UIImageView *img_line = [[UIImageView alloc]init];
        img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_refund.frame)-4,WIDTH-40,0.5);
        [img_line  setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
        img_line .backgroundColor = [UIColor clearColor];
        [img_line  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:img_line];
        
        UIImageView *  img_user = [[UIImageView alloc]init];
        img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
        [img_user  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_user_img objectAtIndex:indexPath.row]]]];
        img_user .backgroundColor = [UIColor clearColor];
        [img_user  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:img_user];
        
        UILabel * user_name = [[UILabel alloc]init];
        user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
        user_name .text = [NSString stringWithFormat:@"%@",[array_user_name objectAtIndex:indexPath.row]];
        user_name .font = [UIFont fontWithName:kFontBold size:15];
        user_name .textColor = [UIColor blackColor];
        user_name .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:user_name];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 270);
            lbl_order_number.frame = CGRectMake(25,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
            lbl_cancelled.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+160,0,250, 45);
            lbl_cancelled_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+150,CGRectGetMaxY(lbl_cancelled.frame)-10,150,25);
            lbl_scheduled_date_time.frame = CGRectMake(15,CGRectGetMidY(lbl_order_number.frame)+30,250, 45);
            scheduled_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_scheduled_date_time.frame)-55,CGRectGetMaxY(lbl_order_number_val.frame)+33,180,20);
            lbl_cancelled_date_time.frame = CGRectMake(15,CGRectGetMidY(lbl_scheduled_date_time.frame),250, 45);
            cancelled_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_scheduled_date_time.frame)-110,CGRectGetMaxY(scheduled_date_val.frame)+3,180,20);
            lbl_cancelled_by.frame = CGRectMake(15,CGRectGetMidY(lbl_cancelled_date_time.frame),100, 45);
            cancelled_by_val .frame = CGRectMake(CGRectGetMaxX(lbl_cancelled_by.frame)-8,CGRectGetMaxY(cancelled_date_val.frame)+1,180,20);
            lbl_refund.frame = CGRectMake(15,CGRectGetMidY(lbl_cancelled_by.frame),100, 45);
            refund_val .frame = CGRectMake(CGRectGetMaxX(lbl_refund.frame)-50,CGRectGetMaxY(cancelled_by_val.frame)+3,180,20);
            lbl_refund_amount.frame = CGRectMake(15,CGRectGetMidY(lbl_refund.frame),100, 45);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_refund.frame)+240,CGRectGetMaxY(lbl_cancelled.frame)+110,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_refund_amount.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(15,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            
            
        }
        else if (IS_IPHONE_6)
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 250);
            lbl_order_number.frame = CGRectMake(10,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
            lbl_cancelled.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+160,0,250, 45);
            lbl_cancelled_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+150,CGRectGetMaxY(lbl_cancelled.frame)-10,150,25);
            lbl_scheduled_date_time.frame = CGRectMake(10,CGRectGetMidY(lbl_order_number.frame)+30,250, 45);
            scheduled_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_scheduled_date_time.frame)-55,CGRectGetMaxY(lbl_order_number_val.frame)+33,180,20);
            lbl_cancelled_date_time.frame = CGRectMake(10,CGRectGetMidY(lbl_scheduled_date_time.frame),250, 45);
            cancelled_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_scheduled_date_time.frame)-110,CGRectGetMaxY(scheduled_date_val.frame)+3,180,20);
            lbl_cancelled_by.frame = CGRectMake(10,CGRectGetMidY(lbl_cancelled_date_time.frame),100, 45);
            cancelled_by_val .frame = CGRectMake(CGRectGetMaxX(lbl_cancelled_by.frame)-8,CGRectGetMaxY(cancelled_date_val.frame)+1,180,20);
            lbl_refund.frame = CGRectMake(11,CGRectGetMidY(lbl_cancelled_by.frame),100, 45);
            refund_val .frame = CGRectMake(CGRectGetMaxX(lbl_refund.frame)-50,CGRectGetMaxY(cancelled_by_val.frame)+3,180,20);
            lbl_refund_amount.frame = CGRectMake(11,CGRectGetMidY(lbl_refund.frame),100, 45);
            
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_refund.frame)+260,CGRectGetMaxY(lbl_cancelled.frame)+70,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_refund_amount.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            
            
        }
        else
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 270);
            lbl_order_number.frame = CGRectMake(10,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
            lbl_cancelled.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+110,0,250, 45);
            lbl_cancelled_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+100,CGRectGetMaxY(lbl_cancelled.frame)-15,150,25);
            lbl_scheduled_date_time.frame = CGRectMake(10,CGRectGetMidY(lbl_order_number.frame)+30,250, 45);
            scheduled_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_scheduled_date_time.frame)-86,CGRectGetMaxY(lbl_order_number_val.frame)+33,180,20);
            lbl_cancelled_date_time.frame = CGRectMake(10,CGRectGetMidY(lbl_scheduled_date_time.frame),250, 45);
            cancelled_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_scheduled_date_time.frame)-130,CGRectGetMaxY(scheduled_date_val.frame)+3,180,20);
            lbl_cancelled_by.frame = CGRectMake(10,CGRectGetMidY(lbl_cancelled_date_time.frame),100, 45);
            cancelled_by_val .frame = CGRectMake(CGRectGetMaxX(lbl_cancelled_by.frame)-22,CGRectGetMaxY(cancelled_date_val.frame)+1,180,20);
            lbl_refund.frame = CGRectMake(11,CGRectGetMidY(lbl_cancelled_by.frame),100, 45);
            refund_val .frame = CGRectMake(CGRectGetMaxX(lbl_refund.frame)-58,CGRectGetMaxY(cancelled_by_val.frame)+3,180,20);
            lbl_refund_amount.frame = CGRectMake(11,CGRectGetMidY(lbl_refund.frame),100, 45);
            
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_refund.frame)+155,CGRectGetMaxY(lbl_cancelled.frame)+110,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_refund_amount.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            
            lbl_cancelled.font = [UIFont fontWithName:kFont size:12];
            lbl_cancelled_val .font = [UIFont fontWithName:kFontBold size:15];
            lbl_scheduled_date_time.font = [UIFont fontWithName:kFont size:11];
            scheduled_date_val .font = [UIFont fontWithName:kFontBold size:11];
            lbl_cancelled_date_time.font = [UIFont fontWithName:kFont size:11];
            cancelled_date_val .font = [UIFont fontWithName:kFontBold size:11];
            lbl_cancelled_by.font = [UIFont fontWithName:kFont size:11];
            cancelled_by_val .font = [UIFont fontWithName:kFontBold size:11];
            lbl_refund.font = [UIFont fontWithName:kFont size:11];
            refund_val .font = [UIFont fontWithName:kFontBold size:11];
            lbl_refund_amount.font = [UIFont fontWithName:kFont size:11];
            user_name .font = [UIFont fontWithName:kFontBold size:12];
            
            
            
        }
        
        
        
        
    }
    else if (tableView == tabl_for_served)
    {
        
        UIImageView * img_bg_for_order_no = [[UIImageView alloc]init];
        img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
        img_bg_for_order_no.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_order_no.backgroundColor = [UIColor whiteColor];
        [img_bg_for_order_no setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_order_no];
        
        UILabel *lbl_order_number = [[UILabel alloc]init ];
        lbl_order_number.frame = CGRectMake(10,0, 100, 45);
        lbl_order_number.text = @"Order no.:";
        lbl_order_number.font = [UIFont fontWithName:kFont size:18];
        lbl_order_number.textColor = [UIColor blackColor];
        lbl_order_number.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_order_number];
        
        // [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1
        
        UILabel * lbl_order_number_val = [[UILabel alloc]init];
        lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
        //lbl_order_number_val .text = [NSString stringWithFormat:@"%@",[array_order_no objectAtIndex:indexPath.row]];
        lbl_order_number_val .font = [UIFont fontWithName:kFontBold size:15];
        lbl_order_number_val .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        lbl_order_number_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_order_number_val ];
        
        UILabel *lbl_value = [[UILabel alloc]init ];
        lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
        lbl_value.text = @"Value:";
        lbl_value.font = [UIFont fontWithName:kFont size:15];
        lbl_value.textColor = [UIColor blackColor];
        lbl_value.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_value];
        
        UILabel * value = [[UILabel alloc]init];
        value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
        // value .text = [NSString stringWithFormat:@"%@",[array_value objectAtIndex:indexPath.row]];
        value .font = [UIFont fontWithName:kFontBold size:15];
        value .textColor = [UIColor blackColor];
        value .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:value ];
        
        UILabel *lbl_serving_date = [[UILabel alloc]init ];
        lbl_serving_date.frame = CGRectMake(40,CGRectGetMidY(lbl_value.frame), 150, 45);
        lbl_serving_date.text = @"Serving Date/Time:";
        lbl_serving_date.font = [UIFont fontWithName:kFont size:12];
        lbl_serving_date.textColor = [UIColor blackColor];
        lbl_serving_date.backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:lbl_serving_date];
        
        UILabel * received_date_val = [[UILabel alloc]init];
        received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_serving_date.frame)-35,CGRectGetMaxY(value.frame),180,20);
        // received_date_val .text = [NSString stringWithFormat:@"%@",[array_recived_date objectAtIndex:indexPath.row]];
        received_date_val .font = [UIFont fontWithName:kFontBold size:12];
        received_date_val .textColor = [UIColor blackColor];
        received_date_val .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:received_date_val];
        
        //         UILabel *lbl_due_in = [[UILabel alloc]init ];
        //         lbl_due_in.frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+140,0, 80, 45);
        //         lbl_due_in.text = @"Due in";
        //         lbl_due_in.font = [UIFont fontWithName:kFont size:15];
        //         lbl_due_in.textColor = [UIColor blackColor];
        //         lbl_due_in.backgroundColor = [UIColor clearColor];
        //         [img_bg_for_order_no addSubview:lbl_due_in];
        //
        //         due_in_val = [[UILabel alloc]init];
        //         due_in_val .frame = CGRectMake(CGRectGetMaxX(lbl_due_in.frame)-30,13,180,20);
        //         due_in_val .text = [NSString stringWithFormat:@"%@",[array_due_time objectAtIndex:indexPath.row]];
        //         due_in_val .font = [UIFont fontWithName:kFontBold size:15];
        //         due_in_val .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        //         due_in_val .backgroundColor = [UIColor clearColor];
        //         [img_bg_for_order_no addSubview:due_in_val];
        //
        UIImageView *  icon_serving_type = [[UIImageView alloc]init];
        icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+210,55,25,25);
        // [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_seving_type objectAtIndex:indexPath.row]]]];
        icon_serving_type .backgroundColor = [UIColor clearColor];
        [icon_serving_type  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:icon_serving_type];
        
        
        UIImageView *img_line = [[UIImageView alloc]init];
        img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_serving_date.frame)-4,WIDTH-40,0.5);
        [img_line  setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
        img_line .backgroundColor = [UIColor clearColor];
        [img_line  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:img_line];
        
        UIImageView *  img_user = [[UIImageView alloc]init];
        img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
        // [img_user  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_user_img objectAtIndex:indexPath.row]]]];
        img_user .backgroundColor = [UIColor clearColor];
        [img_user  setUserInteractionEnabled:YES];
        [img_bg_for_order_no addSubview:img_user];
        
        UILabel * user_name = [[UILabel alloc]init];
        user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
        //user_name .text = [NSString stringWithFormat:@"%@",[array_user_name objectAtIndex:indexPath.row]];
        user_name .font = [UIFont fontWithName:kFontBold size:15];
        user_name .textColor = [UIColor blackColor];
        user_name .backgroundColor = [UIColor clearColor];
        [img_bg_for_order_no addSubview:user_name];
        
        
        lbl_order_number_val .text = [NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"orderno"]];
        value .text = [NSString stringWithFormat:@"$ %@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"value"]];
        received_date_val .text = [NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"recieved"]];
        
        //         if ([[[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"duein"] intValue]<=0)
        //         {
        //             due_in_val .text = [NSString stringWithFormat:@"%@",@"Past"];
        //
        //         }
        //         else{
        //             due_in_val .text = [NSString stringWithFormat:@"%@ min",[[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"duein"]];
        //
        //         }
        
        NSString *url_Img = [NSString stringWithFormat: @"%@", [[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"userimage"]];
        img_user.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img]]];
        
        //[icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_seving_type objectAtIndex:indexPath.row]]]];
        //[img_user  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_user_img objectAtIndex:indexPath.row]]]];
        if ([[NSString stringWithFormat:@"%@",[[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"serve_type"]]isEqualToString:@"dinein"])
        {
            [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"img-dinin@2x.png"]]];
            
        }
        else if ([[NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"serve_type"]]isEqualToString:@"takeout"])
        {
            [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"take-icon@2x.png"]]];
            
        }
        else{
            [icon_serving_type  setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"deliver-icon@2x.png"]]];
            
        }
        user_name .text = [NSString stringWithFormat:@"%@",[[ary_dishListfilter objectAtIndex:indexPath.row] valueForKey:@"username"]];
        
        
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
            lbl_order_number.frame = CGRectMake(25,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
            lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
            value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
            lbl_serving_date.frame = CGRectMake(20,CGRectGetMidY(lbl_value.frame), 150, 45);
            received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_serving_date.frame)-35,CGRectGetMaxY(value.frame),180,20);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+230,55,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_serving_date.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            
        }
        else if (IS_IPHONE_6)
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
            lbl_order_number.frame = CGRectMake(10,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
            lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
            value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
            lbl_serving_date.frame = CGRectMake(40,CGRectGetMidY(lbl_value.frame), 150, 45);
            received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_serving_date.frame)-35,CGRectGetMaxY(value.frame),180,20);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+210,55,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_serving_date.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            
        }
        else
        {
            img_bg_for_order_no.frame =  CGRectMake(-4,0,WIDTH, 170);
            lbl_order_number.frame = CGRectMake(20,0, 100, 45);
            lbl_order_number_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-5,13,100,20);
            lbl_value.frame = CGRectMake(52,CGRectGetMidY(lbl_order_number.frame),70, 45);
            value .frame = CGRectMake(CGRectGetMaxX(lbl_value.frame)-17,CGRectGetMaxY(lbl_value.frame)-30,100,20);
            lbl_serving_date.frame = CGRectMake(15,CGRectGetMidY(lbl_value.frame), 150, 45);
            received_date_val .frame = CGRectMake(CGRectGetMaxX(lbl_serving_date.frame)-35,CGRectGetMaxY(value.frame),180,20);
            icon_serving_type .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)+150,55,25,25);
            img_line .frame = CGRectMake(15,CGRectGetMaxY(lbl_serving_date.frame)-4,WIDTH-40,0.5);
            img_user .frame = CGRectMake(25,CGRectGetMaxY(img_line.frame)+13,50,50);
            user_name .frame = CGRectMake(CGRectGetMaxX(img_user.frame)+15,CGRectGetMaxY(img_line.frame)+25,180,20);
            
        }
        
        
    }
    return cell;
    
}
#pragma table view

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tabl_for_today_orders setHidden:YES];
    lbl_today.text = [array_head_names objectAtIndex:indexPath.row];
    
    //    lbl_food_now.text =[array_head_names objectAtIndex:indexPath.row];
    //    [table_on_drop_down setHidden:YES];
    
    
    if (indexPath.row == 0)
    {
        str_sortby = @"0";
        str_day = @"0";
        
        view_for_todays.hidden = NO;
        view_for_on_request.hidden = YES;
        view_for_past_request.hidden = YES;
        view_for_tomorrow.hidden = YES;
        view_for_active.hidden = YES;
        view_for_cancelled.hidden = YES;
        view_for_served.hidden = YES;
        
        lbl_quantity_available.hidden = NO;
        img_line_under_quantity.hidden = NO;
        btn_on_quantity_available.hidden = NO;
        
        
        [self AFUserDishLists];
        
        
        
        
        
    }
    else if (indexPath.row == 1)
    {
        str_day = @"1";
        str_sortby = @"0";
        
        view_for_todays.hidden = YES;
        view_for_on_request.hidden = NO;
        view_for_past_request.hidden = YES;
        view_for_tomorrow.hidden = YES;
        view_for_active.hidden =YES;
        view_for_cancelled.hidden = YES;
        view_for_served.hidden = YES;
        
        lbl_quantity_available.hidden = YES;
        img_line_under_quantity.hidden = YES;
        btn_on_quantity_available.hidden = YES;
        
        [self AFUserDishLists];
        
    }
    else if (indexPath.row == 2)
    {
        str_day = @"2";
        str_sortby = @"0";
        
        view_for_todays.hidden = YES;
        view_for_on_request.hidden = YES;
        view_for_past_request.hidden = NO;
        view_for_tomorrow.hidden = YES;
        view_for_active.hidden = YES;
        view_for_cancelled.hidden = YES;
        view_for_served.hidden = YES;
        
        lbl_quantity_available.hidden = YES;
        img_line_under_quantity.hidden = YES;
        btn_on_quantity_available.hidden = YES;
        
        [self AFUserDishLists];
        
    }
    else if (indexPath.row == 3)
    {
        str_day = @"3";
        str_sortby = @"0";
        
        view_for_todays.hidden = YES;
        view_for_on_request.hidden = YES;
        view_for_past_request.hidden = YES;
        view_for_tomorrow.hidden = NO;
        view_for_active.hidden = YES;
        view_for_cancelled.hidden = YES;
        view_for_served.hidden = YES;
        
        lbl_quantity_available.hidden = NO;
        img_line_under_quantity.hidden = NO;
        btn_on_quantity_available.hidden = NO;
        [self AFUserDishLists];
    }
    else if (indexPath.row == 4)
    {
        str_day = @"4";
        str_sortby = @"0";
        
        view_for_todays.hidden = YES;
        view_for_on_request.hidden = YES;
        view_for_past_request.hidden = YES;
        view_for_tomorrow.hidden = YES;
        view_for_active.hidden = NO;
        view_for_cancelled.hidden = YES;
        view_for_served.hidden = YES;
        
        lbl_quantity_available.hidden = NO;
        img_line_under_quantity.hidden = NO;
        btn_on_quantity_available.hidden = NO;
        
        [self AFUserDishLists];
    }
    else if (indexPath.row == 5)
    {
        
        str_day = @"5";
        str_sortby = @"0";
        
        view_for_todays.hidden = YES;
        view_for_on_request.hidden = YES;
        view_for_past_request.hidden = YES;
        view_for_tomorrow.hidden = YES;
        view_for_active.hidden = YES;
        view_for_cancelled.hidden = NO;
        view_for_served.hidden = YES;
        
        lbl_quantity_available.hidden = YES;
        img_line_under_quantity.hidden = YES;
        btn_on_quantity_available.hidden = YES;
        
        [self AFUserDishLists];
        
    }
    else if (indexPath.row == 6)
    {
        str_day = @"6";
        str_sortby = @"0";
        
        view_for_todays.hidden = YES;
        view_for_on_request.hidden = YES;
        view_for_past_request.hidden = YES;
        view_for_tomorrow.hidden = YES;
        view_for_active.hidden = YES;
        view_for_cancelled.hidden = YES;
        view_for_served.hidden = NO;
        
        lbl_quantity_available.hidden = YES;
        img_line_under_quantity.hidden = YES;
        btn_on_quantity_available.hidden = YES;
        
        [self AFUserDishLists];
    }
    
    
    
    
    
    
}
#pragma mark popup_traceTaxi

-(void)popup_cancel_oeder
{
    [view_for_popup removeFromSuperview];
    view_for_popup=[[UIView alloc] init];
    view_for_popup.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.7];
    view_for_popup.userInteractionEnabled=TRUE;
    view_for_popup.userInteractionEnabled = YES;
    [self.view addSubview:view_for_popup];
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    [alertViewBody setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    alertViewBody.backgroundColor=[UIColor whiteColor];
    alertViewBody.userInteractionEnabled = YES;
    [view_for_popup addSubview:alertViewBody];
    
    UILabel *lbl_dish_name = [[UILabel alloc]init ];
    lbl_dish_name.frame = CGRectMake(110,02, 300, 45);
    lbl_dish_name.text = @"Spicy Fish Otah";
    lbl_dish_name.font = [UIFont fontWithName:kFontBold size:17];
    lbl_dish_name.textColor = [UIColor blackColor];
    lbl_dish_name.backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:lbl_dish_name];
    
    UIButton *icon_cross =[UIButton buttonWithType:UIButtonTypeCustom];
    icon_cross.frame=CGRectMake(CGRectGetMidX(lbl_dish_name.frame)-15,17, 15,15);
    icon_cross.backgroundColor = [UIColor clearColor];
    [icon_cross addTarget:self action:@selector(click_popup_x_btn:) forControlEvents:UIControlEventTouchUpInside];
    icon_cross.userInteractionEnabled = YES;
    [icon_cross setImage:[UIImage imageNamed:@"cross-img@2x.png"] forState:UIControlStateNormal];
    [alertViewBody  addSubview:icon_cross];
    
    UIButton *icon_serving_type =[UIButton buttonWithType:UIButtonTypeCustom];
    icon_serving_type.frame=CGRectMake(30,CGRectGetMaxY(lbl_dish_name.frame), 15,15);
    icon_serving_type.backgroundColor = [UIColor clearColor];
    // [icon_serving_type addTarget:self action:@selector(click_popup_x_btn:) forControlEvents:UIControlEventTouchUpInside];
    icon_serving_type.userInteractionEnabled = YES;
    [icon_serving_type setImage:[UIImage imageNamed:@"deliver-icon@2x.png"]forState:UIControlStateNormal];
    [alertViewBody  addSubview:icon_serving_type];
    
    UILabel *lbl_serving_quantity = [[UILabel alloc]init ];
    lbl_serving_quantity.frame = CGRectMake(110,02, 300, 45);
    lbl_serving_quantity.text = @"Serving Quantity Left:";
    lbl_serving_quantity.font = [UIFont fontWithName:kFont size:14];
    lbl_serving_quantity.textColor = [UIColor blackColor];
    lbl_serving_quantity.backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:lbl_serving_quantity];
    
    UILabel * lbl_serving_quantity_val = [[UILabel alloc]init];
    lbl_serving_quantity_val .frame = CGRectMake(CGRectGetMaxX(lbl_serving_quantity.frame),13,100,20);
    lbl_serving_quantity_val .text = @"2";
    lbl_serving_quantity_val .font = [UIFont fontWithName:kFontBold size:22];
    lbl_serving_quantity_val .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    lbl_serving_quantity_val .backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:lbl_serving_quantity_val ];
    
    UILabel *lbl_serving_date = [[UILabel alloc]init ];
    lbl_serving_date.frame = CGRectMake(110,02, 300, 45);
    lbl_serving_date.text = @"Serving Date/Time:";
    lbl_serving_date.font = [UIFont fontWithName:kFont size:14];
    lbl_serving_date.textColor = [UIColor blackColor];
    lbl_serving_date.backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:lbl_serving_date];
    
    UILabel *lbl_serving_date_val = [[UILabel alloc]init ];
    lbl_serving_date_val.frame = CGRectMake(110,02, 300, 45);
    lbl_serving_date_val.text = @"16/07/2015, 4:00:PM";
    lbl_serving_date_val.font = [UIFont fontWithName:kFontBold size:14];
    lbl_serving_date_val.textColor = [UIColor blackColor];
    lbl_serving_date_val.backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:lbl_serving_date_val];
    
    UIImageView *img_line = [[UIImageView alloc]init];
    img_line .frame = CGRectMake(20,CGRectGetMaxY(lbl_serving_date.frame)-5,WIDTH-50,0.5);
    [img_line  setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
    img_line .backgroundColor = [UIColor clearColor];
    [img_line  setUserInteractionEnabled:YES];
    [alertViewBody addSubview:img_line];
    
    UILabel *lbl_total_servings = [[UILabel alloc]init ];
    lbl_total_servings.frame = CGRectMake(110,02, 300, 45);
    lbl_total_servings.text = @"Current Total Servings";
    lbl_total_servings.font = [UIFont fontWithName:kFont size:16];
    lbl_total_servings.textColor = [UIColor blackColor];
    lbl_total_servings.backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:lbl_total_servings];
    
    UILabel *lbl_total_servings_val = [[UILabel alloc]init ];
    lbl_total_servings_val.frame = CGRectMake(110,02, 300, 45);
    lbl_total_servings_val.text = @"2";
    lbl_total_servings_val.font = [UIFont fontWithName:kFontBold size:16];
    lbl_total_servings_val.textColor = [UIColor blackColor];
    lbl_total_servings_val.backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:lbl_total_servings_val];
    
    UIImageView *img_line2 = [[UIImageView alloc]init];
    img_line2 .frame = CGRectMake(20,CGRectGetMaxY(lbl_serving_date.frame)-5,WIDTH-50,0.5);
    [img_line2  setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
    img_line2 .backgroundColor = [UIColor clearColor];
    [img_line2  setUserInteractionEnabled:YES];
    [alertViewBody addSubview:img_line2];
    
    UILabel *lbl_add = [[UILabel alloc]init ];
    lbl_add.frame = CGRectMake(110,02, 300, 45);
    lbl_add.text = @"Add";
    lbl_add.font = [UIFont fontWithName:kFont size:16];
    lbl_add.textColor = [UIColor blackColor];
    lbl_add.backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:lbl_add];
    
    
    UIImageView *img_button = [[UIImageView alloc]init];
    img_button .frame = CGRectMake(20,CGRectGetMaxY(lbl_serving_date.frame)-5,WIDTH-50,0.5);
    [img_button  setImage:[UIImage imageNamed:@"button@2x.png"]];
    img_button .backgroundColor = [UIColor clearColor];
    [img_button  setUserInteractionEnabled:YES];
    [alertViewBody addSubview:img_button];
    
    UIButton *btn_on_mainus = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_mainus.frame=CGRectMake(CGRectGetMidX(lbl_dish_name.frame)-15,17, 15,15);
    btn_on_mainus.backgroundColor = [UIColor clearColor];
    [btn_on_mainus addTarget:self action:@selector(click_on_mainus_btn:) forControlEvents:UIControlEventTouchUpInside];
    btn_on_mainus.userInteractionEnabled = YES;
    // [btn_img_top_up setImage:[UIImage imageNamed:@"text button@2x.png"] forState:UIControlStateNormal];
    [img_button  addSubview:btn_on_mainus];
    
    UIButton *btn_on_pluse = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_pluse.frame=CGRectMake(CGRectGetMidX(lbl_dish_name.frame)-15,17, 15,15);
    btn_on_pluse.backgroundColor = [UIColor clearColor];
    [btn_on_pluse addTarget:self action:@selector(click_on_pluse_btn:) forControlEvents:UIControlEventTouchUpInside];
    btn_on_pluse.userInteractionEnabled = YES;
    // [btn_img_top_up setImage:[UIImage imageNamed:@"text button@2x.png"] forState:UIControlStateNormal];
    [img_button  addSubview:btn_on_pluse];
    
    
    
    UIButton *btn_img_top_up = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_top_up.frame=CGRectMake(CGRectGetMidX(lbl_dish_name.frame)-15,17, 15,15);
    btn_img_top_up.backgroundColor = [UIColor clearColor];
    [btn_img_top_up addTarget:self action:@selector(click_on_top_up_btn:) forControlEvents:UIControlEventTouchUpInside];
    btn_img_top_up.userInteractionEnabled = YES;
    [btn_img_top_up setImage:[UIImage imageNamed:@"text button@2x.png"] forState:UIControlStateNormal];
    [alertViewBody  addSubview:btn_img_top_up];
    
    UILabel *lbl_top_up = [[UILabel alloc]init ];
    lbl_top_up.frame = CGRectMake(110,02, 300, 45);
    lbl_top_up.text = @"TOP UP";
    lbl_top_up.font = [UIFont fontWithName:kFont size:20];
    lbl_top_up.textColor = [UIColor whiteColor];
    lbl_top_up.backgroundColor = [UIColor clearColor];
    [btn_img_top_up addSubview:lbl_top_up];
    
    
    
    
    
    
    
    
    if (IS_IPHONE_6Plus)
    {
        view_for_popup.frame = CGRectMake(0,0,414, 736);
        alertViewBody.frame = CGRectMake(20,137,WIDTH-40,300);
        lbl_dish_name.frame = CGRectMake(25,10, 300, 45);
        icon_cross.frame=CGRectMake(CGRectGetMaxX(lbl_dish_name.frame)+15,17, 15,15);
        icon_serving_type.frame=CGRectMake(30,CGRectGetMaxY(lbl_dish_name.frame),30,30);
        lbl_serving_quantity.frame = CGRectMake(170,60, 200, 45);
        lbl_serving_quantity_val .frame = CGRectMake(CGRectGetMaxX(lbl_serving_quantity.frame)-48,73,100,20);
        lbl_serving_date.frame = CGRectMake(70,CGRectGetMidY(lbl_serving_quantity.frame),300, 45);
        lbl_serving_date_val.frame = CGRectMake(CGRectGetMidX(lbl_serving_date.frame)-20,CGRectGetMidY(lbl_serving_quantity_val.frame), 300, 45);
        img_line .frame = CGRectMake(20,CGRectGetMaxY(lbl_serving_date.frame)-5,WIDTH-75,0.5);
        lbl_total_servings.frame = CGRectMake(50,CGRectGetMidY(img_line.frame),300, 45);
        lbl_total_servings_val.frame = CGRectMake(CGRectGetMaxX(lbl_total_servings.frame)-35,CGRectGetMidY(img_line.frame), 300, 45);
        img_line2 .frame = CGRectMake(20,CGRectGetMaxY(lbl_total_servings.frame)-5,WIDTH-75,0.5);
        lbl_add.frame = CGRectMake(180,CGRectGetMaxY(img_line2.frame)+10, 50, 45);
        
        img_button .frame = CGRectMake(CGRectGetMidX(lbl_add.frame)+19,CGRectGetMaxY(img_line2.frame)+15,120,30);
        btn_on_mainus.frame=CGRectMake(0,0, 30,30);
        btn_on_pluse.frame=CGRectMake(CGRectGetMidX(btn_on_mainus.frame)+75,0, 30,30);
        
        btn_img_top_up.frame=CGRectMake(30,CGRectGetMaxY(img_button.frame)+15,310,45);
        lbl_top_up.frame = CGRectMake(110,02, 300, 45);
        
        
    }
    else if (IS_IPHONE_6)
    {
        view_for_popup.frame = CGRectMake(0,0,414, 736);
        alertViewBody.frame = CGRectMake(20,137,WIDTH-40,300);
        lbl_dish_name.frame = CGRectMake(25,10, 300, 45);
        icon_cross.frame=CGRectMake(CGRectGetMaxX(lbl_dish_name.frame)-20,17, 15,15);
        icon_serving_type.frame=CGRectMake(30,CGRectGetMaxY(lbl_dish_name.frame),30,30);
        lbl_serving_quantity.frame = CGRectMake(150,60, 200, 45);
        lbl_serving_quantity_val .frame = CGRectMake(CGRectGetMaxX(lbl_serving_quantity.frame)-48,73,100,20);
        lbl_serving_date.frame = CGRectMake(50,CGRectGetMidY(lbl_serving_quantity.frame),300, 45);
        lbl_serving_date_val.frame = CGRectMake(CGRectGetMidX(lbl_serving_date.frame)-20,CGRectGetMidY(lbl_serving_quantity_val.frame), 300, 45);
        img_line .frame = CGRectMake(20,CGRectGetMaxY(lbl_serving_date.frame)-5,WIDTH-75,0.5);
        lbl_total_servings.frame = CGRectMake(30,CGRectGetMidY(img_line.frame),300, 45);
        lbl_total_servings_val.frame = CGRectMake(CGRectGetMaxX(lbl_total_servings.frame)-35,CGRectGetMidY(img_line.frame), 300, 45);
        img_line2 .frame = CGRectMake(20,CGRectGetMaxY(lbl_total_servings.frame)-5,WIDTH-75,0.5);
        lbl_add.frame = CGRectMake(150,CGRectGetMaxY(img_line2.frame)+10, 50, 45);
        
        img_button .frame = CGRectMake(CGRectGetMidX(lbl_add.frame)+19,CGRectGetMaxY(img_line2.frame)+15,120,30);
        btn_on_mainus.frame=CGRectMake(0,0, 30,30);
        btn_on_pluse.frame=CGRectMake(CGRectGetMidX(btn_on_mainus.frame)+75,0, 30,30);
        
        btn_img_top_up.frame=CGRectMake(20,CGRectGetMaxY(img_button.frame)+15, 293,45);
        lbl_top_up.frame = CGRectMake(110,02, 300, 45);
        
        
        
    }
    else
    {
        view_for_popup.frame = CGRectMake(0,0,414, 736);
        alertViewBody.frame = CGRectMake(20,137,WIDTH-40,300);
        lbl_dish_name.frame = CGRectMake(25,10, 300, 45);
        icon_cross.frame=CGRectMake(CGRectGetMaxX(lbl_dish_name.frame)-75,17, 15,15);
        icon_serving_type.frame=CGRectMake(30,CGRectGetMaxY(lbl_dish_name.frame),30,30);
        lbl_serving_quantity.frame = CGRectMake(120,60, 200, 45);
        lbl_serving_quantity_val .frame = CGRectMake(CGRectGetMaxX(lbl_serving_quantity.frame)-75,70,100,20);
        lbl_serving_date.frame = CGRectMake(30,CGRectGetMidY(lbl_serving_quantity.frame),300, 45);
        lbl_serving_date_val.frame = CGRectMake(CGRectGetMidX(lbl_serving_date.frame)-40,CGRectGetMidY(lbl_serving_quantity_val.frame)+3, 300, 45);
        img_line .frame = CGRectMake(20,CGRectGetMaxY(lbl_serving_date.frame)-5,WIDTH-75,0.5);
        lbl_total_servings.frame = CGRectMake(20,CGRectGetMidY(img_line.frame),300, 45);
        lbl_total_servings_val.frame = CGRectMake(CGRectGetMaxX(lbl_total_servings.frame)-85,CGRectGetMidY(img_line.frame), 300, 45);
        img_line2 .frame = CGRectMake(20,CGRectGetMaxY(lbl_total_servings.frame)-5,WIDTH-75,0.5);
        lbl_add.frame = CGRectMake(100,CGRectGetMaxY(img_line2.frame)+10, 50, 45);
        
        img_button .frame = CGRectMake(CGRectGetMidX(lbl_add.frame)+19,CGRectGetMaxY(img_line2.frame)+15,120,30);
        btn_on_mainus.frame=CGRectMake(0,0, 30,30);
        btn_on_pluse.frame=CGRectMake(CGRectGetMidX(btn_on_mainus.frame)+75,0, 30,30);
        
        btn_img_top_up.frame=CGRectMake(20,CGRectGetMaxY(img_button.frame)+15, 250,45);
        lbl_top_up.frame = CGRectMake(90,02, 300, 45);
        
        
        lbl_serving_quantity.font = [UIFont fontWithName:kFont size:12];
        lbl_serving_quantity_val .font = [UIFont fontWithName:kFontBold size:20];
        lbl_serving_date.font = [UIFont fontWithName:kFont size:12];
        lbl_serving_date_val.font = [UIFont fontWithName:kFontBold size:12];
        lbl_total_servings.font = [UIFont fontWithName:kFont size:14];
        
        
    }
    
    view_for_popup .hidden = YES;
}

#pragma click-events

-(void)click_on_menu_btn:(UIButton *)sender
{
    NSLog(@"click_on_menu_btn:");
    
}
-(void)btn_on_search_bar_click:(UIButton *)sender
{
    NSLog(@"btn_on_search_bar_click:");
}
-(void)click_on_btn_up_down_arrow:(UIButton *)sender
{
    NSLog(@"click_on_btn_up_down_arrow:");
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        str_sortby = @"1";
        
        [self AFUserDishLists];
        
        
    }
    else
    {
        [sender setSelected:NO];
        str_sortby = @"0";
        [self AFUserDishLists];
        
    }
}
-(void)click_on_drop_down_btn:(UIButton *)sender
{
    NSLog(@"click_on_drop_down_btn");
    tabl_for_today_orders.hidden =NO;
}
-(void)click_on_quantity_available_btn:(UIButton *)sender
{
    NSLog(@"click_on_quantity_available_btn:");
}
-(void)click_on_cross_btn:(UIButton *)sender
{
    NSLog(@"click_on_cross_btn");
}
-(void)click_on_quantity_alert_btn:(UIButton *)sender
{
    NSLog(@"click_on_quantity_alert_btn");
    view_for_popup .hidden = NO;
}
-(void)click_on_x_btn:(UIButton *)sender
{
    NSLog(@"click_on_x_btn");
}
-(void)click_on_message_btn:(UIButton *)sender
{
    NSLog(@"click_on_message_btn:");
    
    
    
    ChefChatingVC*vc = [ChefChatingVC new];
    vc.str_nid = [[ary_dishListfilter objectAtIndex:sender.tag] valueForKey:@"MessageID"];
    vc.str_orderId = [[ary_dishListfilter objectAtIndex:sender.tag] valueForKey:@"Order_id"];
    vc.str_ReceiverID= [[ary_dishListfilter objectAtIndex:sender.tag] valueForKey:@"Sender_id"];
    [self presentViewController:vc animated:NO completion:nil];
    
    
}
-(void)click_on_semi_cercle_btn:(UIButton *)sender
{
    NSLog(@"click_on_semi_cercle_btn:");
    
    //icon_semi_cercle.hidden = YES;
    // transperent_view_for_served.hidden = NO;
}
-(void)click_popup_x_btn:(UIButton *)sender
{
    NSLog(@"click_popup_x_btn");
    view_for_popup .hidden = YES;
    
}
-(void)click_on_top_up_btn:(UIButton *)sender
{
    NSLog(@"click_on_top_up_btn");
}
-(void)click_on_mainus_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_pluse_btn:(UIButton *)sender
{
    NSLog(@"");
}
#pragma mark TextField Delegate methods


//{
//    //    if(textField == txt_username)
//    //    {
//    //        if ([string isEqualToString:@" "])
//    //        {
//    //            return NO;
//    //        }
//    //    }
//
//    return YES;
//}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
    if ([str_day isEqualToString:@"0"])
    {
        [tabl_for_today_orders setHidden:NO];
        
    }
    else if ([str_day isEqualToString:@"1"])
    {
        tabl_for_on_request_orders.hidden = NO;
        
        
    }
    else if ([str_day isEqualToString:@"2"])
    {
        tabl_for_past_orders.hidden = NO;
        
    }
    else if ([str_day isEqualToString:@"3"])
    {
        tabl_for_tomarrow.hidden = NO;
        
    }
    else if ([str_day isEqualToString:@"4"])
    {
        tabl_for_active.hidden = NO;
        
    }
    else if ([str_day isEqualToString:@"5"])
    {
        
    }
    else if ([str_day isEqualToString:@"6"])
    {
        tabl_for_served.hidden = NO;
        
    }
    
    
    
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (textField == txt_search)
    {
        if ([textField.text isEqualToString:@""])
        {
            [ary_dishListfilter removeAllObjects];
            
            for (int i=0; i<[ary_dishList count]; i++)
            {
                [ary_dishListfilter addObject:[ary_dishList objectAtIndex:i]];
            }
            
        }
        
    }
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
#pragma mark ---textFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField ==txt_search)
    {
        if(textField.text)
        {
            search_leg = range.location+1;
        }
        else
        {
            search_leg=0;
        }
        
        
        if (ary_dishListfilter)
        {
            [ary_dishListfilter removeAllObjects];
        }
        
        
        for (NSMutableDictionary *dict in  ary_dishList)
        {
            
            NSString *search = [dict valueForKey:@"username"];
            
            if ([search length]>search_leg)
            {
                search = [search substringToIndex:search_leg];
            }
            
            NSString *substring = [NSString stringWithString:textField.text];
            substring = [substring stringByReplacingCharactersInRange:range withString:string];
            BOOL nameMatches = [[substring lowercaseString] hasPrefix:[string lowercaseString]];
            NSRange r = [search rangeOfString:substring options:NSCaseInsensitiveSearch];
            
            if(r.location != NSNotFound || nameMatches)
            {
                if (r.length > 0)
                {
                    [ary_dishListfilter addObject:dict];
                }
            }
            
        }
        
        
        
    }
    
    if ([str_day isEqualToString:@"0"])
    {
        [tabl_for_orders reloadData];
        
    }
    else if ([str_day isEqualToString:@"1"])
    {
        [tabl_for_on_request_orders reloadData];
        
    }
    else if ([str_day isEqualToString:@"2"])
    {
        [tabl_for_past_orders reloadData];
        
    }
    else if ([str_day isEqualToString:@"3"])
    {
        [tabl_for_tomarrow reloadData];
        
    }
    else if ([str_day isEqualToString:@"4"])
    {
        [tabl_for_active reloadData];
        
    }
    else if ([str_day isEqualToString:@"5"])
    {
        
    }
    else if ([str_day isEqualToString:@"6"])
    {
        [tabl_for_served reloadData];
        
    }
    
    
    
    return YES;
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    [txt_search resignFirstResponder];
    
    return YES;
}




#pragma userDishLists-functionality

-(void)AFUserDishLists
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    
    self.view.userInteractionEnabled = NO;
    
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    NSDictionary *params =@{
                            
                            @"uid"                                      :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"sort_time"                                :  str_sortby,
                            @"order_for"                                :  str_day
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:KCheforderlist  parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        self.view.userInteractionEnabled = YES;
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseUserDishLists:JSON];
    }
     
     
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error){
                                         self.view.userInteractionEnabled = YES;
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFUserDishLists];
                                         }
                                     }];
    [operation start];
    
}
-(void)ResponseUserDishLists:(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    [ary_dishList removeAllObjects];
    [ary_dishListfilter removeAllObjects];
    
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        for (int i=0; i<[[TheDict valueForKey:@"ChefDetails"] count]; i++)
        {
            [ary_dishList addObject:[[TheDict valueForKey:@"ChefDetails"]  objectAtIndex:i]];
            [ary_dishListfilter addObject:[[TheDict valueForKey:@"ChefDetails"]  objectAtIndex:i]];
        }
        
        
        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        if ([str_day isEqualToString:@"0"])
        {
            [tabl_for_orders reloadData];
            
        }
        else if ([str_day isEqualToString:@"1"])
        {
            [tabl_for_on_request_orders reloadData];
            
        }
        else if ([str_day isEqualToString:@"2"])
        {
            [tabl_for_past_orders reloadData];
            
        }
        else if ([str_day isEqualToString:@"3"])
        {
            [tabl_for_tomarrow reloadData];
            
        }
        else if ([str_day isEqualToString:@"4"])
        {
            [tabl_for_active reloadData];
            
        }
        else if ([str_day isEqualToString:@"5"])
        {
            
        }
        else if ([str_day isEqualToString:@"6"])
        {
            [tabl_for_served reloadData];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
    }
    
}


# pragma mark ScheduleListServices

-(void) AFQualityalert
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    NSDictionary *params =@{
                            @"uid"                          :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:KQualityalert
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsequalityalert:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFQualityalert];
                                         }
                                     }];
    [operation start];
    
}



-(void) Responsequalityalert :(NSDictionary * )TheDict
{
    [ary_qualityalert removeAllObjects];
    NSLog(@"Dict is %@", TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[[TheDict valueForKey:@"0"] valueForKey:@"ProductDetail"] count]; i++)
        {
            
            [ary_qualityalert addObject:[[[TheDict valueForKey:@"0"] valueForKey:@"ProductDetail"] objectAtIndex:i]];
            
        }
        
        //[self scrollviewmethod];
        
        NSLog(@"Array of Schedule: %@",ary_qualityalert );
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        
        NSLog(@"Not recive data check service");
        
    }
    
    //[table_schedule reloadData];
}

-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
