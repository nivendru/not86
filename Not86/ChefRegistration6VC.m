//
//  ChefRegistration6VC.m
//  Not86
//
//  Created by User on 05/11/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ChefRegistration6VC.h"

@interface ChefRegistration6VC ()
{
    UIImageView * img_header;
    UILabel * lbl_chef_application;
}

@end

@implementation ChefRegistration6VC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self integratehead];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)integratehead
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH,45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_back_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back_arrow.frame = CGRectMake(10,13,20,20);
    icon_back_arrow .backgroundColor = [UIColor clearColor];
    [icon_back_arrow setImage:[UIImage imageNamed:@"arrow@2x.png"] forState:UIControlStateNormal];
    [icon_back_arrow addTarget:self action:@selector(btn_menu_click:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:icon_back_arrow];
    
    
    lbl_chef_application = [[UILabel alloc]init];
    lbl_chef_application.frame = CGRectMake(CGRectGetMaxX(icon_back_arrow.frame)+30,0, 150, 45);
    lbl_chef_application.text = @"Account";
    lbl_chef_application.font = [UIFont fontWithName:kFont size:20];
    lbl_chef_application.textColor = [UIColor whiteColor];
    lbl_chef_application.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_chef_application];
    
    
       
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 11, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
