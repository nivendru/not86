//
//  ForgetpasswordVC.m
//  Not86
//
//  Created by Interwld on 8/21/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ForgetpasswordVC.h"

@interface ForgetpasswordVC ()<UITextFieldDelegate>
{
    AppDelegate *delegate;
    UIView *view_ForgotPassword;
    UITextField*txt_ForgetPasswordemail;
    
    UIView *alertviewBg;
    
    
    
    UIView *view_popup;
    UIImageView *img_alertpop;
    UITextField *txt_forgotusername;
    UITextField *txt_registrationNumber;
    UITextField *txt_emailAddress;
    CGFloat	animatedDistance;
    UIButton *btn_popsubmit;
    UIButton *btn_popcancel;
    
    UIButton *btn_submit;
    UIButton *btn_cancel;
    
}


@end

@implementation ForgetpasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    
//    UIImageView *img_headerbg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
//    [img_headerbg setImage:[UIImage imageNamed:@"img_backGnd@2x.png"]];
//    [img_headerbg setUserInteractionEnabled:YES];
//    [self.view addSubview:img_headerbg];
//    
    UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(10, 15, 15, 15)];
    [img_back setUserInteractionEnabled:YES];
    img_back.backgroundColor=[UIColor clearColor];
    img_back.image=[UIImage imageNamed:@"arrow@2x.png"];
    [self.view addSubview:img_back];
    
    
    UIButton *btn_BackTransP = [[UIButton alloc] init];
    btn_BackTransP.frame =CGRectMake(10, 15, 15, 15);
    btn_BackTransP.backgroundColor = [UIColor clearColor];
    [btn_BackTransP addTarget:self action:@selector(click_BtnBack:) forControlEvents:UIControlEventTouchUpInside ];
    [img_back addSubview:btn_BackTransP];

    UIImageView *img_Logo = [[UIImageView alloc]initWithFrame:CGRectMake(60,20, 200,IS_IPHONE_5 ? 200:180)];
    [img_Logo setImage:[UIImage imageNamed:@"img_logo@2x.png"]];
    [img_Logo setUserInteractionEnabled:YES];
    [self.view addSubview:img_Logo];
    
    
    view_ForgotPassword = [[UIView alloc]initWithFrame:CGRectMake(0, IS_IPHONE_5 ? CGRectGetMaxY(img_Logo.frame)+20 :CGRectGetMaxY(img_Logo.frame)+10  , 320, 150)];
    [view_ForgotPassword setBackgroundColor: [UIColor clearColor]];
    [self.view addSubview:view_ForgotPassword];
    
    
    
    UIImageView *img_textfeild = [[UIImageView alloc]initWithFrame:CGRectMake(36,20, 248,40)];
    [img_textfeild setImage:[UIImage imageNamed:@"img_forgotTextFeild@2x.png"]];
    [img_textfeild setUserInteractionEnabled:YES];
    [view_ForgotPassword addSubview:img_textfeild];
    
    txt_ForgetPasswordemail = [[UITextField alloc] initWithFrame:CGRectMake(74,22, 200, 36)];
    txt_ForgetPasswordemail.borderStyle = UITextBorderStyleNone;
    txt_ForgetPasswordemail.placeholder = @"Email Address ";
    txt_ForgetPasswordemail.textColor = [UIColor blackColor];
    txt_ForgetPasswordemail.font = [UIFont systemFontOfSize:12.0];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 4, 20)];
    txt_ForgetPasswordemail.leftView = paddingView;
    txt_ForgetPasswordemail.leftViewMode = UITextFieldViewModeAlways;
    txt_ForgetPasswordemail.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_ForgetPasswordemail.keyboardType = UIKeyboardTypeDefault;
    txt_ForgetPasswordemail.clearButtonMode = UITextFieldViewModeWhileEditing;
    txt_ForgetPasswordemail.delegate = self;
    [txt_ForgetPasswordemail setValue:[UIColor colorWithRed:167.0/255.0 green:167.0/255.0 blue:167.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    [txt_ForgetPasswordemail setValue:[UIFont fontWithName: @"Helvetica" size: 12] forKeyPath:@"_placeholderLabel.font"];
    [view_ForgotPassword addSubview:txt_ForgetPasswordemail];
    
    
    
    
    UILabel *lbl_termsandconditions = [[UILabel alloc]initWithFrame:CGRectMake(48, CGRectGetMaxY(txt_ForgetPasswordemail.frame)+16, 220, 13)];
    lbl_termsandconditions.text = @"Contact Not86";
    lbl_termsandconditions.font = [UIFont fontWithName:kFont size:11];
    lbl_termsandconditions.textColor = [UIColor colorWithRed:139/255.0f green:78/255.0f  blue:9/255.0f alpha:1];
    lbl_termsandconditions.backgroundColor = [UIColor clearColor];
    [view_ForgotPassword addSubview:lbl_termsandconditions];
    
    
    UIButton *btn_termsconditions =[UIButton buttonWithType:UIButtonTypeCustom];
    btn_termsconditions.frame=CGRectMake(48, CGRectGetMaxY(txt_ForgetPasswordemail.frame)+10, 220, 30);
    btn_termsconditions.backgroundColor = [UIColor clearColor];
    [btn_termsconditions addTarget:self action:@selector(click_ContactBtn:) forControlEvents:UIControlEventTouchUpInside];
    [view_ForgotPassword  addSubview:btn_termsconditions];
    
    
    btn_submit =[UIButton buttonWithType:UIButtonTypeCustom];
    btn_submit.frame=CGRectMake(37, CGRectGetMaxY(img_textfeild.frame)+40, 119,42);
    btn_submit.backgroundColor = [UIColor clearColor];
    [btn_submit addTarget:self action:@selector(click_submitBtn:) forControlEvents:UIControlEventTouchUpInside];
    [btn_submit setImage:[UIImage  imageNamed:@"btn_submitBtn@2x.png"] forState:UIControlStateHighlighted];
    [btn_submit setImage:[UIImage imageNamed:@"btn_submitYellow@2x.png"] forState:UIControlStateNormal];
    [view_ForgotPassword  addSubview:btn_submit];
    
    btn_cancel =[UIButton buttonWithType:UIButtonTypeCustom];
    btn_cancel.frame=CGRectMake(CGRectGetMaxX(btn_submit.frame)+10, CGRectGetMaxY(img_textfeild.frame)+40, 119,42);
    btn_cancel.backgroundColor = [UIColor clearColor];
    [btn_cancel addTarget:self action:@selector(click_cancelBtn:) forControlEvents:UIControlEventTouchUpInside];
    [btn_cancel setImage:[UIImage  imageNamed:@"img_cancel@2x.png"] forState:UIControlStateNormal];
    [btn_cancel setImage:[UIImage imageNamed:@"btn_cancelgray@2x.png"] forState:UIControlStateHighlighted];
    [view_ForgotPassword  addSubview:btn_cancel];
    //
    
    

    // Do any additional setup after loading the view.
}

-(void)click_BtnBack{
     [self.navigationController popViewControllerAnimated:NO];
    
}
-(void)popupForgetemail


{
    view_popup = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,IS_IPHONE_5 ? 568 :480)];
    view_popup.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.1];
    view_popup.userInteractionEnabled=TRUE;
    [self.view addSubview:view_popup];
    
    
    img_alertpop =[[UIImageView alloc] init];
    [img_alertpop setImage: [UIImage imageNamed:@"img_viewvehicalBackGnd@2x.png"]];
    img_alertpop.frame = CGRectMake(20,(IS_IPHONE_5?250:200 ),280,227);
    img_alertpop.layer.cornerRadius =10.0;
    img_alertpop.backgroundColor=[UIColor whiteColor];
    img_alertpop.userInteractionEnabled=YES;
    [view_popup addSubview:img_alertpop];
    
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:(253.0/225.0) green:(167.0/225.0) blue:(11.0/225.0) alpha:1.0];
    lab_alertViewTitle.frame = CGRectMake(10,10,280,23);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewTitle.text=@"Not 86";
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [img_alertpop addSubview:lab_alertViewTitle];
    
    
    UIImageView *img_BackGnd =[[UIImageView alloc] init];
    img_BackGnd.frame = CGRectMake(34,45, 220, 34);
    img_BackGnd.layer.cornerRadius =3.0;
    img_BackGnd.backgroundColor=[UIColor whiteColor];
    img_BackGnd.userInteractionEnabled=YES;
    [img_alertpop addSubview:img_BackGnd];
    
    
    txt_forgotusername = [[UITextField alloc] initWithFrame:CGRectMake(34,45, 220, 34)];
    txt_forgotusername.borderStyle = UITextBorderStyleLine;
    txt_forgotusername.placeholder = @"Username";
    txt_forgotusername.textColor = [UIColor blackColor];
    txt_forgotusername.font = [UIFont systemFontOfSize:12.0];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 4, 20)];
    txt_forgotusername.leftView = paddingView;
    txt_forgotusername.leftViewMode = UITextFieldViewModeAlways;
    txt_forgotusername.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_forgotusername.keyboardType = UIKeyboardTypeDefault;
    txt_forgotusername.clearButtonMode = UITextFieldViewModeWhileEditing;
    txt_forgotusername.delegate = self;
    [txt_forgotusername setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    [txt_forgotusername setValue:[UIFont fontWithName: @"Helvetica" size: 12] forKeyPath:@"_placeholderLabel.font"];
    txt_forgotusername.layer.borderColor = [[UIColor colorWithRed:167.0/255.0 green:167.0/255.0 blue:167.0/255.0 alpha:1.0] CGColor];
    txt_forgotusername.layer.borderWidth = 1.0f;
    txt_forgotusername.layer.cornerRadius = 3.0f;
    [img_alertpop addSubview:txt_forgotusername];
    
    txt_emailAddress = [[UITextField alloc] initWithFrame:CGRectMake(34,CGRectGetMaxY(txt_forgotusername.frame)+5, 220, 34)];
    txt_emailAddress.borderStyle = UITextBorderStyleNone;
    txt_emailAddress.placeholder = @"Email Address";
    txt_emailAddress.textColor = [UIColor blackColor];
    txt_emailAddress.font = [UIFont systemFontOfSize:12.0];
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 4, 20)];
    txt_emailAddress.leftView = paddingView1;
    txt_emailAddress.leftViewMode = UITextFieldViewModeAlways;
    txt_emailAddress.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_emailAddress.keyboardType = UIKeyboardTypeDefault;
    txt_emailAddress.clearButtonMode = UITextFieldViewModeWhileEditing;
    txt_emailAddress.delegate = self;
    [txt_emailAddress setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    [txt_emailAddress setValue:[UIFont fontWithName: @"Helvetica" size: 12] forKeyPath:@"_placeholderLabel.font"];
    txt_emailAddress.layer.borderColor = [[UIColor colorWithRed:167.0/255.0 green:167.0/255.0 blue:167.0/255.0 alpha:1.0] CGColor];
    txt_emailAddress.layer.borderWidth = 1.0f;
    txt_emailAddress.layer.cornerRadius = 3.0f;
    
    [img_alertpop addSubview:txt_emailAddress];
    
    
    
    
    txt_registrationNumber = [[UITextField alloc] initWithFrame:CGRectMake(34,CGRectGetMaxY(txt_emailAddress.frame)+5, 220,34)];
    txt_registrationNumber.borderStyle = UITextBorderStyleNone;
    txt_registrationNumber.placeholder = @"Registration Number";
    txt_registrationNumber.textColor = [UIColor blackColor];
    txt_registrationNumber.font = [UIFont systemFontOfSize:12.0];
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 4, 20)];
    txt_registrationNumber.leftView = paddingView2;
    txt_registrationNumber.leftViewMode = UITextFieldViewModeAlways;
    txt_registrationNumber.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_registrationNumber.keyboardType = UIKeyboardTypeDefault;
    txt_registrationNumber.clearButtonMode = UITextFieldViewModeWhileEditing;
    txt_registrationNumber.delegate = self;
    [txt_registrationNumber setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    [txt_registrationNumber setValue:[UIFont fontWithName: @"Helvetica" size: 12] forKeyPath:@"_placeholderLabel.font"];
    txt_registrationNumber.layer.borderColor = [[UIColor colorWithRed:167.0/255.0 green:167.0/255.0 blue:167.0/255.0 alpha:1.0] CGColor];
    txt_registrationNumber.layer.borderWidth = 1.0f;
    txt_registrationNumber.layer.cornerRadius = 3.0f;
    
    [img_alertpop addSubview:txt_registrationNumber];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.frame = CGRectMake(0, CGRectGetMaxY(txt_registrationNumber.frame)+22, 280, 1);
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [img_alertpop addSubview:imageview_div];
    
    
    
    
    btn_popsubmit=[[UIButton alloc] init] ;
    btn_popsubmit.frame = CGRectMake(0,CGRectGetMaxY(imageview_div.frame), 140, 48);
    [btn_popsubmit setTitle:@"SUBMIT" forState:UIControlStateNormal];
    [btn_popsubmit setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    btn_popsubmit.titleLabel.font = [UIFont fontWithName:kFont size:13];
    btn_popsubmit.backgroundColor=[UIColor clearColor];
    [btn_popsubmit addTarget:self action:@selector(click_btnpopSubmit:) forControlEvents:UIControlEventTouchUpInside];
    
    [img_alertpop addSubview:btn_popsubmit];
    
    UIImageView *imageview_vertdiv=[[UIImageView alloc] init];
    imageview_vertdiv.frame = CGRectMake(CGRectGetMaxX(btn_popsubmit.frame), CGRectGetMaxY(imageview_div.frame),1, 47);
    imageview_vertdiv.backgroundColor=[UIColor lightGrayColor];
    [img_alertpop addSubview:imageview_vertdiv];
    
    btn_popcancel=[[UIButton alloc] init];
    btn_popcancel.frame = CGRectMake(141,CGRectGetMaxY(imageview_div.frame), 140, 48);
    [btn_popcancel setTitle:@"CANCEL" forState:UIControlStateNormal];
    [btn_popcancel setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    btn_popcancel.titleLabel.font = [UIFont fontWithName:kFont size:13];
    btn_popcancel.backgroundColor=[UIColor clearColor];
    [btn_popcancel addTarget:self action:@selector(click_btnpopCancel:) forControlEvents:UIControlEventTouchUpInside];
    [img_alertpop addSubview:btn_popcancel];
    
    
    
    
}


#pragma mark - Validate Email
-(BOOL)ValidateEmail :(NSString *) strToEvaluate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL b = [emailTest evaluateWithObject:strToEvaluate];
    
    return b;
}

- (NSMutableArray *)validateEmailWithString:(NSString*)emails
{
    NSMutableArray *validEmails = [[NSMutableArray alloc] init];
    NSArray *emailArray = [emails componentsSeparatedByString:@","];
    for (NSString *email in emailArray)
    {
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        if ([emailTest evaluateWithObject:email])
            [validEmails addObject:email];
    }
    return validEmails;
}


-(void)click_btnpopCancel:(UIButton *)sender
{
    [view_popup removeFromSuperview];
}
-(void)click_btnpopSubmit:(UIButton *)sender
{
    
    if (txt_forgotusername.text.length == 0)
    {
        [self popup_Alertview:@"Please enter Username"];
        
    }
    
    else if  (txt_emailAddress.text.length <= 0)
    {
        [self popup_Alertview:@"Please enter email address."];
        
    }
    else if(txt_emailAddress.text.length>0 && ![self ValidateEmail:txt_emailAddress.text])
    {
        txt_emailAddress.text = @"";
        
        [self  popup_Alertview:@"Please enter valid email address."];
    }
    else
    {
//        [self AFContactUs];
        
        txt_forgotusername.text = @"";
        txt_registrationNumber.text = @"";
        txt_emailAddress.text = @"";
        
    }
    
    
}


#pragma mark ClickEvents

-(void)click_ContactBtn:(UIButton*)sender
{
    [self popupForgetemail];
}

#pragma mark TextField Delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == txt_forgotusername)
    {
        if ([string isEqualToString:@" "])
        {
            return NO;
        }
    }
    
    
    if ([textField isEqual:txt_registrationNumber])
    {
        NSString *str_number = txt_registrationNumber.text;
        NSString *str_RegNumber = [str_number uppercaseString];
        txt_registrationNumber.text = str_RegNumber;
    }
    
    return YES;
}
//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    if ([textField isEqual:txt_registrationNumber])
//    {
//        NSString *str_number = txt_registrationNumber.text;
//        NSString *str_RegNumber = [str_number uppercaseString];
//        txt_registrationNumber.text = str_RegNumber;
//    }
//    
//    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
//    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
//    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
//   CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
//    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
//    CGFloat heightFraction = numerator / denominator;
//    if (heightFraction < 0.0)
//    {
//        heightFraction = 0.0;
//    }
//    else if (heightFraction > 1.0)
//    {
//        heightFraction = 1.0;
//    }
//    UIInterfaceOrientation orientation =
//    [[UIApplication sharedApplication] statusBarOrientation];
//    if (orientation == UIInterfaceOrientationPortrait ||
//        orientation == UIInterfaceOrientationPortraitUpsideDown)
//    {
//        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
//    }
//    else
//    {
//        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
//    }
//    CGRect viewFrame = self.view.frame;
//    viewFrame.origin.y -= animatedDistance;
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationBeginsFromCurrentState:YES];
//    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
//    [self.view setFrame:viewFrame];
//    [UIView commitAnimations];
//}
//
//
//-(void)textFieldDidEndEditing:(UITextField *)textField
//{
//    
//    if ([textField isEqual:txt_registrationNumber])
//    {
//        NSString *str_number = txt_registrationNumber.text;
//        NSString *str_RegNumber = [str_number uppercaseString];
//        txt_registrationNumber.text = str_RegNumber;
//    }
//    
//    CGRect viewFrame = self.view.frame;
//    viewFrame.origin.y += animatedDistance;
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationBeginsFromCurrentState:YES];
//    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
//    [self.view setFrame:viewFrame];
//    [UIView commitAnimations];
//}


-(void)click_submitBtn:(UIButton*)sender
{
    if (txt_ForgetPasswordemail.text.length <= 0)
    {
        [self popup_Alertview:@"Please enter email address."];
        
    }
    else if(txt_ForgetPasswordemail.text.length>0 && ![self ValidateEmail:txt_ForgetPasswordemail.text])
    {
        txt_ForgetPasswordemail.text = @"";
        
        [self  popup_Alertview:@"Please enter valid email address."];
    }
    else
    {
        
//          [self AFForgetPass];
        txt_ForgetPasswordemail.text = @"";
        
    }
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self resignKeyboard];
    return YES;
}


-(void)resignKeyboard
{
    [txt_ForgetPasswordemail resignFirstResponder];
    
    [txt_forgotusername resignFirstResponder];
    [txt_emailAddress resignFirstResponder];
    [txt_registrationNumber resignFirstResponder];
    
    
    
}
-(void)click_cancelBtn:(UIButton*)sender
{
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    
    
    // [self dismissViewControllerAnimated:YES completion:nil];
    
}
//-(void)click_BtnBack:(UIButton*)sender
//{
//    [self.navigationController popToRootViewControllerAnimated:NO];
//
//}

#pragma mark Alertview Popup

-(void)popup_Alertview:(NSString *)message
{
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    UIImageView *alert_ViewBody =[[UIImageView alloc] init];
    alert_ViewBody.layer.cornerRadius =10.0;
    alert_ViewBody.userInteractionEnabled=YES;
    alert_ViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alert_ViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:(253.0/225.0) green:(167.0/225.0) blue:(11.0/225.0) alpha:1.0];
    lab_alertViewTitle.text=@"Not 86";
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alert_ViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alert_ViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alert_ViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor colorWithRed:(253.0/225.0) green:(167.0/225.0) blue:(11.0/225.0) alpha:1.0] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alert_ViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alert_ViewBody.frame = CGRectMake(50,HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(2,45,WIDTH-104,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:14.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}

//# pragma mark - Forgot Password Services
//
//-(void) AFForgetPass
//{
//    
//    [btn_cancel setUserInteractionEnabled:NO];
//    [btn_submit setUserInteractionEnabled:NO];
//    
//    
//    [self.view addSubview:delegate.activityIndicator];
//    [delegate.activityIndicator startAnimating];
//    //=================================================================BASE URL
//    
//    NSURL *url = [NSURL URLWithString:kBaseUrl];
//    
//    //=================================================================USED PARAMETERS(ONLY TEXT)
//    
//    NSDictionary *params =@{
//                            @"email"                  :   txt_ForgetPasswordemail.text,
//                            };
//    
//    
//    //===========================================AFNETWORKING HEADER
//    
//    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
//    httpClient.parameterEncoding = AFFormURLParameterEncoding;
//    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
//    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
//    
//    //===============================SIMPLE REQUEST
//    
//    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
//                                                            path:kForgetPassword
//                                                      parameters:params];
//    
//    
//    
//    
//    //====================================================RESPONSE
//    
//    
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
//    
//    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
//        
//    }];
//    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSError *error = nil;
//        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
//        
//        [delegate.activityIndicator stopAnimating];
//        [self ResponseForgetPass:JSON];
//    }
//     
//     //==================================================ERROR
//                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                         
//                                         
//                                         [btn_cancel setUserInteractionEnabled:YES];
//                                         [btn_submit setUserInteractionEnabled:YES];
//                                         [delegate.activityIndicator stopAnimating];
//                                         
//                                         if([operation.response statusCode] == 406){
//                                             
//                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
//                                             return;
//                                         }
//                                         
//                                         if([operation.response statusCode] == 403){
//                                             NSLog(@"Upload Failed");
//                                             return;
//                                         }
//                                         if ([[operation error] code] == -1009) {
//                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"SG RoadRunner"
//                                                                                          message:@"Please check your internet connection"
//                                                                                         delegate:nil
//                                                                                cancelButtonTitle:@"OK"
//                                                                                otherButtonTitles:nil];
//                                             [av show];
//                                         }
//                                         else if ([[operation error] code] == -1001) {
//                                             [self AFForgetPass]
//                                             ;
//                                             //
//                                         }
//                                     }];
//    [operation start];
//    
//}
//-(void) ResponseForgetPass :(NSDictionary * )TheDict
//{
//    NSLog(@"the dict %@",TheDict);
//    
//    [btn_cancel setUserInteractionEnabled:YES];
//    [btn_submit setUserInteractionEnabled:YES];
//    
//    
//    if([[TheDict valueForKey:@"error"] intValue] == 0)
//    {
//        [self resignKeyboard];
//        
//        [self popup_Alertview:[TheDict valueForKey:@"message"]];
//        
//        
//    }
//    
//    else{
//        
//        
//        txt_ForgetPasswordemail.text = @"";
//        [self resignKeyboard];
//        
//        [self popup_Alertview:[TheDict valueForKey:@"message"]];
//        
//    }
//    
//    
//}
//
//
//
//
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
