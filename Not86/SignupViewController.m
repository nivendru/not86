//
//  SignupViewController.m
//  Not86
//
//  Created by Interwld on 8/3/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "SignupViewController.h"
#import "ChefSignup.h"
#import "AppDelegate.h"
#import "Define.h"
#import "LoginViewController.h"

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"

@interface SignupViewController ()<UITextFieldDelegate,UITextViewDelegate,UIScrollViewDelegate>
{
    UIImageView *img_BackgroundImg;
    UIImageView *img_topbar;
    UITextField *txt_username;
    UITextField *txt_Password;
    UITextField *txt_conformpassword;
    AppDelegate *delegate;
    UIView *alertviewBg;
    CGFloat	animatedDistance;
    UIImageView *imgView_usernameStatus;
    UIImageView *imgView_PasswordStatus;
    UIImageView *imgView_ConfirmPasswordStatus;
    UITextField *txt_ReferralID ;
    
    NSString*str_success;
    UIToolbar*keyboardToolbar_Date;
    
    

}

@end

@implementation SignupViewController
@synthesize array_Facebook_Details;

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;


- (void)viewDidLoad {
    [super viewDidLoad];
    str_success = [NSString new];
    
    [self IntegrateHeaderDesign];
    [self IntegrateBodyDesign];
        // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)IntegrateHeaderDesign
{
    img_BackgroundImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    [img_BackgroundImg setUserInteractionEnabled:YES];
    img_BackgroundImg.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:img_BackgroundImg];
    
    
    
    img_topbar=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,45)];
    [img_topbar setUserInteractionEnabled:YES];
    img_topbar.image=[UIImage imageNamed:@"img-head@2x.png"];
    //    img_topbar.layer.shadowColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:1].CGColor;
    //    img_topbar.layer.shadowOffset = CGSizeMake(0,10);
    //    img_topbar.layer.shadowOpacity = 0.3;
    //        img_topbar.layer.shadowRadius =1.0;
    [self.view addSubview:img_topbar];
    
    
    UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(10, 17, 15, 15)];
    [img_back setUserInteractionEnabled:YES];
    img_back.backgroundColor=[UIColor clearColor];
    img_back.image=[UIImage imageNamed:@"arrow@2x.png"];
    [img_topbar addSubview:img_back];
    
    
    
    UILabel  * lbl_heading = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(img_back.frame)+20,0, 220,50)];
    lbl_heading.text = @"Sign Up";
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font=[UIFont fontWithName:kFont size:20];
    [img_topbar addSubview:lbl_heading];
    
    
    UIButton *btn_back = [[UIButton alloc] init];
    btn_back.frame = CGRectMake(0, 0, 60,35);
    btn_back.backgroundColor = [UIColor clearColor];
    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_topbar addSubview:btn_back];
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 7, 30, 30)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-login-logo@2x.png"];
    [img_topbar addSubview:img_logo];
    
    
    
    
}
-(void)IntegrateBodyDesign
{
    
    UIImageView *img_logo=[[UIImageView alloc]init];
    //    img_logo.frame=CGRectMake(20, CGRectGetMaxY(img_topbar.frame)+40, WIDTH-60, 90);
    if (IS_IPHONE_6Plus) {
        img_logo.frame=CGRectMake(45, CGRectGetMaxY(img_topbar.frame)+40, WIDTH-120, 85);
    }
    else if (IS_IPHONE_6) {
        img_logo.frame=CGRectMake(40, CGRectGetMaxY(img_topbar.frame)+40, WIDTH-80, 90);
    }
    else
    {
        img_logo.frame=CGRectMake(30, CGRectGetMaxY(img_topbar.frame)+35, WIDTH-80, 75);
    }
    
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-logo-text@2x.png"];
    //    img_logo.contentMode = UIViewContentModeScaleAspectFill;
    //    img_logo.clipsToBounds = YES;
    [img_BackgroundImg addSubview:img_logo];
    
    UIImageView *img_user=[[UIImageView alloc]init];
    //    img_user.frame=CGRectMake(20, CGRectGetMaxY(img_logo.frame)+70, WIDTH-40, 1);
    if (IS_IPHONE_6Plus) {
        img_user.frame=CGRectMake(25, CGRectGetMaxY(img_logo.frame)+73, WIDTH-50, 1);
        
    }
    else if (IS_IPHONE_6) {
        img_user.frame=CGRectMake(25, CGRectGetMaxY(img_logo.frame)+73, WIDTH-50, 1);
        
    }
    else
    {
        img_user.frame=CGRectMake(25, CGRectGetMaxY(img_logo.frame)+73, WIDTH-50, 1);
        
        
    }
    
    img_user.backgroundColor=[UIColor lightGrayColor];
    [img_user setUserInteractionEnabled:YES];
    [img_BackgroundImg addSubview:img_user];
    
    txt_username = [[UITextField alloc] init];
    //    txt_username.frame=CGRectMake(20, CGRectGetMaxY(img_logo.frame)+35, WIDTH-80, 38);
    if (IS_IPHONE_6Plus) {
        txt_username.frame=CGRectMake(20, CGRectGetMaxY(img_logo.frame)+35, WIDTH-80, 38);
    }
    else if (IS_IPHONE_6) {
        txt_username.frame=CGRectMake(20, CGRectGetMaxY(img_logo.frame)+35, WIDTH-80, 38);
        
    }
    else
    {
        txt_username.frame=CGRectMake(20, CGRectGetMaxY(img_logo.frame)+35, WIDTH-80, 38);
        
        
    }
    txt_username.borderStyle = UITextBorderStyleNone;
    txt_username.textColor = [UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1];
    txt_username.font = [UIFont fontWithName:kFont size:18];
    txt_username.placeholder = @"Username";
    if ([[NSString stringWithFormat:@"%@",delegate.login_type]isEqualToString:@"FACEBOOK"])
    {
        txt_username.text = [[array_Facebook_Details objectAtIndex:0] valueForKey:@"name"];
    }
    
    [txt_username setValue:[UIFont fontWithName:kFont size: 18] forKeyPath:@"_placeholderLabel.font"];
    [txt_username setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_username.leftView = padding1;
    txt_username.leftViewMode = UITextFieldViewModeAlways;
    txt_username.userInteractionEnabled=YES;
    txt_username.textAlignment = NSTextAlignmentLeft;
    txt_username.backgroundColor = [UIColor clearColor];
    txt_username.keyboardType = UIKeyboardTypeAlphabet;
    txt_username.delegate = self;
    [img_BackgroundImg addSubview:txt_username];
    
    
//    imgView_usernameStatus=[[UIImageView alloc]init];
//    if (IS_IPHONE_6Plus) {
//        imgView_usernameStatus.frame = CGRectMake(CGRectGetMaxX(txt_username.frame)+2,CGRectGetMaxY(img_logo.frame)+35,20,20);
//    }
//    else if (IS_IPHONE_6) {
//        imgView_usernameStatus.frame = CGRectMake(CGRectGetMaxX(txt_username.frame)+2,CGRectGetMaxY(img_logo.frame)+35,20,20);
//        
//    }
//    else
//    {
//        imgView_usernameStatus.frame = CGRectMake(CGRectGetMaxX(txt_username.frame)+2,CGRectGetMaxY(img_logo.frame)+35,20,20);
//        
//        
//    }
//
//    [imgView_usernameStatus setImage:[UIImage imageNamed:@""]];
//    imgView_usernameStatus.backgroundColor = [UIColor clearColor];
//    imgView_usernameStatus.userInteractionEnabled=YES;
//    [img_BackgroundImg addSubview:imgView_usernameStatus];
//
    
    
    UIImageView *img_pass=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus) {
        img_pass.frame=CGRectMake(25, CGRectGetMaxY(img_user.frame)+63, WIDTH-50, 1);
    }
    else if (IS_IPHONE_6) {
        img_pass.frame=CGRectMake(25, CGRectGetMaxY(img_user.frame)+63, WIDTH-50, 1);
        
    }
    else
    {
        img_pass.frame=CGRectMake(25, CGRectGetMaxY(img_user.frame)+63, WIDTH-50, 1);
        
    }
    img_pass.backgroundColor=[UIColor lightGrayColor];
    [img_pass setUserInteractionEnabled:YES];
    [img_BackgroundImg addSubview:img_pass];
    
    
    txt_Password = [[UITextField alloc] init];
    
    if (IS_IPHONE_6Plus) {
        txt_Password.frame=CGRectMake(20, CGRectGetMaxY(img_user.frame)+25, WIDTH-80, 38);
    }
    else if (IS_IPHONE_6) {
        txt_Password.frame=CGRectMake(20, CGRectGetMaxY(img_user.frame)+25, WIDTH-80, 38);
    }
    else
    {
        txt_Password.frame=CGRectMake(20, CGRectGetMaxY(img_user.frame)+25, WIDTH-80, 38);
    }
    txt_Password.borderStyle = UITextBorderStyleNone;
    txt_Password.textColor = [UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1];
    txt_Password.font = [UIFont fontWithName:kFont size:18];
    txt_Password.placeholder = @"Password";
    [txt_Password setValue:[UIFont fontWithName: kFont size: 18] forKeyPath:@"_placeholderLabel.font"];
    [txt_Password setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_Password.leftView = padding;
    txt_Password.leftViewMode = UITextFieldViewModeAlways;
    txt_Password.userInteractionEnabled=YES;
    txt_Password.textAlignment = NSTextAlignmentLeft;
    txt_Password.backgroundColor = [UIColor clearColor];
    txt_Password.keyboardType = UIKeyboardTypeAlphabet;
    txt_Password.delegate = self;
    txt_Password.secureTextEntry=YES;
    [img_BackgroundImg addSubview:txt_Password];
    
    
    
    imgView_PasswordStatus=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus) {
        imgView_PasswordStatus.frame = CGRectMake(CGRectGetMaxX(txt_Password.frame)+2,CGRectGetMaxY(txt_username.frame)+35,20,20);
    }
    else if (IS_IPHONE_6) {
        imgView_PasswordStatus.frame = CGRectMake(CGRectGetMaxX(txt_Password.frame)+2,CGRectGetMaxY(txt_username.frame)+35,20,20);
        
    }
    else
    {
        imgView_PasswordStatus.frame = CGRectMake(CGRectGetMaxX(txt_Password.frame)+2,CGRectGetMaxY(txt_username.frame)+35,20,20);
      
    }
    
    [imgView_PasswordStatus setImage:[UIImage imageNamed:@""]];
    imgView_PasswordStatus.backgroundColor = [UIColor clearColor];
    imgView_PasswordStatus.userInteractionEnabled=YES;
    [img_BackgroundImg addSubview:imgView_PasswordStatus];
    

    
    
    UIImageView *img_passes=[[UIImageView alloc]init];
    //    img_passes.frame=CGRectMake(CGRectGetMaxX(txt_Password.frame)+2, CGRectGetMaxY(img_user.frame)+24, 25, 25);
    if (IS_IPHONE_6Plus) {
        img_passes.frame=CGRectMake(CGRectGetMaxX(txt_Password.frame)+2, CGRectGetMaxY(img_user.frame)+28, 23, 23);
    }
    else if (IS_IPHONE_6) {
        img_passes.frame=CGRectMake(CGRectGetMaxX(txt_Password.frame)+2, CGRectGetMaxY(img_user.frame)+28, 22, 22);
    }
    else
    {
        img_passes.frame=CGRectMake(CGRectGetMaxX(txt_Password.frame)+2, CGRectGetMaxY(img_user.frame)+28, 25, 25);
    }
    
    
    [img_passes setUserInteractionEnabled:YES];
    img_passes.backgroundColor=[UIColor clearColor];
    img_passes.image=[UIImage imageNamed:@"img-pass@2x.png"];
    img_passes.hidden=YES;
    //    img_logo.contentMode = UIViewContentModeScaleAspectFill;
    //    img_logo.clipsToBounds = YES;
    [self.view addSubview:img_passes];
    
    
    
    
    
    
    UIImageView *img_conpass=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus) {
        img_conpass.frame=CGRectMake(25, CGRectGetMaxY(img_pass.frame)+63, WIDTH-50, 1);
    }
    else if (IS_IPHONE_6) {
        img_conpass.frame=CGRectMake(25, CGRectGetMaxY(img_pass.frame)+63, WIDTH-50, 1);
    }
    else
    {
        img_conpass.frame=CGRectMake(25, CGRectGetMaxY(img_pass.frame)+63, WIDTH-50, 1);
    }
    img_conpass.backgroundColor=[UIColor lightGrayColor];
    [img_conpass setUserInteractionEnabled:YES];
    [img_BackgroundImg addSubview:img_conpass];
    
    
    txt_conformpassword = [[UITextField alloc] init];
    
    if (IS_IPHONE_6Plus) {
        txt_conformpassword.frame=CGRectMake(20, CGRectGetMaxY(img_pass.frame)+25, WIDTH-80, 38);
    }
    else if (IS_IPHONE_6) {
        txt_conformpassword.frame=CGRectMake(20, CGRectGetMaxY(img_pass.frame)+25, WIDTH-80, 38);
    }
    else
    {
        txt_conformpassword.frame=CGRectMake(20, CGRectGetMaxY(img_pass.frame)+25, WIDTH-80, 38);
    }
    txt_conformpassword.borderStyle = UITextBorderStyleNone;
    txt_conformpassword.textColor = [UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1];
    txt_conformpassword.font = [UIFont fontWithName:kFont size:18];
    txt_conformpassword.placeholder = @"Confirm Password";
    [txt_conformpassword setValue:[UIFont fontWithName: kFont size: 18] forKeyPath:@"_placeholderLabel.font"];
    [txt_conformpassword setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_conformpassword.leftView = padding2;
    txt_conformpassword.leftViewMode = UITextFieldViewModeAlways;
    txt_conformpassword.userInteractionEnabled=YES;
    txt_conformpassword.textAlignment = NSTextAlignmentLeft;
    txt_conformpassword.backgroundColor = [UIColor clearColor];
    txt_conformpassword.keyboardType = UIKeyboardTypeAlphabet;
    txt_conformpassword.delegate = self;
    txt_conformpassword.secureTextEntry=YES;
    [img_BackgroundImg addSubview:txt_conformpassword];
    
    imgView_ConfirmPasswordStatus=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus) {
        imgView_ConfirmPasswordStatus.frame = CGRectMake(CGRectGetMaxX(txt_conformpassword.frame)+2,CGRectGetMaxY(txt_Password.frame)+35,20,20);
    }
    else if (IS_IPHONE_6) {
        imgView_ConfirmPasswordStatus.frame = CGRectMake(CGRectGetMaxX(txt_conformpassword.frame)+2,CGRectGetMaxY(txt_Password.frame)+35,20,20);
        
    }
    else
    {
        imgView_ConfirmPasswordStatus.frame = CGRectMake(CGRectGetMaxX(txt_conformpassword.frame)+2,CGRectGetMaxY(txt_Password.frame)+35,20,20);
        
        
    }
    
    [imgView_ConfirmPasswordStatus setImage:[UIImage imageNamed:@""]];
    imgView_ConfirmPasswordStatus.backgroundColor = [UIColor clearColor];
    imgView_ConfirmPasswordStatus.userInteractionEnabled=YES;
    [img_BackgroundImg addSubview:imgView_ConfirmPasswordStatus];
    

    
    
    
    UIImageView *img_confirmpasses=[[UIImageView alloc]init];
    //    img_confirmpasses.frame=CGRectMake(CGRectGetMaxX(txt_conformpassword.frame)+2, CGRectGetMaxY(img_pass.frame)+18, 28, 35);
    
    if (IS_IPHONE_6Plus) {
        img_confirmpasses.frame=CGRectMake(CGRectGetMaxX(txt_conformpassword.frame)+2, CGRectGetMaxY(img_pass.frame)+30, 27, 25);
    }
    else if (IS_IPHONE_6) {
        img_confirmpasses.frame=CGRectMake(CGRectGetMaxX(txt_conformpassword.frame)+2, CGRectGetMaxY(img_pass.frame)+30, 27, 25);
        
    }
    else
    {
        img_confirmpasses.frame=CGRectMake(CGRectGetMaxX(txt_conformpassword.frame)+2, CGRectGetMaxY(img_pass.frame)+30, 28, 25);
    }
    [img_confirmpasses setUserInteractionEnabled:YES];
    img_confirmpasses.backgroundColor=[UIColor clearColor];
    img_confirmpasses.image=[UIImage imageNamed:@""];
    img_confirmpasses.hidden=YES;

    //    img_logo.contentMode = UIViewContentModeScaleAspectFill;
    //    img_logo.clipsToBounds = YES;
    [img_BackgroundImg addSubview:img_confirmpasses];
    
  //  img_BackgroundImg
    
    txt_ReferralID = [[UITextField alloc] init];
    
    if (IS_IPHONE_6Plus) {
        txt_ReferralID.frame=CGRectMake(23, CGRectGetMaxY(txt_conformpassword.frame)+25, WIDTH-80, 38);
    }
    else if (IS_IPHONE_6) {
        txt_ReferralID.frame=CGRectMake(23, CGRectGetMaxY(txt_conformpassword.frame)+25, WIDTH-80, 38);
    }
    else
    {
        txt_ReferralID.frame=CGRectMake(23, CGRectGetMaxY(txt_conformpassword.frame)+25, WIDTH-80, 38);
    }
    txt_ReferralID.borderStyle = UITextBorderStyleNone;
    txt_ReferralID.textColor = [UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1];
    txt_ReferralID.font = [UIFont fontWithName:kFont size:18];
    txt_ReferralID.placeholder = @"Call referral ID";
    [txt_ReferralID setValue:[UIFont fontWithName: kFont size: 18] forKeyPath:@"_placeholderLabel.font"];
    [txt_ReferralID setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_ReferralID.leftViewMode = UITextFieldViewModeAlways;
    txt_ReferralID.userInteractionEnabled=YES;
    txt_ReferralID.textAlignment = NSTextAlignmentLeft;
    txt_ReferralID.backgroundColor = [UIColor clearColor];
    txt_ReferralID.keyboardType = UIKeyboardTypeNumberPad;
    txt_ReferralID.delegate = self;
//    txt_ReferralID.secureTextEntry=YES;
    [img_BackgroundImg addSubview:txt_ReferralID];
    
    
    
    if (keyboardToolbar_Date == nil)
    {
        keyboardToolbar_Date = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, IS_IPHONE_5?45:45)];
        [keyboardToolbar_Date setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_Done)];
        [keyboardToolbar_Date setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    txt_ReferralID.inputAccessoryView = keyboardToolbar_Date;
    

    
    UIImageView *img_Rerralidline=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus) {
        img_Rerralidline.frame=CGRectMake(25, CGRectGetMaxY(txt_ReferralID.frame)+1, WIDTH-50, 1);
    }
    else if (IS_IPHONE_6) {
        img_Rerralidline.frame=CGRectMake(25, CGRectGetMaxY(txt_ReferralID.frame)+1, WIDTH-50, 1);
    }
    else
    {
        img_Rerralidline.frame=CGRectMake(25, CGRectGetMaxY(txt_ReferralID.frame)+1, WIDTH-50, 1);
    }
    img_Rerralidline.backgroundColor=[UIColor lightGrayColor];
    [img_Rerralidline setUserInteractionEnabled:YES];
    [img_BackgroundImg addSubview:img_Rerralidline];
    
    
    
    
    
    
    UIImageView *img_btnsignup=[[UIImageView alloc]init];
    //    img_user.frame=CGRectMake(20, CGRectGetMaxY(img_logo.frame)+70, WIDTH-40, 1);
    if (IS_IPHONE_6Plus) {
        img_btnsignup.frame=CGRectMake(20, CGRectGetMaxY(txt_ReferralID.frame)+50, WIDTH-40, 60);
    }
    else if (IS_IPHONE_6) {
        img_btnsignup.frame=CGRectMake(20, CGRectGetMaxY(txt_ReferralID.frame)+40, WIDTH-40, 60);
    }
    else
    {
        img_btnsignup.frame=CGRectMake(20, CGRectGetMaxY(txt_ReferralID.frame)+40, WIDTH-40, 55);
    }
    img_btnsignup.image=[UIImage imageNamed:@"img-signup@2x.png"];
    //    img_btnlogin.backgroundColor=[UIColor lightGrayColor];
    [img_btnsignup setUserInteractionEnabled:YES];
    [img_BackgroundImg addSubview:img_btnsignup];
   
    UIButton *btn_login =[UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPHONE_6Plus) {
        btn_login.frame=CGRectMake(20, CGRectGetMaxY(txt_ReferralID.frame)+50, WIDTH-40, 60);
    }
    else if (IS_IPHONE_6) {
        btn_login.frame=CGRectMake(20, CGRectGetMaxY(txt_ReferralID.frame)+40, WIDTH-40, 60);
        
    }
    else
    {
        btn_login.frame=CGRectMake(20, CGRectGetMaxY(txt_ReferralID.frame)+40, WIDTH-40, 50);
        
    }
    [btn_login addTarget:self action:@selector(click_Loginbtn) forControlEvents:UIControlEventTouchUpInside];
    btn_login.backgroundColor=[UIColor clearColor];
    [img_BackgroundImg addSubview:btn_login];
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}
-(void)click_Done
{
    [txt_ReferralID resignFirstResponder];
    
}
-(void)Back_btnClick
{
    NSLog(@"Back_btnClick");
//    [self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}
-(void)click_Loginbtn
{
//   ChefSignup *chefsignup=[[ChefSignup alloc]init];
////     [self.navigationController pushViewController:chefsignup animated:NO];
//    [self presentViewController:chefsignup animated:NO completion:nil];
//

    NSLog(@"click_Loginbtn");
    
    if ([txt_username.text isEqualToString:@""])
    {
              [self popup_Alertview:@"Please enter Username"];
        
    }else if ([txt_username.text length]<4)
    {
        [self popup_Alertview:@"Username should be minimum four characters"];
    }
    else if ([txt_Password.text isEqualToString:@""])
    {
             [self popup_Alertview:@"Please enter Password"];
    }
    
    else if ([txt_Password.text length]<8)
    {
        txt_Password.text=@"";
        [self popup_Alertview:@"Password should be alphanumeric and minimum Eight characters"];
    }
    else if ([txt_ReferralID.text length]>16)
    {
        [self popup_Alertview:@"Referal id should be lessthan 16 digits"];
    }
    else if ([txt_conformpassword.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Please enter Confirm Password"];
    }

    else if(![self ValidateConfirmPassword])
    {
//         txt_conformpassword.text=@"";
//        [self popup_Alertview:@"Password should be alphanumeric and minimum eight characters"];
        
       
    }
    else
    {
        [self AFChefSignUp];
    }

  }
//
//#pragma mark ValidateConfirmPassword
//
//-(BOOL)ValidateConfirmPassword
//{
//    BOOL Password_Flag = FALSE;
//    
//    if(!([[txt_Password text] length] == [[txt_conformpassword text] length]))
//    {
//        [txt_Password setText:@""];
//        [txt_conformpassword setText:@""];
//        [self popup_Alertview:@"Your password and confirm password doesn't match"];
//    }
//    else {
//        for(int x = 0; x < [[txt_Password text] length];x++)
//        {
//            if([txt_Password.text characterAtIndex:x] != [txt_conformpassword.text characterAtIndex:x])
//            {
//                [txt_Password setText:@""];
//                [txt_conformpassword setText:@""];
//                [self popup_Alertview:@"Your password and confirm password doesn't match"];
//                Password_Flag = NO;
//                break;
//            }
//            else {
//                Password_Flag = YES;
//            }
//        }
//    }
//    return  Password_Flag;
//}


#pragma mark Alertview Popup


-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    
    if ([str_success isEqualToString:@"SUCESS"])
    {
        ChefSignup *chefsignup=[[ChefSignup alloc]init];
        [self presentViewController:chefsignup animated:NO completion:nil];

    }
    [alertviewBg removeFromSuperview];
    
    
    
}


#pragma mark TextField Delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == txt_username)
{
    [imgView_usernameStatus setImage:[UIImage imageNamed:@""]];
    NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
    return [string isEqualToString:filtered];
    
}else if (textField == txt_Password)
{
    [imgView_PasswordStatus setImage:[UIImage imageNamed:@""]];
    NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
    return [string isEqualToString:filtered];
    
}
else if (textField == txt_ReferralID)
{
    [imgView_PasswordStatus setImage:[UIImage imageNamed:@""]];
    NSString *stringVal = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered =[[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    if (stringVal.length<17 && [string isEqualToString:filtered])
    {
        return YES;
    }
    else{
        return NO;
    }
    return [string isEqualToString:filtered];
    
}
    
    

    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    
    if ([textField isEqual:txt_Password])
    {
        imgView_PasswordStatus.hidden = YES;
    }
    else if ([textField isEqual:txt_conformpassword])
    {
        imgView_ConfirmPasswordStatus.hidden=YES;

    }
}



-(void)textFieldDidEndEditing:(UITextField *)textField
{
  
//     if (textField==txt_username)
//    {
//        if (txt_username.text.length>3)
//        {
//            imgView_usernameStatus.image=[UIImage imageNamed:@"img-conf-pass@2x.png"];
//        }
//        else if (txt_username.text.length>0&&txt_username.text.length<=3)
//        {
//            [imgView_usernameStatus setImage:[UIImage imageNamed:@"img-pass@2x.png"]];
//            
//        }
//        else if (txt_username.text.length==0)
//        {
//            [imgView_usernameStatus setImage:[UIImage imageNamed:@""]];
//            
//        }
//    }
     if (textField==txt_Password&&txt_Password.text.length>0)
     {
         if ([self ValidatePassword])
         {
             imgView_PasswordStatus.hidden = NO;
             imgView_ConfirmPasswordStatus.hidden=NO;

         }
         if (txt_conformpassword.text.length>0)
         {
             [self ValidateConfirmPassword];
         }
     }
     else if (textField == txt_conformpassword && txt_conformpassword.text.length>0 && txt_Password.text.length>0)
     {
         if ([self ValidateConfirmPassword])
         {
             imgView_PasswordStatus.hidden = NO;
              imgView_ConfirmPasswordStatus.hidden=NO;
         }
     }


        CGRect viewFrame = self.view.frame;
        viewFrame.origin.y += animatedDistance;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
        [self.view setFrame:viewFrame];
        [UIView commitAnimations];
}
-(BOOL)ValidateConfirmPassword
{
    [self.view endEditing:YES];
    
    BOOL Password_Flag = FALSE;
    
    if(!([[txt_Password text] length] == [[txt_conformpassword text] length]))
    {
        imgView_PasswordStatus.hidden=YES;
        imgView_ConfirmPasswordStatus.hidden=YES;
        [txt_Password setText:@""];
        [txt_conformpassword setText:@""];
        [self popup_Alertview:@"Your password and confirm password does not match"];
    }
    else {
        for(int x = 0; x < [[txt_Password text] length];x++)
        {
            if([txt_Password.text characterAtIndex:x] != [txt_conformpassword.text characterAtIndex:x])
            {
                imgView_PasswordStatus.hidden=YES;
                imgView_ConfirmPasswordStatus.hidden=YES;
                [txt_Password setText:@""];
                [txt_conformpassword setText:@""];
                [self popup_Alertview:@"Your password and confirm password does not match"];
                Password_Flag = NO;
                break;
            }
            else
            {
                Password_Flag = YES;
            }
        }
    }
    if (Password_Flag == YES)
    {
        [imgView_PasswordStatus setImage:[UIImage imageNamed:@"icon-currect@2x.png"]];
        [imgView_ConfirmPasswordStatus setImage:[UIImage imageNamed:@"icon-currect@2x.png"]];
    }
    else
    {
        [imgView_PasswordStatus setImage:[UIImage imageNamed:@"icon-cross@2x.png"]];
        [imgView_ConfirmPasswordStatus setImage:[UIImage imageNamed:@"icon-cross@2x.png"]];
    }
    return  Password_Flag;
}




-(BOOL)ValidatePassword
{
    [self.view endEditing:YES];
    BOOL letterCharacter = 0;
    BOOL numberCharacter = 0;
    BOOL passwordcorrect = 0;
    
    
    if([txt_Password.text length] >= 8)
    {
        for (int i = 0; i < [txt_Password.text length]; i++)
        {
            unichar c = [txt_Password.text characterAtIndex:i];
            
            if(!letterCharacter)
            {
                letterCharacter = [[NSCharacterSet letterCharacterSet] characterIsMember:c];
            }
            if(!numberCharacter)
            {
                NSString *numberCharacterString = @"0123456789'";
                numberCharacter = [[NSCharacterSet characterSetWithCharactersInString:numberCharacterString] characterIsMember:c];
            }
        }
        if(letterCharacter && numberCharacter)
        {
            passwordcorrect = 1;
            
            return YES;

        }
        else
        {
            txt_Password.text = @"";
            txt_conformpassword.text = @"";
            
            [self popup_Alertview:@"Password should be alphanumeric and minimum eight characters"];
        }
    }
    else
    {
        txt_Password.text = @"";
        txt_conformpassword.text = @"";
        [self popup_Alertview:@"Password should be alphanumeric and minimum eight characters"];
    }
    
    if (passwordcorrect ==1)
    {
      [imgView_PasswordStatus setImage:[UIImage imageNamed:@"img-conf-pass@2x.png"]];

    }
   
    return NO;
}



# pragma mark ChefSignup method

-(void) AFChefSignUp
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            @"username"                         :   txt_username.text,
                            @"password"                         :   txt_Password.text,
                            @"confirm_password"                 :   txt_conformpassword.text,
                            @"field_referal_id"                 :   txt_ReferralID.text,
                            @"role_type"                        :   @"chef_profile",
                            @"chef_type_id"                     :   @"",
                            @"device_udid"                      :   UniqueAppID,
                            @"device_token"                     :   @"Dev",
                            @"device_type"                      :   @"1"
                           
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:KChefSignUp
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUp:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                             
                                             NSLog(@"Successfully Registered");
                                             [self AFChefSignUp];
                                         }
                                     }];
    [operation start];
    
}




-(void) ResponseChefSignUp :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
        [defaults1 removeObjectForKey:@"UserInfo"];
        [defaults1 synchronize];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[TheDict valueForKey:@"profile_info"]  forKey:@"UserInfo"];
        [defaults synchronize];
        
        str_success = @"SUCESS";
        
        [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
        
        
     
    }
    

else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
       
        txt_username.text = @"";
        txt_Password.text = @"";
        txt_conformpassword.text=@"";
        txt_ReferralID.text=@"";
        imgView_PasswordStatus.hidden=YES;
        imgView_ConfirmPasswordStatus.hidden=YES;

        
        
        [self popup_Alertview:[TheDict valueForKey:@"message"]];
        

        
    }
    
}


#pragma mark - AF SocialLogin

-(void) AFSocialLogin
{
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    
    
    //=================================================================BASE URL
    NSURL *url = [NSURL URLWithString:@""];
    
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]])
    {
        str_device_token = @"dev-1234";
    }
    
    NSDictionary *params =@{
                            
                            @"device_udid"           : UniqueAppID,
                            @"device_type"           : @"1",
                            @"device_token"          : str_device_token,
                            @"role_type"             : @"chef_profile",
                            @"username"              : [[array_Facebook_Details objectAtIndex:0] valueForKey:@"id"],
                            @"fbid"                   : [[array_Facebook_Details objectAtIndex:0] valueForKey:@"name"],
                            @"push_notificaiton"           : @"1",
                            
                            
                            };
    //  =================================================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //=================================================================USED FOR UNIQUE FILE NAME AND CONVERT TO DATA
    
    
    //REQUEST WITH FILE
    
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"webservices/fb-login.json" parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[array_Facebook_Details objectAtIndex:0]valueForKey:@"profilepic"]]];
        
        NSTimeInterval timeInterval = [NSDate timeIntervalSinceReferenceDate];
        NSString *photoName=[NSString stringWithFormat:@"%lf-Photo.jpeg",timeInterval];
        
        [formData appendPartWithFileData:data name:@"profile_img" fileName:photoName mimeType:@"image/jpeg"];
    }];
    
    //============================================================RESPONSE
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
         NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
     }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseAFSocialLogin:JSON];
    }
     //=================================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Metta"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001)
                                         {
                                             [self AFSocialLogin];
                                         }
                                     }];
    
    [operation start];
}


-(void) ResponseAFSocialLogin :(NSDictionary * ) TheDict
{
    
    
    //    NSLog(@"SocialLogin%@",TheDict);
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]]isEqualToString:@"0"])
    {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[TheDict valueForKey:@"user_info"]  forKey:@"UserInfo"];
        [defaults synchronize];
        
        
        //[self AFSocialUserCheck];
        
        
        
    }
    else if([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]]isEqualToString:@"1"])
    {
        if (![[TheDict valueForKey:@"user_info"] isKindOfClass:[NSNull class]])
        {
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[TheDict valueForKey:@"user_info"]  forKey:@"UserInfo"];
            [defaults synchronize];
            
            delegate.isFBLogin=YES;
            
            //            SignupViewController *vc = [[SignupViewController alloc] init];
            //            vc.ary_fblogindetails = ary_SocialInfo;
            //            [self.navigationController pushViewController:vc animated:NO];
            
        }
        if (![[TheDict valueForKey:@"user_info"] isKindOfClass:[NSNull class]])
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[TheDict valueForKey:@"user_info"]  forKey:@"UserInfo"];
            [defaults synchronize];
        }
    }
}



@end
