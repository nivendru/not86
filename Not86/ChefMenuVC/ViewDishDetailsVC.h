//
//  ViewDishDetailsVC.h
//  Not86
//
//  Created by User on 01/12/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewDishDetailsVC : UIViewController
{
    
}

@property(nonatomic,strong)NSString*str_dishID;
@property(nonatomic,strong)NSString*str_dishUID;
@property(nonatomic,strong)NSString*str_TYPE;

+ (BOOL) isAppInstalled;


@end
