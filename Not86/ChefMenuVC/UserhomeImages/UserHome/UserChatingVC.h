//
//  UserChatingVC.h
//  Not86
//
//  Created by User on 12/10/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"

@interface UserChatingVC : JWSlideMenuViewController

@property(nonatomic,strong)NSString*str_receiverID;
@property(nonatomic,strong)NSString*str_nid;
@property(nonatomic,strong)NSString*str_orderID;
@property(nonatomic,strong)NSString*str_name;
@property(nonatomic,strong)NSString*str_title;

@end
