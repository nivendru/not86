//
//  PersonalDetailsVC.m
//  Not86
//
//  Created by Admin on 02/09/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "PersonalDetailsVC.h"
#import "ProceedToPaymentVC.h"
#import "Define.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"


@interface PersonalDetailsVC ()<UIScrollViewDelegate,UITextViewDelegate>
{
    UIImageView *img_header;
    UIScrollView * scroll ;
    UITextView *txt_view_for_user_description;
    UITextView * txt_view_for_special_reasons;
    
    CGFloat	animatedDistance;
}

@end

@implementation PersonalDetailsVC
@synthesize ary_totaldetailarray;


static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
     self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [self integrateHeader];
    [self integrateBodyDesign];
}

-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [icon_back addTarget:self action:@selector(click_on_back_arrow:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:icon_back];
    
    
    UILabel *lbl_order_details = [[UILabel alloc]init];
    lbl_order_details.frame = CGRectMake(CGRectGetMaxX(icon_back .frame)+30,0, 200, 45);
    lbl_order_details.text = @"Personal Details";
    lbl_order_details.font = [UIFont fontWithName:kFont size:20];
    lbl_order_details.textColor = [UIColor whiteColor];
    lbl_order_details.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_order_details];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}
-(void)integrateBodyDesign
{
    scroll = [[UIScrollView alloc]init];
    scroll.frame = CGRectMake(5, 48, WIDTH-10, HEIGHT-120);
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor clearColor];
    // scroll.frame = CGRectMake(0, 46, WIDTH, HEIGHT+150);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    
    UIImageView *img_bg = [[UIImageView alloc]init];
    img_bg.frame = CGRectMake(0, 0, WIDTH-5, 440);
    [img_bg setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg];

    
    
    UILabel *lbl_user_description = [[UILabel alloc]init];
    lbl_user_description.frame = CGRectMake(20,5, 200, 45);
    lbl_user_description.text = @"User Description";
    lbl_user_description.font = [UIFont fontWithName:kFontBold size:16];
    lbl_user_description.textColor = [UIColor blackColor];
    lbl_user_description.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_user_description];
    
    UILabel *lbl_optional = [[UILabel alloc]init];
    lbl_optional.frame = CGRectMake(CGRectGetMidX(lbl_user_description .frame)+28,5, 100, 45);
    lbl_optional.text = @"(Optional)";
    lbl_optional.font = [UIFont fontWithName:kFont size:11];
    lbl_optional.textColor = [UIColor blackColor];
    lbl_optional.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_optional];
    
    txt_view_for_user_description =[[UITextView alloc]init];
    txt_view_for_user_description .frame = CGRectMake(20, CGRectGetMaxY(lbl_user_description.frame), WIDTH-45, 130);
    txt_view_for_user_description.scrollEnabled=YES;
    txt_view_for_user_description.text =@"Enter abrief description about yourself for chefs\nto view when you order food or make a request.\nGetting to know you a little better will make chefs\nmore inclined to accept your requests. your\ndescription will be saved in your profile page\n which you can edit at any time.";
    //txt_view_for_user_description.numberOfLines = 5
    txt_view_for_user_description.userInteractionEnabled=YES;
    txt_view_for_user_description.font=[UIFont fontWithName:kFont size:14];
    txt_view_for_user_description.backgroundColor=[UIColor whiteColor];
    txt_view_for_user_description.delegate=self;
    txt_view_for_user_description.textColor=[UIColor lightGrayColor];
    txt_view_for_user_description.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    txt_view_for_user_description.layer.borderWidth=1.0f;
    txt_view_for_user_description.clipsToBounds=YES;
    [img_bg addSubview:txt_view_for_user_description];
     //text_on_popup_bg.numberOfLines = 4;
    
    UILabel *lbl_max_wordes = [[UILabel alloc]init];
    lbl_max_wordes.frame = CGRectMake(WIDTH-120,CGRectGetMaxY( txt_view_for_user_description.frame)+5, 100, 25);
    lbl_max_wordes.text = @"(100 words max.)";
    lbl_max_wordes.font = [UIFont fontWithName:kFont size:11];
    lbl_max_wordes.textColor = [UIColor blackColor];
    lbl_max_wordes.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_max_wordes];
    
    UILabel *lbl_reasons = [[UILabel alloc]init];
    lbl_reasons.frame = CGRectMake(20,CGRectGetMaxY(lbl_max_wordes.frame)+5, WIDTH-40, 48);
    lbl_reasons.text = @"Any special reason you are making this \nrequest";
    lbl_reasons.numberOfLines = 2;
    lbl_reasons.font = [UIFont fontWithName:kFontBold size:14];
    lbl_reasons.textColor = [UIColor blackColor];
    lbl_reasons.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_reasons];
    
    UILabel *lbl_optional2 = [[UILabel alloc]init];
    lbl_optional2.frame = CGRectMake(CGRectGetMinX(lbl_reasons.frame)+63,CGRectGetMaxY(lbl_max_wordes.frame)+35, 100, 45);
    lbl_optional2.text = @"(Optional)";
    lbl_optional2.font = [UIFont fontWithName:kFont size:11];
    lbl_optional2.textColor = [UIColor blackColor];
    lbl_optional2.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_optional2];
    
     txt_view_for_special_reasons =[[UITextView alloc]init];
     txt_view_for_special_reasons .frame = CGRectMake(20, CGRectGetMaxY(lbl_reasons.frame), WIDTH-45, 120);
     txt_view_for_special_reasons.scrollEnabled=YES;
     txt_view_for_special_reasons.text =@"A wedding anniversary, a birthday, a celebration\nfollowing a promotion, anything at all that will make the chef more inclined to accept your request.";
     txt_view_for_special_reasons.userInteractionEnabled=YES;
     txt_view_for_special_reasons.font=[UIFont fontWithName:kFont size:14];
     txt_view_for_special_reasons.backgroundColor=[UIColor whiteColor];
     txt_view_for_special_reasons.delegate=self;
     txt_view_for_special_reasons.textColor=[UIColor lightGrayColor];
     txt_view_for_special_reasons.layer.borderColor=[[UIColor lightGrayColor]CGColor];
     txt_view_for_special_reasons.layer.borderWidth=1.0f;
     txt_view_for_special_reasons.clipsToBounds=YES;
    [img_bg addSubview: txt_view_for_special_reasons];
    //text_on_popup_bg.numberOfLines = 4;

    UILabel *lbl_max_wordes2 = [[UILabel alloc]init];
    lbl_max_wordes2.frame = CGRectMake(WIDTH-120,CGRectGetMaxY(txt_view_for_special_reasons.frame)+5, 100, 45);
    lbl_max_wordes2.text = @"(50 words max.)";
    lbl_max_wordes2.font = [UIFont fontWithName:kFont size:11];
    lbl_max_wordes2.textColor = [UIColor blackColor];
    lbl_max_wordes2.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_max_wordes2];
    
    
    UIImageView *img_proced_to_payments = [[UIImageView alloc]init];
    img_proced_to_payments.frame = CGRectMake(25,HEIGHT-55, WIDTH-50,45);
    [img_proced_to_payments setImage:[UIImage imageNamed:@"button-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_proced_to_payments setUserInteractionEnabled:YES];
    [self.view addSubview:img_proced_to_payments];
    
    UILabel *lbl_proced_to_payment = [[UILabel alloc]init];
    lbl_proced_to_payment.frame = CGRectMake(0,0, WIDTH-50, 45);
    lbl_proced_to_payment.text = @"PROCEED TO PAYMENT DETAILS";
    lbl_proced_to_payment.font = [UIFont fontWithName:kFont size:17];
    lbl_proced_to_payment.textColor = [UIColor whiteColor];
    lbl_proced_to_payment.backgroundColor = [UIColor clearColor];
    lbl_proced_to_payment.textAlignment = NSTextAlignmentCenter;
    [img_proced_to_payments addSubview:lbl_proced_to_payment];
    
    UIButton *btn_on_img_proced_to_payments = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_img_proced_to_payments.frame = CGRectMake(25,HEIGHT-55, WIDTH-50,45);
    btn_on_img_proced_to_payments .backgroundColor = [UIColor clearColor];
   // [btn_on_img_proced_to_payments setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_img_proced_to_payments addTarget:self action:@selector(click_on_proceed_btn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:btn_on_img_proced_to_payments];

    



    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}

#pragma mark TextField Delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //    if(textField == txt_username)
    //    {
    //        if ([string isEqualToString:@" "])
    //        {
    //            return NO;
    //        }
    //    }
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TextField Delegate methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    if ( txt_view_for_user_description .text.length>500)
    {
        return NO;
    }
    else if (txt_view_for_special_reasons.text.length>500)
    {
        return NO;
        
    }
    
    
    return YES;
}
#pragma click_events
-(void)click_on_back_arrow:(UIButton *)sender
{
    NSLog(@"click_on_back:");
    [self dismissViewControllerAnimated:NO completion:nil];
}
-(void)click_on_proceed_btn:(UIButton *)sender
{
    NSLog(@"click_on_proceed_btn");
    ProceedToPaymentVC*vc = [ProceedToPaymentVC new];
    [self presentViewController:vc animated:NO completion:nil];
    
}




//-(void)AFupdatecart
//{
//    
//    [self.view addSubview:delegate.activityIndicator];
//    [delegate.activityIndicator startAnimating];
//    
//    //=================================================================BASE URL
//    
//    NSURL *url = [NSURL URLWithString:kBaseUrl];
//    
//    //=================================================================USED PARAMETERS(ONLY TEXT)
//    
//    
//    
//    
//    NSDictionary *params =@{
//                            @"uid"                         :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
//                            @"cart_id"                         :  str_tosend,
//                            @"user_description"                         : txt_view_for_user_description.text ,
//                            @"reason_request"                         :  txt_view_for_special_reasons.text,
//                            @"type"                     : @"0"
//                            };
//    
//    
//    //===========================================AFNETWORKING HEADER
//    
//    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
//    httpClient.parameterEncoding = AFFormURLParameterEncoding;
//    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
//    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
//    
//    //===============================SIMPLE REQUEST
//    
//    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
//                                                            path:Kupdatecart
//                                                      parameters:params];
//    
//    
//    //====================================================RESPONSE
//    
//    
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
//    
//    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
//        
//    }];
//    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSError *error = nil;
//        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
//        
//        [delegate.activityIndicator stopAnimating];
//        [self ResponseChefServeNow:JSON];
//    }
//     
//     //==================================================ERROR
//                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                         
//                                         
//                                         [delegate.activityIndicator stopAnimating];
//                                         
//                                         if([operation.response statusCode] == 406){
//                                             
//                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
//                                             return;
//                                         }
//                                         
//                                         if([operation.response statusCode] == 403){
//                                             NSLog(@"Upload Failed");
//                                             return;
//                                         }
//                                         if ([[operation error] code] == -1009) {
//                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
//                                                                                          message:@"Please check your internet connection"
//                                                                                         delegate:nil
//                                                                                cancelButtonTitle:@"OK"
//                                                                                otherButtonTitles:nil];
//                                             [av show];
//                                         }
//                                         else if ([[operation error] code] == -1001) {
//                                             
//                                             NSLog(@"Successfully Registered");
//                                             [self AFupdatecart];
//                                         }
//                                     }];
//    [operation start];
//    
//}
//-(void) ResponseChefServeNow :(NSDictionary * )TheDict
//{
//    NSLog(@"Login: %@",TheDict);
//    
//    
//    
//    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
//    {
//        
//        //[self  popup_Alertview:[TheDict valueForKey:@"message"]];
//        PersonalDetailsVC*vc= [PersonalDetailsVC new];
//        
//        [self presentViewController:vc animated:NO completion:nil];
//        
//    }
//    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
//    {
//        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
//        
//        
//    }
//    
//}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
@end
