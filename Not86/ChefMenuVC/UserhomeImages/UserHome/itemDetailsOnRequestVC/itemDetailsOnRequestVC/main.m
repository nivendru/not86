//
//  main.m
//  itemDetailsOnRequestVC
//
//  Created by Interwld on 9/10/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
