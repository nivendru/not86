//
//  DineInFoodNowVC.m
//  Not86
//
//  Created by Interworld on 02/12/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "DineInFoodNowVC.h"
#import "DineInCheckOutVC.h"
#import "AppDelegate.h"

@interface DineInFoodNowVC ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIImageView *img_header;
    UIImageView *img_background;
    UITableView *img_table;
    NSMutableArray *array_total_Items;
    
    AppDelegate *delegate;
    NSMutableArray * array_Amount;
   
    NSMutableArray*ary_dummykithennames;
    
}
@end

@implementation DineInFoodNowVC
@synthesize str_Type;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ary_dummykithennames =[[NSMutableArray alloc]init];

    array_total_Items = [NSMutableArray new];
    array_Amount = [NSMutableArray new];
    
    [self AFCartDetails];
    
    [self integrateHeader];
    [self integrateBody];
    
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}


-(void)integrateHeader
{
    
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu .frame = CGRectMake(10, 13,25,20);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu  addTarget:self action:@selector(click_on_Back_Btn:)forControlEvents:UIControlEventTouchUpInside];
    [icon_menu setImage:[UIImage imageNamed:@"img_arrow@2x"] forState:UIControlStateNormal];
    //    icon_menu.layer.borderWidth = 1.0;
    [icon_menu setBackgroundColor:[UIColor clearColor]];
    [img_header   addSubview:icon_menu ];
    
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+20,0, 150, 45);
    lbl_User_Sign_Up.text = @"Dining Cart";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-35, 9, 27, 27);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
    
}


-(void)integrateBody
{
    
    UIImageView *img_bg = [[UIImageView alloc]init];
    img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 440);
    //    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    //img_bg.layer.borderWidth = 1.0;
    [img_bg setUserInteractionEnabled:YES];
    [self.view addSubview:img_bg];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(10, 10, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"icon-now@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_bg addSubview:icon_user];
    
    
    UILabel *lbl_Head = [[UILabel alloc]init];
    lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
    lbl_Head.text = @"Food Now";
    lbl_Head.font = [UIFont fontWithName:kFont size:20];
    lbl_Head.textColor = [UIColor blackColor];
    lbl_Head.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_Head];
    
    
    UIImageView *line = [[UIImageView alloc]init];
    line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
    line.backgroundColor = [UIColor grayColor];
    [line setUserInteractionEnabled:YES];
    [img_bg addSubview:line];
    
    
    img_table= [[UITableView alloc] init ];
    img_table.frame  = CGRectMake(0,CGRectGetMaxY(line.frame),WIDTH-10,390);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    //    img_table.layer.borderWidth = 1.0;
    img_table.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:img_table];
    
    
    UIButton *btn_MoreFood = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_MoreFood.frame = CGRectMake(30,CGRectGetMaxY(img_bg.frame)+20,(WIDTH-75)/2.0,45);
    btn_MoreFood.layer.cornerRadius=4.0f;
    [btn_MoreFood setTitle:@"More Food" forState:UIControlStateNormal];
    [btn_MoreFood setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_MoreFood setBackgroundColor:[UIColor colorWithRed:39.0/255.0 green:37.0/255.0 blue:48.0/255.0 alpha:1.0f]];
    btn_MoreFood.titleLabel.font=[UIFont fontWithName:kFont size:14];
    [btn_MoreFood setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_MoreFood addTarget:self action:@selector(btn_MoreFood_Method:) forControlEvents:UIControlEventTouchUpInside];
    [self.view  addSubview:btn_MoreFood];
    
    
    UIButton *btn_CheckOut = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_CheckOut.frame = CGRectMake(CGRectGetMaxX(btn_MoreFood.frame)+15,CGRectGetMaxY(img_bg.frame)+20,(WIDTH-75)/2.0,45);
    btn_CheckOut.layer.cornerRadius=4.0f;
    [btn_CheckOut setTitle:@"Check Out" forState:UIControlStateNormal];
    [btn_CheckOut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_CheckOut setBackgroundColor:[UIColor colorWithRed:39.0/255.0 green:37.0/255.0 blue:48.0/255.0 alpha:1.0f]];
    btn_CheckOut.titleLabel.font=[UIFont fontWithName:kFont size:14];
    [btn_CheckOut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_CheckOut addTarget:self action:@selector(btn_CheckOut_Method:) forControlEvents:UIControlEventTouchUpInside];
    [self.view  addSubview:btn_CheckOut];
    
    
    
    if (IS_IPHONE_6Plus)
    {
        img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 600);
        icon_user.frame = CGRectMake(10, 10, 30, 30);
        lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
        line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
        img_table.frame  = CGRectMake(0,CGRectGetMaxY(line.frame),WIDTH-10,550);
        btn_MoreFood.frame = CGRectMake(30,CGRectGetMaxY(img_bg.frame)+25,(WIDTH-75)/2.0,50);
        btn_CheckOut.frame = CGRectMake(CGRectGetMaxX(btn_MoreFood.frame)+15,CGRectGetMaxY(img_bg.frame)+25,(WIDTH-75)/2.0,50);
    }
    else if (IS_IPHONE_6)
    {
        img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 540);
        icon_user.frame = CGRectMake(10, 10, 30, 30);
        lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
        line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
        img_table.frame  = CGRectMake(0,CGRectGetMaxY(line.frame),WIDTH-10,490);
        btn_MoreFood.frame = CGRectMake(30,CGRectGetMaxY(img_bg.frame)+20,(WIDTH-75)/2.0,50);
        btn_CheckOut.frame = CGRectMake(CGRectGetMaxX(btn_MoreFood.frame)+15,CGRectGetMaxY(img_bg.frame)+20,(WIDTH-75)/2.0,50);
    }
    else if (IS_IPHONE_5)
    {
        img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 440);
        icon_user.frame = CGRectMake(10, 10, 30, 30);
        lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
        line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
        img_table.frame  = CGRectMake(0,CGRectGetMaxY(line.frame),WIDTH-10,390);
        btn_MoreFood.frame = CGRectMake(30,CGRectGetMaxY(img_bg.frame)+20,(WIDTH-75)/2.0,45);
        btn_CheckOut.frame = CGRectMake(CGRectGetMaxX(btn_MoreFood.frame)+15,CGRectGetMaxY(img_bg.frame)+20,(WIDTH-75)/2.0,45);
    }
    else
    {
        img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 360);
        icon_user.frame = CGRectMake(10, 10, 30, 30);
        lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
        line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
        img_table.frame  = CGRectMake(0,CGRectGetMaxY(line.frame),WIDTH-10,310);
        btn_MoreFood.frame = CGRectMake(30,CGRectGetMaxY(img_bg.frame)+20,(WIDTH-75)/2.0,40);
        btn_CheckOut.frame = CGRectMake(CGRectGetMaxX(btn_MoreFood.frame)+15,CGRectGetMaxY(img_bg.frame)+20,(WIDTH-75)/2.0,40);
    }
    
}

-(void)click_on_Back_Btn: (UIButton *)sender

{
    NSLog(@"click_on_Back_Btn Click");
    [self dismissViewControllerAnimated:NO completion:nil];
}



-(void)btn_MoreFood_Method: (UIButton *)sender

{
    NSLog(@"btn_MoreFood_Method: Click");
    DineInCheckOutVC *objDineInFN = [DineInCheckOutVC new];
    [self presentViewController:objDineInFN animated:NO completion:nil];
    
}

-(void)btn_CheckOut_Method: (UIButton *)sender

{
    NSLog(@"btn_CheckOut_Method: Click");
    DineInCheckOutVC *objDineInFN = [DineInCheckOutVC new];
    objDineInFN.ary_dinindetails = array_total_Items;
    objDineInFN.ary_amountdetails = array_Amount;
    objDineInFN.str_servetype = str_Type;
    [self presentViewController:objDineInFN animated:NO completion:nil];
    
}

#pragma mark TableviewDelegate&Datasources

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 2;
//}

//-(CGFloat)tableView: (UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 45.0;
//}


//-(UIView *)tableView : (UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//
//    UIView*sectionView;
//    sectionView = [[UIView alloc]init];
//    sectionView.frame = CGRectMake(0, 0, WIDTH-10, 45);
//    sectionView.backgroundColor = [UIColor whiteColor];
////    sectionView.layer.borderWidth = 1.0;
//
//    UILabel *firstLabel = [[UILabel alloc]init];
//    firstLabel.frame = CGRectMake(15, 5, WIDTH-10, 35);
//    firstLabel.backgroundColor = [UIColor clearColor];
//        firstLabel.text = @"Doe's Kitchen";
////    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
////    firstLabel.textAlignment = NSTextAlignmentCenter;
//    firstLabel.textColor = [UIColor blackColor];
//    firstLabel.font = [UIFont fontWithName:kFontBold size:15];
//    firstLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    firstLabel.numberOfLines = 0;
//    [sectionView addSubview:firstLabel];
//
//
//    UIImageView *Line = [[UIImageView alloc]init];
//    Line.frame = CGRectMake(12, CGRectGetMaxY(firstLabel.frame)-5,(WIDTH-10)/3.0 ,2);
//    Line.backgroundColor = [UIColor blackColor];
//    [sectionView addSubview:Line];
//
//    return sectionView;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [array_total_Items count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 190;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
        for (UIView *view in cell.contentView.subviews)
        {
            [view removeFromSuperview];
        }
    
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-5, 190);
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    img_cellBackGnd.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    //img_cellBackGnd.layer.borderWidth = 1.0;
    [cell.contentView addSubview:img_cellBackGnd];
    
    
    UILabel *firstLabel = [[UILabel alloc]init];
    firstLabel.frame = CGRectMake(15, 5, WIDTH-10, 30);
    firstLabel.backgroundColor = [UIColor clearColor];
    firstLabel.textColor = [UIColor blackColor];
    firstLabel.font = [UIFont fontWithName:kFontBold size:15];
    firstLabel.lineBreakMode = NSLineBreakByWordWrapping;
    firstLabel.numberOfLines = 0;
    [img_cellBackGnd addSubview:firstLabel];
    
    
    UIImageView *Line = [[UIImageView alloc]init];
    Line.frame = CGRectMake(12, CGRectGetMaxY(firstLabel.frame)-5,(WIDTH-10)/3.0 ,2);
    Line.backgroundColor = [UIColor blackColor];
    [img_cellBackGnd addSubview:Line];
    
   
    
    if (![ary_dummykithennames containsObject:[[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"kitchen_name"]])
    {
        firstLabel.text = [NSString stringWithFormat: @"%@", [[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"kitchen_name"]];
        [ary_dummykithennames addObject:[[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"kitchen_name"]];
        firstLabel.hidden = NO;
        Line.hidden= NO;
    }
    else
    {
        firstLabel.hidden = YES;
        Line.hidden= YES;
        
    }
    
    
    UILabel *cell_head  = [[UILabel alloc]init];
    cell_head.frame = CGRectMake(10, CGRectGetMaxY(firstLabel.frame), WIDTH-40, 20.0);
    //        round_red_val .text = [NSString stringWithFormat:@"%@",[array_round_red_val objectAtIndex:indexPath.row]];
    cell_head .text = [NSString stringWithFormat: @"Requested Serving Date/Time:%@", [[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"requesting_datetime" ]];
    //    cell_head.textAlignment = NSTextAlignmentCenter;
    cell_head .font = [UIFont fontWithName:kFontBold size:14];
    cell_head .textColor = [UIColor blackColor];
    cell_head .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:cell_head];
    
    
    
    UIImageView *img_dish = [[UIImageView alloc] init];
    img_dish.frame = CGRectMake(10,CGRectGetMaxY(cell_head.frame)+5, 80,  80 );
    
    NSString *url_Img = [NSString stringWithFormat: @"%@", [[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"item_image" ]];
    img_dish.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img]]];
    
    //        NSString *ImagePath1 =[[NSString stringWithFormat: @"%@", [[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"item_image" ]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //        [img_dish setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@""]];
    [img_dish setBackgroundColor:[UIColor grayColor]];
    [img_cellBackGnd addSubview:img_dish];
    
    
    
    UILabel *dish_name = [[UILabel alloc]init];
    dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,CGRectGetMaxY(cell_head.frame),200, 30);
    //        dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
    //  dish_name.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"DishName"];
    dish_name.font = [UIFont fontWithName:kFontBold size:14];
    dish_name.text = [NSString stringWithFormat: @"%@", [[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"item_name" ]];
    dish_name.textColor = [UIColor blackColor];
    dish_name.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:dish_name];
    
    
    
    UIImageView *icon_location = [[UIImageView alloc] init];
    icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,CGRectGetMaxY(dish_name.frame)+20,20, 30);
    [icon_location setImage:[UIImage imageNamed:@"location-icon@2x.png"]];
    [img_cellBackGnd  addSubview:icon_location];
    
    
    
    //        UILabel *meters = [[UILabel alloc]init];
    //          meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
    //               meters.text = @"5km";
    //       // meters.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"kitchenDistance"];
    //        //    meters.text = @"Meter";
    //        meters.font = [UIFont fontWithName:kFont size:15];
    //        meters.textColor = [UIColor blackColor];
    //        meters.backgroundColor = [UIColor clearColor];
    //        [img_cellBackGnd addSubview:meters];
    
    
    
    //        layout=[[UICollectionViewFlowLayout alloc] init];
    //        collView_serviceDirectory = [[UICollectionView alloc] initWithFrame:CGRectMake(30,0,WIDTH-65,70)
    //                                                       collectionViewLayout:layout];
    //
    //        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    //        [collView_serviceDirectory setDataSource:self];
    //        [collView_serviceDirectory setDelegate:self];
    //        collView_serviceDirectory.scrollEnabled = YES;
    //        collView_serviceDirectory.showsVerticalScrollIndicator = NO;
    //        collView_serviceDirectory.showsHorizontalScrollIndicator = NO;
    //        collView_serviceDirectory.pagingEnabled = NO;
    //        [collView_serviceDirectory registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    //        [collView_serviceDirectory setBackgroundColor:[UIColor clearColor]];
    //        layout.minimumInteritemSpacing = 0;
    //        layout.minimumLineSpacing = 0;
    //        collView_serviceDirectory.layer.borderWidth = 1.0;
    //        collView_serviceDirectory.userInteractionEnabled = YES;
    //        [img_cellBackGnd addSubview:collView_serviceDirectory];
    
    
    
    //        UIImageView *img1 = [[UIImageView alloc]init];
    //        img1.frame = CGRectMake(WIDTH-120,CGRectGetMaxY(dish_name.frame)+20,30, 30);
    //
    //    //        doller_rate.text = [NSString stringWithFormat:@"$%@",[[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"Dishpprice"]];
    //         img1.layer.borderWidth = 1.0;
    //        [img_cellBackGnd addSubview:img1];
    //
    //
    //    UIImageView *img2 = [[UIImageView alloc]init];
    //    img2.frame = CGRectMake(CGRectGetMaxX(img1.frame)+5,CGRectGetMaxY(dish_name.frame)+20,30, 30);
    //    //        doller_rate.text = [NSString stringWithFormat:@"$%@",[[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"Dishpprice"]];
    //    img2.layer.borderWidth = 1.0;
    //    [img_cellBackGnd addSubview:img2];
    
    
    UIImageView *img3 = [[UIImageView alloc]init];
    img3.frame = CGRectMake(WIDTH-60,CGRectGetMaxY(dish_name.frame)+20,30, 30);
    if ([[NSString stringWithFormat: @"%@", [[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"serve_type" ]]isEqualToString:@"0"])
    {
        img3.image = [UIImage imageNamed:@"dine-in-img@2x"];
    }
    else if ([[NSString stringWithFormat: @"%@", [[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"serve_type" ]]isEqualToString:@"1"])
    {
        img3.image = [UIImage imageNamed:@"take-icon@2x.png"];
        
    }
    else
    {
        img3.image = [UIImage imageNamed:@"deliver-icon@2x.png"];
        
    }
    //        doller_rate.text = [NSString stringWithFormat:@"$%@",[[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"Dishpprice"]];
    //img3.layer.borderWidth = 1.0;
    [img_cellBackGnd addSubview:img3];
    
    
    
    UIImageView *img_line = [[UIImageView alloc] init];
    img_line.frame = CGRectMake(15,CGRectGetMaxY(img_dish.frame)+5, WIDTH-40, 0.5 );
    [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
    [img_cellBackGnd addSubview:img_line];
    
    
    
    UILabel *quantity_Lbl = [[UILabel alloc]init];
    quantity_Lbl.frame = CGRectMake(15,CGRectGetMaxY(img_line.frame),(WIDTH-50)/3.0, 25);
    quantity_Lbl.text = [NSString stringWithFormat: @"Quantity: %@", [[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"quantity" ]];
    // likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
    quantity_Lbl.font = [UIFont fontWithName:kFont size:10];
    quantity_Lbl.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    quantity_Lbl.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:quantity_Lbl];
    
    
    
    UILabel *serving_Lbl = [[UILabel alloc]init];
    serving_Lbl.frame = CGRectMake(CGRectGetMaxX(quantity_Lbl.frame),CGRectGetMaxY(img_line.frame),(WIDTH-50)/3.0, 25);
    serving_Lbl.text = [NSString stringWithFormat: @"$%@/Serving", [[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"price" ]];
    // likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
    serving_Lbl.font = [UIFont fontWithName:kFont size:10];
    serving_Lbl.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    serving_Lbl.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:serving_Lbl];
    
    
    UILabel *likes = [[UILabel alloc]init];
    likes.frame = CGRectMake(CGRectGetMaxX(serving_Lbl.frame),CGRectGetMaxY(img_line.frame),(WIDTH-50)/3.0, 25);
    likes.text = [NSString stringWithFormat: @"Subtotal:$%@", [[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"sub_total" ]];
    // likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
    likes.font = [UIFont fontWithName:kFont size:10];
    likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    likes.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:likes];
    
    
    
    return cell;
    
}

//if(![Arr_temp containsObject:[arrCuisines objectAtIndex:sender.tag]])
//{
//    [Arr_temp addObject:[arrCuisines objectAtIndex:sender.tag]];
//
//}
//else
//{
//    if (Arr_temp.count >4)
//    {
//        [self popup_Alertview:@"Select upto five cuisines"];
//
//    }
//    else{
//        [Arr_temp addObject:[arrCuisines objectAtIndex:sender.tag]];
//
//    }
//}

#pragma mark servixcesfunctionality

-(void)AFCartDetails
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    NSDictionary *params =@{
                            
                            
                            @"uid"            :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"type"            :  str_Type,
                            
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER  kChefAccountSaleSummary
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kUserDiningCart
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseCartDetailsList:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFCartDetails];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseCartDetailsList :(NSDictionary * )TheDict
{
    
    [array_total_Items removeAllObjects];
    [array_Amount removeAllObjects];
    
    NSLog(@"Response Dict: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"cart_details"] count]; i++)
        {
            [array_total_Items addObject:[[TheDict valueForKey:@"cart_details"] objectAtIndex:i]];
            
        }
        
        NSLog(@"array_for_accounts is: %@",array_total_Items);
        
        
        [array_Amount addObject:[TheDict valueForKey:@"Amount_details"]];
        
        NSLog(@"array_Amount is: %@", array_Amount);
        
        //        for (int i = 0; i < [[[TheDict valueForKey:@"OrderData"]valueForKey:@"TotalitemsList"] count]; i++)
        //        {
        //            [array_total_Items addObject:[[[array_for_accounts objectAtIndex:i]valueForKey:@"TotalitemsList"] objectAtIndex:i]];
        //        }
        //        NSLog(@"array_total_Items is: %@",array_total_Items);
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        NSLog(@"Error 1 check response");
        
    }
    
    [img_table reloadData];
    
    //    [self AlergyList];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
