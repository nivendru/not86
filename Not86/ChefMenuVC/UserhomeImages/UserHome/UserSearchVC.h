//
//  UserSearchVC.h
//  Not86
//
//  Created by Admin on 30/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface UserSearchVC : UIViewController<UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate>{
    
    AppDelegate *appDelegate;
    NSMutableArray *searchResults;
    NSMutableArray *ary_Details;
}

@property (strong, nonatomic)  UITableView *Usertableview;
@property (strong, nonatomic)  UISearchBar *searchBar;

@end
