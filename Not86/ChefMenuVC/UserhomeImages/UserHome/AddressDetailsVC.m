//
//  AddressDetailsVC.m
//  Not86
//
//  Created by Admin on 02/09/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "AddressDetailsVC.h"
#import "PersonalDetailsVC.h"
#import "Define.h"
#import "AppDelegate.h"
#import "ProceedToPaymentVC.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"


@interface AddressDetailsVC ()<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UITextFieldDelegate>
{
    UIImageView *img_header;
    UITableView*table_address;
    NSMutableArray*ary_dummykithennames;
    UIView*alertviewBg;
    
    NSString*str_Delivery_Address;
    
    AppDelegate*delegate;
    NSMutableArray*ary_countrylist;
    NSMutableArray*ary_citylist;
    NSMutableArray*ary_maintosave;
    

    UITextField*txt_street;
    UITextField*txt_postal;
    UILabel *lblAdrs1;
    UILabel *lblAdrs2;
    
    UIToolbar*keyboardToolbar_Date;
    UITableView*tableview_Kcountry;
    UITableView*tableview_Kcity;
    
    
    //delivary
    
    UITextField *txt_delevery_company;
    UITextField *txt_mobile_number2;
    UITextField *txt_persone_name;
    UITextField *txt_person_mobile_number;
    UITextField *txt_first_mile_charge;
    UITextField *txt_each_mile_charge;
    UITextField *txt_flat_charge;
    UITextField*txt_tel_code;
    UITextField*txt_tel_code2;
    
    UIButton*btn_on_tel_code_dropdown;
    
    
    NSString*str_delivarypopdefault;
    NSString*str_delivarypopselectall;
    
    UIView*DeliveryPopUpBg;
    UIImageView*img_bg_for_delevery;
    UITableView*table_for_contry_code1;
    
    UIButton*icon_dropdown_for_tel_code2;
    UIButton*btn_on_tel_code_dropdown2;
    UITableView*table_for_contry_code2;
    UIButton*radio_button_for_delivery_charge;
    UIButton*radio_button_for_delivery_charge2;
    UIButton*btn_check_box_for_Apply_All;
    UIButton*btn_check_box_for_setas_default;
    UIButton*btn_Save_Deliverly_Address;
    
    UIButton*bth_cross;
    

}

@end

@implementation AddressDetailsVC
@synthesize ary_dishdivary,ary_dishdivaryamont,str_Type;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    ary_dummykithennames = [NSMutableArray new];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    ary_countrylist = [NSMutableArray new];
    ary_citylist = [NSMutableArray new];
    ary_maintosave= [NSMutableArray new];
    for (int i=0; i<[ary_dishdivary count]; i++)
    {
        NSMutableDictionary *dict1=[[NSMutableDictionary alloc] init];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:i] valueForKey:@"addressdetails"]] forKey:@"addressdetails"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:i] valueForKey:@"cart_id"]]  forKey:@"cart_id"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:i] valueForKey:@"chef_id"]]  forKey:@"chef_id"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:i] valueForKey:@"chef_name"]] forKey:@"chef_name"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:i] valueForKey:@"delivery_address"]] forKey:@"delivery_address"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:i] valueForKey:@"item_dietry_restrictions"]] forKey:@"item_dietry_restrictions"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:i] valueForKey:@"item_id"]] forKey:@"item_id"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:i] valueForKey:@"item_image"]] forKey:@"item_image"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:i] valueForKey:@"item_name"]] forKey:@"item_name"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:i] valueForKey:@"kitchen_name"]] forKey:@"kitchen_name"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:i] valueForKey:@"price"]] forKey:@"price"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:i] valueForKey:@"quantity"]] forKey:@"quantity"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:i] valueForKey:@"request_remarks"]] forKey:@"request_remarks"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:i] valueForKey:@"requesting_datetime"]] forKey:@"requesting_datetime"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:i] valueForKey:@"serve_type"]] forKey:@"serve_type"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:i] valueForKey:@"sub_total"]] forKey:@"sub_total"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:i] valueForKey:@"type"]] forKey:@"type"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:i] valueForKey:@"uid"]] forKey:@"uid"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",@"0"] forKey:@"delivaryidselected"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",@"NO"] forKey:@"tableshow"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",@""] forKey:@"remarks"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",@""] forKey:@"delivaryid"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivaryamont objectAtIndex:0]valueForKey:@"grand_subtotal"]] forKey:@"grand_subtotal"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivaryamont objectAtIndex:0]valueForKey:@"service_fee"]] forKey:@"service_fee"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivaryamont objectAtIndex:0]valueForKey:@"Grand_Total"]] forKey:@"Grand_Total"];
        [dict1 setValue:[NSString  stringWithFormat:@"%@",[[ary_dishdivaryamont objectAtIndex:0]valueForKey:@"grand_delivery_charge"]] forKey:@"grand_delivery_charge"];
        [ary_maintosave addObject:dict1];
    }


    [self integrateHeader];
    [self integratebody];
    
    [table_address reloadData];
    
//[self deliveryDesign];
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self countryList];
    
}

-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [icon_back addTarget:self action:@selector(click_on_back_arrow:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:icon_back];
    
    
    UILabel *lbl_order_details = [[UILabel alloc]init];
    lbl_order_details.frame = CGRectMake(CGRectGetMaxX(icon_back .frame)+30,0, 200, 45);
    lbl_order_details.text = @"Address Details";
    lbl_order_details.font = [UIFont fontWithName:kFont size:20];
    lbl_order_details.textColor = [UIColor whiteColor];
    lbl_order_details.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_order_details];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}

-(void)integratebody
{
    UIImageView *img_bg = [[UIImageView alloc]init];
    img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, HEIGHT-170);
    //    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
   // img_bg.layer.borderWidth = 1.0;
    [img_bg setUserInteractionEnabled:YES];
    [self.view addSubview:img_bg];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(10, 10, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"iicon-on_reuest@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_bg addSubview:icon_user];
    
    
    UILabel *lbl_Head = [[UILabel alloc]init];
    lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
    lbl_Head.text = @"On Request";
    lbl_Head.font = [UIFont fontWithName:kFont size:20];
    lbl_Head.textColor = [UIColor blackColor];
    lbl_Head.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_Head];
    
    
    UIImageView *line = [[UIImageView alloc]init];
    line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
    line.backgroundColor = [UIColor grayColor];
    [line setUserInteractionEnabled:YES];
    [img_bg addSubview:line];
    
    
    table_address= [[UITableView alloc] init ];
    table_address.frame  = CGRectMake(0,CGRectGetMaxY(line.frame),WIDTH-10,HEIGHT-230);
    [table_address setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_address.delegate = self;
    table_address.dataSource = self;
    table_address.showsVerticalScrollIndicator = NO;
    //table_address.layer.borderWidth = 1.0;
    table_address.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:table_address];
    
    UIImageView *img_check_out = [[UIImageView alloc]init];
    img_check_out.frame = CGRectMake(25,HEIGHT-100, WIDTH-50,45);
    [img_check_out setImage:[UIImage imageNamed:@"button-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_check_out setUserInteractionEnabled:YES];
    [self.view addSubview:img_check_out];
    
    UILabel *lbl_check_out = [[UILabel alloc]init];
    lbl_check_out.frame = CGRectMake(0,0,  WIDTH-50, 45);
    lbl_check_out.text = @"PERSONAL DETAILS ";
    [lbl_check_out setUserInteractionEnabled:YES];
    lbl_check_out.font = [UIFont fontWithName:kFont size:18];
    lbl_check_out.textColor = [UIColor whiteColor];
    lbl_check_out.backgroundColor = [UIColor clearColor];
    lbl_check_out.textAlignment = NSTextAlignmentCenter;
    [img_check_out addSubview:lbl_check_out];
    
    UIButton *btn_on_img_check_out = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_img_check_out.frame = CGRectMake(25,HEIGHT-100, WIDTH-50,45);
    btn_on_img_check_out .backgroundColor = [UIColor clearColor];
    // [btn_on_img_proced_to_payments setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_img_check_out addTarget:self action:@selector(click_on_Personldetail:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:btn_on_img_check_out];
    

}


-(void)deliveryDesign
{
    [DeliveryPopUpBg removeFromSuperview];
    DeliveryPopUpBg=[[UIView alloc] init];
    DeliveryPopUpBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    DeliveryPopUpBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    DeliveryPopUpBg.userInteractionEnabled=TRUE;
    [self.view addSubview:DeliveryPopUpBg];
    

        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        img_cellBackGnd.frame =  CGRectMake(10, 100, WIDTH-20,280);
        //           [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor whiteColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        img_cellBackGnd.layer.borderWidth = 1.0;
        [DeliveryPopUpBg addSubview:img_cellBackGnd];
        
    
    
    bth_cross =[UIButton buttonWithType:UIButtonTypeCustom];
    bth_cross.frame=CGRectMake(img_cellBackGnd.frame.size.width-60,10,30,30);
    bth_cross.backgroundColor = [UIColor clearColor];
    [bth_cross addTarget:self action:@selector(cross_Method) forControlEvents:UIControlEventTouchUpInside];
    bth_cross.userInteractionEnabled = YES;
    [bth_cross setImage:[UIImage imageNamed:@"cross-img@2x.png"] forState:UIControlStateNormal];
    [DeliveryPopUpBg  addSubview:bth_cross];

        
        UIImageView *image = [[UIImageView alloc]init];
        image.frame=CGRectMake(10, 30, 30, 30);
        //        if (IS_IPHONE_6Plus) {
        //            //        image.frame=CGRectMake(10, 5, 30, 30);
        //            image.frame=CGRectMake(10, 12, 30, 30);
        //
        //        }else if (IS_IPHONE_6)
        //
        //        {
        //            image.frame=CGRectMake(10, 11, 30, 30);
        //
        //        }
        //        else{
        //            image.frame=CGRectMake(10, 10, 25, 25);
        //
        //        }
        //        image.image = [UIImage imageNamed:[arryFoodImages objectAtIndex:indexPath.row]];
        image.image = [UIImage imageNamed:@"icon-location@2x.png"];
        [img_cellBackGnd addSubview:image];
        
    
        
        UILabel *lblAdrs = [[UILabel alloc]init];
        lblAdrs.frame=CGRectMake(55, 35, 205, 40);
        //        if (IS_IPHONE_6Plus) {
        //            //        lblTitels.frame=CGRectMake(50, 7, 150, 30);
        //            lblTitels.frame=CGRectMake(50, 12, 150, 30);
        //
        //        }else if (IS_IPHONE_6)
        //
        //        {
        //            lblTitels.frame=CGRectMake(50, 11, 150, 30);
        //
        //        }
        //        else{
        //            lblTitels.frame=CGRectMake(50, 10, 150, 25);
        //
        //        }
        //        lblTitels.text = [arryFoodnames objectAtIndex:indexPath.row];
        //lblAdrs.text = [[ary_displayAddress objectAtIndex:indexPath.row] valueForKey:@"City"];
        lblAdrs.font=[UIFont fontWithName:kFont size:12];
        [img_cellBackGnd addSubview:lblAdrs];
    
    
    txt_street = [[UITextField alloc] init];
    txt_street.frame = CGRectMake(55, 35, 205, 40);
    //        if (IS_IPHONE_6Plus) {
    //            //        lblTitels.frame=CGRectMake(50, 7, 150, 30);
    //            lblTitels.frame=CGRectMake(50, 12, 150, 30);
    //
    //        }else if (IS_IPHONE_6)
    //
    //        {
    //            lblTitels.frame=CGRectMake(50, 11, 150, 30);
    //
    //        }
    //        else{
    //            lblTitels.frame=CGRectMake(50, 10, 150, 25);
    //
    //        }
    //        lblTitels.text = [arryFoodnames objectAtIndex:indexPath.row];

    
    txt_street.borderStyle = UITextBorderStyleNone;
    txt_street.font = [UIFont fontWithName:kFont size:13];
    txt_street.placeholder = @"Street Address";
    [txt_street setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_street setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_street.leftViewMode = UITextFieldViewModeAlways;
    txt_street.userInteractionEnabled=YES;
    txt_street.textAlignment = NSTextAlignmentLeft;
    txt_street.backgroundColor = [UIColor clearColor];
    txt_street.keyboardType = UIKeyboardTypeAlphabet;
    txt_street.delegate=self;
    //txt_street.text=street_valve;
    [img_cellBackGnd addSubview:txt_street];
        
        
        
        UIImageView *img_line = [[UIImageView alloc]init];
        img_line.frame =  CGRectMake(55,CGRectGetMaxY(txt_street.frame), WIDTH-90, 0.5);
        img_line.backgroundColor =[UIColor grayColor];
        [img_line setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line];
        
        
        
     lblAdrs1 = [[UILabel alloc]init];
        lblAdrs1.frame=CGRectMake(55, 80, 205, 40);
        //        if (IS_IPHONE_6Plus) {
        //            //        lblTitels.frame=CGRectMake(50, 7, 150, 30);
        //            lblTitels.frame=CGRectMake(50, 12, 150, 30);
        //
        //        }else if (IS_IPHONE_6)
        //
        //        {
        //            lblTitels.frame=CGRectMake(50, 11, 150, 30);
        //
        //        }
        //        else{
        //            lblTitels.frame=CGRectMake(50, 10, 150, 25);
        //
        //        }
        //        lblTitels.text = [arryFoodnames objectAtIndex:indexPath.row];
        //lblAdrs1.text = [[ary_displayAddress objectAtIndex:indexPath.row] valueForKey:@"Street_Address"];
        lblAdrs1.font=[UIFont fontWithName:kFont size:12];
        [img_cellBackGnd addSubview:lblAdrs1];
        
        
        UIImageView *ddIcon = [[UIImageView alloc]init];
        ddIcon.frame =  CGRectMake(WIDTH-60,CGRectGetMaxY(img_line.frame)+13, 20, 13);
        //        ddIcon.backgroundColor =[UIColor clearColor];
        [ddIcon setUserInteractionEnabled:YES];
        ddIcon.image = [UIImage imageNamed:@"dropdown@2x.png"];
        [img_cellBackGnd addSubview:ddIcon];
        
        
        UIButton *btn_City_dropdown =[UIButton buttonWithType:UIButtonTypeCustom];
        btn_City_dropdown.frame=CGRectMake(55, 80, 230, 40);
        //        if (IS_IPHONE_6Plus)
        //        {
        //            btn_on_tel_code_dropdown.frame=CGRectMake(30,CGRectGetMidY(labl_delevery_company_phone_no.frame)+12,65,25);
        //        }
        //        else if (IS_IPHONE_6)
        //        {
        //            btn_on_tel_code_dropdown.frame=CGRectMake(30,CGRectGetMidY(labl_delevery_company_phone_no.frame)+12,65,25);
        //        }
        //        else if (IS_IPHONE_5)
        //        {
        //            btn_on_tel_code_dropdown.frame=CGRectMake(30,CGRectGetMidY(labl_delevery_company_phone_no.frame)+12,65,25);
        //        }
        //        else
        //        {
        //            btn_on_tel_code_dropdown.frame=CGRectMake(30,CGRectGetMidY(labl_delevery_company_phone_no.frame)+12,65,22);
        //        }
        btn_City_dropdown.backgroundColor = [UIColor clearColor];
        [btn_City_dropdown addTarget:self action:@selector(dropboxbtn_country:) forControlEvents:UIControlEventTouchUpInside];
        btn_City_dropdown.userInteractionEnabled = YES;
        //  [btn_on_dropdown setImage:[UIImage imageNamed:@"dropdown@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd  addSubview:btn_City_dropdown];
        
    
    
        
        UIImageView *img_line1 = [[UIImageView alloc]init];
        img_line1.frame =  CGRectMake(55,CGRectGetMaxY(lblAdrs1.frame), WIDTH-90, 0.5);
        img_line1.backgroundColor =[UIColor grayColor];
        [img_line1 setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line1];
        
        
        
        
        lblAdrs2 = [[UILabel alloc]init];
        lblAdrs2.frame=CGRectMake(55, CGRectGetMaxY(img_line1.frame), 205, 40);
        //        if (IS_IPHONE_6Plus) {
        //            //        lblTitels.frame=CGRectMake(50, 7, 150, 30);
        //            lblTitels.frame=CGRectMake(50, 12, 150, 30);
        //
        //        }else if (IS_IPHONE_6)
        //
        //        {
        //            lblTitels.frame=CGRectMake(50, 11, 150, 30);
        //
        //        }
        //        else{
        //            lblTitels.frame=CGRectMake(50, 10, 150, 25);
        //
        //        }
        //        lblTitels.text = [arryFoodnames objectAtIndex:indexPath.row];
       // lblAdrs2.text = [[ary_displayAddress objectAtIndex:indexPath.row] valueForKey:@"Country"];
        lblAdrs2.font=[UIFont fontWithName:kFont size:12];
        lblAdrs2.textAlignment = NSTextAlignmentJustified;
        [img_cellBackGnd addSubview:lblAdrs2];
        
        
        
        UIImageView *ddIcon1 = [[UIImageView alloc]init];
        ddIcon1.frame =  CGRectMake(WIDTH-60,CGRectGetMaxY(img_line1.frame)+13, 20, 13);
        //        ddIcon1.backgroundColor =[UIColor clearColor];
        [ddIcon1 setUserInteractionEnabled:YES];
        ddIcon1.image = [UIImage imageNamed:@"dropdown@2x.png"];
        [img_cellBackGnd addSubview:ddIcon1];
        
        
        
        
        UIButton *btn_Country_dropdown =[UIButton buttonWithType:UIButtonTypeCustom];
        btn_Country_dropdown.frame=CGRectMake(55, CGRectGetMaxY(img_line1.frame), 230, 40);
        
        //        if (IS_IPHONE_6Plus)
        //        {
        //            btn_on_tel_code_dropdown.frame=CGRectMake(30,CGRectGetMidY(labl_delevery_company_phone_no.frame)+12,65,25);
        //        }
        //        else if (IS_IPHONE_6)
        //        {
        //            btn_on_tel_code_dropdown.frame=CGRectMake(30,CGRectGetMidY(labl_delevery_company_phone_no.frame)+12,65,25);
        //        }
        //        else if (IS_IPHONE_5)
        //        {
        //            btn_on_tel_code_dropdown.frame=CGRectMake(30,CGRectGetMidY(labl_delevery_company_phone_no.frame)+12,65,25);
        //        }
        //        else
        //        {
        //            btn_on_tel_code_dropdown.frame=CGRectMake(30,CGRectGetMidY(labl_delevery_company_phone_no.frame)+12,65,22);
        //        }
        btn_Country_dropdown.backgroundColor = [UIColor clearColor];
        [btn_Country_dropdown addTarget:self action:@selector(dropboxbtn_city:) forControlEvents:UIControlEventTouchUpInside];
        btn_Country_dropdown.userInteractionEnabled = YES;
        //  [btn_on_dropdown setImage:[UIImage imageNamed:@"dropdown@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd  addSubview:btn_Country_dropdown];
        
        
        
        
        
        
        UIImageView *img_line2 = [[UIImageView alloc]init];
        img_line2.frame =  CGRectMake(55,CGRectGetMaxY(lblAdrs2.frame), WIDTH-90, 0.5);
        img_line2.backgroundColor =[UIColor grayColor];
        [img_line2 setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line2];
        
    txt_postal = [[UITextField alloc]init];
    txt_postal.borderStyle = UITextBorderStyleNone;
    txt_postal.frame=CGRectMake(55, CGRectGetMaxY(img_line2.frame), 205, 40);
    //        if (IS_IPHONE_6Plus) {
    //            //        lblTitels.frame=CGRectMake(50, 7, 150, 30);
    //            lblTitels.frame=CGRectMake(50, 12, 150, 30);
    //
    //        }else if (IS_IPHONE_6)
    //
    //        {
    //            lblTitels.frame=CGRectMake(50, 11, 150, 30);
    //
    //        }
    //        else{
    //            lblTitels.frame=CGRectMake(50, 10, 150, 25);
    //
    //        }
    //        lblTitels.text = [arryFoodnames objectAtIndex:indexPath.row];

    txt_postal.font = [UIFont fontWithName:kFont size:13];
    txt_postal.placeholder = @"Postal code";
    [txt_postal setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_postal setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_postal.leftViewMode = UITextFieldViewModeAlways;
    txt_postal.userInteractionEnabled=YES;
    txt_postal.textAlignment = NSTextAlignmentLeft;
    txt_postal.backgroundColor = [UIColor clearColor];
    txt_postal.keyboardType = UIKeyboardTypeNumberPad;
    txt_postal.delegate=self;
    //txt_street.text=street_valve;
    

    if (keyboardToolbar_Date == nil)
    {
        keyboardToolbar_Date = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, IS_IPHONE_5?45:45)];
        [keyboardToolbar_Date setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_Done)];
        [keyboardToolbar_Date setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    txt_postal.inputAccessoryView = keyboardToolbar_Date;
    [img_cellBackGnd addSubview:txt_postal];

        UIImageView *img_line3 = [[UIImageView alloc]init];
        img_line3.frame =  CGRectMake(55,CGRectGetMaxY(txt_postal.frame), WIDTH-90, 0.5);
        img_line3.backgroundColor =[UIColor grayColor];
        [img_line3 setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line3];
        
        
       btn_check_box_for_setas_default  =  [UIButton buttonWithType:UIButtonTypeCustom];
        btn_check_box_for_setas_default.frame = CGRectMake(WIDTH-150, CGRectGetMaxY(txt_postal.frame)+12 ,16,16);
        //        if (IS_IPHONE_6Plus)
        //        {
        //            btn_check_box_for_setas_default.frame = CGRectMake(270,CGRectGetMidY(img_line33.frame)+15,16,16);
        //        }
        //        else if (IS_IPHONE_6)
        //        {
        //            btn_check_box_for_setas_default.frame = CGRectMake(230,CGRectGetMidY(img_line33.frame)+15,16,16);
        //        }
        //        else if (IS_IPHONE_5)
        //        {
        //            btn_check_box_for_setas_default.frame = CGRectMake(200,CGRectGetMidY(img_line33.frame)+15,16,16);
        //        }
        //        else
        //        {
        //            btn_check_box_for_setas_default.frame = CGRectMake(200,CGRectGetMidY(img_line33.frame)+15,16,16);
        //        }
        [btn_check_box_for_setas_default setImage:[UIImage imageNamed:@"img-check-select@2x.png"] forState:UIControlStateSelected];
        [btn_check_box_for_setas_default setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
        //    btn_check_box_for_setas_default.tag = 54;
        [btn_check_box_for_setas_default addTarget:self action:@selector(click_on_check_box_setAsDefault_btnMethod:) forControlEvents:UIControlEventTouchUpInside];
        [img_cellBackGnd addSubview:  btn_check_box_for_setas_default];
        
        
        UILabel *labl_set_as_default = [[UILabel alloc]init];
        labl_set_as_default.frame = CGRectMake(CGRectGetMaxX(btn_check_box_for_setas_default.frame)+10,CGRectGetMaxY(img_line3.frame)+12,140,16);
        //        if (IS_IPHONE_6Plus)
        //        {
        //            labl_set_as_default.frame = CGRectMake(CGRectGetMidX(btn_check_box_for_setas_default.frame)+13,CGRectGetMidY(img_line33.frame)+3,140,40);
        //        }
        //        else if (IS_IPHONE_6)
        //        {
        //            labl_set_as_default.frame = CGRectMake(CGRectGetMidX(btn_check_box_for_setas_default.frame)+13,CGRectGetMidY(img_line33.frame)+3,140,40);
        //        }
        //        else if (IS_IPHONE_5)
        //        {
        //            labl_set_as_default.frame = CGRectMake(CGRectGetMidX(btn_check_box_for_setas_default.frame)+13,CGRectGetMidY(img_line33.frame)+3,140,40);
        //        }
        //        else
        //        {
        //            labl_set_as_default.frame = CGRectMake(CGRectGetMidX(btn_check_box_for_setas_default.frame)+13,CGRectGetMidY(img_line33.frame)+3,140,40);
        //        }
        labl_set_as_default.text = @"Set as Default";
        labl_set_as_default.font = [UIFont fontWithName:kFontBold size:11];
        labl_set_as_default.textColor = [UIColor blackColor];
        labl_set_as_default.textAlignment = NSTextAlignmentJustified;
        labl_set_as_default.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:labl_set_as_default];
        
        
        
        //        ary_displayAddressary_displayAddress
        //        UILabel *lbl_home_r_work = [[UILabel alloc]init];
        //         lbl_home_r_work.frame = CGRectMake(40,10,200, 15);
        //        if (indexPath.row == 0)
        //        {
        //            lbl_home_r_work .text = [NSString stringWithFormat:@"%@",[[ary_displayAddress objectAtIndex:0] valueForKey:@"Address_Name"]];
        //
        //        }
        //        else if (indexPath.row == 1)
        //        {
        //
        //            lbl_home_r_work .text = [NSString stringWithFormat:@"%@",[[ary_displayAddress objectAtIndex:1] valueForKey:@"Address_Name"]];
        //
        //        }
        //
        //        //        lbl_home_r_work.text = [NSString stringWithFormat:@"%@",[array_home_work_address objectAtIndex:indexPath.row]];
        //        lbl_home_r_work.font = [UIFont fontWithName:kFontBold size:13];
        //        lbl_home_r_work.textColor = [UIColor blackColor];
        //        lbl_home_r_work.backgroundColor = [UIColor clearColor];
        //        [img_cellBackGnd addSubview:lbl_home_r_work];
        
        
        
        
        //        UIImageView *info_imges = [[UIImageView alloc]init];
        //        info_imges.frame =  CGRectMake(0,10, 30, 30);
        //        [info_imges setImage:[UIImage imageNamed:@"icon-location@2x.png"]];
        //        info_imges.backgroundColor =[UIColor clearColor];
        //        [info_imges setUserInteractionEnabled:YES];
        //        [img_cellBackGnd addSubview:info_imges];
        //
        //
        //        UILabel *address = [[UILabel alloc]init];
        //        address.frame = CGRectMake(40,25,200, 15);
        //
        //        address.text = [NSString stringWithFormat:@"%@,%@,%@,%@",[[ary_displayAddress objectAtIndex:indexPath.row] valueForKey:@"City"],[[ary_displayAddress objectAtIndex:indexPath.row] valueForKey:@"Country"],[[ary_displayAddress objectAtIndex:indexPath.row] valueForKey:@"Street_Address"],[[ary_displayAddress objectAtIndex:indexPath.row] valueForKey:@"Postal_Code"]];
        //        // address.numberOfLines = 2;
        //        address.font = [UIFont fontWithName:kFont size:13];
        //        address.textColor = [UIColor blackColor];
        //        address.backgroundColor = [UIColor clearColor];
        //        [img_cellBackGnd addSubview:address];
        //
        //        UILabel *label_deliver = [[UILabel alloc]init];
        //       label_deliver.frame = CGRectMake(230,10,200, 15);
        //        label_deliver.text = @"Delivery";
        //        label_deliver.font = [UIFont fontWithName:kFontBold size:13];
        //        label_deliver.textColor = [UIColor blackColor];
        //        label_deliver.backgroundColor = [UIColor clearColor];
        //        [img_cellBackGnd addSubview:label_deliver];
        
        
        
        //        if (IS_IPHONE_6Plus)
        //        {
        //            img_cellBackGnd.frame =  CGRectMake(0,2, WIDTH,80);
        //            img_line.frame =  CGRectMake(0,64, WIDTH-20, 0.5);
        //            lbl_home_r_work.frame = CGRectMake(55,10,200, 15);
        //            //            lbl_home_r_Address.frame=CGRectMake(55, CGRectGetMaxY(info_imges.frame)+10, 200, 15);
        //            info_imges.frame =  CGRectMake(10,10, 30, 30);
        //            address.frame = CGRectMake(55,25,200, 15);
        //            label_deliver.frame = CGRectMake(320,5,200, 15);
        //        }
        //        else if (IS_IPHONE_6)
        //        {
        //
        //            img_cellBackGnd.frame =  CGRectMake(0,2, WIDTH, 75);
        //            info_imges.frame =  CGRectMake(15,10, 35, 35);
        //            lbl_home_r_work.frame = CGRectMake(CGRectGetMaxX(info_imges.frame)+15,10,200, 15);
        //            //            lbl_home_r_Address.frame=CGRectMake(55, CGRectGetMaxY(info_imges.frame)+10, 200, 15);
        //
        //            address.frame = CGRectMake(CGRectGetMaxX(info_imges.frame)+15,25,400, 33);
        //            label_deliver.frame = CGRectMake(280,10,200, 15);
        //            img_line.frame =  CGRectMake(15,70, WIDTH-30, 0.5);
        //        }
        //
        //        else
        //
        //        {
        //            img_cellBackGnd.frame =  CGRectMake(2,2, 290,75);
        //            img_line.frame =  CGRectMake(0,60, 295, 0.5);
        //            lbl_home_r_work.frame = CGRectMake(50,10,200, 15);
        //            //            lbl_home_r_Address.frame=CGRectMake(55, CGRectGetMaxY(info_imges.frame)+10, 200, 15);
        //
        //            info_imges.frame =  CGRectMake(8,10, 30, 30);
        //            address.frame = CGRectMake(50,25,200, 15);
        //            label_deliver.frame = CGRectMake(230,10,200, 15);
        //        }
        
   
    btn_Save_Deliverly_Address  =  [UIButton buttonWithType:UIButtonTypeCustom];
    btn_Save_Deliverly_Address = [[UIButton alloc]init];
    
    btn_Save_Deliverly_Address.frame = CGRectMake(100, CGRectGetMaxY(btn_check_box_for_setas_default.frame)+5, WIDTH-200, 30);
    if (IS_IPHONE_6Plus)
    {
        btn_Save_Deliverly_Address.frame = CGRectMake(90, CGRectGetMaxY(btn_check_box_for_setas_default.frame)+5, WIDTH-200, 30);
    }
    else if (IS_IPHONE_6)
    {
        btn_Save_Deliverly_Address.frame = CGRectMake(90, CGRectGetMaxY(btn_check_box_for_setas_default.frame)+5, WIDTH-200, 30);
    }
    else if (IS_IPHONE_5)
    {
        btn_Save_Deliverly_Address.frame = CGRectMake(90, CGRectGetMaxY(btn_check_box_for_setas_default.frame)+5, WIDTH-200, 30);
    }
    else
    {
        btn_Save_Deliverly_Address.frame = CGRectMake(90, CGRectGetMaxY(btn_check_box_for_setas_default.frame)+5, WIDTH-200, 25);
    }
    [btn_Save_Deliverly_Address setTitle:@"Add Address" forState:UIControlStateNormal];
    [btn_Save_Deliverly_Address setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Save_Deliverly_Address setBackgroundColor:[UIColor colorWithRed:39.0/255.0 green:37.0/255.0 blue:48.0/255.0 alpha:1.0f]];
    btn_Save_Deliverly_Address.layer.cornerRadius = 2.0;
    btn_Save_Deliverly_Address.titleLabel.font=[UIFont fontWithName:kFont size:14];
    [btn_Save_Deliverly_Address addTarget:self action:@selector(save_Deliverly_Address_Method:) forControlEvents:UIControlEventTouchUpInside];
    [img_cellBackGnd addSubview: btn_Save_Deliverly_Address];
    
    
    tableview_Kcountry = [[UITableView alloc]init];
    [tableview_Kcountry setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tableview_Kcountry.delegate = self;
    tableview_Kcountry.dataSource = self;
    tableview_Kcountry.showsVerticalScrollIndicator = NO;
    tableview_Kcountry.backgroundColor = [UIColor whiteColor];
    tableview_Kcountry.layer.borderColor = [[UIColor blackColor]CGColor];
    tableview_Kcountry.layer.borderWidth = 1.0f;
    tableview_Kcountry.clipsToBounds = YES;
    tableview_Kcountry.hidden =YES;
    [img_cellBackGnd addSubview:tableview_Kcountry];
    
    tableview_Kcity = [[UITableView alloc]init];
    [tableview_Kcity setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tableview_Kcity.delegate = self;
    tableview_Kcity.dataSource = self;
    tableview_Kcity.showsVerticalScrollIndicator = NO;
    tableview_Kcity.backgroundColor = [UIColor whiteColor];
    tableview_Kcity.layer.borderColor = [[UIColor blackColor]CGColor];
    tableview_Kcity.layer.borderWidth = 1.0f;
    tableview_Kcity.clipsToBounds = YES;
    tableview_Kcity.hidden =YES;
    [img_cellBackGnd addSubview:tableview_Kcity];
    
    tableview_Kcountry.frame=CGRectMake(20, CGRectGetMaxY(lblAdrs1.frame)+5, WIDTH-70, 100);
    tableview_Kcity.frame=CGRectMake(20, CGRectGetMaxY(lblAdrs2.frame)+5, WIDTH-70, 80);

    
}

-(void)cross_Method
{
    [DeliveryPopUpBg removeFromSuperview];
    
}
-(void)tel_code_dropdownMethod: (UIButton *)sender
{
    NSLog(@")click_on_tel_code_drop_down Button ");
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        table_for_contry_code1.hidden = NO;
    }
    else
    {
        [sender setSelected:NO];
        table_for_contry_code1.hidden = YES;
    }
    
}

-(void)tel_code_dropdown2Method: (UIButton *)sender
{
    NSLog(@")click_on_tel_code_drop_down Button ");
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        table_for_contry_code2.hidden = NO;
        
    }
    else
    {
        [sender setSelected:NO];
        table_for_contry_code2.hidden = YES;
    }
    
}
-(void)click_Done
{
    [txt_postal resignFirstResponder];
    
}

-(void)check_box_ApplyAll_Method: (UIButton *)sender
{
    NSLog(@"check_box_ApplyAll Button Clicked ");
    
    
    if([sender isSelected])
    {
        [sender setSelected:NO];
        [btn_check_box_for_Apply_All  setSelected:NO];
        str_delivarypopselectall = @"0";
    }
    else
    {
        [btn_check_box_for_Apply_All  setSelected:YES];
        str_delivarypopselectall = @"1";
        [sender setSelected:YES];
        
    }
    
    
}

-(void)click_on_check_box_setAsDefault_btnMethod: (UIButton *)sender
{
    NSLog(@"check_box_ApplyAll Button Clicked ");
    
    
    if([sender isSelected])
    {
        [sender setSelected:NO];
        [btn_check_box_for_Apply_All  setSelected:NO];
        str_delivarypopselectall = @"0";
    }
    else
    {
        [btn_check_box_for_Apply_All  setSelected:YES];
        str_delivarypopselectall = @"1";
        [sender setSelected:YES];
        
    }
    
    
}



-(void)get_Deliver_Address_BtnMethod:(UIButton *)sender
{
    NSLog(@")get_Deliver_Address Button Clicked");
    
    int val = [sender.titleLabel.text intValue];
    NSLog(@"sender ta %ld",(long)sender.tag);
   
    
    int totalPage;
    
    totalPage = (int)[[[ary_dishdivary objectAtIndex:val] valueForKey:@"addressdetails"] count]+1;
    
    if (sender.tag==totalPage-1)
    {
        
        [self deliveryDesign];

    }
    else
    {
        str_Delivery_Address = [NSString stringWithFormat:@"%@", [[[[ary_dishdivary objectAtIndex:val] valueForKey:@"addressdetails"] objectAtIndex:sender.tag] valueForKey:@"address"]];
        NSString*str_id =[NSString stringWithFormat:@"%@", [[[[ary_dishdivary objectAtIndex:val] valueForKey:@"addressdetails"] objectAtIndex:sender.tag] valueForKey:@"id"]];
        
        
        img_bg_for_delevery.hidden = YES;
        [DeliveryPopUpBg removeFromSuperview];
        
        for (int i=0; i<[ary_maintosave count]; i++) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[ary_maintosave objectAtIndex:i];
            
            if (sender.tag ==i)
            {
                [dict setValue:@"" forKey:@"Delivery_Address"];
                [dict setValue:@"0" forKey:@"delivaryid"];
                [dict setValue:[NSString  stringWithFormat:@"%@",@"YES"] forKey:@"tableshow"];

            }
            else{
                
                
                [dict setValue:str_Delivery_Address forKey:@"Delivery_Address"];
                [dict setValue:str_id forKey:@"delivaryid"];
                
                [dict setValue:[NSString  stringWithFormat:@"%@",@"NO"] forKey:@"tableshow"];


            }
          
            [table_address reloadData];
            
        }

    }

    
//    if ( [str_Delivery_Address isEqualToString:@"Add delivary address"])
//    {
//        //        img_bg_for_delevery.hidden = NO;
//        [self deliveryDesign];
//    }
//    else
//    {
//       
//        [DeliveryPopUpBg removeFromSuperview];
//        //        img_bg_for_delevery.hidden = YES;
//    }
    
}


-(void)dropboxbtn_country:(UIButton *) sender

{
    tableview_Kcity.hidden=YES;
    
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        tableview_Kcountry.hidden=NO;
        
    }
    else
    {
        [sender setSelected:NO];
        tableview_Kcountry.hidden=YES;
    }
    
}
-(void)dropboxbtn_city:(UIButton *) sender

{
    if (lblAdrs1.text.length<=0)
    {
        [self popup_Alertview:@"Plaese select country first"];
        
    }
    else{
        
        tableview_Kcountry.hidden=YES;
        if (![sender isSelected])
        {
            [sender setSelected:YES];
            tableview_Kcity.hidden=NO;
            
            
            
        }
        else
        {
            [sender setSelected:NO];
            tableview_Kcity.hidden=YES;
        }
        
        
    }
    
}

-(void)save_Deliverly_Address_Method: (UIButton *)sender
{
    
    NSLog(@"save_Deliverly_Address button clicked");
    
    if (txt_street.text.length<=0)
    {
        [self popup_Alertview:@"please enter street name"];
        
    }else if (lblAdrs1.text.length<=0)
    {
        [self popup_Alertview:@"please select country "];
    }
    else if (lblAdrs2.text.length<=0)
    {
        [self popup_Alertview:@"please select city"];
    }
    else if (txt_postal.text.length<=0)
    {
        [self popup_Alertview:@"please enter person name"];
    }
    else{
        
        [DeliveryPopUpBg removeFromSuperview];
        //[self AFAddresslist];
        
    }
    
    
    
    //    img_bg_for_delevery.hidden = YES;
    
}


#pragma mark UITableview methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tableview_Kcountry) {
        return [ary_countrylist count];
        
    }
    else if (tableView == tableview_Kcity)
    {
        return [ary_citylist count];
    }
    else {
        return [ary_maintosave count];

    }
    
}




//-(CGFloat)tableView: (UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 40.0;
//}


//-(UIView *)tableView : (UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    
//    UIView*sectionView;
//    sectionView = [[UIView alloc]init];
//    sectionView.frame = CGRectMake(0, 0, WIDTH-10, 40);
//    sectionView.backgroundColor = [UIColor whiteColor];
//    //    sectionView.layer.borderWidth = 1.0;
//    
//    UILabel *firstLabel = [[UILabel alloc]init];
//    firstLabel.frame = CGRectMake(15, 5, WIDTH-10, 35);
//    firstLabel.backgroundColor = [UIColor clearColor];
//    firstLabel.text = @"Doe's Kitchen";
//    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
//    //    firstLabel.textAlignment = NSTextAlignmentCenter;
//    firstLabel.textColor = [UIColor blackColor];
//    firstLabel.font = [UIFont fontWithName:kFontBold size:15];
//    firstLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    firstLabel.numberOfLines = 0;
//    [sectionView addSubview:firstLabel];
//    
//    
//    UIImageView *Line = [[UIImageView alloc]init];
//    Line.frame = CGRectMake(12, CGRectGetMaxY(firstLabel.frame)-5,(WIDTH-10)/3.0 ,2);
//    Line.backgroundColor = [UIColor blackColor];
//    [sectionView addSubview:Line];
//    
//    return sectionView;
//}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView ==tableview_Kcountry)
    {
        return 30;

    }
    else if (tableView ==tableview_Kcity)
    {
        return 30;
 
    }
    else{
        return 260;

    }
    return 200;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
        for (UIView *view in cell.contentView.subviews)
        {
            [view removeFromSuperview];
        }
    
    if (tableView==tableview_Kcountry)
    {
        UILabel  * lbl_list = [[UILabel alloc]init];
        lbl_list.frame=CGRectMake(12, 0, 100, 30);
        lbl_list.backgroundColor=[UIColor clearColor];
        lbl_list.textColor=[UIColor blackColor];
        lbl_list.font = [UIFont fontWithName:kFont size:12];
        lbl_list.text=[[ary_countrylist objectAtIndex:indexPath.row] valueForKey:@"Country"];
        [cell.contentView addSubview:lbl_list];
        
        
    }else if(tableView == tableview_Kcity){
        UILabel  * lbl_list = [[UILabel alloc]init];
        lbl_list.frame=CGRectMake(12, 0, 100, 30);
        lbl_list.backgroundColor=[UIColor clearColor];
        lbl_list.textColor=[UIColor blackColor];
        lbl_list.font = [UIFont fontWithName:kFont size:12];
        lbl_list.text=[[ary_citylist objectAtIndex:indexPath.row] valueForKey:@"City"];
        [cell.contentView addSubview:lbl_list];
        
        
    }else{
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-10, 250);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        //    img_cellBackGnd.layer.borderWidth = 1.0;
        [cell.contentView addSubview:img_cellBackGnd];
        
        UILabel *firstLabel = [[UILabel alloc]init];
        firstLabel.frame = CGRectMake(15, 5, WIDTH-10, 35);
        firstLabel.backgroundColor = [UIColor clearColor];
        
        firstLabel.textColor = [UIColor blackColor];
        firstLabel.font = [UIFont fontWithName:kFontBold size:15];
        firstLabel.lineBreakMode = NSLineBreakByWordWrapping;
        firstLabel.numberOfLines = 0;
        [img_cellBackGnd addSubview:firstLabel];
        
        
        
        UIImageView *Line = [[UIImageView alloc]init];
        Line.frame = CGRectMake(12, CGRectGetMaxY(firstLabel.frame)-5,(WIDTH-10)/3.0 ,2);
        Line.backgroundColor = [UIColor blackColor];
        [img_cellBackGnd addSubview:Line];
        
        
        UILabel *firstLabel1 = [[UILabel alloc]init];
        firstLabel1.frame = CGRectMake(15, CGRectGetMaxY(Line.frame)+5,WIDTH-80, 25);
        firstLabel1.backgroundColor = [UIColor clearColor];
        firstLabel1.text = [NSString stringWithFormat: @"%@", [[ary_maintosave objectAtIndex:indexPath.row]valueForKey:@"item_name" ]];
        //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
        //    firstLabel.textAlignment = NSTextAlignmentCenter;
        firstLabel1.font = [UIFont fontWithName:kFontBold size:15];
        firstLabel1.lineBreakMode = NSLineBreakByWordWrapping;
        firstLabel1.numberOfLines = 0;
        [img_cellBackGnd addSubview:firstLabel1];
        
        
        if (![ary_dummykithennames containsObject:[[ary_maintosave objectAtIndex:indexPath.row]valueForKey:@"kitchen_name"]])
        {
            firstLabel.text = [NSString stringWithFormat: @"%@", [[ary_maintosave objectAtIndex:indexPath.row]valueForKey:@"kitchen_name"]];
            [ary_dummykithennames addObject:[[ary_maintosave objectAtIndex:indexPath.row]valueForKey:@"kitchen_name"]];
            firstLabel.hidden = NO;
            Line.hidden= NO;
        }
        else
        {
            firstLabel.hidden = YES;
            Line.hidden= YES;
            
        }
        
        UILabel *secondLabel = [[UILabel alloc]init];
        secondLabel.frame = CGRectMake(WIDTH-70, CGRectGetMaxY(firstLabel.frame)+5, 70, 25);
        //    secondLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"subject"];
        //    secondLabel.textAlignment = NSTextAlignmentCenter;
        //    secondLabel.backgroundColor = [UIColor clearColor];
        //    secondLabel.textColor = [UIColor blackColor];
        secondLabel.font = [UIFont fontWithName:kFont size:15];
        secondLabel.lineBreakMode = NSLineBreakByWordWrapping;
        secondLabel.numberOfLines = 0;
        [img_cellBackGnd addSubview:secondLabel];
        
        
        UIImageView *img_line = [[UIImageView alloc] init];
        img_line.frame = CGRectMake(15,CGRectGetMaxY(firstLabel1.frame)+7, WIDTH-40, 0.5 );
        [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
        [img_cellBackGnd addSubview:img_line];
        
        
        
        UILabel *cell_delivary  = [[UILabel alloc]init];
        cell_delivary.frame = CGRectMake(15, CGRectGetMaxY(img_line.frame)+6, 130, 17);
        cell_delivary .text = @"Delivery Address";
        //    cell_head.textAlignment = NSTextAlignmentCenter;
        cell_delivary .font = [UIFont fontWithName:kFontBold size:14];
        cell_delivary .textColor = [UIColor blackColor];
        cell_delivary .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:cell_delivary];
        
        
        UIImageView *img_tableadd = [[UIImageView alloc] init];
        if (IS_IPHONE_6Plus)
        {
            img_tableadd.frame = CGRectMake(15, CGRectGetMaxY(cell_delivary.frame)+6, WIDTH-60 , 30);
        }
        else if (IS_IPHONE_6)
        {
            img_tableadd.frame = CGRectMake(15, CGRectGetMaxY(cell_delivary.frame)+6, WIDTH-60 , 30);
        }
        else if (IS_IPHONE_5)
        {
            img_tableadd.frame = CGRectMake(15, CGRectGetMaxY(cell_delivary.frame)+6, WIDTH-60 , 30);
        }
        else
        {
            img_tableadd.frame = CGRectMake(15, CGRectGetMaxY(cell_delivary.frame)+6, WIDTH-60 , 30);
        }
        [img_tableadd setImage:[UIImage imageNamed:@"dietary-table-img@2x"]];
        [img_cellBackGnd addSubview:img_tableadd];
        
        UILabel *adress = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus)
        {
            adress.frame = CGRectMake(5, 0, WIDTH-80 , 30);
        }
        else if (IS_IPHONE_6)
        {
            adress.frame = CGRectMake(5, 0, WIDTH-80 , 30);
        }
        else if (IS_IPHONE_5)
        {
            adress.frame = CGRectMake(5, 0, WIDTH-80 , 30);
        }
        else
        {
            adress.frame = CGRectMake(5, 0, WIDTH-80 , 30);
        }
        adress.text = [NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:indexPath.row] valueForKey:@"Delivery_Address"]];
        //    secondLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"subject"];
        //    secondLabel.textAlignment = NSTextAlignmentCenter;
        //    secondLabel.backgroundColor = [UIColor clearColor];
        //    secondLabel.textColor = [UIColor blackColor];
        adress.font = [UIFont fontWithName:kFont size:15];
        adress.lineBreakMode = NSLineBreakByWordWrapping;
        [img_tableadd addSubview:adress];
        //
        //
        UIButton*delibery_Btn;
        delibery_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
        //        delibery_Btn.frame = CGRectMake(70, 10, 210 , 30);
        if (IS_IPHONE_6Plus)
        {
            delibery_Btn.frame = CGRectMake(15, CGRectGetMaxY(cell_delivary.frame)+6, WIDTH-60 , 30);
        }
        else if (IS_IPHONE_6)
        {
            delibery_Btn.frame = CGRectMake(15, CGRectGetMaxY(cell_delivary.frame)+6, WIDTH-60 , 30);
        }
        else if (IS_IPHONE_5)
        {
            delibery_Btn.frame = CGRectMake(15, CGRectGetMaxY(cell_delivary.frame)+6, WIDTH-60 , 30);
        }
        else
        {
            delibery_Btn.frame = CGRectMake(15, CGRectGetMaxY(cell_delivary.frame)+6, WIDTH-60 , 30);
        }
        [delibery_Btn addTarget:self action:@selector(delibery_Btn_Method:) forControlEvents:UIControlEventTouchUpInside];
        delibery_Btn.tag = indexPath.row;
        [img_cellBackGnd addSubview: delibery_Btn];
        
        
        UIScrollView*delivery_Scroll;
        delivery_Scroll = [[UIScrollView alloc]init];
        // delivery_Scroll.frame=CGRectMake(80, 25, 210, 125);
        if (IS_IPHONE_6Plus)
        {
            delivery_Scroll.frame=CGRectMake(20, 25, 280, 85);
        }
        else  if (IS_IPHONE_6)
        {
            
            delivery_Scroll.frame=CGRectMake(20, 25, 280, 85);
        }
        else  if (IS_IPHONE_5)
        {
            
            delivery_Scroll.frame=CGRectMake(40, 25, 240, 85);
        }
        else
        {
            delivery_Scroll.frame=CGRectMake(40, 25, 240, 85);
        }
        
        delivery_Scroll.backgroundColor = [UIColor colorWithRed:243/255.0f green:245/255.0f blue:244/255.0f alpha:1.0f];
        delivery_Scroll.bounces=YES;
        delivery_Scroll.layer.borderWidth = 1.0;
        delivery_Scroll.layer.borderColor = [UIColor blackColor].CGColor;
        delivery_Scroll.showsVerticalScrollIndicator = YES;
        delivery_Scroll.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [delivery_Scroll setScrollEnabled:YES];
        delivery_Scroll.userInteractionEnabled = YES;
        [cell.contentView addSubview:delivery_Scroll];
        //[(NSDictionary *) [[ary_maintosave  objectAtIndex:0] valueForKey:@"Delivery_Address"] count]
        
        int totalPage;
        
        totalPage = (int)[[[ary_dishdivary objectAtIndex:indexPath.row] valueForKey:@"addressdetails"] count]+1;
        [delivery_Scroll setContentSize:CGSizeMake(delivery_Scroll.frame.size.width, 25*totalPage)];
        
        
        
        for (int j = 0; j<totalPage; j++)
        {
            UILabel*deliver_Address_Lbl;
            deliver_Address_Lbl = [[UILabel alloc]init];
            deliver_Address_Lbl.frame = CGRectMake(2,25*j, delivery_Scroll.frame.size.width-4, 25);
            if (j==totalPage-1)
            {
                deliver_Address_Lbl.text = @"Add delivary address";
                
            }
            else
            {
                deliver_Address_Lbl.text = [[[[ary_dishdivary objectAtIndex:indexPath.row] valueForKey:@"addressdetails"] objectAtIndex:j] valueForKey:@"address"];
                
            }
            deliver_Address_Lbl.textAlignment = NSTextAlignmentCenter;
            deliver_Address_Lbl.font = [UIFont fontWithName:kFont size:12];
            deliver_Address_Lbl.userInteractionEnabled = YES;
            
            [delivery_Scroll addSubview: deliver_Address_Lbl];
            
            
            UIImageView *imgLine=[[UIImageView alloc]init];
            imgLine.frame=CGRectMake( 0, 25*j, delivery_Scroll.frame.size.width, 0.5);
            [imgLine setUserInteractionEnabled:YES];
            imgLine.image = [UIImage imageNamed:@"line-img@2x"];
            [delivery_Scroll addSubview:imgLine];
            
            UIButton*get_Deliver_Address_Btn;
            get_Deliver_Address_Btn = [[UIButton alloc]init];
            get_Deliver_Address_Btn.frame = CGRectMake(0,25*j, delivery_Scroll.frame.size.width, 25);
            [get_Deliver_Address_Btn addTarget:self action:@selector(get_Deliver_Address_BtnMethod:) forControlEvents:UIControlEventTouchUpInside];
            get_Deliver_Address_Btn.tag = j;
            get_Deliver_Address_Btn.titleLabel.text = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [get_Deliver_Address_Btn setBackgroundColor:[UIColor clearColor]];
            //   get_Deliver_Address_Btn.layer.borderWidth = 1.0;
            [delivery_Scroll addSubview: get_Deliver_Address_Btn];
        }
        
        if ([[NSString stringWithFormat:@"%@",[[ary_maintosave  objectAtIndex:indexPath.row] valueForKey:@"tableshow"]]isEqualToString:@"NO"])
        {
            delivery_Scroll.hidden = YES;
            
        }
        else{
            delivery_Scroll.hidden = NO;
            
        }
        
        
        //
        UIButton*btn_SelectAll;
        
        btn_SelectAll = [[UIButton alloc] init];
        btn_SelectAll.frame = CGRectMake(WIDTH-130, CGRectGetMaxY(delibery_Btn.frame)+5, 20 , 20);
        //    if (IS_IPHONE_6Plus)
        //    {
        //
        //    }
        //    else if (IS_IPHONE_6)
        //    {
        //
        //    }
        //    else if (IS_IPHONE_5)
        //    {
        //
        //    }
        //    else
        //    {
        //
        //    }
        
        btn_SelectAll.tag= indexPath.row;
        if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:indexPath.row] valueForKey:@"delivaryidselected"]]isEqualToString:@"1"])
        {
            [btn_SelectAll setBackgroundImage:[UIImage imageNamed:@"img-check-select@2x"] forState:UIControlStateNormal];
            
        }
        else
        {
            [btn_SelectAll setBackgroundImage:[UIImage imageNamed:@"img-check@2x"] forState:UIControlStateNormal];
            
        }

       // [btn_SelectAll setBackgroundImage:[UIImage imageNamed:@"img-check@2x"] forState:UIControlStateNormal];
        btn_SelectAll.backgroundColor = [UIColor clearColor];
        [btn_SelectAll addTarget:self action:@selector(SelectAll_btnClick:) forControlEvents:UIControlEventTouchUpInside ];
        [img_cellBackGnd addSubview:btn_SelectAll];
        
        
        UILabel  * labl_ApplyAll = [[UILabel alloc]init ];
        labl_ApplyAll.frame=CGRectMake(CGRectGetMaxX(btn_SelectAll.frame)+5, CGRectGetMaxY(delibery_Btn.frame)+5, 100 , 20);
        //    if (IS_IPHONE_6Plus)
        //    {
        //        labl_ApplyAll.frame=CGRectMake(0,imgViewTop.frame.size.height+20, self.view.frame.size.width,32);
        //    }
        //    else if (IS_IPHONE_6)
        //    {
        //        labl_ApplyAll.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,30);
        //    }
        //    else if (IS_IPHONE_5)
        //    {
        //        labl_ApplyAll.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,25);
        //    }
        //    else
        //    {
        //        labl_ApplyAll.frame=CGRectMake(0,imgViewTop.frame.size.height+10, self.view.frame.size.width,20);
        //    }
        labl_ApplyAll.text = @"Apply to all";
        labl_ApplyAll.font = [UIFont fontWithName:kFont size:10];
        labl_ApplyAll.backgroundColor=[UIColor clearColor];
        //    labl_ApplyAll.textAlignment=NSTextAlignmentCenter;
        [img_cellBackGnd addSubview:labl_ApplyAll];
        
        
        UILabel *lbl_req = [[UILabel alloc]init];
        lbl_req.frame = CGRectMake(15,CGRectGetMaxY(labl_ApplyAll.frame),150, 20);
        lbl_req.text = @"Special Requests/Remarks";
        // likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
        lbl_req.font = [UIFont fontWithName:kFontBold size:10];
        lbl_req.textColor = [UIColor blackColor];
        lbl_req.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_req];
        
        UITextView*txt_view_for_user_description;
        txt_view_for_user_description =[[UITextView alloc]init];
        txt_view_for_user_description .frame = CGRectMake(15, CGRectGetMaxY(lbl_req.frame), WIDTH-60, 50);
        txt_view_for_user_description.scrollEnabled=YES;
        txt_view_for_user_description.text = [NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:indexPath.row]valueForKey:@"remarks" ]];
        //txt_view_for_user_description.numberOfLines = 5
        txt_view_for_user_description.userInteractionEnabled=YES;
        txt_view_for_user_description.font=[UIFont fontWithName:kFont size:10];
        txt_view_for_user_description.backgroundColor=[UIColor whiteColor];
        txt_view_for_user_description.delegate=self;
        txt_view_for_user_description.textColor=[UIColor lightGrayColor];
        txt_view_for_user_description.layer.borderColor=[[UIColor lightGrayColor]CGColor];
        txt_view_for_user_description.layer.borderWidth=1.0f;
        txt_view_for_user_description.clipsToBounds=YES;
        txt_view_for_user_description.tag=-1;
        [img_cellBackGnd addSubview:txt_view_for_user_description];

    }
    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tableview_Kcountry) {
        
        lblAdrs1.text =[[ary_countrylist objectAtIndex:indexPath.row] valueForKey:@"Country"];
        lblAdrs2.text = @"";
        [tableview_Kcountry setHidden:YES];
        [self cityList];
        
        
    }else {
        lblAdrs2.text =[[ary_citylist objectAtIndex:indexPath.row] valueForKey:@"City"];
        [tableview_Kcity setHidden:YES];
        
        
    }
    
}


-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma click_events
-(void)click_on_back_arrow:(UIButton *)sender
{
    NSLog(@"click_on_back:");
    [self dismissViewControllerAnimated:NO completion:nil];

}
-(void)delibery_Btn_Method:(UIButton *)sender
{
    if ([[NSString stringWithFormat:@"%@",[[ary_maintosave  objectAtIndex:sender.tag] valueForKey:@"tableshow"]]isEqualToString:@"NO"])
    {
//        for (int i=0; i<[ary_maintosave count]; i++)
//        {
//            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
//            
//            dict=[ary_maintosave objectAtIndex:i];
//            
//            for (int i=0; i<[ary_maintosave count]; i++) {
//                
//                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
//                dict=[ary_maintosave objectAtIndex:i];
//                [dict setValue:[NSString  stringWithFormat:@"%@",@"YES"] forKey:@"tableshow"];
//                
//                [table_address reloadData];
//                
//            }
//        }
        for (int i=0; i<[ary_maintosave count]; i++) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[ary_maintosave objectAtIndex:i];
            if (sender.tag ==i)
            {
                [dict setValue:[NSString  stringWithFormat:@"%@",@"YES"] forKey:@"tableshow"];

            }
            else{
                

            }
            
            [table_address reloadData];
            
        }
        
    }
    else{
//        for (int i=0; i<[ary_maintosave count]; i++)
//        {
//            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
//            
//            dict=[ary_maintosave objectAtIndex:i];
//            
//            for (int i=0; i<[ary_maintosave count]; i++) {
//                
//                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
//                dict=[ary_maintosave objectAtIndex:i];
//                [dict setValue:[NSString  stringWithFormat:@"%@",@"NO"] forKey:@"tableshow"];
//                
//                [table_address reloadData];
//                
//            }
//        }
        
        for (int i=0; i<[ary_maintosave count]; i++) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[ary_maintosave objectAtIndex:i];
            if (sender.tag ==i)
            {
                [dict setValue:[NSString  stringWithFormat:@"%@",@"NO"] forKey:@"tableshow"];
                
            }
            else{
                
            }
            [table_address reloadData];
            
        }
    }
    

}
-(void)SelectAll_btnClick:(UIButton *)sender
{
    
    NSLog(@"apply_Btn Button Clicked");
    
    NSString*str_add = [NSString stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:sender.tag] valueForKey:@"address"]];
    NSString*str_id = [NSString stringWithFormat:@"%@",[[ary_dishdivary objectAtIndex:sender.tag] valueForKey:@"id"]];
    
    //    if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:sender.tag] valueForKey:@"delivaryidselected"]]isEqualToString:@"1"])
    //    {
    //        for (int i=0; i<[ary_maintosave count]; i++)
    //        {
    //            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    //
    //            dict=[ary_maintosave objectAtIndex:i];
    //
    //            for (int i=0; i<[ary_maintosave count]; i++) {
    //
    //                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    //                dict=[ary_maintosave objectAtIndex:i];
    //                [dict setObject:@"0" forKey:@"delivaryidselected"];
    //                [table_DishList reloadData];
    //
    //            }
    //        }
    //    }
    //    else{
    //
    //
    //        for (int i=0; i<[ary_maintosave count]; i++)
    //        {
    //            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    //
    //            dict=[ary_maintosave objectAtIndex:i];
    //
    //            for (int i=0; i<[ary_maintosave count]; i++) {
    //
    //                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    //                dict=[ary_maintosave objectAtIndex:i];
    //                [dict setObject:@"1" forKey:@"delivaryidselected"];
    //                [table_DishList reloadData];
    //
    //            }
    //        }
    //
    //    }
    
    
    
    if ([[NSString stringWithFormat:@"%@",[[ary_maintosave objectAtIndex:sender.tag] valueForKey:@"delivaryidselected"]]isEqualToString:@"1"])
    {
        for (int i=0; i<[ary_maintosave count]; i++)
        {
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[ary_maintosave objectAtIndex:i];
            [dict setValue:@"" forKey:@"addressdetails"];
            [dict setValue:@"0" forKey:@"delivaryid"];
            [dict setValue:[NSString  stringWithFormat:@"%@",@"0"] forKey:@"delivaryidselected"];
            dict=[ary_maintosave objectAtIndex:i];
            
            [table_address reloadData];
        }
    }
    else
    {
        for (int i=0; i<[ary_maintosave count]; i++)
        {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[ary_maintosave objectAtIndex:i];
            [dict setValue:str_add forKey:@"addressdetails"];
            [dict setValue:str_id forKey:@"delivaryid"];
            [dict setValue:[NSString  stringWithFormat:@"%@",@"1"] forKey:@"delivaryidselected"];
            dict=[ary_maintosave objectAtIndex:i];
            
            [table_address reloadData];
        }
        
        
    }
    
}
-(void)click_on_Personldetail:(UIButton *)sender
{
    [self AFupdatecart];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (textField.tag==-1)
    {
        for (int i=0; i<[ary_maintosave count]; i++)
        {
            
            if (i == textField.superview.tag)
            {
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                
                dict=[ary_maintosave objectAtIndex:i];
                
                for (int i=0; i<[ary_maintosave count]; i++) {
                    
                    if (i == textField.superview.tag) {
                        
                        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                        
                        dict=[ary_maintosave objectAtIndex:i];
                        [dict setObject:textField.text forKey:@"remarks"];
                        
                        
                        [table_address reloadData];
                    }
                    
                }
            }
            
        }
        
        
    }

}
# pragma mark Hcountry method

-(void)countryList
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpHcountry
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpHcountry:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self countryList];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpHcountry :(NSDictionary * )TheDict
{
    [ary_countrylist removeAllObjects];
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"Country_CityList"] count]; i++)
        {
            [ary_countrylist addObject:[[TheDict valueForKey:@"Country_CityList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        lblAdrs1.text=@"";
        
        
    }
    
    [tableview_Kcountry reloadData];
    
    
    
    
}

//-(void)click_Done
//{
//    [Mobilenumber resignFirstResponder];
//
//}



# pragma mark Hcity method

-(void)cityList
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            
                            
                            @"country_name"            :  lblAdrs1.text,
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpHcity
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpHcity:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self cityList];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpHcity :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    [ary_citylist removeAllObjects];
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        for (int i=0; i<[[TheDict valueForKey:@"City_List"] count]; i++)
        {
            [ary_citylist addObject:[[TheDict valueForKey:@"City_List"] objectAtIndex:i]];
        }
        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
                [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        lblAdrs2.text=@"";
        
        
    }
    
    [tableview_Kcity reloadData];
}



-(void)AFupdatecart
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)

    NSMutableString*str_tosend;
    str_tosend= [[NSMutableString alloc] init];
    
    
    for (int i=0; i<[ary_maintosave count];i++)
    {
        if (i==[ary_maintosave count]-1)
        {
            
            [str_tosend  appendString:[NSString stringWithFormat:@"%@|%@|%@|%@",[[ary_maintosave objectAtIndex:i]valueForKey:@"cart_id"],[[ary_maintosave objectAtIndex:i]valueForKey:@"item_id"],[[ary_maintosave objectAtIndex:i]valueForKey:@"delivaryid"],[[ary_maintosave objectAtIndex:i]valueForKey:@"remarks"]]];
        }
        else
        {
            
            [str_tosend  appendString:[NSString stringWithFormat:@"%@|%@|%@|%@:",[[ary_maintosave objectAtIndex:i]valueForKey:@"cart_id"],[[ary_maintosave objectAtIndex:i]valueForKey:@"item_id"],[[ary_maintosave objectAtIndex:i]valueForKey:@"delivaryid"],[[ary_maintosave objectAtIndex:i]valueForKey:@"remarks"]]];
            
        }
    }
    
    NSLog(@"str_tosend %@",str_tosend);
    
    
    
    NSDictionary *params =@{
                            @"cart_details"                         :  str_tosend
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:Kupdatecart
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefServeNow:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFupdatecart];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefServeNow :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        //[self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        if ([str_Type isEqualToString:@"3"])
        {
            PersonalDetailsVC*vc= [PersonalDetailsVC new];
            vc.str_servingtype = str_Type;
            [self presentViewController:vc animated:NO completion:nil];
        }
        else{
            
            
            ProceedToPaymentVC*vc= [ProceedToPaymentVC new];
            vc.str_typeofserve = str_Type;
            [self presentViewController:vc animated:NO completion:nil];

        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
