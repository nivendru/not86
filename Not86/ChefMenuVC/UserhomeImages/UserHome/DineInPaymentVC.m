//
//  DineInPaymentVC.m
//  Not86
//
//  Created by Interworld on 03/12/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "DineInPaymentVC.h"

@interface DineInPaymentVC ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIImageView *img_header;
    UITableView *img_table;
}
@end

@implementation DineInPaymentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self integrateHeader];
    [self integrateBody];
    
}


-(BOOL)prefersStatusBarHidden
{
    return YES;
}


-(void)integrateHeader
{
    
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu .frame = CGRectMake(10, 13,25,20);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu  addTarget:self action:@selector(click_on_Back_Btn:)forControlEvents:UIControlEventTouchUpInside];
    [icon_menu setImage:[UIImage imageNamed:@"img_arrow@2x"] forState:UIControlStateNormal];
    //    icon_menu.layer.borderWidth = 1.0;
    [icon_menu setBackgroundColor:[UIColor clearColor]];
    [img_header   addSubview:icon_menu ];
    
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+20,0, 150, 45);
    lbl_User_Sign_Up.text = @"Proceed to Payment";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-35, 9, 27, 27);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
    
}

-(void)integrateBody
{
    
    UIImageView *img_bg = [[UIImageView alloc]init];
    img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 440);
    //    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    img_bg.layer.borderWidth = 1.0;
    [img_bg setUserInteractionEnabled:YES];
    [self.view addSubview:img_bg];
    
    
//    UIImageView *icon_user = [[UIImageView alloc]init];
//    icon_user.frame = CGRectMake(10, 10, 30, 30);
//    [icon_user setImage:[UIImage imageNamed:@"iicon-on_reuest@2x.png"]];
//    //   icon_user.backgroundColor = [UIColor redColor];
//    [icon_user setUserInteractionEnabled:YES];
//    [img_bg addSubview:icon_user];
//    
//    
//    UILabel *lbl_Head = [[UILabel alloc]init];
//    lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
//    lbl_Head.text = @"On Request";
//    lbl_Head.font = [UIFont fontWithName:kFont size:20];
//    lbl_Head.textColor = [UIColor blackColor];
//    lbl_Head.backgroundColor = [UIColor clearColor];
//    [img_bg addSubview:lbl_Head];
    
    
//    UIImageView *line = [[UIImageView alloc]init];
//    line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
//    line.backgroundColor = [UIColor grayColor];
//    [line setUserInteractionEnabled:YES];
//    [img_bg addSubview:line];
    
    
    img_table= [[UITableView alloc] init ];
    img_table.frame  = CGRectMake(0,0,WIDTH-10,440);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    img_table.layer.borderWidth = 1.0;
    img_table.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:img_table];
    
    
    UIButton *btn_ConfirmOrder = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_ConfirmOrder.frame = CGRectMake(30,CGRectGetMaxY(img_bg.frame)+20,(WIDTH-60),45);
    btn_ConfirmOrder.layer.cornerRadius=4.0f;
    [btn_ConfirmOrder setTitle:@"Confirm Order" forState:UIControlStateNormal];
    [btn_ConfirmOrder setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_ConfirmOrder setBackgroundColor:[UIColor colorWithRed:39.0/255.0 green:37.0/255.0 blue:48.0/255.0 alpha:1.0f]];
    btn_ConfirmOrder.titleLabel.font=[UIFont fontWithName:kFont size:14];
    [btn_ConfirmOrder setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_ConfirmOrder addTarget:self action:@selector(btn_ConfirmOrder_Method:) forControlEvents:UIControlEventTouchUpInside];
    [self.view  addSubview:btn_ConfirmOrder];
    
    if (IS_IPHONE_6Plus)
    {
        img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 600);
//        icon_user.frame = CGRectMake(10, 10, 30, 30);
//        lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
//        line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
        img_table.frame  = CGRectMake(0,0,WIDTH-10,600);
        btn_ConfirmOrder.frame = CGRectMake(30,CGRectGetMaxY(img_bg.frame)+25,(WIDTH-60),50);
    }
    else if (IS_IPHONE_6)
    {
        img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 540);
//        icon_user.frame = CGRectMake(10, 10, 30, 30);
//        lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
//        line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
        img_table.frame  = CGRectMake(0,0,WIDTH-10,540);
        btn_ConfirmOrder.frame = CGRectMake(30,CGRectGetMaxY(img_bg.frame)+20,(WIDTH-60),50);
    }
    else if (IS_IPHONE_5)
    {
        img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 440);
//        icon_user.frame = CGRectMake(10, 10, 30, 30);
//        lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
//        line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
        img_table.frame  = CGRectMake(0,0,WIDTH-10,440);
        btn_ConfirmOrder.frame = CGRectMake(30,CGRectGetMaxY(img_bg.frame)+20,(WIDTH-60),45);
    }
    else
    {
        img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 360);
//        icon_user.frame = CGRectMake(10, 10, 30, 30);
//        lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
//        line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
        img_table.frame  = CGRectMake(0,0,WIDTH-10,360);
        btn_ConfirmOrder.frame = CGRectMake(30,CGRectGetMaxY(img_bg.frame)+20,(WIDTH-60),40);
    }
}


-(void)click_on_Back_Btn: (UIButton *)sender

{
    NSLog(@"click_on_Back_Btn Click");
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(void)btn_ConfirmOrder_Method: (UIButton *)sender

{
    NSLog(@"btn_ConfirmOrder_Method: Click");
//    DineInAddressDetailVC *obj = [DineInAddressDetailVC new];
//    [self presentViewController:obj animated:NO completion:nil];
    
}


#pragma mark TableviewDelegate&Datasources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(CGFloat)tableView: (UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}


-(UIView *)tableView : (UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView*sectionView;
    sectionView = [[UIView alloc]init];
    sectionView.frame = CGRectMake(0, 0, WIDTH-10, 40);
    sectionView.backgroundColor = [UIColor whiteColor];
    //    sectionView.layer.borderWidth = 1.0;
    
    
        UIImageView *icon_user = [[UIImageView alloc]init];
        icon_user.frame = CGRectMake(15, 5, 30, 30);
        [icon_user setImage:[UIImage imageNamed:@"iicon-on_reuest@2x.png"]];
        //   icon_user.backgroundColor = [UIColor redColor];
        [icon_user setUserInteractionEnabled:YES];
        [sectionView addSubview:icon_user];
    
    
    UILabel *firstLabel = [[UILabel alloc]init];
    firstLabel.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+10, 5, WIDTH-40, 30);
    firstLabel.backgroundColor = [UIColor clearColor];
    firstLabel.text = @"Food Now";
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    //    firstLabel.textAlignment = NSTextAlignmentCenter;
    firstLabel.textColor = [UIColor blackColor];
    firstLabel.font = [UIFont fontWithName:kFontBold size:15];
    firstLabel.lineBreakMode = NSLineBreakByWordWrapping;
    firstLabel.numberOfLines = 0;
    [sectionView addSubview:firstLabel];
    
 
    //    UILabel *lbl_Head = [[UILabel alloc]init];
    //    lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
    //    lbl_Head.text = @"On Request";
    //    lbl_Head.font = [UIFont fontWithName:kFont size:20];
    //    lbl_Head.textColor = [UIColor blackColor];
    //    lbl_Head.backgroundColor = [UIColor clearColor];
    //    [img_bg addSubview:lbl_Head];
    
    
    //    UIImageView *line = [[UIImageView alloc]init];
    //    line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
    //    line.backgroundColor = [UIColor grayColor];
    //    [line setUserInteractionEnabled:YES];
    //    [img_bg addSubview:line];

    
    UIImageView *Line = [[UIImageView alloc]init];
    Line.frame = CGRectMake(10, CGRectGetMaxY(firstLabel.frame)+5, WIDTH-40, 1.0);
    [Line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
    [sectionView addSubview:Line];
    
    return sectionView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 100.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView*sectionView;
    sectionView = [[UIView alloc]init];
    sectionView.frame = CGRectMake(0, 0, WIDTH-10, 100);
    sectionView.backgroundColor = [UIColor whiteColor];
    //    sectionView.layer.borderWidth = 1.0;
    
    
    UIImageView *img_Footer = [[UIImageView alloc]init];
    img_Footer.frame = CGRectMake(0, 0,WIDTH-10, 100);
    [img_Footer setUserInteractionEnabled:YES];
//    [img_Footer setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    [sectionView addSubview:img_Footer];
    
    
    UILabel *firstLabel = [[UILabel alloc]init];
    firstLabel.frame = CGRectMake(15, 2, (WIDTH-50)/1.5, 22);
    firstLabel.backgroundColor = [UIColor clearColor];
    firstLabel.text = @"Doe's Kitchen";
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    //    firstLabel.textAlignment = NSTextAlignmentCenter;
    firstLabel.textColor = [UIColor blackColor];
    firstLabel.font = [UIFont fontWithName:kFont size:15];
    firstLabel.lineBreakMode = NSLineBreakByWordWrapping;
    firstLabel.numberOfLines = 0;
    [img_Footer addSubview:firstLabel];
    
    
    UILabel *fstLabel = [[UILabel alloc]init];
    fstLabel.frame = CGRectMake(WIDTH-110, 4, 90, 22);
    fstLabel.backgroundColor = [UIColor clearColor];
    fstLabel.text = @"$ 1200.99";
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    fstLabel.textAlignment = NSTextAlignmentRight;
    fstLabel.textColor = [UIColor blackColor];
    fstLabel.font = [UIFont fontWithName:kFontBold size:15];
    fstLabel.lineBreakMode = NSLineBreakByWordWrapping;
    fstLabel.numberOfLines = 0;
    [img_Footer addSubview:fstLabel];
    
    
    UILabel *secondLabel = [[UILabel alloc]init];
    secondLabel.frame = CGRectMake(15, CGRectGetMaxY(firstLabel.frame), (WIDTH-50)/1.5, 22);
    secondLabel.backgroundColor = [UIColor clearColor];
    secondLabel.text = @"Doe's Kitchen afrweyedrtuy";
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    //    firstLabel.textAlignment = NSTextAlignmentCenter;
    secondLabel.textColor = [UIColor blackColor];
    secondLabel.font = [UIFont fontWithName:kFont size:15];
    secondLabel.lineBreakMode = NSLineBreakByWordWrapping;
    secondLabel.numberOfLines = 0;
    [img_Footer addSubview:secondLabel];
    
    
    
    
    UILabel *secLabel = [[UILabel alloc]init];
    secLabel.frame = CGRectMake(WIDTH-110, CGRectGetMaxY(firstLabel.frame), 90, 22);
    secLabel.backgroundColor = [UIColor clearColor];
    secLabel.text = @"$ 1233.13";
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    secLabel.textAlignment = NSTextAlignmentRight;
    secLabel.textColor = [UIColor blackColor];
    secLabel.font = [UIFont fontWithName:kFontBold size:15];
    secLabel.lineBreakMode = NSLineBreakByWordWrapping;
    secLabel.numberOfLines = 0;
    [img_Footer addSubview:secLabel];
    
    
    UILabel *thirdLabel = [[UILabel alloc]init];
    thirdLabel.frame = CGRectMake(15, CGRectGetMaxY(secondLabel.frame), (WIDTH-50)/1.5, 22);
    thirdLabel.backgroundColor = [UIColor clearColor];
    thirdLabel.text = @"dsgdhjfgku";
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    //    firstLabel.textAlignment = NSTextAlignmentCenter;
    thirdLabel.textColor = [UIColor blackColor];
    thirdLabel.font = [UIFont fontWithName:kFont size:15];
    thirdLabel.lineBreakMode = NSLineBreakByWordWrapping;
    thirdLabel.numberOfLines = 0;
    [img_Footer addSubview:thirdLabel];
    
    
    UILabel *thrdLabel = [[UILabel alloc]init];
    thrdLabel.frame = CGRectMake(WIDTH-110, CGRectGetMaxY(secondLabel.frame), 90, 22);
    thrdLabel.backgroundColor = [UIColor clearColor];
    thrdLabel.text = @"$ 122.19";
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    thrdLabel.textAlignment = NSTextAlignmentRight;
    thrdLabel.textColor = [UIColor blackColor];
    thrdLabel.font = [UIFont fontWithName:kFontBold size:15];
    thrdLabel.lineBreakMode = NSLineBreakByWordWrapping;
    thrdLabel.numberOfLines = 0;
    [img_Footer addSubview:thrdLabel];
    
    
    UILabel *fourLabel = [[UILabel alloc]init];
    fourLabel.frame = CGRectMake(15, CGRectGetMaxY(thrdLabel.frame), (WIDTH-50)/1.5, 22);
    fourLabel.backgroundColor = [UIColor clearColor];
    fourLabel.text = @"Total";
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    //    firstLabel.textAlignment = NSTextAlignmentCenter;
    fourLabel.textColor = [UIColor blackColor];
    fourLabel.font = [UIFont fontWithName:kFont size:15];
    fourLabel.lineBreakMode = NSLineBreakByWordWrapping;
    fourLabel.numberOfLines = 0;
    [img_Footer addSubview:fourLabel];
    

    
    
    UILabel *frLabel = [[UILabel alloc]init];
    frLabel.frame = CGRectMake(WIDTH-120, CGRectGetMaxY(thirdLabel.frame),100, 22);
    frLabel.backgroundColor = [UIColor clearColor];
    frLabel.text = @"$ 1221656.23";
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    frLabel.textAlignment = NSTextAlignmentRight;
    frLabel.textColor = [UIColor blackColor];
    frLabel.font = [UIFont fontWithName:kFontBold size:17];
    frLabel.lineBreakMode = NSLineBreakByWordWrapping;
    frLabel.numberOfLines = 0;
    [img_Footer addSubview:frLabel];
    
    
    return sectionView;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    //    for (UIView *view in cell.contentView.subviews)
    //    {
    //        [view removeFromSuperview];
    //    }
    
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-5, 200);
    
    //        if (IS_IPHONE_6Plus)
    //        {
    //            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH+5, 170);
    //
    //        }
    //        else if (IS_IPHONE_6)
    //        {
    //            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-5, 170);
    //
    //        }
    //        else
    //        {    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-10,170);
    //
    //        }
//    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    img_cellBackGnd.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    //    img_cellBackGnd.layer.borderWidth = 1.0;
    [cell.contentView addSubview:img_cellBackGnd];
    
    
    UILabel *cell_Head  = [[UILabel alloc]init];
    cell_Head.frame = CGRectMake(10, 0, WIDTH-40, 25. );
    //        round_red_val .text = [NSString stringWithFormat:@"%@",[array_round_red_val objectAtIndex:indexPath.row]];
    cell_Head .text = @"Requested serving time/date : 13/12/2015, 04:00 PM";
    //    cell_Head.textAlignment = NSTextAlignmentCenter;
    cell_Head .font = [UIFont fontWithName:kFont size:14];
    cell_Head .textColor = [UIColor blackColor];
    cell_Head .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:cell_Head];
    
    UIImageView *icon_Delivery = [[UIImageView alloc]init];
    icon_Delivery.frame = CGRectMake(10, CGRectGetMaxY(cell_Head.frame)+2, 40, 40);
    [icon_Delivery setImage:[UIImage imageNamed:@"iicon-on_reuest@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_Delivery setUserInteractionEnabled:YES];
    [img_cellBackGnd addSubview:icon_Delivery];
    
    
    UILabel *cell_Dish_Name  = [[UILabel alloc]init];
    cell_Dish_Name.frame = CGRectMake(CGRectGetMaxX(icon_Delivery.frame)+10, CGRectGetMaxY(cell_Head.frame)+2, WIDTH-100, 40. );
    //        round_red_val .text = [NSString stringWithFormat:@"%@",[array_round_red_val objectAtIndex:indexPath.row]];
    cell_Dish_Name .text = @"Mushroom Soup";
    //    cell_Dish_Name.textAlignment = NSTextAlignmentCenter;
    cell_Dish_Name .font = [UIFont fontWithName:kFontBold size:16];
    cell_Dish_Name .textColor = [UIColor blackColor];
    cell_Dish_Name .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:cell_Dish_Name];
    
    
    
    UIImageView *img_line = [[UIImageView alloc] init];
    img_line.frame = CGRectMake(10,CGRectGetMaxY(cell_Dish_Name.frame)+5, WIDTH-40, 0.5 );
    [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
    [img_cellBackGnd addSubview:img_line];
    
    
    
    UILabel *quantity_Lbl = [[UILabel alloc]init];
    quantity_Lbl.frame = CGRectMake(15,CGRectGetMaxY(img_line.frame),(WIDTH-50)/3.0, 30);
    quantity_Lbl.text = @"Quantity: 3";
    // likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
    quantity_Lbl.font = [UIFont fontWithName:kFont size:13];
//    quantity_Lbl.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    quantity_Lbl.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:quantity_Lbl];
    
    
    
    UILabel *serving_Lbl = [[UILabel alloc]init];
    serving_Lbl.frame = CGRectMake(CGRectGetMaxX(quantity_Lbl.frame),CGRectGetMaxY(img_line.frame),(WIDTH-50)/3.0, 30);
    serving_Lbl.text = @"$5.45/serving";
    // likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
    serving_Lbl.font = [UIFont fontWithName:kFont size:13];
//    serving_Lbl.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
//    serving_Lbl.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:serving_Lbl];
    
    
    UILabel *likes = [[UILabel alloc]init];
    likes.frame = CGRectMake(CGRectGetMaxX(serving_Lbl.frame),CGRectGetMaxY(img_line.frame),(WIDTH-50)/3.0, 30);
    likes.text = @"Subtotal: $2473.879";
    // likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
    likes.font = [UIFont fontWithName:kFont size:13];
    likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    likes.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:likes];
    
    
    UIImageView *img_line1 = [[UIImageView alloc] init];
    img_line1.frame = CGRectMake(10,CGRectGetMaxY(serving_Lbl.frame), WIDTH-40, 0.5 );
    [img_line1 setImage:[UIImage imageNamed:@"line-img@2x.png"]];
    [img_cellBackGnd addSubview:img_line1];
    
    
    UILabel *firstLabel = [[UILabel alloc]init];
    firstLabel.frame = CGRectMake(10, CGRectGetMaxY(img_line1.frame)+2, (WIDTH-70), 21);
    firstLabel.backgroundColor = [UIColor clearColor];
    firstLabel.text = @"Delivery Address";
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    //    firstLabel.textAlignment = NSTextAlignmentCenter;
    firstLabel.textColor = [UIColor blackColor];
    firstLabel.font = [UIFont fontWithName:kFontBold size:13];
    firstLabel.lineBreakMode = NSLineBreakByWordWrapping;
    firstLabel.numberOfLines = 0;
    [img_cellBackGnd addSubview:firstLabel];
    
    
    UILabel *secondLabel = [[UILabel alloc]init];
    secondLabel.frame = CGRectMake(10, CGRectGetMaxY(firstLabel.frame), (WIDTH-70), 21);
    secondLabel.backgroundColor = [UIColor clearColor];
    secondLabel.text = @"21 Mongefiu Road, #14, NY, BNJHFJ w45369";
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    //    firstLabel.textAlignment = NSTextAlignmentCenter;
    secondLabel.textColor = [UIColor blackColor];
    secondLabel.font = [UIFont fontWithName:kFont size:13];
    secondLabel.lineBreakMode = NSLineBreakByWordWrapping;
    secondLabel.numberOfLines = 0;
    [img_cellBackGnd addSubview:secondLabel];
    
    
    UILabel *thirdLabel = [[UILabel alloc]init];
    thirdLabel.frame = CGRectMake(10, CGRectGetMaxY(secondLabel.frame), (WIDTH-70), 21);
    thirdLabel.backgroundColor = [UIColor clearColor];
    thirdLabel.text = @"Special Requests/Remarks";
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    //    firstLabel.textAlignment = NSTextAlignmentCenter;
    thirdLabel.textColor = [UIColor blackColor];
    thirdLabel.font = [UIFont fontWithName:kFontBold size:13];
    thirdLabel.lineBreakMode = NSLineBreakByWordWrapping;
    thirdLabel.numberOfLines = 0;
    [img_cellBackGnd addSubview:thirdLabel];
    
    
    UILabel *fourLabel = [[UILabel alloc]init];
    fourLabel.frame = CGRectMake(10, CGRectGetMaxY(thirdLabel.frame), (WIDTH-70), 21);
    fourLabel.backgroundColor = [UIColor clearColor];
    fourLabel.text = @"NO Chilli and Cheese";
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    //    firstLabel.textAlignment = NSTextAlignmentCenter;
    fourLabel.textColor = [UIColor blackColor];
    fourLabel.font = [UIFont fontWithName:kFont size:13];
    fourLabel.lineBreakMode = NSLineBreakByWordWrapping;
    fourLabel.numberOfLines = 0;
    [img_cellBackGnd addSubview:fourLabel];
    
    
    UIImageView *img_MainLine = [[UIImageView alloc] init];
    img_MainLine.frame = CGRectMake(10,199, WIDTH-40, 0.5 );
    [img_MainLine setImage:[UIImage imageNamed:@"line-img@2x.png"]];
    [img_cellBackGnd addSubview:img_MainLine];
    
    //        UILabel *labl_time_and_date = [[UILabel alloc]init];
    //        //labl_time_and_date.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
    //        //        labl_time_and_date.text = [NSString stringWithFormat:@"%@",[array_time_and_date objectAtIndex:indexPath.row]];
    //        labl_time_and_date.font = [UIFont fontWithName:kFont size:10];
    //        labl_time_and_date.textColor = [UIColor blackColor];
    //        labl_time_and_date.backgroundColor = [UIColor clearColor];
    //        [img_cellBackGnd addSubview:labl_time_and_date];
    
    
    
    
    
    
    
    
    
    
    
    //        if (IS_IPHONE_6Plus)
    //        {
    //            img_dish.frame = CGRectMake(5,10, 95,  95 );
    //            btn_dish.frame=CGRectMake(0, 0, 95, 95);
    //            img_favorite.frame = CGRectMake(3,62, 30, 30);
    //            img_round_red.frame = CGRectMake(62, 3, 30,30);
    //            round_red_val .frame = CGRectMake(0,0,30, 30);
    //            icon_delete.frame = CGRectMake(WIDTH-60,15, 40, 40);
    //            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,10,200, 20);
    //            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,CGRectGetMaxY(dish_name.frame)+7,15, 25);
    //            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,CGRectGetMaxY(dish_name.frame)+7,200, 20);
    //            collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+5, CGRectGetMaxY(meters.frame)+15, 140, 35);
    //            doller_rate.frame = CGRectMake(WIDTH-65,85,200, 15);
    //            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+17, WIDTH-40, 0.5 );
    //            icon_take.frame = CGRectMake(15, CGRectGetMaxY(img_line.frame)+5, 30, 30);
    //            icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //            img_btn_seving_Now.frame = CGRectMake(180,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //            img_btn_chef_menu.frame = CGRectMake(220,CGRectGetMaxY(img_line.frame)+12,  30, 20);
    //            icon_thumb.frame = CGRectMake(325,CGRectGetMaxY(img_line.frame)+10, 20, 20);
    //            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+7,CGRectGetMaxY(img_line.frame)+12,100, 20);
    //            add_Date_lbl.frame = CGRectMake((WIDTH-10)/2.0+30,168,(WIDTH-10)/2.0, 20);
    //
    //            dish_name.font = [UIFont fontWithName:kFontBold size:15];
    //
    //        }
    //        else if (IS_IPHONE_6)
    //        {
    //            img_dish.frame = CGRectMake(5,10, 95,  95 );
    //            btn_dish.frame=CGRectMake(0, 0, 95, 95);
    //            img_favorite.frame = CGRectMake(3,62, 30, 30);
    //            img_round_red.frame = CGRectMake(62, 3, 30,30);
    //            round_red_val .frame = CGRectMake(0,0,30, 30);
    //            icon_delete.frame = CGRectMake(WIDTH-60,15, 40, 40);
    //            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,10,200, 20);
    //            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,CGRectGetMaxY(dish_name.frame)+7,15, 25);
    //            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,CGRectGetMaxY(dish_name.frame)+7,200, 20);
    //            collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+5, CGRectGetMaxY(meters.frame)+15, 140, 35);
    //            doller_rate.frame = CGRectMake(WIDTH-65,85,200, 15);
    //            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+17, WIDTH-40, 0.5 );
    //            icon_take.frame = CGRectMake(15, CGRectGetMaxY(img_line.frame)+5, 30, 30);
    //            icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //            img_btn_seving_Now.frame = CGRectMake(150,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //            img_btn_chef_menu.frame = CGRectMake(190,CGRectGetMaxY(img_line.frame)+12,  30, 20);
    //            icon_thumb.frame = CGRectMake(290,CGRectGetMaxY(img_line.frame)+10, 20, 20);
    //            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+7,CGRectGetMaxY(img_line.frame)+12,100, 20);
    //            add_Date_lbl.frame = CGRectMake((WIDTH-10)/2.0+15,168,(WIDTH-10)/2.0, 20);
    //            //                labl_time_and_date.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)-100,CGRectGetMaxY(img_cellBackGnd.frame)-3,300, 10);
    //            dish_name.font = [UIFont fontWithName:kFontBold size:15];
    //
    //        }
    //        else if (IS_IPHONE_5)
    //        {
    //            img_dish.frame = CGRectMake(5,10, 95,  95 );
    //            btn_dish.frame=CGRectMake(0, 0, 95, 95);
    //            img_favorite.frame = CGRectMake(3,62, 30, 30);
    //            img_round_red.frame = CGRectMake(62, 3, 30,30);
    //            round_red_val .frame = CGRectMake(0,0,30, 30);
    //            icon_delete.frame = CGRectMake(WIDTH-60,15, 40, 40);
    //            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,10,200, 20);
    //            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,CGRectGetMaxY(dish_name.frame)+7,15, 25);
    //            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,CGRectGetMaxY(dish_name.frame)+7,200, 20);
    //            collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+5, CGRectGetMaxY(meters.frame)+15, 140, 35);
    //
    //            doller_rate.frame = CGRectMake(WIDTH-65,85,200, 15);
    //            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+17, WIDTH-40, 0.5 );
    //            icon_take.frame = CGRectMake(15, CGRectGetMaxY(img_line.frame)+5, 30, 30);
    //            icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //            img_btn_chef_menu.frame = CGRectMake(170,CGRectGetMaxY(img_line.frame)+12,  30, 20);
    //            icon_thumb.frame = CGRectMake(240,CGRectGetMaxY(img_line.frame)+10, 20, 20);
    //            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+7,CGRectGetMaxY(img_line.frame)+12,100, 20);
    //            add_Date_lbl.frame = CGRectMake((WIDTH-10)/2.0,168,(WIDTH-10)/2.0, 20);
    //
    //            dish_name.font = [UIFont fontWithName:kFontBold size:15];
    //
    //        }
    //        else
    //        {
    //            img_dish.frame = CGRectMake(5,10, 95,  95 );
    //            btn_dish.frame=CGRectMake(0, 0, 95, 95);
    //            img_favorite.frame = CGRectMake(3,62, 30, 30);
    //            img_round_red.frame = CGRectMake(62, 3, 30,30);
    //            round_red_val .frame = CGRectMake(0,0,30, 30);
    //            icon_delete.frame = CGRectMake(WIDTH-60,15, 40, 40);
    //            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,10,200, 20);
    //            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,CGRectGetMaxY(dish_name.frame)+7,15, 25);
    //            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,CGRectGetMaxY(dish_name.frame)+7,200, 20);
    //            collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+5, CGRectGetMaxY(meters.frame)+15, 140, 35);
    //            doller_rate.frame = CGRectMake(WIDTH-65,85,200, 15);
    //            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+17, WIDTH-40, 0.5 );
    //            icon_take.frame = CGRectMake(15, CGRectGetMaxY(img_line.frame)+5, 30, 30);
    //            icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //            img_btn_chef_menu.frame = CGRectMake(170,CGRectGetMaxY(img_line.frame)+12,  30, 20);
    //            icon_thumb.frame = CGRectMake(240,CGRectGetMaxY(img_line.frame)+10, 20, 20);
    //            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+7,CGRectGetMaxY(img_line.frame)+12,100, 20);
    //            add_Date_lbl.frame = CGRectMake((WIDTH-10)/2.0,168,(WIDTH-10)/2.0, 20);
    //                dish_name.font = [UIFont fontWithName:kFontBold size:15];
    //        }
    
    
    
    
    
    
    return cell;
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
