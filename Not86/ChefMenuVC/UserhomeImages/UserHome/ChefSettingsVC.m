//
//  ChefSettingsVC.m
//  Not86
//
//  Created by User on 22/09/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ChefSettingsVC.h"
#import "MGInstagram.h"

@interface ChefSettingsVC ()<UIScrollViewDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIDocumentInteractionControllerDelegate>
{
    UIImageView * img_header;
    UIScrollView * scroll;
    
    UIButton * btn_new_order;
    UIButton * btn_push_notification;
    UIButton * btn_on_start_service;
    UIButton * btn_on_review_received;
    UIButton * btn_on_dish_runing_out;
    UIButton * btn_on_atomatically_allow;
    
    int new_order;
    int push_notifications;
    int start_of_service;
    int review_received;
    int dish_runing_out;
    int atomatically_allow;
    AppDelegate *delegate;
    
    NSMutableArray*ary_Mysettings;
    
    NSString * str_flag;
    
    
}
@end

@implementation ChefSettingsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    ary_Mysettings = [NSMutableArray new];
    [self integrateHeader];
    
    [self integrateBodyDesign];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
    [self AFMySettings:@"Y"];
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}


-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [icon_back addTarget:self action:@selector(click_on_back_btn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:icon_back];
    
    
    UILabel *lbl_support_on_header = [[UILabel alloc]init];
    lbl_support_on_header.frame = CGRectMake(CGRectGetMaxX(icon_back .frame)+30,0, 300, 45);
    lbl_support_on_header.text = @"Notification Settings";
    lbl_support_on_header.font = [UIFont fontWithName:kFont size:20];
    lbl_support_on_header.textColor = [UIColor whiteColor];
    lbl_support_on_header.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_support_on_header];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}

-(void)integrateBodyDesign
{
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.frame = CGRectMake(8,46,WIDTH-16,HEIGHT);
    scroll.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    UILabel *lbl_notifications = [[UILabel alloc]init];
    lbl_notifications.frame = CGRectMake(10,0, 100, 45);
    lbl_notifications.text = @"Notification";
    lbl_notifications.font = [UIFont fontWithName:kFontBold size:17];
    lbl_notifications.textColor = [UIColor blackColor];
    lbl_notifications.backgroundColor = [UIColor clearColor];
    [scroll addSubview:lbl_notifications];
    
    UIImageView *img_bg_for_new_orders = [[UIImageView alloc]init];
    img_bg_for_new_orders.frame = CGRectMake(0, CGRectGetMaxY(lbl_notifications.frame), WIDTH-20, 45);
    [img_bg_for_new_orders setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_new_orders setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_for_new_orders];
    
    UILabel *lbl_new_order = [[UILabel alloc]init];
    lbl_new_order.frame = CGRectMake(10,0, 200, 45);
    lbl_new_order.text = @"New Order";
    lbl_new_order.font = [UIFont fontWithName:kFont size:15];
    lbl_new_order.textColor = [UIColor blackColor];
    lbl_new_order.backgroundColor = [UIColor clearColor];
    [img_bg_for_new_orders addSubview:lbl_new_order];
    
    
    btn_new_order  =[UIButton buttonWithType:UIButtonTypeCustom];
    btn_new_order.frame=CGRectMake(CGRectGetMaxX(lbl_new_order.frame)+45,10,100,30);
    [ btn_new_order setImage:[UIImage imageNamed:@"icon-on@2x.png"] forState:UIControlStateSelected];
    [ btn_new_order setImage:[UIImage imageNamed:@"icon-off@2x.png"] forState:UIControlStateNormal];
    [ btn_new_order addTarget:self action:@selector(Click_on_new_order_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_new_orders  addSubview: btn_new_order];
    
    UIImageView *img_bg_for_push_notification = [[UIImageView alloc]init];
    img_bg_for_push_notification.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_new_orders.frame)+10, WIDTH-20, 45);
    [img_bg_for_push_notification setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_push_notification setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_for_push_notification];
    
    UILabel *lbl_push_notification = [[UILabel alloc]init];
    lbl_push_notification.frame = CGRectMake(10,0, 200, 45);
    lbl_push_notification.text = @"Push Notifications";
    lbl_push_notification.font = [UIFont fontWithName:kFont size:15];
    lbl_push_notification.textColor = [UIColor blackColor];
    lbl_push_notification.backgroundColor = [UIColor clearColor];
    [img_bg_for_push_notification  addSubview:lbl_push_notification];
    
    
    btn_push_notification  =[UIButton buttonWithType:UIButtonTypeCustom];
    btn_push_notification.frame=CGRectMake(CGRectGetMaxX(lbl_push_notification.frame)+45,10,100,30);
    [ btn_push_notification setImage:[UIImage imageNamed:@"icon-on@2x.png"] forState:UIControlStateSelected];
    
    
    [btn_push_notification setImage:[UIImage imageNamed:@"icon-off@2x.png"] forState:UIControlStateNormal];
    [btn_push_notification addTarget:self action:@selector(Click_on_push_notification_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_push_notification  addSubview:  btn_push_notification];
    
    UIImageView *img_bg_for_start_of_service = [[UIImageView alloc]init];
    img_bg_for_start_of_service.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_push_notification.frame)+10, WIDTH-20, 45);
    [img_bg_for_start_of_service setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_start_of_service setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_for_start_of_service];
    
    UILabel *lbl_satrt_service = [[UILabel alloc]init];
    lbl_satrt_service.frame = CGRectMake(10,0, 200, 45);
    lbl_satrt_service.text = @"Start of Service";
    lbl_satrt_service.font = [UIFont fontWithName:kFont size:15];
    lbl_satrt_service.textColor = [UIColor blackColor];
    lbl_satrt_service.backgroundColor = [UIColor clearColor];
    [img_bg_for_start_of_service  addSubview:lbl_satrt_service];
    
    
    btn_on_start_service  =[UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_start_service.frame=CGRectMake(CGRectGetMaxX(lbl_satrt_service.frame)+45,10,100,30);
    [btn_on_start_service setImage:[UIImage imageNamed:@"icon-on@2x.png"] forState:UIControlStateSelected];
    [btn_on_start_service setImage:[UIImage imageNamed:@"icon-off@2x.png"] forState:UIControlStateNormal];
    [btn_on_start_service addTarget:self action:@selector(Click_on_start_of_service_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_start_of_service addSubview:  btn_on_start_service];
    
    UIImageView *img_bg_for_review_received = [[UIImageView alloc]init];
    img_bg_for_review_received.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_start_of_service.frame)+10, WIDTH-20, 45);
    [img_bg_for_review_received setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_review_received setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_for_review_received];
    
    UILabel *lbl_review_received = [[UILabel alloc]init];
    lbl_review_received.frame = CGRectMake(10,0, 200, 45);
    lbl_review_received.text = @"Review Received";
    lbl_review_received.font = [UIFont fontWithName:kFont size:15];
    lbl_review_received.textColor = [UIColor blackColor];
    lbl_review_received.backgroundColor = [UIColor clearColor];
    [img_bg_for_review_received  addSubview:lbl_review_received];
    
    
    btn_on_review_received  =[UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_review_received.frame=CGRectMake(CGRectGetMaxX(lbl_review_received.frame)+45,10,100,30);
    [btn_on_review_received setImage:[UIImage imageNamed:@"icon-on@2x.png"] forState:UIControlStateSelected];
    [btn_on_review_received setImage:[UIImage imageNamed:@"icon-off@2x.png"] forState:UIControlStateNormal];
    [btn_on_review_received addTarget:self action:@selector(Click_on_review_received_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_review_received addSubview:  btn_on_review_received];
    
    UIImageView *img_bg_for_dish_runing_out = [[UIImageView alloc]init];
    img_bg_for_dish_runing_out.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_review_received.frame)+10, WIDTH-20, 45);
    [img_bg_for_dish_runing_out setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_dish_runing_out setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_for_dish_runing_out];
    
    UILabel *lbl_dish_runing_out = [[UILabel alloc]init];
    lbl_dish_runing_out.frame = CGRectMake(10,0, 200, 45);
    lbl_dish_runing_out.text = @"Dish Running Out";
    lbl_dish_runing_out.font = [UIFont fontWithName:kFont size:15];
    lbl_dish_runing_out.textColor = [UIColor blackColor];
    lbl_dish_runing_out.backgroundColor = [UIColor clearColor];
    [img_bg_for_dish_runing_out  addSubview:lbl_dish_runing_out];
    
    
    btn_on_dish_runing_out  =[UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_dish_runing_out.frame=CGRectMake(CGRectGetMaxX(lbl_dish_runing_out.frame)+45,10,100,30);
    [btn_on_dish_runing_out setImage:[UIImage imageNamed:@"icon-on@2x.png"] forState:UIControlStateSelected];
    [btn_on_dish_runing_out setImage:[UIImage imageNamed:@"icon-off@2x.png"] forState:UIControlStateNormal];
    [btn_on_dish_runing_out addTarget:self action:@selector(Click_on_dish_runing_out_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_dish_runing_out addSubview:  btn_on_dish_runing_out];
    
    UIImageView *img_bg_for_automatically_allow = [[UIImageView alloc]init];
    img_bg_for_automatically_allow.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_dish_runing_out.frame)+10, WIDTH-20, 45);
    [img_bg_for_automatically_allow setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_automatically_allow setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_for_automatically_allow];
    
    UILabel *lbl_atomatically_allow = [[UILabel alloc]init];
    lbl_atomatically_allow.frame = CGRectMake(10,0, 200, 45);
    lbl_atomatically_allow.text = @"Automatically allow me to\nconsider Orders on Request if I\ndon't have a scheduled serving\ntime.";
    lbl_atomatically_allow.font = [UIFont fontWithName:kFont size:15];
    lbl_atomatically_allow.numberOfLines = 4;
    lbl_atomatically_allow.textColor = [UIColor blackColor];
    lbl_atomatically_allow.backgroundColor = [UIColor clearColor];
    [img_bg_for_automatically_allow  addSubview:lbl_atomatically_allow];
    
    
    btn_on_atomatically_allow  =[UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_atomatically_allow.frame=CGRectMake(CGRectGetMaxX(lbl_atomatically_allow.frame)+45,10,100,30);
    [btn_on_atomatically_allow setImage:[UIImage imageNamed:@"icon-on@2x.png"] forState:UIControlStateSelected];
    [btn_on_atomatically_allow setImage:[UIImage imageNamed:@"icon-off@2x.png"] forState:UIControlStateNormal];
    [btn_on_atomatically_allow addTarget:self action:@selector(Click_on_atomatically_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_automatically_allow addSubview:  btn_on_atomatically_allow];
    
    
    
    if (IS_IPHONE_6Plus)
    {
        scroll.frame = CGRectMake(8,46,WIDTH-16,HEIGHT);
        lbl_notifications.frame = CGRectMake(10,0, 100, 45);
        img_bg_for_new_orders.frame = CGRectMake(0, CGRectGetMaxY(lbl_notifications.frame), WIDTH-20, 45);
        lbl_new_order.frame = CGRectMake(10,0, 200, 45);
        btn_new_order.frame=CGRectMake(CGRectGetMaxX(lbl_new_order.frame)+80,10,100,30);
        
        img_bg_for_push_notification.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_new_orders.frame)+10, WIDTH-20, 45);
        lbl_push_notification.frame = CGRectMake(10,0, 200, 45);
        btn_push_notification.frame=CGRectMake(CGRectGetMaxX(lbl_push_notification.frame)+80,10,100,30);
        
        img_bg_for_start_of_service.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_push_notification.frame)+10, WIDTH-20, 45);
        lbl_satrt_service.frame = CGRectMake(10,0, 200, 45);
        btn_on_start_service.frame=CGRectMake(CGRectGetMaxX(lbl_satrt_service.frame)+80,10,100,30);
        
        img_bg_for_review_received.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_start_of_service.frame)+10, WIDTH-20, 45);
        lbl_review_received.frame = CGRectMake(10,0, 200, 45);
        btn_on_review_received.frame=CGRectMake(CGRectGetMaxX(lbl_review_received.frame)+80,10,100,30);
        
        img_bg_for_dish_runing_out.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_review_received.frame)+10, WIDTH-20, 45);
        lbl_dish_runing_out.frame = CGRectMake(10,0, 200, 45);
        btn_on_dish_runing_out.frame=CGRectMake(CGRectGetMaxX(lbl_dish_runing_out.frame)+80,10,100,30);
        
        img_bg_for_automatically_allow.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_dish_runing_out.frame)+10, WIDTH-20, 100);
        lbl_atomatically_allow.frame = CGRectMake(10,0, 250,100);
        btn_on_atomatically_allow.frame=CGRectMake(CGRectGetMaxX(lbl_atomatically_allow.frame)+33,10,100,30);;
        
        
    }
    else if (IS_IPHONE_6)
    {
        scroll.frame = CGRectMake(8,46,WIDTH-16,HEIGHT);
        lbl_notifications.frame = CGRectMake(10,0, 100, 45);
        img_bg_for_new_orders.frame = CGRectMake(0, CGRectGetMaxY(lbl_notifications.frame), WIDTH-20, 45);
        lbl_new_order.frame = CGRectMake(10,0, 200, 45);
        btn_new_order.frame=CGRectMake(CGRectGetMaxX(lbl_new_order.frame)+45,10,100,30);
        img_bg_for_push_notification.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_new_orders.frame)+10, WIDTH-20, 45);
        lbl_push_notification.frame = CGRectMake(10,0, 200, 45);
        btn_push_notification.frame=CGRectMake(CGRectGetMaxX(lbl_push_notification.frame)+45,10,100,30);
        img_bg_for_start_of_service.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_push_notification.frame)+10, WIDTH-20, 45);
        lbl_satrt_service.frame = CGRectMake(10,0, 200, 45);
        btn_on_start_service.frame=CGRectMake(CGRectGetMaxX(lbl_satrt_service.frame)+45,10,100,30);
        img_bg_for_review_received.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_start_of_service.frame)+10, WIDTH-20, 45);
        lbl_review_received.frame = CGRectMake(10,0, 200, 45);
        btn_on_review_received.frame=CGRectMake(CGRectGetMaxX(lbl_review_received.frame)+45,10,100,30);
        
        img_bg_for_dish_runing_out.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_review_received.frame)+10, WIDTH-20, 45);
        lbl_dish_runing_out.frame = CGRectMake(10,0, 200, 45);
        btn_on_dish_runing_out.frame=CGRectMake(CGRectGetMaxX(lbl_dish_runing_out.frame)+45,10,100,30);
        
        img_bg_for_automatically_allow.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_dish_runing_out.frame)+10, WIDTH-20, 100);
        lbl_atomatically_allow.frame = CGRectMake(10,0, 250,100);
        btn_on_atomatically_allow.frame=CGRectMake(CGRectGetMaxX(lbl_atomatically_allow.frame)-4,10,100,30);
        
        
    }
    else
    {
        scroll.frame = CGRectMake(8,46,WIDTH-16,HEIGHT);
        lbl_notifications.frame = CGRectMake(10,0, 100, 45);
        img_bg_for_new_orders.frame = CGRectMake(0, CGRectGetMaxY(lbl_notifications.frame), WIDTH-20, 45);
        lbl_new_order.frame = CGRectMake(10,0, 200, 45);
        btn_new_order.frame=CGRectMake(CGRectGetMaxX(lbl_new_order.frame),10,100,30);
        
        img_bg_for_push_notification.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_new_orders.frame)+10, WIDTH-20, 45);
        lbl_push_notification.frame = CGRectMake(10,0, 200, 45);
        btn_push_notification.frame=CGRectMake(CGRectGetMaxX(lbl_push_notification.frame)+45,10,100,30);
        
        img_bg_for_start_of_service.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_push_notification.frame)+10, WIDTH-20, 45);
        lbl_satrt_service.frame = CGRectMake(10,0, 200, 45);
        btn_on_start_service.frame=CGRectMake(CGRectGetMaxX(lbl_satrt_service.frame)+45,10,100,30);
        
        img_bg_for_review_received.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_start_of_service.frame)+10, WIDTH-20, 45);
        lbl_review_received.frame = CGRectMake(10,0, 200, 45);
        btn_on_review_received.frame=CGRectMake(CGRectGetMaxX(lbl_review_received.frame)+45,10,100,30);
        
        img_bg_for_dish_runing_out.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_review_received.frame)+10, WIDTH-20, 45);
        lbl_dish_runing_out.frame = CGRectMake(10,0, 200, 45);
        btn_on_dish_runing_out.frame=CGRectMake(CGRectGetMaxX(lbl_dish_runing_out.frame)+45,10,100,30);
        
        img_bg_for_automatically_allow.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_dish_runing_out.frame)+10, WIDTH-20, 100);
        lbl_atomatically_allow.frame = CGRectMake(10,0, 250,100);
        btn_on_atomatically_allow.frame=CGRectMake(CGRectGetMaxX(lbl_atomatically_allow.frame)-4,10,100,30);
        
        
        lbl_new_order.font = [UIFont fontWithName:kFont size:12];
        lbl_push_notification.font = [UIFont fontWithName:kFont size:12];
        lbl_satrt_service.font = [UIFont fontWithName:kFont size:12];
        lbl_review_received.font = [UIFont fontWithName:kFont size:12];
        lbl_dish_runing_out.font = [UIFont fontWithName:kFont size:12];
        lbl_atomatically_allow.font = [UIFont fontWithName:kFont size:12];
        
        
        btn_new_order.frame=CGRectMake(CGRectGetMaxX(lbl_new_order.frame),10,80,30);
        btn_push_notification.frame=CGRectMake(CGRectGetMaxX(lbl_push_notification.frame),10,80,30);
        btn_on_start_service.frame=CGRectMake(CGRectGetMaxX(lbl_satrt_service.frame),10,80,30);
        btn_on_review_received.frame=CGRectMake(CGRectGetMaxX(lbl_review_received.frame),10,80,30);
        btn_on_dish_runing_out.frame=CGRectMake(CGRectGetMaxX(lbl_dish_runing_out.frame),10,80,30);
        btn_on_atomatically_allow.frame=CGRectMake(CGRectGetMaxX(lbl_atomatically_allow.frame)-48,10,80,30);
        
    }
    
    
    
    
    
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma cliclk events
-(void)click_on_back_btn:(UIButton *)sender
{
    NSLog(@"click_on_back_button");
    [self dismissViewControllerAnimated:NO completion:nil];
}
-(void)Click_on_new_order_btn:(UIButton *)sender
{
    NSLog(@"click_on_off_btn:");
    if([sender isSelected])
    {
        
        new_order  = 0;
        [sender setSelected:NO];
        [self AFMySettings:@""];
    }
    else
    {
        
        
        new_order = 1;
        [sender setSelected:YES];
        [self AFMySettings:@""];
    }
    
    
    
    
}
-(void)Click_on_push_notification_btn:(UIButton *)sender
{
    NSLog(@"click_on_off_btn:");
    if([sender isSelected])
    {
        
        push_notifications = 0;
        [sender setSelected:NO];
        [self AFMySettings:@""];
    }
    else
    {
        
        
        push_notifications = 1;
        [sender setSelected:YES];
        [self AFMySettings:@""];
    }
    
    
    
    
}
-(void)Click_on_start_of_service_btn:(UIButton *)sender
{
    NSLog(@"click_on_off_btn:");
    if([sender isSelected])
    {
        
        start_of_service = 0;
        [sender setSelected:NO];
        [self AFMySettings:@""];
    }
    else
    {
        
        
        start_of_service = 1;
        [sender setSelected:YES];
        [self AFMySettings:@""];
    }
    
    
    
    
}
-(void)Click_on_review_received_btn:(UIButton *)sender
{
    NSLog(@"click_on_off_btn:");
    if([sender isSelected])
    {
        
        review_received = 0;
        [sender setSelected:NO];
        [self AFMySettings:@""];
    }
    else
    {
        
        
        review_received = 1;
        [sender setSelected:YES];
        [self AFMySettings:@""];
    }
    
    
}
-(void)Click_on_dish_runing_out_btn:(UIButton *)sender
{
    NSLog(@"click_on_off_btn:");
    if([sender isSelected])
    {
        
        dish_runing_out = 0;
        [sender setSelected:NO];
        [self AFMySettings:@""];
    }
    else
    {
        
        
        dish_runing_out = 1;
        [sender setSelected:YES];
        [self AFMySettings:@""];
    }
    
    
    
    
}
-(void)Click_on_atomatically_btn:(UIButton *)sender
{
    NSLog(@"click_on_off_btn:");
    if([sender isSelected])
    {
        
        atomatically_allow = 0;
        [sender setSelected:NO];
        [self AFMySettings:@""];
    }
    else
    {
        
        
        atomatically_allow = 1;
        [sender setSelected:YES];
        [self AFMySettings:@""];
    }
    
    
    
    
}


-(void)click_on_btn:(UIButton *)sender
{
    NSLog(@"click_on_btn:");
    
    
}

#pragma functionality

-(void) AFMySettings:(NSString *)strStatus
{
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    // int str_slidervalve = [[NSString stringWithFormat:@"%f",mSlider.value] intValue];
    //    [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"]
    
    NSDictionary *    params;
    if ([strStatus isEqualToString:@"Y"])
    {
        params =@{
                  @"uid"                  :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                  
                  };
        
        
    }
    else
    {
        
        params =@{
                  @"uid"                  :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                  @"new_order"                :  [NSString stringWithFormat:@"%d",new_order],
                  @"push_notification"        :  [NSString stringWithFormat:@"%d",push_notifications],
                  @"start_of_service"         :  [NSString stringWithFormat:@"%d",start_of_service],
                  @"review_recieved"          :  [NSString stringWithFormat:@"%d",review_received],
                  @"dish_runningout "         :  [NSString stringWithFormat:@"%d",dish_runing_out],
                  @"orders_on_request"        :  [NSString stringWithFormat:@"%d",atomatically_allow]
                  };
        
    }
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:@"webservices/settings-status.json"
                                    
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseMySettings:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             
                                             //                                             [self popup_Alertview:@"Please check your internet connection"];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully login");
                                             [self AFMySettings:strStatus];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseMySettings :(NSDictionary * )TheDict
{
    NSLog(@"ResponseMysettings: %@",TheDict);
    
    [ary_Mysettings removeAllObjects];
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[TheDict valueForKey:@"user_info"]  forKey:@"UserInfo"];
        [defaults synchronize];
        
        [ary_Mysettings addObject:[TheDict valueForKey:@"user_info"]];
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        // [self popup_Alertview:[TheDict valueForKey:@"Message"]];
        
        
    }
    if ([ary_Mysettings count]>0)
    {
        
        NSString *Str_new_order = [NSString stringWithFormat:@"%@", [[ary_Mysettings objectAtIndex:0]valueForKey:@"New_order"]] ;
        
        new_order = [Str_new_order intValue];
        
        push_notifications=[[NSString stringWithFormat:@"%@", [[ary_Mysettings objectAtIndex:0]valueForKey:@"Push_notification"] ] intValue];
        
        start_of_service  = [[NSString stringWithFormat:@"%@", [[ary_Mysettings objectAtIndex:0]valueForKey:@"Start_of_service"]]intValue];
        
        review_received  = [[NSString stringWithFormat:@"%@", [[ary_Mysettings objectAtIndex:0]valueForKey:@"Review_recieved"]]intValue];
        
        dish_runing_out  = [[NSString stringWithFormat:@"%@", [[ary_Mysettings objectAtIndex:0]valueForKey:@"Dishrunnig_out"]]intValue];
        
        atomatically_allow  = [[NSString stringWithFormat:@"%@", [[ary_Mysettings objectAtIndex:0]valueForKey:@"Orders_on_request"]]intValue];
        
        
        str_flag=@"N";
        
        
        if (new_order==0)
        {
            [btn_new_order setSelected:NO];
            
        }
        else
        {
            [btn_new_order setSelected:YES];
            
            
            
        }
        
        if (push_notifications==0)
        {
            [btn_push_notification setSelected:NO];
            
        }
        else
        {
            [btn_push_notification setSelected:YES];
            
        }
        if(start_of_service == 0)
        {
            [btn_on_start_service setSelected:NO];
        }
        else
        {
            [btn_on_start_service setSelected:YES];
        }
        if (review_received == 0)
        {
            [btn_on_review_received setSelected:NO];
        }
        else
        {
            [btn_on_review_received setSelected:YES];
        }
        if (dish_runing_out == 0)
        {
            [btn_on_dish_runing_out setSelected:NO];
        }
        else
        {
            [btn_on_dish_runing_out setSelected:YES];
        }
        if (atomatically_allow == 0)
        {
            [btn_on_atomatically_allow setSelected:NO];
        }
        else
        {
            [btn_on_atomatically_allow setSelected:YES];
        }
        
        
        
        
    }
}



/////instagram end///





/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
