//
//  UserDishRivewsVC.m
//  Not86
//
//  Created by Interworld on 10/08/15.
//  Copyright (c) 2015 Interworld. All rights reserved.
//

#import "UserDishRivewsVC.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)



@interface UserDishRivewsVC ()<UITextViewDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIImageView *img_header;
    UIImageView *img_bg;
    UIImageView *img_strip;
    UIImageView *img_strip1;
    UIImageView *img_rect;
    UITableView * img_table;
    UITextView  *txtview_message;
    UIView *view_for_reviews;
    UIScrollView *scroll;
    UITextView *txtview_over_all_value;
    UITextView *txtview_taste_value;
}
@end

@implementation UserDishRivewsVC

-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBody];
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)integrateHeader
{
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back addTarget:self action:@selector(btn_back_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_back];
    
    
    UILabel *lbl_dish_detail = [[UILabel alloc]init];
    lbl_dish_detail.frame = CGRectMake(CGRectGetMaxX(icon_back.frame)+30,0, 150, 45);
    lbl_dish_detail.text = @"Dish Detail";
    lbl_dish_detail.font = [UIFont fontWithName:@"Helvitica" size:10];
    lbl_dish_detail.textColor = [UIColor whiteColor];
    lbl_dish_detail.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_dish_detail];
    
    
    
    
    UIImageView *icon_logo = [[UIImageView alloc]init];
    icon_logo .frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_logo  setImage:[UIImage imageNamed:@"img_logo@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_logo  setUserInteractionEnabled:YES];
    [img_header addSubview:icon_logo ];
    
    
}

-(void)integrateBody
{
    
    
    img_bg = [[UIImageView alloc]init];
    //    img_bg .frame = CGRectMake(0,CGRectGetMaxY(img_header.frame), WIDTH, 150);
    [img_bg  setImage:[UIImage imageNamed:@"img-sc-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg  setUserInteractionEnabled:YES];
    [ self.view addSubview:img_bg ];
    
    
    UILabel *text_dish_name = [[UILabel alloc]init];
    //   text_dish_name  .frame = CGRectMake(93,88,200, 15);
    text_dish_name .text = @"Prawn Ramen";
    text_dish_name  .font = [UIFont fontWithName:@"Arial" size:16];
    text_dish_name .textColor = [UIColor whiteColor];
    text_dish_name  .backgroundColor = [UIColor clearColor];
    [img_bg addSubview:text_dish_name  ];
    
    
    UIButton *btn_favorite = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_favorite.frame = CGRectMake(18,118,25,25);
    btn_favorite .backgroundColor = [UIColor clearColor];
    [btn_favorite  addTarget:self action:@selector(btn_favorite_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_favorite setImage:[UIImage imageNamed:@"icon-favorite@2x.png"] forState:UIControlStateNormal];
    [img_bg   addSubview:btn_favorite];
    
    UIButton *btn_fb = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_fb.frame = CGRectMake(CGRectGetMaxX(btn_favorite.frame)+15,118,25,25);
    btn_fb .backgroundColor = [UIColor clearColor];
    [btn_fb  addTarget:self action:@selector(btn_fb_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_fb setImage:[UIImage imageNamed:@"icon-fb@2x.png"] forState:UIControlStateNormal];
    [img_bg   addSubview:btn_fb];
    
    UIButton *btn_tw = [UIButton buttonWithType:UIButtonTypeCustom];
    //    btn_tw.frame = CGRectMake(CGRectGetMaxX(btn_fb.frame)+5,118,25,25);
    btn_tw .backgroundColor = [UIColor clearColor];
    [btn_tw  addTarget:self action:@selector(btn_tw_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_tw setImage:[UIImage imageNamed:@"icon-tw@2x.png"] forState:UIControlStateNormal];
    [img_bg   addSubview:btn_tw];
    
    UIButton *btn_cm = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_cm.frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+5,118,25,25);
    btn_cm .backgroundColor = [UIColor clearColor];
    [btn_cm  addTarget:self action:@selector(btn_cm_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_cm setImage:[UIImage imageNamed:@"icon-cm@2x.png"] forState:UIControlStateNormal];
    [img_bg   addSubview:btn_cm];
    
    UILabel *text_rate = [[UILabel alloc]init];
    //    text_rate  .frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+150,118,200,25);
    text_rate .text = @"$7.50";
    text_rate  .font = [UIFont fontWithName:@"Arial" size:16];
    text_rate .textColor = [UIColor whiteColor];
    text_rate  .backgroundColor = [UIColor clearColor];
    [img_bg addSubview:text_rate];
    
    UILabel *text_dish_details = [[UILabel alloc]init];
    //   text_dish_details.frame = CGRectMake(40,CGRectGetMaxY(img_header.frame)+115,200, 15);
    text_dish_details.text = @"Dish Detail";
    text_dish_details.font= [UIFont fontWithName:@"Arial" size:15];
    // text_chef.textColor = [UIColor redColor];
    text_dish_details.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:text_dish_details];
    
    
    UIButton *btn_food_details = [UIButton buttonWithType:UIButtonTypeCustom];
    //    btn_food_details.frame = CGRectMake(0,CGRectGetMaxY(img_header.frame)+115, WIDTH/2, 15);
    btn_food_details .backgroundColor = [UIColor clearColor];
    [btn_food_details addTarget:self action:@selector(btn_food_details_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg   addSubview:btn_food_details ];
    
    img_strip = [[UIImageView alloc]init];
    //    img_strip.frame = CGRectMake(40, CGRectGetMaxY(img_header.frame)+135, 80, 2);
    [img_strip setImage:[UIImage imageNamed:@"black strip@2x.png"]];
    [img_strip setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip];
    
    UILabel *text_reviews = [[UILabel alloc]init];
    //    text_reviews .frame = CGRectMake(200,CGRectGetMaxY(img_header.frame)+115,200, 15);
    text_reviews.text = @"Reviews";
    text_reviews .font = [UIFont fontWithName:@"Arial" size:15];
    //text_cuisen .textColor = [UIColor redColor];
    text_reviews .backgroundColor = [UIColor clearColor];
    [img_bg addSubview:text_reviews ];
    
    
    UIButton *btn_reviews = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_reviews.frame = CGRectMake(WIDTH/2+10,CGRectGetMaxY(img_header.frame)+115,WIDTH/2, 15);
    btn_reviews .backgroundColor = [UIColor clearColor];
    [btn_reviews addTarget:self action:@selector(btn_reviews_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg   addSubview:btn_reviews];
    
    
    img_strip1 = [[UIImageView alloc]init];
    //   img_strip1.frame = CGRectMake(200, CGRectGetMaxY(img_header.frame)+135, 57, 2);
    [img_strip1 setImage:[UIImage imageNamed:@"black strip@2x.png"]];
    [img_strip1 setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip1];
    
    UILabel *number_of_reviews = [[UILabel alloc]init];
    //   number_of_reviews.frame = CGRectMake(260,165,200, 10);
    number_of_reviews.text = @"[123]";
    number_of_reviews.font = [UIFont fontWithName:@"Arial" size:10];
    number_of_reviews .textColor = [UIColor blackColor];
    number_of_reviews .backgroundColor = [UIColor clearColor];
    [img_bg addSubview:number_of_reviews ];
    
    
    UIImageView *img_percentage_tag = [[UIImageView alloc]init];
    //   img_percentage_tag.frame = CGRectMake(273,151, 25, 13);
    [img_percentage_tag setImage:[UIImage imageNamed:@"img-bg-tx@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_percentage_tag  setUserInteractionEnabled:YES];
    [img_bg addSubview:img_percentage_tag ];
    
    view_for_reviews = [[UIView alloc]init];
    //    view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame)+35,WIDTH,HEIGHT+90);
    view_for_reviews.backgroundColor=[UIColor clearColor];
    [self.view  addSubview: view_for_reviews];
    
    
    img_rect = [[UIImageView alloc]init];
    //  img_rect .frame = CGRectMake(10,10,305, 75);
    [img_rect setImage:[UIImage imageNamed:@"bg1@2x.png"]];
    img_rect.backgroundColor = [UIColor clearColor];
    [img_rect setUserInteractionEnabled:YES];
    [view_for_reviews addSubview:img_rect ];
    
    UILabel *reviews_in_rect = [[UILabel alloc]init];
    //   reviews_in_rect .frame = CGRectMake(45,7,200, 13);
    reviews_in_rect.text = @"Reviews";
    reviews_in_rect .font = [UIFont fontWithName:@"Arial" size:11];
    //text_cuisen .textColor = [UIColor redColor];
    reviews_in_rect .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:reviews_in_rect];
    
    
    UILabel *reviews_rating_in_rect = [[UILabel alloc]init];
    //   reviews_rating_in_rect.frame = CGRectMake(20,5,200, 15);
    reviews_rating_in_rect.text = @"177";
    reviews_rating_in_rect .font = [UIFont fontWithName:@"Arial" size:13];
    //text_cuisen .textColor = [UIColor redColor];
    reviews_rating_in_rect .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:reviews_rating_in_rect];
    
    
    UIImageView *img_thumb = [[UIImageView alloc]init];
    //    img_thumb.frame = CGRectMake(CGRectGetMaxX(reviews_in_rect.frame)-70,2, 20,20);
    [img_thumb setImage:[UIImage imageNamed:@"icon-thumb@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_thumb  setUserInteractionEnabled:YES];
    [img_rect addSubview:img_thumb ];
    
    UILabel *label_over_all = [[UILabel alloc]init];
    //    label_over_all .frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+10,7,200, 15);
    label_over_all .text = @"Overall:";
    label_over_all  .font = [UIFont fontWithName:@"Arial" size:13];
    label_over_all   .textColor = [UIColor redColor];
    label_over_all  .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:label_over_all ];
    
    txtview_over_all_value = [[UITextView alloc]init];
    //    txtview_over_all_value.frame = CGRectMake(247,0,80, 30);
    txtview_over_all_value.scrollEnabled = YES;
    txtview_over_all_value.userInteractionEnabled = YES;
    txtview_over_all_value.font = [UIFont fontWithName:@"Arial" size:12];
    txtview_over_all_value.backgroundColor = [UIColor clearColor];
    txtview_over_all_value.delegate = self;
    txtview_over_all_value.textColor = [UIColor redColor];
    txtview_over_all_value.text =@"87.4% ";
    [img_rect addSubview:txtview_over_all_value];
    
    UIImageView *img_line = [[UIImageView alloc]init];
    //  img_line .frame = CGRectMake(10,35, WIDTH-40,0.3);
    [img_line  setImage:[UIImage imageNamed:@"line@2x.png"]];
    img_line.backgroundColor = [UIColor lightGrayColor];
    [img_line   setUserInteractionEnabled:YES];
    [img_rect addSubview:img_line  ];
    
    UILabel *label_test_in_rect = [[UILabel alloc]init];
    //   label_test_in_rect .frame = CGRectMake(10,CGRectGetMaxY(img_line.frame)+20,200, 15);
    label_test_in_rect.text = @"Taste:";
    label_test_in_rect .font = [UIFont fontWithName:@"Arial" size:13];
    label_test_in_rect  .textColor = [UIColor blackColor];
    label_test_in_rect .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:label_test_in_rect ];
    
    txtview_taste_value = [[UITextView alloc]init];
    //    txtview_taste_value.frame = CGRectMake(45,CGRectGetMaxY(img_line.frame)+13,80, 30);
    txtview_taste_value.scrollEnabled = YES;
    txtview_taste_value.userInteractionEnabled = YES;
    txtview_taste_value.font = [UIFont fontWithName:@"Arial" size:12];
    txtview_taste_value.backgroundColor = [UIColor clearColor];
    txtview_taste_value.delegate = self;
    txtview_taste_value.textColor = [UIColor redColor];
    txtview_taste_value.text =@"75% ";
    [img_rect addSubview: txtview_taste_value];
    
    
    UILabel *label_appeal = [[UILabel alloc]init];
    //   label_appeal .frame = CGRectMake(CGRectGetMaxX(txtview_taste_value.frame)-20,CGRectGetMaxY(img_line.frame)+20,100,15);
    label_appeal .text = @"Appeal:";
    label_appeal   .font = [UIFont fontWithName:@"Arial" size:13];
    label_appeal   .textColor = [UIColor blackColor];
    label_appeal  .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:label_appeal  ];
    
    UITextView * txtview_appeal_value = [[UITextView alloc]init];
    //   txtview_appeal_value.frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-55,CGRectGetMaxY(img_line.frame)+13,80, 30);
    txtview_appeal_value.scrollEnabled = YES;
    txtview_appeal_value.userInteractionEnabled = YES;
    txtview_appeal_value.font = [UIFont fontWithName:@"Arial" size:12];
    txtview_appeal_value.backgroundColor = [UIColor clearColor];
    txtview_appeal_value.delegate = self;
    txtview_appeal_value.textColor = [UIColor redColor];
    txtview_appeal_value.text =@"85% ";
    [img_rect addSubview: txtview_appeal_value];
    
    UILabel *label_value = [[UILabel alloc]init];
    //   label_value .frame = CGRectMake(CGRectGetMaxX(txtview_appeal_value.frame)-20,CGRectGetMaxY(img_line.frame)+20,100,15);
    label_value .text = @"Value:";
    label_value   .font = [UIFont fontWithName:@"Arial" size:13];
    label_value   .textColor = [UIColor blackColor];
    label_value  .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:label_value];
    
    
    UITextView * txtview_value_value = [[UITextView alloc]init];
    //   txtview_value_value.frame = CGRectMake(CGRectGetMaxX(label_value.frame)-55,CGRectGetMaxY(img_line.frame)+13,80, 30);
    txtview_value_value.scrollEnabled = YES;
    txtview_value_value.userInteractionEnabled = YES;
    txtview_value_value.font = [UIFont fontWithName:@"Arial" size:12];
    txtview_value_value.backgroundColor = [UIColor clearColor];
    txtview_value_value.delegate = self;
    txtview_value_value.textColor = [UIColor redColor];
    txtview_value_value.text =@"75% ";
    [img_rect addSubview: txtview_value_value];
    
    
    
    
    
#pragma mark Tableview
    
    img_table= [[UITableView alloc] init ];
    //   img_table.frame  = CGRectMake(13,305,295,250);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    img_table.backgroundColor = [UIColor clearColor];
    [view_for_reviews addSubview:img_table];
    
    /* img_header=[[UIImageView alloc]init];
     img_header.frame = CGRectMake(0, CGRectGetMaxY( img_table.frame), WIDTH, 30);
     [img_header setUserInteractionEnabled:YES];
     img_header.image=[UIImage imageNamed:@"img-head-bg@2x.png"];
     [img_bg addSubview:img_header];
     
     UILabel *lbl_add_to_cart = [[UILabel alloc]init];
     lbl_add_to_cart.frame = CGRectMake(10,5, 150, 45);
     lbl_add_to_cart.text = @"ADD TO CART";
     lbl_add_to_cart.font = [UIFont fontWithName:@"Helvitica" size:10];
     lbl_add_to_cart.textColor = [UIColor whiteColor];
     lbl_add_to_cart.backgroundColor = [UIColor clearColor];
     [img_header addSubview:lbl_add_to_cart];*/
    
    
    
    
    if (IS_IPHONE_6Plus)
    {
        img_bg .frame = CGRectMake(0,CGRectGetMaxY(img_header.frame), WIDTH, 150);
        text_dish_name  .frame = CGRectMake(93,88,200, 15);
        btn_favorite.frame = CGRectMake(18,118,25,25);
        btn_fb.frame = CGRectMake(CGRectGetMaxX(btn_favorite.frame)+15,118,25,25);
        btn_tw.frame = CGRectMake(CGRectGetMaxX(btn_fb.frame)+5,118,25,25);
        btn_cm.frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+5,118,25,25);
        text_rate  .frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+150,118,200,25);
        text_dish_details.frame = CGRectMake(40,CGRectGetMaxY(img_header.frame)+115,200, 15);
        btn_food_details.frame = CGRectMake(0,CGRectGetMaxY(img_header.frame)+115, WIDTH/2, 15);
        img_strip.frame = CGRectMake(40, CGRectGetMaxY(img_header.frame)+135, 80, 2);
        text_reviews .frame = CGRectMake(200,CGRectGetMaxY(img_header.frame)+115,200, 15);
        btn_reviews.frame = CGRectMake(WIDTH/2+10,CGRectGetMaxY(img_header.frame)+115,WIDTH/2, 15);
        img_strip1.frame = CGRectMake(200, CGRectGetMaxY(img_header.frame)+135, 57, 2);
        number_of_reviews.frame = CGRectMake(260,165,200, 10);
        img_percentage_tag.frame = CGRectMake(273,151, 25, 13);
        view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame)+35,WIDTH,HEIGHT+90);
        img_rect .frame = CGRectMake(10,10,WIDTH-20, 75);
        reviews_in_rect .frame = CGRectMake(45,7,200, 13);
        reviews_rating_in_rect.frame = CGRectMake(20,5,200, 15);
        img_thumb.frame = CGRectMake(CGRectGetMaxX(reviews_in_rect.frame),2, 20,20);
        label_over_all .frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+5,7,200, 15);
        txtview_over_all_value.frame = CGRectMake(310,0,80, 30);
        img_line .frame = CGRectMake(10,35, WIDTH-40,0.3);
        label_test_in_rect .frame = CGRectMake(10,CGRectGetMaxY(img_line.frame)+20,200, 15);
        txtview_taste_value.frame = CGRectMake(45,CGRectGetMaxY(img_line.frame)+13,80, 30);
        label_appeal .frame = CGRectMake(CGRectGetMaxX(txtview_taste_value.frame)+15,CGRectGetMaxY(img_line.frame)+20,100,15);
        txtview_appeal_value.frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-55,CGRectGetMaxY(img_line.frame)+13,80, 30);
        label_value .frame = CGRectMake(CGRectGetMaxX(txtview_appeal_value.frame)+15,CGRectGetMaxY(img_line.frame)+20,100,15);
        txtview_value_value.frame = CGRectMake(CGRectGetMaxX(label_value.frame)-55,CGRectGetMaxY(img_line.frame)+13,80, 30);
        img_table.frame  = CGRectMake(10,CGRectGetMaxY(img_rect.frame)+5,WIDTH-20,265);
    }
    else if (IS_IPHONE_6)
    {
        img_bg .frame = CGRectMake(0,CGRectGetMaxY(img_header.frame), WIDTH, 150);
        text_dish_name  .frame = CGRectMake(93,88,200, 15);
        btn_favorite.frame = CGRectMake(18,118,25,25);
        btn_fb.frame = CGRectMake(CGRectGetMaxX(btn_favorite.frame)+15,118,25,25);
        btn_tw.frame = CGRectMake(CGRectGetMaxX(btn_fb.frame)+5,118,25,25);
        btn_cm.frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+5,118,25,25);
        text_rate  .frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+150,118,200,25);
        text_dish_details.frame = CGRectMake(40,CGRectGetMaxY(img_header.frame)+115,200, 15);
        btn_food_details.frame = CGRectMake(0,CGRectGetMaxY(img_header.frame)+115, WIDTH/2, 15);
        img_strip.frame = CGRectMake(40, CGRectGetMaxY(img_header.frame)+135, 80, 2);
        text_reviews .frame = CGRectMake(200,CGRectGetMaxY(img_header.frame)+115,200, 15);
        btn_reviews.frame = CGRectMake(WIDTH/2+10,CGRectGetMaxY(img_header.frame)+115,WIDTH/2, 15);
        img_strip1.frame = CGRectMake(200, CGRectGetMaxY(img_header.frame)+135, 57, 2);
        number_of_reviews.frame = CGRectMake(260,165,200, 10);
        img_percentage_tag.frame = CGRectMake(273,151, 25, 13);
        view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame)+35,WIDTH,HEIGHT+90);
        img_rect .frame = CGRectMake(10,10,WIDTH-20, 75);
        reviews_in_rect .frame = CGRectMake(45,7,200, 13);
        reviews_rating_in_rect.frame = CGRectMake(20,5,200, 15);
        img_thumb.frame = CGRectMake(CGRectGetMaxX(reviews_in_rect.frame)-10,2, 20,20);
        label_over_all .frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+5,7,200, 15);
        txtview_over_all_value.frame = CGRectMake(300,0,80, 30);
        img_line .frame = CGRectMake(10,35, WIDTH-40,0.3);
        label_test_in_rect .frame = CGRectMake(10,CGRectGetMaxY(img_line.frame)+20,200, 15);
        txtview_taste_value.frame = CGRectMake(45,CGRectGetMaxY(img_line.frame)+13,80, 30);
        label_appeal .frame = CGRectMake(CGRectGetMaxX(txtview_taste_value.frame)+15,CGRectGetMaxY(img_line.frame)+20,100,15);
        txtview_appeal_value.frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-55,CGRectGetMaxY(img_line.frame)+13,80, 30);
        label_value .frame = CGRectMake(CGRectGetMaxX(txtview_appeal_value.frame)+10,CGRectGetMaxY(img_line.frame)+20,100,15);
        txtview_value_value.frame = CGRectMake(CGRectGetMaxX(label_value.frame)-65,CGRectGetMaxY(img_line.frame)+13,80, 30);
        img_table.frame  = CGRectMake(10,CGRectGetMaxY(img_rect.frame)+5,WIDTH-20,265);
    }
    else
    {
        img_bg .frame = CGRectMake(0,CGRectGetMaxY(img_header.frame), WIDTH, 150);
        text_dish_name  .frame = CGRectMake(93,88,200, 15);
        btn_favorite.frame = CGRectMake(18,118,25,25);
        btn_fb.frame = CGRectMake(CGRectGetMaxX(btn_favorite.frame)+15,118,25,25);
        btn_tw.frame = CGRectMake(CGRectGetMaxX(btn_fb.frame)+5,118,25,25);
        btn_cm.frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+5,118,25,25);
        text_rate  .frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+150,118,200,25);
        text_dish_details.frame = CGRectMake(40,CGRectGetMaxY(img_header.frame)+115,200, 15);
        btn_food_details.frame = CGRectMake(0,CGRectGetMaxY(img_header.frame)+115, WIDTH/2, 15);
        img_strip.frame = CGRectMake(40, CGRectGetMaxY(img_header.frame)+135, 80, 2);
        text_reviews .frame = CGRectMake(200,CGRectGetMaxY(img_header.frame)+115,200, 15);
        btn_reviews.frame = CGRectMake(WIDTH/2+10,CGRectGetMaxY(img_header.frame)+115,WIDTH/2, 15);
        img_strip1.frame = CGRectMake(200, CGRectGetMaxY(img_header.frame)+135, 57, 2);
        number_of_reviews.frame = CGRectMake(260,165,200, 10);
        img_percentage_tag.frame = CGRectMake(273,151, 25, 13);
        view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame)+35,WIDTH,HEIGHT+90);
        img_rect .frame = CGRectMake(10,10,305, 75);
        reviews_in_rect .frame = CGRectMake(45,7,200, 13);
        reviews_rating_in_rect.frame = CGRectMake(20,5,200, 15);
        img_thumb.frame = CGRectMake(CGRectGetMaxX(reviews_in_rect.frame)-70,2, 20,20);
        label_over_all .frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+10,7,200, 15);
        txtview_over_all_value.frame = CGRectMake(247,0,80, 30);
        img_line .frame = CGRectMake(10,35, WIDTH-40,0.3);
        label_test_in_rect .frame = CGRectMake(10,CGRectGetMaxY(img_line.frame)+20,200, 15);
        txtview_taste_value.frame = CGRectMake(45,CGRectGetMaxY(img_line.frame)+13,80, 30);
        label_appeal .frame = CGRectMake(CGRectGetMaxX(txtview_taste_value.frame)-20,CGRectGetMaxY(img_line.frame)+20,100,15);
        txtview_appeal_value.frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-55,CGRectGetMaxY(img_line.frame)+13,80, 30);
        label_value .frame = CGRectMake(CGRectGetMaxX(txtview_appeal_value.frame)-20,CGRectGetMaxY(img_line.frame)+20,100,15);
        txtview_value_value.frame = CGRectMake(CGRectGetMaxX(label_value.frame)-55,CGRectGetMaxY(img_line.frame)+13,80, 30);
        img_table.frame  = CGRectMake(13,CGRectGetMaxY(img_rect.frame)+5,295,230);
    }
    
    
    
    
}
-(void)btn_back_click:(UIButton *)sender
{
    NSLog(@"icon_back_click Btn Click");
    
}


-(void)btn_favorite_click:(UIButton *)sender
{
    NSLog(@"icon_favorite Btn Click");
    
}
-(void)btn_fb_click:(UIButton *)sender
{
    NSLog(@"icon_fb Btn Click");
    
}


-(void)btn_tw_click:(UIButton *)sender
{
    NSLog(@"icon_tw Btn Click");
    
}


-(void)btn_cm_click:(UIButton *)sender
{
    NSLog(@"icon_cm Btn Click");
    
}
-(void)btn_food_details_click:(UIButton *)sender
{
    NSLog(@"icon_food_details_click Btn Click");
    img_strip.hidden = NO;
    img_strip1.hidden = YES;
    
    
}
-(void)btn_reviews_click:(UIButton *)sender
{
    NSLog(@"icon_reviews_click Btn Click");
    img_strip.hidden = YES;
    img_strip1.hidden = NO;
    
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 160;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    //img_cellBackGnd.frame =  CGRectMake(0,40, 300, 150);
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [cell.contentView addSubview:img_cellBackGnd];
    
    
    UIImageView *img_user = [[UIImageView alloc] init];
    //   img_user.frame = CGRectMake(20,0, 50,  50 );
    [img_user setImage:[UIImage imageNamed:@"img-user@2x.png"]];
    [img_cellBackGnd addSubview:img_user];
    
    
    UIImageView *img_flag = [[UIImageView alloc] init];
    //   img_flag.frame = CGRectMake(260,10, 20,  25 );
    [img_flag setImage:[UIImage imageNamed:@"icon-flag.png"]];
    [img_cellBackGnd addSubview:img_flag];
    
    UILabel *user_name = [[UILabel alloc]init];
    //    user_name  .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,5,200,25);
    user_name .text = @"Jonathan Timothy";
    user_name .font = [UIFont fontWithName:@"Arial" size:13];
    user_name.textColor = [UIColor blackColor];
    user_name .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:user_name];
    
    UILabel *taste = [[UILabel alloc]init];
    //  taste .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,20,200,25);
    taste .text = @"Taste:";
    taste .font = [UIFont fontWithName:@"Arial" size:10];
    taste.textColor = [UIColor blackColor];
    taste .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:taste];
    
    UILabel *taste_value = [[UILabel alloc]init];
    //   taste_value .frame = CGRectMake(CGRectGetMaxX(taste.frame)-170,20,200,25);
    taste_value .text = @"7";
    taste_value .font = [UIFont fontWithName:@"Arial" size:10];
    taste_value.textColor = [UIColor redColor];
    taste_value .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:taste_value];
    
    
    UILabel *text_appeal = [[UILabel alloc]init];
    //   text_appeal .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+70,20,200,25);
    text_appeal.text = @"Appeal:";
    text_appeal .font = [UIFont fontWithName:@"Arial" size:10];
    text_appeal.textColor = [UIColor blackColor];
    text_appeal .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:text_appeal];
    
    UILabel *appeal_value = [[UILabel alloc]init];
    //    appeal_value .frame = CGRectMake(CGRectGetMaxX(text_appeal .frame)-160,20,200,25);
    appeal_value .text = @"8";
    appeal_value .font = [UIFont fontWithName:@"Arial" size:10];
    appeal_value.textColor = [UIColor redColor];
    appeal_value .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:appeal_value];
    
    
    UILabel *text_value = [[UILabel alloc]init];
    //   text_value .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+135,20,200,25);
    text_value.text = @"Value:";
    text_value .font = [UIFont fontWithName:@"Arial" size:10];
    text_value.textColor = [UIColor blackColor];
    text_value .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:text_value];
    
    UILabel *value_value = [[UILabel alloc]init];
    //  value_value.frame = CGRectMake(CGRectGetMaxX(text_value.frame)-165,20,200,25);
    value_value .text = @"9";
    value_value .font = [UIFont fontWithName:@"Arial" size:10];
    value_value.textColor = [UIColor redColor];
    value_value .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:value_value];
    
    
    
    UILabel *text_date = [[UILabel alloc]init];
    //   text_date .frame = CGRectMake(20,CGRectGetMaxY(taste_value .frame)+10,200,25);
    text_date.text = @"18 May 2015";
    text_date .font = [UIFont fontWithName:@"Arial" size:10];
    text_date.textColor = [UIColor blackColor];
    text_date.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:text_date];
    
    
    UILabel *text_time = [[UILabel alloc]init];
    //   text_time .frame = CGRectMake(250,CGRectGetMaxY(text_date  .frame)-25,200,25);
    text_time.text = @"2:30pm";
    text_time .font = [UIFont fontWithName:@"Arial" size:10];
    text_time.textColor = [UIColor blackColor];
    text_time.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:text_time];
    
    txtview_message = [[UITextView alloc]init];
    //  txtview_message.frame = CGRectMake(5,CGRectGetMaxY(img_user.frame)+25, 295,50);
    txtview_message.scrollEnabled = YES;
    txtview_message.userInteractionEnabled = YES;
    txtview_message.font = [UIFont fontWithName:@"Arial" size:10];
    txtview_message.backgroundColor = [UIColor clearColor];
    txtview_message.delegate = self;
    txtview_message.textColor = [UIColor blackColor];
    txtview_message.text =@"LOrem ipsumdotor sit amet.ne dicit virits corpor a per,ut illum liberavisse vet.At mei vidit conse cteture.in vitae definitionem per.tate vetear cliquado ei vim.";
    [img_cellBackGnd addSubview:txtview_message];
    
    
    UILabel *text_replied = [[UILabel alloc]init];
    //   text_replied .frame = CGRectMake(250,CGRectGetMaxY(txtview_message  .frame)-3,200,25);
    text_replied.text = @"Replied";
    text_replied .font = [UIFont fontWithName:@"Arial" size:10];
    text_replied.textColor = [UIColor blackColor];
    text_replied.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:text_replied];
    
    
    
    if (IS_IPHONE_6Plus)
    {
        img_cellBackGnd.frame =  CGRectMake(0,40, WIDTH-20, 165);
        img_user.frame = CGRectMake(20,10, 50,  50 );
        img_flag.frame = CGRectMake(350,10, 20,  25 );
        user_name  .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,5,200,25);
        taste .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,20,200,25);
        
        
        taste_value .frame = CGRectMake(CGRectGetMaxX(taste.frame)-170,20,200,25);
        text_appeal .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+95,20,200,25);
        appeal_value .frame = CGRectMake(CGRectGetMaxX(text_appeal .frame)-160,20,200,25);
        text_value .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+185,20,200,25);
        value_value.frame = CGRectMake(CGRectGetMaxX(text_value.frame)-165,20,200,25);
        text_date .frame = CGRectMake(20,CGRectGetMaxY(taste_value .frame)+20,200,25);
        
        text_time .frame = CGRectMake(340,CGRectGetMaxY(text_date  .frame)-25,200,25);
        txtview_message.frame = CGRectMake(5,CGRectGetMaxY(img_user.frame)+30, WIDTH-40,50);
        text_replied .frame = CGRectMake(340,CGRectGetMaxY(txtview_message  .frame),200,25);
        
    }
    else if (IS_IPHONE_6)
    {
        img_cellBackGnd.frame =  CGRectMake(0,40, WIDTH-20, 165);
        img_user.frame = CGRectMake(20,10, 50,  50 );
        img_flag.frame = CGRectMake(320,10, 20,  25 );
        user_name  .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,5,200,25);
        taste .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,20,200,25);
        
        
        taste_value .frame = CGRectMake(CGRectGetMaxX(taste.frame)-170,20,200,25);
        text_appeal .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+95,20,200,25);
        appeal_value .frame = CGRectMake(CGRectGetMaxX(text_appeal .frame)-160,20,200,25);
        text_value .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+185,20,200,25);
        value_value.frame = CGRectMake(CGRectGetMaxX(text_value.frame)-165,20,200,25);
        text_date .frame = CGRectMake(20,CGRectGetMaxY(taste_value .frame)+20,200,25);
        
        text_time .frame = CGRectMake(305,CGRectGetMaxY(text_date  .frame)-25,200,25);
        txtview_message.frame = CGRectMake(5,CGRectGetMaxY(img_user.frame)+30, WIDTH-40,50);
        text_replied .frame = CGRectMake(305,CGRectGetMaxY(txtview_message.frame)-5,200,25);
    }
    else
    {
        img_cellBackGnd.frame =  CGRectMake(0,40, 300, 150);
        img_user.frame = CGRectMake(20,0, 50,  50 );
        img_flag.frame = CGRectMake(260,10, 20,  25 );
        user_name  .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,5,200,25);
        taste .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,20,200,25);
        
        
        taste_value .frame = CGRectMake(CGRectGetMaxX(taste.frame)-170,20,200,25);
        text_appeal .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+70,20,200,25);
        appeal_value .frame = CGRectMake(CGRectGetMaxX(text_appeal .frame)-160,20,200,25);
        text_value .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+135,20,200,25);
        value_value.frame = CGRectMake(CGRectGetMaxX(text_value.frame)-165,20,200,25);
        text_date .frame = CGRectMake(20,CGRectGetMaxY(taste_value .frame)+10,200,25);
        
        text_time .frame = CGRectMake(250,CGRectGetMaxY(text_date  .frame)-25,200,25);
        txtview_message.frame = CGRectMake(5,CGRectGetMaxY(img_user.frame)+25, 295,50);
        text_replied .frame = CGRectMake(250,CGRectGetMaxY(txtview_message  .frame)-3,200,25);
        
        
    }
    
    
    
    /* UIImageView *img_chef = [[UIImageView alloc]init];
     img_chef .frame = CGRectMake(10,CGRectGetMaxY(text_food_details.frame)+13, 50, 50);
     [img_chef  setImage:[UIImage imageNamed:@"img-chef@2x.png"]];
     //   icon_user.backgroundColor = [UIColor redColor];
     [img_chef  setUserInteractionEnabled:YES];
     [img_cellBackGnd addSubview:img_chef ];*/
    return cell;
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
