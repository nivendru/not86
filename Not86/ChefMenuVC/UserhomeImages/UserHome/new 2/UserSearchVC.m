//
//  UserSearchVC.m
//  Not86
//
//  Created by Admin on 30/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "UserSearchVC.h"

#import "Define.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"




@interface UserSearchVC ()<UITextFieldDelegate>
{
    UIImageView *img_header;
    UIImageView *img_bg;
    UIImageView *img_strip;
    UIImageView *img_strip1;
    CGFloat	animatedDistance;
}
@end

@implementation UserSearchVC

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBody];
}


-(BOOL)prefersStatusBarHidden
{
    return YES;
}


-(void)integrateHeader
{
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 50);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back addTarget:self action:@selector(btn_back_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_back];
    
    UILabel *lbl_serch = [[UILabel alloc]init];
    lbl_serch.frame = CGRectMake(CGRectGetMaxX(icon_back.frame)+30,0, 150, 45);
    lbl_serch.text = @"Search";
    lbl_serch.font = [UIFont fontWithName:kFont size:20];
    lbl_serch.textColor = [UIColor whiteColor];
    lbl_serch.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_serch];
    
    UIImageView *icon_logo = [[UIImageView alloc]init];
    icon_logo .frame = CGRectMake(WIDTH-40, 9, 30, 30);
    [icon_logo  setImage:[UIImage imageNamed:@"img_logo@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_logo  setUserInteractionEnabled:YES];
    [img_header addSubview:icon_logo ];
    
    
}

-(void)integrateBody
{
    
    img_bg = [[UIImageView alloc]init];
    //   img_bg .frame = CGRectMake(0,CGRectGetMaxY(img_header.frame), WIDTH+50, 60);
    [img_bg  setImage:[UIImage imageNamed:@"img-wt-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg  setUserInteractionEnabled:YES];
    [self.view addSubview:img_bg ];
    
    UIImageView *img_line = [[UIImageView alloc]init];
    //    img_line .frame = CGRectMake(WIDTH/2-1,CGRectGetMaxY(img_header.frame)-30, 0.5, 28);
    [img_line  setImage:[UIImage imageNamed:@"smallline@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_line setUserInteractionEnabled:YES];
    [img_bg addSubview:img_line ];
    
    img_strip = [[UIImageView alloc]init];
    //    img_strip.frame = CGRectMake(0, CGRectGetMaxY(img_header.frame)+1, WIDTH/2-5, 5);
    [img_strip setImage:[UIImage imageNamed:@"black strip@2x.png"]];
    [img_strip setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip];
    
    img_strip1 = [[UIImageView alloc]init];
    //   img_strip1.frame = CGRectMake(WIDTH/2+1, CGRectGetMaxY(img_header.frame)+1, WIDTH/2, 5);
    [img_strip1 setImage:[UIImage imageNamed:@"black strip@2x.png"]];
    [img_strip1 setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip1];
    
    img_strip1.hidden = YES;
    
    
    UIButton *btn_chef = [UIButton buttonWithType:UIButtonTypeCustom];
    //    btn_chef.frame = CGRectMake(0,0, WIDTH/2, 60);
    btn_chef .backgroundColor = [UIColor clearColor];
    [btn_chef  addTarget:self action:@selector(btn_chefclick:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg   addSubview:btn_chef];
    
    
    UIButton *btn_cuisen = [UIButton buttonWithType:UIButtonTypeCustom];
    //    btn_cuisen.frame = CGRectMake(WIDTH/2,0, WIDTH/2, 60);
    btn_cuisen .backgroundColor = [UIColor clearColor];
    [btn_cuisen  addTarget:self action:@selector(btn_cuisenclick:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg   addSubview:btn_cuisen];
    
    UILabel *text_chef = [[UILabel alloc]init];
    //   text_chef.frame = CGRectMake(60,CGRectGetMaxY(img_header.frame)-20,200, 15);
    text_chef.text = @"Chef";
    text_chef.font = [UIFont fontWithName:kFont size:15];
    // text_chef.textColor = [UIColor redColor];
    text_chef.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:text_chef];
    
    
    
    
    UILabel *text_cuisen = [[UILabel alloc]init];
    //    text_cuisen .frame = CGRectMake(200,CGRectGetMaxY(img_header.frame)-20,200, 15);
    text_cuisen .text = @"Cuisine";
    text_cuisen .font = [UIFont fontWithName:kFont size:15];
    //text_cuisen .textColor = [UIColor redColor];
    text_cuisen .backgroundColor = [UIColor clearColor];
    [img_bg addSubview:text_cuisen ];
    
    
    UIImageView *img_search_bar = [[UIImageView alloc]init];
    //   img_search_bar .frame = CGRectMake(10,CGRectGetMaxY(img_bg.frame)-48, 306, 60);
    [img_search_bar  setImage:[UIImage imageNamed:@"img-wt-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_search_bar setUserInteractionEnabled:YES];
    [img_bg addSubview:img_search_bar ];
    
    UIImageView *img_search = [[UIImageView alloc]init];
    //    img_search.frame = CGRectMake(265,CGRectGetMaxY(img_bg.frame)-85, 15, 15);
    [img_search setImage:[UIImage imageNamed:@"serach-icon@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_search setUserInteractionEnabled:YES];
    [img_search_bar addSubview:img_search ];
    
    UITextField *txt_search = [[UITextField alloc] init];
    //    txt_search .frame = CGRectMake(25,CGRectGetMaxY(img_bg.frame)-90,200, 25);
    txt_search .borderStyle = UITextBorderStyleNone;
    txt_search .textColor = [UIColor grayColor];
    txt_search .font = [UIFont fontWithName:kFont size:13];
    txt_search .placeholder = @"Search...";
    [txt_search  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_search   setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_search .leftView = padding3;
    txt_search .leftViewMode = UITextFieldViewModeAlways;
    txt_search .userInteractionEnabled=YES;
    txt_search .textAlignment = NSTextAlignmentLeft;
    txt_search .backgroundColor = [UIColor clearColor];
    txt_search .keyboardType = UIKeyboardTypeAlphabet;
    txt_search .delegate = self;
    [self.view addSubview:txt_search ];
    
    
    UIButton *btn_on_search_bar = [UIButton buttonWithType:UIButtonTypeCustom];
    //    btn_on_search_bar.frame = CGRectMake(0,0, 360, 60);
    btn_on_search_bar .backgroundColor = [UIColor clearColor];
    [btn_on_search_bar  addTarget:self action:@selector(btn_on_search_bar_click:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn_on_search_bar];
    
    
    if (IS_IPHONE_6Plus)
    {
        img_bg .frame = CGRectMake(0,CGRectGetMaxY(img_header.frame), WIDTH+50, 60);
        img_line .frame = CGRectMake(WIDTH/2-1,CGRectGetMaxY(img_header.frame)-30, 0.5, 28);
        img_strip.frame = CGRectMake(0, CGRectGetMaxY(img_header.frame)+1, WIDTH/2-5, 5);
        img_strip1.frame = CGRectMake(WIDTH/2+1, CGRectGetMaxY(img_header.frame)+1, WIDTH/2, 5);
        btn_chef.frame = CGRectMake(0,0, WIDTH/2, 60);
        btn_cuisen.frame = CGRectMake(WIDTH/2,0, WIDTH/2, 60);
        text_chef.frame = CGRectMake(60,CGRectGetMaxY(img_header.frame)-25,200, 15);
        text_cuisen .frame = CGRectMake(280,CGRectGetMaxY(img_header.frame)-25,200, 15);
        img_search_bar .frame = CGRectMake(0,CGRectGetMaxY(img_bg.frame)-20, 415, 60);
        img_search.frame = CGRectMake(380,CGRectGetMaxY(img_bg.frame)-85, 15, 15);
        txt_search .frame = CGRectMake(25,CGRectGetMaxY(img_bg.frame)+44,320, 25);
        btn_on_search_bar.frame = CGRectMake(365,130, 40, 60);
        
    }
    else if (IS_IPHONE_6)
    {
        img_bg .frame = CGRectMake(0,CGRectGetMaxY(img_header.frame), WIDTH+50, 60);
        img_line .frame = CGRectMake(WIDTH/2-1,CGRectGetMaxY(img_header.frame)-30, 0.5, 28);
        img_strip.frame = CGRectMake(0, CGRectGetMaxY(img_header.frame)+1, WIDTH/2-5, 5);
        img_strip1.frame = CGRectMake(WIDTH/2+1, CGRectGetMaxY(img_header.frame)+1, WIDTH/2, 5);
        btn_chef.frame = CGRectMake(0,0, WIDTH/2, 60);
        btn_cuisen.frame = CGRectMake(WIDTH/2,0, WIDTH/2, 60);
        text_chef.frame = CGRectMake(60,CGRectGetMaxY(img_header.frame)-25,200, 15);
        text_cuisen .frame = CGRectMake(250,CGRectGetMaxY(img_header.frame)-25,200, 15);
        img_search_bar .frame = CGRectMake(0,CGRectGetMaxY(img_bg.frame)-48, 360, 60);
        img_search.frame = CGRectMake(330,CGRectGetMaxY(img_bg.frame)-85, 15, 15);
        txt_search .frame = CGRectMake(25,CGRectGetMaxY(img_bg.frame)+25,300, 25);
        btn_on_search_bar.frame = CGRectMake(320,117, 40, 60);
        
    }
    else
    {
        img_bg .frame = CGRectMake(0,CGRectGetMaxY(img_header.frame), WIDTH+50, 60);
        img_line .frame = CGRectMake(WIDTH/2-1,CGRectGetMaxY(img_header.frame)-30, 0.5, 28);
        img_strip.frame = CGRectMake(0, CGRectGetMaxY(img_header.frame)+1, WIDTH/2-5, 5);
        img_strip1.frame = CGRectMake(WIDTH/2+1, CGRectGetMaxY(img_header.frame)+1, WIDTH/2, 5);
        btn_chef.frame = CGRectMake(0,0, WIDTH/2, 60);
        btn_cuisen.frame = CGRectMake(WIDTH/2,0, WIDTH/2, 60);
        text_chef.frame = CGRectMake(60,CGRectGetMaxY(img_header.frame)-25,200, 15);
        text_cuisen .frame = CGRectMake(200,CGRectGetMaxY(img_header.frame)-25,200, 15);
        img_search_bar .frame = CGRectMake(0,CGRectGetMaxY(img_bg.frame)-48,IS_IPHONE_5? 320:320, 60);
        img_search.frame = CGRectMake(IS_IPHONE_5?285:285,CGRectGetMaxY(img_bg.frame)-85, 15, 15);
        txt_search .frame = CGRectMake(25,CGRectGetMaxY(img_bg.frame)+22,200, 25);
        btn_on_search_bar.frame = CGRectMake(275,115, 40, 60);
        
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}
-(void)btn_chefclick:(UIButton *)sender
{
    NSLog(@"icon_Drop_Down Btn Click");
    img_strip.hidden = NO;
    img_strip1.hidden = YES;
}
-(void)btn_cuisenclick:(UIButton *)sender
{
    NSLog(@"icon_Drop_Down Btn Click");
    img_strip.hidden = YES;
    img_strip1.hidden = NO;
    
}
-(void)btn_back_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click");
    [self.navigationController popViewControllerAnimated:NO];
    
}
-(void)icon_search_click:(UIButton *)sender
{
    NSLog(@"icon_search_click");
    
}
-(void)btn_on_search_bar_click:(UIButton *)sender
{
    NSLog(@"btn_on_search_bar_click");
    
}



#pragma rerutrn event code

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
