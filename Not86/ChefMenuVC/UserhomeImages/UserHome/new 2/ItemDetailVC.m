//
//  ItemDetailVC.m
//  Not86
//
//  Created by Admin on 30/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ItemDetailVC.h"
//#import "ChefProfileVC.h"
#import "Define.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"




@interface ItemDetailVC ()<UITextViewDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>

{
    UITextView *txtview_adddescription;
    
    UIImageView *img_header;
    UIImageView *img_bg;
    UIImageView *img_strip;
    UIImageView *img_strip1;
    UIImageView *img_bg1;
    UIImageView *img_bg2;
    UIScrollView *scroll;
    UITableView * img_table;
    
    UIView * view_for_chef;
    NSMutableArray * array_display_head_names;
    NSMutableArray * array_img_in_cell;
    NSMutableArray *array_text_field_in_cell;
    UIView*view_for_reviews;
    UIImageView*img_rect;
    UITextView*txtview_over_all_value;
    UITextView*txtview_taste_value;
    CGFloat	animatedDistance;
    
    
    
}

@end

@implementation ItemDetailVC


static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBody];
    array_display_head_names =[[NSMutableArray alloc]initWithObjects:@"Dietary Restriction",@"Cuisines",@"Next Serving Date & Time",@"Accept Out Of Schedule Request", nil];
    array_img_in_cell = [[NSMutableArray alloc]initWithObjects:@"spoons-icon@2x.png",@"spoons-icon@2x.png",      @"date-time-icon@2x.png",@"date-time-icon@2x.png",@"serving-icon@2x.png",nil];
    array_text_field_in_cell = [[NSMutableArray alloc]initWithObjects:@"Non-Veg,Halal",@"chinese,Main Course",@"02/07/15-05/07/15 2pm-4pm",@"Yes",@"10", nil];
    
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}

-(void)integrateHeader
{
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back addTarget:self action:@selector(btn_back_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_back];
    
    
    UILabel *lbl_dish_detail = [[UILabel alloc]init];
    lbl_dish_detail.frame = CGRectMake(CGRectGetMaxX(icon_back.frame)+30,0, 150, 45);
    lbl_dish_detail.text = @"Dish Detail";
    lbl_dish_detail.font = [UIFont fontWithName:kFont size:18];
    lbl_dish_detail.textColor = [UIColor whiteColor];
    lbl_dish_detail.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_dish_detail];
    
    UIImageView *icon_logo = [[UIImageView alloc]init];
    icon_logo .frame = CGRectMake(WIDTH-40, 9, 30, 30);
    [icon_logo  setImage:[UIImage imageNamed:@"img_logo@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_logo  setUserInteractionEnabled:YES];
    [img_header addSubview:icon_logo ];
    
    
}
-(void)integrateBody
{
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor clearColor];
    // scroll.frame = CGRectMake(0, 0, WIDTH, 450);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    img_bg = [[UIImageView alloc]init];
    img_bg .frame = CGRectMake(0,CGRectGetMaxY(img_header.frame), WIDTH, 150);
    [img_bg  setImage:[UIImage imageNamed:@"img-dish-bg@2x.png"]];
    [img_bg  setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg];
    
    UILabel *text_dish_name = [[UILabel alloc]init];
    text_dish_name  .frame = CGRectMake(80,88,200, 15);
    text_dish_name .text = @"Prawn Ramen";
    text_dish_name  .font = [UIFont fontWithName:kFontBold size:20];
    text_dish_name .textColor = [UIColor whiteColor];
    text_dish_name  .backgroundColor = [UIColor clearColor];
    [img_bg addSubview:text_dish_name  ];
    
    UIButton *img_black = [UIButton buttonWithType:UIButtonTypeCustom];
    img_black .frame = CGRectMake(260,CGRectGetMaxY( img_header.frame)-30,150,30);
    img_black .backgroundColor = [UIColor clearColor];
    [img_black  addTarget:self action:@selector(btn_black_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_black  setImage:[UIImage imageNamed:@"shadow-img@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:img_black ];
    
    
    UIButton *icon_location = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_location .frame = CGRectMake(0,0,30,30);
    icon_location .backgroundColor = [UIColor clearColor];
    [icon_location  addTarget:self action:@selector(btn_location_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_location  setImage:[UIImage imageNamed:@"icon-location-w@2x.png"] forState:UIControlStateNormal];
    [img_black  addSubview:icon_location ];
    
    
    UILabel *lbl_km = [[UILabel alloc]init];
    lbl_km.frame = CGRectMake(25,-5, 150,45);
    lbl_km.text = @"5km";
    lbl_km.font = [UIFont fontWithName:kFont size:12];
    lbl_km.textColor = [UIColor whiteColor];
    lbl_km.backgroundColor = [UIColor clearColor];
    [img_black addSubview:lbl_km];
    
    UIButton *btn_favorite = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_favorite.frame = CGRectMake(18,118,25,25);
    btn_favorite .backgroundColor = [UIColor clearColor];
    [btn_favorite  addTarget:self action:@selector(btn_favorite_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_favorite setImage:[UIImage imageNamed:@"favorite1-icon@2x.png"] forState:UIControlStateNormal];
    [img_bg   addSubview:btn_favorite];
    
    UIButton *btn_fb = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_fb.frame = CGRectMake(CGRectGetMaxX(btn_favorite.frame)+15,118,25,25);
    btn_fb .backgroundColor = [UIColor clearColor];
    [btn_fb  addTarget:self action:@selector(btn_fb_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_fb setImage:[UIImage imageNamed:@"fb-icon@2x.png"] forState:UIControlStateNormal];
    [img_bg   addSubview:btn_fb];
    
    UIButton *btn_tw = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_tw.frame = CGRectMake(CGRectGetMaxX(btn_fb.frame)+5,118,25,25);
    btn_tw .backgroundColor = [UIColor clearColor];
    [btn_tw  addTarget:self action:@selector(btn_tw_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_tw setImage:[UIImage imageNamed:@"tw-icon@2x.png"] forState:UIControlStateNormal];
    [img_bg   addSubview:btn_tw];
    
    UIButton *btn_cm = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_cm.frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+5,118,25,25);
    btn_cm .backgroundColor = [UIColor clearColor];
    [btn_cm  addTarget:self action:@selector(btn_cm_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_cm setImage:[UIImage imageNamed:@"cm-icon@2x.png"] forState:UIControlStateNormal];
    [img_bg   addSubview:btn_cm];
    
    UILabel *text_rate = [[UILabel alloc]init];
    text_rate  .frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+100,118,200,25);
    text_rate .text = @"$7.50";
    text_rate  .font = [UIFont fontWithName:kFontBold size:17];
    text_rate .textColor = [UIColor whiteColor];
    text_rate  .backgroundColor = [UIColor clearColor];
    [img_bg addSubview:text_rate];
    
    //text_food details
    
    UILabel *text_food_details = [[UILabel alloc]init];
    text_food_details.frame = CGRectMake(40,CGRectGetMaxY(img_bg.frame)+10,200, 15);
    text_food_details.text = @"Dish Details";
    text_food_details.font= [UIFont fontWithName:kFontBold size:15];
    // text_chef.textColor = [UIColor redColor];
    [text_food_details setUserInteractionEnabled:YES];
    text_food_details.backgroundColor = [UIColor clearColor];
    [scroll addSubview:text_food_details];
    
    UIButton *btn_food_details = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_food_details.frame = CGRectMake(0,CGRectGetMaxY(img_bg.frame)+10, WIDTH/2, 25);
    btn_food_details .backgroundColor = [UIColor clearColor];
    [btn_food_details addTarget:self action:@selector(btn_food_details_click:) forControlEvents:UIControlEventTouchUpInside];
    [scroll   addSubview:btn_food_details ];
    
    img_strip = [[UIImageView alloc]init];
    img_strip.frame = CGRectMake(40, CGRectGetMaxY(img_bg.frame)+29, 80, 2);
    [img_strip setImage:[UIImage imageNamed:@"black strip@2x.png"]];
    [img_strip setUserInteractionEnabled:YES];
    [scroll addSubview:img_strip];
    
    //text_Reviews
    
    UILabel *text_reviews = [[UILabel alloc]init];
    text_reviews .frame = CGRectMake(100,CGRectGetMaxY(img_bg.frame)+10,200, 15);
    text_reviews.text = @"Reviews";
    text_reviews .font = [UIFont fontWithName:kFontBold size:15];
    [text_reviews setUserInteractionEnabled:YES];
    //text_cuisen .textColor = [UIColor blackColor];
    text_reviews .backgroundColor = [UIColor clearColor];
    [scroll addSubview:text_reviews ];
    
    UIButton *btn_reviews = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_reviews.frame = CGRectMake(WIDTH/2+10,CGRectGetMaxY(img_bg.frame)+10,WIDTH/2, 25);
    btn_reviews .backgroundColor = [UIColor clearColor];
    [btn_reviews addTarget:self action:@selector(btn_reviews_click:) forControlEvents:UIControlEventTouchUpInside];
    [scroll   addSubview:btn_reviews];
    
    img_strip1 = [[UIImageView alloc]init];
    img_strip1.frame = CGRectMake(200, CGRectGetMaxY(img_bg.frame)+29, 57, 2);
    [img_strip1 setImage:[UIImage imageNamed:@"black strip@2x.png"]];
    [img_strip1 setUserInteractionEnabled:YES];
    [scroll addSubview:img_strip1];
    
    img_strip1.hidden = YES;
    
    
    UILabel *number_of_reviews = [[UILabel alloc]init];
    number_of_reviews.frame = CGRectMake(260,165,200, 10);
    number_of_reviews.text = @"[123]";
    number_of_reviews.font = [UIFont fontWithName:kFont size:10];
    number_of_reviews .textColor = [UIColor lightGrayColor];
    number_of_reviews .backgroundColor = [UIColor clearColor];
    [img_bg addSubview:number_of_reviews ];
    
    
    UIImageView *img_percentage_tag = [[UIImageView alloc]init];
    img_percentage_tag.frame = CGRectMake(273,151, 25, 13);
    [img_percentage_tag setImage:[UIImage imageNamed:@"img-tag-tx@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_percentage_tag  setUserInteractionEnabled:YES];
    [img_bg addSubview:img_percentage_tag ];
    
    //View_for_chef
    
    view_for_chef = [[UIView alloc]init];
    view_for_chef.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame)+35,WIDTH,HEIGHT+90);
    view_for_chef.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_for_chef];
    
    UIImageView *img_chef = [[UIImageView alloc]init];
    //img_chef .frame = CGRectMake(10,10, 20, 20);
    [img_chef  setImage:[UIImage imageNamed:@"chef-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_chef  setUserInteractionEnabled:YES];
    [view_for_chef addSubview:img_chef ];
    
    UIButton *btn_on_chef_img = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_chef_img .frame = CGRectMake(0,0,50, 50);
    btn_on_chef_img  .backgroundColor = [UIColor clearColor];
    [btn_on_chef_img  addTarget:self action:@selector(btn_on_chef_img_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_chef   addSubview:btn_on_chef_img ];
    
    
    UILabel *name_of_chef = [[UILabel alloc]init];
    name_of_chef .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,25,100, 15);
    name_of_chef.text = @"James Doe";
    name_of_chef .font = [UIFont fontWithName:kFont size:11];
    //text_cuisen .textColor = [UIColor redColor];
    text_reviews .backgroundColor = [UIColor clearColor];
    [view_for_chef addSubview:name_of_chef ];
    
    
    UILabel *text_dish_shedule = [[UILabel alloc]init];
    text_dish_shedule   .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+170,15,100, 15);
    text_dish_shedule  .text = @"Dish Schedule";
    text_dish_shedule   .font = [UIFont fontWithName:kFontBold size:11];
    text_dish_shedule  .textColor = [UIColor blackColor];
    text_dish_shedule   .backgroundColor = [UIColor clearColor];
    [view_for_chef  addSubview:text_dish_shedule   ];
    
    UIImageView *line1=[[UIImageView alloc]init];
    line1.frame=CGRectMake(CGRectGetMaxX(img_chef.frame)+170, 30, 70, 0.5);
    [line1 setUserInteractionEnabled:YES];
    line1.backgroundColor=[UIColor blackColor];
    line1.image=[UIImage imageNamed:@"img-user-line@2x.png"];
    [view_for_chef addSubview:line1];
    
    UIButton *btn_Dish_schedule = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_Dish_schedule.frame = CGRectMake(0,0,75, 15);
    btn_Dish_schedule .backgroundColor = [UIColor clearColor];
    [btn_Dish_schedule addTarget:self action:@selector(btn_Dish_schedule_click:) forControlEvents:UIControlEventTouchUpInside];
    [text_dish_shedule   addSubview:btn_Dish_schedule];
    
    UIImageView *line4=[[UIImageView alloc]init];
    line4.frame=CGRectMake(CGRectGetMaxX(img_chef.frame)+15,45,60, 15);
    [line4 setUserInteractionEnabled:YES];
    line4.backgroundColor=[UIColor blackColor];
    [line4 setUserInteractionEnabled:YES];
    line4.image=[UIImage imageNamed:@"img-user-line@2x.png"];
    [view_for_chef addSubview:line4];
    
    UILabel *name_on_schedule = [[UILabel alloc]init];
    name_on_schedule .frame = CGRectMake(1,2,100, 10);
    name_on_schedule.text = @"On Schedule";
    name_on_schedule .font = [UIFont fontWithName:kFontBold size:9];
    name_on_schedule .textColor = [UIColor greenColor];
    [name_on_schedule setUserInteractionEnabled:YES];
    name_on_schedule .backgroundColor = [UIColor clearColor];
    [line4 addSubview:name_on_schedule ];
    
    UIButton *btn_on_schedule = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_schedule.frame = CGRectMake(0,0,65, 15);
    btn_on_schedule .backgroundColor = [UIColor clearColor];
    [btn_on_schedule addTarget:self action:@selector(btn_on_onSchedule_click:) forControlEvents:UIControlEventTouchUpInside];
    [line4   addSubview:btn_on_schedule];
    
    img_bg1 = [[UIImageView alloc]init];
    img_bg1 .frame = CGRectMake(5,CGRectGetMaxY(img_chef.frame)+10, WIDTH-10, 400);
    [img_bg1  setImage:[UIImage imageNamed:@"bg1@2x.png"]];
    img_bg1.backgroundColor = [UIColor clearColor];
    [img_bg1 setUserInteractionEnabled:YES];
    [view_for_chef  addSubview:img_bg1];
    
    txtview_adddescription = [[UITextView alloc]init];
    txtview_adddescription.frame = CGRectMake(5,10, 295,60);
    txtview_adddescription.scrollEnabled = YES;
    txtview_adddescription.userInteractionEnabled = YES;
    txtview_adddescription.font = [UIFont fontWithName:kFont size:12];
    txtview_adddescription.backgroundColor = [UIColor clearColor];
    txtview_adddescription.delegate = self;
    txtview_adddescription.textColor = [UIColor lightGrayColor];
    txtview_adddescription.text =@"LOrem ipsumdotor sit amet.ne dicit virits corpor a per,ut illum liberavisse vet.At mei vidit conse cteture.in vitae definitionem per.tate vetear cliquado ei vim.";
    [img_bg1 addSubview:txtview_adddescription];
    
    UIImageView *line=[[UIImageView alloc]init];
    line.frame=CGRectMake(10, CGRectGetMaxY(txtview_adddescription.frame), 280, 0.5);
    [line setUserInteractionEnabled:YES];
    line.backgroundColor=[UIColor clearColor];
    line.image=[UIImage imageNamed:@"img_line@2x.png"];
    [img_bg1 addSubview:line];
    
    UILabel *serving_type = [[UILabel alloc]init];
    serving_type .frame = CGRectMake(13,CGRectGetMaxY(line.frame)+5,100, 13);
    serving_type .text = @"Serving Type";
    serving_type .font = [UIFont fontWithName:kFontBold size:10];
    //text_cuisen .textColor = [UIColor redColor];
    number_of_reviews .backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:serving_type];
    
    UIButton *img_dine_in = [UIButton buttonWithType:UIButtonTypeCustom];
    img_dine_in.frame = CGRectMake(15, CGRectGetMaxY(serving_type.frame)+4, 30, 35);
    img_dine_in .backgroundColor = [UIColor clearColor];
    [img_dine_in  addTarget:self action:@selector(btn_on_icon_dine_in_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_dine_in setImage:[UIImage imageNamed:@"dine-in-img@2x.png"] forState:UIControlStateNormal];
    [img_bg1   addSubview:img_dine_in];
    
    UIButton *img_take_out = [UIButton buttonWithType:UIButtonTypeCustom];
    img_take_out.frame = CGRectMake(CGRectGetMaxX(img_dine_in.frame)+17, CGRectGetMaxY(serving_type.frame)+4, 30, 35);
    img_take_out .backgroundColor = [UIColor clearColor];
    [img_take_out  addTarget:self action:@selector(btn_on_icon_take_out_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_take_out setImage:[UIImage imageNamed:@"take-out-img@2x.png"] forState:UIControlStateNormal];
    [img_bg1   addSubview:img_take_out];
    
    UIImageView *line2=[[UIImageView alloc]init];
    line2.frame=CGRectMake(10, CGRectGetMaxY(img_dine_in.frame)+5, 280, 0.5);
    [line2 setUserInteractionEnabled:YES];
    line2.backgroundColor=[UIColor clearColor];
    line2.image=[UIImage imageNamed:@"img_line@2x.png"];
    [img_bg1 addSubview:line2];
    
    
    
#pragma mark Tableview
    
    img_table = [[UITableView alloc] init ];
    img_table.frame  = CGRectMake(5,CGRectGetMaxY(line2.frame)+3,296,206);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    img_table.backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:img_table];
    
    UILabel *label_view_chef_menu = [[UILabel alloc]init];
    label_view_chef_menu .frame = CGRectMake(140,CGRectGetMaxY(img_bg1.frame)+10,150, 10);
    label_view_chef_menu.text = @"View Chef's Menu";
    label_view_chef_menu .font = [UIFont fontWithName:kFontBold size:13];
    label_view_chef_menu .textColor = [UIColor blackColor];
    [label_view_chef_menu setUserInteractionEnabled:YES];
    label_view_chef_menu .backgroundColor = [UIColor clearColor];
    [view_for_chef addSubview:label_view_chef_menu ];
    
    UIImageView *chef_menu = [[UIImageView alloc]init];
    chef_menu.frame=CGRectMake(270, CGRectGetMaxY(img_bg1.frame), 15, 15);
    [chef_menu setUserInteractionEnabled:YES];
    chef_menu.backgroundColor=[UIColor clearColor];
    chef_menu.image=[UIImage imageNamed:@"chef-menu-icon@2x.png"];
    [view_for_chef addSubview:chef_menu];
    
    
    UIButton *btn_on_chef_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_chef_menu.frame = CGRectMake(140, CGRectGetMaxY(img_bg1.frame)+2, 175, 30);
    btn_on_chef_menu .backgroundColor = [UIColor clearColor];
    [btn_on_chef_menu  addTarget:self action:@selector(btn_on_chef_menu_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_chef_menu setImage:[UIImage imageNamed:@"img-take-out@2x.png"] forState:UIControlStateNormal];
    [view_for_chef   addSubview:btn_on_chef_menu];
    
    //
    // //BG img for collection View
    //    img_bg2 = [[UIImageView alloc]init];
    //    img_bg2 .frame = CGRectMake(5,CGRectGetMaxY(label_view_chef_menu.frame)+10, WIDTH-10, 100);
    //    [img_bg2  setImage:[UIImage imageNamed:@"bg1@2x.png"]];
    //    img_bg2.backgroundColor = [UIColor purpleColor];
    //    [img_bg2 setUserInteractionEnabled:YES];
    //    [view_for_chef  addSubview:img_bg2];
    
    UIButton *btn_add_to_cart =[UIButton buttonWithType:UIButtonTypeCustom];
    btn_add_to_cart.frame=CGRectMake(0,HEIGHT-40,WIDTH, 40);
    [btn_add_to_cart setTitle:@"ADD TO CART" forState:UIControlStateNormal];
    [btn_add_to_cart setTitleColor:[UIColor whiteColor ] forState:UIControlStateNormal];
    [btn_add_to_cart addTarget:self action:@selector(img_Btn_add_to_cart_Click:) forControlEvents:UIControlEventTouchUpInside];
    btn_add_to_cart.backgroundColor = [UIColor colorWithRed:40/255.0f green:30/255.0f blue:48/255.0f alpha:1];
    [self.view   addSubview:btn_add_to_cart];
    
    
    view_for_reviews = [[UIView alloc]init];
    //    view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame)+35,WIDTH,HEIGHT+90);
    view_for_reviews.backgroundColor=[UIColor clearColor];
    [scroll addSubview: view_for_reviews];
    
    
    img_rect = [[UIImageView alloc]init];
    //  img_rect .frame = CGRectMake(10,10,305, 75);
    [img_rect setImage:[UIImage imageNamed:@"bg1@2x.png"]];
    img_rect.backgroundColor = [UIColor clearColor];
    [img_rect setUserInteractionEnabled:YES];
    [view_for_reviews addSubview:img_rect ];
    
    UILabel *reviews_in_rect = [[UILabel alloc]init];
    //   reviews_in_rect .frame = CGRectMake(45,7,200, 13);
    reviews_in_rect.text = @"Reviews";
    reviews_in_rect .font = [UIFont fontWithName:kFont size:11];
    //text_cuisen .textColor = [UIColor redColor];
    reviews_in_rect .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:reviews_in_rect];
    
    
    UILabel *reviews_rating_in_rect = [[UILabel alloc]init];
    //   reviews_rating_in_rect.frame = CGRectMake(20,5,200, 15);
    reviews_rating_in_rect.text = @"177";
    reviews_rating_in_rect .font = [UIFont fontWithName:kFontBold size:11];
    //text_cuisen .textColor = [UIColor redColor];
    reviews_rating_in_rect .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:reviews_rating_in_rect];
    
    
    UIImageView *img_thumb = [[UIImageView alloc]init];
    //    img_thumb.frame = CGRectMake(CGRectGetMaxX(reviews_in_rect.frame)-70,2, 20,20);
    [img_thumb setImage:[UIImage imageNamed:@"thumb-icon@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_thumb  setUserInteractionEnabled:YES];
    [img_rect addSubview:img_thumb ];
    
    UILabel *label_over_all = [[UILabel alloc]init];
    //    label_over_all .frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+10,7,200, 15);
    label_over_all .text = @"Overall:";
    label_over_all  .font = [UIFont fontWithName:kFont size:13];
    label_over_all   .textColor =  [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    label_over_all  .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:label_over_all ];
    
    //    txtview_over_all_value = [[UITextView alloc]init];
    //    //    txtview_over_all_value.frame = CGRectMake(247,0,80, 30);
    //    txtview_over_all_value.scrollEnabled = YES;
    //    txtview_over_all_value.userInteractionEnabled = YES;
    //    txtview_over_all_value.font = [UIFont fontWithName:kFontBold size:12];
    //    txtview_over_all_value.backgroundColor = [UIColor clearColor];
    //    txtview_over_all_value.delegate = self;
    //    txtview_over_all_value.textColor =  [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    //    txtview_over_all_value.text =@"87.4% ";
    //    [img_rect addSubview:txtview_over_all_value];
    
    UILabel *label_over_all_value = [[UILabel alloc]init];
    //    label_over_all_value .frame = CGRectMake(247,0,80, 30);
    label_over_all_value .text = @"87.4%";
    label_over_all_value  .font = [UIFont fontWithName:kFontBold size:12];
    label_over_all_value   .textColor =  [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    label_over_all_value  .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:label_over_all_value];
    
    
    UIImageView *img_line = [[UIImageView alloc]init];
    //  img_line .frame = CGRectMake(10,35, WIDTH-40,0.3);
    [img_line  setImage:[UIImage imageNamed:@"white-line@2x.png"]];
    img_line.backgroundColor = [UIColor lightGrayColor];
    [img_line   setUserInteractionEnabled:YES];
    [img_rect addSubview:img_line  ];
    
    UILabel *label_test_in_rect = [[UILabel alloc]init];
    //   label_test_in_rect .frame = CGRectMake(10,CGRectGetMaxY(img_line.frame)+20,200, 15);
    label_test_in_rect.text = @"Taste:";
    label_test_in_rect .font = [UIFont fontWithName:kFont size:13];
    label_test_in_rect  .textColor = [UIColor blackColor];
    label_test_in_rect .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:label_test_in_rect ];
    
    txtview_taste_value = [[UITextView alloc]init];
    //    txtview_taste_value1.frame = CGRectMake(45,CGRectGetMaxY(img_line.frame)+13,80, 30);
    txtview_taste_value.scrollEnabled = YES;
    txtview_taste_value.userInteractionEnabled = NO;
    txtview_taste_value.font = [UIFont fontWithName:kFont size:12];
    txtview_taste_value.backgroundColor = [UIColor clearColor];
    txtview_taste_value.delegate = self;
    txtview_taste_value.textColor =  [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    txtview_taste_value.text =@"75% ";
    txtview_taste_value.editable = NO;
    [img_rect addSubview: txtview_taste_value];
    
    UILabel * txtview_taste_value11 = [[UILabel alloc]init];
    //   txtview_taste_value1 .frame = CGRectMake(45,CGRectGetMaxY(img_line.frame)+13,80, 30);
    txtview_taste_value11 .text = @"75%";
    txtview_taste_value11  .font = [UIFont fontWithName:kFont size:12];
    txtview_taste_value11   .textColor =  [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    txtview_taste_value11  .backgroundColor = [UIColor clearColor];
    [img_rect addSubview: txtview_taste_value11];
    
    
    
    UILabel *label_appeal = [[UILabel alloc]init];
    //   label_appeal .frame = CGRectMake(CGRectGetMaxX(txtview_taste_value1.frame)-20,CGRectGetMaxY(img_line.frame)+20,100,15);
    label_appeal .text = @"Appeal:";
    label_appeal   .font = [UIFont fontWithName:kFont size:13];
    label_appeal   .textColor = [UIColor blackColor];
    label_appeal  .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:label_appeal  ];
    
    UITextView * txtview_appeal_value = [[UITextView alloc]init];
    //   txtview_appeal_value.frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-55,CGRectGetMaxY(img_line.frame)+13,80, 30);
    txtview_appeal_value.scrollEnabled = YES;
    txtview_appeal_value.userInteractionEnabled = NO;
    txtview_appeal_value.font = [UIFont fontWithName:kFont size:12];
    txtview_appeal_value.backgroundColor = [UIColor clearColor];
    txtview_appeal_value.delegate = self;
    txtview_appeal_value.textColor =  [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    txtview_appeal_value.text =@"85% ";
    txtview_appeal_value.editable = NO;
    [img_rect addSubview: txtview_appeal_value];
    
    UILabel *label_value = [[UILabel alloc]init];
    //   label_value .frame = CGRectMake(CGRectGetMaxX(txtview_appeal_value.frame)-20,CGRectGetMaxY(img_line.frame)+20,100,15);
    label_value .text = @"Value:";
    label_value   .font = [UIFont fontWithName:kFont size:13];
    label_value   .textColor = [UIColor blackColor];
    label_value  .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:label_value];
    
    
    UITextView * txtview_value_value = [[UITextView alloc]init];
    //   txtview_value_value.frame = CGRectMake(CGRectGetMaxX(label_value.frame)-55,CGRectGetMaxY(img_line.frame)+13,80, 30);
    txtview_value_value.scrollEnabled = YES;
    txtview_value_value.userInteractionEnabled = NO;
    txtview_value_value.font = [UIFont fontWithName:kFont size:12];
    txtview_value_value.backgroundColor = [UIColor clearColor];
    txtview_value_value.delegate = self;
    txtview_value_value.textColor =  [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    txtview_value_value.text =@"75% ";
    txtview_value_value.editable = NO;
    [img_rect addSubview: txtview_value_value];
    
    
    
    
    
#pragma mark Tableview
    
    UITableView *img_table_for_reviews = [[UITableView alloc] init ];
    //   img_table.frame  = CGRectMake(13,305,295,250);
    [img_table_for_reviews setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table_for_reviews.delegate = self;
    img_table_for_reviews.dataSource = self;
    img_table_for_reviews.showsVerticalScrollIndicator = NO;
    img_table_for_reviews.backgroundColor = [UIColor clearColor];
    [view_for_reviews addSubview:img_table_for_reviews];
    
    /* img_header=[[UIImageView alloc]init];
     img_header.frame = CGRectMake(0, CGRectGetMaxY( img_table.frame), WIDTH, 30);
     [img_header setUserInteractionEnabled:YES];
     img_header.image=[UIImage imageNamed:@"img-head-bg@2x.png"];
     [img_bg addSubview:img_header];
     
     UILabel *lbl_add_to_cart = [[UILabel alloc]init];
     lbl_add_to_cart.frame = CGRectMake(10,5, 150, 45);
     lbl_add_to_cart.text = @"ADD TO CART";
     lbl_add_to_cart.font = [UIFont fontWithName:@"Helvitica" size:10];
     lbl_add_to_cart.textColor = [UIColor whiteColor];
     lbl_add_to_cart.backgroundColor = [UIColor clearColor];
     [img_header addSubview:lbl_add_to_cart];*/
    
    
    //[UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    
    if (IS_IPHONE_6Plus)
    {
        scroll.frame = CGRectMake(0,45, WIDTH, 670);
        img_bg .frame = CGRectMake(0,0, WIDTH, 210);
        text_dish_name  .frame = CGRectMake(140,120,200, 20);
        img_black .frame = CGRectMake(340,CGRectGetMaxY( img_header.frame)-30,150,30);
        icon_location .frame = CGRectMake(0,0,30,30);
        lbl_km.frame = CGRectMake(25,-5, 150,45);
        btn_favorite.frame = CGRectMake(18,165,30,30);
        btn_fb.frame = CGRectMake(CGRectGetMaxX(btn_favorite.frame)+15,165,30,30);
        btn_tw.frame = CGRectMake(CGRectGetMaxX(btn_fb.frame)+5,165,30,30);
        btn_cm.frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+5,165,30,30);
        text_rate  .frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+220,165,200,30);
        text_food_details.frame = CGRectMake(75,CGRectGetMaxY(img_bg.frame)+10,200, 15);
        btn_food_details.frame = CGRectMake(0,CGRectGetMaxY(img_bg.frame)+10, WIDTH/2, 25);
        img_strip.frame = CGRectMake(75, CGRectGetMaxY(img_bg.frame)+29, 80, 2);
        text_reviews .frame = CGRectMake(270,CGRectGetMaxY(img_bg.frame)+10,200, 15);
        btn_reviews.frame = CGRectMake(WIDTH/2+10,CGRectGetMaxY(img_bg.frame)+10,WIDTH/2, 25);
        img_strip1.frame = CGRectMake(270, CGRectGetMaxY(img_bg.frame)+29, 57, 2);
        number_of_reviews.frame = CGRectMake(330,223,200, 10);
        img_percentage_tag.frame = CGRectMake(345,211, 30, 13);
        view_for_chef.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame)+35,WIDTH,HEIGHT+90);
        img_chef .frame = CGRectMake(15,10, 100, 100);
        btn_on_chef_img .frame = CGRectMake(0,0,100, 100);
        name_of_chef .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,35,100, 15);
        text_dish_shedule   .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+220,16,100, 15);
        line1.frame=CGRectMake(CGRectGetMaxX(img_chef.frame)+220, 30, 70, 0.5);
        btn_Dish_schedule.frame = CGRectMake(0,0,100, 30);
        line4.frame=CGRectMake(CGRectGetMaxX(img_chef.frame)+15,55,60, 15);
        name_on_schedule .frame = CGRectMake(1,2,100, 10);
        btn_on_schedule.frame = CGRectMake(0,0,65, 15);
        img_bg1 .frame = CGRectMake(5,CGRectGetMaxY(img_chef.frame)+10, WIDTH-10, 400);
        txtview_adddescription.frame = CGRectMake(5,10, 380,60);
        line.frame=CGRectMake(10, CGRectGetMaxY(txtview_adddescription.frame), 380, 0.5);
        serving_type .frame = CGRectMake(13,CGRectGetMaxY(line.frame)+5,100, 13);
        img_dine_in.frame = CGRectMake(15, CGRectGetMaxY(serving_type.frame)+4, 40, 45);
        img_take_out.frame = CGRectMake(CGRectGetMaxX(img_dine_in.frame)+17, CGRectGetMaxY(serving_type.frame)+4, 40, 45);
        line2.frame=CGRectMake(10, CGRectGetMaxY(img_dine_in.frame)+5, 380, 0.5);
        img_table.frame  = CGRectMake(5,CGRectGetMaxY(line2.frame)+3,296,206);
        label_view_chef_menu .frame = CGRectMake(210,CGRectGetMaxY(img_bg1.frame)+10,150, 15);
        chef_menu.frame=CGRectMake(350, CGRectGetMaxY(img_bg1.frame), 30, 30);
        btn_on_chef_menu.frame = CGRectMake(210, CGRectGetMaxY(img_bg1.frame)+2, 230, 30);
        
        view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame)+35,WIDTH,HEIGHT+90);
        img_rect .frame = CGRectMake(10,10,WIDTH-20, 75);
        reviews_in_rect .frame = CGRectMake(45,7,200, 13);
        reviews_rating_in_rect.frame = CGRectMake(20,5,200, 15);
        img_thumb.frame = CGRectMake(CGRectGetMaxX(reviews_in_rect.frame)-10,2, 20,20);
        label_over_all .frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+5,7,200, 15);
        label_over_all_value.frame = CGRectMake(310,0,80, 30);
        img_line .frame = CGRectMake(10,35, WIDTH-40,0.3);
        label_test_in_rect .frame = CGRectMake(10,CGRectGetMaxY(img_line.frame)+20,200, 15);
        txtview_taste_value.frame = CGRectMake(45,CGRectGetMaxY(img_line.frame)+13,80, 30);
        label_appeal .frame = CGRectMake(CGRectGetMaxX(txtview_taste_value.frame)+11,CGRectGetMaxY(img_line.frame)+20,100,15);
        txtview_appeal_value.frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-50,CGRectGetMaxY(img_line.frame)+13,80, 30);
        label_value .frame = CGRectMake(CGRectGetMaxX(txtview_appeal_value.frame)+15,CGRectGetMaxY(img_line.frame)+20,100,15);
        txtview_value_value.frame = CGRectMake(CGRectGetMaxX(label_value.frame)-55,CGRectGetMaxY(img_line.frame)+13,80, 30);
        img_table_for_reviews.frame  = CGRectMake(10,CGRectGetMaxY(img_rect.frame)+5,WIDTH-20,265);
        
        
    }
    else if (IS_IPHONE_6)
    {
        scroll.frame = CGRectMake(0, 45, WIDTH, 650);
        img_bg .frame = CGRectMake(0,0, WIDTH, 200);
        text_dish_name  .frame = CGRectMake(140,120,200, 20);
        img_black .frame = CGRectMake(300,CGRectGetMaxY( img_header.frame)-30,150,30);
        icon_location .frame = CGRectMake(0,0,30,30);
        lbl_km.frame = CGRectMake(25,-5, 150,45);
        btn_favorite.frame = CGRectMake(18,165,30,30);
        btn_fb.frame = CGRectMake(CGRectGetMaxX(btn_favorite.frame)+15,165,30,30);
        btn_tw.frame = CGRectMake(CGRectGetMaxX(btn_fb.frame)+5,165,30,30);
        btn_cm.frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+5,165,30,30);
        text_rate  .frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+190,165,200,30);
        text_food_details.frame = CGRectMake(75,CGRectGetMaxY(img_bg.frame)+15,200, 15);
        btn_food_details.frame = CGRectMake(0,CGRectGetMaxY(img_bg.frame)+15, WIDTH/2, 25);
        img_strip.frame = CGRectMake(75, CGRectGetMaxY(img_bg.frame)+34, 80, 2);
        text_reviews .frame = CGRectMake(240,CGRectGetMaxY(img_bg.frame)+15,200, 15);
        btn_reviews.frame = CGRectMake(WIDTH/2+10,CGRectGetMaxY(img_bg.frame)+15,WIDTH/2, 25);
        img_strip1.frame = CGRectMake(240, CGRectGetMaxY(img_bg.frame)+34, 57, 2);
        number_of_reviews.frame = CGRectMake(300,219,200, 10);
        img_percentage_tag.frame = CGRectMake(314,206, 30, 13);
        view_for_chef.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame)+35,WIDTH,HEIGHT+90);
        img_chef .frame = CGRectMake(15,10, 50, 50);
        btn_on_chef_img .frame = CGRectMake(0,0,50, 50);
        name_of_chef .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,15,100, 15);
        text_dish_shedule   .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+220,10,100, 15);
        line1.frame=CGRectMake(CGRectGetMaxX(img_chef.frame)+220, 30, 70, 0.5);
        btn_Dish_schedule.frame = CGRectMake(0,0,100, 25);
        line4.frame=CGRectMake(CGRectGetMaxX(img_chef.frame)+15,34,60, 15);
        name_on_schedule .frame = CGRectMake(1,2,100, 10);
        btn_on_schedule.frame = CGRectMake(0,0,65, 15);
        img_bg1 .frame = CGRectMake(5,CGRectGetMaxY(img_chef.frame)+10, WIDTH-10, 400);
        txtview_adddescription.frame = CGRectMake(5,10, 350,60);
        line.frame=CGRectMake(10, CGRectGetMaxY(txtview_adddescription.frame), 330, 0.5);
        serving_type .frame = CGRectMake(13,CGRectGetMaxY(line.frame)+5,100, 13);
        img_dine_in.frame = CGRectMake(15, CGRectGetMaxY(serving_type.frame)+4, 40, 45);
        img_take_out.frame = CGRectMake(CGRectGetMaxX(img_dine_in.frame)+17, CGRectGetMaxY(serving_type.frame)+4, 40, 45);
        line2.frame=CGRectMake(10, CGRectGetMaxY(img_dine_in.frame)+10, 330, 0.5);
        img_table.frame  = CGRectMake(5,CGRectGetMaxY(line2.frame)+10,320,206);
        label_view_chef_menu .frame = CGRectMake(180,CGRectGetMaxY(img_bg1.frame)+10,150, 15);
        chef_menu.frame=CGRectMake(330, CGRectGetMaxY(img_bg1.frame), 30, 30);
        btn_on_chef_menu.frame = CGRectMake(180, CGRectGetMaxY(img_bg1.frame)+2, 230, 30);
        
        view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame)+35,WIDTH,HEIGHT+90);
        img_rect .frame = CGRectMake(10,10,WIDTH-20, 75);
        reviews_in_rect .frame = CGRectMake(45,7,200, 13);
        reviews_rating_in_rect.frame = CGRectMake(20,5,200, 15);
        img_thumb.frame = CGRectMake(CGRectGetMaxX(reviews_in_rect.frame)-20,2, 20,20);
        label_over_all .frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+5,7,200, 15);
        label_over_all_value.frame = CGRectMake(300,0,80, 30);
        img_line .frame = CGRectMake(10,35, WIDTH-40,0.3);
        label_test_in_rect .frame = CGRectMake(10,CGRectGetMaxY(img_line.frame)+20,200, 15);
        txtview_taste_value.frame = CGRectMake(45,CGRectGetMaxY(img_line.frame)+13,80, 30);
        label_appeal .frame = CGRectMake(CGRectGetMaxX(txtview_taste_value.frame)+11,CGRectGetMaxY(img_line.frame)+20,100,15);
        txtview_appeal_value.frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-50,CGRectGetMaxY(img_line.frame)+13,80, 30);
        label_value .frame = CGRectMake(CGRectGetMaxX(txtview_appeal_value.frame)+10,CGRectGetMaxY(img_line.frame)+20,100,15);
        txtview_value_value.frame = CGRectMake(CGRectGetMaxX(label_value.frame)-65,CGRectGetMaxY(img_line.frame)+13,80, 30);
        img_table_for_reviews.frame  = CGRectMake(10,CGRectGetMaxY(img_rect.frame)+5,WIDTH-20,265);
        
        
        
    }
    else
    {
        scroll.frame = CGRectMake(0, 45, WIDTH,500);
        img_bg .frame = CGRectMake(0,0, WIDTH, 150);
        text_dish_name  .frame = CGRectMake(93,88,200, 15);
        img_black .frame = CGRectMake(260,CGRectGetMaxY( img_header.frame)-30,150,30);
        icon_location .frame = CGRectMake(0,0,30,30);
        lbl_km.frame = CGRectMake(25,-5, 150,45);
        btn_favorite.frame = CGRectMake(18,118,25,25);
        btn_fb.frame = CGRectMake(CGRectGetMaxX(btn_favorite.frame)+15,118,25,25);
        btn_tw.frame = CGRectMake(CGRectGetMaxX(btn_fb.frame)+5,118,25,25);
        btn_cm.frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+5,118,25,25);
        text_rate  .frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+150,118,200,25);
        text_food_details.frame = CGRectMake(40,CGRectGetMaxY(img_bg.frame)+10,200, 15);
        btn_food_details.frame = CGRectMake(0,CGRectGetMaxY(img_bg.frame)+10, WIDTH/2, 25);
        img_strip.frame = CGRectMake(40, CGRectGetMaxY(img_bg.frame)+29, 80, 2);
        text_reviews .frame = CGRectMake(200,CGRectGetMaxY(img_bg.frame)+10,200, 15);
        btn_reviews.frame = CGRectMake(WIDTH/2+10,CGRectGetMaxY(img_bg.frame)+10,WIDTH/2, 25);
        img_strip1.frame = CGRectMake(200, CGRectGetMaxY(img_bg.frame)+29, 57, 2);
        number_of_reviews.frame = CGRectMake(260,165,200, 10);
        img_percentage_tag.frame = CGRectMake(273,151, 25, 13);
        view_for_chef.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame)+35,WIDTH,HEIGHT+90);
        img_chef .frame = CGRectMake(10,10, 50, 50);
        btn_on_chef_img .frame = CGRectMake(0,0,50, 50);
        name_of_chef .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,25,100, 15);
        text_dish_shedule   .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+170,15,100, 15);
        line1.frame=CGRectMake(CGRectGetMaxX(img_chef.frame)+170, 30, 70, 0.5);
        btn_Dish_schedule.frame = CGRectMake(0,0,75, 25);
        line4.frame=CGRectMake(CGRectGetMaxX(img_chef.frame)+15,45,60, 15);
        name_on_schedule .frame = CGRectMake(1,2,100, 10);
        btn_on_schedule.frame = CGRectMake(0,0,65, 15);
        img_bg1 .frame = CGRectMake(5,CGRectGetMaxY(img_chef.frame)+10, WIDTH-10, 400);
        txtview_adddescription.frame = CGRectMake(5,10, 295,60);
        line.frame=CGRectMake(10, CGRectGetMaxY(txtview_adddescription.frame), 280, 0.5);
        serving_type .frame = CGRectMake(13,CGRectGetMaxY(line.frame)+5,100, 13);
        img_dine_in.frame = CGRectMake(15, CGRectGetMaxY(serving_type.frame)+4, 30, 35);
        img_take_out.frame = CGRectMake(CGRectGetMaxX(img_dine_in.frame)+17, CGRectGetMaxY(serving_type.frame)+4, 30, 35);
        line2.frame=CGRectMake(10, CGRectGetMaxY(img_dine_in.frame)+5, 280, 0.5);
        img_table.frame  = CGRectMake(5,CGRectGetMaxY(line2.frame)+3,296,206);
        label_view_chef_menu .frame = CGRectMake(140,CGRectGetMaxY(img_bg1.frame)+10,150, 10);
        chef_menu.frame=CGRectMake(270, CGRectGetMaxY(img_bg1.frame), 30, 30);
        btn_on_chef_menu.frame = CGRectMake(140, CGRectGetMaxY(img_bg1.frame)+2, 175, 30);
        
        view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame)+35,WIDTH,HEIGHT+90);
        img_rect .frame = CGRectMake(10,10,305, 75);
        reviews_in_rect .frame = CGRectMake(45,7,200, 13);
        reviews_rating_in_rect.frame = CGRectMake(20,5,200, 15);
        img_thumb.frame = CGRectMake(CGRectGetMaxX(reviews_in_rect.frame)-80,2, 20,20);
        label_over_all .frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+10,7,200, 15);
        label_over_all_value.frame = CGRectMake(247,0,80, 30);
        img_line .frame = CGRectMake(10,35, WIDTH-40,0.3);
        label_test_in_rect .frame = CGRectMake(10,CGRectGetMaxY(img_line.frame)+20,200, 15);
        txtview_taste_value.frame = CGRectMake(45,CGRectGetMaxY(img_line.frame)+13,80, 30);
        label_appeal .frame = CGRectMake(CGRectGetMaxX(txtview_taste_value.frame)-20,CGRectGetMaxY(img_line.frame)+20,100,15);
        txtview_appeal_value.frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-50,CGRectGetMaxY(img_line.frame)+13,80, 30);
        label_value .frame = CGRectMake(CGRectGetMaxX(txtview_appeal_value.frame)-20,CGRectGetMaxY(img_line.frame)+20,100,15);
        txtview_value_value.frame = CGRectMake(CGRectGetMaxX(label_value.frame)-55,CGRectGetMaxY(img_line.frame)+13,80, 30);
        img_table_for_reviews.frame  = CGRectMake(13,CGRectGetMaxY(img_rect.frame)+5,295,360);
        
        
    }
    
    view_for_reviews.hidden = YES;
    view_for_chef.hidden = NO;
    
    [scroll setContentSize:CGSizeMake(0,850)];
    
}

#pragma scroll View Delegates

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
}

#pragma table View Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == img_table)
    {
        
        return [array_display_head_names count];
    }
    else{
        return 10;
    }
    return 0;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == img_table)
    {
        
        return 50;
    }
    else{
        return 160;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    if (tableView == img_table)
    {
        cell.backgroundColor = [UIColor clearColor];
        
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH, 57);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg1-img@2x.png"]];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        UIImageView *imgs_in_cells = [[UIImageView alloc] init];
        imgs_in_cells.frame = CGRectMake(5,7, 30,  30 );
        [imgs_in_cells setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[ array_img_in_cell objectAtIndex:indexPath.row]]]];
        [img_cellBackGnd addSubview:imgs_in_cells];
        
        UILabel *text_head_in_cells = [[UILabel alloc]init];
        text_head_in_cells  .frame = CGRectMake(CGRectGetMaxX(imgs_in_cells.frame)+10,5,200, 20);
        text_head_in_cells .text =[NSString stringWithFormat:@"%@",[array_display_head_names objectAtIndex:indexPath.row]];
        text_head_in_cells  .font = [UIFont fontWithName:kFontBold size:10];
        text_head_in_cells .textColor = [UIColor blackColor];
        text_head_in_cells  .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:text_head_in_cells ];
        
        UITextView *txtview_in_cells = [[UITextView alloc]init];
        txtview_in_cells .frame = CGRectMake(40,CGRectGetMaxY(text_head_in_cells.frame)-10, 295,60);
        txtview_in_cells  .scrollEnabled = YES;
        txtview_in_cells .userInteractionEnabled = YES;
        txtview_in_cells .font = [UIFont fontWithName:kFont size:10];
        txtview_in_cells .backgroundColor = [UIColor clearColor];
        txtview_in_cells .delegate = self;
        txtview_in_cells .textColor = [UIColor blackColor];
        txtview_in_cells .text =[NSString stringWithFormat:@"%@",[array_text_field_in_cell
                                                                  objectAtIndex:indexPath.row]];
        [img_cellBackGnd addSubview:txtview_in_cells ];
        
        UIImageView *line2=[[UIImageView alloc]init];
        line2.frame=CGRectMake(10,54, 280, 0.7);
        [line2 setUserInteractionEnabled:YES];
        line2.backgroundColor=[UIColor clearColor];
        line2.image=[UIImage imageNamed:@"img-user-line@2x.png"];
        [img_cellBackGnd addSubview:line2];
        
    }
    
    
    else
    {
        cell.backgroundColor = [UIColor clearColor];
        
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        //img_cellBackGnd.frame =  CGRectMake(0,40, 300, 150);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        UIImageView *img_user = [[UIImageView alloc] init];
        //   img_user.frame = CGRectMake(20,0, 50,  50 );
        [img_user setImage:[UIImage imageNamed:@"user-img@2x.png"]];
        [img_cellBackGnd addSubview:img_user];
        
        
        UIImageView *img_flag = [[UIImageView alloc] init];
        //   img_flag.frame = CGRectMake(260,10, 20,  25 );
        [img_flag setImage:[UIImage imageNamed:@"icon-flag@2x.png"]];
        [img_cellBackGnd addSubview:img_flag];
        
        UILabel *user_name = [[UILabel alloc]init];
        //    user_name  .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,5,200,25);
        user_name .text = @"Jonathan Timothy";
        user_name .font = [UIFont fontWithName:kFont size:13];
        user_name.textColor = [UIColor blackColor];
        user_name .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:user_name];
        
        UILabel *taste = [[UILabel alloc]init];
        //  taste .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,20,200,25);
        taste .text = @"Taste:";
        taste .font = [UIFont fontWithName:kFont size:10];
        taste.textColor = [UIColor blackColor];
        taste .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:taste];
        
        UILabel *taste_value = [[UILabel alloc]init];
        //   taste_value .frame = CGRectMake(CGRectGetMaxX(taste.frame)-170,20,200,25);
        taste_value .text = @"7";
        taste_value .font = [UIFont fontWithName:kFont size:10];
        taste_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        taste_value .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:taste_value];
        
        
        UILabel *text_appeal = [[UILabel alloc]init];
        //   text_appeal .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+70,20,200,25);
        text_appeal.text = @"Appeal:";
        text_appeal .font = [UIFont fontWithName:kFont size:10];
        text_appeal.textColor = [UIColor blackColor];
        text_appeal .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:text_appeal];
        
        UILabel *appeal_value = [[UILabel alloc]init];
        //    appeal_value .frame = CGRectMake(CGRectGetMaxX(text_appeal .frame)-160,20,200,25);
        appeal_value .text = @"8";
        appeal_value .font = [UIFont fontWithName:kFont size:10];
        appeal_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];       appeal_value .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:appeal_value];
        
        
        UILabel *text_value = [[UILabel alloc]init];
        //   text_value .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+135,20,200,25);
        text_value.text = @"Value:";
        text_value .font = [UIFont fontWithName:kFont size:10];
        text_value.textColor = [UIColor blackColor];
        text_value .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:text_value];
        
        UILabel *value_value = [[UILabel alloc]init];
        //  value_value.frame = CGRectMake(CGRectGetMaxX(text_value.frame)-165,20,200,25);
        value_value .text = @"9";
        value_value .font = [UIFont fontWithName:kFont size:10];
        value_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        value_value .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:value_value];
        
        
        
        UILabel *text_date = [[UILabel alloc]init];
        //   text_date .frame = CGRectMake(20,CGRectGetMaxY(taste_value .frame)+10,200,25);
        text_date.text = @"18 May 2015";
        text_date .font = [UIFont fontWithName:kFont size:10];
        text_date.textColor = [UIColor blackColor];
        text_date.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:text_date];
        
        
        UILabel *text_time = [[UILabel alloc]init];
        //   text_time .frame = CGRectMake(250,CGRectGetMaxY(text_date  .frame)-25,200,25);
        text_time.text = @"2:30pm";
        text_time .font = [UIFont fontWithName:kFont size:10];
        text_time.textColor = [UIColor blackColor];
        text_time.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:text_time];
        
        UITextView *txtview_message = [[UITextView alloc]init];
        //  txtview_message.frame = CGRectMake(5,CGRectGetMaxY(img_user.frame)+25, 295,50);
        txtview_message.scrollEnabled = YES;
        txtview_message.userInteractionEnabled = YES;
        txtview_message.font = [UIFont fontWithName:kFont size:10];
        txtview_message.backgroundColor = [UIColor clearColor];
        txtview_message.delegate = self;
        txtview_message.textColor = [UIColor blackColor];
        txtview_message.text =@"Lorem ipsumdotor sit amet.ne dicit virits corpor a per,ut illum liberavisse vet.At mei vidit conse cteture.in vitae definitionem per.tate vetear cliquado ei vim.";
        [img_cellBackGnd addSubview:txtview_message];
        
        
        UILabel *text_replied = [[UILabel alloc]init];
        //   text_replied .frame = CGRectMake(250,CGRectGetMaxY(txtview_message  .frame)-3,200,25);
        text_replied.text = @"Replied";
        text_replied .font = [UIFont fontWithName:kFont size:10];
        text_replied.textColor = [UIColor blackColor];
        text_replied.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:text_replied];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,40, WIDTH-20, 165);
            img_user.frame = CGRectMake(20,10, 50,  50 );
            img_flag.frame = CGRectMake(350,10, 20,  25 );
            user_name  .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,5,200,25);
            taste .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,20,200,25);
            
            
            taste_value .frame = CGRectMake(CGRectGetMaxX(taste.frame)-170,20,200,25);
            text_appeal .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+95,20,200,25);
            appeal_value .frame = CGRectMake(CGRectGetMaxX(text_appeal .frame)-160,20,200,25);
            text_value .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+185,20,200,25);
            value_value.frame = CGRectMake(CGRectGetMaxX(text_value.frame)-165,20,200,25);
            text_date .frame = CGRectMake(20,CGRectGetMaxY(taste_value .frame)+20,200,25);
            
            text_time .frame = CGRectMake(340,CGRectGetMaxY(text_date  .frame)-25,200,25);
            txtview_message.frame = CGRectMake(5,CGRectGetMaxY(img_user.frame)+30, WIDTH-40,50);
            text_replied .frame = CGRectMake(340,CGRectGetMaxY(txtview_message  .frame),200,25);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,40, WIDTH-20, 165);
            img_user.frame = CGRectMake(20,10, 50,  50 );
            img_flag.frame = CGRectMake(320,10, 20,  25 );
            user_name  .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,5,200,25);
            taste .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,20,200,25);
            
            
            taste_value .frame = CGRectMake(CGRectGetMaxX(taste.frame)-170,20,200,25);
            text_appeal .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+95,20,200,25);
            appeal_value .frame = CGRectMake(CGRectGetMaxX(text_appeal .frame)-160,20,200,25);
            text_value .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+185,20,200,25);
            value_value.frame = CGRectMake(CGRectGetMaxX(text_value.frame)-165,20,200,25);
            text_date .frame = CGRectMake(20,CGRectGetMaxY(taste_value .frame)+20,200,25);
            
            text_time .frame = CGRectMake(305,CGRectGetMaxY(text_date  .frame)-25,200,25);
            txtview_message.frame = CGRectMake(5,CGRectGetMaxY(img_user.frame)+30, WIDTH-40,50);
            text_replied .frame = CGRectMake(305,CGRectGetMaxY(txtview_message.frame)-5,200,25);
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,40, 300, 150);
            img_user.frame = CGRectMake(20,0, 50,  50 );
            img_flag.frame = CGRectMake(260,10, 20,  25 );
            user_name  .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,5,200,25);
            taste .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,20,200,25);
            
            
            taste_value .frame = CGRectMake(CGRectGetMaxX(taste.frame)-170,20,200,25);
            text_appeal .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+70,20,200,25);
            appeal_value .frame = CGRectMake(CGRectGetMaxX(text_appeal .frame)-160,20,200,25);
            text_value .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+135,20,200,25);
            value_value.frame = CGRectMake(CGRectGetMaxX(text_value.frame)-165,20,200,25);
            text_date .frame = CGRectMake(20,CGRectGetMaxY(taste_value .frame)+10,200,25);
            
            text_time .frame = CGRectMake(250,CGRectGetMaxY(text_date  .frame)-25,200,25);
            txtview_message.frame = CGRectMake(5,CGRectGetMaxY(img_user.frame)+25, 295,50);
            text_replied .frame = CGRectMake(250,CGRectGetMaxY(txtview_message  .frame)-3,200,25);
            
            
        }
        
        
        
    }
    
    return cell;
    
}


-(void)btn_back_click:(UIButton *)sender
{
    NSLog(@"icon_back_click Btn Click");
    [self.navigationController popViewControllerAnimated:NO];
    
}
-(void)btn_black_click:(UIButton *)sender
{
    NSLog(@"icon_back_click Btn Click");
    
}

-(void)btn_location_click:(UIButton *)sender
{
    NSLog(@"icon_back_click Btn Click");
    
}


-(void)btn_favorite_click:(UIButton *)sender
{
    NSLog(@"icon_favorite Btn Click");
    
}
-(void)btn_fb_click:(UIButton *)sender
{
    NSLog(@"icon_fb Btn Click");
    
}


-(void)btn_tw_click:(UIButton *)sender
{
    NSLog(@"icon_tw Btn Click");
    
}


-(void)btn_cm_click:(UIButton *)sender
{
    NSLog(@"icon_cm Btn Click");
    
}
-(void)btn_Dish_schedule_click:(UIButton *)sender
{
    NSLog(@"btn_Dish_schedule_click:");
    
}
-(void)btn_on_chef_img_click:(UIButton *)sender
{
    NSLog(@"btn_on_chef_img_click:");
    
}
-(void)btn_on_onSchedule_click:(UIButton *)sender
{
    NSLog(@"btn_on_onSchedule_click:");
    
}
-(void)btn_on_icon_dine_in_click:(UIButton *)sender
{
    NSLog(@"btn_on_icon_dine_in_click:");
    
}
-(void)btn_on_icon_take_out_click:(UIButton *)sender
{
    NSLog(@"btn_on_icon_take_out_click:");
    
}
-(void)btn_on_chef_menu_click:(UIButton *)sender
{
    //    NSLog(@"btn_on_chef_menu_click:");
    //    ChefProfileVC *vc = [[ChefProfileVC alloc]init];
    //    [self.navigationController pushViewController:vc animated:NO];
    //
    
    
}

-(void)btn_food_details_click:(UIButton *)sender
{
    NSLog(@"icon_food_details_click Btn Click");
    img_strip.hidden = NO;
    img_strip1.hidden = YES;
    view_for_reviews.hidden = YES;
    view_for_chef.hidden = NO;
    
    
    
}
-(void)btn_reviews_click:(UIButton *)sender
{
    NSLog(@"icon_reviews_click Btn Click");
    img_strip.hidden = YES;
    img_strip1.hidden = NO;
    view_for_reviews.hidden = NO;
    view_for_chef.hidden = YES;
    
    
    
}
-(void)img_Btn_add_to_cart_Click:(UIButton *)sender
{
    NSLog(@"mg_Btn_Next Click");
}



#pragma mark TextField Delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //    if(textField == txt_username)
    //    {
    //        if ([string isEqualToString:@" "])
    //        {
    //            return NO;
    //        }
    //    }
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([textView isEqual:txtview_adddescription])
    {
        if([text isEqualToString:@"\n"])
        {
            [txtview_adddescription resignFirstResponder];
            
        }
        
    }
    
    return YES;
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
@end
