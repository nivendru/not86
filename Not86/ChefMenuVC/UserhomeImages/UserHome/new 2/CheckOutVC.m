//
//  CheckOutVC.m
//  Not86
//
//  Created by Admin on 01/09/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "CheckOutVC.h"
#import "Define.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"

@interface CheckOutVC ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    UIImageView * img_header;
    UIScrollView * scroll;
    UITableView *  img_table;
     UITableView *  img_table2;
    
    NSMutableArray * array_serving_time_date;
    NSMutableArray * array_dish_names;
    NSMutableArray * array_dish_imges;
    NSMutableArray * array_icon_dietary;
    NSMutableArray * array_icon_serving_type;
    NSMutableArray * array_quantity;
    NSMutableArray * array_serving_charge;
    NSMutableArray * array_sub_total;
    NSMutableArray *array_headers_in_sections;
    
    UIView *view_for_popup;
    UITableView * img_table_for_popup;
    
    
    int selectedSection;
    CGFloat tableHeight;

}

@end

@implementation CheckOutVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBodyDesign];
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    
    array_serving_time_date = [[NSMutableArray alloc]initWithObjects:@"Now",@"Now",@"Now",@"Now",nil];
    array_dish_imges = [[NSMutableArray alloc]initWithObjects:@"dish2-img@2x.png",@"dish2-img@2x.png",@"dish2-img@2x.png",@"dish2-img@2x.png",nil];
    array_dish_names = [[NSMutableArray alloc]initWithObjects:@"Spicy Chicken Salad",@"Raspberry Custard",@"Spicy Chicken Salad",@"Raspberry Custard",nil];
    array_icon_dietary = [[NSMutableArray alloc]initWithObjects:@"halal-icon@2x.png",@"halal-icon@2x.png",@"halal-icon@2x.png",@"halal-icon@2x.png",nil];
    array_icon_serving_type = [[NSMutableArray alloc]initWithObjects:@"take-icon@2x.png",@"take-icon@2x.png",@"take-icon@2x.png",@"take-icon@2x.png",nil];
    array_quantity =  [[NSMutableArray alloc]initWithObjects:@"3",@"3",@"3",@"3",nil];
    array_serving_charge = [[NSMutableArray alloc]initWithObjects:@"$5.50",@"$5.50",@"$5.50",@"$5.50",nil];
    array_sub_total = [[NSMutableArray alloc]initWithObjects:@"$16.50",@"$16.50",@"$16.50",@"$16.50",nil];
    
    //headers in sections
    array_headers_in_sections = [[NSMutableArray alloc]initWithObjects:@"Doe's Kitchen",@"Jane's Cakes", nil];
    
    [self popup_cancel_order];
    
    
}

-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [icon_back addTarget:self action:@selector(click_back_arrow:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:icon_back];
    
    
    UILabel *lbl_order_details = [[UILabel alloc]init];
    lbl_order_details.frame = CGRectMake(CGRectGetMaxX(icon_back .frame)+30,0, 200, 45);
    lbl_order_details.text = @"Checkout";
    lbl_order_details.font = [UIFont fontWithName:kFont size:20];
    lbl_order_details.textColor = [UIColor whiteColor];
    lbl_order_details.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_order_details];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}
-(void)integrateBodyDesign
{
    scroll = [[UIScrollView alloc]init];
    scroll.frame = CGRectMake(5, 48, WIDTH-10, HEIGHT-200);
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor whiteColor];
    // scroll.frame = CGRectMake(0, 46, WIDTH, HEIGHT+150);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    
    UIImageView *icon_now = [[UIImageView alloc]init];
    icon_now.frame = CGRectMake(20, 15, 30, 30);
    [icon_now setImage:[UIImage imageNamed:@"now@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_now setUserInteractionEnabled:YES];
    [scroll addSubview:icon_now];
    
    UILabel *lbl_food_now = [[UILabel alloc]init];
    lbl_food_now.frame = CGRectMake(CGRectGetMaxX(icon_now.frame)+10,05, 200, 45);
    lbl_food_now.text = @"Food Now";
    lbl_food_now.font = [UIFont fontWithName:kFontBold size:15];
    lbl_food_now.textColor = [UIColor blackColor];
    lbl_food_now.backgroundColor = [UIColor clearColor];
    [scroll addSubview:lbl_food_now];
    
    
    UIImageView *img_line = [[UIImageView alloc]init];
    img_line.frame = CGRectMake(20, CGRectGetMaxY(icon_now.frame)+8, WIDTH-40, 0.5);
    [img_line setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_line setUserInteractionEnabled:YES];
    [scroll addSubview:img_line];
    
//    UILabel *lbl_Does_kitchen = [[UILabel alloc]init];
//    lbl_Does_kitchen.frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)-5, 200, 45);
//    lbl_Does_kitchen.text = @"Doe's Kitchen";
//    lbl_Does_kitchen.font = [UIFont fontWithName:kFontBold size:16];
//    lbl_Does_kitchen.textColor = [UIColor blackColor];
//    lbl_Does_kitchen.backgroundColor = [UIColor clearColor];
//    [scroll addSubview:lbl_Does_kitchen];
//    
//    UIImageView *img_line2 = [[UIImageView alloc]init];
//    img_line2.frame = CGRectMake(20, CGRectGetMaxY(lbl_Does_kitchen.frame)-13, 105, 1.5);
//    [img_line2 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
//    //   icon_user.backgroundColor = [UIColor redColor];
//    [img_line2 setUserInteractionEnabled:YES];
//    [scroll addSubview:img_line2];
    
#pragma mark Tableview
    
    img_table = [[UITableView alloc] init ];
    img_table.frame  = CGRectMake(5,CGRectGetMaxY(img_line.frame)+5,WIDTH-26,150);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    img_table.backgroundColor = [UIColor clearColor];
    [scroll addSubview:img_table];
    
//    UILabel *lbl_janes_cakes = [[UILabel alloc]init];
//    lbl_janes_cakes.frame = CGRectMake(20,CGRectGetMaxY(img_table.frame), 200, 45);
//    lbl_janes_cakes.text = @"Jane's Cakes";
//    lbl_janes_cakes.font = [UIFont fontWithName:kFontBold size:16];
//    lbl_janes_cakes.textColor = [UIColor blackColor];
//    lbl_janes_cakes.backgroundColor = [UIColor clearColor];
//    [scroll addSubview:lbl_janes_cakes];
//    
//    UIImageView *img_line3 = [[UIImageView alloc]init];
//    img_line3.frame = CGRectMake(20, CGRectGetMaxY(lbl_janes_cakes.frame)-13, 105, 1.5);
//    [img_line3 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
//    //   icon_user.backgroundColor = [UIColor redColor];
//    [img_line3 setUserInteractionEnabled:YES];
//    [scroll addSubview:img_line3];
//    
//    img_table2 = [[UITableView alloc] init ];
//    img_table2.frame  = CGRectMake(5,CGRectGetMaxY(img_line3.frame)+5,WIDTH-26,150);
//    [img_table2 setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//    img_table2.delegate = self;
//    img_table2.dataSource = self;
//    img_table2.showsVerticalScrollIndicator = NO;
//    img_table2.backgroundColor = [UIColor clearColor];
//    [scroll addSubview:img_table2];
//    
//    UILabel *lbl_food_bill = [[UILabel alloc]init];
//    lbl_food_bill.frame = CGRectMake(10,CGRectGetMaxY(img_table2.frame), 200, 45);
//    lbl_food_bill.text = @"Total food bill";
//    lbl_food_bill.font = [UIFont fontWithName:kFont size:14];
//    lbl_food_bill.textColor = [UIColor blackColor];
//    lbl_food_bill.backgroundColor = [UIColor clearColor];
//    [scroll addSubview:lbl_food_bill];
    
//    UILabel *food_bill_val = [[UILabel alloc]init];
//    food_bill_val.frame = CGRectMake(CGRectGetMaxX(lbl_food_bill.frame)+135,CGRectGetMaxY(img_table2.frame), 200, 45);
//    food_bill_val.text = @"$34.90";
//    food_bill_val.font = [UIFont fontWithName:kFontBold size:13];
//    food_bill_val.textColor = [UIColor blackColor];
//    food_bill_val.backgroundColor = [UIColor clearColor];
//    [scroll addSubview:food_bill_val];
//    
//    UILabel *lbl_not86_sevice_fee = [[UILabel alloc]init];
//    lbl_not86_sevice_fee.frame = CGRectMake(10,CGRectGetMaxY(lbl_food_bill.frame)-20, 200, 45);
//    lbl_not86_sevice_fee.text = @"not86 service fee(7.5%)";
//    lbl_not86_sevice_fee.font = [UIFont fontWithName:kFont size:14];
//    lbl_not86_sevice_fee.textColor = [UIColor blackColor];
//    lbl_not86_sevice_fee.backgroundColor = [UIColor clearColor];
//    [scroll addSubview:lbl_not86_sevice_fee];
//    
//    UILabel *not86_service_fee_val = [[UILabel alloc]init];
//    not86_service_fee_val.frame = CGRectMake(CGRectGetMaxX(lbl_not86_sevice_fee.frame)+143,CGRectGetMaxY(food_bill_val.frame)-20, 200, 45);
//    not86_service_fee_val.text = @"$7.50";
//    not86_service_fee_val.font = [UIFont fontWithName:kFontBold size:13];
//    not86_service_fee_val.textColor = [UIColor blackColor];
//    not86_service_fee_val.backgroundColor = [UIColor clearColor];
//    [scroll addSubview:not86_service_fee_val];
//    
//    UILabel *lbl_total = [[UILabel alloc]init];
//    lbl_total.frame = CGRectMake(10,CGRectGetMaxY(lbl_not86_sevice_fee.frame)-20, 200, 45);
//    lbl_total.text = @"Total";
//    lbl_total.font = [UIFont fontWithName:kFont size:14];
//    lbl_total.textColor = [UIColor blackColor];
//    lbl_total.backgroundColor = [UIColor clearColor];
//    [scroll addSubview:lbl_total];
//    
//    UILabel *total_val = [[UILabel alloc]init];
//    total_val.frame = CGRectMake(CGRectGetMaxX(lbl_total.frame)+127,CGRectGetMaxY(not86_service_fee_val.frame)-20, 200, 45);
//    total_val.text = @"$42.90";
//    total_val.font = [UIFont fontWithName:kFontBold size:16];
//    total_val.textColor = [UIColor blackColor];
//    total_val.backgroundColor = [UIColor clearColor];
//    [scroll addSubview:total_val];
//
    
    UIButton *btn_more_food = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_more_food.frame = CGRectMake(20,CGRectGetMaxY(scroll.frame)+20,185,40);
    btn_more_food .backgroundColor = [UIColor clearColor];
    [btn_more_food setImage:[UIImage imageNamed:@"btn-more-food@2x.png"] forState:UIControlStateNormal];
    [btn_more_food addTarget:self action:@selector(click_on_x_btn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view  addSubview:btn_more_food];
    
}

#pragma mark popup_traceTaxi

-(void)popup_cancel_order
{
    [view_for_popup removeFromSuperview];
    view_for_popup=[[UIView alloc] init];
    view_for_popup.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.7];
    view_for_popup.userInteractionEnabled=TRUE;
    [self.view addSubview:view_for_popup];
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    [alertViewBody setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    alertViewBody.backgroundColor=[UIColor whiteColor];
    alertViewBody.userInteractionEnabled = YES;
    [view_for_popup addSubview:alertViewBody];
    
    UIImageView *img_cross =[[UIImageView alloc] init];
    [img_cross setImage:[UIImage imageNamed:@"cross-img@2x.png"]];
    img_cross.backgroundColor=[UIColor whiteColor];
    img_cross.userInteractionEnabled = YES;
    [alertViewBody addSubview:img_cross];
    
    UIButton *btn_on_cross = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_cross .backgroundColor = [UIColor clearColor];
    //[btn_on_x setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_cross addTarget:self action:@selector(btn_on_cross_img_click:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody   addSubview:btn_on_cross];
    
    //    UILabel *text_on_popup_bg = [[UILabel alloc]init];
    //    text_on_popup_bg .text = @"There is less than 24 hours left to your \nordered serving time, in accordance\nwith our terms & Conditions, no refund \n is applicable";
    //    text_on_popup_bg.numberOfLines = 4;
    //    text_on_popup_bg .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    //    text_on_popup_bg .textAlignment = NSTextAlignmentLeft;
    //    text_on_popup_bg .backgroundColor = [UIColor clearColor];
    //    [alertViewBody  addSubview:text_on_popup_bg];
    
    UILabel *text_cancel_order = [[UILabel alloc]init];
    text_cancel_order .text = @"Are you sure you want to cancel your \norder?";
    text_cancel_order.numberOfLines = 2;
    text_cancel_order .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    text_cancel_order .textAlignment = NSTextAlignmentLeft;
    text_cancel_order .backgroundColor = [UIColor clearColor];
    [alertViewBody  addSubview:text_cancel_order];
    
    UIImageView *img_line4 = [[UIImageView alloc]init];
    // [img_line setUserInteractionEnabled:YES];
    img_line4.image=[UIImage imageNamed:@"line1@2x.png"];
    [alertViewBody addSubview:img_line4];
    
    
#pragma mark Tableview
    
    img_table_for_popup = [[UITableView alloc] init ];
    [img_table_for_popup setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table_for_popup.delegate = self;
    img_table_for_popup.dataSource = self;
    img_table_for_popup.showsVerticalScrollIndicator = NO;
    img_table_for_popup.backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:img_table_for_popup];
    
    UIImageView *img_line5 = [[UIImageView alloc]init];
    // [img_line setUserInteractionEnabled:YES];
    img_line5.image=[UIImage imageNamed:@"line1@2x.png"];
    [alertViewBody addSubview:img_line5];
    
    UIImageView *img_yes = [[UIImageView alloc]init];
    [img_yes setUserInteractionEnabled:YES];
    img_yes.image=[UIImage imageNamed:@"img-yes@2x.png"];
    [alertViewBody addSubview:img_yes];
    
    UIButton *btn_on_yes = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_yes  .backgroundColor = [UIColor clearColor];
    //[btn_on_x setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_yes  addTarget:self action:@selector(btn_on_yes_img_click:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody    addSubview:btn_on_yes ];
    
    
    UIImageView *img_no = [[UIImageView alloc]init];
    [img_no setUserInteractionEnabled:YES];
    img_no.image=[UIImage imageNamed:@"img-no@2x.png"];
    [alertViewBody addSubview:img_no];
    
    UIButton *btn_on_no = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_no .backgroundColor = [UIColor clearColor];
    //[btn_on_x setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_no addTarget:self action:@selector(btn_on_no_img_click:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody    addSubview:btn_on_no];
    
    
    
    
    
    
    if (IS_IPHONE_6)
    {
        
        view_for_popup.frame = CGRectMake(0,0,414, 736);
        alertViewBody.frame = CGRectMake(20,137,WIDTH-40,374);
        img_cross.frame = CGRectMake(335,10,15,15);
        btn_on_cross.frame = CGRectMake(330,10,40,40);
        
        //        text_on_popup_bg .frame = CGRectMake(20,40, 500, 90);
        //        text_on_popup_bg .font = [UIFont fontWithName:kFont size:17];
        
        text_cancel_order .frame = CGRectMake(20,10, 500, 90);
        text_cancel_order .font = [UIFont fontWithName:kFontBold size:16];
        
        img_line4.frame = CGRectMake(20,CGRectGetMaxY(text_cancel_order.frame)+4.5, WIDTH-70, 0.8);
        img_table_for_popup.frame  = CGRectMake(5,CGRectGetMaxY(text_cancel_order.frame)+5,WIDTH-55,34);
        img_line5.frame = CGRectMake(20,CGRectGetMaxY(img_table_for_popup.frame), WIDTH-70, 0.5);
        
        img_yes .frame = CGRectMake(13,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        btn_on_yes .frame = CGRectMake(13,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        
        img_no .frame = CGRectMake(195,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        btn_on_no.frame =CGRectMake(195,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        
    }
    else if (IS_IPHONE_6Plus)
    {
        
        view_for_popup.frame = CGRectMake(0,0,414, 736);
        alertViewBody.frame = CGRectMake(20,178,WIDTH-40,300);
        img_cross.frame = CGRectMake(335,10,15,15);
        btn_on_cross.frame = CGRectMake(330,10,40,40);
        
        //        text_on_popup_bg .frame = CGRectMake(20,40, 500, 90);
        //        text_on_popup_bg .font = [UIFont fontWithName:kFont size:17];
        
        text_cancel_order .frame = CGRectMake(20,10, 500, 90);
        text_cancel_order .font = [UIFont fontWithName:kFontBold size:16];
        
        img_line4.frame = CGRectMake(20,CGRectGetMaxY(text_cancel_order.frame)+4.5, WIDTH-70, 0.8);
        img_table_for_popup.frame  = CGRectMake(5,CGRectGetMaxY(text_cancel_order.frame)+5,WIDTH-55,34);
        img_line5.frame = CGRectMake(20,CGRectGetMaxY(img_table_for_popup.frame), WIDTH-70, 0.5);
        
        img_yes .frame = CGRectMake(13,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        btn_on_yes .frame = CGRectMake(13,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        
        img_no .frame = CGRectMake(195,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        btn_on_no.frame =CGRectMake(195,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        
    }
    else
    {
        view_for_popup.frame = CGRectMake(0,0,414, 736);
        alertViewBody.frame = CGRectMake(20,137,WIDTH-40,374);
        img_cross.frame = CGRectMake(335,10,15,15);
        btn_on_cross.frame = CGRectMake(330,10,40,40);
        
        //        text_on_popup_bg .frame = CGRectMake(20,40, 500, 90);
        //        text_on_popup_bg .font = [UIFont fontWithName:kFont size:17];
        
        text_cancel_order .frame = CGRectMake(20,10, 500, 90);
        text_cancel_order .font = [UIFont fontWithName:kFontBold size:16];
        
        img_line4.frame = CGRectMake(20,CGRectGetMaxY(text_cancel_order.frame)+4.5, WIDTH-70, 0.8);
        img_table_for_popup.frame  = CGRectMake(5,CGRectGetMaxY(text_cancel_order.frame)+5,WIDTH-55,34);
        img_line5.frame = CGRectMake(20,CGRectGetMaxY(img_table_for_popup.frame), WIDTH-70, 0.5);
        
        img_yes .frame = CGRectMake(13,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        btn_on_yes .frame = CGRectMake(13,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        
        img_no .frame = CGRectMake(195,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        btn_on_no.frame =CGRectMake(195,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        
        
    }
    
    view_for_popup .hidden = YES;
    
}

#pragma mark TableView dataSource/delegate Methods

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 32;
}
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *view = [[UIView alloc]init];
    [view setBackgroundColor:[UIColor clearColor]];
    
    UILabel *lbl_Title = [[UILabel alloc] init];
    //lbl_Title.text = [NSString stringWithFormat:@"  %@",[[ary_faqDetail valueForKey:@"Question"]objectAtIndex:section]];
    
    if (section ==0)
    {
        lbl_Title.text = @"siva";
        
    }
    else
    {
        lbl_Title.text = @"prabhu";

    }
    
    lbl_Title.textColor = [UIColor blackColor];
    lbl_Title.font = [UIFont fontWithName:kFont size:12.0f];
    lbl_Title.backgroundColor = [UIColor colorWithRed:191.0/255.0 green:0.0/255.0 blue:40.0/255.0 alpha:1.0];
    [view addSubview:lbl_Title];
    view.frame = CGRectMake(0,0, WIDTH,32);
    lbl_Title.frame = CGRectMake(0,2,WIDTH-30,26);

    return view;
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  130;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0)
    {
        return 2;
    }
    if (section==1)
    {
        return 3;
    }
     return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH+5, 120);
        
    }
    else if (IS_IPHONE_6)
    {
        img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-5, 120);
        
    }
    else
    {    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-10,120);
        
    }
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    img_cellBackGnd.backgroundColor = [UIColor redColor];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [cell.contentView addSubview:img_cellBackGnd];
    
    UILabel *lbl_Title = [[UILabel alloc] init];
    //lbl_Title.text = [NSString stringWithFormat:@"  %@",[[ary_faqDetail valueForKey:@"Question"]objectAtIndex:section]];
    
    if (indexPath.section ==0)
    {
        lbl_Title.text = @"siva";
        
    }
    else
    {
        lbl_Title.text = @"prabhu";
        
    }
    
    lbl_Title.textColor = [UIColor blackColor];
    lbl_Title.frame = CGRectMake(0,2,WIDTH,26);
    lbl_Title.font = [UIFont fontWithName:kFont size:12.0f];
    lbl_Title.backgroundColor = [UIColor colorWithRed:191.0/255.0 green:0.0/255.0 blue:40.0/255.0 alpha:1.0];
    [img_cellBackGnd addSubview:lbl_Title];
    
    return cell;
}


#pragma table_view delegates



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)click_back_arrow:(UIButton *)sender
{
    NSLog(@"click back arrow");
}
-(void)click_on_x_btn:(UIButton *)sender
{
    NSLog(@"click_on_x_btn:");
    view_for_popup .hidden = NO;
}
-(void)btn_on_cross_img_click:(UIButton *)sender
{
    NSLog(@"btn_on_x_img_click:");
    view_for_popup .hidden = YES;
    
}
-(void)btn_on_yes_img_click:(UIButton *)sender
{
    NSLog(@"btn_on_yes_img_click:");
    
}
-(void)btn_on_no_img_click:(UIButton *)sender
{
    NSLog(@"btn_on_no_img_click:");
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
