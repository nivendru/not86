//
//  NotificationsVC.m
//  Not86
//
//  Created by Admin on 03/09/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "NotificationsVC.h"
#import "Define.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"



@interface NotificationsVC ()<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>
{
    UIImageView * img_header;
    
    UIView *   view_for_notifications ;
    
    UITextView *  txt_view_for_notifications;
    UIDatePicker *  datePicker;
    NSDateFormatter *  formatter;
    UITextField *txt_date_from;
    UIToolbar *keyboardToolbar_Date;
    
    
    UITextField *txt_date_to;
    UIDatePicker *  datePicker2;
    NSDateFormatter *  formatter2;
    
    
    UITableView *   table_for_notifications;
    NSMutableArray * arra_chef_images;
    NSMutableArray * array_chef_names;

     NSIndexPath *indexSelected;

    
    
}

@end

@implementation NotificationsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBodyDesign];
    
    arra_chef_images = [[NSMutableArray alloc]initWithObjects:@"chef1-img@2x.png",@"chef1-img@2x.png",@"chef1-img@2x.png",@"chef1-img@2x.png",nil];
    array_chef_names = [[NSMutableArray alloc]initWithObjects:@"John Doe",@"John Doe",@"John Doe",@"John Doe",nil];
}

-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu.frame = CGRectMake(10,13,20,20);
    icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
    [icon_menu addTarget:self action:@selector(click_on_menu_btn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:icon_menu];
    
    
    UILabel *lbl_support_on_header = [[UILabel alloc]init];
    lbl_support_on_header.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 300, 45);
    lbl_support_on_header.text = @"Notifications";
    lbl_support_on_header.font = [UIFont fontWithName:kFont size:20];
    lbl_support_on_header.textColor = [UIColor whiteColor];
    lbl_support_on_header.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_support_on_header];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}
-(void)integrateBodyDesign
{
    
    view_for_notifications = [[UIView alloc]init];
    view_for_notifications.frame = CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+90);
    view_for_notifications.backgroundColor=[UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [view_for_notifications setUserInteractionEnabled:YES];
    [self.view addSubview:  view_for_notifications];
    
    UILabel *lbl_date = [[UILabel alloc]init];
    lbl_date.frame = CGRectMake(WIDTH/2-40,0, 100, 45);
    lbl_date.text = @"Date";
    lbl_date.font = [UIFont fontWithName:kFontBold size:18];
    lbl_date.textColor = [UIColor blackColor];
    lbl_date.backgroundColor = [UIColor clearColor];
    [view_for_notifications addSubview:lbl_date];
//    
//    UIImageView *img_bg_for_date = [[UIImageView alloc]init];
//    img_bg_for_date.frame = CGRectMake(0, CGRectGetMaxY(lbl_date.frame), WIDTH, 45);
//    [img_bg_for_date setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
//    //   icon_user.backgroundColor = [UIColor redColor];
//    [img_bg_for_date setUserInteractionEnabled:YES];
//    [view_for_my_orders addSubview:img_bg_for_date];
    

    UIImageView *bg_for_date = [[UIImageView alloc]init];
    bg_for_date.frame = CGRectMake(-5, CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
    [bg_for_date setUserInteractionEnabled:YES];
    bg_for_date.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_notifications addSubview:bg_for_date];

    UILabel *lbl_from = [[UILabel alloc]init];
    lbl_from.frame = CGRectMake(30,0, 100, 45);
    lbl_from.text = @"From:";
    lbl_from.font = [UIFont fontWithName:kFont size:15];
    lbl_from.textColor = [UIColor blackColor];
    lbl_from.backgroundColor = [UIColor clearColor];
    [bg_for_date addSubview:lbl_from];
    
    
//    UIImageView *img_calender = [[UIImageView alloc]init];
//    img_calender.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+85,9.5,25, 25);
//    [img_calender setUserInteractionEnabled:YES];
//    img_calender.image=[UIImage imageNamed:@"date1-icon@2x.png"];
//    [bg_for_date addSubview:img_calender];
    
    txt_date_from = [[UITextField alloc] init];
    txt_date_from.frame = CGRectMake(CGRectGetMidX(lbl_from.frame),9.5,110,25);
    txt_date_from .borderStyle = UITextBorderStyleNone;
    txt_date_from .textColor = [UIColor blackColor];
    txt_date_from .font = [UIFont fontWithName:kFont size:13];
    txt_date_from .placeholder = @"";
    [txt_date_from  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_date_from  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_date_from .leftView = padding1;
    txt_date_from .leftViewMode = UITextFieldViewModeAlways;
    txt_date_from .userInteractionEnabled=YES;
    txt_date_from .textAlignment = NSTextAlignmentLeft;
    txt_date_from .backgroundColor = [UIColor clearColor];
    txt_date_from .keyboardType = UIKeyboardTypeAlphabet;
    txt_date_from .delegate = self;
    [bg_for_date addSubview:txt_date_from ];
    
    if (keyboardToolbar_Date == nil)
    {
        keyboardToolbar_Date = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar_Date setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_DoneDate_from:)];
        [keyboardToolbar_Date setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    txt_date_from.inputAccessoryView = keyboardToolbar_Date;
    txt_date_from.backgroundColor=[UIColor clearColor];
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(datePickerValueChanged_from:) forControlEvents:UIControlEventValueChanged];
    txt_date_from.inputView = datePicker;
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(datePickerValueChanged_from:) forControlEvents:UIControlEventValueChanged];
    txt_date_from.inputView = datePicker;
    
    formatter = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    //datePicker.minimumDate = [formatter dateFromString:@"1900-01-01"];
    
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now];
    components.year = components.year  ;
    
    datePicker.maximumDate =now ;
    datePicker.minimumDate = [formatter dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components.year-18, (long)components.month-18,(long)components.day-18]];
    
      UIButton *icon_calender = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_calender.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+85,9.5,25, 25);
    icon_calender .backgroundColor = [UIColor clearColor];
    [icon_calender setImage:[UIImage imageNamed:@"date1-icon@2x.png"] forState:UIControlStateNormal];
    [icon_calender addTarget:self action:@selector(click_on_calender_btn:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_date addSubview:icon_calender];
    
    UIImageView *line_img = [[UIImageView alloc]init];
    line_img.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 35,110, 0.5);
    [line_img setUserInteractionEnabled:YES];
    line_img.image=[UIImage imageNamed:@"line1@2x.png"];
    [bg_for_date addSubview:line_img];

    UILabel *lbl_to = [[UILabel alloc]init];
    lbl_to.frame = CGRectMake(CGRectGetMidX(icon_calender.frame)+30,0, 100, 45);
    lbl_to.text = @"to";
    lbl_to.font = [UIFont fontWithName:kFont size:15];
    lbl_to.textColor = [UIColor blackColor];
    lbl_to.backgroundColor = [UIColor clearColor];
    [bg_for_date addSubview:lbl_to];
    
    UIImageView *line_img2 = [[UIImageView alloc]init];
    line_img2.frame = CGRectMake(CGRectGetMidX(lbl_to.frame)-20, 35,110, 0.5);
    [line_img2 setUserInteractionEnabled:YES];
    line_img2.image=[UIImage imageNamed:@"line1@2x.png"];
    [bg_for_date addSubview:line_img2];
    
//    txt_date_to = [[UITextField alloc] init];
//     txt_date_to.frame = CGRectMake(CGRectGetMidX(lbl_to.frame)-19,9.5,110,25);
//     txt_date_to .borderStyle = UITextBorderStyleNone;
//     txt_date_to .textColor = [UIColor blackColor];
//     txt_date_to .font = [UIFont fontWithName:kFont size:13];
//     txt_date_to .placeholder = @"";
//    [ txt_date_to  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
//    [ txt_date_to  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
//    UIView *padding2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
//     txt_date_to .leftView = padding2;
//     txt_date_to .leftViewMode = UITextFieldViewModeAlways;
//     txt_date_to .userInteractionEnabled=YES;
//     txt_date_to .textAlignment = NSTextAlignmentLeft;
//     txt_date_to .backgroundColor = [UIColor purpleColor];
//     txt_date_to .keyboardType = UIKeyboardTypeAlphabet;
//     txt_date_to .delegate = self;
//    [bg_for_date addSubview: txt_date_to ];
//    
//    if (keyboardToolbar_Date == nil)
//    {
//        keyboardToolbar_Date = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
//        [keyboardToolbar_Date setBarStyle:UIBarStyleBlackTranslucent];
//        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_DoneDate_to:)];
//        [keyboardToolbar_Date setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
//    }
//     txt_date_to.inputAccessoryView = keyboardToolbar_Date;
//     txt_date_to.backgroundColor=[UIColor clearColor];
//    datePicker = [[UIDatePicker alloc] init];
//    datePicker.datePickerMode = UIDatePickerModeDate;
//    [datePicker addTarget:self action:@selector(datePickerValueChanged_to:) forControlEvents:UIControlEventValueChanged];
//     txt_date_to.inputView = datePicker;
//
//    datePicker2 = [[UIDatePicker alloc] init];
//    datePicker2.datePickerMode = UIDatePickerModeDate;
//    [datePicker2 addTarget:self action:@selector(datePickerValueChanged_to:)forControlEvents:UIControlEventValueChanged];
//     txt_date_to.inputView = datePicker2;
//    
//    formatter2 = [[NSDateFormatter alloc] init];
//    //[formatter2 setDateFormat:@"yyyy-MM-dd"];
//    [formatter2 setDateFormat:@"dd-MM-yyyy"];
//    //datePicker.minimumDate = [formatter dateFromString:@"1900-01-01"];
//
//    NSDate *now1 = [NSDate date];
//    NSCalendar *calendar1 = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
//    NSDateComponents *components1 = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now];
//    components.year = components.year  ;
//    
//    datePicker2.maximumDate =now ;
//    datePicker2.minimumDate = [formatter dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components.year-18, (long)components.month-18,(long)components.day-18]];
    

    
//    UIImageView *img_calender2 = [[UIImageView alloc]init];
//    img_calender2.frame = CGRectMake(CGRectGetMidX(lbl_to.frame)+65,9.5,25, 25);
//    [img_calender2 setUserInteractionEnabled:YES];
//    img_calender2.image=[UIImage imageNamed:@"date1-icon@2x.png"];
//    [bg_for_date addSubview:img_calender2];
    
    UIButton *icon_calender2 = [UIButton buttonWithType:UIButtonTypeCustom];
     icon_calender2.frame = CGRectMake(CGRectGetMidX(lbl_to.frame)+65,9.5,25, 25);
    icon_calender2 .backgroundColor = [UIColor clearColor];
    [icon_calender2 setImage:[UIImage imageNamed:@"date1-icon@2x.png"] forState:UIControlStateNormal];
    [icon_calender2 addTarget:self action:@selector(click_on_calender2_btn:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_date    addSubview:icon_calender2];
    

    
    table_for_notifications = [[UITableView alloc] init ];
     table_for_notifications.frame = CGRectMake(5,CGRectGetMaxY(bg_for_date.frame)+5,WIDTH-10,HEIGHT-150);
    [table_for_notifications setSeparatorStyle:UITableViewCellSeparatorStyleNone];
     table_for_notifications.delegate = self;
     table_for_notifications.dataSource = self;
     table_for_notifications.showsVerticalScrollIndicator = NO;
     table_for_notifications.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [view_for_notifications addSubview: table_for_notifications];
  
}

#pragma  table_view_delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arra_chef_images count];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    UIView *SwipeView = [[UIView alloc]init];
    SwipeView.tag = -1001;
    [cell.contentView addSubview:SwipeView];

    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    img_cellBackGnd.frame =  CGRectMake(-3,0, WIDTH+10, 115);
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [img_cellBackGnd  setContentMode:UIViewContentModeScaleAspectFill];
    [img_cellBackGnd  setClipsToBounds:YES];
    [SwipeView addSubview:img_cellBackGnd];
    
    UIImageView *img_chef = [[UIImageView alloc] init];
    img_chef.frame = CGRectMake(7,20,80,80 );
    [img_chef setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arra_chef_images objectAtIndex:indexPath.row]]]];
    [img_cellBackGnd addSubview:img_chef];
    
    UILabel *lbl_chef_name = [[UILabel alloc]init];
    lbl_chef_name.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+8,18,200, 15);
    lbl_chef_name.text = [NSString stringWithFormat:@"%@",[array_chef_names objectAtIndex:indexPath.row]];
    lbl_chef_name.font = [UIFont fontWithName:kFontBold size:15];
    lbl_chef_name.textColor = [UIColor blackColor];
    lbl_chef_name.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_chef_name];
    
     txt_view_for_notifications =[[UITextView alloc]init];
     txt_view_for_notifications .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+8, CGRectGetMaxY(lbl_chef_name.frame), WIDTH-130,46);
     txt_view_for_notifications.scrollEnabled=YES;
     txt_view_for_notifications.text =@"A wedding anniversary, a birthday, a celebration\nfollowing a promotion, anything at all that will make the chef more inclined to accept your request.";
     txt_view_for_notifications.userInteractionEnabled=NO;
     txt_view_for_notifications.font=[UIFont fontWithName:kFont size:14];
     txt_view_for_notifications.backgroundColor=[UIColor whiteColor];
     txt_view_for_notifications.delegate=self;
     txt_view_for_notifications.textColor=[UIColor blackColor];
     txt_view_for_notifications.layer.borderColor=[[UIColor lightGrayColor]CGColor];
     txt_view_for_notifications.layer.borderWidth=0.0f;
     txt_view_for_notifications.clipsToBounds=YES;
    [img_cellBackGnd addSubview:txt_view_for_notifications];
    
    UILabel *lbl_date_and_time = [[UILabel alloc]init];
    lbl_date_and_time.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+140,CGRectGetMaxY(txt_view_for_notifications.frame)-8,300, 45);
    lbl_date_and_time.text = @"18-9-2015, 2:30 PM";
    lbl_date_and_time.font = [UIFont fontWithName:kFont size:15];
    lbl_date_and_time.textColor = [UIColor blackColor];
    lbl_date_and_time.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_date_and_time];
    
    
    UISwipeGestureRecognizer* swipeGestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(cellSwipedLeft:)];
    [swipeGestureLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [cell addGestureRecognizer:swipeGestureLeft];
    
    
    UISwipeGestureRecognizer* swipeGestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(cellSwipedRight:)];
    [swipeGestureRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [cell addGestureRecognizer:swipeGestureRight];
    
    UIButton *btn_icon_delete =[UIButton buttonWithType:UIButtonTypeCustom];
    btn_icon_delete.frame = CGRectMake(250,-92,130,300);
     [btn_icon_delete setImage:[UIImage imageNamed:@"img-delete@2x.png"] forState:UIControlStateNormal];
    [btn_icon_delete addTarget:self action:@selector(click_on_delete_btn:) forControlEvents:UIControlEventTouchUpInside];
    [btn_icon_delete setTag:indexPath.row];
    btn_icon_delete.backgroundColor=[UIColor clearColor];
    [img_cellBackGnd addSubview:btn_icon_delete];
    [btn_icon_delete setHidden:YES];
    //[btn_RetailHistory setHidden:YES];
    
//    UIButton *icon_delete = [UIButton buttonWithType:UIButtonTypeCustom];
//    icon_delete.frame = CGRectMake(250,-92,130,300);
//    icon_delete .backgroundColor = [UIColor clearColor];
//   
//    [icon_delete addTarget:self action:@selector(click_on_delete_btn:) forControlEvents:UIControlEventTouchUpInside];
//    [img_cellBackGnd   addSubview:icon_delete];



    
    return cell;
    
    
}

- (void)cellSwipedLeft:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        UITableViewCell *cell = (UITableViewCell *)gestureRecognizer.view;
        NSIndexPath * indexPath = [ table_for_notifications indexPathForCell:cell];
        
        
        if ([indexPath isEqual:indexSelected])
        {
        }
        else
        {
            
            [self hideButtonForIndex:indexSelected animated:YES];
            [self showButtonForIndex:indexPath animated:YES];
            [ table_for_notifications reloadData];
        }
    }
}


- (void)cellSwipedRight:(UIGestureRecognizer *)gestureRecognizer {
    
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        UITableViewCell *cell = (UITableViewCell *)gestureRecognizer.view;
        NSIndexPath* indexPath = [ table_for_notifications indexPathForCell:cell];
        if ([indexPath isEqual:indexSelected])
        {
            [self hideButtonForIndex:indexSelected animated:YES];
            [ table_for_notifications reloadData];
        }
    }
}


-(void) hideButtonForIndex:(NSIndexPath *) index animated:(BOOL) animated
{
    UITableViewCell *cell = [ table_for_notifications cellForRowAtIndexPath:index];
    UIView *swipeview = [cell.contentView viewWithTag:-1001];
    CGRect rect = swipeview.frame;
    rect.origin.x = 0;
    if (animated)
    {
        [UIView animateWithDuration:0.3 animations:^{
            swipeview.frame = rect;
        }];
    }
    else
    {
        swipeview.frame = rect;
    }
    indexSelected = nil;
}

-(void) showButtonForIndex:(NSIndexPath *) index animated:(BOOL) animated{
    
    UITableViewCell *cell = [ table_for_notifications cellForRowAtIndexPath:index];
    UIView *swipeview = [cell.contentView viewWithTag:-1001];
    CGRect rect = swipeview.frame;
//    if (IPAD)
//    {
//        rect.origin.x = -1000;
//    }
//    else
//        rect.origin.x = -100;
//    
    if (animated)
    {
        [UIView animateWithDuration:0.3 animations:^{
            swipeview.frame = rect;
        }];
    }
    else{
        swipeview.frame = rect;
    }
    indexSelected = index;
}


#pragma mark - Date Picker

- (void)datePickerValueChanged_from:(id)sender
{
    [formatter setDateFormat:@"dd/MM/yyyy"];
    txt_date_from.textColor = [UIColor blackColor];
    [txt_date_from setText:[formatter stringFromDate:datePicker.date]];
}

-(void)click_DoneDate_from:(id) sender
{
    [formatter setDateFormat:@"dd/MM/yyyy"];
    txt_date_from.textColor = [UIColor blackColor];
    NSLog(@"gdj%@",[formatter stringFromDate:datePicker.date]);
    [txt_date_from setText:[formatter stringFromDate:datePicker.date]];
    [self.view endEditing:YES];
}

//- (void)datePickerValueChanged_to:(id)sender
//{
//    [formatter setDateFormat:@"dd/MM/yyyy"];
//    txt_date_from.textColor = [UIColor blackColor];
//    [txt_date_from setText:[formatter stringFromDate:datePicker.date]];
//}
//
//-(void)click_DoneDate_to:(id) sender
//{
//    [formatter setDateFormat:@"dd/MM/yyyy"];
//    txt_date_from.textColor = [UIColor blackColor];
//    NSLog(@"gdj%@",[formatter stringFromDate:datePicker.date]);
//    [txt_date_from setText:[formatter stringFromDate:datePicker.date]];
//    [self.view endEditing:YES];
//}
//


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma  click events
-(void)click_on_menu_btn:(UIButton *)sender
{
    NSLog(@"click_on_menu_btn");
}
-(void)click_on_calender_btn:(UIButton *)sender
{
    NSLog(@"click_on_calender_btn");
}
-(void)click_on_calender2_btn:(UIButton *)sender
{
    NSLog(@"click_on_calender_btn");
}
-(void)click_on_delete_btn:(UIButton *)sender
{
    NSLog(@"click_on_delete_btn");
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
