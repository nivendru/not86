//
//  OrderDetailsVC.m
//  Not86
//
//  Created by Admin on 30/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "OrderDetailsVC.h"
#import "Define.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"


@interface OrderDetailsVC ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    UIImageView *img_header;
    
    UIView  *view_for_my_orders;
    UIScrollView *scroll;
    NSMutableArray * array_order_no;
    
    UITableView *img_table;
    NSMutableArray * array_delivery_items;
    NSMutableArray * array_qt_no;
    NSMutableArray * array_rates_of_item;
    
    UIView * view_for_popup;
    UITableView *img_table_for_popup;
    
}

@end

@implementation OrderDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBodyDesign];
    [self popup_cancel_oeder];
    
    array_order_no = [[NSMutableArray alloc]initWithObjects:@"10847", nil];
    array_delivery_items = [[NSMutableArray alloc]initWithObjects:@"Raspberry Custard",@"Hawaiian Pizza",@"Cheesy Paster",@"Pizza", nil];
    array_qt_no = [[NSMutableArray alloc]initWithObjects:@"X3",@"X2",@"X1",@"X1", nil];
    array_rates_of_item = [[NSMutableArray alloc]initWithObjects:@"$13.90",@"$20.80",@"$16.50",@"$16.50",nil];
    
}

-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [icon_back addTarget:self action:@selector(btn_back_click:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:icon_back];
    
    
    UILabel *lbl_order_details = [[UILabel alloc]init];
    lbl_order_details.frame = CGRectMake(CGRectGetMaxX(icon_back .frame)+30,0, 200, 45);
    lbl_order_details.text = @"Order Details";
    lbl_order_details.font = [UIFont fontWithName:kFont size:20];
    lbl_order_details.textColor = [UIColor whiteColor];
    lbl_order_details.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_order_details];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}
-(void)integrateBodyDesign
{
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    scroll.frame = CGRectMake(0, 46, WIDTH, HEIGHT+150);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    
    UIImageView *img_bg = [[UIImageView alloc]init];
    img_bg.frame = CGRectMake(5, 5, WIDTH-3, 175);
    [img_bg setUserInteractionEnabled:YES];
    img_bg.image=[UIImage imageNamed:@"img-white-bg1@2x.png"];
    [scroll addSubview:img_bg];
    
    //    img-chef1@2x.png
    UIImageView *img_chef = [[UIImageView alloc]init];
    img_chef.frame = CGRectMake(20, 10, 50, 50);
    [img_chef setUserInteractionEnabled:YES];
    img_chef.image=[UIImage imageNamed:@"img-profile1@2x.png"];
    [img_bg  addSubview:img_chef];
    
    UILabel *text_chef_name = [[UILabel alloc]init];
    text_chef_name.frame = CGRectMake(CGRectGetMaxX(img_chef .frame)+10,13, 200, 45);
    text_chef_name.text = @"Chef Rahman";
    text_chef_name.font = [UIFont fontWithName:kFontBold size:18];
    text_chef_name.textColor = [UIColor blackColor];
    text_chef_name.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:text_chef_name];
    
    UIImageView *img_line = [[UIImageView alloc]init];
    img_line.frame = CGRectMake(20,CGRectGetMaxY(img_chef.frame)+5, WIDTH-33, 0.5);
    // [img_line setUserInteractionEnabled:YES];
    img_line.image=[UIImage imageNamed:@"line1@2x.png"];
    [img_bg addSubview:img_line];
    
    UILabel *text_order_no = [[UILabel alloc]init];
    text_order_no.frame = CGRectMake(100,CGRectGetMaxY(img_line.frame)-5, 200, 45);
    text_order_no.text = @"Order no.:";
    text_order_no.font = [UIFont fontWithName:kFontBold size:15];
    text_order_no.textColor = [UIColor blackColor];
    text_order_no.backgroundColor = [UIColor clearColor];
   // [img_bg addSubview:text_order_no];
    
    UILabel *order_no_value = [[UILabel alloc]init];
    order_no_value.frame = CGRectMake(CGRectGetMidX(text_order_no.frame)-22,CGRectGetMaxY(img_line.frame)-5, 200, 45);
    order_no_value.text = @"10847";
    order_no_value.font = [UIFont fontWithName:kFontBold size:15];
    order_no_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    order_no_value.textAlignment = NSTextAlignmentLeft;
    order_no_value.backgroundColor = [UIColor clearColor];
   // [img_bg addSubview:order_no_value];
    
    UILabel *serving_time = [[UILabel alloc]init];
    serving_time.frame = CGRectMake(95,CGRectGetMaxY(text_order_no.frame)-36, 200, 45);
    serving_time.text = @"Requested on:";
    serving_time.font = [UIFont fontWithName:kFont size:11];
    serving_time.textColor = [UIColor blackColor];
    serving_time.textAlignment = NSTextAlignmentLeft;
    serving_time.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:serving_time];
    
    UILabel *serving_time_value = [[UILabel alloc]init];
    serving_time_value.frame = CGRectMake(178,CGRectGetMaxY(text_order_no.frame)-36, 200, 45);
    serving_time_value.text = @"16/07/2015, 4:00:Pm";
    serving_time_value.font = [UIFont fontWithName:kFontBold size:11];
    serving_time_value.textColor = [UIColor blackColor];
    serving_time_value.textAlignment = NSTextAlignmentLeft;
    serving_time_value.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:serving_time_value];
    
    UILabel *text_serving_address = [[UILabel alloc]init];
    text_serving_address.frame = CGRectMake(85,CGRectGetMaxY(serving_time.frame)-23, 200, 45);
    text_serving_address.text = @"Serving Address:";
    text_serving_address.font = [UIFont fontWithName:kFont size:11];
    text_serving_address.textColor = [UIColor blackColor];
    text_serving_address.textAlignment = NSTextAlignmentLeft;
    text_serving_address.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:text_serving_address];
    
    UILabel *location_name = [[UILabel alloc]init];
    location_name.frame = CGRectMake(178,CGRectGetMaxY(serving_time_value.frame)-23, 200, 45);
    location_name.text = @"Smith St, Paris, 78374";
    location_name.font = [UIFont fontWithName:kFont size:11];
    location_name.textColor = [UIColor blackColor];
    location_name.textAlignment = NSTextAlignmentLeft;
    location_name.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:location_name];
    
    UILabel *text_status = [[UILabel alloc]init];
    text_status.frame = CGRectMake(138,CGRectGetMaxY(text_serving_address.frame)-23, 200, 45);
    text_status.text = @"Status:";
    text_status.font = [UIFont fontWithName:kFont size:11];
    text_status.textColor = [UIColor blackColor];
    text_status.textAlignment = NSTextAlignmentLeft;
    text_status.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:text_status];
    
    UILabel *text_in_progress = [[UILabel alloc]init];
    text_in_progress.frame = CGRectMake(178,CGRectGetMaxY(location_name.frame)-23, 200, 45);
    text_in_progress.text = @"On Request";
    text_in_progress.font = [UIFont fontWithName:kFont size:11];
    text_in_progress.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    text_in_progress.textAlignment = NSTextAlignmentLeft;
    text_in_progress.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:text_in_progress];
    
    UIImageView *img_x = [[UIImageView alloc]init];
    img_x.frame = CGRectMake(347, CGRectGetMaxY(img_line.frame),55, 103);
    [img_x setUserInteractionEnabled:YES];
    img_x.image=[UIImage imageNamed:@"img-x@2x.png"];
    [img_bg addSubview:img_x];
    
    UIButton *btn_on_x = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_x.frame = CGRectMake(347, CGRectGetMaxY(img_line.frame),55, 103);
    btn_on_x .backgroundColor = [UIColor clearColor];
    //[btn_on_x setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_x addTarget:self action:@selector(btn_on_x_img_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg   addSubview:btn_on_x];


    
    UIImageView *img_bg2 = [[UIImageView alloc]init];
    img_bg2.frame = CGRectMake(5, CGRectGetMaxY(img_bg.frame)-5, WIDTH-3,410);
    [img_bg2 setUserInteractionEnabled:YES];
    img_bg2.image=[UIImage imageNamed:@"img-white-bg1@2x.png"];
    [scroll addSubview:img_bg2];
    
#pragma mark Tableview
    
    img_table = [[UITableView alloc] init ];
    img_table.frame  = CGRectMake(5,7,WIDTH-25,150);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    img_table.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:img_table];
    
    UILabel *text_delivery_fee = [[UILabel alloc]init];
    text_delivery_fee.frame = CGRectMake(20,CGRectGetMaxY(img_table.frame)-10, 200, 45);
    text_delivery_fee.text = @"Delivery Fee";
    text_delivery_fee.font = [UIFont fontWithName:kFont size:11];
    text_delivery_fee.textColor = [UIColor blackColor];
    text_delivery_fee.textAlignment = NSTextAlignmentLeft;
    text_delivery_fee.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_delivery_fee];
    
    UILabel *delivery_fee = [[UILabel alloc]init];
    delivery_fee.frame = CGRectMake(CGRectGetMaxX(text_delivery_fee.frame)+130,CGRectGetMaxY(img_table.frame)-10, 200, 45);
    delivery_fee.text = @"$5.00";
    delivery_fee.font = [UIFont fontWithName:kFontBold size:10];
    delivery_fee.textColor = [UIColor blackColor];
    delivery_fee.textAlignment = NSTextAlignmentLeft;
    delivery_fee.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:delivery_fee];
    
    UILabel *text_sub_total = [[UILabel alloc]init];
    text_sub_total.frame = CGRectMake(20,CGRectGetMaxY(text_delivery_fee.frame)-15, 200, 45);
    text_sub_total.text = @"Subtotal";
    text_sub_total.font = [UIFont fontWithName:kFont size:11];
    text_sub_total.textColor = [UIColor blackColor];
    text_sub_total.textAlignment = NSTextAlignmentLeft;
    text_sub_total.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_sub_total];
    
    UILabel *sub_total_val = [[UILabel alloc]init];
    sub_total_val.frame = CGRectMake(CGRectGetMaxX(text_sub_total.frame)+124,CGRectGetMaxY(delivery_fee.frame)-15, 200, 45);
    sub_total_val.text = @"$56.00";
    sub_total_val.font = [UIFont fontWithName:kFontBold size:10];
    sub_total_val.textColor = [UIColor blackColor];
    sub_total_val.textAlignment = NSTextAlignmentLeft;
    sub_total_val.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:sub_total_val];
    
    UILabel *text_serving_fee = [[UILabel alloc]init];
    text_serving_fee.frame = CGRectMake(20,CGRectGetMaxY(text_sub_total.frame)-15, 200, 45);
    text_serving_fee.text = @"Not86 Service Fee (7%)";
    text_serving_fee.font = [UIFont fontWithName:kFont size:11];
    text_serving_fee.textColor = [UIColor blackColor];
    text_serving_fee.textAlignment = NSTextAlignmentLeft;
    text_serving_fee.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_serving_fee];
    
    UILabel *service_fee_val = [[UILabel alloc]init];
    service_fee_val.frame = CGRectMake(CGRectGetMaxX(text_serving_fee.frame)+130,CGRectGetMaxY(sub_total_val.frame)-15, 200, 45);
    service_fee_val.text = @"$3.00";
    service_fee_val.font = [UIFont fontWithName:kFontBold size:10];
    service_fee_val.textColor = [UIColor blackColor];
    service_fee_val.textAlignment = NSTextAlignmentLeft;
    service_fee_val.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:service_fee_val];
    
    UILabel *text_total_bill = [[UILabel alloc]init];
    text_total_bill .frame = CGRectMake(20,CGRectGetMaxY(text_serving_fee.frame)-15, 200, 45);
    text_total_bill .text = @"Total Bill";
    text_total_bill .font = [UIFont fontWithName:kFont size:11];
    text_total_bill .textColor = [UIColor blackColor];
    text_total_bill .textAlignment = NSTextAlignmentLeft;
    text_total_bill .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_total_bill ];
    
    UILabel *toal_bill_val = [[UILabel alloc]init];
    toal_bill_val.frame = CGRectMake(CGRectGetMaxX(text_total_bill.frame)+110,CGRectGetMaxY(service_fee_val.frame)-15, 200, 45);
    toal_bill_val.text = @"$67.50";
    toal_bill_val.font = [UIFont fontWithName:kFontBold size:15];
    toal_bill_val.textColor = [UIColor blackColor];
    toal_bill_val.textAlignment = NSTextAlignmentLeft;
    toal_bill_val.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:toal_bill_val];
    
    UIImageView *img_line2 = [[UIImageView alloc]init];
    img_line2.frame = CGRectMake(20,CGRectGetMaxY(text_total_bill.frame)+5, WIDTH-50, 0.5);
    // [img_line setUserInteractionEnabled:YES];
    img_line2.image=[UIImage imageNamed:@"line1@2x.png"];
    [img_bg2 addSubview:img_line2];

    UILabel *text_request_remarks = [[UILabel alloc]init];
    text_request_remarks .frame = CGRectMake(20,CGRectGetMaxY(img_line2 .frame)-10, 200, 45);
    text_request_remarks .text = @"Special Requests/Remarks";
    text_request_remarks .font = [UIFont fontWithName:kFontBold size:11];
    text_request_remarks .textColor = [UIColor blackColor];
    text_request_remarks .textAlignment = NSTextAlignmentLeft;
    text_request_remarks .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_request_remarks];
    
    
    UILabel *text_No_chilli = [[UILabel alloc]init];
    text_No_chilli .frame = CGRectMake(20,CGRectGetMaxY(text_request_remarks .frame)-25, 200, 45);
    text_No_chilli .text = @"No chilli and cheese.";
    text_No_chilli .font = [UIFont fontWithName:kFont size:11];
    text_No_chilli .textColor = [UIColor blackColor];
    text_No_chilli .textAlignment = NSTextAlignmentLeft;
    text_No_chilli .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_No_chilli];
    
    UIImageView *img_line3 = [[UIImageView alloc]init];
    img_line3.frame = CGRectMake(20,CGRectGetMaxY(text_No_chilli.frame), WIDTH-50, 0.5);
    // [img_line setUserInteractionEnabled:YES];
    img_line3.image=[UIImage imageNamed:@"line1@2x.png"];
    [img_bg2 addSubview:img_line3];
    
    UILabel *text_approval = [[UILabel alloc]init];
    text_approval .frame = CGRectMake(100,CGRectGetMaxY(img_line3.frame)-5, 200, 45);
    text_approval .text = @"WAITING FOR APPROVAL";
    text_approval .font = [UIFont fontWithName:kFontBold size:15];
    text_approval .textColor = [UIColor colorWithRed:9/255.0f green:0/255.0f blue:57/255.0f alpha:1];
    text_approval .textAlignment = NSTextAlignmentLeft;
    text_approval .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_approval];

   //[UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];






}

#pragma mark popup_traceTaxi

-(void)popup_cancel_oeder
{
    [view_for_popup removeFromSuperview];
    view_for_popup=[[UIView alloc] init];
    view_for_popup.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.7];
    view_for_popup.userInteractionEnabled=TRUE;
    [self.view addSubview:view_for_popup];
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    [alertViewBody setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    alertViewBody.backgroundColor=[UIColor whiteColor];
    alertViewBody.userInteractionEnabled = YES;
    [view_for_popup addSubview:alertViewBody];
    
    UIImageView *img_cross =[[UIImageView alloc] init];
    [img_cross setImage:[UIImage imageNamed:@"cross-img@2x.png"]];
    img_cross.backgroundColor=[UIColor whiteColor];
    img_cross.userInteractionEnabled = YES;
    [alertViewBody addSubview:img_cross];
    
    UIButton *btn_on_cross = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_cross .backgroundColor = [UIColor clearColor];
    //[btn_on_x setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_cross addTarget:self action:@selector(btn_on_cross_img_click:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody   addSubview:btn_on_cross];
    
    UILabel *text_on_popup_bg = [[UILabel alloc]init];
    text_on_popup_bg .text = @"There is less than 24 hours left to your \nordered serving time, in accordance\nwith our terms & Conditions, no refund \n is applicable";
    text_on_popup_bg.numberOfLines = 4;
    text_on_popup_bg .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    text_on_popup_bg .textAlignment = NSTextAlignmentLeft;
    text_on_popup_bg .backgroundColor = [UIColor clearColor];
    [alertViewBody  addSubview:text_on_popup_bg];
    
    UILabel *text_cancel_order = [[UILabel alloc]init];
    text_cancel_order .text = @"Are you sure you want to cancel your \norder?";
    text_cancel_order.numberOfLines = 2;
    text_cancel_order .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    text_cancel_order .textAlignment = NSTextAlignmentLeft;
    text_cancel_order .backgroundColor = [UIColor clearColor];
    [alertViewBody  addSubview:text_cancel_order];
    
    UIImageView *img_line4 = [[UIImageView alloc]init];
    // [img_line setUserInteractionEnabled:YES];
    img_line4.image=[UIImage imageNamed:@"line1@2x.png"];
    [alertViewBody addSubview:img_line4];
    
    
#pragma mark Tableview
    
    img_table_for_popup = [[UITableView alloc] init ];
    [img_table_for_popup setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table_for_popup.delegate = self;
    img_table_for_popup.dataSource = self;
    img_table_for_popup.showsVerticalScrollIndicator = NO;
    img_table_for_popup.backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:img_table_for_popup];
    
    UIImageView *img_line5 = [[UIImageView alloc]init];
    // [img_line setUserInteractionEnabled:YES];
    img_line5.image=[UIImage imageNamed:@"line1@2x.png"];
    [alertViewBody addSubview:img_line5];
    
    UIImageView *img_yes = [[UIImageView alloc]init];
    [img_yes setUserInteractionEnabled:YES];
    img_yes.image=[UIImage imageNamed:@"img-yes@2x.png"];
    [alertViewBody addSubview:img_yes];
    
    UIButton *btn_on_yes = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_yes  .backgroundColor = [UIColor clearColor];
    //[btn_on_x setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_yes  addTarget:self action:@selector(btn_on_yes_img_click:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody    addSubview:btn_on_yes ];
    
    
    UIImageView *img_no = [[UIImageView alloc]init];
    [img_no setUserInteractionEnabled:YES];
    img_no.image=[UIImage imageNamed:@"img-no@2x.png"];
    [alertViewBody addSubview:img_no];
    
    UIButton *btn_on_no = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_no .backgroundColor = [UIColor clearColor];
    //[btn_on_x setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_no addTarget:self action:@selector(btn_on_no_img_click:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody    addSubview:btn_on_no];
    
      
    if (IS_IPHONE_6)
    {
        
        view_for_popup.frame = CGRectMake(0,0,414, 736);
        alertViewBody.frame = CGRectMake(20,137,WIDTH-40,374);
        img_cross.frame = CGRectMake(335,10,15,15);
        btn_on_cross.frame = CGRectMake(330,10,40,40);
        
        text_on_popup_bg .frame = CGRectMake(20,40, 500, 90);
        text_on_popup_bg .font = [UIFont fontWithName:kFont size:17];
        
        text_cancel_order .frame = CGRectMake(20,CGRectGetMaxY(text_on_popup_bg.frame), 500, 90);
        text_cancel_order .font = [UIFont fontWithName:kFontBold size:16];
        
        img_line4.frame = CGRectMake(20,CGRectGetMaxY(text_cancel_order.frame)+4.5, WIDTH-70, 0.8);
        img_table_for_popup.frame  = CGRectMake(5,CGRectGetMaxY(text_cancel_order.frame)+5,WIDTH-55,34);
        img_line5.frame = CGRectMake(20,CGRectGetMaxY(img_table_for_popup.frame), WIDTH-70, 0.5);
        
        img_yes .frame = CGRectMake(13,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        btn_on_yes .frame = CGRectMake(13,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        
        img_no .frame = CGRectMake(195,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        btn_on_no.frame =CGRectMake(195,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        
    }
    else if (IS_IPHONE_6Plus)
    {
        
        view_for_popup.frame = CGRectMake(0,0,414, 736);
        alertViewBody.frame = CGRectMake(20,137,WIDTH-40,374);
        img_cross.frame = CGRectMake(335,10,15,15);
        btn_on_cross.frame = CGRectMake(330,10,40,40);
        
        text_on_popup_bg .frame = CGRectMake(20,40, 500, 90);
        text_on_popup_bg .font = [UIFont fontWithName:kFont size:17];
        
        text_cancel_order .frame = CGRectMake(20,CGRectGetMaxY(text_on_popup_bg.frame), 500, 90);
        text_cancel_order .font = [UIFont fontWithName:kFontBold size:16];
        
        img_line4.frame = CGRectMake(20,CGRectGetMaxY(text_cancel_order.frame)+4.5, WIDTH-70, 0.8);
        img_table_for_popup.frame  = CGRectMake(5,CGRectGetMaxY(text_cancel_order.frame)+5,WIDTH-55,34);
        img_line5.frame = CGRectMake(20,CGRectGetMaxY(img_table_for_popup.frame), WIDTH-70, 0.5);
        
        img_yes .frame = CGRectMake(13,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        btn_on_yes .frame = CGRectMake(13,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        
        img_no .frame = CGRectMake(195,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        btn_on_no.frame =CGRectMake(195,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        
    }
    else
    {
        view_for_popup.frame = CGRectMake(0,0,414, 736);
        alertViewBody.frame = CGRectMake(20,137,WIDTH-40,374);
        img_cross.frame = CGRectMake(335,10,15,15);
        btn_on_cross.frame = CGRectMake(330,10,40,40);
        
        text_on_popup_bg .frame = CGRectMake(20,40, 500, 90);
        text_on_popup_bg .font = [UIFont fontWithName:kFont size:17];
        
        text_cancel_order .frame = CGRectMake(20,CGRectGetMaxY(text_on_popup_bg.frame), 500, 90);
        text_cancel_order .font = [UIFont fontWithName:kFontBold size:16];
        
        img_line4.frame = CGRectMake(20,CGRectGetMaxY(text_cancel_order.frame)+4.5, WIDTH-70, 0.8);
        img_table_for_popup.frame  = CGRectMake(5,CGRectGetMaxY(text_cancel_order.frame)+5,WIDTH-55,34);
        img_line5.frame = CGRectMake(20,CGRectGetMaxY(img_table_for_popup.frame), WIDTH-70, 0.5);
        
        img_yes .frame = CGRectMake(13,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        btn_on_yes .frame = CGRectMake(13,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        
        img_no .frame = CGRectMake(195,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        btn_on_no.frame =CGRectMake(195,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        
        
    }
    
    view_for_popup .hidden = YES;
    
    
    
}


#pragma tableview delegates


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [array_delivery_items count];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews)
    {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-15, 37);
    [img_cellBackGnd setImage:[UIImage imageNamed:@"img-white-bg1@2x.png"]];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [cell.contentView addSubview:img_cellBackGnd];
    
    UIImageView *delivery_img = [[UIImageView alloc]init];
    delivery_img.frame =  CGRectMake(10,5, 20, 20);
    [delivery_img setImage:[UIImage imageNamed:@"img-delivery1@2x.png"]];
    [delivery_img setUserInteractionEnabled:YES];
    [img_cellBackGnd addSubview:delivery_img];
    
  //  [NSString stringWithFormat:@"%@",[ary_displaynames objectAtIndex:indexPath.row]];
    
    UILabel *items_name = [[UILabel alloc]init];
    items_name.frame = CGRectMake(CGRectGetMaxX(delivery_img.frame)+8,-5, 170, 40);
    items_name.text = [NSString stringWithFormat:@"%@",[array_delivery_items objectAtIndex:indexPath.row]];
    items_name.font = [UIFont fontWithName:kFontBold size:11];
    items_name.textColor = [UIColor blackColor];
    items_name.textAlignment = NSTextAlignmentLeft;
    items_name.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:items_name];
    
    UILabel *items_qt = [[UILabel alloc]init];
    items_qt.frame = CGRectMake(CGRectGetMaxX(items_name.frame)+20,-5, 50, 40);
    items_qt.text = [NSString stringWithFormat:@"%@",[array_qt_no objectAtIndex:indexPath.row]];
    items_qt.font = [UIFont fontWithName:kFont size:11];
    items_qt.textColor = [UIColor blackColor];
    items_qt.textAlignment = NSTextAlignmentLeft;
    items_qt.backgroundColor = [UIColor clearColor];
   [img_cellBackGnd  addSubview:items_qt];
    
    UILabel *items_rate = [[UILabel alloc]init];
    items_rate.frame = CGRectMake(CGRectGetMaxX(items_qt.frame)+60,-5, 200, 40);
    items_rate.text = [NSString stringWithFormat:@"%@",[array_rates_of_item objectAtIndex:indexPath.row]];
    items_rate.font = [UIFont fontWithName:kFontBold size:10];
    items_rate.textColor = [UIColor blackColor];
    items_rate.textAlignment = NSTextAlignmentLeft;
    items_rate.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:items_rate];
    if (IS_IPHONE_6Plus)
    {
        if (tableView ==  img_table)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-15, 37);
            delivery_img.frame =  CGRectMake(10,5, 20, 20);
            items_name.frame = CGRectMake(CGRectGetMaxX(delivery_img.frame)+8,-5, 170, 40);
            items_qt.frame = CGRectMake(CGRectGetMaxX(items_name.frame)+20,-5, 50, 40);
            items_rate.frame = CGRectMake(CGRectGetMaxX(items_qt.frame)+60,-5, 200, 40);
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,-1, WIDTH-47, 37);
            delivery_img.frame =  CGRectMake(15,5, 20, 20);
            items_name.frame = CGRectMake(CGRectGetMaxX(delivery_img.frame)+8,-5, 170, 40);
            items_qt.frame = CGRectMake(CGRectGetMaxX(items_name.frame)+20,-5, 50, 40);
            items_rate.frame = CGRectMake(CGRectGetMaxX(items_qt.frame)+30,-5, 200, 40);
        }
        
    }
    else if(IS_IPHONE_6)
    {
        if ( img_table)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-15, 37);
            delivery_img.frame =  CGRectMake(10,5, 20, 20);
            items_name.frame = CGRectMake(CGRectGetMaxX(delivery_img.frame)+8,-5, 170, 40);
            items_qt.frame = CGRectMake(CGRectGetMaxX(items_name.frame)+20,-5, 50, 40);
            items_rate.frame = CGRectMake(CGRectGetMaxX(items_qt.frame)+60,-5, 200, 40);
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-14, 37);
            delivery_img.frame =  CGRectMake(10,5, 20, 20);
            items_name.frame = CGRectMake(CGRectGetMaxX(delivery_img.frame)+8,-5, 170, 40);
            items_qt.frame = CGRectMake(CGRectGetMaxX(items_name.frame)+20,-5, 50, 40);
            items_rate.frame = CGRectMake(CGRectGetMaxX(items_qt.frame)+60,-5, 200, 40);
        }
        
    }
    else
    {
        if ( img_table)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-15, 37);
            delivery_img.frame =  CGRectMake(10,5, 20, 20);
            items_name.frame = CGRectMake(CGRectGetMaxX(delivery_img.frame)+8,-5, 170, 40);
            items_qt.frame = CGRectMake(CGRectGetMaxX(items_name.frame)+20,-5, 50, 40);
            items_rate.frame = CGRectMake(CGRectGetMaxX(items_qt.frame)+60,-5, 200, 40);
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-14, 37);
            delivery_img.frame =  CGRectMake(10,5, 20, 20);
            items_name.frame = CGRectMake(CGRectGetMaxX(delivery_img.frame)+8,-5, 170, 40);
            items_qt.frame = CGRectMake(CGRectGetMaxX(items_name.frame)+20,-5, 50, 40);
            items_rate.frame = CGRectMake(CGRectGetMaxX(items_qt.frame)+60,-5, 200, 40);
        }
        
    }
    
    
    return cell;
    
}
-(void)btn_back_click:(UIButton *)sender
{
    NSLog(@"btn_back_click:");
    
}

-(void)btn_on_x_img_click:(UIButton *)sender
{
    NSLog(@"btn_on_x_img_click:");
    view_for_popup .hidden = NO;
    
}
-(void)btn_on_cross_img_click:(UIButton *)sender
{
    NSLog(@"btn_on_x_img_click:");
    view_for_popup .hidden = YES;
    
}
-(void)btn_on_yes_img_click:(UIButton *)sender
{
    NSLog(@"btn_on_yes_img_click:");
    
}
-(void)btn_on_no_img_click:(UIButton *)sender
{
    NSLog(@"btn_on_no_img_click:");
    
}








- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
