//
//  AddToCartFoodNowVC.m
//  Not86
//
//  Created by Admin on 30/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "AddToCartFoodNowVC.h"
#import "Define.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"

@interface AddToCartFoodNowVC ()

@end

@implementation AddToCartFoodNowVC
{
    UIImageView *img_header;
    UIImageView *img_bg;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBody];
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)integrateHeader
{
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 50);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,5,35,35);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back addTarget:self action:@selector(btn_back_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_back];
    
    
    UILabel *lbl_dish_detail = [[UILabel alloc]init];
    lbl_dish_detail.frame = CGRectMake(CGRectGetMaxX(icon_back.frame)+30,0, 150, 45);
    lbl_dish_detail.text = @"Prawns Ramen";
    lbl_dish_detail.font = [UIFont fontWithName:kFont size:20];
    lbl_dish_detail.textColor = [UIColor whiteColor];
    lbl_dish_detail.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_dish_detail];
    
    
    
    
    UIImageView *icon_logo = [[UIImageView alloc]init];
    icon_logo .frame = CGRectMake(WIDTH-40, 9, 30, 30);
    [icon_logo  setImage:[UIImage imageNamed:@"img_logo@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_logo  setUserInteractionEnabled:YES];
    [img_header addSubview:icon_logo ];
    
    
}
-(void)integrateBody
{
    
    
    img_bg = [[UIImageView alloc]init];
    //  img_bg .frame = CGRectMake(0,CGRectGetMaxY(img_header.frame)+10, WIDTH+10, 350);
    [img_bg  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg  setUserInteractionEnabled:YES];
    [ self.view addSubview:img_bg ];
    
    UILabel *lbl_kitchen_name = [[UILabel alloc]init];
    // lbl_kitchen_name.frame = CGRectMake(18,20, 350, 20);
    lbl_kitchen_name.text = @"Name of Kitchen: Doe's Kitchen";
    lbl_kitchen_name.font = [UIFont fontWithName:kFont size:15];
    lbl_kitchen_name.textColor = [UIColor blackColor];
    lbl_kitchen_name.backgroundColor = [UIColor clearColor];
    [ img_bg addSubview:lbl_kitchen_name];
    
    
    UILabel *lbl_dish_name = [[UILabel alloc]init];
    //lbl_dish_name.frame = CGRectMake(18,CGRectGetMaxY(lbl_kitchen_name.frame)+10, 350, 20);
    lbl_dish_name.text = @"Dish/Meal Name: Prawn Ramen";
    lbl_dish_name.font = [UIFont fontWithName:kFont size:15];
    lbl_dish_name.textColor = [UIColor blackColor];
    lbl_dish_name.backgroundColor = [UIColor clearColor];
    [ img_bg addSubview:lbl_dish_name];
    
    
    UILabel *lbl_seving_time = [[UILabel alloc]init];
    //lbl_seving_time .frame = CGRectMake(18,CGRectGetMaxY(lbl_dish_name.frame)+10, 350, 20);
    lbl_seving_time .text = @"Serving Time: Now ";
    lbl_seving_time .font = [UIFont fontWithName:kFont size:15];
    lbl_seving_time .textColor = [UIColor blackColor];
    lbl_seving_time .backgroundColor = [UIColor clearColor];
    [ img_bg addSubview:lbl_seving_time ];
    
    
    UILabel *lbl_seving_qty = [[UILabel alloc]init];
    // lbl_seving_qty .frame = CGRectMake(18,CGRectGetMaxY(lbl_seving_time.frame)+10, 350, 20);
    lbl_seving_qty .text = @"Available Serving Qty Now:15";
    lbl_seving_qty .font = [UIFont fontWithName:kFont size:15];
    lbl_seving_qty .textColor = [UIColor blackColor];
    lbl_seving_qty .backgroundColor = [UIColor clearColor];
    [ img_bg addSubview:lbl_seving_qty ];
    
    UILabel *lbl_price = [[UILabel alloc]init];
    //lbl_price  .frame = CGRectMake(18,CGRectGetMaxY(lbl_seving_qty.frame)+10, 350, 20);
    lbl_price  .text = @"Price/Serving: $5.90";
    lbl_price .font = [UIFont fontWithName:kFont size:15];
    lbl_price  .textColor = [UIColor blackColor];
    lbl_price  .backgroundColor = [UIColor clearColor];
    [ img_bg addSubview:lbl_price  ];
    
    
    UIImageView *img_line = [[UIImageView alloc]init];
    //img_line .frame = CGRectMake(13,CGRectGetMaxY(lbl_price.frame)+10, 275, 0.5);
    [img_line setImage:[UIImage imageNamed:@"line2@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_line setUserInteractionEnabled:YES];
    [img_bg addSubview:img_line ];
    
    
    UILabel *lbl_serving_qty_order = [[UILabel alloc]init];
    //lbl_serving_qty_order  .frame = CGRectMake(18,CGRectGetMaxY(img_line.frame)+10, 350, 20);
    lbl_serving_qty_order  .text = @"Serving Quantity Ordered:";
    lbl_serving_qty_order .font = [UIFont fontWithName:kFontBold size:15];
    lbl_serving_qty_order  .textColor = [UIColor blackColor];
    lbl_serving_qty_order  .backgroundColor = [UIColor clearColor];
    [ img_bg addSubview:lbl_serving_qty_order ];
    
    UIImageView *img_rect = [[UIImageView alloc]init];
    //img_rect .frame = CGRectMake(210,CGRectGetMaxY(img_line.frame)+10, 50, 20);
    [img_rect setImage:[UIImage imageNamed:@"rectangle@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_rect setUserInteractionEnabled:YES];
    [img_bg addSubview:img_rect ];
    
    
    UIButton *icon_drop_down = [UIButton buttonWithType:UIButtonTypeCustom];
    //icon_drop_down .frame = CGRectMake(32,7,12,10);
    icon_drop_down  .backgroundColor = [UIColor clearColor];
    [icon_drop_down  addTarget:self action:@selector(btn_drop_down_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_drop_down  setImage:[UIImage imageNamed:@"icon-drop-down@2x.png"] forState:UIControlStateNormal];
    [img_rect   addSubview:icon_drop_down ];
    
    
    UILabel *lbl_qty_valu_inrect = [[UILabel alloc]init];
    // lbl_qty_valu_inrect  .frame = CGRectMake(10,5,12,12);
    lbl_qty_valu_inrect  .text = @"3";
    lbl_qty_valu_inrect.font = [UIFont fontWithName:kFont size:15];
    lbl_qty_valu_inrect  .textColor = [UIColor blackColor];
    lbl_qty_valu_inrect .backgroundColor = [UIColor clearColor];
    [ img_rect  addSubview:lbl_qty_valu_inrect];
    
    UILabel *lbl_serving_type = [[UILabel alloc]init];
    // lbl_serving_type .frame = CGRectMake(75,CGRectGetMaxY(lbl_serving_qty_order .frame)+10, 350, 20);
    lbl_serving_type .text = @"Select Serving Type";
    lbl_serving_type .font = [UIFont fontWithName:kFontBold size:15];
    lbl_serving_type  .textColor = [UIColor blackColor];
    lbl_serving_type  .backgroundColor = [UIColor clearColor];
    [ img_bg addSubview:lbl_serving_type];
    
    UIButton *img_dine_in = [UIButton buttonWithType:UIButtonTypeCustom];
    // img_dine_in .frame = CGRectMake(45,CGRectGetMaxY(lbl_serving_type .frame)+10, 60, 60);
    img_dine_in .backgroundColor = [UIColor clearColor];
    [img_dine_in addTarget:self action:@selector(btn_serving1_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_dine_in setImage:[UIImage imageNamed:@"img-dine@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:img_dine_in];
    
    UIButton *img_take_out = [UIButton buttonWithType:UIButtonTypeCustom];
    // img_take_out .frame = CGRectMake(125,CGRectGetMaxY(lbl_serving_type .frame)+10, 60, 60);
    img_take_out .backgroundColor = [UIColor clearColor];
    [img_take_out  addTarget:self action:@selector(btn_serving2_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_take_out setImage:[UIImage imageNamed:@"icon-takeout5@2x.png"] forState:UIControlStateNormal];
    [img_bg   addSubview:img_take_out];
    
    
    UIImageView *img_tick = [[UIImageView alloc]init];
    //img_tick .frame = CGRectMake(140,CGRectGetMaxY(lbl_serving_type .frame)+40, 30, 30);
    [img_tick setImage:[UIImage imageNamed:@"icon-red-tik@2x.png"]];
    img_tick.backgroundColor = [UIColor clearColor];
    [img_tick setUserInteractionEnabled:YES];
    [img_bg addSubview:img_tick ];
    
    UIButton *img_deliver = [UIButton buttonWithType:UIButtonTypeCustom];
    //img_deliver .frame = CGRectMake(210,CGRectGetMaxY(lbl_serving_type .frame)+10, 60, 60);
    img_deliver.backgroundColor = [UIColor clearColor];
    [img_deliver addTarget:self action:@selector(btn_serving3_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_deliver setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [img_bg   addSubview:img_deliver];
    
    
    UIButton *img_add_to_cart = [UIButton buttonWithType:UIButtonTypeCustom];
    //img_add_to_cart .frame = CGRectMake(-10,CGRectGetMaxY( img_bg .frame)+72, WIDTH+19, 35);
    img_add_to_cart .backgroundColor = [UIColor colorWithRed:39/255.0f green:37/255.0f blue:45/255.0f alpha:1];
    [img_add_to_cart  addTarget:self action:@selector(btn_add_to_cart_click:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:img_add_to_cart];
    
    
    UILabel *lbl_add_to_cart = [[UILabel alloc]init];
    //lbl_add_to_cart.frame = CGRectMake(100,5, 300, 20);
    lbl_add_to_cart .text = @"ADD TO CART";
    lbl_add_to_cart.font = [UIFont fontWithName:kFontBold size:20];
    lbl_add_to_cart .textColor = [UIColor whiteColor];
    lbl_add_to_cart .backgroundColor = [UIColor clearColor];
    lbl_add_to_cart.textAlignment = NSTextAlignmentCenter;
    [img_add_to_cart addSubview:lbl_add_to_cart];
    
    //    UIButton *btn_on_add_to_cart = [UIButton buttonWithType:UIButtonTypeCustom];
    //    //btn_on_add_to_cart .frame = CGRectMake(10,CGRectGetMaxY( img_bg .frame)+72, WIDTH+19, 35);
    //    btn_on_add_to_cart .backgroundColor = [UIColor redColor];
    //    [btn_on_add_to_cart  addTarget:self action:@selector(btn_add_to_cart_click:) forControlEvents:UIControlEventTouchUpInside];
    //   // [img_add_to_cart setImage:[UIImage imageNamed:@"img-cart@2x.png"] forState:UIControlStateNormal];
    //    [self.view  addSubview:btn_on_add_to_cart];
    
    
    if (IS_IPHONE_6Plus)
    {
        img_bg .frame = CGRectMake(0,CGRectGetMaxY(img_header.frame)+10, WIDTH+10, 370);
        lbl_kitchen_name.frame = CGRectMake(35,30, 350, 20);
        lbl_dish_name.frame = CGRectMake(35,CGRectGetMaxY(lbl_kitchen_name.frame)+10, 350, 20);
        lbl_seving_time .frame = CGRectMake(35,CGRectGetMaxY(lbl_dish_name.frame)+10, 350, 20);
        lbl_seving_qty .frame = CGRectMake(35,CGRectGetMaxY(lbl_seving_time.frame)+10, 350, 20);
        lbl_price  .frame = CGRectMake(35,CGRectGetMaxY(lbl_seving_qty.frame)+10, 350, 20);
        img_line .frame = CGRectMake(18,CGRectGetMaxY(lbl_price.frame)+10, WIDTH-50, 0.5);
        lbl_serving_qty_order  .frame = CGRectMake(35,CGRectGetMaxY(img_line.frame)+10, 350, 20);
        img_rect .frame = CGRectMake(230,CGRectGetMaxY(img_line.frame)+10, 50, 20);
        icon_drop_down .frame = CGRectMake(32,7,12,10);
        lbl_qty_valu_inrect  .frame = CGRectMake(10,5,12,12);
        lbl_serving_type .frame = CGRectMake(130,CGRectGetMaxY(lbl_serving_qty_order .frame)+20, 350, 20);
        img_dine_in .frame = CGRectMake(55,CGRectGetMaxY(lbl_serving_type .frame)+10, 60, 60);
        img_take_out .frame = CGRectMake(175,CGRectGetMaxY(lbl_serving_type .frame)+10, 60, 60);
        img_tick .frame = CGRectMake(195,CGRectGetMaxY(lbl_serving_type .frame)+40, 30, 30);
        img_deliver .frame = CGRectMake(290,CGRectGetMaxY(lbl_serving_type .frame)+10, 60, 60);
        img_add_to_cart .frame = CGRectMake(0,HEIGHT-60, WIDTH, 60);
        lbl_add_to_cart.frame = CGRectMake(40,5, 300, 50);
        // btn_on_add_to_cart .frame = CGRectMake(0,CGRectGetMaxY( img_bg .frame)+60, WIDTH+19, 39);
        
    }
    else if (IS_IPHONE_6)
    {
        img_bg .frame = CGRectMake(0,CGRectGetMaxY(img_header.frame)+10, WIDTH+7, 370);
        lbl_kitchen_name.frame = CGRectMake(35,30, 350, 20);
        lbl_dish_name.frame = CGRectMake(35,CGRectGetMaxY(lbl_kitchen_name.frame)+10, 350, 20);
        lbl_seving_time .frame = CGRectMake(35,CGRectGetMaxY(lbl_dish_name.frame)+10, 350, 20);
        lbl_seving_qty .frame = CGRectMake(35,CGRectGetMaxY(lbl_seving_time.frame)+10, 350, 20);
        lbl_price  .frame = CGRectMake(35,CGRectGetMaxY(lbl_seving_qty.frame)+10, 350, 20);
        img_line .frame = CGRectMake(18,CGRectGetMaxY(lbl_price.frame)+10, WIDTH-50, 0.5);
        lbl_serving_qty_order  .frame = CGRectMake(35,CGRectGetMaxY(img_line.frame)+10, 350, 20);
        img_rect .frame = CGRectMake(230,CGRectGetMaxY(img_line.frame)+10, 50, 20);
        icon_drop_down .frame = CGRectMake(32,7,12,10);
        lbl_qty_valu_inrect  .frame = CGRectMake(10,5,12,12);
        lbl_serving_type .frame = CGRectMake(130,CGRectGetMaxY(lbl_serving_qty_order .frame)+20, 350, 20);
        img_dine_in .frame = CGRectMake(55,CGRectGetMaxY(lbl_serving_type .frame)+10, 60, 60);
        img_take_out .frame = CGRectMake(175,CGRectGetMaxY(lbl_serving_type .frame)+10, 60, 60);
        img_tick .frame = CGRectMake(195,CGRectGetMaxY(lbl_serving_type .frame)+40, 30, 30);
        img_deliver .frame = CGRectMake(290,CGRectGetMaxY(lbl_serving_type .frame)+10, 60, 60);
        img_add_to_cart .frame = CGRectMake(-50,HEIGHT-60, WIDTH+100, 60);
        lbl_add_to_cart.frame = CGRectMake(90,0, 300, 60);
        // btn_on_add_to_cart .frame = CGRectMake(0,CGRectGetMaxY(img_bg .frame)+140, WIDTH+19, 39);
        
        
    }
    else
    {
        img_bg .frame = CGRectMake(-2,CGRectGetMaxY(img_header.frame)+10, WIDTH+10, 350);
        lbl_kitchen_name.frame = CGRectMake(28,20, 350, 20);
        lbl_dish_name.frame = CGRectMake(28,CGRectGetMaxY(lbl_kitchen_name.frame)+10, 350, 20);
        lbl_seving_time .frame = CGRectMake(28,CGRectGetMaxY(lbl_dish_name.frame)+10, 350, 20);
        lbl_seving_qty .frame = CGRectMake(28,CGRectGetMaxY(lbl_seving_time.frame)+10, 350, 20);
        lbl_price  .frame = CGRectMake(28,CGRectGetMaxY(lbl_seving_qty.frame)+10, 350, 20);
        img_line .frame = CGRectMake(22,CGRectGetMaxY(lbl_price.frame)+10, 275, 0.5);
        lbl_serving_qty_order  .frame = CGRectMake(28,CGRectGetMaxY(img_line.frame)+10, 350, 20);
        img_rect .frame = CGRectMake(220,CGRectGetMaxY(img_line.frame)+10, 50, 20);
        icon_drop_down .frame = CGRectMake(32,7,12,10);
        lbl_qty_valu_inrect  .frame = CGRectMake(10,5,12,12);
        lbl_serving_type .frame = CGRectMake(100,CGRectGetMaxY(lbl_serving_qty_order .frame)+10, 350, 20);
        img_dine_in .frame = CGRectMake(35,CGRectGetMaxY(lbl_serving_type .frame)+10, 60, 60);
        img_take_out .frame = CGRectMake(135,CGRectGetMaxY(lbl_serving_type .frame)+10, 60, 60);
        img_tick .frame = CGRectMake(153,CGRectGetMaxY(lbl_serving_type .frame)+37, 25, 25);
        img_deliver .frame = CGRectMake(230,CGRectGetMaxY(lbl_serving_type .frame)+10, 60, 60);
        img_add_to_cart .frame = CGRectMake(0,HEIGHT-43, WIDTH+100, 50);
        lbl_add_to_cart.frame = CGRectMake(0,9, 300, 20);
        // btn_on_add_to_cart .frame = CGRectMake(0,IS_IPHONE_5?CGRectGetMaxY( img_bg .frame)+120:CGRectGetMaxY( img_bg .frame)+30, WIDTH+19, 39);
        
        
    }
    
    
}

-(void)btn_back_click:(UIButton *)sender
{
    NSLog(@"icon_back_click Btn Click");
    [self.navigationController popViewControllerAnimated:NO];
    
    
}

-(void)btn_drop_down_click:(UIButton *)sender
{
    NSLog(@"icon_drop_down_click Btn Click");
    
}
-(void)btn_serving1_click:(UIButton *)sender
{
    NSLog(@"icon_drop_down_click Btn Click");
    
}

-(void)btn_serving2_click:(UIButton *)sender
{
    NSLog(@"icon_drop_down_click Btn Click");
    
}

-(void)btn_serving3_click:(UIButton *)sender
{
    NSLog(@"icon_drop_down_click Btn Click");
    
}
-(void)btn_add_to_cart_click:(UIButton *)sender
{
    NSLog(@"icon_add_to_cart_click Btn Click");
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
