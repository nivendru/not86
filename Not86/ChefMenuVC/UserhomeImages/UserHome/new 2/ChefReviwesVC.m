//
//  ChefReviwesVC.m
//  Not86
//
//  Created by Admin on 30/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#import "Define.h"



#import "ChefReviwesVC.h"
#import "Define.h"


@interface ChefReviwesVC ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>
{
    UIImageView *img_background;
    UIImageView * img_header;
    CGFloat	animatedDistance;
    UIView * view_body;
    UITableView * table_for_reviwes;
    
    UITextView *txtview_message;
    NSMutableArray *array_text_view;
    
    
}

@end

@implementation ChefReviwesVC

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
    
    [self integrateBody];
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)integrateHeader
{
    img_header=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 50)];
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back addTarget:self action:@selector(btn_back_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_back];
    
    UILabel *lbl_Login = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(icon_back.frame)+30,0,200, 45)];
    lbl_Login.text = @"Chef Reviews";
    lbl_Login.font = [UIFont fontWithName:kFont size:20];
    lbl_Login.textColor = [UIColor whiteColor];
    lbl_Login.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_Login];
    
    UIImageView *img_circle = [[UIImageView alloc]init];
    img_circle.frame =CGRectMake(WIDTH-40, 8, 30, 30);
    [img_circle setImage:[UIImage imageNamed:@"logo@2x.png"]];
    //img_circle.backgroundColor = [UIColor whiteColor];
    [img_circle setUserInteractionEnabled:YES];
    [img_header addSubview:img_circle];
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}
-(void)integrateBody
{
    
    
#pragma view_for_menu
    
    view_body = [[UIView alloc]init];
    view_body.frame=CGRectMake(0,51,WIDTH,HEIGHT);
    view_body.backgroundColor=[UIColor clearColor];
    [self.view  addSubview:view_body];
    
    
    UIImageView *img_serch_bar = [[UIImageView alloc]init];
    img_serch_bar.frame = CGRectMake(25,80, WIDTH-50, 40);
    [img_serch_bar setImage:[UIImage imageNamed:@"serch-bar@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_serch_bar setUserInteractionEnabled:YES];
    [self.view addSubview:img_serch_bar];
    
    
    UITextField *txt_search = [[UITextField alloc] init];
    txt_search.frame = CGRectMake(5,0, 200, 45);
    txt_search .borderStyle = UITextBorderStyleNone;
    txt_search .textColor = [UIColor grayColor];
    txt_search .font = [UIFont fontWithName:kFont size:15];
    txt_search .placeholder = @"Search...";
    [txt_search  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_search  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_search .leftView = padding;
    txt_search .leftViewMode = UITextFieldViewModeAlways;
    txt_search .userInteractionEnabled=YES;
    txt_search .textAlignment = NSTextAlignmentLeft;
    txt_search .backgroundColor = [UIColor clearColor];
    txt_search .keyboardType = UIKeyboardTypeAlphabet;
    txt_search .delegate = self;
    [img_serch_bar addSubview:txt_search ];
    
    
    UIImageView *icon_search = [[UIImageView alloc]init];
    icon_search.frame = CGRectMake(290,5, 25, 25);
    [icon_search setImage:[UIImage imageNamed:@"serach1@2x.png"]];
    icon_search.backgroundColor = [UIColor clearColor];
    [icon_search setUserInteractionEnabled:YES];
    [img_serch_bar addSubview:icon_search];
    
    UIButton *btn_on_search_bar = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_search_bar.frame = CGRectMake(310,85, 30, 30);
    btn_on_search_bar .backgroundColor = [UIColor clearColor];
    [btn_on_search_bar addTarget:self action:@selector(btn_on_search_bar_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [self.view  addSubview:btn_on_search_bar];
    
    UIImageView *img_calender = [[UIImageView alloc]init];
    img_calender.frame = CGRectMake(35,CGRectGetMaxY(img_serch_bar.frame)-15, 30, 30);
    [img_calender setImage:[UIImage imageNamed:@"img-calender@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_calender setUserInteractionEnabled:YES];
    [view_body addSubview:img_calender];
    
    
    UILabel *lbl_date = [[UILabel alloc]init];
    lbl_date.frame = CGRectMake(CGRectGetMaxX(img_calender.frame)+10,CGRectGetMaxY(img_serch_bar.frame)-10, 120, 25);
    lbl_date.text = @"17-05-2015";
    lbl_date.font = [UIFont fontWithName:kFont size:20];
    lbl_date.textColor = [UIColor whiteColor];
    lbl_date.backgroundColor = [UIColor blackColor];
    [view_body addSubview:lbl_date];
    
    UIImageView *img_line = [[UIImageView alloc]init];
    img_line.frame = CGRectMake(190,CGRectGetMaxY(img_serch_bar.frame)-20, 0.7, 50);
    [img_line setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
    //   img_line.backgroundColor = [UIColor redColor];
    [img_line setUserInteractionEnabled:YES];
    [view_body addSubview:img_line];
    
    
    UIImageView *img_calender2 = [[UIImageView alloc]init];
    img_calender2.frame = CGRectMake(210,CGRectGetMaxY(img_serch_bar.frame)-15, 30, 30);
    [img_calender2 setImage:[UIImage imageNamed:@"img-calender@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_calender2 setUserInteractionEnabled:YES];
    [view_body addSubview:img_calender2];
    
    UILabel *lbl_date2 = [[UILabel alloc]init];
    lbl_date2.frame = CGRectMake(CGRectGetMaxX(img_calender2.frame)+10,CGRectGetMaxY(img_serch_bar.frame)-10, 120, 25);
    lbl_date2.text = @"17-06-2015";
    lbl_date2.font = [UIFont fontWithName:kFont size:20];
    lbl_date2.textColor = [UIColor whiteColor];
    lbl_date2.backgroundColor = [UIColor blackColor];
    [view_body addSubview:lbl_date2];
    
#pragma mark Tableview
    
    table_for_reviwes = [[UITableView alloc] init ];
    table_for_reviwes.frame  = CGRectMake(13,CGRectGetMaxY(img_calender.frame)+25,WIDTH-26,500);
    [table_for_reviwes setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_reviwes.delegate = self;
    table_for_reviwes.dataSource = self;
    table_for_reviwes.showsVerticalScrollIndicator = NO;
    table_for_reviwes.backgroundColor = [UIColor clearColor];
    [view_body addSubview: table_for_reviwes];
    
    if (IS_IPHONE_6Plus)
    {
        view_body.frame=CGRectMake(0,51,WIDTH,HEIGHT);
        img_serch_bar.frame = CGRectMake(25,80, WIDTH-50, 40);
        txt_search.frame = CGRectMake(5,0, 200, 45);
        icon_search.frame = CGRectMake(330,5, 25, 25);
        btn_on_search_bar.frame = CGRectMake(355,85, 35, 35);
        img_calender.frame = CGRectMake(35,CGRectGetMaxY(img_serch_bar.frame)-15, 30, 30);
        lbl_date.frame = CGRectMake(CGRectGetMaxX(img_calender.frame)+10,CGRectGetMaxY(img_serch_bar.frame)-10, 120, 25);
        img_line.frame = CGRectMake(190,CGRectGetMaxY(img_serch_bar.frame)-20, 0.7, 50);
        img_calender2.frame = CGRectMake(210,CGRectGetMaxY(img_serch_bar.frame)-15, 30, 30);
        lbl_date2.frame = CGRectMake(CGRectGetMaxX(img_calender2.frame)+10,CGRectGetMaxY(img_serch_bar.frame)-10, 120, 25);
        table_for_reviwes.frame  = CGRectMake(13,CGRectGetMaxY(img_calender.frame)+25,WIDTH-26,500);
        
    }
    else if(IS_IPHONE_6)
    {
        view_body.frame=CGRectMake(0,51,WIDTH,HEIGHT);
        img_serch_bar.frame = CGRectMake(25,80, WIDTH-50, 40);
        txt_search.frame = CGRectMake(5,0, 200, 45);
        icon_search.frame = CGRectMake(290,5, 25, 25);
        btn_on_search_bar.frame = CGRectMake(310,85, 30, 30);
        img_calender.frame = CGRectMake(35,CGRectGetMaxY(img_serch_bar.frame)-15, 30, 30);
        lbl_date.frame = CGRectMake(CGRectGetMaxX(img_calender.frame)+10,CGRectGetMaxY(img_serch_bar.frame)-10, 120, 25);
        img_line.frame = CGRectMake(190,CGRectGetMaxY(img_serch_bar.frame)-20, 0.7, 50);
        img_calender2.frame = CGRectMake(210,CGRectGetMaxY(img_serch_bar.frame)-15, 30, 30);
        lbl_date2.frame = CGRectMake(CGRectGetMaxX(img_calender2.frame)+10,CGRectGetMaxY(img_serch_bar.frame)-10, 120, 25);
        table_for_reviwes.frame  = CGRectMake(13,CGRectGetMaxY(img_calender.frame)+25,WIDTH-26,450);
    }
    else
    {
        view_body.frame=CGRectMake(0,51,WIDTH,HEIGHT);
        img_serch_bar.frame = CGRectMake(25,80, WIDTH-40, 40);
        txt_search.frame = CGRectMake(5,0, 200, 45);
        icon_search.frame = CGRectMake(250,5, 25, 25);
        btn_on_search_bar.frame = CGRectMake(270,85, 30, 30);
        img_calender.frame = CGRectMake(30,CGRectGetMaxY(img_serch_bar.frame)-15, 25, 25);
        lbl_date.frame = CGRectMake(CGRectGetMaxX(img_calender.frame)+5,CGRectGetMaxY(img_serch_bar.frame)-10, 90, 20);
        img_line.frame = CGRectMake(155,CGRectGetMaxY(img_serch_bar.frame)-20, 0.7, 40);
        img_calender2.frame = CGRectMake(175,CGRectGetMaxY(img_serch_bar.frame)-15, 25, 25);
        lbl_date2.frame = CGRectMake(CGRectGetMaxX(img_calender2.frame)+5,CGRectGetMaxY(img_serch_bar.frame)-10, 90, 20);
        table_for_reviwes.frame  = CGRectMake(13,CGRectGetMaxY(img_calender.frame)+25,WIDTH-26,500);
        lbl_date.font = [UIFont fontWithName:kFont size:15];
        lbl_date2.font = [UIFont fontWithName:kFont size:15];
        
        
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 160;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    //img_cellBackGnd.frame =  CGRectMake(0,40, 300, 150);
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [cell.contentView addSubview:img_cellBackGnd];
    
    
    UIImageView *img_user = [[UIImageView alloc] init];
    //   img_user.frame = CGRectMake(20,0, 50,  50 );
    [img_user setImage:[UIImage imageNamed:@"img-user@2x.png"]];
    [img_cellBackGnd addSubview:img_user];
    
    
    UIImageView *img_flag = [[UIImageView alloc] init];
    //   img_flag.frame = CGRectMake(260,10, 20,  25 );
    [img_flag setImage:[UIImage imageNamed:@"icon-flag.png"]];
    [img_cellBackGnd addSubview:img_flag];
    
    UILabel *user_name = [[UILabel alloc]init];
    //    user_name  .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,5,200,25);
    user_name .text = @"Jonathan Timothy";
    user_name .font = [UIFont fontWithName:kFont size:13];
    user_name.textColor = [UIColor blackColor];
    user_name .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:user_name];
    
    UILabel *taste = [[UILabel alloc]init];
    //  taste .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,20,200,25);
    taste .text = @"Taste:";
    taste .font = [UIFont fontWithName:kFont size:10];
    taste.textColor = [UIColor blackColor];
    taste .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:taste];
    
    UILabel *taste_value = [[UILabel alloc]init];
    //   taste_value .frame = CGRectMake(CGRectGetMaxX(taste.frame)-170,20,200,25);
    taste_value .text = @"7";
    taste_value .font = [UIFont fontWithName:kFont size:10];
    taste_value.textColor = [UIColor redColor];
    taste_value .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:taste_value];
    
    
    UILabel *text_appeal = [[UILabel alloc]init];
    //   text_appeal .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+70,20,200,25);
    text_appeal.text = @"Appeal:";
    text_appeal .font = [UIFont fontWithName:kFont size:10];
    text_appeal.textColor = [UIColor blackColor];
    text_appeal .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:text_appeal];
    
    UILabel *appeal_value = [[UILabel alloc]init];
    //    appeal_value .frame = CGRectMake(CGRectGetMaxX(text_appeal .frame)-160,20,200,25);
    appeal_value .text = @"8";
    appeal_value .font = [UIFont fontWithName:kFont size:10];
    appeal_value.textColor = [UIColor redColor];
    appeal_value .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:appeal_value];
    
    
    UILabel *text_value = [[UILabel alloc]init];
    //   text_value .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+135,20,200,25);
    text_value.text = @"Value:";
    text_value .font = [UIFont fontWithName:kFont size:10];
    text_value.textColor = [UIColor blackColor];
    text_value .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:text_value];
    
    UILabel *value_value = [[UILabel alloc]init];
    //  value_value.frame = CGRectMake(CGRectGetMaxX(text_value.frame)-165,20,200,25);
    value_value .text = @"9";
    value_value .font = [UIFont fontWithName:kFont size:10];
    value_value.textColor = [UIColor redColor];
    value_value .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:value_value];
    
    
    
    UILabel *text_date = [[UILabel alloc]init];
    //   text_date .frame = CGRectMake(20,CGRectGetMaxY(taste_value .frame)+10,200,25);
    text_date.text = @"18 May 2015";
    text_date .font = [UIFont fontWithName:kFont size:10];
    text_date.textColor = [UIColor blackColor];
    text_date.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:text_date];
    
    
    UILabel *text_time = [[UILabel alloc]init];
    //   text_time .frame = CGRectMake(250,CGRectGetMaxY(text_date  .frame)-25,200,25);
    text_time.text = @"2:30pm";
    text_time .font = [UIFont fontWithName:kFont size:10];
    text_time.textColor = [UIColor blackColor];
    text_time.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:text_time];
    
    txtview_message = [[UITextView alloc]init];
    //  txtview_message.frame = CGRectMake(5,CGRectGetMaxY(img_user.frame)+25, 295,50);
    txtview_message.scrollEnabled = YES;
    txtview_message.userInteractionEnabled = YES;
    txtview_message.font = [UIFont fontWithName:kFont size:10];
    txtview_message.backgroundColor = [UIColor clearColor];
    txtview_message.delegate = self;
    txtview_message.textColor = [UIColor blackColor];
    txtview_message.text =@"LOrem ipsumdotor sit amet.ne dicit virits corpor a per,ut illum liberavisse vet.At mei vidit conse cteture.in vitae definitionem per.tate vetear cliquado ei vim.";
    [img_cellBackGnd addSubview:txtview_message];
    
    
    UILabel *text_replied = [[UILabel alloc]init];
    //   text_replied .frame = CGRectMake(250,CGRectGetMaxY(txtview_message  .frame)-3,200,25);
    text_replied.text = @"Replied";
    text_replied .font = [UIFont fontWithName:kFont size:10];
    text_replied.textColor = [UIColor blackColor];
    text_replied.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:text_replied];
    
    
    
    if (IS_IPHONE_6Plus)
    {
        img_cellBackGnd.frame =  CGRectMake(0,40, WIDTH-20, 165);
        img_user.frame = CGRectMake(20,10, 50,  50 );
        img_flag.frame = CGRectMake(350,10, 20,  25 );
        user_name  .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,5,200,25);
        taste .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,20,200,25);
        
        
        taste_value .frame = CGRectMake(CGRectGetMaxX(taste.frame)-170,20,200,25);
        text_appeal .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+95,20,200,25);
        appeal_value .frame = CGRectMake(CGRectGetMaxX(text_appeal .frame)-160,20,200,25);
        text_value .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+185,20,200,25);
        value_value.frame = CGRectMake(CGRectGetMaxX(text_value.frame)-165,20,200,25);
        text_date .frame = CGRectMake(20,CGRectGetMaxY(taste_value .frame)+20,200,25);
        
        text_time .frame = CGRectMake(340,CGRectGetMaxY(text_date  .frame)-25,200,25);
        txtview_message.frame = CGRectMake(5,CGRectGetMaxY(img_user.frame)+30, WIDTH-40,50);
        text_replied .frame = CGRectMake(340,CGRectGetMaxY(txtview_message  .frame),200,25);
        
    }
    else if (IS_IPHONE_6)
    {
        img_cellBackGnd.frame =  CGRectMake(0,40, WIDTH-20, 255);
        img_user.frame = CGRectMake(20,10, 50,  50 );
        img_flag.frame = CGRectMake(320,10, 20,  25 );
        user_name  .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,5,200,25);
        taste .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,20,200,25);
        
        
        taste_value .frame = CGRectMake(CGRectGetMaxX(taste.frame)-170,20,200,25);
        text_appeal .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+95,20,200,25);
        appeal_value .frame = CGRectMake(CGRectGetMaxX(text_appeal .frame)-160,20,200,25);
        text_value .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+185,20,200,25);
        value_value.frame = CGRectMake(CGRectGetMaxX(text_value.frame)-165,20,200,25);
        text_date .frame = CGRectMake(20,CGRectGetMaxY(taste_value .frame)+20,200,25);
        
        text_time .frame = CGRectMake(305,CGRectGetMaxY(text_date  .frame)-25,200,25);
        txtview_message.frame = CGRectMake(5,CGRectGetMaxY(img_user.frame)+30, WIDTH-40,50);
        text_replied .frame = CGRectMake(305,CGRectGetMaxY(txtview_message.frame)-5,200,25);
    }
    else
    {
        img_cellBackGnd.frame =  CGRectMake(0,40, 300, 150);
        img_user.frame = CGRectMake(20,0, 50,  50 );
        img_flag.frame = CGRectMake(260,10, 20,  25 );
        user_name  .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,5,200,25);
        taste .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,20,200,25);
        
        
        taste_value .frame = CGRectMake(CGRectGetMaxX(taste.frame)-170,20,200,25);
        text_appeal .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+70,20,200,25);
        appeal_value .frame = CGRectMake(CGRectGetMaxX(text_appeal .frame)-160,20,200,25);
        text_value .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+135,20,200,25);
        value_value.frame = CGRectMake(CGRectGetMaxX(text_value.frame)-165,20,200,25);
        text_date .frame = CGRectMake(20,CGRectGetMaxY(taste_value .frame)+10,200,25);
        
        text_time .frame = CGRectMake(250,CGRectGetMaxY(text_date  .frame)-25,200,25);
        txtview_message.frame = CGRectMake(5,CGRectGetMaxY(img_user.frame)+25, 295,50);
        text_replied .frame = CGRectMake(250,CGRectGetMaxY(txtview_message  .frame)-3,200,25);
        
        
    }
    
    
    
    /* UIImageView *img_chef = [[UIImageView alloc]init];
     img_chef .frame = CGRectMake(10,CGRectGetMaxY(text_food_details.frame)+13, 50, 50);
     [img_chef  setImage:[UIImage imageNamed:@"img-chef@2x.png"]];
     //   icon_user.backgroundColor = [UIColor redColor];
     [img_chef  setUserInteractionEnabled:YES];
     [img_cellBackGnd addSubview:img_chef ];*/
    return cell;
    
    
}

-(void)btn_back_click:(UIButton *)sender
{
    NSLog(@"icon_Food_Later_BtnClick:");
    
}
-(void)btn_on_search_bar_click:(UIButton *)sender
{
    NSLog(@"btn_on_search_bar_click:");
    
}

#pragma mark TextField Delegate methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //    if(textField == txt_username)
    //    {
    //        if ([string isEqualToString:@" "])
    //        {
    //            return NO;
    //        }
    //    }
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
