//
//  DeliverDetailsVC.m
//  Not86
//
//  Created by Admin on 11/09/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "DeliverDetailsVC.h"

#import "ChefOrdersVC.h"
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"


@interface DeliverDetailsVC ()<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>
{
    CGFloat	animatedDistance;
    UIImageView * img_header;
    UIScrollView * scroll;
    UITableView *  table_for_delivery_items;
    NSMutableArray * array_order_no;
    NSMutableArray *  array_delivery_items;
    NSMutableArray * array_qt_no;
    NSMutableArray * array_rates_of_item;
    UIView * view_for_popup;
    UITableView * img_table_for_popup;
    UITextView * txtview_adddescription;
    
    UIView * view_for_individual_items;
}

@end

@implementation DeliverDetailsVC

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [self integrateHeader];
   [self integrateBodyDesign];
  [self popup_cancel_oeder];
    [self individual_items];
    
    array_order_no = [[NSMutableArray alloc]initWithObjects:@"10847", nil];
    array_delivery_items = [[NSMutableArray alloc]initWithObjects:@"Raspberry Custard",@"Hawaiian Pizza",@"Cheesy Paster",@"Pizza", nil];
    array_qt_no = [[NSMutableArray alloc]initWithObjects:@"X3",@"X2",@"X1",@"X1", nil];
    array_rates_of_item = [[NSMutableArray alloc]initWithObjects:@"$13.90",@"$20.80",@"$16.50",@"$16.50",nil];

    
}


-(BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)integrateHeader
{
    
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [ self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back addTarget:self action:@selector(click_on_menu_btn:) forControlEvents:UIControlEventTouchUpInside];
    [icon_back setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_back];
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init ];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_back.frame)+30,0, 150, 45);
    lbl_User_Sign_Up.text = @"Orders";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    UIImageView *icon_user = [[UIImageView alloc]init ];
    icon_user .frame = CGRectMake(WIDTH-40, 8, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"img_logo@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
    
}

-(void)integrateBodyDesign
{
    
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    scroll.frame = CGRectMake(0, 45, WIDTH, HEIGHT);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    UIImageView *img_bg = [[UIImageView alloc]init];
    img_bg.frame = CGRectMake(10,5, WIDTH-20, 150);
    [img_bg setUserInteractionEnabled:YES];
    img_bg.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [ scroll addSubview:img_bg];
    
    UIImageView *img_user = [[UIImageView alloc]init ];
    img_user .frame = CGRectMake(10,05,50,50);
    [img_user setImage:[UIImage imageNamed:@"img-user@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_user setUserInteractionEnabled:YES];
    [img_bg addSubview:img_user];

    UILabel *lbl_User_name = [[UILabel alloc]init ];
    lbl_User_name.frame = CGRectMake(CGRectGetMaxX(img_user.frame)+10,10, 150, 45);
    lbl_User_name.text = @"Jane Doe";
    lbl_User_name.font = [UIFont fontWithName:kFontBold size:15];
    lbl_User_name.textColor = [UIColor blackColor];
    lbl_User_name.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_User_name];
    
    UIButton *icon_message = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_message.frame = CGRectMake(CGRectGetMaxX(img_user.frame)+250,20,25,20);
    icon_message .backgroundColor = [UIColor clearColor];
    [icon_message addTarget:self action:@selector(click_on_message_btn:) forControlEvents:UIControlEventTouchUpInside];
    [icon_message setImage:[UIImage imageNamed:@"icon-msg@2x.png"] forState:UIControlStateNormal];
    [img_bg   addSubview:icon_message];
    
    
    UIImageView *img_line = [[UIImageView alloc]init];
    img_line .frame = CGRectMake(15,CGRectGetMaxY(img_user.frame)+5,WIDTH-50,0.5);
    [img_line  setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
    img_line .backgroundColor = [UIColor clearColor];
    [img_line  setUserInteractionEnabled:YES];
    [img_bg addSubview:img_line];
    
    UILabel *lbl_order_no = [[UILabel alloc]init ];
    lbl_order_no.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)-5, 150, 45);
    lbl_order_no.text = @"Order no.:";
    lbl_order_no.font = [UIFont fontWithName:kFont size:18];
    lbl_order_no.textColor = [UIColor blackColor];
    lbl_order_no.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_order_no];
    
    UILabel * lbl_order_number_val = [[UILabel alloc]init];
    lbl_order_number_val .frame = CGRectMake(CGRectGetMidX(lbl_order_no.frame)+17,CGRectGetMaxY(img_line.frame)+8,80,20);
    lbl_order_number_val .text = @"10847";
    lbl_order_number_val .font = [UIFont fontWithName:kFontBold size:15];
    lbl_order_number_val .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    lbl_order_number_val .backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_order_number_val ];
    
    UILabel *lbl_value = [[UILabel alloc]init ];
    lbl_value.frame = CGRectMake(100,CGRectGetMaxY(lbl_order_no.frame)-25, 150, 45);
    lbl_value.text = @"Value:";
    lbl_value.font = [UIFont fontWithName:kFont size:12];
    lbl_value.textColor = [UIColor blackColor];
    lbl_value.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_value];
    
    UILabel *value = [[UILabel alloc]init];
    value .frame = CGRectMake(CGRectGetMinX(lbl_value.frame)+43,CGRectGetMaxY(lbl_order_number_val.frame),80,20);
    value .text = @"$50.80";
    value .font = [UIFont fontWithName:kFontBold size:12];
    value .textColor = [UIColor blackColor];
    value .backgroundColor = [UIColor clearColor];
    [img_bg addSubview:value ];
    
    UILabel *lbl_order_date = [[UILabel alloc]init ];
    lbl_order_date.frame = CGRectMake(20,CGRectGetMinY(lbl_value.frame)+15, 150, 45);
    lbl_order_date.text = @"Ordered Date/Time:";
    lbl_order_date.font = [UIFont fontWithName:kFont size:12];
    lbl_order_date.textColor = [UIColor blackColor];
    lbl_order_date.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_order_date];
    
    UILabel *order_date_val = [[UILabel alloc]init];
    order_date_val .frame = CGRectMake(CGRectGetMinX(lbl_order_date.frame)+123,CGRectGetMaxY(value.frame)-4,150,20);
    order_date_val .text = @"16/07/2015, 4:00:PM";
    order_date_val .font = [UIFont fontWithName:kFont size:12];
    order_date_val .textColor = [UIColor blackColor];
    order_date_val .backgroundColor = [UIColor clearColor];
    [img_bg addSubview:order_date_val];
    
    UILabel *lbl_serving_date = [[UILabel alloc]init ];
    lbl_serving_date.frame = CGRectMake(28,CGRectGetMinY(lbl_order_date.frame)+15, 150, 45);
    lbl_serving_date.text = @"Serving Date/Time:";
    lbl_serving_date.font = [UIFont fontWithName:kFont size:12];
    lbl_serving_date.textColor = [UIColor blackColor];
    lbl_serving_date.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_serving_date];
    
    UILabel *serving_date_val = [[UILabel alloc]init];
    serving_date_val .frame = CGRectMake(CGRectGetMinX(lbl_serving_date.frame)+115,CGRectGetMaxY(order_date_val.frame)-4,150,20);
    serving_date_val .text = @"17/07/2015, 1:00:PM";
    serving_date_val .font = [UIFont fontWithName:kFontBold size:12];
    serving_date_val .textColor = [UIColor blackColor];
    serving_date_val .backgroundColor = [UIColor clearColor];
    [img_bg addSubview:serving_date_val];
    
    UIButton *icon_cross = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_cross.frame = CGRectMake(CGRectGetMaxX(serving_date_val.frame),CGRectGetMaxY(img_line.frame),63,90);
    icon_cross .backgroundColor = [UIColor clearColor];
    [icon_cross addTarget:self action:@selector(click_on_x_btn:) forControlEvents:UIControlEventTouchUpInside];
    [icon_cross setImage:[UIImage imageNamed:@"img-x@2x.png"]forState:UIControlStateNormal];
    [img_bg   addSubview:icon_cross];
    
    UIImageView *img_bg2 = [[UIImageView alloc]init];
    img_bg2.frame = CGRectMake(8,CGRectGetMaxY(img_bg.frame), WIDTH-11,650);
    [img_bg2 setUserInteractionEnabled:YES];
    img_bg2.image=[UIImage imageNamed:@"bg-img@2x.png"];
    [scroll addSubview:img_bg2];
    
#pragma mark Tableview
    
    table_for_delivery_items = [[UITableView alloc] init ];
    table_for_delivery_items.frame  = CGRectMake(5,7,WIDTH-25,150);
    [table_for_delivery_items setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_delivery_items.delegate = self;
    table_for_delivery_items.dataSource = self;
    table_for_delivery_items.showsVerticalScrollIndicator = NO;
    table_for_delivery_items.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:table_for_delivery_items];
    
    UILabel *text_delivery_fee = [[UILabel alloc]init];
    text_delivery_fee.frame = CGRectMake(20,CGRectGetMaxY(table_for_delivery_items.frame)-10, 200, 45);
    text_delivery_fee.text = @"Delivery Fee";
    text_delivery_fee.font = [UIFont fontWithName:kFont size:12];
    text_delivery_fee.textColor = [UIColor blackColor];
    text_delivery_fee.textAlignment = NSTextAlignmentLeft;
    text_delivery_fee.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_delivery_fee];
    
    UILabel *delivery_fee = [[UILabel alloc]init];
    delivery_fee.frame = CGRectMake(CGRectGetMaxX(text_delivery_fee.frame)+85,CGRectGetMaxY(table_for_delivery_items.frame)-10, 200, 45);
    delivery_fee.text = @"$5.00";
    delivery_fee.font = [UIFont fontWithName:kFontBold size:12];
    delivery_fee.textColor = [UIColor blackColor];
    delivery_fee.textAlignment = NSTextAlignmentLeft;
    delivery_fee.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:delivery_fee];
    
    UILabel *text_sub_total = [[UILabel alloc]init];
    text_sub_total.frame = CGRectMake(20,CGRectGetMaxY(text_delivery_fee.frame)-15, 200, 45);
    text_sub_total.text = @"Subtotal";
    text_sub_total.font = [UIFont fontWithName:kFont size:12];
    text_sub_total.textColor = [UIColor blackColor];
    text_sub_total.textAlignment = NSTextAlignmentLeft;
    text_sub_total.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_sub_total];
    
    UILabel *sub_total_val = [[UILabel alloc]init];
    sub_total_val.frame = CGRectMake(CGRectGetMaxX(text_sub_total.frame)+80,CGRectGetMaxY(delivery_fee.frame)-15, 200, 45);
    sub_total_val.text = @"$56.00";
    sub_total_val.font = [UIFont fontWithName:kFontBold size:12];
    sub_total_val.textColor = [UIColor blackColor];
    sub_total_val.textAlignment = NSTextAlignmentLeft;
    sub_total_val.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:sub_total_val];
    
    UILabel *text_serving_fee = [[UILabel alloc]init];
    text_serving_fee.frame = CGRectMake(20,CGRectGetMaxY(text_sub_total.frame)-15, 200, 45);
    text_serving_fee.text = @"Not86 Service Fee (7%)";
    text_serving_fee.font = [UIFont fontWithName:kFont size:12];
    text_serving_fee.textColor = [UIColor blackColor];
    text_serving_fee.textAlignment = NSTextAlignmentLeft;
    text_serving_fee.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_serving_fee];
    
    UILabel *service_fee_val = [[UILabel alloc]init];
    service_fee_val.frame = CGRectMake(CGRectGetMaxX(text_serving_fee.frame)+85,CGRectGetMaxY(sub_total_val.frame)-15, 200, 45);
    service_fee_val.text = @"$3.00";
    service_fee_val.font = [UIFont fontWithName:kFontBold size:12];
    service_fee_val.textColor = [UIColor blackColor];
    service_fee_val.textAlignment = NSTextAlignmentLeft;
    service_fee_val.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:service_fee_val];
    
    UILabel *text_total_bill = [[UILabel alloc]init];
    text_total_bill .frame = CGRectMake(20,CGRectGetMaxY(text_serving_fee.frame)-15, 200, 45);
    text_total_bill .text = @"Total Bill";
    text_total_bill .font = [UIFont fontWithName:kFont size:12];
    text_total_bill .textColor = [UIColor blackColor];
    text_total_bill .textAlignment = NSTextAlignmentLeft;
    text_total_bill .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_total_bill ];
    
    UILabel *toal_bill_val = [[UILabel alloc]init];
    toal_bill_val.frame = CGRectMake(CGRectGetMaxX(text_total_bill.frame)+70,CGRectGetMaxY(service_fee_val.frame)-15, 200, 45);
    toal_bill_val.text = @"$67.50";
    toal_bill_val.font = [UIFont fontWithName:kFontBold size:16];
    toal_bill_val.textColor = [UIColor blackColor];
    toal_bill_val.textAlignment = NSTextAlignmentLeft;
    toal_bill_val.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:toal_bill_val];
    
    UIImageView *img_line2 = [[UIImageView alloc]init];
    img_line2.frame = CGRectMake(20,CGRectGetMaxY(text_total_bill.frame), WIDTH-50, 0.5);
    // [img_line setUserInteractionEnabled:YES];
    img_line2.image=[UIImage imageNamed:@"line1@2x.png"];
    [img_bg2 addSubview:img_line2];
    
    UILabel *text_dietary_restrictions = [[UILabel alloc]init];
    text_dietary_restrictions .frame = CGRectMake(20,CGRectGetMaxY(img_line2 .frame)-10, 200, 45);
    text_dietary_restrictions .text = @"Dietary Resstrictions";
    text_dietary_restrictions .font = [UIFont fontWithName:kFontBold size:12];
    text_dietary_restrictions .textColor = [UIColor blackColor];
    text_dietary_restrictions .textAlignment = NSTextAlignmentLeft;
    text_dietary_restrictions .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_dietary_restrictions];
    
    
    UILabel *text_low_sodium = [[UILabel alloc]init];
    text_low_sodium .frame = CGRectMake(20,CGRectGetMaxY(text_dietary_restrictions .frame)-25, 200, 45);
    text_low_sodium .text = @"Low Sodium, Organic";
    text_low_sodium .font = [UIFont fontWithName:kFont size:12];
    text_low_sodium .textColor = [UIColor blackColor];
    text_low_sodium .textAlignment = NSTextAlignmentLeft;
    text_low_sodium .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_low_sodium];
    
    UILabel *labl_food_allergies = [[UILabel alloc]init];
    labl_food_allergies .frame = CGRectMake(20,CGRectGetMaxY(text_low_sodium.frame)-19, 200, 45);
    labl_food_allergies .text = @"Food Allergies";
    labl_food_allergies .font = [UIFont fontWithName:kFontBold size:12];
    labl_food_allergies .textColor = [UIColor blackColor];
    labl_food_allergies .textAlignment = NSTextAlignmentLeft;
    labl_food_allergies .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:labl_food_allergies];
    
    
    UILabel *text_nuts = [[UILabel alloc]init];
    text_nuts .frame = CGRectMake(20,CGRectGetMaxY(labl_food_allergies .frame)-25, 200, 45);
    text_nuts .text = @"Nuts";
    text_nuts .font = [UIFont fontWithName:kFont size:12];
    text_nuts .textColor = [UIColor blackColor];
    text_nuts .textAlignment = NSTextAlignmentLeft;
    text_nuts .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_nuts];

    
    UIImageView *img_line3 = [[UIImageView alloc]init];
    img_line3.frame = CGRectMake(20,CGRectGetMaxY(text_nuts.frame)-5, WIDTH-50, 0.5);
    // [img_line setUserInteractionEnabled:YES];
    img_line3.image=[UIImage imageNamed:@"line1@2x.png"];
    [img_bg2 addSubview:img_line3];
    
    UILabel *text_spl_request = [[UILabel alloc]init];
    text_spl_request .frame = CGRectMake(20,CGRectGetMaxY(img_line3.frame)-5, 200, 45);
    text_spl_request .text = @"Special Requests/Remarks";
    text_spl_request .font = [UIFont fontWithName:kFontBold size:12];
    text_spl_request .textColor = [UIColor blackColor];
    text_spl_request .textAlignment = NSTextAlignmentLeft;
    text_spl_request .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_spl_request];
    
    UILabel *text_no_chilli = [[UILabel alloc]init];
    text_no_chilli .frame = CGRectMake(20,CGRectGetMaxY(text_spl_request .frame)-25, 200, 45);
    text_no_chilli .text = @"No chilli and cheese";
    text_no_chilli .font = [UIFont fontWithName:kFont size:12];
    text_no_chilli .textColor = [UIColor blackColor];
    text_no_chilli .textAlignment = NSTextAlignmentLeft;
    text_no_chilli .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_no_chilli];

    UIImageView *img_line4 = [[UIImageView alloc]init];
    img_line4.frame = CGRectMake(20,CGRectGetMaxY(text_no_chilli.frame)-5, WIDTH-50, 0.5);
    // [img_line setUserInteractionEnabled:YES];
    img_line4.image=[UIImage imageNamed:@"line1@2x.png"];
    [img_bg2 addSubview:img_line4];
    
    UILabel *text_delivery_address = [[UILabel alloc]init];
    text_delivery_address .frame = CGRectMake(20,CGRectGetMaxY(img_line4.frame)-5, 200, 45);
    text_delivery_address .text = @"Delivery Address";
    text_delivery_address .font = [UIFont fontWithName:kFontBold size:12];
    text_delivery_address .textColor = [UIColor blackColor];
    text_delivery_address .textAlignment = NSTextAlignmentLeft;
    text_delivery_address .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_delivery_address];
    
    UILabel *text_address = [[UILabel alloc]init];
    text_address .frame = CGRectMake(20,CGRectGetMaxY(text_delivery_address .frame)-25, 200, 45);
    text_address .text = @"Smith St, Paris 12345";
    text_address .font = [UIFont fontWithName:kFont size:12];
    text_address .textColor = [UIColor blackColor];
    text_address .textAlignment = NSTextAlignmentLeft;
    text_address .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_address];
    
    UIImageView *img_line5 = [[UIImageView alloc]init];
    img_line5.frame = CGRectMake(20,CGRectGetMaxY(text_address.frame)-5, WIDTH-50, 0.5);
    // [img_line setUserInteractionEnabled:YES];
    img_line5.image=[UIImage imageNamed:@"line1@2x.png"];
    [img_bg2 addSubview:img_line5];
    
    UILabel *text_delivery_company = [[UILabel alloc]init];
    text_delivery_company .frame = CGRectMake(20,CGRectGetMaxY(img_line5.frame)-5, 200, 45);
    text_delivery_company .text = @"Delivery Company";
    text_delivery_company .font = [UIFont fontWithName:kFontBold size:12];
    text_delivery_company .textColor = [UIColor blackColor];
    text_delivery_company .textAlignment = NSTextAlignmentLeft;
    text_delivery_company .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_delivery_company];
    
    UILabel *company_address = [[UILabel alloc]init];
    company_address .frame = CGRectMake(20,CGRectGetMaxY(text_delivery_company .frame)-25, 200, 45);
    company_address .text = @"Time Delivery";
    company_address .font = [UIFont fontWithName:kFont size:12];
    company_address .textColor = [UIColor blackColor];
    company_address .textAlignment = NSTextAlignmentLeft;
    company_address .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:company_address];
    
    UIImageView *img_line6 = [[UIImageView alloc]init];
    img_line6.frame = CGRectMake(20,CGRectGetMaxY(company_address.frame)-5, WIDTH-50, 0.5);
    // [img_line setUserInteractionEnabled:YES];
    img_line6.image=[UIImage imageNamed:@"line1@2x.png"];
    [img_bg2 addSubview:img_line6];
    
    UILabel *text_delivery_cantact = [[UILabel alloc]init];
    text_delivery_cantact .frame = CGRectMake(20,CGRectGetMaxY(img_line6.frame)-5, 200, 45);
    text_delivery_cantact .text = @"Delivery Contact";
    text_delivery_cantact .font = [UIFont fontWithName:kFontBold size:12];
    text_delivery_cantact .textColor = [UIColor blackColor];
    text_delivery_cantact .textAlignment = NSTextAlignmentLeft;
    text_delivery_cantact .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:text_delivery_cantact];
    
    UILabel *cantact_no = [[UILabel alloc]init];
    cantact_no .frame = CGRectMake(20,CGRectGetMaxY(text_delivery_cantact .frame)-25, 200, 45);
    cantact_no .text = @"+98 267 782 734";
    cantact_no .font = [UIFont fontWithName:kFont size:12];
    cantact_no .textColor = [UIColor blackColor];
    cantact_no .textAlignment = NSTextAlignmentLeft;
    cantact_no .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:cantact_no];

    [scroll setContentSize:CGSizeMake(0,1080)];

}

#pragma mark popup_traceTaxi

-(void)popup_cancel_oeder
{
    [view_for_popup removeFromSuperview];
    view_for_popup=[[UIView alloc] init];
    view_for_popup.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.7];
    view_for_popup.userInteractionEnabled=TRUE;
    [self.view addSubview:view_for_popup];
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    [alertViewBody setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    alertViewBody.backgroundColor=[UIColor whiteColor];
    alertViewBody.userInteractionEnabled = YES;
    [view_for_popup addSubview:alertViewBody];
    
    UIImageView *img_cross =[[UIImageView alloc] init];
    [img_cross setImage:[UIImage imageNamed:@"cross-img@2x.png"]];
    img_cross.backgroundColor=[UIColor whiteColor];
    img_cross.userInteractionEnabled = YES;
    [alertViewBody addSubview:img_cross];
    
    UIButton *btn_on_cross = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_cross .backgroundColor = [UIColor clearColor];
    //[btn_on_x setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_cross addTarget:self action:@selector(btn_on_cross_img_click:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody   addSubview:btn_on_cross];
    
    UILabel *text_on_popup_bg = [[UILabel alloc]init];
    text_on_popup_bg .text = @"Are you sure you want to\ncancel this order?";
    text_on_popup_bg.numberOfLines = 2;
    text_on_popup_bg .textColor = [UIColor blackColor];
    text_on_popup_bg .textAlignment = NSTextAlignmentLeft;
    text_on_popup_bg .backgroundColor = [UIColor clearColor];
    [alertViewBody  addSubview:text_on_popup_bg];
    #pragma mark Tableview
    
    img_table_for_popup = [[UITableView alloc] init ];
    [img_table_for_popup setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table_for_popup.delegate = self;
    img_table_for_popup.dataSource = self;
    img_table_for_popup.showsVerticalScrollIndicator = NO;
    img_table_for_popup.backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:img_table_for_popup];
    
    UILabel *text_delivery_fee = [[UILabel alloc]init];
    text_delivery_fee.frame = CGRectMake(20,CGRectGetMaxY(img_table_for_popup.frame)-10, 200, 45);
    text_delivery_fee.text = @"Delivery Fee";
    text_delivery_fee.font = [UIFont fontWithName:kFont size:14];
    text_delivery_fee.textColor = [UIColor blackColor];
    text_delivery_fee.textAlignment = NSTextAlignmentLeft;
    text_delivery_fee.backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:text_delivery_fee];
    
    UILabel *delivery_fee = [[UILabel alloc]init];
    delivery_fee.frame = CGRectMake(CGRectGetMaxX(text_delivery_fee.frame)+85,CGRectGetMaxY(img_table_for_popup.frame)-10, 200, 45);
    delivery_fee.text = @"$5.00";
    delivery_fee.font = [UIFont fontWithName:kFontBold size:14];
    delivery_fee.textColor = [UIColor blackColor];
    delivery_fee.textAlignment = NSTextAlignmentLeft;
    delivery_fee.backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:delivery_fee];
    
    UILabel *text_sub_total = [[UILabel alloc]init];
    text_sub_total.frame = CGRectMake(20,CGRectGetMaxY(text_delivery_fee.frame)-15, 200, 45);
    text_sub_total.text = @"Subtotal";
    text_sub_total.font = [UIFont fontWithName:kFont size:14];
    text_sub_total.textColor = [UIColor blackColor];
    text_sub_total.textAlignment = NSTextAlignmentLeft;
    text_sub_total.backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:text_sub_total];
    
    UILabel *sub_total_val = [[UILabel alloc]init];
    sub_total_val.frame = CGRectMake(CGRectGetMaxX(text_sub_total.frame)+80,CGRectGetMaxY(delivery_fee.frame)-15, 200, 45);
    sub_total_val.text = @"$56.00";
    sub_total_val.font = [UIFont fontWithName:kFontBold size:14];
    sub_total_val.textColor = [UIColor blackColor];
    sub_total_val.textAlignment = NSTextAlignmentLeft;
    sub_total_val.backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:sub_total_val];
    
    UILabel *text_serving_fee = [[UILabel alloc]init];
    text_serving_fee.frame = CGRectMake(20,CGRectGetMaxY(text_sub_total.frame)-15, 200, 45);
    text_serving_fee.text = @"Not86 Service Fee (7%)";
    text_serving_fee.font = [UIFont fontWithName:kFont size:14];
    text_serving_fee.textColor = [UIColor blackColor];
    text_serving_fee.textAlignment = NSTextAlignmentLeft;
    text_serving_fee.backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:text_serving_fee];
    
    UILabel *service_fee_val = [[UILabel alloc]init];
    service_fee_val.frame = CGRectMake(CGRectGetMaxX(text_serving_fee.frame)+85,CGRectGetMaxY(sub_total_val.frame)-15, 200, 45);
    service_fee_val.text = @"$3.00";
    service_fee_val.font = [UIFont fontWithName:kFontBold size:14];
    service_fee_val.textColor = [UIColor blackColor];
    service_fee_val.textAlignment = NSTextAlignmentLeft;
    service_fee_val.backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:service_fee_val];
    
    UILabel *text_total_bill = [[UILabel alloc]init];
    text_total_bill .frame = CGRectMake(20,CGRectGetMaxY(text_serving_fee.frame)-15, 200, 45);
    text_total_bill .text = @"Total Value";
    text_total_bill .font = [UIFont fontWithName:kFont size:14];
    text_total_bill .textColor = [UIColor blackColor];
    text_total_bill .textAlignment = NSTextAlignmentLeft;
    text_total_bill .backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:text_total_bill ];
    
    UILabel *toal_bill_val = [[UILabel alloc]init];
    toal_bill_val.frame = CGRectMake(CGRectGetMaxX(text_total_bill.frame)+70,CGRectGetMaxY(service_fee_val.frame)-15, 200, 45);
    toal_bill_val.text = @"$67.50";
    toal_bill_val.font = [UIFont fontWithName:kFontBold size:18];
    toal_bill_val.textColor = [UIColor blackColor];
    toal_bill_val.textAlignment = NSTextAlignmentLeft;
    toal_bill_val.backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:toal_bill_val];
    
    UILabel *text_note = [[UILabel alloc]init];
    text_note .text = @"Note that cancelling entire orders will result\nin your not86 account being reviewed. A\nrefund will be granted back to the diner.";
    text_note.frame = CGRectMake(20,CGRectGetMaxY(text_total_bill.frame), 400, 45);
    text_note.font = [UIFont fontWithName:kFont size:12];
    text_note.numberOfLines = 4;
    text_note .textColor = [UIColor clearColor];
    text_note .textAlignment = NSTextAlignmentLeft;
    text_note .backgroundColor = [UIColor clearColor];
    [alertViewBody  addSubview:text_note];
    
    UILabel *text_cancellation = [[UILabel alloc]init];
    text_cancellation .text = @"Please write your reason for the cancellation";
    text_cancellation.frame = CGRectMake(20,CGRectGetMaxY(text_note.frame), 400, 45);
    text_cancellation.font = [UIFont fontWithName:kFont size:12];
    text_cancellation.numberOfLines = 0;
    text_cancellation .textColor = [UIColor blackColor];
    text_cancellation .textAlignment = NSTextAlignmentLeft;
    text_cancellation .backgroundColor = [UIColor clearColor];
    [alertViewBody  addSubview:text_cancellation];
    
    UIImageView *img_rect = [[UIImageView alloc]init];
    img_rect.frame = CGRectMake(20,CGRectGetMaxY(text_cancellation.frame), WIDTH-50, 50);
    // [img_line setUserInteractionEnabled:YES];
    img_rect.userInteractionEnabled = YES;
    img_rect.image=[UIImage imageNamed:@"img-rectangle@2x.png"];
    [alertViewBody addSubview:img_rect];
    
    txtview_adddescription = [[UITextView alloc]init];
    txtview_adddescription.frame = CGRectMake(20,20, 288,60);
    txtview_adddescription.scrollEnabled = YES;
    txtview_adddescription.userInteractionEnabled = YES;
    txtview_adddescription.font = [UIFont fontWithName:kFont size:12];
    txtview_adddescription.backgroundColor = [UIColor clearColor];
    txtview_adddescription.delegate = self;
    txtview_adddescription.textColor = [UIColor blackColor];
    txtview_adddescription.text =@"";
    [img_rect addSubview:txtview_adddescription];

    
    UIImageView *img_yes = [[UIImageView alloc]init];
    img_yes.frame = CGRectMake(20,CGRectGetMaxY(img_rect.frame), 150, 50);
    [img_yes setUserInteractionEnabled:YES];
    img_yes.image=[UIImage imageNamed:@"img-btn-confirm@2x.png"];
    [alertViewBody addSubview:img_yes];
    
    UIButton *btn_on_yes = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_yes  .backgroundColor = [UIColor clearColor];
    btn_on_yes.frame = CGRectMake(20,CGRectGetMaxY(img_rect.frame), 150, 50);
    //[btn_on_x setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_yes  addTarget:self action:@selector(btn_on_conform_img_click:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody    addSubview:btn_on_yes ];
    
    
    UIImageView *img_no = [[UIImageView alloc]init];
     img_no.frame = CGRectMake(CGRectGetMaxX(img_yes.frame)+20,CGRectGetMaxY(img_rect.frame), 150, 50);
    [img_no setUserInteractionEnabled:YES];
    img_no.image=[UIImage imageNamed:@"img-btn-decline2x.png"];
    [alertViewBody addSubview:img_no];
    
    UIButton *btn_on_no = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_no.frame = CGRectMake(CGRectGetMaxX(img_yes.frame)+20,CGRectGetMaxY(img_rect.frame), 150, 50);
    btn_on_no .backgroundColor = [UIColor clearColor];
    //[btn_on_x setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_no addTarget:self action:@selector(btn_on_declint_img_click:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody    addSubview:btn_on_no];
    



//rectangle@2x.png
    
    
    
    if (IS_IPHONE_6)
    {
        
        view_for_popup.frame = CGRectMake(0,0,414, 736);
        alertViewBody.frame = CGRectMake(20,15,WIDTH-40,630);
        img_cross.frame = CGRectMake(300,20,20,20);
        btn_on_cross.frame = CGRectMake(290,15,50,50);
        
        text_on_popup_bg .frame = CGRectMake(20,-10, 500, 90);
        text_on_popup_bg .font = [UIFont fontWithName:kFontBold size:19];
        
        img_table_for_popup.frame  = CGRectMake(5,CGRectGetMaxY(text_on_popup_bg.frame)-10,WIDTH-55,100);
        text_delivery_fee.frame = CGRectMake(20,CGRectGetMaxY(img_table_for_popup.frame)-10, 200, 45);
        delivery_fee.frame = CGRectMake(CGRectGetMaxX(text_delivery_fee.frame)+65,CGRectGetMaxY(img_table_for_popup.frame)-10, 200, 45);
        text_sub_total.frame = CGRectMake(20,CGRectGetMaxY(text_delivery_fee.frame)-15, 200, 45);
        sub_total_val.frame = CGRectMake(CGRectGetMaxX(text_sub_total.frame)+60,CGRectGetMaxY(delivery_fee.frame)-15, 200, 45);
        text_serving_fee.frame = CGRectMake(20,CGRectGetMaxY(text_sub_total.frame)-15, 200, 45);
        service_fee_val.frame = CGRectMake(CGRectGetMaxX(text_serving_fee.frame)+65,CGRectGetMaxY(sub_total_val.frame)-15, 200, 45);
        text_total_bill .frame = CGRectMake(20,CGRectGetMaxY(text_serving_fee.frame)-15, 200, 45);
        toal_bill_val.frame = CGRectMake(CGRectGetMaxX(text_total_bill.frame)+50,CGRectGetMaxY(service_fee_val.frame)-15, 200, 45);
    
        text_note.frame = CGRectMake(20,CGRectGetMaxY(text_total_bill.frame), 600, 45);
        text_note.font = [UIFont fontWithName:kFont size:14.5];
        
        text_cancellation.frame = CGRectMake(20,CGRectGetMaxY(text_note.frame), 400, 45);
        text_cancellation.font = [UIFont fontWithName:kFont size:14];
        
         img_rect.frame = CGRectMake(25,CGRectGetMaxY(text_cancellation.frame), WIDTH-90, 150);
         txtview_adddescription.frame = CGRectMake(0,0, 290,60);
        
        img_yes.frame = CGRectMake(10,CGRectGetMaxY(img_rect.frame)+15, 150, 50);
        btn_on_yes.frame = CGRectMake(10,CGRectGetMaxY(img_rect.frame)+15, 150, 50);
        img_no.frame = CGRectMake(CGRectGetMaxX(img_yes.frame)+15,CGRectGetMaxY(img_rect.frame)+15, 150, 50);
        btn_on_no.frame = CGRectMake(CGRectGetMaxX(img_yes.frame)+15,CGRectGetMaxY(img_rect.frame)+15, 150, 50);

        
    }
    else if (IS_IPHONE_6Plus)
    {
        
        view_for_popup.frame = CGRectMake(0,0,414, 736);
        alertViewBody.frame = CGRectMake(20,137,WIDTH-40,374);
        img_cross.frame = CGRectMake(335,10,15,15);
        btn_on_cross.frame = CGRectMake(290,10,50,50);
        
        text_on_popup_bg .frame = CGRectMake(20,10, 500, 90);
        text_on_popup_bg .font = [UIFont fontWithName:kFontBold size:17];
        
        img_table_for_popup.frame  = CGRectMake(5,CGRectGetMaxY(text_on_popup_bg.frame)+5,WIDTH-55,55);
        text_delivery_fee.frame = CGRectMake(20,CGRectGetMaxY(img_table_for_popup.frame)-10, 200, 45);
        delivery_fee.frame = CGRectMake(CGRectGetMaxX(text_delivery_fee.frame)+85,CGRectGetMaxY(img_table_for_popup.frame)-10, 200, 45);
        text_sub_total.frame = CGRectMake(20,CGRectGetMaxY(text_delivery_fee.frame)-15, 200, 45);
        sub_total_val.frame = CGRectMake(CGRectGetMaxX(text_sub_total.frame)+80,CGRectGetMaxY(delivery_fee.frame)-15, 200, 45);
        text_serving_fee.frame = CGRectMake(20,CGRectGetMaxY(text_sub_total.frame)-15, 200, 45);
        service_fee_val.frame = CGRectMake(CGRectGetMaxX(text_serving_fee.frame)+85,CGRectGetMaxY(sub_total_val.frame)-15, 200, 45);
        text_total_bill .frame = CGRectMake(20,CGRectGetMaxY(text_serving_fee.frame)-15, 200, 45);
        toal_bill_val.frame = CGRectMake(CGRectGetMaxX(text_total_bill.frame)+70,CGRectGetMaxY(service_fee_val.frame)-15, 200, 45);
        
        text_note.frame = CGRectMake(20,CGRectGetMaxY(text_total_bill.frame), 400, 45);
        text_note.font = [UIFont fontWithName:kFont size:12];
        
        text_cancellation.frame = CGRectMake(20,CGRectGetMaxY(text_note.frame), 400, 45);
        text_cancellation.font = [UIFont fontWithName:kFont size:12];
        
         img_rect.frame = CGRectMake(20,CGRectGetMaxY(text_cancellation.frame), WIDTH-70, 100);
         txtview_adddescription.frame = CGRectMake(0,0, 290,60);
        
        img_yes.frame = CGRectMake(20,CGRectGetMaxY(img_rect.frame), 150, 50);
        btn_on_yes.frame = CGRectMake(20,CGRectGetMaxY(img_rect.frame), 150, 50);
        img_no.frame = CGRectMake(CGRectGetMaxX(img_yes.frame)+20,CGRectGetMaxY(img_rect.frame), 150, 50);
        btn_on_no.frame = CGRectMake(CGRectGetMaxX(img_yes.frame)+20,CGRectGetMaxY(img_rect.frame), 150, 50);


        
    }
    else
    {
        view_for_popup.frame = CGRectMake(0,0,414, 736);
        alertViewBody.frame = CGRectMake(20,137,WIDTH-40,374);
        img_cross.frame = CGRectMake(335,10,15,15);
        btn_on_cross.frame = CGRectMake(290,10,50,50);
        
        text_on_popup_bg .frame = CGRectMake(20,10, 500, 90);
        text_on_popup_bg .font = [UIFont fontWithName:kFontBold size:17];
        
        img_table_for_popup.frame  = CGRectMake(5,CGRectGetMaxY(text_on_popup_bg.frame)+5,WIDTH-55,34);
        text_delivery_fee.frame = CGRectMake(20,CGRectGetMaxY(img_table_for_popup.frame)-10, 200, 45);
        delivery_fee.frame = CGRectMake(CGRectGetMaxX(text_delivery_fee.frame)+85,CGRectGetMaxY(img_table_for_popup.frame)-10, 200, 45);
        text_sub_total.frame = CGRectMake(20,CGRectGetMaxY(text_delivery_fee.frame)-15, 200, 45);
        sub_total_val.frame = CGRectMake(CGRectGetMaxX(text_sub_total.frame)+80,CGRectGetMaxY(delivery_fee.frame)-15, 200, 45);
        text_serving_fee.frame = CGRectMake(20,CGRectGetMaxY(text_sub_total.frame)-15, 200, 45);
        service_fee_val.frame = CGRectMake(CGRectGetMaxX(text_serving_fee.frame)+85,CGRectGetMaxY(sub_total_val.frame)-15, 200, 45);
        text_total_bill .frame = CGRectMake(20,CGRectGetMaxY(text_serving_fee.frame)-15, 200, 45);
        toal_bill_val.frame = CGRectMake(CGRectGetMaxX(text_total_bill.frame)+70,CGRectGetMaxY(service_fee_val.frame)-15, 200, 45);
        
        text_note.frame = CGRectMake(20,CGRectGetMaxY(text_total_bill.frame), 400, 45);
        text_note.font = [UIFont fontWithName:kFont size:12];
        
        text_cancellation.frame = CGRectMake(20,CGRectGetMaxY(text_note.frame), 400, 45);
        text_cancellation.font = [UIFont fontWithName:kFont size:12];
        
         img_rect.frame = CGRectMake(20,CGRectGetMaxY(text_cancellation.frame), WIDTH-70, 100);
         txtview_adddescription.frame = CGRectMake(0,0, 290,60);
        
        img_yes.frame = CGRectMake(20,CGRectGetMaxY(img_rect.frame), 150, 50);
        btn_on_yes.frame = CGRectMake(20,CGRectGetMaxY(img_rect.frame), 150, 50);
        img_no.frame = CGRectMake(CGRectGetMaxX(img_yes.frame)+20,CGRectGetMaxY(img_rect.frame), 150, 50);
        btn_on_no.frame = CGRectMake(CGRectGetMaxX(img_yes.frame)+20,CGRectGetMaxY(img_rect.frame), 150, 50);
        
    }
    
    view_for_popup .hidden = YES;
}


#pragma tableview delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [array_delivery_items count];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table_for_delivery_items)
    {
         return 48;
    }
    else
    {
     return 50;
    }
    return 0;
   
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews)
    {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-15, 37);
    [img_cellBackGnd setImage:[UIImage imageNamed:@"img-white-bg1@2x.png"]];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [cell.contentView addSubview:img_cellBackGnd];
    
    UIImageView *delivery_img = [[UIImageView alloc]init];
    delivery_img.frame =  CGRectMake(10,5, 20, 20);
    [delivery_img setImage:[UIImage imageNamed:@"img-delivery1@2x.png"]];
    [delivery_img setUserInteractionEnabled:YES];
    [img_cellBackGnd addSubview:delivery_img];
    
    //  [NSString stringWithFormat:@"%@",[ary_displaynames objectAtIndex:indexPath.row]];
    
    UILabel *items_name = [[UILabel alloc]init];
    items_name.frame = CGRectMake(CGRectGetMaxX(delivery_img.frame)+8,-5, 170, 40);
    items_name.text = [NSString stringWithFormat:@"%@",[array_delivery_items objectAtIndex:indexPath.row]];
    items_name.font = [UIFont fontWithName:kFontBold size:12];
    items_name.textColor = [UIColor blackColor];
    items_name.textAlignment = NSTextAlignmentLeft;
    items_name.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:items_name];
    
    UILabel *items_qt = [[UILabel alloc]init];
    items_qt.frame = CGRectMake(CGRectGetMaxX(items_name.frame)+20,-5, 50, 40);
    items_qt.text = [NSString stringWithFormat:@"%@",[array_qt_no objectAtIndex:indexPath.row]];
    items_qt.font = [UIFont fontWithName:kFont size:12];
    items_qt.textColor = [UIColor blackColor];
    items_qt.textAlignment = NSTextAlignmentLeft;
    items_qt.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:items_qt];
    
    UILabel *items_rate = [[UILabel alloc]init];
    items_rate.frame = CGRectMake(CGRectGetMaxX(items_qt.frame)+60,-5, 200, 40);
    items_rate.text = [NSString stringWithFormat:@"%@",[array_rates_of_item objectAtIndex:indexPath.row]];
    items_rate.font = [UIFont fontWithName:kFontBold size:12];
    items_rate.textColor = [UIColor blackColor];
    items_rate.textAlignment = NSTextAlignmentLeft;
    items_rate.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:items_rate];
    if (IS_IPHONE_6Plus)
    {
        if (tableView ==  table_for_delivery_items)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-15, 45);
            delivery_img.frame =  CGRectMake(10,5, 20, 20);
            items_name.frame = CGRectMake(CGRectGetMaxX(delivery_img.frame)+8,-5, 170, 40);
            items_qt.frame = CGRectMake(CGRectGetMaxX(items_name.frame)+20,-5, 50, 40);
            items_rate.frame = CGRectMake(CGRectGetMaxX(items_qt.frame)+60,-5, 200, 40);
        }
        else if (tableView == img_table_for_popup )
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-15, 45);
            delivery_img.frame =  CGRectMake(10,5, 20, 20);
            items_name.frame = CGRectMake(CGRectGetMaxX(delivery_img.frame)+8,-5, 170, 40);
            items_qt.frame = CGRectMake(CGRectGetMaxX(items_name.frame)+20,-5, 50, 40);
            items_rate.frame = CGRectMake(CGRectGetMaxX(items_qt.frame)+60,-5, 200, 40);
        }
        
    }
    else if(IS_IPHONE_6)
    {
        if ( table_for_delivery_items)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-20, 54);
            delivery_img.frame =  CGRectMake(10,10, 25,25);
            items_name.frame = CGRectMake(CGRectGetMaxX(delivery_img.frame)+8,5, 170, 40);
            items_qt.frame = CGRectMake(CGRectGetMaxX(items_name.frame)+20,5, 50, 40);
            items_rate.frame = CGRectMake(CGRectGetMaxX(items_qt.frame)+10,5, 200, 40);
        }
        else if (tableView == img_table_for_popup)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-80, 54);
            delivery_img.frame =  CGRectMake(10,10, 25,25);
            items_name.frame = CGRectMake(CGRectGetMaxX(delivery_img.frame)+8,5, 170, 40);
            items_qt.frame = CGRectMake(CGRectGetMaxX(items_name.frame)+0,5, 50, 40);
            items_rate.frame = CGRectMake(CGRectGetMaxX(items_qt.frame)-50,5, 200, 40);

        }
        
    }
    else
    {
        if ( table_for_delivery_items)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-15, 37);
            delivery_img.frame =  CGRectMake(10,5, 20, 20);
            items_name.frame = CGRectMake(CGRectGetMaxX(delivery_img.frame)+8,-5, 170, 40);
            items_qt.frame = CGRectMake(CGRectGetMaxX(items_name.frame)+20,-5, 50, 40);
            items_rate.frame = CGRectMake(CGRectGetMaxX(items_qt.frame)+60,-5, 200, 40);
        }
        else if (tableView == img_table_for_popup)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-15, 37);
            delivery_img.frame =  CGRectMake(10,5, 20, 20);
            items_name.frame = CGRectMake(CGRectGetMaxX(delivery_img.frame)+8,-5, 170, 40);
            items_qt.frame = CGRectMake(CGRectGetMaxX(items_name.frame)+20,-5, 50, 40);
            items_rate.frame = CGRectMake(CGRectGetMaxX(items_qt.frame)+60,-5, 200, 40);
        }
        
    }
    return cell;
    
}
-(void)individual_items
{
    
    [view_for_individual_items removeFromSuperview];
    view_for_individual_items=[[UIView alloc] init];
    view_for_individual_items.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.7];
    view_for_individual_items.userInteractionEnabled=TRUE;
    [self.view addSubview:view_for_individual_items];
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    [alertViewBody setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    alertViewBody.backgroundColor=[UIColor whiteColor];
    alertViewBody.userInteractionEnabled = YES;
    [view_for_individual_items addSubview:alertViewBody];

    
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
   // [self individual_items];
    
}



-(void)click_on_menu_btn:(UIButton *)sender
{
    NSLog(@"click_on_menu_btn:");
}
-(void)click_on_message_btn:(UIButton *)sender
{
    NSLog(@"click_on_message_btn:");
}
-(void)click_on_x_btn:(UIButton *)sender
{
    NSLog(@"click_on_x_btn:");
    view_for_popup.hidden = NO;
}
-(void)btn_on_cross_img_click:(UIButton *)sender
{
    NSLog(@"btn_on_cross_img_click:");
     view_for_popup.hidden = YES;
}
-(void)btn_on_conform_img_click:(UIButton *)sender
{
    NSLog(@"btn_on_conform_img_click:");
}
-(void)btn_on_declint_img_click:(UIButton *)sender
{
    NSLog(@"btn_on_declint_img_click:");
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}

#pragma mark TextField Delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //    if(textField == txt_username)
    //    {
    //        if ([string isEqualToString:@" "])
    //        {
    //            return NO;
    //        }
    //    }
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
