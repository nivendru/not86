//
//  SerchForFoodLaterVC.m
//  Not86
//
//  Created by Admin on 30/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "SerchForFoodLaterVC.h"
//#import "LocationVC.h"
#import "Define.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"

@interface SerchForFoodLaterVC ()<UITextFieldDelegate,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    UIImageView *img_header;
    UIView *view_serch_for_food_later;
    UIScrollView *scroll;
    UITableView * img_table;
    NSMutableArray *array_displaynames;
    NSMutableArray*ary_displaynames;
    NSMutableArray * array_img;
    
    CGFloat	animatedDistance;
    
    
}
@end

@implementation SerchForFoodLaterVC

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    ary_displaynames =[[NSMutableArray alloc]initWithObjects:@"Steamed Thai oath",@"Rasberry custored", nil];
    array_img = [[NSMutableArray alloc]initWithObjects:@"img-dish1@2x.png",@"img-dish2@2x.png", nil];
    [self integrateHeader];
    [self integrateBodyDesign];
    
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}
-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 50);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu.frame = CGRectMake(10,13,20,20);
    icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    [icon_menu addTarget:self action:@selector(btn_menu_click:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:icon_menu];
    
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 150, 45);
    lbl_User_Sign_Up.text = @"Food Later";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    UIButton *icon_drop_down = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_drop_down.frame = CGRectMake(CGRectGetMaxX(lbl_User_Sign_Up.frame)-35,15,20,20);
    icon_drop_down .backgroundColor = [UIColor clearColor];
    [icon_drop_down addTarget:self action:@selector(btn_drop_down:) forControlEvents:UIControlEventTouchUpInside];
    [icon_drop_down setImage:[UIImage imageNamed:@"img-white-dropd-@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_drop_down];
    
    UIButton *btn_on_food_later_in_head = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_food_later_in_head.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0,150,45);
    btn_on_food_later_in_head .backgroundColor = [UIColor clearColor];
    [btn_on_food_later_in_head addTarget:self action:@selector(btn_on_food_later_label_click:) forControlEvents:UIControlEventTouchUpInside];
    //[icon_drop_down setImage:[UIImage imageNamed:@"drop-down@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:btn_on_food_later_in_head];
    
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 11, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}
-(void)integrateBodyDesign
{
    
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor clearColor];
    scroll.frame = CGRectMake(0, 46, WIDTH, HEIGHT+150);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    
    view_serch_for_food_later = [[UIView alloc]init];
    view_serch_for_food_later.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT);
    view_serch_for_food_later.backgroundColor=[UIColor clearColor];
    [scroll  addSubview:  view_serch_for_food_later];
    
      UILabel *lbl_date = [[UILabel alloc]init];
    lbl_date.frame = CGRectMake(130,0, 150, 45);
    lbl_date.text = @"Date";
    lbl_date.font = [UIFont fontWithName:kFont size:18];
    lbl_date.textColor = [UIColor blackColor];
    lbl_date.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_date];
    
    UIImageView *img_calender = [[UIImageView alloc]init];
    img_calender.frame = CGRectMake(85,CGRectGetMaxY(lbl_date.frame), 30, 30);
    [img_calender setImage:[UIImage imageNamed:@"img-calender@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_calender setUserInteractionEnabled:YES];
    [view_serch_for_food_later addSubview:img_calender];
    
    UITextField *txt_date = [[UITextField alloc] init];
    txt_date .frame = CGRectMake(CGRectGetMaxX( img_calender.frame)+5,CGRectGetMaxY(lbl_date.frame), 100, 30);
    txt_date .borderStyle = UITextBorderStyleNone;
    txt_date .textColor = [UIColor grayColor];
    txt_date .font = [UIFont fontWithName:kFont size:13];
    txt_date .placeholder = @"15-07-2015";
    [txt_date  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_date  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_date .leftView = padding;
    txt_date .leftViewMode = UITextFieldViewModeAlways;
    txt_date .userInteractionEnabled=YES;
    txt_date .textAlignment = NSTextAlignmentLeft;
    txt_date .backgroundColor = [UIColor clearColor];
    txt_date .keyboardType = UIKeyboardTypeAlphabet;
    txt_date .delegate = self;
    [view_serch_for_food_later addSubview:txt_date ];
    
    UIButton *btn_set_date = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_set_date.frame = CGRectMake(85,CGRectGetMaxY(lbl_date.frame),150,35);
    btn_set_date .backgroundColor = [UIColor clearColor];
    [btn_set_date addTarget:self action:@selector(btn_set_date_click:) forControlEvents:UIControlEventTouchUpInside];
    // [btn_set_date setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_set_date];
    
    
    UILabel *lbl_serving_time = [[UILabel alloc]init];
    lbl_serving_time.frame = CGRectMake(105,CGRectGetMaxY(txt_date.frame)+5, 150, 45);
    lbl_serving_time.text = @"Serving Time";
    lbl_serving_time.font = [UIFont fontWithName:kFont size:18];
    lbl_serving_time.textColor = [UIColor blackColor];
    lbl_serving_time.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_serving_time];
    
    UIImageView *img_clock = [[UIImageView alloc]init];
    img_clock.frame = CGRectMake(85,CGRectGetMaxY(lbl_serving_time.frame), 30, 30);
    [img_clock setImage:[UIImage imageNamed:@"img-clock@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_clock setUserInteractionEnabled:YES];
    [view_serch_for_food_later addSubview:img_clock];
    
    UITextField *txt_set_time = [[UITextField alloc] init];
    txt_set_time .frame = CGRectMake(CGRectGetMaxX( img_clock.frame)+5,CGRectGetMaxY(lbl_serving_time.frame), 100, 30);
    txt_set_time .borderStyle = UITextBorderStyleNone;
    txt_set_time .textColor = [UIColor grayColor];
    txt_set_time .font = [UIFont fontWithName:kFont size:13];
    txt_set_time .placeholder = @"10:45AM";
    [txt_set_time  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_set_time  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_set_time .leftView = padding1;
    txt_set_time .leftViewMode = UITextFieldViewModeAlways;
    txt_set_time .userInteractionEnabled=YES;
    txt_set_time .textAlignment = NSTextAlignmentLeft;
    txt_set_time .backgroundColor = [UIColor clearColor];
    txt_set_time .keyboardType = UIKeyboardTypeAlphabet;
    txt_set_time .delegate = self;
    [view_serch_for_food_later addSubview:txt_set_time ];
    
    UIButton *btn_set_time = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_set_time.frame = CGRectMake(85,CGRectGetMaxY(lbl_serving_time.frame),150,35);
    btn_set_time .backgroundColor = [UIColor clearColor];
    [btn_set_time addTarget:self action:@selector(btn_set_time_click:) forControlEvents:UIControlEventTouchUpInside];
    // [btn_set_date setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_set_time];
    
    UILabel *lbl_location = [[UILabel alloc]init];
    lbl_location.frame = CGRectMake(105,CGRectGetMaxY(img_clock.frame)+5, 150, 45);
    lbl_location.text = @"Location";
    lbl_location.font = [UIFont fontWithName:kFont size:18];
    lbl_location.textColor = [UIColor blackColor];
    lbl_location.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_location];
    
    UIImageView *img_pointer_location = [[UIImageView alloc]init];
    img_pointer_location.frame = CGRectMake(25,CGRectGetMaxY(lbl_location.frame), 30, 30);
    [img_pointer_location setImage:[UIImage imageNamed:@"location-pointer@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_pointer_location setUserInteractionEnabled:YES];
    [view_serch_for_food_later addSubview:img_pointer_location];
    
    UILabel *lbl_set_location = [[UILabel alloc]init];
    lbl_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+5,CGRectGetMaxY(lbl_location.frame), 250, 30);
    lbl_set_location.text = @"Smith Street, 77 Block (5km)";
    lbl_set_location.font = [UIFont fontWithName:kFont size:18];
    lbl_set_location.textColor = [UIColor blackColor];
    lbl_set_location.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_set_location];
    
    
    UIButton *btn_img_right = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_right.frame = CGRectMake(CGRectGetMidX(lbl_set_location.frame)+80,CGRectGetMaxY(lbl_location.frame),30,30);
    btn_img_right .backgroundColor = [UIColor clearColor];
    [btn_img_right addTarget:self action:@selector(btn_right_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_right setImage:[UIImage imageNamed:@"img-right@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_img_right];
    
    UIButton *btn_on_set_location = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_set_location.frame = CGRectMake(25,CGRectGetMaxY(lbl_location.frame),270,35);
    btn_on_set_location .backgroundColor = [UIColor clearColor];
    [btn_on_set_location addTarget:self action:@selector(btn_on_set_location_click:) forControlEvents:UIControlEventTouchUpInside];
    // [btn_set_date setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_on_set_location];
    
    UILabel *lbl_seving_type = [[UILabel alloc]init];
    lbl_seving_type.frame = CGRectMake(105,CGRectGetMaxY(lbl_set_location.frame)+5, 150, 45);
    lbl_seving_type.text = @"Serving Type";
    lbl_seving_type.font = [UIFont fontWithName:kFont size:18];
    lbl_seving_type.textColor = [UIColor blackColor];
    lbl_seving_type.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_seving_type];
    
    
    UIButton *btn_img_dine_in = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_dine_in.frame = CGRectMake(45,CGRectGetMaxY(lbl_seving_type.frame),50,50);
    btn_img_dine_in .backgroundColor = [UIColor clearColor];
    [btn_img_dine_in addTarget:self action:@selector(btn_img_dine_in_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_dine_in setImage:[UIImage imageNamed:@"img-dine@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_img_dine_in];
    
    
    UIButton *btn_img_take_out = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_take_out.frame = CGRectMake(CGRectGetMidX(btn_img_dine_in.frame)+65,CGRectGetMaxY(lbl_seving_type.frame),50,50);
    btn_img_take_out .backgroundColor = [UIColor clearColor];
    [btn_img_take_out addTarget:self action:@selector(btn_img_take_out_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_take_out setImage:[UIImage imageNamed:@"img-take-out1@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_img_take_out];
    
    
    UIImageView *img_red_tik = [[UIImageView alloc]init];
    img_red_tik.frame =  CGRectMake(CGRectGetMidX(btn_img_dine_in.frame)+135,CGRectGetMaxY(lbl_seving_type.frame)+15,25,25);
    [img_red_tik setImage:[UIImage imageNamed:@"icon-red-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik setUserInteractionEnabled:YES];
    [view_serch_for_food_later addSubview:img_red_tik];
    
    UIButton *btn_img_delivery = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_delivery.frame = CGRectMake(CGRectGetMidX(btn_img_take_out.frame)+65,CGRectGetMaxY(lbl_seving_type.frame),50,50);
    btn_img_delivery .backgroundColor = [UIColor clearColor];
    [btn_img_delivery addTarget:self action:@selector(btn_img_delivery_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_delivery setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_img_delivery];
    
    UILabel *lbl_keywords = [[UILabel alloc]init];
    lbl_keywords.frame = CGRectMake(130,CGRectGetMaxY(btn_img_take_out.frame)+5, 150, 45);
    lbl_keywords.text = @"Keywords";
    lbl_keywords.font = [UIFont fontWithName:kFont size:18];
    lbl_keywords.textColor = [UIColor blackColor];
    lbl_keywords.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_keywords];
    
    UIImageView *img_serch_bar = [[UIImageView alloc]init];
    img_serch_bar.frame = CGRectMake(25,CGRectGetMaxY(lbl_keywords.frame), 290, 30);
    [img_serch_bar setImage:[UIImage imageNamed:@"serch-bar@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_serch_bar setUserInteractionEnabled:YES];
    [view_serch_for_food_later addSubview:img_serch_bar];
    
    UITextField *txt_search = [[UITextField alloc] init];
    //  txt_search.frame = CGRectMake(10,-4, 200, 45);
    txt_search .borderStyle = UITextBorderStyleNone;
    txt_search .textColor = [UIColor grayColor];
    txt_search .font = [UIFont fontWithName:kFont size:15];
    txt_search .placeholder = @"Search...";
    [txt_search  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_search  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_search .leftView = padding3;
    txt_search .leftViewMode = UITextFieldViewModeAlways;
    txt_search .userInteractionEnabled=YES;
    txt_search .textAlignment = NSTextAlignmentLeft;
    txt_search .backgroundColor = [UIColor clearColor];
    txt_search .keyboardType = UIKeyboardTypeAlphabet;
    txt_search .delegate = self;
    [img_serch_bar addSubview:txt_search ];
    
    
    UIImageView *icon_search = [[UIImageView alloc]init];
    //   icon_search.frame = CGRectMake(255,8, 15, 15);
    [icon_search setImage:[UIImage imageNamed:@"serach1@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_search setUserInteractionEnabled:YES];
    [img_serch_bar addSubview:icon_search];
    
    UIButton *btn_on_search_bar = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_search_bar.frame = CGRectMake(240,0, 30, 30);
    btn_on_search_bar .backgroundColor = [UIColor clearColor];
    [btn_on_search_bar addTarget:self action:@selector(btn_on_search_bar_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [img_serch_bar   addSubview:btn_on_search_bar];
    
    
    UIButton *btn_img_more_filters = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_img_more_filters.frame = CGRectMake(30,CGRectGetMaxY(img_serch_bar.frame)+20, 260, 40);
    btn_img_more_filters .backgroundColor = [UIColor clearColor];
    [btn_img_more_filters addTarget:self action:@selector(btn_img_on_more_filters_click:) forControlEvents:UIControlEventTouchUpInside];
    [ btn_img_more_filters setImage:[UIImage imageNamed:@"more-services@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_img_more_filters];
    
#pragma mark Tableview
    
    img_table = [[UITableView alloc] init ];
    //   img_table.frame  = CGRectMake(15,CGRectGetMaxY(view_serch_for_food_later.frame)+5,WIDTH-30,370);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    img_table.backgroundColor = [UIColor clearColor];
    [scroll addSubview:img_table];
    
    if (IS_IPHONE_6Plus)
    {
        scroll.frame = CGRectMake(0, 46, WIDTH, HEIGHT+150);
        view_serch_for_food_later.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame)-50,WIDTH,HEIGHT+150);
        lbl_date.frame = CGRectMake(174,0, 150, 45);
        img_calender.frame = CGRectMake(130,CGRectGetMaxY(lbl_date.frame), 30, 30);
        txt_date .frame = CGRectMake(CGRectGetMaxX( img_calender.frame)+5,CGRectGetMaxY(lbl_date.frame), 100, 30);
        lbl_serving_time.frame = CGRectMake(153,CGRectGetMaxY(txt_date.frame)+5, 150, 45);
        img_clock.frame = CGRectMake(150,CGRectGetMaxY(lbl_serving_time.frame), 30, 30);
        txt_set_time .frame = CGRectMake(CGRectGetMaxX( img_clock.frame)+5,CGRectGetMaxY(lbl_serving_time.frame), 100, 30);
        btn_set_time.frame = CGRectMake(125,CGRectGetMaxY(lbl_serving_time.frame),150,35);
        lbl_location.frame = CGRectMake(170,CGRectGetMaxY(img_clock.frame)+5, 150, 45);
        img_pointer_location.frame = CGRectMake(70,CGRectGetMaxY(lbl_location.frame), 30, 30);
        //txt_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+20,CGRectGetMaxY(lbl_location.frame), 250, 30);
        lbl_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+5,CGRectGetMaxY(lbl_location.frame), 250, 30);
        btn_img_right.frame = CGRectMake(CGRectGetMidX(lbl_set_location.frame)+130,CGRectGetMaxY(lbl_location.frame),30,30);
        btn_on_set_location.frame = CGRectMake(25,CGRectGetMaxY(lbl_location.frame),270,35);
        lbl_seving_type.frame = CGRectMake(153,CGRectGetMaxY(lbl_set_location.frame)+5, 150, 45);
        btn_img_dine_in.frame = CGRectMake(95,CGRectGetMaxY(lbl_seving_type.frame),50,50);
        btn_img_take_out.frame = CGRectMake(CGRectGetMidX(btn_img_dine_in.frame)+65,CGRectGetMaxY(lbl_seving_type.frame),50,50);
        img_red_tik.frame =  CGRectMake(CGRectGetMidX(btn_img_dine_in.frame)+85,CGRectGetMaxY(lbl_seving_type.frame)+15,25,25);
        btn_img_delivery.frame = CGRectMake(CGRectGetMidX(btn_img_take_out.frame)+65,CGRectGetMaxY(lbl_seving_type.frame),50,50);
        lbl_keywords.frame = CGRectMake(170,CGRectGetMaxY(btn_img_take_out.frame)+5, 150, 45);
        img_serch_bar.frame = CGRectMake(30,CGRectGetMaxY(lbl_keywords.frame), WIDTH-60, 30);
        txt_search.frame = CGRectMake(0,-8, 200, 45);
        icon_search.frame = CGRectMake(330,8, 15, 15);
        btn_on_search_bar.frame = CGRectMake(330,0, 30, 30);
        btn_img_more_filters.frame = CGRectMake(20,CGRectGetMaxY(img_serch_bar.frame)+20, WIDTH-40, 50);
        img_table.frame  = CGRectMake(5,CGRectGetMaxY(btn_img_more_filters.frame),WIDTH-10,370);
    }
    else if (IS_IPHONE_6)
    {
        scroll.frame = CGRectMake(0, 46, WIDTH, HEIGHT+150);
        view_serch_for_food_later.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame)-50,WIDTH,HEIGHT+150);
        lbl_date.frame = CGRectMake(164,0, 150, 45);
        img_calender.frame = CGRectMake(120,CGRectGetMaxY(lbl_date.frame), 30, 30);
        txt_date .frame = CGRectMake(CGRectGetMaxX( img_calender.frame)+5,CGRectGetMaxY(lbl_date.frame), 100, 30);
        lbl_serving_time.frame = CGRectMake(133,CGRectGetMaxY(txt_date.frame)+5, 150, 45);
        img_clock.frame = CGRectMake(133,CGRectGetMaxY(lbl_serving_time.frame), 30, 30);
        txt_set_time .frame = CGRectMake(CGRectGetMaxX( img_clock.frame)+5,CGRectGetMaxY(lbl_serving_time.frame), 100, 30);
        btn_set_time.frame = CGRectMake(125,CGRectGetMaxY(lbl_serving_time.frame),150,35);
        lbl_location.frame = CGRectMake(145,CGRectGetMaxY(img_clock.frame)+5, 150, 45);
        img_pointer_location.frame = CGRectMake(30,CGRectGetMaxY(lbl_location.frame), 30, 30);
        //txt_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+20,CGRectGetMaxY(lbl_location.frame), 250, 30);
        lbl_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+15,CGRectGetMaxY(lbl_location.frame), 250, 30);
        
        btn_img_right.frame = CGRectMake(CGRectGetMidX( lbl_set_location.frame)+125,CGRectGetMaxY(lbl_location.frame),30,30);
        btn_on_set_location.frame = CGRectMake(25,CGRectGetMaxY(lbl_location.frame),270,35);
        lbl_seving_type.frame = CGRectMake(133,CGRectGetMaxY( lbl_set_location.frame)+5, 150, 45);
        btn_img_dine_in.frame = CGRectMake(55,CGRectGetMaxY(lbl_seving_type.frame),50,50);
        btn_img_take_out.frame = CGRectMake(CGRectGetMidX(btn_img_dine_in.frame)+85,CGRectGetMaxY(lbl_seving_type.frame),50,50);
        img_red_tik.frame =  CGRectMake(CGRectGetMidX(btn_img_dine_in.frame)+110,CGRectGetMaxY(lbl_seving_type.frame)+18,25,25);
        btn_img_delivery.frame = CGRectMake(CGRectGetMidX(btn_img_take_out.frame)+75,CGRectGetMaxY(lbl_seving_type.frame),50,50);
        lbl_keywords.frame = CGRectMake(155,CGRectGetMaxY(btn_img_take_out.frame)+5, 150, 45);
        img_serch_bar.frame = CGRectMake(30,CGRectGetMaxY(lbl_keywords.frame), WIDTH-60, 30);
        txt_search.frame = CGRectMake(0,-8, 200, 45);
        icon_search.frame = CGRectMake(290,8, 15, 15);
        btn_on_search_bar.frame = CGRectMake(290,0, 30, 30);
        btn_img_more_filters.frame = CGRectMake(30,CGRectGetMaxY(img_serch_bar.frame)+20, WIDTH-60, 50);
        img_table.frame  = CGRectMake(5,CGRectGetMaxY(btn_img_more_filters.frame),WIDTH-10,370);
    }
    else
    {
        scroll.frame = CGRectMake(0, 46, WIDTH, HEIGHT+150);
        view_serch_for_food_later.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame)-45,WIDTH,HEIGHT+350);
        lbl_date.frame = CGRectMake(130,0, 150, 45);
        img_calender.frame = CGRectMake(85,CGRectGetMaxY(lbl_date.frame), 30, 30);
        txt_date .frame = CGRectMake(CGRectGetMaxX( img_calender.frame)+5,CGRectGetMaxY(lbl_date.frame), 100, 30);
        lbl_serving_time.frame = CGRectMake(105,CGRectGetMaxY(txt_date.frame)+5, 150, 45);
        img_clock.frame = CGRectMake(85,CGRectGetMaxY(lbl_serving_time.frame), 30, 30);
        txt_set_time .frame = CGRectMake(CGRectGetMaxX( img_clock.frame)+5,CGRectGetMaxY(lbl_serving_time.frame), 100, 30);
        btn_set_time.frame = CGRectMake(85,CGRectGetMaxY(lbl_serving_time.frame),150,35);
        lbl_location.frame = CGRectMake(105,CGRectGetMaxY(img_clock.frame)+5, 150, 45);
        img_pointer_location.frame = CGRectMake(25,CGRectGetMaxY(lbl_location.frame), 30, 30);
        //  txt_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+5,CGRectGetMaxY(lbl_location.frame), 250, 30);
        lbl_set_location.font = [UIFont fontWithName:kFont size:15];
        lbl_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+15,CGRectGetMaxY(lbl_location.frame), 250, 30);
        
        btn_img_right.frame = CGRectMake(CGRectGetMidX( lbl_set_location.frame)+90,CGRectGetMaxY(lbl_location.frame),30,30);
        btn_on_set_location.frame = CGRectMake(25,CGRectGetMaxY(lbl_location.frame),270,35);
        lbl_seving_type.frame = CGRectMake(105,CGRectGetMaxY( lbl_set_location.frame)+5, 150, 45);
        btn_img_dine_in.frame = CGRectMake(45,CGRectGetMaxY(lbl_seving_type.frame),50,50);
        btn_img_take_out.frame = CGRectMake(CGRectGetMidX(btn_img_dine_in.frame)+65,CGRectGetMaxY(lbl_seving_type.frame),50,50);
        img_red_tik.frame =  CGRectMake(CGRectGetMidX(btn_img_dine_in.frame)+90,CGRectGetMaxY(lbl_seving_type.frame)+18,25,25);
        btn_img_delivery.frame = CGRectMake(CGRectGetMidX(btn_img_take_out.frame)+65,CGRectGetMaxY(lbl_seving_type.frame),50,50);
        lbl_keywords.frame = CGRectMake(130,CGRectGetMaxY(btn_img_take_out.frame)+5, 150, 45);
        img_serch_bar.frame = CGRectMake(25,CGRectGetMaxY(lbl_keywords.frame), 290, 30);
        txt_search.frame = CGRectMake(10,-8, 200, 45);
        icon_search.frame = CGRectMake(255,8, 15, 15);
        btn_on_search_bar.frame = CGRectMake(255,0, 30, 30);
        btn_img_more_filters.frame = CGRectMake(30,CGRectGetMaxY(img_serch_bar.frame)+20, 260, 40);
        img_table.frame  = CGRectMake(5,CGRectGetMaxY( btn_img_more_filters.frame)+5,WIDTH-10,370);
    }
    
    [scroll setContentSize:CGSizeMake(0,CGRectGetMaxY(img_table.frame)+150)];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ary_displaynames count];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 160;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    //  img_cellBackGnd.frame =  CGRectMake(0,15, 300, 160);
    img_cellBackGnd.image=[UIImage imageNamed:@"bg@2x.png"];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [cell.contentView addSubview:img_cellBackGnd];
    
    
    
    UIImageView *img_dish = [[UIImageView alloc] init];
    //  img_dish.frame = CGRectMake(0,7, 90,  95 );
    [img_dish setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_img objectAtIndex:indexPath.row]]]];
    [img_cellBackGnd addSubview:img_dish];
    
    UIButton *btn_favorite = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_favorite.frame = CGRectMake(5,58, 35, 35);
    //icon_cow .backgroundColor = [UIColor clearColor];
    [btn_favorite addTarget:self action:@selector(btn_favorite_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_favorite setImage:[UIImage imageNamed:@"icon-favorite2@2x.png"] forState:UIControlStateNormal];
    [img_dish   addSubview:btn_favorite];
    
    
    UIButton *btn_number_three = [UIButton buttonWithType:UIButtonTypeCustom];
    //  btn_number_three.frame = CGRectMake(58,-5, 35, 35);
    //icon_cow .backgroundColor = [UIColor clearColor];
    [btn_number_three addTarget:self action:@selector(btn_number_three_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_number_three setImage:[UIImage imageNamed:@"icon-red3@2x.png"] forState:UIControlStateNormal];
    [img_dish   addSubview:btn_number_three];
    
    
    UIButton *icon_delete = [UIButton buttonWithType:UIButtonTypeCustom];
    //  icon_delete.frame = CGRectMake(250,20, 40, 40);
    //icon_cow .backgroundColor = [UIColor clearColor];
    [icon_delete addTarget:self action:@selector(btn_delete_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_delete setImage:[UIImage imageNamed:@"img-del@2x.png"] forState:UIControlStateNormal];
    [cell.contentView   addSubview:icon_delete];
    
    UILabel *dish_name = [[UILabel alloc]init];
    //  dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
    dish_name.text = [NSString stringWithFormat:@"%@",[ary_displaynames objectAtIndex:indexPath.row]];
    dish_name.font = [UIFont fontWithName:kFontBold size:15];
    dish_name.textColor = [UIColor blackColor];
    dish_name.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:dish_name];
    
    
    UIImageView *icon_location = [[UIImageView alloc] init];
    //   icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
    [icon_location setImage:[UIImage imageNamed:@"icon-location1@2x.png"]];
    [img_cellBackGnd  addSubview:icon_location];
    
    UILabel *meters = [[UILabel alloc]init];
    //   meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
    meters.text = @"5km";
    meters.font = [UIFont fontWithName:kFont size:15];
    meters.textColor = [UIColor blackColor];
    meters.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:meters];
    
    UIButton *icon_server = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
    //icon_server .backgroundColor = [UIColor clearColor];
    [icon_server  addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_server setImage:[UIImage imageNamed:@"icon-serving@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_server];
    
    UIButton *icon_halal = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
    //icon_halal .backgroundColor = [UIColor clearColor];
    [icon_halal addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_halal setImage:[UIImage imageNamed:@"icon-halal@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_halal];
    
    UIImageView *img_non_veg = [[UIImageView alloc] init];
    //   img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
    [img_non_veg setImage:[UIImage imageNamed:@"non veg bt@2x.png"]];
    [img_cellBackGnd addSubview:img_non_veg];
    
    UIButton *icon_cow = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
    //icon_cow .backgroundColor = [UIColor clearColor];
    [icon_cow addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_cow setImage:[UIImage imageNamed:@"icon-cow@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_cow];
    
    UIButton *icon_fronce = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
    //icon_cow .backgroundColor = [UIColor clearColor];
    [icon_fronce addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_fronce setImage:[UIImage imageNamed:@"icon-fronce@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_fronce];
    
    UILabel *doller_rate = [[UILabel alloc]init];
    //    doller_rate.frame = CGRectMake(CGRectGetMaxX(icon_fronce.frame)+25,85,200, 15);
    doller_rate.text = @"$14.90";
    doller_rate.font = [UIFont fontWithName:kFontBold size:15];
    doller_rate.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    doller_rate.backgroundColor = [UIColor clearColor];
    
    [img_cellBackGnd addSubview:doller_rate];
    
    
    
    
    UIImageView *img_line = [[UIImageView alloc] init];
    //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
    [img_line setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_cellBackGnd addSubview:img_line];
    
    
    UIButton *icon_take = [UIButton buttonWithType:UIButtonTypeCustom];
    //    icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
    //icon_take .backgroundColor = [UIColor clearColor];
    [icon_take addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_take setImage:[UIImage imageNamed:@"icon-take@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_take];
    
    UIButton *icon_deliver = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //icon_deliver .backgroundColor = [UIColor clearColor];
    [icon_deliver addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_deliver setImage:[UIImage imageNamed:@"icon-deliver.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_deliver];
    
    
    
    UIButton *img_btn_seving_Now = [UIButton buttonWithType:UIButtonTypeCustom];
    //   img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //img_btn_deliver_Now .backgroundColor = [UIColor clearColor];
    [img_btn_seving_Now addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [img_btn_seving_Now setImage:[UIImage imageNamed:@"icon_now@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:img_btn_seving_Now];
    
    
    UIButton *img_btn_chef_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    //   img_btn_chef_menu.frame = CGRectMake(165,CGRectGetMaxY(img_line.frame)-5,  60, 60);
    //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
    [img_btn_chef_menu addTarget:self action:@selector(btn_chef_menu_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_btn_chef_menu setImage:[UIImage imageNamed:@"icon-chef-menu@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:img_btn_chef_menu];
    
    UIButton *icon_thumb = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_thumb.frame = CGRectMake(235,CGRectGetMaxY(img_line.frame)+8, 20, 20);
    //icon_thumb .backgroundColor = [UIColor clearColor];
    [icon_thumb addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_thumb setImage:[UIImage imageNamed:@"icon-thumb@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_thumb];
    
    UILabel *likes = [[UILabel alloc]init];
    //    likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,125,200, 10);
    likes.text = @"87.4%";
    likes.font = [UIFont fontWithName:kFont size:10];
    likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    likes.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:likes];
    
    if (IS_IPHONE_6Plus)
    {
        img_cellBackGnd.frame =  CGRectMake(05,15, WIDTH-10, 160);
        img_dish.frame = CGRectMake(5,7, 90,  95 );
        btn_favorite.frame = CGRectMake(5,58, 35, 35);
        btn_number_three.frame = CGRectMake(58,-5, 35, 35);
        icon_delete.frame = CGRectMake(WIDTH-60,20, 40, 40);
        dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
        icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
        icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
        img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
        icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
        icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
        doller_rate.frame = CGRectMake(CGRectGetMaxX(icon_fronce.frame)+125,85,200, 15);
        img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
        icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_seving_Now.frame = CGRectMake(110,CGRectGetMaxY(img_line.frame)+5,  100, 30);
        img_btn_chef_menu.frame = CGRectMake(165,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        
        icon_thumb.frame = CGRectMake(WIDTH-80,CGRectGetMaxY(img_line.frame)+8, 20, 20);
        likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,130,200, 10);
        
    }
    else if (IS_IPHONE_6)
    {
        img_cellBackGnd.frame =  CGRectMake(05,15, WIDTH-10, 160);
        img_dish.frame = CGRectMake(5,7, 90,  95 );
        btn_favorite.frame = CGRectMake(5,58, 35, 35);
        btn_number_three.frame = CGRectMake(58,-5, 35, 35);
        icon_delete.frame = CGRectMake(WIDTH-60,20, 40, 40);
        dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
        icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
        icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
        img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
        icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
        icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
        doller_rate.frame = CGRectMake(CGRectGetMaxX(icon_fronce.frame)+85,85,200, 15);
        img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
        icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_seving_Now.frame = CGRectMake(110,CGRectGetMaxY(img_line.frame)+5,  100, 30);
        img_btn_chef_menu.frame = CGRectMake(165,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        
        icon_thumb.frame = CGRectMake(WIDTH-80,CGRectGetMaxY(img_line.frame)+8, 20, 20);
        likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,130,200, 10);
    }
    else
    {
        img_cellBackGnd.frame =  CGRectMake(5,15, WIDTH-10, 160);
        img_dish.frame = CGRectMake(0,7, 90,  95 );
        btn_favorite.frame = CGRectMake(5,58, 35, 35);
        btn_number_three.frame = CGRectMake(58,-5, 35, 35);
        icon_delete.frame = CGRectMake(260,20, 40, 40);
        dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
        icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
        icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
        img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
        icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
        icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
        doller_rate.frame = CGRectMake(CGRectGetMaxX(icon_fronce.frame)+40,85,200, 15);
        img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
        icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_seving_Now.frame = CGRectMake(70,CGRectGetMaxY(img_line.frame)+5,  100, 30);
        img_btn_chef_menu.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)-10, 60, 60);
        
        icon_thumb.frame = CGRectMake(245,CGRectGetMaxY(img_line.frame)+8, 20, 20);
        likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,125,200, 10);
        
    }
    
    
    return cell;
    
}
#pragma mark Click Events
-(void)btn_menu_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click");
    [self.navigationController popViewControllerAnimated:NO];
    
    
}
-(void)btn_on_food_later_label_click:(UIButton *)sender
{
    NSLog(@"btn_on_food_later_label_click");
    
}
-(void)btn_drop_down:(UIButton *)sender
{
    NSLog(@"btn_on_food_later_label_click");
    
}
-(void)btn_set_date_click:(UIButton *)sender
{
    NSLog(@"img_fb_click Btn Click");
    
}
-(void)btn_set_time_click:(UIButton *)sender
{
    NSLog(@"btn_set_time_click:");
    
}
-(void)btn_right_click:(UIButton *)sender
{
    NSLog(@"btn_right_click:");
    
}
-(void)btn_on_set_location_click:(UIButton *)sender
{
    NSLog(@"btn_on_set_location_click::");
    //LocationVC*vc = [[LocationVC alloc]init];
    //[self.navigationController pushViewController:vc animated:NO];
    
    
}
-(void)btn_img_dine_in_click:(UIButton *)sender
{
    NSLog(@"btn_img_dine_in_click:");
    
}
-(void)btn_img_take_out_click:(UIButton *)sender
{
    NSLog(@"btn_img_take_out_click:");
    
}
-(void)btn_img_delivery_click:(UIButton *)sender
{
    NSLog(@"btn_img_delivery_click:");
    
}
-(void)btn_on_search_bar_click:(UIButton *)sender
{
    NSLog(@"btn_on_search_bar_click:");
    
}
-(void)btn_img_on_more_filters_click:(UIButton *)sender
{
    NSLog(@"btn_img_on_more_filters_click:");
       
}


#pragma mark Click Events for table views

-(void)btn_delete_click:(UIButton *)sender
{
    NSLog(@"btn_delete_click:");
    
}
-(void)btn_favorite_click:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
    
}
-(void)btn_number_three_click:(UIButton *)sender
{
    NSLog(@"btn_number_three_click:");
}


-(void)icon_Food_Later_BtnClick:(UIButton *)sender
{
    NSLog(@"icon_Food_Later_BtnClick:");
    
}
-(void)icon_halal_BtnClick:(UIButton *)sender
{
    NSLog(@"icon_halal_BtnClick:");
    
}
-(void)icon_server_BtnClick:(UIButton *)sender
{
    NSLog(@"icon_server_BtnClick:");
    
}
-(void)icon_cow_BtnClick:(UIButton *)sender
{
    NSLog(@"icon_cow_BtnClick:");
    
}
-(void)icon_fronce_BtnClick:(UIButton *)sender
{
    NSLog(@"btn_img_on_more_filters_click:");
    
}
-(void)btn_serving_now_click:(UIButton *)sender
{
    NSLog(@"btn_serving_now_click:");
}
-(void)btn_chef_menu_click:(UIButton *)sender
{
    NSLog(@"btn_chef_menu_click:");
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}
#pragma return action


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
