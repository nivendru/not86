//
//  UserLoginVC.m
//  Not86
//
//  Created by Admin on 29/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "UserLoginVC.h"

#import "UserLoginVC.h"
//#import "HomeVC.h"
#import "AFNetworking.h"
#import "AppDelegate.h"

//#import "UserForgotPasswordVC.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"




@interface UserLoginVC ()<UITextFieldDelegate>
{
    UIImageView *img_header;
    UIImageView *img_background;
    CGFloat	animatedDistance;
    
    AppDelegate *delegate;
    UIView *view_popup;
    UIImageView *img_alertpop;
    
    UITextField *txt_user_Name;
    UITextField *txt_password;
}


@end

@implementation UserLoginVC

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
   [self integrateBodyDesign];
    
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}


-(void)integrateHeader
{
    img_header=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 45)];
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,18,18);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back addTarget:self action:@selector(btn_back_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_back];
    
    UILabel *lbl_Login = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(icon_back.frame)+30,0,200, 45)];
    lbl_Login.text = @"Login";
    lbl_Login.font = [UIFont fontWithName:kFont size:20];
    lbl_Login.textColor = [UIColor whiteColor];
    lbl_Login.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_Login];
    
    UIImageView *img_circle = [[UIImageView alloc]init];
    img_circle.frame =CGRectMake(WIDTH-40, 8, 30, 30);
    [img_circle setImage:[UIImage imageNamed:@"img_user_icon@2x.png"]];
    //img_circle.backgroundColor = [UIColor whiteColor];
    [img_circle setUserInteractionEnabled:YES];
    [img_header addSubview:img_circle];
    
    
}
-(void)integrateBodyDesign
{
    img_background = [[UIImageView alloc]init];
    // img_background.frame = CGRectMake(30, CGRectGetMaxY(img_header.frame)+30, WIDTH-55, 85);
    [img_background setImage:[UIImage imageNamed:@"img_logo_name@2x.png"]];
    //    img_eat.backgroundColor = [UIColor redColor];
    [img_background setUserInteractionEnabled:YES];
    [self.view addSubview:img_background];
    
    
    
    txt_user_Name = [[UITextField alloc] init];
    //txt_user_Name .frame=CGRectMake(10, CGRectGetMaxY( img_background.frame)-20, WIDTH, 30);
    txt_user_Name .borderStyle = UITextBorderStyleNone;
    txt_user_Name .textColor = [UIColor grayColor];
    txt_user_Name .font = [UIFont fontWithName:kFont size:13];
    txt_user_Name .placeholder = @"Username";
    [txt_user_Name  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_user_Name  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_user_Name .leftView = padding1;
    txt_user_Name .leftViewMode = UITextFieldViewModeAlways;
    txt_user_Name .userInteractionEnabled=YES;
    txt_user_Name .textAlignment = NSTextAlignmentLeft;
    txt_user_Name .backgroundColor = [UIColor clearColor];
    txt_user_Name .keyboardType = UIKeyboardTypeAlphabet;
    txt_user_Name .delegate = self;
    [self.view addSubview:txt_user_Name ];
    
    UIImageView * img_line = [[UIImageView alloc]init];
    // img_line .frame = CGRectMake(10, CGRectGetMaxY(  txt_user_Name.frame)+5, WIDTH-60, 0.5);
    [img_line setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_line setUserInteractionEnabled:YES];
    [img_background addSubview:img_line];
    
    txt_password = [[UITextField alloc] init];
    //  txt_password .frame=CGRectMake(10, CGRectGetMaxY( img_line.frame)+10, WIDTH, 30);
    txt_password .borderStyle = UITextBorderStyleNone;
    txt_password .textColor = [UIColor grayColor];
    txt_password .font = [UIFont fontWithName:kFont size:13];
    txt_password .placeholder = @"Password";
    [txt_password  setValue:[UIFont fontWithName:kFont size:16]forKeyPath:@"_placeholderLabel.font"];
    [txt_password  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_password .leftView = padding2;
    txt_password .leftViewMode = UITextFieldViewModeAlways;
    txt_password .userInteractionEnabled=YES;
    txt_password .textAlignment = NSTextAlignmentLeft;
    txt_password .backgroundColor = [UIColor clearColor];
    txt_password .keyboardType = UIKeyboardTypeAlphabet;
    txt_password .delegate = self;
    [self.view addSubview:txt_password ];
    
    UIImageView * img_line1 = [[UIImageView alloc]init];
    // img_line1 .frame = CGRectMake(10, CGRectGetMaxY(  txt_password.frame)+5, WIDTH-60, 0.5);
    [img_line1 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_line1 setUserInteractionEnabled:YES];
    [img_background addSubview:img_line1];
    
    
    UIButton *img_chek_box = [UIButton buttonWithType:UIButtonTypeCustom];
    // img_chek_box.frame = CGRectMake(10,CGRectGetMaxY(  img_line1.frame)+15,20,20);
    img_chek_box .backgroundColor = [UIColor clearColor];
    [img_chek_box addTarget:self action:@selector(btn_check_box_click:) forControlEvents:UIControlEventTouchUpInside];
    img_chek_box.userInteractionEnabled = YES;
    [img_chek_box setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
    [img_background   addSubview:img_chek_box];
    
    //    UITextField *txt_remember_me = [[UITextField alloc] init];
    //   // txt_remember_me .frame=CGRectMake( CGRectGetMaxX( img_chek_box.frame), CGRectGetMaxY( img_line1.frame)+15, 200, 20);
    //    txt_remember_me .borderStyle = UITextBorderStyleNone;
    //    txt_remember_me .textColor = [UIColor grayColor];
    //    txt_remember_me .font = [UIFont fontWithName:@"Arial" size:13];
    //    txt_remember_me .placeholder = @"Remember me";
    //    [txt_remember_me  setValue:[UIFont fontWithName:@"Arial" size: 12] forKeyPath:@"_placeholderLabel.font"];
    //    [txt_remember_me  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    //    UIView *padding3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    //    txt_remember_me .leftView = padding3;
    //    txt_remember_me .leftViewMode = UITextFieldViewModeAlways;
    //    txt_remember_me .userInteractionEnabled=YES;
    //    txt_remember_me .textAlignment = NSTextAlignmentLeft;
    //    txt_remember_me .backgroundColor = [UIColor clearColor];
    //    txt_remember_me .keyboardType = UIKeyboardTypeAlphabet;
    //    txt_remember_me .delegate = self;
    //    [self.view addSubview:txt_remember_me ];
    
    UILabel *lbl_remember_me = [[UILabel alloc]init ];
    //   lbl_remember_me.frame =  CGRectMake( CGRectGetMaxX( img_chek_box.frame)+65, CGRectGetMaxY( img_line1.frame)+270, 100, 20);
    lbl_remember_me.text = @"Remember me";
    lbl_remember_me.font = [UIFont fontWithName:kFont size:13];
    lbl_remember_me.textColor = [UIColor lightGrayColor];
    lbl_remember_me.backgroundColor = [UIColor clearColor];
    [self.view addSubview:lbl_remember_me];
    
    UIButton *btn_on_remember_me = [UIButton buttonWithType:UIButtonTypeCustom];
    // btn_on_remember_me.frame = CGRectMake( CGRectGetMaxX( img_chek_box.frame)+5, CGRectGetMaxY( img_line1.frame)+85, 130, 20);
    btn_on_remember_me .backgroundColor = [UIColor clearColor];
    [btn_on_remember_me addTarget:self action:@selector(btn_on_remember_me_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_remember_me setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [self.view   addSubview:btn_on_remember_me];
    
    
    UILabel *lbl_forgot_password = [[UILabel alloc]init ];
    //    lbl_forgot_password.frame =  CGRectMake( CGRectGetMaxX( img_chek_box.frame)+70, CGRectGetMaxY( img_line1.frame)+270, 100, 20);
    lbl_forgot_password.text = @"Forgot password?";
    lbl_forgot_password.font = [UIFont fontWithName:kFont size:13];
    lbl_forgot_password.textColor = [UIColor lightGrayColor];
    lbl_forgot_password.backgroundColor = [UIColor clearColor];
    [self.view addSubview:lbl_forgot_password];
    
    UIButton *btn_on_forgot_pass = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_on_forgot_pass.frame = CGRectMake( CGRectGetMaxX( img_chek_box.frame)+150, CGRectGetMaxY( img_line1.frame)+85, 110, 20);
    btn_on_forgot_pass .backgroundColor = [UIColor clearColor];
    [btn_on_forgot_pass addTarget:self action:@selector(btn_on_forgot_pass_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_remember_me setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [self.view   addSubview:btn_on_forgot_pass];
    
    
    
    UIButton *img_login = [UIButton buttonWithType:UIButtonTypeCustom];
    // img_login.frame = CGRectMake(10,CGRectGetMaxY(  img_chek_box.frame)+20,WIDTH-55,55);
    img_login .backgroundColor = [UIColor clearColor];
    img_login.userInteractionEnabled = YES;
    [img_login setImage:[UIImage imageNamed:@"login-bg-text@2x.png"] forState:UIControlStateNormal];
    [img_login addTarget:self action:@selector(btn_login_click:) forControlEvents:UIControlEventTouchUpInside];
    [self.view  addSubview:img_login];
    
    if (IS_IPHONE_6Plus)
    {
        img_background.frame = CGRectMake(40, CGRectGetMaxY(img_header.frame)+30, WIDTH-130, 85);
        txt_user_Name .frame=CGRectMake(25, CGRectGetMaxY( img_background.frame)+20, WIDTH, 50);
        img_line .frame = CGRectMake(-10, CGRectGetMaxY(  txt_user_Name.frame)-85, WIDTH-70, 0.5);
        txt_password .frame=CGRectMake(25, CGRectGetMaxY( img_line.frame)+95, WIDTH, 55);
        img_line1 .frame = CGRectMake(-10, CGRectGetMaxY(  txt_password.frame)-85, WIDTH-70, 0.5);
        img_chek_box.frame = CGRectMake(-10,CGRectGetMaxY(  img_line1.frame)+25,20,20);
        
        lbl_remember_me.frame =  CGRectMake( CGRectGetMaxX( img_chek_box.frame)+48, CGRectGetMaxY( img_line1.frame)+100, 200, 20);
        btn_on_remember_me.frame = CGRectMake(CGRectGetMaxX( img_chek_box.frame)+5, CGRectGetMaxY( img_line1.frame)+100, 130, 20);
        
        lbl_forgot_password.frame =  CGRectMake(CGRectGetMaxX( img_chek_box.frame)+259, CGRectGetMaxY( img_line1.frame)+100, 200, 20);
        btn_on_forgot_pass.frame = CGRectMake(CGRectGetMaxX( img_chek_box.frame)+255, CGRectGetMaxY( img_line1.frame)+100, 110, 20);
        img_login.frame = CGRectMake(-10,CGRectGetMaxY(img_chek_box.frame)+110,WIDTH,55);
        
    }
    else if (IS_IPHONE_6)
    {
        img_background.frame = CGRectMake(30, CGRectGetMaxY(img_header.frame)+30, WIDTH-85, 85);
        txt_user_Name .frame=CGRectMake(25, CGRectGetMaxY( img_background.frame)+25, WIDTH, 50);
        img_line .frame = CGRectMake(0, CGRectGetMaxY(  txt_user_Name.frame)-85, WIDTH-60, 0.5);
        txt_password .frame=CGRectMake(25, CGRectGetMaxY( img_line.frame)+90, WIDTH, 55);
        img_line1 .frame = CGRectMake(0, CGRectGetMaxY(  txt_password.frame)-85, WIDTH-60, 0.5);
        img_chek_box.frame = CGRectMake(0,CGRectGetMaxY(  img_line1.frame)+29,20,20);
        
        lbl_remember_me.frame =  CGRectMake( CGRectGetMaxX( img_chek_box.frame)+38, CGRectGetMaxY( img_line1.frame)+105, 200, 20);
        btn_on_remember_me.frame = CGRectMake(CGRectGetMaxX( img_chek_box.frame)+5, CGRectGetMaxY( img_line1.frame)+94, 130, 20);
        
        lbl_forgot_password.frame =  CGRectMake(CGRectGetMaxX( img_chek_box.frame)+207, CGRectGetMaxY( img_line1.frame)+105, 200, 20);
        btn_on_forgot_pass.frame = CGRectMake(CGRectGetMaxX( img_chek_box.frame)+207, CGRectGetMaxY( img_line1.frame)+95, 110, 20);
        img_login.frame = CGRectMake(25,CGRectGetMaxY(img_chek_box.frame)+110,WIDTH-50,55);
        
        lbl_remember_me.font = [UIFont fontWithName:kFont size:14];
        lbl_forgot_password.font = [UIFont fontWithName:kFont size:14];
    }
    else
    {
        img_background.frame = CGRectMake(30, CGRectGetMaxY(img_header.frame)+30, WIDTH-75,IS_IPHONE_5?75:70);
        txt_user_Name .frame=CGRectMake(25,IS_IPHONE_5?CGRectGetMaxY( img_background.frame)+20:CGRectGetMaxY( img_background.frame)+20, WIDTH, 50);
        img_line .frame = CGRectMake(0, CGRectGetMaxY(  txt_user_Name.frame)-85, WIDTH-60, 0.5);
        txt_password .frame=CGRectMake(25, CGRectGetMaxY( img_line.frame)+80, WIDTH, 55);
        img_line1 .frame = CGRectMake(0, CGRectGetMaxY(  txt_password.frame)-85, WIDTH-60, 0.5);
        img_chek_box.frame = CGRectMake(0,CGRectGetMaxY(  img_line1.frame)+25,20,20);
        
        lbl_remember_me.frame =  CGRectMake( CGRectGetMaxX( img_chek_box.frame)+38, CGRectGetMaxY( img_line1.frame)+100, 200, 20);
        btn_on_remember_me.frame = CGRectMake(CGRectGetMaxX( img_chek_box.frame)+5, CGRectGetMaxY( img_line1.frame)+104, 130, 20);
        
        lbl_forgot_password.frame =  CGRectMake(CGRectGetMaxX( img_chek_box.frame)+160, CGRectGetMaxY( img_line1.frame)+100, 200, 20);
        btn_on_forgot_pass.frame = CGRectMake(CGRectGetMaxX( img_chek_box.frame)+160, CGRectGetMaxY( img_line1.frame)+104, 110, 20);
        img_login.frame = CGRectMake(25,CGRectGetMaxY(img_chek_box.frame)+110,WIDTH-50,55);
        lbl_remember_me.font = [UIFont fontWithName:kFont size:13];
        lbl_forgot_password.font = [UIFont fontWithName:kFont size:13];
        
    }
    
} 
#pragma mark Click Events

-(void)btn_back_click:(UIButton *)sender
{
    NSLog(@"btn_back_click:");
    [self.navigationController popViewControllerAnimated:NO];
    
    
}
-(void)btn_on_remember_me_click:(UIButton *)sender
{
    NSLog(@"btn_on_remember_me_click:");
    
}
-(void)btn_on_forgot_pass_click:(UIButton *)sender
{
    NSLog(@"btn_on_remember_me_click:");
    
//    UserForgotPasswordVC *forgetpassword=[[UserForgotPasswordVC alloc]init];
//    [self.navigationController pushViewController:forgetpassword animated:YES];
    
    
    
    NSLog(@"click_BtnForgotPass");
   // [self AFForgotPass];
    
    
}
-(void)btn_check_box_click:(UIButton *)sender
{
    NSLog(@"btn_check_box_click:");
    
}
-(void)btn_login_click:(UIButton *)sender
{
    NSLog(@"btn_login_click:");
    //    HomeVC*vc = [[HomeVC alloc]init];
    //    [self.navigationController pushViewController:vc animated:NO];
    
    
    
    if ([txt_user_Name.text isEqualToString:@""])
    {
        
        // [self popup_Alertview:@"Please enter Username"];
        
    }
    else if ([txt_password.text isEqualToString:@""])
    {
        //[self popup_Alertview:@"Please enter Password"];
    }
    
    else if ([txt_password.text length]<6)
    {
        txt_password.text=@"";
        // [self popup_Alertview:@"Password should be minimum of 8 characters"];
    }
    
    else
    {
//[self AFUserSignUp];
    }
    
}

-(void)popupForgetemail


{
    view_popup = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,IS_IPHONE_5 ? 568 :480)];
    view_popup.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.1];
    view_popup.userInteractionEnabled=TRUE;
    [self.view addSubview:view_popup];
    
    
    img_alertpop =[[UIImageView alloc] init];
    [img_alertpop setImage: [UIImage imageNamed:@"img_viewvehicalBackGnd@2x.png"]];
    img_alertpop.frame = CGRectMake(20,(IS_IPHONE_5?250:200 ),280,227);
    img_alertpop.layer.cornerRadius =10.0;
    img_alertpop.backgroundColor=[UIColor whiteColor];
    img_alertpop.userInteractionEnabled=YES;
    [view_popup addSubview:img_alertpop];
    
}



#pragma rerutrn event code

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}




- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

//# pragma mark forgotpass method
//
//-(void)AFForgotPass{
//    
//    [self.view addSubview:delegate.activityIndicator];
//    [delegate.activityIndicator startAnimating];
//    
//    //=================================================================BASE URL
//    
//    NSURL *url = [NSURL URLWithString:kBaseUrl];
//    
//    //=================================================================USED PARAMETERS(ONLY TEXT)
//    
//    if (TARGET_IPHONE_SIMULATOR)
//    {
//        delegate.devicestr = @"";
//    }
//    
//    
//    UIDevice *myDevice=[UIDevice currentDevice];
//    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
//    
//    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
//    
//    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
//        str_device_token = @"dev-signup";
//        
//    }
//    
//    NSDictionary *params =@{
//                            @"email"                            :   txt_user_Name.text
//                            
//                            };
//    
//    
//    //===========================================AFNETWORKING HEADER
//    
//    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
//    httpClient.parameterEncoding = AFFormURLParameterEncoding;
//    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
//    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
//    
//    //===============================SIMPLE REQUEST
//    
//    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
//                                                            path:kForgetPassword
//                                                      parameters:params];
//    
//    
//    
//    
//    //====================================================RESPONSE
//    
//    
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
//    
//    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
//        
//    }];
//    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSError *error = nil;
//        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
//        
//        [delegate.activityIndicator stopAnimating];
//        [self ResponseForgetpass:JSON];
//    }
//     
//     //==================================================ERROR
//                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                         
//                                         
//                                         [delegate.activityIndicator stopAnimating];
//                                         
//                                         if([operation.response statusCode] == 406){
//                                             
//                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
//                                             return;
//                                         }
//                                         
//                                         if([operation.response statusCode] == 403){
//                                             NSLog(@"Upload Failed");
//                                             return;
//                                         }
//                                         if ([[operation error] code] == -1009) {
//                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
//                                                                                          message:@"Please check your internet connection"
//                                                                                         delegate:nil
//                                                                                cancelButtonTitle:@"OK"
//                                                                                otherButtonTitles:nil];
//                                             [av show];
//                                         }
//                                         else if ([[operation error] code] == -1001) {
//                                             
//                                             NSLog(@"Successfully login");
//                                             [self AFForgotPass];
//                                         }
//                                     }];
//    [operation start];
//    
//    
//    
//}
//-(void) ResponseForgetpass :(NSDictionary * )TheDict
//{
//    NSLog(@"Login: %@",TheDict);
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
//    {
//        
//        [defaults setObject:[TheDict valueForKey:@"user_info"]  forKey:@"UserInfo"];
//        [defaults setObject:[[TheDict valueForKey:@"user_info"] valueForKey:@"default_car_id"] forKey:@"defaultCarId"];
//        if ([[NSUserDefaults standardUserDefaults]valueForKey:@"defaultCarId"]) {
//            
//        }
//        else
//            [defaults setObject:@"0" forKey:@"defaultCarId"];
//        [defaults synchronize];
//        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
//        
//        
//        
//    }
//    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
//    {
//        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
//        txt_user_Name.text = @"";
//        
//        //        [alertViewBody setHidden:NO];
//        
//        
//    }
//    
//}
//
//# pragma mark login method
//
//-(void) AFUserSignUp
//{
//    
//    [self.view addSubview:delegate.activityIndicator];
//    [delegate.activityIndicator startAnimating];
//    
//    //=================================================================BASE URL
//    
//    NSURL *url = [NSURL URLWithString:kBaseUrl];
//    
//    //=================================================================USED PARAMETERS(ONLY TEXT)
//    
//    if (TARGET_IPHONE_SIMULATOR)
//    {
//        delegate.devicestr = @"";
//    }
//    
//    
//    UIDevice *myDevice=[UIDevice currentDevice];
//    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
//    
//    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
//    
//    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
//        str_device_token = @"dev-signup";
//        
//    }
//    
//    NSDictionary *params =@{
//                            @"username"                         :   txt_user_Name.text,
//                            @"password"                         :   txt_password.text,
//                            @"device_udid"                      :   UniqueAppID,
//                            @"device_token"                     :   str_device_token,
//                            @"device_type"                      :   @"1"
//                            
//                            };
//    
//    
//    //===========================================AFNETWORKING HEADER
//    
//    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
//    httpClient.parameterEncoding = AFFormURLParameterEncoding;
//    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
//    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
//    
//    //===============================SIMPLE REQUEST
//    
//    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
//                                                            path:kUserLogin
//                                                      parameters:params];
//    
//    
//    
//    
//    //====================================================RESPONSE
//    
//    
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
//    
//    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
//        
//    }];
//    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSError *error = nil;
//        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
//        
//        [delegate.activityIndicator stopAnimating];
//        [self ResponseLogin:JSON];
//    }
//     
//     //==================================================ERROR
//                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                         
//                                         
//                                         [delegate.activityIndicator stopAnimating];
//                                         
//                                         if([operation.response statusCode] == 406){
//                                             
//                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
//                                             return;
//                                         }
//                                         
//                                         if([operation.response statusCode] == 403){
//                                             NSLog(@"Upload Failed");
//                                             return;
//                                         }
//                                         if ([[operation error] code] == -1009) {
//                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
//                                                                                          message:@"Please check your internet connection"
//                                                                                         delegate:nil
//                                                                                cancelButtonTitle:@"OK"
//                                                                                otherButtonTitles:nil];
//                                             [av show];
//                                         }
//                                         else if ([[operation error] code] == -1001) {
//                                             
//                                             NSLog(@"Successfully login");
//                                             [self AFUserSignUp];
//                                         }
//                                     }];
//    [operation start];
//    
//}
//-(void) ResponseLogin :(NSDictionary * )TheDict
//{
//    NSLog(@"Login: %@",TheDict);
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
//    {
//        
//        [defaults setObject:[TheDict valueForKey:@"user_info"]  forKey:@"UserInfo"];
//        [defaults setObject:[[TheDict valueForKey:@"user_info"] valueForKey:@"default_car_id"] forKey:@"defaultCarId"];
//        if ([[NSUserDefaults standardUserDefaults]valueForKey:@"defaultCarId"]) {
//            
//        }
//        else
//            [defaults setObject:@"0" forKey:@"defaultCarId"];
//        [defaults synchronize];
//        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
//        
//        
//        
//    }
//    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
//    {
//        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
//        txt_user_Name.text = @"";
//        txt_password.text = @"";
//        
//        //        [alertViewBody setHidden:NO];
//        
//        
//    }
//    
//}
//
//
//
///*
//#pragma mark - Navigation
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}
//*/

@end
