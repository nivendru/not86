//
//  DiningCartVC.m
//  Not86
//
//  Created by Admin on 31/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "DiningCartVC.h"
#import "DineInFoodNowVC.h"
#import "JWSlideMenuController.h"
#import "JWNavigationController.h"
#import "JWSlideMenuViewController.h"



#import "Define.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"



@interface DiningCartVC ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    
    UIImageView *img_header;
    UIImageView *img_background;
    UITableView *img_table;
    NSMutableArray*ary_displaynames;
    NSMutableArray * array_img_on_cells;
    NSMutableArray *array_total_Items;
    AppDelegate *delegate;
    
    
    NSString *str_FoodNow;
    NSString *str_FoodLater;
    NSString *str_OnRequest;
    
}

@end




@implementation DiningCartVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    array_img_on_cells = [[NSMutableArray alloc]initWithObjects:@"icon-now@2x.png",@"icon-timer@2x.png",@"iicon-on_reuest@2x.png", nil];
    ary_displaynames =[[NSMutableArray alloc]initWithObjects:@"Food Now",@"Food Later",@"On Request", nil];
    
    array_total_Items = [NSMutableArray new];
    
    [self integrateHeader];
    
    
    // [self integrateBodyDesign];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController.slideMenuController  UnhideHomeButton];
    [self AFDiningCart];

}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}


-(void)integrateHeader
{
    
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu .frame = CGRectMake(10, 13,25,20);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu  addTarget:self action:@selector(click_on_Back_Btn:)forControlEvents:UIControlEventTouchUpInside];
    [icon_menu setImage:[UIImage imageNamed:@"img_arrow@2x"] forState:UIControlStateNormal];
    //    icon_menu.layer.borderWidth = 1.0;
    [icon_menu setBackgroundColor:[UIColor clearColor]];
    //[img_header   addSubview:icon_menu ];
    
    
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+20,0, 150, 45);
    lbl_User_Sign_Up.text = @"Dine In Cart";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-35, 9, 27, 27);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
#pragma mark Tableview
    
    img_table= [[UITableView alloc] init ];
    img_table.frame  = CGRectMake(10,CGRectGetMaxY(img_header.frame)+5,WIDTH-20,165);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    //img_table.layer.borderWidth = 1.0;
    img_table.backgroundColor = [UIColor clearColor];
    [self.view addSubview:img_table];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ary_displaynames  count];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews)
    {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    img_cellBackGnd.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    //    img_cellBackGnd.layer.borderWidth = 1.0;
    [cell.contentView addSubview:img_cellBackGnd];
    
    
    UIImageView *icon_on_cells = [[UIImageView alloc]init];
    [icon_on_cells setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_img_on_cells objectAtIndex:indexPath.row]]]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_on_cells setUserInteractionEnabled:YES];
    [img_cellBackGnd addSubview:icon_on_cells];
    
    
    UILabel *lbl_on_cells = [[UILabel alloc]init];
    lbl_on_cells.text =[NSString stringWithFormat:@"%@",[ary_displaynames objectAtIndex:indexPath.row]];
    lbl_on_cells.font = [UIFont fontWithName:kFont size:15];
    lbl_on_cells.textColor = [UIColor blackColor];
    lbl_on_cells.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_on_cells];
    
    
    UIImageView *img_round_red = [[UIImageView alloc]init];
    [img_round_red setUserInteractionEnabled:YES];
    img_round_red.image=[UIImage imageNamed:@"icon_round_red@2x.png"];
    [img_cellBackGnd addSubview:img_round_red];
    
    
    UILabel *round_red_val  = [[UILabel alloc]init];
    if (indexPath.row == 0)
    {
        round_red_val .text = str_FoodNow;
    }
    else if (indexPath.row == 1)
    {
        round_red_val .text = str_FoodLater;
    }
    else if (indexPath.row == 2)
    {
        round_red_val .text = str_OnRequest;
    }
    //    round_red_val .text = @"3";
    round_red_val.textAlignment = NSTextAlignmentCenter;
    round_red_val .font = [UIFont fontWithName:kFontBold size:14];
    round_red_val .textColor = [UIColor whiteColor];
    round_red_val .backgroundColor = [UIColor clearColor];
    [img_round_red addSubview:round_red_val];
    
    
    
    UIImageView *icon_right_arrow = [[UIImageView alloc]init];
    [icon_right_arrow setImage:[UIImage imageNamed:@"right-arrow@2x.png"]];
    // icon_right_arrow.backgroundColor = [UIColor redColor];
    [icon_right_arrow setUserInteractionEnabled:YES];
    [img_cellBackGnd addSubview:icon_right_arrow];
    
    
    img_cellBackGnd.frame =  CGRectMake(0,2, WIDTH-15,50);
    icon_on_cells.frame = CGRectMake(10, 10, 30, 30);
    lbl_on_cells.frame = CGRectMake(CGRectGetMaxX(icon_on_cells.frame)+20, 0, 150, 50);
    img_round_red.frame = CGRectMake(WIDTH-90, 12, 25, 25);
    round_red_val.frame = CGRectMake(0, 0, 25, 25);
    icon_right_arrow .frame =  CGRectMake(CGRectGetMaxX(img_round_red.frame)+15,16, 12, 17);
    
    
    //    if (IS_IPHONE_6Plus)
    //    {
    //        img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH+5, 50);
    //        icon_on_cells.frame = CGRectMake(10, 9, 27, 27);
    //
    //        lbl_on_cells.frame = CGRectMake(CGRectGetMaxX(icon_on_cells.frame),10, 150, 30);
    //
    //        icon_right_arrow .frame =  CGRectMake(250,15, 15, 15);
    //
    //
    //    }
    //    else if (IS_IPHONE_6)
    //    {
    //        img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-5, 60);
    //        icon_on_cells.frame = CGRectMake(15, 15, 27, 27);
    //        lbl_on_cells.frame = CGRectMake(CGRectGetMaxX(icon_on_cells.frame)+15,13, 150, 30);
    //        icon_right_arrow .frame =  CGRectMake(330,20, 15, 18);
    //
    //
    //
    //    }
    //    else
    //    {    img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-10,170);
    //        icon_on_cells.frame = CGRectMake(10, 9, 27, 27);
    //
    //        lbl_on_cells.frame = CGRectMake(CGRectGetMaxX(icon_on_cells.frame)+30,0, 150, 45);
    //        icon_right_arrow .frame =  CGRectMake(250,15, 15, 15);
    //
    //
    //    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    objDineInFN = [DineInFoodNowVC new];
    if (indexPath.row == 0)
    {
        objDineInFN.str_Type =   @"0";
    }
    else if (indexPath.row == 1)
    {
        
        objDineInFN.str_Type =  @"1";
        
    }
    else if (indexPath.row == 2)
    {
        objDineInFN.str_Type = @"2";
    }
    
    [self presentViewController:objDineInFN animated:NO completion:nil];
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma click-events
-(void)click_on_Back_Btn:(UIButton *)sender
{
    NSLog(@"click_on_Back_Btn click");
}


#pragma mark servixcesfunctionality

-(void)AFDiningCart
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    NSDictionary *params =@{
                            
                            
                            @"uid"            :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER  kChefAccountSaleSummary
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:@"webservices/foodnow_foodlater_onschedule_Count.json"
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseDiningCartList:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFDiningCart];
                                         }
                                     }];
    [operation start];
    
}

-(void) ResponseDiningCartList :(NSDictionary * )TheDict
{
    
    
    NSLog(@"Response Dict: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        str_FoodNow = [NSString stringWithFormat: @"%@",[[TheDict valueForKey:@"FoodNow_FoodLater_OnRequestList"] valueForKey:@"FoodNow"]];
        str_FoodLater = [NSString stringWithFormat: @"%@",[[TheDict valueForKey:@"FoodNow_FoodLater_OnRequestList"] valueForKey:@"FoodLater"]];
        str_OnRequest = [NSString stringWithFormat: @"%@",[[TheDict valueForKey:@"FoodNow_FoodLater_OnRequestList"] valueForKey:@"OnRequest"]];
    }
    
    
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        NSLog(@"Error 1 check response");
        
    }
    
    [img_table reloadData];
    
    //    [self AlergyList];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
