//
//  LocationVC.m
//  Not86
//
//  Created by Admin on 30/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "LocationVC.h"
#import "Define.h"
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"

@interface LocationVC ()<UITextFieldDelegate>
{
    UIImageView *img_header;
    UIView *view_location;
    CGFloat	animatedDistance;
}

@end

@implementation LocationVC

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBodyDesign];
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)integrateHeader
{
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 50);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back addTarget:self action:@selector(btn_back_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_back];
    
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_back.frame)+30,0, 150, 45);
    lbl_User_Sign_Up.text = @"Location";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}
-(void)integrateBodyDesign
{
    
    view_location = [[UIView alloc]init];
    view_location.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT);
    view_location.backgroundColor=[UIColor clearColor];
    [self.view  addSubview:  view_location];
    
    
    UILabel *lbl_enter_your_location = [[UILabel alloc]init];
    lbl_enter_your_location.frame = CGRectMake(60,0,250, 45);
    lbl_enter_your_location .text = @"Enter your location and\n        start ordering!";
    lbl_enter_your_location .font = [UIFont fontWithName:kFont size:17];
    lbl_enter_your_location .textColor = [UIColor blackColor];
    lbl_enter_your_location .backgroundColor = [UIColor clearColor];
    lbl_enter_your_location.numberOfLines = 0;
    [view_location addSubview:lbl_enter_your_location ];
    
    
    UITextField *txt_favorite_location = [[UITextField alloc] init];
    txt_favorite_location .frame = CGRectMake(70, CGRectGetMaxY( lbl_enter_your_location.frame)+5, WIDTH-40, 30);
    txt_favorite_location .borderStyle = UITextBorderStyleNone;
    txt_favorite_location .textColor = [UIColor grayColor];
    txt_favorite_location .font = [UIFont fontWithName:kFont size:13];
    txt_favorite_location .placeholder = @"Favorite Location";
    [txt_favorite_location  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_favorite_location  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding1 = [[UIView alloc]init];
    padding1.frame = CGRectMake(0, 0, 5, 10);
    txt_favorite_location .leftView = padding1;
    txt_favorite_location .leftViewMode = UITextFieldViewModeAlways;
    txt_favorite_location .userInteractionEnabled=YES;
    txt_favorite_location .textAlignment = NSTextAlignmentLeft;
    txt_favorite_location .backgroundColor = [UIColor clearColor];
    txt_favorite_location .keyboardType = UIKeyboardTypeAlphabet;
    txt_favorite_location .delegate = self;
    [view_location addSubview:txt_favorite_location ];
    
    UIImageView * img_line = [[UIImageView alloc]init];
    img_line .frame = CGRectMake(30, CGRectGetMaxY(  txt_favorite_location.frame)+5, WIDTH-60, 0.5);
    [img_line setImage:[UIImage imageNamed:@"img_line@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_line setUserInteractionEnabled:YES];
    [view_location addSubview:img_line];
    
    
    UIImageView *icon_pointer = [[UIImageView alloc]init];
    icon_pointer.frame = CGRectMake(30, CGRectGetMaxY( lbl_enter_your_location.frame), 30, 30);
    [icon_pointer setImage:[UIImage imageNamed:@"pointer-location@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_pointer setUserInteractionEnabled:YES];
    [view_location addSubview:icon_pointer];
    
    UIButton *icon_drop_down = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_drop_down.frame = CGRectMake(250, CGRectGetMaxY( lbl_enter_your_location.frame)+10, 30, 30);
    icon_drop_down .backgroundColor = [UIColor clearColor];
    [icon_drop_down addTarget:self action:@selector(btn_drop_down_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_drop_down setImage:[UIImage imageNamed:@"drop-down@2x.png"] forState:UIControlStateNormal];
    [view_location   addSubview:icon_drop_down];
    
    UIButton *btn_favorite_location = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_favorite_location.frame = CGRectMake(25, CGRectGetMaxY( lbl_enter_your_location.frame)+3, 270, 35);
    btn_favorite_location .backgroundColor = [UIColor clearColor];
    [btn_favorite_location addTarget:self action:@selector(btn_click_on_favorite_location:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_favorite_location setImage:[UIImage imageNamed:@"drop-down@2x.png"] forState:UIControlStateNormal];
    [view_location   addSubview:btn_favorite_location];
    
    
    UITextField *txt_street_name = [[UITextField alloc] init];
    txt_street_name .frame = CGRectMake(70, CGRectGetMaxY( img_line.frame)+10, WIDTH-40, 30);
    txt_street_name .borderStyle = UITextBorderStyleNone;
    txt_street_name .textColor = [UIColor grayColor];
    txt_street_name .font = [UIFont fontWithName:kFont size:13];
    txt_street_name .placeholder = @"Street Name";
    [txt_street_name  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_street_name  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_street_name .leftView = padding2;
    txt_street_name .leftViewMode = UITextFieldViewModeAlways;
    txt_street_name .userInteractionEnabled=YES;
    txt_street_name .textAlignment = NSTextAlignmentLeft;
    txt_street_name .backgroundColor = [UIColor clearColor];
    txt_street_name .keyboardType = UIKeyboardTypeAlphabet;
    txt_street_name .delegate = self;
    [view_location addSubview:txt_street_name ];
    
    UIImageView * img_line2 = [[UIImageView alloc]init];
    img_line2 .frame = CGRectMake(30, CGRectGetMaxY(  txt_street_name.frame)+5, WIDTH-60, 0.5);
    [img_line2 setImage:[UIImage imageNamed:@"img_line@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_line2 setUserInteractionEnabled:YES];
    [view_location addSubview:img_line2];
    
    
    UIImageView *icon_pointer1 = [[UIImageView alloc]init];
    icon_pointer1.frame = CGRectMake(30, CGRectGetMaxY( img_line.frame)+10, 30, 30);
    [icon_pointer1 setImage:[UIImage imageNamed:@"pointer-location@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_pointer setUserInteractionEnabled:YES];
    [view_location addSubview:icon_pointer1];
    
    UIButton *icon_drop_down1 = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_drop_down1.frame = CGRectMake(250, CGRectGetMaxY( img_line.frame)+20, 30, 30);
    icon_drop_down1 .backgroundColor = [UIColor clearColor];
    [icon_drop_down1 addTarget:self action:@selector(btn_drop_down1_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_drop_down1 setImage:[UIImage imageNamed:@"drop-down@2x.png"] forState:UIControlStateNormal];
    [view_location   addSubview:icon_drop_down1];
    
    UIButton *btn_street_name = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_street_name.frame = CGRectMake(25, CGRectGetMaxY( img_line.frame)+10, 270, 35);
    btn_street_name .backgroundColor = [UIColor clearColor];
    [btn_street_name addTarget:self action:@selector(btn_click_on_street_name:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_favorite_location setImage:[UIImage imageNamed:@"drop-down@2x.png"] forState:UIControlStateNormal];
    [view_location   addSubview:btn_street_name];
    
    UITextField *txt_postel_code = [[UITextField alloc] init];
    txt_postel_code .frame = CGRectMake(70, CGRectGetMaxY( img_line2.frame)+10, WIDTH-40, 30);
    txt_postel_code .borderStyle = UITextBorderStyleNone;
    txt_postel_code .textColor = [UIColor grayColor];
    txt_postel_code .font = [UIFont fontWithName:kFont size:13];
    txt_postel_code .placeholder = @"Postal Code";
    [txt_postel_code  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_postel_code  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_postel_code .leftView = padding3;
    txt_postel_code .leftViewMode = UITextFieldViewModeAlways;
    txt_postel_code .userInteractionEnabled=YES;
    txt_postel_code .textAlignment = NSTextAlignmentLeft;
    txt_postel_code .backgroundColor = [UIColor clearColor];
    txt_postel_code .keyboardType = UIKeyboardTypeAlphabet;
    txt_postel_code .delegate = self;
    [view_location addSubview:txt_postel_code ];
    
    UIImageView * img_line3 = [[UIImageView alloc]init];
    img_line3 .frame = CGRectMake(30, CGRectGetMaxY( txt_postel_code.frame)+5, WIDTH-60, 0.5);
    [img_line3 setImage:[UIImage imageNamed:@"img_line@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_line3 setUserInteractionEnabled:YES];
    [view_location addSubview:img_line3];
    
    
    UIImageView *icon_location = [[UIImageView alloc]init];
    icon_location.frame = CGRectMake(30, CGRectGetMaxY( img_line2.frame)+15, 20, 25);
    [icon_location setImage:[UIImage imageNamed:@"icon-location1@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_location setUserInteractionEnabled:YES];
    [view_location addSubview:icon_location];
    
    UIButton *btn_postel_code = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_postel_code.frame = CGRectMake(25, CGRectGetMaxY( img_line2.frame)+10, 270, 35);
    btn_postel_code .backgroundColor = [UIColor clearColor];
    [btn_postel_code addTarget:self action:@selector(btn_click_on_postel_code:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_favorite_location setImage:[UIImage imageNamed:@"drop-down@2x.png"] forState:UIControlStateNormal];
    [view_location   addSubview:btn_postel_code];
    
    UITextField *txt_my_current_location = [[UITextField alloc] init];
    txt_my_current_location .frame = CGRectMake(70, CGRectGetMaxY( img_line3.frame)+10, WIDTH-40, 30);
    txt_my_current_location .borderStyle = UITextBorderStyleNone;
    txt_my_current_location .textColor = [UIColor grayColor];
    txt_my_current_location .font = [UIFont fontWithName:kFont size:13];
    txt_my_current_location .placeholder = @"Use my current location";
    [txt_my_current_location  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_my_current_location  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_my_current_location .leftView = padding4;
    txt_my_current_location .leftViewMode = UITextFieldViewModeAlways;
    txt_my_current_location .userInteractionEnabled=YES;
    txt_my_current_location .textAlignment = NSTextAlignmentLeft;
    txt_my_current_location .backgroundColor = [UIColor clearColor];
    txt_my_current_location .keyboardType = UIKeyboardTypeAlphabet;
    txt_my_current_location .delegate = self;
    [view_location addSubview:txt_my_current_location ];
    
    
    UIImageView *icon_navigatore = [[UIImageView alloc]init];
    icon_navigatore.frame = CGRectMake(30, CGRectGetMaxY( img_line3.frame)+15, 30, 30);
    [icon_navigatore setImage:[UIImage imageNamed:@"img-navigatore@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_navigatore setUserInteractionEnabled:YES];
    [view_location addSubview:icon_navigatore];
    
    UIButton *btn_my_current_location = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_my_current_location.frame = CGRectMake(25, CGRectGetMaxY( img_line3.frame)+10, 270, 35);
    btn_my_current_location .backgroundColor = [UIColor clearColor];
    [btn_my_current_location addTarget:self action:@selector(btn_click_on_my_current_location:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_favorite_location setImage:[UIImage imageNamed:@"drop-down@2x.png"] forState:UIControlStateNormal];
    [view_location   addSubview:btn_my_current_location];
    
    UIButton *btn_img_done = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_done.frame = CGRectMake(25, CGRectGetMaxY( txt_my_current_location.frame)+20, 270, 40);
    btn_img_done .backgroundColor = [UIColor clearColor];
    [btn_img_done addTarget:self action:@selector(btn_click_on_done:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_done setImage:[UIImage imageNamed:@" button-done@2x.png"] forState:UIControlStateNormal];
    [view_location   addSubview:btn_img_done];
    
    if (IS_IPHONE_6Plus)
    {
        view_location.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT);
        lbl_enter_your_location.frame = CGRectMake(100,15,250, 45);
        txt_favorite_location .frame = CGRectMake(100, CGRectGetMaxY( lbl_enter_your_location.frame)+15, WIDTH-40, 30);
        img_line .frame = CGRectMake(55, CGRectGetMaxY(  txt_favorite_location.frame)+5, WIDTH-140, 0.5);
        icon_pointer.frame = CGRectMake(60, CGRectGetMaxY( lbl_enter_your_location.frame)+10, 30, 30);
        icon_drop_down.frame = CGRectMake(WIDTH-125, CGRectGetMaxY( lbl_enter_your_location.frame)+20, 30, 30);
        btn_favorite_location.frame = CGRectMake(295, CGRectGetMaxY( lbl_enter_your_location.frame)+13, 27, 35);
        txt_street_name .frame = CGRectMake(100, CGRectGetMaxY( img_line.frame)+20, WIDTH-40, 30);
        img_line2 .frame = CGRectMake(55, CGRectGetMaxY(  txt_street_name.frame)+5, WIDTH-140, 0.5);
        icon_pointer1.frame = CGRectMake(60, CGRectGetMaxY( img_line.frame)+20, 30, 30);
        icon_drop_down1.frame = CGRectMake(WIDTH-125, CGRectGetMaxY( img_line.frame)+30, 30, 30);
        btn_street_name.frame = CGRectMake(295, CGRectGetMaxY( img_line.frame)+20, 27, 35);
        txt_postel_code .frame = CGRectMake(100, CGRectGetMaxY( img_line2.frame)+20, WIDTH-40, 30);
        img_line3 .frame = CGRectMake(55, CGRectGetMaxY( txt_postel_code.frame)+5, WIDTH-140, 0.5);
        icon_location.frame = CGRectMake(60, CGRectGetMaxY( img_line2.frame)+25, 20, 25);
        btn_postel_code.frame = CGRectMake(295, CGRectGetMaxY( img_line2.frame)+20, 27, 35);
        txt_my_current_location .frame = CGRectMake(100, CGRectGetMaxY( img_line3.frame)+20, WIDTH-40, 30);
        icon_navigatore.frame = CGRectMake(60, CGRectGetMaxY( img_line3.frame)+25, 30, 30);
        btn_my_current_location.frame = CGRectMake(57, CGRectGetMaxY( img_line3.frame)+20, 40, 35);
        btn_img_done.frame = CGRectMake(0, CGRectGetMaxY( txt_my_current_location.frame)+40, WIDTH, 50);
        
    }
    else if (IS_IPHONE_6)
    {
        view_location.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT);
        lbl_enter_your_location.frame = CGRectMake(85,25,250, 45);
        txt_favorite_location .frame = CGRectMake(100, CGRectGetMaxY( lbl_enter_your_location.frame)+25, WIDTH-40, 30);
        img_line .frame = CGRectMake(55, CGRectGetMaxY(  txt_favorite_location.frame)+5, WIDTH-120, 0.5);
        icon_pointer.frame = CGRectMake(60, CGRectGetMaxY( lbl_enter_your_location.frame)+20, 30, 30);
        icon_drop_down.frame = CGRectMake(WIDTH-95, CGRectGetMaxY( lbl_enter_your_location.frame)+30, 30, 30);
        btn_favorite_location.frame = CGRectMake(280, CGRectGetMaxY( lbl_enter_your_location.frame)+23, 27, 35);
        txt_street_name .frame = CGRectMake(100, CGRectGetMaxY( img_line.frame)+20, WIDTH-40, 30);
        img_line2 .frame = CGRectMake(55, CGRectGetMaxY(  txt_street_name.frame)+5, WIDTH-120, 0.5);
        icon_pointer1.frame = CGRectMake(60, CGRectGetMaxY( img_line.frame)+20, 30, 30);
        icon_drop_down1.frame = CGRectMake(WIDTH-95, CGRectGetMaxY( img_line.frame)+30, 30, 30);
        btn_street_name.frame = CGRectMake(280, CGRectGetMaxY( img_line.frame)+20, 27, 35);
        txt_postel_code .frame = CGRectMake(100, CGRectGetMaxY( img_line2.frame)+20, WIDTH-40, 30);
        img_line3 .frame = CGRectMake(55, CGRectGetMaxY( txt_postel_code.frame)+5, WIDTH-120, 0.5);
        icon_location.frame = CGRectMake(60, CGRectGetMaxY( img_line2.frame)+25, 20, 25);
        btn_postel_code.frame = CGRectMake(280, CGRectGetMaxY( img_line2.frame)+20, 27, 35);
        txt_my_current_location .frame = CGRectMake(100, CGRectGetMaxY( img_line3.frame)+20, WIDTH-40, 30);
        icon_navigatore.frame = CGRectMake(60, CGRectGetMaxY( img_line3.frame)+25, 30, 30);
        btn_my_current_location.frame = CGRectMake(57, CGRectGetMaxY( img_line3.frame)+20, 40, 35);
        btn_img_done.frame = CGRectMake(40, CGRectGetMaxY( txt_my_current_location.frame)+40, WIDTH-80, 50);
        
    }
    else
    {
        view_location.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT);
        lbl_enter_your_location.frame = CGRectMake(70,25,250, 45);
        txt_favorite_location .frame = CGRectMake(80, CGRectGetMaxY( lbl_enter_your_location.frame)+25, WIDTH-40, 30);
        img_line .frame = CGRectMake(40, CGRectGetMaxY(  txt_favorite_location.frame)+5, WIDTH-70, 0.5);
        icon_pointer.frame = CGRectMake(40, CGRectGetMaxY( lbl_enter_your_location.frame)+20, 30, 30);
        icon_drop_down.frame = CGRectMake(WIDTH-65, CGRectGetMaxY( lbl_enter_your_location.frame)+30, 30, 30);
        btn_favorite_location.frame = CGRectMake(260, CGRectGetMaxY( lbl_enter_your_location.frame)+23, 27, 35);
        txt_street_name .frame = CGRectMake(80, CGRectGetMaxY( img_line.frame)+20, WIDTH-40, 30);
        img_line2 .frame = CGRectMake(40, CGRectGetMaxY(  txt_street_name.frame)+5, WIDTH-70, 0.5);
        icon_pointer1.frame = CGRectMake(40, CGRectGetMaxY( img_line.frame)+20, 30, 30);
        icon_drop_down1.frame = CGRectMake(WIDTH-65, CGRectGetMaxY( img_line.frame)+30, 30, 30);
        btn_street_name.frame = CGRectMake(260, CGRectGetMaxY( img_line.frame)+20, 27, 35);
        txt_postel_code .frame = CGRectMake(80, CGRectGetMaxY( img_line2.frame)+20, WIDTH-40, 30);
        img_line3 .frame = CGRectMake(40, CGRectGetMaxY( txt_postel_code.frame)+5, WIDTH-70, 0.5);
        icon_location.frame = CGRectMake(40, CGRectGetMaxY( img_line2.frame)+25, 20, 25);
        btn_postel_code.frame = CGRectMake(260, CGRectGetMaxY( img_line2.frame)+20, 27, 35);
        txt_my_current_location .frame = CGRectMake(80, CGRectGetMaxY( img_line3.frame)+20, WIDTH-40, 30);
        icon_navigatore.frame = CGRectMake(40, CGRectGetMaxY( img_line3.frame)+25, 30, 30);
        btn_my_current_location.frame = CGRectMake(37, CGRectGetMaxY( img_line3.frame)+20, 40, 35);
        btn_img_done.frame = CGRectMake(20, CGRectGetMaxY( txt_my_current_location.frame)+40, WIDTH-40, 50);
    }
    
    
    
}
-(void)btn_back_click:(UIButton *)sender
{
    NSLog(@"img_fb_click Btn Click");
    [self.navigationController popViewControllerAnimated:NO];
    
    
}
-(void)btn_drop_down_click:(UIButton *)sender
{
    NSLog(@"btn_drop_down_click:");
    
}
-(void)btn_click_on_favorite_location:(UIButton *)sender
{
    NSLog(@"btn_click_on_favorite_location:");
    
}
-(void)btn_drop_down1_click:(UIButton *)sender
{
    NSLog(@"btn_drop_down1_click:");
    
}

-(void)btn_click_on_street_name:(UIButton *)sender
{
    NSLog(@"btn_click_on_street_name:");
    
}
-(void)btn_click_on_postel_code:(UIButton *)sender
{
    NSLog(@"btn_click_on_postel_code:");
    
}
-(void)btn_click_on_my_current_location:(UIButton *)sender
{
    NSLog(@"btn_click_on_my_current_location::");
    
}
-(void)btn_click_on_done:(UIButton *)sender
{
    NSLog(@"btn_click_on_done:");
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
