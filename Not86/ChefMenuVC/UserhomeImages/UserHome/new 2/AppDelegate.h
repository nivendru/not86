//
//  AppDelegate.h
//  Not86
//
//  Created by Interwld on 7/29/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
     UIView *splashView;
   BOOL isFBLogin;
}
@property(nonatomic,strong)UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController * navigationC;
@property (nonatomic,assign) BOOL isFBLogin;
@property (nonatomic, strong)NSString *devicestr;
@property (strong, nonatomic) NSString *str_Rotation;


-(void)callME;


@end

