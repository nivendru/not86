//
//  UserNotificationsVC.m
//  Not86
//
//  Created by Admin on 02/09/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "UserNotificationsVC.h"
#import "Define.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"


@interface UserNotificationsVC ()<UIScrollViewDelegate>
{
    UIImageView * img_header;
    UIScrollView *scroll;
    UIButton *  btn_push_notification;
    UIButton * btn_notification_sound;
    UIButton * btn_on_favorite_chef_serving_now;
    UIButton * btn_on_dish_serving_now;

    
    
     int push_notification;
     int notification_soundl;
     int favorite_chef_serving_now;
     int favorite_dish_serving_now;
}

@end

@implementation UserNotificationsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [self integrateHeader];
    [self integrateBodyDesign];
}

-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [icon_back addTarget:self action:@selector(click_on_back_btn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:icon_back];
    
    
    UILabel *lbl_support_on_header = [[UILabel alloc]init];
    lbl_support_on_header.frame = CGRectMake(CGRectGetMaxX(icon_back .frame)+30,0, 300, 45);
    lbl_support_on_header.text = @"Notification Settings";
    lbl_support_on_header.font = [UIFont fontWithName:kFont size:20];
    lbl_support_on_header.textColor = [UIColor whiteColor];
    lbl_support_on_header.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_support_on_header];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}

-(void)integrateBodyDesign
{
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.frame = CGRectMake(8,46,WIDTH-16,HEIGHT);
    scroll.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    UILabel *lbl_notifications = [[UILabel alloc]init];
    lbl_notifications.frame = CGRectMake(10,0, 100, 45);
    lbl_notifications.text = @"Notification";
    lbl_notifications.font = [UIFont fontWithName:kFontBold size:17];
    lbl_notifications.textColor = [UIColor blackColor];
    lbl_notifications.backgroundColor = [UIColor clearColor];
    [scroll addSubview:lbl_notifications];
    
    
    UIImageView *img_bg_for_push_notifications = [[UIImageView alloc]init];
    img_bg_for_push_notifications.frame = CGRectMake(0, CGRectGetMaxY(lbl_notifications.frame), WIDTH-20, 45);
    [img_bg_for_push_notifications setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_push_notifications setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_for_push_notifications];
    
    UILabel *lbl_push_notifications = [[UILabel alloc]init];
    lbl_push_notifications.frame = CGRectMake(10,0, 200, 45);
    lbl_push_notifications.text = @"Push Notifications";
    lbl_push_notifications.font = [UIFont fontWithName:kFont size:15];
    lbl_push_notifications.textColor = [UIColor blackColor];
    lbl_push_notifications.backgroundColor = [UIColor clearColor];
    [img_bg_for_push_notifications addSubview:lbl_push_notifications];
    
    
    btn_push_notification  =[UIButton buttonWithType:UIButtonTypeCustom];
     btn_push_notification.frame=CGRectMake(CGRectGetMaxX(lbl_push_notifications.frame)+45,10,100,30);
    [ btn_push_notification setImage:[UIImage imageNamed:@"icon-on@2x.png"] forState:UIControlStateSelected];
    [ btn_push_notification setImage:[UIImage imageNamed:@"icon-off@2x.png"] forState:UIControlStateNormal];
    [ btn_push_notification addTarget:self action:@selector(Click_on_push_notification_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_push_notifications  addSubview: btn_push_notification];
    
    UIImageView *img_bg_for_notification_sound = [[UIImageView alloc]init];
    img_bg_for_notification_sound.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_push_notifications.frame)+10, WIDTH-20, 45);
    [img_bg_for_notification_sound setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_notification_sound setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_for_notification_sound];
    
    UILabel *lbl_notification_sound = [[UILabel alloc]init];
    lbl_notification_sound.frame = CGRectMake(10,0, 200, 45);
    lbl_notification_sound.text = @"Notification Sound";
    lbl_notification_sound.font = [UIFont fontWithName:kFont size:15];
    lbl_notification_sound.textColor = [UIColor blackColor];
    lbl_notification_sound.backgroundColor = [UIColor clearColor];
    [img_bg_for_notification_sound  addSubview:lbl_notification_sound];
    
    
    btn_notification_sound  =[UIButton buttonWithType:UIButtonTypeCustom];
    btn_notification_sound.frame=CGRectMake(CGRectGetMaxX(lbl_push_notifications.frame)+45,10,100,30);
    [btn_notification_sound setImage:[UIImage imageNamed:@"icon-on@2x.png"] forState:UIControlStateSelected];
    [btn_notification_sound setImage:[UIImage imageNamed:@"icon-off@2x.png"] forState:UIControlStateNormal];
    [btn_notification_sound addTarget:self action:@selector(Click_on_notification_sound_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_notification_sound  addSubview:  btn_notification_sound];
    
    UIImageView *img_bg_for_favorite_chef_serving_now = [[UIImageView alloc]init];
    img_bg_for_favorite_chef_serving_now.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_notification_sound.frame)+10, WIDTH-20, 45);
    [img_bg_for_favorite_chef_serving_now setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_favorite_chef_serving_now setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_for_favorite_chef_serving_now];
    
    UILabel *lbl_favorite_chef_serving_now = [[UILabel alloc]init];
    lbl_favorite_chef_serving_now.frame = CGRectMake(10,0, 200, 45);
    lbl_favorite_chef_serving_now.text = @"Favorite Chef Serving Now";
    lbl_favorite_chef_serving_now.font = [UIFont fontWithName:kFont size:15];
    lbl_favorite_chef_serving_now.textColor = [UIColor blackColor];
    lbl_favorite_chef_serving_now.backgroundColor = [UIColor clearColor];
    [img_bg_for_favorite_chef_serving_now  addSubview:lbl_favorite_chef_serving_now];
    

    btn_on_favorite_chef_serving_now  =[UIButton buttonWithType:UIButtonTypeCustom];
        btn_on_favorite_chef_serving_now.frame=CGRectMake(CGRectGetMaxX(lbl_push_notifications.frame)+45,10,100,30);
    [btn_on_favorite_chef_serving_now setImage:[UIImage imageNamed:@"icon-on@2x.png"] forState:UIControlStateSelected];
    [btn_on_favorite_chef_serving_now setImage:[UIImage imageNamed:@"icon-off@2x.png"] forState:UIControlStateNormal];
    [btn_on_favorite_chef_serving_now addTarget:self action:@selector(Click_on_favorite_chef_seving_now_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_favorite_chef_serving_now addSubview:  btn_on_favorite_chef_serving_now];
    
    UIImageView *img_bg_for_favorite_dish_serving_now = [[UIImageView alloc]init];
    img_bg_for_favorite_dish_serving_now.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_favorite_chef_serving_now.frame)+10, WIDTH-20, 45);
    [img_bg_for_favorite_dish_serving_now setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_favorite_dish_serving_now setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_for_favorite_dish_serving_now];
    
    UILabel *lbl_favorite_dish_serving_now = [[UILabel alloc]init];
    lbl_favorite_dish_serving_now.frame = CGRectMake(10,0, 200, 45);
    lbl_favorite_dish_serving_now.text = @"Favorite Dish Serving Now";
    lbl_favorite_dish_serving_now.font = [UIFont fontWithName:kFont size:15];
    lbl_favorite_dish_serving_now.textColor = [UIColor blackColor];
    lbl_favorite_dish_serving_now.backgroundColor = [UIColor clearColor];
    [img_bg_for_favorite_dish_serving_now  addSubview:lbl_favorite_dish_serving_now];
    
 
    btn_on_dish_serving_now  =[UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_dish_serving_now.frame=CGRectMake(CGRectGetMaxX(lbl_push_notifications.frame)+45,10,100,30);
    [btn_on_dish_serving_now setImage:[UIImage imageNamed:@"icon-on@2x.png"] forState:UIControlStateSelected];
    [btn_on_dish_serving_now setImage:[UIImage imageNamed:@"icon-off@2x.png"] forState:UIControlStateNormal];
    [btn_on_dish_serving_now addTarget:self action:@selector(Click_on_favorite_dish_seving_now_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_favorite_dish_serving_now addSubview:  btn_on_dish_serving_now];
    
    if (IS_IPHONE_6Plus)
    {
        scroll.frame = CGRectMake(8,46,WIDTH-16,HEIGHT);
        lbl_notifications.frame = CGRectMake(10,0, 100, 45);
        img_bg_for_push_notifications.frame = CGRectMake(0, CGRectGetMaxY(lbl_notifications.frame), WIDTH-20, 45);
        lbl_push_notifications.frame = CGRectMake(10,0, 200, 45);
        btn_push_notification.frame=CGRectMake(CGRectGetMaxX(lbl_push_notifications.frame)+45,10,100,30);
        img_bg_for_notification_sound.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_push_notifications.frame)+10, WIDTH-20, 45);
        lbl_notification_sound.frame = CGRectMake(10,0, 200, 45);
        btn_notification_sound.frame=CGRectMake(CGRectGetMaxX(lbl_push_notifications.frame)+45,10,100,30);
        img_bg_for_favorite_chef_serving_now.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_notification_sound.frame)+10, WIDTH-20, 45);
        lbl_favorite_chef_serving_now.frame = CGRectMake(10,0, 200, 45);
        btn_on_favorite_chef_serving_now.frame=CGRectMake(CGRectGetMaxX(lbl_push_notifications.frame)+45,10,100,30);
        img_bg_for_favorite_dish_serving_now.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_favorite_chef_serving_now.frame)+10, WIDTH-20, 45);
        lbl_favorite_dish_serving_now.frame = CGRectMake(10,0, 200, 45);
        btn_on_dish_serving_now.frame=CGRectMake(CGRectGetMaxX(lbl_push_notifications.frame)+45,10,100,30);
    }
    else if (IS_IPHONE_6)
    {
        scroll.frame = CGRectMake(8,46,WIDTH-16,HEIGHT);
        lbl_notifications.frame = CGRectMake(10,0, 100, 45);
        img_bg_for_push_notifications.frame = CGRectMake(0, CGRectGetMaxY(lbl_notifications.frame), WIDTH-20, 45);
        lbl_push_notifications.frame = CGRectMake(10,0, 200, 45);
        btn_push_notification.frame=CGRectMake(CGRectGetMaxX(lbl_push_notifications.frame)+45,10,100,30);
        img_bg_for_notification_sound.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_push_notifications.frame)+10, WIDTH-20, 45);
        lbl_notification_sound.frame = CGRectMake(10,0, 200, 45);
        btn_notification_sound.frame=CGRectMake(CGRectGetMaxX(lbl_push_notifications.frame)+45,10,100,30);
        img_bg_for_favorite_chef_serving_now.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_notification_sound.frame)+10, WIDTH-20, 45);
        lbl_favorite_chef_serving_now.frame = CGRectMake(10,0, 200, 45);
        btn_on_favorite_chef_serving_now.frame=CGRectMake(CGRectGetMaxX(lbl_push_notifications.frame)+45,10,100,30);
        img_bg_for_favorite_dish_serving_now.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_favorite_chef_serving_now.frame)+10, WIDTH-20, 45);
        lbl_favorite_dish_serving_now.frame = CGRectMake(10,0, 200, 45);
        btn_on_dish_serving_now.frame=CGRectMake(CGRectGetMaxX(lbl_push_notifications.frame)+45,10,100,30);
    }
    else
    {
        scroll.frame = CGRectMake(8,46,WIDTH-16,HEIGHT);
        lbl_notifications.frame = CGRectMake(10,0, 100, 45);
        img_bg_for_push_notifications.frame = CGRectMake(0, CGRectGetMaxY(lbl_notifications.frame), WIDTH-20, 45);
        lbl_push_notifications.frame = CGRectMake(10,0, 200, 45);
        btn_push_notification.frame=CGRectMake(CGRectGetMaxX(lbl_push_notifications.frame)+45,10,100,30);
        img_bg_for_notification_sound.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_push_notifications.frame)+10, WIDTH-20, 45);
        lbl_notification_sound.frame = CGRectMake(10,0, 200, 45);
        btn_notification_sound.frame=CGRectMake(CGRectGetMaxX(lbl_push_notifications.frame)+45,10,100,30);
        img_bg_for_favorite_chef_serving_now.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_notification_sound.frame)+10, WIDTH-20, 45);
        lbl_favorite_chef_serving_now.frame = CGRectMake(10,0, 200, 45);
        btn_on_favorite_chef_serving_now.frame=CGRectMake(CGRectGetMaxX(lbl_push_notifications.frame)+45,10,100,30);
        img_bg_for_favorite_dish_serving_now.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_favorite_chef_serving_now.frame)+10, WIDTH-20, 45);
        lbl_favorite_dish_serving_now.frame = CGRectMake(10,0, 200, 45);
        btn_on_dish_serving_now.frame=CGRectMake(CGRectGetMaxX(lbl_push_notifications.frame)+45,10,100,30);
    }

    

    



}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma cliclk events
-(void)click_on_back_btn:(UIButton *)sender
{
    NSLog(@"click_on_back_button");
}
-(void)Click_on_push_notification_btn:(UIButton *)sender
{
    NSLog(@"click_on_off_btn:");
    if([sender isSelected])
    {
        
        push_notification  = 0;
        [sender setSelected:NO];
       // [self AFMySettings:@""];
    }
    else
    {
        
        
         push_notification = 1;
        [sender setSelected:YES];
       // [self AFMySettings:@""];
    }

    
   
    
}
-(void)Click_on_notification_sound_btn:(UIButton *)sender
{
    NSLog(@"click_on_off_btn:");
    if([sender isSelected])
    {
        
         notification_soundl = 0;
        [sender setSelected:NO];
        // [self AFMySettings:@""];
    }
    else
    {
        
        
         notification_soundl = 1;
        [sender setSelected:YES];
        // [self AFMySettings:@""];
    }
    
    
    
    
}
-(void)Click_on_favorite_chef_seving_now_btn:(UIButton *)sender
{
    NSLog(@"click_on_off_btn:");
    if([sender isSelected])
    {
        
        favorite_chef_serving_now = 0;
        [sender setSelected:NO];
        // [self AFMySettings:@""];
    }
    else
    {
        
        
        favorite_chef_serving_now = 1;
        [sender setSelected:YES];
        // [self AFMySettings:@""];
    }
    
    
    
    
}
-(void)Click_on_favorite_dish_seving_now_btn:(UIButton *)sender
{
    NSLog(@"click_on_off_btn:");
    if([sender isSelected])
    {
        
        favorite_dish_serving_now = 0;
        [sender setSelected:NO];
        // [self AFMySettings:@""];
    }
    else
    {
        
        
        favorite_dish_serving_now = 1;
        [sender setSelected:YES];
        // [self AFMySettings:@""];
    }
    
    
    
    
}



-(void)click_on_btn:(UIButton *)sender
{
    NSLog(@"click_on_btn:");
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
