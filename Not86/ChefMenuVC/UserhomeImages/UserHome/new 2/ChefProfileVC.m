//
//  ChefProfileVC.m
//  Not86
//
//  Created by Interworld on 12/08/15.
//  Copyright (c) 2015 Interworld. All rights reserved.
//

#import "ChefProfileVC.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"



@interface ChefProfileVC ()<UITextViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIImageView * img_header;
    UIImageView * img_bg;
    UIImageView * img_strip;
    UIImageView * img_strip2;
    UIImageView * img_strip3;
    UIImageView * img_strip4;
    
    UIScrollView *scroll;
    
    NSMutableArray * ary_chef_info_titles;
    NSMutableArray * array_img;
    NSMutableArray * array_chef_details;
    NSMutableArray * ary_displaynames;
    NSMutableArray * array_serving_img;
    NSMutableArray * array_serving_time;
    NSMutableArray * array_serving_items;
    
    UIView * view_personal;
    UIView * view_menu;
    UIView * view_schedule;
    UIView * view_for_reviews;
    
    UITableView * table_menu;
    UITableView * img_table;
    UITableView * table_schedule;
    
    UIButton * btn_calender;
    UIButton * btn_menu;
    UIButton * btn_personal;
    UIButton * btn_on_reviews;
    
    UITextView  * txtview_adddescription;
    
    UILabel *lbl_min_order_value;
}

@end

@implementation ChefProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBody];
    
    // personal info array declaration
    
    ary_chef_info_titles =[[NSMutableArray alloc]initWithObjects:@"Username",@"Full Name",@"Chef Type",@"Kithen Name",@"Serving Type",@"Cooking Qualification",@"Cooking Experience", nil];
    array_img = [[NSMutableArray alloc]initWithObjects:@"user1-icon@2x.png",@"user1-icon@2x.png",@"food-icon@2x.png",@"kichan1-icon@2x.png",@"seving-type-icon@2x.png",@"cooking-Q-icon@2x.png",@"cooking-Ex-icon@2x.png",nil];
    array_chef_details = [[NSMutableArray alloc]initWithObjects:@"Charles1990",@"Charles dan",@"Home Chef",@"Doe's Kitchen",@"Dine-in,Takaway,Deliver",@"Diploma in Cooking Industry" ,@"6-10 Years",nil];
    
    //array declaration for menu
    
    ary_displaynames =[[NSMutableArray alloc]initWithObjects:@"Steamed Thai oath",@"Rasberry custored", nil];
    
    //array declaration for schedule
    
    array_serving_img = [[NSMutableArray alloc]initWithObjects:@"seving-type-icon@2x.png", @"seving-type-icon@2x.png",@"seving-type-icon@2x.png",nil];
    array_serving_time = [[NSMutableArray alloc]initWithObjects:@"10:00AM-12:00PM",@"02:00PM-05:00PM",@"06:00PM-10:00PM", nil];
    array_serving_items =[[NSMutableArray alloc]initWithObjects:@"Chicken 65,Chill Chicken",@"Chicken 65,Chill Chicken",@"Chicken 65,Chill Chicken", nil];
    
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}


-(void)integrateHeader
{
    
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu.frame = CGRectMake(10,13,20,20);
    icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu addTarget:self action:@selector(btn_menu_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_menu setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_menu];
    
    
    UILabel *lbl_my_profile = [[UILabel alloc]init];
    lbl_my_profile.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 150, 45);
    lbl_my_profile.text = @"Chef Profile";
    lbl_my_profile.font = [UIFont fontWithName:kFont size:20];
    lbl_my_profile.textColor = [UIColor whiteColor];
    lbl_my_profile.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_my_profile];
    
    UIImageView *icon_logo = [[UIImageView alloc]init];
    icon_logo.frame = CGRectMake(WIDTH-40, 9, 30, 30);
    [icon_logo setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_logo setUserInteractionEnabled:YES];
    [img_header addSubview:icon_logo];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}
-(void)integrateBody
{
    
    
    
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor clearColor];
    //  scroll.frame = CGRectMake(0, 51, WIDTH, 450);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    img_bg = [[UIImageView alloc]init];
    //   img_bg .frame = CGRectMake(0,CGRectGetMaxY( img_header.frame), WIDTH, 210);
    [img_bg  setImage:[UIImage imageNamed:@"bg2-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg  setUserInteractionEnabled:YES];
    [ scroll addSubview:img_bg ];

    
    
    UIButton *chef_img = [UIButton buttonWithType:UIButtonTypeCustom];
    //    chef_img.frame = CGRectMake(110,CGRectGetMaxY( img_header.frame)-30,95,95);
    chef_img.backgroundColor = [UIColor clearColor];
    [chef_img addTarget:self action:@selector(btn_img_user_click:) forControlEvents:UIControlEventTouchUpInside];
    [chef_img setImage:[UIImage imageNamed:@"chef3-img@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:chef_img];
    
    
    lbl_min_order_value = [[UILabel alloc]init];
    lbl_min_order_value.frame = CGRectMake(130,CGRectGetMaxY(chef_img.frame)+139, 200,45);
    lbl_min_order_value.text = @"Min order value: $15.00";
    lbl_min_order_value.font = [UIFont fontWithName:kFontBold size:12];
    lbl_min_order_value.textColor = [UIColor whiteColor];
    lbl_min_order_value.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_min_order_value];
    
    
    UILabel *lbl_chef_name = [[UILabel alloc]init];
    lbl_chef_name.frame = CGRectMake(150,CGRectGetMaxY(chef_img.frame)+155, 200,45);
    lbl_chef_name.text = @"Charles Dan";
    lbl_chef_name.font = [UIFont fontWithName:kFontBold size:15];
    lbl_chef_name.textColor = [UIColor whiteColor];
    lbl_chef_name.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_chef_name];
    
    
    
    UIButton *icon_hart = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_hart.frame = CGRectMake(10,CGRectGetMaxY( img_header.frame)-30,30,30);
    icon_hart.backgroundColor = [UIColor clearColor];
    [icon_hart addTarget:self action:@selector(btn_icon_hart_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_hart setImage:[UIImage imageNamed:@"icon-favorite@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:icon_hart];
    
    UIButton *img_black = [UIButton buttonWithType:UIButtonTypeCustom];
    //    img_black .frame = CGRectMake(260,CGRectGetMaxY( img_header.frame)-30,150,30);
    //img_black .backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:1];
    [img_black  addTarget:self action:@selector(btn_black_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_black  setImage:[UIImage imageNamed:@"black-img@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:img_black ];
    
    
    UIButton *icon_location = [UIButton buttonWithType:UIButtonTypeCustom];
    //    icon_location .frame = CGRectMake(0,0,30,30);
    icon_location .backgroundColor = [UIColor clearColor];
    [icon_location  addTarget:self action:@selector(btn_location_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_location  setImage:[UIImage imageNamed:@"icon-w-location@2x.png"] forState:UIControlStateNormal];
    [img_black  addSubview:icon_location ];
    
    
    UILabel *lbl_km = [[UILabel alloc]init];
    //   lbl_km.frame = CGRectMake(25,-5, 150,45);
    lbl_km.text = @"5 km";
    lbl_km.font = [UIFont fontWithName:kFontBold size:18];
    lbl_km.textColor = [UIColor whiteColor];
    lbl_km.backgroundColor = [UIColor clearColor];
    [img_black addSubview:lbl_km];
    
    
    
#pragma PERSONAL_INFO_BODY
    
    UIButton *icon_info = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_info.frame = CGRectMake(40,CGRectGetMaxY( img_header.frame)+100,30,30);
    icon_info.backgroundColor = [UIColor clearColor];
    [icon_info addTarget:self action:@selector(btn_location_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_info setImage:[UIImage imageNamed:@"info-icon@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:icon_info];
    
    
    UILabel *lbl_personal_info = [[UILabel alloc]init];
    //    lbl_personal_info.frame = CGRectMake(24,CGRectGetMaxY(icon_info .frame)-10, 75,40);
    lbl_personal_info.text = @"Personal Info";
    lbl_personal_info.font = [UIFont fontWithName:kFont size:12];
    lbl_personal_info.textColor = [UIColor whiteColor];
    lbl_personal_info.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_personal_info];
    
    img_strip = [[UIImageView alloc]init];
    //   img_strip.frame = CGRectMake(50,CGRectGetMaxY(lbl_personal_info.frame)+5, WIDTH/2-80, 3);
    [img_strip setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    //img_strip.backgroundColor = [UIColor redColor];
    [img_strip setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip];
    
    btn_personal = [UIButton buttonWithType:UIButtonTypeCustom];
    //    btn_personal.frame = CGRectMake(25,CGRectGetMaxY( img_header.frame)+100,70,65);
    btn_personal.backgroundColor = [UIColor clearColor];
    [   btn_personal addTarget:self action:@selector(btn_personal_info_strip_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg  addSubview:   btn_personal];
    
    
#pragma View_personal
    
    view_personal = [[UIView alloc]init];
    //    view_personal.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,300);
    view_personal.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_personal];
    
    UIImageView *img_bg1 = [[UIImageView alloc]init];
    //   img_bg1.frame = CGRectMake(0,10, WIDTH+5, 300);
    [img_bg1  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg1  setUserInteractionEnabled:YES];
    [ view_personal addSubview:img_bg1 ];
    
    txtview_adddescription = [[UITextView alloc]init];
    //   txtview_adddescription.frame = CGRectMake(20,20, 295,60);
    txtview_adddescription.scrollEnabled = YES;
    txtview_adddescription.userInteractionEnabled = YES;
    txtview_adddescription.font = [UIFont fontWithName:kFont size:12];
    txtview_adddescription.backgroundColor = [UIColor clearColor];
    txtview_adddescription.delegate = self;
    txtview_adddescription.textColor = [UIColor blackColor];
    txtview_adddescription.text =@"LOrem ipsumdotor sit amet.ne dicit virits corpor a per,ut illum liberavisse vet.At mei vidit conse cteture.in vitae definitionem per.tate vetear cliquado ei vim.";
    [img_bg1 addSubview:txtview_adddescription];
    
    UIImageView *img_line = [[UIImageView alloc]init];
    //   img_line .frame = CGRectMake(20,CGRectGetMaxY(txtview_adddescription.frame)+5, 280, 0.5);
    [img_line setImage:[UIImage imageNamed:@"line2@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_line setUserInteractionEnabled:YES];
    [img_bg1 addSubview:img_line ];
    
    
#pragma mark personal_Tableview
    
    img_table = [[UITableView alloc] init ];
    //    img_table.frame  = CGRectMake(13,CGRectGetMaxY(img_line.frame),294,200);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    img_table.backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:img_table];
    
    
#pragma MENU_BODY
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    //    icon_menu.frame = CGRectMake(120,CGRectGetMaxY( img_header.frame)+100,30,30);
    icon_menu.backgroundColor = [UIColor clearColor];
    [icon_menu addTarget:self action:@selector(btn_menu1_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_menu setImage:[UIImage imageNamed:@"menu1-icon@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:icon_menu];
    
    UILabel *lbl_menu = [[UILabel alloc]init];
    //    lbl_menu.frame = CGRectMake(120,CGRectGetMaxY(icon_menu.frame)-10, 50,35);
    lbl_menu.text = @"Menu";
    lbl_menu.font = [UIFont fontWithName:kFont size:12];
    lbl_menu.textColor = [UIColor whiteColor];
    lbl_menu.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_menu];
    
    img_strip2 = [[UIImageView alloc]init];
    //   img_strip2.frame = CGRectMake(50,CGRectGetMaxY(lbl_personal_info.frame)+5, WIDTH/2-80, 3);
    [img_strip2 setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    [img_strip2 setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip2];
    
    btn_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_menu.frame = CGRectMake(100,CGRectGetMaxY( img_header.frame)+100,60,65);
    btn_menu.backgroundColor = [UIColor clearColor];
    [  btn_menu addTarget:self action:@selector(btn_menu_strip_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg  addSubview:  btn_menu];
    
    
#pragma view_for_menu
    
    view_menu = [[UIView alloc]init];
    //    view_menu.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,300);
    view_menu.backgroundColor=[UIColor clearColor];
    [scroll addSubview:view_menu];
    
    UIImageView *img_bg2 = [[UIImageView alloc]init];
    //    img_bg2.frame = CGRectMake(0,10, WIDTH+5, 100);
    [img_bg2  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg2  setUserInteractionEnabled:YES];
    [ view_menu addSubview:img_bg2 ];
    
    UIImageView *img_line1 = [[UIImageView alloc]init];
    //   img_line1.frame =  CGRectMake(15,49, 280, 0.5);
    img_line1.backgroundColor =[UIColor grayColor];
    [img_line1 setUserInteractionEnabled:YES];
    [img_bg2 addSubview:img_line1];
    
    UIImageView *icon_cooked = [[UIImageView alloc]init];
    //   icon_cooked .frame = CGRectMake(20, 10, 30, 30);
    [icon_cooked  setImage:[UIImage imageNamed:@"cooked-icon@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_cooked  setUserInteractionEnabled:YES];
    [img_bg2 addSubview:icon_cooked ];
    
    UILabel *label_cooked = [[UILabel alloc]init];
    //    label_cooked.frame = CGRectMake(60,0, 150,40);
    label_cooked.text = @"Cuisines Cooked";
    label_cooked.font = [UIFont fontWithName:kFontBold size:12];
    label_cooked.textColor = [UIColor blackColor];
    label_cooked.backgroundColor = [UIColor clearColor];
    [img_bg2  addSubview:label_cooked];
    
    UILabel *label_Cooked_types = [[UILabel alloc]init];
    //   label_Cooked_types.frame = CGRectMake(60,15, 150,40);
    label_Cooked_types.text = @"Chainese,Mexican,Italian,Korean";
    label_Cooked_types.font = [UIFont fontWithName:kFont size:10];
    label_Cooked_types.textColor = [UIColor blackColor];
    label_Cooked_types.backgroundColor = [UIColor clearColor];
    [img_bg2  addSubview:label_Cooked_types];
    
    UIImageView *icon_diet = [[UIImageView alloc]init];
    //    icon_diet.frame = CGRectMake(20, 60, 30, 30);
    [icon_diet setImage:[UIImage imageNamed:@"diet-icon@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_diet setUserInteractionEnabled:YES];
    [img_bg2 addSubview:icon_diet];
    
    UILabel *label_dietary = [[UILabel alloc]init];
    //   label_dietary.frame = CGRectMake(60,45, 150,40);
    label_dietary.text = @"Dietary Restrictions";
    label_dietary.font = [UIFont fontWithName:kFontBold size:12];
    label_dietary.textColor = [UIColor blackColor];
    label_dietary.backgroundColor = [UIColor clearColor];
    [img_bg2  addSubview:label_dietary];
    
    UILabel *label_dietary_type = [[UILabel alloc]init];
    //   label_dietary_type.frame = CGRectMake(60,60, 150,40);
    label_dietary_type.text = @"Halal";
    label_dietary_type.font = [UIFont fontWithName:kFont size:10];
    label_dietary_type.textColor = [UIColor blackColor];
    label_dietary_type.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:label_dietary_type];
    
    
#pragma table_for_menu
    
    table_menu = [[UITableView alloc] init ];
    //   table_menu.frame  = CGRectMake(13,CGRectGetMaxY(img_bg2.frame),294,160);
    [table_menu setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_menu.delegate = self;
    table_menu.dataSource = self;
    table_menu.showsVerticalScrollIndicator = NO;
    table_menu.backgroundColor = [UIColor redColor];
    [view_menu addSubview:table_menu];
    
    
#pragma SCHEDULE_BODY
    
    UIButton *icon_calender = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_calender.frame = CGRectMake(190,CGRectGetMaxY( img_header.frame)+105,30,30);
    icon_calender.backgroundColor = [UIColor clearColor];
    [icon_calender addTarget:self action:@selector(btn_schedule_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_calender setImage:[UIImage imageNamed:@"calender-w-icon@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:icon_calender];
    
    UILabel *lbl_shedule = [[UILabel alloc]init];
    //   lbl_shedule .frame = CGRectMake(180,CGRectGetMaxY(icon_menu.frame)-10, 60,43);
    lbl_shedule.text = @"Schedule";
    lbl_shedule.font = [UIFont fontWithName:kFont size:12];
    lbl_shedule.textColor = [UIColor whiteColor];
    lbl_shedule.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_shedule];
    
    img_strip3 = [[UIImageView alloc]init];
    //    img_strip3.frame = CGRectMake(180,CGRectGetMaxY(lbl_shedule.frame)-5, WIDTH/2-100, 3);
    [img_strip3 setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    [img_strip3 setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip3];
    
    
    btn_calender = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_calender.frame = CGRectMake(180,CGRectGetMaxY( img_header.frame)+100,60,65);
    btn_calender.backgroundColor = [UIColor clearColor];
    [ btn_calender addTarget:self action:@selector(btn_schedule_strip_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg  addSubview: btn_calender];
    
    
#pragma view_for_schedule
    
    view_schedule = [[UIView alloc]init];
    //view_schedule.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,310);
    view_schedule.backgroundColor=[UIColor clearColor];
    [scroll addSubview:view_schedule];
    
    UIImageView *img_calender = [[UIImageView alloc]init];
    //  img_calender.frame = CGRectMake(0,10, WIDTH, 200);
    [img_calender  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_calender  setUserInteractionEnabled:YES];
    [ view_schedule addSubview:img_calender ];
    
    UIImageView *img_bg3 = [[UIImageView alloc]init];
    //  img_bg3.frame = CGRectMake(0,CGRectGetMaxY(img_calender.frame), WIDTH, 150);
    [img_bg3  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg3  setUserInteractionEnabled:YES];
    [ view_schedule addSubview:img_bg3 ];
    
    
#pragma table_for_schedule
    
    table_schedule = [[UITableView alloc] init ];
    //    table_schedule.frame  = CGRectMake(12,CGRectGetMaxY(img_bg2.frame)+105,290,160);
    [table_schedule setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_schedule.delegate = self;
    table_schedule.dataSource = self;
    table_schedule.showsVerticalScrollIndicator = NO;
    table_schedule.backgroundColor = [UIColor clearColor];
    [view_schedule addSubview:table_schedule];
    
    
#pragma REVIEW_BODY
    
    UIButton *icon_reviews = [UIButton buttonWithType:UIButtonTypeCustom];
    //  icon_reviews.frame = CGRectMake(260,CGRectGetMaxY( img_header.frame)+100,30,30);
    icon_reviews.backgroundColor = [UIColor clearColor];
    [icon_reviews addTarget:self action:@selector(btn_reviews_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_reviews setImage:[UIImage imageNamed:@"reviews-w-icon@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:icon_reviews];
    
    UILabel *label_reviews = [[UILabel alloc]init];
    //   label_reviews.frame = CGRectMake(260,CGRectGetMaxY(icon_reviews.frame)-10, 150,45);
    label_reviews.text = @"Reviews";
    label_reviews.font = [UIFont fontWithName:kFont size:12];
    label_reviews.textColor = [UIColor whiteColor];
    label_reviews.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:label_reviews];
    
    img_strip4 = [[UIImageView alloc]init];
    //   img_strip4.frame = CGRectMake(260,CGRectGetMaxY(lbl_shedule.frame)-5, WIDTH/2-100, 3);
    [img_strip4 setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    [img_strip4 setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip4];
    
    btn_on_reviews = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_on_reviews.frame = CGRectMake(260,CGRectGetMaxY( img_header.frame)+100,60,65);
    btn_on_reviews.backgroundColor = [UIColor clearColor];
    [ btn_on_reviews addTarget:self action:@selector(btn_reviews_strip_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg  addSubview:btn_on_reviews];
    
#pragma view_for_reviews
    
    view_for_reviews = [[UIView alloc]init];
    view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame)+255,WIDTH,300);
    view_for_reviews.backgroundColor=[UIColor clearColor];
    [scroll addSubview:view_for_reviews];
    
    
    UIImageView *img_bg_in_reviews = [[UIImageView alloc]init];
    img_bg_in_reviews.frame = CGRectMake(10,20, WIDTH-20, 250);
    [img_bg_in_reviews  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    [img_bg_in_reviews  setUserInteractionEnabled:YES];
    [ view_for_reviews addSubview:img_bg_in_reviews ];
    
    UILabel *label_favorite_by_user = [[UILabel alloc]init];
    label_favorite_by_user.frame = CGRectMake(10,10, 250,30);
    label_favorite_by_user.text = @"Chef Favorite by Users";
    label_favorite_by_user.font = [UIFont fontWithName:kFont size:15];
    label_favorite_by_user.textColor = [UIColor blackColor];
    label_favorite_by_user.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_favorite_by_user];
    
    //    UITextView * txtview_by_user_value = [[UITextView alloc]init];
    //    txtview_by_user_value.frame = CGRectMake(230,10, 295,60);
    //    txtview_by_user_value.scrollEnabled = YES;
    //    txtview_by_user_value.userInteractionEnabled = YES;
    //    txtview_by_user_value.font = [UIFont fontWithName:kFont size:18];
    //    txtview_by_user_value.backgroundColor = [UIColor clearColor];
    //    txtview_by_user_value.delegate = self;
    //    txtview_by_user_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:   1];
    //  txtview_by_user_value.text =@"1677 ";
    //    [img_bg_in_reviews addSubview:txtview_by_user_value];
    
    
    UILabel *label_favorite_value = [[UILabel alloc]init];
    label_favorite_value.frame = CGRectMake(230,10, 295,60);
    label_favorite_value.text = @"1677";
    label_favorite_value.font = [UIFont fontWithName:kFontBold size:20];
    label_favorite_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:   1];
    label_favorite_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_favorite_value];
    
    
    
    UIImageView *img_line_in_reviews = [[UIImageView alloc]init];
    img_line_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_favorite_by_user.frame)+10, WIDTH-50, 0.5);
    [img_line_in_reviews  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line_in_reviews  setUserInteractionEnabled:YES];
    [img_bg_in_reviews addSubview:img_line_in_reviews];
    
    UILabel *label_total_reviews = [[UILabel alloc]init];
    label_total_reviews.frame = CGRectMake(10,CGRectGetMaxY(img_line_in_reviews.frame)+10, 250,30);
    label_total_reviews.text = @"Total Reviews";
    label_total_reviews.font = [UIFont fontWithName:kFont size:15];
    label_total_reviews.textColor = [UIColor blackColor];
    label_total_reviews.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_total_reviews];
    
    //    UITextView * txtview_total_reviwes_value = [[UITextView alloc]init];
    //    txtview_total_reviwes_value.frame = CGRectMake(210,CGRectGetMaxY(img_line_in_reviews.frame)+10, 150,60);
    //    txtview_total_reviwes_value.scrollEnabled = YES;
    //    txtview_total_reviwes_value.userInteractionEnabled = YES;
    //    txtview_total_reviwes_value.font = [UIFont fontWithName:kFont size:18];
    //    txtview_total_reviwes_value.backgroundColor = [UIColor clearColor];
    //    txtview_total_reviwes_value.delegate = self;
    //    txtview_total_reviwes_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    //    txtview_total_reviwes_value.text =@"118 ";
    //    [img_bg_in_reviews addSubview:txtview_total_reviwes_value];
    
    UILabel *label_total_reviews_value = [[UILabel alloc]init];
    label_total_reviews_value.frame = CGRectMake(10,CGRectGetMaxY(img_line_in_reviews.frame)+10, 250,30);
    label_total_reviews_value.text = @"118";
    label_total_reviews_value.font = [UIFont fontWithName:kFontBold size:20];
    label_total_reviews_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    label_total_reviews_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_total_reviews_value];
    
    
    UIButton *icon_red_right_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_red_right_arrow.frame = CGRectMake(260,CGRectGetMaxY( img_line_in_reviews.frame)+15,20,20);
    icon_red_right_arrow.backgroundColor = [UIColor clearColor];
    [icon_red_right_arrow addTarget:self action:@selector(btn_reviews_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_red_right_arrow setImage:[UIImage imageNamed:@"red-right-arrow@2x.png"] forState:UIControlStateNormal];
    [img_bg_in_reviews  addSubview:icon_red_right_arrow];
    
    
    
    
    UIImageView *img_line2_in_reviews = [[UIImageView alloc]init];
    img_line2_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_total_reviews.frame)+10, WIDTH-50, 0.5);
    [img_line2_in_reviews  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line2_in_reviews  setUserInteractionEnabled:YES];
    [img_bg_in_reviews addSubview:img_line2_in_reviews];
    
    UILabel *label_overall_rating = [[UILabel alloc]init];
    label_overall_rating.frame = CGRectMake(10,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 250,30);
    label_overall_rating.text = @"Overall Ratings";
    label_overall_rating.font = [UIFont fontWithName:kFont size:15];
    label_overall_rating.textColor = [UIColor blackColor];
    label_overall_rating.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_overall_rating];
    
    
    UIImageView *img_thumb = [[UIImageView alloc]init];
    img_thumb .frame = CGRectMake(200,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 30,30);
    [img_thumb   setImage:[UIImage imageNamed:@"thumb-icon@2x.png"]];
    [img_thumb   setUserInteractionEnabled:YES];
    [img_bg_in_reviews addSubview:img_thumb ];
    
    //    UITextView * txtview_voverall_rating_value = [[UITextView alloc]init];
    //    txtview_voverall_rating_value.frame = CGRectMake(230,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 150,60);
    //    txtview_voverall_rating_value.scrollEnabled = YES;
    //    txtview_voverall_rating_value.userInteractionEnabled = YES;
    //    txtview_voverall_rating_value.font = [UIFont fontWithName:kFontBold size:15];
    //    txtview_voverall_rating_value.backgroundColor = [UIColor clearColor];
    //    txtview_voverall_rating_value.delegate = self;
    //    txtview_voverall_rating_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    //    txtview_voverall_rating_value.text =@"87.4% ";
    //    [img_bg_in_reviews addSubview:txtview_voverall_rating_value];
    
    
    UILabel *label_overall_rating_value = [[UILabel alloc]init];
    label_overall_rating_value.frame = CGRectMake(230,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 150,60);
    label_overall_rating_value.text = @"87.4%";
    label_overall_rating_value.font = [UIFont fontWithName:kFontBold size:20];
    label_overall_rating_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    label_overall_rating_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_overall_rating_value];
    
    
    
    
    
    UIImageView *img_line3_in_reviews = [[UIImageView alloc]init];
    img_line3_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_overall_rating.frame)+10, WIDTH-50, 0.5);
    [img_line3_in_reviews  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line3_in_reviews  setUserInteractionEnabled:YES];
    [img_bg_in_reviews addSubview:img_line3_in_reviews];
    
    UILabel *label_food_ratings = [[UILabel alloc]init];
    label_food_ratings.frame = CGRectMake(10,CGRectGetMaxY(img_line3_in_reviews.frame)+10, 250,30);
    label_food_ratings.text = @"Food Ratings";
    label_food_ratings.font = [UIFont fontWithName:kFont size:15];
    label_food_ratings.textColor = [UIColor blackColor];
    label_food_ratings.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_food_ratings];
    
    UILabel *label_taste = [[UILabel alloc]init];
    label_taste.frame = CGRectMake(10,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
    label_taste.text = @"Taste:";
    label_taste.font = [UIFont fontWithName:kFont size:12];
    label_taste.textColor = [UIColor blackColor];
    label_taste.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_taste];
    
    UILabel *taste_value = [[UILabel alloc]init];
    taste_value.frame = CGRectMake(50,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
    taste_value.text = @"75%";
    taste_value.font = [UIFont fontWithName:kFont size:15];
    taste_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];    taste_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:taste_value];
    
    UILabel *label_appeal = [[UILabel alloc]init];
    label_appeal.frame = CGRectMake(90,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
    label_appeal.text = @"Appeal:";
    label_appeal.font = [UIFont fontWithName:kFont size:15];
    label_appeal.textColor = [UIColor blackColor];
    label_appeal.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_appeal];
    
    UILabel *appeal_value = [[UILabel alloc]init];
    appeal_value.frame = CGRectMake(150,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
    appeal_value.text = @"85%";
    appeal_value.font = [UIFont fontWithName:@"Arial" size:15];
    appeal_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    appeal_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:appeal_value];
    
    UILabel *label_value = [[UILabel alloc]init];
    label_value.frame = CGRectMake(200,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
    label_value.text = @"Value:";
    label_value.font = [UIFont fontWithName:kFont size:15];
    label_value.textColor = [UIColor blackColor];
    label_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_value];
    
    UILabel *value_value = [[UILabel alloc]init];
    value_value.frame = CGRectMake(250,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
    value_value.text = @"75%";
    value_value.font = [UIFont fontWithName:kFont size:15];
    value_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];    value_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:value_value];
    
    
    
    view_personal.hidden = NO;
    view_menu.hidden = YES;
    view_schedule.hidden = YES;
    view_for_reviews.hidden = YES;
    
    img_strip.hidden = NO;
    img_strip2.hidden = YES;
    img_strip3.hidden = YES;
    img_strip4.hidden = YES;
    
    
    
    if (IS_IPHONE_6Plus)
    {
        
        scroll.frame = CGRectMake(0, 45, WIDTH, 760);
        img_bg .frame = CGRectMake(0,0, WIDTH, 245);
        chef_img.frame = CGRectMake(110,CGRectGetMaxY( img_header.frame)-50,175,175);
        //        lbl_min_order_value.frame = CGRectMake(130,CGRectGetMaxY(chef_img.frame)+139, 200,45);
        //        lbl_chef_name.frame = CGRectMake(150,CGRectGetMaxY(chef_img.frame)+155, 200,45);
        icon_hart.frame = CGRectMake(10,CGRectGetMaxY( img_header.frame)-30,30,30);
        img_black .frame = CGRectMake(320,CGRectGetMaxY( img_header.frame)-30,150,30);
        icon_location .frame = CGRectMake(0,0,30,30);
        lbl_km.frame = CGRectMake(25,-8, 100,45);
        icon_info.frame = CGRectMake(40,CGRectGetMaxY( img_header.frame)+150,30,25);
        lbl_personal_info.frame = CGRectMake(24,CGRectGetMaxY(icon_info .frame)-10, 75,40);
        img_strip.frame = CGRectMake(0,CGRectGetMaxY(lbl_personal_info.frame)-9, WIDTH/2-90, 4);
        btn_personal.frame = CGRectMake(0,CGRectGetMaxY( img_header.frame)+150,110,60);
        icon_menu.frame = CGRectMake(150,CGRectGetMaxY( img_header.frame)+150,30,25);
        lbl_menu.frame = CGRectMake(150,CGRectGetMaxY(icon_menu.frame)-8, 50,35);
        img_strip2.frame = CGRectMake(140,CGRectGetMaxY(lbl_personal_info.frame)-9, WIDTH/2-135, 4);
        btn_menu.frame = CGRectMake(110,CGRectGetMaxY( img_header.frame)+150,120,60);
        icon_calender.frame = CGRectMake(240,CGRectGetMaxY( img_header.frame)+149,32,25);
        lbl_shedule .frame = CGRectMake(230,CGRectGetMaxY(icon_menu.frame)-13, 60,43);
        img_strip3.frame = CGRectMake(230,CGRectGetMaxY(lbl_shedule.frame)-9, WIDTH/2-140, 4);
        btn_calender.frame = CGRectMake(230,CGRectGetMaxY( img_header.frame)+150,100,60);
        icon_reviews.frame = CGRectMake(330,CGRectGetMaxY( img_header.frame)+150,30,25);
        label_reviews.frame = CGRectMake(330,CGRectGetMaxY(icon_menu.frame)-13, 150,45);
        img_strip4.frame = CGRectMake(330,CGRectGetMaxY(lbl_shedule.frame)-9, WIDTH/2-125, 4);
        btn_on_reviews.frame = CGRectMake(330,CGRectGetMaxY( img_header.frame)+150,100,60);
        
        view_personal.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,300);
        img_bg1.frame = CGRectMake(0,10, WIDTH+5, 450);
        txtview_adddescription.frame = CGRectMake(20,20, WIDTH-40,60);
        img_line .frame = CGRectMake(20,CGRectGetMaxY(txtview_adddescription.frame)+5, WIDTH-50, 0.5);
        img_table.frame  = CGRectMake(15,CGRectGetMaxY(img_line.frame),WIDTH-33,340);
        
        view_menu.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,300);
        img_bg2.frame = CGRectMake(0,10, WIDTH+5, 100);
        img_line1.frame =  CGRectMake(18,49, WIDTH-40, 0.5);
        icon_cooked .frame = CGRectMake(20, 10, 30, 30);
        label_cooked.frame = CGRectMake(60,0, 150,40);
        label_Cooked_types.frame = CGRectMake(60,15, 150,40);
        icon_diet.frame = CGRectMake(20, 60, 30, 30);
        label_dietary.frame = CGRectMake(60,45, 150,40);
        label_dietary_type.frame = CGRectMake(60,60, 150,40);
        table_menu.frame  = CGRectMake(13,CGRectGetMaxY(img_bg2.frame),WIDTH-30,300);
        
        view_schedule.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,HEIGHT);
        img_calender.frame = CGRectMake(0,10, WIDTH, 200);
        img_bg3.frame = CGRectMake(0,CGRectGetMaxY(img_calender.frame), WIDTH, 170);
        table_schedule.frame  = CGRectMake(12,CGRectGetMaxY(img_bg2.frame)+105,WIDTH-40,160);
        
        view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,300);
        img_bg_in_reviews.frame = CGRectMake(10,20, WIDTH-20, 250);
        label_favorite_by_user.frame = CGRectMake(10,10, 250,30);
        label_favorite_value.frame = CGRectMake(WIDTH-90,0, 295,60);
        img_line_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_favorite_by_user.frame)+10, WIDTH-50, 0.5);
        label_total_reviews.frame = CGRectMake(10,CGRectGetMaxY(img_line_in_reviews.frame)+10, 250,30);
        label_total_reviews_value.frame = CGRectMake(WIDTH-120,CGRectGetMaxY(img_line_in_reviews.frame),150,60);
        icon_red_right_arrow.frame = CGRectMake(WIDTH-65,CGRectGetMaxY( img_line_in_reviews.frame)+20,20,20);
        img_line2_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_total_reviews.frame)+10, WIDTH-50, 0.5);
        label_overall_rating.frame = CGRectMake(10,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 250,30);
        img_thumb .frame = CGRectMake(275,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 30,30);
        label_overall_rating_value.frame = CGRectMake(315,CGRectGetMaxY(img_line2_in_reviews.frame), 150,60);
        img_line3_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_overall_rating.frame)+10, WIDTH-50, 0.5);
        label_food_ratings.frame = CGRectMake(10,CGRectGetMaxY(img_line3_in_reviews.frame)+10, 250,30);
        label_taste.frame = CGRectMake(10,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
        taste_value.frame = CGRectMake(50,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
        label_appeal.frame = CGRectMake(150,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
        appeal_value.frame = CGRectMake(210,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
        label_value.frame = CGRectMake(290,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
        value_value.frame = CGRectMake(340,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
        
    }
    else if (IS_IPHONE_6)
    {
        scroll.frame = CGRectMake(0, 45, WIDTH, 650);
        img_bg .frame = CGRectMake(0,0, WIDTH, 215);
        chef_img.frame = CGRectMake(130,CGRectGetMaxY( img_header.frame)-30,95,95);
        lbl_min_order_value.frame = CGRectMake(120,CGRectGetMaxY(chef_img.frame)-15, 200,45);
        lbl_chef_name.frame = CGRectMake(140,CGRectGetMaxY(chef_img.frame)+5, 200,45);
        icon_hart.frame = CGRectMake(10,CGRectGetMaxY( img_header.frame)-30,30,30);
        img_black .frame = CGRectMake(260,CGRectGetMaxY( img_header.frame)-30,150,30);
        icon_location .frame = CGRectMake(0,0,30,30);
        lbl_km.frame = CGRectMake(30,-8, 150,45);
        
        icon_info.frame = CGRectMake(40,CGRectGetMaxY( img_header.frame)+110,36,30);
        lbl_personal_info.frame = CGRectMake(24,CGRectGetMaxY(icon_info .frame)-8, 75,40);
        img_strip.frame = CGRectMake(0,CGRectGetMaxY(icon_info.frame)+26, WIDTH/2-80, 4);
        btn_personal.frame = CGRectMake(25,CGRectGetMaxY( img_header.frame)+100,70,65);
        
        icon_menu.frame = CGRectMake(140,CGRectGetMaxY( img_header.frame)+113,35,30);
        lbl_menu.frame = CGRectMake(140,CGRectGetMaxY(icon_menu.frame)-8, 50,35);
        img_strip2.frame = CGRectMake(130,CGRectGetMaxY(icon_menu.frame)+23, WIDTH/2-130, 4);
        btn_menu.frame = CGRectMake(145,CGRectGetMaxY( img_header.frame)+100,60,65);
        
        icon_calender.frame = CGRectMake(230,CGRectGetMaxY( img_header.frame)+111,35,30);
        lbl_shedule .frame = CGRectMake(225,CGRectGetMaxY(icon_menu.frame)-10, 60,43);
        img_strip3.frame = CGRectMake(220,CGRectGetMaxY(lbl_shedule.frame)-10, WIDTH/2-120, 4);
        btn_calender.frame = CGRectMake(220,CGRectGetMaxY( img_header.frame)+100,60,65);
        
        icon_reviews.frame = CGRectMake(320,CGRectGetMaxY( img_header.frame)+113,35,30);
        label_reviews.frame = CGRectMake(315,CGRectGetMaxY(icon_menu.frame)-12, 150,45);
        img_strip4.frame = CGRectMake(305,CGRectGetMaxY(icon_reviews.frame)+23, WIDTH/2-120, 4);
        btn_on_reviews.frame = CGRectMake(290,CGRectGetMaxY( img_header.frame)+100,60,65);
        
        view_personal.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,300);
        img_bg1.frame = CGRectMake(0,10, WIDTH+5, 400);
        txtview_adddescription.frame = CGRectMake(20,20, 330,60);
        img_line .frame = CGRectMake(20,CGRectGetMaxY(txtview_adddescription.frame)+5, 310, 0.5);
        img_table.frame  = CGRectMake(13,CGRectGetMaxY(img_line.frame),WIDTH-40,290);
        
        view_menu.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,300);
        img_bg2.frame = CGRectMake(0,10, WIDTH+5, 100);
        img_line1.frame =  CGRectMake(15,49, WIDTH-40, 0.5);
        icon_cooked .frame = CGRectMake(20, 10, 30, 30);
        label_cooked.frame = CGRectMake(60,0, 150,40);
        label_Cooked_types.frame = CGRectMake(60,15, 300,40);
        icon_diet.frame = CGRectMake(20, 60, 30, 30);
        label_dietary.frame = CGRectMake(60,45, 150,40);
        label_dietary_type.frame = CGRectMake(60,60, 150,40);
        table_menu.frame  = CGRectMake(5,CGRectGetMaxY(img_bg2.frame),WIDTH-10,275);
        
        view_schedule.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,HEIGHT);
        img_calender.frame = CGRectMake(0,10, WIDTH, 200);
        img_bg3.frame = CGRectMake(0,CGRectGetMaxY(img_calender.frame), WIDTH, 170);
        table_schedule.frame  = CGRectMake(13,CGRectGetMaxY(img_bg2.frame)+104,WIDTH,150);
        
        
        view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,300);
        img_bg_in_reviews.frame = CGRectMake(10,20, WIDTH-20, 250);
        label_favorite_by_user.frame = CGRectMake(10,10, 250,30);
        label_favorite_value.frame = CGRectMake(WIDTH-86,0, 295,60);
        img_line_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_favorite_by_user.frame)+10, WIDTH-50, 0.5);
        label_total_reviews.frame = CGRectMake(10,CGRectGetMaxY(img_line_in_reviews.frame)+10, 250,30);
        label_total_reviews_value.frame = CGRectMake(WIDTH-120,CGRectGetMaxY(img_line_in_reviews.frame), 150,60);
        icon_red_right_arrow.frame = CGRectMake(WIDTH-65,CGRectGetMaxY( img_line_in_reviews.frame)+17,20,23);
        img_line2_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_total_reviews.frame)+10, WIDTH-50, 0.5);
        label_overall_rating.frame = CGRectMake(10,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 250,30);
        img_thumb .frame = CGRectMake(240,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 30,30);
        label_overall_rating_value.frame = CGRectMake(285,CGRectGetMaxY(img_line2_in_reviews.frame), 150,60);
        img_line3_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_overall_rating.frame)+10, WIDTH-50, 0.5);
        label_food_ratings.frame = CGRectMake(10,CGRectGetMaxY(img_line3_in_reviews.frame)+10, 250,30);
        label_taste.frame = CGRectMake(10,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
        taste_value.frame = CGRectMake(50,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
        label_appeal.frame = CGRectMake(130,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
        appeal_value.frame = CGRectMake(195,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
        label_value.frame = CGRectMake(260,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
        value_value.frame = CGRectMake(310,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
        
    }
    else
    {
        scroll.frame = CGRectMake(0, 45, WIDTH, 510);
        img_bg .frame = CGRectMake(0,0, WIDTH, 205);
        chef_img.frame = CGRectMake(110,CGRectGetMaxY( img_header.frame)-35,95,95);
        lbl_min_order_value.frame = CGRectMake(100,CGRectGetMaxY(chef_img.frame)-13, 200,45);
        lbl_chef_name.frame = CGRectMake(120,CGRectGetMaxY(chef_img.frame)+2, 200,45);
        icon_hart.frame = CGRectMake(10,CGRectGetMaxY( img_header.frame)-30,30,30);
        img_black .frame = CGRectMake(240,CGRectGetMaxY( img_header.frame)-30,150,30);
        icon_location .frame = CGRectMake(0,0,30,30);
        lbl_km.frame = CGRectMake(25,-8, 150,45);
        icon_info.frame = CGRectMake(35,CGRectGetMaxY( img_header.frame)+100,30,30);
        lbl_personal_info.frame = CGRectMake(18,CGRectGetMaxY(icon_info .frame)-10, 75,40);
        img_strip.frame = CGRectMake(0,CGRectGetMaxY(lbl_personal_info.frame)-3, WIDTH/2-70, 4);
        btn_personal.frame = CGRectMake(25,CGRectGetMaxY( img_header.frame)+100,70,65);
        
        icon_menu.frame = CGRectMake(120,CGRectGetMaxY( img_header.frame)+100,30,30);
        lbl_menu.frame = CGRectMake(120,CGRectGetMaxY(icon_menu.frame)-7, 50,35);
        img_strip2.frame = CGRectMake(100,CGRectGetMaxY(lbl_personal_info.frame)-3, WIDTH/2-90, 4);
        btn_menu.frame = CGRectMake(100,CGRectGetMaxY( img_header.frame)+100,60,65);
        
        icon_calender.frame = CGRectMake(190,CGRectGetMaxY( img_header.frame)+100,30,30);
        lbl_shedule .frame = CGRectMake(180,CGRectGetMaxY(icon_menu.frame)-10, 60,43);
        img_strip3.frame = CGRectMake(180,CGRectGetMaxY(lbl_shedule.frame)-5, WIDTH/2-100, 3);
        btn_calender.frame = CGRectMake(180,CGRectGetMaxY( img_header.frame)+100,60,65);
        
        icon_reviews.frame = CGRectMake(260,CGRectGetMaxY( img_header.frame)+100,30,30);
        label_reviews.frame = CGRectMake(260,CGRectGetMaxY(icon_menu.frame)-10, 150,45);
        img_strip4.frame = CGRectMake(255,CGRectGetMaxY(lbl_shedule.frame)-5, WIDTH/2-95, 3);
        btn_on_reviews.frame = CGRectMake(260,CGRectGetMaxY( img_header.frame)+100,60,65);
        
        view_personal.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,300);
        img_bg1.frame = CGRectMake(0,10, WIDTH+5, 300);
        txtview_adddescription.frame = CGRectMake(16,20, 295,60);
        img_line .frame = CGRectMake(20,CGRectGetMaxY(txtview_adddescription.frame)+5, 280, 0.5);
        img_table.frame  = CGRectMake(13,CGRectGetMaxY(img_line.frame),335,IS_IPHONE_5?196:130);
        
        view_menu.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,300);
        img_bg2.frame = CGRectMake(0,10, WIDTH+5, 100);
        img_line1.frame =  CGRectMake(15,49, 280, 0.5);
        icon_cooked .frame = CGRectMake(20, 10, 30, 30);
        label_cooked.frame = CGRectMake(60,0, 150,40);
        label_Cooked_types.frame = CGRectMake(60,15, 150,40);
        icon_diet.frame = CGRectMake(20, 60, 30, 30);
        label_dietary.frame = CGRectMake(60,45, 150,40);
        label_dietary_type.frame = CGRectMake(60,60, 150,40);
        table_menu.frame  = CGRectMake(13,CGRectGetMaxY(img_bg2.frame),294,200);
        
        view_schedule.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,310);
        img_calender.frame = CGRectMake(0,10, WIDTH, 200);
        img_bg3.frame = CGRectMake(0,CGRectGetMaxY(img_calender.frame), WIDTH, 150);
        table_schedule.frame  = CGRectMake(12,CGRectGetMaxY(img_bg2.frame)+105,290,150);
        
        view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,300);
        img_bg_in_reviews.frame = CGRectMake(10,20, WIDTH-20, 250);
        label_favorite_by_user.frame = CGRectMake(10,10, 250,30);
        label_favorite_value.frame = CGRectMake(230,0, 295,60);
        img_line_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_favorite_by_user.frame)+10, WIDTH-50, 0.5);
        label_total_reviews.frame = CGRectMake(10,CGRectGetMaxY(img_line_in_reviews.frame)+10, 250,30);
        label_total_reviews_value.frame = CGRectMake(210,CGRectGetMaxY(img_line_in_reviews.frame), 150,60);
        icon_red_right_arrow.frame = CGRectMake(260,CGRectGetMaxY( img_line_in_reviews.frame)+20,20,20);
        img_line2_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_total_reviews.frame)+10, WIDTH-50, 0.5);
        label_overall_rating.frame = CGRectMake(10,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 250,30);
        img_thumb .frame = CGRectMake(190,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 30,30);
        label_overall_rating_value.frame = CGRectMake(230,CGRectGetMaxY(img_line2_in_reviews.frame), 150,60);
        img_line3_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_overall_rating.frame)+10, WIDTH-50, 0.5);
        label_food_ratings.frame = CGRectMake(10,CGRectGetMaxY(img_line3_in_reviews.frame)+10, 250,30);
        label_taste.frame = CGRectMake(10,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
        taste_value.frame = CGRectMake(50,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
        label_appeal.frame = CGRectMake(98,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
        appeal_value.frame = CGRectMake(160,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
        label_value.frame = CGRectMake(205,CGRectGetMaxY(label_food_ratings.frame)+11, 250,30);
        value_value.frame = CGRectMake(255,CGRectGetMaxY(label_food_ratings.frame)+10, 250,30);
        
    }
    
    
    [scroll setContentSize:CGSizeMake(0,0)];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView == img_table)
    {
        return [ary_chef_info_titles count];
    }
    else if (tableView == table_menu)
    {
        
        return [ary_displaynames count];
    }
    else if(tableView == table_schedule)
    {
        return [array_serving_img count];
    }
    return 0;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table_menu)
    {
        return 165;
        
    }
    else if (tableView == table_schedule)
    {
        return 50;
    }
    else
    {
        return 50;
        
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    
    if(tableView == table_menu)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        //      img_cellBackGnd.frame =  CGRectMake(0,15, 300, 160);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        
        UIImageView *img_dish = [[UIImageView alloc] init];
        //      img_dish.frame = CGRectMake(0,7, 90,  95 );
        [img_dish setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        [img_cellBackGnd addSubview:img_dish];
        
        UIButton *icon_delete = [UIButton buttonWithType:UIButtonTypeCustom];
        //       icon_delete.frame = CGRectMake(250,20, 40, 40);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_delete addTarget:self action:@selector(btn_delete_click:) forControlEvents:UIControlEventTouchUpInside];
        [icon_delete setImage:[UIImage imageNamed:@"img-cart@2x.png"] forState:UIControlStateNormal];
        [cell.contentView   addSubview:icon_delete];
        
        UILabel *dish_name = [[UILabel alloc]init];
        //     dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
        dish_name.text = [NSString stringWithFormat:@"%@",[ary_displaynames objectAtIndex:indexPath.row]];
        dish_name.font = [UIFont fontWithName:kFontBold size:15];
        dish_name.textColor = [UIColor blackColor];
        dish_name.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:dish_name];
        
        
        UIImageView *icon_location = [[UIImageView alloc] init];
        //    icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        [icon_location setImage:[UIImage imageNamed:@"location-icon@2x.png"]];
        [img_cellBackGnd  addSubview:icon_location];
        
        UILabel *meters = [[UILabel alloc]init];
        //      meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        meters.text = @"5km";
        meters.font = [UIFont fontWithName:kFont size:15];
        meters.textColor = [UIColor blackColor];
        meters.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:meters];
        
        UIButton *icon_server = [UIButton buttonWithType:UIButtonTypeCustom];
        //     icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
        //icon_server .backgroundColor = [UIColor clearColor];
        [icon_server  addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [icon_server setImage:[UIImage imageNamed:@"icon-meal.png"]forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_server];
        
        UIButton *icon_halal = [UIButton buttonWithType:UIButtonTypeCustom];
        //     icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
        //icon_halal .backgroundColor = [UIColor clearColor];
        [icon_halal addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [icon_halal setImage:[UIImage imageNamed:@"halal-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_halal];
        
        UIImageView *img_non_veg = [[UIImageView alloc] init];
        //    img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
        [img_non_veg setImage:[UIImage imageNamed:@"non-veg bt@2x.png"]];
        [img_cellBackGnd addSubview:img_non_veg];
        
        UIButton *icon_cow = [UIButton buttonWithType:UIButtonTypeCustom];
        //    icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_cow addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [icon_cow setImage:[UIImage imageNamed:@"cow-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_cow];
        
        UIButton *icon_fronce = [UIButton buttonWithType:UIButtonTypeCustom];
        //    icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_fronce addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [icon_fronce setImage:[UIImage imageNamed:@"fronce-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_fronce];
        
        UILabel *doller_rate = [[UILabel alloc]init];
        //     doller_rate.frame = CGRectMake(CGRectGetMaxX(icon_fronce.frame)+25,85,200, 15);
        doller_rate.text = @"$14.90";
        doller_rate.font = [UIFont fontWithName:kFontBold size:15];
        doller_rate.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        dish_name.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:doller_rate];
        
        
        
        
        UIImageView *img_line = [[UIImageView alloc] init];
        //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
        [img_line setImage:[UIImage imageNamed:@"img-line@2x.png"]];
        [img_cellBackGnd addSubview:img_line];
        
        
        UIButton *icon_take = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        //icon_take .backgroundColor = [UIColor clearColor];
        [icon_take addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [icon_take setImage:[UIImage imageNamed:@"take-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_take];
        
        UIButton *icon_deliver = [UIButton buttonWithType:UIButtonTypeCustom];
        //    icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //icon_deliver .backgroundColor = [UIColor clearColor];
        [icon_deliver addTarget:self action:@selector(icon_Food_Later_BtnClick1:) forControlEvents:UIControlEventTouchUpInside];
        [icon_deliver setImage:[UIImage imageNamed:@"deliver-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_deliver];
        
        
        UIButton *img_btn_seving_Now = [UIButton buttonWithType:UIButtonTypeCustom];
        //   img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //img_btn_deliver_Now .backgroundColor = [UIColor clearColor];
        [img_btn_seving_Now addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_seving_Now setImage:[UIImage imageNamed:@"now-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_seving_Now];
        
        
        UIButton *img_btn_chef_menu = [UIButton buttonWithType:UIButtonTypeCustom];
        //    img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
        [img_btn_chef_menu addTarget:self action:@selector(btn_chef_menu_click:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_chef_menu setImage:[UIImage imageNamed:@"chef-menu-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_chef_menu];
        
        
        UIButton *icon_thumb = [UIButton buttonWithType:UIButtonTypeCustom];
        //    icon_thumb.frame = CGRectMake(235,CGRectGetMaxY(img_line.frame)+8, 20, 20);
        //icon_thumb .backgroundColor = [UIColor clearColor];
        [icon_thumb addTarget:self action:@selector(icon_thumb_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [icon_thumb setImage:[UIImage imageNamed:@"thumb-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_thumb];
        
        UILabel *likes = [[UILabel alloc]init];
        //    likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,125,200, 10);
        likes.text = @"87.4%";
        likes.font = [UIFont fontWithName:kFont size:10];
        likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        likes.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:likes];
        
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-25, 160);
            img_dish.frame = CGRectMake(5,7, 90,  95 );
            icon_delete.frame = CGRectMake(WIDTH-80,20, 40, 40);
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(CGRectGetMaxX(icon_fronce.frame)+110,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+15, WIDTH-60, 0.5 );
            icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(110,CGRectGetMaxY(img_line.frame)+5,  100, 30);
            img_btn_chef_menu.frame = CGRectMake(170,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
            
            icon_thumb.frame = CGRectMake(WIDTH-100,CGRectGetMaxY(img_line.frame)+8, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,125,200, 10);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,5, WIDTH-5, 165);
            img_dish.frame = CGRectMake(5,7, 95,95);
            icon_delete.frame = CGRectMake(WIDTH-60,10, 40, 40);
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(CGRectGetMaxX(icon_fronce.frame)+85,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+10, WIDTH-45, 0.5 );
            icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,30, 30);
            img_btn_seving_Now.frame = CGRectMake(110,CGRectGetMaxY(img_line.frame)+5,  100, 30);
            img_btn_chef_menu.frame = CGRectMake(170,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
            
            icon_thumb.frame = CGRectMake(290,CGRectGetMaxY(img_line.frame)+8, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+8,124,200, 10);
            
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,5, 298, 160);
            img_dish.frame = CGRectMake(05,7, 90,  95 );
            icon_delete.frame = CGRectMake(250,20, 40, 40);
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(CGRectGetMaxX(icon_fronce.frame)+25,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
            icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(90,CGRectGetMaxY(img_line.frame)+5,  100, 30);
            img_btn_chef_menu.frame = CGRectMake(143,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
            
            icon_thumb.frame = CGRectMake(235,CGRectGetMaxY(img_line.frame)+8, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,130,200, 10);
            
        }
        
        
    }
    else if (tableView == table_schedule)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        //    img_cellBackGnd.frame =  CGRectMake(0,2, 320, 50);
        //    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor whiteColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UIImageView *img_line = [[UIImageView alloc]init];
        //    img_line.frame =  CGRectMake(7,49, 280, 0.5);
        img_line.backgroundColor =[UIColor grayColor];
        [img_line setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line];
        
        UIImageView *servingimges = [[UIImageView alloc]init];
        //   servingimges.frame =  CGRectMake(10,10, 30, 30);
        [servingimges setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_serving_img objectAtIndex:indexPath.row]]]];
        servingimges.backgroundColor =[UIColor clearColor];
        [servingimges setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:servingimges];
        
        UILabel *serving_timeings = [[UILabel alloc]init];
        //    serving_timeings .frame = CGRectMake(50,10,200, 15);
        serving_timeings .text = [NSString stringWithFormat:@"%@",[array_serving_time objectAtIndex:indexPath.row]];
        serving_timeings .font = [UIFont fontWithName:kFontBold size:12];
        serving_timeings .textColor = [UIColor blackColor];
        serving_timeings .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:serving_timeings ];
        
        UILabel *serving_items = [[UILabel alloc]init];
        //    serving_items .frame = CGRectMake(50,30,200, 15);
        serving_items .text = [NSString stringWithFormat:@"%@",[ array_serving_items objectAtIndex:indexPath.row]];
        serving_items .font = [UIFont fontWithName:kFont size:10];
        serving_items .textColor = [UIColor blackColor];
        serving_items.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:serving_items ];
        
        
        UIButton *icon_right_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
        //    icon_right_arrow.frame = CGRectMake(260,10, 20, 20);
        //icon_thumb .backgroundColor = [UIColor clearColor];
        [icon_right_arrow addTarget:self action:@selector(icon_right_arrow_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [icon_right_arrow setImage:[UIImage imageNamed:@"right-arrow-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_right_arrow];
        
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(5,2, WIDTH-20, 50);
            img_line.frame =  CGRectMake(7,49, WIDTH-20, 0.5);
            servingimges.frame =  CGRectMake(10,10, 30, 30);
            serving_timeings .frame = CGRectMake(50,10,200, 15);
            serving_items .frame = CGRectMake(50,30,300, 15);
            icon_right_arrow.frame = CGRectMake(WIDTH-68,15, 20, 20);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, WIDTH-40, 50);
            img_line.frame =  CGRectMake(7,49, WIDTH-40, 0.5);
            servingimges.frame =  CGRectMake(10,10, 30, 30);
            serving_timeings .frame = CGRectMake(50,10,200, 15);
            serving_items .frame = CGRectMake(50,30,300, 15);
            icon_right_arrow.frame = CGRectMake(WIDTH-65,15, 20, 20);
            
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, 320, 50);
            img_line.frame =  CGRectMake(7,49, 280, 0.5);
            servingimges.frame =  CGRectMake(10,10, 30, 30);
            serving_timeings .frame = CGRectMake(50,10,200, 15);
            serving_items .frame = CGRectMake(50,30,200, 15);
            icon_right_arrow.frame = CGRectMake(260,15, 20, 20);
            
        }
        
        
    }
    else
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        //   img_cellBackGnd.frame =  CGRectMake(0,2, 294, 50);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor whiteColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UIImageView *img_line = [[UIImageView alloc]init];
        //      img_line.frame =  CGRectMake(7,49, 280, 0.5);
        img_line.backgroundColor =[UIColor grayColor];
        [img_line setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line];
        
        UIImageView *info_imges = [[UIImageView alloc]init];
        //     info_imges.frame =  CGRectMake(10,10, 30, 30);
        [info_imges setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_img objectAtIndex:indexPath.row]]]];
        info_imges.backgroundColor =[UIColor clearColor];
        [info_imges setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:info_imges];
        
        UILabel *chef_info_titiles = [[UILabel alloc]init];
        //   chef_info_titiles .frame = CGRectMake(50,10,200, 15);
        chef_info_titiles .text = [NSString stringWithFormat:@"%@",[ary_chef_info_titles objectAtIndex:indexPath.row]];
        chef_info_titiles .font = [UIFont fontWithName:kFontBold size:12];
        chef_info_titiles .textColor = [UIColor blackColor];
        chef_info_titiles .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:chef_info_titiles ];
        
        UILabel *chef_detailes = [[UILabel alloc]init];
        //   chef_detailes .frame = CGRectMake(50,30,200, 15);
        chef_detailes .text = [NSString stringWithFormat:@"%@",[ array_chef_details objectAtIndex:indexPath.row]];
        chef_detailes .font = [UIFont fontWithName:kFont size:10];
        chef_detailes .textColor = [UIColor blackColor];
        chef_detailes.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:chef_detailes ];
        
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, WIDTH, 45);
            // img_line.frame =  CGRectMake(7,49, 280, 0.5);
            info_imges.frame =  CGRectMake(10,10, 30, 30);
            chef_info_titiles .frame = CGRectMake(50,10,200, 15);
            chef_detailes .frame = CGRectMake(50,30,200, 15);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(1,2, WIDTH, 50);
            img_line.frame =  CGRectMake(7,49, 280, 0.5);
            info_imges.frame =  CGRectMake(10,10, 30, 30);
            chef_info_titiles .frame = CGRectMake(50,10,200, 15);
            chef_detailes .frame = CGRectMake(50,30,200, 15);
            
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, 294, 50);
            img_line.frame =  CGRectMake(7,49, 280, 0.5);
            info_imges.frame =  CGRectMake(10,10, 30, 30);
            chef_info_titiles .frame = CGRectMake(50,10,200, 15);
            chef_detailes .frame = CGRectMake(50,30,200, 15);
            
            
        }
        
    }
    
    return cell;
    
}

-(void)btn_menu_click:(UIButton *)sender
{
    NSLog(@"icon_menu_click Btn Click");
    [self.navigationController popViewControllerAnimated:NO];
    
}
-(void)btn_img_user_click:(UIButton *)sender
{
    NSLog(@"img_user_click Btn Click");
    
}
-(void)btn_icon_hart_click:(UIButton *)sender
{
    NSLog(@"btn_icon_hart_click:");
    
}
-(void)btn_black_click:(UIButton *)sender
{
    NSLog(@"icon_black_click Btn Click");
    
}


-(void)btn_location_click:(UIButton *)sender
{
    NSLog(@"icon_location_click Btn Click");
    
}
-(void)btn_img_user_info_click:(UIButton *)sender
{
    NSLog(@"img_user_info_click Btn Click");
    
}
-(void)btn_menu1_click:(UIButton *)sender
{
    NSLog(@"img_menu_click Btn Click");
    
}
-(void)btn_schedule_click:(UIButton *)sender
{
    NSLog(@"img_schedule_click Btn Click");
    
}

-(void)btn_reviews_click:(UIButton *)sender
{
    NSLog(@"img_reviews_click Btn Click");
    
}
-(void)btn_delete_click:(UIButton *)sender
{
    NSLog(@"btn_delete_click:");
    
}

-(void)icon_Food_Later_BtnClick:(UIButton *)sender
{
    NSLog(@"icon_Food_Later_BtnClick:");
    
}
-(void)icon_Food_Later_BtnClick1:(UIButton *)sender
{
    NSLog(@"icon_Food_Later_BtnClick1:");
    
}
-(void)btn_serving_now_click:(UIButton *)sender
{
    NSLog(@"btn_serving_now_click:");
}
-(void)btn_chef_menu_click:(UIButton *)sender
{
    NSLog(@"btn_chef_menu_click:");
}

-(void)icon_thumb_BtnClick:(UIButton *)sender
{
    NSLog(@"icon_thumb_BtnClick:");
    
}
-(void)icon_right_arrow_BtnClick:(UIButton *)sender
{
    NSLog(@"icon_thumb_BtnClick:");
    
}
-(void)btn_personal_info_strip_click:(UIButton *)sender
{
    
    NSLog(@" Btn_items_Click");
    img_strip.hidden = NO;
    img_strip2.hidden = YES;
    img_strip3.hidden = YES;
    img_strip4.hidden = YES;
    
    view_personal.hidden = NO;
    view_menu.hidden = YES;
    view_schedule.hidden = YES;
    view_for_reviews.hidden = YES;
    
}
-(void)btn_menu_strip_click:(UIButton *)sender
{
    lbl_min_order_value.hidden = NO;
    
    NSLog(@" Btn_chef_Click");
    img_strip.hidden = YES;
    img_strip2.hidden = NO;
    img_strip3.hidden = YES;
    img_strip4.hidden = YES;
    
    view_personal.hidden = YES;
    view_menu.hidden = NO;
    view_schedule.hidden = YES;
    view_for_reviews.hidden = YES;
    
    
}
-(void)btn_schedule_strip_click:(UIButton *)sender
{
    lbl_min_order_value.hidden = YES;
    
    NSLog(@" Btn_chef_Click");
    img_strip.hidden = YES;
    img_strip2.hidden = YES;
    img_strip3.hidden = NO;
    img_strip4.hidden = YES;
    
    view_personal.hidden = YES;
    view_menu.hidden = YES;
    view_schedule.hidden = NO;
    view_for_reviews.hidden = YES;
    
    
    
    
    
    
    
}
-(void)btn_reviews_strip_click:(UIButton *)sender
{
    lbl_min_order_value.hidden = NO;
    
    NSLog(@" Btn_chef_Click");
    img_strip.hidden = YES;
    img_strip2.hidden = YES;
    img_strip3.hidden = YES;
    img_strip4.hidden = NO;
    
    
    view_schedule.hidden = YES;
    view_personal.hidden = YES;
    view_menu.hidden = YES;
    view_for_reviews.hidden = NO;
    
    
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
