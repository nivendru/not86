//
//  ChefProfileInChefSideVC.m
//  Not86
//
//  Created by Admin on 15/09/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ChefProfileInChefSideVC.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"

@interface ChefProfileInChefSideVC ()<UIScrollViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UITextViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIImageView * img_header;
    UIScrollView * scroll;
    UIImageView *  img_bg;
    
    UICollectionViewFlowLayout *  layout;
    UICollectionView * collView_for_icons;
    NSMutableArray * array_for_icons;
    NSMutableArray * array_icons_name;
    
    UIImageView * img_strip;
    UIView * view_personal;
    UITableView *  table_for_personal_info;
    NSMutableArray * ary_chef_info_titles;
    NSMutableArray * array_img;
    NSMutableArray * array_chef_details;
    
    UIImageView * img_strip1 ;
    UIView * view_for_kitchen;
    UITableView * table_for_kitchen_info;
    NSMutableArray * array_icon_kitchen;
    NSMutableArray * array_head_names_in_kitchen;
    NSMutableArray * array_kitchen_name;
    NSMutableArray * array_icon_storages_hygiene;
    NSMutableArray * array_head_names_in_food_storage;
    UICollectionViewFlowLayout *  layout2;
    UICollectionView * collView_for_kitchen_imgs;
    NSMutableArray * array_kitchen_imgs;
    
    
    UIImageView *  img_strip2;
    UIView * view_for_dine_in;
    
    UIImageView * img_strip3;
    UIView * view_for_menu;
    
    UIView *  view_for_schedule;
    
    UIView *view_for_reviews;
    
    UIView * view_for_delivery;

   
}

@end

@implementation ChefProfileInChefSideVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateBody];
    [self integrateHeader];
    
    array_for_icons = [[NSMutableArray alloc]initWithObjects:@"icon-personal-info-w@2x.png",@"icon-kichan-w@2x.png",@"icon-dine-in-w@2x.png",@"icon-menu-w@2x.png",@"calender-w-icon@2x.png",@"reviews-w-icon@2x.png",@"img-delivery-w@2x.png",nil];
    
    array_icons_name = [[NSMutableArray alloc]initWithObjects:@"Personal Info",@"   Kitchen",@"   Dine-in",@"     Menu",@"   Schedule",@"   Reviews",@"Delivert Info", nil];
   
    // personal info array declaration
    
    ary_chef_info_titles =[[NSMutableArray alloc]initWithObjects:@"Username",@"Full Name",@"Chef Type",@"Date of Birth",@"Social Security no./National ID",@"Email address",@"Mobile no.",@"Home Address",@"Serving Type",@"Cooking Qualification",@"Cooking Experience", @"Paypal Account",@"Payment Received",nil];
    array_img = [[NSMutableArray alloc]initWithObjects:@"user1-icon@2x.png",@"user1-icon@2x.png",@"food-icon@2x.png",@"icon-date-of-birth@2x.png",@"icon-social@2x.png",@"icon-email@2x.png",@"icon-phone2x.png",@"icon-Home@2x.png",@"seving-type-icon@2x.png",@"cooking-Q-icon@2x.png",@"cooking-Ex-icon@2x.png",@"icon-paypal@2x.png",@"icon-paypal@2x.png",nil];
    array_chef_details = [[NSMutableArray alloc]initWithObjects:@"Charles1990",@"Charles dan",@"Home Chef",@"29 july 1974",@"s123456789",@"Charlesdan@gmail.com",@"+01 643 124 5681",@"Smith St, Suntec City, Paris 743844",@"Dine-in,Takaway,Deliver",@"Diploma in Cooking Industry" ,@"6-10 Years",@"Charles@paypal.com",@"End of Month",nil];
    
    // kitchen info array declaration
    
    array_icon_kitchen = [[NSMutableArray alloc]initWithObjects:@"icon-kichan1@2x.png",@"icon-kichan1@2x.png", nil];
    array_head_names_in_kitchen = [[NSMutableArray alloc]initWithObjects:@"Kitchen Name",@"Kitchen Address",nil];
    array_kitchen_name = [[NSMutableArray alloc]initWithObjects:@"Charles X Restaruant",@"Cartel St, Sams City, Paris\n743659", nil];
    array_icon_storages_hygiene = [[NSMutableArray alloc]initWithObjects:@"icon-storage@2x.png",@"icon-hygiene@2x.png", nil];
    array_kitchen_imgs = [[NSMutableArray alloc]initWithObjects:@"img-kitchen1@2x.png",@"img-kitchen2@2x.png",@"img-kitchen3@2x.png",@"img-kitchen4@2x.png",@"img-kitchen5@2x.png",@"img-kitchen6@2x.png",@"img-kitchen7@2x.png",@"img-kitchen8@2x.png",@"img-kitchen1@2x.png",@"img-kitchen1@2x.png",nil];
    array_head_names_in_food_storage = [[NSMutableArray alloc]initWithObjects:@"icon-storage@2x.png",@"icon-hygiene@2x.png", nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)prefersStatusBarHidden
{
    return YES;
}


-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu.frame = CGRectMake(10,13,20,20);
    icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu addTarget:self action:@selector(btn_menu_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_menu setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_menu];
    
    
    UILabel *lbl_my_profile = [[UILabel alloc]init];
    lbl_my_profile.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 150, 45);
    lbl_my_profile.text = @"My Profile";
    lbl_my_profile.font = [UIFont fontWithName:kFont size:20];
    lbl_my_profile.textColor = [UIColor whiteColor];
    lbl_my_profile.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_my_profile];
    
    UIImageView *icon_logo = [[UIImageView alloc]init];
    icon_logo.frame = CGRectMake(WIDTH-40, 9, 30, 30);
    [icon_logo setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_logo setUserInteractionEnabled:YES];
    [img_header addSubview:icon_logo];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}
-(void)integrateBody
{
    
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor clearColor];
    scroll.frame = CGRectMake(0,0, WIDTH, 650);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    img_bg = [[UIImageView alloc]init];
    img_bg .frame = CGRectMake(0,25, WIDTH, 210);
    [img_bg  setImage:[UIImage imageNamed:@"bg2-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg  setUserInteractionEnabled:YES];
    [ scroll addSubview:img_bg ];
    
    UIButton *chef_img = [UIButton buttonWithType:UIButtonTypeCustom];
    chef_img.frame = CGRectMake(110,30,80,80);
    chef_img.backgroundColor = [UIColor clearColor];
    [chef_img addTarget:self action:@selector(btn_img_user_click:) forControlEvents:UIControlEventTouchUpInside];
    [chef_img setImage:[UIImage imageNamed:@"img-chef@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:chef_img];
    
   
    UIImageView * img_black = [[UIImageView alloc]init];
    img_black.frame = CGRectMake(0,67,80,20);
    [img_black setUserInteractionEnabled:YES];
    img_black.image=[UIImage imageNamed:@"img-black@2x.png"];
    [chef_img addSubview:img_black];
    
    UILabel *lbl_on_reuest = [[UILabel alloc]init];
    lbl_on_reuest.frame = CGRectMake(9,0, 100,20);
    lbl_on_reuest.text = @"On Request";
    lbl_on_reuest.font = [UIFont fontWithName:kFontBold size:11];
    lbl_on_reuest.textColor = [UIColor whiteColor];
    lbl_on_reuest.backgroundColor = [UIColor clearColor];
    [ img_black addSubview:lbl_on_reuest];


    
    UILabel *lbl_chef_name = [[UILabel alloc]init];
    lbl_chef_name.frame = CGRectMake(110,CGRectGetMaxY(chef_img.frame), 200,45);
    lbl_chef_name.text = @"Charles Dan";
    lbl_chef_name.font = [UIFont fontWithName:kFontBold size:15];
    lbl_chef_name.textColor = [UIColor whiteColor];
    lbl_chef_name.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_chef_name];
    
    UIButton *img_pencil = [UIButton buttonWithType:UIButtonTypeCustom];
    img_pencil.frame = CGRectMake(280,35,20,20);
    img_pencil.backgroundColor = [UIColor clearColor];
    [img_pencil addTarget:self action:@selector(btn_img_pencil_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_pencil setImage:[UIImage imageNamed:@"icon-edit@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:img_pencil];
    
#pragma collection view
    
    layout = [[UICollectionViewFlowLayout alloc] init];
    collView_for_icons = [[UICollectionView alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(chef_img.frame)+45,WIDTH,50)
                                        collectionViewLayout:layout];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_icons setDataSource:self];
    [collView_for_icons setDelegate:self];
    collView_for_icons.scrollEnabled = YES;
    collView_for_icons.showsVerticalScrollIndicator = NO;
    collView_for_icons.showsHorizontalScrollIndicator = NO;
    collView_for_icons.pagingEnabled = YES;
    [collView_for_icons registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_icons setBackgroundColor:[UIColor clearColor]];
    layout.minimumInteritemSpacing = 12;
    layout.minimumLineSpacing = 0;
    collView_for_icons.userInteractionEnabled = YES;
    [img_bg  addSubview: collView_for_icons];

#pragma BODY FOR INFO
    
    img_strip = [[UIImageView alloc]init];
    img_strip.frame = CGRectMake(0,CGRectGetMaxY(collView_for_icons.frame)+2,78, 3);
    [img_strip setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    //img_strip.backgroundColor = [UIColor redColor];
    [img_strip setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip];
    
     //view for info
    
    view_personal = [[UIView alloc]init];
    view_personal.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_personal.backgroundColor=[UIColor redColor];
    [scroll  addSubview: view_personal];
    
//    UIImageView *img_bg1 = [[UIImageView alloc]init];
//    //   img_bg1.frame = CGRectMake(0,10, WIDTH+5, 300);
//    [img_bg1  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
//    //   icon_user.backgroundColor = [UIColor redColor];
//    [img_bg1  setUserInteractionEnabled:YES];
//    [ view_personal addSubview:img_bg1 ];
    
    UIImageView *img_bg1 = [[UIImageView alloc]init];
    img_bg1.frame = CGRectMake(0,10, WIDTH+5, 300);
    [img_bg1  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg1  setUserInteractionEnabled:YES];
    [view_personal addSubview:img_bg1 ];
    
    UITextView * txtview_adddescription = [[UITextView alloc]init];
    txtview_adddescription.frame = CGRectMake(20,20, 295,60);
    txtview_adddescription.scrollEnabled = YES;
    txtview_adddescription.userInteractionEnabled = NO;
    txtview_adddescription.font = [UIFont fontWithName:kFont size:12];
    txtview_adddescription.backgroundColor = [UIColor clearColor];
    txtview_adddescription.delegate = self;
    txtview_adddescription.textColor = [UIColor blackColor];
    txtview_adddescription.text =@"LOrem ipsumdotor sit amet.ne dicit virits corpor a per,ut illum liberavisse vet.At mei vidit conse cteture.in vitae definitionem per.tate vetear cliquado ei vim.";
    [img_bg1 addSubview:txtview_adddescription];
    
    UIImageView *img_line = [[UIImageView alloc]init];
    img_line .frame = CGRectMake(20,CGRectGetMaxY(txtview_adddescription.frame)+5, 280, 0.5);
    [img_line setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_line setUserInteractionEnabled:YES];
    [img_bg1 addSubview:img_line ];
    
    
#pragma mark personal_Tableview
    
    table_for_personal_info = [[UITableView alloc] init ];
    table_for_personal_info.frame  = CGRectMake(13,CGRectGetMaxY(img_line.frame),294,200);
    [table_for_personal_info setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_personal_info.delegate = self;
    table_for_personal_info.dataSource = self;
    table_for_personal_info.showsVerticalScrollIndicator = NO;
    table_for_personal_info.backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:table_for_personal_info];

#pragma BODY OF KITCHEN
    
    img_strip1 = [[UIImageView alloc]init];
    img_strip1.frame = CGRectMake(CGRectGetMaxX(img_strip.frame),CGRectGetMaxY(collView_for_icons.frame)+2,77, 3);
    [img_strip1 setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    //img_strip1.backgroundColor = [UIColor redColor];
    [img_strip1 setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip1];
    img_strip1.hidden = YES;
    
    //view for kitchen
    
    view_for_kitchen = [[UIView alloc]init];
    view_for_kitchen.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_kitchen.backgroundColor=[UIColor purpleColor];
    [scroll  addSubview: view_for_kitchen];
    view_for_kitchen.hidden = YES;
    
    
    UIImageView *img_bg2 = [[UIImageView alloc]init];
    img_bg2.frame = CGRectMake(0,10, WIDTH+2, 150);
    [img_bg2  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [img_bg2  setUserInteractionEnabled:YES];
    [view_for_kitchen addSubview:img_bg2 ];
    
#pragma mark table for kitchen info
    
    table_for_kitchen_info = [[UITableView alloc] init ];
    table_for_kitchen_info.frame  = CGRectMake(8,4,305,140);
    [table_for_kitchen_info setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_kitchen_info.delegate = self;
    table_for_kitchen_info.dataSource = self;
    table_for_kitchen_info.showsVerticalScrollIndicator = NO;
    table_for_kitchen_info.backgroundColor = [UIColor redColor];
    [img_bg2 addSubview:table_for_kitchen_info];
    
    UIImageView *img_bg_for_kitchen_imgs = [[UIImageView alloc]init];
    img_bg_for_kitchen_imgs.frame = CGRectMake(0,CGRectGetMaxY(img_bg2.frame)+5, WIDTH+2, 250);
    [img_bg_for_kitchen_imgs  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //img_bg_for_kitchen_imgs.backgroundColor = [UIColor redColor];
    [img_bg_for_kitchen_imgs  setUserInteractionEnabled:YES];
    [view_for_kitchen addSubview:img_bg_for_kitchen_imgs];
    
    UIImageView * img_video = [[UIImageView alloc]init];
    img_video.frame = CGRectMake(8,10,302,100);
    [img_video setUserInteractionEnabled:YES];
    img_video.image=[UIImage imageNamed:@"img-dine-area@2x.png"];
    [img_bg_for_kitchen_imgs addSubview:img_video];
    
    layout2 = [[UICollectionViewFlowLayout alloc] init];
    collView_for_kitchen_imgs = [[UICollectionView alloc] initWithFrame:CGRectMake(10,CGRectGetMaxY(img_video.frame),300,50)
                                            collectionViewLayout:layout2];
    [layout2 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_kitchen_imgs setDataSource:self];
    [collView_for_kitchen_imgs setDelegate:self];
    collView_for_kitchen_imgs.scrollEnabled = YES;
    collView_for_kitchen_imgs.showsVerticalScrollIndicator = YES;
    collView_for_kitchen_imgs.showsHorizontalScrollIndicator = NO;
    collView_for_kitchen_imgs.pagingEnabled = YES;
    [collView_for_kitchen_imgs registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_kitchen_imgs setBackgroundColor:[UIColor clearColor]];
    layout2.minimumInteritemSpacing = 12;
    layout2.minimumLineSpacing = 0;
    collView_for_kitchen_imgs.userInteractionEnabled = YES;
    [img_bg_for_kitchen_imgs  addSubview: collView_for_kitchen_imgs];


    
    UIImageView *img_bg_for_food_storage = [[UIImageView alloc]init];
    img_bg_for_food_storage.frame = CGRectMake(0,CGRectGetMaxY(img_bg_for_kitchen_imgs.frame)+5, WIDTH+2, 150);
    [img_bg_for_food_storage  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //img_bg_for_food_storage.backgroundColor = [UIColor redColor];
    [img_bg_for_food_storage  setUserInteractionEnabled:YES];
    [view_for_kitchen addSubview:img_bg_for_food_storage];
    
    


    
#pragma BODY FOR DINE_IN
    
    img_strip2 = [[UIImageView alloc]init];
    img_strip2.frame = CGRectMake(CGRectGetMaxX(img_strip1.frame)+7,CGRectGetMaxY(collView_for_icons.frame)+2,77, 3);
    [img_strip2 setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    //img_strip2.backgroundColor = [UIColor redColor];
    [img_strip2 setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip2];
    img_strip2.hidden = YES;
    
    //view for dine-in
    
    view_for_dine_in = [[UIView alloc]init];
    view_for_dine_in.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_dine_in.backgroundColor=[UIColor redColor];
    [scroll  addSubview: view_for_dine_in];
    view_for_dine_in.hidden = YES;
    
    img_strip3 = [[UIImageView alloc]init];
    img_strip3.frame = CGRectMake(CGRectGetMaxX(img_strip2.frame)+3,CGRectGetMaxY(collView_for_icons.frame)+2,80, 3);
    [img_strip3 setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    //img_strip3.backgroundColor = [UIColor redColor];
    [img_strip3 setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip3];
    img_strip3.hidden = YES;
    
#pragma View menu
    
    view_for_menu = [[UIView alloc]init];
    view_for_menu.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_menu.backgroundColor=[UIColor purpleColor];
    [scroll  addSubview: view_for_menu];
    view_for_menu.hidden = YES;
    
    
#pragma View for schedule
    
    view_for_schedule = [[UIView alloc]init];
    view_for_schedule.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_schedule.backgroundColor=[UIColor yellowColor];
    [scroll  addSubview: view_for_schedule];
    view_for_schedule.hidden = YES;
    
#pragma View_reviews
    
    view_for_reviews = [[UIView alloc]init];
    view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_reviews.backgroundColor=[UIColor blackColor];
    [scroll  addSubview: view_for_reviews];
    view_for_reviews.hidden = YES;
    
#pragma View_reviews
    
    view_for_delivery = [[UIView alloc]init];
    view_for_delivery.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_delivery.backgroundColor=[UIColor redColor];
    [scroll  addSubview: view_for_delivery];
    view_for_delivery.hidden = YES;

    

  [scroll setContentSize:CGSizeMake(0,1000)];


 }


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == table_for_personal_info)
    {
        return [ary_chef_info_titles count];
    }
    else if (tableView == table_for_kitchen_info)
    {
        return [array_icon_kitchen count];
    }
    
    return  0;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == table_for_personal_info)
     {
      return 1;
     }
     else if (tableView == table_for_kitchen_info)
     {
         return 1;
     }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table_for_personal_info)
    {
      return 50;
    }
    else if (tableView == table_for_kitchen_info)
    {
         return 70;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    if (tableView == table_for_personal_info)
    {
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    //   img_cellBackGnd.frame =  CGRectMake(0,2, 294, 50);
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
    img_cellBackGnd.backgroundColor =[UIColor whiteColor];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [cell.contentView addSubview:img_cellBackGnd];
    
    UIImageView *img_line = [[UIImageView alloc]init];
    //      img_line.frame =  CGRectMake(7,49, 280, 0.5);
    img_line.backgroundColor =[UIColor grayColor];
    [img_line setUserInteractionEnabled:YES];
    [img_cellBackGnd addSubview:img_line];
    
    UIImageView *info_imges = [[UIImageView alloc]init];
    //     info_imges.frame =  CGRectMake(10,10, 30, 30);
    [info_imges setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_img objectAtIndex:indexPath.row]]]];
    info_imges.backgroundColor =[UIColor clearColor];
    [info_imges setUserInteractionEnabled:YES];
    [img_cellBackGnd addSubview:info_imges];
    
    UILabel *chef_info_titiles = [[UILabel alloc]init];
    //   chef_info_titiles .frame = CGRectMake(50,10,200, 15);
    chef_info_titiles .text = [NSString stringWithFormat:@"%@",[ary_chef_info_titles objectAtIndex:indexPath.row]];
    chef_info_titiles .font = [UIFont fontWithName:kFontBold size:12];
    chef_info_titiles .textColor = [UIColor blackColor];
    chef_info_titiles .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:chef_info_titiles ];
    
    UILabel *chef_detailes = [[UILabel alloc]init];
    //   chef_detailes .frame = CGRectMake(50,30,200, 15);
    chef_detailes .text = [NSString stringWithFormat:@"%@",[ array_chef_details objectAtIndex:indexPath.row]];
    chef_detailes .font = [UIFont fontWithName:kFont size:10];
    chef_detailes .textColor = [UIColor blackColor];
    chef_detailes.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:chef_detailes ];
    
    
    if (IS_IPHONE_6Plus)
    {
        img_cellBackGnd.frame =  CGRectMake(0,2, WIDTH, 45);
        // img_line.frame =  CGRectMake(7,49, 280, 0.5);
        info_imges.frame =  CGRectMake(10,10, 30, 30);
        chef_info_titiles .frame = CGRectMake(50,10,200, 15);
        chef_detailes .frame = CGRectMake(50,30,200, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        img_cellBackGnd.frame =  CGRectMake(1,2, WIDTH, 50);
        img_line.frame =  CGRectMake(7,49, 280, 0.5);
        info_imges.frame =  CGRectMake(10,10, 30, 30);
        chef_info_titiles .frame = CGRectMake(50,10,200, 15);
        chef_detailes .frame = CGRectMake(50,30,200, 15);
        
    }
    else
    {
        img_cellBackGnd.frame =  CGRectMake(0,2, 294, 50);
        img_line.frame =  CGRectMake(7,49, 280, 0.5);
        info_imges.frame =  CGRectMake(10,10, 30, 30);
        chef_info_titiles .frame = CGRectMake(50,10,200, 15);
        chef_detailes .frame = CGRectMake(50,30,200, 15);
        
        
    }
  }
  else if (tableView == table_for_kitchen_info)
  {
      UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
      img_cellBackGnd.frame =  CGRectMake(0,2, 310, 70);
      [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
      img_cellBackGnd.backgroundColor =[UIColor whiteColor];
      [img_cellBackGnd setUserInteractionEnabled:YES];
      [cell.contentView addSubview:img_cellBackGnd];
      
      UIImageView *kitchen_icon_in_cell = [[UIImageView alloc]init];
       kitchen_icon_in_cell.frame =  CGRectMake(20,10, 40, 40);
      [kitchen_icon_in_cell setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_icon_kitchen objectAtIndex:indexPath.row]]]];
      kitchen_icon_in_cell.backgroundColor =[UIColor clearColor];
      [kitchen_icon_in_cell setUserInteractionEnabled:YES];
      [img_cellBackGnd addSubview:kitchen_icon_in_cell];
      
      UIImageView *img_line = [[UIImageView alloc]init];
      img_line.frame =  CGRectMake(7,67, 280, 0.5);
      img_line.backgroundColor =[UIColor grayColor];
      [img_line setUserInteractionEnabled:YES];
      [img_cellBackGnd addSubview:img_line];
      
      UILabel *lbl_head_in_kitchen_cell = [[UILabel alloc]init];
      lbl_head_in_kitchen_cell .frame = CGRectMake(CGRectGetMaxX(kitchen_icon_in_cell.frame)+15,10,200, 15);
      lbl_head_in_kitchen_cell .text = [NSString stringWithFormat:@"%@",[array_head_names_in_kitchen objectAtIndex:indexPath.row]];
      lbl_head_in_kitchen_cell .font = [UIFont fontWithName:kFontBold size:15];
      lbl_head_in_kitchen_cell .textColor = [UIColor blackColor];
      lbl_head_in_kitchen_cell .backgroundColor = [UIColor clearColor];
      [img_cellBackGnd addSubview:lbl_head_in_kitchen_cell ];

      UILabel *lbl_kitchen_details = [[UILabel alloc]init];
      lbl_kitchen_details .frame = CGRectMake(CGRectGetMaxX(kitchen_icon_in_cell.frame)+15,CGRectGetMaxY(lbl_head_in_kitchen_cell.frame)+10,200, 15);
      lbl_kitchen_details .text = [NSString stringWithFormat:@"%@",[array_kitchen_name objectAtIndex:indexPath.row]];
      lbl_kitchen_details .font = [UIFont fontWithName:kFont size:12];
      lbl_kitchen_details .textColor = [UIColor blackColor];
      lbl_kitchen_details.backgroundColor = [UIColor clearColor];
      [img_cellBackGnd addSubview:lbl_kitchen_details];
      
      


      

  }

    return cell;
}

#pragma mark UiCollectionView Delegate Methods for dish

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(collectionView == collView_for_icons)
    {
      return [array_for_icons count];
    }
    else if (collectionView == collView_for_kitchen_imgs)
    {
        return [array_kitchen_imgs count];
    }
    
    return 0;
    
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView1 dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    
    if (collectionView1 == collView_for_icons)
    {
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,-5,80,103);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        UIImageView *chef_pro_icons= [[UIImageView alloc]init];
        chef_pro_icons .frame = CGRectMake(20,30,35,30);
        [chef_pro_icons setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_for_icons objectAtIndex:indexPath.row]]]];
        //[img_dish  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        chef_pro_icons .backgroundColor = [UIColor redColor];
        [chef_pro_icons  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:chef_pro_icons];
        
        UILabel *icons_name = [[UILabel alloc]init];
        icons_name.frame = CGRectMake(10,CGRectGetMaxY(chef_pro_icons.frame),200, 15);
        icons_name.text = [NSString stringWithFormat:@"%@",[array_icons_name objectAtIndex:indexPath.row]];
        icons_name.font = [UIFont fontWithName:kFontBold size:10];
        icons_name.textColor = [UIColor blackColor];
        icons_name.backgroundColor = [UIColor clearColor];
        [cell_for_collection_view addSubview:icons_name];
        
        UIButton *btn_on_icons = [UIButton buttonWithType:UIButtonTypeCustom];
        btn_on_icons .frame = CGRectMake(0,0,30,30);
        btn_on_icons .backgroundColor = [UIColor clearColor];
        [btn_on_icons  addTarget:self action:@selector(btn_on_icons_click:) forControlEvents:UIControlEventTouchUpInside];
        //[btn_on_icons  setImage:[UIImage imageNamed:@"icon-w-location@2x.png"] forState:UIControlStateNormal];
        [cell_for_collection_view  addSubview:btn_on_icons ];
    }
    else if (collectionView1 == collView_for_kitchen_imgs)
    {
       
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,-5,80,103);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        UIImageView *kitchen_images = [[UIImageView alloc]init];
        kitchen_images .frame = CGRectMake(20,30,70,70);
        [kitchen_images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_kitchen_imgs objectAtIndex:indexPath.row]]]];
        //[img_dish  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        kitchen_images .backgroundColor = [UIColor redColor];
        [kitchen_images  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:kitchen_images];
    }
    
        
   
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((80), 90);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row ==0)
    {
        img_strip.hidden = NO;
        img_strip1.hidden =YES;
        img_strip2.hidden = YES;
        img_strip3.hidden = YES;
       
        
        view_personal.hidden = NO;
        view_for_kitchen.hidden = YES;
        view_for_dine_in.hidden = YES;
        view_for_menu.hidden = YES;
        view_for_schedule.hidden = YES;
        view_for_reviews.hidden = YES;
        view_for_delivery.hidden = YES;
        
    }
    else if (indexPath.row == 1)
    {
        img_strip.hidden = YES;
        img_strip1.hidden =NO;
        img_strip2.hidden = YES;
        img_strip3.hidden = YES;
       
        
        view_personal.hidden = YES;
        view_for_kitchen.hidden = NO;
        view_for_dine_in.hidden = YES;
        view_for_menu .hidden = YES;
         view_for_schedule.hidden = YES;
         view_for_reviews.hidden = YES;
         view_for_delivery.hidden = YES;

    }
    else if (indexPath.row == 2)
    {
        img_strip.hidden = YES;
        img_strip1.hidden =YES;
        img_strip2.hidden = NO;
        img_strip3.hidden = YES;
        
        view_personal.hidden = YES;
        view_for_kitchen.hidden = YES;
        view_for_dine_in.hidden = NO;
        view_for_menu.hidden = YES;
         view_for_schedule.hidden = YES;
         view_for_reviews.hidden = YES;
         view_for_delivery.hidden = YES;
        
    }
    else if (indexPath.row == 3)
    {
    
        img_strip.hidden = YES;
        img_strip1.hidden =YES;
        img_strip2.hidden = YES;
        img_strip3.hidden = NO;
        
        view_personal.hidden = YES;
        view_for_kitchen.hidden = YES;
        view_for_dine_in.hidden = YES;
        view_for_menu.hidden = NO;
         view_for_schedule.hidden = YES;
         view_for_reviews.hidden = YES;
         view_for_delivery.hidden = YES;

    }
    else if (indexPath.row == 4)
    {
        img_strip.hidden = YES;
        img_strip1.hidden =YES;
        img_strip2.hidden = YES;
        img_strip3.hidden = NO;
        
        view_personal.hidden = YES;
        view_for_kitchen.hidden = YES;
        view_for_dine_in.hidden = YES;
        view_for_menu.hidden = YES;
        view_for_schedule.hidden = NO;
         view_for_reviews.hidden = YES;
         view_for_delivery.hidden = YES;
    }
    else if (indexPath.row == 5)
    {
        img_strip.hidden = YES;
        img_strip1.hidden =YES;
        img_strip2.hidden = YES;
        img_strip3.hidden = NO;
        
        view_personal.hidden = YES;
        view_for_kitchen.hidden = YES;
        view_for_dine_in.hidden = YES;
        view_for_menu.hidden = YES;
        view_for_schedule.hidden = YES;
        view_for_reviews.hidden = NO;
         view_for_delivery.hidden = YES;

    }
    else if (indexPath.row == 6)
    {
        img_strip.hidden = YES;
        img_strip1.hidden =YES;
        img_strip2.hidden = YES;
        img_strip3.hidden = NO;
        
        view_personal.hidden = YES;
        view_for_kitchen.hidden = YES;
        view_for_dine_in.hidden = YES;
        view_for_menu.hidden = YES;
        view_for_schedule.hidden = YES;
        view_for_reviews.hidden = YES;
        view_for_delivery.hidden = NO;
        

    }
}


#pragma  click events
-(void)btn_menu_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click:");
}
-(void)btn_img_user_click:(UIButton *)sender
{
    NSLog(@"btn_img_user_click:");
}
-(void)btn_img_pencil_click:(UIButton *)sender
{
    NSLog(@"btn_img_pencil_click:");
}
-(void)btn_on_icons_click:(UIButton *)sender
{
    NSLog(@"btn_on_icons_click:");
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
