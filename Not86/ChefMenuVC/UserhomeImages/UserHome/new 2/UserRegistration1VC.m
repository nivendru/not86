//
//  UserRegistration1VC.m
//  Not86
//
//  Created by Admin on 29/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "UserRegistration1VC.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"



@interface UserRegistration1VC ()<UITextFieldDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIImageView *img_header;
    UIImageView *img_background;
    CGFloat	animatedDistance;
    
    UIButton *  user_img ;
    UIScrollView *scroll;
    
    
    UITableView *img_table_for_contry_code;
    

}

@end

@implementation UserRegistration1VC
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];

    [self integrateHeader];
    [self integrateBodyDesign];
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)integrateHeader
{
    
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [ self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back addTarget:self action:@selector(btn_back_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_back];
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init ];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_back.frame)+30,0, 150, 45);
    lbl_User_Sign_Up.text = @"User Sign Up";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    UIImageView *icon_user = [[UIImageView alloc]init ];
    icon_user .frame = CGRectMake(WIDTH-40, 8, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"img_user_icon@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}

-(void)integrateBodyDesign
{
    
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor whiteColor];
    //  scroll.frame = CGRectMake(0, 51, WIDTH, 450);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    user_img = [UIButton buttonWithType:UIButtonTypeCustom];
    // user_img.frame = CGRectMake(95, CGRectGetMaxY(img_header.frame)+10, 120, 120);
    user_img .backgroundColor = [UIColor clearColor];
    [user_img addTarget:self action:@selector(user_img_click:) forControlEvents:UIControlEventTouchUpInside];
    user_img.userInteractionEnabled = YES;
    [user_img setImage:[UIImage imageNamed:@"img-user@2x.png"] forState:UIControlStateNormal];
    [ scroll   addSubview:user_img];
    
    
    
    UILabel *lbl_User_Info = [[UILabel alloc]init ];
    //  lbl_User_Info.frame = CGRectMake(-10,CGRectGetMaxY(user_img.frame)-45,200, 15);
    lbl_User_Info.text = @"Personal Information";
    lbl_User_Info.font = [UIFont fontWithName:kFontBold size:15];
    lbl_User_Info.textColor = [UIColor blackColor];
    lbl_User_Info.backgroundColor = [UIColor clearColor];
    [ scroll addSubview:lbl_User_Info];
    
    UITextField *txt_First_Name = [[UITextField alloc] init];
    //   txt_First_Name .frame=CGRectMake(-70, CGRectGetMaxY(lbl_User_Info.frame)+10, WIDTH-80, 38);
    txt_First_Name .borderStyle = UITextBorderStyleNone;
    txt_First_Name .textColor = [UIColor blackColor];
    txt_First_Name .font = [UIFont fontWithName:kFont size:16];
    txt_First_Name .placeholder = @"First Name";
    [txt_First_Name  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_First_Name  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_First_Name .leftView = padding1;
    txt_First_Name .leftViewMode = UITextFieldViewModeAlways;
    txt_First_Name .userInteractionEnabled=YES;
    txt_First_Name .textAlignment = NSTextAlignmentLeft;
    txt_First_Name .backgroundColor = [UIColor clearColor];
    txt_First_Name .keyboardType = UIKeyboardTypeAlphabet;
    txt_First_Name .delegate = self;
    [ scroll addSubview:txt_First_Name ];
    
    UIImageView *line=[[UIImageView alloc]init];
    //  line.frame=CGRectMake(-65, CGRectGetMaxY(txt_First_Name .frame)-5, 270, 0.5);
    [line setUserInteractionEnabled:YES];
    line.backgroundColor=[UIColor clearColor];
    line.image=[UIImage imageNamed:@"img-line@2x.png"];
    [ scroll addSubview:line];
    
    
    UITextField *txt_Middle_Name = [[UITextField alloc] init];
    //   txt_Middle_Name.frame=CGRectMake(-70, CGRectGetMaxY(line.frame)+10, WIDTH-80, 38);
    txt_Middle_Name.borderStyle = UITextBorderStyleNone;
    txt_Middle_Name.textColor = [UIColor blackColor];
    txt_Middle_Name.font = [UIFont fontWithName:kFont size:16];
    txt_Middle_Name.placeholder = @"Middle Name";
    [txt_Middle_Name setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_Middle_Name setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_Middle_Name.leftView = padding2;
    txt_Middle_Name.leftViewMode = UITextFieldViewModeAlways;
    txt_Middle_Name.userInteractionEnabled=YES;
    txt_Middle_Name.textAlignment = NSTextAlignmentLeft;
    txt_Middle_Name.backgroundColor = [UIColor clearColor];
    txt_Middle_Name.keyboardType = UIKeyboardTypeAlphabet;
    txt_Middle_Name.delegate = self;
    [ scroll addSubview:txt_Middle_Name];
    
    UIImageView *line2=[[UIImageView alloc]init];
    //   line2.frame=CGRectMake(-65, CGRectGetMaxY(txt_Middle_Name.frame)-5, 270, 0.5);
    [line2 setUserInteractionEnabled:YES];
    line2.backgroundColor=[UIColor clearColor];
    line2.image=[UIImage imageNamed:@"img-line@2x.png"];
    [ scroll addSubview:line2];
    
    UILabel *lbl_optional = [[UILabel alloc]init ];
    // lbl_optional.frame = CGRectMake(150,CGRectGetMaxY(line2.frame)+230,100, 15);
    lbl_optional.text = @"(optional)";
    lbl_optional.font = [UIFont fontWithName:kFont size:12];
    lbl_optional.textColor = [UIColor blackColor];
    lbl_optional.backgroundColor = [UIColor clearColor];
    [ scroll addSubview:lbl_optional];
    
    
    
    UITextField *txt_Last_Name = [[UITextField alloc] init];
    //   txt_Last_Name .frame=CGRectMake(-70, CGRectGetMaxY(line2.frame)+10, WIDTH-80, 38);
    txt_Last_Name .borderStyle = UITextBorderStyleNone;
    txt_Last_Name .textColor = [UIColor blackColor];
    txt_Last_Name .font = [UIFont fontWithName:kFont size:16];
    txt_Last_Name .placeholder = @"Last Name";
    [txt_Last_Name  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_Last_Name  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_Last_Name .leftView = padding3;
    txt_Last_Name .leftViewMode = UITextFieldViewModeAlways;
    txt_Last_Name .userInteractionEnabled=YES;
    txt_Last_Name .textAlignment = NSTextAlignmentLeft;
    txt_Last_Name .backgroundColor = [UIColor clearColor];
    txt_Last_Name .keyboardType = UIKeyboardTypeAlphabet;
    txt_Last_Name .delegate = self;
    [ scroll addSubview:txt_Last_Name ];
    
    UIImageView *line3=[[UIImageView alloc]init];
    //   line3.frame=CGRectMake(-65, CGRectGetMaxY(txt_Last_Name.frame)-5, 270, 0.5);
    [line3 setUserInteractionEnabled:YES];
    line3.backgroundColor=[UIColor clearColor];
    line3.image=[UIImage imageNamed:@"img-line@2x.png"];
    [ scroll addSubview:line3];
    
    
    UILabel *Date_Of_Birth = [[UILabel alloc]init];
    //   Date_Of_Birth.frame = CGRectMake(-65, CGRectGetMaxY(line3.frame)+10, WIDTH, 38);
    Date_Of_Birth.text = @"Date of Birth __ __ / __ __ / __ __ __ __";
    Date_Of_Birth.font = [UIFont fontWithName:kFont size:15];
    Date_Of_Birth.textColor = [UIColor blackColor];
    Date_Of_Birth.backgroundColor = [UIColor clearColor];
    [ scroll addSubview:Date_Of_Birth];
    
    
    UITextField *txt_Tel_Code = [[UITextField alloc] init];
    //   txt_Tel_Code  .frame=CGRectMake(-70, CGRectGetMaxY(Date_Of_Birth.frame)+10, WIDTH-80, 38);
    txt_Tel_Code  .borderStyle = UITextBorderStyleNone;
    txt_Tel_Code  .textColor = [UIColor grayColor];
    txt_Tel_Code  .font = [UIFont fontWithName:kFont size:16];
    txt_Tel_Code  .placeholder = @"+65";
    [txt_Tel_Code   setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_Tel_Code   setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding5 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_Tel_Code  .leftView = padding5;
    txt_Tel_Code  .leftViewMode = UITextFieldViewModeAlways;
    txt_Tel_Code  .userInteractionEnabled=YES;
    txt_Tel_Code  .textAlignment = NSTextAlignmentLeft;
    txt_Tel_Code .backgroundColor = [UIColor clearColor];
    txt_Tel_Code  .keyboardType = UIKeyboardTypeAlphabet;
    txt_Tel_Code  .delegate = self;
    [ scroll addSubview:txt_Tel_Code ];
    
    UITextField *txt_Mobile_No = [[UITextField alloc] init];
    //    txt_Mobile_No   .frame=CGRectMake(-10, CGRectGetMaxY(Date_Of_Birth.frame)+10, WIDTH-80, 38);
    txt_Mobile_No   .borderStyle = UITextBorderStyleNone;
    txt_Mobile_No  .textColor = [UIColor blackColor];
    txt_Mobile_No .font = [UIFont fontWithName:kFont size:16];
    txt_Mobile_No  .placeholder = @"Mobile no.";
    [txt_Mobile_No   setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_Mobile_No   setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_Mobile_No   .leftView = padding6;
    txt_Mobile_No   .leftViewMode = UITextFieldViewModeAlways;
    txt_Mobile_No   .userInteractionEnabled=YES;
    txt_Mobile_No  .textAlignment = NSTextAlignmentLeft;
    txt_Mobile_No  .backgroundColor = [UIColor clearColor];
    txt_Mobile_No   .keyboardType = UIKeyboardTypeAlphabet;
    txt_Mobile_No  .delegate = self;
    [ scroll addSubview:txt_Mobile_No  ];
    
    
    
    
    UIButton *icon_dropdown =[UIButton buttonWithType:UIButtonTypeCustom];
    //  icon_dropdown.frame=CGRectMake(CGRectGetMaxX(txt_Tel_Code.frame)-203, CGRectGetMaxY(Date_Of_Birth.frame)+25, 15, 10);
    //icon_dropdown.backgroundColor = [UIColor clearColor];
    [icon_dropdown addTarget:self action:@selector(icon_drop_down_click:) forControlEvents:UIControlEventTouchUpInside];
    icon_dropdown.userInteractionEnabled = YES;
    
    [icon_dropdown setImage:[UIImage imageNamed:@"dropdown@2x.png"] forState:UIControlStateNormal];
    [ scroll   addSubview:icon_dropdown];
    
#pragma mark Tableview
    
    img_table_for_contry_code = [[UITableView alloc] init ];
    //  img_table_for_contry_code.frame  = CGRectMake(13,290,296,370);
    [img_table_for_contry_code setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table_for_contry_code.delegate = self;
    img_table_for_contry_code.dataSource = self;
    img_table_for_contry_code.showsVerticalScrollIndicator = NO;
    img_table_for_contry_code.backgroundColor = [UIColor redColor];
    [scroll  addSubview:img_table_for_contry_code];
    
    img_table_for_contry_code.hidden =YES;
    

    
    
    
    UIImageView *line4=[[UIImageView alloc]init];
    //   line4.frame=CGRectMake(-65, CGRectGetMaxY(txt_Tel_Code.frame)-5, 50, 0.5);
    [line4 setUserInteractionEnabled:YES];
    line4.backgroundColor=[UIColor clearColor];
    line4.image=[UIImage imageNamed:@"img-line@2x.png"];
    [ scroll addSubview:line4];
    
    UIImageView *line5=[[UIImageView alloc]init];
    //  line5.frame=CGRectMake(CGRectGetMaxX(line4.frame)+10, CGRectGetMaxY(txt_Tel_Code.frame)-5, 210, 0.5);
    [line5 setUserInteractionEnabled:YES];
    line5.backgroundColor=[UIColor clearColor];
    line5.image=[UIImage imageNamed:@"img-line@2x.png"];
    [ scroll addSubview:line5];
    
    
    UITextField *txt_Gmail = [[UITextField alloc] init];
    //   txt_Gmail .frame=CGRectMake(-70, CGRectGetMaxY(line5.frame)+10, WIDTH-30, 38);
    txt_Gmail .borderStyle = UITextBorderStyleNone;
    txt_Gmail .textColor = [UIColor blackColor];
    txt_Gmail .font = [UIFont fontWithName:kFont size:5];
    txt_Gmail .placeholder = @"Email Address(e.g. Chef@gmail.com)";
    [txt_Gmail  setValue:[UIFont fontWithName:kFont size: 14] forKeyPath:@"_placeholderLabel.font"];
    [txt_Gmail  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding7 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_Gmail .leftView = padding7;
    txt_Gmail .leftViewMode = UITextFieldViewModeAlways;
    txt_Gmail .userInteractionEnabled=YES;
    txt_Gmail .textAlignment = NSTextAlignmentLeft;
    txt_Gmail .backgroundColor = [UIColor clearColor];
    txt_Gmail .keyboardType = UIKeyboardTypeAlphabet;
    txt_Gmail .delegate = self;
    [ scroll addSubview:txt_Gmail ];
    
    UIImageView *line6=[[UIImageView alloc]init];
    //   line6.frame=CGRectMake(-65, CGRectGetMaxY(txt_Gmail.frame), 270, 0.5);
    [line6 setUserInteractionEnabled:YES];
    line6.backgroundColor=[UIColor clearColor];
    line6.image=[UIImage imageNamed:@"img-line@2x.png"];
    [ scroll addSubview:line6];
    
    UILabel *page = [[UILabel alloc]init];
    //   page.frame = CGRectMake(24, CGRectGetMaxY(line6.frame)+11, WIDTH-80, 38);
    page.text = @" Page 1/2";
    page.font = [UIFont fontWithName:kFont size:15];
    page.textColor = [UIColor lightGrayColor];
    page.backgroundColor = [UIColor clearColor];
    [ self.view addSubview:page];
    
    
    UIButton *btn_Next =[UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_Next.frame=CGRectMake(-80,CGRectGetMaxY(line6.frame)+49, 300, 45);
    [btn_Next setTitle:@"NEXT" forState:UIControlStateNormal];
    [btn_Next setTitleColor:[UIColor whiteColor ] forState:UIControlStateNormal];
    [btn_Next addTarget:self action:@selector(btn_next_click:) forControlEvents:UIControlEventTouchUpInside];
    btn_Next.layer.cornerRadius = 5.0f;
    btn_Next.userInteractionEnabled = YES;
    btn_Next.backgroundColor = [UIColor colorWithRed:40/255.0f green:30/255.0f blue:48/255.0f alpha:1];
    [btn_Next setImage:[UIImage imageNamed:@"img-bg-next@2x.png"] forState:UIControlStateNormal];
    [self.view   addSubview:btn_Next];
    
    
    if (IS_IPHONE_6Plus)
    {
        scroll.frame = CGRectMake(05, 51, WIDTH-10, 510);
        
        user_img.frame = CGRectMake(140, CGRectGetMaxY(img_header.frame)-38, 120, 120);
        lbl_User_Info.frame = CGRectMake(130,CGRectGetMaxY(user_img.frame)+15,200, 15);
        
        txt_First_Name .frame=CGRectMake(20, CGRectGetMaxY(lbl_User_Info.frame)+15, WIDTH-80, 38);
        line.frame=CGRectMake(23, CGRectGetMaxY(txt_First_Name .frame), WIDTH-46, 0.5);
        
        txt_Middle_Name.frame=CGRectMake(20, CGRectGetMaxY(line.frame)+15, WIDTH-80, 38);
        line2.frame=CGRectMake(23, CGRectGetMaxY(txt_Middle_Name.frame), WIDTH-46, 0.5);
        lbl_optional.frame = CGRectMake(330,CGRectGetMaxY(line2.frame),100, 15);
        
        txt_Last_Name .frame=CGRectMake(20, CGRectGetMaxY(line2.frame)+15, WIDTH-80, 38);
        line3.frame=CGRectMake(23, CGRectGetMaxY(txt_Last_Name.frame), WIDTH-46, 0.5);
        
        Date_Of_Birth.frame = CGRectMake(20, CGRectGetMaxY(line3.frame)+15, WIDTH, 38);
        
        txt_Tel_Code  .frame=CGRectMake(20, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
        txt_Mobile_No   .frame=CGRectMake(80, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
        icon_dropdown.frame=CGRectMake(CGRectGetMaxX(txt_Tel_Code.frame)-295, CGRectGetMaxY(Date_Of_Birth.frame)+30, 15, 10);
        img_table_for_contry_code.frame  = CGRectMake(25,CGRectGetMaxY(icon_dropdown.frame)+5,50,100);
        line4.frame=CGRectMake(23, CGRectGetMaxY(txt_Tel_Code.frame), 50, 0.5);
        line5.frame=CGRectMake(CGRectGetMaxX(line4.frame)+10, CGRectGetMaxY(txt_Tel_Code.frame), WIDTH-106, 0.5);
        
        txt_Gmail .frame=CGRectMake(20, CGRectGetMaxY(line5.frame)+15, WIDTH-30, 38);
        line6.frame=CGRectMake(23, CGRectGetMaxY(txt_Gmail.frame), WIDTH-46, 0.5);
        
        page.frame = CGRectMake(WIDTH/2-40, CGRectGetMaxY(line6.frame)+105, WIDTH-80, 38);
        btn_Next.frame=CGRectMake(20,HEIGHT-90, WIDTH-42, 55);
        
        
    }
    else if (IS_IPHONE_6)
    {
        scroll.frame = CGRectMake(5, 51, WIDTH-10, 490);
        
        user_img.frame = CGRectMake(120, CGRectGetMaxY(img_header.frame)-38, 120, 120);
        lbl_User_Info.frame = CGRectMake(120,CGRectGetMaxY(user_img.frame)+10,200, 15);
        
        txt_First_Name .frame=CGRectMake(20, CGRectGetMaxY(lbl_User_Info.frame)+25, WIDTH-80, 38);
        line.frame=CGRectMake(23, CGRectGetMaxY(txt_First_Name .frame), WIDTH-46, 0.5);
        
        txt_Middle_Name.frame=CGRectMake(20, CGRectGetMaxY(line.frame)+15, WIDTH-80, 38);
        line2.frame=CGRectMake(23, CGRectGetMaxY(txt_Middle_Name.frame), WIDTH-46, 0.5);
        lbl_optional.frame = CGRectMake(290,CGRectGetMaxY(line2.frame),100, 15);
        
        txt_Last_Name .frame=CGRectMake(20, CGRectGetMaxY(line2.frame)+15, WIDTH-80, 38);
        line3.frame=CGRectMake(23, CGRectGetMaxY(txt_Last_Name.frame), WIDTH-46, 0.5);
        
        Date_Of_Birth.frame = CGRectMake(20, CGRectGetMaxY(line3.frame)+15, WIDTH, 38);
        
        txt_Tel_Code  .frame=CGRectMake(20, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
        txt_Mobile_No   .frame=CGRectMake(80, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
        icon_dropdown.frame=CGRectMake(CGRectGetMaxX(txt_Tel_Code.frame)-258, CGRectGetMaxY(Date_Of_Birth.frame)+30, 15, 10);
        img_table_for_contry_code.frame  = CGRectMake(25,CGRectGetMaxY(icon_dropdown.frame)+5,50,100);
        line4.frame=CGRectMake(23, CGRectGetMaxY(txt_Tel_Code.frame), 50, 0.5);
        line5.frame=CGRectMake(CGRectGetMaxX(line4.frame)+10, CGRectGetMaxY(txt_Tel_Code.frame), WIDTH-106, 0.5);
        
        txt_Gmail .frame=CGRectMake(20, CGRectGetMaxY(line5.frame)+10, WIDTH-30, 38);
        line6.frame=CGRectMake(23, CGRectGetMaxY(txt_Gmail.frame), WIDTH-46, 0.5);
        
        page.frame = CGRectMake(WIDTH/2-40, CGRectGetMaxY(line6.frame)+75, WIDTH-80, 38);
        btn_Next.frame=CGRectMake(20,HEIGHT-70, WIDTH-42, 55);
        
        
        
    }
    else if(IS_IPHONE_5)
    {
        scroll.frame = CGRectMake(5, 51, WIDTH-10, 400);
        
        user_img.frame = CGRectMake(95, CGRectGetMaxY(img_header.frame)-38, 120, 120);
        lbl_User_Info.frame = CGRectMake(90,CGRectGetMaxY(user_img.frame)+10,200, 15);
        
        txt_First_Name .frame=CGRectMake(20, CGRectGetMaxY(lbl_User_Info.frame)+15, WIDTH-80, 38);
        line.frame=CGRectMake(23, CGRectGetMaxY(txt_First_Name .frame), 270, 0.5);
        
        txt_Middle_Name.frame=CGRectMake(20, CGRectGetMaxY(line.frame)+15, WIDTH-80, 38);
        line2.frame=CGRectMake(23, CGRectGetMaxY(txt_Middle_Name.frame), 270, 0.5);
        lbl_optional.frame = CGRectMake(230,CGRectGetMaxY(line2.frame),100, 15);
        
        txt_Last_Name .frame=CGRectMake(20, CGRectGetMaxY(line2.frame)+15, WIDTH-80, 38);
        line3.frame=CGRectMake(23, CGRectGetMaxY(txt_Last_Name.frame), 270, 0.5);
        
        Date_Of_Birth.frame = CGRectMake(20, CGRectGetMaxY(line3.frame)+15, WIDTH, 38);
        
        txt_Tel_Code  .frame=CGRectMake(20, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
        txt_Mobile_No   .frame=CGRectMake(80, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
        icon_dropdown.frame=CGRectMake(CGRectGetMaxX(txt_Tel_Code.frame)-203, CGRectGetMaxY(Date_Of_Birth.frame)+30, 15, 10);
        img_table_for_contry_code.frame  = CGRectMake(25,CGRectGetMaxY(icon_dropdown.frame)+5,50,100);
        line4.frame=CGRectMake(23, CGRectGetMaxY(txt_Tel_Code.frame), 50, 0.5);
        line5.frame=CGRectMake(CGRectGetMaxX(line4.frame)+10, CGRectGetMaxY(txt_Tel_Code.frame), 210, 0.5);
        
        txt_Gmail .frame=CGRectMake(20, CGRectGetMaxY(line5.frame)+15, WIDTH-10, 38);
        line6.frame=CGRectMake(23, CGRectGetMaxY(txt_Gmail.frame), 270, 0.5);
        
        page.frame = CGRectMake(125, CGRectGetMaxY(line6.frame)-10, WIDTH-80, 38);
        btn_Next.frame=CGRectMake(15,HEIGHT-65, WIDTH-30, 50);
        
    }
    else
    {
        scroll.frame = CGRectMake(5, 51, WIDTH-10, 320);
        
        user_img.frame = CGRectMake(95, CGRectGetMaxY(img_header.frame)-38, 120, 120);
        lbl_User_Info.frame = CGRectMake(90,CGRectGetMaxY(user_img.frame)+10,200, 15);
        
        txt_First_Name .frame=CGRectMake(20, CGRectGetMaxY(lbl_User_Info.frame)+15, WIDTH-80, 38);
        line.frame=CGRectMake(23, CGRectGetMaxY(txt_First_Name .frame), 270, 0.5);
    
        txt_Middle_Name.frame=CGRectMake(20, CGRectGetMaxY(line.frame)+15, WIDTH-80, 38);
        line2.frame=CGRectMake(23, CGRectGetMaxY(txt_Middle_Name.frame), 270, 0.5);
        lbl_optional.frame = CGRectMake(230,CGRectGetMaxY(line2.frame),100, 15);
        
        txt_Last_Name .frame=CGRectMake(20, CGRectGetMaxY(line2.frame)+15, WIDTH-80, 38);
        line3.frame=CGRectMake(23, CGRectGetMaxY(txt_Last_Name.frame), 270, 0.5);
        
        Date_Of_Birth.frame = CGRectMake(20, CGRectGetMaxY(line3.frame)+15, WIDTH, 38);
        
        txt_Tel_Code  .frame=CGRectMake(20, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
        txt_Mobile_No   .frame=CGRectMake(80, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
        icon_dropdown.frame=CGRectMake(CGRectGetMaxX(txt_Tel_Code.frame)-203, CGRectGetMaxY(Date_Of_Birth.frame)+25, 15, 10);
        img_table_for_contry_code.frame  = CGRectMake(25,CGRectGetMaxY(icon_dropdown.frame)+5,50,100);
        line4.frame=CGRectMake(23, CGRectGetMaxY(txt_Tel_Code.frame), 50, 0.5);
        line5.frame=CGRectMake(CGRectGetMaxX(line4.frame)+10, CGRectGetMaxY(txt_Tel_Code.frame), 210, 0.5);
        
        txt_Gmail .frame=CGRectMake(20, CGRectGetMaxY(line5.frame)+15, WIDTH-10, 38);
        line6.frame=CGRectMake(23, CGRectGetMaxY(txt_Gmail.frame), 270, 0.5);
        
        page.frame = CGRectMake(125, CGRectGetMaxY(line6.frame)-100, WIDTH-80, 38);
        btn_Next.frame=CGRectMake(15,HEIGHT-65, WIDTH-30, 50);
  
    }
    
    [scroll setContentSize:CGSizeMake(0,CGRectGetMaxY(page.frame)+80)];
    
    
}

#pragma table view delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 25;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPHONE_6Plus)
    {
        return 28;

    }
    else if (IS_IPHONE_6)
    {
        return 20;

    }
    else if(IS_IPHONE_5)
    {
        return 35;

    }
    else
    {
      return 20;
    }
   }
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews)
    {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor redColor];
    
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    //[img_cellBackGnd setImage:[UIImage imageNamed:@"img-white-bg1@2x.png"]];
    img_cellBackGnd.backgroundColor = [UIColor purpleColor];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [cell.contentView addSubview:img_cellBackGnd];
    
    UILabel *cuntry_code = [[UILabel alloc]init];
    cuntry_code.text = @"+65";
    cuntry_code.font = [UIFont fontWithName:kFont size:15];
    cuntry_code.textColor = [UIColor lightGrayColor];
    cuntry_code.backgroundColor = [UIColor clearColor];
    [ img_cellBackGnd addSubview:cuntry_code];

    
//    UIImageView *white_bg = [[UIImageView alloc]init];
//    white_bg.frame =  CGRectMake(0,0, WIDTH-15, 37);
//    [white_bg setImage:[UIImage imageNamed:@"img-white-bg1@2x.png"]];
//    [white_bg setUserInteractionEnabled:YES];
//    [img_cellBackGnd addSubview:white_bg];

    
    if (IS_IPHONE_6Plus)
    {
       img_cellBackGnd.frame =  CGRectMake(0,3, 50, 30);
    cuntry_code.frame = CGRectMake(05, 5, 50, 20);

        // white_bg.frame =  CGRectMake(0,0, 40,30);
    }
    else if (IS_IPHONE_6)
    {
        img_cellBackGnd.frame =  CGRectMake(0,10, 50, 30);
         cuntry_code.frame = CGRectMake(05, 5, 50, 20);
        
    }
    else if(IS_IPHONE_5)
    {
        img_cellBackGnd.frame =  CGRectMake(0,3, 50, 60);
         cuntry_code.frame = CGRectMake(05, 5, 50, 20);
    }
    else
    {
        img_cellBackGnd.frame =  CGRectMake(0,3, 50, 30);
         cuntry_code.frame = CGRectMake(05, 5, 50, 20);

    }

    
    
    
    return cell;
    
}

-(void)btn_back_click:(UIButton *)sender
{
    NSLog(@"btn_back_click:");
    [self.navigationController popViewControllerAnimated:NO];
    
}
-(void)user_img_click:(UIButton *)sender
{
    NSLog(@"user_img_click:");
    
}
-(void)icon_drop_down_click:(UIButton *)sender
{
    NSLog(@"icon_drop_down_click:");
//    img_table_for_contry_code.hidden =NO;
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        img_table_for_contry_code.hidden=NO;
        
    }
    else
    {
        [sender setSelected:NO];
        img_table_for_contry_code.hidden=YES;
    }

   // img_table_for_contry_code.hidden =YES;

    

    
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}
-(void)btn_next_click:(UIButton *)sender
{
    NSLog(@"btn_nextclick:");
//    UserRegistrationStep2VC*vc = [[UserRegistrationStep2VC alloc]init];
//    [self.navigationController pushViewController:vc animated:NO];
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}

#pragma mark TextField Delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //    if(textField == txt_username)
    //    {
    //        if ([string isEqualToString:@" "])
    //        {
    //            return NO;
    //        }
    //    }
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
