//
//  AppDelegate.m
//  Not86
//
//  Created by Interwld on 7/29/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "AppDelegate.h"
#import "SplashScreensViewController.h"
#import "HomeViewController1.h"
#import "LoginViewController.h"
#import "ChefSignup.h" 
#import "ChefSignup2.h"
#import "Chefsignup3.h"
#import "ChefSignup4.h"
#import "ChefSignup5.h"
#import "ChefSignup6.h"


#import "ChefHomeViewController.h"
#import "ChefHomeViewSecondController.h"
#import "MenuScreenVC.h"

#import "SignupViewController.h"
#import "ChefServeNowVC.h"
#import "MenuScreenVC.h"
#import "MyScheduleScreenVC.h"

#import "SignupViewController.h"
#import "MyScheduleScreenVC.h"
//chef side
#import "ChefProfileInChefSideVC.h"

//#import "ServeNowVC.h"
//#import "ServeLaterVC.h"
#import "ChefOrdersVC.h"
#import "DeliverDetailsVC.h"

//USER
#import "UserLogonVC.h"
#import "UserLoginVC.h"
#import "UserSignUpVC.h"
#import "UserRegistration1VC.h"
#import "UserRegistration2VC.h"
#import "HomeVC.h"
#import "UserSearchVC.h"
#import "ItemDetailVC.h"
#import "UserDishRivewsVC.h"
#import "AddToCartFoodLaterVC.h"
#import "AddToCartFoodNowVC.h"
#import "ChefReviwesVC.h"
#import "UserProfileVC.h"

#import "ChefProfileVC.h"
#import "SerchForFoodNowVC.h"

#import "OrderDetailsInProgressVC.h"
#import "OrderDetailsVC.h"
#import "DiningCartVC.h"
#import "ViewDishVC.h"
#import "CheckOutVC.h"
#import "AddressDetailsVC.h"

#import "PersonalDetailsVC.h"
#import "ProceedToPaymentVC.h"

#import "NotificationsVC.h"
#import "UserMessagesVC.h"
#import "UserSupportVC.h"
#import "UserNotificationsVC.h"




@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize activityIndicator;
@synthesize isFBLogin;
@synthesize navigationC;
@synthesize devicestr;
@synthesize str_Rotation;
NSString *str_appState;
UIAlertView *alert_view;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
     [[UIApplication sharedApplication] setStatusBarHidden:YES];
//    ChefSignup2 *chefsignup=[[ChefSignup alloc]initWithNibName:@"ChefSignup" bundle:[NSBundle mainBundle]];
//    navigationC=[[UINavigationController alloc]initWithRootViewController:chefsignup];
//    self.window.rootViewController=navigationC;
    ChefProfileInChefSideVC *SplashScreen=[ChefProfileInChefSideVC new];
    navigationC = [[UINavigationController alloc]
                   initWithRootViewController:SplashScreen];
    navigationC.navigationBarHidden = YES;
    self.window.rootViewController=navigationC;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
   
    if (IS_OS_8_OR_LATER)
    {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
        
        
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert |UIRemoteNotificationTypeBadge |UIRemoteNotificationTypeSound |
         UIRemoteNotificationTypeNewsstandContentAvailability)];
        
        
    }
    
    
    
    //*************** For the Activity Indicator *******************
    self.activityIndicator = [[UIActivityIndicatorView alloc]init];
    self.activityIndicator.frame = CGRectMake(138,IS_IPHONE_5?242:205,50,50);
    self.activityIndicator.backgroundColor = [UIColor clearColor];
    [ self.activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicator.color = [UIColor colorWithRed:29/255.0f green:59/255.0f blue:148/255.0f alpha:1];
    [self.window addSubview:self.activityIndicator];
    [self.window makeKeyAndVisible];
    
    
    
    //  For Starting the application
    
       return YES;
}





#pragma mark ------------------------- PUSH NOTIFICATION-----------------------START

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *str = [NSString stringWithFormat:@"%@",deviceToken];
    self.devicestr=	str;
    self.devicestr=[self.devicestr stringByReplacingOccurrencesOfString:@"<" withString:@""];
    self.devicestr=[self.devicestr stringByReplacingOccurrencesOfString:@">" withString:@""];
    
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    
    NSLog(@"notifications  %@",userInfo);
    NSString *text= [[userInfo valueForKey:@"aps"] valueForKey:@"alert"] ;
    NSString *type= [[userInfo valueForKey:@"aps"] valueForKey:@"type"] ;
    NSString *badgeno= [[userInfo valueForKey:@"aps"] valueForKey:@"badge"] ;
    int bad_no=[badgeno intValue];
    
    //here i am increasing cdhat count and notification count
    
    
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateNotification" object:userInfo userInfo:nil];
    
    //-----------------------APNS HANDLE----------------
    
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive)
    {
        application.applicationIconBadgeNumber=0;
        //        NSString *decoded = [text stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //        UIAlertView *alert_view = [[UIAlertView alloc] initWithTitle:@"Metta" message:decoded delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //
        //        [alert_view show];
        NSString *decoded = [NSString stringWithFormat:@"%@",[[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        alert_view = [[UIAlertView alloc] initWithTitle:@"Metta" message:decoded delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert_view show];
        
    }
    //=====================================================
    
    else if((state==UIApplicationStateBackground) || (state==UIApplicationStateInactive))
    {
        str_appState=@"inactive";
        
        // here i am increasing the badges
        application.applicationIconBadgeNumber=bad_no;
        NSString *decoded = [NSString stringWithFormat:@"%@",[[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        alert_view = [[UIAlertView alloc] initWithTitle:@"Metta" message:decoded delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert_view show];
    }
    
    // For the Activity Indicator ****
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(140, IS_IPHONE_5?264:220, 80, 80)];
    self.activityIndicator.backgroundColor = [UIColor clearColor];
    [self.activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.window addSubview:self.activityIndicator];
    
    
    [self.window makeKeyAndVisible];
    
    
    
    
}
-(void)callME
{

}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
