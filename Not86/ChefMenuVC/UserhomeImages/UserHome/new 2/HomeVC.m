//
//  HomeVC.m
//  Not86
//
//  Created by Admin on 29/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "HomeVC.h"
//#import "UserFoodInfoVC.h"
//#import "SearchForMoreFiltersVC.h"
//#import "SerchForFoodLaterVC.h"
//#import "SerchForFoodNowVC.h"
//#import "UserDishRivewsVC.h"
////#import "ChefProfileVC.h"
//#import "AddToCartFoodNowVC.h"
//#import "ItemDetailVC.h"
//#import "UserSerchVC.h"



#import "Define.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"



@interface HomeVC ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIImageView *img_header;
    UIImageView *img_background;
    UITableView *img_table;
    NSMutableArray*ary_displaynames;
    NSMutableArray * array_img;
    
    
}

@end




@implementation HomeVC


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    ary_displaynames =[[NSMutableArray alloc]initWithObjects:@"Steamed Thai otah",@"Rasberry custored", nil];
    array_img = [[NSMutableArray alloc]initWithObjects:@"dish1-img@2x.png",@"dish2-img@2x.png", nil];
    [self integrateHeader];
   [self integrateBodyDesign];
    
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}


-(void)integrateHeader
{
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu .frame = CGRectMake(10, 13,20,20);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu  addTarget:self action:@selector(btn_action_on_menu:) forControlEvents:UIControlEventTouchUpInside];
    [icon_menu setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_menu ];
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 150, 45);
    lbl_User_Sign_Up.text = @"Home";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    UIButton *icon_serch = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_serch .frame = CGRectMake(WIDTH-65, 13, 20, 20);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_serch  addTarget:self action:@selector(btn_action_on_serch:) forControlEvents:UIControlEventTouchUpInside];
    [icon_serch setImage:[UIImage imageNamed:@"search-icon@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_serch ];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-35, 9, 27, 27);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
    
}


-(void)integrateBodyDesign
{
    
    
    img_background = [[UIImageView alloc]init];
    //  img_background .frame = CGRectMake(0, CGRectGetMaxY(img_header.frame), WIDTH, 210);
    [img_background setImage:[UIImage imageNamed:@"home-bg@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_background setUserInteractionEnabled:YES];
    [self.view addSubview:img_background];
    
    
    UIButton *icon_Food_Now = [UIButton buttonWithType:UIButtonTypeCustom];
    // icon_Food_Now .frame = CGRectMake(20, CGRectGetMaxY(img_header.frame)-20, 125, 60);
    //icon_Food_Now .backgroundColor = [UIColor clearColor];
    [icon_Food_Now  addTarget:self action:@selector(icon_Food_Now_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_Food_Now setImage:[UIImage imageNamed:@"img-food-now@2x.png"] forState:UIControlStateNormal];
    [img_background   addSubview:icon_Food_Now ];
    
    UIButton *icon_Food_Later = [UIButton buttonWithType:UIButtonTypeCustom];
    //  icon_Food_Later.frame = CGRectMake(175, CGRectGetMaxY(img_header.frame)-20, 125, 60);
    //icon_Food_Later .backgroundColor = [UIColor clearColor];
    [icon_Food_Later  addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_Food_Later setImage:[UIImage imageNamed:@"food-later-img@2x.png"] forState:UIControlStateNormal];
    [img_background   addSubview:icon_Food_Later];
    
    
    UILabel *dish_number = [[UILabel alloc]init];
    //   dish_number.frame = CGRectMake(27,CGRectGetMaxY(img_header.frame)+70,100, 15);
    dish_number.text = @"1234";
    dish_number.font = [UIFont fontWithName:kFont size:20];
    dish_number.textColor = [UIColor whiteColor];
    dish_number.backgroundColor = [UIColor clearColor];
    [img_background  addSubview:dish_number];
    
    
    UILabel *favorite_dishes = [[UILabel alloc]init];
    //   favorite_dishes.frame = CGRectMake(10,CGRectGetMaxY(dish_number.frame)+10,100, 15);
    favorite_dishes.text = @"Favorite Items";
    favorite_dishes.font = [UIFont fontWithName:kFontBold size:15];
    favorite_dishes.textColor = [UIColor whiteColor];
    favorite_dishes.backgroundColor = [UIColor clearColor];
    [img_background  addSubview:favorite_dishes];
    
    UIButton *btn_favorite_dishes = [UIButton buttonWithType:UIButtonTypeCustom];
    //  btn_favorite_dishes .frame = CGRectMake(10, CGRectGetMaxY(img_header.frame)+63, 100, 60);
    btn_favorite_dishes  .backgroundColor = [UIColor clearColor];
    [btn_favorite_dishes   addTarget:self action:@selector(btn_favorite_dish_click:) forControlEvents:UIControlEventTouchUpInside];
    //    [btn_favorite_dishes  setImage:[UIImage imageNamed:@"img-food-later@2x.png"] forState:UIControlStateNormal];
    [img_background   addSubview:btn_favorite_dishes ];
    
    
    UIImageView *img_line1 = [[UIImageView alloc]init];
    //    img_line1 .frame =  CGRectMake(110,CGRectGetMaxY(img_header.frame)+60, 1, 30);
    [img_line1  setImage:[UIImage imageNamed:@"small-line@2x.png"]];
    [img_line1  setUserInteractionEnabled:YES];
    [img_background addSubview:img_line1 ];
    
    UILabel *cuisines_number = [[UILabel alloc]init];
    //  cuisines_number.frame = CGRectMake(140,CGRectGetMaxY(img_header.frame)+70,85, 15);
    cuisines_number.text = @"1234";
    cuisines_number.font = [UIFont fontWithName:kFont size:20];
    cuisines_number.textColor = [UIColor whiteColor];
    cuisines_number.backgroundColor = [UIColor clearColor];
    [img_background addSubview:cuisines_number];
    
    
    UILabel *favorite_cuisines = [[UILabel alloc]init];
    //  favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,110, 15);
    favorite_cuisines.text = @"Favorite Cuisines";
    favorite_cuisines.font = [UIFont fontWithName:kFontBold size:13.5];
    favorite_cuisines.textColor = [UIColor whiteColor];
    favorite_cuisines.backgroundColor = [UIColor clearColor];
    [img_background  addSubview:favorite_cuisines];
    
    UIButton *btn_favorite_cuisines = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
    btn_favorite_cuisines  .backgroundColor = [UIColor clearColor];
    [btn_favorite_cuisines   addTarget:self action:@selector(btn_favorite_cuisines_click:) forControlEvents:UIControlEventTouchUpInside];
    //    [btn_favorite_dishes  setImage:[UIImage imageNamed:@"img-food-later@2x.png"] forState:UIControlStateNormal];
    [img_background   addSubview:btn_favorite_cuisines ];
    
    
    UIImageView *img_line2 = [[UIImageView alloc]init];
    //   img_line2.frame =  CGRectMake(230,CGRectGetMaxY(img_header.frame)+60, 1, 30);
    [img_line2 setImage:[UIImage imageNamed:@"small-line@2x.png"]];
    [img_line2 setUserInteractionEnabled:YES];
    [img_background addSubview:img_line2];
    
    UILabel *all_dish_numbers = [[UILabel alloc]init];
    //    all_dish_numbers.frame = CGRectMake(255,CGRectGetMaxY(img_header.frame)+70,200, 15);
    all_dish_numbers.text = @"1234";
    all_dish_numbers.font = [UIFont fontWithName:kFont size:20];
    all_dish_numbers.textColor = [UIColor whiteColor];
    all_dish_numbers.backgroundColor = [UIColor clearColor];
    [img_background addSubview:all_dish_numbers];
    
    
    UILabel *all_dishes = [[UILabel alloc]init];
    //   all_dishes.frame = CGRectMake(245,CGRectGetMaxY(all_dish_numbers.frame)+10,200, 15);
    all_dishes.text = @"All Items";
    all_dishes.font = [UIFont fontWithName:kFontBold size:15];
    all_dishes.textColor = [UIColor whiteColor];
    all_dishes.backgroundColor = [UIColor clearColor];
    [img_background  addSubview:all_dishes];
    
    UIButton *btn_all_dishes = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_all_dishes .frame = CGRectMake(232, CGRectGetMaxY(img_header.frame)+60, 100, 63);
    btn_all_dishes  .backgroundColor = [UIColor clearColor];
    [btn_all_dishes   addTarget:self action:@selector(btn_all_dishes_click:) forControlEvents:UIControlEventTouchUpInside];
    //    [btn_favorite_dishes  setImage:[UIImage imageNamed:@"img-food-later@2x.png"] forState:UIControlStateNormal];
    [img_background   addSubview:btn_all_dishes ];
    
    UIImageView *img_line =[[UIImageView alloc]init];
    //   img_line.frame = CGRectMake(10, CGRectGetMaxY(favorite_dishes .frame)+13, WIDTH-25, 1);
    [img_line setUserInteractionEnabled:YES];
    img_line.backgroundColor=[UIColor clearColor];
    img_line.image=[UIImage imageNamed:@"img-white-line@2x.png"];
    [img_background addSubview:img_line];
    
    
    UILabel *serv_now = [[UILabel alloc]init];
    //    serv_now .frame = CGRectMake(95,CGRectGetMaxY(img_line.frame)+5,200, 28);
    serv_now .text = @"SERVING NOW";
    serv_now .font = [UIFont fontWithName:kFont size:20];
    serv_now .textColor = [UIColor whiteColor];
    serv_now .backgroundColor = [UIColor clearColor];
    [img_background  addSubview:serv_now ];
    
    UIButton *btn_sevrving_now = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_sevrving_now .frame = CGRectMake(0, CGRectGetMaxY(img_line.frame)+2,320, 37);
    btn_sevrving_now  .backgroundColor = [UIColor clearColor];
    [btn_sevrving_now   addTarget:self action:@selector(btn_serving_now_click:) forControlEvents:UIControlEventTouchUpInside];
    //    [btn_favorite_dishes  setImage:[UIImage imageNamed:@"img-food-later@2x.png"] forState:UIControlStateNormal];
    [img_background   addSubview:btn_sevrving_now ];
    
    
    UITextField *text_favorite_dishes = [[UITextField alloc]init];
    //   text_favorite_dishes.frame = CGRectMake(13, CGRectGetMaxY(img_background.frame)-40, WIDTH-80, 30);
    text_favorite_dishes.text = @"Favorite Items Serving Now";
    text_favorite_dishes.font = [UIFont fontWithName:kFontBold size:16];
    text_favorite_dishes.textColor = [UIColor blackColor];
    text_favorite_dishes.backgroundColor = [UIColor clearColor];
    [img_background addSubview:text_favorite_dishes];
    
    
    
    
    
#pragma mark Tableview
    
    img_table= [[UITableView alloc] init ];
    //   img_table.frame  = CGRectMake(13,290,296,370);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    img_table.backgroundColor = [UIColor clearColor];
    [self.view addSubview:img_table];
    
    
    
    if (IS_IPHONE_6Plus)
    {
        img_background .frame = CGRectMake(0, CGRectGetMaxY(img_header.frame), WIDTH, 220);
        icon_Food_Now .frame = CGRectMake(-25, CGRectGetMaxY(img_header.frame)-50, 264, 115);
        icon_Food_Later.frame = CGRectMake(175, CGRectGetMaxY(img_header.frame)-50, 264, 115);
        dish_number.frame = CGRectMake(30,CGRectGetMaxY(img_header.frame)+67,100, 20);
        favorite_dishes.frame = CGRectMake(13,CGRectGetMaxY(dish_number.frame)+10,150, 15);
        //favorite_dishes.frame = CGRectMake(10,CGRectGetMaxY(dish_number.frame)+10,100, 15);
        // favorite_dishes.frame = CGRectMake(10,CGRectGetMaxY(dish_number.frame)+10,100, 15);
        btn_favorite_dishes .frame = CGRectMake(0, CGRectGetMaxY(img_header.frame)+63, 100, 60);
        img_line1 .frame =  CGRectMake(130,CGRectGetMaxY(img_header.frame)+60, 1, 40);
        cuisines_number.frame = CGRectMake(170,CGRectGetMaxY(img_header.frame)+70,85, 15);
        //  cuisines_number.frame = CGRectMake(140,CGRectGetMaxY(img_header.frame)+70,85, 15);
        favorite_cuisines.frame = CGRectMake(140,CGRectGetMaxY(cuisines_number.frame)+12,180, 15);
        // favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,110, 15);
        // favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,110, 15);
        // favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,110, 15);
        //favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,110, 15);
        btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
        // btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
        //  btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
        img_line2.frame =  CGRectMake(260,CGRectGetMaxY(img_header.frame)+60, 1, 40);
        all_dish_numbers.frame = CGRectMake(312,CGRectGetMaxY(img_header.frame)+70,200, 15);
        all_dishes.frame = CGRectMake(315,CGRectGetMaxY(all_dish_numbers.frame)+12,200, 15);
        //all_dishes.frame = CGRectMake(245,CGRectGetMaxY(all_dish_numbers.frame)+10,200, 15);
        btn_all_dishes .frame = CGRectMake(232, CGRectGetMaxY(img_header.frame)+60, 150, 63);
        img_line.frame = CGRectMake(10, CGRectGetMaxY(favorite_dishes .frame)+13, WIDTH-25, 1);
        serv_now .frame = CGRectMake(120,CGRectGetMaxY(img_line.frame)+8,200, 28);
        btn_sevrving_now .frame = CGRectMake(0, CGRectGetMaxY(img_line.frame)+2,320, 37);
        text_favorite_dishes.frame = CGRectMake(15, CGRectGetMaxY(img_background.frame)-45, WIDTH-80, 30);
        //  text_favorite_dishes.frame = CGRectMake(13, CGRectGetMaxY(img_background.frame)-40, WIDTH-80, 30);
        img_table.frame  = CGRectMake(0,300,WIDTH,480);
        
        
    }
    else if (IS_IPHONE_6)
    {
        img_background .frame = CGRectMake(0, CGRectGetMaxY(img_header.frame), WIDTH, 220);
        icon_Food_Now .frame = CGRectMake(-34, CGRectGetMaxY(img_header.frame)-50, 264, 115);
        icon_Food_Later.frame = CGRectMake(147, CGRectGetMaxY(img_header.frame)-50, 264, 115);
        dish_number.frame = CGRectMake(30,CGRectGetMaxY(img_header.frame)+67,100, 20);
        favorite_dishes.frame = CGRectMake(13,CGRectGetMaxY(dish_number.frame)+10,150, 15);
        //favorite_dishes.frame = CGRectMake(10,CGRectGetMaxY(dish_number.frame)+10,100, 15);
        // favorite_dishes.frame = CGRectMake(10,CGRectGetMaxY(dish_number.frame)+10,100, 15);
        btn_favorite_dishes .frame = CGRectMake(0, CGRectGetMaxY(img_header.frame)+63, 100, 60);
        img_line1 .frame =  CGRectMake(130,CGRectGetMaxY(img_header.frame)+60, 1, 40);
        cuisines_number.frame = CGRectMake(170,CGRectGetMaxY(img_header.frame)+70,85, 15);
        //  cuisines_number.frame = CGRectMake(140,CGRectGetMaxY(img_header.frame)+70,85, 15);
        favorite_cuisines.frame = CGRectMake(140,CGRectGetMaxY(cuisines_number.frame)+12,180, 15);
        // favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,110, 15);
        // favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,110, 15);
        // favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,110, 15);
        //favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,110, 15);
        btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
        // btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
        //  btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
        img_line2.frame =  CGRectMake(260,CGRectGetMaxY(img_header.frame)+60, 1, 40);
        all_dish_numbers.frame = CGRectMake(290,CGRectGetMaxY(img_header.frame)+70,200, 15);
        all_dishes.frame = CGRectMake(290,CGRectGetMaxY(all_dish_numbers.frame)+12,200, 15);
        //all_dishes.frame = CGRectMake(245,CGRectGetMaxY(all_dish_numbers.frame)+10,200, 15);
        btn_all_dishes .frame = CGRectMake(232, CGRectGetMaxY(img_header.frame)+60, 150, 63);
        img_line.frame = CGRectMake(10, CGRectGetMaxY(favorite_dishes .frame)+13, WIDTH-25, 1);
        serv_now .frame = CGRectMake(120,CGRectGetMaxY(img_line.frame)+8,200, 28);
        btn_sevrving_now .frame = CGRectMake(0, CGRectGetMaxY(img_line.frame)+2,320, 37);
        text_favorite_dishes.frame = CGRectMake(15, CGRectGetMaxY(img_background.frame)-45, WIDTH-80, 30);
        // text_favorite_dishes.frame = CGRectMake(13, CGRectGetMaxY(img_background.frame)-40, WIDTH-80, 30);
        img_table.frame  = CGRectMake(5,300,WIDTH-10,380);
        
    }
    else
    {
        img_background .frame = CGRectMake(0, CGRectGetMaxY(img_header.frame), WIDTH, 210);
        icon_Food_Now .frame = CGRectMake(20, CGRectGetMaxY(img_header.frame)-20, 125, 60);
        icon_Food_Later.frame = CGRectMake(175, CGRectGetMaxY(img_header.frame)-20, 125, 60);
        
        dish_number.frame = CGRectMake(27,CGRectGetMaxY(img_header.frame)+70,100, 15);
        favorite_dishes.frame = CGRectMake(10,CGRectGetMaxY(dish_number.frame)+10,150, 15);
        //        favorite_dishes.frame = CGRectMake(10,CGRectGetMaxY(dish_number.frame)+10,150, 15);
        //        favorite_dishes.frame = CGRectMake(10,CGRectGetMaxY(dish_number.frame)+10,150, 15);
        btn_favorite_dishes .frame = CGRectMake(10, CGRectGetMaxY(img_header.frame)+63, 100, 60);
        img_line1 .frame =  CGRectMake(114,CGRectGetMaxY(img_header.frame)+60, 1, 34);
        cuisines_number.frame = CGRectMake(140,CGRectGetMaxY(img_header.frame)+70,85, 15);
        cuisines_number.frame = CGRectMake(140,CGRectGetMaxY(img_header.frame)+70,85, 15);
        favorite_cuisines.frame = CGRectMake(120,CGRectGetMaxY(cuisines_number.frame)+10,180, 15);
        btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
        btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
        btn_favorite_cuisines .frame = CGRectMake(113, CGRectGetMaxY(img_header.frame)+60, 115, 63);
        img_line2.frame =  CGRectMake(234,CGRectGetMaxY(img_header.frame)+60, 1, 34);
        all_dish_numbers.frame = CGRectMake(255,CGRectGetMaxY(img_header.frame)+70,200, 15);
        all_dishes.frame = CGRectMake(245,CGRectGetMaxY(all_dish_numbers.frame)+10,200, 15);
        all_dishes.frame = CGRectMake(245,CGRectGetMaxY(all_dish_numbers.frame)+10,200, 15);
        btn_all_dishes .frame = CGRectMake(232, CGRectGetMaxY(img_header.frame)+60, 100, 63);
        img_line.frame = CGRectMake(10, CGRectGetMaxY(favorite_dishes .frame)+13, WIDTH-25, 1);
        serv_now .frame = CGRectMake(95,CGRectGetMaxY(img_line.frame)+5,200, 28);
        btn_sevrving_now .frame = CGRectMake(0, CGRectGetMaxY(img_line.frame)+2,320, 37);
        text_favorite_dishes.frame = CGRectMake(15, CGRectGetMaxY(img_background.frame)-45, WIDTH-80, 30);
        // text_favorite_dishes.frame = CGRectMake(13, CGRectGetMaxY(img_background.frame)-40, WIDTH-80, 30);
        img_table.frame  = CGRectMake(5,290,WIDTH-5,IS_IPHONE_5?275:195);
        
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ary_displaynames count];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 170;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH+5, 170);
        
    }
    else if (IS_IPHONE_6)
    {
        img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-5, 170);
        
    }
    else
    {    img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-10,170);
        
    }
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    img_cellBackGnd.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [cell.contentView addSubview:img_cellBackGnd];
    
    
    
    UIImageView *img_dish = [[UIImageView alloc] init];
    //    img_dish.frame = CGRectMake(7,7, 90,  95 );
    [img_dish setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_img objectAtIndex:indexPath.row]]]];
    [img_cellBackGnd addSubview:img_dish];
    
    UIButton *btn_favorite = [UIButton buttonWithType:UIButtonTypeCustom];
    //  btn_favorite.frame = CGRectMake(5,58, 35, 35);
    //icon_cow .backgroundColor = [UIColor clearColor];
    [btn_favorite addTarget:self action:@selector(btn_favorite_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_favorite setImage:[UIImage imageNamed:@"favorite-icon@2x.png"] forState:UIControlStateNormal];
    [img_dish   addSubview:btn_favorite];
    
    UIButton *btn_number_three = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_number_three.frame = CGRectMake(58,-5, 35, 35);
    //icon_cow .backgroundColor = [UIColor clearColor];
    [btn_number_three addTarget:self action:@selector(btn_number_three_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_number_three setImage:[UIImage imageNamed:@"red3-icon@2x.png"] forState:UIControlStateNormal];
    [img_dish   addSubview:btn_number_three];
    
    
    UIButton *icon_delete = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_delete.frame = CGRectMake(WIDTH-70,20, 40, 40);
    //icon_cow .backgroundColor = [UIColor clearColor];
    [icon_delete addTarget:self action:@selector(btn_add_to_cart_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_delete setImage:[UIImage imageNamed:@"img-cart@2x.png"] forState:UIControlStateNormal];
    [cell.contentView   addSubview:icon_delete];
    
    UILabel *dish_name = [[UILabel alloc]init];
    //   dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
    dish_name.text = [NSString stringWithFormat:@"%@",[ary_displaynames objectAtIndex:indexPath.row]];
    dish_name.font = [UIFont fontWithName:kFontBold size:8];
    dish_name.textColor = [UIColor blackColor];
    dish_name.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:dish_name];
    
    
    UIImageView *icon_location = [[UIImageView alloc] init];
    //    icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
    [icon_location setImage:[UIImage imageNamed:@"location-icon@2x.png"]];
    [img_cellBackGnd  addSubview:icon_location];
    
    UILabel *meters = [[UILabel alloc]init];
    //  meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
    meters.text = @"5km";
    meters.font = [UIFont fontWithName:kFont size:15];
    meters.textColor = [UIColor blackColor];
    meters.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:meters];
    
    UIButton *icon_server = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
    //icon_server .backgroundColor = [UIColor clearColor];
    [icon_server  addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_server setImage:[UIImage imageNamed:@"serving-icon@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_server];
    
    UIButton *icon_halal = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
    //icon_halal .backgroundColor = [UIColor clearColor];
    [icon_halal addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_halal setImage:[UIImage imageNamed:@"halal-icon@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_halal];
    
    UIImageView *img_non_veg = [[UIImageView alloc] init];
    //  img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
    [img_non_veg setImage:[UIImage imageNamed:@"non-veg bt@2x.png"]];
    [img_cellBackGnd addSubview:img_non_veg];
    
    UIButton *icon_cow = [UIButton buttonWithType:UIButtonTypeCustom];
    //  icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
    //icon_cow .backgroundColor = [UIColor clearColor];
    [icon_cow addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_cow setImage:[UIImage imageNamed:@"cow-icon@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_cow];
    
    UIButton *icon_fronce = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
    //icon_cow .backgroundColor = [UIColor clearColor];
    [icon_fronce addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_fronce setImage:[UIImage imageNamed:@"fronce-icon@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_fronce];
    
    UILabel *doller_rate = [[UILabel alloc]init];
    //   doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
    doller_rate.text = @"$14.90";
    doller_rate.font = [UIFont fontWithName:kFontBold size:16];
    doller_rate.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    doller_rate.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:doller_rate];
    
    UIImageView *img_line = [[UIImageView alloc] init];
    //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
    [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
    [img_cellBackGnd addSubview:img_line];
    
    
    UIButton *icon_take = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
    //icon_take .backgroundColor = [UIColor clearColor];
    [icon_take addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_take setImage:[UIImage imageNamed:@"take-icon@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_take];
    
    UIButton *icon_deliver = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //icon_deliver .backgroundColor = [UIColor clearColor];
    [icon_deliver addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_deliver setImage:[UIImage imageNamed:@"deliver-icon@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_deliver];
    
    
    UIButton *img_btn_seving_Now = [UIButton buttonWithType:UIButtonTypeCustom];
    //   img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //img_btn_deliver_Now .backgroundColor = [UIColor clearColor];
    [img_btn_seving_Now addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [img_btn_seving_Now setImage:[UIImage imageNamed:@"now-icon@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:img_btn_seving_Now];
    
    
    UIButton *img_btn_chef_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    //    img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
    //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
    [img_btn_chef_menu addTarget:self action:@selector(btn_chef_menu_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_btn_chef_menu setImage:[UIImage imageNamed:@"chef-menu-icon@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:img_btn_chef_menu];
    
    
    
    UIButton *icon_thumb = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+5, 20, 20);
    //icon_thumb .backgroundColor = [UIColor clearColor];
    [icon_thumb addTarget:self action:@selector(icon_Food_Later_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_thumb setImage:[UIImage imageNamed:@"thumb-icon@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_thumb];
    
    UILabel *likes = [[UILabel alloc]init];
    //  likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
    likes.text = @"87.4%";
    likes.font = [UIFont fontWithName:kFont size:10];
    likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    likes.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:likes];
    
    if (IS_IPHONE_6Plus)
    {
        img_dish.frame = CGRectMake(7,7, 90,  95 );
        btn_favorite.frame = CGRectMake(5,58, 35, 35);
        btn_number_three.frame = CGRectMake(58,-5, 35, 35);
        
        icon_delete.frame = CGRectMake(WIDTH-50,20, 40, 40);
        
        dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
        icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
        icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
        img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
        icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
        icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
        doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
        img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
        icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+9, 20, 20);
        likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+9,128,100, 10);
        
        dish_name.font = [UIFont fontWithName:kFontBold size:18];
        
    }
    else if (IS_IPHONE_6)
    {
        img_dish.frame = CGRectMake(7,7, 90,  95 );
        btn_favorite.frame = CGRectMake(5,58, 35, 35);
        btn_number_three.frame = CGRectMake(58,-5, 35, 35);
        
        icon_delete.frame = CGRectMake(WIDTH-62,20, 40, 40);
        
        dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
        icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
        icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
        img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
        icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
        icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
        doller_rate.frame = CGRectMake(WIDTH-90,85,200, 15);
        img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
        icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        icon_thumb.frame = CGRectMake(280,CGRectGetMaxY(img_line.frame)+5, 20, 20);
        likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
        
        
        dish_name.font = [UIFont fontWithName:kFontBold size:18];
        
    }
    else
    {
        img_dish.frame = CGRectMake(7,7, 90,  95 );
        btn_favorite.frame = CGRectMake(5,58, 35, 35);
        btn_number_three.frame = CGRectMake(58,-5, 35, 35);
        
        icon_delete.frame = CGRectMake(WIDTH-65,20, 40, 40);
        
        dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
        icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
        icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
        img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
        icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
        icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
        doller_rate.frame = CGRectMake(IS_IPHONE_5?WIDTH-80:WIDTH-80,85,200, 15);
        img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
        icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        icon_thumb.frame = CGRectMake(IS_IPHONE_5?330:230,CGRectGetMaxY(img_line.frame)+5, 20, 20);
        likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
        
        dish_name.font = [UIFont fontWithName:kFontBold size:IS_IPHONE_5?15:15];
        
    }
    
    
    return cell;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // ItemDetailVC *VC = [[ItemDetailVC alloc] init];
    //[self.navigationController pushViewController:VC animated:NO];
    
}

#pragma mark Click Events
-(void)btn_action_on_menu:(UIButton *)sender
{
    NSLog(@"btn_action_on_menu");
    //   ChefProfileVC*vc = [[ChefProfileVC alloc]init];
    //    [self.navigationController pushViewController:vc animated:NO];
    
}
-(void)btn_action_on_serch:(UIButton *)sender
{
    NSLog(@"btn_action_on_serch");
   // UserSerchVC *vc = [[UserSerchVC alloc]init];
    //[self.navigationController pushViewController:vc animated:NO];
}

-(void)btn_number_three_click:(UIButton *)sender
{
    NSLog(@"btn_number_three_click:");
}


-(void)icon_Food_Now_BtnClick:(UIButton *)sender
{
    NSLog(@"icon_Drop_Down Btn Click");
    //    SearchForMoreFiltersVC*vc = [[SearchForMoreFiltersVC alloc]init];
    //    [self.navigationController pushViewController:vc animated:NO];
   // SerchForFoodNowVC *vc = [[SerchForFoodNowVC alloc]init];
   // [self.navigationController pushViewController:vc animated:NO];
    
}
-(void)icon_Food_Later_BtnClick:(UIButton *)sender
{
    NSLog(@"icon_Drop_Down Btn Click");
   // SerchForFoodLaterVC*vc = [[SerchForFoodLaterVC alloc]init];
   // [self.navigationController pushViewController:vc animated:NO];
    
}
-(void)btn_favorite_click:(UIButton *)sender
{
    NSLog(@"btn_favorite_click");
}

-(void)btn_add_to_cart_click:(UIButton *)sender
{
    NSLog(@"btn_add_to_cart_click:");
   // AddToCartFoodNowVC *vc = [[AddToCartFoodNowVC alloc]init];
   // [self.navigationController pushViewController:vc animated:NO];
}

-(void)btn_favorite_dish_click:(UIButton *)sender
{
    NSLog(@"btn_favorite_dish_click:");
}
-(void)btn_favorite_cuisines_click:(UIButton *)sender
{
    NSLog(@"btn_favorite_cuisines_click");
}
-(void)btn_all_dishes_click:(UIButton *)sender
{
    NSLog(@"btn_all_dishes_click");
}
-(void)btn_serving_now_click:(UIButton *)sender
{
    NSLog(@"btn_serving_now_click:");
}
-(void)btn_chef_menu_click:(UIButton *)sender
{
    NSLog(@"btn_chef_menu_click:");
}
-(void)individual_items
{
    
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
    [self individual_items];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
