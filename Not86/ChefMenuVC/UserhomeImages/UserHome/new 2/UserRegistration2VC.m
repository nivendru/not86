//
//  UserRegistration2VC.m
//  Not86
//
//  Created by Admin on 29/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "UserRegistration2VC.h"

//#import "HomeVC.h"
#import "Define.h"
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"



@interface UserRegistration2VC ()<UITextFieldDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIImageView *img_topbar;
    UIImageView *img_BackgroundImg;
    UIScrollView *scrollview4;
    
    UITextField *txt_popular;
    
    UITableView *cuisines_table;
    UITableView *diatary_table;
    UITableView *allergies_table;
    
    NSMutableArray *arrCuisines;
    NSMutableArray *arrrestrictions;
    NSMutableArray *array_food_allergies;
    
    UITableView * img_table_for_popular;
    
    CGFloat	animatedDistance;
}

@end

@implementation UserRegistration2VC


static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self IntegrateHeaderDesign];
    [self IntegrateBodyDesign];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}
-(void)IntegrateHeaderDesign{
    
    
    img_topbar=[[UIImageView alloc]init];
    UIButton *btn_back = [[UIButton alloc] init];
    UILabel  * lbl_heading = [[UILabel alloc]init];
    UIImageView *img_logo=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus){
        
        img_topbar.frame=CGRectMake(0, 0,WIDTH,45);
        btn_back.frame = CGRectMake(10, 15, 20,20);
        lbl_heading.frame = CGRectMake(CGRectGetMaxX(btn_back.frame)+20,0, 220,50);
        img_logo.frame = CGRectMake(WIDTH-40, 10, 30, 30);
        scrollview4=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 50, WIDTH, HEIGHT+200)];
    }
    
    
    if (IS_IPHONE_5)
    {
        img_topbar.frame=CGRectMake(0, 0,WIDTH,45);
        btn_back.frame = CGRectMake(10, 13, 20,20);
        lbl_heading.frame =  CGRectMake(CGRectGetMaxX(btn_back.frame)+30,0, 150, 45);
        img_logo.frame = CGRectMake(WIDTH-40, 9, 30, 30);
        scrollview4=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 50, WIDTH, HEIGHT-80)];
    }
    else if (IS_IPHONE_6)
    {
        
        img_topbar.frame=CGRectMake(0, 0,WIDTH,45);
        btn_back.frame = CGRectMake(10, 13, 20,20);
        lbl_heading.frame =  CGRectMake(CGRectGetMaxX(btn_back.frame)+30,0, 150, 45);
        img_logo.frame = CGRectMake(WIDTH-40, 9, 30, 30);
        scrollview4=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 50, WIDTH, HEIGHT-80)];
    }
    else
    {
        img_topbar.frame=CGRectMake(0, 0,WIDTH,45);
        btn_back.frame = CGRectMake(10, 13, 20,20);
        lbl_heading.frame =  CGRectMake(CGRectGetMaxX(btn_back.frame)+30,0, 150, 45);
        img_logo.frame = CGRectMake(WIDTH-40, 9, 30, 30);
        scrollview4=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 50, WIDTH, HEIGHT-80)];
    }
    [img_topbar setUserInteractionEnabled:YES];
    img_topbar.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:img_topbar];
    
    btn_back.backgroundColor = [UIColor clearColor];
    UIImage *image=[UIImage imageNamed:@"arrow@2x.png"];
    [btn_back setBackgroundImage:image forState:UIControlStateNormal];
    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_topbar addSubview:btn_back];
    
    lbl_heading.text = @"User Sign Up";
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:kFont size:20];
    [img_topbar addSubview:lbl_heading];
    
    
    
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img_user_icon@2x.png"];
    [img_topbar addSubview:img_logo];
    
    scrollview4=[[UIScrollView alloc]init];
    [scrollview4 setShowsVerticalScrollIndicator:NO];
    scrollview4.delegate = self;
    scrollview4.scrollEnabled = YES;
    scrollview4.showsVerticalScrollIndicator = NO;
    [scrollview4 setUserInteractionEnabled:YES];
    scrollview4.backgroundColor =  [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [self.view addSubview:scrollview4];
    
    
    
    
}
-(void)Back_btnClick{
    [self.navigationController popViewControllerAnimated:NO];
    
}

-(void)IntegrateBodyDesign
{
    
    
    
    UILabel  *lbl_enjoycooking = [[UILabel alloc]init];
    UIImageView *line_img=[[UIImageView alloc]init];
    UIButton *next = [[UIButton alloc] init];
    txt_popular = [[UITextField alloc] init];
    UILabel  *lbl_cuisines = [[UILabel alloc]init];
    UIImageView *image=[[UIImageView alloc]init];
    cuisines_table= [[UITableView alloc] init ];
    
    UILabel  *lbl_Restrictions = [[UILabel alloc]init];
    diatary_table= [[UITableView alloc] init ];
    UIImageView *lineimage=[[UIImageView alloc]init];
    UILabel  * page4 = [[UILabel alloc]init];
    UIButton *Nextview = [[UIButton alloc] init];
    
    
    UIImageView *img_bg1 = [[UIImageView alloc]init];
    img_bg1.frame =  CGRectMake(5,5, WIDTH-10, 390);
    [img_bg1 setUserInteractionEnabled:YES];
    [img_bg1 setImage:[UIImage imageNamed:@"small-line@2x.png"]];
    [img_bg1 setUserInteractionEnabled:YES];
    [scrollview4 addSubview:img_bg1];
    

    
    
    lbl_enjoycooking.text = @"Favourite Cuisines(choose up to 5)";
    lbl_enjoycooking.backgroundColor=[UIColor clearColor];
    lbl_enjoycooking.textColor=[UIColor blackColor];
    lbl_enjoycooking.numberOfLines = 0;
    lbl_enjoycooking.font = [UIFont fontWithName:kFont size:13];
    [img_bg1 addSubview:lbl_enjoycooking];
    
    txt_popular.borderStyle = UITextBorderStyleNone;
    txt_popular.textColor = [UIColor grayColor];
    txt_popular.font = [UIFont fontWithName:kFont size:15];
    txt_popular.placeholder = @"Popular";
    [txt_popular setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_popular setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    txt_popular.leftViewMode = UITextFieldViewModeAlways;
    txt_popular.userInteractionEnabled=YES;
    txt_popular.textAlignment = NSTextAlignmentLeft;
    txt_popular.backgroundColor = [UIColor clearColor];
    txt_popular.keyboardType = UIKeyboardTypeAlphabet;
    txt_popular.delegate = self;
    [img_bg1 addSubview:txt_popular];
    
    [line_img setUserInteractionEnabled:YES];
    line_img.backgroundColor=[UIColor clearColor];
    line_img.image=[UIImage imageNamed:@"line1.png"];
    [img_bg1 addSubview:line_img];
    
    next.backgroundColor = [UIColor clearColor];
    [next setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [next setUserInteractionEnabled:YES];
    [next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [next addTarget:self action:@selector(drop_btnClick:) forControlEvents:UIControlEventTouchUpInside ];
    [img_bg1 addSubview:next];
    
    UIButton *btn_on_popular = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_popular .frame = CGRectMake(20, CGRectGetMaxY(lbl_enjoycooking.frame)+62, 380, 30);
    btn_on_popular .backgroundColor = [UIColor clearColor];
    [btn_on_popular  addTarget:self action:@selector(btn_action_on_popular:) forControlEvents:UIControlEventTouchUpInside];
   // [btn_on_popular setImage:[UIImage imageNamed:@"search-icon@2x.png"] forState:UIControlStateNormal];
    [img_bg1   addSubview:btn_on_popular ];

    
//#pragma mark Tableview
//    
//    img_table_for_popular = [[UITableView alloc] init ];
//    img_table_for_popular.frame  = CGRectMake(20,CGRectGetMaxY(line_img.frame),380,150);
//    [img_table_for_popular setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//    img_table_for_popular.delegate = self;
//    img_table_for_popular.dataSource = self;
//    img_table_for_popular.showsVerticalScrollIndicator = NO;
//    img_table_for_popular.backgroundColor = [UIColor redColor];
//    [img_bg1  addSubview: img_table_for_popular];
//    
//    img_table_for_popular.hidden =YES;
    

    
    lbl_cuisines.text = @"Cuisines";
    lbl_cuisines.backgroundColor=[UIColor clearColor];
    lbl_cuisines.textColor=[UIColor blackColor];
    lbl_cuisines.numberOfLines = 0;
    lbl_cuisines.font = [UIFont fontWithName:kFont size:16];
    [img_bg1 addSubview:lbl_cuisines];
    
    [image setUserInteractionEnabled:YES];
    image.backgroundColor=[UIColor greenColor];
    image.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    image.layer.borderWidth=1.0f;
    image.clipsToBounds=YES;
    image.image=[UIImage imageNamed:@"line1.png"];
    [img_bg1 addSubview:image];
    
    
    
#pragma mark Tableview
    
    [cuisines_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    cuisines_table.delegate = self;
    cuisines_table.dataSource = self;
    cuisines_table.showsVerticalScrollIndicator = NO;
    cuisines_table.backgroundColor = [UIColor clearColor];
    [image addSubview:cuisines_table];
    arrCuisines=[[NSMutableArray alloc]initWithObjects:@"Biscults and cookies",@"Burgers",@"Bread",@"Cakes & Pies",@"Coffee & Tea",@"Fresh juices",@"Hotdogs",@"Pizzas",@"Sandwiches",@"Other homemade drinks", nil];
    
    
    arrrestrictions=[[NSMutableArray alloc]initWithObjects:@"None",@"Dairy Free",@"Gluten Free",@"Halal",@"Kosher",@"Low Sodium",@"Organic",@"Vegetarian",@"Vegan",@"Others(please specify)", nil];
    
    array_food_allergies = [[NSMutableArray alloc]initWithObjects:@"None",@"peanuts",@"Nuts(All)",@"Milk",@"Mushrooms",@"Eggs",@"Wheat",@"Soy",@"Fish",@"Shellfish",@"Others(please specify)", nil];
    
    
    UIImageView *img_bg2 = [[UIImageView alloc]init];
    img_bg2.frame =  CGRectMake(5,CGRectGetMaxY(img_bg1.frame)+10, WIDTH-10, 390);
    [img_bg2 setUserInteractionEnabled:YES];
    [img_bg2 setImage:[UIImage imageNamed:@"small-line@2x.png"]];
    [img_bg2 setUserInteractionEnabled:YES];
    [scrollview4 addSubview:img_bg2];

    lbl_Restrictions.text = @"Dietary restrictions";
    lbl_Restrictions.backgroundColor=[UIColor clearColor];
    lbl_Restrictions.textColor=[UIColor blackColor];
    //lbl_Restrictions.numberOfLines = 0;
    lbl_Restrictions.font = [UIFont fontWithName:kFontBold size:12];
    [img_bg2 addSubview:lbl_Restrictions];
    
    [diatary_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    diatary_table.delegate = self;
    diatary_table.dataSource = self;
    diatary_table.showsVerticalScrollIndicator = NO;
    diatary_table.backgroundColor = [UIColor whiteColor];
    [img_bg2 addSubview:diatary_table];
    
    UIImageView *line=[[UIImageView alloc]init];
    [line setUserInteractionEnabled:YES];
    line.backgroundColor=[UIColor clearColor];
    line.image=[UIImage imageNamed:@"line1@2x.png"];
    [img_bg2 addSubview:line];
    
    UIImageView *img_bg3 = [[UIImageView alloc]init];
    img_bg3.frame =  CGRectMake(5,CGRectGetMaxY(img_bg1.frame)+10, WIDTH-10, 390);
    [img_bg3 setUserInteractionEnabled:YES];
    [img_bg3 setImage:[UIImage imageNamed:@"small-line@2x.png"]];
    [img_bg3 setUserInteractionEnabled:YES];
    [scrollview4 addSubview:img_bg3];
    

    
    
    UILabel *lbl_food_allergies = [[UILabel alloc]init];
    lbl_food_allergies.text = @"Food Allergies";
    lbl_food_allergies.backgroundColor=[UIColor clearColor];
    lbl_food_allergies.textColor=[UIColor blackColor];
    lbl_food_allergies.numberOfLines = 0;
    lbl_food_allergies.font = [UIFont fontWithName:kFontBold size:12];
    [img_bg3 addSubview: lbl_food_allergies];
    
    allergies_table = [[UITableView alloc]init];
    [allergies_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    allergies_table.delegate = self;
    allergies_table.dataSource = self;
    allergies_table.showsVerticalScrollIndicator = NO;
    allergies_table.backgroundColor = [UIColor clearColor];
    [img_bg3 addSubview:allergies_table];
    
    UIImageView *line2 = [[UIImageView alloc]init];
    [line2 setUserInteractionEnabled:YES];
    line2.backgroundColor=[UIColor clearColor];
    line2.image=[UIImage imageNamed:@"line1@2x.png"];
    [img_bg3 addSubview:line2];
    
    UIImageView *img_bg4 = [[UIImageView alloc]init];
    img_bg4.frame =  CGRectMake(-13, 660, WIDTH+35, 80);
    [img_bg4 setUserInteractionEnabled:YES];
    img_bg4.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [self.view addSubview:img_bg4];

    
    UIImageView *img_terms_and_conditions = [[UIImageView alloc]init];
    [img_terms_and_conditions setUserInteractionEnabled:YES];
    img_terms_and_conditions.backgroundColor=[UIColor clearColor];
    img_terms_and_conditions.image=[UIImage imageNamed:@"text-img@2x.png"];
    [img_bg4 addSubview:img_terms_and_conditions];
    
    UIButton *btn_on_terms_and_conditions =[UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_terms_and_conditions.frame=CGRectMake(5,CGRectGetMaxY(img_terms_and_conditions.frame)+33, 158, 20);
    btn_on_terms_and_conditions.backgroundColor = [UIColor clearColor];
    [btn_on_terms_and_conditions addTarget:self action:@selector(btn_on_terms_and_conditions:) forControlEvents:UIControlEventTouchUpInside];
    btn_on_terms_and_conditions.userInteractionEnabled = YES;
    // [btn_on_terms_and_conditions setImage:[UIImage imageNamed:@"icon-dropdown@2x.png"] forState:UIControlStateNormal];
    [img_terms_and_conditions  addSubview:btn_on_terms_and_conditions];
    
    
    
    
    page4.text = @"(Page2/2)";
    page4.backgroundColor=[UIColor clearColor];
    page4.textColor=[UIColor lightGrayColor];
    page4.numberOfLines = 0;
    page4.font = [UIFont fontWithName:kFont size:13];
    [img_bg4 addSubview:page4];
    
    Nextview.backgroundColor = [UIColor clearColor];
    //    [Nextview setTitle:@"Next" forState:UIControlStateNormal];
    [Nextview setImage:[UIImage imageNamed:@"sign-up@2x.png"] forState:UIControlStateNormal];
    [Nextview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [Nextview addTarget:self action:@selector(sign_up_btnClick:) forControlEvents:UIControlEventTouchUpInside ];
    Nextview.layer.cornerRadius=4.0f;
    [img_bg4 addSubview:Nextview];
    
    if (IS_IPHONE_6Plus){
        scrollview4.frame = CGRectMake(0, 45, WIDTH, HEIGHT-230);
        
        img_bg1.frame =  CGRectMake(5,5, WIDTH-10, 390);
        lbl_enjoycooking.frame = CGRectMake(20,0, 300,50);
        txt_popular.frame=CGRectMake(20,CGRectGetMaxY(lbl_enjoycooking.frame)+1, 150, 38);
        line_img.frame=CGRectMake(20, CGRectGetMaxY(txt_popular.frame)-10, self.view.frame.size.width-40, 0.5);
        next.frame = CGRectMake(self.view.frame.size.width-50, CGRectGetMaxY(lbl_enjoycooking.frame)+15, 20, 10);
        lbl_cuisines.frame = CGRectMake(self.view.frame.size.width/2-50,CGRectGetMaxY(txt_popular.frame),300,30);
        image.frame=CGRectMake(20, CGRectGetMaxY(lbl_cuisines.frame)+3, self.view.frame.size.width-40, 250);
        cuisines_table.frame  = CGRectMake(0,0,400,250);
        
         img_bg2.frame =  CGRectMake(5,CGRectGetMaxY(img_bg1.frame)+10, WIDTH-10, 310);
         lbl_Restrictions.frame = CGRectMake(20,-10, 300,50);
         diatary_table.frame  = CGRectMake(20, CGRectGetMaxY(lbl_Restrictions.frame)-10,300,250);
         lineimage.frame=CGRectMake(50, CGRectGetMaxY(diatary_table.frame)+20, self.view.frame.size.width-70, 0.5);
         line.frame=CGRectMake(50, CGRectGetMaxY(diatary_table .frame)+10, 300, 0.5);

         img_bg3.frame =  CGRectMake(5,CGRectGetMaxY(img_bg2.frame)+10, WIDTH-10, 310);
         lbl_food_allergies.frame = CGRectMake(25,-10, 300,50);
          allergies_table.frame  = CGRectMake(20, CGRectGetMaxY(lbl_food_allergies.frame)-10,300,250);
        line2.frame=CGRectMake(50, CGRectGetMaxY( allergies_table .frame)+10, 300, 0.5);
        
            img_bg4.frame =  CGRectMake(-13, CGRectGetMaxY(scrollview4.frame), WIDTH+35,210);

      img_terms_and_conditions.frame=CGRectMake(90, 5, 260, 70);
       btn_on_terms_and_conditions.frame=CGRectMake(0, 0, 270, 80);
       page4.frame = CGRectMake(self.view.frame.size.width/2-20,CGRectGetMaxY( img_terms_and_conditions.frame)+14, 100,30);
        Nextview.frame = CGRectMake(13, CGRectGetMaxY(page4.frame)+10, WIDTH,45);
        
    }
    else if (IS_IPHONE_6){
        scrollview4.frame = CGRectMake(0, 46, WIDTH, HEIGHT-230);
        
        img_bg1.frame =  CGRectMake(5,5, WIDTH-10, 390);
        img_bg2.frame =  CGRectMake(5,CGRectGetMaxY(img_bg1.frame)+10, WIDTH-10, 390);

        
        lbl_enjoycooking.frame = CGRectMake(20,0, 300,50);
        txt_popular.frame=CGRectMake(20,CGRectGetMaxY(lbl_enjoycooking.frame)+1, 150, 38);
        line_img.frame=CGRectMake(20, CGRectGetMaxY(txt_popular.frame)+1, self.view.frame.size.width-40, 0.5);
        next.frame = CGRectMake(self.view.frame.size.width-50, CGRectGetMaxY(lbl_enjoycooking.frame)+15, 20, 10);
        lbl_cuisines.frame = CGRectMake(self.view.frame.size.width/2-50,CGRectGetMaxY(txt_popular.frame)+10, 300,30);
        image.frame=CGRectMake(20, CGRectGetMaxY(lbl_cuisines.frame)+3, self.view.frame.size.width-40, 250);
        cuisines_table.frame  = CGRectMake(0,0,400,250);
        
         img_bg2.frame =  CGRectMake(5,CGRectGetMaxY(img_bg1.frame)+10, WIDTH-10, 310);
         lbl_Restrictions.frame = CGRectMake(20,-5, 300,50);
         diatary_table.frame  = CGRectMake(20, CGRectGetMaxY(lbl_Restrictions.frame)-10,300,250);
         lineimage.frame=CGRectMake(50, CGRectGetMaxY(diatary_table.frame)+20, self.view.frame.size.width-70, 0.5);
         line.frame=CGRectMake(50, CGRectGetMaxY(diatary_table .frame)+10, 290, 0.5);
      
         img_bg3.frame =  CGRectMake(5,CGRectGetMaxY(img_bg2.frame)+10, WIDTH-10, 310);
          lbl_food_allergies.frame = CGRectMake(25,-5, 300,50);
          allergies_table.frame  = CGRectMake(20, CGRectGetMaxY(lbl_food_allergies.frame)-10,300,250);
          line2.frame=CGRectMake(50, CGRectGetMaxY( allergies_table .frame)+10, 290, 0.5);
        
        img_bg4.frame =  CGRectMake(-13, CGRectGetMaxY(scrollview4.frame), WIDTH+35,210);
        
        img_terms_and_conditions.frame=CGRectMake(70, 05, 250, 70);
        btn_on_terms_and_conditions.frame=CGRectMake(-10,0, 280, 75);
        
        page4.frame = CGRectMake(self.view.frame.size.width/2-30,CGRectGetMaxY( img_terms_and_conditions.frame)+15, 100,30);
        Nextview.frame = CGRectMake(30, CGRectGetMaxY(page4.frame)+10, WIDTH-40,45);
        
    }
    else
    {
        scrollview4.frame = CGRectMake(0, 45, WIDTH, HEIGHT-230);
        
        img_bg1.frame =  CGRectMake(5,5, WIDTH-10, 360);
        
        lbl_enjoycooking.frame = CGRectMake(20,-10, 300,50);
        txt_popular.frame=CGRectMake(20,CGRectGetMaxY(lbl_enjoycooking.frame)-10, 150, 38);
        line_img.frame=CGRectMake(20, CGRectGetMaxY(txt_popular.frame)-10, self.view.frame.size.width-45, 0.5);
        next.frame = CGRectMake(self.view.frame.size.width-50, CGRectGetMaxY(lbl_enjoycooking.frame)+6, 20, 10);
        lbl_cuisines.frame = CGRectMake(self.view.frame.size.width/2-50,CGRectGetMaxY(txt_popular.frame),300,30);
        image.frame=CGRectMake(20, CGRectGetMaxY(lbl_cuisines.frame)+3, self.view.frame.size.width-40, 250);
        cuisines_table.frame  = CGRectMake(0,0,400,250);
        
         img_bg2.frame =  CGRectMake(5,CGRectGetMaxY(img_bg1.frame)+10, WIDTH-10, 320);
         lbl_Restrictions.frame = CGRectMake(20,0, 300,50);
         diatary_table.frame  = CGRectMake(20, CGRectGetMaxY(lbl_Restrictions.frame)-5,290,250);
         lineimage.frame=CGRectMake(50, CGRectGetMaxY(diatary_table.frame)+20, self.view.frame.size.width-70, 0.5);
        line.frame=CGRectMake(50, CGRectGetMaxY(diatary_table .frame)+10, 240, 0.5);
       
          img_bg3.frame =  CGRectMake(5,CGRectGetMaxY(img_bg2.frame)+10, WIDTH-10, 315);
          lbl_food_allergies.frame = CGRectMake(25,0, 300,50);
          allergies_table.frame  = CGRectMake(20, CGRectGetMaxY(lbl_food_allergies.frame)-5,290,250);
          line2.frame=CGRectMake(50, CGRectGetMaxY( allergies_table .frame)+10, 240, 0.5);
        
          img_bg4.frame =  CGRectMake(-13, CGRectGetMaxY(scrollview4.frame), WIDTH+35,210);
         img_terms_and_conditions.frame=CGRectMake(50,10, 240, 70);
         btn_on_terms_and_conditions.frame=CGRectMake(-10,0, 280, 80);
       
         page4.frame = CGRectMake(self.view.frame.size.width/2-25,CGRectGetMaxY( img_terms_and_conditions.frame)+15, 100,30);
         Nextview.frame = CGRectMake(33, CGRectGetMaxY(page4.frame)+10, WIDTH-40,45);
        
    }
    [scrollview4 setContentSize:CGSizeMake(0,1100)];
    
}

//-(void)Next_btnClick
//{
//    ChefSignup5 *chefsignup5=[[ChefSignup5 alloc]init];
//    [self.navigationController pushViewController:chefsignup5 animated:YES];
//}
-(void)drop_btnClick:(id)sender
{
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView== cuisines_table)
    {
        return [arrCuisines count];
        
    }
    else if (tableView == diatary_table )
    {
        return [arrrestrictions count];
        
    }
    else if(tableView ==  allergies_table)
    {
        return [ array_food_allergies count];
    }
    else
    {
        return 25;
    }
    return 0;
    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    UIButton *next = [[UIButton alloc] init];
    next.frame = CGRectMake(5, 7, 12, 12);
    next.backgroundColor = [UIColor clearColor];
    [next setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
    [next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [next addTarget:self action:@selector(drop_btnClick:) forControlEvents:UIControlEventTouchUpInside ];
    [cell.contentView addSubview:next];
    
    UILabel  *lbl_Restrictions = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(next.frame)+15,0,150,25)];
    if (tableView== cuisines_table)
    {
        lbl_Restrictions.text = [arrCuisines objectAtIndex:indexPath.row];
        
    }
    else if (tableView == diatary_table)
    {
        lbl_Restrictions.text = [arrrestrictions objectAtIndex:indexPath.row];
        
    }
    else if(tableView == allergies_table)
    {
        lbl_Restrictions.text = [array_food_allergies objectAtIndex:indexPath.row];
    }
    else
    {
        lbl_Restrictions.text = @"popular";
    }
    lbl_Restrictions.backgroundColor=[UIColor clearColor];
    lbl_Restrictions.textColor=[UIColor lightGrayColor];
    lbl_Restrictions.numberOfLines = 5;
    lbl_Restrictions.font = [UIFont fontWithName:kFont size:12];
    [cell.contentView addSubview:lbl_Restrictions];
    
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView== cuisines_table)
    {
        return 25;
        
    }
    else if (tableView == diatary_table)
    {
        return 25;
        
    }
    else if(tableView == allergies_table)
    {
        return 25;
    }
    else
    {
        return  25;
    }
    return 0;
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}
-(void)btn_action_on_popular:(UIButton *)sender
{
    NSLog(@"btn_action_on_popular");
    img_table_for_popular.hidden =NO;

}
-(void)btn_on_terms_and_conditions:(UIButton *)sender
{
    NSLog(@"btn_on_terms_and_conditions:");
    
}




-(void)sign_up_btnClick:(UIButton *)sender
{
    NSLog(@"btn_back_click:");
//    HomeVC*vc = [[HomeVC alloc]init];
//    [self.navigationController pushViewController:vc animated:NO];
    
    
    
}


#pragma rerutrn event code

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

//-(BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    [self.view endEditing:YES];
//    return [textField resignFirstResponder];
//
//}
//-(void)drop_btnClick1:(UIButton *)sender
//{
//    NSLog(@"drop_btnClick1:");
//
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
