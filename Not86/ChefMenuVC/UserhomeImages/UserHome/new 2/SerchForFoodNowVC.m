//
//  SerchForFoodNowVC.m
//  Not86
//
//  Created by Admin on 30/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "SerchForFoodNowVC.h"
#import "Define.h"

//#import "LocationVC.h"
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"

@interface SerchForFoodNowVC ()<UITextFieldDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
{
    UIImageView *img_header;
    CGFloat	animatedDistance;
    UILabel *lbl_food_now;
    

    UIScrollView *scroll;
    UIView *view_serch_for_food_now;
    UIView *  view_serch_for_food_later;

    
    UITableView *  table_on_drop_down ;
    UIImageView *img_bg_for_first_tbl;
    UILabel * lbl_food_now_r_later ;
    
    
    UIButton * btn_on_single_dish;
    UIImageView *img_red_tik1;
    UIButton * btn_on_meal;
    UIImageView *img_red_tik2;
    int ABC;
    NSMutableArray * array_temp;
    

    
    UIButton * btn_on_dine_in;
    UIImageView *  img_red_tik_on_dine_in;
    UIButton *  btn_on_take_out;
    UIImageView * img_red_tik_on_take_out;
    UIButton *  btn_on_delever;
    UIImageView * img_red_tik_on_deliver;
    int DEF;
    NSMutableArray * array_temp2;
    
    
    
    UIView * view_for_more_filters;
    UITableView * table_for_items;
    NSMutableArray *array_displaynames;
    NSMutableArray*ary_displaynames;
    NSMutableArray * array_img;
    NSMutableArray * array_head_names;
    
    UIView * view_for_cuisines;
    
    
    UIImageView *img_strip1;
    UIImageView *img_strip2;
    UIView * view_for_all;
    UIView * view_for_my_favorites;
    
    //for Favorites
    UICollectionViewFlowLayout *layout;
    UICollectionView * collView_dish_types;
    UICollectionViewFlowLayout * layout2;
    UICollectionView * collView_for_course;
    UICollectionViewFlowLayout * layout3;
    UICollectionView * collView_for_dietary;
    
    NSMutableArray *array_imges;
    NSMutableArray *array_dish_names;
    NSMutableArray * array_course_images;
    NSMutableArray * array_course_names;
    NSMutableArray * array_dietary_images;
    NSMutableArray * array_names_in_dietary_table;
    
    UITableView * table_for_dietary;
    
    //for All
    UICollectionViewFlowLayout *layout4;
    UICollectionView * collView_dish_types2;
    UICollectionViewFlowLayout * layout5;
    UICollectionView * collView_for_course2;
    UICollectionViewFlowLayout * layout6;
    UICollectionView * collView_for_dietary2;
    
    NSMutableArray *array_imges_in_all;
    NSMutableArray *array_dish_names_in_all;
    NSMutableArray * array_course_images_in_all;
    NSMutableArray * array_dietary_images_in_all;
    NSMutableArray * array_names_in_dietary_table_in_all;
    
    UITableView *  table_for_dietary_in_all;
    
    //food later
    UIImageView  *img_red_tik3;
    UIButton * btn_on_meal2;
    UIImageView *  img_red_tik4;
    
    UIButton * btn_on_dine_in_later;
    UIImageView *  img_red_tik_on_dine_in2;
    UIButton * btn_on_take_out2;
    
    UIView   * view_for_more_filters_in_later;
    UITableView * table_for_items_in_later;
    UIView *view_for_cuisines_in_later;
    
    
}
@end

@implementation SerchForFoodNowVC

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    array_temp = [[NSMutableArray alloc]init];
    array_temp2 = [[NSMutableArray alloc]init];

    // Do any additional setup after loading the view.
    array_head_names = [[NSMutableArray alloc]initWithObjects:@"Food Now",@"Food Later" ,nil];
    ary_displaynames =[[NSMutableArray alloc]initWithObjects:@"Steamed Thai oath",@"Rasberry custored", nil];
    array_img = [[NSMutableArray alloc]initWithObjects:@"img-dish1@2x.png",@"img-dish2@2x.png", nil];
    [self integrateBodyDesign];
    [self integrateHeader];
    
    array_imges = [[NSMutableArray alloc]initWithObjects:@"b-chines@2x.png",@"b-french@2x.png",@"b-japanes@2x.png",@"b-italian@2x.png", nil];
    array_dish_names = [[NSMutableArray alloc]initWithObjects:@"Chinese",@"French",@"Japanese",@"Italian", nil];
    
    array_course_images = [[NSMutableArray alloc]initWithObjects:@"b-breakfast@2x.png",@"main-couse-img@2x.png",@"b-dessert@2x.png",nil];
    
    array_course_names = [[NSMutableArray alloc]initWithObjects:@"Brackfast",@"Main Course",@"Dessert",nil];
    
    array_dietary_images = [[NSMutableArray alloc]initWithObjects:@"img-halal@2x.png",@"img-koser@2x.png",@"img-non-veg@2x.png",@"img-veg@2x.png", nil];
    //names in table view in favorites
    array_names_in_dietary_table = [[NSMutableArray alloc]initWithObjects:@"Rating (High-Low)",@"Price (Low-High)",@"Price (High-Low)", nil];
    
    
    //array declaration in all
    
    array_imges_in_all = [[NSMutableArray alloc]initWithObjects:@"b-chines@2x.png",@"b-french@2x.png",@"b-japanes@2x.png",@"b-italian@2x.png", nil];
    array_dish_names_in_all = [[NSMutableArray alloc]initWithObjects:@"Chinese",@"French",@"Japanese",@"Italian", nil];
    
    array_course_images_in_all = [[NSMutableArray alloc]initWithObjects:@"b-breakfast@2x.png",@"main-couse-img@2x.png",@"dessert-b@2x.png",nil];
    
    array_dietary_images_in_all = [[NSMutableArray alloc]initWithObjects:@"halal-img@2x.png",@"koser-img@2x.png",@"non-veg-img@2x.png",@"veg-img@2x.png", nil];
    
    //names in table view in all
    array_names_in_dietary_table_in_all = [[NSMutableArray alloc]initWithObjects:@"Rating (High-Low)",@"Price (Low-High)",@"Price (High-Low)", nil];
      
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}
-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH,45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu.frame = CGRectMake(10,13,20,20);
    icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
    [icon_menu addTarget:self action:@selector(btn_menu_click:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:icon_menu];
    
    
    lbl_food_now = [[UILabel alloc]init];
    lbl_food_now.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 150, 45);
    lbl_food_now.text = @"Food Now";
    lbl_food_now.font = [UIFont fontWithName:kFont size:20];
    lbl_food_now.textColor = [UIColor whiteColor];
    lbl_food_now.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_food_now];
    
    UIButton *icon_drop_down = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_drop_down.frame = CGRectMake(CGRectGetMaxX(lbl_food_now.frame)-45,15,20,20);
    icon_drop_down .backgroundColor = [UIColor clearColor];
    [icon_drop_down addTarget:self action:@selector(btn_drop_down:) forControlEvents:UIControlEventTouchUpInside];
    [icon_drop_down setImage:[UIImage imageNamed:@"white-dropd-@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_drop_down];
    
    UIButton *btn_on_food_later_in_head = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_food_later_in_head.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0,150,45);
    btn_on_food_later_in_head .backgroundColor = [UIColor clearColor];
    [btn_on_food_later_in_head addTarget:self action:@selector(btn_on_food_later_label_click:) forControlEvents:UIControlEventTouchUpInside];
    //[icon_drop_down setImage:[UIImage imageNamed:@"drop-down@2x.png"] forState:UIControlStateNormal];
    [self.view   addSubview:btn_on_food_later_in_head];
    
#pragma mark Tableview
    
    table_on_drop_down = [[UITableView alloc] init ];
    table_on_drop_down .frame  = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,45,150,100);
    [ table_on_drop_down  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
     table_on_drop_down .delegate = self;
     table_on_drop_down .dataSource = self;
     table_on_drop_down .showsVerticalScrollIndicator = NO;
     table_on_drop_down .backgroundColor = [UIColor purpleColor];
    table_on_drop_down.hidden = YES;
    [self.view addSubview: table_on_drop_down ];
    

    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 11, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}
-(void)integrateBodyDesign
{
    
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor clearColor];
    scroll.frame = CGRectMake(0, 46, WIDTH, HEIGHT+150);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    view_serch_for_food_now = [[UIView alloc]init];
    view_serch_for_food_now.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT);
    [view_serch_for_food_now setUserInteractionEnabled:YES];
    view_serch_for_food_now.backgroundColor=[UIColor redColor];
    [scroll  addSubview:  view_serch_for_food_now];
    
    
    
    UILabel *lbl_location = [[UILabel alloc]init];
    lbl_location.frame = CGRectMake(105,5, 150, 45);
    lbl_location.text = @"Location";
    lbl_location.font = [UIFont fontWithName:kFont size:18];
    lbl_location.textColor = [UIColor blackColor];
    lbl_location.backgroundColor = [UIColor clearColor];
    [ view_serch_for_food_now addSubview:lbl_location];

   
    
    UIImageView *bg_for_location = [[UIImageView alloc]init];
    bg_for_location.frame = CGRectMake(-5, CGRectGetMaxY(lbl_location.frame)-5, WIDTH+20, 50);
    [bg_for_location setUserInteractionEnabled:YES];
    bg_for_location.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [ view_serch_for_food_now addSubview:bg_for_location];

    UIImageView *img_pointer_location = [[UIImageView alloc]init];
    img_pointer_location.frame = CGRectMake(25,0, 30, 30);
    [img_pointer_location setImage:[UIImage imageNamed:@"pointer-location@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_pointer_location setUserInteractionEnabled:YES];
    [bg_for_location addSubview:img_pointer_location];
    
    UILabel *lbl_set_location = [[UILabel alloc]init];
    lbl_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+5,CGRectGetMaxY(lbl_location.frame), 250, 30);
    lbl_set_location.text = @"Smith Street, 77 Block (5km)";
    lbl_set_location.font = [UIFont fontWithName:kFont size:18];
    lbl_set_location.textColor = [UIColor blackColor];
    lbl_set_location.backgroundColor = [UIColor clearColor];
    [bg_for_location addSubview:lbl_set_location];
    
    
    UIButton *btn_img_right = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_right.frame = CGRectMake(CGRectGetMidX(lbl_set_location.frame)+80,CGRectGetMaxY(lbl_location.frame),30,30);
    btn_img_right .backgroundColor = [UIColor clearColor];
    [btn_img_right addTarget:self action:@selector(btn_right_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_right setImage:[UIImage imageNamed:@"right-arrow@2x.png"] forState:UIControlStateNormal];
    [bg_for_location   addSubview:btn_img_right];
    
    
    UIButton *btn_on_set_location = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_set_location.frame = CGRectMake(25,CGRectGetMaxY(lbl_location.frame),270,35);
    btn_on_set_location .backgroundColor = [UIColor purpleColor];
    [btn_on_set_location addTarget:self action:@selector(btn_on_set_location_click:) forControlEvents:UIControlEventTouchUpInside];
    // [btn_set_date setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    [bg_for_location   addSubview:btn_on_set_location];
    
    UILabel *lbl_dish_r_meal = [[UILabel alloc]init];
    // lbl_dish_r_meal.frame = CGRectMake(105,CGRectGetMaxY(txt_set_location.frame)+10, 150, 45);
    lbl_dish_r_meal.text = @"Dish/Meal";
    lbl_dish_r_meal.font = [UIFont fontWithName:kFont size:18];
    lbl_dish_r_meal.textColor = [UIColor blackColor];
    lbl_dish_r_meal.backgroundColor = [UIColor clearColor];
    [ view_serch_for_food_now addSubview:lbl_dish_r_meal];
    
    UIImageView *bg_for_meal = [[UIImageView alloc]init];
    bg_for_meal.frame = CGRectMake(-5,CGRectGetMaxY(lbl_dish_r_meal.frame)-5, WIDTH+20, 50);
    [bg_for_meal setUserInteractionEnabled:YES];
    bg_for_meal.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [ view_serch_for_food_now addSubview:bg_for_meal];
    
    
    btn_on_single_dish  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_single_dish.frame = CGRectMake(90,CGRectGetMaxY(lbl_dish_r_meal.frame),60,50);
    [btn_on_single_dish setImage:[UIImage imageNamed:@"img-single-bl@2x.png"] forState:UIControlStateSelected];
    [btn_on_single_dish setImage:[UIImage imageNamed:@"single-dish-icon@2x.png"] forState:UIControlStateNormal];
    btn_on_single_dish.tag = 54;
    [btn_on_single_dish addTarget:self action:@selector(btn_on_dish_click:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_meal addSubview:  btn_on_single_dish];

    
    btn_on_meal  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_meal.frame = CGRectMake(CGRectGetMaxX( btn_on_single_dish.frame)+20,5,60,50);
    [btn_on_meal setImage:[UIImage imageNamed:@"iocn-meal-bl@2x.png"] forState:UIControlStateSelected];
    [btn_on_meal setImage:[UIImage imageNamed:@"iocn-meal@2x.png"] forState:UIControlStateNormal];
     btn_on_meal.tag = 55;
    [btn_on_meal addTarget:self action:@selector(btn_on_dish_click:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_meal addSubview:  btn_on_meal];
    
    
    img_red_tik1 = [[UIImageView alloc]init];
    //img_red_tik1.frame =  CGRectMake(CGRectGetMidX(btn_img_single_dish.frame)+135,CGRectGetMaxY(lbl_dish_r_meal.frame)+15,25,25);
    [img_red_tik1 setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik1 setUserInteractionEnabled:YES];
    [bg_for_meal addSubview:img_red_tik1];
    img_red_tik1.hidden = YES;
    
    img_red_tik2 = [[UIImageView alloc]init];
    //img_red_tik2.frame =  CGRectMake(CGRectGetMidX(btn_img_single_dish.frame)+135,CGRectGetMaxY(lbl_dish_r_meal.frame)+15,25,25);
    [img_red_tik2 setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik2 setUserInteractionEnabled:YES];
    [bg_for_meal addSubview:img_red_tik2];
    img_red_tik2.hidden = YES;

    
    
    UILabel *lbl_seving_type = [[UILabel alloc]init];
    lbl_seving_type.frame = CGRectMake(105,CGRectGetMaxY(bg_for_meal.frame)+150, 150, 45);
    lbl_seving_type.text = @"Serving Type";
    lbl_seving_type.font = [UIFont fontWithName:kFont size:18];
    lbl_seving_type.textColor = [UIColor blackColor];
    lbl_seving_type.backgroundColor = [UIColor clearColor];
    [ view_serch_for_food_now addSubview:lbl_seving_type];
    
    UIImageView *bg_for_serving_type = [[UIImageView alloc]init];
    bg_for_serving_type.frame = CGRectMake(-5,CGRectGetMaxY(lbl_seving_type.frame)-5, WIDTH+20, 50);
    [bg_for_serving_type setUserInteractionEnabled:YES];
    bg_for_serving_type.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [ view_serch_for_food_now addSubview:bg_for_serving_type];

     btn_on_dine_in  = [UIButton buttonWithType:UIButtonTypeCustom];
     btn_on_dine_in.frame = CGRectMake(45,CGRectGetMaxY(lbl_seving_type.frame),50,50);
    [ btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in-bl@2x.png"] forState:UIControlStateSelected];
    [ btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in@2x.png"] forState:UIControlStateNormal];
     btn_on_dine_in.tag = 0;
    [ btn_on_dine_in addTarget:self action:@selector(click_on_serving_type:)forControlEvents:UIControlEventTouchUpInside];
    [bg_for_serving_type addSubview:   btn_on_dine_in];
    
    img_red_tik_on_dine_in = [[UIImageView alloc]init];
    img_red_tik_on_dine_in.frame =  CGRectMake(CGRectGetMidX(btn_on_dine_in.frame)+135,CGRectGetMaxY(lbl_dish_r_meal.frame)+15,25,25);
    [img_red_tik_on_dine_in setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik_on_dine_in setUserInteractionEnabled:YES];
    [bg_for_serving_type addSubview:img_red_tik_on_dine_in];
    img_red_tik_on_dine_in.hidden = YES;

    
    btn_on_take_out  = [UIButton buttonWithType:UIButtonTypeCustom];
     btn_on_take_out.frame = CGRectMake(45,CGRectGetMaxY(lbl_seving_type.frame),50,50);
    [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out-bl@2x.png"]forState:UIControlStateSelected];
    [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out@2x.png"]forState:UIControlStateNormal];
     btn_on_take_out.tag = 1;
    [btn_on_take_out addTarget:self action:@selector(click_on_serving_type:)forControlEvents:UIControlEventTouchUpInside];
    [bg_for_serving_type addSubview:btn_on_take_out];
    
    img_red_tik_on_take_out = [[UIImageView alloc]init];
    img_red_tik_on_take_out.frame =  CGRectMake(CGRectGetMidX(btn_on_take_out .frame)+135,CGRectGetMaxY(lbl_dish_r_meal.frame)+15,25,25);
    [img_red_tik_on_take_out setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik_on_take_out setUserInteractionEnabled:YES];
    [bg_for_serving_type addSubview:img_red_tik_on_take_out];
    img_red_tik_on_take_out.hidden = YES;

    
    btn_on_delever  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_delever.frame = CGRectMake(45,CGRectGetMaxY(lbl_seving_type.frame),50,50);
    [btn_on_delever setImage:[UIImage imageNamed:@"img-delever-bl@2x.png"] forState:UIControlStateSelected];
    [btn_on_delever setImage:[UIImage imageNamed:@"img-delever@2x.png"] forState:UIControlStateNormal];
    btn_on_delever.tag = 2;
    [btn_on_delever addTarget:self action:@selector(click_on_serving_type:)forControlEvents:UIControlEventTouchUpInside];
    [bg_for_serving_type addSubview:btn_on_delever];
    
    img_red_tik_on_deliver = [[UIImageView alloc]init];
    img_red_tik_on_deliver.frame =  CGRectMake(CGRectGetMidX(btn_on_delever.frame)+135,CGRectGetMaxY(lbl_dish_r_meal.frame)+15,25,25);
    [img_red_tik_on_deliver setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik_on_deliver setUserInteractionEnabled:YES];
    [bg_for_serving_type addSubview:img_red_tik_on_deliver];
    img_red_tik_on_deliver.hidden = YES;

    UILabel *lbl_keywords = [[UILabel alloc]init];
    lbl_keywords.frame = CGRectMake(130,CGRectGetMaxY(bg_for_serving_type.frame)+5, 150, 45);
    lbl_keywords.text = @"Keywords";
    lbl_keywords.font = [UIFont fontWithName:kFont size:18];
    lbl_keywords.textColor = [UIColor blackColor];
    lbl_keywords.backgroundColor = [UIColor clearColor];
    [ view_serch_for_food_now addSubview:lbl_keywords];
    
    UIImageView *bg_for_Keyword = [[UIImageView alloc]init];
    bg_for_Keyword.frame = CGRectMake(-5,CGRectGetMaxY(lbl_keywords.frame)-5, WIDTH+20, 50);
    [bg_for_Keyword setUserInteractionEnabled:YES];
    bg_for_Keyword.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [ view_serch_for_food_now addSubview:bg_for_Keyword];

    
    UIImageView *img_serch_bar = [[UIImageView alloc]init];
    img_serch_bar.frame = CGRectMake(25,5, 290, 30);
    [img_serch_bar setImage:[UIImage imageNamed:@"search-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_serch_bar setUserInteractionEnabled:YES];
    [bg_for_Keyword addSubview:img_serch_bar];
    
    
    UITextField *txt_search = [[UITextField alloc] init];
    //  txt_search.frame = CGRectMake(10,-4, 200, 45);
    txt_search .borderStyle = UITextBorderStyleNone;
    txt_search .textColor = [UIColor grayColor];
    txt_search .font = [UIFont fontWithName:kFont size:15];
    txt_search .placeholder = @"Search...";
    [txt_search  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_search  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_search .leftView = padding3;
    txt_search .leftViewMode = UITextFieldViewModeAlways;
    txt_search .userInteractionEnabled=YES;
    txt_search .textAlignment = NSTextAlignmentLeft;
    txt_search .backgroundColor = [UIColor clearColor];
    txt_search .keyboardType = UIKeyboardTypeAlphabet;
    txt_search .delegate = self;
    [img_serch_bar addSubview:txt_search ];
    
    
    UIImageView *icon_search = [[UIImageView alloc]init];
    //   icon_search.frame = CGRectMake(255,8, 15, 15);
    [icon_search setImage:[UIImage imageNamed:@"icon-search@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_search setUserInteractionEnabled:YES];
    [img_serch_bar addSubview:icon_search];
    
    UIButton *btn_on_search_bar = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_search_bar.frame = CGRectMake(240,0, 30, 30);
    btn_on_search_bar .userInteractionEnabled=YES;
    btn_on_search_bar .backgroundColor = [UIColor purpleColor];
    [btn_on_search_bar addTarget:self action:@selector(btn_on_search_bar_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [img_serch_bar   addSubview:btn_on_search_bar];
    
 //view for btn more filters ans  dish table view
    
    view_for_more_filters = [[UIView alloc]init];
    view_for_more_filters.frame=CGRectMake(5,CGRectGetMaxY(bg_for_Keyword.frame),WIDTH-10,300);
    [view_for_more_filters setUserInteractionEnabled:YES];
    view_for_more_filters.backgroundColor=[UIColor purpleColor];
    view_for_more_filters.hidden = NO;
    [ view_serch_for_food_now  addSubview:  view_for_more_filters];
    
    
    UIButton *btn_img_more_filters = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_img_more_filters.frame = CGRectMake(30,CGRectGetMaxY(img_serch_bar.frame)+20, 260, 40);
    btn_img_more_filters .backgroundColor = [UIColor clearColor];
    [btn_img_more_filters addTarget:self action:@selector(btn_img_on_more_filters_click:) forControlEvents:UIControlEventTouchUpInside];
    [ btn_img_more_filters setImage:[UIImage imageNamed:@"img-more-filters@2x.png"] forState:UIControlStateNormal];
    [view_for_more_filters   addSubview:btn_img_more_filters];
    
    
#pragma mark Tableview
    
    table_for_items = [[UITableView alloc] init ];
    //   table_for_items.frame  = CGRectMake(15,CGRectGetMaxY(btn_img_more_filters.frame)+5,WIDTH-30,370);
    [table_for_items setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_items.delegate = self;
    table_for_items.dataSource = self;
    table_for_items.showsVerticalScrollIndicator = NO;
    table_for_items.backgroundColor = [UIColor redColor];
    [view_for_more_filters addSubview:table_for_items];
    
    view_for_cuisines = [[UIView alloc]init];
    view_for_cuisines.frame=CGRectMake(5,CGRectGetMaxY(bg_for_Keyword.frame),WIDTH-10,300);
    [view_for_cuisines setUserInteractionEnabled:YES];
    view_for_cuisines.backgroundColor=[UIColor greenColor];
    view_for_cuisines.hidden = YES;
    [ view_serch_for_food_now  addSubview:  view_for_cuisines];



    UILabel *lbl_cuisines = [[UILabel alloc]init];
    lbl_cuisines.frame = CGRectMake(130,5, 150, 45);
    lbl_cuisines.text = @"Cuisines";
    lbl_cuisines.font = [UIFont fontWithName:kFontBold size:18];
    lbl_cuisines.textColor = [UIColor blackColor];
    lbl_cuisines.backgroundColor = [UIColor clearColor];
    [view_for_cuisines addSubview:lbl_cuisines];
    
    UIImageView *bg_for_favorits_and_all = [[UIImageView alloc]init];
    bg_for_favorits_and_all.frame = CGRectMake(0,CGRectGetMaxY(lbl_keywords.frame)-5, WIDTH, 50);
    [bg_for_favorits_and_all setUserInteractionEnabled:YES];
    bg_for_favorits_and_all.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_cuisines addSubview:bg_for_favorits_and_all];

    
    UILabel *lbl_my_favorites = [[UILabel alloc]init];
    lbl_my_favorites.frame = CGRectMake(130,CGRectGetMaxY(lbl_cuisines.frame)+5, 150, 45);
    lbl_my_favorites.text = @"My Favorites";
    [lbl_my_favorites setUserInteractionEnabled:YES];
    lbl_my_favorites.font = [UIFont fontWithName:kFontBold size:18];
    lbl_my_favorites.textColor = [UIColor blackColor];
    lbl_my_favorites.backgroundColor = [UIColor clearColor];
    [ bg_for_favorits_and_all addSubview:lbl_my_favorites];

    img_strip1 = [[UIImageView alloc]init];
    img_strip1.frame = CGRectMake(200, CGRectGetMaxY(lbl_my_favorites.frame), 57, 2);
    [img_strip1 setImage:[UIImage imageNamed:@"black strip@2x.png"]];
    [img_strip1 setUserInteractionEnabled:YES];
    [bg_for_favorits_and_all addSubview:img_strip1];

    UIButton *btn_my_favorites = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_my_favorites.frame = CGRectMake(130,CGRectGetMaxY(img_serch_bar.frame)+5, 150, 45);
    btn_my_favorites .backgroundColor = [UIColor clearColor];
    [btn_my_favorites setUserInteractionEnabled:YES];
    [btn_my_favorites addTarget:self action:@selector(btn_on_my_favorites_click:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_favorits_and_all addSubview:btn_my_favorites];

    UILabel *lbl_all = [[UILabel alloc]init];
    lbl_all.frame = CGRectMake(130,CGRectGetMaxY(lbl_cuisines.frame)+5, 150, 45);
    lbl_all.text = @"All";
    [lbl_all setUserInteractionEnabled:YES];
    lbl_all.font = [UIFont fontWithName:kFontBold size:18];
    lbl_all.textColor = [UIColor blackColor];
    lbl_all.backgroundColor = [UIColor clearColor];
    [bg_for_favorits_and_all addSubview:lbl_all];
    
    img_strip2 = [[UIImageView alloc]init];
    img_strip2.frame = CGRectMake(200, CGRectGetMaxY(lbl_my_favorites.frame), 57, 2);
    [img_strip2 setImage:[UIImage imageNamed:@"black strip@2x.png"]];
    [img_strip2 setUserInteractionEnabled:YES];
    [bg_for_favorits_and_all addSubview:img_strip2];
    
    img_strip2.hidden = YES;
    
    UIButton *btn_on_all = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_all.frame = CGRectMake(130,CGRectGetMaxY(img_serch_bar.frame)+5, 150, 45);
    [btn_on_all setUserInteractionEnabled:YES];
    btn_on_all .backgroundColor = [UIColor clearColor];
    [btn_on_all addTarget:self action:@selector(btn_all_click:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_favorits_and_all  addSubview:btn_on_all];
    
   // view for MY FAVORITES
    
    view_for_my_favorites = [[UIView alloc]init];
    view_for_my_favorites.frame=CGRectMake(0,CGRectGetMaxY(lbl_my_favorites.frame)+92,WIDTH,HEIGHT+90);
    view_for_my_favorites.backgroundColor=[UIColor orangeColor];
    [view_for_my_favorites setUserInteractionEnabled:YES];
    [view_for_cuisines addSubview:  view_for_my_favorites];

    UIImageView *bg_for_cuisines = [[UIImageView alloc]init];
    bg_for_cuisines.frame = CGRectMake(0,CGRectGetMaxY(bg_for_favorits_and_all.frame)-5, WIDTH, 50);
    [bg_for_cuisines setUserInteractionEnabled:YES];
    bg_for_cuisines.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_my_favorites addSubview:bg_for_cuisines];
    
    // first collection view for dish
    
    layout=[[UICollectionViewFlowLayout alloc] init];
    collView_dish_types = [[UICollectionView alloc] initWithFrame:CGRectMake(15,10,WIDTH-30,90)
                                             collectionViewLayout:layout];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_dish_types setDataSource:self];
    [collView_dish_types setDelegate:self];
    collView_dish_types.scrollEnabled = YES;
    collView_dish_types.showsVerticalScrollIndicator = NO;
    collView_dish_types.showsHorizontalScrollIndicator = NO;
    collView_dish_types.pagingEnabled = NO;
    [collView_dish_types registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_dish_types setBackgroundColor:[UIColor redColor]];
    layout.minimumInteritemSpacing = 2;
    layout.minimumLineSpacing = 0;
    collView_dish_types.userInteractionEnabled = YES;
    [bg_for_cuisines addSubview: collView_dish_types];
    
    //search for food later
    
#pragma view for food later
    
    view_serch_for_food_later = [[UIView alloc]init];
    view_serch_for_food_later.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT);
    [view_serch_for_food_later setUserInteractionEnabled:YES];
    view_serch_for_food_later.backgroundColor=[UIColor yellowColor];
    [scroll  addSubview:  view_serch_for_food_later];
    
    view_serch_for_food_later.hidden = YES;
    
    
    UILabel *lbl_date = [[UILabel alloc]init];
    lbl_date.frame = CGRectMake(130,0, 150, 45);
    lbl_date.text = @"Date";
    lbl_date.font = [UIFont fontWithName:kFont size:18];
    lbl_date.textColor = [UIColor blackColor];
    lbl_date.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_date];
    
    UIImageView *bg_for_date = [[UIImageView alloc]init];
    bg_for_date.frame = CGRectMake(-5,CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
    [bg_for_date setUserInteractionEnabled:YES];
    bg_for_date.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_serch_for_food_later addSubview:bg_for_date];

    
    UIImageView *img_calender = [[UIImageView alloc]init];
    img_calender.frame = CGRectMake(85,CGRectGetMaxY(lbl_date.frame), 30, 30);
    [img_calender setImage:[UIImage imageNamed:@"img-calender@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_calender setUserInteractionEnabled:YES];
    [bg_for_date addSubview:img_calender];
    
    UITextField *txt_date = [[UITextField alloc] init];
    txt_date .frame = CGRectMake(CGRectGetMaxX( img_calender.frame)+5,CGRectGetMaxY(lbl_date.frame), 100, 30);
    txt_date .borderStyle = UITextBorderStyleNone;
    txt_date .textColor = [UIColor grayColor];
    txt_date .font = [UIFont fontWithName:kFont size:13];
    txt_date .placeholder = @"15-07-2015";
    [txt_date  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_date  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_date .leftView = padding;
    txt_date .leftViewMode = UITextFieldViewModeAlways;
    txt_date .userInteractionEnabled=NO;
    txt_date .textAlignment = NSTextAlignmentLeft;
    txt_date .backgroundColor = [UIColor clearColor];
    txt_date .keyboardType = UIKeyboardTypeAlphabet;
    txt_date .delegate = self;
    [bg_for_date addSubview:txt_date ];
    
    UIButton *btn_set_date = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_set_date.frame = CGRectMake(85,CGRectGetMaxY(lbl_date.frame),150,35);
    btn_set_date .backgroundColor = [UIColor clearColor];
    [btn_set_date addTarget:self action:@selector(btn_set_date_click:) forControlEvents:UIControlEventTouchUpInside];
    // [btn_set_date setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    [bg_for_date   addSubview:btn_set_date];
    
    
    UILabel *lbl_serving_time = [[UILabel alloc]init];
    lbl_serving_time.frame = CGRectMake(105,CGRectGetMaxY(txt_date.frame)+5, 150, 45);
    lbl_serving_time.text = @"Serving Time";
    lbl_serving_time.font = [UIFont fontWithName:kFont size:18];
    lbl_serving_time.textColor = [UIColor blackColor];
    lbl_serving_time.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_serving_time];
    
    UIImageView *bg_for_time = [[UIImageView alloc]init];
    bg_for_time.frame = CGRectMake(-5,CGRectGetMaxY(lbl_serving_time.frame)-5, WIDTH+20, 50);
    [bg_for_time setUserInteractionEnabled:YES];
    bg_for_time.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_serch_for_food_later addSubview:bg_for_time];

    
    UIImageView *img_clock = [[UIImageView alloc]init];
    img_clock.frame = CGRectMake(85,CGRectGetMaxY(lbl_serving_time.frame), 30, 30);
    [img_clock setImage:[UIImage imageNamed:@"img-clock@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_clock setUserInteractionEnabled:YES];
    [bg_for_time addSubview:img_clock];
    
    UITextField *txt_set_time = [[UITextField alloc] init];
    txt_set_time .frame = CGRectMake(CGRectGetMaxX( img_clock.frame)+5,CGRectGetMaxY(lbl_serving_time.frame), 100, 30);
    txt_set_time .borderStyle = UITextBorderStyleNone;
    txt_set_time .textColor = [UIColor grayColor];
    txt_set_time .font = [UIFont fontWithName:kFont size:13];
    txt_set_time .placeholder = @"10:45AM";
    [txt_set_time  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_set_time  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_set_time .leftView = padding1;
    txt_set_time .leftViewMode = UITextFieldViewModeAlways;
    txt_set_time .userInteractionEnabled=NO;
    txt_set_time .textAlignment = NSTextAlignmentLeft;
    txt_set_time .backgroundColor = [UIColor clearColor];
    txt_set_time .keyboardType = UIKeyboardTypeAlphabet;
    txt_set_time .delegate = self;
    [bg_for_time addSubview:txt_set_time ];
    
    UIButton *btn_set_time = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_set_time.frame = CGRectMake(85,CGRectGetMaxY(lbl_serving_time.frame),150,35);
    btn_set_time .backgroundColor = [UIColor clearColor];
    [btn_set_time addTarget:self action:@selector(btn_set_time_click:) forControlEvents:UIControlEventTouchUpInside];
    // [btn_set_date setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    [bg_for_time   addSubview:btn_set_time];
    
    
    UILabel *lbl_location_in_food_later = [[UILabel alloc]init];
    lbl_location_in_food_later.frame = CGRectMake(105,5, 150, 45);
    lbl_location_in_food_later.text = @"Location";
    lbl_location_in_food_later.font = [UIFont fontWithName:kFont size:18];
    lbl_location_in_food_later.textColor = [UIColor blackColor];
    lbl_location_in_food_later.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_location_in_food_later];
    
    
    
    UIImageView *bg_for_location2 = [[UIImageView alloc]init];
    bg_for_location2.frame = CGRectMake(-5, CGRectGetMaxY(lbl_location.frame)-5, WIDTH+20, 50);
    [bg_for_location2 setUserInteractionEnabled:YES];
    bg_for_location2.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [ view_serch_for_food_later addSubview:bg_for_location2];

    UIImageView *img_pointer_location2 = [[UIImageView alloc]init];
    img_pointer_location2.frame = CGRectMake(25,0, 30, 30);
    [img_pointer_location2 setImage:[UIImage imageNamed:@"pointer-location@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_pointer_location2 setUserInteractionEnabled:YES];
    [bg_for_location2 addSubview:img_pointer_location2];
    
    UILabel *lbl_set_location2 = [[UILabel alloc]init];
    lbl_set_location2 .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+5,CGRectGetMaxY(lbl_location.frame), 250, 30);
    lbl_set_location2.text = @"Smith Street, 77 Block (5km)";
    lbl_set_location2.font = [UIFont fontWithName:kFont size:18];
    lbl_set_location2.textColor = [UIColor blackColor];
    lbl_set_location2.backgroundColor = [UIColor clearColor];
    [bg_for_location2 addSubview:lbl_set_location2];
    

    UIButton *btn_img_right2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_right2.frame = CGRectMake(CGRectGetMidX(lbl_set_location.frame)+80,CGRectGetMaxY(lbl_location.frame),30,30);
    btn_img_right2 .backgroundColor = [UIColor clearColor];
    [btn_img_right2 addTarget:self action:@selector(btn_right_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_right2 setImage:[UIImage imageNamed:@"right-arrow@2x.png"] forState:UIControlStateNormal];
    [bg_for_location2   addSubview:btn_img_right2];
    
    
    UIButton *btn_on_set_location2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_set_location2.frame = CGRectMake(25,CGRectGetMaxY(lbl_location.frame),270,35);
    btn_on_set_location2 .backgroundColor = [UIColor purpleColor];
    [btn_on_set_location2 addTarget:self action:@selector(btn_on_set_location_click:) forControlEvents:UIControlEventTouchUpInside];
    // [btn_set_date setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    [bg_for_location   addSubview:btn_on_set_location2];
    
    UILabel *lbl_dish_r_meal2 = [[UILabel alloc]init];
    // lbl_dish_r_meal2.frame = CGRectMake(105,CGRectGetMaxY(txt_set_location.frame)+10, 150, 45);
    lbl_dish_r_meal2.text = @"Dish/Meal";
    lbl_dish_r_meal2.font = [UIFont fontWithName:kFont size:18];
    lbl_dish_r_meal2.textColor = [UIColor blackColor];
    lbl_dish_r_meal2.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_dish_r_meal2];
    
    UIImageView *bg_for_meal2 = [[UIImageView alloc]init];
    bg_for_meal2.frame = CGRectMake(-5,CGRectGetMaxY(lbl_dish_r_meal.frame)-5, WIDTH+20, 50);
    [bg_for_meal2 setUserInteractionEnabled:YES];
    bg_for_meal2.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [ view_serch_for_food_later addSubview:bg_for_meal2];
    
    
    btn_on_single_dish  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_single_dish.frame = CGRectMake(90,CGRectGetMaxY(lbl_dish_r_meal.frame),60,50);
    [btn_on_single_dish setImage:[UIImage imageNamed:@"img-single-bl@2x.png"] forState:UIControlStateSelected];
    [btn_on_single_dish setImage:[UIImage imageNamed:@"single-dish-icon@2x.png"] forState:UIControlStateNormal];
    btn_on_single_dish.tag = 54;
    [btn_on_single_dish addTarget:self action:@selector(btn_on_dish_click:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_meal2 addSubview:  btn_on_single_dish];
    
    
    img_red_tik3 = [[UIImageView alloc]init];
    //img_red_tik3.frame =  CGRectMake(CGRectGetMidX(btn_img_single_dish.frame)+135,CGRectGetMaxY(lbl_dish_r_meal.frame)+15,25,25);
    [img_red_tik3 setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik3 setUserInteractionEnabled:YES];
    [bg_for_meal2 addSubview:img_red_tik3];
    img_red_tik3.hidden = YES;
    
    btn_on_meal2  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_meal2.frame = CGRectMake(CGRectGetMaxX( btn_on_single_dish.frame)+20,5,60,50);
    [btn_on_meal2 setImage:[UIImage imageNamed:@"iocn-meal-bl@2x.png"] forState:UIControlStateSelected];
    [btn_on_meal2 setImage:[UIImage imageNamed:@"iocn-meal@2x.png"] forState:UIControlStateNormal];
    btn_on_meal2.tag = 55;
    [btn_on_meal2 addTarget:self action:@selector(btn_on_dish_click:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_meal2 addSubview:  btn_on_meal2];

    
    img_red_tik4 = [[UIImageView alloc]init];
    //img_red_tik4.frame =  CGRectMake(CGRectGetMidX(btn_img_single_dish.frame)+135,CGRectGetMaxY(lbl_dish_r_meal.frame)+15,25,25);
    [img_red_tik4 setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik4 setUserInteractionEnabled:YES];
    [bg_for_meal addSubview:img_red_tik4];
    img_red_tik4.hidden = YES;
    


    UILabel *lbl_seving_type2 = [[UILabel alloc]init];
    lbl_seving_type2.frame = CGRectMake(105,CGRectGetMaxY(bg_for_meal.frame)+150, 150, 45);
    lbl_seving_type2.text = @"Serving Type";
    lbl_seving_type2.font = [UIFont fontWithName:kFont size:18];
    lbl_seving_type2.textColor = [UIColor blackColor];
    lbl_seving_type2.backgroundColor = [UIColor clearColor];
    [ view_serch_for_food_later addSubview:lbl_seving_type2];
    
    UIImageView *bg_for_serving_type2 = [[UIImageView alloc]init];
    bg_for_serving_type2.frame = CGRectMake(-5,CGRectGetMaxY(lbl_seving_type.frame)-5, WIDTH+20, 50);
    [bg_for_serving_type2 setUserInteractionEnabled:YES];
    bg_for_serving_type2.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [ view_serch_for_food_later addSubview:bg_for_serving_type2];
    
//    btn_on_dine_in_later  = [UIButton buttonWithType:UIButtonTypeCustom];
//    btn_on_dine_in_later.frame = CGRectMake(45,CGRectGetMaxY(lbl_seving_type.frame),50,50);
//    [ btn_on_dine_in_later setImage:[UIImage imageNamed:@"img-dine-in-bl@2x.png"] forState:UIControlStateSelected];
//    [ btn_on_dine_in_later setImage:[UIImage imageNamed:@"img-dine-in@2x.png"] forState:UIControlStateNormal];
//    btn_on_dine_in_later.tag = 0;
//    [ btn_on_dine_in_later addTarget:self action:@selector(click_on_serving_type_later:)forControlEvents:UIControlEventTouchUpInside];
//    [bg_for_serving_type2 addSubview: btn_on_dine_in_later];
//
//    img_red_tik_on_dine_in2 = [[UIImageView alloc]init];
//    img_red_tik_on_dine_in.frame =  CGRectMake(CGRectGetMidX(btn_on_dine_in.frame)+135,CGRectGetMaxY(lbl_dish_r_meal.frame)+15,25,25);
//    [img_red_tik_on_dine_in setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
//    //   img_red_tik.backgroundColor = [UIColor redColor];
//    [img_red_tik_on_dine_in setUserInteractionEnabled:YES];
//    [bg_for_serving_type2 addSubview:img_red_tik_on_dine_in];
//    img_red_tik_on_dine_in.hidden = YES;
//    
////
//    btn_on_take_out2  = [UIButton buttonWithType:UIButtonTypeCustom];
//    btn_on_take_out.frame = CGRectMake(45,CGRectGetMaxY(lbl_seving_type.frame),50,50);
//    [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out-bl@2x.png"]forState:UIControlStateSelected];
//    [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out@2x.png"]forState:UIControlStateNormal];
//    btn_on_take_out.tag = 1;
//    [btn_on_take_out addTarget:self action:@selector(click_on_serving_type:)forControlEvents:UIControlEventTouchUpInside];
//    [bg_for_serving_type2 addSubview:btn_on_take_out];
////
//    img_red_tik_on_take_out = [[UIImageView alloc]init];
//    img_red_tik_on_take_out.frame =  CGRectMake(CGRectGetMidX(btn_on_take_out .frame)+135,CGRectGetMaxY(lbl_dish_r_meal.frame)+15,25,25);
//    [img_red_tik_on_take_out setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
//    //   img_red_tik.backgroundColor = [UIColor redColor];
//    [img_red_tik_on_take_out setUserInteractionEnabled:YES];
//    [bg_for_serving_type addSubview:img_red_tik_on_take_out];
//    img_red_tik_on_take_out.hidden = YES;
//    
//    
//    btn_on_delever  = [UIButton buttonWithType:UIButtonTypeCustom];
//    btn_on_delever.frame = CGRectMake(45,CGRectGetMaxY(lbl_seving_type.frame),50,50);
//    [btn_on_delever setImage:[UIImage imageNamed:@"img-delever-bl@2x.png"] forState:UIControlStateSelected];
//    [btn_on_delever setImage:[UIImage imageNamed:@"img-delever@2x.png"] forState:UIControlStateNormal];
//    btn_on_delever.tag = 2;
//    [btn_on_delever addTarget:self action:@selector(click_on_serving_type:)forControlEvents:UIControlEventTouchUpInside];
//    [bg_for_serving_type addSubview:btn_on_delever];
//    
//    img_red_tik_on_deliver = [[UIImageView alloc]init];
//    img_red_tik_on_deliver.frame =  CGRectMake(CGRectGetMidX(btn_on_delever.frame)+135,CGRectGetMaxY(lbl_dish_r_meal.frame)+15,25,25);
//    [img_red_tik_on_deliver setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
//    //   img_red_tik.backgroundColor = [UIColor redColor];
//    [img_red_tik_on_deliver setUserInteractionEnabled:YES];
//    [bg_for_serving_type addSubview:img_red_tik_on_deliver];
//    img_red_tik_on_deliver.hidden = YES;
//    
    UILabel *lbl_keywords2 = [[UILabel alloc]init];
    lbl_keywords2.frame = CGRectMake(130,CGRectGetMaxY(bg_for_serving_type.frame)+5, 150, 45);
    lbl_keywords2.text = @"Keywords";
    lbl_keywords2.font = [UIFont fontWithName:kFont size:18];
    lbl_keywords2.textColor = [UIColor blackColor];
    lbl_keywords2.backgroundColor = [UIColor clearColor];
    [ view_serch_for_food_later addSubview:lbl_keywords2];

    UIImageView *bg_for_Keyword2 = [[UIImageView alloc]init];
    bg_for_Keyword2.frame = CGRectMake(-5,CGRectGetMaxY(lbl_keywords.frame)-5, WIDTH+20, 50);
    [bg_for_Keyword2 setUserInteractionEnabled:YES];
    bg_for_Keyword2.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [ view_serch_for_food_later addSubview:bg_for_Keyword2];
    
    
    UIImageView *img_serch_bar2 = [[UIImageView alloc]init];
    img_serch_bar2.frame = CGRectMake(25,5, 290, 30);
    [img_serch_bar2 setImage:[UIImage imageNamed:@"search-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_serch_bar2 setUserInteractionEnabled:YES];
    [bg_for_Keyword2 addSubview:img_serch_bar2];
    
    
    UITextField *txt_search2 = [[UITextField alloc] init];
    //  txt_search.frame = CGRectMake(10,-4, 200, 45);
    txt_search2 .borderStyle = UITextBorderStyleNone;
    txt_search2 .textColor = [UIColor grayColor];
    txt_search2 .font = [UIFont fontWithName:kFont size:15];
    txt_search2 .placeholder = @"Search...";
    [txt_search2  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_search2 setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_search2 .leftView = padding4;
    txt_search2 .leftViewMode = UITextFieldViewModeAlways;
    txt_search2 .userInteractionEnabled=YES;
    txt_search2 .textAlignment = NSTextAlignmentLeft;
    txt_search2 .backgroundColor = [UIColor clearColor];
    txt_search2 .keyboardType = UIKeyboardTypeAlphabet;
    txt_search2 .delegate = self;
    [img_serch_bar2 addSubview:txt_search2];
    
    
    UIImageView *icon_search2 = [[UIImageView alloc]init];
    //   icon_search2.frame = CGRectMake(255,8, 15, 15);
    [icon_search2 setImage:[UIImage imageNamed:@"icon-search@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_search2 setUserInteractionEnabled:YES];
    [img_serch_bar2 addSubview:icon_search2];
    
    UIButton *btn_on_search_bar2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_search_bar2.frame = CGRectMake(240,0, 30, 30);
    btn_on_search_bar2 .userInteractionEnabled=YES;
    btn_on_search_bar2 .backgroundColor = [UIColor purpleColor];
    [btn_on_search_bar2 addTarget:self action:@selector(btn_on_search_bar_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar2 setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [img_serch_bar2   addSubview:btn_on_search_bar2];
    
    //view for btn more filters ans  dish table view
    
    view_for_more_filters_in_later = [[UIView alloc]init];
    view_for_more_filters_in_later.frame=CGRectMake(5,CGRectGetMaxY(bg_for_Keyword.frame),WIDTH-10,300);
    [view_for_more_filters_in_later setUserInteractionEnabled:YES];
    view_for_more_filters_in_later.backgroundColor=[UIColor purpleColor];
    view_for_more_filters_in_later.hidden = NO;
    [ view_serch_for_food_later addSubview: view_for_more_filters_in_later];
    
    
    UIButton *btn_img_more_filters_in_later = [UIButton buttonWithType:UIButtonTypeCustom];
    //  btn_img_more_filters_in_later.frame = CGRectMake(30,CGRectGetMaxY(img_serch_bar.frame)+20, 260, 40);
    btn_img_more_filters_in_later .backgroundColor = [UIColor clearColor];
    [btn_img_more_filters_in_later addTarget:self action:@selector(btn_img_on_more_filters_later_click:) forControlEvents:UIControlEventTouchUpInside];
    [ btn_img_more_filters_in_later setImage:[UIImage imageNamed:@"img-more-filters@2x.png"] forState:UIControlStateNormal];
    [view_for_more_filters_in_later   addSubview:btn_img_more_filters_in_later];
    
    
#pragma mark Tableview
    
    table_for_items_in_later = [[UITableView alloc] init ];
    //   table_for_items_in_later.frame  = CGRectMake(15,CGRectGetMaxY(btn_img_more_filters.frame)+5,WIDTH-30,370);
    [table_for_items_in_later setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_items_in_later.delegate = self;
    table_for_items_in_later.dataSource = self;
    table_for_items_in_later.showsVerticalScrollIndicator = NO;
    table_for_items_in_later.backgroundColor = [UIColor redColor];
    [view_for_more_filters_in_later addSubview:table_for_items_in_later];
    
    view_for_cuisines_in_later = [[UIView alloc]init];
    view_for_cuisines_in_later.frame=CGRectMake(5,CGRectGetMaxY(bg_for_Keyword.frame),WIDTH-10,300);
    [view_for_cuisines_in_later setUserInteractionEnabled:YES];
    view_for_cuisines_in_later.backgroundColor=[UIColor purpleColor];
    view_for_cuisines_in_later.hidden = YES;
    [ view_serch_for_food_later addSubview: view_for_cuisines_in_later];



    
    if (IS_IPHONE_6Plus)
    {
        
    }
    else if (IS_IPHONE_6)
    {
        scroll.frame = CGRectMake(0, 46, WIDTH, HEIGHT-100);
         view_serch_for_food_now.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+150);
        
        lbl_location.frame = CGRectMake(145,5, 150, 45);
        
        bg_for_location.frame = CGRectMake(-5, CGRectGetMaxY(lbl_location.frame)-5, WIDTH+20, 50);
        img_pointer_location.frame = CGRectMake(25,7, 30, 30);
       //  txt_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+20,CGRectGetMaxY(lbl_location.frame), 250, 30);
        lbl_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+20,9, 250, 30);
        
        btn_img_right.frame = CGRectMake(CGRectGetMidX(lbl_set_location.frame)+140,10,30,30);
        btn_on_set_location.frame = CGRectMake(0,0,WIDTH+5,50);
        
       
        lbl_dish_r_meal.frame = CGRectMake(140,CGRectGetMaxY( bg_for_location.frame)+10, 150, 45);
        bg_for_meal.frame = CGRectMake(-5,CGRectGetMaxY(lbl_dish_r_meal.frame)-5, WIDTH+20, 55);
        btn_on_single_dish.frame = CGRectMake(100,5,60,50);
        img_red_tik1.frame =  CGRectMake(CGRectGetMidX( btn_on_single_dish.frame)+18,40,20,15);
        btn_on_meal.frame = CGRectMake(CGRectGetMaxX( btn_on_single_dish.frame)+50,3,50,50);
        img_red_tik2.frame =  CGRectMake(CGRectGetMidX(btn_on_meal.frame)+18,40,20,15);
        
        lbl_seving_type.frame = CGRectMake(135,CGRectGetMaxY(bg_for_meal.frame)+5, 150, 45);
        bg_for_serving_type.frame = CGRectMake(-5,CGRectGetMaxY(lbl_seving_type.frame)-5, WIDTH+20, 70);
        btn_on_dine_in.frame = CGRectMake(45,10,50,50);
        img_red_tik_on_dine_in.frame = CGRectMake(CGRectGetMidX(btn_on_dine_in.frame)+5,35,25,20);
        btn_on_take_out.frame = CGRectMake(CGRectGetMaxX(btn_on_dine_in.frame)+70,10,50,50);
        img_red_tik_on_take_out.frame = CGRectMake(CGRectGetMidX(btn_on_take_out.frame)+5,35,25,20);
        btn_on_delever.frame = CGRectMake(CGRectGetMaxX( btn_on_take_out.frame)+70,10,50,50);
        img_red_tik_on_deliver.frame = CGRectMake(CGRectGetMidX(btn_on_delever.frame)+5,35,25,20);

        lbl_keywords.frame = CGRectMake(155,CGRectGetMaxY(bg_for_serving_type.frame)+5, 150, 45);
        bg_for_Keyword.frame = CGRectMake(-5,CGRectGetMaxY(lbl_keywords.frame)-5, WIDTH+20, 50);
        img_serch_bar.frame = CGRectMake(15,10, WIDTH-20, 30);
        txt_search.frame = CGRectMake(0,-8, 300, 45);
        icon_search.frame = CGRectMake(330,8, 15, 15);
        btn_on_search_bar.frame = CGRectMake(330,0, 30, 30);
        
         view_for_more_filters.frame=CGRectMake(0,CGRectGetMaxY(bg_for_Keyword.frame)+20,WIDTH,380);
         btn_img_more_filters.frame = CGRectMake(40,10, WIDTH-80, 45);
         table_for_items.frame  = CGRectMake(5,CGRectGetMaxY(btn_img_more_filters.frame)+5,WIDTH-10,300);
        
         view_for_cuisines.frame=CGRectMake(5,CGRectGetMaxY(bg_for_Keyword.frame),WIDTH-10,300);
         lbl_cuisines.frame = CGRectMake(130,5, 150, 45);
         bg_for_favorits_and_all.frame = CGRectMake(0,CGRectGetMaxY(lbl_cuisines.frame), WIDTH-11, 50);
         lbl_my_favorites.frame = CGRectMake(30,03, 150, 45);
         img_strip1.frame = CGRectMake(35, CGRectGetMaxY(lbl_my_favorites.frame)-5, 100, 3);
         btn_my_favorites.frame = CGRectMake(0,03, 180, 45);
         lbl_all.frame = CGRectMake(250,3, 150, 45);
        img_strip2.frame = CGRectMake(250, CGRectGetMaxY(lbl_my_favorites.frame)-5, 57, 3);
        btn_on_all.frame = CGRectMake(185,03, 180, 45);
        
        view_for_my_favorites.frame=CGRectMake(-5,CGRectGetMaxY(bg_for_favorits_and_all.frame)+1,WIDTH,350);
        
        bg_for_cuisines.frame = CGRectMake(0,8, WIDTH, 93);
        collView_dish_types = [[UICollectionView alloc] initWithFrame:CGRectMake(15,10,WIDTH-30,90)
                                                 collectionViewLayout:layout];
        
        
        //FRAMES FOR FOOD LATER
        
        view_serch_for_food_later.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT);
        lbl_date.frame = CGRectMake(150,0, 150, 45);
        bg_for_date.frame = CGRectMake(-5,CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
        img_calender.frame = CGRectMake(85,5, 30, 30);
        txt_date .frame = CGRectMake(CGRectGetMaxX( img_calender.frame)+5,5, 100, 30);
        btn_set_date.frame = CGRectMake(85,5,150,35);
        
        lbl_serving_time.frame = CGRectMake(125,CGRectGetMaxY(bg_for_date.frame)+5, 150, 45);
        bg_for_time.frame = CGRectMake(-5,CGRectGetMaxY(lbl_serving_time.frame)-5, WIDTH+20, 50);
        img_clock.frame = CGRectMake(120,5, 30, 30);
        txt_set_time .frame = CGRectMake(CGRectGetMaxX( img_clock.frame)+5,5, 100, 30);
        btn_set_time.frame = CGRectMake(85,5,150,35);
        
        lbl_location_in_food_later.frame = CGRectMake(140,CGRectGetMaxY(bg_for_time.frame), 150, 45);
        bg_for_location2.frame = CGRectMake(-5, CGRectGetMaxY(lbl_location_in_food_later.frame)-5, WIDTH+20, 50);
        img_pointer_location2.frame = CGRectMake(25,5, 30, 30);
        lbl_set_location2.frame = CGRectMake(CGRectGetMaxX(img_pointer_location2.frame)+10,5,250, 30);
        btn_img_right2.frame = CGRectMake(CGRectGetMidX(lbl_set_location2.frame)+140,5,30,30);
         btn_on_set_location2.frame = CGRectMake(0,0,WIDTH+5,50);
        lbl_dish_r_meal2.frame = CGRectMake(140,CGRectGetMaxY( bg_for_location2.frame)+10, 150, 45);
        bg_for_meal2.frame = CGRectMake(-5,CGRectGetMaxY(lbl_dish_r_meal2.frame)-5, WIDTH+20, 55);
        btn_on_single_dish.frame = CGRectMake(100,5,60,50);

        img_red_tik3.frame =   CGRectMake(CGRectGetMidX( btn_on_single_dish.frame)+18,40,20,15);
        btn_on_meal2.frame = CGRectMake(CGRectGetMaxX( btn_on_single_dish.frame)+50,3,50,50);
        img_red_tik4.frame =  CGRectMake(CGRectGetMidX(btn_on_meal2.frame)+135,CGRectGetMaxY(lbl_dish_r_meal2.frame)+15,25,25);
        lbl_seving_type2.frame = CGRectMake(135,CGRectGetMaxY(bg_for_meal2.frame)+5, 150, 45);
        bg_for_serving_type2.frame = CGRectMake(-5,CGRectGetMaxY(lbl_seving_type2.frame)-5, WIDTH+20, 70);
//         btn_on_dine_in_later.frame = CGRectMake(45,10,50,50);
//        img_red_tik_on_dine_in2.frame = CGRectMake(CGRectGetMidX(btn_on_dine_in.frame)+5,35,25,20);
        //       btn_on_take_out2.frame = CGRectMake(CGRectGetMaxX(btn_on_dine_in.frame)+70,10,50,50);
//        img_red_tik_on_take_out.frame =  CGRectMake(CGRectGetMidX(btn_on_take_out .frame)+135,CGRectGetMaxY(lbl_dish_r_meal.frame)+15,25,25);
//        btn_on_delever.frame = CGRectMake(45,CGRectGetMaxY(lbl_seving_type.frame),50,50);
//        img_red_tik_on_deliver.frame =  CGRectMake(CGRectGetMidX(btn_on_delever.frame)+135,CGRectGetMaxY(lbl_dish_r_meal.frame)+15,25,25);
        
          lbl_keywords2.frame = CGRectMake(155,CGRectGetMaxY(bg_for_serving_type2.frame), 150, 45);
          bg_for_Keyword2.frame =CGRectMake(-5,CGRectGetMaxY(lbl_keywords2.frame)-5, WIDTH+20, 50);
          img_serch_bar2.frame = CGRectMake(15,10, WIDTH-20, 30);
          txt_search2.frame = CGRectMake(0,-8, 300, 45);
          icon_search2.frame = CGRectMake(330,8, 15, 15);
          btn_on_search_bar2.frame = CGRectMake(330,0, 30, 30);
        
         view_for_more_filters_in_later.frame=CGRectMake(0,CGRectGetMaxY(bg_for_Keyword2.frame)+20,WIDTH,400);
         btn_img_more_filters_in_later.frame = CGRectMake(40,10, WIDTH-80, 45);
         table_for_items_in_later.frame  = CGRectMake(5,CGRectGetMaxY(btn_img_more_filters_in_later.frame)+5,WIDTH-10,300);
        view_for_cuisines_in_later.frame=CGRectMake(5,CGRectGetMaxY(bg_for_Keyword2.frame),WIDTH-10,300);
        
     
    }
    else
    {
        
    }
    
    [scroll setContentSize:CGSizeMake(0,890)];
    
}

#pragma mark UiCollectionView Delegate Methods for dish

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    
    
    
    if (collectionView == collView_dish_types)
    {
        return [array_imges count];
        
    }
    else if (collectionView == collView_for_course)
    {
        return [array_course_images count];
    }
    else if (collectionView == collView_for_dietary)
    {
        return [array_dietary_images count];
    }
    
    else if (collectionView == collView_dish_types2)
    {
        return [array_imges_in_all count];
        
    }
    else if (collectionView == collView_for_course2)
    {
        return [array_course_images_in_all count];
    }
    else if (collectionView == collView_for_dietary2)
    {
        return [array_dietary_images_in_all count];
    }
    
    
    
    return 4;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView1 dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    
    
    UIImageView *img_bg_on_dish_cell = [[UIImageView alloc]init];
    img_bg_on_dish_cell.frame =  CGRectMake(9, 0, 80, 90);
    img_bg_on_dish_cell.backgroundColor = [UIColor purpleColor];
    [cell.contentView addSubview:img_bg_on_dish_cell];
    
    UIImageView *dish_type_images = [[UIImageView alloc]init];
    
    UILabel *lbl_dish_names = [[UILabel alloc]init];
    lbl_dish_names.font = [UIFont fontWithName:kFont size:10];
    lbl_dish_names.textColor = [UIColor lightGrayColor];
    lbl_dish_names.backgroundColor = [UIColor clearColor];
    [img_bg_on_dish_cell addSubview:lbl_dish_names];
    
    
    
    UILabel *lbl_coures_names = [[UILabel alloc]init];
    lbl_coures_names.font = [UIFont fontWithName:kFont size:9];
    lbl_coures_names.textColor = [UIColor lightGrayColor];
    lbl_coures_names.backgroundColor = [UIColor clearColor];
    [img_bg_on_dish_cell addSubview:lbl_coures_names];
    
    
      if (collectionView1 == collView_dish_types)
    {
        dish_type_images.frame=CGRectMake(10, 8, 65,65);
        [dish_type_images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_imges objectAtIndex:indexPath.row]]]];
        
        lbl_coures_names.frame = CGRectMake(22,74, 150, 15);
        lbl_coures_names.text = [NSString stringWithFormat:@"%@",[ array_dish_names objectAtIndex:indexPath.row]];
        
    }
    else if (collectionView1 == collView_for_course)
    {
        dish_type_images.frame=CGRectMake(25, 15, 35,30);
        [dish_type_images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_course_images objectAtIndex:indexPath.row]]]];
        
        lbl_coures_names.frame = CGRectMake(22,50, 100, 15);
        lbl_coures_names.text = [NSString stringWithFormat:@"%@",[ array_course_names objectAtIndex:indexPath.row]];
        
    }
    else if (collectionView1 == collView_for_dietary)
    {
        dish_type_images.frame=CGRectMake(13, 5, 50,55);
        [dish_type_images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_dietary_images objectAtIndex:indexPath.row]]]];
    }
    
    else if (collectionView1 == collView_dish_types2)
    {
        dish_type_images.frame=CGRectMake(10, 8, 65,65);
        [dish_type_images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_imges_in_all objectAtIndex:indexPath.row]]]];
        
        lbl_coures_names.frame = CGRectMake(22,74, 150, 15);
        lbl_coures_names.text = [NSString stringWithFormat:@"%@",[ array_dish_names_in_all objectAtIndex:indexPath.row]];
        
    }
    else if (collectionView1 == collView_for_course2)
    {
        dish_type_images.frame=CGRectMake(25, 15, 35,30);
        [dish_type_images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_course_images_in_all objectAtIndex:indexPath.row]]
                                    ]];
        
        lbl_coures_names.frame = CGRectMake(22,50, 100, 15);
        lbl_coures_names.text = [NSString stringWithFormat:@"%@",[ array_course_names objectAtIndex:indexPath.row]];
        
        
    }
    else if (collectionView1 == collView_for_dietary2)
    {
        dish_type_images.frame=CGRectMake(13, 5, 50,55);
        [dish_type_images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_dietary_images_in_all objectAtIndex:indexPath.row]]]];
    }
    
    
    
    [dish_type_images setUserInteractionEnabled:YES];
    //    [img_Images setContentMode:UIViewContentModeScaleAspectFill];
    //    [img_Images setClipsToBounds:YES];
    [dish_type_images setUserInteractionEnabled:YES];
    [img_bg_on_dish_cell addSubview:dish_type_images];
    
    
     return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == collView_dish_types)
    {
        return CGSizeMake((WIDTH-33)/4, 88);
    }
    else if (collectionView == collView_for_course)
    {
        return CGSizeMake((WIDTH)/3, 80);
        
    }
    else if (collectionView == collView_for_dietary)
    {
        return CGSizeMake((WIDTH-68)/4, 90);
    }
    else if (collectionView == collView_dish_types2)
    {
        return CGSizeMake((WIDTH-33)/4, 88);
        
    }
    else if (collectionView == collView_for_course2)
    {
        return CGSizeMake((WIDTH)/3, 80);
    }
    else if (collectionView == collView_for_dietary2)
    {
        return CGSizeMake((WIDTH-68)/4, 90);
    }
    return CGSizeMake((WIDTH-56)/4, 90);}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
}

#pragma tableview delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == table_on_drop_down)
    {
        return 2;
    }
    else if (tableView == table_for_items)
    {
        return [ary_displaynames count];
       
    }
    else if (tableView == table_for_items_in_later)
    {
        return [ary_displaynames count];

    }
    return 0;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table_on_drop_down)
    {
        return 40;
    }
    else if (tableView == table_for_items)
    {
        
        return 180;

    }
    else if (tableView == table_for_items_in_later)
    {
         return 180;
    }
    return 0;
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    if (tableView == table_on_drop_down)
    {
        img_bg_for_first_tbl = [[UIImageView alloc]init];
        img_bg_for_first_tbl.frame =  CGRectMake(0,5, 150, 40);
        img_bg_for_first_tbl.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_bg_for_first_tbl setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_first_tbl];
        
        lbl_food_now_r_later = [[UILabel alloc]init];
        lbl_food_now_r_later .frame = CGRectMake(5,10,200, 15);
        lbl_food_now_r_later .text = [NSString stringWithFormat:@"%@",[ array_head_names objectAtIndex:indexPath.row]];
        lbl_food_now_r_later .font = [UIFont fontWithName:kFontBold size:15];
        lbl_food_now_r_later .textColor = [UIColor blackColor];
        lbl_food_now_r_later .backgroundColor = [UIColor clearColor];
        [img_bg_for_first_tbl addSubview: lbl_food_now_r_later ];

    }
    else if (tableView == table_for_items)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH+5, 170);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-5, 170);
            
        }
        else
        {    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-10,170);
            
        }
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        UIImageView *img_dish = [[UIImageView alloc] init];
        //    img_dish.frame = CGRectMake(7,7, 90,  95 );
        [img_dish setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_img objectAtIndex:indexPath.row]]]];
        [img_cellBackGnd addSubview:img_dish];
        
        UIButton *btn_favorite = [UIButton buttonWithType:UIButtonTypeCustom];
        //  btn_favorite.frame = CGRectMake(5,58, 35, 35);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [btn_favorite addTarget:self action:@selector(btn_favorite_click:) forControlEvents:UIControlEventTouchUpInside];
        [btn_favorite setImage:[UIImage imageNamed:@"favorite-icon@2x.png"] forState:UIControlStateNormal];
        [img_dish   addSubview:btn_favorite];
        
        UIButton *btn_number_three = [UIButton buttonWithType:UIButtonTypeCustom];
        //   btn_number_three.frame = CGRectMake(58,-5, 35, 35);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [btn_number_three addTarget:self action:@selector(btn_number_three_click:) forControlEvents:UIControlEventTouchUpInside];
        [btn_number_three setImage:[UIImage imageNamed:@"red3-icon@2x.png"] forState:UIControlStateNormal];
        [img_dish   addSubview:btn_number_three];
        
        
        UIButton *icon_delete = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_delete.frame = CGRectMake(WIDTH-70,20, 40, 40);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_delete addTarget:self action:@selector(btn_add_to_cart_click:) forControlEvents:UIControlEventTouchUpInside];
        [icon_delete setImage:[UIImage imageNamed:@"img-cart@2x.png"] forState:UIControlStateNormal];
        [cell.contentView   addSubview:icon_delete];
        
        UILabel *dish_name = [[UILabel alloc]init];
        //   dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
        dish_name.text = [NSString stringWithFormat:@"%@",[ary_displaynames objectAtIndex:indexPath.row]];
        dish_name.font = [UIFont fontWithName:kFontBold size:8];
        dish_name.textColor = [UIColor blackColor];
        dish_name.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:dish_name];
        
        
        UIImageView *icon_location = [[UIImageView alloc] init];
        //    icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        [icon_location setImage:[UIImage imageNamed:@"location-icon@2x.png"]];
        [img_cellBackGnd  addSubview:icon_location];
        
        UILabel *meters = [[UILabel alloc]init];
        //  meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        meters.text = @"5km";
        meters.font = [UIFont fontWithName:kFont size:15];
        meters.textColor = [UIColor blackColor];
        meters.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:meters];
        
        UIButton *icon_server = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
        //icon_server .backgroundColor = [UIColor clearColor];
        [icon_server  addTarget:self action:@selector(click_on_icon_sever_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_server setImage:[UIImage imageNamed:@"serving-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_server];
        
        UIButton *icon_halal = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
        //icon_halal .backgroundColor = [UIColor clearColor];
        [icon_halal addTarget:self action:@selector(click_on_icon_halal_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_halal setImage:[UIImage imageNamed:@"halal-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_halal];
        
        UIImageView *img_non_veg = [[UIImageView alloc] init];
        //  img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
        [img_non_veg setImage:[UIImage imageNamed:@"non-veg bt@2x.png"]];
        [img_cellBackGnd addSubview:img_non_veg];
        
        UIButton *icon_cow = [UIButton buttonWithType:UIButtonTypeCustom];
        //  icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_cow addTarget:self action:@selector(click_on_icon_cow_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_cow setImage:[UIImage imageNamed:@"cow-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_cow];
        
        UIButton *icon_fronce = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_fronce addTarget:self action:@selector(click_on_fronce_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_fronce setImage:[UIImage imageNamed:@"fronce-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_fronce];
        
        UILabel *doller_rate = [[UILabel alloc]init];
        //   doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
        doller_rate.text = @"$14.90";
        doller_rate.font = [UIFont fontWithName:kFontBold size:16];
        doller_rate.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        doller_rate.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:doller_rate];
        
        UIImageView *img_line = [[UIImageView alloc] init];
        //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
        [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
        [img_cellBackGnd addSubview:img_line];
        
        
        UIButton *icon_take = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        //icon_take .backgroundColor = [UIColor clearColor];
        [icon_take addTarget:self action:@selector(click_on_take_out_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_take setImage:[UIImage imageNamed:@"take-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_take];
        
        UIButton *icon_deliver = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //icon_deliver .backgroundColor = [UIColor clearColor];
        [icon_deliver addTarget:self action:@selector(click_on_icon_delivery_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_deliver setImage:[UIImage imageNamed:@"deliver-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_deliver];
        
        
        UIButton *img_btn_seving_Now = [UIButton buttonWithType:UIButtonTypeCustom];
        //   img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //img_btn_deliver_Now .backgroundColor = [UIColor clearColor];
        [img_btn_seving_Now addTarget:self action:@selector(click_on_seving_now_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_seving_Now setImage:[UIImage imageNamed:@"now-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_seving_Now];
        
        
        UIButton *img_btn_chef_menu = [UIButton buttonWithType:UIButtonTypeCustom];
        //    img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
        [img_btn_chef_menu addTarget:self action:@selector(click_on_icon_chef_menu_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_chef_menu setImage:[UIImage imageNamed:@"chef-menu-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_chef_menu];
        
        
        
        UIButton *icon_thumb = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+5, 20, 20);
        //icon_thumb .backgroundColor = [UIColor clearColor];
        [icon_thumb addTarget:self action:@selector(click_on_icon_thumb_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_thumb setImage:[UIImage imageNamed:@"thumb-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_thumb];
        
        UILabel *likes = [[UILabel alloc]init];
        //  likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
        likes.text = @"87.4%";
        likes.font = [UIFont fontWithName:kFont size:10];
        likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        likes.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:likes];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            btn_number_three.frame = CGRectMake(58,-5, 35, 35);
            
            icon_delete.frame = CGRectMake(WIDTH-70,20, 40, 40);
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+4, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+9,128,100, 10);
            
            dish_name.font = [UIFont fontWithName:kFontBold size:18];
            
        }
        else if (IS_IPHONE_6)
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            btn_number_three.frame = CGRectMake(58,-5, 35, 35);
            
            icon_delete.frame = CGRectMake(WIDTH-62,20, 40, 40);
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(300,CGRectGetMaxY(img_line.frame)+5, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
            
            
            dish_name.font = [UIFont fontWithName:kFontBold size:18];
            
        }
        else
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            btn_number_three.frame = CGRectMake(58,-5, 35, 35);
            
            icon_delete.frame = CGRectMake(WIDTH-65,20, 40, 40);
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(IS_IPHONE_5?WIDTH-75:WIDTH-80,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(IS_IPHONE_5?240:230,CGRectGetMaxY(img_line.frame)+5, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
            
            
            dish_name.font = [UIFont fontWithName:kFontBold size:IS_IPHONE_5?15:15];
    }
    
    }
    else if (tableView == table_for_items_in_later)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH+5, 170);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-5, 170);
            
        }
        else
        {    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-10,170);
            
        }
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        UIImageView *img_dish = [[UIImageView alloc] init];
        //    img_dish.frame = CGRectMake(7,7, 90,  95 );
        [img_dish setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_img objectAtIndex:indexPath.row]]]];
        [img_cellBackGnd addSubview:img_dish];
        
        UIButton *btn_favorite = [UIButton buttonWithType:UIButtonTypeCustom];
        //  btn_favorite.frame = CGRectMake(5,58, 35, 35);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [btn_favorite addTarget:self action:@selector(btn_favorite_click:) forControlEvents:UIControlEventTouchUpInside];
        [btn_favorite setImage:[UIImage imageNamed:@"favorite-icon@2x.png"] forState:UIControlStateNormal];
        [img_dish   addSubview:btn_favorite];
        
        UIButton *btn_number_three = [UIButton buttonWithType:UIButtonTypeCustom];
        //   btn_number_three.frame = CGRectMake(58,-5, 35, 35);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [btn_number_three addTarget:self action:@selector(btn_number_three_click:) forControlEvents:UIControlEventTouchUpInside];
        [btn_number_three setImage:[UIImage imageNamed:@"red3-icon@2x.png"] forState:UIControlStateNormal];
        [img_dish   addSubview:btn_number_three];
        
        
        UIButton *icon_delete = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_delete.frame = CGRectMake(WIDTH-70,20, 40, 40);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_delete addTarget:self action:@selector(btn_add_to_cart_click:) forControlEvents:UIControlEventTouchUpInside];
        [icon_delete setImage:[UIImage imageNamed:@"img-cart@2x.png"] forState:UIControlStateNormal];
        [cell.contentView   addSubview:icon_delete];
        
        UILabel *dish_name = [[UILabel alloc]init];
        //   dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
        dish_name.text = [NSString stringWithFormat:@"%@",[ary_displaynames objectAtIndex:indexPath.row]];
        dish_name.font = [UIFont fontWithName:kFontBold size:8];
        dish_name.textColor = [UIColor blackColor];
        dish_name.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:dish_name];
        
        
        UIImageView *icon_location = [[UIImageView alloc] init];
        //    icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        [icon_location setImage:[UIImage imageNamed:@"location-icon@2x.png"]];
        [img_cellBackGnd  addSubview:icon_location];
        
        UILabel *meters = [[UILabel alloc]init];
        //  meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        meters.text = @"5km";
        meters.font = [UIFont fontWithName:kFont size:15];
        meters.textColor = [UIColor blackColor];
        meters.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:meters];
        
        UIButton *icon_server = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
        //icon_server .backgroundColor = [UIColor clearColor];
        [icon_server  addTarget:self action:@selector(click_on_icon_sever_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_server setImage:[UIImage imageNamed:@"serving-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_server];
        
        UIButton *icon_halal = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
        //icon_halal .backgroundColor = [UIColor clearColor];
        [icon_halal addTarget:self action:@selector(click_on_icon_halal_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_halal setImage:[UIImage imageNamed:@"halal-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_halal];
        
        UIImageView *img_non_veg = [[UIImageView alloc] init];
        //  img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
        [img_non_veg setImage:[UIImage imageNamed:@"non-veg bt@2x.png"]];
        [img_cellBackGnd addSubview:img_non_veg];
        
        UIButton *icon_cow = [UIButton buttonWithType:UIButtonTypeCustom];
        //  icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_cow addTarget:self action:@selector(click_on_icon_cow_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_cow setImage:[UIImage imageNamed:@"cow-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_cow];
        
        UIButton *icon_fronce = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_fronce addTarget:self action:@selector(click_on_fronce_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_fronce setImage:[UIImage imageNamed:@"fronce-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_fronce];
        
        UILabel *doller_rate = [[UILabel alloc]init];
        //   doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
        doller_rate.text = @"$14.90";
        doller_rate.font = [UIFont fontWithName:kFontBold size:16];
        doller_rate.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        doller_rate.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:doller_rate];
        
        UIImageView *img_line = [[UIImageView alloc] init];
        //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
        [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
        [img_cellBackGnd addSubview:img_line];
        
        
        UIButton *icon_take = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        //icon_take .backgroundColor = [UIColor clearColor];
        [icon_take addTarget:self action:@selector(click_on_take_out_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_take setImage:[UIImage imageNamed:@"take-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_take];
        
        UIButton *icon_deliver = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //icon_deliver .backgroundColor = [UIColor clearColor];
        [icon_deliver addTarget:self action:@selector(click_on_icon_delivery_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_deliver setImage:[UIImage imageNamed:@"deliver-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_deliver];
        
        
        UIButton *img_btn_seving_Now = [UIButton buttonWithType:UIButtonTypeCustom];
        //   img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //img_btn_deliver_Now .backgroundColor = [UIColor clearColor];
        [img_btn_seving_Now addTarget:self action:@selector(click_on_seving_now_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_seving_Now setImage:[UIImage imageNamed:@"now-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_seving_Now];
        
        
        UIButton *img_btn_chef_menu = [UIButton buttonWithType:UIButtonTypeCustom];
        //    img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
        [img_btn_chef_menu addTarget:self action:@selector(click_on_icon_chef_menu_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_chef_menu setImage:[UIImage imageNamed:@"chef-menu-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_chef_menu];
        
        
        
        UIButton *icon_thumb = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+5, 20, 20);
        //icon_thumb .backgroundColor = [UIColor clearColor];
        [icon_thumb addTarget:self action:@selector(click_on_icon_thumb_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_thumb setImage:[UIImage imageNamed:@"thumb-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_thumb];
        
        UILabel *likes = [[UILabel alloc]init];
        //  likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
        likes.text = @"87.4%";
        likes.font = [UIFont fontWithName:kFont size:10];
        likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        likes.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:likes];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            btn_number_three.frame = CGRectMake(58,-5, 35, 35);
            
            icon_delete.frame = CGRectMake(WIDTH-70,20, 40, 40);
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+4, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+9,128,100, 10);
            
            dish_name.font = [UIFont fontWithName:kFontBold size:18];
            
        }
        else if (IS_IPHONE_6)
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            btn_number_three.frame = CGRectMake(58,-5, 35, 35);
            
            icon_delete.frame = CGRectMake(WIDTH-62,20, 40, 40);
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(300,CGRectGetMaxY(img_line.frame)+5, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
            
            
            dish_name.font = [UIFont fontWithName:kFontBold size:18];
            
        }
        else
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            btn_number_three.frame = CGRectMake(58,-5, 35, 35);
            
            icon_delete.frame = CGRectMake(WIDTH-65,20, 40, 40);
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(IS_IPHONE_5?WIDTH-75:WIDTH-80,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(IS_IPHONE_5?240:230,CGRectGetMaxY(img_line.frame)+5, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
            
            
            dish_name.font = [UIFont fontWithName:kFontBold size:IS_IPHONE_5?15:15];
        }

    }
  
    return cell;
    
}

#pragma table view

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
            lbl_food_now.text =[array_head_names objectAtIndex:indexPath.row];
        [table_on_drop_down setHidden:YES];
        if (indexPath.row == 0)
        {
            view_serch_for_food_now.hidden = NO;
            view_serch_for_food_later.hidden = YES;
            
        }
        else if (indexPath.row == 1)
        {
            view_serch_for_food_now.hidden = YES;
            view_serch_for_food_later.hidden = NO;
            
        }
    
}


#pragma scroll View Delegates

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
}

#pragma mark Click Events
-(void)btn_menu_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click");
    [self.navigationController popViewControllerAnimated:NO];
}
-(void)btn_on_food_later_label_click:(UIButton *)sender
{
    NSLog(@"btn_on_food_later_label_click");
    
    table_on_drop_down.hidden=YES;
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        table_on_drop_down.hidden=NO;
        
        
    }
    else
    {
        [sender setSelected:NO];
        table_on_drop_down.hidden=YES;
    }

    
}
-(void)btn_drop_down:(UIButton *)sender
{
    NSLog(@"btn_on_food_later_label_click");
    
}
-(void)btn_set_date_click:(UIButton *)sender
{
    NSLog(@"img_fb_click Btn Click");
    
}
-(void)btn_set_time_click:(UIButton *)sender
{
    NSLog(@"btn_set_time_click:");
    
}
-(void)btn_right_click:(UIButton *)sender
{
    NSLog(@"btn_right_click:");
    
}
-(void)btn_on_set_location_click:(UIButton *)sender
{
    NSLog(@"btn_on_set_location_click::");
   // LocationVC*vc = [[LocationVC alloc]init];
  //  [self.navigationController pushViewController:vc animated:NO];
   
    
}

-(void)btn_on_dish_click:(UIButton *)sender
{
    NSLog(@"btn_singel_dish_click:");
    
    ABC=(int)sender.tag;
    
    if([ array_temp containsObject:[NSString stringWithFormat:@"%d",ABC]])
    {
        [ array_temp removeObject:[NSString stringWithFormat:@"%d",ABC]];
        
        if (sender.tag==54)
        {
            
            [btn_on_single_dish setImage:[UIImage imageNamed:@"single-dish-icon@2x.png"] forState:UIControlStateNormal];
            img_red_tik1.hidden=YES;
            img_red_tik3.hidden=YES;

        }
        else if (sender.tag==55)
        {
            img_red_tik2.hidden=YES;
            img_red_tik4.hidden=YES;
            
            [btn_on_meal setImage:[UIImage imageNamed:@"iocn-meal@2x.png"] forState:UIControlStateNormal];
        }
    }
    else
    {
        [ array_temp addObject:[NSString stringWithFormat:@"%d",ABC]];
        if (sender.tag==54)
        {
             [ btn_on_single_dish setImage:[UIImage imageNamed:@"img-single-bl@2x.png"] forState:UIControlStateNormal];
            img_red_tik1.hidden=NO;
            img_red_tik3.hidden=NO;
            
        }
        else if (sender.tag==55)
        {
            [btn_on_meal setImage:[UIImage imageNamed:@"iocn-meal-bl@2x.png"] forState:UIControlStateNormal];
            img_red_tik2.hidden=NO;
            img_red_tik4.hidden=NO;
            
        }
           }
    
 }
-(void)click_on_serving_type:(UIButton *)sender
{
    NSLog(@"btn_img_dine_in_click:");
    DEF=(int)sender.tag;
    
    if([ array_temp2 containsObject:[NSString stringWithFormat:@"%d",DEF]])
    {
        [array_temp2 removeObject:[NSString stringWithFormat:@"%d",DEF]];
        
        if (sender.tag == 0)
        {
            
            [btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in@2x.png"]forState:UIControlStateNormal];
             img_red_tik_on_dine_in.hidden=YES;
            
            
        }
        else if (sender.tag == 1)
        {
             img_red_tik_on_take_out.hidden=YES;
            [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out@2x.png"] forState:UIControlStateNormal];
        }
        else if (sender.tag == 2)
        {
             img_red_tik_on_deliver.hidden=YES;
            [btn_on_delever setImage:[UIImage imageNamed:@"img-delever@2x.png"] forState:UIControlStateNormal];

        }
    }
    else
    {
        [array_temp2 addObject:[NSString stringWithFormat:@"%d",DEF]];
        if (sender.tag == 0)
        {
            [btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in-bl@2x.png"]forState:UIControlStateNormal];
             img_red_tik_on_dine_in.hidden=NO;
        }
        else if (sender.tag == 1)
        {
          [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out-bl@2x.png"]forState:UIControlStateNormal];
             img_red_tik_on_take_out.hidden=NO;
            
        }
        else if (sender.tag == 2)
        {
            [btn_on_delever setImage:[UIImage imageNamed:@"img-delever-bl@2x.png"]forState:UIControlStateNormal];
             img_red_tik_on_deliver.hidden=NO;
            
        }
    }
   
 }

//-(void)click_on_serving_type_later:(UIButton *)sender
//{
//    NSLog(@"btn_img_dine_in_click:");
//    DEF=(int)sender.tag;
//    
//    if([ array_temp2 containsObject:[NSString stringWithFormat:@"%d",DEF]])
//    {
//        [array_temp2 removeObject:[NSString stringWithFormat:@"%d",DEF]];
//        
//        if (sender.tag == 0)
//        {
//            
//            [btn_on_dine_in_later setImage:[UIImage imageNamed:@"img-dine-in@2x.png"]forState:UIControlStateNormal];
//            img_red_tik_on_dine_in2.hidden=YES;
//            
//            
//        }
//        else if (sender.tag == 1)
//        {
//            img_red_tik_on_take_out.hidden=YES;
//            [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out@2x.png"] forState:UIControlStateNormal];
//        }
//        else if (sender.tag == 2)
//        {
//            img_red_tik_on_deliver.hidden=YES;
//            [btn_on_delever setImage:[UIImage imageNamed:@"img-delever@2x.png"] forState:UIControlStateNormal];
//            
//        }
//    }
//    else
//    {
//        [array_temp2 addObject:[NSString stringWithFormat:@"%d",DEF]];
//        if (sender.tag == 0)
//        {
//            [btn_on_dine_in_later setImage:[UIImage imageNamed:@"img-dine-in-bl@2x.png"]forState:UIControlStateNormal];
//            img_red_tik_on_dine_in2.hidden=NO;
//        }
//        else if (sender.tag == 1)
//        {
//            [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out-bl@2x.png"]forState:UIControlStateNormal];
//            img_red_tik_on_take_out.hidden=NO;
//            
//        }
//        else if (sender.tag == 2)
//        {
//            [btn_on_delever setImage:[UIImage imageNamed:@"img-delever-bl@2x.png"]forState:UIControlStateNormal];
//            img_red_tik_on_deliver.hidden=NO;
//            
//        }
//    }
//    
//}
-(void)btn_on_search_bar_click:(UIButton *)sender
{
    NSLog(@"btn_on_search_bar_click:");
    
}
-(void)btn_img_on_more_filters_click:(UIButton *)sender
{
    NSLog(@"btn_img_on_more_filters_click:");
    view_for_more_filters.hidden = YES;
     view_for_cuisines.hidden = NO;

    
}
-(void)btn_img_on_more_filters_later_click:(UIButton *)sender
{
    NSLog(@"btn_img_on_more_filters_click:");
    view_for_more_filters_in_later.hidden = YES;
    view_for_cuisines_in_later.hidden = NO;
}


#pragma mark Click Events for table views in food now r later

-(void)btn_add_to_cart_click:(UIButton *)sender
{
    NSLog(@"btn_delete_click:");
    
}
-(void)btn_favorite_click:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
    
}
-(void)btn_number_three_click:(UIButton *)sender
{
    NSLog(@"btn_number_three_click:");
}


-(void)icon_Food_Later_BtnClick:(UIButton *)sender
{
    NSLog(@"icon_Food_Later_BtnClick:");
    
}
-(void)click_on_icon_halal_btn:(UIButton *)sender
{
    NSLog(@"icon_halal_BtnClick:");
    
}
-(void)click_on_icon_sever_btn:(UIButton *)sender
{
    NSLog(@"icon_server_BtnClick:");
    
}
-(void)click_on_icon_cow_btn:(UIButton *)sender
{
    NSLog(@"icon_cow_BtnClick:");
    
}
-(void)click_on_fronce_btn:(UIButton *)sender
{
    NSLog(@"btn_img_on_more_filters_click:");
    
}
-(void)click_on_take_out_btn:(UIButton *)sender
{
    NSLog(@"btn_chef_menu_click:");
}
-(void)click_on_icon_delivery_btn:(UIButton *)sender
{
    NSLog(@"btn_chef_menu_click:");
}

-(void)click_on_seving_now_btn:(UIButton *)sender
{
    NSLog(@"btn_serving_now_click:");
}
-(void)click_on_icon_chef_menu_btn:(UIButton *)sender
{
    NSLog(@"btn_chef_menu_click:");
}
-(void)click_on_icon_thumb_btn:(UIButton *)sender
{
    NSLog(@"btn_chef_menu_click:");
}


-(void)btn_on_my_favorites_click:(UIButton *)sender
{
    NSLog(@"btn_on_my_favorites_click");
    img_strip1.hidden = NO;
    img_strip2.hidden = YES;
    
    view_for_my_favorites.hidden = NO;
    view_for_all.hidden = YES;
    
}
-(void)btn_all_click:(UIButton *)sender
{
    NSLog(@"btn_all_click");
    img_strip1.hidden = YES;
    img_strip2.hidden = NO;
    
    view_for_all.hidden = NO;
    view_for_my_favorites.hidden = YES;
    
    
    
}
-(void)btn_on_left_arrow_click:(UIButton *)sender
{
    NSLog(@"btn_on_left_arrow_click:");
    
}
-(void)btn_on_right_arrow_click:(UIButton *)sender
{
    NSLog(@"btn_on_right_arrow2_click:");
    
}
//-(void)btn_on_left_arrow2_click:(UIButton *)sender
//{
//    NSLog(@"btn_on_left_arrow_click:");
//    
//}
//-(void)btn_on_right_arrow2_click:(UIButton *)sender
//{
//    NSLog(@"btn_on_right_arrow2_click:");
//    
//}
//-(void)btn_on_left_arrow3_click:(UIButton *)sender
//{
//    NSLog(@"btn_on_left_arrow3_click:");
//    
//}
//-(void)btn_on_right_arrow3_click:(UIButton *)sender
//{
//    NSLog(@"btn_on_right_arrow3_click:");
//    
//}



#pragma return action

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
