//
//  Header.h
//  Not86
//
//  Created by Interwld on 8/4/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#ifndef Not86_Header_h
#define Not86_Header_h



#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

//#define kFontbold @"century gothic bold"
//Helvetica LT Black.ttf



//#define KFontBlack @"HelveticaNeue-CondensedBlack"
#define KFontSizeHTML UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? @"22px" : @"20px"
#define kColorGreen [UIColor colorWithRed:107.0/255.0 green:196.0/255.0 blue:13.0/255.0 alpha:1.0]
#define kColorGrayTextfield [UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:36.0/255.0 alpha:1.0]

#define HEIGHT    self.view.frame.size.height
#define WIDTH     self.view.frame.size.width
#define HEADER_HEIGHT 40


#define kBaseUrl                    @"http://sgappstore.com/not86/"


#define kUserLogin                  @"webservices/user-login.json"

#define kFacebookLogin                 @"/webservices/fb-login.json"
#define kUserRegistration              @"w1/user-register.json"
#define kForgetPassword                @"webservices/forget_password.json"
#define KChefSignUp                    @"webservices/user-register.json"
#define AFChefSignUpFirst              @"/webservices/add-chef-profile.json"
#define AFChefSigUpHcountry            @"webservices/get_countrylist.json"
#define AFChefSigUpHcity               @"webservices/get_citylist.json"
#define AFChefSigUpCuisineCategoryList @"webservices/cuisine-category.json"
#define AFChefSigUpCuisineList         @"webservices/cuisine-list.json"


#define kCenterEventList            @"w1/event-centre-listing.json"
#define KeventDetail                @"w1/event-detail.json"
#define Knewsbloglist               @"w1/news-blogs.json"
#define KLocateus                   @"w1/locateus-list.json"
#define klogout                     @"w1/logout-user.json"
#define KEventOpportunitiesList     @"w1/event-opportunity.json"
#define kviewprofile                @"w1/view-profile.json"
#define KCenterOpportunitiesList    @"w1/centre-opportunity.json"
#define kNotificationlist           @"w1/notifications-list.json"
#define kdeletenotification         @"w1/delete-notification.json"
#define kmissionandvision           @"w1/mission-vison.json"
#define KCenterDetail               @"w1/centre-detail.json"
#define ksocialuser                 @"w1/socialuser-check.json"
#define kaddvolunteer               @"w1/add-volunteer.json"
#define kScanQR                     @"w1/scan-eventqrcode.json"
#define kcenterlist                 @"w1/centre-listing.json"
#define kslecectevent               @"w1/submit-opportunity.json"
#define kslectcenter                @"w1/submit-opportunity.json"
#define kparticipate                @"w1/participants-user.json"
#define Keditvolunteer              @"w1/edit-volunteer.json"
#define Kdishlist                   @"webservices/dishes-list.json"

//user profile
#define kUserProfileInfo                @"webservices/view-profile.json"
#endif