//
//  UserSignUpVC.m
//  Not86
//
//  Created by Admin on 29/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "UserSignUpVC.h"
#import "Define.h"
//#import "SignupViewController.h"
//#import "UserRegitrationStep1VC.h"
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"


@interface UserSignUpVC ()<UITextFieldDelegate>
{
    UIImageView * img_header;
    UIImageView *img_background;
    CGFloat	animatedDistance;
}

@end

@implementation UserSignUpVC


static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBodyDesign];
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}


-(void)integrateHeader
{
    img_header=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH,45)];
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,15,15,15);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back addTarget:self action:@selector(btn_back_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_back];
    
    UILabel *lbl_Login = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(icon_back.frame)+30,0, 100, 45)];
    lbl_Login.text = @"Sign Up";
    lbl_Login.font = [UIFont fontWithName:kFont size:18];
    lbl_Login.textColor = [UIColor whiteColor];
    lbl_Login.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_Login];
    
    UIImageView *img_circle = [[UIImageView alloc]init];
    img_circle.frame =CGRectMake(WIDTH-40, 8, 30, 30);
    [img_circle setImage:[UIImage imageNamed:@"img_user_icon@2x.png"]];
    //img_circle.backgroundColor = [UIColor whiteColor];
    [img_circle setUserInteractionEnabled:YES];
    [img_header addSubview:img_circle];
    
    
}

-(void)integrateBodyDesign
{
    img_background = [[UIImageView alloc]init];
    //  img_background.frame = CGRectMake(30, CGRectGetMaxY(img_header.frame)+30, WIDTH-50, 85);
    [img_background setImage:[UIImage imageNamed:@"img_logo_name@2x.png"]];
    //    img_eat.backgroundColor = [UIColor redColor];
    [img_background setUserInteractionEnabled:YES];
    [self.view addSubview:img_background];
    
    
    
    UITextField *txt_user_Name = [[UITextField alloc] init];
    //   txt_user_Name .frame=CGRectMake(10, CGRectGetMaxY( img_background.frame)-20, WIDTH, 30);
    txt_user_Name .borderStyle = UITextBorderStyleNone;
    txt_user_Name .textColor = [UIColor blackColor];
    txt_user_Name .font = [UIFont fontWithName:kFont size:13];
    txt_user_Name .placeholder = @"Username";
    [txt_user_Name  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_user_Name  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_user_Name .leftView = padding1;
    txt_user_Name .leftViewMode = UITextFieldViewModeAlways;
    txt_user_Name .userInteractionEnabled=YES;
    txt_user_Name .textAlignment = NSTextAlignmentLeft;
    txt_user_Name .backgroundColor = [UIColor clearColor];
    txt_user_Name .keyboardType = UIKeyboardTypeAlphabet;
    txt_user_Name .delegate = self;
    [self.view addSubview:txt_user_Name ];
    
    UIImageView * img_line = [[UIImageView alloc]init];
    //   img_line .frame = CGRectMake(10, CGRectGetMaxY(  txt_user_Name.frame)+5, WIDTH-60, 0.5);
    [img_line setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_line setUserInteractionEnabled:YES];
    [self.view addSubview:img_line];
    
    UITextField *txt_password = [[UITextField alloc] init];
    //    txt_password .frame=CGRectMake(10, CGRectGetMaxY( img_line.frame)+10, WIDTH, 30);
    txt_password .borderStyle = UITextBorderStyleNone;
    txt_password .textColor = [UIColor blackColor];
    txt_password .font = [UIFont fontWithName:kFont size:13];
    txt_password .placeholder = @"Password";
    [txt_password  setValue:[UIFont fontWithName:kFont size:16] forKeyPath:@"_placeholderLabel.font"];
    [txt_password  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_password .leftView = padding2;
    txt_password .leftViewMode = UITextFieldViewModeAlways;
    txt_password .userInteractionEnabled=YES;
    txt_password .textAlignment = NSTextAlignmentLeft;
    txt_password .backgroundColor = [UIColor clearColor];
    txt_password .keyboardType = UIKeyboardTypeAlphabet;
    txt_password .delegate = self;
    [self.view addSubview:txt_password ];
    
    UIImageView * img_cross = [[UIImageView alloc]init];
    //   img_cross .frame = CGRectMake(235,CGRectGetMaxY(img_line.frame)+15, 25, 25);
    [img_cross setImage:[UIImage imageNamed:@"icon-cross@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_cross setUserInteractionEnabled:YES];
    [self.view addSubview:img_cross];
    
    UIImageView * img_line1 = [[UIImageView alloc]init];
    //  img_line1 .frame = CGRectMake(10, CGRectGetMaxY(  txt_password.frame)+5, WIDTH-60, 0.5);
    [img_line1 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_line1 setUserInteractionEnabled:YES];
    [self.view addSubview:img_line1];
    
    
    UITextField *txt_conform_password = [[UITextField alloc] init];
    //    txt_conform_password .frame=CGRectMake(10, CGRectGetMaxY( img_line1.frame)+10, WIDTH, 30);
    txt_conform_password .borderStyle = UITextBorderStyleNone;
    txt_conform_password .textColor = [UIColor blackColor];
    txt_conform_password .font = [UIFont fontWithName:kFont size:13];
    txt_conform_password .placeholder = @"Confirm Password";
    [txt_conform_password  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_conform_password   setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_conform_password .leftView = padding3;
    txt_conform_password .leftViewMode = UITextFieldViewModeAlways;
    txt_conform_password .userInteractionEnabled=YES;
    txt_conform_password .textAlignment = NSTextAlignmentLeft;
    txt_conform_password .backgroundColor = [UIColor clearColor];
    txt_conform_password .keyboardType = UIKeyboardTypeAlphabet;
    txt_conform_password .delegate = self;
    [self.view addSubview:txt_conform_password ];
    
    UIImageView * img_currect = [[UIImageView alloc]init];
    //   img_currect .frame = CGRectMake(235,CGRectGetMaxY(img_line1.frame)+15, 25, 25);
    [img_currect setImage:[UIImage imageNamed:@"icon-currect@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_currect setUserInteractionEnabled:YES];
    [self.view addSubview:img_currect];
    
    
    UIImageView * img_line2 = [[UIImageView alloc]init];
    //    img_line2 .frame = CGRectMake(10, CGRectGetMaxY(  txt_conform_password.frame)+5, WIDTH-60, 0.5);
    [img_line2 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_line2 setUserInteractionEnabled:YES];
    [self.view addSubview:img_line2];
    
    UIButton *btn_sign_up = [UIButton buttonWithType:UIButtonTypeCustom];
    //    btn_sign_up.frame = CGRectMake(10,CGRectGetMaxY(  img_line2.frame)+30,WIDTH-55,50);
    btn_sign_up .backgroundColor = [UIColor clearColor];
    [btn_sign_up setImage:[UIImage imageNamed:@"sign-up@2x.png"] forState:UIControlStateNormal];
    
    [btn_sign_up addTarget:self action:@selector(btn_sign_up_click:) forControlEvents:UIControlEventTouchUpInside];
    btn_sign_up.userInteractionEnabled = YES;
    [self.view   addSubview:btn_sign_up];
    
    
    
    if (IS_IPHONE_6Plus)
    {
        img_background.frame = CGRectMake(40, CGRectGetMaxY(img_header.frame)+30, WIDTH-130, 85);
        txt_user_Name .frame=CGRectMake(35, CGRectGetMaxY( img_background.frame)+50, WIDTH-40, 30);
        img_line .frame = CGRectMake(40, CGRectGetMaxY(  txt_user_Name.frame)+5, WIDTH-80, 0.5);
        txt_password .frame=CGRectMake(35, CGRectGetMaxY( img_line.frame)+20, WIDTH-40, 30);
        img_cross .frame = CGRectMake(340,CGRectGetMaxY(img_line.frame)+15, 25, 25);
        img_line1 .frame = CGRectMake(40, CGRectGetMaxY(  txt_password.frame)+5, WIDTH-80, 0.5);
        txt_conform_password .frame=CGRectMake(35, CGRectGetMaxY( img_line1.frame)+20, WIDTH-40, 30);
        img_currect .frame = CGRectMake(340,CGRectGetMaxY(img_line1.frame)+15, 25, 25);
        img_line2 .frame = CGRectMake(40, CGRectGetMaxY(  txt_conform_password.frame)+5, WIDTH-80, 0.5);
        img_line2 .frame = CGRectMake(40, CGRectGetMaxY(  txt_conform_password.frame)+5, WIDTH-80, 0.5);
        btn_sign_up.frame = CGRectMake(37,CGRectGetMaxY(  img_line2.frame)+35,WIDTH-74,55);
        
    }
    else if (IS_IPHONE_6)
    {
        img_background.frame = CGRectMake(30, CGRectGetMaxY(img_header.frame)+30, WIDTH-80, 90);
        txt_user_Name .frame=CGRectMake(35, CGRectGetMaxY( img_background.frame)+50, WIDTH-40, 30);
        img_line .frame = CGRectMake(40, CGRectGetMaxY(  txt_user_Name.frame)+5, WIDTH-80, 0.5);
        txt_password .frame=CGRectMake(35, CGRectGetMaxY( img_line.frame)+20, WIDTH-40, 30);
        img_cross .frame = CGRectMake(300,CGRectGetMaxY(img_line.frame)+15, 25, 25);
        img_line1 .frame = CGRectMake(40, CGRectGetMaxY(  txt_password.frame)+5, WIDTH-80, 0.5);
        txt_conform_password .frame=CGRectMake(35, CGRectGetMaxY( img_line1.frame)+20, WIDTH-40, 30);
        img_currect .frame = CGRectMake(300,CGRectGetMaxY(img_line1.frame)+15, 25, 25);
        img_line2 .frame = CGRectMake(40, CGRectGetMaxY(  txt_conform_password.frame)+5, WIDTH-80, 0.5);
        img_line2 .frame = CGRectMake(40, CGRectGetMaxY(  txt_conform_password.frame)+5, WIDTH-80, 0.5);
        btn_sign_up.frame = CGRectMake(37,CGRectGetMaxY(  img_line2.frame)+35,WIDTH-74,55);
        
        
    }
    else
    {
        img_background.frame = CGRectMake(30, CGRectGetMaxY(img_header.frame)+30, WIDTH-80, 70);
        txt_user_Name .frame=CGRectMake(35, CGRectGetMaxY( img_background.frame)+50, WIDTH-40, 30);
        img_line .frame = CGRectMake(40, CGRectGetMaxY(  txt_user_Name.frame)+5, WIDTH-80, 0.5);
        txt_password .frame=CGRectMake(35, CGRectGetMaxY( img_line.frame)+20, WIDTH-40, 30);
        img_cross .frame = CGRectMake(250,CGRectGetMaxY(img_line.frame)+18, 25, 25);
        img_line1 .frame = CGRectMake(40, CGRectGetMaxY(  txt_password.frame)+5, WIDTH-80, 0.5);
        txt_conform_password .frame=CGRectMake(35, CGRectGetMaxY( img_line1.frame)+20, WIDTH-40, 30);
        img_currect .frame = CGRectMake(250,CGRectGetMaxY(img_line1.frame)+18, 25, 25);
        img_line2 .frame = CGRectMake(40, CGRectGetMaxY(  txt_conform_password.frame)+5, WIDTH-80, 0.5);
        img_line2 .frame = CGRectMake(40, CGRectGetMaxY(  txt_conform_password.frame)+5, WIDTH-80, 0.5);
        btn_sign_up.frame = CGRectMake(37,CGRectGetMaxY(  img_line2.frame)+35,WIDTH-74,50);
        
        
    }
    
}

#pragma mark Click Events

-(void)btn_back_click:(UIButton *)sender
{
    NSLog(@"btn_back_click:");
    [self.navigationController popViewControllerAnimated:NO];
    
    
}
-(void)btn_sign_up_click:(UIButton *)sender
{
    NSLog(@"btn_back_click:");
    //    UserRegitrationStep1VC*vc = [[UserRegitrationStep1VC alloc]init];
    //    [self.navigationController pushViewController:vc animated:NO];
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}

#pragma rerutrn event code

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
