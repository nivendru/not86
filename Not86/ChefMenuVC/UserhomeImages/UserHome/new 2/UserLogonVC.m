//
//  UserLogonVC.m
//  
//
//  Created by Admin on 29/08/15.
//
//
//6+-icon-signup@2x.png
#import "UserLogonVC.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"


@interface UserLogonVC ()
{
    UIImageView *img_header;
    UIImageView *img_topbar;
    NSMutableArray *ary_SocialInfo;
    
    NSString*str_fblogin;
    AppDelegate *delegate;
    
    UIImageView * img_background;
    
    CGFloat	animatedDistance;
}

@end

@implementation UserLogonVC


static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

- (void)viewDidLoad {
    [super viewDidLoad];
    ary_SocialInfo = [NSMutableArray new];
    str_fblogin = [NSString new];
    [self integrateHeader];
    [self integrateBody];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)integrateHeader
{
    img_background = [[UIImageView alloc]init];
    img_background .frame = CGRectMake(0, 0, WIDTH, HEIGHT);
    [img_background setImage:[UIImage imageNamed:@"img_background@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_background setUserInteractionEnabled:YES];
    [self.view addSubview:img_background];
    
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [ self.view addSubview:img_header];
    
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,9,28,28);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back addTarget:self action:@selector(btn_back_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_back];
    
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_back.frame)+30,0, 150, 45);
    lbl_User_Sign_Up.text = @"Eat";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"img_logo@2x.png"]];
    // icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
    
}

-(void)integrateBody
{
    
    
    UIButton *btn_fb = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_fb.frame = CGRectMake(45,CGRectGetMaxY( img_header.frame)+310,60,60);
    btn_fb.backgroundColor = [UIColor clearColor];
    [btn_fb addTarget:self action:@selector(btn_fb_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_fb setImage:[UIImage imageNamed:@"img_facebook@2x.png"] forState:UIControlStateNormal];
    [img_background  addSubview:btn_fb];
    
    UIButton *btn_google_pluse = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_google_pluse.frame = CGRectMake(207,CGRectGetMaxY( img_header.frame)+310,60,60);
    btn_google_pluse.backgroundColor = [UIColor clearColor];
    [btn_google_pluse addTarget:self action:@selector(btn_google_pluse_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_google_pluse setImage:[UIImage imageNamed:@"img_g plus@2x.png"] forState:UIControlStateNormal];
    [img_background  addSubview:btn_google_pluse];
    
    UIImageView * img_line = [[UIImageView alloc]init];
    //   img_line .frame = CGRectMake(157, CGRectGetMaxY( img_header.frame)+300, 0.5, 85);
    [img_line setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_line setUserInteractionEnabled:YES];
    [img_background addSubview:img_line];
    
    UIImageView * img_text_login = [[UIImageView alloc]init];
    img_text_login .frame = CGRectMake(10, CGRectGetMaxY( btn_fb.frame)+35, WIDTH-20,15);
    [img_text_login setImage:[UIImage imageNamed:@"6+-icon-siginor signup.png"]];
    img_text_login.backgroundColor = [UIColor clearColor];
    [img_text_login setUserInteractionEnabled:YES];
    [self.view addSubview:img_text_login];
    
    UIImageView * img_login = [[UIImageView alloc]init];
    img_login .frame = CGRectMake(10, CGRectGetMaxY( btn_fb.frame)+35, WIDTH-20,15);
    [img_login setImage:[UIImage imageNamed:@"img_login button@2x.png"]];
    img_login.backgroundColor = [UIColor clearColor];
    [img_login setUserInteractionEnabled:YES];
    [self.view addSubview:img_login];
    
    
    UIButton *btn_login = [UIButton buttonWithType:UIButtonTypeCustom];
    //btn_login.frame = CGRectMake(10,CGRectGetMaxY(  img_login.frame)+15,150,55);
    btn_login.backgroundColor = [UIColor clearColor];
    [btn_login addTarget:self action:@selector(btn_login_click:) forControlEvents:UIControlEventTouchUpInside];
    // [btn_login setImage:[UIImage imageNamed:@"btn-login2@2x.png"] forState:UIControlStateNormal];
    [self.view  addSubview:btn_login];
    
    UILabel *lbl_text = [[UILabel alloc]init];
    // lbl_text.frame = CGRectMake(20,CGRectGetMaxY(btn_login.frame), 150, 20);
    lbl_text.text = @"Sign in to your not86 account";
    lbl_text.font = [UIFont fontWithName:kFont size:10];
    lbl_text.textColor = [UIColor blackColor];
    lbl_text.backgroundColor = [UIColor clearColor];
    [img_background addSubview:lbl_text];
    
    
    UIImageView * img_sign_up = [[UIImageView alloc]init];
    img_sign_up .frame = CGRectMake(10, CGRectGetMaxY( btn_fb.frame)+35, WIDTH-20,15);
    [img_sign_up setImage:[UIImage imageNamed:@"signup@2x.png"]];
    img_sign_up.backgroundColor = [UIColor clearColor];
    [img_sign_up setUserInteractionEnabled:YES];
    [self.view addSubview:img_sign_up];
    
    UIButton *btn_sign_up = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_sign_up.frame = CGRectMake(CGRectGetMaxX(btn_login.frame)+3,CGRectGetMaxY(  img_login.frame)+15,150,55);
    btn_sign_up.backgroundColor = [UIColor clearColor];
    [btn_sign_up addTarget:self action:@selector(btn_sign_up_click:) forControlEvents:UIControlEventTouchUpInside];
    // [btn_sign_up setImage:[UIImage imageNamed:@"btn-signup2@2x.png"] forState:UIControlStateNormal];
    [self.view addSubview:btn_sign_up];
    
    UILabel *lbl_text1 = [[UILabel alloc]init];
    // lbl_text1.frame = CGRectMake(CGRectGetMaxX(lbl_text.frame),CGRectGetMaxY(btn_sign_up.frame), 150, 20);
    lbl_text1.text = @"Apply to become a not86 user";
    lbl_text1.font = [UIFont fontWithName:kFont size:10];
    lbl_text1.textColor = [UIColor blackColor];
    lbl_text1.backgroundColor = [UIColor clearColor];
    [img_background addSubview:lbl_text1];
    
    if (IS_IPHONE_6Plus)
    {
        [img_background setImage:[UIImage imageNamed:@"6+-img-bg@2x.png"]];
        [btn_fb setImage:[UIImage imageNamed:@"6+-icon-fb.png"] forState:UIControlStateNormal];
        [btn_google_pluse setImage:[UIImage imageNamed:@"6+-icon-G+.png"] forState:UIControlStateNormal];
        [img_text_login setImage:[UIImage imageNamed:@"6+-icon-siginor signup.png"]];
        [img_login setImage:[UIImage imageNamed:@"6+-login-img@2x.png"]];
        [img_sign_up setImage:[UIImage imageNamed:@"6+-icon-signup@2x.png"]];
        
        btn_fb.frame = CGRectMake(62,450,77,77);
        btn_google_pluse.frame = CGRectMake(267,450,77,77);
        img_line .frame = CGRectMake(205, 450, 1, 100);
        img_text_login .frame = CGRectMake(10, CGRectGetMaxY( btn_fb.frame)+50, WIDTH-20,20);
        
        img_login .frame = CGRectMake(10, CGRectGetMaxY( btn_fb.frame)+85, 190,70);
        btn_login.frame = CGRectMake(10,CGRectGetMaxY(  btn_fb.frame)+65,190,70);
        lbl_text.frame = CGRectMake(20,CGRectGetMaxY(btn_login.frame)+28, 200, 20);
        img_sign_up .frame = CGRectMake(215, CGRectGetMaxY( btn_fb.frame)+85, 190,70);
        btn_sign_up.frame = CGRectMake(215, CGRectGetMaxY( btn_fb.frame)+65, 190,70);
        lbl_text1.frame = CGRectMake(CGRectGetMaxX(lbl_text.frame),CGRectGetMaxY(btn_sign_up.frame)+28, 200, 20);
        
        lbl_text.font = [UIFont fontWithName:kFont size:12];
        lbl_text1.font = [UIFont fontWithName:kFont size:12];
        
    }
    else if (IS_IPHONE_6)
    {
        [img_background setImage:[UIImage imageNamed:@"6+-img-bg@2x.png"]];
        [btn_fb setImage:[UIImage imageNamed:@"6+-icon-fb.png"] forState:UIControlStateNormal];
        [btn_google_pluse setImage:[UIImage imageNamed:@"6+-icon-G+.png"] forState:UIControlStateNormal];
        [img_text_login setImage:[UIImage imageNamed:@"6+-icon-siginor signup.png"]];
        [img_login setImage:[UIImage imageNamed:@"6+-login-img@2x.png"]];
        [img_sign_up setImage:[UIImage imageNamed:@"6+-icon-signup@2x.png"]];
        
        btn_fb.frame = CGRectMake(58,400,68,77);
        btn_google_pluse.frame = CGRectMake(235,400,68,77);
        img_line .frame = CGRectMake(184, 400, 1, 100);
        img_text_login .frame = CGRectMake(10, CGRectGetMaxY( btn_fb.frame)+45, WIDTH-20,20);
        
        img_login .frame = CGRectMake(15, CGRectGetMaxY( btn_fb.frame)+80, 165,70);
        btn_login.frame = CGRectMake(15,CGRectGetMaxY(  btn_fb.frame)+80,165,70);
        lbl_text.frame = CGRectMake(24,CGRectGetMaxY(btn_login.frame)+5, 150, 20);
        img_sign_up .frame = CGRectMake(195, CGRectGetMaxY( btn_fb.frame)+80, 165,70);
        btn_sign_up.frame = CGRectMake(195, CGRectGetMaxY( btn_fb.frame)+80, 165,70);
        lbl_text1.frame = CGRectMake(CGRectGetMaxX(lbl_text.frame)+30,CGRectGetMaxY(btn_sign_up.frame)+5, 150, 20);
        
        
        
    }
    else if (IS_IPHONE_5)
    {
        btn_fb.frame = CGRectMake(45,IS_IPHONE_5?CGRectGetMaxY( img_header.frame)+310: CGRectGetMaxY( img_header.frame)+240,60,60);
        btn_google_pluse.frame = CGRectMake(207,IS_IPHONE_5? CGRectGetMaxY( img_header.frame)+310: CGRectGetMaxY( img_header.frame)+240,60,60);
        [img_login setImage:[UIImage imageNamed:@"6+-login-img@2x.png"]];
        img_line .frame = CGRectMake(IS_IPHONE_5?156:157,IS_IPHONE_5?350:290, 1, 73);
        [img_text_login setImage:[UIImage imageNamed:@"6+-icon-siginor signup.png"]];
        img_text_login .frame = CGRectMake(10, CGRectGetMaxY( btn_fb.frame)+30, WIDTH-20,15);
        
        img_login .frame = CGRectMake(15, CGRectGetMaxY(btn_fb.frame)+58, 140,55);
        btn_login.frame = CGRectMake(15,CGRectGetMaxY(btn_fb.frame)+58,140,55);
        lbl_text.frame = CGRectMake(19,CGRectGetMaxY(btn_login.frame)+6, 150, 20);
        img_sign_up .frame = CGRectMake(167, CGRectGetMaxY( btn_fb.frame)+58, 140,55);
        btn_sign_up.frame = CGRectMake(167, CGRectGetMaxY( btn_fb.frame)+58, 140,55);
        lbl_text1.frame = CGRectMake(CGRectGetMaxX(lbl_text.frame)+3,CGRectGetMaxY(btn_sign_up.frame)+6, 150, 18);
        
        lbl_text.font = [UIFont fontWithName:kFont size:9];
        lbl_text1.font = [UIFont fontWithName:kFont size:9];
        
        
    }
    else
    {
        btn_fb.frame = CGRectMake(45,IS_IPHONE_5?CGRectGetMaxY( img_header.frame)+310: CGRectGetMaxY( img_header.frame)+240,60,60);
        btn_google_pluse.frame = CGRectMake(207,IS_IPHONE_5? CGRectGetMaxY( img_header.frame)+310: CGRectGetMaxY( img_header.frame)+240,60,60);
        [img_login setImage:[UIImage imageNamed:@"6+-login-img@2x.png"]];
        img_line .frame = CGRectMake(IS_IPHONE_5?156:157,IS_IPHONE_5?350:290, 1, 73);
        [img_text_login setImage:[UIImage imageNamed:@"6+-icon-siginor signup.png"]];
        img_text_login .frame = CGRectMake(10, CGRectGetMaxY( btn_fb.frame)+30, WIDTH-20,15);
        
        img_login .frame = CGRectMake(15, CGRectGetMaxY(btn_fb.frame)+53, 140,55);
        btn_login.frame = CGRectMake(15,CGRectGetMaxY(btn_fb.frame)+53,140,55);
        lbl_text.frame = CGRectMake(19,CGRectGetMaxY(btn_login.frame)+3, 150, 20);
        img_sign_up .frame = CGRectMake(167, CGRectGetMaxY( btn_fb.frame)+53, 140,55);
        btn_sign_up.frame = CGRectMake(167, CGRectGetMaxY( btn_fb.frame)+53, 140,55);
        lbl_text1.frame = CGRectMake(CGRectGetMaxX(lbl_text.frame)+3,CGRectGetMaxY(btn_sign_up.frame)+3, 150, 18);
        
        lbl_text.font = [UIFont fontWithName:kFont size:9];
        lbl_text1.font = [UIFont fontWithName:kFont size:9];
    }
}
-(void)btn_fb_click:(UIButton *)sender
{
    NSLog(@"img_fb_click Btn Click");
    [self.navigationController popViewControllerAnimated:NO];
    
}
-(void)btn_google_pluse_click:(UIButton *)sender
{
    NSLog(@"img_btn_google_pluse_click:");
    
}
-(void)btn_login_click:(UIButton *)sender
{
    NSLog(@"btn_login_click:");
//    UserLoginVC *vc=[[UserLoginVC alloc]init];
//    [self.navigationController pushViewController:vc animated:NO];
}
-(void)btn_sign_up_click:(UIButton *)sender
{
    NSLog(@"btn_sign_up_click:");
//    UserSignUpVC *vc=[[UserSignUpVC alloc] init];
//    [self.navigationController pushViewController:vc animated:NO];
//    
}


-(void)btn_back_click:(UIButton *)sender
{
    NSLog(@"btn_back_click:");
    
}

-(void)Click_fbbtn
{
    
    
}
-(void)Click_googlebtn
{
    
    
    
}
-(void)Click_loginbtn
{
//    UserLoginVC *vc=[[UserLoginVC alloc]init];
//    [self.navigationController pushViewController:vc animated:NO];
    //
}
-(void)Click_signupbtn
{
//    UserSignUpVC *vc=[[UserSignUpVC alloc] init];
//    [self.navigationController pushViewController:vc animated:NO];
}


#pragma rerutrn event code

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
