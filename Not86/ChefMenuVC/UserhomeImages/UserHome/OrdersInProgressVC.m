//
//  OrdersInProgressVC.m
//  Not86
//
//  Created by Admin on 30/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "OrdersInProgressVC.h"
#import "Define.h"


#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"


@interface OrdersInProgressVC ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate>
{
    UITableView*img_table;
    UIImageView * img_header;
    UIScrollView *scroll;
    UIView  *view_for_my_orders;
    
    NSMutableArray * array_requested_on;
    NSMutableArray * array_Request_date_time;
    NSMutableArray * array_dish_imges;
    NSMutableArray * array_order_no;
    
}

@end

@implementation OrdersInProgressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBodyDesign];
    array_requested_on = [[NSMutableArray alloc]initWithObjects:@"Requestd on:", nil];
    array_Request_date_time = [[NSMutableArray alloc]initWithObjects:@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015,4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM", nil];
    array_dish_imges = [[NSMutableArray alloc]initWithObjects:@"img-dish4@2x.png",@"img-dish2@2x.png",@"img-dish1@2x.png",@"img-dish4@2x.png",@"img-dish2@2x.png", nil];
    array_order_no = [[NSMutableArray alloc]initWithObjects:@"10847",@"10847",@"10847",@"10847",@"10847", nil];
    
    
}

-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu.frame = CGRectMake(10,13,20,20);
    icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    [icon_menu addTarget:self action:@selector(btn_menu_click:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:icon_menu];
    
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 200, 45);
    lbl_User_Sign_Up.text = @"Orders In Progress";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    UIButton *icon_drop_down = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_drop_down.frame = CGRectMake(CGRectGetMaxX(lbl_User_Sign_Up.frame)-20,15,20,20);
    icon_drop_down .backgroundColor = [UIColor clearColor];
    [icon_drop_down addTarget:self action:@selector(btn_dropdown_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_drop_down setImage:[UIImage imageNamed:@"img-white-dropd-@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_drop_down];
    
    UIButton *btn_on_order_on_request = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_order_on_request.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0,215,45);
    btn_on_order_on_request .backgroundColor = [UIColor clearColor];
    [btn_on_order_on_request addTarget:self action:@selector(btn_on_orders_on_request_click:) forControlEvents:UIControlEventTouchUpInside];
    //[icon_drop_down setImage:[UIImage imageNamed:@"drop-down@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:btn_on_order_on_request];
    
    
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}

-(void)integrateBodyDesign
{
    
    view_for_my_orders = [[UIView alloc]init];
    view_for_my_orders.frame = CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+90);
    view_for_my_orders.backgroundColor=[UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [view_for_my_orders setUserInteractionEnabled:YES];
    [self.view addSubview:  view_for_my_orders];
    
    UILabel *lbl_date = [[UILabel alloc]init];
    lbl_date.frame = CGRectMake(WIDTH/2-40,0, 100, 45);
    lbl_date.text = @"Date";
    lbl_date.font = [UIFont fontWithName:kFontBold size:18];
    lbl_date.textColor = [UIColor blackColor];
    lbl_date.backgroundColor = [UIColor clearColor];
    [view_for_my_orders addSubview:lbl_date];
    
    UIImageView *bg_for_date = [[UIImageView alloc]init];
    bg_for_date.frame = CGRectMake(-5, CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
    [bg_for_date setUserInteractionEnabled:YES];
    bg_for_date.image=[UIImage imageNamed:@"bg@2x.png"];
    [view_for_my_orders addSubview:bg_for_date];
    
    UILabel *lbl_from = [[UILabel alloc]init];
    lbl_from.frame = CGRectMake(30,0, 100, 45);
    lbl_from.text = @"From:";
    lbl_from.font = [UIFont fontWithName:kFont size:15];
    lbl_from.textColor = [UIColor blackColor];
    lbl_from.backgroundColor = [UIColor clearColor];
    [bg_for_date addSubview:lbl_from];
    
    UIImageView *line_img = [[UIImageView alloc]init];
    line_img.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 35,110, 0.5);
    [line_img setUserInteractionEnabled:YES];
    line_img.image=[UIImage imageNamed:@"line1@2x.png"];
    [bg_for_date addSubview:line_img];
    
    UIImageView *img_calender = [[UIImageView alloc]init];
    img_calender.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+85,9.5,25, 25);
    [img_calender setUserInteractionEnabled:YES];
    img_calender.image=[UIImage imageNamed:@"img-calender@2x.png"];
    [bg_for_date addSubview:img_calender];
    
    UILabel *lbl_to = [[UILabel alloc]init];
    lbl_to.frame = CGRectMake(CGRectGetMidX(img_calender.frame)+30,0, 100, 45);
    lbl_to.text = @"to";
    lbl_to.font = [UIFont fontWithName:kFont size:15];
    lbl_to.textColor = [UIColor blackColor];
    lbl_to.backgroundColor = [UIColor clearColor];
    [bg_for_date addSubview:lbl_to];
    
    UIImageView *line_img2 = [[UIImageView alloc]init];
    line_img2.frame = CGRectMake(CGRectGetMidX(lbl_to.frame)-20, 35,110, 0.5);
    [line_img2 setUserInteractionEnabled:YES];
    line_img2.image=[UIImage imageNamed:@"line1@2x.png"];
    [bg_for_date addSubview:line_img2];
    
    UIImageView *img_calender2 = [[UIImageView alloc]init];
    img_calender2.frame = CGRectMake(CGRectGetMidX(lbl_to.frame)+65,9.5,25, 25);
    [img_calender2 setUserInteractionEnabled:YES];
    img_calender2.image=[UIImage imageNamed:@"img-calender@2x.png"];
    [bg_for_date addSubview:img_calender2];
    
    img_table = [[UITableView alloc] init ];
    img_table.frame = CGRectMake(5, CGRectGetMaxY(bg_for_date.frame)+5, WIDTH-10, HEIGHT-150);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    img_table.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [view_for_my_orders addSubview:img_table];
    
    
    
    
    
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 142;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    img_cellBackGnd.frame =  CGRectMake(-3,0, WIDTH+10, 135);
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
    [img_cellBackGnd  setContentMode:UIViewContentModeScaleAspectFill];
    [img_cellBackGnd  setClipsToBounds:YES];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    
    [cell.contentView addSubview:img_cellBackGnd];
    
    UILabel *text_order_no = [[UILabel alloc]init];
    text_order_no.frame = CGRectMake(19,-5, 200, 45);
    text_order_no.text = @"Order no.:";
    text_order_no.font = [UIFont fontWithName:kFont size:15];
    text_order_no.textColor = [UIColor blackColor];
    text_order_no.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:text_order_no];
    
    UILabel *order_no_value = [[UILabel alloc]init];
    order_no_value.frame = CGRectMake(CGRectGetMidX(text_order_no.frame)-23,-4, 200, 45);
    order_no_value.text =[NSString stringWithFormat:@"%@",[ array_order_no objectAtIndex:indexPath.row]];
    order_no_value.font = [UIFont fontWithName:kFontBold size:13];
    order_no_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    order_no_value.textAlignment = NSTextAlignmentLeft;
    order_no_value.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:order_no_value];
    
    
    
    UILabel *lbl_requeste_on = [[UILabel alloc]init];
    lbl_requeste_on.frame = CGRectMake(30,CGRectGetMidY(text_order_no.frame)-5, 200, 45);
    lbl_requeste_on.text = @"Serving Time:";
    lbl_requeste_on.font = [UIFont fontWithName:kFont size:10];
    lbl_requeste_on.textColor = [UIColor blackColor];
    lbl_requeste_on.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_requeste_on];
    
    UILabel *lbl_date_time = [[UILabel alloc]init];
    lbl_date_time.frame = CGRectMake(CGRectGetMaxY(text_order_no.frame)+55,12, 200, 45);
    lbl_date_time.text =[NSString stringWithFormat:@"%@",[array_Request_date_time objectAtIndex:indexPath.row]];
    lbl_date_time.font = [UIFont fontWithName:kFont size:10];
    lbl_date_time.textColor = [UIColor blackColor];
    lbl_date_time.textAlignment = NSTextAlignmentLeft;
    lbl_date_time.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_date_time];
    
    UILabel *lbl_status = [[UILabel alloc]init];
    lbl_status.frame = CGRectMake(61,CGRectGetMidY(lbl_date_time.frame)-5, 100, 45);
    lbl_status.text = @"Status:";
    lbl_status.font = [UIFont fontWithName:kFont size:10];
    lbl_status.textColor = [UIColor blackColor];
    lbl_status.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_status];
    
    UILabel *lbl_on_request = [[UILabel alloc]init];
    lbl_on_request.frame = CGRectMake(CGRectGetMidX(lbl_status.frame)-13,CGRectGetMidY(lbl_date_time.frame)-5, 100, 45);
    lbl_on_request.text = @"In Progress";
    lbl_on_request.font = [UIFont fontWithName:kFont size:10];
    lbl_on_request.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    lbl_on_request.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_on_request];
    
    UIImageView *img_left_arrow = [[UIImageView alloc]init];
    img_left_arrow.frame = CGRectMake(11,95,8,11);
    [img_left_arrow setImage:[UIImage imageNamed:@"left-arrow@2x.png"]];
    img_left_arrow.backgroundColor = [UIColor clearColor];
    [img_left_arrow setUserInteractionEnabled:YES];
    [img_cellBackGnd addSubview:img_left_arrow];
    
    UIButton *btn_on_left_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_left_arrow.frame = CGRectMake(4,59, 14,75);
    btn_on_left_arrow .backgroundColor = [UIColor clearColor];
    [btn_on_left_arrow setUserInteractionEnabled:YES];
    [btn_on_left_arrow addTarget:self action:@selector(btn_on_left_arrow_click:)forControlEvents:UIControlEventTouchUpInside];
    [img_cellBackGnd addSubview:btn_on_left_arrow];
    
    UIImageView *img_right_arrow = [[UIImageView alloc]init];
    img_right_arrow.frame = CGRectMake(391,95, 8,11);
    [img_right_arrow setImage:[UIImage imageNamed:@"right-arrow@2x.png"]];
    img_right_arrow.backgroundColor = [UIColor clearColor];
    [img_right_arrow setUserInteractionEnabled:YES];
    [img_cellBackGnd addSubview:img_right_arrow];
    
    UIButton *btn_on_right_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_right_arrow.frame = CGRectMake(382,59,14, 75);
    btn_on_right_arrow .backgroundColor = [UIColor clearColor];
    [btn_on_right_arrow setUserInteractionEnabled:YES];
    [btn_on_right_arrow addTarget:self action:@selector(btn_on_right_arrow_click:)forControlEvents:UIControlEventTouchUpInside];
    [img_cellBackGnd addSubview:btn_on_right_arrow];
    
    
    
    UICollectionViewFlowLayout *layout;
    UICollectionView *collView_serviceDirectory;
    
    
    layout=[[UICollectionViewFlowLayout alloc] init];
    collView_serviceDirectory = [[UICollectionView alloc] initWithFrame:CGRectMake(19,77,WIDTH-48,50)
                                                   collectionViewLayout:layout];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_serviceDirectory setDataSource:self];
    [collView_serviceDirectory setDelegate:self];
    collView_serviceDirectory.scrollEnabled = YES;
    collView_serviceDirectory.showsVerticalScrollIndicator = NO;
    collView_serviceDirectory.showsHorizontalScrollIndicator = NO;
    collView_serviceDirectory.pagingEnabled = NO;
    [collView_serviceDirectory registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_serviceDirectory setBackgroundColor:[UIColor clearColor]];
    layout.minimumInteritemSpacing = 2;
    layout.minimumLineSpacing = 0;
    collView_serviceDirectory.userInteractionEnabled = YES;
    [cell.contentView addSubview:collView_serviceDirectory];
    
    
    
    if (IS_IPHONE_6Plus)
    {
        
        
        
    }
    else if (IS_IPHONE_6)
    {
        
    }
    else
    {
        
        
        
    }
    
    
    
    /* UIImageView *img_chef = [[UIImageView alloc]init];
     img_chef .frame = CGRectMake(10,CGRectGetMaxY(text_food_details.frame)+13, 50, 50);
     [img_chef  setImage:[UIImage imageNamed:@"img-chef@2x.png"]];
     //   icon_user.backgroundColor = [UIColor redColor];
     [img_chef  setUserInteractionEnabled:YES];
     [img_cellBackGnd addSubview:img_chef ];*/
    return cell;
    
    
}


//table complete

#pragma mark UiCollectionView Delegate Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 10;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView1 dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    UIImageView *img_backGnd = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    img_backGnd.backgroundColor = [UIColor clearColor];
    [cell.contentView addSubview:img_backGnd];
    
    
    UIImageView *img_Images = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 50,50)];
    //    NSString *ImagePath1 =  [[NSString stringWithFormat:@"%@",[[ary_CatagoryList objectAtIndex:indexPath.row] valueForKey:@"cat_img"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //    [img_Images setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@"img_merchantPlaceholder@2x.png"]];
    [img_Images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",@"img_h_b_ground@2x .png"]]];
    [img_Images setUserInteractionEnabled:YES];
    [img_Images setContentMode:UIViewContentModeScaleAspectFill];
    [img_Images setClipsToBounds:YES];
    [img_Images setUserInteractionEnabled:YES];
    [img_backGnd addSubview:img_Images];
    
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((WIDTH-140)/5, 50);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
}
#pragma buttne actions

-(void)btn_menu_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click");
    [self.navigationController popViewControllerAnimated:NO];
    
    
}
-(void)btn_dropdown_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click");
    
    
}
-(void)btn_on_orders_on_request_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click");
    
    
}
-(void)btn_on_left_arrow_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click");
    
    
}
-(void)btn_on_right_arrow_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click");
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
