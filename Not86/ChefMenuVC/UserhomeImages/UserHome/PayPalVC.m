
//
//  PayPalVC.m
//  LocoGalleria
//
//  Created by User9 on 09/07/14.
//  Copyright (c) 2014 User1. All rights reserved.
//

#import "PayPalVC.h"
#import "JWSlideMenuController.h"
#import "Define.h"
#import "AppDelegate.h"
#import "Define.h"
#import "JWSlideMenuViewController.h"
#define kPayPalEnvironment PayPalEnvironmentSandbox

@interface PayPalVC ()
{
    AppDelegate *delegate;
    
    UIView *alertviewBg;
    BOOL bool_Pay;
    
}

@end

@implementation PayPalVC
@synthesize environment,acceptCreditCards,resultText;
@synthesize str_CartInfo;
@synthesize  str_Price,str_SERVINGTYPE;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    bool_Pay = NO;
    
    self.title = @"PayPal SDK Demo";
    
    // Set up payPalConfig
    _payPalConfig = [[PayPalConfiguration alloc] init];
    _payPalConfig.acceptCreditCards = YES;
    _payPalConfig.languageOrLocale = @"en";
    _payPalConfig.merchantName = @"Awesome Shirts, Inc.";
    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
    
    // Setting the languageOrLocale property is optional.
    //
    // If you do not set languageOrLocale, then the PayPalPaymentViewController will present
    // its user interface according to the device's current language setting.
    //
    // Setting languageOrLocale to a particular language (e.g., @"es" for Spanish) or
    // locale (e.g., @"es_MX" for Mexican Spanish) forces the PayPalPaymentViewController
    // to use that language/locale.
    //
    // For full details, including a list of available languages and locales, see PayPalPaymentViewController.h.
    
    _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    
    // Do any additional setup after loading the view, typically from a nib.
    
    //self.successView.hidden = YES;
    
    // use default environment, should be Production in real life
    self.environment = kPayPalEnvironment;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    [self.navigationController.slideMenuController HideHomeButton];
    
    [PayPalMobile preconnectWithEnvironment:self.environment];
    
    if (!bool_Pay)
    {
        [self pay];
        
    }
    
}


-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}



//-(void)HideAlertView
//{
//    [alertviewBg removeFromSuperview];
//}

#pragma mark - Receive Single Payment

- (void)pay {
    // Remove our last completed payment, just for demo purposes.
    //self.resultText = nil;
    
    // Note: For purposes of illustration, this example shows a payment that includes
    //       both payment details (subtotal, shipping, tax) and multiple items.
    //       You would only specify these if appropriate to your situation.
    //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
    //       and simply set payment.amount to your total charge.
    
    
    // Optional: include payment details
    
    NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:str_Price];
    
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = shipping;
    payment.currencyCode = @"SGD";
    payment.shortDescription = @"Amount";
    payment.items = nil;  // if not including multiple items, then leave payment.items as nil
    payment.paymentDetails = nil; // if not including payment details, then leave payment.paymentDetails as nil
    
    if (!payment.processable) {
        // This particular payment will always be processable. If, for
        // example, the amount was negative or the shortDescription was
        // empty, this payment wouldn't be processable, and you'd want
        // to handle that here.
    }
    
    bool_Pay = YES;
    
    // Update payPalConfig re accepting credit cards.
    self.payPalConfig.acceptCreditCards = YES;
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                configuration:self.payPalConfig
                                                                                                     delegate:self];
    [self presentViewController:paymentViewController animated:YES completion:nil];
}

#pragma mark PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    NSLog(@"PayPal Payment Success!");
    self.resultText = [completedPayment description];
    
    [self sendCompletedPaymentToServer:completedPayment]; // Payment was processed successfully; send to server for verification and fulfillment
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    self.resultText = nil;
    
    
    [self dismissViewControllerAnimated:YES completion:^
     {
         [self dismissViewControllerAnimated:NO completion:nil];

     }];
    
}


#pragma mark Proof of payment validation

- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment {
    // TODO: Send completedPayment.confirmation to server
    
    //    if ([[[completedPayment.confirmation valueForKey:@"response"]valueForKey:@"state"] isEqualToString:@"apporved"])
    //    {
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setValue:[[completedPayment.confirmation valueForKey:@"response"]valueForKey:@"create_time"] forKey:@"order_time"];
    [dict setValue:[[completedPayment.confirmation valueForKey:@"response"]valueForKey:@"id"] forKey:@"transaction_id"];
    [dict setValue:[[completedPayment.confirmation valueForKey:@"response"]valueForKey:@"state"] forKey:@"payment_status"];
    [dict setValue:completedPayment.amount forKey:@"amount"];
    [dict setValue:completedPayment.currencyCode forKey:@"currency_type"];
    
    //    [self AFCheckOut:[[completedPayment.confirmation valueForKey:@"response"]valueForKey:@"id"]];
    
    [self AFCheckOut:dict];
    
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [alertviewBg removeFromSuperview];
}

#pragma mark AF

-(void) AFCheckOut:(NSDictionary *)paymentInfo
{
    self.view.userInteractionEnabled = NO;
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];

    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    //  parameter: uid,cartvalue(10:10:10:2009-10-11:2009-10-11|10:10:10:2009-10-11:2009-10-11) (pid:qty:subtotal:fromrent:torent), transaction_id, total
    NSDictionary *params;
    
    
    if ([str_SERVINGTYPE isEqualToString:@"3"])
    {
        params =@{
                  @"uid"              :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                  @"paypal_email"          :   [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Paypal_Account"],
                  @"transaction_id"          :   [paymentInfo valueForKey:@"transaction_id"],
                  @"sub_total"          :   str_Price,
                  @"payment_status"     : [paymentInfo valueForKey:@"payment_status"],
                  @"type"               : str_SERVINGTYPE,
                  @"user_description"     : [NSString stringWithFormat:@"%@",@"0"],
                  @"reason_request"     : [NSString stringWithFormat:@"%@",@"0"],
                  
                  
                  };
    }
    else{
        params =@{
                  @"uid"              :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                  @"paypal_email"          :   [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"Paypal_Account"],
                  @"transaction_id"          :   [paymentInfo valueForKey:@"transaction_id"],
                  @"sub_total"          :   str_Price,
                  @"payment_status"     : [paymentInfo valueForKey:@"payment_status"],
                  @"type"               : str_SERVINGTYPE,
                  };
    }
    
   
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:KCheckOut
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        self.view.userInteractionEnabled = YES;
        [delegate.activityIndicator stopAnimating];
        [self ResponseCheckOut:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         self.view.userInteractionEnabled = YES;
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         
                                         if ([[operation error] code] == -1009) {
                                             
                                             [self popup_Alertview:@"Please check your internet connection."];
                                         }
                                         else if ([[operation error] code] == -1001)
                                         {
                                             [self AFCheckOut:paymentInfo];
                                         }
                                     }];
    
    [operation start];
    
}

-(void) ResponseCheckOut :(NSDictionary * )TheDict
{
    
    self.view.userInteractionEnabled = YES;
    
    if (![TheDict isKindOfClass:[NSNull class]])
    {
        if([[TheDict valueForKey:@"error"] intValue] == 0)
        {
            
            
                           // [self.navigationController popAllChildClass];
                             [self.navigationController.slideMenuController ChangeToMenu:0];
                             [self.navigationController.slideMenuController toggleMenu];

//            NSMutableArray *ary_MyCard = [[NSMutableArray alloc]init];
//            
//            
//            for (int i=0; i<[[[NSUserDefaults standardUserDefaults]valueForKey:@"MyCard"] count]; i++)
//            {
//                if ([[[[[NSUserDefaults standardUserDefaults] valueForKey:@"MyCard"]objectAtIndex:i] valueForKey:@"UserID"] isEqualToString:[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"]])
//                {
//                    if ([[[[[NSUserDefaults standardUserDefaults]valueForKey:@"MyCard"] objectAtIndex:i]valueForKey:@"Approved"] intValue] == 1)
//                    {
//                        [ary_MyCard addObject:[[[NSUserDefaults standardUserDefaults]valueForKey:@"MyCard"] objectAtIndex:i]];
//                    }
//                }
//                else
//                {
//                    [ary_MyCard addObject:[[[NSUserDefaults standardUserDefaults]valueForKey:@"MyCard"] objectAtIndex:i]];
//                }
//            }
//            
//            
//            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"MyCard"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            
//            
//            [[NSUserDefaults standardUserDefaults] setObject:ary_MyCard  forKey:@"MyCard"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            
//            
//            [self dismissViewControllerAnimated:YES completion:^
//             {
//                 
//                 [self.navigationController popAllChildClass];
//                 [self.navigationController.slideMenuController ChangeToMenu:8];
//                 [self.navigationController.slideMenuController toggleMenu];
//             }];
            
        }
        
    }
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
