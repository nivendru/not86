//
//  UserSupportVC.m
//  Not86
//
//  Created by Admin on 02/09/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "UserSupportVC.h"
#import <Social/Social.h>
#import "AboutUS.h"
#import "Define.h"
#import "UserNotificationsVC.h"
#import "JWSlideMenuViewController.h"
#import "JWSlideMenuController.h"
#import "UserLogonVC.h"
#import "SplashScreensViewController.h"
#import "MGInstagram.h"
#import "iTellAFriend.h"


#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"


@interface UserSupportVC ()<UIScrollViewDelegate>
{
    UIImageView *img_header;
    UIScrollView *scroll;
    
}

@end

@implementation UserSupportVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.slideMenuController  HideHomeButton];
    
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [self integrateHeader];
    [self integrateBodyDesign];
}

-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [icon_back addTarget:self action:@selector(click_on_back_arrow:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:icon_back];
    
    
    UILabel *lbl_support_on_header = [[UILabel alloc]init];
    lbl_support_on_header.frame = CGRectMake(CGRectGetMaxX(icon_back .frame)+30,0, 300, 45);
    lbl_support_on_header.text = @"Support";
    lbl_support_on_header.font = [UIFont fontWithName:kFont size:20];
    lbl_support_on_header.textColor = [UIColor whiteColor];
    lbl_support_on_header.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_support_on_header];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}
-(void)integrateBodyDesign
{
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.frame = CGRectMake(8,46,WIDTH-16,HEIGHT);
    scroll.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    UILabel *lbl_support = [[UILabel alloc]init];
    lbl_support.frame = CGRectMake(10,0, 100, 45);
    lbl_support.text = @"Support";
    lbl_support.font = [UIFont fontWithName:kFontBold size:15];
    lbl_support.textColor = [UIColor blackColor];
    lbl_support.backgroundColor = [UIColor clearColor];
    [scroll addSubview:lbl_support];
    
    
    UIImageView *img_bg_for_about = [[UIImageView alloc]init];
    img_bg_for_about.frame = CGRectMake(0, CGRectGetMaxY(lbl_support.frame), WIDTH-20, 30);
    [img_bg_for_about setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_about setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_for_about];
    
    UILabel *lbl_about_app = [[UILabel alloc]init];
    lbl_about_app.frame = CGRectMake(10,-8, 100, 45);
    lbl_about_app.text = @"About App";
    lbl_about_app.font = [UIFont fontWithName:kFont size:13];
    lbl_about_app.textColor = [UIColor blackColor];
    lbl_about_app.backgroundColor = [UIColor clearColor];
    [img_bg_for_about addSubview:lbl_about_app];
    
    UIImageView *img_rght_arrow = [[UIImageView alloc]init];
    img_rght_arrow.frame = CGRectMake(CGRectGetMaxX(lbl_about_app.frame)+260,10,10,15);
    [img_rght_arrow setImage:[UIImage imageNamed:@"right-arrow-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_rght_arrow setUserInteractionEnabled:YES];
    [img_bg_for_about addSubview:img_rght_arrow];
    
    UIButton *btn_on_about_app = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_about_app.frame = CGRectMake(0, 0, WIDTH, 30);
    btn_on_about_app .backgroundColor = [UIColor clearColor];
    //  [btn_on_about_app setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_about_app addTarget:self action:@selector(click_on_about_app_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_about  addSubview:btn_on_about_app];
    
    UIImageView *img_bg_faq = [[UIImageView alloc]init];
    img_bg_faq.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_about.frame)+10, WIDTH-20, 30);
    [img_bg_faq setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_faq setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_faq];
    
    UILabel *lbl_faq = [[UILabel alloc]init];
    lbl_faq.frame = CGRectMake(10,-8, 100, 45);
    lbl_faq.text = @"FAQ";
    lbl_faq.font = [UIFont fontWithName:kFont size:13];
    lbl_faq.textColor = [UIColor blackColor];
    lbl_faq.backgroundColor = [UIColor clearColor];
    [img_bg_faq addSubview:lbl_faq];
    
    UIImageView *img_rght_arrow2 = [[UIImageView alloc]init];
    img_rght_arrow2.frame = CGRectMake(CGRectGetMaxX(lbl_about_app.frame)+260,10,10,15);
    [img_rght_arrow2 setImage:[UIImage imageNamed:@"right-arrow-img@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [img_rght_arrow2 setUserInteractionEnabled:YES];
    [img_bg_faq addSubview:img_rght_arrow2];
    
    UIButton *btn_on_faq = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_faq.frame = CGRectMake(0, 0, WIDTH, 30);
    btn_on_faq .backgroundColor = [UIColor clearColor];
    //  [btn_on_about_app setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_faq addTarget:self action:@selector(click_on_faq_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_faq  addSubview:btn_on_faq];
    
    UIImageView *img_bg_for_privacy_policy = [[UIImageView alloc]init];
    img_bg_for_privacy_policy.frame = CGRectMake(0, CGRectGetMaxY(img_bg_faq.frame)+10, WIDTH-20, 30);
    [img_bg_for_privacy_policy setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_privacy_policy setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_for_privacy_policy];
    
    UILabel *lbl_privacy_policy = [[UILabel alloc]init];
    lbl_privacy_policy.frame = CGRectMake(10,-8, 200, 45);
    lbl_privacy_policy.text = @"Privacy Policy";
    lbl_privacy_policy.font = [UIFont fontWithName:kFont size:13];
    lbl_privacy_policy.textColor = [UIColor blackColor];
    lbl_privacy_policy.backgroundColor = [UIColor clearColor];
    [img_bg_for_privacy_policy addSubview:lbl_privacy_policy];
    
    UIImageView *img_rght_arrow3 = [[UIImageView alloc]init];
    img_rght_arrow3.frame = CGRectMake(CGRectGetMaxX(lbl_about_app.frame)+260,10,10,15);
    [img_rght_arrow3 setImage:[UIImage imageNamed:@"right-arrow-img@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [img_rght_arrow3 setUserInteractionEnabled:YES];
    [img_bg_for_privacy_policy addSubview:img_rght_arrow3];
    
    UIButton *btn_on_privacy_policy = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_privacy_policy.frame = CGRectMake(0, 0, WIDTH, 30);
    btn_on_privacy_policy .backgroundColor = [UIColor clearColor];
    //  [btn_on_about_app setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_privacy_policy addTarget:self action:@selector(click_on_privacy_policy_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_privacy_policy  addSubview:btn_on_privacy_policy];
    
    
    
    UIImageView *img_bg_for_terms_conditions = [[UIImageView alloc]init];
    img_bg_for_terms_conditions.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_privacy_policy .frame)+10, WIDTH-20, 30);
    [img_bg_for_terms_conditions setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_terms_conditions setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_for_terms_conditions];
    
    UILabel *lbl_terms_and_conditions = [[UILabel alloc]init];
    lbl_terms_and_conditions.frame = CGRectMake(10,-8, 200, 45);
    lbl_terms_and_conditions.text = @"Terms & Conditions";
    lbl_terms_and_conditions.font = [UIFont fontWithName:kFont size:13];
    lbl_terms_and_conditions.textColor = [UIColor blackColor];
    lbl_terms_and_conditions.backgroundColor = [UIColor clearColor];
    [img_bg_for_terms_conditions addSubview:lbl_terms_and_conditions];
    
    UIImageView *img_rght_arrow4 = [[UIImageView alloc]init];
    img_rght_arrow4.frame = CGRectMake(CGRectGetMaxX(lbl_terms_and_conditions.frame)+160,10,10,15);
    [img_rght_arrow4 setImage:[UIImage imageNamed:@"right-arrow-img@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [img_rght_arrow4 setUserInteractionEnabled:YES];
    [img_bg_for_terms_conditions addSubview:img_rght_arrow4];
    
    UIButton *btn_on_terms_conditions = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_terms_conditions.frame = CGRectMake(0, 0, WIDTH, 30);
    btn_on_terms_conditions .backgroundColor = [UIColor clearColor];
    //  [btn_on_about_app setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_terms_conditions addTarget:self action:@selector(click_on_terms_conditions_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_terms_conditions  addSubview:btn_on_terms_conditions];
    
    UIImageView *img_bg_for_contact_us = [[UIImageView alloc]init];
    img_bg_for_contact_us.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_terms_conditions .frame)+10, WIDTH-20, 30);
    [img_bg_for_contact_us setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_contact_us setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_for_contact_us];
    
    UILabel *lbl_contact_us = [[UILabel alloc]init];
    lbl_contact_us.frame = CGRectMake(10,-8, 200, 45);
    lbl_contact_us.text = @"Contact Us";
    lbl_contact_us.font = [UIFont fontWithName:kFont size:13];
    lbl_contact_us.textColor = [UIColor blackColor];
    lbl_contact_us.backgroundColor = [UIColor clearColor];
    [img_bg_for_contact_us addSubview:lbl_contact_us];
    
    UIImageView *img_rght_arrow5 = [[UIImageView alloc]init];
    img_rght_arrow5.frame = CGRectMake(CGRectGetMaxX(lbl_contact_us.frame)+160,10,10,15);
    [img_rght_arrow5 setImage:[UIImage imageNamed:@"right-arrow-img@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [img_rght_arrow5 setUserInteractionEnabled:YES];
    [img_bg_for_contact_us addSubview:img_rght_arrow5];
    
    UIButton *btn_on_contact_us = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_contact_us.frame = CGRectMake(0, 0, WIDTH, 30);
    btn_on_contact_us .backgroundColor = [UIColor clearColor];
    //  [btn_on_about_app setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_contact_us addTarget:self action:@selector(click_on_contact_us_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_contact_us  addSubview:btn_on_contact_us];
    
    
    UILabel *lbl_share = [[UILabel alloc]init];
    lbl_share .frame = CGRectMake(10,CGRectGetMaxY(img_bg_for_contact_us.frame), 100, 45);
    lbl_share .text = @"Share";
    lbl_share .font = [UIFont fontWithName:kFontBold size:15];
    lbl_share .textColor = [UIColor blackColor];
    lbl_share .backgroundColor = [UIColor clearColor];
    [scroll addSubview:lbl_share ];
    
    UIImageView *img_bg_for_shar_this_app = [[UIImageView alloc]init];
    img_bg_for_shar_this_app.frame = CGRectMake(0, CGRectGetMaxY(lbl_share .frame), WIDTH-20, 30);
    [img_bg_for_shar_this_app setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_shar_this_app setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_for_shar_this_app];
    
    UILabel *lbl_shar_this_app = [[UILabel alloc]init];
    lbl_shar_this_app .frame = CGRectMake(10,-8, 200, 45);
    lbl_shar_this_app .text = @"Share this App";
    lbl_shar_this_app .font = [UIFont fontWithName:kFont size:13];
    lbl_shar_this_app .textColor = [UIColor blackColor];
    lbl_shar_this_app .backgroundColor = [UIColor clearColor];
    [img_bg_for_shar_this_app addSubview:lbl_shar_this_app ];
    
    UIImageView *img_cm = [[UIImageView alloc]init];
    img_cm.frame = CGRectMake(CGRectGetMaxX(lbl_shar_this_app.frame)+70,01,28,28);
    [img_cm setImage:[UIImage imageNamed:@"icon-cm2@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [img_cm setUserInteractionEnabled:YES];
    [img_bg_for_shar_this_app addSubview:img_cm];
    
    UIButton *btn_on_cm = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_cm.frame = CGRectMake(CGRectGetMaxX(lbl_shar_this_app.frame)+70,01,28,28);
    btn_on_cm .backgroundColor = [UIColor clearColor];
    //  [btn_on_about_app setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_cm addTarget:self action:@selector(click_on_cm_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_shar_this_app   addSubview:btn_on_cm];
    
    
    UIImageView *img_fb = [[UIImageView alloc]init];
    img_fb.frame = CGRectMake(CGRectGetMaxX(img_cm.frame)+10,01,28,28);
    [img_fb setImage:[UIImage imageNamed:@"icon-fb2@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [img_fb setUserInteractionEnabled:YES];
    [img_bg_for_shar_this_app addSubview:img_fb];
    
    UIButton *btn_on_fb = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_fb.frame = CGRectMake(CGRectGetMaxX(img_cm.frame)+10,01,28,28);
    btn_on_fb .backgroundColor = [UIColor clearColor];
    //  [btn_on_about_app setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_fb addTarget:self action:@selector(click_on_fb_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_shar_this_app   addSubview:btn_on_fb];
    
    
    UIImageView *img_tw = [[UIImageView alloc]init];
    img_tw.frame = CGRectMake(CGRectGetMaxX(img_fb.frame)+10,1,28,28);
    [img_tw setImage:[UIImage imageNamed:@"icon-tw2@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [img_tw setUserInteractionEnabled:YES];
    [img_bg_for_shar_this_app addSubview:img_tw];
    
    UIButton *btn_on_tw = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_tw.frame = CGRectMake(CGRectGetMaxX(img_fb.frame)+10,01,28,28);
    btn_on_tw .backgroundColor = [UIColor clearColor];
    //  [btn_on_about_app setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_tw addTarget:self action:@selector(click_on_tw_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_shar_this_app   addSubview:btn_on_tw];
    
    UIImageView *img_bg_for_rating_this_app = [[UIImageView alloc]init];
    img_bg_for_rating_this_app.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_shar_this_app .frame)+10, WIDTH-20, 30);
    [img_bg_for_rating_this_app setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_rating_this_app setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_for_rating_this_app];
    
    UILabel *lbl_rate_this_app = [[UILabel alloc]init];
    lbl_rate_this_app .frame = CGRectMake(10,-8, 200, 45);
    lbl_rate_this_app .text = @"Rate this App";
    lbl_rate_this_app .font = [UIFont fontWithName:kFont size:13];
    lbl_rate_this_app .textColor = [UIColor blackColor];
    lbl_rate_this_app .backgroundColor = [UIColor clearColor];
    [img_bg_for_rating_this_app addSubview:lbl_rate_this_app ];
    
    UIImageView *img_stars = [[UIImageView alloc]init];
    img_stars.frame = CGRectMake(CGRectGetMaxX(lbl_rate_this_app.frame)+70,12,90,20);
    [img_stars setImage:[UIImage imageNamed:@"img-reating@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [img_stars setUserInteractionEnabled:YES];
    [lbl_rate_this_app addSubview:img_stars];
    
    UIButton *btn_on_star = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_star.frame = CGRectMake(0, 0, 90, 30);
    btn_on_star .backgroundColor = [UIColor clearColor];
    //  [btn_on_about_app setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_star addTarget:self action:@selector(click_on_rate_settings_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_stars  addSubview:btn_on_star];
    
    
    UILabel *lbl_settings = [[UILabel alloc]init];
    lbl_settings .frame = CGRectMake(10,CGRectGetMaxY(img_bg_for_rating_this_app.frame), 100, 45);
    lbl_settings .text = @"Setting";
    lbl_settings .font = [UIFont fontWithName:kFontBold size:15];
    lbl_settings .textColor = [UIColor blackColor];
    lbl_settings .backgroundColor = [UIColor clearColor];
    [scroll addSubview:lbl_settings ];
    
    UIImageView *img_bg_for_notifications = [[UIImageView alloc]init];
    img_bg_for_notifications.frame = CGRectMake(0, CGRectGetMaxY(lbl_settings.frame), WIDTH-20, 30);
    [img_bg_for_notifications setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_notifications setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_for_notifications];
    
    UILabel *lbl_notifications = [[UILabel alloc]init];
    lbl_notifications .frame = CGRectMake(10,-8, 200, 45);
    lbl_notifications .text = @"Notification Settings";
    lbl_notifications .font = [UIFont fontWithName:kFont size:13];
    lbl_notifications .textColor = [UIColor blackColor];
    lbl_notifications .backgroundColor = [UIColor clearColor];
    [img_bg_for_notifications addSubview:lbl_notifications];
    
    UIImageView *img_rght_arrow6 = [[UIImageView alloc]init];
    img_rght_arrow6.frame = CGRectMake(CGRectGetMaxX(lbl_notifications.frame)+160,10,10,15);
    [img_rght_arrow6 setImage:[UIImage imageNamed:@"right-arrow-img@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [img_rght_arrow6 setUserInteractionEnabled:YES];
    [img_bg_for_notifications addSubview:img_rght_arrow6];
    
    UIButton *btn_on_notifications = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_notifications.frame = CGRectMake(0, 0, WIDTH, 30);
    btn_on_notifications .backgroundColor = [UIColor clearColor];
    //  [btn_on_about_app setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_notifications addTarget:self action:@selector(click_on_nottification_settings_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_notifications  addSubview:btn_on_notifications];
    
    UIImageView *img_bg_for_sign_out = [[UIImageView alloc]init];
    img_bg_for_sign_out.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_notifications.frame)+10, WIDTH-20, 30);
    [img_bg_for_sign_out setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_sign_out setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg_for_sign_out];
    
    UILabel *lbl_sign_out = [[UILabel alloc]init];
    lbl_sign_out .frame = CGRectMake(10,-8, 200, 45);
    lbl_sign_out .text = @"Sign Out";
    lbl_sign_out .font = [UIFont fontWithName:kFont size:13];
    lbl_sign_out .textColor = [UIColor blackColor];
    lbl_sign_out .backgroundColor = [UIColor clearColor];
    [img_bg_for_sign_out addSubview:lbl_sign_out];
    
    UIImageView *img_rght_arrow7 = [[UIImageView alloc]init];
    img_rght_arrow7.frame = CGRectMake(CGRectGetMaxX(lbl_notifications.frame)+160,10,10,15);
    [img_rght_arrow7 setImage:[UIImage imageNamed:@"right-arrow-img@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [img_rght_arrow7 setUserInteractionEnabled:YES];
    [img_bg_for_sign_out addSubview:img_rght_arrow7];
    
    UIButton *btn_on_Sign_out = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_Sign_out.frame = CGRectMake(0, 0, WIDTH, 30);
    btn_on_Sign_out .backgroundColor = [UIColor clearColor];
    //  [btn_on_about_app setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_Sign_out addTarget:self action:@selector(click_on_sign_up_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg_for_sign_out  addSubview:btn_on_Sign_out];
    
    if (IS_IPHONE_6Plus)
    {
        scroll.frame = CGRectMake(8,46,WIDTH-16,HEIGHT);
        
        lbl_support.frame = CGRectMake(10,0, 100, 45);
        img_bg_for_about.frame = CGRectMake(0, CGRectGetMaxY(lbl_support.frame), WIDTH-20, 30);
        lbl_about_app.frame = CGRectMake(10,-8, 100, 45);
        img_rght_arrow.frame = CGRectMake(CGRectGetMaxX(lbl_about_app.frame)+260,10,10,15);
        btn_on_about_app.frame = CGRectMake(0, 0, WIDTH, 30);
        
        img_bg_faq.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_about.frame)+10, WIDTH-20, 30);
        lbl_faq.frame = CGRectMake(10,-8, 100, 45);
        img_rght_arrow2.frame = CGRectMake(CGRectGetMaxX(lbl_about_app.frame)+260,10,10,15);
        btn_on_faq.frame = CGRectMake(0, 0, WIDTH, 30);
        
        img_bg_for_privacy_policy.frame = CGRectMake(0, CGRectGetMaxY(img_bg_faq.frame)+10, WIDTH-20, 30);
        lbl_privacy_policy.frame = CGRectMake(10,-8, 200, 45);
        img_rght_arrow3.frame = CGRectMake(CGRectGetMaxX(lbl_about_app.frame)+260,10,10,15);
        btn_on_privacy_policy.frame = CGRectMake(0, 0, WIDTH, 30);
        
        img_bg_for_terms_conditions.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_privacy_policy .frame)+10, WIDTH-20, 30);
        lbl_terms_and_conditions.frame = CGRectMake(10,-8, 200, 45);
        img_rght_arrow4.frame = CGRectMake(CGRectGetMaxX(lbl_terms_and_conditions.frame)+160,10,10,15);
        btn_on_terms_conditions.frame = CGRectMake(0, 0, WIDTH, 30);
        
        img_bg_for_contact_us.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_terms_conditions .frame)+10, WIDTH-20, 30);
        lbl_contact_us.frame = CGRectMake(10,-8, 200, 45);
        img_rght_arrow5.frame = CGRectMake(CGRectGetMaxX(lbl_contact_us.frame)+160,10,10,15);
        btn_on_contact_us.frame = CGRectMake(0, 0, WIDTH, 30);
        
        lbl_share .frame = CGRectMake(10,CGRectGetMaxY(img_bg_for_contact_us.frame), 100, 45);
        img_bg_for_shar_this_app.frame = CGRectMake(0, CGRectGetMaxY(lbl_share .frame), WIDTH-20, 30);
        lbl_shar_this_app .frame = CGRectMake(10,-8, 200, 45);
        img_cm.frame = CGRectMake(CGRectGetMaxX(lbl_shar_this_app.frame)+70,01,28,28);
        btn_on_cm.frame = CGRectMake(CGRectGetMaxX(lbl_shar_this_app.frame)+70,01,28,28);
        img_fb.frame = CGRectMake(CGRectGetMaxX(img_cm.frame)+10,01,28,28);
        btn_on_fb.frame = CGRectMake(CGRectGetMaxX(img_cm.frame)+10,01,28,28);
        img_tw.frame = CGRectMake(CGRectGetMaxX(img_fb.frame)+10,1,28,28);
        btn_on_tw.frame = CGRectMake(CGRectGetMaxX(img_fb.frame)+10,01,28,28);
        
        img_bg_for_rating_this_app.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_shar_this_app .frame)+10, WIDTH-20, 30);
        lbl_rate_this_app .frame = CGRectMake(10,-8, 200, 45);
        img_stars.frame = CGRectMake(CGRectGetMaxX(lbl_rate_this_app.frame)+70,12,90,20);
        btn_on_star.frame = CGRectMake(0, 0, 90, 30);
        
        lbl_settings .frame = CGRectMake(10,CGRectGetMaxY(img_bg_for_rating_this_app.frame), 100, 45);
        img_bg_for_notifications.frame = CGRectMake(0, CGRectGetMaxY(lbl_settings.frame), WIDTH-20, 30);
        lbl_notifications .frame = CGRectMake(10,-8, 200, 45);
        img_rght_arrow6.frame = CGRectMake(CGRectGetMaxX(lbl_notifications.frame)+160,10,10,15);
        btn_on_notifications.frame = CGRectMake(0, 0, WIDTH, 30);
        
        img_bg_for_sign_out.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_notifications.frame)+10, WIDTH-20, 30);
        lbl_sign_out .frame = CGRectMake(10,-8, 200, 45);
        img_rght_arrow7.frame = CGRectMake(CGRectGetMaxX(lbl_notifications.frame)+160,10,10,15);
        btn_on_Sign_out.frame = CGRectMake(0, 0, WIDTH, 30);
        
    }
    else if (IS_IPHONE_6)
    {
        scroll.frame = CGRectMake(8,46,WIDTH-16,HEIGHT);
        
        lbl_support.frame = CGRectMake(10,0, 100, 45);
        img_bg_for_about.frame = CGRectMake(0, CGRectGetMaxY(lbl_support.frame), WIDTH-20, 30);
        lbl_about_app.frame = CGRectMake(10,-8, 100, 45);
        img_rght_arrow.frame = CGRectMake(CGRectGetMaxX(lbl_about_app.frame)+225,10,10,15);
        btn_on_about_app.frame = CGRectMake(0, 0, WIDTH, 30);
        
        img_bg_faq.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_about.frame)+10, WIDTH-20, 30);
        lbl_faq.frame = CGRectMake(10,-8, 100, 45);
        img_rght_arrow2.frame = CGRectMake(CGRectGetMaxX(lbl_about_app.frame)+225,10,10,15);
        btn_on_faq.frame = CGRectMake(0, 0, WIDTH, 30);
        
        img_bg_for_privacy_policy.frame = CGRectMake(0, CGRectGetMaxY(img_bg_faq.frame)+10, WIDTH-20, 30);
        lbl_privacy_policy.frame = CGRectMake(10,-8, 200, 45);
        img_rght_arrow3.frame = CGRectMake(CGRectGetMaxX(lbl_about_app.frame)+225,10,10,15);
        btn_on_privacy_policy.frame = CGRectMake(0, 0, WIDTH, 30);
        
        img_bg_for_terms_conditions.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_privacy_policy .frame)+10, WIDTH-20, 30);
        lbl_terms_and_conditions.frame = CGRectMake(10,-8, 200, 45);
        img_rght_arrow4.frame = CGRectMake(CGRectGetMaxX(lbl_terms_and_conditions.frame)+125,10,10,15);
        btn_on_terms_conditions.frame = CGRectMake(0, 0, WIDTH, 30);
        
        img_bg_for_contact_us.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_terms_conditions .frame)+10, WIDTH-20, 30);
        lbl_contact_us.frame = CGRectMake(10,-8, 200, 45);
        img_rght_arrow5.frame = CGRectMake(CGRectGetMaxX(lbl_contact_us.frame)+125,10,10,15);
        btn_on_contact_us.frame = CGRectMake(0, 0, WIDTH, 30);
        
        lbl_share .frame = CGRectMake(10,CGRectGetMaxY(img_bg_for_contact_us.frame), 100, 45);
        img_bg_for_shar_this_app.frame = CGRectMake(0, CGRectGetMaxY(lbl_share .frame), WIDTH-20, 30);
        lbl_shar_this_app .frame = CGRectMake(10,-8, 200, 45);
        img_cm.frame = CGRectMake(CGRectGetMaxX(lbl_shar_this_app.frame)+35,01,28,28);
        btn_on_cm.frame = CGRectMake(CGRectGetMaxX(lbl_shar_this_app.frame)+35,01,28,28);
        img_fb.frame = CGRectMake(CGRectGetMaxX(img_cm.frame)+10,01,28,28);
        btn_on_fb.frame = CGRectMake(CGRectGetMaxX(img_cm.frame)+10,01,28,28);
        img_tw.frame = CGRectMake(CGRectGetMaxX(img_fb.frame)+10,1,28,28);
        btn_on_tw.frame = CGRectMake(CGRectGetMaxX(img_fb.frame)+10,01,28,28);
        
        img_bg_for_rating_this_app.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_shar_this_app .frame)+10, WIDTH-20, 30);
        lbl_rate_this_app .frame = CGRectMake(10,-8, 200, 45);
        img_stars.frame = CGRectMake(CGRectGetMaxX(lbl_rate_this_app.frame)+35,12,90,20);
        btn_on_star.frame = CGRectMake(0, 0, 90, 30);
        
        lbl_settings .frame = CGRectMake(10,CGRectGetMaxY(img_bg_for_rating_this_app.frame), 100, 45);
        img_bg_for_notifications.frame = CGRectMake(0, CGRectGetMaxY(lbl_settings.frame), WIDTH-20, 30);
        lbl_notifications .frame = CGRectMake(10,-8, 200, 45);
        img_rght_arrow6.frame = CGRectMake(CGRectGetMaxX(lbl_notifications.frame)+125,10,10,15);
        btn_on_notifications.frame = CGRectMake(0, 0, WIDTH, 30);
        
        img_bg_for_sign_out.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_notifications.frame)+10, WIDTH-20, 30);
        lbl_sign_out .frame = CGRectMake(10,-8, 200, 45);
        img_rght_arrow7.frame = CGRectMake(CGRectGetMaxX(lbl_notifications.frame)+125,10,10,15);
        btn_on_Sign_out.frame = CGRectMake(0, 0, WIDTH, 30);
        
    }
    else
    {
        scroll.frame = CGRectMake(8,46,WIDTH-16,HEIGHT-60);
        
        lbl_support.frame = CGRectMake(10,0, 100, 45);
        img_bg_for_about.frame = CGRectMake(0, CGRectGetMaxY(lbl_support.frame), WIDTH-20, 30);
        lbl_about_app.frame = CGRectMake(10,-8, 100, 45);
        img_rght_arrow.frame = CGRectMake(CGRectGetMaxX(lbl_about_app.frame)+170,10,10,15);
        btn_on_about_app.frame = CGRectMake(0, 0, WIDTH, 30);
        
        img_bg_faq.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_about.frame)+10, WIDTH-20, 30);
        lbl_faq.frame = CGRectMake(10,-8, 100, 45);
        img_rght_arrow2.frame = CGRectMake(CGRectGetMaxX(lbl_about_app.frame)+170,10,10,15);
        btn_on_faq.frame = CGRectMake(0, 0, WIDTH, 30);
        
        img_bg_for_privacy_policy.frame = CGRectMake(0, CGRectGetMaxY(img_bg_faq.frame)+10, WIDTH-20, 30);
        lbl_privacy_policy.frame = CGRectMake(10,-8, 200, 45);
        img_rght_arrow3.frame = CGRectMake(CGRectGetMaxX(lbl_about_app.frame)+170,10,10,15);
        btn_on_privacy_policy.frame = CGRectMake(0, 0, WIDTH, 30);
        
        img_bg_for_terms_conditions.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_privacy_policy .frame)+10, WIDTH-20, 30);
        lbl_terms_and_conditions.frame = CGRectMake(10,-8, 200, 45);
        img_rght_arrow4.frame = CGRectMake(CGRectGetMaxX(lbl_terms_and_conditions.frame)+70,10,10,15);
        btn_on_terms_conditions.frame = CGRectMake(0, 0, WIDTH, 30);
        
        img_bg_for_contact_us.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_terms_conditions .frame)+10, WIDTH-20, 30);
        lbl_contact_us.frame = CGRectMake(10,-8, 200, 45);
        img_rght_arrow5.frame = CGRectMake(CGRectGetMaxX(lbl_contact_us.frame)+70,10,10,15);
        btn_on_contact_us.frame = CGRectMake(0, 0, WIDTH, 30);
        
        lbl_share .frame = CGRectMake(10,CGRectGetMaxY(img_bg_for_contact_us.frame), 100, 45);
        img_bg_for_shar_this_app.frame = CGRectMake(0, CGRectGetMaxY(lbl_share .frame), WIDTH-20, 30);
        lbl_shar_this_app .frame = CGRectMake(10,-8, 200, 45);
        img_cm.frame = CGRectMake(CGRectGetMaxX(lbl_shar_this_app.frame)-20,01,28,28);
        btn_on_cm.frame = CGRectMake(CGRectGetMaxX(lbl_shar_this_app.frame)-20,01,28,28);
        img_fb.frame = CGRectMake(CGRectGetMaxX(img_cm.frame)+10,01,28,28);
        btn_on_fb.frame = CGRectMake(CGRectGetMaxX(img_cm.frame)+10,01,28,28);
        img_tw.frame = CGRectMake(CGRectGetMaxX(img_fb.frame)+10,1,28,28);
        btn_on_tw.frame = CGRectMake(CGRectGetMaxX(img_fb.frame)+10,01,28,28);
        
        img_bg_for_rating_this_app.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_shar_this_app .frame)+10, WIDTH-20, 30);
        lbl_rate_this_app .frame = CGRectMake(10,-8, 200, 45);
        img_stars.frame = CGRectMake(CGRectGetMaxX(lbl_rate_this_app.frame)-20,12,90,20);
        btn_on_star.frame = CGRectMake(0, 0, 90, 30);
        
        lbl_settings .frame = CGRectMake(10,CGRectGetMaxY(img_bg_for_rating_this_app.frame), 100, 45);
        img_bg_for_notifications.frame = CGRectMake(0, CGRectGetMaxY(lbl_settings.frame), WIDTH-20, 30);
        lbl_notifications .frame = CGRectMake(10,-8, 200, 45);
        img_rght_arrow6.frame = CGRectMake(CGRectGetMaxX(lbl_notifications.frame)+70,10,10,15);
        btn_on_notifications.frame = CGRectMake(0, 0, WIDTH, 30);
        
        img_bg_for_sign_out.frame = CGRectMake(0, CGRectGetMaxY(img_bg_for_notifications.frame)+10, WIDTH-20, 30);
        lbl_sign_out .frame = CGRectMake(10,-8, 200, 45);
        img_rght_arrow7.frame = CGRectMake(CGRectGetMaxX(lbl_notifications.frame)+70,10,10,15);
        btn_on_Sign_out.frame = CGRectMake(0, 0, WIDTH, 30);
        
    }
    [scroll setContentSize:CGSizeMake(0,630)];
    
}


-(void)Aflogout
{
    
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    NSDictionary *params =@{
                            
                            
                            @"loginuid"            :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"device_type"         : @"1"
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:@"webservices/api-logout.json"
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [self ResponseChefSignUpHcity:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self Aflogout];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpHcity :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        
        NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
        [defaults1 removeObjectForKey:@"UserInfo"];
        [defaults1 synchronize];
        
        //        UserLogonVC*vc = [UserLogonVC new];
        //        [self presentViewController:vc animated:NO completion:nil];
        
        
        
        SplashScreensViewController*vc = [SplashScreensViewController new];
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
    }
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma click_events
-(void)click_on_back_arrow:(UIButton *)sender
{
    NSLog(@"click_on_back_arrow:");
    //    [self.navigationController popViewController];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    
}

-(void)click_on_about_app_btn:(UIButton *)sender
{
    NSLog(@"click_on_about_app_btn:");
    AboutUS *vc = [[AboutUS alloc]init];
    vc.str_Title = @"About Us";
    vc.str_URL = @"ws/pages-contents.php?nid=64";
    [self presentViewController:vc animated:NO completion:nil];
    
    //  [self.navigationController pushViewController:vc];
    
}
-(void)click_on_faq_btn:(UIButton *)sender
{
    NSLog(@"click_on_faq_btn:");
    AboutUS *vc = [[AboutUS alloc]init];
    vc.str_Title = @"FAQ";
    vc.str_URL = @"65";
    [self presentViewController:vc animated:NO completion:nil];
    //    [self.navigationController pushViewController:vc];
}
-(void)click_on_privacy_policy_btn:(UIButton *)sender
{
    NSLog(@"click_on_privacy_policy_btn:");
    AboutUS *vc = [[AboutUS alloc]init];
    vc.str_Title = @"Privacy Policy";
    vc.str_URL = @"66";
    [self presentViewController:vc animated:NO completion:nil];
    
    //[self.navigationController pushViewController:vc];
}
-(void)click_on_rate_settings_btn:(UIButton *)sender
{
    [iTellAFriend sharedInstance].appStoreID = 803688066;
    [[iTellAFriend sharedInstance] rateThisAppWithAlertView:YES];
    
}
-(void)click_on_terms_conditions_btn:(UIButton *)sender
{
    NSLog(@"click_on_terms_conditions_btn:");
    AboutUS *vc = [[AboutUS alloc]init];
    vc.str_Title = @"Terms and conditions";
    vc.str_URL = @"67";
    [self presentViewController:vc animated:NO completion:nil];
    
    // [self.navigationController pushViewController:vc];
}
-(void)click_on_contact_us_btn:(UIButton *)sender
{
    NSLog(@"click_on_contact_us_btn:");
    AboutUS *vc = [[AboutUS alloc]init];
    vc.str_Title = @"Contact Us";
    vc.str_URL = @"ws/pages-contents.php?nid=68 ";
    [self presentViewController:vc animated:NO completion:nil];
    
    // [self.navigationController pushViewController:vc];
    
    
}
//-(void)click_on_cm_btn:(UIButton *)sender
//{
//    NSLog(@"click_on_cm_btn:");
//}

#pragma mark Instagram

/////instagram///
-(void)getimageBtnClick
{
    
    UIImagePickerController* imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

+ (BOOL) isAppInstalled
{
    NSURL *appURL = [NSURL URLWithString:kInstagramAppURLString];
    return [[UIApplication sharedApplication] canOpenURL:appURL];
}


-(UIImage*)thumbnailFromView:(UIView*)_myView{
    return [self thumbnailFromView:_myView withSize:_myView.frame.size];
}

-(UIImage*)thumbnailFromView:(UIView*)_myView withSize:(CGSize)viewsize{
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0)) {
        // Retina display
        CGSize newSize = viewsize;
        newSize.height=newSize.height*2;
        newSize.width=newSize.width*2;
        viewsize=newSize;
    }
    
    UIGraphicsBeginImageContext(_myView.bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGContextSetShouldAntialias(context, YES);
    [_myView.layer renderInContext: context];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    CGSize size = _myView.frame.size;
    CGFloat scale = MAX(viewsize.width / size.width, viewsize.height / size.height);
    
    UIGraphicsBeginImageContext(viewsize);
    CGFloat width = size.width * scale;
    CGFloat height = size.height * scale;
    float dwidth = ((viewsize.width - width) / 2.0f);
    float dheight = ((viewsize.height - height) / 2.0f);
    CGRect rect = CGRectMake(dwidth, dheight, size.width * scale, size.height * scale);
    [image drawInRect:rect];
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newimg;
}

-(void)click_on_cm_btn:(UIButton *)sender
{
    NSLog(@"icon_cm Btn Click");
    
    if ([UserSupportVC isAppInstalled])
    {
        
        UIImageView * imageView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 200,200)];
        [imageView setUserInteractionEnabled:TRUE];
        //        [imageView setImageWithURL:[NSURL URLWithString:[[ary_dishdetaillist objectAtIndex:0] valueForKey:@"DishImage"]]];
        
        imageView.image =  [UIImage imageNamed:@"57x57.png"];
        
        [self.view addSubview:imageView];
        imageView.hidden=YES;
        
        
        
        
        UIImage *image = imageView.image;
        
        NSString *description=  @"Not86";
        
        if ([MGInstagram isAppInstalled])
        {
            //[self AFProductShare:str_productID];
            [MGInstagram postImage:image withCaption:description inView:self.view];
        }
        else
            [self.notInstalledAlert show];
        
        
        
        
    }
    else
    {
        [self.notInstalledAlert show];
        NSLog(@"app not installed");
        //[self InstagramPopUp];
        
        
        //[SVProgressHUD showErrorWithStatus:@"Your Device should have the Instagrm Application"];
    }
    
    
}
- (UIAlertView*) notInstalledAlert
{
    return [[UIAlertView alloc] initWithTitle:@"Instagram Not Installed!" message:@"Instagram must be installed on the device in order to post images" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
}

-(void)click_on_fb_btn:(UIButton *)sender
{
    NSLog(@"click_on_fb_btn:");
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *facebook = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result)
        {
            if (result == SLComposeViewControllerResultDone)
            {
            }
            [facebook dismissViewControllerAnimated:YES completion:Nil];
        };
        facebook.completionHandler =myBlock;
        
        
        [facebook addImage:[UIImage imageNamed:@"57x57.png"]];
        [facebook setInitialText:@"Not86"];
        [self presentViewController:facebook animated:YES completion:nil];
        
    }
    else
    {
        
        
        //            [SVProgressHUD showErrorWithStatus:@"You can't send a share right now, make sure your device has an internet connection and you have at least one facebook account setup"];
        
    }
    
}
-(void)click_on_tw_btn:(UIButton *)sender
{
    NSLog(@"click_on_tw_btn:");
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        // Create a compose view controller for the service type Twitter
        SLComposeViewController *mulitpartPost = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result)
        {
            if (result == SLComposeViewControllerResultDone)
            {
            }
            [mulitpartPost dismissViewControllerAnimated:YES completion:Nil];
        };
        mulitpartPost.completionHandler =myBlock;
        
        
        
        
        // Set the text of the tweet
        [mulitpartPost setInitialText:@"Not86"];
        
        [mulitpartPost addImage:[UIImage imageNamed:@"57x57.png"]];
        
        
        
        
        // Display the tweet sheet to the user
        [self presentViewController:mulitpartPost animated:YES completion:nil];
        
    }
    else
    {
        //        [SVProgressHUD showErrorWithStatus:@"You can't send a share right now, make sure your device has an internet connection and you have at least one Twitter account setup"];
        
        //        [self callAlertView:@"Sorry" message:@"You can't send a share right now, make sure your device has an internet connection and you have at least one Twitter account setup"];
    }
    
}
-(void)click_on_nottification_settings_btn:(UIButton *)sender
{
    NSLog(@"click_on_nottification_settings_btn:");
    
    UserNotificationsVC *vc = [[UserNotificationsVC alloc]init];
    [self presentViewController:vc animated:NO completion:nil];
}
-(void)click_on_sign_up_btn:(UIButton *)sender
{
    NSLog(@"click_on_sign_up_btn:");
    [self Aflogout];
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
