//
//  NearByLocationVC.m
//  Joel
//
//  Created by User9 on 12/06/14.
//  Copyright (c) 2014 User1. All rights reserved.
//

#import "NearByLocationVC.h"
//#import "Foursquare2.h"
//#import "FSVenue.h"
//#import "FSConverter.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "JWSlideMenuController.h"
#import "AppDelegate.h"
#import "Define.h"
#import "HomeVC.h"
#import "LocationVC.h"



//For Google places API

#define kGoogleAPIKey @"AIzaSyCHFR9k8arFpJLLb3Y98LT5y5LUnY9pcR8"

@interface NearByLocationVC ()<CLLocationManagerDelegate,UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,MKMapViewDelegate,UIGestureRecognizerDelegate,MKReverseGeocoderDelegate,LocationSelectionDelegate>
{
    AppDelegate *delegate1;
    
    CLLocationManager *_locationManager;
    
    MKMapView  *mapviewlocation;
    UITextField *textfield_Location;
    UITableView *tableView_PlacesList;
    
    NSMutableArray *ary_SelectLocation;
    
    NSString *str_Latitude;
    NSString *str_Longitude;
    UIImageView *imageview_TopBar;
    UIButton *btn_BackHeader;
    
    NSString *cityName;
    
    double latitude;
    double longitude;
    
    int selectedIndex;
    
    NSString *str_location;
    UIImageView *img_BackGround;
    UIButton *btn_BackTransP;
    
    
    UILabel*lbl_Map;
    UILabel*lbl_foursquare;
    UIImageView*img_currentlocation;
    
    // TRAutocompleteView *_autocompleteView;
    
     NSString*str_mapselection;
    
    NSString*str_GooglemapLat;
    NSString*str_Googlemaplong;
    
    
    NSString*str_address;
    
    UIView*alertviewBg;
    
   // HMLocalization*localization;
    
    
}

@end

@implementation NearByLocationVC
@synthesize delegate;
@synthesize str_Title;
@synthesize str_Latitude;
@synthesize str_Longitude;

@synthesize myGeoCoder;
@synthesize locationManager;
@synthesize mUserCurrentLocation;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self prefersStatusBarHidden];
//    localization = [HMLocalization sharedInstance];
//    [localization setLanguage];
    str_address = [NSString new];
    
    str_location=@"";
    str_mapselection = [NSString new];
    str_mapselection = @"MAP";
    
    str_GooglemapLat = [NSString new];
    str_Googlemaplong = [NSString new];
    
    delegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    ary_SelectLocation = [NSMutableArray new];
    
    [delegate1 updateCurrentLocation];
    CLLocation *myLocation = [delegate1 getCurrentLocation];
    
    if(myLocation.coordinate.latitude == 0.00000 && myLocation.coordinate.longitude == 0.00000)
        myLocation = [[CLLocation alloc] initWithLatitude:1.3677537583316497 longitude:103.8892936706543];
    
    selectedIndex = -1;
    
 
    
//    UIButton *btn_Done = [[UIButton alloc] init];
//    [btn_Done addTarget:self action:@selector(click_Done) forControlEvents:UIControlEventTouchUpInside ];
//    [btn_Done setUserInteractionEnabled:YES];
//    btn_Done.backgroundColor=[UIColor redColor];
//    [self.view addSubview:btn_Done];
    
    
    UIButton *btn_img_done = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_done .backgroundColor = [UIColor colorWithRed:39/255.0f green:37/255.0f blue:45/255.0f alpha:1];
    [btn_img_done setTitle:@"Done" forState:UIControlStateNormal];
    [btn_img_done setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_img_done addTarget:self action:@selector(click_Done) forControlEvents:UIControlEventTouchUpInside];
    //[btn_img_done setImage:[UIImage imageNamed:@"button-done@2x.png"] forState:UIControlStateNormal];
    [self.view   addSubview:btn_img_done];

    
    mapviewlocation = [[MKMapView alloc]initWithFrame:CGRectMake(0,0,WIDTH,HEIGHT-(100))];
    mapviewlocation.delegate = self;
    mapviewlocation.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:mapviewlocation];
    
    
    UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self action:@selector(tapGestureHandler:)];
    tgr.delegate = self;  //also add <UIGestureRecognizerDelegate> to @interface
    [mapviewlocation addGestureRecognizer:tgr];
    
    
    
    if (IS_IPHONE_6) {
        btn_img_done.frame=CGRectMake(WIDTH/2-50,HEIGHT-70,100,50);
    }
    else if (IS_IPHONE_6Plus) {
        
        btn_img_done.frame=CGRectMake(WIDTH/2-50,HEIGHT-70,100,50);

    }
    else
    {
         btn_img_done.frame=CGRectMake(WIDTH/2-50,HEIGHT-70,90,30);
    }
    
    
    
}


-(BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void)viewWillDisappear:(BOOL)animated
{
    
//    if (str_location.length<=0)
//    {
//        
//        HomeVC *vc = [[HomeVC alloc]init];
//        vc.str_PickingAddress=textfield_Location.text;
//        vc.str_MapLocationChange = @"NO";
//        [self.navigationController popViewController];
//
//    }
//    else
//    {
//    HomeVC *vc = [[HomeVC alloc]init];
//    vc.str_PickingAddress=textfield_Location.text;
//    vc.str_MapLocationChange = @"YES";
//    [self.navigationController popViewController];
//    }

}
#pragma mark  viewWillAppear

-(void)viewWillAppear:(BOOL)animated
{
 
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
}

-(void)click_Done
{
    [self.piddelegate selectedLocationWithName:str_address latitude:latitude longitude:longitude MapLocationChange:@"YES"];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
}


-(void)ShowImportLocation
{
    if ([str_Title isEqual:[NSNull null]])
    {
        str_address = @"";
    }
    else
    {
        str_address = str_Title;
    }
    
    [self removeAllAnnotationExceptOfCurrentUser];
    //  CGFloat latitude1 = [str_Latitude floatValue];
    //  CGFloat longitude1 = [str_Longitude floatValue];
    latitude = [str_Latitude floatValue];
    longitude = [str_Longitude floatValue];
    
    
    CLLocationCoordinate2D MapCoordinate=CLLocationCoordinate2DMake(latitude, longitude);
    MKPointAnnotation *point=[[MKPointAnnotation alloc] init];
    point.coordinate=MapCoordinate;
    point.title=str_Title;
    [mapviewlocation addAnnotation:point];
    [self centerMap:latitude long:longitude];
    str_location=@"Yes";
}

-(void)click_BtnAddLocation
{
    if (str_address.length == 0)
    {
//        [SVProgressHUD showErrorWithStatus:@"Please enter location"];
    }
    else
    {
        [self removeAllAnnotationExceptOfCurrentUser];
        CLLocationCoordinate2D MapCoordinate=CLLocationCoordinate2DMake(latitude, longitude);
        MKPointAnnotation *point=[[MKPointAnnotation alloc] init];
        point.coordinate=MapCoordinate;
        point.title=str_address;
        [mapviewlocation addAnnotation:point];
        [self centerMap:latitude long:longitude];
        //[self proccessAnnotations];
    }
}

-(void)click_BtnBack:(UIButton *)sender
{
    [self.navigationController popViewController];
   
}
#pragma mark UsersCurrentLocation
-(void)fetchUsersCurrentLocation
{
    
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
        // Use one or the other, not both. Depending on what you put in info.plist
        [locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    [locationManager startUpdatingLocation];
    
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    [locationManager startUpdatingLocation];
    
    mUserCurrentLocation = [[CLLocation alloc]init];
    mUserCurrentLocation = [self.locationManager location];
    
    
    CLLocationCoordinate2D coordinate;
    coordinate.longitude = locationManager.location.coordinate.longitude;
    coordinate.latitude = locationManager.location.coordinate.latitude;
    
    NSString *str_lon =[NSString stringWithFormat:@"%f",coordinate.longitude];
    NSString *str_lat =[NSString stringWithFormat:@"%f",coordinate.latitude];
    
    NSLog(@"str_lat:%@ str_lon:%@",str_lat,str_lon);
    
    
}



- (void)updateCurrentLocation
{
    self.locationManager = [[CLLocationManager alloc] init];
    
    
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
        // Use one or the other, not both. Depending on what you put in info.plist
        //[self.locationManager  requestWhenInUseAuthorization];
        [self.locationManager  requestAlwaysAuthorization];
    }
#endif
    
    
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    
    
    //    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
    //        [self.locationManager requestWhenInUseAuthorization];
    //    }
    
    [self.locationManager startUpdatingLocation];
    
    mUserCurrentLocation = [[CLLocation alloc]init];
    mUserCurrentLocation = [self.locationManager location];
    
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude);
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = coordinate;
    point.title = cityName;
    [mapviewlocation addAnnotation:point];
    
    if ([cityName isEqual:[NSNull null]])
    {
        str_address = @"";
    }
    else
    {
        str_address=cityName;
    }
    //    CLLocationCoordinate2D coordinate1 = CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude);
    //
    //    MKPointAnnotation *point1 = [[MKPointAnnotation alloc] init];
    //    point1.coordinate = coordinate1;
    //    point1.title = cityName;
    //    [mapviewlocation addAnnotation:point1];
    
    
    
    //    [self getCurrentLocation];
    
    
    CLGeocoder  *geoCoder = [[CLGeocoder alloc] init];
    CLLocation *location = [[CLLocation alloc]initWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude];
    [geoCoder reverseGeocodeLocation:location completionHandler: ^(NSArray *placemarks, NSError *error) {
        if(!error)
        {
            
            NSString *str_Add = @"";
            
            CLPlacemark *place;
            place = [placemarks objectAtIndex:0];
            NSLog(@"%@",place);
            
            if ([place.addressDictionary valueForKey:@"Street"]) {
                
                if (![[place.addressDictionary valueForKey:@"Street"] isEqual:[NSNull null]])
                {
                    str_Add = [NSString stringWithFormat:@"%@",[place.addressDictionary valueForKey:@"Street"]];
                }
            }
            if (![[place.addressDictionary valueForKey:@"State"] isEqual:[NSNull null]])
            {
                str_Add = [NSString stringWithFormat:@"%@,%@",str_Add,[place.addressDictionary valueForKey:@"State"]];
            }
            if (![[place.addressDictionary valueForKey:@"Country"] isEqual:[NSNull null]])
            {
                str_Add = [NSString stringWithFormat:@"%@,%@",str_Add,[place.addressDictionary valueForKey:@"Country"]];
            }
            
            
            if ([str_Add hasPrefix:@","])
            {
                str_Add = [str_Add substringFromIndex:1];
            }
            
            if([str_Add isEqual:[NSNull null]])
            {
                str_address=@"";
            }
            else
            {
                str_address = str_Add;
            }
            NSLog(@"%@",place.addressDictionary);
            
            
            
            [self removeAllAnnotationExceptOfCurrentUser];
            
            latitude = locationManager.location.coordinate.latitude;
            longitude = locationManager.location.coordinate.longitude;
            MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
            point.coordinate = coordinate;
            point.title = str_Add;
            [mapviewlocation addAnnotation:point];
            
            [self centerMap:latitude long:longitude];
            
            
        }
        else
        {
            
        }
    }];
    
    
}

-(CLLocation *) getCurrentLocation{
    
    
    return mUserCurrentLocation;
}

- (NSString *)deviceLocation {
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude];
}
- (NSString *)deviceLat {
    return [NSString stringWithFormat:@"%f", locationManager.location.coordinate.latitude];
}
- (NSString *)deviceLon {
    return [NSString stringWithFormat:@"%f", locationManager.location.coordinate.longitude];
}
- (NSString *)deviceAlt {
    return [NSString stringWithFormat:@"%f",locationManager.location.altitude];
}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    // NSLog(@"didFailWithError: %@", error);
    
    //  printf("\nerror");
    //    UIAlertView *alert = [ [UIAlertView alloc] initWithTitle:@"Error"
    //                                                     message:@"Error while getting your current location."
    //                                                    delegate:self
    //                                           cancelButtonTitle:@"OK"
    //                                           otherButtonTitles:nil ];
    //
    //    [alert show];
}

//- (void)locationManager:(CLLocationManager )manager didUpdateToLocation:(CLLocation )newLocation fromLocation:(CLLocation *)oldLocation
//{
//
//    CLLocation *currentLocation = newLocation;
//
//
//
//    if (currentLocation != nil) {
//        mUserCurrentLocation = newLocation;
//    }
//
//    // Stop Location Manager
//    [locationManager stopUpdatingLocation];
//
//}

#pragma mark - LocationManager Delegate Methods

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations objectAtIndex:0];
    NSLog(@"lat%f - lon%f", location.coordinate.latitude, location.coordinate.longitude);
    
    [self.delegate setLocationWithName:@"" lat:location.coordinate.latitude  long:location.coordinate.longitude];
    
    
    //   [self ShowImportLocation];
    
    [locationManager stopUpdatingLocation];
    
    if (!self.myGeoCoder)
    {
        self.myGeoCoder = [[CLGeocoder alloc] init];
    }
    
    [self.myGeoCoder reverseGeocodeLocation: locationManager.location completionHandler:
     
     ^(NSArray *placemarks, NSError *error)
     {
         //Get nearby address
         
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         
         //String to hold address
         
         NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
         
         NSLog(@"located at %@",locatedAt);
         cityName = @"";
         cityName = [placemark.addressDictionary valueForKey:@"City"];
         NSLog(@"City name is %@",cityName);
         if ([cityName isEqual:[NSNull null]]) {
             str_address=@"";
         }
         else
         {
             str_address=[placemark.addressDictionary valueForKey:@"Name"];
         }
         str_location=@"Yes";
         [self.delegate setLocationWithName:cityName lat:location.coordinate.latitude  long:location.coordinate.longitude];
         
     }];
    
    
}


// this delegate is called when the app successfully finds your current location
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    // this creates a MKReverseGeocoder to find a placemark using the found coordinates
    MKReverseGeocoder *geoCoder = [[MKReverseGeocoder alloc] initWithCoordinate:newLocation.coordinate];
    geoCoder.delegate = self;
    [geoCoder start];
}

// this delegate method is called if an error occurs in locating your current location


// this delegate is called when the reverseGeocoder finds a placemark
- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark
{
    MKPlacemark * myPlacemark = placemark;
    // with the placemark you can now retrieve the city name
    //    NSString city = [myPlacemark.addressDictionary objectForKey:(NSString) kABPersonAddressCityKey];
}

// this delegate is called when the reversegeocoder fails to find a placemark




#pragma mark ResignTextField
-(void)ResignTextField:(UIButton *)sender
{
    [textfield_Location resignFirstResponder];
    
}


-(void)removeAllAnnotationExceptOfCurrentUser
{
    NSMutableArray *annForRemove = [[NSMutableArray alloc] initWithArray:mapviewlocation.annotations];
    if ([mapviewlocation.annotations.lastObject isKindOfClass:[MKUserLocation class]]) {
        [annForRemove removeObject:mapviewlocation.annotations.lastObject];
    }else{
        for (id <MKAnnotation> annot_ in mapviewlocation.annotations)
        {
            if ([annot_ isKindOfClass:[MKUserLocation class]] ) {
                [annForRemove removeObject:annot_];
                break;
            }
        }
    }
    
    
    [mapviewlocation removeAnnotations:annForRemove];
}

-(void)proccessAnnotations
{
    [self removeAllAnnotationExceptOfCurrentUser];
    [mapviewlocation addAnnotation:[ary_SelectLocation objectAtIndex:selectedIndex]];
    [self centerMap:latitude long:longitude];
    
}

-(void)setupMapForLocatoion:(CLLocation*)newLocation{
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.003;
    span.longitudeDelta = 0.003;
    CLLocationCoordinate2D location;
    location.latitude = newLocation.coordinate.latitude;
    location.longitude = newLocation.coordinate.longitude;
    region.span = span;
    region.center = location;
    [mapviewlocation setRegion:region animated:YES];
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer
                         :(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)tapGestureHandler:(UITapGestureRecognizer *)tgr
{
    
    
    CGPoint touchPoint = [tgr locationInView:mapviewlocation];
    
    CLLocationCoordinate2D touchMapCoordinate = [mapviewlocation convertPoint:touchPoint toCoordinateFromView:mapviewlocation];
    
    NSLog(@"tapGestureHandler: touchMapCoordinate = %f,%f",
          touchMapCoordinate.latitude, touchMapCoordinate.longitude);
    
    // CLLocation *myLocation = [[CLLocation alloc]initWithLatitude:touchMapCoordinate.latitude longitude:touchMapCoordinate.longitude];
    
    
    
    CLGeocoder  *geoCoder = [[CLGeocoder alloc] init];
    CLLocation *location = [[CLLocation alloc]initWithLatitude:touchMapCoordinate.latitude longitude:touchMapCoordinate.longitude];
    [geoCoder reverseGeocodeLocation:location completionHandler: ^(NSArray *placemarks, NSError *error) {
        if(!error)
        {
            
            
            NSString *str_Add = @"";
            
            CLPlacemark *place;
            place = [placemarks objectAtIndex:0];
            NSLog(@"%@",place);
            
            if ([place.addressDictionary valueForKey:@"Street"]) {
                
                if (![[place.addressDictionary valueForKey:@"Street"] isEqual:[NSNull null]])
                {
                    str_Add = [NSString stringWithFormat:@"%@",[place.addressDictionary valueForKey:@"Street"]];
                }
            }
            if (![[place.addressDictionary valueForKey:@"State"] isEqual:[NSNull null]])
            {
                str_Add = [NSString stringWithFormat:@"%@,%@",str_Add,[place.addressDictionary valueForKey:@"State"]];
            }
            
            if (![[place.addressDictionary valueForKey:@"ZIP"] isEqual:[NSNull null]])
            {
                str_Add = [NSString stringWithFormat:@"%@,%@",str_Add,[place.addressDictionary valueForKey:@"ZIP"]];
            }
            
            if (![[place.addressDictionary valueForKey:@"Country"] isEqual:[NSNull null]])
            {
                str_Add = [NSString stringWithFormat:@"%@,%@",str_Add,[place.addressDictionary valueForKey:@"Country"]];
            }
            
            
            if ([str_Add hasPrefix:@","])
            {
                str_Add = [str_Add substringFromIndex:1];
            }
            
            if ([str_Add isEqual:[NSNull null]])
            {
                str_address=@"";
            }
            else
            {
                str_address = str_Add;
            }
            NSLog(@"%@",place.addressDictionary);
            
            
            
            [self removeAllAnnotationExceptOfCurrentUser];
            
            latitude = touchMapCoordinate.latitude;
            longitude = touchMapCoordinate.longitude;
            MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
            point.coordinate = touchMapCoordinate;
            point.title = str_Add;
            [mapviewlocation addAnnotation:point];
            
            [self centerMap:latitude long:longitude];
            
            
        }
        else
        {
            
        }
    }];
    
    //    NSNumber *num = [[NSNumber alloc]initWithInt:0];
    //    [self getVenuesForLocation:myLocation radius:num];
    //    [self setupMapForLocatoion:myLocation];
    
    
    //    [self getVenuesForLocation:myLocation];
    //    [self setupMapForLocatoion:myLocation];
    
    str_location=@"Yes";
    
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ary_SelectLocation count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if ([ary_SelectLocation count]) {
        return 1;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont systemFontOfSize:12];
        cell.textLabel.textColor = [UIColor grayColor];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
        cell.detailTextLabel.textColor = [UIColor grayColor];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
       return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark textField Delegate Methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    str_location=@"";
    
    if (ary_SelectLocation)
    {
        [ary_SelectLocation removeAllObjects];
    }
    
    for (int i=0; i<[self.nearbyVenues count];i++)
    {
        NSString *search= [self.nearbyVenues[i] name];
        NSString *substring = [NSString stringWithString:textField.text];
        substring = [substring stringByReplacingCharactersInRange:range withString:string];
        NSRange r = [search rangeOfString:substring options:NSCaseInsensitiveSearch];
        
        if(r.location != NSNotFound)
        {
            if ([str_mapselection isEqualToString:@"MAP"])
            {
                
            }
            else{
                if (r.length > 0)
                {
                    [ary_SelectLocation addObject:[self.nearbyVenues objectAtIndex:i]];
                }
            }
            
        }
    }
    
    if ([str_mapselection isEqualToString:@"MAP"])
    {
        
    }
    else{
        if ([ary_SelectLocation count]<6)
        {
            tableView_PlacesList.frame = CGRectMake(35,CGRectGetMaxY(textfield_Location.frame)+100,WIDTH-70,[ary_SelectLocation count]*40);
        }
        else
        {
            tableView_PlacesList.frame = CGRectMake(35,CGRectGetMaxY(textfield_Location.frame)+100,WIDTH-70,40*6);
        }
        
        
        [tableView_PlacesList setHidden:NO];
        [tableView_PlacesList reloadData];

    }
        return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField.text isEqualToString:@""])
    {
         [self removeAllAnnotationExceptOfCurrentUser];
        
        if ([str_mapselection isEqualToString:@"MAP"])
        {
            
        }
        else{
            [ary_SelectLocation removeAllObjects];
            for (int i=0; i<[self.nearbyVenues count]; i++)
            {
                [ary_SelectLocation addObject:[self.nearbyVenues objectAtIndex:i]];
            }
            [tableView_PlacesList setHidden:NO];
            [tableView_PlacesList reloadData];

        }

       
    }
    else
    {
        if ([ary_SelectLocation count]==0)
        {
            [self removeAllAnnotationExceptOfCurrentUser];
//            [SVProgressHUD showErrorWithStatus:@"No location found"];
        }
        
    }
    
    if ([str_mapselection isEqualToString:@"MAP"])
    {
        
    }
    else{

        if ([ary_SelectLocation count]<6)
        {
            tableView_PlacesList.frame = CGRectMake(35,CGRectGetMaxY(textfield_Location.frame)+100,WIDTH-70,[ary_SelectLocation count]*40);
        }
        else
        {
            tableView_PlacesList.frame = CGRectMake(35,CGRectGetMaxY(textfield_Location.frame)+100,WIDTH-70,40*6);
        }
        [textField resignFirstResponder];
    }

   }


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
    
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

//- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
//{
//    if (annotation == mapView.userLocation)
//        return nil;
//    
//    static NSString *s = @"ann";
//    MKAnnotationView *pin = [mapView dequeueReusableAnnotationViewWithIdentifier:s];
//    if (!pin) {
//        pin = [[MKPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:s];
//        pin.frame = CGRectMake(0, 0, 15, 15);
//        pin.canShowCallout = YES;
//        pin.image = [UIImage imageNamed:@"MapPin@2x.png"];
//        pin.calloutOffset = CGPointMake(0, 0);
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//        //        [button addTarget:self
//        //                   action:@selector(checkinButton) forControlEvents:UIControlEventTouchUpInside];
//        pin.rightCalloutAccessoryView = button;
//        
//    }
//    return pin;
//}

#pragma mark Calculate Center location in Map
- (void)centerMap:(CGFloat)Latitude long:(CGFloat)Longitude
{
    MKCoordinateRegion region;
    
    CLLocationDegrees maxLat = -90;
    CLLocationDegrees maxLon = -180;
    CLLocationDegrees minLat = 90;
    CLLocationDegrees minLon = 180;
    
    CLLocation *trafficLocation = nil;
    trafficLocation = [[CLLocation alloc] initWithLatitude:Latitude longitude:Longitude];
    if(trafficLocation.coordinate.latitude > maxLat)
        maxLat = trafficLocation.coordinate.latitude;
    if(trafficLocation.coordinate.latitude < minLat)
        minLat = trafficLocation.coordinate.latitude;
    if(trafficLocation.coordinate.longitude > maxLon)
        maxLon = trafficLocation.coordinate.longitude;
    if(trafficLocation.coordinate.longitude < minLon)
        minLon = trafficLocation.coordinate.longitude;
    
    region.center.latitude     = (maxLat + minLat) / 2;
    region.center.longitude    = (maxLon + minLon) / 2;
    region.span.latitudeDelta  = maxLat - minLat;
    region.span.longitudeDelta = maxLon - minLon;
    
    if(region.center.longitude == 0.000000 || region.center.longitude == 0.0000000 )
    {
    }
    else
    {
        [mapviewlocation setRegion:region animated:YES];
        [mapviewlocation regionThatFits:region];
    }
}

//-(void)Map_BtnClick:(UIButton *)sender
//{
//    tableView_PlacesList.hidden  =YES;
//    _autocompleteView.hidden = NO;
//    str_mapselection = @"MAP";
//    lbl_Map.textColor=[UIColor colorWithRed:243/255.0f green:141/255.0f blue:10/255.0f alpha:1];
//    lbl_foursquare.textColor=[UIColor colorWithRed:85/255.0f green:85/255.0f blue:85/255.0f alpha:1];
//}
//-(void)foursquare_BtnClick:(UIButton *)sender
//{
//    str_mapselection = @"FOURSQUARE";
//    _autocompleteView.hidden = YES;
//    tableView_PlacesList.hidden  =NO;
//    lbl_Map.textColor=[UIColor colorWithRed:85/255.0f green:85/255.0f blue:85/255.0f alpha:1];
//    lbl_foursquare.textColor=[UIColor colorWithRed:243/255.0f green:141/255.0f blue:10/255.0f alpha:1];
//}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)NotificationHome
{
    delegate1.isComeFromLocation = NO;
    [self.navigationController popViewController];
}

-(void)NotificationCame:(id)userInfo
{
    
//    NSString *type= [[[userInfo valueForKey:@"object"] valueForKey:@"aps"]valueForKey:@"type"] ;
//    
//    if ([type isEqualToString:@"5"])
//    {
//        
//    }
//    else if ([type isEqualToString:@"8"])
//    {
//        
//    }
//    else if ([type isEqualToString:@"9"])
//    {
//        
//        
//    }
//    else if ([type isEqualToString:@"12"])
//    {
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        [defaults removeObjectForKey:@"UserDetail"];
//        [defaults removeObjectForKey:@"CurrentLocation"];
//        [defaults synchronize];
//        
//        LoginVC *login_VC = [[LoginVC alloc]init];
//        [self presentViewController:login_VC animated:NO completion:nil];
        // [self.navigationController pushViewController:login_VC animated:YES];
        
        
   // }
    
    
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    
//    if ([annotation isKindOfClass:[MKUserLocation class]]){
//        return nil;
//    }
//    else if ([annotation isKindOfClass:[Annotations class]])
//    {
//        
//        [mapviewlocation removeAnnotations:[mapviewlocation annotations]];
//
//        MKAnnotationView *pinView = nil;
//        
//        static NSString *defaultPinID = @"pin2";
//        
//        pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
//        
//        pinView.frame = CGRectMake(0,0, 20, 30);
//        pinView.backgroundColor = [UIColor clearColor];
//        [pinView setCanShowCallout:YES];
//        
//        UIImageView *pointImageView = [[UIImageView alloc] init];
//        pointImageView.frame = CGRectMake(0, 0, 20, 25);
//        
//        if ([((Annotations *)annotation).title isEqualToString: @"Destination"])
//        {
//            // pointImageView.frame = CGRectMake(0, 0, 20, 25);
//            pinView.image = [UIImage imageNamed:@"MapPin@2x.png"];
//        }
//        else{
//            pinView.image = [UIImage imageNamed:@"MapPin@2x.png"];
//        }
//
//        [pointImageView setUserInteractionEnabled:NO];
//        [pinView addSubview:pointImageView];
//        
//        return pinView;
//    }
//    
//    return nil;
//    
    
    
    if (annotation == mapView.userLocation)
        return nil;
    
    static NSString *s = @"ann";
    MKAnnotationView *pin = [mapView dequeueReusableAnnotationViewWithIdentifier:s];
    if (!pin) {
        pin = [[MKPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:s];
        pin.frame = CGRectMake(0, 0, 15, 15);
        pin.canShowCallout = YES;
        pin.image = [UIImage imageNamed:@"MapPin@2x.png"];
        pin.calloutOffset = CGPointMake(0, 0);
        UIButton *button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        //        [button addTarget:self
        //                   action:@selector(checkinButton) forControlEvents:UIControlEventTouchUpInside];
        pin.rightCalloutAccessoryView = button;
        
    }
    return pin;


}


- (BOOL) shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
