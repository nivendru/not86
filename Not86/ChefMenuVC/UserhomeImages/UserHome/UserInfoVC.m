//
//  UserInfoVC.m
//  Not86
//
//  Created by User on 17/10/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "UserInfoVC.h"
#import "UserSupportVC.h"
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"

@interface UserInfoVC ()<UIScrollViewDelegate>
{
    UIImageView * img_header;
    UIScrollView * scroll;
    UIImageView * img_bg;
}

@end

@implementation UserInfoVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self integrateHeader];
    [self integratebody];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void)integrateHeader
{
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    scroll.frame = CGRectMake(0, 0, WIDTH, 650);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];

    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu .frame = CGRectMake(10, 13,20,20);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu  addTarget:self action:@selector(btn_HomeScreenClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_menu setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_menu ];
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 150, 45);
    lbl_User_Sign_Up.text = @"User";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    
      UIButton *icon_logo = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_logo .frame = CGRectMake(WIDTH-35, 9, 27, 27);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_logo  addTarget:self action:@selector(click_on_logo_btn:) forControlEvents:UIControlEventTouchUpInside];
    [icon_logo setImage:[UIImage imageNamed:@"logo-img@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_logo ];
    
}
-(void)integratebody
{
    
    
    img_bg = [[UIImageView alloc]init];
    img_bg .frame = CGRectMake(0,0, WIDTH, 320);
    [img_bg  setImage:[UIImage imageNamed:@"img-bg-pic-for-user@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg  setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg ];
    
    
    UIButton *user_img = [UIButton buttonWithType:UIButtonTypeCustom];
    user_img.frame = CGRectMake(120,60,120,120);
    user_img.backgroundColor = [UIColor clearColor];
    [user_img addTarget:self action:@selector(btn_img_user_click:) forControlEvents:UIControlEventTouchUpInside];
    [user_img setImage:[UIImage imageNamed:@"img-profile@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:user_img];
    
    UILabel *lbl_user_name = [[UILabel alloc]init];
    lbl_user_name.frame = CGRectMake(140,CGRectGetMaxY(user_img.frame)-5,300,45);
    lbl_user_name.text = @"James Doe";
    lbl_user_name.font = [UIFont fontWithName:kFontBold size:17];
    lbl_user_name.textColor = [UIColor whiteColor];
    lbl_user_name.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_user_name];
    
    
    UILabel *lbl_orders_number = [[UILabel alloc]init];
    lbl_orders_number.frame = CGRectMake(62,CGRectGetMaxY(user_img.frame)+60,70,45);
    lbl_orders_number.text = @"233";
    lbl_orders_number.font = [UIFont fontWithName:kFontBold size:30];
    lbl_orders_number.textColor = [UIColor whiteColor];
    lbl_orders_number.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_orders_number];
    
    UILabel *lbl_orders_made = [[UILabel alloc]init];
    lbl_orders_made.frame = CGRectMake(40,CGRectGetMaxY(lbl_orders_number.frame)-12,150,45);
    lbl_orders_made.text = @"Orders made";
    lbl_orders_made.font = [UIFont fontWithName:kFont size:15];
    lbl_orders_made.textColor = [UIColor whiteColor];
    lbl_orders_made.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_orders_made];
    
    
    
    UIImageView * img_white_line = [[UIImageView alloc]init];
    img_white_line.frame = CGRectMake(CGRectGetMaxX(lbl_orders_number.frame)+45,CGRectGetMaxY(user_img.frame)+75,1, 50);
    [img_white_line setUserInteractionEnabled:YES];
    img_white_line.image=[UIImage imageNamed:@"small-line@2x.png"];
    [img_bg addSubview:img_white_line];
    
    
    UILabel *lbl_review_number = [[UILabel alloc]init];
    lbl_review_number.frame = CGRectMake(CGRectGetMaxX(img_white_line.frame)+75,CGRectGetMaxY(user_img.frame)+60,70,45);
    lbl_review_number.text = @"341";
    lbl_review_number.font = [UIFont fontWithName:kFontBold size:30];
    lbl_review_number.textColor = [UIColor whiteColor];
    lbl_review_number.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_review_number];
    
    
    UILabel *lbl_reviews = [[UILabel alloc]init];
    lbl_reviews.frame = CGRectMake(CGRectGetMaxX(img_white_line.frame)+55,CGRectGetMaxY(lbl_orders_number.frame)-12,150,45);
    lbl_reviews.text = @"Reviews given";
    lbl_reviews.font = [UIFont fontWithName:kFont size:15];
    lbl_reviews.textColor = [UIColor whiteColor];
    lbl_reviews.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_reviews];
    
    UILabel *lbl_desicription = [[UILabel alloc]init];
    lbl_desicription.frame = CGRectMake(10,CGRectGetMaxY(img_bg.frame)+5,350,100);
    lbl_desicription.text = @"Lorem ips olor sit amet, cons elit.Ut ultric d\nissim purus eu scele venenatis et ex et tincidu\nQuisque Lorem ips olor sit amet, cons elit. Ut u\nissim purus eu scele venenatis et ex et tincidu\nQuisque ipsu";
    lbl_desicription.font = [UIFont fontWithName:kFont size:15];
    lbl_desicription.numberOfLines = 5;
    lbl_desicription.textColor = [UIColor blackColor];
    lbl_desicription.backgroundColor = [UIColor clearColor];
    [scroll addSubview:lbl_desicription];
    
    
    UIImageView *white_bg = [[UIImageView alloc]init];
    white_bg.frame = CGRectMake(5,CGRectGetMaxY(lbl_desicription.frame)+5,WIDTH-10,200);
    [white_bg setUserInteractionEnabled:YES];
    white_bg.image=[UIImage imageNamed:@"bg-img@2x.png"];
    [scroll addSubview:white_bg];

    UIImageView *img_favorite = [[UIImageView alloc]init];
    img_favorite.frame = CGRectMake(20,15,30,30);
    [img_favorite setUserInteractionEnabled:YES];
    img_favorite.image=[UIImage imageNamed:@"cooked-icon@2x.png"];
    [white_bg addSubview:img_favorite];
    
    
    UILabel *lbl_favorit = [[UILabel alloc]init];
    lbl_favorit.frame = CGRectMake(CGRectGetMaxX(img_favorite.frame)+25,0,150,45);
    lbl_favorit.text = @"Favorite Cuisines";
    lbl_favorit.font = [UIFont fontWithName:kFontBold size:15];
    lbl_favorit.textColor = [UIColor blackColor];
    lbl_favorit.backgroundColor = [UIColor clearColor];
    [white_bg addSubview:lbl_favorit];
    
    
    UILabel *lbl_favorit_dishes = [[UILabel alloc]init];
    lbl_favorit_dishes.frame = CGRectMake(CGRectGetMaxX(img_favorite.frame)+25,CGRectGetMidY(lbl_favorit.frame),300,45);
    lbl_favorit_dishes.text = @"Chinese, Mexican, Italian";
    lbl_favorit_dishes.font = [UIFont fontWithName:kFont size:15];
    lbl_favorit_dishes.textColor = [UIColor blackColor];
    lbl_favorit_dishes.backgroundColor = [UIColor clearColor];
    [white_bg addSubview:lbl_favorit_dishes];
    
    
    UIImageView *img_line = [[UIImageView alloc]init];
    img_line.frame = CGRectMake(15,CGRectGetMaxY(img_favorite.frame)+20,330,1);
    [img_line setUserInteractionEnabled:YES];
    img_line.image=[UIImage imageNamed:@"line1@2x.png"];
    [white_bg addSubview:img_line];
    
    UIImageView *img_dietary = [[UIImageView alloc]init];
    img_dietary.frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)+15,30,30);
    [img_dietary setUserInteractionEnabled:YES];
    img_dietary.image=[UIImage imageNamed:@"diet-icon@2x.png"];
    [white_bg addSubview:img_dietary];
    
    
    UILabel *lbl_dietary = [[UILabel alloc]init];
    lbl_dietary.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(img_line.frame)-5,150,45);
    lbl_dietary.text = @"Dietary Restrictions";
    lbl_dietary.font = [UIFont fontWithName:kFontBold size:15];
    lbl_dietary.textColor = [UIColor blackColor];
    lbl_dietary.backgroundColor = [UIColor clearColor];
    [white_bg addSubview:lbl_dietary];
    
    
    UILabel *lbl_dietary_restriction = [[UILabel alloc]init];
    lbl_dietary_restriction.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(lbl_dietary.frame),300,45);
    lbl_dietary_restriction.text = @"Vegetarian, Kosher";
    lbl_dietary_restriction.font = [UIFont fontWithName:kFont size:15];
    lbl_dietary_restriction.textColor = [UIColor blackColor];
    lbl_dietary_restriction.backgroundColor = [UIColor clearColor];
    [white_bg addSubview:lbl_dietary_restriction];
    
    
    UIImageView *img_line2 = [[UIImageView alloc]init];
    img_line2.frame = CGRectMake(15,CGRectGetMaxY(img_dietary.frame)+15,330,1);
    [img_line2 setUserInteractionEnabled:YES];
    img_line2.image=[UIImage imageNamed:@"line1@2x.png"];
    [white_bg addSubview:img_line2];
    
    
    UIImageView *img_food_allergies = [[UIImageView alloc]init];
    img_food_allergies.frame = CGRectMake(20,CGRectGetMaxY(img_line2.frame)+15,30,30);
    [img_food_allergies setUserInteractionEnabled:YES];
    img_food_allergies.image=[UIImage imageNamed:@"icon-restict_food@2x.png"];
    [white_bg addSubview:img_food_allergies];
    
    
    UILabel *lbl_allergies = [[UILabel alloc]init];
    lbl_allergies.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(img_line2.frame)-5,150,45);
    lbl_allergies.text = @"Food Allergies";
    lbl_allergies.font = [UIFont fontWithName:kFontBold size:15];
    lbl_allergies.textColor = [UIColor blackColor];
    lbl_allergies.backgroundColor = [UIColor clearColor];
    [white_bg addSubview:lbl_allergies];
    
    
    UILabel *lbl_food_allegies_val = [[UILabel alloc]init];
    lbl_food_allegies_val.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(lbl_allergies.frame),300,45);
    lbl_food_allegies_val.text = @"Nuts, Shellfish";
    lbl_food_allegies_val.font = [UIFont fontWithName:kFont size:15];
    lbl_food_allegies_val.textColor = [UIColor blackColor];
    lbl_food_allegies_val.backgroundColor = [UIColor clearColor];
    [white_bg addSubview:lbl_food_allegies_val];

    UILabel *lbl_message = [[UILabel alloc]init];
    lbl_message.frame = CGRectMake(50,CGRectGetMaxY(white_bg.frame)+10,270, 50);
    lbl_message .text = @"MESSAGE";
    lbl_message .font = [UIFont fontWithName:kFont size:18];
    lbl_message .textColor = [UIColor whiteColor];
    lbl_message .backgroundColor = [UIColor colorWithRed:39/255.0f green:37/255.0f blue:48/255.0f alpha:1];
    lbl_message.layer.cornerRadius = 4.0f;
    lbl_message.clipsToBounds = YES;
    lbl_message.textAlignment = NSTextAlignmentCenter;
    lbl_message.userInteractionEnabled = YES;
    [scroll  addSubview:lbl_message];
    
    
    UIButton *btn_apply = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_apply .backgroundColor = [UIColor clearColor];
    btn_apply.userInteractionEnabled = YES;
    [btn_apply addTarget:self action:@selector(btn_on_message_click:) forControlEvents:UIControlEventTouchUpInside];
    [scroll    addSubview:btn_apply];

    if (IS_IPHONE_6Plus)
    {
        
          scroll.frame = CGRectMake(0, 0, WIDTH, 750);
        
        img_bg .frame = CGRectMake(0,0, WIDTH, 330);
        user_img.frame = CGRectMake(130,70,120,120);
        lbl_user_name.frame = CGRectMake(150,CGRectGetMaxY(user_img.frame)-5,300,45);
        lbl_orders_number.frame = CGRectMake(62,CGRectGetMaxY(user_img.frame)+60,70,45);
        lbl_orders_made.frame = CGRectMake(40,CGRectGetMaxY(lbl_orders_number.frame)-12,150,45);
        img_white_line.frame = CGRectMake(CGRectGetMaxX(lbl_orders_number.frame)+60,CGRectGetMaxY(user_img.frame)+75,1, 50);
        lbl_review_number.frame = CGRectMake(CGRectGetMaxX(img_white_line.frame)+85,CGRectGetMaxY(user_img.frame)+60,70,45);
        lbl_reviews.frame = CGRectMake(CGRectGetMaxX(img_white_line.frame)+65,CGRectGetMaxY(lbl_orders_number.frame)-12,150,45);
        
        lbl_desicription.frame = CGRectMake(10,CGRectGetMaxY(img_bg.frame)+5,410,100);
        white_bg.frame = CGRectMake(5,CGRectGetMaxY(lbl_desicription.frame)+5,WIDTH-10,200);
        img_favorite.frame = CGRectMake(20,15,30,30);
        lbl_favorit.frame = CGRectMake(CGRectGetMaxX(img_favorite.frame)+25,0,150,45);
        lbl_favorit_dishes.frame = CGRectMake(CGRectGetMaxX(img_favorite.frame)+25,CGRectGetMidY(lbl_favorit.frame),300,45);
        img_line.frame = CGRectMake(15,CGRectGetMaxY(img_favorite.frame)+20,360,1);
        img_dietary.frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)+15,30,30);
        lbl_dietary.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(img_line.frame)-5,150,45);
        lbl_dietary_restriction.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(lbl_dietary.frame),300,45);
        img_line2.frame = CGRectMake(15,CGRectGetMaxY(img_dietary.frame)+15,360,1);
        img_food_allergies.frame = CGRectMake(20,CGRectGetMaxY(img_line2.frame)+15,30,30);
        lbl_allergies.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(img_line2.frame)-5,150,45);
        lbl_food_allegies_val.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(lbl_allergies.frame),300,45);
        lbl_message.frame = CGRectMake(60,CGRectGetMaxY(white_bg.frame)+10,270, 50);
        btn_apply .backgroundColor = [UIColor clearColor];
        
           lbl_desicription.font = [UIFont fontWithName:kFont size:16];

    }
    else if (IS_IPHONE_6)
    {
        
          scroll.frame = CGRectMake(0, 0, WIDTH, 650);
        img_bg .frame = CGRectMake(0,0, WIDTH, 320);
        user_img.frame = CGRectMake(120,60,120,120);
        lbl_user_name.frame = CGRectMake(140,CGRectGetMaxY(user_img.frame)-5,300,45);
        lbl_orders_number.frame = CGRectMake(62,CGRectGetMaxY(user_img.frame)+60,70,45);
        lbl_orders_made.frame = CGRectMake(40,CGRectGetMaxY(lbl_orders_number.frame)-12,150,45);
        img_white_line.frame = CGRectMake(CGRectGetMaxX(lbl_orders_number.frame)+45,CGRectGetMaxY(user_img.frame)+75,1, 50);
        lbl_review_number.frame = CGRectMake(CGRectGetMaxX(img_white_line.frame)+75,CGRectGetMaxY(user_img.frame)+60,70,45);
        lbl_reviews.frame = CGRectMake(CGRectGetMaxX(img_white_line.frame)+55,CGRectGetMaxY(lbl_orders_number.frame)-12,150,45);
        lbl_desicription.frame = CGRectMake(10,CGRectGetMaxY(img_bg.frame)+5,350,100);
        white_bg.frame = CGRectMake(5,CGRectGetMaxY(lbl_desicription.frame)+5,WIDTH-10,200);
        img_favorite.frame = CGRectMake(20,15,30,30);
        lbl_favorit.frame = CGRectMake(CGRectGetMaxX(img_favorite.frame)+25,0,150,45);
        lbl_favorit_dishes.frame = CGRectMake(CGRectGetMaxX(img_favorite.frame)+25,CGRectGetMidY(lbl_favorit.frame),300,45);
        img_line.frame = CGRectMake(15,CGRectGetMaxY(img_favorite.frame)+20,330,1);
        img_dietary.frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)+15,30,30);
        lbl_dietary.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(img_line.frame)-5,150,45);
        lbl_dietary_restriction.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(lbl_dietary.frame),300,45);
        img_line2.frame = CGRectMake(15,CGRectGetMaxY(img_dietary.frame)+15,330,1);
        img_food_allergies.frame = CGRectMake(20,CGRectGetMaxY(img_line2.frame)+15,30,30);
        lbl_allergies.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(img_line2.frame)-5,150,45);
        lbl_food_allegies_val.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(lbl_allergies.frame),300,45);
        lbl_message.frame = CGRectMake(50,CGRectGetMaxY(white_bg.frame)+10,270, 50);
        btn_apply .backgroundColor = [UIColor clearColor];

    }
    else if (IS_IPHONE_5)
    {
        
          scroll.frame = CGRectMake(0, 0, WIDTH, 650);
        
        img_bg .frame = CGRectMake(0,0, WIDTH, 290);
        user_img.frame = CGRectMake(100,60,120,120);
        lbl_user_name.frame = CGRectMake(120,CGRectGetMaxY(user_img.frame)-5,300,45);
        lbl_orders_number.frame = CGRectMake(42,CGRectGetMaxY(user_img.frame)+40,70,45);
        lbl_orders_made.frame = CGRectMake(20,CGRectGetMidY(lbl_orders_number.frame)+5,150,45);
        img_white_line.frame = CGRectMake(CGRectGetMaxX(lbl_orders_number.frame)+45,CGRectGetMaxY(user_img.frame)+50,1,45);
        lbl_review_number.frame = CGRectMake(CGRectGetMaxX(img_white_line.frame)+55,CGRectGetMaxY(user_img.frame)+40,70,45);
        lbl_reviews.frame = CGRectMake(CGRectGetMaxX(img_white_line.frame)+35,CGRectGetMidY(lbl_orders_number.frame)+5,150,45);
        lbl_desicription.frame = CGRectMake(10,CGRectGetMaxY(img_bg.frame)+5,350,100);
        white_bg.frame = CGRectMake(5,CGRectGetMaxY(lbl_desicription.frame)+5,WIDTH-10,200);
        img_favorite.frame = CGRectMake(20,15,30,30);
        lbl_favorit.frame = CGRectMake(CGRectGetMaxX(img_favorite.frame)+25,0,150,45);
        lbl_favorit_dishes.frame = CGRectMake(CGRectGetMaxX(img_favorite.frame)+25,CGRectGetMidY(lbl_favorit.frame),300,45);
        img_line.frame = CGRectMake(15,CGRectGetMaxY(img_favorite.frame)+20,260,1);
        img_dietary.frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)+15,30,30);
        lbl_dietary.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(img_line.frame)-5,150,45);
        lbl_dietary_restriction.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(lbl_dietary.frame),300,45);
        img_line2.frame = CGRectMake(15,CGRectGetMaxY(img_dietary.frame)+15,260,1);
        img_food_allergies.frame = CGRectMake(20,CGRectGetMaxY(img_line2.frame)+15,30,30);
        lbl_allergies.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(img_line2.frame)-5,150,45);
        lbl_food_allegies_val.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(lbl_allergies.frame),300,45);
        lbl_message.frame = CGRectMake(35,CGRectGetMaxY(white_bg.frame)+10,250, 50);
        btn_apply .backgroundColor = [UIColor clearColor];
        
        lbl_desicription.font = [UIFont fontWithName:kFont size:13];

 
    }
    else
    {
        
        scroll.frame = CGRectMake(0, 0, WIDTH, 650);
        
        img_bg .frame = CGRectMake(0,0, WIDTH, 290);
        user_img.frame = CGRectMake(100,60,120,120);
        lbl_user_name.frame = CGRectMake(120,CGRectGetMaxY(user_img.frame)-5,300,45);
        lbl_orders_number.frame = CGRectMake(42,CGRectGetMaxY(user_img.frame)+40,70,45);
        lbl_orders_made.frame = CGRectMake(20,CGRectGetMidY(lbl_orders_number.frame)+5,150,45);
        img_white_line.frame = CGRectMake(CGRectGetMaxX(lbl_orders_number.frame)+45,CGRectGetMaxY(user_img.frame)+50,1,45);
        lbl_review_number.frame = CGRectMake(CGRectGetMaxX(img_white_line.frame)+55,CGRectGetMaxY(user_img.frame)+40,70,45);
        lbl_reviews.frame = CGRectMake(CGRectGetMaxX(img_white_line.frame)+35,CGRectGetMidY(lbl_orders_number.frame)+5,150,45);
        lbl_desicription.frame = CGRectMake(10,CGRectGetMaxY(img_bg.frame)+5,350,100);
        white_bg.frame = CGRectMake(5,CGRectGetMaxY(lbl_desicription.frame)+5,WIDTH-10,200);
        img_favorite.frame = CGRectMake(20,15,30,30);
        lbl_favorit.frame = CGRectMake(CGRectGetMaxX(img_favorite.frame)+25,0,150,45);
        lbl_favorit_dishes.frame = CGRectMake(CGRectGetMaxX(img_favorite.frame)+25,CGRectGetMidY(lbl_favorit.frame),300,45);
        img_line.frame = CGRectMake(15,CGRectGetMaxY(img_favorite.frame)+20,260,1);
        img_dietary.frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)+15,30,30);
        lbl_dietary.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(img_line.frame)-5,150,45);
        lbl_dietary_restriction.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(lbl_dietary.frame),300,45);
        img_line2.frame = CGRectMake(15,CGRectGetMaxY(img_dietary.frame)+15,260,1);
        img_food_allergies.frame = CGRectMake(20,CGRectGetMaxY(img_line2.frame)+15,30,30);
        lbl_allergies.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(img_line2.frame)-5,150,45);
        lbl_food_allegies_val.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+25,CGRectGetMidY(lbl_allergies.frame),300,45);
        lbl_message.frame = CGRectMake(35,CGRectGetMaxY(white_bg.frame)+10,250, 50);
        btn_apply .backgroundColor = [UIColor clearColor];
        
        lbl_desicription.font = [UIFont fontWithName:kFont size:13];
        

    }
   //cooked-icon@2x.png

    [scroll setContentSize:CGSizeMake(0,1000)];
   
    

    
}

#pragma mark Click Events

-(void)btn_HomeScreenClick:(UIButton *)sender
{
    NSLog(@"btn_HomeScreenClick");
}
-(void)btn_action_on_serch:(UIButton *)sender
{
    NSLog(@"btn_action_on_serch");
}

-(void)click_on_logo_btn:(UIButton *)sender
{
    NSLog(@"click_on_logo_btn");
    UserSupportVC *vc = [[UserSupportVC alloc]init];
    [self presentViewController:vc animated:NO completion:nil];
}
-(void)btn_img_user_click:(UIButton *)sender
{
    NSLog(@"btn_img_user_click");
}
-(void)btn_on_message_click:(UIButton *)sender
{
    NSLog(@"btn_on_message_click");
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
