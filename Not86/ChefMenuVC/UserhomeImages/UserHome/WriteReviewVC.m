//
//  WriteReviewVC.m
//  Not86
//
//  Created by User on 16/10/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "WriteReviewVC.h"
#import "AppDelegate.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"



@interface WriteReviewVC ()<UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate,UITextViewDelegate>
{
    UIImageView * img_header;
   // UIView * view_for_reviews;
    
    UICollectionViewFlowLayout *  layout;
    UICollectionView * collView_for_dish;
    
    UICollectionViewFlowLayout *  layout2;
    UICollectionView * collView_for_taste_ratings;
  
    
    UICollectionViewFlowLayout *  layout3;
    UICollectionView * collView_for_appeal_ratings;
    
    UICollectionViewFlowLayout *  layout4;
    UICollectionView * collView_for_value_ratings;
    
    UITextView * txt_view_for_special_reasons;
    
   
    NSMutableArray * array_dish_img;
    UIScrollView * scroll;
    
    NSMutableArray * array_numbers;
    
      int selectedindex;
    NSIndexPath *indexSelected;
    
    //===========functionality part
    
    AppDelegate * delegate;
    NSMutableArray * array_for_reviews;
    
    UILabel *lbl_oreder_val ;
    
     NSString  * taste_val;
     NSString *  appeal;
     NSString * value;
    
   }

@end

@implementation WriteReviewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self integrateHeader];
    [self integratebody];
    
    selectedindex=-1;
    indexSelected = nil;

    taste_val = [[NSString alloc]init];
    appeal = [[NSString alloc]init];
    value = [[NSString alloc]init];
    
    taste_val = @"0";
    appeal = @"0";
    value = @"0";
    
    
  
    array_dish_img = [[NSMutableArray alloc]initWithObjects:@"img-dish-bg@2x.png",@"img-dish-bg@2x.png",@"img-dish-bg@2x.png",@"img-dish-bg@2x.png",nil];
   // array_numbers=[NSMutableArray new];
   array_numbers = [[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10", nil];
    // Do any additional setup after loading the view.
}

-(void)integrateHeader
{
//    
//    view_for_reviews = [[UIView alloc]init];
//    view_for_reviews.frame = CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,600);
//    view_for_reviews.backgroundColor=[UIColor redColor];
//    [view_for_reviews setUserInteractionEnabled:YES];
//    [self.view addSubview: view_for_reviews];
    
    
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor purpleColor];
    scroll.frame = CGRectMake(0,0,WIDTH,654);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];

    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [icon_back addTarget:self action:@selector(click_on_back_arrow:) forControlEvents:UIControlEventTouchUpInside];
    [img_header   addSubview:icon_back];
    
    
    UILabel *lbl_support_on_header = [[UILabel alloc]init];
    lbl_support_on_header.frame = CGRectMake(CGRectGetMaxX(icon_back .frame)+30,0, 300, 45);
    lbl_support_on_header.text = @"Write Review";
    lbl_support_on_header.font = [UIFont fontWithName:kFont size:20];
    lbl_support_on_header.textColor = [UIColor whiteColor];
    lbl_support_on_header.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_support_on_header];
    
    UIButton *btn_logo = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_logo.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    btn_logo .backgroundColor = [UIColor clearColor];
    [btn_logo setImage:[UIImage imageNamed:@"logo-img@2x.png"] forState:UIControlStateNormal];
    [btn_logo addTarget:self action:@selector(click_on_logo:) forControlEvents:UIControlEventTouchUpInside];
    [img_header   addSubview:btn_logo];
    
    
    
}
-(void)integratebody
{
    
    
    
    
    layout = [[UICollectionViewFlowLayout alloc] init];
    collView_for_dish = [[UICollectionView alloc] initWithFrame:CGRectMake(0,0,WIDTH,240)
                                                         collectionViewLayout:layout];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_dish setDataSource:self];
    [collView_for_dish setDelegate:self];
    collView_for_dish.scrollEnabled = YES;
    collView_for_dish.showsVerticalScrollIndicator = NO;
    collView_for_dish.showsHorizontalScrollIndicator = NO;
    collView_for_dish.pagingEnabled = YES;
    [collView_for_dish registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_dish setBackgroundColor:[UIColor clearColor]];
    layout.minimumInteritemSpacing = 12;
    layout.minimumLineSpacing = 0;
    collView_for_dish.userInteractionEnabled = YES;
    [scroll  addSubview: collView_for_dish];
    
    UIImageView *white_bg = [[UIImageView alloc]init];
    white_bg.frame = CGRectMake(0,CGRectGetMaxY(collView_for_dish.frame),WIDTH,35);
    [white_bg setUserInteractionEnabled:YES];
    white_bg.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [scroll addSubview:white_bg];
    
    
    UILabel *lbl_oreder_no = [[UILabel alloc]init];
    lbl_oreder_no.frame = CGRectMake(110,-5, 100, 45);
    lbl_oreder_no.text = @"Order on.:";
    lbl_oreder_no.font = [UIFont fontWithName:kFont size:15];
    lbl_oreder_no.textColor = [UIColor blackColor];
    lbl_oreder_no.backgroundColor = [UIColor clearColor];
    [white_bg addSubview:lbl_oreder_no];
    
    
    
    lbl_oreder_val = [[UILabel alloc]init];
    lbl_oreder_val.frame = CGRectMake(CGRectGetMaxX(lbl_oreder_no.frame)-25,-5, 300, 45);
    lbl_oreder_val.text = @"12345";
    lbl_oreder_val.font = [UIFont fontWithName:kFontBold size:15];
    lbl_oreder_val.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    lbl_oreder_val.backgroundColor = [UIColor clearColor];
    [white_bg addSubview:lbl_oreder_val];
    
    UILabel *lbl_Taste= [[UILabel alloc]init];
    lbl_Taste.frame = CGRectMake(150,CGRectGetMaxY(white_bg.frame),100,45);
    lbl_Taste.text = @"Taste";
    lbl_Taste.font = [UIFont fontWithName:kFontBold size:18];
    lbl_Taste.textColor = [UIColor blackColor];
    lbl_Taste.backgroundColor = [UIColor clearColor];
    [scroll addSubview:lbl_Taste];
    
    
    UIImageView *white_bg_for_taste = [[UIImageView alloc]init];
    white_bg_for_taste.frame = CGRectMake(5,CGRectGetMaxY(lbl_Taste.frame),WIDTH-10,50);
    [white_bg_for_taste setUserInteractionEnabled:YES];
    white_bg_for_taste.image=[UIImage imageNamed:@"bg-img@2x.png"];
    [scroll addSubview:white_bg_for_taste];
    
    layout2 = [[UICollectionViewFlowLayout alloc] init];
    collView_for_taste_ratings = [[UICollectionView alloc] initWithFrame:CGRectMake(10,5,IS_IPHONE_6?340:IS_IPHONE_6Plus?370:285,40)
                                           collectionViewLayout:layout2];
    [layout2 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_taste_ratings setDataSource:self];
    [collView_for_taste_ratings setDelegate:self];
    collView_for_taste_ratings.scrollEnabled = YES;
    collView_for_taste_ratings.showsVerticalScrollIndicator = NO;
    collView_for_taste_ratings.showsHorizontalScrollIndicator = NO;
    collView_for_taste_ratings.pagingEnabled = YES;
    [collView_for_taste_ratings registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_taste_ratings setBackgroundColor:[UIColor redColor]];
    layout2.minimumInteritemSpacing = 12;
    layout2.minimumLineSpacing = 0;
    collView_for_taste_ratings.userInteractionEnabled = YES;
    [white_bg_for_taste  addSubview: collView_for_taste_ratings];
    
    
    UILabel *lbl_appeal = [[UILabel alloc]init];
    lbl_appeal.frame = CGRectMake(150,CGRectGetMaxY(white_bg_for_taste.frame),100,45);
    lbl_appeal.text = @"Appeal";
    lbl_appeal.font = [UIFont fontWithName:kFontBold size:18];
    lbl_appeal.textColor = [UIColor blackColor];
    lbl_appeal.backgroundColor = [UIColor clearColor];
    [scroll addSubview:lbl_appeal];
    
    UIImageView *white_bg_for_appeal = [[UIImageView alloc]init];
    white_bg_for_appeal.frame = CGRectMake(5,CGRectGetMaxY(lbl_appeal.frame),WIDTH-10,50);
    [white_bg_for_appeal setUserInteractionEnabled:YES];
    white_bg_for_appeal.image=[UIImage imageNamed:@"bg-img@2x.png"];
    [scroll addSubview:white_bg_for_appeal];
    
    layout3 = [[UICollectionViewFlowLayout alloc] init];
    collView_for_appeal_ratings = [[UICollectionView alloc] initWithFrame:CGRectMake(10,5,IS_IPHONE_6?340:IS_IPHONE_6Plus?370:285,40)
                                                    collectionViewLayout:layout3];
    [layout3 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_appeal_ratings setDataSource:self];
    [collView_for_appeal_ratings setDelegate:self];
    collView_for_appeal_ratings.scrollEnabled = YES;
    collView_for_appeal_ratings.showsVerticalScrollIndicator = NO;
    collView_for_appeal_ratings.showsHorizontalScrollIndicator = NO;
    collView_for_appeal_ratings.pagingEnabled = YES;
    [collView_for_appeal_ratings registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_appeal_ratings setBackgroundColor:[UIColor redColor]];
    layout3.minimumInteritemSpacing = 5;
    layout3.minimumLineSpacing = 0;
    collView_for_appeal_ratings.userInteractionEnabled = YES;
    [white_bg_for_appeal  addSubview: collView_for_appeal_ratings];
    
    
    
    UILabel *lbl_value = [[UILabel alloc]init];
    lbl_value.frame = CGRectMake(150,CGRectGetMaxY(white_bg_for_appeal.frame),100,45);
    lbl_value.text = @"Value";
    lbl_value.font = [UIFont fontWithName:kFontBold size:18];
    lbl_value.textColor = [UIColor blackColor];
    lbl_value.backgroundColor = [UIColor clearColor];
    [scroll addSubview:lbl_value];
    
    
    UIImageView *white_bg_for_value = [[UIImageView alloc]init];
    white_bg_for_value.frame = CGRectMake(5,CGRectGetMaxY(lbl_value.frame),WIDTH-10,50);
    [white_bg_for_value setUserInteractionEnabled:YES];
    white_bg_for_value.image=[UIImage imageNamed:@"bg-img@2x.png"];
    [scroll addSubview:white_bg_for_value];
    
    layout4 = [[UICollectionViewFlowLayout alloc] init];
    collView_for_value_ratings = [[UICollectionView alloc] initWithFrame:CGRectMake(10,5,IS_IPHONE_6?340:IS_IPHONE_6Plus?370:285,40)
                                                     collectionViewLayout:layout4];
    [layout4 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_value_ratings setDataSource:self];
    [collView_for_value_ratings setDelegate:self];
    collView_for_value_ratings.scrollEnabled = YES;
    collView_for_value_ratings.showsVerticalScrollIndicator = NO;
    collView_for_value_ratings.showsHorizontalScrollIndicator = NO;
    collView_for_value_ratings.pagingEnabled = YES;
    [collView_for_value_ratings registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_value_ratings setBackgroundColor:[UIColor redColor]];
    layout4.minimumInteritemSpacing = 5;
    layout4.minimumLineSpacing = 0;
    collView_for_value_ratings.userInteractionEnabled = YES;
    [white_bg_for_value  addSubview: collView_for_value_ratings];
    
    
    
    UILabel *lbl_review = [[UILabel alloc]init];
    lbl_review.frame = CGRectMake(150,CGRectGetMaxY(white_bg_for_value.frame),100,45);
    lbl_review.text = @"Review";
    lbl_review.font = [UIFont fontWithName:kFontBold size:18];
    lbl_review.textColor = [UIColor blackColor];
    lbl_review.backgroundColor = [UIColor clearColor];
    [scroll addSubview:lbl_review];
    
    
    UIImageView *white_bg_for_review = [[UIImageView alloc]init];
    white_bg_for_review.frame = CGRectMake(5,CGRectGetMaxY(lbl_review.frame),WIDTH-10,170);
    [white_bg_for_review setUserInteractionEnabled:YES];
    white_bg_for_review.image=[UIImage imageNamed:@"bg-img@2x.png"];
    [scroll addSubview:white_bg_for_review];
    
    txt_view_for_special_reasons =[[UITextView alloc]init];
    txt_view_for_special_reasons .frame = CGRectMake(20,15, WIDTH-55, 120);
    txt_view_for_special_reasons.scrollEnabled=YES;
    txt_view_for_special_reasons.text =@"";
    txt_view_for_special_reasons.userInteractionEnabled=YES;
    txt_view_for_special_reasons.font=[UIFont fontWithName:kFont size:14];
    txt_view_for_special_reasons.backgroundColor=[UIColor whiteColor];
    txt_view_for_special_reasons.delegate=self;
    txt_view_for_special_reasons.textColor=[UIColor lightGrayColor];
    txt_view_for_special_reasons.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    txt_view_for_special_reasons.layer.borderWidth=1.0f;
    txt_view_for_special_reasons.clipsToBounds=YES;
    [white_bg_for_review addSubview: txt_view_for_special_reasons];
    
    UILabel *lbl_max_wordes = [[UILabel alloc]init];
    lbl_max_wordes.frame = CGRectMake(253,CGRectGetMaxY(txt_view_for_special_reasons.frame)-15, 100, 45);
    lbl_max_wordes.text = @"(500 words max.)";
    lbl_max_wordes.font = [UIFont fontWithName:kFont size:11];
    lbl_max_wordes.textColor = [UIColor blackColor];
    lbl_max_wordes.backgroundColor = [UIColor clearColor];
    [white_bg_for_review addSubview:lbl_max_wordes];

//    UIButton *btn_post = [UIButton buttonWithType:UIButtonTypeCustom];
//    btn_post.frame = CGRectMake(20,CGRectGetMaxY(white_bg_for_review.frame),300, 50);
//    btn_post .backgroundColor = [UIColor clearColor];
//    [btn_post setImage:[UIImage imageNamed:@"btn-check-out@2x.png"]forState:UIControlStateNormal];
//    [btn_post addTarget:self action:@selector(click_on_post_btn:) forControlEvents:UIControlEventTouchUpInside];
//    //[scroll  addSubview:btn_post];
    
    
//    UIButton *btn_apply = [UIButton buttonWithType:UIButtonTypeCustom];
//    btn_apply .backgroundColor = [UIColor yellowColor];
//    btn_apply.userInteractionEnabled = YES;
//    [btn_apply addTarget:self action:@selector(btn_post_click:) forControlEvents:UIControlEventTouchUpInside];
//    [scroll    addSubview:btn_apply];
    
    UIButton *btn_post = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_post.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    btn_post .backgroundColor = [UIColor clearColor];
    [btn_post setImage:[UIImage imageNamed:@"img-black-btn@2x.png"] forState:UIControlStateNormal];
    [btn_post addTarget:self action:@selector(btn_post_click:) forControlEvents:UIControlEventTouchUpInside];
    [scroll   addSubview:btn_post];
    
    UILabel *lbl_post = [[UILabel alloc]init];
    lbl_post.frame = CGRectMake(60,5,60,45);
    lbl_post.text = @"post";
    lbl_post.font = [UIFont fontWithName:kFontBold size:18];
    lbl_post.textColor = [UIColor whiteColor];
    lbl_post.backgroundColor = [UIColor clearColor];
    [btn_post addSubview:lbl_post];

    
    
    
    

    if (IS_IPHONE_6Plus)
    {
         scroll.frame = CGRectMake(0,0,WIDTH,725);
        
        white_bg.frame = CGRectMake(0,CGRectGetMaxY(collView_for_dish.frame),WIDTH,35);
        lbl_oreder_no.frame = CGRectMake(140,-5, 100, 45);
        
        lbl_oreder_val.frame = CGRectMake(CGRectGetMaxX(lbl_oreder_no.frame)-25,-5, 300, 45);
        lbl_Taste.frame = CGRectMake(175,CGRectGetMaxY(white_bg.frame),100,45);
        white_bg_for_taste.frame = CGRectMake(5,CGRectGetMaxY(lbl_Taste.frame),WIDTH-10,50);
        lbl_appeal.frame = CGRectMake(175,CGRectGetMaxY(white_bg_for_taste.frame),100,45);
        white_bg_for_appeal.frame = CGRectMake(5,CGRectGetMaxY(lbl_appeal.frame),WIDTH-10,50);
        lbl_value.frame = CGRectMake(175,CGRectGetMaxY(white_bg_for_appeal.frame),100,45);
        white_bg_for_value.frame = CGRectMake(5,CGRectGetMaxY(lbl_value.frame),WIDTH-10,50);
        lbl_review.frame = CGRectMake(175,CGRectGetMaxY(white_bg_for_value.frame),100,45);
        white_bg_for_review.frame = CGRectMake(5,CGRectGetMaxY(lbl_review.frame),WIDTH-10,170);
        txt_view_for_special_reasons .frame = CGRectMake(20,15, WIDTH-55, 120);
        lbl_max_wordes.frame = CGRectMake(290,CGRectGetMaxY(txt_view_for_special_reasons.frame)-15, 100, 45);
        
        
        btn_post.frame = CGRectMake(40,CGRectGetMaxY(white_bg_for_review.frame)+40,WIDTH-80, 50);
        lbl_post.frame = CGRectMake(WIDTH/2-60,5,60,45);

        
    }
    else if (IS_IPHONE_6)
    {
         scroll.frame = CGRectMake(0,0,WIDTH,654);
        
        white_bg.frame = CGRectMake(0,CGRectGetMaxY(collView_for_dish.frame),WIDTH,35);
        lbl_oreder_no.frame = CGRectMake(110,-5, 100, 45);
        
        lbl_oreder_val.frame = CGRectMake(CGRectGetMaxX(lbl_oreder_no.frame)-25,-5, 300, 45);
        lbl_Taste.frame = CGRectMake(150,CGRectGetMaxY(white_bg.frame),100,45);
        white_bg_for_taste.frame = CGRectMake(5,CGRectGetMaxY(lbl_Taste.frame),WIDTH-10,50);
        lbl_appeal.frame = CGRectMake(150,CGRectGetMaxY(white_bg_for_taste.frame),100,45);
        white_bg_for_appeal.frame = CGRectMake(5,CGRectGetMaxY(lbl_appeal.frame),WIDTH-10,50);
        lbl_value.frame = CGRectMake(150,CGRectGetMaxY(white_bg_for_appeal.frame),100,45);
        white_bg_for_value.frame = CGRectMake(5,CGRectGetMaxY(lbl_value.frame),WIDTH-10,50);
        lbl_review.frame = CGRectMake(150,CGRectGetMaxY(white_bg_for_value.frame),100,45);
        white_bg_for_review.frame = CGRectMake(5,CGRectGetMaxY(lbl_review.frame),WIDTH-10,170);
        txt_view_for_special_reasons .frame = CGRectMake(20,15, WIDTH-55, 120);
        lbl_max_wordes.frame = CGRectMake(253,CGRectGetMaxY(txt_view_for_special_reasons.frame)-15, 100, 45);
        

        btn_post.frame = CGRectMake(30,CGRectGetMaxY(white_bg_for_review.frame)+40,WIDTH-60, 50);
        lbl_post.frame = CGRectMake(WIDTH/2-50,5,60,45);
        
    }
    else if (IS_IPHONE_5)
    {
         scroll.frame = CGRectMake(0,0,WIDTH,555);
        
        white_bg.frame = CGRectMake(0,CGRectGetMaxY(collView_for_dish.frame),WIDTH,35);
        lbl_oreder_no.frame = CGRectMake(100,-5, 100, 45);
        
        lbl_oreder_val.frame = CGRectMake(CGRectGetMaxX(lbl_oreder_no.frame)-25,-5, 300, 45);
        lbl_Taste.frame = CGRectMake(140,CGRectGetMaxY(white_bg.frame),100,45);
        white_bg_for_taste.frame = CGRectMake(5,CGRectGetMaxY(lbl_Taste.frame),WIDTH-10,50);
        lbl_appeal.frame = CGRectMake(140,CGRectGetMaxY(white_bg_for_taste.frame),100,45);
        white_bg_for_appeal.frame = CGRectMake(5,CGRectGetMaxY(lbl_appeal.frame),WIDTH-10,50);
        lbl_value.frame = CGRectMake(140,CGRectGetMaxY(white_bg_for_appeal.frame),100,45);
        white_bg_for_value.frame = CGRectMake(5,CGRectGetMaxY(lbl_value.frame),WIDTH-10,50);
        lbl_review.frame = CGRectMake(140,CGRectGetMaxY(white_bg_for_value.frame),100,45);
        white_bg_for_review.frame = CGRectMake(5,CGRectGetMaxY(lbl_review.frame),WIDTH-10,170);
        txt_view_for_special_reasons .frame = CGRectMake(20,15, WIDTH-55, 120);
        lbl_max_wordes.frame = CGRectMake(203,CGRectGetMaxY(txt_view_for_special_reasons.frame)-15, 100, 45);
        

        btn_post.frame = CGRectMake(30,CGRectGetMaxY(white_bg_for_review.frame),WIDTH-60, 50);
        lbl_post.frame = CGRectMake(WIDTH/2-40,5,60,45);
        
    }
    else
    {
        scroll.frame = CGRectMake(0,0,WIDTH,469);
        
        white_bg.frame = CGRectMake(0,CGRectGetMaxY(collView_for_dish.frame),WIDTH,35);
        lbl_oreder_no.frame = CGRectMake(100,-5, 100, 45);
        
        lbl_oreder_val.frame = CGRectMake(CGRectGetMaxX(lbl_oreder_no.frame)-25,-5, 300, 45);
        lbl_Taste.frame = CGRectMake(140,CGRectGetMaxY(white_bg.frame),100,45);
        white_bg_for_taste.frame = CGRectMake(5,CGRectGetMaxY(lbl_Taste.frame),WIDTH-10,50);
        lbl_appeal.frame = CGRectMake(140,CGRectGetMaxY(white_bg_for_taste.frame),100,45);
        white_bg_for_appeal.frame = CGRectMake(5,CGRectGetMaxY(lbl_appeal.frame),WIDTH-10,50);
        lbl_value.frame = CGRectMake(140,CGRectGetMaxY(white_bg_for_appeal.frame),100,45);
        white_bg_for_value.frame = CGRectMake(5,CGRectGetMaxY(lbl_value.frame),WIDTH-10,50);
        lbl_review.frame = CGRectMake(140,CGRectGetMaxY(white_bg_for_value.frame),100,45);
        white_bg_for_review.frame = CGRectMake(5,CGRectGetMaxY(lbl_review.frame),WIDTH-10,170);
        txt_view_for_special_reasons .frame = CGRectMake(20,15, WIDTH-55, 120);
        lbl_max_wordes.frame = CGRectMake(203,CGRectGetMaxY(txt_view_for_special_reasons.frame)-15, 100, 45);
        

        
        btn_post.frame = CGRectMake(30,CGRectGetMaxY(white_bg_for_review.frame),WIDTH-60, 50);
        lbl_post.frame = CGRectMake(WIDTH/2-40,5,60,45);
        
        
    }



    
     [scroll setContentSize:CGSizeMake(0,1000)];
    
}

#pragma mark UiCollectionView Delegate Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView ==  collView_for_dish)
    {
         return [array_dish_img count];
    }
    else if (collectionView ==  collView_for_taste_ratings)
    {
        return 10;
    }
    else if (collectionView ==  collView_for_appeal_ratings)
    {
        return 10;
    }
    else if (collectionView == collView_for_value_ratings)
    {
        return 10;
    }
    return 0;
   
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    
    if (collectionView ==  collView_for_dish)
    {
       return 1;
    }
    else if (collectionView ==  collView_for_taste_ratings)
    {
        return 1;
    }
    else if (collectionView ==  collView_for_appeal_ratings)
    {
        return 1;
    }
    else if (collectionView == collView_for_value_ratings)
    {
        return 1;
    }

    return 1;

}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView1 dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    cell.backgroundColor=[UIColor clearColor];
    
    if (collectionView1 == collView_for_dish)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH,250);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH+5, 250);
            
        }
        else
        {    img_cellBackGnd.frame =  CGRectMake(0,10, WIDTH,140);
            
        }
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        
         UIImageView * img_dish = [[UIImageView alloc] init];
        img_dish.frame = CGRectMake(0,0, WIDTH,  250 );
        [img_dish setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_dish_img objectAtIndex:indexPath.row]]]];
        //   NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[array_chef_img objectAtIndex:indexPath.row] valueForKey:@""]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //  [img_chef setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory]placeholderImage:[UIImage imageNamed:@"img_profile@2x.png"]];
        [img_dish setContentMode:UIViewContentModeScaleAspectFill];
        [img_dish setClipsToBounds:YES];
        [img_cellBackGnd addSubview:img_dish];
        

    }
    else if (collectionView1 == collView_for_taste_ratings)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, 60,50);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(-4,0, 53,40);
            
        }
        else
        {    img_cellBackGnd.frame =  CGRectMake(0,0, 45,40);
            
        }
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UILabel * lbl_numbers_in_taste = [[UILabel alloc]init];
        lbl_numbers_in_taste.frame = CGRectMake(19,0,45,45);
        lbl_numbers_in_taste.text =[NSString stringWithFormat:@"%@",[array_numbers objectAtIndex:indexPath.row]];
    //   lbl_numbers_in_taste .text = [NSString stringWithFormat:@"%@",[[array_numbers objectAtIndex:indexPath.row] valueForKey:@""]];
        lbl_numbers_in_taste.font = [UIFont fontWithName:kFontBold size:18];
        lbl_numbers_in_taste.textColor = [UIColor colorWithRed:14/255.0f green:27/255.0f blue:71/255.0f alpha:1];
        lbl_numbers_in_taste.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_numbers_in_taste];
        
        
      
        UIImageView  *icon_circle = [[UIImageView alloc]init];
        icon_circle.frame = CGRectMake(8,3,35,35);
        if (indexPath.row == 9)
        {
            icon_circle.frame = CGRectMake(12,3,35,35);
        }
        else
        {
            icon_circle.frame = CGRectMake(8,3,35,35);
        }

        [icon_circle setImage:[UIImage imageNamed:@"circle-icon@2x.png"]];
        icon_circle .backgroundColor = [UIColor clearColor];
        [icon_circle setUserInteractionEnabled:YES];
        [img_cellBackGnd  addSubview:icon_circle];

        if (indexPath.row==selectedindex)
        {
            icon_circle.hidden=NO;
            
            
          
        }
        else
        {
            icon_circle.hidden=YES;
            
        }
        
        
     
    }
    else if (collectionView1 ==  collView_for_appeal_ratings)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, 60,50);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(-4,0, 53,40);
            
        }
        else
        {    img_cellBackGnd.frame =  CGRectMake(0,0, 45,40);
            
        }
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        
        UILabel * lbl_numbers_in_taste = [[UILabel alloc]init];
        lbl_numbers_in_taste.frame = CGRectMake(19,0,45,45);
        lbl_numbers_in_taste.text =[NSString stringWithFormat:@"%@",[array_numbers objectAtIndex:indexPath.row]];
        //   lbl_numbers_in_taste .text = [NSString stringWithFormat:@"%@",[[array_numbers objectAtIndex:indexPath.row] valueForKey:@""]];
        lbl_numbers_in_taste.font = [UIFont fontWithName:kFontBold size:18];
        lbl_numbers_in_taste.textColor = [UIColor colorWithRed:14/255.0f green:27/255.0f blue:71/255.0f alpha:1];
        lbl_numbers_in_taste.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_numbers_in_taste];
        
        
        
        UIImageView  *icon_circle = [[UIImageView alloc]init];
        icon_circle.frame = CGRectMake(8,3,35,35);
        if (indexPath.row == 9)
        {
            icon_circle.frame = CGRectMake(12,3,35,35);
        }
        else
        {
            icon_circle.frame = CGRectMake(8,3,35,35);
        }
        
        [icon_circle setImage:[UIImage imageNamed:@"circle-icon@2x.png"]];
        icon_circle .backgroundColor = [UIColor clearColor];
        [icon_circle setUserInteractionEnabled:YES];
        [img_cellBackGnd  addSubview:icon_circle];
        
        if (indexPath.row==selectedindex)
        {
            icon_circle.hidden=NO;
            
            
            
        }
        else
        {
            icon_circle.hidden=YES;
            
        }

    }

    else if (collectionView1 == collView_for_value_ratings)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, 60,50);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(-4,0,53,40);
            
        }
        else
        {    img_cellBackGnd.frame =  CGRectMake(0,0, 45,40);
            
        }
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        
        UILabel * lbl_numbers_in_taste = [[UILabel alloc]init];
        lbl_numbers_in_taste.frame = CGRectMake(19,0,45,45);
        lbl_numbers_in_taste.text =[NSString stringWithFormat:@"%@",[array_numbers objectAtIndex:indexPath.row]];
        //   lbl_numbers_in_taste .text = [NSString stringWithFormat:@"%@",[[array_numbers objectAtIndex:indexPath.row] valueForKey:@""]];
        lbl_numbers_in_taste.font = [UIFont fontWithName:kFontBold size:18];
        lbl_numbers_in_taste.textColor = [UIColor colorWithRed:14/255.0f green:27/255.0f blue:71/255.0f alpha:1];
        lbl_numbers_in_taste.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_numbers_in_taste];
        
        
        
        UIImageView  *icon_circle = [[UIImageView alloc]init];
        icon_circle.frame = CGRectMake(8,3,35,35);
        if (indexPath.row == 9)
        {
            icon_circle.frame = CGRectMake(12,3,35,35);
        }
        else
        {
            icon_circle.frame = CGRectMake(8,3,35,35);
        }
        
        [icon_circle setImage:[UIImage imageNamed:@"circle-icon@2x.png"]];
        icon_circle .backgroundColor = [UIColor clearColor];
        [icon_circle setUserInteractionEnabled:YES];
        [img_cellBackGnd  addSubview:icon_circle];
        
        if (indexPath.row==selectedindex)
        {
            icon_circle.hidden=NO;
            
            
            
        }
        else
        {
            icon_circle.hidden=YES;
            
        }
    }

    
      return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView ==  collView_for_dish)
    {
        if (IS_IPHONE_6Plus)
        {
             return CGSizeMake(WIDTH,250);
        }
        else if (IS_IPHONE_6)
        {
             return CGSizeMake(WIDTH,250);
        }
        else if (IS_IPHONE_5)
        {
             return CGSizeMake(WIDTH,250);
        }
        else
        {
             return CGSizeMake(WIDTH,250);
        }
       return CGSizeMake(WIDTH,250);
    }
    else if (collectionView ==  collView_for_taste_ratings)
    {
        if (IS_IPHONE_6Plus)
        {
           return CGSizeMake(WIDTH/7,40);
        }
        else if (IS_IPHONE_6)
        {
            return CGSizeMake(WIDTH/7,40);
        }
        else if (IS_IPHONE_5)
        {
            return CGSizeMake(WIDTH/7,40);
        }
        else
        {
            return CGSizeMake(WIDTH/7,40);
        }

        return CGSizeMake(WIDTH/7,40);
    }

    else if (collectionView ==  collView_for_appeal_ratings)
    {
        if (IS_IPHONE_6Plus)
        {
            return CGSizeMake(WIDTH/7,40);
        }
        else if (IS_IPHONE_6)
        {
            return CGSizeMake(WIDTH/7,40);
        }
        else if (IS_IPHONE_5)
        {
            return CGSizeMake(WIDTH/7,40);
        }
        else
        {
            return CGSizeMake(WIDTH/7,40);
        }
         return CGSizeMake(WIDTH/7,40);
    }
    else if (collectionView == collView_for_value_ratings)
    {
        if (IS_IPHONE_6Plus)
        {
            return CGSizeMake(WIDTH/7,40);
        }
        else if (IS_IPHONE_6)
        {
            return CGSizeMake(WIDTH/7,40);

        }
        else if (IS_IPHONE_5)
        {
            return CGSizeMake(WIDTH/7,40);

        }
        else
        {
            return CGSizeMake(WIDTH/7,40);

        }

        return CGSizeMake(WIDTH/7,40);
    }

    return CGSizeMake(WIDTH,250);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
        selectedindex= (int)indexPath.row;
    
    
    if (collectionView == collView_for_taste_ratings)
    {
        if (indexPath.row==selectedindex)
        {
           
            
            indexSelected = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
        }
        taste_val = [NSString stringWithFormat:@"%ld",indexPath.row+1];
        
       [collView_for_taste_ratings reloadData];
    }
    else if (collectionView == collView_for_appeal_ratings)
    {
        if (indexPath.row==selectedindex)
        {
            
            indexSelected = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
        }
        
        appeal = [NSString stringWithFormat:@"%ld",indexPath.row+1];
        
        [collView_for_appeal_ratings reloadData];
    }
    else if (collectionView == collView_for_value_ratings)
    {
        if (indexPath.row==selectedindex)
        {
            indexSelected = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
        }
        value = [NSString stringWithFormat:@"%ld",indexPath.row+1];
        [collView_for_value_ratings reloadData];
    }

    
    
}

#pragma mark Textview Delegate


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    
    if (txt_view_for_special_reasons.text.length>500)
    {
        return NO;
    }
    
    return YES;
}


#pragma click_events

-(void)click_on_back_arrow:(UIButton *)sender
{
    NSLog(@"click_on_back_arrow");
}
-(void)click_on_logo:(UIButton *)sender
{
    NSLog(@"click_on_logo");
}
-(void)click_on_post_btn:(UIButton *)sender
{
    NSLog(@"click_on_post_btn");
}
-(void)btn_post_click:(UIButton *)sender
{
    NSLog(@"btn_post_click");
    
    
    if ([taste_val isEqualToString:@"0"])
    {
        
    }
    else if ([appeal isEqualToString:@"0"])
    {
        
    }
    else if ([value isEqualToString:@"0"])
    {
        
    }
    else{
        [self write_reviews];

    }
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma functionality


-(void)write_reviews
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    NSDictionary *params =@{
                            
                            
                            @"uid"               :  @"477",
                            @"dish_id"           :  @"3",
                            @"order_id"          :  lbl_oreder_val.text,
                            @"taste"             :  taste_val,
                            @"appeal"            :  appeal,
                            @"value"             :  value,
                            @"review"            :  txt_view_for_special_reasons.text,
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path: kwritereviews
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpCuisineList:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self write_reviews];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpCuisineList :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        txt_popular.text=@"";
        
        
    }
    
    // [tab reloadData];
    
    //    [self AlergyList];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
