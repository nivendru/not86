//
//  ProceedtopayVC.m
//  Not86
//
//  Created by apple on 03/12/15.
//  Copyright © 2015 com.interworld. All rights reserved.
//

#import "ProceedtopayVC.h"

@interface ProceedtopayVC ()
{
    UIImageView*img_header;
    
}

@end

@implementation ProceedtopayVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self intgegratebody];
    
    
}
-(void)integrateHeader
{
    
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu .frame = CGRectMake(10, 13,20,20);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu  addTarget:self action:@selector(click_on_Back_Btn:)forControlEvents:UIControlEventTouchUpInside];
    //    [icon_menu setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
    icon_menu.layer.borderWidth = 1.0;
    [icon_menu setBackgroundColor:[UIColor whiteColor]];
    [img_header   addSubview:icon_menu ];
    
    
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 150, 45);
    lbl_User_Sign_Up.text = @"Dining Cart";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-35, 9, 27, 27);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
    
}

-(void)intgegratebody
{
    UIImageView *img_bg = [[UIImageView alloc]init];
    img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 440);
    //    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    img_bg.layer.borderWidth = 1.0;
    [img_bg setUserInteractionEnabled:YES];
    [self.view addSubview:img_bg];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(10, 10, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"icon-now@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_bg addSubview:icon_user];
    
    
    UILabel *lbl_Head = [[UILabel alloc]init];
    lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
    lbl_Head.text = @"Food Now";
    lbl_Head.font = [UIFont fontWithName:kFont size:20];
    lbl_Head.textColor = [UIColor blackColor];
    lbl_Head.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_Head];
    
    
    UIImageView *line = [[UIImageView alloc]init];
    line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
    line.backgroundColor = [UIColor grayColor];
    [line setUserInteractionEnabled:YES];
    [img_bg addSubview:line];
    
    
//    img_table= [[UITableView alloc] init ];
//    img_table.frame  = CGRectMake(0,CGRectGetMaxY(line.frame),WIDTH-10,390);
//    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//    img_table.delegate = self;
//    img_table.dataSource = self;
//    img_table.showsVerticalScrollIndicator = NO;
//    //    img_table.layer.borderWidth = 1.0;
//    img_table.backgroundColor = [UIColor clearColor];
//    [img_bg addSubview:img_table];
    

}
-(void)click_on_Back_Btn: (UIButton *)sender

{
    NSLog(@"click_on_Back_Btn Click");
    [self dismissViewControllerAnimated:NO completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
