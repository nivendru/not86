//
//  ViewDishVC.m
//  Not86
//
//  Created by Admin on 01/09/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ViewDishVC.h"
#import "Define.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"

@interface ViewDishVC ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    UIImageView * img_header;
    UIScrollView * scroll;
    UITableView *  img_table;
    
    NSMutableArray * array_serving_time_date;
    NSMutableArray * array_dish_names;
    NSMutableArray * array_dish_imges;
    NSMutableArray * array_icon_dietary;
    NSMutableArray * array_icon_serving_type;
    NSMutableArray * array_quantity;
    NSMutableArray * array_serving_charge;
    NSMutableArray * array_sub_total;
    
    UIView *view_for_popup;
    UITableView * img_table_for_popup;
}

@end

@implementation ViewDishVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBodyDesign];
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    
    array_serving_time_date = [[NSMutableArray alloc]initWithObjects:@"Now",@"Now",@"Now",@"Now",nil];
    array_dish_imges = [[NSMutableArray alloc]initWithObjects:@"dish2-img@2x.png",@"dish2-img@2x.png",@"dish2-img@2x.png",@"dish2-img@2x.png",nil];
    array_dish_names = [[NSMutableArray alloc]initWithObjects:@"Spicy Chicken Salad",@"Raspberry Custard",@"Spicy Chicken Salad",@"Raspberry Custard",nil];
    array_icon_dietary = [[NSMutableArray alloc]initWithObjects:@"halal-icon@2x.png",@"halal-icon@2x.png",@"halal-icon@2x.png",@"halal-icon@2x.png",nil];
    array_icon_serving_type = [[NSMutableArray alloc]initWithObjects:@"take-icon@2x.png",@"take-icon@2x.png",@"take-icon@2x.png",@"take-icon@2x.png",nil];
    array_quantity =  [[NSMutableArray alloc]initWithObjects:@"3",@"3",@"3",@"3",nil];
    array_serving_charge = [[NSMutableArray alloc]initWithObjects:@"$5.50",@"$5.50",@"$5.50",@"$5.50",nil];
    array_sub_total = [[NSMutableArray alloc]initWithObjects:@"$16.50",@"$16.50",@"$16.50",@"$16.50",nil];
    
    [self popup_cancel_order];
    
    
}

-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [icon_back addTarget:self action:@selector(click_back_arrow:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:icon_back];
    
    
    UILabel *lbl_order_details = [[UILabel alloc]init];
    lbl_order_details.frame = CGRectMake(CGRectGetMaxX(icon_back .frame)+30,0, 200, 45);
    lbl_order_details.text = @"Dining Cart";
    lbl_order_details.font = [UIFont fontWithName:kFont size:20];
    lbl_order_details.textColor = [UIColor whiteColor];
    lbl_order_details.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_order_details];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}
-(void)integrateBodyDesign
{
    scroll = [[UIScrollView alloc]init];
    scroll.frame = CGRectMake(5, 48, WIDTH-10, HEIGHT-130);
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor whiteColor];
   // scroll.frame = CGRectMake(0, 46, WIDTH, HEIGHT+150);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    
    UIImageView *icon_now = [[UIImageView alloc]init];
    icon_now.frame = CGRectMake(20, 15, 30, 30);
    [icon_now setImage:[UIImage imageNamed:@"now@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_now setUserInteractionEnabled:YES];
    [scroll addSubview:icon_now];
    
    UILabel *lbl_food_now = [[UILabel alloc]init];
    lbl_food_now.frame = CGRectMake(CGRectGetMaxX(icon_now.frame)+10,05, 200, 45);
    lbl_food_now.text = @"Food Now";
    lbl_food_now.font = [UIFont fontWithName:kFontBold size:15];
    lbl_food_now.textColor = [UIColor blackColor];
    lbl_food_now.backgroundColor = [UIColor clearColor];
    [scroll addSubview:lbl_food_now];

    
    UIImageView *img_line = [[UIImageView alloc]init];
    img_line.frame = CGRectMake(20, CGRectGetMaxY(icon_now.frame)+8, WIDTH-40, 0.5);
    [img_line setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_line setUserInteractionEnabled:YES];
    [scroll addSubview:img_line];
    
    UILabel *lbl_Does_kitchen = [[UILabel alloc]init];
    lbl_Does_kitchen.frame = CGRectMake(20,CGRectGetMaxY(img_line.frame)-5, 200, 45);
    lbl_Does_kitchen.text = @"Doe's Kitchen";
    lbl_Does_kitchen.font = [UIFont fontWithName:kFontBold size:16];
    lbl_Does_kitchen.textColor = [UIColor blackColor];
    lbl_Does_kitchen.backgroundColor = [UIColor clearColor];
    [scroll addSubview:lbl_Does_kitchen];
    
    UIImageView *img_line2 = [[UIImageView alloc]init];
    img_line2.frame = CGRectMake(20, CGRectGetMaxY(lbl_Does_kitchen.frame)-13, 105, 1.5);
    [img_line2 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_line2 setUserInteractionEnabled:YES];
    [scroll addSubview:img_line2];
    
#pragma mark Tableview
    
    img_table = [[UITableView alloc] init ];
    img_table.frame  = CGRectMake(5,CGRectGetMaxY(img_line2.frame)+5,WIDTH-26,490);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    img_table.backgroundColor = [UIColor clearColor];
    [scroll addSubview:img_table];
    
    UIButton *btn_more_food = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_more_food.frame = CGRectMake(20,CGRectGetMaxY(scroll.frame)+20,185,40);
    btn_more_food .backgroundColor = [UIColor clearColor];
    [btn_more_food setImage:[UIImage imageNamed:@"btn-more-food@2x.png"] forState:UIControlStateNormal];
    [btn_more_food addTarget:self action:@selector(click_on_x_btn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view  addSubview:btn_more_food];
    
    
    UIButton *btn_check_out = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_check_out .frame = CGRectMake(CGRectGetMaxX(btn_more_food.frame)+10,CGRectGetMaxY(scroll.frame)+20,185,40);
    btn_check_out  .backgroundColor = [UIColor clearColor];
    [btn_check_out  setImage:[UIImage imageNamed:@"btn-check-out@2x.png"] forState:UIControlStateNormal];
    [btn_check_out  addTarget:self action:@selector(click_on_x_btn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:btn_check_out ];
    

   
}

#pragma mark popup_traceTaxi

-(void)popup_cancel_order
{
    [view_for_popup removeFromSuperview];
    view_for_popup=[[UIView alloc] init];
    view_for_popup.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.7];
    view_for_popup.userInteractionEnabled=TRUE;
    [self.view addSubview:view_for_popup];
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    [alertViewBody setImage:[UIImage imageNamed:@"wht-popup@2x.png"]];
    alertViewBody.backgroundColor=[UIColor whiteColor];
    alertViewBody.userInteractionEnabled = YES;
    [view_for_popup addSubview:alertViewBody];
    
    UIImageView *img_cross =[[UIImageView alloc] init];
    [img_cross setImage:[UIImage imageNamed:@"cross-img@2x.png"]];
    img_cross.backgroundColor=[UIColor whiteColor];
    img_cross.userInteractionEnabled = YES;
    [alertViewBody addSubview:img_cross];
    
    UIButton *btn_on_cross = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_cross .backgroundColor = [UIColor clearColor];
    //[btn_on_x setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_cross addTarget:self action:@selector(btn_on_cross_img_click:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody   addSubview:btn_on_cross];
    
//    UILabel *text_on_popup_bg = [[UILabel alloc]init];
//    text_on_popup_bg .text = @"There is less than 24 hours left to your \nordered serving time, in accordance\nwith our terms & Conditions, no refund \n is applicable";
//    text_on_popup_bg.numberOfLines = 4;
//    text_on_popup_bg .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
//    text_on_popup_bg .textAlignment = NSTextAlignmentLeft;
//    text_on_popup_bg .backgroundColor = [UIColor clearColor];
//    [alertViewBody  addSubview:text_on_popup_bg];
    
    UILabel *text_cancel_order = [[UILabel alloc]init];
    text_cancel_order .text = @"Are you sure you want to cancel your \norder?";
    text_cancel_order.numberOfLines = 2;
    text_cancel_order .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    text_cancel_order .textAlignment = NSTextAlignmentLeft;
    text_cancel_order .backgroundColor = [UIColor clearColor];
    [alertViewBody  addSubview:text_cancel_order];
    
    UIImageView *img_line4 = [[UIImageView alloc]init];
    // [img_line setUserInteractionEnabled:YES];
    img_line4.image=[UIImage imageNamed:@"line1@2x.png"];
    [alertViewBody addSubview:img_line4];
    
    
#pragma mark Tableview
    
    img_table_for_popup = [[UITableView alloc] init ];
    [img_table_for_popup setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table_for_popup.delegate = self;
    img_table_for_popup.dataSource = self;
    img_table_for_popup.showsVerticalScrollIndicator = NO;
    img_table_for_popup.backgroundColor = [UIColor clearColor];
    [alertViewBody addSubview:img_table_for_popup];
    
    UIImageView *img_line5 = [[UIImageView alloc]init];
    // [img_line setUserInteractionEnabled:YES];
    img_line5.image=[UIImage imageNamed:@"line1@2x.png"];
    [alertViewBody addSubview:img_line5];
    
    UIImageView *img_yes = [[UIImageView alloc]init];
    [img_yes setUserInteractionEnabled:YES];
    img_yes.image=[UIImage imageNamed:@"img-yes@2x.png"];
    [alertViewBody addSubview:img_yes];
    
    UIButton *btn_on_yes = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_yes  .backgroundColor = [UIColor clearColor];
    //[btn_on_x setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_yes  addTarget:self action:@selector(btn_on_yes_img_click:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody    addSubview:btn_on_yes ];
    
    
    UIImageView *img_no = [[UIImageView alloc]init];
    [img_no setUserInteractionEnabled:YES];
    img_no.image=[UIImage imageNamed:@"img-no@2x.png"];
    [alertViewBody addSubview:img_no];
    
    UIButton *btn_on_no = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_no .backgroundColor = [UIColor clearColor];
    //[btn_on_x setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_no addTarget:self action:@selector(btn_on_no_img_click:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody    addSubview:btn_on_no];
    
    
    
    
    
    
    if (IS_IPHONE_6)
    {
        
        view_for_popup.frame = CGRectMake(0,0,414, 736);
        alertViewBody.frame = CGRectMake(20,137,WIDTH-40,374);
        img_cross.frame = CGRectMake(335,10,15,15);
        btn_on_cross.frame = CGRectMake(330,10,40,40);
        
//        text_on_popup_bg .frame = CGRectMake(20,40, 500, 90);
//        text_on_popup_bg .font = [UIFont fontWithName:kFont size:17];
        
        text_cancel_order .frame = CGRectMake(20,10, 500, 90);
        text_cancel_order .font = [UIFont fontWithName:kFontBold size:16];
        
        img_line4.frame = CGRectMake(20,CGRectGetMaxY(text_cancel_order.frame)+4.5, WIDTH-70, 0.8);
        img_table_for_popup.frame  = CGRectMake(5,CGRectGetMaxY(text_cancel_order.frame)+5,WIDTH-55,34);
        img_line5.frame = CGRectMake(20,CGRectGetMaxY(img_table_for_popup.frame), WIDTH-70, 0.5);
        
        img_yes .frame = CGRectMake(13,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        btn_on_yes .frame = CGRectMake(13,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        
        img_no .frame = CGRectMake(195,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        btn_on_no.frame =CGRectMake(195,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        
    }
    else if (IS_IPHONE_6Plus)
    {
        
        view_for_popup.frame = CGRectMake(0,0,414, 736);
        alertViewBody.frame = CGRectMake(20,178,WIDTH-40,300);
        img_cross.frame = CGRectMake(335,10,15,15);
        btn_on_cross.frame = CGRectMake(330,10,40,40);
        
//        text_on_popup_bg .frame = CGRectMake(20,40, 500, 90);
//        text_on_popup_bg .font = [UIFont fontWithName:kFont size:17];
        
        text_cancel_order .frame = CGRectMake(20,10, 500, 90);
        text_cancel_order .font = [UIFont fontWithName:kFontBold size:16];
        
        img_line4.frame = CGRectMake(20,CGRectGetMaxY(text_cancel_order.frame)+4.5, WIDTH-70, 0.8);
        img_table_for_popup.frame  = CGRectMake(5,CGRectGetMaxY(text_cancel_order.frame)+5,WIDTH-55,34);
        img_line5.frame = CGRectMake(20,CGRectGetMaxY(img_table_for_popup.frame), WIDTH-70, 0.5);
        
        img_yes .frame = CGRectMake(13,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        btn_on_yes .frame = CGRectMake(13,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        
        img_no .frame = CGRectMake(195,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        btn_on_no.frame =CGRectMake(195,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        
    }
    else
    {
        view_for_popup.frame = CGRectMake(0,0,414, 736);
        alertViewBody.frame = CGRectMake(20,137,WIDTH-40,374);
        img_cross.frame = CGRectMake(335,10,15,15);
        btn_on_cross.frame = CGRectMake(330,10,40,40);
        
//        text_on_popup_bg .frame = CGRectMake(20,40, 500, 90);
//        text_on_popup_bg .font = [UIFont fontWithName:kFont size:17];
        
        text_cancel_order .frame = CGRectMake(20,10, 500, 90);
        text_cancel_order .font = [UIFont fontWithName:kFontBold size:16];
        
        img_line4.frame = CGRectMake(20,CGRectGetMaxY(text_cancel_order.frame)+4.5, WIDTH-70, 0.8);
        img_table_for_popup.frame  = CGRectMake(5,CGRectGetMaxY(text_cancel_order.frame)+5,WIDTH-55,34);
        img_line5.frame = CGRectMake(20,CGRectGetMaxY(img_table_for_popup.frame), WIDTH-70, 0.5);
        
        img_yes .frame = CGRectMake(13,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        btn_on_yes .frame = CGRectMake(13,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        
        img_no .frame = CGRectMake(195,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        btn_on_no.frame =CGRectMake(195,CGRectGetMaxY(img_line5.frame)+20, 165, 50);
        
        
    }
    
      view_for_popup .hidden = YES;
    
    
    
}



#pragma table_view delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [array_dish_imges count];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-20, 130);
        
    }
    else if (IS_IPHONE_6)
    {
        img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-5, 170);
        
    }
    else
    {    img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-10,170);
        
    }
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    img_cellBackGnd.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [cell.contentView addSubview:img_cellBackGnd];
    
    UILabel *lbl_serving_date_and_time = [[UILabel alloc]init];
    lbl_serving_date_and_time.frame = CGRectMake(10,-10,200, 45);
    lbl_serving_date_and_time.text = @"Reruested Serving Date/Time:";
    lbl_serving_date_and_time.font = [UIFont fontWithName:kFont size:11];
    lbl_serving_date_and_time.textColor = [UIColor blackColor];
    lbl_serving_date_and_time.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_serving_date_and_time];
    
    UILabel *lbl_array_serving_date_and_time = [[UILabel alloc]init];
    lbl_array_serving_date_and_time.frame = CGRectMake(CGRectGetMaxX(lbl_serving_date_and_time.frame)-40,-10,100, 45);
    lbl_array_serving_date_and_time.text = [NSString stringWithFormat:@"%@",[array_serving_time_date objectAtIndex:indexPath.row]];
    lbl_array_serving_date_and_time.font = [UIFont fontWithName:kFontBold size:11];
    lbl_array_serving_date_and_time.textColor = [UIColor blackColor];
    lbl_array_serving_date_and_time.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_array_serving_date_and_time];


    UIImageView *img_dish = [[UIImageView alloc] init];
    img_dish.frame = CGRectMake(7,CGRectGetMaxY(lbl_serving_date_and_time.frame)-10,60,60);
    [img_dish setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_dish_imges objectAtIndex:indexPath.row]]]];
    [img_cellBackGnd addSubview:img_dish];
    
    UILabel *lbl_dish_name = [[UILabel alloc]init];
    lbl_dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+8,CGRectGetMaxY(lbl_serving_date_and_time.frame)-25,200, 45);
    lbl_dish_name.text = [NSString stringWithFormat:@"%@",[array_dish_names objectAtIndex:indexPath.row]];
    lbl_dish_name.font = [UIFont fontWithName:kFontBold size:15];
    lbl_dish_name.textColor = [UIColor blackColor];
    lbl_dish_name.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_dish_name];
    
    UIImageView *img_dietary = [[UIImageView alloc] init];
    img_dietary.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+8,CGRectGetMaxY(lbl_dish_name.frame)+10,20,20);
    NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@",[[array_icon_dietary objectAtIndex:indexPath.row] valueForKey:@"DietaryImage"]]];
    
    NSData *myData = [NSData dataWithContentsOfURL:url];
    
    img_dietary.image = [UIImage imageWithData:myData];
    //[img_dietary setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_icon_dietary objectAtIndex:indexPath.row]]]];
    [img_cellBackGnd addSubview:img_dietary];
    
    UIImageView *img_serving_type = [[UIImageView alloc] init];
    img_serving_type.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+255,CGRectGetMaxY(lbl_dish_name.frame)+10,20,20);
    [img_serving_type setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_icon_serving_type objectAtIndex:indexPath.row]]]];
    [img_cellBackGnd addSubview:img_serving_type];
    
    
    UIButton *img_cross = [UIButton buttonWithType:UIButtonTypeCustom];
    img_cross.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+246,CGRectGetMaxY(lbl_dish_name.frame)-41,45,80);
    img_cross .backgroundColor = [UIColor clearColor];
    [img_cross setImage:[UIImage imageNamed:@"img-x@2x.png"] forState:UIControlStateNormal];
    [img_cross addTarget:self action:@selector(click_on_x_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_cellBackGnd   addSubview:img_cross];


    
    UIImageView *img_line3 = [[UIImageView alloc]init];
    img_line3.frame = CGRectMake(10, CGRectGetMaxY(img_dish.frame)+8, WIDTH-50, 0.5);
    [img_line3 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_line3 setUserInteractionEnabled:YES];
    [img_cellBackGnd addSubview:img_line3];
    
    UILabel *lbl_quantity = [[UILabel alloc]init];
    lbl_quantity.frame = CGRectMake(10,CGRectGetMaxY(img_line3.frame)-8,200, 45);
    lbl_quantity.text = @"Quantity:";
    lbl_quantity.font = [UIFont fontWithName:kFont size:11];
    lbl_quantity.textColor = [UIColor blackColor];
    lbl_quantity.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_quantity];
    
    UILabel *quantity_val = [[UILabel alloc]init];
    quantity_val.frame = CGRectMake(CGRectGetMidX(lbl_quantity.frame)-48,CGRectGetMaxY(img_line3.frame)-8,100,45);
    quantity_val.text = [NSString stringWithFormat:@"%@",[array_quantity objectAtIndex:indexPath.row]];
    quantity_val.font = [UIFont fontWithName:kFontBold size:11];
    quantity_val.textColor = [UIColor blackColor];
    quantity_val.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:quantity_val];
    
    UILabel *serving_charg_val = [[UILabel alloc]init];
    serving_charg_val.frame = CGRectMake(CGRectGetMidX(lbl_quantity.frame)+38,CGRectGetMaxY(img_line3.frame)-8,100,45);
    serving_charg_val.text = [NSString stringWithFormat:@"%@",[array_serving_charge objectAtIndex:indexPath.row]];
    serving_charg_val.font = [UIFont fontWithName:kFontBold size:11];
    serving_charg_val.textColor = [UIColor blackColor];
    serving_charg_val.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:serving_charg_val];
    
    
    UILabel *lbl_serving_charge = [[UILabel alloc]init];
    lbl_serving_charge.frame = CGRectMake(CGRectGetMidX(serving_charg_val.frame)-22,CGRectGetMaxY(img_line3.frame)-8,200, 45);
    lbl_serving_charge.text = @"/serving";
    lbl_serving_charge.font = [UIFont fontWithName:kFont size:11];
    lbl_serving_charge.textColor = [UIColor blackColor];
    lbl_serving_charge.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_serving_charge];
    
    
    UILabel *lbl_subtotal = [[UILabel alloc]init];
    lbl_subtotal.frame = CGRectMake(CGRectGetMaxX(lbl_serving_charge.frame)-80,CGRectGetMaxY(img_line3.frame)-8,200, 45);
    lbl_subtotal.text = @"Subtotal:";
    lbl_subtotal.font = [UIFont fontWithName:kFont size:11];
    lbl_subtotal.textColor = [UIColor blackColor];
    lbl_subtotal.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_subtotal];
    
    UILabel *subtotal_val = [[UILabel alloc]init];
    subtotal_val.frame = CGRectMake(CGRectGetMinX(lbl_subtotal.frame)+48,CGRectGetMaxY(img_line3.frame)-8,100,45);
    subtotal_val.text = [NSString stringWithFormat:@"%@",[array_sub_total objectAtIndex:indexPath.row]];
    subtotal_val.font = [UIFont fontWithName:kFontBold size:11];
    subtotal_val.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    subtotal_val.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:subtotal_val];
    
     return cell;
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)click_back_arrow:(UIButton *)sender
{
    NSLog(@"click back arrow");
}
-(void)click_on_x_btn:(UIButton *)sender
{
    NSLog(@"click_on_x_btn:");
     view_for_popup .hidden = NO;
}
-(void)btn_on_cross_img_click:(UIButton *)sender
{
    NSLog(@"btn_on_x_img_click:");
    view_for_popup .hidden = YES;
    
}
-(void)btn_on_yes_img_click:(UIButton *)sender
{
    NSLog(@"btn_on_yes_img_click:");
    
}
-(void)btn_on_no_img_click:(UIButton *)sender
{
    NSLog(@"btn_on_no_img_click:");
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
