//
//  MessageListVC.m
//  Not86
//
//  Created by User on 13/10/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "MessageListVC.h"
#import "AppDelegate.h"
#import "JWNavigationController.h"
#import "JWSlideMenuViewController.h"

#import "UserChatingVC.h"


#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface MessageListVC ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    UIImageView * img_header;
    UITableView * table_for_chef_orders;
    
    AppDelegate *delegate;
    
    NSMutableArray * array_chef_img;
    NSMutableArray * array_chef_name;
    NSMutableArray * array_order_no;
    NSMutableArray * array_subject;
    NSMutableArray * array_no_of_message;
    NSMutableArray * array_time;
    
    UIImageView *img_chef;
    UILabel * chef_name;
    UILabel * order_val;
    UILabel * subject_val;
    UILabel * labl_time_and_date;
    UILabel * lab_number_in_redround;
    NSMutableArray * array_messages;
    UITextField *txt_search;
    NSMutableArray*ary_mesfilter;
    int search_leg;
    
}

@end

@implementation MessageListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ary_mesfilter = [NSMutableArray new];
    
    
    [self integrateHeader];
    [self integratebody];
    
    array_chef_img = [[NSMutableArray alloc]initWithObjects:@"chef-img@2x.png",@"chef-img@2x.png",@"chef-img@2x.png",@"chef-img@2x.png",nil];
    array_chef_name = [[NSMutableArray alloc]initWithObjects:@"Jihanatha Timothy",@"Sall Home Chef",@"Jihanatha Timothy",@"Sall Home Chef",nil];
    array_order_no = [[NSMutableArray alloc]initWithObjects:@"221411",@"221411",@"221411",@"221411", nil];
    array_subject = [[NSMutableArray alloc]initWithObjects:@"Lorem ipsum dolor sit",@"Lorem ipsum dolor sit",@"Lorem ipsum dolor sit",@"Lorem ipsum dolor sit", nil];
    array_no_of_message = [[NSMutableArray alloc]initWithObjects:@"3",@"3",@"3",@"3",nil];
    array_time = [[NSMutableArray alloc]initWithObjects:@"18-9-2015, 2:30PM",@"18-9-2015, 2:30PM",@"18-9-2015, 2:30PM",@"18-9-2015, 2:30PM", nil];
    // Do any additional setup after loading the view.
    
    array_messages = [NSMutableArray new];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self AFMessages];
}

-(void)integrateHeader
{
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu .frame = CGRectMake(10, 13,20,20);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu  addTarget:self action:@selector(btn_HomeScreenClick:) forControlEvents:UIControlEventTouchUpInside];
    [icon_menu setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
    // [img_header   addSubview:icon_menu ];
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 150, 45);
    lbl_User_Sign_Up.text = @"Messages";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    UIButton *icon_serch = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_serch .frame = CGRectMake(WIDTH-65, 13, 20, 20);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_serch  addTarget:self action:@selector(btn_action_on_serch:) forControlEvents:UIControlEventTouchUpInside];
    [icon_serch setImage:[UIImage imageNamed:@"search-icon@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_serch ];
    
    
    //    UIImageView *icon_user = [[UIImageView alloc]init];
    //    icon_user.frame = CGRectMake(WIDTH-35, 9, 27, 27);
    //    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //    //   icon_user.backgroundColor = [UIColor redColor];
    //    [icon_user setUserInteractionEnabled:YES];
    //    [img_header addSubview:icon_user];
    
    UIButton *icon_logo = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_logo .frame = CGRectMake(WIDTH-35, 9, 27, 27);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_logo  addTarget:self action:@selector(click_on_logo_btn:) forControlEvents:UIControlEventTouchUpInside];
    [icon_logo setImage:[UIImage imageNamed:@"logo-img@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_logo ];
    
    
    
    
}
-(void)integratebody
{
    UIImageView *white_bg = [[UIImageView alloc]init];
    white_bg.frame = CGRectMake(0,CGRectGetMaxY(img_header.frame)+5,WIDTH,70);
    [white_bg setUserInteractionEnabled:YES];
    white_bg.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [self.view addSubview:white_bg];
    
    
    UIImageView *img_serch_bar = [[UIImageView alloc]init];
    img_serch_bar.frame = CGRectMake(15,20, WIDTH-25, 40);
    [img_serch_bar setImage:[UIImage imageNamed:@"search-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_serch_bar setUserInteractionEnabled:YES];
    [white_bg addSubview:img_serch_bar];
    
    
    txt_search = [[UITextField alloc] init];
    txt_search.frame = CGRectMake(0,-8, 300, 45);
    txt_search .borderStyle = UITextBorderStyleNone;
    txt_search .textColor = [UIColor grayColor];
    txt_search .font = [UIFont fontWithName:kFont size:15];
    txt_search .placeholder = @"Search...";
    [txt_search  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_search  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_search .leftView = padding3;
    txt_search .leftViewMode = UITextFieldViewModeAlways;
    txt_search .userInteractionEnabled=YES;
    txt_search .textAlignment = NSTextAlignmentLeft;
    txt_search .backgroundColor = [UIColor clearColor];
    txt_search .keyboardType = UIKeyboardTypeAlphabet;
    txt_search .delegate = self;
    [img_serch_bar addSubview:txt_search ];
    
    
    UIImageView *icon_search = [[UIImageView alloc]init];
    icon_search.frame = CGRectMake(320,8, 20,20);
    [icon_search setImage:[UIImage imageNamed:@"icon-search@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_search setUserInteractionEnabled:YES];
    [img_serch_bar addSubview:icon_search];
    
    UIButton *btn_on_search_bar = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_search_bar.frame = CGRectMake(320,0, 40, 40);
    btn_on_search_bar .userInteractionEnabled=YES;
    btn_on_search_bar .backgroundColor = [UIColor clearColor];
    [btn_on_search_bar addTarget:self action:@selector(btn_on_search_bar_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [img_serch_bar   addSubview:btn_on_search_bar];
    
#pragma mark Tableview
    
    table_for_chef_orders = [[UITableView alloc] init ];
    table_for_chef_orders.frame  = CGRectMake(0,CGRectGetMaxY(white_bg.frame),WIDTH,530);
    [table_for_chef_orders setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_chef_orders.delegate = self;
    table_for_chef_orders.dataSource = self;
    table_for_chef_orders.showsVerticalScrollIndicator = NO;
    table_for_chef_orders.backgroundColor = [UIColor clearColor];
    [self.view addSubview:table_for_chef_orders];
    if (IS_IPHONE_6Plus)
    {
        white_bg.frame = CGRectMake(0,CGRectGetMaxY(img_header.frame)+5,WIDTH,70);
        img_serch_bar.frame = CGRectMake(15,20, WIDTH-25, 40);
        txt_search.frame = CGRectMake(0,-8, 300, 45);
        icon_search.frame = CGRectMake(320,8, 20,20);
        btn_on_search_bar.frame = CGRectMake(320,0, 40, 40);
        table_for_chef_orders.frame  = CGRectMake(0,CGRectGetMaxY(white_bg.frame),WIDTH,600);
        
    }
    else if (IS_IPHONE_6)
    {
        white_bg.frame = CGRectMake(0,CGRectGetMaxY(img_header.frame)+5,WIDTH,70);
        img_serch_bar.frame = CGRectMake(15,20, WIDTH-25, 40);
        txt_search.frame = CGRectMake(0,-8, 300, 45);
        icon_search.frame = CGRectMake(320,8, 20,20);
        btn_on_search_bar.frame = CGRectMake(320,0, 40, 40);
        table_for_chef_orders.frame  = CGRectMake(0,CGRectGetMaxY(white_bg.frame),WIDTH,530);
        
    }
    else if (IS_IPHONE_5)
    {
        white_bg.frame = CGRectMake(0,CGRectGetMaxY(img_header.frame)+5,WIDTH,70);
        img_serch_bar.frame = CGRectMake(15,20, WIDTH-25, 40);
        txt_search.frame = CGRectMake(0,-8, 300, 45);
        icon_search.frame = CGRectMake(320,8, 20,20);
        btn_on_search_bar.frame = CGRectMake(320,0, 40, 40);
        table_for_chef_orders.frame  = CGRectMake(0,CGRectGetMaxY(white_bg.frame),WIDTH,440);
        
    }
    else
    {
        white_bg.frame = CGRectMake(0,CGRectGetMaxY(img_header.frame)+5,WIDTH,70);
        img_serch_bar.frame = CGRectMake(15,20, WIDTH-25, 40);
        txt_search.frame = CGRectMake(0,-8, 300, 45);
        icon_search.frame = CGRectMake(320,8, 20,20);
        btn_on_search_bar.frame = CGRectMake(320,0, 40, 40);
        table_for_chef_orders.frame  = CGRectMake(0,CGRectGetMaxY(white_bg.frame),WIDTH,350);
        
        
    }
    
    
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ ary_mesfilter count];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        img_cellBackGnd.frame =  CGRectMake(3,15, WIDTH+5, 140);
        
    }
    else if (IS_IPHONE_6)
    {
        img_cellBackGnd.frame =  CGRectMake(3,15, WIDTH, 140);
        
    }
    else
    {    img_cellBackGnd.frame =  CGRectMake(5,15, WIDTH-10,140);
        
    }
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    img_cellBackGnd.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [cell.contentView addSubview:img_cellBackGnd];
    
    
    
    img_chef = [[UIImageView alloc] init];
    img_chef.frame = CGRectMake(7,14, 90,  95 );
    //    [img_chef setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[[array_messages objectAtIndex:indexPath.row] valueForKey:@""]]]];
    NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[ary_mesfilter objectAtIndex:indexPath.row] valueForKey:@"Sender_ProfileImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [img_chef setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@"img_profile@2x.png"]];
    [img_chef setContentMode:UIViewContentModeScaleAspectFill];
    [img_chef setClipsToBounds:YES];
    [img_cellBackGnd addSubview:img_chef];
    
    chef_name = [[UILabel alloc]init];
    chef_name .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,20,200,20);
    chef_name .text = [NSString stringWithFormat:@"%@",[[ary_mesfilter objectAtIndex:indexPath.row] valueForKey:@"Sender_Name"]];
    chef_name .font = [UIFont fontWithName:kFontBold size:17];
    chef_name .textColor = [UIColor blackColor];
    chef_name .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview: chef_name];
    
    UILabel *lbl_order_number = [[UILabel alloc]init];
    lbl_order_number.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,40, 150, 20);
    lbl_order_number.text = @"Order Number:";
    lbl_order_number.font = [UIFont fontWithName:kFont size:14];
    lbl_order_number.textColor = [UIColor blackColor];
    lbl_order_number.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_order_number];
    
    order_val = [[UILabel alloc]init];
    order_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-40,40,200,20);
    order_val .text = [NSString stringWithFormat:@"%@",[[ary_mesfilter objectAtIndex:indexPath.row] valueForKey:@"Order_id"]];
    order_val .font = [UIFont fontWithName:kFont size:14];
    order_val .textColor = [UIColor blackColor];
    order_val .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview: order_val];
    
    UILabel *lbl_subject = [[UILabel alloc]init];
    lbl_subject.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,CGRectGetMaxY(lbl_order_number.frame), 100, 20);
    lbl_subject.text = @"Subject:";
    lbl_subject.font = [UIFont fontWithName:kFont size:14];
    lbl_subject.textColor = [UIColor blackColor];
    lbl_subject.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_subject];
    
    subject_val = [[UILabel alloc]init];
    subject_val .frame = CGRectMake(CGRectGetMaxX(lbl_subject.frame)-40,CGRectGetMaxY(lbl_order_number.frame),200,20);
    subject_val .text = [NSString stringWithFormat:@"%@",[[ary_mesfilter objectAtIndex:indexPath.row] valueForKey:@"sender_message"]];
    subject_val .font = [UIFont fontWithName:kFontBold size:14];
    subject_val .textColor = [UIColor blackColor];
    subject_val .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview: subject_val];
    
    double timestampval =  [[[ary_mesfilter objectAtIndex:indexPath.row] valueForKey:@"message_time"] doubleValue];
    NSTimeInterval timestamp = (NSTimeInterval)timestampval;
    NSDate *updatetimestamp = [NSDate dateWithTimeIntervalSince1970:timestamp];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd/MM/yyyy";
    NSString *str_Time = [dateFormatter stringFromDate:updatetimestamp];
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    dateFormatter1.dateFormat = @"hh:mm a";
    NSString *str_Time1 = [dateFormatter1 stringFromDate:updatetimestamp];
    
    
    labl_time_and_date = [[UILabel alloc]init];
    labl_time_and_date .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,CGRectGetMaxY(lbl_subject.frame),200,20);
    labl_time_and_date.text = [NSString stringWithFormat:@"%@ %@",str_Time,str_Time1];
    labl_time_and_date .font = [UIFont fontWithName:kFont size:14];
    labl_time_and_date .textColor = [UIColor blackColor];
    labl_time_and_date .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview: labl_time_and_date];
    
    UIImageView *icon_round_red = [[UIImageView alloc]init];
    icon_round_red.frame = CGRectMake(WIDTH-50,45, 27, 27);
    [icon_round_red setImage:[UIImage imageNamed:@"icon_round_red@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    //  [icon_round_red setUserInteractionEnabled:YES];
    [img_cellBackGnd addSubview:icon_round_red];
    
    
    lab_number_in_redround = [[UILabel alloc]init];
    lab_number_in_redround .frame = CGRectMake(8,3,200,20);
    lab_number_in_redround .text = [NSString stringWithFormat:@"%@",[[ary_mesfilter objectAtIndex:indexPath.row] valueForKey:@"unread_msg_count"]];
    lab_number_in_redround .font = [UIFont fontWithName:kFontBold size:17];
    lab_number_in_redround .textColor = [UIColor whiteColor];
    lab_number_in_redround .backgroundColor = [UIColor clearColor];
    [icon_round_red addSubview: lab_number_in_redround];
    if (IS_IPHONE_6Plus)
    {
        img_chef.frame = CGRectMake(7,14, 90,  95 );
        chef_name .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,20,200,20);
        lbl_order_number.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,40, 150, 20);
        order_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-40,40,200,20);
        lbl_subject.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,CGRectGetMaxY(lbl_order_number.frame), 100, 20);
        subject_val .frame = CGRectMake(CGRectGetMaxX(lbl_subject.frame)-40,CGRectGetMaxY(lbl_order_number.frame),200,20);
        labl_time_and_date .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,CGRectGetMaxY(lbl_subject.frame),200,20);
        icon_round_red.frame = CGRectMake(WIDTH-50,45, 27, 27);
        lab_number_in_redround .frame = CGRectMake(8,3,200,20);
        
    }
    else if (IS_IPHONE_6)
    {
        img_chef.frame = CGRectMake(7,14, 90,  95 );
        chef_name .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,20,200,20);
        lbl_order_number.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,40, 150, 20);
        order_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-40,40,200,20);
        lbl_subject.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,CGRectGetMaxY(lbl_order_number.frame), 100, 20);
        subject_val .frame = CGRectMake(CGRectGetMaxX(lbl_subject.frame)-40,CGRectGetMaxY(lbl_order_number.frame),200,20);
        labl_time_and_date .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,CGRectGetMaxY(lbl_subject.frame),200,20);
        icon_round_red.frame = CGRectMake(WIDTH-50,45, 27, 27);
        lab_number_in_redround .frame = CGRectMake(8,3,200,20);
        
    }
    else if (IS_IPHONE_5)
    {
        img_chef.frame = CGRectMake(7,14, 70,  75 );
        chef_name .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,20,200,20);
        lbl_order_number.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,40, 150, 20);
        order_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-45,40,200,20);
        lbl_subject.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,CGRectGetMaxY(lbl_order_number.frame), 100, 20);
        subject_val .frame = CGRectMake(CGRectGetMaxX(lbl_subject.frame)-50,CGRectGetMaxY(lbl_order_number.frame),200,20);
        labl_time_and_date .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,CGRectGetMaxY(lbl_subject.frame),200,20);
        icon_round_red.frame = CGRectMake(WIDTH-50,45, 27, 27);
        lab_number_in_redround .frame = CGRectMake(8,3,200,20);
        
        chef_name .font = [UIFont fontWithName:kFontBold size:14];
        lbl_order_number.font = [UIFont fontWithName:kFont size:14];
        order_val .font = [UIFont fontWithName:kFont size:12];
        lbl_subject.font = [UIFont fontWithName:kFont size:12];
        subject_val .font = [UIFont fontWithName:kFontBold size:12];
        labl_time_and_date .font = [UIFont fontWithName:kFont size:12];
        lab_number_in_redround .font = [UIFont fontWithName:kFontBold size:17];
        
        
    }
    else
    {
        img_chef.frame = CGRectMake(7,14, 70,  75 );
        chef_name .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,20,200,20);
        lbl_order_number.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,40, 150, 20);
        order_val .frame = CGRectMake(CGRectGetMaxX(lbl_order_number.frame)-45,40,200,20);
        lbl_subject.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,CGRectGetMaxY(lbl_order_number.frame), 100, 20);
        subject_val .frame = CGRectMake(CGRectGetMaxX(lbl_subject.frame)-50,CGRectGetMaxY(lbl_order_number.frame),200,20);
        labl_time_and_date .frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,CGRectGetMaxY(lbl_subject.frame),200,20);
        icon_round_red.frame = CGRectMake(WIDTH-50,45, 27, 27);
        lab_number_in_redround .frame = CGRectMake(8,3,200,20);
        
        chef_name .font = [UIFont fontWithName:kFontBold size:14];
        lbl_order_number.font = [UIFont fontWithName:kFont size:14];
        order_val .font = [UIFont fontWithName:kFont size:12];
        lbl_subject.font = [UIFont fontWithName:kFont size:12];
        subject_val .font = [UIFont fontWithName:kFontBold size:12];
        labl_time_and_date .font = [UIFont fontWithName:kFont size:12];
        lab_number_in_redround .font = [UIFont fontWithName:kFontBold size:17];
    }
    
    
    
    return cell;
    
}

#pragma table view

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserChatingVC *vc=[[UserChatingVC alloc]init];
    vc.str_nid = [[array_messages objectAtIndex:indexPath.row] valueForKey:@"MessageID"];
    vc.str_orderID =[[array_messages objectAtIndex:indexPath.row] valueForKey:@"Order_id"];
    vc.str_receiverID =[[array_messages objectAtIndex:indexPath.row] valueForKey:@"Sender_id"];
    vc.str_name =[[array_messages objectAtIndex:indexPath.row] valueForKey:@"Sender_Name"];
    vc.str_title =[[array_messages objectAtIndex:indexPath.row] valueForKey:@"Sender_Name"];
    
    [self.navigationController pushViewController:vc];
    
}

#pragma mark ---textFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField ==txt_search)
    {
        if(textField.text)
        {
            search_leg = range.location+1;
        }
        else
        {
            search_leg=0;
        }
        
        
        if (ary_mesfilter)
        {
            [ary_mesfilter removeAllObjects];
        }
        
        
        for (NSMutableDictionary *dict in  array_messages)
        {
            
            NSString *search = [dict valueForKey:@"Sender_Name"];
            
            if ([search length]>search_leg)
            {
                search = [search substringToIndex:search_leg];
            }
            
            NSString *substring = [NSString stringWithString:textField.text];
            substring = [substring stringByReplacingCharactersInRange:range withString:string];
            BOOL nameMatches = [[substring lowercaseString] hasPrefix:[string lowercaseString]];
            NSRange r = [search rangeOfString:substring options:NSCaseInsensitiveSearch];
            
            if(r.location != NSNotFound || nameMatches)
            {
                if (r.length > 0)
                {
                    [ary_mesfilter addObject:dict];
                }
            }
            
        }
    }
    [table_for_chef_orders reloadData];
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == txt_search)
    {
        if ([textField.text isEqualToString:@""])
        {
            [ary_mesfilter removeAllObjects];
            
            for (int i=0; i<[array_messages count]; i++)
            {
                [ary_mesfilter addObject:[array_messages objectAtIndex:i]];
            }
            
            
        }
        
    }
    else
    {
        if ([ary_mesfilter count]==0)
        {
            
            //[SVProgressHUD showErrorWithStatus:@"No result found"];
            txt_search.text = @"";
        }
        
    }
    
    [table_for_chef_orders reloadData];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [table_for_chef_orders setHidden:NO];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    [txt_search resignFirstResponder];
    
    return YES;
}

-(void)btn_on_search_bar_click:(UIButton*)sender
{
    
}

#pragma user-chating-profile-functionality

-(void) AFMessages
{
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    // int str_slidervalve = [[NSString stringWithFormat:@"%f",mSlider.value] intValue];
    //    [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserDetail"] valueForKey:@"UserId"]
    
    NSDictionary *params;
    
    params =@{
              @"login_uid"                  :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
              
              };
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kchatingmessages
                                    
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseMySettings:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             
                                             // [self popup_Alertview:@"Please check your internet connection"];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully login");
                                             [self AFMessages];
                                         }
                                     }];
    [operation start];
    
}

-(void) ResponseMySettings :(NSDictionary * )TheDict
{
    NSLog(@"ResponseMysettings: %@",TheDict);
    
    [array_messages removeAllObjects];
    [ary_mesfilter removeAllObjects];
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        if (![[TheDict valueForKey:@"userlast_chat"] isKindOfClass:[NSNull class]]||(TheDict != (NSDictionary*) [NSNull null]))
        {
            for (int i=0; i<[[TheDict valueForKey:@"userlast_chat"]count]; i++) {
                
                [array_messages addObject:[[TheDict valueForKey:@"userlast_chat"] objectAtIndex:i]];
                [ary_mesfilter addObject:[[TheDict valueForKey:@"userlast_chat"] objectAtIndex:i]];
                
            }
        }
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //[self popup_Alertview:[TheDict valueForKey:@"Message"]];
        
        
    }
    
    [table_for_chef_orders reloadData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
