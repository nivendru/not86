//
//  ViewMenuVC.m
//  Not86
//
//  Created by User on 19/10/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ViewMenuVC.h"
#import "UserSupportVC.h"
#import "MenuScreenVC.h"
#import "ViewDishDetailsVC.h"
#import "JWSlideMenuController.h"
#import "JWSlideMenuViewController.h"


#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"
#import "AppDelegate.h"


@interface ViewMenuVC ()<UIScrollViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIScrollView * scroll;
    UIImageView * img_header;
    
    UICollectionViewFlowLayout *  layout;
    UICollectionView * collView_for_cource ;
    
    NSMutableArray * array_cource_img;
    NSMutableArray * array_cource_names;
    
    UITableView * table_for_dish_items;
    NSMutableArray * ary_displaynames;
    NSMutableArray * array_img;
    
    NSMutableArray * array_course_infoodnow;
    NSMutableArray *array_DishesList;
    
    NSMutableArray * ary_CuisinesList;
    NSMutableArray *ary_DietaryList;
    
    NSMutableArray *ary_CollectionCount;
    
    NSMutableArray * Arr_temp_course_infoodnow;
    AppDelegate *delegate;
    
    NSString *str_CourseId;
    UIView*alertviewBg;
    UIButton *icon_menu;
    NSMutableArray*ary_TEmp;
    int selectedindex;
    NSIndexPath*indexSelected;
    
    
    
    
    
}

@end

@implementation ViewMenuVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ary_TEmp = [NSMutableArray new];
    selectedindex = -1;
    indexSelected = nil;
    
    
    
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    
    array_cource_img = [[NSMutableArray alloc]initWithObjects:@"b-breakfast@2x.png",@"main-couse-img@2x.png",@"b-dessert@2x.png", nil];
    array_cource_names  = [[NSMutableArray alloc]initWithObjects:@"Appetizer",@"Main Course",@"Dessert" ,nil];
    
    //array_for_dish_item_table
    ary_displaynames =[[NSMutableArray alloc]initWithObjects:@"Steamed Thai oath",@"Rasberry custored",@"Steamed Thai oath",@"Steamed Thai oath",nil];
    array_img = [[NSMutableArray alloc]initWithObjects:@"img-dish1@2x.png",@"img-dish2@2x.png",@"img-dish1@2x.png",@"img-dish1@2x.png", nil];
    
    
    ary_CollectionCount  = [[NSMutableArray alloc]init];
    ary_CuisinesList = [[NSMutableArray alloc]init];
    ary_DietaryList = [[NSMutableArray alloc]init];
    
    array_course_infoodnow = [[NSMutableArray alloc]init];
    Arr_temp_course_infoodnow = [[NSMutableArray alloc]init];
    array_DishesList = [[NSMutableArray alloc]init];
    
    
    [self integrateHeader];
    [self integratebody];
    
    // Do any additional setup after loading the view.
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    if ([_str_comefrom isEqualToString:@"HOME"])
    {
        [self.navigationController.slideMenuController HideHomeButton];
        icon_menu.hidden = NO;
        
    }
    else{
        [self.navigationController.slideMenuController UnhideHomeButton];
        icon_menu.hidden = YES;
        
        
    }
    
    [self AFCourse_in_dishandmeal];
    
}
-(void)integrateHeader
{
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    //    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    //    icon_menu .frame = CGRectMake(10, 13,20,20);
    //    //icon_menu .backgroundColor = [UIColor clearColor];
    //    [icon_menu  addTarget:self action:@selector(btn_HomeScreenClick:) forControlEvents:UIControlEventTouchUpInside];
    //    [icon_menu setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
    // [img_header   addSubview:icon_menu ];
    
    icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu .frame = CGRectMake(10, 13,25,20);
    //icon_menu .backgroundColor = [UIColor clearColor];
    
    if ([_str_comefrom isEqualToString:@"HOME"])
    {
        [icon_menu  addTarget:self action:@selector(click_on_Back_Btn:)forControlEvents:UIControlEventTouchUpInside];
        
        
    }
    else{
        
        
    }
    [icon_menu setImage:[UIImage imageNamed:@"img_arrow@2x"] forState:UIControlStateNormal];
    //    icon_menu.layer.borderWidth = 1.0;
    [icon_menu setBackgroundColor:[UIColor clearColor]];
    [img_header   addSubview:icon_menu ];
    
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 150, 45);
    lbl_User_Sign_Up.text = @"Menu";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    
    UIButton *icon_logo = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_logo .frame = CGRectMake(WIDTH-35, 9, 27, 27);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_logo  addTarget:self action:@selector(click_on_logo_btn:) forControlEvents:UIControlEventTouchUpInside];
    [icon_logo setImage:[UIImage imageNamed:@"logo-img@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_logo];
    
}

-(void)click_on_Back_Btn: (UIButton *)sender

{
    NSLog(@"click_on_Back_Btn Click");
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)integratebody
{
    
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor clearColor];
    scroll.frame = CGRectMake(0,50, WIDTH,HEIGHT);
    //[scroll setContentSize:CGSizeMake(0, 1000)];
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    //  scroll.layer.borderWidth = 1.0;
    [self.view addSubview:scroll];
    
    
    UIButton *btn_on_dishandmeal = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_dishandmeal .frame = CGRectMake(30, 10, WIDTH-60, 50);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [btn_on_dishandmeal  addTarget:self action:@selector(click_on_dishandmeal_btn:) forControlEvents:UIControlEventTouchUpInside];
    [btn_on_dishandmeal setImage:[UIImage imageNamed:@"img-black-btn@2x.png"] forState:UIControlStateNormal];
    [scroll   addSubview:btn_on_dishandmeal];
    
    
    UILabel *lbl_add_dishandmeal = [[UILabel alloc]init];
    lbl_add_dishandmeal.frame = CGRectMake(0,0, WIDTH-60, 50);
    lbl_add_dishandmeal.text = @"ADD DISH/MEAL";
    lbl_add_dishandmeal.font = [UIFont fontWithName:kFont size:20];
    lbl_add_dishandmeal.textColor = [UIColor whiteColor];
    lbl_add_dishandmeal.backgroundColor = [UIColor clearColor];
    lbl_add_dishandmeal.textAlignment = NSTextAlignmentCenter;
    [btn_on_dishandmeal addSubview:lbl_add_dishandmeal];
    
    
    UIImageView *white_bg = [[UIImageView alloc]init];
    white_bg.frame = CGRectMake(0,CGRectGetMaxY(btn_on_dishandmeal.frame)+10,WIDTH,75);
    [white_bg setUserInteractionEnabled:YES];
    white_bg.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [scroll addSubview:white_bg];
    
    
    layout = [[UICollectionViewFlowLayout alloc] init];
    collView_for_cource = [[UICollectionView alloc] initWithFrame:CGRectMake(10,5,WIDTH-20,65)
                                             collectionViewLayout:layout];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_cource setDataSource:self];
    [collView_for_cource setDelegate:self];
    collView_for_cource.scrollEnabled = YES;
    collView_for_cource.showsVerticalScrollIndicator = NO;
    collView_for_cource.showsHorizontalScrollIndicator = NO;
    collView_for_cource.pagingEnabled = YES;
    [collView_for_cource registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_cource setBackgroundColor:[UIColor clearColor]];
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 0;
    // collView_for_cource.layer.borderWidth = 1.0;
    collView_for_cource.userInteractionEnabled = YES;
    [white_bg  addSubview: collView_for_cource];
    
    
    UIImageView *img_left_arrow4_food_later = [[UIImageView alloc]init];
    img_left_arrow4_food_later.frame = CGRectMake(5,30,10, 15);
    [img_left_arrow4_food_later setUserInteractionEnabled:YES];
    img_left_arrow4_food_later.image=[UIImage imageNamed:@"left-arrow-img@2x.png"];
    // [white_bg addSubview:img_left_arrow4_food_later];
    
    
    UIButton *btn_on_lefet_arrow4_in_later = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_lefet_arrow4_in_later.frame = CGRectMake(0,0, 20,70);
    btn_on_lefet_arrow4_in_later .backgroundColor = [UIColor clearColor];
    [btn_on_lefet_arrow4_in_later setUserInteractionEnabled:YES];
    [btn_on_lefet_arrow4_in_later addTarget:self action:@selector(click_on_left_arrow_btn:) forControlEvents:UIControlEventTouchUpInside];
    //  [white_bg addSubview:btn_on_lefet_arrow4_in_later];
    
    
    
    UIImageView *img_right_arrow4_in_food_later = [[UIImageView alloc]init];
    img_right_arrow4_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_cource.frame)+3,30,10, 15);
    [img_right_arrow4_in_food_later setUserInteractionEnabled:YES];
    img_right_arrow4_in_food_later.image=[UIImage imageNamed:@"right-arrow@2x.png"];
    // [white_bg addSubview:img_right_arrow4_in_food_later];
    
    UIButton *btn_on_right_arrow4_in_later = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_right_arrow4_in_later.frame = CGRectMake(CGRectGetMaxX(collView_for_cource.frame),0,20,70);
    btn_on_right_arrow4_in_later .backgroundColor = [UIColor clearColor];
    [btn_on_right_arrow4_in_later setUserInteractionEnabled:YES];
    [btn_on_right_arrow4_in_later addTarget:self action:@selector(click_on_right_arrow_btn:) forControlEvents:UIControlEventTouchUpInside];
    //[white_bg addSubview:btn_on_right_arrow4_in_later];
    
    
#pragma mark Tableview
    
    table_for_dish_items = [[UITableView alloc] init ];
    
    if (IS_IPHONE_6Plus) {
        table_for_dish_items.frame =CGRectMake(10,CGRectGetMaxY(collView_for_cource.frame)+80,WIDTH-20,HEIGHT);
    }
    else if (IS_IPHONE_6)
    {
        table_for_dish_items.frame =CGRectMake(10,CGRectGetMaxY(collView_for_cource.frame)+80,WIDTH-20,HEIGHT-200);
    }
    else if (IS_IPHONE_5)
    {
        table_for_dish_items.frame =CGRectMake(10,CGRectGetMaxY(collView_for_cource.frame)+80,WIDTH-20,HEIGHT-200);
    }
    else
    {
        table_for_dish_items.frame =CGRectMake(10,CGRectGetMaxY(collView_for_cource.frame)+80,WIDTH-20,HEIGHT-200);
    }
    
    [table_for_dish_items setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_dish_items.delegate = self;
    table_for_dish_items.dataSource = self;
    table_for_dish_items.showsVerticalScrollIndicator = NO;
    table_for_dish_items.backgroundColor = [UIColor clearColor];
    [scroll addSubview:table_for_dish_items];
    
    
    
    
}

#pragma mark UiCollectionView Delegate Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [array_course_infoodnow count];
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    //
    //    UIImageView *img_backGnd = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    //    img_backGnd.backgroundColor = [UIColor clearColor];
    //    [cell.contentView addSubview:img_backGnd];
    
    
    if (collectionView == collView_for_cource)
    {
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,0,WIDTH-20/3,75);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        //cell_for_collection_view.layer.borderWidth = 1.0;
        [cell.contentView addSubview:cell_for_collection_view];
        
        
        UIImageView *course_icon = [[UIImageView alloc]init];
        course_icon .frame = CGRectMake(15,28,50,25);
        //        if (indexPath.row == 0)
        //        {
        //            course_icon .frame = CGRectMake(15,28,50,25);
        //        }
        //        if (indexPath.row == 1)
        //        {
        //            course_icon .frame = CGRectMake(30,22,35,35);
        //        }
        //        if (indexPath.row == 2)
        //        {
        //            course_icon .frame = CGRectMake(40,22,30,30);
        //        }
        
        //  [course_icon setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_course_images objectAtIndex:indexPath.row]]]];
        NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[array_course_infoodnow objectAtIndex:indexPath.row] valueForKey:@"DishCourseImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [course_icon setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@""]];
        //[chef_pro_icons  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        course_icon .backgroundColor = [UIColor clearColor];
        [course_icon  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:course_icon];
        
        
        
        
        UIButton *btn_incell_tikmark = [[UIButton alloc]init];
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 6,((WIDTH-65)/3),75);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 19,((WIDTH-60)/3),53);
        }
        else
        {
            btn_incell_tikmark.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        btn_incell_tikmark.backgroundColor=[UIColor clearColor];
        btn_incell_tikmark.layer.cornerRadius = 7.0;
        btn_incell_tikmark.layer.borderColor = [[UIColor whiteColor] CGColor];
        if (IS_IPHONE_6Plus)
        {
            //btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,30,((WIDTH-65)/4),0);
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        //btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,20,40,-40);
        //{top, left, bottom, right};
        //  btn_TableCell_CheckBox1.titleEdgeInsets = UIEdgeInsetsMake(0,-10,4.5,0);
        
        
        [btn_incell_tikmark addTarget:self action:@selector(click_selectObjectAt_favoritecuisines:) forControlEvents:UIControlEventTouchUpInside];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"] forState:UIControlStateSelected];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        btn_incell_tikmark.tag = indexPath.row;
        [cell_for_collection_view addSubview:btn_incell_tikmark];
        
        if ([ary_TEmp containsObject:[array_course_infoodnow objectAtIndex:indexPath.row]])
        {
            [btn_incell_tikmark setSelected:YES];
        }
        else
        {
            [btn_incell_tikmark setSelected:NO];
            
        }
        btn_incell_tikmark.tag = indexPath.row;
        [cell.contentView addSubview:btn_incell_tikmark];
        
        
        
        
        UILabel  * lbl_headings = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus)
        {
            lbl_headings.frame =CGRectMake(15,CGRectGetMaxY(course_icon.frame)+1, ((WIDTH-65)/3),10);
            
        }
        else if (IS_IPHONE_6)
        {
            lbl_headings.frame =CGRectMake(15,CGRectGetMaxY(course_icon.frame)+1, ((WIDTH-65)/3),10);
            
        }
        else
        {
            lbl_headings.frame =CGRectMake(15,CGRectGetMaxY(course_icon.frame)+1, ((WIDTH-65)/3),10);
            
        }
        
        lbl_headings.text =[[array_course_infoodnow objectAtIndex:indexPath.row] valueForKey:@"DishCourseName"];
        
        lbl_headings.backgroundColor=[UIColor clearColor];
        lbl_headings.textColor=[UIColor blackColor];
        lbl_headings.textAlignment=NSTextAlignmentCenter;
        lbl_headings.font = [UIFont fontWithName:kFont size:9];
        [cell_for_collection_view addSubview:lbl_headings];
        
        
        
        int selectedindex=(int)indexPath.row;
        NSLog(@"%d",selectedindex);
        
        
        
        if (IS_IPHONE_6Plus)
        {
            
            cell_for_collection_view .frame = CGRectMake(0,0,((WIDTH-20)/3),75);
            course_icon .frame = CGRectMake(50,20,30,30);
            lbl_headings.frame =CGRectMake(0,CGRectGetMaxY(course_icon.frame)+1, ((WIDTH-25)/3),15);
            //            course_name.frame = CGRectMake(20,CGRectGetMaxY(course_icon.frame)+5,200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
            
        }
        else if(IS_IPHONE_6)
        {
            
            cell_for_collection_view .frame = CGRectMake(0,0,((WIDTH-20)/3),75);
            course_icon .frame = CGRectMake(50,20,30,30);
            lbl_headings.frame =CGRectMake(0,CGRectGetMaxY(course_icon.frame)+1, ((WIDTH-25)/3),15);
            //            course_name.frame = CGRectMake(20,CGRectGetMaxY(course_icon.frame)+5,200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
            
        }
        else
        {
            
            cell_for_collection_view .frame = CGRectMake(0,0,((WIDTH-20)/3),75);
            course_icon .frame = CGRectMake(40,20,30,30);
            lbl_headings.frame =CGRectMake(0,CGRectGetMaxY(course_icon.frame)+1, ((WIDTH-25)/3),15);
            //            course_name.frame = CGRectMake(20,CGRectGetMaxY(course_icon.frame)+5,200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
            
        }
        
        
    }
    
    else
    {
        return cell;
    }
    
    return cell;
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(((WIDTH-20)/3),75);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    str_CourseId = [NSString stringWithFormat:@"%@", [[array_course_infoodnow objectAtIndex:indexPath.row] valueForKey:@"DishCourseID"]];
    [self AFDishesList];
}

-(void)click_selectObjectAt_favoritecuisines:(UIButton *) sender
{
    
    selectedindex= (int)sender.tag;
    
    
    if (sender.tag == selectedindex)
    {
        indexSelected = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        
    }
    
    
    str_CourseId = [[array_course_infoodnow objectAtIndex:sender.tag] valueForKey:@"DishCourseID"];
    [self AFDishesList];
    
    
    /*if([ary_TEmp containsObject:[array_course_infoodnow objectAtIndex:sender.tag]])
     {
     [ary_TEmp removeObject:[array_course_infoodnow objectAtIndex:sender.tag]];
     }
     else
     {
     [ary_TEmp addObject:[array_course_infoodnow objectAtIndex:sender.tag]];
     
     }*/
    
    [ary_TEmp removeAllObjects];
    [ary_TEmp addObject:[array_course_infoodnow objectAtIndex:sender.tag]];
    
    
    [collView_for_cource reloadData];
    
}

#pragma mark tableview_delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [array_DishesList
            count];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 170;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    else{
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
    }
    cell.backgroundColor = [UIColor clearColor];
    
    
    
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH+5, 170);
        
    }
    else if (IS_IPHONE_6)
    {
        img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-5, 170);
        
    }
    else
    {    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-10,170);
        
    }
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    img_cellBackGnd.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [cell.contentView addSubview:img_cellBackGnd];
    
    
    UIImageView *img_dish = [[UIImageView alloc] init];
    img_dish.frame = CGRectMake(7,7, 90,  95 );
    NSString *ImagePath1 =[NSString stringWithFormat:@"%@",[[array_DishesList objectAtIndex:indexPath.row]valueForKey:@"DishImage"]];
    [img_dish setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@"img-user@2x"]];
    [img_cellBackGnd addSubview:img_dish];
    
    /*UIButton *btn_favorite = [UIButton buttonWithType:UIButtonTypeCustom];
     btn_favorite.frame = CGRectMake(5,58, 35, 35);
     //icon_cow .backgroundColor = [UIColor clearColor];
     [btn_favorite addTarget:self action:@selector(btn_favorite_click:) forControlEvents:UIControlEventTouchUpInside];
     [btn_favorite setImage:[UIImage imageNamed:@"favorite-icon@2x.png"] forState:UIControlStateNormal];
     [img_dish   addSubview:btn_favorite];*/
    
    UIButton *btn_number_three = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_number_three.frame = CGRectMake(58,-5, 35, 35);
    // btn_number_three setTitle:[NSString stringWithFormat:@"%@",[[array_DishesList objectAtIndex:indexPath.row]valueForKey:@"DishImage"]] forState:<#(UIControlState)#>
    //icon_cow .backgroundColor = [UIColor clearColor];
    [btn_number_three addTarget:self action:@selector(btn_number_three_click:) forControlEvents:UIControlEventTouchUpInside];
    // btn_number_three.backgroundColor = [UIColor colorWithRed:17/255.0f green:41/255.0f blue:25/255.0f alpha:1];
    //[btn_number_three setImage:[UIImage imageNamed:@"red3-icon@2x.png"] forState:UIControlStateNormal];
    [img_dish   addSubview:btn_number_three];
    
    
    /*UIButton *icon_delete = [UIButton buttonWithType:UIButtonTypeCustom];
     icon_delete.frame = CGRectMake(WIDTH-70,20, 40, 40);
     //icon_cow .backgroundColor = [UIColor clearColor];
     [icon_delete addTarget:self action:@selector(btn_add_to_cart_click:) forControlEvents:UIControlEventTouchUpInside];
     [icon_delete setImage:[UIImage imageNamed:@"img-cart@2x.png"] forState:UIControlStateNormal];
     [cell.contentView   addSubview:icon_delete];*/
    
    
    UILabel *dish_name = [[UILabel alloc]init];
    dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
    dish_name.text = [NSString stringWithFormat:@"%@",[[array_DishesList objectAtIndex:indexPath.row]valueForKey: @"DishName"]];
    dish_name.font = [UIFont fontWithName:kFontBold size:8];
    dish_name.textColor = [UIColor blackColor];
    dish_name.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:dish_name];
    
    
    //    UIImageView *icon_location = [[UIImageView alloc] init];
    //    icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
    //    [icon_location setImage:[UIImage imageNamed:@"location-icon@2x.png"]];
    //    [img_cellBackGnd  addSubview:icon_location];
    //
    //    UILabel *meters = [[UILabel alloc]init];
    //    meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
    //    meters.text = @"5km";
    //    meters.font = [UIFont fontWithName:kFont size:15];
    //    meters.textColor = [UIColor blackColor];
    //    meters.backgroundColor = [UIColor clearColor];
    //    [img_cellBackGnd addSubview:meters];
    
    
    
    
    
    //    UIButton *icon_server = [UIButton buttonWithType:UIButtonTypeCustom];
    //    //   icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
    //    //icon_server .backgroundColor = [UIColor clearColor];
    //    [icon_server  addTarget:self action:@selector(click_on_icon_sever_btn:) forControlEvents:UIControlEventTouchUpInside];
    //    [icon_server setImage:[UIImage imageNamed:@"serving-icon@2x.png"] forState:UIControlStateNormal];
    //    [img_cellBackGnd   addSubview:icon_server];
    
    
    //    UIButton *icon_halal = [UIButton buttonWithType:UIButtonTypeCustom];
    //    icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
    //    //icon_halal .backgroundColor = [UIColor clearColor];
    //    [icon_halal addTarget:self action:@selector(click_on_icon_halal_btn:) forControlEvents:UIControlEventTouchUpInside];
    //    [icon_halal setImage:[UIImage imageNamed:@"halal-icon@2x.png"] forState:UIControlStateNormal];
    //    [img_cellBackGnd   addSubview:icon_halal];
    //
    //    UIImageView *img_non_veg = [[UIImageView alloc] init];
    //    img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
    //    [img_non_veg setImage:[UIImage imageNamed:@"non-veg bt@2x.png"]];
    //    [img_cellBackGnd addSubview:img_non_veg];
    //
    //    UIButton *icon_cow = [UIButton buttonWithType:UIButtonTypeCustom];
    //    icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
    //    //icon_cow .backgroundColor = [UIColor clearColor];
    //    [icon_cow addTarget:self action:@selector(click_on_icon_cow_btn:) forControlEvents:UIControlEventTouchUpInside];
    //    [icon_cow setImage:[UIImage imageNamed:@"cow-icon@2x.png"] forState:UIControlStateNormal];
    //    [img_cellBackGnd   addSubview:icon_cow];
    //
    //    UIButton *icon_fronce = [UIButton buttonWithType:UIButtonTypeCustom];
    //    icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
    //    //icon_cow .backgroundColor = [UIColor clearColor];
    //    [icon_fronce addTarget:self action:@selector(click_on_fronce_btn:) forControlEvents:UIControlEventTouchUpInside];
    //    [icon_fronce setImage:[UIImage imageNamed:@"fronce-icon@2x.png"] forState:UIControlStateNormal];
    //    [img_cellBackGnd   addSubview:icon_fronce];
    
    UILabel *doller_rate = [[UILabel alloc]init];
    doller_rate.frame = CGRectMake(WIDTH-90,85,220, 15);
    doller_rate.text = [NSString stringWithFormat:@"$%@",[[array_DishesList objectAtIndex:indexPath.row]valueForKey: @"DishPrice"]];
    [doller_rate setTextAlignment:NSTextAlignmentLeft];
    doller_rate.font = [UIFont fontWithName:kFontBold size:16];
    doller_rate.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    doller_rate.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:doller_rate];
    
    
    UIImageView *img_line = [[UIImageView alloc] init];
    img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
    [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
    [img_cellBackGnd addSubview:img_line];
    
    
    UIButton *icon_take = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
    
    UIButton *icon_deliver = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    
    UIImageView *icon_Dine = [[UIImageView alloc] init];
    icon_Dine.frame = CGRectMake(CGRectGetMaxX(icon_deliver.frame)+2,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    
    //icon_take .backgroundColor = [UIColor clearColor];
    //    [icon_take addTarget:self action:@selector(click_on_take_out_btn:) forControlEvents:UIControlEventTouchUpInside];
    if ([[[[array_DishesList objectAtIndex:indexPath.row]valueForKey:@"takeout"] stringValue] isEqualToString:@"1"])
    {
        [icon_take setImage:[UIImage imageNamed:@"take-icon@2x.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        icon_deliver.frame = CGRectMake(10,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        icon_Dine.frame = CGRectMake(CGRectGetMaxX(icon_deliver.frame)+2,CGRectGetMaxY(img_line.frame)+5,  28, 30);
        [icon_take setImage:[UIImage imageNamed:@""]forState:UIControlStateNormal];
        
    }
    //    [icon_take setImage:[UIImage imageNamed:@"take-icon@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_take];
    
    //icon_deliver .backgroundColor = [UIColor clearColor];
    [icon_deliver addTarget:self action:@selector(click_on_icon_delivery_btn:) forControlEvents:UIControlEventTouchUpInside];
    if ([[[[array_DishesList objectAtIndex:indexPath.row]valueForKey:@"delivery"] stringValue] isEqualToString:@"1"])
    {
        [icon_deliver setImage:[UIImage imageNamed:@"deliver-icon@2x.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        icon_Dine.frame = CGRectMake(CGRectGetMaxX(icon_take.frame)+2,CGRectGetMaxY(img_line.frame)+5,  28, 30);
        [icon_deliver setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        ;
    }
    [img_cellBackGnd   addSubview:icon_deliver];
    
    
    
    if ([[[[array_DishesList objectAtIndex:indexPath.row]valueForKey:@"dinein"] stringValue] isEqualToString:@"1"])
    {
        [icon_Dine setImage:[UIImage imageNamed:@"dine-in-img@2x"]];
    }
    else
    {
        [icon_Dine setImage:[UIImage imageNamed:@""]] ;
    }
    //    [icon_Dine setImage:[UIImage imageNamed:@"dine-in-img@2x"]];
    [img_cellBackGnd addSubview:icon_Dine];
    
    
    UIButton *img_btn_seving_Now = [UIButton buttonWithType:UIButtonTypeCustom];
    img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //img_btn_deliver_Now .backgroundColor = [UIColor clearColor];
    [img_btn_seving_Now addTarget:self action:@selector(click_on_seving_now_btn:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([[[[array_DishesList objectAtIndex:indexPath.row]valueForKey:@"DishlistDetail"]valueForKey:@"Serving_Type"]isEqualToString:@"serve_later"])
    {
        [img_btn_seving_Now setImage:[UIImage imageNamed:@"icon-serving-time@2x"]forState:UIControlStateNormal];
    }
    else if ([[[[array_DishesList objectAtIndex:indexPath.row]valueForKey:@"DishlistDetail"]valueForKey:@"Serving_Type"]isEqualToString:@"on_request"])
    {
        [img_btn_seving_Now setImage:[UIImage imageNamed:@"iicon-on_reuest@2x"]forState:UIControlStateNormal];
    }
    else
    {
        [img_btn_seving_Now setImage:[UIImage imageNamed:@"now-icon@2x.png"]forState:UIControlStateNormal];
    }
    //    [img_btn_seving_Now setImage:[UIImage imageNamed:@"now-icon@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:img_btn_seving_Now];
    
    
    //    UIButton *img_btn_chef_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    //    img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
    //    //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
    //    [img_btn_chef_menu addTarget:self action:@selector(click_on_icon_chef_menu_btn:) forControlEvents:UIControlEventTouchUpInside];
    //    [img_btn_chef_menu setImage:[UIImage imageNamed:@"chef-menu-icon@2x.png"] forState:UIControlStateNormal];
    //    [img_cellBackGnd   addSubview:img_btn_chef_menu];
    //
    //
    //
    //    UILabel *quantity = [[UILabel alloc]init];
    //    //  quantity.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
    //    quantity.font = [UIFont fontWithName:kFontBold size:10];
    //    quantity.text = [NSString stringWithFormat:@"%@", [[array_DishesList objectAtIndex:indexPath.row]valueForKey:@"quantity"]];
    //    //    likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
    //    quantity.textColor = [UIColor whiteColor];
    //    quantity.textAlignment = NSTextAlignmentCenter;
    //    quantity.backgroundColor = [UIColor blackColor];
    //    [img_btn_chef_menu addSubview:quantity];
    
    
    UILabel *total_Servings = [[UILabel alloc]init];
    
    total_Servings.text = @"Total Servings";
    //    likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
    total_Servings.font = [UIFont fontWithName:kFontBold size:11];
    //    total_Servings.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    total_Servings.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:total_Servings];
    
    
    UILabel *Serving_NO = [[UILabel alloc]init];
    //    Serving_NO.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)+2,CGRectGetMaxY(total_Servings.frame),100, 12);  Total_Serving
    Serving_NO.font = [UIFont fontWithName:kFont size:11];
    Serving_NO.text = [NSString stringWithFormat:@"%@", [[array_DishesList objectAtIndex:indexPath.row]valueForKey:@"Total_Serving"]];
    
    //    likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
    
    //    Serving_NO.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    Serving_NO.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:Serving_NO];
    
    
    UIButton *icon_thumb = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+5, 20, 20);
    //icon_thumb .backgroundColor = [UIColor clearColor];
    [icon_thumb addTarget:self action:@selector(click_on_icon_thumb_btn:) forControlEvents:UIControlEventTouchUpInside];
    [icon_thumb setImage:[UIImage imageNamed:@"thumb-icon@2x.png"] forState:UIControlStateNormal];
    [img_cellBackGnd   addSubview:icon_thumb];
    
    UILabel *likes = [[UILabel alloc]init];
    likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
    likes.text = [NSString stringWithFormat:@"%@%%", [[array_DishesList objectAtIndex:indexPath.row]valueForKey:@"likes"]];
    likes.font = [UIFont fontWithName:kFont size:10];
    likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    likes.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:likes];
    
    
    if (IS_IPHONE_6Plus)
    {
        img_dish.frame = CGRectMake(7,7, 90,  95 );
        //btn_favorite.frame = CGRectMake(5,58, 35, 35);
        btn_number_three.frame = CGRectMake(58,-5, 35, 35);
        //icon_delete.frame = CGRectMake(WIDTH-70,30, 40, 40);
        dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 20);
        //        icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        //        meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        //icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
        //        icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
        //        img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
        //        icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
        //        icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
        doller_rate.frame = CGRectMake(WIDTH-90,85,220, 15);
        img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
        icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        icon_Dine.frame = CGRectMake(CGRectGetMaxX(icon_deliver.frame)+2,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        
        img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        total_Servings.frame = CGRectMake(CGRectGetMaxX(img_btn_seving_Now.frame)+2,CGRectGetMaxY(img_line.frame)+8,80, 15);
        Serving_NO.frame = CGRectMake(CGRectGetMaxX(img_btn_seving_Now.frame)+2,CGRectGetMaxY(total_Servings.frame),80, 12);
        //        img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+4, 20, 20);
        likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+9,128,100, 10);
        
        dish_name.font = [UIFont fontWithName:kFontBold size:18];
        
    }
    else if (IS_IPHONE_6)
    {
        img_dish.frame = CGRectMake(7,7, 90,  95 );
        //btn_favorite.frame = CGRectMake(5,58, 35, 35);
        btn_number_three.frame = CGRectMake(58,-5, 35, 35);
        
        //icon_delete.frame = CGRectMake(WIDTH-62,30, 40, 40);
        
        dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 20);
        //        icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        //        meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        //icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
        //        icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
        //        img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
        //        icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
        //        icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
        doller_rate.frame = CGRectMake(WIDTH-90,85,220, 15);
        img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
        icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        icon_Dine.frame = CGRectMake(CGRectGetMaxX(icon_deliver.frame)+2,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        icon_Dine.frame = CGRectMake(CGRectGetMaxX(icon_deliver.frame)+2,CGRectGetMaxY(img_line.frame)+5,  28, 30);
        img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //        img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        total_Servings.frame = CGRectMake(CGRectGetMaxX(img_btn_seving_Now.frame)+2,CGRectGetMaxY(img_line.frame)+8,80, 15);
        Serving_NO.frame = CGRectMake(CGRectGetMaxX(img_btn_seving_Now.frame)+2,CGRectGetMaxY(total_Servings.frame),80, 12);
        icon_thumb.frame = CGRectMake(300,CGRectGetMaxY(img_line.frame)+5, 20, 20);
        likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
        
        
        dish_name.font = [UIFont fontWithName:kFontBold size:18];
        
    }
    else
    {
        img_dish.frame = CGRectMake(7,7, 90,  95 );
        //btn_favorite.frame = CGRectMake(5,58, 35, 35);
        btn_number_three.frame = CGRectMake(58,-5, 35, 35);
        
        //icon_delete.frame = CGRectMake(WIDTH-65,30, 40, 40);
        
        dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 20);
        //        icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        //        meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        //icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
        //        icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
        //        img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
        //        icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
        //        icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
        doller_rate.frame = CGRectMake(IS_IPHONE_5?WIDTH-80:WIDTH-90,85,220, 15);
        img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+21, WIDTH-50, 0.5 );
        icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        icon_Dine.frame = CGRectMake(CGRectGetMaxX(icon_deliver.frame)+2,CGRectGetMaxY(img_line.frame)+5,  28, 30);
        img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        total_Servings.frame = CGRectMake(CGRectGetMaxX(img_btn_seving_Now.frame)+2,CGRectGetMaxY(img_line.frame)+8,80, 15);
        Serving_NO.frame = CGRectMake(CGRectGetMaxX(img_btn_seving_Now.frame)+2,CGRectGetMaxY(total_Servings.frame),80, 12);
        icon_thumb.frame = CGRectMake(IS_IPHONE_5?240:230,CGRectGetMaxY(img_line.frame)+5, 20, 20);
        likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
        
        
        dish_name.font = [UIFont fontWithName:kFontBold size:IS_IPHONE_5?15:15];
    }
    
    
    
    UIScrollView*delivery_Scroll;
    delivery_Scroll = [[UIScrollView alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+7, CGRectGetMaxY(dish_name.frame)+55, 180, 30);
    }
    else  if (IS_IPHONE_6)
    {
        
        delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+7, CGRectGetMaxY(dish_name.frame)+55, 180, 30);
    }
    else  if (IS_IPHONE_5)
    {
        
        delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+2, CGRectGetMaxY(dish_name.frame)+55, 140, 30);
    }
    else
    {
        delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+7, CGRectGetMaxY(dish_name.frame)+40, 120, 30);
    }
    
    delivery_Scroll.backgroundColor = [UIColor whiteColor];
    delivery_Scroll.bounces=YES;
    //delivery_Scroll.layer.borderWidth = 1.0;
    delivery_Scroll.layer.borderColor = [UIColor blackColor].CGColor;
    delivery_Scroll.showsVerticalScrollIndicator = NO;
    delivery_Scroll.showsHorizontalScrollIndicator = NO;
    
    //delivery_Scroll.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [delivery_Scroll setScrollEnabled:YES];
    delivery_Scroll.userInteractionEnabled = YES;
    [img_cellBackGnd addSubview:delivery_Scroll];
    
    int totalPage;
    
    totalPage = (int)[[[array_DishesList objectAtIndex:indexPath.row] valueForKey:@"DishRestrictions"] count];
    [delivery_Scroll setContentSize:CGSizeMake(40*totalPage, delivery_Scroll.frame.size.height)];
    
    
    
    for (int j = 0; j<totalPage; j++)
    {
        
        
        UIImageView *img_non_veg = [[UIImageView alloc] init];
        
        
        img_non_veg.frame = CGRectMake(20*j,0, 18, 18);
        
        
        NSString *url_Img = [NSString stringWithFormat: @"%@", [[[[array_DishesList objectAtIndex:indexPath.row] valueForKey:@"DishRestrictions"] objectAtIndex:j] valueForKey:@"resImage"]];
        
        [img_non_veg setImageWithURL:[NSURL URLWithString:url_Img] placeholderImage:nil];
        
        //img_non_veg.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img]]];
        
        UIImage *scaleImage = [self imageWithImage: img_non_veg.image convertToSize:CGSizeMake(75, 75)];
        img_non_veg.image = scaleImage;
        
        img_non_veg.backgroundColor=[UIColor clearColor];
        [delivery_Scroll addSubview:img_non_veg];
        
        
        //        UIButton*get_Deliver_Address_Btn;
        //        get_Deliver_Address_Btn = [[UIButton alloc]init];
        //        get_Deliver_Address_Btn.frame = CGRectMake(0,25*j, delivery_Scroll.frame.size.width, 25);
        //        [get_Deliver_Address_Btn addTarget:self action:@selector(get_Deliver_Address_BtnMethod:) forControlEvents:UIControlEventTouchUpInside];
        //        get_Deliver_Address_Btn.tag = j;
        //        get_Deliver_Address_Btn.titleLabel.text = [NSString stringWithFormat:@"%d",indexPath.row];
        //        [get_Deliver_Address_Btn setBackgroundColor:[UIColor clearColor]];
        //        //   get_Deliver_Address_Btn.layer.borderWidth = 1.0;
        //        [delivery_Scroll addSubview: get_Deliver_Address_Btn];
    }
    
    return cell;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    [self.navigationController pushViewController:VC animated:NO];
    
    
    ViewDishDetailsVC*vc = [ViewDishDetailsVC new];
    vc.str_dishID = [[array_DishesList objectAtIndex:indexPath.row] valueForKey:@"DishID"];
    vc.str_dishUID =[[array_DishesList objectAtIndex:indexPath.row] valueForKey:@"DishUID"];
    vc.str_TYPE = [[array_DishesList objectAtIndex:indexPath.row] valueForKey:@"dish_type"];
    [self presentViewController:vc animated:NO completion:nil];
    
}


- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}


#pragma mark click_events

-(void)btn_HomeScreenClick:(UIButton *)sender
{
    NSLog(@"btn_HomeScreenClick");
}

-(void)click_on_logo_btn:(UIButton *)sender
{
    NSLog(@"click_on_logo_btn");
    UserSupportVC *vc = [[UserSupportVC alloc]init];
    [self presentViewController:vc animated:NO completion:nil];
}
-(void)btn_on_message_click:(UIButton *)sender
{
    NSLog(@"btn_on_message_click");
}

-(void)click_on_left_arrow_btn:(UIButton *)sender
{
    NSLog(@"click_on_left_arrow_btn");
}
-(void)click_on_right_arrow_btn:(UIButton *)sender
{
    NSLog(@"click_on_right_arrow_btn");
}

-(void)click_on_dishandmeal_btn:(UIButton *)sender
{
    NSLog(@"click_on_dishandmeal_btn");
    
    MenuScreenVC *vcc = [[MenuScreenVC alloc]init];
    //[self.navigationController pushViewController:vcc animated:NO];
    [self presentViewController:vcc animated:NO completion:nil];
    
}

-(void)btn_favorite_click:(UIButton *)sender
{
    
}
-(void)btn_number_three_click:(UIButton *)sender
{
    
}
-(void)btn_add_to_cart_click:(UIButton *)sender
{
    
}
-(void)click_on_icon_sever_btn:(UIButton *)sender
{
    
}
-(void)click_on_icon_delivery_btn:(UIButton *)sender
{
    
}
-(void)click_on_seving_now_btn:(UIButton *)sender
{
    
}
-(void)click_on_icon_thumb_btn:(UIButton *)sender
{
    
}

-(void) click_selectObjectAt_course_infoodnow:(UIButton *)sender
{
    
    
    if([Arr_temp_course_infoodnow containsObject:[array_course_infoodnow objectAtIndex:sender.tag]])
    {
        [Arr_temp_course_infoodnow removeObject:[array_course_infoodnow objectAtIndex:sender.tag]];
    }
    else
    {
        [Arr_temp_course_infoodnow addObject:[array_course_infoodnow objectAtIndex:sender.tag]];
    }
    
    //[collectionView_FashionCategory reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:sender.tag inSection:0]]  withRowAnimation:UITableViewRowAnimationFade];
    [collView_for_cource reloadData];
    
    str_CourseId = [NSString stringWithFormat:@"%@", [[array_course_infoodnow objectAtIndex:sender.tag] valueForKey:@"DishCourseID"]];
    [self AFDishesList];
}


//========================================== functionality ===========================

# pragma mark Course in food now

-(void)AFCourse_in_dishandmeal
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kCourses
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsecourseindishandmeal:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFCourse_in_dishandmeal];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsecourseindishandmeal :(NSDictionary * )TheDict
{
    [array_course_infoodnow removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DishCoursetList"] count]; i++)
        {
            [array_course_infoodnow addObject:[[TheDict valueForKey:@"DishCoursetList"] objectAtIndex:i]];
            
        }
        
        if ([ary_TEmp count] == 0) {
            str_CourseId =[NSString stringWithFormat:@"%@", [[array_course_infoodnow objectAtIndex:0] valueForKey:@"DishCourseID"]];
            [ary_TEmp addObject:[array_course_infoodnow objectAtIndex:0]];
        }
        else{
            str_CourseId =[NSString stringWithFormat:@"%@", [[ary_TEmp objectAtIndex:0] valueForKey:@"DishCourseID"]];
        }
        
        //str_CourseId =[NSString stringWithFormat:@"%@", [[array_course_infoodnow objectAtIndex:0] valueForKey:@"DishCourseID"]];
        
        
        [self AFDishesList];
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //  txt_popular.text=@"";
        
        
    }
    
    [collView_for_cource reloadData];
    
    // str_cuisenCatId_in_foodnow = @"80";
    
    
    
}


# pragma mark DishesList

-(void)AFDishesList
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    
    NSDictionary *params = @{
                             @"uid" :    [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                             @"course_id" : str_CourseId
                             };
    
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kChefProfileInUserMenu
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseDishesList:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFDishesList];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseDishesList :(NSDictionary * )TheDict
{
    [array_DishesList removeAllObjects];
    //    [ary_CuisinesList removeAllObjects];
    //    [ary_DietaryList removeAllObjects];
    //    [ary_CollectionCount removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DishsList"] count]; i++)
        {
            [array_DishesList addObject:[[TheDict valueForKey:@"DishsList"] objectAtIndex:i]];
            
        }
        
        //        NSLog(@"Dish List array: %@",array_DishesList);
        //        for (int i = 0 ; i< [array_DishesList count]; i++)
        //        {
        //            [ary_DietaryList addObject: [[array_DishesList objectAtIndex:i]valueForKey: @"DishRestrictions" ]];
        //
        //            [ary_CuisinesList addObject:[[array_DishesList objectAtIndex:i]valueForKey: @"cuisenCetegory" ]];
        //
        //            ary_CollectionCount = [[ary_CuisinesList arrayByAddingObjectsFromArray:ary_DietaryList]mutableCopy];
        //        }
        //        NSLog(@"Dish List array: %@",ary_CollectionCount);
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        NSLog(@"No item found on this date");
        
        [self popup_Alertview:[TheDict valueForKey:@"message"]];
    }
    
    [table_for_dish_items reloadData];
    
    // str_cuisenCatId_in_foodnow = @"80";
    
    
    
    
}
-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
