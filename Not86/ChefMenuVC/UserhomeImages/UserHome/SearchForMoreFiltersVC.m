//
//  SearchForMoreFiltersVC.m
//  Not86
//
//  Created by Admin on 30/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "SearchForMoreFiltersVC.h"
//#import "LocationVC.h"
#import "Define.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


@interface SearchForMoreFiltersVC ()<UIScrollViewDelegate,UITextFieldDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

{
    UIImageView *img_header;
    UIView *view_serch_for_food_later;
    UIScrollView *scroll;
    UICollectionViewFlowLayout *layout;
    UICollectionView *collView_serviceDirectory;
    //NSMutableArray *ary_CatagoryList;
    
}

@end

@implementation SearchForMoreFiltersVC
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBodyDesign];
    //[self integrateCollectionview];
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [img_header setUserInteractionEnabled:YES];
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu.frame = CGRectMake(10,13,20,20);
    icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    [icon_menu addTarget:self action:@selector(btn_menu_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_header  addSubview:icon_menu];
    
    UIButton *icon_menut = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menut.frame = CGRectMake(0,0,50,50);
    icon_menut .backgroundColor = [UIColor clearColor];
    [icon_menut addTarget:self action:@selector(btn_menu_click:) forControlEvents:UIControlEventTouchUpInside];
    //[icon_drop_down setImage:[UIImage imageNamed:@"drop-down@2x.png"] forState:UIControlStateNormal];
    [self.view   addSubview:icon_menut];
    
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 100, 45);
    lbl_User_Sign_Up.text = @"Food Later";
    lbl_User_Sign_Up.font = [UIFont fontWithName:@"Helvitica" size:10];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    UIButton *icon_drop_down = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_drop_down.frame = CGRectMake(CGRectGetMaxX(lbl_User_Sign_Up.frame),15,20,20);
    icon_drop_down .backgroundColor = [UIColor clearColor];
    [icon_drop_down addTarget:self action:@selector(btn_drop_down:) forControlEvents:UIControlEventTouchUpInside];
    [icon_drop_down setImage:[UIImage imageNamed:@"drop-down@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_drop_down];
    
    UIButton *btn_on_food_later_in_head = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_food_later_in_head.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0,150,45);
    btn_on_food_later_in_head .backgroundColor = [UIColor clearColor];
    [btn_on_food_later_in_head addTarget:self action:@selector(btn_on_food_later_label_click:) forControlEvents:UIControlEventTouchUpInside];
    //[icon_drop_down setImage:[UIImage imageNamed:@"drop-down@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:btn_on_food_later_in_head];
    
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}
-(void)integrateBodyDesign
{
    
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor clearColor];
    scroll.frame = CGRectMake(0, 0, WIDTH, HEIGHT);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    
    view_serch_for_food_later = [[UIView alloc]init];
    view_serch_for_food_later.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT);
    view_serch_for_food_later.backgroundColor=[UIColor clearColor];
    [scroll  addSubview:  view_serch_for_food_later];
    
    
    UILabel *lbl_date = [[UILabel alloc]init];
    lbl_date.frame = CGRectMake(130,0, 150, 45);
    lbl_date.text = @"Date";
    lbl_date.font = [UIFont fontWithName:@"Arial" size:18];
    lbl_date.textColor = [UIColor blackColor];
    lbl_date.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_date];
    
    UIImageView *img_calender = [[UIImageView alloc]init];
    img_calender.frame = CGRectMake(85,CGRectGetMaxY(lbl_date.frame), 30, 30);
    [img_calender setImage:[UIImage imageNamed:@"img-calender@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_calender setUserInteractionEnabled:YES];
    [view_serch_for_food_later addSubview:img_calender];
    
    UITextField *txt_date = [[UITextField alloc] init];
    txt_date .frame = CGRectMake(CGRectGetMaxX( img_calender.frame)+5,CGRectGetMaxY(lbl_date.frame), 100, 30);
    txt_date .borderStyle = UITextBorderStyleNone;
    txt_date .textColor = [UIColor grayColor];
    txt_date .font = [UIFont fontWithName:@"Arial" size:13];
    txt_date .placeholder = @"15-07-2015";
    [txt_date  setValue:[UIFont fontWithName:@"Arial" size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_date  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_date .leftView = padding;
    txt_date .leftViewMode = UITextFieldViewModeAlways;
    txt_date .userInteractionEnabled=YES;
    txt_date .textAlignment = NSTextAlignmentLeft;
    txt_date .backgroundColor = [UIColor clearColor];
    txt_date .keyboardType = UIKeyboardTypeAlphabet;
    txt_date .delegate = self;
    [view_serch_for_food_later addSubview:txt_date ];
    
    UIButton *btn_set_date = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_set_date.frame = CGRectMake(85,CGRectGetMaxY(lbl_date.frame),150,35);
    btn_set_date .backgroundColor = [UIColor clearColor];
    [btn_set_date addTarget:self action:@selector(btn_set_date_click:) forControlEvents:UIControlEventTouchUpInside];
    // [btn_set_date setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_set_date];
    
    
    UILabel *lbl_serving_time = [[UILabel alloc]init];
    lbl_serving_time.frame = CGRectMake(105,CGRectGetMaxY(txt_date.frame)+5, 150, 45);
    lbl_serving_time.text = @"Serving Time";
    lbl_serving_time.font = [UIFont fontWithName:@"Arial" size:18];
    lbl_serving_time.textColor = [UIColor blackColor];
    lbl_serving_time.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_serving_time];
    
    UIImageView *img_clock = [[UIImageView alloc]init];
    img_clock.frame = CGRectMake(85,CGRectGetMaxY(lbl_serving_time.frame), 30, 30);
    [img_clock setImage:[UIImage imageNamed:@"img-clock@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_clock setUserInteractionEnabled:YES];
    [view_serch_for_food_later addSubview:img_clock];
    
    UITextField *txt_set_time = [[UITextField alloc] init];
    txt_set_time .frame = CGRectMake(CGRectGetMaxX( img_clock.frame)+5,CGRectGetMaxY(lbl_serving_time.frame), 100, 30);
    txt_set_time .borderStyle = UITextBorderStyleNone;
    txt_set_time .textColor = [UIColor grayColor];
    txt_set_time .font = [UIFont fontWithName:@"Arial" size:13];
    txt_set_time .placeholder = @"10:45AM";
    [txt_set_time  setValue:[UIFont fontWithName:@"Arial" size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_set_time  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_set_time .leftView = padding1;
    txt_set_time .leftViewMode = UITextFieldViewModeAlways;
    txt_set_time .userInteractionEnabled=YES;
    txt_set_time .textAlignment = NSTextAlignmentLeft;
    txt_set_time .backgroundColor = [UIColor clearColor];
    txt_set_time .keyboardType = UIKeyboardTypeAlphabet;
    txt_set_time .delegate = self;
    [view_serch_for_food_later addSubview:txt_set_time ];
    
    UIButton *btn_set_time = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_set_time.frame = CGRectMake(85,CGRectGetMaxY(lbl_serving_time.frame),150,35);
    btn_set_time .backgroundColor = [UIColor clearColor];
    [btn_set_time addTarget:self action:@selector(btn_set_time_click:) forControlEvents:UIControlEventTouchUpInside];
    // [btn_set_date setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_set_time];
    
    UILabel *lbl_location = [[UILabel alloc]init];
    lbl_location.frame = CGRectMake(105,CGRectGetMaxY(img_clock.frame)+5, 150, 45);
    lbl_location.text = @"Location";
    lbl_location.font = [UIFont fontWithName:@"Arial" size:18];
    lbl_location.textColor = [UIColor blackColor];
    lbl_location.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_location];
    
    UIImageView *img_pointer_location = [[UIImageView alloc]init];
    img_pointer_location.frame = CGRectMake(25,CGRectGetMaxY(lbl_location.frame), 30, 30);
    [img_pointer_location setImage:[UIImage imageNamed:@"location-pointer@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_pointer_location setUserInteractionEnabled:YES];
    [view_serch_for_food_later addSubview:img_pointer_location];
    
    
    UITextField *txt_set_location = [[UITextField alloc] init];
    txt_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+5,CGRectGetMaxY(lbl_location.frame), 250, 30);
    txt_set_location .borderStyle = UITextBorderStyleNone;
    txt_set_location .textColor = [UIColor grayColor];
    txt_set_location .font = [UIFont fontWithName:@"Arial" size:15];
    txt_set_location .placeholder = @"Smith Street, 77 Block (5km)";
    [txt_set_location  setValue:[UIFont fontWithName:@"Arial" size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_set_location  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_set_location .leftView = padding2;
    txt_set_location .leftViewMode = UITextFieldViewModeAlways;
    txt_set_location .userInteractionEnabled=YES;
    txt_set_location .textAlignment = NSTextAlignmentLeft;
    txt_set_location .backgroundColor = [UIColor clearColor];
    txt_set_location .keyboardType = UIKeyboardTypeAlphabet;
    txt_set_location .delegate = self;
    [view_serch_for_food_later addSubview:txt_set_location ];
    
    UIButton *btn_img_right = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_right.frame = CGRectMake(CGRectGetMidX(txt_set_location.frame)+80,CGRectGetMaxY(lbl_location.frame),30,30);
    btn_img_right .backgroundColor = [UIColor clearColor];
    [btn_img_right addTarget:self action:@selector(btn_right_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_right setImage:[UIImage imageNamed:@"img-right@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_img_right];
    
    UIButton *btn_on_set_location = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_set_location.frame = CGRectMake(25,CGRectGetMaxY(lbl_location.frame),270,35);
    btn_on_set_location .backgroundColor = [UIColor clearColor];
    [btn_on_set_location addTarget:self action:@selector(btn_on_set_location_click:) forControlEvents:UIControlEventTouchUpInside];
    // [btn_set_date setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_on_set_location];
    
    UILabel *lbl_seving_type = [[UILabel alloc]init];
    lbl_seving_type.frame = CGRectMake(105,CGRectGetMaxY(txt_set_location.frame)+5, 150, 45);
    lbl_seving_type.text = @"Serving Type";
    lbl_seving_type.font = [UIFont fontWithName:kFontHelvetica size:18];
    lbl_seving_type.textColor = [UIColor blackColor];
    lbl_seving_type.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_seving_type];
    
    
    UIButton *btn_img_dine_in = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_dine_in.frame = CGRectMake(45,CGRectGetMaxY(lbl_seving_type.frame),50,50);
    btn_img_dine_in .backgroundColor = [UIColor clearColor];
    [btn_img_dine_in addTarget:self action:@selector(btn_img_dine_in_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_dine_in setImage:[UIImage imageNamed:@"img-dine@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_img_dine_in];
    
    
    UIButton *btn_img_take_out = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_take_out.frame = CGRectMake(CGRectGetMidX(btn_img_dine_in.frame)+65,CGRectGetMaxY(lbl_seving_type.frame),50,50);
    btn_img_take_out .backgroundColor = [UIColor clearColor];
    [btn_img_take_out addTarget:self action:@selector(btn_img_take_out_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_take_out setImage:[UIImage imageNamed:@"img-take-out@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_img_take_out];
    
    UIButton *btn_img_delivery = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_delivery.frame = CGRectMake(CGRectGetMidX(btn_img_take_out.frame)+65,CGRectGetMaxY(lbl_seving_type.frame),50,50);
    btn_img_delivery .backgroundColor = [UIColor clearColor];
    [btn_img_delivery addTarget:self action:@selector(btn_img_delivery_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_delivery setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_img_delivery];
    
    UILabel *lbl_keywords = [[UILabel alloc]init];
    lbl_keywords.frame = CGRectMake(130,CGRectGetMaxY(btn_img_take_out.frame)+5, 150, 45);
    lbl_keywords.text = @"Keywords";
    lbl_keywords.font = [UIFont fontWithName:kFontHelvetica size:18];
    lbl_keywords.textColor = [UIColor blackColor];
    lbl_keywords.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_keywords];
    
    UIImageView *img_serch_bar = [[UIImageView alloc]init];
    img_serch_bar.frame = CGRectMake(25,CGRectGetMaxY(lbl_keywords.frame), 290, 30);
    [img_serch_bar setImage:[UIImage imageNamed:@"serch-bar@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_serch_bar setUserInteractionEnabled:YES];
    [view_serch_for_food_later addSubview:img_serch_bar];
    
    UILabel *lbl_search = [[UILabel alloc]init];
    lbl_search.frame = CGRectMake(10,-4, 200, 45);
    lbl_search.text = @"Search...";
    lbl_search.font = [UIFont fontWithName:kFontHelvetica size:15];
    lbl_search.textColor = [UIColor blackColor];
    lbl_search.backgroundColor = [UIColor clearColor];
    [img_serch_bar addSubview:lbl_search];
    
    UIImageView *icon_search = [[UIImageView alloc]init];
    icon_search.frame = CGRectMake(255,8, 15, 15);
    [icon_search setImage:[UIImage imageNamed:@"serach1@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_search setUserInteractionEnabled:YES];
    [img_serch_bar addSubview:icon_search];
    
    UIButton *btn_on_search_bar = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_search_bar.frame = CGRectMake(0,0, 290, 30);
    btn_on_search_bar .backgroundColor = [UIColor clearColor];
    [btn_on_search_bar addTarget:self action:@selector(btn_on_search_bar_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [img_serch_bar   addSubview:btn_on_search_bar];
    
    
    layout=[[UICollectionViewFlowLayout alloc] init];
    collView_serviceDirectory = [[UICollectionView alloc] initWithFrame:CGRectMake(30,CGRectGetMaxY(btn_on_search_bar.frame)+5,260,150)
                                                   collectionViewLayout:layout];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_serviceDirectory setDataSource:self];
    [collView_serviceDirectory setDelegate:self];
    collView_serviceDirectory.scrollEnabled = YES;
    collView_serviceDirectory.showsVerticalScrollIndicator = NO;
    collView_serviceDirectory.pagingEnabled = NO;
    [collView_serviceDirectory registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_serviceDirectory setBackgroundColor:[UIColor clearColor]];
    layout.minimumInteritemSpacing = 4;
    layout.minimumLineSpacing = 7;
    collView_serviceDirectory.userInteractionEnabled = YES;
    [self.view addSubview:collView_serviceDirectory];
    
    
    [scroll setContentSize:CGSizeMake(0,1000)];
}
#pragma mark Collectionview


#pragma mark UiCollectionView Delegate Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 5;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView1 dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    UIImageView *img_backGnd = [[UIImageView alloc]initWithFrame:CGRectMake(1, 0, 82, 110)];
    //[img_backGnd setImage:[UIImage imageNamed:@"img_BackGnd@2x.png"]];
    img_backGnd.backgroundColor = [UIColor clearColor];
    [cell.contentView addSubview:img_backGnd];
    
    
    
    UIImageView *img_dots = [[UIImageView alloc]initWithFrame:CGRectMake(2, 3, 75,75)];
    [img_dots setImage:[UIImage imageNamed:@"img_whitecircle@2x.png"]];
    [img_dots setUserInteractionEnabled:YES];
    [img_dots setContentMode:UIViewContentModeScaleAspectFill];
    [img_dots setClipsToBounds:YES];
    [img_dots setUserInteractionEnabled:YES];
    [img_backGnd addSubview:img_dots];
    
    
    
    UIImageView *img_Images = [[UIImageView alloc]initWithFrame:CGRectMake(6, 8, 66,66)];
    //    NSString *ImagePath1 =  [[NSString stringWithFormat:@"%@",[[ary_CatagoryList objectAtIndex:indexPath.row] valueForKey:@"cat_img"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //    [img_Images setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@"img_merchantPlaceholder@2x.png"]];
    //
    //    [img_Images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[[ary_CatagoryList objectAtIndex:indexPath.row] valueForKey:@"cat_img"]]]];
    img_Images.layer.cornerRadius = 66/2;
    [img_Images setUserInteractionEnabled:YES];
    [img_Images setContentMode:UIViewContentModeScaleAspectFill];
    [img_Images setClipsToBounds:YES];
    [img_Images setUserInteractionEnabled:YES];
    [img_backGnd addSubview:img_Images];
    
    
    // str_amtCC = [ary_amtCC objectAtIndex:indexPath.row];
    UILabel *lbl_title = [[UILabel alloc]initWithFrame:CGRectMake(1,CGRectGetMaxY(img_dots.frame)+4,78, 45)];
    lbl_title.font = [UIFont fontWithName:kFontBold size:10];
    //    lbl_title.text = [NSString stringWithFormat:@"%@",[[ary_CatagoryList objectAtIndex:indexPath.row] valueForKey:@"title"]];
    lbl_title.textColor = [UIColor whiteColor];
    lbl_title.numberOfLines = 0;
    lbl_title.textAlignment = NSTextAlignmentCenter;
    lbl_title.backgroundColor = [UIColor clearColor];
    [img_backGnd addSubview:lbl_title];
    
    //str_img = [ary_images objectAtIndex:indexPath.row];
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(80, 130);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}



-(void)btn_menu_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click");
    [self.navigationController popViewControllerAnimated:NO];
    
    
}
-(void)btn_on_food_later_label_click:(UIButton *)sender
{
    NSLog(@"btn_on_food_later_label_click");
    
}
-(void)btn_drop_down:(UIButton *)sender
{
    NSLog(@"btn_on_food_later_label_click");
    
}
-(void)btn_set_date_click:(UIButton *)sender
{
    NSLog(@"img_fb_click Btn Click");
    
}
-(void)btn_set_time_click:(UIButton *)sender
{
    NSLog(@"btn_set_time_click:");
    
}
-(void)btn_right_click:(UIButton *)sender
{
    NSLog(@"btn_right_click:");
    
}
-(void)btn_on_set_location_click:(UIButton *)sender
{
    NSLog(@"btn_on_set_location_click::");
   // LocationVC*vc = [[LocationVC alloc]init];
   // [self.navigationController pushViewController:vc animated:NO];
    
    
}
-(void)btn_img_dine_in_click:(UIButton *)sender
{
    NSLog(@"btn_img_dine_in_click:");
    
}
-(void)btn_img_take_out_click:(UIButton *)sender
{
    NSLog(@"btn_img_take_out_click:");
    
}
-(void)btn_img_delivery_click:(UIButton *)sender
{
    NSLog(@"btn_img_delivery_click:");
    
}
-(void)btn_on_search_bar_click:(UIButton *)sender
{
    NSLog(@"btn_on_search_bar_click:");
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
