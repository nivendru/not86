


//
//  SerchForFoodNowVC.m
//  Not86
//
//  Created by Admin on 30/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "SerchForFoodNowVC.h"
#import "Define.h"
#import "AppDelegate.h"
#import "JWSlideMenuViewController.h"
#import "JWSlideMenuController.h"
#import "JWNavigationController.h"
#import <MapKit/MapKit.h>


#import "LocationVC.h"
#import "ItemDetailVC.h"
#import "AddToCartFoodNowVC.h"
#import "AddToCartFoodLaterVC.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"

@interface SerchForFoodNowVC ()<UITextFieldDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,MKMapViewDelegate,CLLocationManagerDelegate>
{
    UIImageView *img_header;
    CGFloat	animatedDistance;
    UILabel *lbl_food_now;
    UITableView*table_for_itemsfilter;
    
    
    UIView*alertviewBg;
    
    UITableView *  table_on_drop_down ;
    int ABC;
    NSMutableArray * array_temp;
    
    
    UIScrollView *scroll;
    UIView *view_serch_for_food_now;
    
    UIButton * btn_on_single_dish;
    UIButton * btn_on_meal;
    UIImageView *img_red_tik1;
    UIImageView *img_red_tik2;
    UIButton * btn_on_dine_in;
    UIImageView *  img_red_tik_on_dine_in;
    
    
    UIImageView *img_bg_for_first_tbl;
    UILabel * lbl_food_now_r_later ;
    
    UILabel *lbl_distance_in_later;
    
    UIButton *  btn_on_take_out;
    UIImageView * img_red_tik_on_take_out;
    UIButton *  btn_on_delever;
    UIImageView * img_red_tik_on_deliver;
    int DEF;
    NSMutableArray * array_temp2;
    
    
    
    UIView * view_for_more_filters;
    UITableView * table_for_items;
    NSMutableArray *array_displaynames;
    NSMutableArray*ary_displaynames;
    NSMutableArray * array_img;
    NSMutableArray * array_head_names;
    
    UIView * view_for_food_items;
    
    
    UIImageView *img_strip1;
    
    UIView * view_for_my_favorites;
    
    UICollectionViewFlowLayout *  layout;
    UICollectionView * collView_for_cuisines;
    
    NSMutableArray * array_cuisine_imges;
    NSMutableArray * array_cuisines_names;
    NSMutableArray * array_cuisines_blue_imges;
    NSMutableArray * array_cuisines_blue_name;
    
    
    UIImageView *img_strip2;
    
    UIView * view_for_all_in_food_now;
    UICollectionViewFlowLayout *  layout2;
    UICollectionView * collView_for_cuisines_in_all_food_now;
    
    //collection view for course in food now
    
    UICollectionViewFlowLayout *  layout3;
    UICollectionView * collView_for_course_in_food_now;
    NSMutableArray * array_course_images;
    NSMutableArray * array_course_names;
    
    //collection view for Dietary restrictions in food now
    UICollectionViewFlowLayout *  layout4;
    UICollectionView * collView_for_dietary_in_food_now;
    NSMutableArray * array_dietary_images;
    NSMutableArray * array_dietary_nemes;
    
    
    
    UILabel *lbl_distance;
    
    
    NSMutableArray * array_names_in_dietary_table;
    
    UITableView * table_for_dietary;
    UIImageView * img_bg_for_distance;
    UILabel * lbl_for_distance_cell;
    
    UIToolbar *keyboardToolbar_time;
    
    UIDatePicker*datePickertime;
    NSDateFormatter *formattertime;
    
    //for All
    // UICollectionViewFlowLayout *layout4;
    UICollectionView * collView_dish_types2;
    UICollectionViewFlowLayout * layout5;
    UICollectionView * collView_for_course2;
    UICollectionViewFlowLayout * layout6;
    UICollectionView * collView_for_dietary2;
    
    NSMutableArray *array_imges_in_all;
    NSMutableArray *array_dish_names_in_all;
    NSMutableArray * array_course_images_in_all;
    NSMutableArray * array_dietary_images_in_all;
    
    
    //  UITableView *  table_for_dietary_in_all;
    UITableView * table_for_distance;
    NSMutableArray * array_names_in_distance;
    
    //============================================================ food later =====================================
    UIView *  view_serch_for_food_later;
    
    UITextField * txt_date;
    
    UIToolbar * keyboardToolbar_Date;
    NSDateFormatter *formatter;
    UIDatePicker *datePicker;
    
    UIImageView  *img_red_tik3;
    UIButton * btn_on_meal2;
    UIImageView *  img_red_tik4;
    
    UIButton * btn_on_dine_in_later;
    UIImageView *  img_red_tik_on_dine_in2;
    UIButton * btn_on_take_out2;
    
    UIView   * view_for_more_filters_in_later;
    UITableView * table_for_items_in_later;
    UIView *view_for_cuisines_in_later;
    
    UIImageView * img_strip3;
    UIView * view_for_my_favorites_in_later;
    UICollectionView * collView_for_cuisines_in_later;
    UIImageView * img_strip4;
    UIView * view_for_all_in_food_later;
    
    UICollectionView * collView_for_cuisines_in_all_food_later;
    
    UICollectionView * collView_for_course_in_food_later;
    UICollectionView * collView_for_dietary_in_food_later;
    
    UITableView * table_for_distance_in_later;
    UILabel * lbl_distance_in_food_now;
    UILabel * lbl_distance_in_food_later;
    
    int AAA;
    NSMutableArray * array_temp_in_later;
    UIButton * btn_on_single_dish_in_later;
    UIButton * btn_on_meal_in_later;
    
    UIButton*btn_foodlater_dinein_serve;
    UIButton*btn_foodlater_takeout;
    UIButton*btn_foodlater_delivary;
    UIImageView*img_red_tik_on_dine_in_foodlater;
    UIImageView*img_red_tik_on_takeout_foodlater;
    UIImageView*img_red_tik_on_delivary_foodlater;
    
    int foodlater;
    NSMutableArray*ary_foodlater_serveinglater;
    
    UITextField *txt_search;
    UITextField * txt_time;
    
    UITextField *txt_search2 ;
    
    UITableView * table_for_itemsfilterlater;
    
    //=========================================================== functionalyti declaretions ====================================
    
    
    AppDelegate *delegate;
    NSMutableArray * array_for_food;
    NSString * str_page_number;
    NSString*str_DOBfinal;
    
    
    
    NSMutableArray * Arr_temp;
    
    NSMutableArray * array_dietary_restryctionlist;
    
    NSMutableArray * array_cuisinelist_infoodnow;
    NSMutableArray * Arr_temp_favoritecuisin_infoodnow;
    
    NSMutableArray * array_course_infoodnow;
    NSMutableArray *  Arr_temp_course_infoodnow;
    
    NSMutableArray * array_all_cuisinelist_infoodnow;
    NSMutableArray * array_temp_all_cuisine_infoodnow;
    
    
    NSMutableArray * array_favorate_cuisinelist_infoodlater;
    NSMutableArray * array_temp_favorate_cuisines_in_foodlater;
    
    NSMutableArray  * array_all_cuisinelist_infoodlater;
    NSMutableArray  * array_temp_all_cuisine_in_foodlater;
    
    NSMutableArray  * array_course_in_foodlater;
    NSMutableArray * array_temp_course_in_food_later;
    
    NSMutableArray * array_dietary_restryctionlist_infoodlater;
    NSMutableArray * array_temp_dietary_restryction_infoodlater;
    
    NSString*str_type;
    
    NSMutableArray*ary_foodnowlist;
    
    NSString*str_foodtype;
    
    
    
    
    int selectedindex;
    NSIndexPath *indexSelected;
    
    //location
    CLLocation *myLocation;
    CLGeocoder *geocoder;
    UILabel *lbl_CurrentLoc;
    
    CLLocationManager *locationManager;
    CLGeocoder *myGeoCoder;
    BOOL bool_LocationSelect;
    NSString *str_Latitude;
    NSString *str_Longitude;
    CLPlacemark *place;
    NSString *str_street;
    

    NSString*str_servenowdishtype;
    NSString*str_sortby;
    NSString*str_servelaterdishtype;
    NSString*str_sortbylater;
    
    
    //paging
    
    int lastSeen_index;
    float PagenoInt;
    float pagelimit;
     NSMutableArray *arr_PageArray;

    
    
}
@end

@implementation SerchForFoodNowVC

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    arr_PageArray=[[NSMutableArray alloc]init];
    lastSeen_index=0;
    PagenoInt=0;
    pagelimit=0;
    
    array_temp = [[NSMutableArray alloc]init];
    array_temp2 = [[NSMutableArray alloc]init];
    array_temp_in_later= [[NSMutableArray alloc]init];
    ary_foodlater_serveinglater= [[NSMutableArray alloc]init];
    str_type = [NSString new];
    str_type = @"Favorite";
    str_foodtype = [NSString new];
    str_foodtype= @"0";
    str_servenowdishtype = [NSMutableString new];
    str_servenowdishtype = @"0";
    str_servelaterdishtype =[NSMutableString new];
    str_servelaterdishtype = @"0";
    str_sortbylater = [NSString new];
    str_sortbylater = @"5";

    str_sortby = [NSString new];
    str_sortby = @"5";
    
    Arr_temp  = [NSMutableArray new];
    ary_foodnowlist = [NSMutableArray new];
    
    array_dietary_restryctionlist = [[NSMutableArray alloc]init];
    
    array_cuisinelist_infoodnow = [[NSMutableArray alloc]init];
    Arr_temp_favoritecuisin_infoodnow = [[NSMutableArray alloc]init];
    
    array_course_infoodnow = [[NSMutableArray alloc]init];
    Arr_temp_course_infoodnow = [[NSMutableArray alloc]init];
    
    array_all_cuisinelist_infoodnow = [[NSMutableArray alloc]init];
    array_temp_all_cuisine_infoodnow = [[NSMutableArray alloc]init];
    
    array_favorate_cuisinelist_infoodlater = [[NSMutableArray alloc]init];
    array_temp_favorate_cuisines_in_foodlater = [[NSMutableArray alloc]init];
    
    
    array_all_cuisinelist_infoodlater = [[NSMutableArray alloc]init];
    array_temp_all_cuisine_in_foodlater = [[NSMutableArray alloc]init];
    
    array_course_in_foodlater = [[NSMutableArray alloc]init];
    array_temp_course_in_food_later = [[NSMutableArray alloc]init];
    
    
    array_dietary_restryctionlist_infoodlater = [[NSMutableArray alloc]init];
    array_temp_dietary_restryction_infoodlater = [[NSMutableArray alloc]init];
    
    
    str_DOBfinal = [NSString new];
    
    selectedindex=-1;
    indexSelected = nil;
    
    // Do any additional setup after loading the view.
    array_head_names = [[NSMutableArray alloc]initWithObjects:@"Food Now",@"Food Later" ,nil];
//    ary_displaynames =[[NSMutableArray alloc]initWithObjects:@"Steamed Thai oath",@"Rasberry custored", nil];
//    array_img = [[NSMutableArray alloc]initWithObjects:@"img-dish1@2x.png",@"img-dish2@2x.png", nil];
//   
//    
//    // MY FAVORITE CUISNISE
//    
//    array_cuisine_imges = [[NSMutableArray alloc]initWithObjects:@"b-chines@2x.png",@"b-french@2x.png",@"b-japanes@2x.png",@"b-italian@2x.png", nil];
//    array_cuisines_names = [[NSMutableArray alloc]initWithObjects:@"   Chinese",@"     French",@"   Japanese",@"     Italian", nil];
//    
//    array_cuisines_blue_imges = [[NSMutableArray alloc]initWithObjects:@"bl-chines@2x.png",@"bl-french@2x.png",@"bl-japanes@2x.png",@"bl-italian@2x.png", nil];
//    
//    // array declaration for COURESE
//    
//    array_course_images = [[NSMutableArray alloc]initWithObjects:@"b-breakfast@2x.png",@"main-couse-img@2x.png",@"b-dessert@2x.png",nil];
//    
//    array_course_names = [[NSMutableArray alloc]initWithObjects:@"  Brackfast",@"Main Course",@"     Dessert",nil];
//    
//    // array declaration for dietary restrictions
//    
//    array_dietary_images = [[NSMutableArray alloc]initWithObjects:@"halal-img@2x.png",@"koser-img@2x.png",@"non-veg-img@2x.png",@"veg-img@2x.png", nil];
//    array_dietary_nemes = [[NSMutableArray alloc]initWithObjects:@"Halal",@"Kosher",@"Non-Veg",@"Veg",nil];
//    
//    //names in table view in favorites
//    
//    array_names_in_distance = [[NSMutableArray alloc]initWithObjects:@"Rating (High-Low)",@"Price (Low-High)",@"Price (High-Low)", nil];
//    
//    
//    //array declaration in all
//    
//    array_imges_in_all = [[NSMutableArray alloc]initWithObjects:@"b-chines@2x.png",@"b-french@2x.png",@"b-japanes@2x.png",@"b-italian@2x.png", nil];
//    array_dish_names_in_all = [[NSMutableArray alloc]initWithObjects:@"Chinese",@"French",@"Japanese",@"Italian", nil];
//    
//    array_course_images_in_all = [[NSMutableArray alloc]initWithObjects:@"b-breakfast@2x.png",@"main-couse-img@2x.png",@"dessert-b@2x.png",nil];
//    
//    array_dietary_images_in_all = [[NSMutableArray alloc]initWithObjects:@"halal-img@2x.png",@"koser-img@2x.png",@"non-veg-img@2x.png",@"veg-img@2x.png", nil];
//    
    //names in table view in all
    array_names_in_distance = [[NSMutableArray alloc]initWithObjects:@"Rating (High-Low)",@"Price (Low-High)",@"Price (High-Low)", nil];
    
    
    //=================================functionaliti part
    
    str_page_number =  [NSMutableString new];
    
    
    
    
    
    
    [self integrateBodyDesign];
    [self integrateHeader];
    
    
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    [self.navigationController.slideMenuController UnhideHomeButton];
    
   // [self AFFoodNow];
    
    
    
    
    


}
-(void)viewDidAppear:(BOOL)animated
{
    [delegate fetchUsersCurrentLocation];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER)
    {
        [locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    [locationManager startUpdatingLocation];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    [locationManager startUpdatingLocation];
    
    myLocation = [[CLLocation alloc]init];
    myLocation = [locationManager location];
    
    CLLocationCoordinate2D coordinate;
    coordinate.longitude = locationManager.location.coordinate.longitude;
    coordinate.latitude = locationManager.location.coordinate.latitude;
    
    str_Longitude =[NSString stringWithFormat:@"%f",coordinate.longitude];
    str_Latitude =[NSString stringWithFormat:@"%f",coordinate.latitude];
    
    NSLog(@"str_lat:%@ str_lon:%@",str_Latitude,str_Longitude);

    
    
    [self AFfoodnowNowlist];
    [self AFDietary_restrictions_in_foodnow];
    // [self AFfavoriteCuisine_in_foodnow];
    [self AFCourse_in_foodnow];
    [self AFAll_Cuisine_in_foodnow];
    //[self AFFavorate_Cuisine_in_foodlater];
    [self AFAll_Cuisine_in_foodlater];
    [self AFCourse_in_foodlater];
    [self AFDietary_restrictions_in_foodlater];
    
    array_favorate_cuisinelist_infoodlater = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Food_Info"] valueForKey:@"FavoriteCuisinesArray"];
    array_cuisinelist_infoodnow= [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Food_Info"] valueForKey:@"FavoriteCuisinesArray"];
    
    [collView_for_cuisines reloadData];
    [collView_for_cuisines reloadData];

}

-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH,45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu.frame = CGRectMake(10,13,20,20);
    icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
    [icon_menu addTarget:self action:@selector(btn_menu_click:) forControlEvents:UIControlEventTouchUpInside];
    // [self.view   addSubview:icon_menu];
    
    
    lbl_food_now = [[UILabel alloc]init];
    lbl_food_now.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 150, 45);
    lbl_food_now.text = @"Food Now";
    lbl_food_now.font = [UIFont fontWithName:kFont size:20];
    lbl_food_now.textColor = [UIColor whiteColor];
    lbl_food_now.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_food_now];
    
    UIButton *icon_drop_down = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_drop_down.frame = CGRectMake(CGRectGetMaxX(lbl_food_now.frame)-45,15,20,20);
    icon_drop_down .backgroundColor = [UIColor clearColor];
    [icon_drop_down addTarget:self action:@selector(btn_drop_down:) forControlEvents:UIControlEventTouchUpInside];
    [icon_drop_down setImage:[UIImage imageNamed:@"white-dropd-@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_drop_down];
    
    UIButton *btn_on_food_later_in_head = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_food_later_in_head.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0,150,45);
    btn_on_food_later_in_head .backgroundColor = [UIColor clearColor];
    [btn_on_food_later_in_head addTarget:self action:@selector(btn_on_food_later_label_click:) forControlEvents:UIControlEventTouchUpInside];
    //[icon_drop_down setImage:[UIImage imageNamed:@"drop-down@2x.png"] forState:UIControlStateNormal];
    [self.view   addSubview:btn_on_food_later_in_head];
    
#pragma mark Tableview
    
    table_on_drop_down = [[UITableView alloc] init ];
    table_on_drop_down .frame  = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,45,150,100);
    [ table_on_drop_down  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_on_drop_down .delegate = self;
    table_on_drop_down .dataSource = self;
    table_on_drop_down .showsVerticalScrollIndicator = NO;
    table_on_drop_down .backgroundColor = [UIColor clearColor];
    table_on_drop_down.hidden = YES;
    [self.view addSubview: table_on_drop_down ];
    
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 11, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}


-(void)integrateBodyDesign
{
    
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.bounces = NO;
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor clearColor];
    scroll.frame = CGRectMake(0, 46, WIDTH, HEIGHT);
    scroll.scrollEnabled = YES;
    //    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    
    view_serch_for_food_now = [[UIView alloc]init];
    view_serch_for_food_now.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+380);
    [view_serch_for_food_now setUserInteractionEnabled:YES];
    view_serch_for_food_now.backgroundColor=[UIColor clearColor];
    [scroll  addSubview:  view_serch_for_food_now];
    
    
    
    UILabel *lbl_location = [[UILabel alloc]init];
    lbl_location.frame = CGRectMake(145,5, 150, 45);
    lbl_location.text = @"Location";
    lbl_location.font = [UIFont fontWithName:kFont size:18];
    lbl_location.textColor = [UIColor blackColor];
    lbl_location.backgroundColor = [UIColor clearColor];
    [ view_serch_for_food_now addSubview:lbl_location];
    
    
    UIImageView *bg_for_location = [[UIImageView alloc]init];
    bg_for_location.frame = CGRectMake(-5, CGRectGetMaxY(lbl_location.frame)-5, WIDTH+20, 50);
    [bg_for_location setUserInteractionEnabled:YES];
    bg_for_location.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [ view_serch_for_food_now addSubview:bg_for_location];
    
    UIImageView *img_pointer_location = [[UIImageView alloc]init];
    img_pointer_location.frame = CGRectMake(25,7, 30, 30);
    [img_pointer_location setImage:[UIImage imageNamed:@"pointer-location@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_pointer_location setUserInteractionEnabled:YES];
    [bg_for_location addSubview:img_pointer_location];
    
    UILabel *lbl_set_location = [[UILabel alloc]init];
    lbl_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+20,9, 250, 30);
    
    if (delegate.str_Location1.length>0)
    {
        lbl_set_location.text = delegate.str_Location1;

    }
    lbl_set_location.font = [UIFont fontWithName:kFont size:18];
    lbl_set_location.textColor = [UIColor blackColor];
    lbl_set_location.backgroundColor = [UIColor clearColor];
    [bg_for_location addSubview:lbl_set_location];
    
    
    UIButton *btn_img_right = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_right.frame = CGRectMake(CGRectGetMidX(lbl_set_location.frame)+140,10,30,30);    btn_img_right .backgroundColor = [UIColor clearColor];
    [btn_img_right addTarget:self action:@selector(btn_right_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_right setImage:[UIImage imageNamed:@"right-arrow@2x.png"] forState:UIControlStateNormal];
    [bg_for_location   addSubview:btn_img_right];
    
    
    UIButton *btn_on_set_location = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_set_location.frame = CGRectMake(0,0,WIDTH+5,50);
    btn_on_set_location .backgroundColor = [UIColor clearColor];
    [btn_on_set_location addTarget:self action:@selector(btn_on_set_location_click:) forControlEvents:UIControlEventTouchUpInside];
    // [btn_set_date setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    [bg_for_location   addSubview:btn_on_set_location];
    
    
    UILabel *lbl_dish_r_meal = [[UILabel alloc]init];
    lbl_dish_r_meal.frame = CGRectMake(140,CGRectGetMaxY( bg_for_location.frame)+10, 150, 45);
    lbl_dish_r_meal.text = @"Dish/Meal";
    lbl_dish_r_meal.font = [UIFont fontWithName:kFont size:18];
    lbl_dish_r_meal.textColor = [UIColor blackColor];
    lbl_dish_r_meal.backgroundColor = [UIColor clearColor];
    [ view_serch_for_food_now addSubview:lbl_dish_r_meal];
    
    UIImageView *bg_for_meal = [[UIImageView alloc]init];
    bg_for_meal.frame = CGRectMake(-5,CGRectGetMaxY(lbl_dish_r_meal.frame)-5, WIDTH+20, 55);
    [bg_for_meal setUserInteractionEnabled:YES];
    bg_for_meal.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [ view_serch_for_food_now addSubview:bg_for_meal];
    
    
    btn_on_single_dish  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_single_dish.frame = CGRectMake(100,5,60,50);
    [btn_on_single_dish setImage:[UIImage imageNamed:@"img-single-bl@2x.png"] forState:UIControlStateSelected];
    [btn_on_single_dish setImage:[UIImage imageNamed:@"single-dish-icon@2x.png"] forState:UIControlStateNormal];
    btn_on_single_dish.tag = 54;
    [btn_on_single_dish addTarget:self action:@selector(btn_on_dish_click:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_meal addSubview:  btn_on_single_dish];
    
    
    btn_on_meal  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_meal.frame = CGRectMake(CGRectGetMaxX( btn_on_single_dish.frame)+50,3,50,50);
    [btn_on_meal setImage:[UIImage imageNamed:@"iocn-meal-bl@2x.png"] forState:UIControlStateSelected];
    [btn_on_meal setImage:[UIImage imageNamed:@"iocn-meal@2x.png"] forState:UIControlStateNormal];
    btn_on_meal.tag = 55;
    [btn_on_meal addTarget:self action:@selector(btn_on_dish_click:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_meal addSubview:  btn_on_meal];
    
    
    img_red_tik1 = [[UIImageView alloc]init];
    img_red_tik1.frame =  CGRectMake(CGRectGetMidX( btn_on_single_dish.frame)+18,40,20,15);
    [img_red_tik1 setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik1 setUserInteractionEnabled:YES];
    [bg_for_meal addSubview:img_red_tik1];
    img_red_tik1.hidden = YES;
    
    img_red_tik2 = [[UIImageView alloc]init];
    img_red_tik2.frame =  CGRectMake(CGRectGetMidX(btn_on_meal.frame)+18,40,20,15);
    [img_red_tik2 setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik2 setUserInteractionEnabled:YES];
    [bg_for_meal addSubview:img_red_tik2];
    img_red_tik2.hidden = YES;
    
    UILabel *lbl_seving_type = [[UILabel alloc]init];
    lbl_seving_type.frame = CGRectMake(135,CGRectGetMaxY(bg_for_meal.frame)+5, 150, 45);
    lbl_seving_type.text = @"Serving Type";
    lbl_seving_type.font = [UIFont fontWithName:kFont size:18];
    lbl_seving_type.textColor = [UIColor blackColor];
    lbl_seving_type.backgroundColor = [UIColor clearColor];
    [ view_serch_for_food_now addSubview:lbl_seving_type];
    
    UIImageView *bg_for_serving_type = [[UIImageView alloc]init];
    bg_for_serving_type.frame = CGRectMake(-5,CGRectGetMaxY(lbl_seving_type.frame)-5, WIDTH+20, 70);
    [bg_for_serving_type setUserInteractionEnabled:YES];
    bg_for_serving_type.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [ view_serch_for_food_now addSubview:bg_for_serving_type];
    
    btn_on_dine_in  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_dine_in.frame = CGRectMake(45,10,50,50);
    [ btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in-bl@2x.png"] forState:UIControlStateSelected];
    [ btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in@2x.png"] forState:UIControlStateNormal];
    btn_on_dine_in.tag = 0;
    [ btn_on_dine_in addTarget:self action:@selector(click_on_serving_type:)forControlEvents:UIControlEventTouchUpInside];
    [bg_for_serving_type addSubview:   btn_on_dine_in];
    
    img_red_tik_on_dine_in = [[UIImageView alloc]init];
    img_red_tik_on_dine_in.frame = CGRectMake(CGRectGetMidX(btn_on_dine_in.frame)+5,35,25,20);  [img_red_tik_on_dine_in setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik_on_dine_in setUserInteractionEnabled:YES];
    [bg_for_serving_type addSubview:img_red_tik_on_dine_in];
    img_red_tik_on_dine_in.hidden = YES;
    
    
    btn_on_take_out  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_take_out.frame = CGRectMake(CGRectGetMaxX(btn_on_dine_in.frame)+70,10,50,50);
    [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out-bl@2x.png"]forState:UIControlStateSelected];
    [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out@2x.png"]forState:UIControlStateNormal];
    btn_on_take_out.tag = 1;
    [btn_on_take_out addTarget:self action:@selector(click_on_serving_type:)forControlEvents:UIControlEventTouchUpInside];
    [bg_for_serving_type addSubview:btn_on_take_out];
    
    img_red_tik_on_take_out = [[UIImageView alloc]init];
    img_red_tik_on_take_out.frame = CGRectMake(CGRectGetMidX(btn_on_take_out.frame)+5,35,25,20);
    [img_red_tik_on_take_out setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik_on_take_out setUserInteractionEnabled:YES];
    [bg_for_serving_type addSubview:img_red_tik_on_take_out];
    img_red_tik_on_take_out.hidden = YES;
    
    
    btn_on_delever  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_delever.frame = CGRectMake(CGRectGetMaxX( btn_on_take_out.frame)+70,10,50,50);
    [btn_on_delever setImage:[UIImage imageNamed:@"img-delever-bl@2x.png"] forState:UIControlStateSelected];
    [btn_on_delever setImage:[UIImage imageNamed:@"img-delever@2x.png"] forState:UIControlStateNormal];
    btn_on_delever.tag = 2;
    [btn_on_delever addTarget:self action:@selector(click_on_serving_type:)forControlEvents:UIControlEventTouchUpInside];
    [bg_for_serving_type addSubview:btn_on_delever];
    
    img_red_tik_on_deliver = [[UIImageView alloc]init];
    img_red_tik_on_deliver.frame = CGRectMake(CGRectGetMidX(btn_on_delever.frame)+5,35,25,20);
    [img_red_tik_on_deliver setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik_on_deliver setUserInteractionEnabled:YES];
    [bg_for_serving_type addSubview:img_red_tik_on_deliver];
    img_red_tik_on_deliver.hidden = YES;
    
    UILabel *lbl_keywords = [[UILabel alloc]init];
    lbl_keywords.frame = CGRectMake(155,CGRectGetMaxY(bg_for_serving_type.frame)+5, 150, 45);
    lbl_keywords.text = @"Keywords";
    lbl_keywords.font = [UIFont fontWithName:kFont size:18];
    lbl_keywords.textColor = [UIColor blackColor];
    lbl_keywords.backgroundColor = [UIColor clearColor];
    [ view_serch_for_food_now addSubview:lbl_keywords];
    
    UIImageView *bg_for_Keyword = [[UIImageView alloc]init];
    bg_for_Keyword.frame = CGRectMake(-5,CGRectGetMaxY(lbl_keywords.frame)-5, WIDTH+20, 50);
    [bg_for_Keyword setUserInteractionEnabled:YES];
    bg_for_Keyword.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [ view_serch_for_food_now addSubview:bg_for_Keyword];
    
    
    UIImageView *img_serch_bar = [[UIImageView alloc]init];
    img_serch_bar.frame = CGRectMake(15,10, WIDTH-20, 30);
    [img_serch_bar setImage:[UIImage imageNamed:@"search-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_serch_bar setUserInteractionEnabled:YES];
    [bg_for_Keyword addSubview:img_serch_bar];
    
    
    txt_search = [[UITextField alloc] init];
    txt_search.frame = CGRectMake(0,-8, 300, 45);
    txt_search .borderStyle = UITextBorderStyleNone;
    txt_search .textColor = [UIColor grayColor];
    txt_search .font = [UIFont fontWithName:kFont size:15];
    txt_search .placeholder = @"Search...";
    [txt_search  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_search  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_search .leftView = padding3;
    txt_search .leftViewMode = UITextFieldViewModeAlways;
    txt_search .userInteractionEnabled=YES;
    txt_search .textAlignment = NSTextAlignmentLeft;
    txt_search .backgroundColor = [UIColor clearColor];
    txt_search .keyboardType = UIKeyboardTypeAlphabet;
    txt_search .delegate = self;
    [img_serch_bar addSubview:txt_search ];
    
    
    UIImageView *icon_search = [[UIImageView alloc]init];
    icon_search.frame = CGRectMake(330,8, 15, 15);
    [icon_search setImage:[UIImage imageNamed:@"icon-search@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_search setUserInteractionEnabled:YES];
    [img_serch_bar addSubview:icon_search];
    
    UIButton *btn_on_search_bar = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_search_bar.frame = CGRectMake(330,0, 30, 30);
    btn_on_search_bar .userInteractionEnabled=YES;
    btn_on_search_bar .backgroundColor = [UIColor clearColor];
    [btn_on_search_bar addTarget:self action:@selector(btn_on_search_bar_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [img_serch_bar   addSubview:btn_on_search_bar];
    
    
    //view for btn more filters ans  dish table view
    
    view_for_more_filters = [[UIView alloc]init];
    view_for_more_filters.frame=CGRectMake(0,CGRectGetMaxY(bg_for_Keyword.frame)+20,WIDTH,380);
    [view_for_more_filters setUserInteractionEnabled:YES];
    view_for_more_filters.backgroundColor=[UIColor clearColor];
    view_for_more_filters.hidden = NO;
    [ view_serch_for_food_now  addSubview:  view_for_more_filters];
    
    
    UIButton *btn_img_more_filters = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_more_filters.frame = CGRectMake(40,10, WIDTH-80, 45);
    btn_img_more_filters .backgroundColor = [UIColor clearColor];
    [btn_img_more_filters addTarget:self action:@selector(btn_img_on_more_filters_click:) forControlEvents:UIControlEventTouchUpInside];
    [ btn_img_more_filters setImage:[UIImage imageNamed:@"img-more-filters@2x.png"] forState:UIControlStateNormal];
    [view_for_more_filters   addSubview:btn_img_more_filters];
    
    
#pragma mark Tableview
    
    table_for_items = [[UITableView alloc] init ];
    table_for_items.frame  = CGRectMake(5,CGRectGetMaxY(btn_img_more_filters.frame)+5,WIDTH-10,300);
    [table_for_items setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_items.delegate = self;
    table_for_items.dataSource = self;
    table_for_items.showsVerticalScrollIndicator = NO;
    table_for_items.backgroundColor = [UIColor clearColor];
    [view_for_more_filters addSubview:table_for_items];
    
    view_for_food_items = [[UIView alloc]init];
    view_for_food_items.frame=CGRectMake(0,CGRectGetMaxY(bg_for_Keyword.frame),WIDTH,800);
    [view_for_food_items setUserInteractionEnabled:YES];
    view_for_food_items.backgroundColor=[UIColor clearColor];
    view_for_food_items.hidden = YES;
    [ view_serch_for_food_now  addSubview:  view_for_food_items];
    
    
    
    UILabel *lbl_cuisines = [[UILabel alloc]init];
    lbl_cuisines.frame = CGRectMake(130,5, 150, 45);
    lbl_cuisines.text = @"Cuisines";
    lbl_cuisines.font = [UIFont fontWithName:kFont size:18];
    lbl_cuisines.textColor = [UIColor blackColor];
    lbl_cuisines.backgroundColor = [UIColor clearColor];
    [view_for_food_items addSubview:lbl_cuisines];
    
    UIImageView *bg_for_favorits_and_all = [[UIImageView alloc]init];
    bg_for_favorits_and_all.frame = CGRectMake(0,CGRectGetMaxY(lbl_cuisines.frame), WIDTH, 50);
    [bg_for_favorits_and_all setUserInteractionEnabled:YES];
    bg_for_favorits_and_all.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_food_items addSubview:bg_for_favorits_and_all];
    
    
    UILabel *lbl_my_favorites = [[UILabel alloc]init];
    lbl_my_favorites.frame = CGRectMake(30,03, 150, 45);
    lbl_my_favorites.text = @"My Favorites";
    [lbl_my_favorites setUserInteractionEnabled:YES];
    lbl_my_favorites.font = [UIFont fontWithName:kFontBold size:18];
    lbl_my_favorites.textColor = [UIColor blackColor];
    lbl_my_favorites.backgroundColor = [UIColor clearColor];
    [ bg_for_favorits_and_all addSubview:lbl_my_favorites];
    
    img_strip1 = [[UIImageView alloc]init];
    img_strip1.frame = CGRectMake(35, CGRectGetMaxY(lbl_my_favorites.frame)-5, 100, 3);
    [img_strip1 setImage:[UIImage imageNamed:@"black strip@2x.png"]];
    [img_strip1 setUserInteractionEnabled:YES];
    [bg_for_favorits_and_all addSubview:img_strip1];
    
    UIButton *btn_my_favorites = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_my_favorites.frame = CGRectMake(0,03, 180, 45);
    btn_my_favorites .backgroundColor = [UIColor clearColor];
    [btn_my_favorites setUserInteractionEnabled:YES];
    [btn_my_favorites addTarget:self action:@selector(btn_on_my_favorites_click:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_favorits_and_all addSubview:btn_my_favorites];
    
    // view for MY FAVORITES in FOOD NOW
    
    view_for_my_favorites = [[UIView alloc]init];
    view_for_my_favorites.frame=CGRectMake(0,CGRectGetMaxY(lbl_my_favorites.frame)+92,WIDTH,50);
    view_for_my_favorites.backgroundColor=[UIColor yellowColor];
    [view_for_my_favorites setUserInteractionEnabled:YES];
    [view_for_food_items addSubview:  view_for_my_favorites];
    
    UIImageView *bg_for_cuisines = [[UIImageView alloc]init];
    bg_for_cuisines.frame = CGRectMake(0,02, WIDTH, 100);
    [bg_for_cuisines setUserInteractionEnabled:YES];
    bg_for_cuisines.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_my_favorites addSubview:bg_for_cuisines];
    
    // first collection view for cuisines
    
#pragma collection view for favorites in food now
    
    layout = [[UICollectionViewFlowLayout alloc] init];
    collView_for_cuisines = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,95)
                                               collectionViewLayout:layout];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_cuisines setDataSource:self];
    [collView_for_cuisines setDelegate:self];
    collView_for_cuisines.scrollEnabled = YES;
    collView_for_cuisines.showsVerticalScrollIndicator = NO;
    collView_for_cuisines.showsHorizontalScrollIndicator = NO;
    collView_for_cuisines.pagingEnabled = YES;
    [collView_for_cuisines registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_cuisines setBackgroundColor:[UIColor clearColor]];
    layout.minimumInteritemSpacing = 12;
    layout.minimumLineSpacing = 0;
    collView_for_cuisines.userInteractionEnabled = YES;
    [bg_for_cuisines  addSubview: collView_for_cuisines];
    
    UIImageView *img_left_arrow1_food_now = [[UIImageView alloc]init];
    img_left_arrow1_food_now.frame = CGRectMake(5,40,10, 15);
    [img_left_arrow1_food_now setUserInteractionEnabled:YES];
    img_left_arrow1_food_now.image=[UIImage imageNamed:@"left-arrow-img@2x.png"];
    [bg_for_cuisines addSubview:img_left_arrow1_food_now];
    
    UIButton *btn_on_lefet_arrow1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_lefet_arrow1.frame = CGRectMake(0,0, 20,100);
    btn_on_lefet_arrow1 .backgroundColor = [UIColor clearColor];
    [btn_on_lefet_arrow1 setUserInteractionEnabled:YES];
    [btn_on_lefet_arrow1 addTarget:self action:@selector(click_on_left_arrow1_btn:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_cuisines addSubview:btn_on_lefet_arrow1];
    
    
    UIImageView *img_right_arrow1_in_food_now = [[UIImageView alloc]init];
    img_right_arrow1_in_food_now.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame)+3,40,10, 15);
    [img_right_arrow1_in_food_now setUserInteractionEnabled:YES];
    img_right_arrow1_in_food_now.image=[UIImage imageNamed:@"right-arrow@2x.png"];
    [bg_for_cuisines addSubview:img_right_arrow1_in_food_now];
    
    UIButton *btn_on_right_arrow1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_right_arrow1.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,100);
    btn_on_right_arrow1 .backgroundColor = [UIColor clearColor];
    [btn_on_right_arrow1 setUserInteractionEnabled:YES];
    [btn_on_right_arrow1 addTarget:self action:@selector(click_on_right_arrow1_btn:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_cuisines addSubview:btn_on_right_arrow1];
    
    
    
    UILabel *lbl_all = [[UILabel alloc]init];
    lbl_all.frame = CGRectMake(250,3, 150, 45);
    lbl_all.text = @"All";
    [lbl_all setUserInteractionEnabled:YES];
    lbl_all.font = [UIFont fontWithName:kFontBold size:18];
    lbl_all.textColor = [UIColor blackColor];
    lbl_all.backgroundColor = [UIColor clearColor];
    [bg_for_favorits_and_all addSubview:lbl_all];
    
    
    img_strip2 = [[UIImageView alloc]init];
    img_strip2.frame = CGRectMake(250, CGRectGetMaxY(lbl_my_favorites.frame)-5, 57, 3);
    [img_strip2 setImage:[UIImage imageNamed:@"black strip@2x.png"]];
    [img_strip2 setUserInteractionEnabled:YES];
    [bg_for_favorits_and_all addSubview:img_strip2];
    
    img_strip2.hidden = YES;
    
    UIButton *btn_on_all = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_all.frame = CGRectMake(185,03, 180, 45);
    [btn_on_all setUserInteractionEnabled:YES];
    btn_on_all .backgroundColor = [UIColor clearColor];
    [btn_on_all addTarget:self action:@selector(btn_all_click:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_favorits_and_all  addSubview:btn_on_all];
    
    // view for ALL IN FOOD NOW
    
    view_for_all_in_food_now = [[UIView alloc]init];
    view_for_all_in_food_now.frame=CGRectMake(0,CGRectGetMaxY(bg_for_favorits_and_all.frame),WIDTH,100);
    view_for_all_in_food_now.backgroundColor=[UIColor yellowColor];
    [view_for_all_in_food_now setUserInteractionEnabled:YES];
    [view_for_food_items addSubview:  view_for_all_in_food_now];
    view_for_all_in_food_now.hidden = YES;
    
    
    UIImageView *bg_for_all_in_food_now = [[UIImageView alloc]init];
    bg_for_all_in_food_now.frame = CGRectMake(0,02, WIDTH, 100);
    [bg_for_all_in_food_now setUserInteractionEnabled:YES];
    bg_for_all_in_food_now.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_all_in_food_now addSubview:bg_for_all_in_food_now];
    
    
    layout2 = [[UICollectionViewFlowLayout alloc] init];
    collView_for_cuisines_in_all_food_now = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,95)
                                                               collectionViewLayout:layout2];
    [layout2 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_cuisines_in_all_food_now setDataSource:self];
    [collView_for_cuisines_in_all_food_now setDelegate:self];
    collView_for_cuisines_in_all_food_now.scrollEnabled = YES;
    collView_for_cuisines_in_all_food_now.showsVerticalScrollIndicator = NO;
    collView_for_cuisines_in_all_food_now.showsHorizontalScrollIndicator = NO;
    collView_for_cuisines_in_all_food_now.pagingEnabled = YES;
    [collView_for_cuisines_in_all_food_now registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_cuisines_in_all_food_now setBackgroundColor:[UIColor clearColor]];
    layout2.minimumInteritemSpacing = 12;
    layout2.minimumLineSpacing = 0;
    collView_for_cuisines_in_all_food_now.userInteractionEnabled = YES;
    [bg_for_all_in_food_now  addSubview: collView_for_cuisines_in_all_food_now];
    
    UIImageView *img_left_arrow2_food_now = [[UIImageView alloc]init];
    img_left_arrow2_food_now.frame = CGRectMake(5,40,10, 15);
    [img_left_arrow2_food_now setUserInteractionEnabled:YES];
    img_left_arrow2_food_now.image=[UIImage imageNamed:@"left-arrow-img@2x.png"];
    [bg_for_all_in_food_now addSubview:img_left_arrow2_food_now];
    
    UIButton *btn_on_lefet_arrow2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_lefet_arrow2.frame = CGRectMake(0,0, 20,100);
    btn_on_lefet_arrow2 .backgroundColor = [UIColor clearColor];
    [btn_on_lefet_arrow2 setUserInteractionEnabled:YES];
    [btn_on_lefet_arrow2 addTarget:self action:@selector(click_on_left_arrow2_btn:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_all_in_food_now addSubview:btn_on_lefet_arrow2];
    
    
    UIImageView *img_right_arrow2_in_food_now = [[UIImageView alloc]init];
    img_right_arrow2_in_food_now.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame)+3,40,10, 15);
    [img_right_arrow2_in_food_now setUserInteractionEnabled:YES];
    img_right_arrow2_in_food_now.image=[UIImage imageNamed:@"right-arrow@2x.png"];
    [bg_for_all_in_food_now addSubview:img_right_arrow2_in_food_now];
    
    UIButton *btn_on_right_arrow2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_right_arrow2.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,100);
    btn_on_right_arrow2 .backgroundColor = [UIColor clearColor];
    [btn_on_right_arrow2 setUserInteractionEnabled:YES];
    [btn_on_right_arrow2 addTarget:self action:@selector(click_on_right_arrow2_btn:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_all_in_food_now addSubview:btn_on_right_arrow2];
    
    
    
    
    UILabel *lbl_course_in_food_now = [[UILabel alloc]init];
    lbl_course_in_food_now.frame = CGRectMake(140,CGRectGetMaxY(view_for_my_favorites.frame), 150, 45);
    lbl_course_in_food_now.text = @"Course";
    [lbl_course_in_food_now setUserInteractionEnabled:YES];
    lbl_course_in_food_now.font = [UIFont fontWithName:kFont size:18];
    lbl_course_in_food_now.textColor = [UIColor blackColor];
    lbl_course_in_food_now.backgroundColor = [UIColor clearColor];
    [view_for_food_items addSubview:lbl_course_in_food_now];
    
    UIImageView *bg_for_course_in_food_now = [[UIImageView alloc]init];
    bg_for_course_in_food_now.frame = CGRectMake(0,CGRectGetMaxY(lbl_course_in_food_now.frame)-10, WIDTH, 65);
    [bg_for_course_in_food_now setUserInteractionEnabled:YES];
    bg_for_course_in_food_now.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_food_items addSubview:bg_for_course_in_food_now];
    
    layout3 = [[UICollectionViewFlowLayout alloc] init];
    collView_for_course_in_food_now = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,60)
                                                         collectionViewLayout:layout3];
    [layout3 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_course_in_food_now setDataSource:self];
    [collView_for_course_in_food_now setDelegate:self];
    collView_for_course_in_food_now.scrollEnabled = YES;
    collView_for_course_in_food_now.showsVerticalScrollIndicator = NO;
    collView_for_course_in_food_now.showsHorizontalScrollIndicator = NO;
    collView_for_course_in_food_now.pagingEnabled = YES;
    [collView_for_course_in_food_now registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_course_in_food_now setBackgroundColor:[UIColor clearColor]];
    layout3.minimumInteritemSpacing = 12;
    layout3.minimumLineSpacing = 0;
    collView_for_course_in_food_now.userInteractionEnabled = YES;
    [bg_for_course_in_food_now  addSubview: collView_for_course_in_food_now];
    
    
    UIImageView *img_left_arrow3_food_now = [[UIImageView alloc]init];
    img_left_arrow3_food_now.frame = CGRectMake(5,24,10, 15);
    [img_left_arrow3_food_now setUserInteractionEnabled:YES];
    img_left_arrow3_food_now.image=[UIImage imageNamed:@"left-arrow-img@2x.png"];
    [bg_for_course_in_food_now addSubview:img_left_arrow3_food_now];
    
    UIButton *btn_on_lefet_arrow3 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_lefet_arrow3.frame = CGRectMake(0,0, 20,65);
    btn_on_lefet_arrow3 .backgroundColor = [UIColor clearColor];
    [btn_on_lefet_arrow3 setUserInteractionEnabled:YES];
    [btn_on_lefet_arrow3 addTarget:self action:@selector(click_on_left_arrow3_btn:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_course_in_food_now addSubview:btn_on_lefet_arrow3];
    
    
    UIImageView *img_right_arrow3_in_food_now = [[UIImageView alloc]init];
    img_right_arrow3_in_food_now.frame = CGRectMake(CGRectGetMaxX(collView_for_course_in_food_now.frame)+3,24,10, 15);
    [img_right_arrow3_in_food_now setUserInteractionEnabled:YES];
    img_right_arrow3_in_food_now.image=[UIImage imageNamed:@"right-arrow@2x.png"];
    [bg_for_course_in_food_now addSubview:img_right_arrow3_in_food_now];
    
    UIButton *btn_on_right_arrow3 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_right_arrow3.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,65);
    btn_on_right_arrow3 .backgroundColor = [UIColor clearColor];
    [btn_on_right_arrow3 setUserInteractionEnabled:YES];
    [btn_on_right_arrow3 addTarget:self action:@selector(click_on_right_arrow3_btn:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_course_in_food_now addSubview:btn_on_right_arrow3];
    
    
    
    UILabel *lbl_Dietary_in_food_now = [[UILabel alloc]init];
    lbl_Dietary_in_food_now.frame = CGRectMake(110,CGRectGetMidY(bg_for_course_in_food_now.frame)+25,250,45);
    lbl_Dietary_in_food_now.text = @"Dietary Restrictions";
    [lbl_Dietary_in_food_now setUserInteractionEnabled:YES];
    lbl_Dietary_in_food_now.font = [UIFont fontWithName:kFont size:18];
    lbl_Dietary_in_food_now.textColor = [UIColor blackColor];
    lbl_Dietary_in_food_now.backgroundColor = [UIColor clearColor];
    [view_for_food_items addSubview:lbl_Dietary_in_food_now];
    
    UIImageView *bg_for_dietary_in_food_now = [[UIImageView alloc]init];
    bg_for_dietary_in_food_now.frame = CGRectMake(0,CGRectGetMaxY(lbl_Dietary_in_food_now.frame)-5, WIDTH, 85);
    [bg_for_dietary_in_food_now setUserInteractionEnabled:YES];
    bg_for_dietary_in_food_now.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_food_items addSubview:bg_for_dietary_in_food_now];
    
    layout4 = [[UICollectionViewFlowLayout alloc] init];
    collView_for_dietary_in_food_now = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,80)
                                                          collectionViewLayout:layout4];
    [layout4 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_dietary_in_food_now setDataSource:self];
    [collView_for_dietary_in_food_now setDelegate:self];
    collView_for_dietary_in_food_now.scrollEnabled = YES;
    collView_for_dietary_in_food_now.showsVerticalScrollIndicator = NO;
    collView_for_dietary_in_food_now.showsHorizontalScrollIndicator = NO;
    collView_for_dietary_in_food_now.pagingEnabled = YES;
    [collView_for_dietary_in_food_now registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_dietary_in_food_now setBackgroundColor:[UIColor clearColor]];
    layout4.minimumInteritemSpacing = 12;
    layout4.minimumLineSpacing = 0;
    collView_for_dietary_in_food_now.userInteractionEnabled = YES;
    [bg_for_dietary_in_food_now  addSubview: collView_for_dietary_in_food_now];
    
    UIImageView *img_left_arrow4_food_now = [[UIImageView alloc]init];
    img_left_arrow4_food_now.frame = CGRectMake(5,35,10, 15);
    [img_left_arrow4_food_now setUserInteractionEnabled:YES];
    img_left_arrow4_food_now.image=[UIImage imageNamed:@"left-arrow-img@2x.png"];
    [bg_for_dietary_in_food_now addSubview:img_left_arrow4_food_now];
    
    UIButton *btn_on_lefet_arrow4 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_lefet_arrow4.frame = CGRectMake(0,0, 20,85);
    btn_on_lefet_arrow4 .backgroundColor = [UIColor clearColor];
    [btn_on_lefet_arrow4 setUserInteractionEnabled:YES];
    [btn_on_lefet_arrow4 addTarget:self action:@selector(click_on_left_arrow4_btn:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_dietary_in_food_now addSubview:btn_on_lefet_arrow4];
    
    
    UIImageView *img_right_arrow4_in_food_now = [[UIImageView alloc]init];
    img_right_arrow4_in_food_now.frame = CGRectMake(CGRectGetMaxX(collView_for_course_in_food_now.frame)+3,35,10, 15);
    [img_right_arrow4_in_food_now setUserInteractionEnabled:YES];
    img_right_arrow4_in_food_now.image=[UIImage imageNamed:@"right-arrow@2x.png"];
    [bg_for_dietary_in_food_now addSubview:img_right_arrow4_in_food_now];
    
    UIButton *btn_on_right_arrow4 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_right_arrow4.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,85);
    btn_on_right_arrow4 .backgroundColor = [UIColor clearColor];
    [btn_on_right_arrow4 setUserInteractionEnabled:YES];
    [btn_on_right_arrow4 addTarget:self action:@selector(click_on_right_arrow4_btn:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_dietary_in_food_now addSubview:btn_on_right_arrow4];
    
    
    
    
    UILabel *lbl_sortby = [[UILabel alloc]init];
    lbl_sortby.frame = CGRectMake(150,CGRectGetMidY(bg_for_dietary_in_food_now.frame)+35,250,45);
    lbl_sortby.text = @"Sort By";
    [lbl_sortby setUserInteractionEnabled:YES];
    lbl_sortby.font = [UIFont fontWithName:kFont size:18];
    lbl_sortby.textColor = [UIColor blackColor];
    lbl_sortby.backgroundColor = [UIColor clearColor];
    [view_for_food_items addSubview:lbl_sortby];
    
    UIImageView *rect_for_distance = [[UIImageView alloc]init];
    rect_for_distance.frame = CGRectMake(40,CGRectGetMaxY(lbl_sortby.frame)-5, WIDTH-80, 25);
    [rect_for_distance setUserInteractionEnabled:YES];
    rect_for_distance.image=[UIImage imageNamed:@"dietary-table-img@2x.png"];
    [view_for_food_items addSubview:rect_for_distance];
    
    lbl_distance = [[UILabel alloc]init];
    lbl_distance.frame = CGRectMake(05,-10,250,45);
    lbl_distance.text = @"Distance";
    [lbl_distance setUserInteractionEnabled:YES];
    lbl_distance.font = [UIFont fontWithName:kFont size:15];
    lbl_distance.textColor = [UIColor blackColor];
    lbl_distance.backgroundColor = [UIColor clearColor];
    [rect_for_distance addSubview:lbl_distance];
    
    UIButton *btn_on_rect_distance = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_rect_distance.frame = CGRectMake(0,0, 295, 30);
    btn_on_rect_distance .backgroundColor = [UIColor clearColor];
    // [btn_on_rect_distance setImage:[UIImage imageNamed:@"dietary-table-img@2x.png"] forState:UIControlStateNormal];
    [btn_on_rect_distance addTarget:self action:@selector(click_on_distance_btn:) forControlEvents:UIControlEventTouchUpInside];
    [rect_for_distance   addSubview:btn_on_rect_distance];
    
#pragma mark Tableview
    
    
    UIButton *btn_img_filters = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_filters.frame = CGRectMake(40,CGRectGetMaxY(rect_for_distance.frame)+90, WIDTH-80,45);
    btn_img_filters .backgroundColor = [UIColor yellowColor];
    [btn_img_filters setImage:[UIImage imageNamed:@"button-filter@2x.png"] forState:UIControlStateNormal];
    [btn_img_filters addTarget:self action:@selector(click_on_filters_btn:) forControlEvents:UIControlEventTouchUpInside];
    btn_img_filters.userInteractionEnabled = YES;
    [view_for_food_items   addSubview:btn_img_filters];
    
    table_for_distance = [[UITableView alloc] init ];
    table_for_distance.frame  = CGRectMake(40,CGRectGetMidY(rect_for_distance.frame)+12,295,110);
    [table_for_distance setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_distance.delegate = self;
    table_for_distance.dataSource = self;
    table_for_distance.showsVerticalScrollIndicator = NO;
    table_for_distance.layer.borderColor = [[UIColor blackColor]CGColor];
    table_for_distance.layer.borderWidth = 1.0f;
    table_for_distance.clipsToBounds = YES;
    table_for_distance.backgroundColor = [UIColor clearColor];
    [view_for_food_items addSubview:table_for_distance];
    table_for_distance.hidden = YES;

    
    table_for_itemsfilter = [[UITableView alloc] init ];
    table_for_itemsfilter.frame  = CGRectMake(5,CGRectGetMaxY(btn_img_filters.frame)+15,WIDTH-10,210);
    [table_for_itemsfilter setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_itemsfilter.delegate = self;
    table_for_itemsfilter.dataSource = self;
    table_for_itemsfilter.showsVerticalScrollIndicator = NO;
    table_for_itemsfilter.backgroundColor = [UIColor clearColor];
    table_for_itemsfilter.scrollEnabled = YES;
    [view_for_food_items addSubview:table_for_itemsfilter];

    
    
    //=================================== search for food later ========================================
    
#pragma mark view for food later
    
    
    view_serch_for_food_later = [[UIView alloc]init];
    view_serch_for_food_later.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT);
    [view_serch_for_food_later setUserInteractionEnabled:YES];
    view_serch_for_food_later.backgroundColor=[UIColor clearColor];
    [scroll  addSubview:  view_serch_for_food_later];
    view_serch_for_food_later.hidden = YES;
    
    UILabel *lbl_date = [[UILabel alloc]init];
    lbl_date.frame = CGRectMake(150,0, 150, 45);
    lbl_date.text = @"Date";
    lbl_date.font = [UIFont fontWithName:kFont size:18];
    lbl_date.textColor = [UIColor blackColor];
    lbl_date.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_date];
    
    UIImageView *bg_for_date = [[UIImageView alloc]init];
    bg_for_date.frame = CGRectMake(-5,CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
    [bg_for_date setUserInteractionEnabled:YES];
    bg_for_date.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_serch_for_food_later addSubview:bg_for_date];
    
    
    UIImageView *img_calender = [[UIImageView alloc]init];
    img_calender.frame = CGRectMake(85,5, 30, 30);
    [img_calender setImage:[UIImage imageNamed:@"img-calender@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_calender setUserInteractionEnabled:YES];
    [bg_for_date addSubview:img_calender];
    
    //    UITextField *txt_date = [[UITextField alloc] init];
    //    txt_date .frame = CGRectMake(CGRectGetMaxX( img_calender.frame)+5,5, 100, 30);
    //    txt_date .borderStyle = UITextBorderStyleNone;
    //    txt_date .textColor = [UIColor grayColor];
    //    txt_date .font = [UIFont fontWithName:kFont size:13];
    //    txt_date .placeholder = @"15-07-2015";
    //    [txt_date  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    //    [txt_date  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    //    UIView *padding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    //    txt_date .leftView = padding;
    //    txt_date .leftViewMode = UITextFieldViewModeAlways;
    //    txt_date .userInteractionEnabled=NO;
    //    txt_date .textAlignment = NSTextAlignmentLeft;
    //    txt_date .backgroundColor = [UIColor clearColor];
    //    txt_date .keyboardType = UIKeyboardTypeAlphabet;
    //    txt_date .delegate = self;
    //    [bg_for_date addSubview:txt_date ];
    //
    
    
    
    txt_date = [[UITextField alloc] init];
    txt_date.borderStyle = UITextBorderStyleNone;
    txt_date.placeholder = @"dd-mm- yyyy";//@"__ __ / __ __ / __ __ __ __";
    
    if(IS_IPHONE_6Plus)
    {
        //        txt_Date.frame = CGRectMake(5, 5, 200, 20);
        txt_date.placeholder = @"dd-mm- yyyy";
        txt_date.font = [UIFont systemFontOfSize:17.0];
        [txt_date setValue:[UIFont fontWithName:kFont size: 17] forKeyPath:@"_placeholderLabel.font"];
        
        
        
    }
    else if(IS_IPHONE_6)
    {
        //         txt_Date.frame = CGRectMake(5, 5, 200, 20);
        txt_date.placeholder = @"dd-mm- yyyy";
        txt_date.font = [UIFont systemFontOfSize:15.0];
        [txt_date setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
        
    }
    else
    {
        //         txt_Date.frame = CGRectMake(5, 5, 200, 20);
        txt_date.placeholder = @"dd-mm- yyyy";
        txt_date.font = [UIFont systemFontOfSize:14.0];
        [txt_date setValue:[UIFont fontWithName:kFont size: 14] forKeyPath:@"_placeholderLabel.font"];
        
    }
    txt_date.textColor = [UIColor colorWithRed:167.0/255.0 green:167.0/255.0 blue:167.0/255.0 alpha:1.0];
    // txt_Date.font = [UIFont systemFontOfSize:15.0];
    //txt_date.background = [UIColor redColor];
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 4, 20)];
    txt_date.leftView = paddingView1;
    txt_date.leftViewMode = UITextFieldViewModeAlways;
    txt_date.autocorrectionType = UITextAutocorrectionTypeNo;
    //   txt_Date.keyboardType = UIKeyboardTypeDefault;
    txt_date.clearButtonMode = UITextFieldViewModeWhileEditing;
    txt_date.delegate = self;
    txt_date.userInteractionEnabled = YES;
    [txt_date setValue:[UIColor colorWithRed:167.0/255.0 green:167.0/255.0 blue:167.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    // [txt_Date setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [bg_for_date addSubview:txt_date];
    
    if (keyboardToolbar_Date == nil)
    {
        keyboardToolbar_Date = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar_Date setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_DoneDate:)];
        [keyboardToolbar_Date setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    txt_date.inputAccessoryView = keyboardToolbar_Date;
    txt_date.backgroundColor=[UIColor clearColor];
    
    //   txt_DateBirth = txt_date_of_birth;
    
    formatter = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    //datePicker.minimumDate = [formatter dateFromString:@"1900-01-01"];
    
    
    NSDate *currentDate = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate];
    components.day=components.day;
    components.month=components.month;
    components.year = components.year;
    //[components setYear:0];
    
    //NSString *temp=[NSString stringWithFormat:@" Day:%d Months:%d Year:%ld",(long)components.day,(long)components.month,(long)components.year];
    //    NSLog(@"%@",temp);
    //    NSString *string = [NSString stringWithFormat:@"%ld.%ld.%ld", (long)components.day, (long)components.month, (long)components.year];
   // NSDate *minDate=[calendar dateByAddingComponents:components toDate:currentDate  options:0];
    NSDate *minDate=  [formatter dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components.year, (long)components.month,(long)components.day]];

    NSLog(@"Minimum date is :: %@",minDate);
    
    //[components setYear:20];
    
    //NSDate *maxDate = [calendar dateByAddingComponents:components toDate:currentDate  options:0];
    
    NSDate *maxDate=  [formatter dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components.year+20, (long)components.month,(long)components.day]];
    
    //   datePicker.maximumDate= [formatter dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components.year-20, (long)components.month,(long)components.day]];
    
    NSLog(@"Maximum date is :: %@",maxDate);
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [datePicker setMinimumDate:currentDate];
    [datePicker setMaximumDate:maxDate];
    [datePicker setDate:currentDate];
    txt_date.inputView = datePicker;
    
    
    
    
    
    
    
    
    
    
    
    
    //    UIButton *btn_set_date = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_set_date.frame = CGRectMake(85,5,150,35);
    //    btn_set_date .backgroundColor = [UIColor purpleColor];
    //    [btn_set_date addTarget:self action:@selector(btn_set_date_click:) forControlEvents:UIControlEventTouchUpInside];
    //    // [btn_set_date setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    //    [bg_for_date   addSubview:btn_set_date];
    
    
    UILabel *lbl_serving_time = [[UILabel alloc]init];
    lbl_serving_time.frame = CGRectMake(125,CGRectGetMaxY(bg_for_date.frame)+5, 150, 45);
    lbl_serving_time.text = @"Serving Time";
    lbl_serving_time.font = [UIFont fontWithName:kFont size:18];
    lbl_serving_time.textColor = [UIColor blackColor];
    lbl_serving_time.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_serving_time];
    
    UIImageView *bg_for_time = [[UIImageView alloc]init];
    bg_for_time.frame = CGRectMake(-5,CGRectGetMaxY(lbl_serving_time.frame)-5, WIDTH+20, 50);
    [bg_for_time setUserInteractionEnabled:YES];
    bg_for_time.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_serch_for_food_later addSubview:bg_for_time];
    
    UIImageView *img_clock = [[UIImageView alloc]init];
    img_clock.frame = CGRectMake(120,5, 30, 30);
    [img_clock setImage:[UIImage imageNamed:@"img-clock@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_clock setUserInteractionEnabled:YES];
    [bg_for_time addSubview:img_clock];
    
    //    UITextField *txt_set_time = [[UITextField alloc] init];
    //   txt_set_time .frame = CGRectMake(CGRectGetMaxX( img_clock.frame)+5,5, 100, 30);
    //    txt_set_time .borderStyle = UITextBorderStyleNone;
    //    txt_set_time .textColor = [UIColor grayColor];
    //    txt_set_time .font = [UIFont fontWithName:kFont size:13];
    //    txt_set_time .placeholder = @"10:45AM";
    //    [txt_set_time  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    //    [txt_set_time  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    //    UIView *padding1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    //    txt_set_time .leftView = padding1;
    //    txt_set_time .leftViewMode = UITextFieldViewModeAlways;
    //    txt_set_time .userInteractionEnabled=NO;
    //    txt_set_time .textAlignment = NSTextAlignmentLeft;
    //    txt_set_time .backgroundColor = [UIColor clearColor];
    //    txt_set_time .keyboardType = UIKeyboardTypeAlphabet;
    //    txt_set_time .delegate = self;
    //    [bg_for_time addSubview:txt_set_time ];
    //
    
    NSDate *currDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"hh:mm"];
    [dateFormatter setAMSymbol:@"am"];
    [dateFormatter setPMSymbol:@"pm"];
    [dateFormatter setTimeStyle: NSDateFormatterShortStyle];
    NSString*dateString;
    
    dateString = [dateFormatter stringFromDate:currDate];
    
    txt_time = [[UITextField alloc] init];
    txt_time.borderStyle = UITextBorderStyleNone;
    txt_time.placeholder = @"time";
    txt_time.textColor = [UIColor colorWithRed:167.0/255.0 green:167.0/255.0 blue:167.0/255.0 alpha:1.0];
    txt_time.font = [UIFont systemFontOfSize:13.0];
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 4, 20)];
    txt_time.leftView = paddingView2;
    txt_time.leftViewMode = UITextFieldViewModeAlways;
    txt_time.autocorrectionType = UITextAutocorrectionTypeNo;
    //   txt_Date.keyboardType = UIKeyboardTypeDefault;
    txt_time.clearButtonMode = UITextFieldViewModeWhileEditing;
    txt_time.delegate = self;
    txt_time.userInteractionEnabled = YES;
    txt_time.text = dateString;
    
    [txt_time setValue:[UIColor colorWithRed:167.0/255.0 green:167.0/255.0 blue:167.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    [txt_time setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [bg_for_time addSubview:txt_time];
    
    if (keyboardToolbar_time == nil)
    {
        keyboardToolbar_time = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar_time setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_Donetime:)];
        [keyboardToolbar_time setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    txt_time.inputAccessoryView = keyboardToolbar_time;
    txt_time.backgroundColor=[UIColor clearColor];
    
    datePickertime = [[UIDatePicker alloc] init];
    datePickertime.datePickerMode = UIDatePickerModeTime;
    [datePickertime addTarget:self action:@selector(timedatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    txt_time.inputView = datePickertime;
    
    formattertime = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"yyyy-MM-dd"];
    [formattertime setDateFormat:@"hh:mm a"];
    //datePicker.minimumDate = [formatter dateFromString:@"1900-01-01"];
    
    NSDate *now1 = [NSDate date];
    NSCalendar *calendar1 = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components1 = [calendar1 components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now1];
    components1.year = components1.year  ;
    
    datePickertime.minimumDate = now1;
    
    datePickertime.maximumDate = [formattertime dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components1.year+20, (long)components1.month,(long)components1.day]];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    UIButton *btn_set_time = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_set_time.frame = CGRectMake(85,5,150,35);
    btn_set_time .backgroundColor = [UIColor clearColor];
    [btn_set_time addTarget:self action:@selector(btn_set_time_click:) forControlEvents:UIControlEventTouchUpInside];
    // [btn_set_date setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    //[bg_for_time   addSubview:btn_set_time];
    
    
    UILabel *lbl_location_in_food_later = [[UILabel alloc]init];
    lbl_location_in_food_later.frame = CGRectMake(140,CGRectGetMaxY(bg_for_time.frame), 150, 45);
    lbl_location_in_food_later.text = @"Location";
    lbl_location_in_food_later.font = [UIFont fontWithName:kFont size:18];
    lbl_location_in_food_later.textColor = [UIColor blackColor];
    lbl_location_in_food_later.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_location_in_food_later];
    
    UIImageView *bg_for_location2 = [[UIImageView alloc]init];
    bg_for_location2.frame = CGRectMake(-5, CGRectGetMaxY(lbl_location_in_food_later.frame)-5, WIDTH+20, 50);
    [bg_for_location2 setUserInteractionEnabled:YES];
    bg_for_location2.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [ view_serch_for_food_later addSubview:bg_for_location2];
    
    UIImageView *img_pointer_location2 = [[UIImageView alloc]init];
    img_pointer_location2.frame = CGRectMake(25,5, 30, 30);
    [img_pointer_location2 setImage:[UIImage imageNamed:@"pointer-location@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_pointer_location2 setUserInteractionEnabled:YES];
    [bg_for_location2 addSubview:img_pointer_location2];
    
    UILabel *lbl_set_location2 = [[UILabel alloc]init];
    lbl_set_location2.frame = CGRectMake(CGRectGetMaxX(img_pointer_location2.frame)+10,5,250, 30);
    lbl_set_location2.text = @"Smith Street, 77 Block (5km)";
    lbl_set_location2.font = [UIFont fontWithName:kFont size:18];
    lbl_set_location2.textColor = [UIColor blackColor];
    lbl_set_location2.backgroundColor = [UIColor clearColor];
    [bg_for_location2 addSubview:lbl_set_location2];
    
    
    UIButton *btn_img_right2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_right2.frame = CGRectMake(CGRectGetMidX(lbl_set_location2.frame)+140,5,30,30);
    btn_img_right2 .backgroundColor = [UIColor clearColor];
    [btn_img_right2 addTarget:self action:@selector(btn_right_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_right2 setImage:[UIImage imageNamed:@"right-arrow@2x.png"] forState:UIControlStateNormal];
    [bg_for_location2   addSubview:btn_img_right2];
    
    
    UIButton *btn_on_set_location2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_set_location2.frame = CGRectMake(0,0,WIDTH+5,50);
    btn_on_set_location2 .backgroundColor = [UIColor clearColor];
    [btn_on_set_location2 addTarget:self action:@selector(btn_on_set_location_click:) forControlEvents:UIControlEventTouchUpInside];
    // [btn_set_date setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    [bg_for_location2   addSubview:btn_on_set_location2];
    
    UILabel *lbl_dish_r_meal2 = [[UILabel alloc]init];
    lbl_dish_r_meal2.frame = CGRectMake(140,CGRectGetMaxY( bg_for_location2.frame)+10, 150, 45);
    lbl_dish_r_meal2.text = @"Dish/Meal";
    lbl_dish_r_meal2.font = [UIFont fontWithName:kFont size:18];
    lbl_dish_r_meal2.textColor = [UIColor blackColor];
    lbl_dish_r_meal2.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_dish_r_meal2];
    
    UIImageView *bg_for_meal2 = [[UIImageView alloc]init];
    bg_for_meal2.frame = CGRectMake(-5,CGRectGetMaxY(lbl_dish_r_meal2.frame)-5, WIDTH+20, 55);
    [bg_for_meal2 setUserInteractionEnabled:YES];
    bg_for_meal2.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [ view_serch_for_food_later addSubview:bg_for_meal2];
    
    
    btn_on_single_dish_in_later  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_single_dish_in_later.frame = CGRectMake(100,5,60,50);
    //    [btn_on_single_dish_in_later setImage:[UIImage imageNamed:@"img-single-bl@2x.png"] forState:UIControlStateSelected];
    [btn_on_single_dish_in_later setImage:[UIImage imageNamed:@"single-dish-icon@2x.png"] forState:UIControlStateNormal];
    btn_on_single_dish_in_later.tag = 1;
    [btn_on_single_dish_in_later addTarget:self action:@selector(btn_on_dish_in_later_click:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_meal2 addSubview:  btn_on_single_dish_in_later];
    
    
    img_red_tik3 = [[UIImageView alloc]init];
    img_red_tik3.frame =   CGRectMake(CGRectGetMidX( btn_on_single_dish_in_later.frame)+18,40,20,15);
    [img_red_tik3 setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik3 setUserInteractionEnabled:YES];
    [bg_for_meal2 addSubview:img_red_tik3];
    img_red_tik3.hidden = YES;
    
    btn_on_meal2  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_meal2.frame = CGRectMake(CGRectGetMaxX( btn_on_single_dish_in_later.frame)+50,3,50,50);
    //    [btn_on_meal2 setImage:[UIImage imageNamed:@"iocn-meal-bl@2x.png"] forState:UIControlStateSelected];
    [btn_on_meal2 setImage:[UIImage imageNamed:@"iocn-meal@2x.png"] forState:UIControlStateNormal];
    btn_on_meal2.tag = 2;
    [btn_on_meal2 addTarget:self action:@selector(btn_on_dish_in_later_click:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_meal2 addSubview:  btn_on_meal2];
    
    
    img_red_tik4 = [[UIImageView alloc]init];
    img_red_tik4.frame =   CGRectMake(CGRectGetMidX( btn_on_meal2.frame)+18,40,20,15);
    [img_red_tik4 setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik4 setUserInteractionEnabled:YES];
    [bg_for_meal2 addSubview:img_red_tik4];
    img_red_tik4.hidden = YES;
#pragma mark prabhu servicing change
    
    
    UILabel *lbl_seving_type2 = [[UILabel alloc]init];
    lbl_seving_type2.frame = CGRectMake(135,CGRectGetMaxY(bg_for_meal2.frame)+5, 150, 45);
    lbl_seving_type2.text = @"Serving Type";
    lbl_seving_type2.font = [UIFont fontWithName:kFont size:18];
    lbl_seving_type2.textColor = [UIColor blackColor];
    lbl_seving_type2.backgroundColor = [UIColor clearColor];
    [ view_serch_for_food_later addSubview:lbl_seving_type2];
    
    UIImageView *bg_for_serving_type2 = [[UIImageView alloc]init];
    bg_for_serving_type2.frame = CGRectMake(-5,CGRectGetMaxY(lbl_seving_type2.frame)-5, WIDTH+20, 70);
    [bg_for_serving_type2 setUserInteractionEnabled:YES];
    bg_for_serving_type2.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [ view_serch_for_food_later addSubview:bg_for_serving_type2];
    
    
    
    
    btn_foodlater_dinein_serve  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_foodlater_dinein_serve.frame = CGRectMake(45,10,50,50);
    [ btn_foodlater_dinein_serve setImage:[UIImage imageNamed:@"img-dine-in-bl@2x.png"] forState:UIControlStateSelected];
    [ btn_foodlater_dinein_serve setImage:[UIImage imageNamed:@"img-dine-in@2x.png"] forState:UIControlStateNormal];
    btn_foodlater_dinein_serve.tag = 0;
    [ btn_foodlater_dinein_serve addTarget:self action:@selector(click_on_serving_typefoodlater:)forControlEvents:UIControlEventTouchUpInside];
    [bg_for_serving_type2 addSubview:btn_foodlater_dinein_serve];
    
    img_red_tik_on_dine_in_foodlater = [[UIImageView alloc]init];
    img_red_tik_on_dine_in_foodlater.frame = CGRectMake(CGRectGetMidX(btn_foodlater_dinein_serve.frame)+5,35,25,20);  [img_red_tik_on_dine_in_foodlater setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik_on_dine_in_foodlater setUserInteractionEnabled:YES];
    [bg_for_serving_type2 addSubview:img_red_tik_on_dine_in_foodlater];
    img_red_tik_on_dine_in_foodlater.hidden = YES;
    
    
    btn_foodlater_takeout  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_foodlater_takeout.frame = CGRectMake(CGRectGetMaxX(btn_on_dine_in.frame)+70,10,50,50);
    [btn_foodlater_takeout setImage:[UIImage imageNamed:@"img-take-out-bl@2x.png"]forState:UIControlStateSelected];
    [btn_foodlater_takeout setImage:[UIImage imageNamed:@"img-take-out@2x.png"]forState:UIControlStateNormal];
    btn_foodlater_takeout.tag = 1;
    [btn_foodlater_takeout addTarget:self action:@selector(click_on_serving_typefoodlater:)forControlEvents:UIControlEventTouchUpInside];
    [bg_for_serving_type2 addSubview:btn_foodlater_takeout];
    
    img_red_tik_on_takeout_foodlater = [[UIImageView alloc]init];
    img_red_tik_on_takeout_foodlater.frame = CGRectMake(CGRectGetMidX(btn_foodlater_takeout.frame)+5,35,25,20);
    [img_red_tik_on_takeout_foodlater setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik_on_takeout_foodlater setUserInteractionEnabled:YES];
    [bg_for_serving_type2 addSubview:img_red_tik_on_takeout_foodlater];
    img_red_tik_on_takeout_foodlater.hidden = YES;
    
    
    btn_foodlater_delivary  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_foodlater_delivary.frame = CGRectMake(CGRectGetMaxX( btn_on_take_out.frame)+70,10,50,50);
    [btn_foodlater_delivary setImage:[UIImage imageNamed:@"img-delever-bl@2x.png"] forState:UIControlStateSelected];
    [btn_foodlater_delivary setImage:[UIImage imageNamed:@"img-delever@2x.png"] forState:UIControlStateNormal];
    btn_foodlater_delivary.tag = 2;
    [btn_foodlater_delivary addTarget:self action:@selector(click_on_serving_typefoodlater:)forControlEvents:UIControlEventTouchUpInside];
    [bg_for_serving_type2 addSubview:btn_foodlater_delivary];
    
    img_red_tik_on_delivary_foodlater = [[UIImageView alloc]init];
    img_red_tik_on_delivary_foodlater.frame = CGRectMake(CGRectGetMidX(btn_foodlater_delivary.frame)+5,35,25,20);
    [img_red_tik_on_delivary_foodlater setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik_on_delivary_foodlater setUserInteractionEnabled:YES];
    [bg_for_serving_type2 addSubview:img_red_tik_on_delivary_foodlater];
    img_red_tik_on_delivary_foodlater.hidden = YES;
    
    
    
    UILabel *lbl_keywords2 = [[UILabel alloc]init];
    lbl_keywords2.frame = CGRectMake(155,CGRectGetMaxY(bg_for_serving_type2.frame), 150, 45);
    lbl_keywords2.text = @"Keywords";
    lbl_keywords2.font = [UIFont fontWithName:kFont size:18];
    lbl_keywords2.textColor = [UIColor blackColor];
    lbl_keywords2.backgroundColor = [UIColor clearColor];
    [ view_serch_for_food_later addSubview:lbl_keywords2];
    
    UIImageView *bg_for_Keyword2 = [[UIImageView alloc]init];
    bg_for_Keyword2.frame =CGRectMake(-5,CGRectGetMaxY(lbl_keywords2.frame)-5, WIDTH+20, 50);
    [bg_for_Keyword2 setUserInteractionEnabled:YES];
    bg_for_Keyword2.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [ view_serch_for_food_later addSubview:bg_for_Keyword2];
    
    UIImageView *img_serch_bar2 = [[UIImageView alloc]init];
    img_serch_bar2.frame = CGRectMake(15,10, WIDTH-20, 30);
    [img_serch_bar2 setImage:[UIImage imageNamed:@"img-search-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_serch_bar2 setUserInteractionEnabled:YES];
    [bg_for_Keyword2 addSubview:img_serch_bar2];
    
    txt_search2 = [[UITextField alloc] init];
    txt_search2.frame = CGRectMake(0,-8, 300, 45);
    txt_search2 .borderStyle = UITextBorderStyleNone;
    txt_search2 .textColor = [UIColor grayColor];
    txt_search2 .font = [UIFont fontWithName:kFont size:15];
    txt_search2 .placeholder = @"Search...";
    [txt_search2  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_search2 setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_search2 .leftView = padding4;
    txt_search2 .leftViewMode = UITextFieldViewModeAlways;
    txt_search2 .userInteractionEnabled=YES;
    txt_search2 .textAlignment = NSTextAlignmentLeft;
    txt_search2 .backgroundColor = [UIColor clearColor];
    txt_search2 .keyboardType = UIKeyboardTypeAlphabet;
    txt_search2 .delegate = self;
    [img_serch_bar2 addSubview:txt_search2];
    
    
    UIImageView *icon_search2 = [[UIImageView alloc]init];
    icon_search2.frame = CGRectMake(330,8, 15, 15);
    [icon_search2 setImage:[UIImage imageNamed:@"icon-search@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_search2 setUserInteractionEnabled:YES];
    [img_serch_bar2 addSubview:icon_search2];
    
    UIButton *btn_on_search_bar2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_search_bar2.frame = CGRectMake(330,0, 30, 30);
    btn_on_search_bar2 .userInteractionEnabled=YES;
    btn_on_search_bar2 .backgroundColor = [UIColor clearColor];
    [btn_on_search_bar2 addTarget:self action:@selector(btn_on_search_bar_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar2 setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [img_serch_bar2   addSubview:btn_on_search_bar2];
    
    //view for btn more filters ans  dish table view
    
    view_for_more_filters_in_later = [[UIView alloc]init];
    view_for_more_filters_in_later.frame=CGRectMake(0,CGRectGetMaxY(bg_for_Keyword2.frame)+20,WIDTH,400);
    [view_for_more_filters_in_later setUserInteractionEnabled:YES];
    view_for_more_filters_in_later.backgroundColor=[UIColor clearColor];
    view_for_more_filters_in_later.hidden = NO;
    [ view_serch_for_food_later addSubview: view_for_more_filters_in_later];
    
    
    UIButton *btn_img_more_filters_in_later = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_more_filters_in_later.frame = CGRectMake(40,10, WIDTH-80, 45);
    btn_img_more_filters_in_later .backgroundColor = [UIColor clearColor];
    [btn_img_more_filters_in_later addTarget:self action:@selector(btn_img_on_more_filters_later_click:) forControlEvents:UIControlEventTouchUpInside];
    [ btn_img_more_filters_in_later setImage:[UIImage imageNamed:@"img-more-filters@2x.png"] forState:UIControlStateNormal];
    [view_for_more_filters_in_later   addSubview:btn_img_more_filters_in_later];
    
    
#pragma mark Tableview
    
    table_for_items_in_later = [[UITableView alloc] init ];
    table_for_items_in_later.frame  = CGRectMake(5,CGRectGetMaxY(btn_img_more_filters_in_later.frame)+5,WIDTH-10,300);
    [table_for_items_in_later setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_items_in_later.delegate = self;
    table_for_items_in_later.dataSource = self;
    table_for_items_in_later.showsVerticalScrollIndicator = NO;
    table_for_items_in_later.backgroundColor = [UIColor clearColor];
    [view_for_more_filters_in_later addSubview:table_for_items_in_later];
    
    view_for_cuisines_in_later = [[UIView alloc]init];
    view_for_cuisines_in_later.frame=CGRectMake(0,CGRectGetMaxY(bg_for_Keyword2.frame),WIDTH,500);
    [view_for_cuisines_in_later setUserInteractionEnabled:YES];
    view_for_cuisines_in_later.backgroundColor=[UIColor clearColor];
    view_for_cuisines_in_later.hidden = YES;
    [ view_serch_for_food_later addSubview: view_for_cuisines_in_later];
    
    UILabel *lbl_cuisines_in_later = [[UILabel alloc]init];
    lbl_cuisines_in_later.frame = CGRectMake(130,5, 150, 45);
    lbl_cuisines_in_later.text = @"Cuisines";
    lbl_cuisines_in_later.font = [UIFont fontWithName:kFont size:18];
    lbl_cuisines_in_later.textColor = [UIColor blackColor];
    lbl_cuisines_in_later.backgroundColor = [UIColor clearColor];
    [view_for_cuisines_in_later addSubview:lbl_cuisines_in_later];
    
    UIImageView *bg_for_favorits_and_all_in_later = [[UIImageView alloc]init];
    bg_for_favorits_and_all_in_later.frame = CGRectMake(0,CGRectGetMaxY(lbl_cuisines_in_later.frame), WIDTH, 50);
    [bg_for_favorits_and_all_in_later setUserInteractionEnabled:YES];
    bg_for_favorits_and_all_in_later.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_cuisines_in_later addSubview:bg_for_favorits_and_all_in_later];
    
    
    UILabel *lbl_my_favorites_in_later = [[UILabel alloc]init];
    lbl_my_favorites_in_later.frame = CGRectMake(30,03, 150, 45);
    lbl_my_favorites_in_later.text = @"My Favorites";
    [lbl_my_favorites_in_later setUserInteractionEnabled:YES];
    lbl_my_favorites_in_later.font = [UIFont fontWithName:kFontBold size:18];
    lbl_my_favorites_in_later.textColor = [UIColor blackColor];
    lbl_my_favorites_in_later.backgroundColor = [UIColor clearColor];
    [bg_for_favorits_and_all_in_later addSubview:lbl_my_favorites_in_later];
    
    img_strip3 = [[UIImageView alloc]init];
    img_strip3.frame = CGRectMake(35, CGRectGetMaxY(lbl_my_favorites_in_later.frame)-5, 100, 3);
    [img_strip3 setImage:[UIImage imageNamed:@"black strip@2x.png"]];
    [img_strip3 setUserInteractionEnabled:YES];
    [bg_for_favorits_and_all_in_later addSubview:img_strip3];
    
    UIButton *btn_my_favorites_in_later = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_my_favorites_in_later.frame = CGRectMake(0,03, 180, 45);
    btn_my_favorites_in_later .backgroundColor = [UIColor clearColor];
    [btn_my_favorites_in_later setUserInteractionEnabled:YES];
    [btn_my_favorites_in_later addTarget:self action:@selector(btn_on_my_favorites_in_later_click:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_favorits_and_all_in_later addSubview:btn_my_favorites_in_later];
    
    // view for MY FAVORITES in FOOD NOW
    
    view_for_my_favorites_in_later = [[UIView alloc]init];
    view_for_my_favorites_in_later.frame=CGRectMake(0,CGRectGetMaxY(bg_for_favorits_and_all_in_later.frame),WIDTH,90);
    view_for_my_favorites_in_later.backgroundColor=[UIColor clearColor];
    [view_for_my_favorites_in_later setUserInteractionEnabled:YES];
    [view_for_cuisines_in_later addSubview:  view_for_my_favorites_in_later];
    
    UIImageView *bg_for_cuisines_in_later = [[UIImageView alloc]init];
    bg_for_cuisines_in_later.frame = CGRectMake(0,02, WIDTH, 100);
    [bg_for_cuisines_in_later setUserInteractionEnabled:YES];
    bg_for_cuisines_in_later.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_my_favorites_in_later addSubview:bg_for_cuisines_in_later];
    
    
#pragma collection view for favorites in food now
    
    layout4 = [[UICollectionViewFlowLayout alloc] init];
    collView_for_cuisines_in_later = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,95)
                                                        collectionViewLayout:layout4];
    [layout4 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_cuisines_in_later setDataSource:self];
    [collView_for_cuisines_in_later setDelegate:self];
    collView_for_cuisines_in_later.scrollEnabled = YES;
    collView_for_cuisines_in_later.showsVerticalScrollIndicator = NO;
    collView_for_cuisines_in_later.showsHorizontalScrollIndicator = NO;
    collView_for_cuisines_in_later.pagingEnabled = YES;
    [collView_for_cuisines_in_later registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_cuisines_in_later setBackgroundColor:[UIColor clearColor]];
    layout4.minimumInteritemSpacing = 12;
    layout4.minimumLineSpacing = 0;
    collView_for_cuisines_in_later.userInteractionEnabled = YES;
    [bg_for_cuisines_in_later  addSubview: collView_for_cuisines_in_later];
    
    UIImageView *img_left_arrow1_food_later = [[UIImageView alloc]init];
    img_left_arrow1_food_later.frame = CGRectMake(5,40,10, 15);
    [img_left_arrow1_food_later setUserInteractionEnabled:YES];
    img_left_arrow1_food_later.image=[UIImage imageNamed:@"left-arrow-img@2x.png"];
    [bg_for_cuisines_in_later addSubview:img_left_arrow1_food_later];
    
    UIButton *btn_on_lefet_arrow1_in_food_later = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_lefet_arrow1_in_food_later.frame = CGRectMake(0,0, 20,100);
    btn_on_lefet_arrow1_in_food_later .backgroundColor = [UIColor clearColor];
    [btn_on_lefet_arrow1_in_food_later setUserInteractionEnabled:YES];
    [btn_on_lefet_arrow1_in_food_later addTarget:self action:@selector(click_on_left_arrow1_in_food_later_btn:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_cuisines_in_later addSubview:btn_on_lefet_arrow1_in_food_later];
    
    UIImageView *img_right_arrow1_in_food_later = [[UIImageView alloc]init];
    img_right_arrow1_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame)+3,40,10, 15);
    [img_right_arrow1_in_food_later setUserInteractionEnabled:YES];
    img_right_arrow1_in_food_later.image=[UIImage imageNamed:@"right-arrow@2x.png"];
    [bg_for_cuisines_in_later addSubview:img_right_arrow1_in_food_later];
    
    UIButton *btn_on_right_arrow1_in_food_later = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_right_arrow1_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,100);
    btn_on_right_arrow1_in_food_later .backgroundColor = [UIColor clearColor];
    [btn_on_right_arrow1_in_food_later setUserInteractionEnabled:YES];
    [btn_on_right_arrow1_in_food_later addTarget:self action:@selector(click_on_right_arrow1in_food_later_btn:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_cuisines_in_later addSubview:btn_on_right_arrow1_in_food_later];
    
    
    
    UILabel *lbl_all_in_later = [[UILabel alloc]init];
    lbl_all_in_later.frame = CGRectMake(250,3, 150, 45);
    lbl_all_in_later.text = @"All";
    [lbl_all_in_later setUserInteractionEnabled:YES];
    lbl_all_in_later.font = [UIFont fontWithName:kFontBold size:18];
    lbl_all_in_later.textColor = [UIColor blackColor];
    lbl_all_in_later.backgroundColor = [UIColor clearColor];
    [bg_for_favorits_and_all_in_later addSubview:lbl_all_in_later];
    
    
    img_strip4 = [[UIImageView alloc]init];
    img_strip4.frame = CGRectMake(250, CGRectGetMaxY(lbl_my_favorites.frame)-5, 57, 3);
    [img_strip4 setImage:[UIImage imageNamed:@"black strip@2x.png"]];
    [img_strip4 setUserInteractionEnabled:YES];
    [bg_for_favorits_and_all_in_later addSubview:img_strip4];
    img_strip4.hidden = YES;
    
    UIButton *btn_on_all_in_later = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_all_in_later.frame = CGRectMake(185,03, 180, 45);
    [btn_on_all_in_later setUserInteractionEnabled:YES];
    btn_on_all_in_later .backgroundColor = [UIColor clearColor];
    [btn_on_all_in_later addTarget:self action:@selector(btn_all_in_later_click:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_favorits_and_all_in_later  addSubview:btn_on_all_in_later];
    
    // view for ALL IN FOOD NOW
    
    view_for_all_in_food_later = [[UIView alloc]init];
    view_for_all_in_food_later.frame=CGRectMake(0,CGRectGetMaxY(bg_for_favorits_and_all.frame),WIDTH,100);
    view_for_all_in_food_later.backgroundColor=[UIColor clearColor];
    [view_for_all_in_food_later setUserInteractionEnabled:YES];
    [view_for_cuisines_in_later addSubview:  view_for_all_in_food_later];
    view_for_all_in_food_later.hidden = YES;
    
    
    UIImageView *bg_for_all_in_food_later = [[UIImageView alloc]init];
    bg_for_all_in_food_later.frame = CGRectMake(0,02, WIDTH, 100);
    [bg_for_all_in_food_later setUserInteractionEnabled:YES];
    bg_for_all_in_food_later.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_all_in_food_later addSubview:bg_for_all_in_food_later];
    
    
    layout5 = [[UICollectionViewFlowLayout alloc] init];
    collView_for_cuisines_in_all_food_later = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,95)
                                                                 collectionViewLayout:layout5];
    [layout5 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_cuisines_in_all_food_later setDataSource:self];
    [collView_for_cuisines_in_all_food_later setDelegate:self];
    collView_for_cuisines_in_all_food_later.scrollEnabled = YES;
    collView_for_cuisines_in_all_food_later.showsVerticalScrollIndicator = NO;
    collView_for_cuisines_in_all_food_later.showsHorizontalScrollIndicator = NO;
    collView_for_cuisines_in_all_food_later.pagingEnabled = YES;
    [collView_for_cuisines_in_all_food_later registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_cuisines_in_all_food_later setBackgroundColor:[UIColor clearColor]];
    layout5.minimumInteritemSpacing = 12;
    layout5.minimumLineSpacing = 0;
    collView_for_cuisines_in_all_food_later.userInteractionEnabled = YES;
    [bg_for_all_in_food_later  addSubview: collView_for_cuisines_in_all_food_later];
    
    
    UIImageView *img_left_arrow2_food_later = [[UIImageView alloc]init];
    img_left_arrow2_food_later.frame = CGRectMake(5,40,10, 15);
    [img_left_arrow2_food_later setUserInteractionEnabled:YES];
    img_left_arrow2_food_later.image=[UIImage imageNamed:@"left-arrow-img@2x.png"];
    [bg_for_all_in_food_later addSubview:img_left_arrow2_food_later];
    
    UIButton *btn_on_lefet_arrow2_in_later = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_lefet_arrow2_in_later.frame = CGRectMake(0,0, 20,100);
    btn_on_lefet_arrow2_in_later .backgroundColor = [UIColor clearColor];
    [btn_on_lefet_arrow2_in_later setUserInteractionEnabled:YES];
    [btn_on_lefet_arrow2_in_later addTarget:self action:@selector(click_on_left_arrow2_in_later_btn:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_all_in_food_later addSubview:btn_on_lefet_arrow2_in_later];
    
    
    UIImageView *img_right_arrow2_in_food_later = [[UIImageView alloc]init];
    img_right_arrow2_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines_in_all_food_later.frame)+3,40,10, 15);
    [img_right_arrow2_in_food_later setUserInteractionEnabled:YES];
    img_right_arrow2_in_food_later.image=[UIImage imageNamed:@"right-arrow@2x.png"];
    [bg_for_all_in_food_later addSubview:img_right_arrow2_in_food_later];
    
    UIButton *btn_on_right_arrow2_in_later = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_right_arrow2_in_later.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines_in_all_food_later.frame),0,20,100);
    btn_on_right_arrow2_in_later .backgroundColor = [UIColor clearColor];
    [btn_on_right_arrow2_in_later setUserInteractionEnabled:YES];
    [btn_on_right_arrow2_in_later addTarget:self action:@selector(click_on_right_arrow2_in_later_btn:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_all_in_food_later addSubview:btn_on_right_arrow2_in_later];
    
    UILabel *lbl_course_in_food_later = [[UILabel alloc]init];
    lbl_course_in_food_later.frame = CGRectMake(140,CGRectGetMaxY(view_for_my_favorites.frame), 150, 45);
    lbl_course_in_food_later.text = @"Course";
    [lbl_course_in_food_later setUserInteractionEnabled:YES];
    lbl_course_in_food_later.font = [UIFont fontWithName:kFont size:18];
    lbl_course_in_food_later.textColor = [UIColor blackColor];
    lbl_course_in_food_later.backgroundColor = [UIColor clearColor];
    [view_for_cuisines_in_later addSubview:lbl_course_in_food_later];
    
    UIImageView *bg_for_course_in_food_later = [[UIImageView alloc]init];
    bg_for_course_in_food_later.frame = CGRectMake(0,CGRectGetMaxY(lbl_course_in_food_now.frame)-10, WIDTH, 65);
    [bg_for_course_in_food_later setUserInteractionEnabled:YES];
    bg_for_course_in_food_later.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_cuisines_in_later addSubview:bg_for_course_in_food_later];
    
    
    layout6 = [[UICollectionViewFlowLayout alloc] init];
    collView_for_course_in_food_later = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,60)
                                                           collectionViewLayout:layout6];
    [layout6 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_course_in_food_later setDataSource:self];
    [collView_for_course_in_food_later setDelegate:self];
    collView_for_course_in_food_later.scrollEnabled = YES;
    collView_for_course_in_food_later.showsVerticalScrollIndicator = NO;
    collView_for_course_in_food_later.showsHorizontalScrollIndicator = NO;
    collView_for_course_in_food_later.pagingEnabled = YES;
    [collView_for_course_in_food_later registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_course_in_food_later setBackgroundColor:[UIColor clearColor]];
    layout6.minimumInteritemSpacing = 12;
    layout6.minimumLineSpacing = 0;
    collView_for_course_in_food_later.userInteractionEnabled = YES;
    [bg_for_course_in_food_later  addSubview: collView_for_course_in_food_later];
    
    
    UIImageView *img_left_arrow3_food_later = [[UIImageView alloc]init];
    img_left_arrow3_food_later.frame = CGRectMake(5,24,10, 15);
    [img_left_arrow3_food_later setUserInteractionEnabled:YES];
    img_left_arrow3_food_later.image=[UIImage imageNamed:@"left-arrow-img@2x.png"];
    [bg_for_course_in_food_later addSubview:img_left_arrow3_food_later];
    
    UIButton *btn_on_lefet_arrow3_in_later = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_lefet_arrow3_in_later.frame = CGRectMake(0,0, 20,65);
    btn_on_lefet_arrow3_in_later .backgroundColor = [UIColor clearColor];
    [btn_on_lefet_arrow3_in_later setUserInteractionEnabled:YES];
    [btn_on_lefet_arrow3_in_later addTarget:self action:@selector(click_on_left_arrow3_in_later_btn:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_course_in_food_later addSubview:btn_on_lefet_arrow3_in_later];
    
    
    UIImageView *img_right_arrow3_in_food_later = [[UIImageView alloc]init];
    img_right_arrow3_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_course_in_food_later.frame)+3,24,10, 15);
    [img_right_arrow3_in_food_later setUserInteractionEnabled:YES];
    img_right_arrow3_in_food_later.image=[UIImage imageNamed:@"right-arrow@2x.png"];
    [bg_for_course_in_food_later addSubview:img_right_arrow3_in_food_later];
    
    
    UIButton *btn_on_right_arrow3_in_later = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_right_arrow3_in_later.frame = CGRectMake(CGRectGetMaxX(collView_for_course_in_food_later.frame),0,20,65);
    btn_on_right_arrow3_in_later .backgroundColor = [UIColor clearColor];
    [btn_on_right_arrow3_in_later setUserInteractionEnabled:YES];
    [btn_on_right_arrow3_in_later addTarget:self action:@selector(click_on_right_arrow3_in_later_btn:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_course_in_food_later addSubview:btn_on_right_arrow3_in_later];
    
    
    
    UILabel *lbl_Dietary_in_food_later = [[UILabel alloc]init];
    lbl_Dietary_in_food_later.frame = CGRectMake(110,CGRectGetMidY(bg_for_course_in_food_later.frame)+25,250,45);
    lbl_Dietary_in_food_later.text = @"Dietary Restrictions";
    [lbl_Dietary_in_food_later setUserInteractionEnabled:YES];
    lbl_Dietary_in_food_later.font = [UIFont fontWithName:kFont size:18];
    lbl_Dietary_in_food_later.textColor = [UIColor blackColor];
    lbl_Dietary_in_food_later.backgroundColor = [UIColor clearColor];
    [view_for_cuisines_in_later addSubview:lbl_Dietary_in_food_later];
    
    UIImageView *bg_for_dietary_in_food_later = [[UIImageView alloc]init];
    bg_for_dietary_in_food_later.frame = CGRectMake(0,CGRectGetMaxY(lbl_Dietary_in_food_later.frame)-5, WIDTH, 85);
    [bg_for_dietary_in_food_later setUserInteractionEnabled:YES];
    bg_for_dietary_in_food_later.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_cuisines_in_later addSubview:bg_for_dietary_in_food_later];
    
    layout6 = [[UICollectionViewFlowLayout alloc] init];
    collView_for_dietary_in_food_later = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,80)
                                                            collectionViewLayout:layout6];
    [layout6 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_dietary_in_food_later setDataSource:self];
    [collView_for_dietary_in_food_later setDelegate:self];
    collView_for_dietary_in_food_later.scrollEnabled = YES;
    collView_for_dietary_in_food_later.showsVerticalScrollIndicator = NO;
    collView_for_dietary_in_food_later.showsHorizontalScrollIndicator = NO;
    collView_for_dietary_in_food_later.pagingEnabled = YES;
    [collView_for_dietary_in_food_later registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_dietary_in_food_later setBackgroundColor:[UIColor clearColor]];
    layout6.minimumInteritemSpacing = 12;
    layout6.minimumLineSpacing = 0;
    collView_for_dietary_in_food_later.userInteractionEnabled = YES;
    [bg_for_dietary_in_food_later  addSubview: collView_for_dietary_in_food_later];
    
    
    UIImageView *img_left_arrow4_food_later = [[UIImageView alloc]init];
    img_left_arrow4_food_later.frame = CGRectMake(5,35,10, 15);
    [img_left_arrow4_food_later setUserInteractionEnabled:YES];
    img_left_arrow4_food_later.image=[UIImage imageNamed:@"left-arrow-img@2x.png"];
    [bg_for_dietary_in_food_later addSubview:img_left_arrow4_food_later];
    
    UIButton *btn_on_lefet_arrow4_in_later = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_lefet_arrow4_in_later.frame = CGRectMake(0,0, 20,85);
    btn_on_lefet_arrow4_in_later .backgroundColor = [UIColor clearColor];
    [btn_on_lefet_arrow4_in_later setUserInteractionEnabled:YES];
    [btn_on_lefet_arrow4_in_later addTarget:self action:@selector(click_on_left_arrow4_in_later_btn:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_dietary_in_food_later addSubview:btn_on_lefet_arrow4_in_later];
    
    
    UIImageView *img_right_arrow4_in_food_later = [[UIImageView alloc]init];
    img_right_arrow4_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_dietary_in_food_later.frame)+3,35,10, 15);
    [img_right_arrow4_in_food_later setUserInteractionEnabled:YES];
    img_right_arrow4_in_food_later.image=[UIImage imageNamed:@"right-arrow@2x.png"];
    [bg_for_dietary_in_food_later addSubview:img_right_arrow4_in_food_later];
    
    UIButton *btn_on_right_arrow4_in_later = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_right_arrow4_in_later.frame = CGRectMake(CGRectGetMaxX(collView_for_dietary_in_food_later.frame),0,20,85);
    btn_on_right_arrow4_in_later .backgroundColor = [UIColor clearColor];
    [btn_on_right_arrow4_in_later setUserInteractionEnabled:YES];
    [btn_on_right_arrow4_in_later addTarget:self action:@selector(click_on_right_arrow4_in_later_btn:) forControlEvents:UIControlEventTouchUpInside];
    [bg_for_dietary_in_food_later addSubview:btn_on_right_arrow4_in_later];
    
    
    
    UILabel *lbl_sortby_in_later = [[UILabel alloc]init];
    lbl_sortby_in_later.frame = CGRectMake(150,CGRectGetMidY(bg_for_dietary_in_food_later.frame)+35,250,45);
    lbl_sortby_in_later.text = @"Sort By";
    [lbl_sortby_in_later setUserInteractionEnabled:YES];
    lbl_sortby_in_later.font = [UIFont fontWithName:kFont size:18];
    lbl_sortby_in_later.textColor = [UIColor blackColor];
    lbl_sortby_in_later.backgroundColor = [UIColor clearColor];
    [view_for_cuisines_in_later addSubview:lbl_sortby_in_later];
    
    UIImageView *rect_for_distance_in_later = [[UIImageView alloc]init];
    rect_for_distance_in_later.frame = CGRectMake(40,CGRectGetMaxY(lbl_sortby_in_later.frame)-5, WIDTH-80, 25);
    [rect_for_distance_in_later setUserInteractionEnabled:YES];
    rect_for_distance_in_later.image=[UIImage imageNamed:@"dietary-table-img@2x.png"];
    [view_for_cuisines_in_later addSubview:rect_for_distance_in_later];
    
    lbl_distance_in_later = [[UILabel alloc]init];
    lbl_distance_in_later.frame = CGRectMake(05,-10,250,45);
    lbl_distance_in_later.text = @"Distance";
    [lbl_distance_in_later setUserInteractionEnabled:YES];
    lbl_distance_in_later.font = [UIFont fontWithName:kFont size:15];
    lbl_distance_in_later.textColor = [UIColor blackColor];
    lbl_distance_in_later.backgroundColor = [UIColor clearColor];
    [rect_for_distance_in_later addSubview:lbl_distance_in_later];
    
    UIButton *btn_on_rect_distance_in_later = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_rect_distance_in_later.frame = CGRectMake(0,0, 295, 30);
    btn_on_rect_distance_in_later .backgroundColor = [UIColor clearColor];
    // [btn_on_rect_distance setImage:[UIImage imageNamed:@"dietary-table-img@2x.png"] forState:UIControlStateNormal];
    [btn_on_rect_distance_in_later addTarget:self action:@selector(click_on_distance_in_later_btn:) forControlEvents:UIControlEventTouchUpInside];
    [rect_for_distance_in_later   addSubview:btn_on_rect_distance_in_later];
    
   
    
#pragma mark Tableview
    
    
    UIButton *btn_img_filters_in_later = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_filters_in_later.frame = CGRectMake(40,CGRectGetMaxY(rect_for_distance_in_later.frame)+90, WIDTH-80,45);
    btn_img_filters_in_later .backgroundColor = [UIColor clearColor];
    [btn_img_filters_in_later setImage:[UIImage imageNamed:@"button-filter@2x.png"] forState:UIControlStateNormal];
    [btn_img_filters_in_later addTarget:self action:@selector(click_on_filters_in_laterbtn:) forControlEvents:UIControlEventTouchUpInside];
    [view_for_cuisines_in_later   addSubview:btn_img_filters_in_later];
    
    
    table_for_distance_in_later = [[UITableView alloc] init ];
    table_for_distance_in_later.frame  = CGRectMake(40,CGRectGetMidY(rect_for_distance_in_later.frame)+12,295,70);
    [table_for_distance_in_later setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_distance_in_later.delegate = self;
    table_for_distance_in_later.dataSource = self;
    table_for_distance_in_later.showsVerticalScrollIndicator = NO;
    table_for_distance_in_later.layer.borderColor = [[UIColor blackColor]CGColor];
    table_for_distance_in_later.layer.borderWidth = 1.0f;
    table_for_distance_in_later.clipsToBounds = YES;
    table_for_distance_in_later.backgroundColor = [UIColor clearColor];
    [view_for_cuisines_in_later addSubview:table_for_distance_in_later];
    table_for_distance_in_later.hidden = YES;

    
    table_for_itemsfilterlater = [[UITableView alloc] init ];
    table_for_itemsfilterlater.frame  = CGRectMake(5,CGRectGetMaxY(btn_img_filters_in_later.frame)+15,WIDTH-10,210);
    [table_for_itemsfilterlater setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_itemsfilterlater.delegate = self;
    table_for_itemsfilterlater.dataSource = self;
    table_for_itemsfilterlater.showsVerticalScrollIndicator = NO;
    table_for_itemsfilterlater.backgroundColor = [UIColor clearColor];
    table_for_itemsfilterlater.scrollEnabled = YES;
    [view_for_cuisines_in_later addSubview:table_for_itemsfilterlater];

    
    
    
    if (IS_IPHONE_6Plus)
    {
        
        scroll.frame = CGRectMake(0, 46, WIDTH, HEIGHT);
        
        view_serch_for_food_now.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+380);
        
        lbl_location.frame = CGRectMake(145,5, 150, 45);
        bg_for_location.frame = CGRectMake(-5, CGRectGetMaxY(lbl_location.frame)-5, WIDTH+20, 50);
        img_pointer_location.frame = CGRectMake(25,7, 30, 30);
        lbl_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+20,9, 250, 30);
        btn_img_right.frame = CGRectMake(CGRectGetMidX(lbl_set_location.frame)+140,10,30,30);
        btn_on_set_location.frame = CGRectMake(0,0,WIDTH+5,50);
        
        lbl_dish_r_meal.frame = CGRectMake(140,CGRectGetMaxY( bg_for_location.frame)+10, 150, 45);
        bg_for_meal.frame = CGRectMake(-5,CGRectGetMaxY(lbl_dish_r_meal.frame)-5, WIDTH+20, 55);
        btn_on_single_dish.frame = CGRectMake(100,5,60,50);
        btn_on_meal.frame = CGRectMake(CGRectGetMaxX( btn_on_single_dish.frame)+60,3,50,50);
        img_red_tik1.frame =  CGRectMake(CGRectGetMidX( btn_on_single_dish.frame)+18,40,20,15);
        img_red_tik2.frame =  CGRectMake(CGRectGetMidX(btn_on_meal.frame)+18,40,20,15);
        
        lbl_seving_type.frame = CGRectMake(135,CGRectGetMaxY(bg_for_meal.frame)+5, 150, 45);
        bg_for_serving_type.frame = CGRectMake(-5,CGRectGetMaxY(lbl_seving_type.frame)-5, WIDTH+20, 70);
        btn_on_dine_in.frame = CGRectMake(60,10,50,50);
        img_red_tik_on_dine_in.frame = CGRectMake(CGRectGetMidX(btn_on_dine_in.frame)+5,35,25,20);
        btn_on_take_out.frame = CGRectMake(CGRectGetMaxX(btn_on_dine_in.frame)+70,10,50,50);
        img_red_tik_on_take_out.frame = CGRectMake(CGRectGetMidX(btn_on_take_out.frame)+5,35,25,20);
        btn_on_delever.frame = CGRectMake(CGRectGetMaxX( btn_on_take_out.frame)+70,10,50,50);
        img_red_tik_on_deliver.frame = CGRectMake(CGRectGetMidX(btn_on_delever.frame)+5,35,25,20);
        
        lbl_keywords.frame = CGRectMake(155,CGRectGetMaxY(bg_for_serving_type.frame)+5, 150, 45);
        bg_for_Keyword.frame = CGRectMake(-5,CGRectGetMaxY(lbl_keywords.frame)-5, WIDTH+20, 50);
        img_serch_bar.frame = CGRectMake(15,10, WIDTH-20, 30);
        txt_search.frame = CGRectMake(0,-8, 300, 45);
        icon_search.frame = CGRectMake(375,8, 15, 15);
        btn_on_search_bar.frame = CGRectMake(375,0, 30, 30);
        
        view_for_more_filters.frame=CGRectMake(0,CGRectGetMaxY(bg_for_Keyword.frame)+20,WIDTH,380);
        btn_img_more_filters.frame = CGRectMake(40,10, WIDTH-80, 45);
        table_for_items.frame  = CGRectMake(5,CGRectGetMaxY(btn_img_more_filters.frame)+5,WIDTH-10,300);
        
        view_for_food_items.frame=CGRectMake(0,CGRectGetMaxY(bg_for_Keyword.frame),WIDTH,630);
        lbl_cuisines.frame = CGRectMake(160,5, 150, 45);
        bg_for_favorits_and_all.frame = CGRectMake(0,CGRectGetMaxY(lbl_cuisines.frame), WIDTH, 50);
        lbl_my_favorites.frame = CGRectMake(55,03, 150, 45);
        img_strip1.frame = CGRectMake(55, CGRectGetMaxY(lbl_my_favorites.frame)-5, 100, 3);
        btn_my_favorites.frame = CGRectMake(0,03, 180, 45);
        
        view_for_my_favorites.frame=CGRectMake(0,CGRectGetMaxY(bg_for_favorits_and_all.frame),WIDTH,100);        bg_for_cuisines.frame = CGRectMake(0,02, WIDTH, 100);
        //   collView_for_cuisines = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,95)
        //   collectionViewLayout:layout];
        lbl_all.frame = CGRectMake(270,3, 150, 45);
        img_strip2.frame = CGRectMake(260, CGRectGetMaxY(lbl_my_favorites.frame)-5, 57, 3);
        btn_on_all.frame = CGRectMake(185,03, 180, 45);
        
        view_for_all_in_food_now.frame=CGRectMake(0,CGRectGetMaxY(bg_for_favorits_and_all.frame),WIDTH,100);
        bg_for_all_in_food_now.frame = CGRectMake(0,02, WIDTH, 100);
        //   collView_for_cuisines_in_all_food_now = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,95)
        //  collectionViewLayout:layout2];
        
        lbl_course_in_food_now.frame = CGRectMake(140,CGRectGetMaxY(view_for_my_favorites.frame), 150, 45);
        
        bg_for_course_in_food_now.frame = CGRectMake(0,CGRectGetMaxY(lbl_course_in_food_now.frame)-10, WIDTH, 65);
        //  collView_for_course_in_food_now = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,60)
        //                                        collectionViewLayout:layout3];
        lbl_Dietary_in_food_now.frame = CGRectMake(110,CGRectGetMidY(bg_for_course_in_food_now.frame)+25,250,45);
        bg_for_dietary_in_food_now.frame = CGRectMake(0,CGRectGetMaxY(lbl_Dietary_in_food_now.frame)-5, WIDTH, 85);
        //  collView_for_dietary_in_food_now = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,80)
        // collectionViewLayout:layout4];
        lbl_sortby.frame = CGRectMake(150,CGRectGetMidY(bg_for_dietary_in_food_now.frame)+35,250,45);
        rect_for_distance.frame = CGRectMake(40,CGRectGetMaxY(lbl_sortby.frame)-5, WIDTH-80, 25);
        lbl_distance.frame = CGRectMake(05,-10,250,45);
        btn_on_rect_distance.frame = CGRectMake(0,0, 295, 30);
        btn_img_filters.frame = CGRectMake(40,CGRectGetMaxY(rect_for_distance.frame)+90, WIDTH-80,45);
        table_for_distance.frame  = CGRectMake(38,CGRectGetMidY(rect_for_distance.frame)+12,295,110);

        view_serch_for_food_later.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+600);
        
        lbl_date.frame = CGRectMake(150,0, 150, 45);
        bg_for_date.frame = CGRectMake(-5,CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
        img_calender.frame = CGRectMake(110,5, 30, 30);
        txt_date .frame = CGRectMake(CGRectGetMaxX( img_calender.frame)+5,5, 200, 45);
        //   btn_set_date.frame = CGRectMake(85,5,150,35);
        
        lbl_serving_time.frame = CGRectMake(125,CGRectGetMaxY(bg_for_date.frame)+5, 150, 45);
        bg_for_time.frame = CGRectMake(-5,CGRectGetMaxY(lbl_serving_time.frame)-5, WIDTH+20, 50);
        img_clock.frame = CGRectMake(120,5, 30, 30);
        txt_time .frame = CGRectMake(CGRectGetMaxX( img_clock.frame)+5,5, 250, 45);
        btn_set_time.frame = CGRectMake(85,5,150,35);
        
        lbl_location_in_food_later.frame = CGRectMake(140,CGRectGetMaxY(bg_for_time.frame), 150, 45);
        bg_for_location2.frame = CGRectMake(-5, CGRectGetMaxY(lbl_location_in_food_later.frame)-5, WIDTH+20, 50);
        img_pointer_location2.frame = CGRectMake(25,5, 30, 30);
        lbl_set_location2.frame = CGRectMake(CGRectGetMaxX(img_pointer_location2.frame)+10,5,250, 30);
        btn_img_right2.frame = CGRectMake(CGRectGetMidX(lbl_set_location2.frame)+140,5,30,30);
        btn_on_set_location2.frame = CGRectMake(0,0,WIDTH+5,50);
        
        lbl_dish_r_meal2.frame = CGRectMake(140,CGRectGetMaxY( bg_for_location2.frame)+10, 150, 45);
        bg_for_meal2.frame = CGRectMake(-5,CGRectGetMaxY(lbl_dish_r_meal2.frame)-5, WIDTH+20, 55);
        btn_on_single_dish_in_later.frame = CGRectMake(100,5,60,50);
        img_red_tik3.frame =   CGRectMake(CGRectGetMidX( btn_on_single_dish_in_later.frame)+18,40,20,15);
        btn_on_meal2.frame = CGRectMake(CGRectGetMaxX( btn_on_single_dish_in_later.frame)+50,3,50,50);
        img_red_tik4.frame =   CGRectMake(CGRectGetMidX( btn_on_meal2.frame)+18,40,20,15);
        
        lbl_seving_type2.frame = CGRectMake(135,CGRectGetMaxY(bg_for_meal2.frame)+5, 150, 45);
        bg_for_serving_type2.frame = CGRectMake(-5,CGRectGetMaxY(lbl_seving_type2.frame)-5, WIDTH+20, 70);
        btn_foodlater_dinein_serve.frame = CGRectMake(45,10,50,55);
        img_red_tik_on_dine_in_foodlater.frame = CGRectMake(CGRectGetMidX(btn_foodlater_dinein_serve.frame)+5,35,25,20);
        btn_foodlater_takeout.frame = CGRectMake(CGRectGetMaxX(btn_on_dine_in.frame)+70,10,50,55);
        img_red_tik_on_takeout_foodlater.frame = CGRectMake(CGRectGetMidX(btn_foodlater_takeout.frame)+5,35,25,20);
        btn_foodlater_delivary.frame = CGRectMake(CGRectGetMaxX( btn_on_take_out.frame)+70,10,50,55);
        img_red_tik_on_delivary_foodlater.frame = CGRectMake(CGRectGetMidX(btn_foodlater_delivary.frame)+5,35,25,20);
        
        
        lbl_keywords2.frame = CGRectMake(155,CGRectGetMaxY(bg_for_serving_type2.frame), 150, 45);
        bg_for_Keyword2.frame =CGRectMake(-5,CGRectGetMaxY(lbl_keywords2.frame)-5, WIDTH+20, 50);
        img_serch_bar2.frame = CGRectMake(15,10, WIDTH-20, 30);
        txt_search2.frame = CGRectMake(0,-8, 300, 45);
        icon_search2.frame = CGRectMake(330,8, 15, 15);
        btn_on_search_bar2.frame = CGRectMake(330,0, 30, 30);
        
        view_for_more_filters_in_later.frame=CGRectMake(0,CGRectGetMaxY(bg_for_Keyword2.frame)+20,WIDTH,400);
        btn_img_more_filters_in_later.frame = CGRectMake(40,10, WIDTH-80, 45);
        table_for_items_in_later.frame  = CGRectMake(5,CGRectGetMaxY(btn_img_more_filters_in_later.frame)+5,WIDTH-10,300);
        
        view_for_cuisines_in_later.frame=CGRectMake(0,CGRectGetMaxY(bg_for_Keyword2.frame),WIDTH,610);
        
        lbl_cuisines_in_later.frame = CGRectMake(130,5, 150, 45);
        bg_for_favorits_and_all_in_later.frame = CGRectMake(0,CGRectGetMaxY(lbl_cuisines_in_later.frame), WIDTH, 50);
        lbl_my_favorites_in_later.frame = CGRectMake(30,03, 150, 45);
        img_strip3.frame = CGRectMake(35, CGRectGetMaxY(lbl_my_favorites_in_later.frame)-5, 100, 3);
        btn_my_favorites_in_later.frame = CGRectMake(0,03, 180, 45);
        view_for_my_favorites_in_later.frame=CGRectMake(0,CGRectGetMaxY(bg_for_favorits_and_all_in_later.frame),WIDTH,90);
        bg_for_cuisines_in_later.frame = CGRectMake(0,02, WIDTH, 100);
        //    collView_for_cuisines_in_later = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,95)
        //                                                      collectionViewLayout:layout4];
        lbl_all_in_later.frame = CGRectMake(250,3, 150, 45);
        img_strip4.frame = CGRectMake(250, CGRectGetMaxY(lbl_my_favorites.frame)-5, 57, 3);
        btn_on_all_in_later.frame = CGRectMake(185,03, 180, 45);
        view_for_all_in_food_later.frame=CGRectMake(0,CGRectGetMaxY(bg_for_favorits_and_all.frame),WIDTH,100);
        bg_for_all_in_food_later.frame = CGRectMake(0,02, WIDTH, 100);
        //     collView_for_cuisines_in_all_food_later = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,95)
        //                                                               collectionViewLayout:layout5];
        lbl_course_in_food_later.frame = CGRectMake(140,CGRectGetMaxY(view_for_my_favorites.frame), 150, 45);
        bg_for_course_in_food_later.frame = CGRectMake(0,CGRectGetMaxY(lbl_course_in_food_now.frame)-10, WIDTH, 65);
        //   collView_for_course_in_food_later = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,60)
        //                                                      collectionViewLayout:layout6];
        lbl_Dietary_in_food_later.frame = CGRectMake(110,CGRectGetMidY(bg_for_course_in_food_later.frame)+25,250,45);
        bg_for_dietary_in_food_later.frame = CGRectMake(0,CGRectGetMaxY(lbl_Dietary_in_food_later.frame)-5, WIDTH, 85);
        // collView_for_dietary_in_food_later = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,80)
        //    collectionViewLayout:layout6];
        lbl_sortby_in_later.frame = CGRectMake(150,CGRectGetMidY(bg_for_dietary_in_food_later.frame)+35,250,45);
        rect_for_distance_in_later.frame = CGRectMake(40,CGRectGetMaxY(lbl_sortby_in_later.frame)-5, WIDTH-80, 25);
        lbl_distance_in_later.frame = CGRectMake(05,-10,250,45);
        btn_on_rect_distance_in_later.frame = CGRectMake(0,0, 295, 30);
        btn_img_filters_in_later.frame = CGRectMake(40,CGRectGetMaxY(rect_for_distance_in_later.frame)+90, WIDTH-80,45);
        table_for_distance_in_later.frame  = CGRectMake(40,CGRectGetMidY(rect_for_distance_in_later.frame)+12,295,110);

        
        img_left_arrow1_food_now.frame = CGRectMake(5,40,10, 15);
        btn_on_lefet_arrow1.frame = CGRectMake(0,0, 20,100);
        img_right_arrow1_in_food_now.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame)+3,40,10, 15);
        btn_on_right_arrow1.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,100);
        img_left_arrow2_food_now.frame = CGRectMake(5,40,10, 15);
        btn_on_lefet_arrow2.frame = CGRectMake(0,0, 20,100);
        img_right_arrow2_in_food_now.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame)+3,40,10, 15);
        btn_on_right_arrow2.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,100);
        img_left_arrow3_food_now.frame = CGRectMake(5,24,10, 15);
        btn_on_lefet_arrow3.frame = CGRectMake(0,0, 20,65);
        img_right_arrow3_in_food_now.frame = CGRectMake(CGRectGetMaxX(collView_for_course_in_food_now.frame)+3,24,10, 15);
        btn_on_right_arrow3.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,65);
        img_left_arrow4_food_now.frame = CGRectMake(5,35,10, 15);
        btn_on_lefet_arrow4.frame = CGRectMake(0,0, 20,85);
        img_right_arrow4_in_food_now.frame = CGRectMake(CGRectGetMaxX(collView_for_course_in_food_now.frame)+3,35,10, 15);
        btn_on_right_arrow4.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,85);
        img_left_arrow1_food_later.frame = CGRectMake(5,40,10, 15);
        
        btn_on_lefet_arrow1_in_food_later.frame = CGRectMake(0,0, 20,100);
        img_right_arrow1_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame)+3,40,10, 15);
        btn_on_right_arrow1_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,100);
        img_left_arrow2_food_later.frame = CGRectMake(5,40,10, 15);
        btn_on_lefet_arrow2_in_later.frame = CGRectMake(0,0, 20,100);
        img_right_arrow2_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines_in_all_food_later.frame)+3,40,10, 15);
        img_left_arrow3_food_later.frame = CGRectMake(5,24,10, 15);
        btn_on_lefet_arrow3_in_later.frame = CGRectMake(0,0, 20,65);
        img_right_arrow3_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_course_in_food_later.frame)+3,24,10, 15);
        btn_on_right_arrow3_in_later.frame = CGRectMake(CGRectGetMaxX(collView_for_course_in_food_later.frame),0,20,65);
        img_left_arrow4_food_later.frame = CGRectMake(5,35,10, 15);
        btn_on_lefet_arrow4_in_later.frame = CGRectMake(0,0, 20,85);
        img_right_arrow4_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_dietary_in_food_later.frame)+3,35,10, 15);
        btn_on_right_arrow4_in_later.frame = CGRectMake(CGRectGetMaxX(collView_for_dietary_in_food_later.frame),0,20,85);
        
        
        
        
    }
    else if (IS_IPHONE_6)
    {
        
        scroll.frame = CGRectMake(0, 46, WIDTH, HEIGHT);
        
        view_serch_for_food_now.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+580);
        
        lbl_location.frame = CGRectMake(145,5, 150, 45);
        bg_for_location.frame = CGRectMake(-5, CGRectGetMaxY(lbl_location.frame)-5, WIDTH+20, 50);
        img_pointer_location.frame = CGRectMake(25,7, 30, 30);
        lbl_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+20,9, 250, 30);
        btn_img_right.frame = CGRectMake(CGRectGetMidX(lbl_set_location.frame)+140,10,30,30);
        btn_on_set_location.frame = CGRectMake(0,0,WIDTH+5,50);
        
        lbl_dish_r_meal.frame = CGRectMake(140,CGRectGetMaxY( bg_for_location.frame)+10, 150, 45);
        bg_for_meal.frame = CGRectMake(-5,CGRectGetMaxY(lbl_dish_r_meal.frame)-5, WIDTH+20, 55);
        btn_on_single_dish.frame = CGRectMake(100,5,60,50);
        btn_on_meal.frame = CGRectMake(CGRectGetMaxX( btn_on_single_dish.frame)+50,3,50,50);
        img_red_tik1.frame =  CGRectMake(CGRectGetMidX( btn_on_single_dish.frame)+18,40,20,15);
        img_red_tik2.frame =  CGRectMake(CGRectGetMidX(btn_on_meal.frame)+18,40,20,15);
        
        lbl_seving_type.frame = CGRectMake(135,CGRectGetMaxY(bg_for_meal.frame)+5, 150, 45);
        bg_for_serving_type.frame = CGRectMake(-5,CGRectGetMaxY(lbl_seving_type.frame)-5, WIDTH+20, 70);
        btn_on_dine_in.frame = CGRectMake(45,10,50,50);
        img_red_tik_on_dine_in.frame = CGRectMake(CGRectGetMidX(btn_on_dine_in.frame)+5,35,25,20);
        btn_on_take_out.frame = CGRectMake(CGRectGetMaxX(btn_on_dine_in.frame)+70,10,50,50);
        img_red_tik_on_take_out.frame = CGRectMake(CGRectGetMidX(btn_on_take_out.frame)+5,35,25,20);
        btn_on_delever.frame = CGRectMake(CGRectGetMaxX( btn_on_take_out.frame)+70,10,50,50);
        img_red_tik_on_deliver.frame = CGRectMake(CGRectGetMidX(btn_on_delever.frame)+5,35,25,20);
        
        lbl_keywords.frame = CGRectMake(155,CGRectGetMaxY(bg_for_serving_type.frame)+5, 150, 45);
        bg_for_Keyword.frame = CGRectMake(-5,CGRectGetMaxY(lbl_keywords.frame)-5, WIDTH+20, 50);
        img_serch_bar.frame = CGRectMake(15,10, WIDTH-20, 30);
        txt_search.frame = CGRectMake(0,-8, 300, 45);
        icon_search.frame = CGRectMake(330,8, 15, 15);
        btn_on_search_bar.frame = CGRectMake(330,0, 30, 30);
        
        view_for_more_filters.frame=CGRectMake(0,CGRectGetMaxY(bg_for_Keyword.frame)+20,WIDTH,380);
        btn_img_more_filters.frame = CGRectMake(40,10, WIDTH-80, 45);
        table_for_items.frame  = CGRectMake(5,CGRectGetMaxY(btn_img_more_filters.frame)+5,WIDTH-10,300);
        
        view_for_food_items.frame=CGRectMake(0,CGRectGetMaxY(bg_for_Keyword.frame),WIDTH,830);
        lbl_cuisines.frame = CGRectMake(130,5, 150, 45);
        bg_for_favorits_and_all.frame = CGRectMake(0,CGRectGetMaxY(lbl_cuisines.frame), WIDTH, 50);
        lbl_my_favorites.frame = CGRectMake(30,03, 150, 45);
        img_strip1.frame = CGRectMake(35, CGRectGetMaxY(lbl_my_favorites.frame)-5, 100, 3);
        btn_my_favorites.frame = CGRectMake(0,03, 180, 45);
        
        view_for_my_favorites.frame=CGRectMake(0,CGRectGetMaxY(bg_for_favorits_and_all.frame),WIDTH,100);
        bg_for_cuisines.frame = CGRectMake(0,02, WIDTH, 100);
        //   collView_for_cuisines = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,95)
        //   collectionViewLayout:layout];
        lbl_all.frame = CGRectMake(250,3, 150, 45);
        img_strip2.frame = CGRectMake(250, CGRectGetMaxY(lbl_my_favorites.frame)-5, 57, 3);
        btn_on_all.frame = CGRectMake(185,03, 180, 45);
        
        view_for_all_in_food_now.frame=CGRectMake(0,CGRectGetMaxY(bg_for_favorits_and_all.frame),WIDTH,100);
        bg_for_all_in_food_now.frame = CGRectMake(0,02, WIDTH, 100);
        //   collView_for_cuisines_in_all_food_now = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,95)
        //  collectionViewLayout:layout2];
        
        lbl_course_in_food_now.frame = CGRectMake(140,CGRectGetMaxY(view_for_my_favorites.frame), 150, 45);
        
        bg_for_course_in_food_now.frame = CGRectMake(0,CGRectGetMaxY(lbl_course_in_food_now.frame)-10, WIDTH, 65);
        //  collView_for_course_in_food_now = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,60)
        //                                        collectionViewLayout:layout3];
        lbl_Dietary_in_food_now.frame = CGRectMake(110,CGRectGetMidY(bg_for_course_in_food_now.frame)+25,250,45);
        bg_for_dietary_in_food_now.frame = CGRectMake(0,CGRectGetMaxY(lbl_Dietary_in_food_now.frame)-5, WIDTH, 85);
        //  collView_for_dietary_in_food_now = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,80)
        // collectionViewLayout:layout4];
        lbl_sortby.frame = CGRectMake(150,CGRectGetMidY(bg_for_dietary_in_food_now.frame)+35,250,45);
        rect_for_distance.frame = CGRectMake(40,CGRectGetMaxY(lbl_sortby.frame)-5, WIDTH-80, 25);
        lbl_distance.frame = CGRectMake(05,-10,250,45);
        btn_on_rect_distance.frame = CGRectMake(0,0, 295, 30);
        btn_img_filters.frame = CGRectMake(40,CGRectGetMaxY(rect_for_distance.frame)+90, WIDTH-80,45);
        table_for_distance.frame  = CGRectMake(40,CGRectGetMidY(rect_for_distance.frame)+12,295,110);

        view_serch_for_food_later.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+600);
        
        lbl_date.frame = CGRectMake(150,0, 150, 45);
        bg_for_date.frame = CGRectMake(-5,CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
        img_calender.frame = CGRectMake(110,5, 30, 30);
        txt_date .frame = CGRectMake(CGRectGetMaxX( img_calender.frame)+5,5, 200, 45);
        //  btn_set_date.frame = CGRectMake(85,5,150,35);
        
        lbl_serving_time.frame = CGRectMake(125,CGRectGetMaxY(bg_for_date.frame)+5, 150, 45);
        bg_for_time.frame = CGRectMake(-5,CGRectGetMaxY(lbl_serving_time.frame)-5, WIDTH+20, 50);
        img_clock.frame = CGRectMake(120,5, 30, 30);
        txt_time .frame = CGRectMake(CGRectGetMaxX( img_clock.frame)+5,5,250, 45);
        btn_set_time.frame = CGRectMake(85,5,150,35);
        
        lbl_location_in_food_later.frame = CGRectMake(140,CGRectGetMaxY(bg_for_time.frame), 150, 45);
        bg_for_location2.frame = CGRectMake(-5, CGRectGetMaxY(lbl_location_in_food_later.frame)-5, WIDTH+20, 50);
        img_pointer_location2.frame = CGRectMake(25,5, 30, 30);
        lbl_set_location2.frame = CGRectMake(CGRectGetMaxX(img_pointer_location2.frame)+10,5,250, 30);
        btn_img_right2.frame = CGRectMake(CGRectGetMidX(lbl_set_location2.frame)+140,5,30,30);
        btn_on_set_location2.frame = CGRectMake(0,0,WIDTH+5,50);
        
        lbl_dish_r_meal2.frame = CGRectMake(140,CGRectGetMaxY( bg_for_location2.frame)+10, 150, 45);
        bg_for_meal2.frame = CGRectMake(-5,CGRectGetMaxY(lbl_dish_r_meal2.frame)-5, WIDTH+20, 55);
        btn_on_single_dish_in_later.frame = CGRectMake(100,5,60,50);
        img_red_tik3.frame =   CGRectMake(CGRectGetMidX( btn_on_single_dish_in_later.frame)+18,40,20,15);
        btn_on_meal2.frame = CGRectMake(CGRectGetMaxX( btn_on_single_dish_in_later.frame)+50,3,50,50);
        img_red_tik4.frame =   CGRectMake(CGRectGetMidX( btn_on_meal2.frame)+18,40,20,15);
        
        lbl_seving_type2.frame = CGRectMake(135,CGRectGetMaxY(bg_for_meal2.frame)+5, 150, 45);
        bg_for_serving_type2.frame = CGRectMake(-5,CGRectGetMaxY(lbl_seving_type2.frame)-5, WIDTH+20, 70);
        btn_foodlater_dinein_serve.frame = CGRectMake(45,10,50,55);
        img_red_tik_on_dine_in_foodlater.frame = CGRectMake(CGRectGetMidX(btn_foodlater_dinein_serve.frame)+5,35,25,20);
        btn_foodlater_takeout.frame = CGRectMake(CGRectGetMaxX(btn_on_dine_in.frame)+70,10,50,55);
        img_red_tik_on_takeout_foodlater.frame = CGRectMake(CGRectGetMidX(btn_foodlater_takeout.frame)+5,35,25,20);
        btn_foodlater_delivary.frame = CGRectMake(CGRectGetMaxX( btn_on_take_out.frame)+70,10,50,55);
        img_red_tik_on_delivary_foodlater.frame = CGRectMake(CGRectGetMidX(btn_foodlater_delivary.frame)+5,35,25,20);
        
        
        lbl_keywords2.frame = CGRectMake(155,CGRectGetMaxY(bg_for_serving_type2.frame), 150, 45);
        bg_for_Keyword2.frame =CGRectMake(-5,CGRectGetMaxY(lbl_keywords2.frame)-5, WIDTH+20, 50);
        img_serch_bar2.frame = CGRectMake(15,10, WIDTH-20, 30);
        txt_search2.frame = CGRectMake(0,-8, 300, 45);
        icon_search2.frame = CGRectMake(330,8, 15, 15);
        btn_on_search_bar2.frame = CGRectMake(330,0, 30, 30);
        
        view_for_more_filters_in_later.frame=CGRectMake(0,CGRectGetMaxY(bg_for_Keyword2.frame)+20,WIDTH,400);
        btn_img_more_filters_in_later.frame = CGRectMake(40,10, WIDTH-80, 45);
        table_for_items_in_later.frame  = CGRectMake(5,CGRectGetMaxY(btn_img_more_filters_in_later.frame)+5,WIDTH-10,300);
        
        view_for_cuisines_in_later.frame=CGRectMake(0,CGRectGetMaxY(bg_for_Keyword2.frame),WIDTH,610);
        
        lbl_cuisines_in_later.frame = CGRectMake(130,5, 150, 45);
        bg_for_favorits_and_all_in_later.frame = CGRectMake(0,CGRectGetMaxY(lbl_cuisines_in_later.frame), WIDTH, 50);
        lbl_my_favorites_in_later.frame = CGRectMake(30,03, 150, 45);
        img_strip3.frame = CGRectMake(35, CGRectGetMaxY(lbl_my_favorites_in_later.frame)-5, 100, 3);
        btn_my_favorites_in_later.frame = CGRectMake(0,03, 180, 45);
        view_for_my_favorites_in_later.frame=CGRectMake(0,CGRectGetMaxY(bg_for_favorits_and_all_in_later.frame),WIDTH,90);
        bg_for_cuisines_in_later.frame = CGRectMake(0,02, WIDTH, 100);
        //    collView_for_cuisines_in_later = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,95)
        //                                                      collectionViewLayout:layout4];
        lbl_all_in_later.frame = CGRectMake(250,3, 150, 45);
        img_strip4.frame = CGRectMake(250, CGRectGetMaxY(lbl_my_favorites.frame)-5, 57, 3);
        btn_on_all_in_later.frame = CGRectMake(185,03, 180, 45);
        view_for_all_in_food_later.frame=CGRectMake(0,CGRectGetMaxY(bg_for_favorits_and_all.frame),WIDTH,100);
        bg_for_all_in_food_later.frame = CGRectMake(0,02, WIDTH, 100);
        //     collView_for_cuisines_in_all_food_later = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,95)
        //                                                               collectionViewLayout:layout5];
        lbl_course_in_food_later.frame = CGRectMake(140,CGRectGetMaxY(view_for_my_favorites.frame), 150, 45);
        bg_for_course_in_food_later.frame = CGRectMake(0,CGRectGetMaxY(lbl_course_in_food_now.frame)-10, WIDTH, 65);
        //   collView_for_course_in_food_later = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,60)
        //                                                      collectionViewLayout:layout6];
        lbl_Dietary_in_food_later.frame = CGRectMake(110,CGRectGetMidY(bg_for_course_in_food_later.frame)+25,250,45);
        bg_for_dietary_in_food_later.frame = CGRectMake(0,CGRectGetMaxY(lbl_Dietary_in_food_later.frame)-5, WIDTH, 85);
        // collView_for_dietary_in_food_later = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,80)
        //    collectionViewLayout:layout6];
        lbl_sortby_in_later.frame = CGRectMake(150,CGRectGetMidY(bg_for_dietary_in_food_later.frame)+35,250,45);
        rect_for_distance_in_later.frame = CGRectMake(40,CGRectGetMaxY(lbl_sortby_in_later.frame)-5, WIDTH-80, 25);
        lbl_distance_in_later.frame = CGRectMake(05,-10,250,45);
        btn_on_rect_distance_in_later.frame = CGRectMake(0,0, 295, 30);
        btn_img_filters_in_later.frame = CGRectMake(40,CGRectGetMaxY(rect_for_distance_in_later.frame)+90, WIDTH-80,45);
        table_for_distance_in_later.frame  = CGRectMake(40,CGRectGetMidY(rect_for_distance_in_later.frame)+12,295,110);

        
        img_left_arrow1_food_now.frame = CGRectMake(5,40,10, 15);
        btn_on_lefet_arrow1.frame = CGRectMake(0,0, 20,100);
        img_right_arrow1_in_food_now.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame)+3,40,10, 15);
        btn_on_right_arrow1.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,100);
        img_left_arrow2_food_now.frame = CGRectMake(5,40,10, 15);
        btn_on_lefet_arrow2.frame = CGRectMake(0,0, 20,100);
        img_right_arrow2_in_food_now.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame)+3,40,10, 15);
        btn_on_right_arrow2.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,100);
        img_left_arrow3_food_now.frame = CGRectMake(5,24,10, 15);
        btn_on_lefet_arrow3.frame = CGRectMake(0,0, 20,65);
        img_right_arrow3_in_food_now.frame = CGRectMake(CGRectGetMaxX(collView_for_course_in_food_now.frame)+3,24,10, 15);
        btn_on_right_arrow3.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,65);
        img_left_arrow4_food_now.frame = CGRectMake(5,35,10, 15);
        btn_on_lefet_arrow4.frame = CGRectMake(0,0, 20,85);
        img_right_arrow4_in_food_now.frame = CGRectMake(CGRectGetMaxX(collView_for_course_in_food_now.frame)+3,35,10, 15);
        btn_on_right_arrow4.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,85);
        img_left_arrow1_food_later.frame = CGRectMake(5,40,10, 15);
        
        btn_on_lefet_arrow1_in_food_later.frame = CGRectMake(0,0, 20,100);
        img_right_arrow1_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame)+3,40,10, 15);
        btn_on_right_arrow1_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,100);
        img_left_arrow2_food_later.frame = CGRectMake(5,40,10, 15);
        btn_on_lefet_arrow2_in_later.frame = CGRectMake(0,0, 20,100);
        img_right_arrow2_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines_in_all_food_later.frame)+3,40,10, 15);
        img_left_arrow3_food_later.frame = CGRectMake(5,24,10, 15);
        btn_on_lefet_arrow3_in_later.frame = CGRectMake(0,0, 20,65);
        img_right_arrow3_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_course_in_food_later.frame)+3,24,10, 15);
        btn_on_right_arrow3_in_later.frame = CGRectMake(CGRectGetMaxX(collView_for_course_in_food_later.frame),0,20,65);
        img_left_arrow4_food_later.frame = CGRectMake(5,35,10, 15);
        btn_on_lefet_arrow4_in_later.frame = CGRectMake(0,0, 20,85);
        img_right_arrow4_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_dietary_in_food_later.frame)+3,35,10, 15);
        btn_on_right_arrow4_in_later.frame = CGRectMake(CGRectGetMaxX(collView_for_dietary_in_food_later.frame),0,20,85);
        
        
        
    }
    else
    {
        
        scroll.frame = CGRectMake(0, 46, WIDTH, HEIGHT);
        
        view_serch_for_food_now.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+450);
        
        lbl_location.frame = CGRectMake(120,5, 150, 45);
        bg_for_location.frame = CGRectMake(-5, CGRectGetMaxY(lbl_location.frame)-5, WIDTH+20, 50);
        img_pointer_location.frame = CGRectMake(25,7, 30, 30);
        lbl_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+20,9, 250, 30);
        btn_img_right.frame = CGRectMake(CGRectGetMidX(lbl_set_location.frame)+140,10,30,30);
        btn_on_set_location.frame = CGRectMake(0,0,WIDTH+5,50);
        
        lbl_dish_r_meal.frame = CGRectMake(120,CGRectGetMaxY( bg_for_location.frame)+10, 150, 45);
        bg_for_meal.frame = CGRectMake(-5,CGRectGetMaxY(lbl_dish_r_meal.frame)-5, WIDTH+20, 55);
        btn_on_single_dish.frame = CGRectMake(77,5,60,50);
        img_red_tik1.frame =  CGRectMake(CGRectGetMidX( btn_on_single_dish.frame)+30,40,20,15);
        btn_on_meal.frame = CGRectMake(CGRectGetMaxX( btn_on_single_dish.frame)+50,3,50,50);
        img_red_tik2.frame =  CGRectMake(CGRectGetMidX(btn_on_meal.frame)+18,40,20,15);
        
        lbl_seving_type.frame = CGRectMake(120,CGRectGetMaxY(bg_for_meal.frame)+5, 150, 45);
        bg_for_serving_type.frame = CGRectMake(-5,CGRectGetMaxY(lbl_seving_type.frame)-5, WIDTH+20, 70);
        btn_on_dine_in.frame = CGRectMake(35,10,50,50);
        img_red_tik_on_dine_in.frame = CGRectMake(CGRectGetMidX(btn_on_dine_in.frame)+5,35,25,20);
        btn_on_take_out.frame = CGRectMake(CGRectGetMaxX(btn_on_dine_in.frame)+60,10,50,50);
        img_red_tik_on_take_out.frame = CGRectMake(CGRectGetMidX(btn_on_take_out.frame)+5,35,25,20);
        btn_on_delever.frame = CGRectMake(CGRectGetMaxX( btn_on_take_out.frame)+60,10,50,50);
        img_red_tik_on_deliver.frame = CGRectMake(CGRectGetMidX(btn_on_delever.frame)+5,35,25,20);
        
        lbl_keywords.frame = CGRectMake(120,CGRectGetMaxY(bg_for_serving_type.frame)+5, 150, 45);
        bg_for_Keyword.frame = CGRectMake(-5,CGRectGetMaxY(lbl_keywords.frame)-5, WIDTH+20, 50);
        img_serch_bar.frame = CGRectMake(15,10, WIDTH-20, 30);
        txt_search.frame = CGRectMake(0,-8, 300, 45);
        icon_search.frame = CGRectMake(270,8, 15, 15);
        btn_on_search_bar.frame = CGRectMake(270,0, 30, 30);
        
        view_for_more_filters.frame=CGRectMake(0,CGRectGetMaxY(bg_for_Keyword.frame)+20,WIDTH,380);
        btn_img_more_filters.frame = CGRectMake(40,10, WIDTH-80, 45);
        table_for_items.frame  = CGRectMake(5,CGRectGetMaxY(btn_img_more_filters.frame)+5,WIDTH-10,300);
        
        view_for_food_items.frame=CGRectMake(0,CGRectGetMaxY(bg_for_Keyword.frame),WIDTH,850);
        lbl_cuisines.frame = CGRectMake(130,5, 150, 45);
        bg_for_favorits_and_all.frame = CGRectMake(0,CGRectGetMaxY(lbl_cuisines.frame), WIDTH, 50);
        lbl_my_favorites.frame = CGRectMake(30,03, 150, 45);
        img_strip1.frame = CGRectMake(35, CGRectGetMaxY(lbl_my_favorites.frame)-5, 100, 3);
        btn_my_favorites.frame = CGRectMake(0,03, 180, 45);
        
        view_for_my_favorites.frame=CGRectMake(0,CGRectGetMaxY(bg_for_favorits_and_all.frame),WIDTH,100);
        bg_for_cuisines.frame = CGRectMake(0,02, WIDTH, 100);
        //   collView_for_cuisines = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,95)
        //   collectionViewLayout:layout];
        lbl_all.frame = CGRectMake(250,3, 150, 45);
        img_strip2.frame = CGRectMake(250, CGRectGetMaxY(lbl_my_favorites.frame)-5, 57, 3);
        btn_on_all.frame = CGRectMake(185,03, 180, 45);
        
        view_for_all_in_food_now.frame=CGRectMake(0,CGRectGetMaxY(bg_for_favorits_and_all.frame),WIDTH,100);
        bg_for_all_in_food_now.frame = CGRectMake(0,02, WIDTH, 100);
        //   collView_for_cuisines_in_all_food_now = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,95)
        //  collectionViewLayout:layout2];
        
        lbl_course_in_food_now.frame = CGRectMake(140,CGRectGetMaxY(view_for_my_favorites.frame), 150, 45);
        
        bg_for_course_in_food_now.frame = CGRectMake(0,CGRectGetMaxY(lbl_course_in_food_now.frame)-5, WIDTH, 65);
        //  collView_for_course_in_food_now = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,60)
        //                                        collectionViewLayout:layout3];
        lbl_Dietary_in_food_now.frame = CGRectMake(85,CGRectGetMidY(bg_for_course_in_food_now.frame)+25,250,45);
        bg_for_dietary_in_food_now.frame = CGRectMake(0,CGRectGetMaxY(lbl_Dietary_in_food_now.frame)-5, WIDTH, 85);
        //  collView_for_dietary_in_food_now = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,80)
        // collectionViewLayout:layout4];
        lbl_sortby.frame = CGRectMake(150,CGRectGetMidY(bg_for_dietary_in_food_now.frame)+35,250,45);
        rect_for_distance.frame = CGRectMake(40,CGRectGetMaxY(lbl_sortby.frame)-5, WIDTH-80, 25);
        lbl_distance.frame = CGRectMake(05,-10,250,45);
        btn_on_rect_distance.frame = CGRectMake(0,0, 250, 30);
        btn_img_filters.frame = CGRectMake(40,CGRectGetMaxY(rect_for_distance.frame)+90, WIDTH-80,45);
        table_for_distance.frame  = CGRectMake(40,CGRectGetMidY(rect_for_distance.frame)+12,240,110);

        table_for_itemsfilter.frame  = CGRectMake(5,CGRectGetMaxY(btn_img_filters.frame)+5,WIDTH-10,210);

        view_serch_for_food_later.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+600);
        
        lbl_date.frame = CGRectMake(120,0, 150, 45);
        bg_for_date.frame = CGRectMake(-5,CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
        img_calender.frame = CGRectMake(85,5, 30, 30);
        txt_date .frame = CGRectMake(CGRectGetMaxX( img_calender.frame)+5,5,180, 40);
        //    btn_set_date.frame = CGRectMake(85,5,150,35);
        
        lbl_serving_time.frame = CGRectMake(105,CGRectGetMaxY(bg_for_date.frame)+5, 150, 45);
        bg_for_time.frame = CGRectMake(-5,CGRectGetMaxY(lbl_serving_time.frame)-5, WIDTH+20, 50);
        img_clock.frame = CGRectMake(120,5, 30, 30);
        txt_time .frame = CGRectMake(CGRectGetMaxX( img_clock.frame)+5,5, 200, 40);
        btn_set_time.frame = CGRectMake(85,5,150,35);
        
        lbl_location_in_food_later.frame = CGRectMake(120,CGRectGetMaxY(bg_for_time.frame), 150, 45);
        bg_for_location2.frame = CGRectMake(-5, CGRectGetMaxY(lbl_location_in_food_later.frame)-5, WIDTH+20, 50);
        img_pointer_location2.frame = CGRectMake(25,5, 30, 30);
        lbl_set_location2.frame = CGRectMake(CGRectGetMaxX(img_pointer_location2.frame)+10,5,250, 30);
        btn_img_right2.frame = CGRectMake(CGRectGetMidX(lbl_set_location2.frame)+140,5,30,30);
        btn_on_set_location2.frame = CGRectMake(0,0,WIDTH+5,50);
        
        lbl_dish_r_meal2.frame = CGRectMake(120,CGRectGetMaxY( bg_for_location2.frame)+10, 150, 45);
        bg_for_meal2.frame = CGRectMake(-5,CGRectGetMaxY(lbl_dish_r_meal2.frame)-5, WIDTH+20, 55);
        btn_on_single_dish_in_later.frame = CGRectMake(77,5,60,50);
        img_red_tik3.frame =   CGRectMake(CGRectGetMidX( btn_on_single_dish_in_later.frame)+18,40,20,15);
        btn_on_meal2.frame = CGRectMake(CGRectGetMaxX( btn_on_single_dish_in_later.frame)+50,3,50,50);
        img_red_tik4.frame =   CGRectMake(CGRectGetMidX( btn_on_meal2.frame)+18,40,20,15);
        
        lbl_seving_type2.frame = CGRectMake(105,CGRectGetMaxY(bg_for_meal2.frame)+5, 150, 45);
        bg_for_serving_type2.frame = CGRectMake(-5,CGRectGetMaxY(lbl_seving_type2.frame)-5, WIDTH+20, 70);
        btn_foodlater_dinein_serve.frame = CGRectMake(45,10,50,50);
        img_red_tik_on_dine_in_foodlater.frame = CGRectMake(CGRectGetMidX(btn_foodlater_dinein_serve.frame)+5,35,25,20);
        btn_foodlater_takeout.frame = CGRectMake(CGRectGetMaxX(btn_on_dine_in.frame)+70,10,50,50);
        img_red_tik_on_takeout_foodlater.frame = CGRectMake(CGRectGetMidX(btn_foodlater_takeout.frame)+5,35,25,20);
        btn_foodlater_delivary.frame = CGRectMake(CGRectGetMaxX( btn_on_take_out.frame)+70,10,50,50);
        img_red_tik_on_delivary_foodlater.frame = CGRectMake(CGRectGetMidX(btn_foodlater_delivary.frame)+5,35,25,20);
        
        
        lbl_keywords2.frame = CGRectMake(125,CGRectGetMaxY(bg_for_serving_type2.frame), 150, 45);
        bg_for_Keyword2.frame =CGRectMake(-5,CGRectGetMaxY(lbl_keywords2.frame)-5, WIDTH+20, 50);
        img_serch_bar2.frame = CGRectMake(15,10, WIDTH-20, 30);
        txt_search2.frame = CGRectMake(0,-8, 300, 45);
        icon_search2.frame = CGRectMake(280,8, 15, 15);
        btn_on_search_bar2.frame = CGRectMake(280,0, 30, 30);
        
        view_for_more_filters_in_later.frame=CGRectMake(0,CGRectGetMaxY(bg_for_Keyword2.frame)+20,WIDTH,400);
        btn_img_more_filters_in_later.frame = CGRectMake(40,10, WIDTH-80, 45);
        table_for_items_in_later.frame  = CGRectMake(5,CGRectGetMaxY(btn_img_more_filters_in_later.frame)+5,WIDTH-10,300);
        
        view_for_cuisines_in_later.frame=CGRectMake(0,CGRectGetMaxY(bg_for_Keyword2.frame),WIDTH,610);
        
        lbl_cuisines_in_later.frame = CGRectMake(130,5, 150, 45);
        bg_for_favorits_and_all_in_later.frame = CGRectMake(0,CGRectGetMaxY(lbl_cuisines_in_later.frame), WIDTH, 50);
        lbl_my_favorites_in_later.frame = CGRectMake(30,03, 150, 45);
        img_strip3.frame = CGRectMake(35, CGRectGetMaxY(lbl_my_favorites_in_later.frame)-5, 100, 3);
        btn_my_favorites_in_later.frame = CGRectMake(0,03, 180, 45);
        view_for_my_favorites_in_later.frame=CGRectMake(0,CGRectGetMaxY(bg_for_favorits_and_all_in_later.frame),WIDTH,90);
        bg_for_cuisines_in_later.frame = CGRectMake(0,02, WIDTH, 100);
        //    collView_for_cuisines_in_later = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,95)
        //                                                      collectionViewLayout:layout4];
        lbl_all_in_later.frame = CGRectMake(250,3, 150, 45);
        img_strip4.frame = CGRectMake(250, CGRectGetMaxY(lbl_my_favorites.frame)-5, 57, 3);
        btn_on_all_in_later.frame = CGRectMake(185,03, 180, 45);
        view_for_all_in_food_later.frame=CGRectMake(0,CGRectGetMaxY(bg_for_favorits_and_all.frame),WIDTH,100);
        bg_for_all_in_food_later.frame = CGRectMake(0,02, WIDTH, 100);
        //     collView_for_cuisines_in_all_food_later = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,95)
        //                                                               collectionViewLayout:layout5];
        lbl_course_in_food_later.frame = CGRectMake(140,CGRectGetMaxY(view_for_my_favorites.frame), 150, 45);
        bg_for_course_in_food_later.frame = CGRectMake(0,CGRectGetMaxY(lbl_course_in_food_now.frame)-5, WIDTH, 65);
        //   collView_for_course_in_food_later = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,60)
        //                                                      collectionViewLayout:layout6];
        lbl_Dietary_in_food_later.frame = CGRectMake(85,CGRectGetMidY(bg_for_course_in_food_later.frame)+25,250,45);
        bg_for_dietary_in_food_later.frame = CGRectMake(0,CGRectGetMaxY(lbl_Dietary_in_food_later.frame)-5, WIDTH, 85);
        // collView_for_dietary_in_food_later = [[UICollectionView alloc] initWithFrame:CGRectMake(20,0,WIDTH-40,80)
        //    collectionViewLayout:layout6];
        lbl_sortby_in_later.frame = CGRectMake(150,CGRectGetMidY(bg_for_dietary_in_food_later.frame)+35,250,45);
        rect_for_distance_in_later.frame = CGRectMake(40,CGRectGetMaxY(lbl_sortby_in_later.frame)-5, WIDTH-80, 25);
        lbl_distance_in_later.frame = CGRectMake(05,-10,250,45);
        btn_on_rect_distance_in_later.frame = CGRectMake(0,0, 295, 30);
        table_for_distance_in_later.frame  = CGRectMake(40,CGRectGetMidY(rect_for_distance_in_later.frame)+12,240,110);
        btn_img_filters_in_later.frame = CGRectMake(40,CGRectGetMaxY(rect_for_distance_in_later.frame)+90, WIDTH-80,45);
        table_for_itemsfilterlater.frame  = CGRectMake(5,CGRectGetMaxY(btn_img_filters_in_later.frame)+15,WIDTH-10,210);

        
        img_left_arrow1_food_now.frame = CGRectMake(5,40,10, 15);
        btn_on_lefet_arrow1.frame = CGRectMake(0,0, 20,100);
        img_right_arrow1_in_food_now.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame)+3,40,10, 15);
        btn_on_right_arrow1.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,100);
        img_left_arrow2_food_now.frame = CGRectMake(5,40,10, 15);
        btn_on_lefet_arrow2.frame = CGRectMake(0,0, 20,100);
        img_right_arrow2_in_food_now.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame)+3,40,10, 15);
        btn_on_right_arrow2.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,100);
        img_left_arrow3_food_now.frame = CGRectMake(5,24,10, 15);
        btn_on_lefet_arrow3.frame = CGRectMake(0,0, 20,65);
        img_right_arrow3_in_food_now.frame = CGRectMake(CGRectGetMaxX(collView_for_course_in_food_now.frame)+3,24,10, 15);
        btn_on_right_arrow3.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,65);
        img_left_arrow4_food_now.frame = CGRectMake(5,35,10, 15);
        btn_on_lefet_arrow4.frame = CGRectMake(0,0, 20,85);
        img_right_arrow4_in_food_now.frame = CGRectMake(CGRectGetMaxX(collView_for_course_in_food_now.frame)+3,35,10, 15);
        btn_on_right_arrow4.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,85);
        img_left_arrow1_food_later.frame = CGRectMake(5,40,10, 15);
        
        btn_on_lefet_arrow1_in_food_later.frame = CGRectMake(0,0, 20,100);
        img_right_arrow1_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame)+3,40,10, 15);
        btn_on_right_arrow1_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines.frame),0,20,100);
        img_left_arrow2_food_later.frame = CGRectMake(5,40,10, 15);
        btn_on_lefet_arrow2_in_later.frame = CGRectMake(0,0, 20,100);
        img_right_arrow2_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_cuisines_in_all_food_later.frame)+3,40,10, 15);
        img_left_arrow3_food_later.frame = CGRectMake(5,24,10, 15);
        btn_on_lefet_arrow3_in_later.frame = CGRectMake(0,0, 20,65);
        img_right_arrow3_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_course_in_food_later.frame)+3,24,10, 15);
        btn_on_right_arrow3_in_later.frame = CGRectMake(CGRectGetMaxX(collView_for_course_in_food_later.frame),0,20,65);
        img_left_arrow4_food_later.frame = CGRectMake(5,35,10, 15);
        btn_on_lefet_arrow4_in_later.frame = CGRectMake(0,0, 20,85);
        img_right_arrow4_in_food_later.frame = CGRectMake(CGRectGetMaxX(collView_for_dietary_in_food_later.frame)+3,35,10, 15);
        btn_on_right_arrow4_in_later.frame = CGRectMake(CGRectGetMaxX(collView_for_dietary_in_food_later.frame),0,20,85);
        
        
        
    }
    
    if (IS_IPHONE_6Plus)
    {
        [scroll setContentSize:CGSizeMake(0,1450)];
        
    }
    else if (IS_IPHONE_6)
    {
        [scroll setContentSize:CGSizeMake(0,1450)];
        
    }
    else
    {
        [scroll setContentSize:CGSizeMake(0,1650)];
        
    }
    
}

#pragma tableview delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == table_on_drop_down)
    {
        return 2;
    }
    
    else if (tableView == table_for_items)
    {
        return [ary_foodnowlist count];
        
    }
    else if (tableView == table_for_itemsfilter)
    {
        return [ary_foodnowlist count];
    }
    else if (tableView == table_for_distance)
    {
        return 3;
    }
    
    else if (tableView == table_for_items_in_later)
    {
        return [ary_foodnowlist count];
        
    }
    else if (tableView == table_for_distance_in_later)
    {
        return 3;
    }
    else if (tableView == table_for_itemsfilterlater)
    {
        return [ary_foodnowlist count];
    }
    
    return 0;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table_on_drop_down)
    {
        return 40;
    }
    else if (tableView == table_for_itemsfilter)
    {
        return 180;
    }
    else if (tableView == table_for_items)
    {
        
        return 180;
        
    }
    else if (tableView == table_for_distance)
    {
        return 40;
    }
    
    else if (tableView == table_for_items_in_later)
    {
        return 180;
    }
    else if (tableView == table_for_distance_in_later)
    {
        return 40;
    }
    else if (tableView == table_for_itemsfilterlater)
    {
        return 180;
    }
    
    return 0;
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    if (tableView == table_on_drop_down)
    {
        img_bg_for_first_tbl = [[UIImageView alloc]init];
        img_bg_for_first_tbl.frame =  CGRectMake(0,5, 150, 40);
        img_bg_for_first_tbl.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_bg_for_first_tbl setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_first_tbl];
        
        lbl_food_now_r_later = [[UILabel alloc]init];
        lbl_food_now_r_later .frame = CGRectMake(5,10,200, 15);
        lbl_food_now_r_later .text = [NSString stringWithFormat:@"%@",[ array_head_names objectAtIndex:indexPath.row]];
        lbl_food_now_r_later .font = [UIFont fontWithName:kFontBold size:15];
        lbl_food_now_r_later .textColor = [UIColor blackColor];
        lbl_food_now_r_later .backgroundColor = [UIColor clearColor];
        [img_bg_for_first_tbl addSubview: lbl_food_now_r_later ];
        
    }
    else if (tableView == table_for_items)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH+5, 170);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-5, 170);
            
        }
        else
        {    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-10,170);
            
        }
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        UIImageView *img_dish = [[UIImageView alloc] init];
        //    img_dish.frame = CGRectMake(7,7, 90,  95 );
        
        NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"dishimage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [img_dish setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@""]];
        
        //[img_dish setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_img objectAtIndex:indexPath.row]]]];
        [img_cellBackGnd addSubview:img_dish];
        
        UIButton *btn_favorite = [UIButton buttonWithType:UIButtonTypeCustom];
        //  btn_favorite.frame = CGRectMake(5,58, 35, 35);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [btn_favorite addTarget:self action:@selector(btn_favorite_click:) forControlEvents:UIControlEventTouchUpInside];
        [btn_favorite setImage:[UIImage imageNamed:@"favorite-icon@2x.png"] forState:UIControlStateNormal];
        [img_dish   addSubview:btn_favorite];
        
        UIButton *btn_number_three = [UIButton buttonWithType:UIButtonTypeCustom];
        //   btn_number_three.frame = CGRectMake(58,-5, 35, 35);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [btn_number_three addTarget:self action:@selector(btn_number_three_click:) forControlEvents:UIControlEventTouchUpInside];
        [btn_number_three setImage:[UIImage imageNamed:@"red3-icon@2x.png"] forState:UIControlStateNormal];
        [img_dish   addSubview:btn_number_three];
        
        
        UIButton *icon_delete = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_delete.frame = CGRectMake(WIDTH-70,20, 40, 40);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_delete addTarget:self action:@selector(btn_add_to_cart_click:) forControlEvents:UIControlEventTouchUpInside];
        [icon_delete setImage:[UIImage imageNamed:@"img-cart@2x.png"] forState:UIControlStateNormal];
        [cell.contentView   addSubview:icon_delete];
        
        UILabel *dish_name = [[UILabel alloc]init];
        //   dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
        dish_name.text = [NSString stringWithFormat:@"%@",[[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"dishname"]];
        dish_name.font = [UIFont fontWithName:kFontBold size:6];
        dish_name.textColor = [UIColor blackColor];
        dish_name.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:dish_name];
        
        
        UIImageView *icon_location = [[UIImageView alloc] init];
        //    icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        [icon_location setImage:[UIImage imageNamed:@"location-icon@2x.png"]];
        [img_cellBackGnd  addSubview:icon_location];
        
        UILabel *meters = [[UILabel alloc]init];
        //  meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        meters.text = [[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"distance"];
        meters.font = [UIFont fontWithName:kFont size:15];
        meters.textColor = [UIColor blackColor];
        meters.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:meters];
        
        
        
//        UIButton *icon_server = [UIButton buttonWithType:UIButtonTypeCustom];
//        //   icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
//        //icon_server .backgroundColor = [UIColor clearColor];
//        [icon_server  addTarget:self action:@selector(click_on_icon_sever_btn:) forControlEvents:UIControlEventTouchUpInside];
//        [icon_server setImage:[UIImage imageNamed:@"serving-icon@2x.png"] forState:UIControlStateNormal];
//        [img_cellBackGnd   addSubview:icon_server];
//        
//        UIButton *icon_halal = [UIButton buttonWithType:UIButtonTypeCustom];
//        //   icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
//        //icon_halal .backgroundColor = [UIColor clearColor];
//        [icon_halal addTarget:self action:@selector(click_on_icon_halal_btn:) forControlEvents:UIControlEventTouchUpInside];
//        [icon_halal setImage:[UIImage imageNamed:@"halal-icon@2x.png"] forState:UIControlStateNormal];
//        [img_cellBackGnd   addSubview:icon_halal];
//        
//        UIImageView *img_non_veg = [[UIImageView alloc] init];
//        //  img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
//        [img_non_veg setImage:[UIImage imageNamed:@"non-veg bt@2x.png"]];
//        [img_cellBackGnd addSubview:img_non_veg];
//        
//        UIButton *icon_cow = [UIButton buttonWithType:UIButtonTypeCustom];
//        //  icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
//        //icon_cow .backgroundColor = [UIColor clearColor];
//        [icon_cow addTarget:self action:@selector(click_on_icon_cow_btn:) forControlEvents:UIControlEventTouchUpInside];
//        [icon_cow setImage:[UIImage imageNamed:@"cow-icon@2x.png"] forState:UIControlStateNormal];
//        [img_cellBackGnd   addSubview:icon_cow];
//        
//        UIButton *icon_fronce = [UIButton buttonWithType:UIButtonTypeCustom];
//        //   icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
//        //icon_cow .backgroundColor = [UIColor clearColor];
//        [icon_fronce addTarget:self action:@selector(click_on_fronce_btn:) forControlEvents:UIControlEventTouchUpInside];
//        [icon_fronce setImage:[UIImage imageNamed:@"fronce-icon@2x.png"] forState:UIControlStateNormal];
//        [img_cellBackGnd   addSubview:icon_fronce];
        
        UILabel *doller_rate = [[UILabel alloc]init];
        //   doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
        doller_rate.text = [NSString stringWithFormat:@"$%@",[[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"dishprice"]];
        doller_rate.font = [UIFont fontWithName:kFontBold size:16];
        doller_rate.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        doller_rate.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:doller_rate];
        
        UIImageView *img_line = [[UIImageView alloc] init];
        //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
        [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
        [img_cellBackGnd addSubview:img_line];
        
        
        UIButton *icon_take = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        //icon_take .backgroundColor = [UIColor clearColor];
        [icon_take addTarget:self action:@selector(click_on_take_out_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_take setImage:[UIImage imageNamed:@"take-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_take];
        
        UIButton *icon_deliver = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //icon_deliver .backgroundColor = [UIColor clearColor];
        [icon_deliver addTarget:self action:@selector(click_on_icon_delivery_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_deliver setImage:[UIImage imageNamed:@"deliver-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_deliver];
        
        
        UIButton *img_btn_seving_Now = [UIButton buttonWithType:UIButtonTypeCustom];
        //   img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //img_btn_deliver_Now .backgroundColor = [UIColor clearColor];
        [img_btn_seving_Now addTarget:self action:@selector(click_on_seving_now_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_seving_Now setImage:[UIImage imageNamed:@"now-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_seving_Now];
        
        
        UIButton *img_btn_chef_menu = [UIButton buttonWithType:UIButtonTypeCustom];
        //    img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
        [img_btn_chef_menu addTarget:self action:@selector(click_on_icon_chef_menu_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_chef_menu setImage:[UIImage imageNamed:@"chef-menu-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_chef_menu];
        
        
        
        UIButton *icon_thumb = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+5, 20, 20);
        //icon_thumb .backgroundColor = [UIColor clearColor];
        [icon_thumb addTarget:self action:@selector(click_on_icon_thumb_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_thumb setImage:[UIImage imageNamed:@"thumb-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_thumb];
        
        UILabel *likes = [[UILabel alloc]init];
        //  likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
        likes.text = [[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"likes"];
        likes.font = [UIFont fontWithName:kFont size:10];
        likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        likes.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:likes];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            btn_number_three.frame = CGRectMake(58,-5, 35, 35);
            
            icon_delete.frame = CGRectMake(WIDTH-70,20, 40, 40);
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 19);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
//            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
//            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
//            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
//            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
//            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+4, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+9,128,100, 10);
            
            dish_name.font = [UIFont fontWithName:kFontBold size:15];
            
        }
        else if (IS_IPHONE_6)
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            btn_number_three.frame = CGRectMake(58,-5, 35, 35);
            
            icon_delete.frame = CGRectMake(WIDTH-62,20, 40, 40);
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 19);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
//            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
//            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
//            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
//            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
//            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(300,CGRectGetMaxY(img_line.frame)+5, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
            
            
            dish_name.font = [UIFont fontWithName:kFontBold size:15];
            
        }
        else
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            btn_number_three.frame = CGRectMake(58,-5, 35, 35);
            
            icon_delete.frame = CGRectMake(WIDTH-65,20, 40, 40);
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 16);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
//            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
//            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
//            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
//            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
//            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(IS_IPHONE_5?WIDTH-75:WIDTH-80,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(IS_IPHONE_5?240:230,CGRectGetMaxY(img_line.frame)+5, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
            
            
            dish_name.font = [UIFont fontWithName:kFontBold size:IS_IPHONE_5?13:13];
        }
        
        
        UIScrollView*delivery_Scroll;
        delivery_Scroll = [[UIScrollView alloc]init];
        
        if (IS_IPHONE_6Plus)
        {
            delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(icon_location.frame)+5,150,40);
        }
        else  if (IS_IPHONE_6)
        {
            
            delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(icon_location.frame)+5,150,40);
        }
        else  if (IS_IPHONE_5)
        {
            
            delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(icon_location.frame)+5,150,40);
        }
        else
        {
            delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(icon_location.frame)+5,150,40);
        }
        
        delivery_Scroll.backgroundColor = [UIColor whiteColor];
        delivery_Scroll.bounces=YES;
        //delivery_Scroll.layer.borderWidth = 1.0;
        delivery_Scroll.layer.borderColor = [UIColor blackColor].CGColor;
        delivery_Scroll.showsVerticalScrollIndicator = NO;
        delivery_Scroll.showsHorizontalScrollIndicator = NO;
        
        //delivery_Scroll.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [delivery_Scroll setScrollEnabled:YES];
        delivery_Scroll.userInteractionEnabled = YES;
        [img_cellBackGnd addSubview:delivery_Scroll];
        //[(NSDictionary *) [[ary_maintosave  objectAtIndex:0] valueForKey:@"Delivery_Address"] count]
        
        int totalPage;
        
        totalPage = (int)[[[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"field_dish_dietary_restrictions"] count];
        [delivery_Scroll setContentSize:CGSizeMake(50*totalPage, delivery_Scroll.frame.size.height)];
        
        
        
        for (int j = 0; j<totalPage; j++)
        {
            
            
            UIImageView *img_non_veg = [[UIImageView alloc] init];
            img_non_veg.frame = CGRectMake(50*j,10, 25, 25);
            
            //        NSString *ImagePath1 =[[NSString stringWithFormat:@"%@",[[[[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"DishRestrictions"] objectAtIndex:j] valueForKey:@"resImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            //        [img_non_veg setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@""]];
            
            NSString *url_Img = [NSString stringWithFormat: @"%@", [[[[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"field_dish_dietary_restrictions"] objectAtIndex:j] valueForKey:@"resImage"]];
            img_non_veg.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img]]];
            
            //    NSString *ImagePath1 =  [[NSString stringWithFormat:@"%@",[[array_items_name objectAtIndex:indexPath.row] valueForKey:@"resImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            //    [img_non_veg setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@"img_merchantPlaceholder@2x.png"]];
            img_non_veg.backgroundColor=[UIColor clearColor];
            [delivery_Scroll addSubview:img_non_veg];
            
            
            //        UIButton*get_Deliver_Address_Btn;
            //        get_Deliver_Address_Btn = [[UIButton alloc]init];
            //        get_Deliver_Address_Btn.frame = CGRectMake(0,25*j, delivery_Scroll.frame.size.width, 25);
            //        [get_Deliver_Address_Btn addTarget:self action:@selector(get_Deliver_Address_BtnMethod:) forControlEvents:UIControlEventTouchUpInside];
            //        get_Deliver_Address_Btn.tag = j;
            //        get_Deliver_Address_Btn.titleLabel.text = [NSString stringWithFormat:@"%d",indexPath.row];
            //        [get_Deliver_Address_Btn setBackgroundColor:[UIColor clearColor]];
            //        //   get_Deliver_Address_Btn.layer.borderWidth = 1.0;
            //        [delivery_Scroll addSubview: get_Deliver_Address_Btn];
        }

    }
    
    else if (tableView == table_for_distance)
    {
        
        img_bg_for_distance = [[UIImageView alloc]init];
        img_bg_for_distance.frame =  CGRectMake(0,0, 295, 40);
        img_bg_for_distance.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_bg_for_distance setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_distance];
        
        UILabel *distance_names_in_later = [[UILabel alloc]init];
        distance_names_in_later.frame = CGRectMake(5,10,200,15);
        distance_names_in_later.text = [NSString stringWithFormat:@"%@",[array_names_in_distance objectAtIndex:indexPath.row]];
        distance_names_in_later.font = [UIFont fontWithName:kFontBold size:12];
        distance_names_in_later.textColor = [UIColor blackColor];
        distance_names_in_later.backgroundColor = [UIColor clearColor];
        [img_bg_for_distance addSubview:distance_names_in_later];
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_distance.frame =  CGRectMake(0,0, 295, 40);
            distance_names_in_later.frame = CGRectMake(5,10,200,15);
        }
        else if (IS_IPHONE_6)
        {
            img_bg_for_distance.frame =  CGRectMake(0,0, 295, 40);
            distance_names_in_later.frame = CGRectMake(5,10,200,15);
        }
        else
        {
            img_bg_for_distance.frame =  CGRectMake(0,0, 250, 40);
            distance_names_in_later.frame = CGRectMake(5,10,200,15);
        }
        
    }
    
    
    
    else if (tableView == table_for_items_in_later)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH+5, 170);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-5, 170);
            
        }
        else
        {    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-10,170);
            
        }
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        UIImageView *img_dish = [[UIImageView alloc] init];
        //    img_dish.frame = CGRectMake(7,7, 90,  95 );
        NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"dishimage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [img_dish setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@""]];
        [img_cellBackGnd addSubview:img_dish];
        
        UIButton *btn_favorite = [UIButton buttonWithType:UIButtonTypeCustom];
        //  btn_favorite.frame = CGRectMake(5,58, 35, 35);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [btn_favorite addTarget:self action:@selector(btn_favorite_click:) forControlEvents:UIControlEventTouchUpInside];
        [btn_favorite setImage:[UIImage imageNamed:@"favorite-icon@2x.png"] forState:UIControlStateNormal];
        [img_dish   addSubview:btn_favorite];
        
        UIButton *btn_number_three = [UIButton buttonWithType:UIButtonTypeCustom];
        //   btn_number_three.frame = CGRectMake(58,-5, 35, 35);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [btn_number_three addTarget:self action:@selector(btn_number_three_click:) forControlEvents:UIControlEventTouchUpInside];
        [btn_number_three setImage:[UIImage imageNamed:@"red3-icon@2x.png"] forState:UIControlStateNormal];
        [img_dish   addSubview:btn_number_three];
        
        
        UIButton *icon_delete = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_delete.frame = CGRectMake(WIDTH-70,20, 40, 40);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_delete addTarget:self action:@selector(btn_add_to_cart_click:) forControlEvents:UIControlEventTouchUpInside];
        [icon_delete setImage:[UIImage imageNamed:@"img-cart@2x.png"] forState:UIControlStateNormal];
        [cell.contentView   addSubview:icon_delete];
        
        UILabel *dish_name = [[UILabel alloc]init];
        //   dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
        //dish_name.text = [NSString stringWithFormat:@"%@",[ary_displaynames objectAtIndex:indexPath.row]];
        dish_name.font = [UIFont fontWithName:kFontBold size:8];
        dish_name.textColor = [UIColor blackColor];
        dish_name.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:dish_name];
        
        
        UIImageView *icon_location = [[UIImageView alloc] init];
        //    icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        [icon_location setImage:[UIImage imageNamed:@"location-icon@2x.png"]];
        [img_cellBackGnd  addSubview:icon_location];
        
        UILabel *meters = [[UILabel alloc]init];
        //  meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        // meters.text = @"5km";
        meters.font = [UIFont fontWithName:kFont size:15];
        meters.textColor = [UIColor blackColor];
        meters.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:meters];
        
//        UIButton *icon_server = [UIButton buttonWithType:UIButtonTypeCustom];
//        //   icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
//        //icon_server .backgroundColor = [UIColor clearColor];
//        [icon_server  addTarget:self action:@selector(click_on_icon_sever_btn:) forControlEvents:UIControlEventTouchUpInside];
//        [icon_server setImage:[UIImage imageNamed:@"serving-icon@2x.png"] forState:UIControlStateNormal];
//        [img_cellBackGnd   addSubview:icon_server];
//        
//        UIButton *icon_halal = [UIButton buttonWithType:UIButtonTypeCustom];
//        //   icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
//        //icon_halal .backgroundColor = [UIColor clearColor];
//        [icon_halal addTarget:self action:@selector(click_on_icon_halal_btn:) forControlEvents:UIControlEventTouchUpInside];
//        [icon_halal setImage:[UIImage imageNamed:@"halal-icon@2x.png"] forState:UIControlStateNormal];
//        [img_cellBackGnd   addSubview:icon_halal];
//        
//        UIImageView *img_non_veg = [[UIImageView alloc] init];
//        //  img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
//        [img_non_veg setImage:[UIImage imageNamed:@"non-veg bt@2x.png"]];
//        [img_cellBackGnd addSubview:img_non_veg];
//        
//        UIButton *icon_cow = [UIButton buttonWithType:UIButtonTypeCustom];
//        //  icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
//        //icon_cow .backgroundColor = [UIColor clearColor];
//        [icon_cow addTarget:self action:@selector(click_on_icon_cow_btn:) forControlEvents:UIControlEventTouchUpInside];
//        [icon_cow setImage:[UIImage imageNamed:@"cow-icon@2x.png"] forState:UIControlStateNormal];
//        [img_cellBackGnd   addSubview:icon_cow];
//        
//        UIButton *icon_fronce = [UIButton buttonWithType:UIButtonTypeCustom];
//        //   icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
//        //icon_cow .backgroundColor = [UIColor clearColor];
//        [icon_fronce addTarget:self action:@selector(click_on_fronce_btn:) forControlEvents:UIControlEventTouchUpInside];
//        [icon_fronce setImage:[UIImage imageNamed:@"fronce-icon@2x.png"] forState:UIControlStateNormal];
//        [img_cellBackGnd   addSubview:icon_fronce];
        
        UILabel *doller_rate = [[UILabel alloc]init];
        //   doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
        // doller_rate.text = @"$14.90";
        doller_rate.font = [UIFont fontWithName:kFontBold size:16];
        doller_rate.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        doller_rate.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:doller_rate];
        
        UIImageView *img_line = [[UIImageView alloc] init];
        //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
        [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
        [img_cellBackGnd addSubview:img_line];
        
        
        UIButton *icon_take = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        //icon_take .backgroundColor = [UIColor clearColor];
        [icon_take addTarget:self action:@selector(click_on_take_out_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_take setImage:[UIImage imageNamed:@"take-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_take];
        
        UIButton *icon_deliver = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //icon_deliver .backgroundColor = [UIColor clearColor];
        [icon_deliver addTarget:self action:@selector(click_on_icon_delivery_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_deliver setImage:[UIImage imageNamed:@"deliver-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_deliver];
        
        
        UIButton *img_btn_seving_Now = [UIButton buttonWithType:UIButtonTypeCustom];
        //   img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //img_btn_deliver_Now .backgroundColor = [UIColor clearColor];
        [img_btn_seving_Now addTarget:self action:@selector(click_on_seving_now_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_seving_Now setImage:[UIImage imageNamed:@"now-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_seving_Now];
        
        
        UIButton *img_btn_chef_menu = [UIButton buttonWithType:UIButtonTypeCustom];
        //    img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
        [img_btn_chef_menu addTarget:self action:@selector(click_on_icon_chef_menu_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_chef_menu setImage:[UIImage imageNamed:@"chef-menu-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_chef_menu];
        
        
        
        UIButton *icon_thumb = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+5, 20, 20);
        //icon_thumb .backgroundColor = [UIColor clearColor];
        [icon_thumb addTarget:self action:@selector(click_on_icon_thumb_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_thumb setImage:[UIImage imageNamed:@"thumb-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_thumb];
        
        UILabel *likes = [[UILabel alloc]init];
        //  likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
        //likes.text = @"87.4%";
        likes.font = [UIFont fontWithName:kFont size:10];
        likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        likes.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:likes];
        
        
        dish_name.text = [NSString stringWithFormat:@"%@",[[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"dishname"]];
        meters.text = [[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"distance"];
        doller_rate.text = [NSString stringWithFormat:@"$%@",[[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"dishprice"]];
        likes.text = [[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"likes"];
        
        
        if (IS_IPHONE_6Plus)
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            btn_number_three.frame = CGRectMake(58,-5, 35, 35);
            
            icon_delete.frame = CGRectMake(WIDTH-70,20, 40, 40);
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
//            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
//            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
//            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
//            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
//            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+4, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+9,128,100, 10);
            
            dish_name.font = [UIFont fontWithName:kFontBold size:18];
            
        }
        else if (IS_IPHONE_6)
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            btn_number_three.frame = CGRectMake(58,-5, 35, 35);
            
            icon_delete.frame = CGRectMake(WIDTH-62,20, 40, 40);
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
//            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
//            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
//            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
//            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
//            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(300,CGRectGetMaxY(img_line.frame)+5, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
            
            
            dish_name.font = [UIFont fontWithName:kFontBold size:18];
            
        }
        else
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            btn_number_three.frame = CGRectMake(58,-5, 35, 35);
            
            icon_delete.frame = CGRectMake(WIDTH-65,20, 40, 40);
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
//            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
//            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
//            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
//            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
//            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(IS_IPHONE_5?WIDTH-75:WIDTH-80,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(IS_IPHONE_5?240:230,CGRectGetMaxY(img_line.frame)+5, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
            
            
            dish_name.font = [UIFont fontWithName:kFontBold size:IS_IPHONE_5?15:15];
        }
        
        UIScrollView*delivery_Scroll;
        delivery_Scroll = [[UIScrollView alloc]init];
        
        if (IS_IPHONE_6Plus)
        {
            delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(icon_location.frame)+5,150,40);
        }
        else  if (IS_IPHONE_6)
        {
            
            delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(icon_location.frame)+5,150,40);
        }
        else  if (IS_IPHONE_5)
        {
            
            delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(icon_location.frame)+5,130,40);
        }
        else
        {
            delivery_Scroll.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(icon_location.frame)+5,130,40);
        }
        
        delivery_Scroll.backgroundColor = [UIColor whiteColor];
        delivery_Scroll.bounces=YES;
        //delivery_Scroll.layer.borderWidth = 1.0;
        delivery_Scroll.layer.borderColor = [UIColor blackColor].CGColor;
        delivery_Scroll.showsVerticalScrollIndicator = NO;
        delivery_Scroll.showsHorizontalScrollIndicator = NO;
        
        //delivery_Scroll.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [delivery_Scroll setScrollEnabled:YES];
        delivery_Scroll.userInteractionEnabled = YES;
        [cell.contentView addSubview:delivery_Scroll];
        //[(NSDictionary *) [[ary_maintosave  objectAtIndex:0] valueForKey:@"Delivery_Address"] count]
        
        int totalPage;
        
        totalPage = (int)[[[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"field_dish_dietary_restrictions"] count];
        [delivery_Scroll setContentSize:CGSizeMake(50*totalPage, delivery_Scroll.frame.size.height)];
        
        
        
        for (int j = 0; j<totalPage; j++)
        {
            
            
            UIImageView *img_non_veg = [[UIImageView alloc] init];
            img_non_veg.frame = CGRectMake(50*j,10, 25, 25);
            
            //        NSString *ImagePath1 =[[NSString stringWithFormat:@"%@",[[[[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"DishRestrictions"] objectAtIndex:j] valueForKey:@"resImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            //        [img_non_veg setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@""]];
            
            NSString *url_Img = [NSString stringWithFormat: @"%@", [[[[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"field_dish_dietary_restrictions"] objectAtIndex:j] valueForKey:@"resImage"]];
            img_non_veg.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img]]];
            
            //    NSString *ImagePath1 =  [[NSString stringWithFormat:@"%@",[[array_items_name objectAtIndex:indexPath.row] valueForKey:@"resImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            //    [img_non_veg setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@"img_merchantPlaceholder@2x.png"]];
            img_non_veg.backgroundColor=[UIColor clearColor];
            [delivery_Scroll addSubview:img_non_veg];
            
            
            //        UIButton*get_Deliver_Address_Btn;
            //        get_Deliver_Address_Btn = [[UIButton alloc]init];
            //        get_Deliver_Address_Btn.frame = CGRectMake(0,25*j, delivery_Scroll.frame.size.width, 25);
            //        [get_Deliver_Address_Btn addTarget:self action:@selector(get_Deliver_Address_BtnMethod:) forControlEvents:UIControlEventTouchUpInside];
            //        get_Deliver_Address_Btn.tag = j;
            //        get_Deliver_Address_Btn.titleLabel.text = [NSString stringWithFormat:@"%d",indexPath.row];
            //        [get_Deliver_Address_Btn setBackgroundColor:[UIColor clearColor]];
            //        //   get_Deliver_Address_Btn.layer.borderWidth = 1.0;
            //        [delivery_Scroll addSubview: get_Deliver_Address_Btn];
        }

        
    }
    
    else if (tableView == table_for_distance_in_later)
    {
        img_bg_for_distance = [[UIImageView alloc]init];
        img_bg_for_distance.frame =  CGRectMake(0,0, 295, 40);
        img_bg_for_distance.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_bg_for_distance setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_distance];
        
        UILabel *distance_names_in_later = [[UILabel alloc]init];
        distance_names_in_later.frame = CGRectMake(5,10,200,15);
        distance_names_in_later.text = [NSString stringWithFormat:@"%@",[array_names_in_distance objectAtIndex:indexPath.row]];
        distance_names_in_later.font = [UIFont fontWithName:kFontBold size:12];
        distance_names_in_later.textColor = [UIColor blackColor];
        distance_names_in_later.backgroundColor = [UIColor clearColor];
        [img_bg_for_distance addSubview:distance_names_in_later];
        
        
    }
    else if (tableView == table_for_itemsfilter)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH+5, 170);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-5, 170);
            
        }
        else
        {    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-10,170);
            
        }
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        UIImageView *img_dish = [[UIImageView alloc] init];
        //    img_dish.frame = CGRectMake(7,7, 90,  95 );
        
        NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"dishimage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [img_dish setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@""]];
        
        //[img_dish setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_img objectAtIndex:indexPath.row]]]];
        [img_cellBackGnd addSubview:img_dish];
        
        UIButton *btn_favorite = [UIButton buttonWithType:UIButtonTypeCustom];
        //  btn_favorite.frame = CGRectMake(5,58, 35, 35);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [btn_favorite addTarget:self action:@selector(btn_favorite_click:) forControlEvents:UIControlEventTouchUpInside];
        [btn_favorite setImage:[UIImage imageNamed:@"favorite-icon@2x.png"] forState:UIControlStateNormal];
        [img_dish   addSubview:btn_favorite];
        
        UIButton *btn_number_three = [UIButton buttonWithType:UIButtonTypeCustom];
        //   btn_number_three.frame = CGRectMake(58,-5, 35, 35);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [btn_number_three addTarget:self action:@selector(btn_number_three_click:) forControlEvents:UIControlEventTouchUpInside];
        [btn_number_three setImage:[UIImage imageNamed:@"red3-icon@2x.png"] forState:UIControlStateNormal];
        [img_dish   addSubview:btn_number_three];
        
        
        UIButton *icon_delete = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_delete.frame = CGRectMake(WIDTH-70,20, 40, 40);
        //icon_cow .backgroundColor = [UIColor clearColor];
        icon_delete.tag = indexPath.row;
        [icon_delete addTarget:self action:@selector(btn_add_to_cart_click:) forControlEvents:UIControlEventTouchUpInside];
        [icon_delete setImage:[UIImage imageNamed:@"img-cart@2x.png"] forState:UIControlStateNormal];
        [cell.contentView   addSubview:icon_delete];
        
        UILabel *dish_name = [[UILabel alloc]init];
        //   dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
        dish_name.text = [NSString stringWithFormat:@"%@",[[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"dishname"]];
        dish_name.font = [UIFont fontWithName:kFontBold size:8];
        dish_name.textColor = [UIColor blackColor];
        dish_name.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:dish_name];
        
        
        UIImageView *icon_location = [[UIImageView alloc] init];
        //    icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        [icon_location setImage:[UIImage imageNamed:@"location-icon@2x.png"]];
        [img_cellBackGnd  addSubview:icon_location];
        
        UILabel *meters = [[UILabel alloc]init];
        //  meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        meters.text = [[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"distance"];
        meters.font = [UIFont fontWithName:kFont size:15];
        meters.textColor = [UIColor blackColor];
        meters.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:meters];
        
        UIButton *icon_server = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
        //icon_server .backgroundColor = [UIColor clearColor];
        [icon_server  addTarget:self action:@selector(click_on_icon_sever_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_server setImage:[UIImage imageNamed:@"serving-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_server];
        
        UIButton *icon_halal = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
        //icon_halal .backgroundColor = [UIColor clearColor];
        [icon_halal addTarget:self action:@selector(click_on_icon_halal_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_halal setImage:[UIImage imageNamed:@"halal-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_halal];
        
        UIImageView *img_non_veg = [[UIImageView alloc] init];
        //  img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
        [img_non_veg setImage:[UIImage imageNamed:@"non-veg bt@2x.png"]];
        [img_cellBackGnd addSubview:img_non_veg];
        
        UIButton *icon_cow = [UIButton buttonWithType:UIButtonTypeCustom];
        //  icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_cow addTarget:self action:@selector(click_on_icon_cow_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_cow setImage:[UIImage imageNamed:@"cow-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_cow];
        
        UIButton *icon_fronce = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_fronce addTarget:self action:@selector(click_on_fronce_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_fronce setImage:[UIImage imageNamed:@"fronce-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_fronce];
        
        UILabel *doller_rate = [[UILabel alloc]init];
        //   doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
        doller_rate.text = [NSString stringWithFormat:@"$%@",[[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"dishprice"]];
        doller_rate.font = [UIFont fontWithName:kFontBold size:16];
        doller_rate.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        doller_rate.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:doller_rate];
        
        UIImageView *img_line = [[UIImageView alloc] init];
        //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
        [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
        [img_cellBackGnd addSubview:img_line];
        
        
        UIButton *icon_take = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        //icon_take .backgroundColor = [UIColor clearColor];
        [icon_take addTarget:self action:@selector(click_on_take_out_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_take setImage:[UIImage imageNamed:@"take-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_take];
        
        UIButton *icon_deliver = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //icon_deliver .backgroundColor = [UIColor clearColor];
        [icon_deliver addTarget:self action:@selector(click_on_icon_delivery_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_deliver setImage:[UIImage imageNamed:@"deliver-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_deliver];
        
        
        UIButton *img_btn_seving_Now = [UIButton buttonWithType:UIButtonTypeCustom];
        //   img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //img_btn_deliver_Now .backgroundColor = [UIColor clearColor];
        [img_btn_seving_Now addTarget:self action:@selector(click_on_seving_now_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_seving_Now setImage:[UIImage imageNamed:@"now-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_seving_Now];
        
        
        UIButton *img_btn_chef_menu = [UIButton buttonWithType:UIButtonTypeCustom];
        //    img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
        [img_btn_chef_menu addTarget:self action:@selector(click_on_icon_chef_menu_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_chef_menu setImage:[UIImage imageNamed:@"chef-menu-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_chef_menu];
        
        
        
        UIButton *icon_thumb = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+5, 20, 20);
        //icon_thumb .backgroundColor = [UIColor clearColor];
        [icon_thumb addTarget:self action:@selector(click_on_icon_thumb_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_thumb setImage:[UIImage imageNamed:@"thumb-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_thumb];
        
        UILabel *likes = [[UILabel alloc]init];
        //  likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
        likes.text = [[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"likes"];
        likes.font = [UIFont fontWithName:kFont size:10];
        likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        likes.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:likes];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            btn_number_three.frame = CGRectMake(58,-5, 35, 35);
            
            icon_delete.frame = CGRectMake(WIDTH-70,20, 40, 40);
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+4, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+9,128,100, 10);
            
            dish_name.font = [UIFont fontWithName:kFontBold size:18];
            
        }
        else if (IS_IPHONE_6)
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            btn_number_three.frame = CGRectMake(58,-5, 35, 35);
            
            icon_delete.frame = CGRectMake(WIDTH-62,20, 40, 40);
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(300,CGRectGetMaxY(img_line.frame)+5, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
            
            
            dish_name.font = [UIFont fontWithName:kFontBold size:18];
            
        }
        else
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            btn_number_three.frame = CGRectMake(58,-5, 35, 35);
            
            icon_delete.frame = CGRectMake(WIDTH-65,20, 40, 40);
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(IS_IPHONE_5?WIDTH-75:WIDTH-80,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(IS_IPHONE_5?240:230,CGRectGetMaxY(img_line.frame)+5, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
            
            
            dish_name.font = [UIFont fontWithName:kFontBold size:IS_IPHONE_5?15:15];
        }
        
    }
    else if (tableView == table_for_itemsfilterlater)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH+5, 170);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-5, 170);
            
        }
        else
        {    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-10,170);
            
        }
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        UIImageView *img_dish = [[UIImageView alloc] init];
        //    img_dish.frame = CGRectMake(7,7, 90,  95 );
        
        NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"dishimage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [img_dish setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@""]];
        
        //[img_dish setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_img objectAtIndex:indexPath.row]]]];
        [img_cellBackGnd addSubview:img_dish];
        
        UIButton *btn_favorite = [UIButton buttonWithType:UIButtonTypeCustom];
        //  btn_favorite.frame = CGRectMake(5,58, 35, 35);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [btn_favorite addTarget:self action:@selector(btn_favorite_click:) forControlEvents:UIControlEventTouchUpInside];
        [btn_favorite setImage:[UIImage imageNamed:@"favorite-icon@2x.png"] forState:UIControlStateNormal];
        [img_dish   addSubview:btn_favorite];
        
        UIButton *btn_number_three = [UIButton buttonWithType:UIButtonTypeCustom];
        //   btn_number_three.frame = CGRectMake(58,-5, 35, 35);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [btn_number_three addTarget:self action:@selector(btn_number_three_click:) forControlEvents:UIControlEventTouchUpInside];
        [btn_number_three setImage:[UIImage imageNamed:@"red3-icon@2x.png"] forState:UIControlStateNormal];
        [img_dish   addSubview:btn_number_three];
        
        
        UIButton *icon_delete = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_delete.frame = CGRectMake(WIDTH-70,20, 40, 40);
        //icon_cow .backgroundColor = [UIColor clearColor];
        icon_delete.tag = indexPath.row;
        [icon_delete addTarget:self action:@selector(btn_add_to_cart_click:) forControlEvents:UIControlEventTouchUpInside];
        [icon_delete setImage:[UIImage imageNamed:@"img-cart@2x.png"] forState:UIControlStateNormal];
        [cell.contentView   addSubview:icon_delete];
        
        UILabel *dish_name = [[UILabel alloc]init];
        //   dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
        dish_name.text = [NSString stringWithFormat:@"%@",[[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"dishname"]];
        dish_name.font = [UIFont fontWithName:kFontBold size:8];
        dish_name.textColor = [UIColor blackColor];
        dish_name.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:dish_name];
        
        
        UIImageView *icon_location = [[UIImageView alloc] init];
        //    icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        [icon_location setImage:[UIImage imageNamed:@"location-icon@2x.png"]];
        [img_cellBackGnd  addSubview:icon_location];
        
        UILabel *meters = [[UILabel alloc]init];
        //  meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        meters.text = [[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"distance"];
        meters.font = [UIFont fontWithName:kFont size:15];
        meters.textColor = [UIColor blackColor];
        meters.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:meters];
        
        UIButton *icon_server = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
        //icon_server .backgroundColor = [UIColor clearColor];
        [icon_server  addTarget:self action:@selector(click_on_icon_sever_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_server setImage:[UIImage imageNamed:@"serving-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_server];
        
        UIButton *icon_halal = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
        //icon_halal .backgroundColor = [UIColor clearColor];
        [icon_halal addTarget:self action:@selector(click_on_icon_halal_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_halal setImage:[UIImage imageNamed:@"halal-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_halal];
        
        UIImageView *img_non_veg = [[UIImageView alloc] init];
        //  img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
        [img_non_veg setImage:[UIImage imageNamed:@"non-veg bt@2x.png"]];
        [img_cellBackGnd addSubview:img_non_veg];
        
        UIButton *icon_cow = [UIButton buttonWithType:UIButtonTypeCustom];
        //  icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_cow addTarget:self action:@selector(click_on_icon_cow_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_cow setImage:[UIImage imageNamed:@"cow-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_cow];
        
        UIButton *icon_fronce = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_fronce addTarget:self action:@selector(click_on_fronce_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_fronce setImage:[UIImage imageNamed:@"fronce-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_fronce];
        
        UILabel *doller_rate = [[UILabel alloc]init];
        //   doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
        doller_rate.text = [NSString stringWithFormat:@"$%@",[[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"dishprice"]];
        doller_rate.font = [UIFont fontWithName:kFontBold size:16];
        doller_rate.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        doller_rate.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:doller_rate];
        
        UIImageView *img_line = [[UIImageView alloc] init];
        //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
        [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
        [img_cellBackGnd addSubview:img_line];
        
        
        UIButton *icon_take = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        //icon_take .backgroundColor = [UIColor clearColor];
        [icon_take addTarget:self action:@selector(click_on_take_out_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_take setImage:[UIImage imageNamed:@"take-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_take];
        
        UIButton *icon_deliver = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //icon_deliver .backgroundColor = [UIColor clearColor];
        [icon_deliver addTarget:self action:@selector(click_on_icon_delivery_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_deliver setImage:[UIImage imageNamed:@"deliver-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_deliver];
        
        
        UIButton *img_btn_seving_Now = [UIButton buttonWithType:UIButtonTypeCustom];
        //   img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //img_btn_deliver_Now .backgroundColor = [UIColor clearColor];
        [img_btn_seving_Now addTarget:self action:@selector(click_on_seving_now_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_seving_Now setImage:[UIImage imageNamed:@"now-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_seving_Now];
        
        
        UIButton *img_btn_chef_menu = [UIButton buttonWithType:UIButtonTypeCustom];
        //    img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
        [img_btn_chef_menu addTarget:self action:@selector(click_on_icon_chef_menu_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_chef_menu setImage:[UIImage imageNamed:@"chef-menu-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_chef_menu];
        
        
        
        UIButton *icon_thumb = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+5, 20, 20);
        //icon_thumb .backgroundColor = [UIColor clearColor];
        [icon_thumb addTarget:self action:@selector(click_on_icon_thumb_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_thumb setImage:[UIImage imageNamed:@"thumb-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_thumb];
        
        UILabel *likes = [[UILabel alloc]init];
        //  likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
        likes.text = [[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"likes"];
        likes.font = [UIFont fontWithName:kFont size:10];
        likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        likes.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:likes];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            btn_number_three.frame = CGRectMake(58,-5, 35, 35);
            
            icon_delete.frame = CGRectMake(WIDTH-70,20, 40, 40);
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+4, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+9,128,100, 10);
            
            dish_name.font = [UIFont fontWithName:kFontBold size:18];
            
        }
        else if (IS_IPHONE_6)
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            btn_number_three.frame = CGRectMake(58,-5, 35, 35);
            
            icon_delete.frame = CGRectMake(WIDTH-62,20, 40, 40);
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(300,CGRectGetMaxY(img_line.frame)+5, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
            
            
            dish_name.font = [UIFont fontWithName:kFontBold size:18];
            
        }
        else
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            btn_number_three.frame = CGRectMake(58,-5, 35, 35);
            
            icon_delete.frame = CGRectMake(WIDTH-65,20, 40, 40);
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(IS_IPHONE_5?WIDTH-75:WIDTH-80,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(IS_IPHONE_5?240:230,CGRectGetMaxY(img_line.frame)+5, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
            
            
            dish_name.font = [UIFont fontWithName:kFontBold size:IS_IPHONE_5?15:15];
        }
    }
    
    
    return cell;
    
}

#pragma table view

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView == table_on_drop_down)
    {
        if (indexPath.row == 0)
        {
            view_serch_for_food_now.hidden = NO;
            view_serch_for_food_later.hidden = YES;
            str_foodtype = @"0";
            lbl_food_now.text =[array_head_names objectAtIndex:indexPath.row];
            
            //[self AFFoodNow];
            
            //[self AFfoodnowNowlist];
            
            
        }
        else if (indexPath.row == 1)
        {
            view_serch_for_food_now.hidden = YES;
            view_serch_for_food_later.hidden = NO;
            str_foodtype = @"1";
             lbl_food_now.text =[array_head_names objectAtIndex:indexPath.row];
            
            [self AFfoodnowNowlist];
            
            
        }
        
      
        [table_on_drop_down setHidden:YES];
    }
    else if (tableView == table_for_items||tableView==table_for_items_in_later)
    {
        
        
        if ([[NSString stringWithFormat:@"%@",str_foodtype]isEqualToString:@"0"])
        {
            ItemDetailVC*vc = [ItemDetailVC new];
            vc.str_dishID = [[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"dishid"];
            vc.str_type = @"0";
            [self presentViewController:vc animated:NO completion:nil];

        }
        else{
            ItemDetailVC*vc = [ItemDetailVC new];
            vc.str_dishID = [[[ary_foodnowlist objectAtIndex:indexPath.row] valueForKey:@"DishDetail"] valueForKey:@"dishid"];
            vc.str_type = @"1";
            [self presentViewController:vc animated:NO completion:nil];

        }
        
    }
    else if (tableView == table_for_distance)
    {
        [table_for_distance setHidden:YES];
        lbl_distance.text =[array_names_in_distance objectAtIndex:indexPath.row];
        if (indexPath.row == 0)
        {
            str_sortby = @"2";
        }
        else if (indexPath.row == 1)
        {
            str_sortby = @"4";
        }
        else if (indexPath.row == 2)
        {
            str_sortby = @"5";
        
        }
        //[self AFfoodnowNowlist];
        
        
    }
    else if (tableView == table_for_distance_in_later)
    {
        lbl_distance_in_later.text =[array_names_in_distance objectAtIndex:indexPath.row];
        [table_for_distance_in_later setHidden:YES];
                if (indexPath.row == 0)
                {
                str_sortbylater = @"2";
                }
                else if (indexPath.row == 1)
                {
                str_sortbylater = @"4";
                }
                else if (indexPath.row == 2)
                {
                str_sortbylater = @"5";
                }
        //[self AFfoodnowNowlist];

    }
    
    
    else if (tableView == table_for_distance_in_later)
    {
        
    }
    
    
}
#pragma mark UiCollectionView Delegate Methods for dish

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(collectionView == collView_for_cuisines)
    {
        return [array_cuisinelist_infoodnow count];
    }
    else if (collectionView == collView_for_cuisines_in_all_food_now)
    {
        return [array_all_cuisinelist_infoodnow count];
    }
    else if (collectionView == collView_for_course_in_food_now)
    {
        return [array_course_infoodnow count];
    }
    else if  (collectionView == collView_for_dietary_in_food_now)
    {
        return [array_dietary_restryctionlist count];
    }
    
    // collection view for food later
    else if (collectionView == collView_for_cuisines_in_later)
    {
        return [array_favorate_cuisinelist_infoodlater count];
    }
    else if (collectionView == collView_for_cuisines_in_all_food_later)
    {
        return [array_all_cuisinelist_infoodlater count];
    }
    else if (collectionView == collView_for_course_in_food_later)
    {
        return [array_course_in_foodlater count];
    }
    else if (collectionView == collView_for_dietary_in_food_later)
    {
        return [array_dietary_restryctionlist_infoodlater count];
    }
    
    return 0;
    
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if(collectionView == collView_for_cuisines)
    {
        return 1;
    }
    else if (collectionView == collView_for_cuisines_in_all_food_now)
    {
        return 1;
    }
    if (collectionView == collView_for_course_in_food_now)
    {
        return 1;
    }
    if (collectionView == collView_for_dietary_in_food_now)
    {
        return 1;
    }
    
    // collection view for food later
    if (collectionView == collView_for_cuisines_in_later)
    {
        return 1;
    }
    if (collectionView == collView_for_cuisines_in_all_food_later)
    {
        return 1;
    }
    else if (collectionView == collView_for_course_in_food_later)
    {
        return 1;
    }
    else if (collectionView == collView_for_dietary_in_food_later)
    {
        return 1;
    }
    
    
    
    
    return 1;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView1 dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    
    if (collectionView1 == collView_for_cuisines)
    {
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        
        UIImageView *cuisines_icon = [[UIImageView alloc]init];
        cuisines_icon .frame = CGRectMake(5,8,70,70);
        //  [cuisines_icon setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_cuisine_imges objectAtIndex:indexPath.row]]]];
        // [chef_pro_icons  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[array_cuisinelist_infoodnow objectAtIndex:indexPath.row] valueForKey:@"image"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [cuisines_icon setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@""]];
        cuisines_icon .backgroundColor = [UIColor clearColor];
        [cuisines_icon  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:cuisines_icon];
        cuisines_icon.hidden = NO;
        
        
        
        
        UIButton *btn_incell_tikmark = [[UIButton alloc]init];
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 6,((WIDTH-65)/4),75);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 0,((WIDTH-65)/4),94);
        }
        else
        {
            btn_incell_tikmark.frame = CGRectMake(0, 25,((WIDTH-65)/4),70);
        }
        btn_incell_tikmark.backgroundColor=[UIColor clearColor];
        btn_incell_tikmark.layer.cornerRadius = 7.0;
        btn_incell_tikmark.layer.borderColor = [[UIColor whiteColor] CGColor];
        //   btn_TableCell_CheckBox1.layer.borderWidth = 1.0;
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        //btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,20,40,-40);
        //{top, left, bottom, right};
        //  btn_TableCell_CheckBox1.titleEdgeInsets = UIEdgeInsetsMake(0,-10,4.5,0);
        
        [btn_incell_tikmark addTarget:self action:@selector(click_selectObjectAt_favoritecuisines:) forControlEvents:UIControlEventTouchUpInside];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"] forState:UIControlStateSelected];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        btn_incell_tikmark.tag = indexPath.row;
        [cell_for_collection_view addSubview:btn_incell_tikmark];
        
        if ([Arr_temp_favoritecuisin_infoodnow containsObject:[array_cuisinelist_infoodnow objectAtIndex:indexPath.row]])
        {
            [btn_incell_tikmark setSelected:YES];
        }
        else
        {
            [btn_incell_tikmark setSelected:NO];
            
        }
        btn_incell_tikmark.tag = indexPath.row;
        [cell.contentView addSubview:btn_incell_tikmark];
        
        
        
        
        UILabel  * lbl_headings = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(cuisines_icon.frame)+1, ((WIDTH-65)/3),10);
            
        }
        else if (IS_IPHONE_6)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(cuisines_icon.frame)+1, ((WIDTH-65)/4),10);
            
        }
        else
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(cuisines_icon.frame)+1, ((WIDTH-65)/4),10);
            
        }
        
        lbl_headings.text =[[array_cuisinelist_infoodnow objectAtIndex:indexPath.row] valueForKey:@"name"];
        
        lbl_headings.backgroundColor=[UIColor clearColor];
        lbl_headings.textColor=[UIColor blackColor];
        lbl_headings.textAlignment=NSTextAlignmentCenter;
        lbl_headings.font = [UIFont fontWithName:kFont size:9];
        [cell_for_collection_view addSubview:lbl_headings];
        
        
        //        int selectedindex=(int)indexPath.row;
        //        NSLog(@"%d",selectedindex);
        
        
        
        
        if (IS_IPHONE_6Plus)
        {
            cell_for_collection_view .frame = CGRectMake(0,-5,100,103);
            cuisines_icon .frame = CGRectMake(5,8,70,70);
            
            
        }
        else if(IS_IPHONE_6)
        {
            cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
            cuisines_icon .frame = CGRectMake(5,8,70,70);
            
        }
        else
        {
            cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
            cuisines_icon .frame = CGRectMake(5,8,70,70);
            
        }
        
    }
    else if (collectionView1 == collView_for_cuisines_in_all_food_now)
    {
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        UIImageView *cuisines_icon = [[UIImageView alloc]init];
        cuisines_icon .frame = CGRectMake(5,8,70,70);
        //  [cuisines_icon setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_cuisine_imges objectAtIndex:indexPath.row]]]];
        // [chef_pro_icons  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[array_all_cuisinelist_infoodnow objectAtIndex:indexPath.row] valueForKey:@"CuisineImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [cuisines_icon setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@""]];
        cuisines_icon .backgroundColor = [UIColor clearColor];
        [cuisines_icon  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:cuisines_icon];
        cuisines_icon.hidden = NO;
        
        
        
        UIButton *btn_incell_tikmark = [[UIButton alloc]init];
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 6,((WIDTH-65)/4),75);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 0,((WIDTH-65)/4),94);
        }
        else
        {
            btn_incell_tikmark.frame = CGRectMake(0, 25,((WIDTH-65)/4),70);
        }
        btn_incell_tikmark.backgroundColor=[UIColor clearColor];
        btn_incell_tikmark.layer.cornerRadius = 7.0;
        btn_incell_tikmark.layer.borderColor = [[UIColor whiteColor] CGColor];
        //   btn_TableCell_CheckBox1.layer.borderWidth = 1.0;
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        //btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,20,40,-40);
        //{top, left, bottom, right};
        //  btn_TableCell_CheckBox1.titleEdgeInsets = UIEdgeInsetsMake(0,-10,4.5,0);
        
        [btn_incell_tikmark addTarget:self action:@selector(click_selectObjectAt_Allcuisines:) forControlEvents:UIControlEventTouchUpInside];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"] forState:UIControlStateSelected];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        btn_incell_tikmark.tag = indexPath.row;
        [cell_for_collection_view addSubview:btn_incell_tikmark];
        
        
        if ([array_temp_all_cuisine_infoodnow containsObject:[array_all_cuisinelist_infoodnow objectAtIndex:indexPath.row]])
        {
            [btn_incell_tikmark setSelected:YES];
        }
        else
        {
            [btn_incell_tikmark setSelected:NO];
            
        }
        btn_incell_tikmark.tag = indexPath.row;
        [cell.contentView addSubview:btn_incell_tikmark];
        
        
        
        
        
        UILabel  * lbl_headings = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(cuisines_icon.frame)+1, ((WIDTH-65)/3),10);
            
        }
        else if (IS_IPHONE_6)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(cuisines_icon.frame)+1, ((WIDTH-65)/4),10);
            
        }
        else
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(cuisines_icon.frame)+1, ((WIDTH-65)/4),10);
            
        }
        
        lbl_headings.text =[[array_all_cuisinelist_infoodnow objectAtIndex:indexPath.row] valueForKey:@"CuisineName"];
        
        lbl_headings.backgroundColor=[UIColor clearColor];
        lbl_headings.textColor=[UIColor blackColor];
        lbl_headings.textAlignment=NSTextAlignmentCenter;
        lbl_headings.font = [UIFont fontWithName:kFont size:9];
        [cell_for_collection_view addSubview:lbl_headings];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
            cuisines_icon .frame = CGRectMake(5,8,70,70);
            //             cuisines_icon_in_blue .frame = CGRectMake(5,8,70,70);
            //             cuisines_name.frame = CGRectMake(10,CGRectGetMaxY(cuisines_icon.frame),200, 15);
            //             btn_on_icons .frame = CGRectMake(0,0,30,30);
            
        }
        else if(IS_IPHONE_6)
        {
            cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
            cuisines_icon .frame = CGRectMake(5,8,70,70);
            //             cuisines_icon_in_blue .frame = CGRectMake(5,8,70,70);
            //             cuisines_name.frame = CGRectMake(10,CGRectGetMaxY(cuisines_icon.frame),200, 15);
            //             btn_on_icons .frame = CGRectMake(0,0,30,30);
            
        }
        else
        {
            cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
            cuisines_icon .frame = CGRectMake(5,8,70,70);
            //             cuisines_icon_in_blue .frame = CGRectMake(5,8,70,70);
            //             cuisines_name.frame = CGRectMake(10,CGRectGetMaxY(cuisines_icon.frame),200, 15);
            //             btn_on_icons .frame = CGRectMake(0,0,30,30);
            
        }
        
        
    }
    
    if (collectionView1 == collView_for_course_in_food_now)
    {
        
        
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,01,120,75);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        UIImageView *course_icon = [[UIImageView alloc]init];
        course_icon .frame = CGRectMake(15,28,50,25);
        //  [course_icon setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_course_images objectAtIndex:indexPath.row]]]];
        NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[array_course_infoodnow objectAtIndex:indexPath.row] valueForKey:@"DishCourseImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [course_icon setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@""]];
        //[chef_pro_icons  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        course_icon .backgroundColor = [UIColor clearColor];
        [course_icon  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:course_icon];
        course_icon.hidden = NO;
        
        
        
        
        UIButton *btn_incell_tikmark = [[UIButton alloc]init];
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 6,((WIDTH-65)/3),75);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 19,((WIDTH-60)/3),53);
        }
        else
        {
            btn_incell_tikmark.frame = CGRectMake(0, 23,((WIDTH-65)/3),70);
        }
        btn_incell_tikmark.backgroundColor=[UIColor clearColor];
        btn_incell_tikmark.layer.cornerRadius = 7.0;
        btn_incell_tikmark.layer.borderColor = [[UIColor whiteColor] CGColor];
        //   btn_TableCell_CheckBox1.layer.borderWidth = 1.0;
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        //btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,20,40,-40);
        //{top, left, bottom, right};
        //  btn_TableCell_CheckBox1.titleEdgeInsets = UIEdgeInsetsMake(0,-10,4.5,0);
        
        [btn_incell_tikmark addTarget:self action:@selector(click_selectObjectAt_course_infoodnow:) forControlEvents:UIControlEventTouchUpInside];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"] forState:UIControlStateSelected];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        btn_incell_tikmark.tag = indexPath.row;
        [cell_for_collection_view addSubview:btn_incell_tikmark];
        
        if ([Arr_temp_course_infoodnow containsObject:[array_course_infoodnow objectAtIndex:indexPath.row]])
        {
            [btn_incell_tikmark setSelected:YES];
        }
        else
        {
            [btn_incell_tikmark setSelected:NO];
            
        }
        btn_incell_tikmark.tag = indexPath.row;
        [cell.contentView addSubview:btn_incell_tikmark];
        
        
        
        UILabel  * lbl_headings = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(course_icon.frame)+1, ((WIDTH-65)/3),10);
            
        }
        else if (IS_IPHONE_6)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(course_icon.frame)+1, ((WIDTH-65)/3),10);
            
        }
        else
        {
            lbl_headings.frame =CGRectMake(8,CGRectGetMaxY(course_icon.frame)+1, ((WIDTH-65)/3),10);
            
        }
        
        lbl_headings.text =[[array_course_infoodnow objectAtIndex:indexPath.row] valueForKey:@"DishCourseName"];
        
        lbl_headings.backgroundColor=[UIColor clearColor];
        lbl_headings.textColor=[UIColor blackColor];
        lbl_headings.textAlignment=NSTextAlignmentCenter;
        lbl_headings.font = [UIFont fontWithName:kFont size:9];
        [cell_for_collection_view addSubview:lbl_headings];
        
        
        
        
        
        //        int selectedindex=(int)indexPath.row;
        //        NSLog(@"%d",selectedindex);
        
        
        
        
        
        if (IS_IPHONE_6Plus)
        {
            
            cell_for_collection_view .frame = CGRectMake(0,01,120,75);
            course_icon .frame = CGRectMake(40,22,30,30);
            //            course_name.frame = CGRectMake(20,CGRectGetMaxY(course_icon.frame)+5,200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
            
        }
        else if(IS_IPHONE_6)
        {
            
            cell_for_collection_view .frame = CGRectMake(0,01,120,75);
            course_icon .frame = CGRectMake(40,22,30,30);
            //            course_name.frame = CGRectMake(20,CGRectGetMaxY(course_icon.frame)+5,200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
            
        }
        else
        {
            
            cell_for_collection_view .frame = CGRectMake(0,01,120,75);
            course_icon .frame = CGRectMake(40,22,30,30);
            //            course_name.frame = CGRectMake(20,CGRectGetMaxY(course_icon.frame)+5,200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
            
        }
        
        
    }
    if (collectionView1 == collView_for_dietary_in_food_now)
    {
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        
        
        UIImageView *dietary_icon = [[UIImageView alloc]init];
        dietary_icon .frame = CGRectMake(10,15,60,60);
        
        NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@",[[array_dietary_restryctionlist objectAtIndex:indexPath.row] valueForKey:@"DietaryImage"]]];
        
        NSData *myData = [NSData dataWithContentsOfURL:url];
        
        dietary_icon.image = [UIImage imageWithData:myData];
        
        //        NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[array_dietary_restryctionlist objectAtIndex:indexPath.row] valueForKey:@"DietaryImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //        [dietary_icon setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@""]];
        dietary_icon .backgroundColor = [UIColor clearColor];
        [dietary_icon  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:dietary_icon];
        dietary_icon.hidden = NO;
        
        
        if (IS_IPHONE_6Plus)
        {
            cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
            dietary_icon .frame = CGRectMake(10,15,60,60);
            //            dietary_names.frame = CGRectMake(20,CGRectGetMaxY(dietary_icon.frame),200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
            
        }
        else if(IS_IPHONE_6)
        {
            cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
            dietary_icon .frame = CGRectMake(10,15,60,60);
            //            dietary_names.frame = CGRectMake(20,CGRectGetMaxY(dietary_icon.frame),200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
            //
        }
        else
        {
            cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
            dietary_icon .frame = CGRectMake(10,15,60,60);
            //            dietary_names.frame = CGRectMake(20,CGRectGetMaxY(dietary_icon.frame),200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
            
            
        }
        
        
        UIButton *btn_incell_tikmark = [[UIButton alloc]init];
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 6,((WIDTH-65)/4),75);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 6,((WIDTH-65)/4),75);
        }
        else
        {
            btn_incell_tikmark.frame = CGRectMake(0, 15,((WIDTH-65)/4),70);
        }
        btn_incell_tikmark.backgroundColor=[UIColor clearColor];
        btn_incell_tikmark.layer.cornerRadius = 7.0;
        btn_incell_tikmark.layer.borderColor = [[UIColor whiteColor] CGColor];
        //   btn_TableCell_CheckBox1.layer.borderWidth = 1.0;
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,26,10,10);
        }
        //btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,20,40,-40);
        //{top, left, bottom, right};
        //  btn_TableCell_CheckBox1.titleEdgeInsets = UIEdgeInsetsMake(0,-10,4.5,0);
        
        [btn_incell_tikmark setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"] forState:UIControlStateSelected];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        btn_incell_tikmark.tag = indexPath.row;
        [btn_incell_tikmark addTarget:self action:@selector(click_selectObjectAt:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        if ([Arr_temp containsObject:[array_dietary_restryctionlist objectAtIndex:indexPath.row]])
        {
            [btn_incell_tikmark setSelected:YES];
        }
        else
        {
            // [btn_incell_tikmark setSelected:YES];
            
            [btn_incell_tikmark setSelected:NO];
            
        }
        [cell.contentView addSubview:btn_incell_tikmark];
        
        UILabel  * lbl_headings = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(dietary_icon.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        else if (IS_IPHONE_6)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(dietary_icon.frame)+1, ((WIDTH-65)/4),10);
            
        }
        else
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(dietary_icon.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        
        lbl_headings.text =[[array_dietary_restryctionlist objectAtIndex:indexPath.row] valueForKey:@"DietaryName"];
        
        lbl_headings.backgroundColor=[UIColor clearColor];
        lbl_headings.textColor=[UIColor blackColor];
        lbl_headings.textAlignment=NSTextAlignmentCenter;
        lbl_headings.font = [UIFont fontWithName:kFont size:9];
        [cell_for_collection_view addSubview:lbl_headings];
        
        
        
        
        
        //        int selectedindex=(int)indexPath.row;
        //        NSLog(@"%d",selectedindex);
        
        
        
        
        
        
        
    }
    
    
    // =======================collection view for food later ====================================
    
    
    if (collectionView1 == collView_for_cuisines_in_later)
    {
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        
        UIImageView *cuisines_icon = [[UIImageView alloc]init];
        cuisines_icon .frame = CGRectMake(5,8,70,70);
        //   [cuisines_icon setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_cuisine_imges objectAtIndex:indexPath.row]]]];
        NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[array_favorate_cuisinelist_infoodlater objectAtIndex:indexPath.row] valueForKey:@"image"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [cuisines_icon setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@""]];
        // [chef_pro_icons  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        cuisines_icon .backgroundColor = [UIColor clearColor];
        [cuisines_icon  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:cuisines_icon];
        cuisines_icon.hidden = NO;
        
        
        
        
        UIButton *btn_incell_tikmark = [[UIButton alloc]init];
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 6,((WIDTH-65)/4),75);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 0,((WIDTH-65)/4),94);
        }
        else
        {
            btn_incell_tikmark.frame = CGRectMake(0, 25,((WIDTH-65)/4),70);
        }
        btn_incell_tikmark.backgroundColor=[UIColor clearColor];
        btn_incell_tikmark.layer.cornerRadius = 7.0;
        btn_incell_tikmark.layer.borderColor = [[UIColor whiteColor] CGColor];
        //   btn_TableCell_CheckBox1.layer.borderWidth = 1.0;
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        //btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,20,40,-40);
        //{top, left, bottom, right};
        //  btn_TableCell_CheckBox1.titleEdgeInsets = UIEdgeInsetsMake(0,-10,4.5,0);
        
        [btn_incell_tikmark addTarget:self action:@selector(click_selectObjectAt_favoratecuisines_infoodlater:) forControlEvents:UIControlEventTouchUpInside];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"] forState:UIControlStateSelected];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        btn_incell_tikmark.tag = indexPath.row;
        [cell_for_collection_view addSubview:btn_incell_tikmark];
        
        
        if ([array_temp_favorate_cuisines_in_foodlater containsObject:[array_favorate_cuisinelist_infoodlater objectAtIndex:indexPath.row]])
        {
            [btn_incell_tikmark setSelected:YES];
        }
        else
        {
            [btn_incell_tikmark setSelected:NO];
            
        }
        btn_incell_tikmark.tag = indexPath.row;
        [cell.contentView addSubview:btn_incell_tikmark];
        
        
        
        
        
        UILabel  * lbl_headings = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(cuisines_icon.frame)+1, ((WIDTH-65)/3),10);
            
        }
        else if (IS_IPHONE_6)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(cuisines_icon.frame)+1, ((WIDTH-65)/4),10);
            
        }
        else
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(cuisines_icon.frame)+1, ((WIDTH-65)/4),10);
            
        }
        
        lbl_headings.text =[[array_favorate_cuisinelist_infoodlater objectAtIndex:indexPath.row] valueForKey:@"name"];
        
        lbl_headings.backgroundColor=[UIColor clearColor];
        lbl_headings.textColor=[UIColor blackColor];
        lbl_headings.textAlignment=NSTextAlignmentCenter;
        lbl_headings.font = [UIFont fontWithName:kFont size:9];
        [cell_for_collection_view addSubview:lbl_headings];
        
        
        //        int selectedindex=(int)indexPath.row;
        //        NSLog(@"%d",selectedindex);
        
        
        
        
        
        if (IS_IPHONE_6Plus)
        {
            cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
            cuisines_icon .frame = CGRectMake(5,8,70,70);
            //            cuisines_icon_in_blue .frame = CGRectMake(5,8,70,70);
            //            cuisines_name.frame = CGRectMake(10,CGRectGetMaxY(cuisines_icon.frame)+4,200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
        }
        else if(IS_IPHONE_6)
        {
            cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
            cuisines_icon .frame = CGRectMake(5,8,70,70);
            //            cuisines_icon_in_blue .frame = CGRectMake(5,8,70,70);
            //            cuisines_name.frame = CGRectMake(10,CGRectGetMaxY(cuisines_icon.frame)+4,200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
        }
        else
        {
            cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
            cuisines_icon .frame = CGRectMake(5,8,70,70);
            //            cuisines_icon_in_blue .frame = CGRectMake(5,8,70,70);
            //            cuisines_name.frame = CGRectMake(10,CGRectGetMaxY(cuisines_icon.frame)+4,200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
        }
        
        
    }
    
    if (collectionView1 == collView_for_cuisines_in_all_food_later)
    {
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        
        UIImageView *cuisines_icon = [[UIImageView alloc]init];
        cuisines_icon .frame = CGRectMake(5,8,70,70);
        //  [cuisines_icon setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_cuisine_imges objectAtIndex:indexPath.row]]]];
        NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[array_all_cuisinelist_infoodlater objectAtIndex:indexPath.row] valueForKey:@"CuisineImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [cuisines_icon setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@""]];
        // [chef_pro_icons  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        cuisines_icon .backgroundColor = [UIColor clearColor];
        [cuisines_icon  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:cuisines_icon];
        cuisines_icon.hidden = NO;
        
        
        
        
        
        
        UIButton *btn_incell_tikmark = [[UIButton alloc]init];
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 6,((WIDTH-65)/4),75);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 0,((WIDTH-65)/4),94);
        }
        else
        {
            btn_incell_tikmark.frame = CGRectMake(0, 25,((WIDTH-65)/4),70);
        }
        btn_incell_tikmark.backgroundColor=[UIColor clearColor];
        btn_incell_tikmark.layer.cornerRadius = 7.0;
        btn_incell_tikmark.layer.borderColor = [[UIColor whiteColor] CGColor];
        //   btn_TableCell_CheckBox1.layer.borderWidth = 1.0;
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        //btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,20,40,-40);
        //{top, left, bottom, right};
        //  btn_TableCell_CheckBox1.titleEdgeInsets = UIEdgeInsetsMake(0,-10,4.5,0);
        
        [btn_incell_tikmark addTarget:self action:@selector(click_selectObjectAt_allcuisines_infoodlater:) forControlEvents:UIControlEventTouchUpInside];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"] forState:UIControlStateSelected];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        btn_incell_tikmark.tag = indexPath.row;
        [cell_for_collection_view addSubview:btn_incell_tikmark];
        
        
        if ([array_temp_all_cuisine_in_foodlater containsObject:[array_all_cuisinelist_infoodlater objectAtIndex:indexPath.row]])
        {
            [btn_incell_tikmark setSelected:YES];
        }
        else
        {
            [btn_incell_tikmark setSelected:NO];
            
        }
        btn_incell_tikmark.tag = indexPath.row;
        [cell.contentView addSubview:btn_incell_tikmark];
        
        
        
        
        
        UILabel  * lbl_headings = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(cuisines_icon.frame)+1, ((WIDTH-65)/3),10);
            
        }
        else if (IS_IPHONE_6)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(cuisines_icon.frame)+1, ((WIDTH-65)/4),10);
            
        }
        else
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(cuisines_icon.frame)+1, ((WIDTH-65)/4),10);
            
        }
        
        lbl_headings.text =[[array_all_cuisinelist_infoodlater objectAtIndex:indexPath.row] valueForKey:@"CuisineName"];
        
        lbl_headings.backgroundColor=[UIColor clearColor];
        lbl_headings.textColor=[UIColor blackColor];
        lbl_headings.textAlignment=NSTextAlignmentCenter;
        lbl_headings.font = [UIFont fontWithName:kFont size:9];
        [cell_for_collection_view addSubview:lbl_headings];
        
        
        //        int selectedindex=(int)indexPath.row;
        //        NSLog(@"%d",selectedindex);
        
        
        
        if (IS_IPHONE_6Plus)
        {
            cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
            cuisines_icon .frame = CGRectMake(5,8,70,70);
            //            cuisines_icon_in_blue .frame = CGRectMake(5,8,70,70);
            //            cuisines_name.frame = CGRectMake(10,CGRectGetMaxY(cuisines_icon.frame)+4,200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
            
        }
        else if(IS_IPHONE_6)
        {
            cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
            cuisines_icon .frame = CGRectMake(5,8,70,70);
            //            cuisines_icon_in_blue .frame = CGRectMake(5,8,70,70);
            //            cuisines_name.frame = CGRectMake(10,CGRectGetMaxY(cuisines_icon.frame)+4,200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
            
        }
        else
        {
            cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
            cuisines_icon .frame = CGRectMake(5,8,70,70);
            //            cuisines_icon_in_blue .frame = CGRectMake(5,8,70,70);
            //            cuisines_name.frame = CGRectMake(10,CGRectGetMaxY(cuisines_icon.frame)+4,200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
            
        }
        
        
    }
    else if (collectionView1 == collView_for_course_in_food_later)
    {
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,01,120,75);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        UIImageView *course_icon = [[UIImageView alloc]init];
        course_icon.frame = CGRectMake(15,28,50,25);
        // [course_icon setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_course_images objectAtIndex:indexPath.row]]]];
        NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[array_course_in_foodlater objectAtIndex:indexPath.row] valueForKey:@"DishCourseImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [course_icon setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@""]];
        //[chef_pro_icons  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        course_icon .backgroundColor = [UIColor clearColor];
        [course_icon  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:course_icon];
        course_icon.hidden = NO;
        
        
        
        UIButton *btn_incell_tikmark = [[UIButton alloc]init];
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 6,((WIDTH-65)/3),75);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 19,((WIDTH-60)/3),53);
        }
        else
        {
            btn_incell_tikmark.frame = CGRectMake(0, 23,((WIDTH-65)/3),70);
        }
        btn_incell_tikmark.backgroundColor=[UIColor clearColor];
        btn_incell_tikmark.layer.cornerRadius = 7.0;
        btn_incell_tikmark.layer.borderColor = [[UIColor whiteColor] CGColor];
        //   btn_TableCell_CheckBox1.layer.borderWidth = 1.0;
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        //btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,20,40,-40);
        //{top, left, bottom, right};
        //  btn_TableCell_CheckBox1.titleEdgeInsets = UIEdgeInsetsMake(0,-10,4.5,0);
        
        [btn_incell_tikmark addTarget:self action:@selector(click_selectObjectAt_course_infoodlater:) forControlEvents:UIControlEventTouchUpInside];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"] forState:UIControlStateSelected];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        btn_incell_tikmark.tag = indexPath.row;
        [cell_for_collection_view addSubview:btn_incell_tikmark];
        
        
        if ([array_temp_course_in_food_later containsObject:[array_course_in_foodlater objectAtIndex:indexPath.row]])
        {
            [btn_incell_tikmark setSelected:YES];
        }
        else
        {
            [btn_incell_tikmark setSelected:NO];
            
        }
        btn_incell_tikmark.tag = indexPath.row;
        [cell.contentView addSubview:btn_incell_tikmark];
        
        
        
        UILabel  * lbl_headings = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(course_icon.frame)+1, ((WIDTH-65)/3),10);
            
        }
        else if (IS_IPHONE_6)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(course_icon.frame)+1, ((WIDTH-65)/3),10);
            
        }
        else
        {
            lbl_headings.frame =CGRectMake(8,CGRectGetMaxY(course_icon.frame)+1, ((WIDTH-65)/3),10);
            
        }
        
        lbl_headings.text =[[array_course_in_foodlater objectAtIndex:indexPath.row] valueForKey:@"DishCourseName"];
        
        lbl_headings.backgroundColor=[UIColor clearColor];
        lbl_headings.textColor=[UIColor blackColor];
        lbl_headings.textAlignment=NSTextAlignmentCenter;
        lbl_headings.font = [UIFont fontWithName:kFont size:9];
        [cell_for_collection_view addSubview:lbl_headings];
        
        
        
        
        
        int selectedindex=(int)indexPath.row;
        NSLog(@"%d",selectedindex);
        
        
        
        
        
        
        
        if (IS_IPHONE_6Plus)
        {
            cell_for_collection_view .frame = CGRectMake(0,01,120,75);
            course_icon .frame = CGRectMake(40,22,30,30);
            //            course_name.frame = CGRectMake(20,CGRectGetMaxY(course_icon.frame)+5,200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
            
        }
        else if(IS_IPHONE_6)
        {
            cell_for_collection_view .frame = CGRectMake(0,01,120,75);
            course_icon .frame = CGRectMake(40,22,30,30);
            //            course_name.frame = CGRectMake(20,CGRectGetMaxY(course_icon.frame)+5,200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
            
        }
        else
        {
            cell_for_collection_view .frame = CGRectMake(0,01,120,75);
            course_icon .frame = CGRectMake(40,22,30,30);
            //            course_name.frame = CGRectMake(20,CGRectGetMaxY(course_icon.frame)+5,200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
            
        }
        
        
    }
    
    else if (collectionView1 == collView_for_dietary_in_food_later)
    {
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        
        UIImageView *dietary_icon = [[UIImageView alloc]init];
        dietary_icon .frame = CGRectMake(10,15,60,60);
        NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@",[[array_dietary_restryctionlist_infoodlater objectAtIndex:indexPath.row] valueForKey:@"DietaryImage"]]];
        
        NSData *myData = [NSData dataWithContentsOfURL:url];
        
        dietary_icon.image = [UIImage imageWithData:myData];
        //        NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[array_dietary_restryctionlist_infoodlater objectAtIndex:indexPath.row] valueForKey:@"DietaryImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //        [dietary_icon setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@""]];
        dietary_icon .backgroundColor = [UIColor clearColor];
        [dietary_icon  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:dietary_icon];
        dietary_icon.hidden = NO;
        
        
        
        
        UIButton *btn_incell_tikmark = [[UIButton alloc]init];
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 6,((WIDTH-65)/4),75);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 6,((WIDTH-65)/4),75);
        }
        else
        {
            btn_incell_tikmark.frame = CGRectMake(0,15,((WIDTH-65)/4),70);
        }
        btn_incell_tikmark.backgroundColor=[UIColor clearColor];
        btn_incell_tikmark.layer.cornerRadius = 7.0;
        btn_incell_tikmark.layer.borderColor = [[UIColor whiteColor] CGColor];
        //   btn_TableCell_CheckBox1.layer.borderWidth = 1.0;
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        //btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,20,40,-40);
        //{top, left, bottom, right};
        //  btn_TableCell_CheckBox1.titleEdgeInsets = UIEdgeInsetsMake(0,-10,4.5,0);
        
        [btn_incell_tikmark setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"] forState:UIControlStateSelected];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        btn_incell_tikmark.tag = indexPath.row;
        [btn_incell_tikmark addTarget:self action:@selector(click_selectObjectAt_dietary_restryctions_in_foodlater:) forControlEvents:UIControlEventTouchUpInside];

        [cell.contentView addSubview:btn_incell_tikmark];
        
        if ([array_temp_dietary_restryction_infoodlater containsObject:[array_dietary_restryctionlist_infoodlater objectAtIndex:indexPath.row]])
        {
            [btn_incell_tikmark setSelected:YES];
        }
        else
        {
            [btn_incell_tikmark setSelected:NO];
            
        }
        btn_incell_tikmark.tag = indexPath.row;
        [cell.contentView addSubview:btn_incell_tikmark];
        
        
        
        
        
        
        UILabel  * lbl_headings = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(dietary_icon.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        else if (IS_IPHONE_6)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(dietary_icon.frame), ((WIDTH-65)/4),10);
            
        }
        else
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(dietary_icon.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        
        lbl_headings.text =[[array_dietary_restryctionlist_infoodlater objectAtIndex:indexPath.row] valueForKey:@"DietaryName"];
        
        lbl_headings.backgroundColor=[UIColor clearColor];
        lbl_headings.textColor=[UIColor blackColor];
        lbl_headings.textAlignment=NSTextAlignmentCenter;
        lbl_headings.font = [UIFont fontWithName:kFont size:9];
        [cell_for_collection_view addSubview:lbl_headings];
        
        
        
        
        
        //        int selectedindex=(int)indexPath.row;
        //        NSLog(@"%d",selectedindex);
        
        
        
        
        if (IS_IPHONE_6Plus)
        {
            cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
            dietary_icon .frame = CGRectMake(10,15,60,60);
            //            dietary_names.frame = CGRectMake(20,CGRectGetMaxY(dietary_icon.frame),200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
            
        }
        else if(IS_IPHONE_6)
        {
            cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
            dietary_icon .frame = CGRectMake(10,15,60,60);
            //            dietary_names.frame = CGRectMake(20,CGRectGetMaxY(dietary_icon.frame),200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
            
        }
        else
        {
            cell_for_collection_view .frame = CGRectMake(0,-5,85,103);
            dietary_icon .frame = CGRectMake(10,15,60,60);
            //            dietary_names.frame = CGRectMake(20,CGRectGetMaxY(dietary_icon.frame),200, 15);
            //            btn_on_icons .frame = CGRectMake(0,0,30,30);
            
            
        }
        
    }
    
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == collView_for_cuisines)
    {
        return CGSizeMake((85), 90);
    }
    else if (collectionView == collView_for_cuisines_in_all_food_now)
    {
        return CGSizeMake((85), 90);
    }
    
    else if (collectionView == collView_for_course_in_food_now)
    {
        return CGSizeMake((120), 90);
    }
    else if (collectionView == collView_for_dietary_in_food_now)
    {
        return CGSizeMake((85), 90);
    }
    
    //collection layout for food later
    else if (collectionView == collView_for_cuisines_in_later)
    {
        return CGSizeMake((85), 90);
    }
    else if (collectionView == collView_for_cuisines_in_all_food_later)
    {
        return CGSizeMake((85), 90);
    }
    else if (collectionView == collView_for_course_in_food_later)
    {
        return CGSizeMake((120), 90);
    }
    else if (collectionView == collView_for_dietary_in_food_later)
    {
        return CGSizeMake((85), 90);
    }
    return CGSizeMake((85), 90);
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        
    }
    
}


#pragma scroll View Delegates

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
}


#pragma mark - Date Picker
- (void)datePickerValueChanged:(id)sender
{
    [formatter setDateFormat:@"dd/MM/yyyy"];
    //    @"d  d   m  m   y  y  y  y"
    NSString *str=[formatter stringFromDate:datePicker.date];
    
    
    if(IS_IPHONE_6Plus)
    {
        str_DOBfinal = [NSString stringWithFormat:@" %@%@-%@%@-%@%@%@%@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
        
    }
    else if(IS_IPHONE_6)
    {
        str_DOBfinal = [NSString stringWithFormat:@"%@%@-%@%@-%@%@%@%@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
    }
    else if (IS_IPHONE_5)
    {
        str_DOBfinal = [NSString stringWithFormat:@"%@%@-%@%@-%@%@%@%@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
    }
    else
    {
        str_DOBfinal = [NSString stringWithFormat:@"%@%@-%@%@-%@%@%@%@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
    }
    
    //    str_DOBfinal = [NSString stringWithFormat:@"  %@     %@          %@     %@        %@      %@     %@      %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
    //    NSLog(@"fdyhsag %@",str_DOBfinal);
    
    txt_date.font = [UIFont fontWithName:kFont size:14];
    txt_date.textColor = [UIColor blackColor];
    
    NSLog(@"gdj%@", [str substringToIndex:2]);
    
    [txt_date setText:str_DOBfinal];
    
}

-(void) click_DoneDate:(id) sender
{
    [formatter setDateFormat:@"dd/MM/yyyy"];
    //    @"d  d   m  m   y  y  y  y"
    NSString *str=[formatter stringFromDate:datePicker.date];
    
    if(IS_IPHONE_6Plus)
    {
        str_DOBfinal = [NSString stringWithFormat:@"%@%@-%@%@-%@%@%@%@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
        
    }
    else if(IS_IPHONE_6)
    {
        str_DOBfinal = [NSString stringWithFormat:@"%@%@-%@%@-%@%@%@%@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
    }
    else if (IS_IPHONE_5)
    {
        str_DOBfinal = [NSString stringWithFormat:@"%@%@-%@%@-%@%@%@%@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
    }
    else
    {
        str_DOBfinal = [NSString stringWithFormat:@"%@%@-%@%@-%@%@%@%@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
    }
    
    //    str_DOBfinal = [NSString stringWithFormat:@"  %@     %@          %@     %@        %@      %@     %@      %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
    //    NSLog(@"fdyhsag %@",str_DOBfinal);
    
    txt_date.font = [UIFont fontWithName:kFont size:14];
    txt_date.textColor = [UIColor blackColor];
    
    NSLog(@"gdj%@", [str substringToIndex:2]);
    
    [txt_date setText:str_DOBfinal];
    [self.view endEditing:YES];
}



- (void)tinetoValueChanged:(id)sender
{
    
}

#pragma mark - Time picker

- (void)timedatePickerValueChanged:(id)sender
{
    [formattertime setDateFormat:@"hh:mm a"];
    [formattertime setTimeStyle: NSDateFormatterShortStyle];
    NSString*dateString;
    dateString = [formattertime stringFromDate:datePickertime.date];
    [txt_time setText:dateString];
    
}

-(void) click_Donetime:(id) sender
{
    if (txt_time.text.length == 0)
        [formattertime setDateFormat:@"hh:mm a"];
    [formattertime setTimeStyle: NSDateFormatterShortStyle];
    NSString*dateString;
    dateString = [formattertime stringFromDate:datePickertime.date];
    [txt_time setText:dateString];
    
    
    //        [self timedatePickerValueChanged:datePickertime];
    
    [self.view endEditing:YES];
}







#pragma mark Click Events
-(void)btn_menu_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click");
    [self dismissViewControllerAnimated:NO completion:nil];
    //    [self.navigationController popViewControllerAnimated:NO];
}
-(void)btn_on_food_later_label_click:(UIButton *)sender
{
    NSLog(@"btn_on_food_later_label_click");
    
    table_on_drop_down.hidden=YES;
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        table_on_drop_down.hidden=NO;
        
        
    }
    else
    {
        [sender setSelected:NO];
        table_on_drop_down.hidden=YES;
    }
    
    
}
-(void)btn_drop_down:(UIButton *)sender
{
    NSLog(@"btn_on_food_later_label_click");
    
}
-(void)btn_set_date_click:(UIButton *)sender
{
    NSLog(@"img_fb_click Btn Click");
    
}
-(void)btn_set_time_click:(UIButton *)sender
{
    NSLog(@"btn_set_time_click:");
    
}
-(void)btn_right_click:(UIButton *)sender
{
    NSLog(@"btn_right_click:");
    
}
-(void)btn_on_set_location_click:(UIButton *)sender
{
    NSLog(@"btn_on_set_location_click::");
    LocationVC* vc = [[LocationVC alloc]init];
    [self presentViewController:vc animated:NO completion:nil];
    //  [self.navigationController pushViewController:vc animated:NO];
    
    
}

-(void)btn_on_dish_click:(UIButton *)sender
{
    NSLog(@"btn_singel_dish_click:");
    
    ABC=(int)sender.tag;
    
    
    if([ array_temp containsObject:[NSString stringWithFormat:@"%d",ABC]])
    {
        [array_temp removeObject:[NSString stringWithFormat:@"%d",ABC]];

        if (sender.tag==54)
        {
            
            [btn_on_single_dish setImage:[UIImage imageNamed:@"single-dish-icon@2x.png"] forState:UIControlStateNormal];
            img_red_tik1.hidden=YES;
            //            img_red_tik3.hidden=YES;
            
        }
        else if (sender.tag==55)
        {
            img_red_tik2.hidden=YES;
            //            img_red_tik4.hidden=YES;
            [btn_on_meal setImage:[UIImage imageNamed:@"iocn-meal@2x.png"] forState:UIControlStateNormal];
        }
    }
    else
    {
        [array_temp addObject:[NSString stringWithFormat:@"%d",ABC]];
        if (sender.tag==54)
        {
            [ btn_on_single_dish setImage:[UIImage imageNamed:@"img-single-bl@2x.png"] forState:UIControlStateNormal];
            img_red_tik1.hidden=NO;
            str_servenowdishtype= @"Dish";
            [self AFFoodNow];
            //            img_red_tik3.hidden=NO;
            
        }
        else if (sender.tag==55)
        {
            [btn_on_meal setImage:[UIImage imageNamed:@"iocn-meal-bl@2x.png"] forState:UIControlStateNormal];
            img_red_tik2.hidden=NO;
            str_servenowdishtype = @"Meal";
            [self AFFoodNow];
            //            img_red_tik4.hidden=NO;
            
        }
    }
    
}
-(void)btn_on_dish_in_later_click:(UIButton *)sender
{
    NSLog(@"btn_on_dish_in_later_click:");
    AAA =(int)sender.tag;
    
    if([ array_temp_in_later containsObject:[NSString stringWithFormat:@"%d",AAA]])
    {
        [array_temp_in_later removeObject:[NSString stringWithFormat:@"%d",AAA]];
        
        if (sender.tag==1)
        {
            
            [btn_on_single_dish_in_later setImage:[UIImage imageNamed:@"single-dish-icon@2x.png"] forState:UIControlStateNormal];
            
            img_red_tik3.hidden=YES;
            
        }
        else if (sender.tag==2)
        {
            
            
            [btn_on_meal2 setImage:[UIImage imageNamed:@"iocn-meal@2x.png"] forState:UIControlStateNormal];
            img_red_tik4.hidden=YES;
            
        }
    }
    else
    {
        [array_temp_in_later addObject:[NSString stringWithFormat:@"%d",AAA]];
        if (sender.tag==1)
        {
            [btn_on_single_dish_in_later setImage:[UIImage imageNamed:@"img-single-bl@2x.png"] forState:UIControlStateNormal];
            
            img_red_tik3.hidden=NO;
            str_servelaterdishtype = @"Dish";
            [self AFFoodLater];
            
        }
        else if (sender.tag==2)
        {
            [btn_on_meal2 setImage:[UIImage imageNamed:@"iocn-meal-bl@2x.png"] forState:UIControlStateNormal];
            
            img_red_tik4.hidden=NO;
            str_servelaterdishtype = @"Meal";
            [self AFFoodLater];


        }
    }
    
    
    
    
}
-(void)click_on_serving_type:(UIButton *)sender
{
    NSLog(@"btn_img_dine_in_click:");
    DEF=(int)sender.tag;
    
    if([ array_temp2 containsObject:[NSString stringWithFormat:@"%d",DEF]])
    {
        [array_temp2 removeObject:[NSString stringWithFormat:@"%d",DEF]];
        
        if (sender.tag == 0)
        {
            
            [btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in@2x.png"]forState:UIControlStateNormal];
            img_red_tik_on_dine_in.hidden=YES;
            
            
        }
        else if (sender.tag == 1)
        {
            img_red_tik_on_take_out.hidden=YES;
            [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out@2x.png"] forState:UIControlStateNormal];
        }
        else if (sender.tag == 2)
        {
            img_red_tik_on_deliver.hidden=YES;
            [btn_on_delever setImage:[UIImage imageNamed:@"img-delever@2x.png"] forState:UIControlStateNormal];
            
        }
    }
    else
    {
        [array_temp2 addObject:[NSString stringWithFormat:@"%d",DEF]];
        
        [self AFFoodNow];
        
        if (sender.tag == 0)
        {
            [btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in-bl@2x.png"]forState:UIControlStateNormal];
            img_red_tik_on_dine_in.hidden=NO;
        }
        else if (sender.tag == 1)
        {
            [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out-bl@2x.png"]forState:UIControlStateNormal];
            img_red_tik_on_take_out.hidden=NO;
            
        }
        else if (sender.tag == 2)
        {
            [btn_on_delever setImage:[UIImage imageNamed:@"img-delever-bl@2x.png"]forState:UIControlStateNormal];
            img_red_tik_on_deliver.hidden=NO;
            
        }
    }
    
}

-(void)click_on_serving_typefoodlater:(UIButton *)sender
{
    NSLog(@"btn_img_dine_in_click:");
    foodlater=(int)sender.tag;
    
    if([ ary_foodlater_serveinglater containsObject:[NSString stringWithFormat:@"%d",foodlater]])
    {
        [ary_foodlater_serveinglater removeObject:[NSString stringWithFormat:@"%d",foodlater]];
        
        if (sender.tag == 0)
        {
            
            [btn_foodlater_dinein_serve setImage:[UIImage imageNamed:@"img-dine-in@2x.png"]forState:UIControlStateNormal];
            img_red_tik_on_dine_in_foodlater.hidden=YES;
            
            
        }
        else if (sender.tag == 1)
        {
            img_red_tik_on_takeout_foodlater.hidden=YES;
            [btn_foodlater_takeout setImage:[UIImage imageNamed:@"img-take-out@2x.png"] forState:UIControlStateNormal];
        }
        else if (sender.tag == 2)
        {
            img_red_tik_on_delivary_foodlater.hidden=YES;
            [btn_foodlater_delivary setImage:[UIImage imageNamed:@"img-delever@2x.png"] forState:UIControlStateNormal];
            
        }
    }
    else
    {
        [ary_foodlater_serveinglater addObject:[NSString stringWithFormat:@"%d",foodlater]];
        [self AFFoodLater];
        if (sender.tag == 0)
        {
            [btn_foodlater_dinein_serve setImage:[UIImage imageNamed:@"img-dine-in-bl@2x.png"]forState:UIControlStateNormal];
            img_red_tik_on_dine_in_foodlater.hidden=NO;
        }
        else if (sender.tag == 1)
        {
            [btn_foodlater_takeout setImage:[UIImage imageNamed:@"img-take-out-bl@2x.png"]forState:UIControlStateNormal];
            img_red_tik_on_takeout_foodlater.hidden=NO;
            
        }
        else if (sender.tag == 2)
        {
            [btn_foodlater_delivary setImage:[UIImage imageNamed:@"img-delever-bl@2x.png"]forState:UIControlStateNormal];
            img_red_tik_on_delivary_foodlater.hidden=NO;
            
        }
    }
    
}



//-(void)click_on_serving_type_later:(UIButton *)sender
//{
//    NSLog(@"btn_img_dine_in_click:");
//    DEF=(int)sender.tag;
//
//    if([ array_temp2 containsObject:[NSString stringWithFormat:@"%d",DEF]])
//    {
//        [array_temp2 removeObject:[NSString stringWithFormat:@"%d",DEF]];
//
//        if (sender.tag == 0)
//        {
//
//            [btn_on_dine_in_later setImage:[UIImage imageNamed:@"img-dine-in@2x.png"]forState:UIControlStateNormal];
//            img_red_tik_on_dine_in2.hidden=YES;
//
//
//        }
//        else if (sender.tag == 1)
//        {
//            img_red_tik_on_take_out.hidden=YES;
//            [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out@2x.png"] forState:UIControlStateNormal];
//        }
//        else if (sender.tag == 2)
//        {
//            img_red_tik_on_deliver.hidden=YES;
//            [btn_on_delever setImage:[UIImage imageNamed:@"img-delever@2x.png"] forState:UIControlStateNormal];
//
//        }
//    }
//    else
//    {
//        [array_temp2 addObject:[NSString stringWithFormat:@"%d",DEF]];
//        if (sender.tag == 0)
//        {
//            [btn_on_dine_in_later setImage:[UIImage imageNamed:@"img-dine-in-bl@2x.png"]forState:UIControlStateNormal];
//            img_red_tik_on_dine_in2.hidden=NO;
//        }
//        else if (sender.tag == 1)
//        {
//            [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out-bl@2x.png"]forState:UIControlStateNormal];
//            img_red_tik_on_take_out.hidden=NO;
//
//        }
//        else if (sender.tag == 2)
//        {
//            [btn_on_delever setImage:[UIImage imageNamed:@"img-delever-bl@2x.png"]forState:UIControlStateNormal];
//            img_red_tik_on_deliver.hidden=NO;
//
//        }
//    }
//
//}
-(void)btn_on_search_bar_click:(UIButton *)sender
{
    NSLog(@"btn_on_search_bar_click:");
    
    [self AFFoodNow];
    
    
}
-(void)btn_img_on_more_filters_click:(UIButton *)sender
{
    NSLog(@"btn_img_on_more_filters_click:");
    view_for_more_filters.hidden = YES;
    view_for_food_items.hidden = NO;
    
    
    
}
-(void)btn_img_on_more_filters_later_click:(UIButton *)sender
{
    NSLog(@"btn_img_on_more_filters_click:");
    view_for_more_filters_in_later.hidden = YES;
    view_for_cuisines_in_later.hidden = NO;
}


#pragma mark Click Events for table views in food now r later

-(void)click_selectObjectAt_favoritecuisines:(UIButton *) sender
{
    
    selectedindex= (int)sender.tag;
    
    
    if (sender.tag == selectedindex)
    {
        indexSelected = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        
    }
    
    if([Arr_temp_favoritecuisin_infoodnow containsObject:[array_cuisinelist_infoodnow objectAtIndex:sender.tag]])
    {
        [Arr_temp_favoritecuisin_infoodnow removeObject:[array_cuisinelist_infoodnow objectAtIndex:sender.tag]];
    }
    else
    {
        [Arr_temp_favoritecuisin_infoodnow addObject:[array_cuisinelist_infoodnow objectAtIndex:sender.tag]];
        [self AFFoodNow];
    }
    
    //[collectionView_FashionCategory reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:sender.tag inSection:0]]  withRowAnimation:UITableViewRowAnimationFade];
    [collView_for_cuisines reloadData];
    
    
    
    
}
-(void)click_selectObjectAt_Allcuisines:(UIButton *)sender
{
    selectedindex= (int)sender.tag;
    
    
    if (sender.tag == selectedindex)
    {
        indexSelected = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        
    }
    
    if([array_temp_all_cuisine_infoodnow containsObject:[array_all_cuisinelist_infoodnow objectAtIndex:sender.tag]])
    {
        [array_temp_all_cuisine_infoodnow removeObject:[array_all_cuisinelist_infoodnow objectAtIndex:sender.tag]];
    }
    else
    {
        [array_temp_all_cuisine_infoodnow addObject:[array_all_cuisinelist_infoodnow objectAtIndex:sender.tag]];
       //[self AFFoodNow];
    }
    
    //[collectionView_FashionCategory reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:sender.tag inSection:0]]  withRowAnimation:UITableViewRowAnimationFade];
    [collView_for_cuisines_in_all_food_now reloadData];
    
}
-(void) click_selectObjectAt_course_infoodnow:(UIButton *)sender
{
    
    selectedindex= (int)sender.tag;
    
    
    if (sender.tag == selectedindex)
    {
        indexSelected = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        
    }
    
    
    if([Arr_temp_course_infoodnow containsObject:[array_course_infoodnow objectAtIndex:sender.tag]])
    {
        [Arr_temp_course_infoodnow removeObject:[array_course_infoodnow objectAtIndex:sender.tag]];
    }
    else
    {
        [Arr_temp_course_infoodnow addObject:[array_course_infoodnow objectAtIndex:sender.tag]];
        //[self AFFoodNow];
    }
    
    //[collectionView_FashionCategory reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:sender.tag inSection:0]]  withRowAnimation:UITableViewRowAnimationFade];
    [collView_for_course_in_food_now reloadData];
    
    
}

-(void) click_selectObjectAt:(UIButton *) sender
{
    
    selectedindex= (int)sender.tag;
    
    
    if (sender.tag == selectedindex)
    {
        indexSelected = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        
    }
    
    if([Arr_temp containsObject:[array_dietary_restryctionlist objectAtIndex:sender.tag]])
    {
        [Arr_temp removeObject:[array_dietary_restryctionlist objectAtIndex:sender.tag]];
    }
    else
    {
        [Arr_temp addObject:[array_dietary_restryctionlist objectAtIndex:sender.tag]];
        //[self AFFoodNow];
    }
    
    //[collectionView_FashionCategory reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:sender.tag inSection:0]]  withRowAnimation:UITableViewRowAnimationFade];
    [collView_for_dietary_in_food_now reloadData];
    
    
}



-(void) click_selectObjectAt_favoratecuisines_infoodlater:(UIButton *)sender
{
    selectedindex= (int)sender.tag;
    
    
    if (sender.tag == selectedindex)
    {
        indexSelected = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        
    }
    
    
    if([array_temp_favorate_cuisines_in_foodlater containsObject:[array_favorate_cuisinelist_infoodlater objectAtIndex:sender.tag]])
    {
        [array_temp_favorate_cuisines_in_foodlater removeObject:[array_favorate_cuisinelist_infoodlater objectAtIndex:sender.tag]];
    }
    else
    {
        [array_temp_favorate_cuisines_in_foodlater addObject:[array_favorate_cuisinelist_infoodlater objectAtIndex:sender.tag]];
    }
    
    //[collectionView_FashionCategory reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:sender.tag inSection:0]]  withRowAnimation:UITableViewRowAnimationFade];
    [collView_for_cuisines_in_later reloadData];
    
    
    
}
-(void)click_selectObjectAt_allcuisines_infoodlater:(UIButton *)sender
{
    
    selectedindex= (int)sender.tag;
    
    
    if (sender.tag == selectedindex)
    {
        indexSelected = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        
    }
    
    if([array_temp_all_cuisine_in_foodlater containsObject:[array_all_cuisinelist_infoodlater objectAtIndex:sender.tag]])
    {
        [array_temp_all_cuisine_in_foodlater removeObject:[array_all_cuisinelist_infoodlater objectAtIndex:sender.tag]];
    }
    else
    {
        [array_temp_all_cuisine_in_foodlater addObject:[array_all_cuisinelist_infoodlater objectAtIndex:sender.tag]];
    }
    
    //[collectionView_FashionCategory reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:sender.tag inSection:0]]  withRowAnimation:UITableViewRowAnimationFade];
    [collView_for_cuisines_in_all_food_later reloadData];
    
    
    
    
}

-(void)click_selectObjectAt_course_infoodlater:(UIButton *)sender
{
    
    selectedindex= (int)sender.tag;
    
    
    if (sender.tag == selectedindex)
    {
        indexSelected = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        
    }
    
    if([array_temp_course_in_food_later containsObject:[array_course_in_foodlater objectAtIndex:sender.tag]])
    {
        [array_temp_course_in_food_later removeObject:[array_course_in_foodlater objectAtIndex:sender.tag]];
    }
    else
    {
        [array_temp_course_in_food_later addObject:[array_course_in_foodlater objectAtIndex:sender.tag]];
    }
    
    //[collectionView_FashionCategory reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:sender.tag inSection:0]]  withRowAnimation:UITableViewRowAnimationFade];
    [collView_for_course_in_food_now reloadData];
    
}

-(void)click_selectObjectAt_dietary_restryctions_in_foodlater:(UIButton *)sender
{
    
    
    selectedindex= (int)sender.tag;
    
    
    if (sender.tag == selectedindex)
    {
        indexSelected = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        
    }
    
    
    if([array_temp_dietary_restryction_infoodlater containsObject:[array_dietary_restryctionlist_infoodlater objectAtIndex:sender.tag]])
    {
        [array_temp_dietary_restryction_infoodlater removeObject:[array_dietary_restryctionlist_infoodlater objectAtIndex:sender.tag]];
    }
    else
    {
        [array_temp_dietary_restryction_infoodlater addObject:[array_dietary_restryctionlist_infoodlater objectAtIndex:sender.tag]];
    }
    
    //[collectionView_FashionCategory reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:sender.tag inSection:0]]  withRowAnimation:UITableViewRowAnimationFade];
    
    [collView_for_dietary_in_food_later reloadData];
    
    
    
    
    
    
    
}



-(void)btn_add_to_cart_click:(UIButton *)sender
{
    NSLog(@"btn_delete_click:");
    
//    if ([[NSString stringWithFormat:@"%@",str_foodtype]isEqualToString:@"0"])
//    {
//        AddToCartFoodNowVC*vc = [AddToCartFoodNowVC new];
//        vc.str_dishID = [[[ary_foodnowlist objectAtIndex:sender.tag] valueForKey:@"DishDetail"] valueForKey:@"dishid"];
//        vc.str_type = @"0";
//        [self presentViewController:vc animated:NO completion:nil];
//        
//    }
//    else{
//        AddToCartFoodLaterVC*vc = [AddToCartFoodLaterVC new];
//        vc.str_dishID = [[[ary_foodnowlist objectAtIndex:sender.tag] valueForKey:@"DishDetail"] valueForKey:@"dishid"];
//        vc.str_type = @"1";
//        [self presentViewController:vc animated:NO completion:nil];
//        
//    }

    
}
-(void)btn_favorite_click:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
    
}
-(void)btn_number_three_click:(UIButton *)sender
{
    NSLog(@"btn_number_three_click:");
}


-(void)icon_Food_Later_BtnClick:(UIButton *)sender
{
    NSLog(@"icon_Food_Later_BtnClick:");
    
}
-(void)click_on_icon_halal_btn:(UIButton *)sender
{
    NSLog(@"icon_halal_BtnClick:");
    
}
-(void)click_on_icon_sever_btn:(UIButton *)sender
{
    NSLog(@"icon_server_BtnClick:");
    
}
-(void)click_on_icon_cow_btn:(UIButton *)sender
{
    NSLog(@"icon_cow_BtnClick:");
    
}
-(void)click_on_fronce_btn:(UIButton *)sender
{
    NSLog(@"btn_img_on_more_filters_click:");
    
}
-(void)click_on_take_out_btn:(UIButton *)sender
{
    NSLog(@"btn_chef_menu_click:");
}
-(void)click_on_icon_delivery_btn:(UIButton *)sender
{
    NSLog(@"btn_chef_menu_click:");
}

-(void)click_on_seving_now_btn:(UIButton *)sender
{
    NSLog(@"btn_serving_now_click:");
}
-(void)click_on_icon_chef_menu_btn:(UIButton *)sender
{
    NSLog(@"btn_chef_menu_click:");
}
-(void)click_on_icon_thumb_btn:(UIButton *)sender
{
    NSLog(@"btn_chef_menu_click:");
}


-(void)btn_on_my_favorites_click:(UIButton *)sender
{
    NSLog(@"btn_on_my_favorites_click");
    img_strip1.hidden = NO;
    img_strip2.hidden = YES;
    str_type = @"Favorite";
    
    view_for_my_favorites.hidden = NO;
    view_for_all_in_food_now.hidden = YES;
    
}
-(void)btn_all_click:(UIButton *)sender
{
    NSLog(@"btn_all_click");
    img_strip1.hidden = YES;
    img_strip2.hidden = NO;
    str_type = @"All";
    view_for_all_in_food_now.hidden = NO;
    view_for_my_favorites.hidden = YES;
    
    
    
}
-(void)btn_on_left_arrow_click:(UIButton *)sender
{
    NSLog(@"btn_on_left_arrow_click:");
    
}
-(void)btn_on_right_arrow_click:(UIButton *)sender
{
    NSLog(@"btn_on_right_arrow2_click:");
    
}
-(void)click_on_distance_btn:(UIButton *)sender
{
    NSLog(@"click_on_distance_btn");
    table_for_distance.hidden = NO;
    
}
-(void)click_on_filters_btn:(UIButton *)sender
{
    NSLog(@"click_on_filters_btn:");
    
    [self AFFoodNow];
    
    
}
-(void)btn_on_my_favorites_in_later_click:(UIButton *)sender
{
    NSLog(@"btn_on_my_favorites_in_later_click:");
    
    img_strip3.hidden = NO;
    img_strip4.hidden = YES;
    
    view_for_my_favorites_in_later.hidden = NO;
    view_for_all_in_food_later.hidden = YES;
}
-(void)btn_all_in_later_click:(UIButton *)sender
{
    NSLog(@"btn_all_click");
    img_strip3.hidden = YES;
    img_strip4.hidden = NO;
    
    view_for_my_favorites_in_later.hidden = YES;
    view_for_all_in_food_later.hidden = NO;
}

-(void)click_on_distance_in_later_btn:(UIButton *)sender
{
    NSLog(@"click_on_distance_in_later_btn");
    
    table_for_distance_in_later.hidden = NO;
}
-(void)click_on_filters_in_laterbtn:(UIButton *)sender
{
    NSLog(@"click_on_filters_in_laterbtn");
      //[self  AFFoodLater];
    [self AFFoodLater];
    
}

-(void)click_on_left_arrow1_btn:(UIButton *)sender
{
    NSLog(@"click_on_left_arrow1_btn");
}
-(void)click_on_right_arrow1_btn:(UIButton *)sender
{
    NSLog(@"click_on_right_arrow1_btn");
}
-(void)click_on_left_arrow2_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_right_arrow2_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_left_arrow3_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_right_arrow3_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_left_arrow4_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_right_arrow4_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_left_arrow1_in_food_later_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_right_arrow1in_food_later_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_left_arrow2_in_later_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_right_arrow2_in_later_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_left_arrow3_in_later_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_right_arrow3_in_later_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_left_arrow4_in_later_btn:(UIButton *)sender
{
    NSLog(@"");
    
}
-(void)click_on_right_arrow4_in_later_btn:(UIButton *)sender
{
    NSLog(@"");
}
#pragma return action

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

//#pragma functionality
//
//
//-(void)serchforfood_now_r_later
//{
//
//    [self.view addSubview:delegate.activityIndicator];
//    [delegate.activityIndicator startAnimating];
//
//    //=================================================================BASE URL
//
//    NSURL *url = [NSURL URLWithString:kBaseUrl];
//
//    //=================================================================USED PARAMETERS(ONLY TEXT)
//
//    if (TARGET_IPHONE_SIMULATOR)
//    {
//        delegate.devicestr = @"";
//    }
//    NSDictionary *params =@{
//
//
//                            @"page_number"            :  @"1",
//                            @"food_type"              :  @"",
//                            @"lat"                    :  @"",
//                            @"long"                   :  @"",
//                            @"serving_type"           :  @"",
//                            @"keyword"                :  @"",
//                            @"meal_type"              :  @"",
//                            @"course"                 :  @"",
//                            @"cuisenes"               :  @"",
//                            @"dietry_restrictions"    :  @"",
//                            @"sort_by"                :  @"",
//
//                            };
//
//
//
//    //===========================================AFNETWORKING HEADER
//
//    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
//    httpClient.parameterEncoding = AFFormURLParameterEncoding;
//    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
//    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
//
//    //===============================SIMPLE REQUEST
//
//    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
//                                                            path: kfoodnow_later
//                                                      parameters:params];
//
//
//
//
//    //====================================================RESPONSE
//
//
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
//
//    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
//
//    }];
//    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSError *error = nil;
//        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
//
//        [delegate.activityIndicator stopAnimating];
//        [self ResponseChefSignUpCuisineList:JSON];
//    }
//
//     //==================================================ERROR
//                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//
//
//                                         [delegate.activityIndicator stopAnimating];
//
//                                         if([operation.response statusCode] == 406){
//
//                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
//                                             return;
//                                         }
//
//                                         if([operation.response statusCode] == 403){
//                                             NSLog(@"Upload Failed");
//                                             return;
//                                         }
//                                         if ([[operation error] code] == -1009) {
//                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
//                                                                                          message:@"Please check your internet connection"
//                                                                                         delegate:nil
//                                                                                cancelButtonTitle:@"OK"
//                                                                                otherButtonTitles:nil];
//                                             [av show];
//                                         }
//                                         else if ([[operation error] code] == -1001) {
//
//                                             NSLog(@"Successfully Registered");
//                                             [self serchforfood_now_r_later];
//                                         }
//                                     }];
//    [operation start];
//
//}
//-(void) ResponseChefSignUpCuisineList :(NSDictionary * )TheDict
//{
//    [array_for_food removeAllObjects];
//
//
//    NSLog(@"Login: %@",TheDict);
//
//    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
//    {
//        for (int i=0; i<[[TheDict valueForKey:@"OrderDeta"] count]; i++)
//        {
//            [array_for_food addObject:[[TheDict valueForKey:@"OrderDeta"] objectAtIndex:i]];
//
//        }
//
//
//    }
//    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
//    {
//        //        txt_popular.text=@"";
//
//
//    }
//
//    // [tab reloadData];
//
//    //    [self AlergyList];
//}
//



# pragma mark Favorite cuisines in food now

-(void)AFfavoriteCuisine_in_foodnow
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpCuisineList
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsefavoritecuisinlistinfoodnow:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFfavoriteCuisine_in_foodnow];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsefavoritecuisinlistinfoodnow :(NSDictionary * )TheDict
{
    [array_cuisinelist_infoodnow removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"CuisineList"] count]; i++)
        {
            [array_cuisinelist_infoodnow addObject:[[TheDict valueForKey:@"CuisineList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //  txt_popular.text=@"";
        
        
    }
    
    [collView_for_cuisines reloadData];
    
    // str_cuisenCatId_in_foodnow = @"80";
    
    
    
    
}

# pragma mark ALL cuisines in food now

-(void)AFAll_Cuisine_in_foodnow
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpCuisineList
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseAllcuisinlistinfoodnow:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFAll_Cuisine_in_foodnow];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseAllcuisinlistinfoodnow :(NSDictionary * )TheDict
{
    [array_all_cuisinelist_infoodnow removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"CuisineList"] count]; i++)
        {
            [array_all_cuisinelist_infoodnow addObject:[[TheDict valueForKey:@"CuisineList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //  txt_popular.text=@"";
        
        
    }
    
    [collView_for_cuisines_in_all_food_now reloadData];
    
    // str_cuisenCatId_in_foodnow = @"80";
    
    
    
    
}




# pragma mark Course in food now

-(void)AFCourse_in_foodnow
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kCourses
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsecourseinfoodnow:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFCourse_in_foodnow];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsecourseinfoodnow :(NSDictionary * )TheDict
{
    [array_course_infoodnow removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DishCoursetList"] count]; i++)
        {
            [array_course_infoodnow addObject:[[TheDict valueForKey:@"DishCoursetList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //  txt_popular.text=@"";
        
        
    }
    
    [collView_for_course_in_food_now reloadData];
    
    // str_cuisenCatId_in_foodnow = @"80";
    
    
    
    
}



# pragma mark ditary_ristrictions method

-(void)AFDietary_restrictions_in_foodnow
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kDietary_restryctions
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError *error = nil;
         NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
         
         [delegate.activityIndicator stopAnimating];
         [self Responsediet:JSON];
     }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFDietary_restrictions_in_foodnow];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsediet :(NSDictionary * )TheDict
{
    [array_dietary_restryctionlist removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DietarytList"] count]; i++)
        {
            [array_dietary_restryctionlist addObject:[[TheDict valueForKey:@"DietarytList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //  txt_popular.text=@"";
        
        
    }
    
    [collView_for_dietary_in_food_now reloadData];
    
    
    
}

# pragma mark favarote cuisines in food later

-(void)AFFavorate_Cuisine_in_foodlater
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpCuisineList
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsefavoratecuisinlistinfoodlater:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFFavorate_Cuisine_in_foodlater];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsefavoratecuisinlistinfoodlater :(NSDictionary * )TheDict
{
    [array_favorate_cuisinelist_infoodlater removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"CuisineList"] count]; i++)
        {
            [array_favorate_cuisinelist_infoodlater addObject:[[TheDict valueForKey:@"CuisineList"] objectAtIndex:i]];
            
        }
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //  txt_popular.text=@"";
        
        
    }
    
    [collView_for_cuisines_in_later reloadData];
    
    // str_cuisenCatId_in_foodnow = @"80";
    
    
    
    
}

# pragma mark all cuisines in food later

-(void)AFAll_Cuisine_in_foodlater
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpCuisineList
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseAllcuisinlistinfoodlater:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFAll_Cuisine_in_foodlater];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseAllcuisinlistinfoodlater :(NSDictionary * )TheDict
{
    [array_all_cuisinelist_infoodlater removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"CuisineList"] count]; i++)
        {
            [array_all_cuisinelist_infoodlater addObject:[[TheDict valueForKey:@"CuisineList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //  txt_popular.text=@"";
        
        
    }
    
    [collView_for_cuisines_in_all_food_later reloadData];
    
    // str_cuisenCatId_in_foodnow = @"80";
    
    
    
    
    
}


# pragma mark Course in food now

-(void)AFCourse_in_foodlater
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kCourses
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsecourseinfoodlater:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFCourse_in_foodlater];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsecourseinfoodlater :(NSDictionary * )TheDict
{
    [array_course_in_foodlater removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DishCoursetList"] count]; i++)
        {
            [array_course_in_foodlater addObject:[[TheDict valueForKey:@"DishCoursetList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //  txt_popular.text=@"";
        
        
    }
    
    [collView_for_course_in_food_later reloadData];
    
    // str_cuisenCatId_in_foodnow = @"80";
    
    
    
    
}


# pragma mark ditary_ristrictions method in foodlater

-(void)AFDietary_restrictions_in_foodlater
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kDietary_restryctions
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError *error = nil;
         NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
         
         [delegate.activityIndicator stopAnimating];
         [self Responsediet_deitary_in_foodlater:JSON];
     }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406)
                                         {
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403)
                                         {
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009)
                                         {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFDietary_restrictions_in_foodlater];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsediet_deitary_in_foodlater :(NSDictionary * )TheDict
{
    [array_dietary_restryctionlist_infoodlater removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DietarytList"] count]; i++)
        {
            [array_dietary_restryctionlist_infoodlater addObject:[[TheDict valueForKey:@"DietarytList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //  txt_popular.text=@"";
        
        
    }
    
    [collView_for_dietary_in_food_later reloadData];
    
    
    
}



#pragma parameters for FoodNow
-(void)AFFoodNow
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    
    // NSString *dateOfBirth = self.txt_DateBirth.text;
    
    //    NSMutableString * str_mealtype_in_foodnow = [[NSMutableString alloc] init];
    //
    //    for (int i=0; i<[Arr_temp_favoritecuisin_infoodnow count]; i++)
    //    {
    //
    //
    //        if (i==[Arr_temp_favoritecuisin_infoodnow count]-1)
    //        {
    //            [str_mealtype_in_foodnow  appendString:[NSString stringWithFormat:@"%@",[[Arr_temp_favoritecuisin_infoodnow objectAtIndex:i] valueForKey:@"CuisineID"]]];
    //        }
    //        else
    //        {
    //            [str_mealtype_in_foodnow  appendString:[NSString stringWithFormat:@"%@||",[[Arr_temp_favoritecuisin_infoodnow objectAtIndex:i] valueForKey:@"CuisineID"]]];
    //        }
    //
    //
    //    }
    
    
    NSMutableString * str_favoratecuisenid_in_foodnow = [[NSMutableString alloc] init];
    
    for (int i=0; i<[Arr_temp_favoritecuisin_infoodnow count]; i++)
    {
        
        
        if (i==[Arr_temp_favoritecuisin_infoodnow count]-1)
        {
            [str_favoratecuisenid_in_foodnow  appendString:[NSString stringWithFormat:@"%@",[[Arr_temp_favoritecuisin_infoodnow objectAtIndex:i] valueForKey:@"CuisineID"]]];
        }
        else
        {
            [str_favoratecuisenid_in_foodnow  appendString:[NSString stringWithFormat:@"%@,",[[Arr_temp_favoritecuisin_infoodnow objectAtIndex:i] valueForKey:@"CuisineID"]]];
        }
        
        
    }
    
    NSMutableString * str_allcuisenid_In_foodnow = [[NSMutableString alloc] init];
    
    for (int i=0; i<[array_temp_all_cuisine_infoodnow count]; i++)
    {
        
        
        if (i==[array_temp_all_cuisine_infoodnow count]-1)
        {
            [str_allcuisenid_In_foodnow  appendString:[NSString stringWithFormat:@"%@",[[array_temp_all_cuisine_infoodnow objectAtIndex:i] valueForKey:@"CuisineID"]]];
        }
        else
        {
            [str_allcuisenid_In_foodnow  appendString:[NSString stringWithFormat:@"%@,",[[array_temp_all_cuisine_infoodnow objectAtIndex:i] valueForKey:@"CuisineID"]]];
        }
        
    }
    
    NSMutableString * str_Course_in_foodnow = [[NSMutableString alloc] init];
    
    for (int i=0; i<[Arr_temp_course_infoodnow count]; i++)
    {
        
        
        if (i==[Arr_temp_course_infoodnow count]-1)
        {
            [str_Course_in_foodnow  appendString:[NSString stringWithFormat:@"%@",[[Arr_temp_course_infoodnow objectAtIndex:i] valueForKey:@"DishCourseID"]]];
        }
        else
        {
            [str_Course_in_foodnow  appendString:[NSString stringWithFormat:@"%@,",[[Arr_temp_course_infoodnow objectAtIndex:i] valueForKey:@"DishCourseID"]]];
        }
        
    }
    
    
    NSMutableString *str_dietryids_in_foodnow = [[NSMutableString alloc] init];
    
    for (int i=0; i<[Arr_temp count]; i++)
    {
        
        
        if (i==[Arr_temp count]-1)
        {
            [str_dietryids_in_foodnow  appendString:[NSString stringWithFormat:@"%@",[[Arr_temp objectAtIndex:i] valueForKey:@"DietaryID"]]];
        }
        else
        {
            [str_dietryids_in_foodnow  appendString:[NSString stringWithFormat:@"%@,",[[Arr_temp objectAtIndex:i] valueForKey:@"DietaryID"]]];
        }
        
        
    }
    
    NSMutableString *str_servingtype = [[NSMutableString alloc] init];
    
    for (int i=0; i<[array_temp2 count]; i++)
    {
        
        
        if (i==[array_temp2 count]-1)
        {
            [str_servingtype  appendString:[NSString stringWithFormat:@"%@",[array_temp2 objectAtIndex:i]]];
        }
        else
        {
            [str_servingtype  appendString:[NSString stringWithFormat:@"%@,",[array_temp2 objectAtIndex:i]]];
        }
        
        
    }
    
    NSString*str_meal;
    str_meal = @"NO";
    
    if ([array_temp count]>0)
    {
        if ([array_temp count]==2)
        {
            str_meal = @"NO";

        }
        else{
            
        }
        str_meal = @"YES";

    }
    else
    {
        str_meal = @"NO";
        
    }
    int page =PagenoInt;
    
    if (page == (int)nil )
    {
        page=0;
    }
    

    
    NSDictionary *params;
    
    if ([[NSString stringWithFormat:@"%@",str_foodtype]isEqualToString:@"0"])
    {
        
        if ([str_meal isEqualToString:@"NO"])
        {
            params =@{
                      @"uid"                       :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                      @"page_number"               :  [NSString stringWithFormat:@"%d",page],
                      @"food_type"                 :  @"0",
                      @"lat"                       :  str_Latitude,
                      @"long"                      :  str_Longitude,
                      @"serving_type"              :  str_servingtype,
                      @"keyword"                   :  txt_search.text,
                      @"cuisenes"                  :  str_favoratecuisenid_in_foodnow,
                      @"course"                    :  str_Course_in_foodnow,
                      @"dietry_restrictions"       :  str_dietryids_in_foodnow,
                      @"sort_by"                   :  str_sortby,
                      @"keyword"                   : txt_search.text

                      };

        }
        else{
            params =@{
                      @"uid"                       :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                      @"page_number"               :  [NSString stringWithFormat:@"%d",page],
                      @"food_type"                 :  @"0",
                      @"lat"                       :  str_Latitude,
                      @"long"                      :  str_Longitude,
                      @"serving_type"              :  str_servingtype,
                      @"keyword"                   :  txt_search.text,
                      @"cuisenes"                  :  str_favoratecuisenid_in_foodnow,
                      @"course"                    :  str_Course_in_foodnow,
                      @"dietry_restrictions"       :  str_dietryids_in_foodnow,
                      @"sort_by"                   :  str_sortby,
                      @"meal_type"                 :  str_servenowdishtype,
                      @"keyword"                   : txt_search.text
                      
                      };
        }
       
        
    }
    else{
        params =@{
                  @"uid"                       : [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                  @"page_number"               :  @"1",
                  @"food_type"                 :  @"0",
                  @"lat"                       :  str_Latitude,
                  @"long"                      :  str_Longitude,
                  @"serving_type"              :  @"1",
                  @"keyword"                   :  txt_search.text,
                  @"meal_type"                 :  @"3",
                  @"cuisenes"                  :  str_allcuisenid_In_foodnow,
                  @"course"                    :  str_Course_in_foodnow,
                  @"dietry_restrictions"       :  @"200,201",
                  @"sort_by"                   :  str_sortby,
                  
                  
                  };
        
    }
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    
    // ===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kUserSearchFoodNow
                                                      parameters:params];
    
    NSLog(@"request =%@",request);
    
    
    
    //====================================================RESPONSE
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        NSLog(@"JSON = %@",JSON);
        [delegate.activityIndicator stopAnimating];
        
        [self ResponseFoodNow:JSON];
        
    }
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
         [delegate.activityIndicator stopAnimating];
         
         [self.view setUserInteractionEnabled:YES];
         if([operation.response statusCode] == 406)
         {
             
             return;
         }
         
         if([operation.response statusCode] == 403)
         {
             NSLog(@"Upload Failed");
             return;
         }
         if ([[operation error] code] == -1009)
         {
             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                          message:@"Please check your internet connection"
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
             [av show];
             
             
         }
         else if ([[operation error] code] == -1001)
         {
             NSLog(@"Successfully Registered");
             [self AFFoodNow];
             
         }
     }];
    [operation start];
    
}


-(void)ResponseFoodNow:(NSDictionary * )TheDict
{
    NSLog(@"ResponseDefault: %@",TheDict);
    
    [self.view setUserInteractionEnabled:YES];
    
    
    NSLog(@"foodlist: %@",TheDict);
    
    
    
//    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
//    {
//        for (int i=0; i<[[TheDict valueForKey:@"DataDetail"] count]; i++)
//        {
//            
//            [ary_foodnowlist addObject:[[TheDict valueForKey:@"DataDetail"] objectAtIndex:i]];
//            
//        }
//        
//        if ([str_foodtype isEqualToString:@"1"])
//        {
//            [table_for_items_in_later reloadData];
//            
//        }
//        else{
//            [table_for_items reloadData];
//            [table_for_itemsfilter reloadData];
//            
//            
//        }
//        
//    }
    
    
    [arr_PageArray removeAllObjects];
    
    
    if ([[TheDict valueForKey:@"error"] isEqualToString:@"0"])
    {
        
        float  slotlimit=[[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"CurrentDishPageSlots"]] floatValue];
        
        float totalProduct=[[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"TotalDishCount"]] floatValue];
        
        pagelimit=totalProduct/slotlimit;
        PagenoInt=[[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"CurrentDishPageNumber"]] integerValue];
        //    PagenoInt=PagenoInt+1;
        NSLog(@"%lu",(unsigned long)[[TheDict valueForKey:@"DataDetail"]  count]);
        
        
        if (ary_foodnowlist.count==0)
        {
            for(int i = 0; i < [[TheDict valueForKey:@"DataDetail"]  count] ; i++)
            {
                [ary_foodnowlist addObject:[[TheDict valueForKey:@"DataDetail"] objectAtIndex:i] ];
            }
            
            
        }
        else
        {
            
            for(int i = 0; i < [[TheDict valueForKey:@"DataDetail"]  count] ; i++)
            {
                [arr_PageArray addObject:[[TheDict valueForKey:@"DataDetail"] objectAtIndex:i] ];
            }
            
            for (int i=0; i<[arr_PageArray count]; i++)
            {
                
                if([ary_foodnowlist containsObject:[arr_PageArray objectAtIndex:i]])
                {
                    
                }
                else
                {
                    [ary_foodnowlist addObject:[arr_PageArray objectAtIndex:i]];
                }
                
                
            }
            
        }
        
        
        
        if (ary_foodnowlist.count>0)
        {
            
            if (lastSeen_index==0)
            {
                
                
            }
            else
            {
                
                if (ary_foodnowlist.count>lastSeen_index)
                {
                    if ([str_foodtype isEqualToString:@"1"])
                    {
                        [table_for_items_in_later reloadData];
                        
                    }
                    else{
                        [table_for_items reloadData];
                        [table_for_itemsfilter reloadData];
                        
                        
                    }
                    
                }
                else
                {
                    
                }
                
                
            }
            
        }
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
      [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        if ([str_foodtype isEqualToString:@"1"])
        {
            [table_for_items_in_later reloadData];
            [table_for_itemsfilter reloadData];
            
        }
        else{
            [table_for_items reloadData];
            [table_for_itemsfilter reloadData];
            
        }
        
    }
    
}

-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}


-(void)AFfoodnowNowlist
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    NSDictionary *params =@{
                            
                            @"uid"                              :   [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"food_type"                 :  str_foodtype,
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kfoodnow_later
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponsefoodnowNowlist:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFfoodnowNowlist];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponsefoodnowNowlist :(NSDictionary * )TheDict
{
    NSLog(@"foodlist: %@",TheDict);
    
    [ary_foodnowlist removeAllObjects];
    [table_for_items_in_later reloadData];
    [table_for_items reloadData];
    [table_for_itemsfilter reloadData];
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DataDetail"] count]; i++)
        {
            
            [ary_foodnowlist addObject:[[TheDict valueForKey:@"DataDetail"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    if ([str_foodtype isEqualToString:@"1"])
    {
        [table_for_items reloadData];
        [table_for_itemsfilter reloadData];
        
    }
    else{
        [table_for_items reloadData];
        [table_for_itemsfilter reloadData];
        
    }
    
    
}


#pragma parameters for FoodNow
-(void)AFFoodLater
{


    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];

    NSURL *url = [NSURL URLWithString:kBaseUrl];


    // NSString *dateOfBirth = self.txt_DateBirth.text;

    //    NSMutableString * str_mealtype_in_foodnow = [[NSMutableString alloc] init];
    //
    //    for (int i=0; i<[Arr_temp_favoritecuisin_infoodnow count]; i++)
    //    {
    //
    //
    //        if (i==[Arr_temp_favoritecuisin_infoodnow count]-1)
    //        {
    //            [str_mealtype_in_foodnow  appendString:[NSString stringWithFormat:@"%@",[[Arr_temp_favoritecuisin_infoodnow objectAtIndex:i] valueForKey:@"CuisineID"]]];
    //        }
    //        else
    //        {
    //            [str_mealtype_in_foodnow  appendString:[NSString stringWithFormat:@"%@||",[[Arr_temp_favoritecuisin_infoodnow objectAtIndex:i] valueForKey:@"CuisineID"]]];
    //        }
    //
    //
    //    }



    NSMutableString * str_favoratecuisenid_in_foodlater = [[NSMutableString alloc] init];

    for (int i=0; i<[array_temp_favorate_cuisines_in_foodlater count]; i++)
    {


        if (i==[array_temp_favorate_cuisines_in_foodlater count]-1)
        {
            [str_favoratecuisenid_in_foodlater  appendString:[NSString stringWithFormat:@"%@",[[array_temp_favorate_cuisines_in_foodlater objectAtIndex:i] valueForKey:@"CuisineID"]]];
        }
        else
        {
            [str_favoratecuisenid_in_foodlater  appendString:[NSString stringWithFormat:@"%@,",[[array_temp_favorate_cuisines_in_foodlater objectAtIndex:i] valueForKey:@"CuisineID"]]];
        }


    }

    NSMutableString * str_allcuisenid_In_foodlater = [[NSMutableString alloc] init];

    for (int i=0; i<[array_temp_all_cuisine_in_foodlater count]; i++)
    {


        if (i==[array_temp_all_cuisine_in_foodlater count]-1)
        {
            [str_allcuisenid_In_foodlater  appendString:[NSString stringWithFormat:@"%@",[[array_temp_all_cuisine_in_foodlater objectAtIndex:i] valueForKey:@"CuisineID"]]];
        }
        else
        {
            [str_allcuisenid_In_foodlater  appendString:[NSString stringWithFormat:@"%@,",[[array_temp_all_cuisine_in_foodlater objectAtIndex:i] valueForKey:@"CuisineID"]]];
        }

    }

    NSMutableString * str_Course_in_foodlater = [[NSMutableString alloc] init];

    for (int i=0; i<[array_temp_course_in_food_later count]; i++)
    {


        if (i==[array_temp_course_in_food_later count]-1)
        {
            [str_Course_in_foodlater  appendString:[NSString stringWithFormat:@"%@",[[array_temp_course_in_food_later objectAtIndex:i] valueForKey:@"DishCourseID"]]];
        }
        else
        {
            [str_Course_in_foodlater  appendString:[NSString stringWithFormat:@"%@,",[[array_temp_course_in_food_later objectAtIndex:i] valueForKey:@"DishCourseID"]]];
        }

    }


    NSMutableString *str_dietryids_in_foodlater = [[NSMutableString alloc] init];

    for (int i=0; i<[array_temp_dietary_restryction_infoodlater count]; i++)
    {


        if (i==[array_temp_dietary_restryction_infoodlater count]-1)
        {
            [str_dietryids_in_foodlater  appendString:[NSString stringWithFormat:@"%@",[[array_temp_dietary_restryction_infoodlater objectAtIndex:i] valueForKey:@"DietaryID"]]];
        }
        else
        {
            [str_dietryids_in_foodlater  appendString:[NSString stringWithFormat:@"%@,",[[array_temp_dietary_restryction_infoodlater objectAtIndex:i] valueForKey:@"DietaryID"]]];
        }


    }
    NSMutableString *str_servingtype = [[NSMutableString alloc] init];
    
    for (int i=0; i<[ary_foodlater_serveinglater count]; i++)
    {
        
        
        if (i==[ary_foodlater_serveinglater count]-1)
        {
            [str_servingtype  appendString:[NSString stringWithFormat:@"%@",[ary_foodlater_serveinglater objectAtIndex:i]]];
        }
        else
        {
            [str_servingtype  appendString:[NSString stringWithFormat:@"%@,",[ary_foodlater_serveinglater objectAtIndex:i]]];
        }
        
        
    }

    
    NSString*str_meal;
    str_meal = @"NO";
    
    if ([array_temp count]>0)
    {
        if ([array_temp count]==2)
        {
            str_meal = @"NO";
            
        }
        else{
            
        }
        str_meal = @"YES";
        
    }
    else
    {
        str_meal = @"NO";
        
    }
    
    [formatter setDateFormat:@"yyyy/MM/dd"];
    //    @"d  d   m  m   y  y  y  y"
    NSString *str=[formatter stringFromDate:datePicker.date];
    
    [formattertime setDateFormat:@"HH:mm"];
    NSString*dateString;
    dateString = [formattertime stringFromDate:datePickertime.date];
    
    int page =PagenoInt;
    
    if (page == (int)nil )
    {
        page=0;
    }
    

    
    NSDictionary *params;
    
    if ([[NSString stringWithFormat:@"%@",str_foodtype]isEqualToString:@"1"])
    {
        
        if ([str_meal isEqualToString:@"NO"])
        {
            params =@{
                      @"uid"                       :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                      @"page_number"               : [NSString stringWithFormat:@"%d",page],
                      @"food_type"                 :  @"0",
                      @"lat"                       :  str_Latitude,
                      @"long"                      :  str_Longitude,
                      @"serving_type"              :  str_servingtype,
                      @"keyword"                   :  txt_search.text,
                      @"cuisenes"                  :  str_allcuisenid_In_foodlater,
                      @"course"                    :  str_Course_in_foodlater,
                      @"dietry_restrictions"       :  str_dietryids_in_foodlater,
                      @"sort_by"                   :  str_sortbylater,
                      @"keyword"                   : txt_search2.text,
                      @"serve_date"                : str,
                      @"serve_time"                : dateString
                      

                      };
            
        }
        else{
            params =@{
                      @"uid"                       :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                      @"page_number"               :  [NSString stringWithFormat:@"%d",page],
                      @"food_type"                 :  @"0",
                      @"lat"                       :  str_Latitude,
                      @"long"                      :  str_Longitude,
                      @"serving_type"              :  str_servingtype,
                      @"keyword"                   :  txt_search.text,
                      @"cuisenes"                  :  str_allcuisenid_In_foodlater,
                      @"course"                    :  str_Course_in_foodlater,
                      @"dietry_restrictions"       :  str_dietryids_in_foodlater,
                      @"sort_by"                   :  str_sortbylater,
                      @"meal_type"                 :  str_servelaterdishtype,
                      @"keyword"                   : txt_search2.text,
                      @"serve_date"                : str,
                      @"serve_time"                : dateString

                      };
        }
        
        
    }
    else{
        params =@{
                  @"uid"                       :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                  @"page_number"               :  @"1",
                  @"food_type"                 :  @"0",
                  @"lat"                       :  str_Latitude,
                  @"long"                      :  str_Longitude,
                  @"serving_type"              :  str_servingtype,
                  @"keyword"                   :  txt_search.text,
                  @"cuisenes"                  :  str_allcuisenid_In_foodlater,
                  @"course"                    :  str_Course_in_foodlater,
                  @"dietry_restrictions"       :  str_dietryids_in_foodlater,
                  @"sort_by"                   :  str_sortbylater,
                  @"meal_type"                 :  str_servelaterdishtype

                  
                  
                  };
        
    }
    


    //===========================================AFNETWORKING HEADER

    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];


    // ===============================SIMPLE REQUEST

    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kUserSearchFoodNow
                                                      parameters:params];

    NSLog(@"request =%@",request);



    //====================================================RESPONSE

    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];

    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {

    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];

        NSLog(@"JSON = %@",JSON);
        [delegate.activityIndicator stopAnimating];

        [self ResponseFoodLater:JSON];

    }
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {

         [delegate.activityIndicator stopAnimating];

         [self.view setUserInteractionEnabled:YES];
         if([operation.response statusCode] == 406)
         {

             return;
         }

         if([operation.response statusCode] == 403)
         {
             NSLog(@"Upload Failed");
             return;
         }
         if ([[operation error] code] == -1009)
         {
             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                          message:@"Please check your internet connection"
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
             [av show];


         }
         else if ([[operation error] code] == -1001)
         {
             NSLog(@"Successfully Registered");
             [self AFFoodLater];

         }
     }];
    [operation start];

}


-(void)ResponseFoodLater:(NSDictionary * )TheDict
{
    NSLog(@"foodlist: %@",TheDict);
    
    [ary_foodnowlist removeAllObjects];
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DataDetail"] count]; i++)
        {
            
            [ary_foodnowlist addObject:[[TheDict valueForKey:@"DataDetail"] objectAtIndex:i]];
            
        }
        
        if ([str_foodtype isEqualToString:@"1"])
        {
            [table_for_items_in_later reloadData];
            [table_for_itemsfilter reloadData];
            
        }
        else{
            [table_for_items reloadData];
            [table_for_itemsfilter reloadData];
            
            
        }
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        if ([str_foodtype isEqualToString:@"1"])
        {
            [table_for_items_in_later reloadData];
            
        }
        else{
            [table_for_items reloadData];
            [table_for_itemsfilter reloadData];
            
        }
        
    }
    

}

#pragma mark UIScrollView Delegate Method
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView ==table_for_items_in_later)
    {
        
        
        NSArray *visibleRows = [table_for_items_in_later visibleCells];
        UITableViewCell *lastVisibleCell = [visibleRows lastObject];
        NSIndexPath *path = [table_for_items_in_later indexPathForCell:lastVisibleCell];
        if (scrollView.contentOffset.y <= 0)
        {
            //   PagenoInt = 0;
        }
        NSLog(@"pagelimit::%f",pagelimit);
        NSLog(@"PagenoInt::%f",PagenoInt);
        lastSeen_index=[[NSString stringWithFormat:@"%ld",(long)path.row] integerValue];
        if (PagenoInt < pagelimit)
        {
            
          
            PagenoInt = PagenoInt +1;
            
            [self AFFoodLater];
        
        }
        
        
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
