//
//  Annotations.h
//  RoadRunner
//
//  Created by i-phone on 12/10/14.
//  Copyright (c) 2014 abc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <MapKit/MapKit.h>

#import <CoreLocation/CoreLocation.h>

@interface Annotations : NSObject<MKAnnotation>


@property (nonatomic, strong) UIImage *logo;
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;


@end
