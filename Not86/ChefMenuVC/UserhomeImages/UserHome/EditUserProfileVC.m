//
//  UserProfileVC.m
//  Not86
//
//  Created by Admin on 08/09/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "UserProfileVC.h"
#import "Define.h"
#import "AppDelegate.h"
#import "Define.h"
#import "UIImageView+AFNetworking.h"
#import "AddToCartFoodNowVC.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


@interface UserProfileVC ()<UITextViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDelegate,UIScrollViewDelegate>
{
    UIImageView *img_header;
    UIScrollView * scroll;
    UIImageView *img_bg;
    
    UIImageView * img_strip;
    UIButton * btn_personal;
    UIView * view_personal;
    
    UITextView*txtview_adddescription;
    UITableView *img_table;
    NSMutableArray *ary_displaynames;
    NSMutableArray *ary_displayAddress;
    NSMutableArray *array_img;
    UITextView *set_names;
    NSMutableArray *aryray_text_in_cell ;
    
    UIImageView * img_strip2;
    UIButton * btn_on_adress;
    UIView * view_for_address;
    UITableView *table_for_address;
    NSMutableArray *array_home_work_address;
    NSMutableArray *array_address;
    
    UIImageView * img_strip3;
    UIButton * btn_on_fodd_info;
    UIView * view_for_foodinfo;
    UITableView * table_for_food_info;
    NSMutableArray * array_icons_in_food_info;
    NSMutableArray * array_lable_in_food_info;
    NSMutableArray * array_items_in_food_info;
    UICollectionViewFlowLayout *layout;
    UICollectionView *collView_serviceDirectory;
    
    UICollectionViewFlowLayout *layout2;
    UICollectionView *collView_serviceDirectory2;
    
    
    UIImageView * img_strip4;
    UIButton * btn_on_favorite;
    UIView * view_favorites;
    UIImageView * img_strip_for_items;
    UIView * view_for_items;
    UIImageView * img_rect;
    UILabel *text_distance;
    UIButton *btn_on_distance;
    UITableView *table_short_favorite_items;
    NSMutableArray * array_short_items;
    UILabel * lbl_short_items;
    UITableView * table_items_in_favorites;
    NSMutableArray * array_items_name;
    NSMutableArray *ary_itemsinformation;
    NSMutableArray *ary_Chefinformation;
    NSMutableArray * array_items_imgs;
    NSMutableArray * array_icon_hart;
    NSMutableArray * array_rating_number;
    NSMutableArray * array_icon_cart;
    NSMutableArray * array_distance;
    NSMutableArray * array_seving_type;
    NSMutableArray * array_dietary_halal;
    NSMutableArray * array_dietary_cow;
    NSMutableArray * array_dietary_fronce;
    NSMutableArray * array_doller_rate;
    NSMutableArray *  array_icon_take_out;
    NSMutableArray * array_icon_delivery;
    NSMutableArray * array_icon_now;
    NSMutableArray * array_icon_chef_menu;
    NSMutableArray * array_likes;
    NSMutableArray * array_time_and_date;
    
    UIImageView *  img_strip_for_chef;
    UIView * view_for_chef_side;
    UITableView *table_for_chef_in_favorites;
    NSMutableArray * array_chef_img;
    NSMutableArray * array_chef_name;
    NSMutableArray * ary_FoodinFo;
    NSString *str_favorite_type;
    NSMutableArray *ary_HomeAddress;
    
    int selectedindex;
    NSIndexPath *indexSelected;
    
    
    
    AppDelegate *delegate;
    
}


@end

@implementation UserProfileVC

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateBody];
    [self integrateHeader];
    ary_FoodinFo=[[NSMutableArray alloc]init];
    aryray_text_in_cell =[[NSMutableArray alloc]init];
    ary_displayAddress=[[NSMutableArray alloc]init];
    array_items_name=[NSMutableArray new];
    array_chef_name=[NSMutableArray new];
    array_chef_img=[NSMutableArray new];
    ary_itemsinformation=[NSMutableArray new];
    ary_Chefinformation=[NSMutableArray new];
    
    
    ary_HomeAddress=[NSMutableArray new];
    
    str_favorite_type=@"Dish";
    selectedindex=-1;
    indexSelected = nil;
    
    
    
    ary_displaynames =[[NSMutableArray alloc]initWithObjects:@"Username",@"Full Name",@"Date Of Birth",@"Email address",@"Mobile no.",@"Paypal Account", nil];
    array_img = [[NSMutableArray alloc]initWithObjects:@"user-des@2x.png",@"user-des@2x.png",@"icon-date-of-birth@2x.png",@"icon-msg@2x.png",@"icon-mobile@2x.png",@"icon-paypal@2x.png" ,nil];
    //    aryray_text_in_cell = [[NSMutableArray alloc]initWithObjects:@"jameslikestoeat",@"James Doe",@"14 june 1987 ",@"james@doe.com",@"+7627899883",@"Charles@paypal.com", nil];
    
    //user address
    array_home_work_address =[[NSMutableArray alloc]initWithObjects:@"Home Address",@"Work Address", nil];
    array_address =[[NSMutableArray alloc]initWithObjects:@"Cartel St, Sams City, Paris\n743659",@"Smith St,Suntec City,Paris\n743844", nil];
    
    //user food info
    array_icons_in_food_info = [[NSMutableArray alloc]initWithObjects:@"icon-order-food@2x.png",@"icon-allo_food@2x.png",@"icon-restict_food@2x.png",nil];
    array_lable_in_food_info =[[NSMutableArray alloc]initWithObjects:@"Favorite Cuisines",@"Dietary Restrictions",@"Food Allergies", nil];
    //    array_items_in_food_info =[[NSMutableArray alloc]initWithObjects:@"Chainese,Mexican,Italian,Korean",@"Vegetarian,Kosher ",@"Nuts,Shellfish", nil];
    
    //user favorites
    array_short_items = [[NSMutableArray alloc]initWithObjects:@"Distance",@"Course",@"Price", nil];
    //array-for-items
    //    array_items_name =[[NSMutableArray alloc]initWithObjects:@"Steamed Thai otah",@"Rasberry custored", nil];
    array_items_imgs = [[NSMutableArray alloc]initWithObjects:@"dish1-img@2x.png",@"dish2-img@2x.png", nil];
    array_icon_hart = [[NSMutableArray alloc]initWithObjects:@" favorite-icon@2x.png",@" favorite-icon@2x.png",nil];
    array_rating_number = [[NSMutableArray alloc]initWithObjects:@"red3-icon@2x.png",@"red3-icon@2x.png",nil];
    array_icon_cart = [[NSMutableArray alloc]initWithObjects:@"img-cart@2x.png",@"img-cart@2x.png" ,nil];
    array_distance = [[NSMutableArray alloc]initWithObjects:@"5 km",@"10 km",nil];
    array_seving_type = [[NSMutableArray alloc]initWithObjects:@"",@"",nil];
    array_dietary_halal = [[NSMutableArray alloc]initWithObjects: @"halal-icon@2x.png",@"halal-icon@2x.png",nil];
    array_dietary_cow = [[NSMutableArray alloc]initWithObjects:@"cow-icon@2x.png",@"cow-icon@2x.png",nil];
    array_dietary_fronce = [[NSMutableArray alloc]initWithObjects:@"fronce-icon@2x.png",@"fronce-icon@2x.png",nil];
    array_doller_rate = [[NSMutableArray alloc]initWithObjects:@"$14.90",@"$10.90",nil];
    array_icon_take_out = [[NSMutableArray alloc]initWithObjects:@"take-icon@2x.png",@"take-icon@2x.png" ,nil];
    array_icon_delivery = [[NSMutableArray alloc]initWithObjects:@"deliver-icon@2x.png",@"deliver-icon@2x.png", nil];
    array_icon_now = [[NSMutableArray alloc]initWithObjects:@"now-icon@2x.png",@"now-icon@2x.png",nil];
    array_icon_chef_menu = [[NSMutableArray alloc]initWithObjects:@"chef-menu-icon@2x.png",@"chef-menu-icon@2x.png" ,nil];
    array_likes = [[NSMutableArray alloc]initWithObjects:@"87.4%",@"90.4%",nil];
    array_time_and_date = [[NSMutableArray alloc]initWithObjects:@"Added on 17/5/2015, 9:05:PM",@"Added on 17/5/2015, 9:05:PM", nil];
    
    //array_chef-name-and-chef-images
    
    //    array_chef_name =[[NSMutableArray alloc]initWithObjects:@"Rahaman Bensly",@"Robert bensly", nil];
    //    array_chef_img = [[NSMutableArray alloc]initWithObjects:@"img-chef2@2x.png",@"img-chef2@2x.png", nil];
    
    
    
    
    
    
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    //    [self UserProfileFavorites];
    
    [self UserProfileInfo];
    //    [self UserProfileAddress];
}
-(void)integrateHeader
{
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu.frame = CGRectMake(10,13,25,20);
    icon_menu.backgroundColor = [UIColor clearColor];
    [icon_menu addTarget:self action:@selector(btn_back_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_menu setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_menu];
    
    
    UILabel *lbl_my_profile = [[UILabel alloc]init];
    lbl_my_profile.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 150, 45);
    lbl_my_profile.text = @"My Profile";
    lbl_my_profile.font = [UIFont fontWithName:kFont size:20];
    lbl_my_profile.textColor = [UIColor whiteColor];
    lbl_my_profile.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_my_profile];
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}

-(void)integrateBody
{
    
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    scroll.frame = CGRectMake(0, 51, WIDTH, 450);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    img_bg = [[UIImageView alloc]init];
    //   img_bg .frame = CGRectMake(0,CGRectGetMaxY( img_header.frame), WIDTH, 220);
    [img_bg  setImage:[UIImage imageNamed:@"img-bg-pic-for-user@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg  setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg ];
    
    
    UIButton *user_img = [UIButton buttonWithType:UIButtonTypeCustom];
    //   user_img.frame = CGRectMake(120,CGRectGetMaxY( img_header.frame)-30,100,100);
    user_img.backgroundColor = [UIColor clearColor];
    [user_img addTarget:self action:@selector(btn_img_user_click:) forControlEvents:UIControlEventTouchUpInside];
    [user_img setImage:[UIImage imageNamed:@"img-profile@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:user_img];
    
    UILabel *lbl_user_name = [[UILabel alloc]init];
    lbl_user_name.frame = CGRectMake(120,CGRectGetMaxY(user_img.frame),300,45);
    lbl_user_name.text = @"James Doe";
    lbl_user_name.font = [UIFont fontWithName:kFontBold size:15];
    lbl_user_name.textColor = [UIColor whiteColor];
    lbl_user_name.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_user_name];
    
    
    
    UIButton *img_pencil = [UIButton buttonWithType:UIButtonTypeCustom];
    //   img_pencil.frame = CGRectMake(280,CGRectGetMaxY( img_header.frame)-30,20,20);
    img_pencil.backgroundColor = [UIColor clearColor];
    [img_pencil addTarget:self action:@selector(btn_img_pencil_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_pencil setImage:[UIImage imageNamed:@"icon-edit@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:img_pencil];
    
#pragma USER-PERSONAL-INFO
    
    UIButton *icon_user_info = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_user_info.frame = CGRectMake(40,CGRectGetMaxY( img_header.frame)+100,40,40);
    icon_user_info.backgroundColor = [UIColor clearColor];
    [icon_user_info addTarget:self action:@selector(btn_img_user_info_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_user_info setImage:[UIImage imageNamed:@"icon-user-info@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:icon_user_info];
    
    
    UILabel *lbl_personal_info = [[UILabel alloc]init];
    //  lbl_personal_info.frame = CGRectMake(24,CGRectGetMaxY(icon_user_info.frame)-10, 150,45);
    lbl_personal_info.text = @"Personal Info";
    lbl_personal_info.font = [UIFont fontWithName:kFont size:12];
    lbl_personal_info.textColor = [UIColor whiteColor];
    lbl_personal_info.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_personal_info];
    
    img_strip = [[UIImageView alloc]init];
    //img_strip.frame = CGRectMake(50,CGRectGetMaxY(lbl_personal_info.frame)+5, WIDTH/2-80, 3);
    [img_strip setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    img_strip.backgroundColor = [UIColor clearColor];
    [img_strip setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip];
    img_strip.hidden = NO;
    
    btn_personal = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_personal.frame = CGRectMake(25,CGRectGetMaxY( img_header.frame)+100,70,65);
    btn_personal.backgroundColor = [UIColor clearColor];
    [btn_personal addTarget:self action:@selector(btn_personal_info_strip_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg  addSubview:   btn_personal];
    
#pragma View-for-user-personal-info
    
    view_personal = [[UIView alloc]init];
    // view_personal.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,300);
    view_personal.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_personal];
    view_personal.hidden = NO;
    
    UIImageView *img_bg1 = [[UIImageView alloc]init];
    // img_bg1.frame = CGRectMake(0,CGRectGetMaxY( img_bg.frame), WIDTH, 285);
    [img_bg1  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg1  setUserInteractionEnabled:YES];
    [view_personal addSubview:img_bg1 ];
    
    
    txtview_adddescription = [[UITextView alloc]init];
    //   txtview_adddescription.frame = CGRectMake(20,20, 295,60);
    txtview_adddescription.scrollEnabled = YES;
    txtview_adddescription.userInteractionEnabled = YES;
    txtview_adddescription.font = [UIFont fontWithName:kFont size:12];
    txtview_adddescription.backgroundColor = [UIColor clearColor];
    txtview_adddescription.delegate = self;
    txtview_adddescription.textColor = [UIColor blackColor];
    //    txtview_adddescription.text =@"LOrem ipsumdotor sit amet.ne dicit virits corpor a per,ut illum liberavisse vet.At mei vidit conse cteture.in vitae definitionem per.tate vetear cliquado ei vim.";
    //    txtview_adddescription.text = [NSString stringWithFormat:@"%@",[[ary_displaynames objectAtIndex:0] valueForKey:@"About_Us"]];
    
    [img_bg1 addSubview:txtview_adddescription];
    
    UIImageView *img_line = [[UIImageView alloc]init];
    //    img_line .frame = CGRectMake(13,CGRectGetMaxY(txtview_adddescription.frame)+5, 275, 0.5);
    [img_line setImage:[UIImage imageNamed:@"line2-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_line setUserInteractionEnabled:YES];
    [img_bg1 addSubview:img_line ];
    
    
#pragma mark Tableview
    
    img_table= [[UITableView alloc] init ];
    // img_table.frame  = CGRectMake(13,CGRectGetMaxY(img_line.frame),290,250);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    img_table.backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:img_table];
    
    
    
#pragma USER-ADDRESS
    
    UIButton *icon_address = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_address.frame = CGRectMake(120,CGRectGetMaxY( img_header.frame)+100,40,40);
    icon_address.backgroundColor = [UIColor clearColor];
    [icon_address addTarget:self action:@selector(btn_address_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_address setImage:[UIImage imageNamed:@"icon-address@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:icon_address];
    
    UILabel *lbl_address_info = [[UILabel alloc]init];
    //   lbl_address_info.frame = CGRectMake(120,CGRectGetMaxY(icon_address.frame)-10, 150,45);
    lbl_address_info.text = @"Address";
    lbl_address_info.font = [UIFont fontWithName:kFont size:12];
    lbl_address_info.textColor = [UIColor whiteColor];
    lbl_address_info.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_address_info];
    
    img_strip2 = [[UIImageView alloc]init];
    //img_strip2.frame = CGRectMake(50,CGRectGetMaxY(lbl_personal_info.frame)+5, WIDTH/2-80, 3);
    [img_strip2 setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    img_strip2.backgroundColor = [UIColor clearColor];
    [img_strip2 setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip2];
    img_strip2.hidden = YES;
    
    btn_on_adress = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_adress.frame = CGRectMake(25,CGRectGetMaxY( img_header.frame)+100,70,65);
    btn_on_adress.backgroundColor = [UIColor clearColor];
    [btn_on_adress addTarget:self action:@selector(btn_address_strip_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg  addSubview:   btn_on_adress];
    
#pragma View-for-user-address
    
    view_for_address = [[UIView alloc]init];
    // view_for_address.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,300);
    view_for_address.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_for_address];
    view_for_address.hidden = YES;
    
    UIImageView *img_bg_for_address = [[UIImageView alloc]init];
    //img_bg_for_address .frame = CGRectMake(10,CGRectGetMaxY(img_bg.frame)+5, 300,104);
    [img_bg_for_address setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //    img_bg_for_address .backgroundColor = [UIColor redColor];
    [img_bg_for_address setUserInteractionEnabled:YES];
    [view_for_address addSubview:img_bg_for_address ];
    
    
    
    UIButton *icon_add = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_add.frame = CGRectMake(120,CGRectGetMaxY( img_bg1.frame)+30,75,75);
    icon_add.backgroundColor = [UIColor clearColor];
    [icon_add addTarget:self action:@selector(click_on_add_btn:)forControlEvents:UIControlEventTouchUpInside];
    [icon_add setImage:[UIImage imageNamed:@"icon_add@2x.png"] forState:UIControlStateNormal];
    [view_for_address addSubview:icon_add];
    
    
#pragma address table
    
    table_for_address = [[UITableView alloc]init ];
    //table_for_address.frame  = CGRectMake(0,0,290,250);
    [ table_for_address setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_address.delegate = self;
    table_for_address.dataSource = self;
    table_for_address.showsVerticalScrollIndicator = NO;
    table_for_address.backgroundColor = [UIColor clearColor];
    [img_bg_for_address addSubview: table_for_address];
    
    
    
#pragma USER-FOOD-INFO
    
    UIButton *icon_food_info = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_food_info.frame = CGRectMake(190,CGRectGetMaxY( img_header.frame)+100,40,40);
    icon_food_info.backgroundColor = [UIColor clearColor];
    [icon_food_info addTarget:self action:@selector(btn_food_info_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_food_info setImage:[UIImage imageNamed:@"icon-food_info@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:icon_food_info];
    
    UILabel *lbl_food_info = [[UILabel alloc]init];
    //   lbl_food_info .frame = CGRectMake(190,CGRectGetMaxY(icon_address.frame)-10, 150,45);
    lbl_food_info.text = @"Food Info";
    lbl_food_info.font = [UIFont fontWithName:kFont size:12];
    lbl_food_info.textColor = [UIColor whiteColor];
    lbl_food_info.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_food_info];
    
    img_strip3 = [[UIImageView alloc]init];
    //img_strip3.frame = CGRectMake(50,CGRectGetMaxY(lbl_personal_info.frame)+5, WIDTH/2-80, 3);
    [img_strip3 setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    img_strip3.backgroundColor = [UIColor clearColor];
    [img_strip3 setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip3];
    img_strip3.hidden = YES;
    
    btn_on_fodd_info = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_fodd_info.frame = CGRectMake(25,CGRectGetMaxY( img_header.frame)+100,70,65);
    btn_on_fodd_info.backgroundColor = [UIColor clearColor];
    [btn_on_fodd_info addTarget:self action:@selector(btn_foodinfo_strip_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg  addSubview:btn_on_fodd_info];
    
#pragma View-for-user-foodinfo
    
    view_for_foodinfo = [[UIView alloc]init];
    // view_for_foodinfo.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,300);
    view_for_foodinfo.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_for_foodinfo];
    view_for_foodinfo.hidden = YES;
    
    UIImageView *img_bg_for_food_info = [[UIImageView alloc]init];
    //img_bg_for_food_info .frame = CGRectMake(10,CGRectGetMaxY(img_bg.frame)+5, 300,154);
    [img_bg_for_food_info setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //img_bg_for_food_info .backgroundColor = [UIColor redColor];
    [img_bg_for_food_info setUserInteractionEnabled:YES];
    [view_for_foodinfo addSubview:img_bg_for_food_info ];
    
#pragma mark Tableview-for-foodinfo
    
    table_for_food_info = [[UITableView alloc] init ];
    //table_for_food_info.frame  = CGRectMake(0,0,290,250);
    [table_for_food_info setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_food_info.delegate = self;
    table_for_food_info.dataSource = self;
    table_for_food_info.showsVerticalScrollIndicator = NO;
    table_for_food_info.backgroundColor = [UIColor clearColor];
    [img_bg_for_food_info addSubview:table_for_food_info];
    
    
    
    
#pragma USER-FAVORITES
    
    UIButton *icon_favorite = [UIButton buttonWithType:UIButtonTypeCustom];
    //   icon_favorite.frame = CGRectMake(260,CGRectGetMaxY( img_header.frame)+100,40,40);
    icon_favorite.backgroundColor = [UIColor clearColor];
    [icon_favorite addTarget:self action:@selector(btn_favorite_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_favorite setImage:[UIImage imageNamed:@"icon-user-favorit@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:icon_favorite];
    
    UILabel *lbl_favorite = [[UILabel alloc]init];
    //   lbl_favorite.frame = CGRectMake(260,CGRectGetMaxY(icon_address.frame)-10, 150,45);
    lbl_favorite.text = @"Favorites";
    lbl_favorite.font = [UIFont fontWithName:kFont size:12];
    lbl_favorite.textColor = [UIColor whiteColor];
    lbl_favorite.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_favorite];
    
    img_strip4 = [[UIImageView alloc]init];
    // img_strip4.frame = CGRectMake(50,CGRectGetMaxY(lbl_personal_info.frame)+5, WIDTH/2-80, 3);
    [img_strip4 setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    img_strip4.backgroundColor = [UIColor clearColor];
    [img_strip4 setUserInteractionEnabled:YES];
    [img_bg addSubview: img_strip4];
    img_strip4.hidden = YES;
    
    btn_on_favorite = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_favorite.frame = CGRectMake(25,CGRectGetMaxY( img_header.frame)+100,70,65);
    btn_on_favorite.backgroundColor = [UIColor clearColor];
    [btn_on_favorite addTarget:self action:@selector(btn_favorites_strip_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_bg  addSubview: btn_on_favorite];
    
#pragma View-for-user-favorites
    
    view_favorites = [[UIView alloc]init];
    //view_favorites.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,300);
    view_favorites.backgroundColor=[UIColor clearColor];
    [scroll  addSubview:  view_favorites];
    view_favorites.hidden = YES;
    
    UILabel *text_items = [[UILabel alloc]init];
    // text_items  .frame = CGRectMake(70,10,200, 15);
    text_items  .text = @"Items";
    text_items  .font = [UIFont fontWithName:kFontBold size:15];
    text_items.userInteractionEnabled = YES;
    text_items  .backgroundColor = [UIColor clearColor];
    [view_favorites  addSubview:text_items ];
    
    img_strip_for_items = [[UIImageView alloc]init];
    //img_strip_for_items.frame = CGRectMake(50,CGRectGetMaxY(text_chef.frame)+5, WIDTH/2-80, 3);
    [img_strip_for_items setImage:[UIImage imageNamed:@"black strip@2x.png"]];
    [img_strip_for_items setUserInteractionEnabled:YES];
    [view_favorites addSubview:img_strip_for_items];
    img_strip_for_items .hidden = NO;
    
    UIButton * btn_on_items = [UIButton buttonWithType:UIButtonTypeCustom];
    //btn_on_items.frame = CGRectMake(0,0, WIDTH/2, 40);
    btn_on_items .backgroundColor = [UIColor clearColor];
    [btn_on_items  addTarget:self action:@selector(click_on_items_btn:)forControlEvents:UIControlEventTouchUpInside];
    [view_favorites   addSubview:btn_on_items];
    
    
#pragma sub_view_items
    
    
    view_for_items = [[UIView alloc]init];
    //view_for_items.frame=CGRectMake(0,CGRectGetMaxY(btn_items.frame),WIDTH,250);
    view_for_items.backgroundColor=[UIColor clearColor];
    [view_favorites addSubview: view_for_items];
    view_for_items .hidden = NO;
    
    img_rect = [[UIImageView alloc]init];
    // img_rect.frame = CGRectMake(0,10, WIDTH+5, 40);
    [img_rect setUserInteractionEnabled:YES];
    img_rect.image=[UIImage imageNamed:@"img-bg@2x.png"];
    [view_for_items addSubview:img_rect];
    
    text_distance = [[UILabel alloc]init];
    //   text_distance.frame = CGRectMake(20,10,200, 15);
    text_distance.text = @"Distance";
    text_distance.font = [UIFont fontWithName:kFontBold size:15];
    // text_chef.textColor = [UIColor redColor];
    text_distance.backgroundColor = [UIColor clearColor];
    [img_rect addSubview:text_distance];
    
    UIButton *icon_drop_dow = [UIButton buttonWithType:UIButtonTypeCustom];
    // icon_drop_dow.frame = CGRectMake(280,13,20,20);
    icon_drop_dow .backgroundColor = [UIColor clearColor];
    [icon_drop_dow addTarget:self action:@selector(click_on_dropdown_btn:) forControlEvents:UIControlEventTouchUpInside];
    [icon_drop_dow setImage:[UIImage imageNamed:@"dropdown@2x.png"] forState:UIControlStateNormal];
    [img_rect   addSubview:icon_drop_dow];
    
    
    btn_on_distance = [UIButton buttonWithType:UIButtonTypeCustom];
    //btn_on_distance .frame = CGRectMake(0,0, WIDTH, 40);
    btn_on_distance .backgroundColor = [UIColor clearColor];
    [btn_on_distance   addTarget:self action:@selector(click_on_distance_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_rect   addSubview:btn_on_distance ];
    
    
#pragma mark Tableview-for-items-in-favorites
    
    table_items_in_favorites = [[UITableView alloc] init ];
    //table_items_in_favorites.frame  = CGRectMake(13,CGRectGetMaxY(img_rect.frame),296,175);
    [table_items_in_favorites setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_items_in_favorites.delegate = self;
    table_items_in_favorites.dataSource = self;
    table_items_in_favorites.showsVerticalScrollIndicator = NO;
    table_items_in_favorites.backgroundColor = [UIColor clearColor];
    [view_for_items addSubview: table_items_in_favorites];
    
#pragma mark Tableview-for-short-favorites
    
    table_short_favorite_items = [[UITableView alloc] init ];
    //table_short_favorite_items.frame  = CGRectMake(13,CGRectGetMaxY(img_rect.frame),296,175);
    [table_short_favorite_items setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_short_favorite_items.delegate = self;
    table_short_favorite_items.dataSource = self;
    table_short_favorite_items.showsVerticalScrollIndicator = NO;
    table_short_favorite_items.backgroundColor = [UIColor clearColor];
    [view_for_items addSubview: table_short_favorite_items];
    table_short_favorite_items.hidden = YES;
    
    
    
    
    //chef-actions
    
    UILabel *text_chef = [[UILabel alloc]init];
    //    text_chef.frame = CGRectMake(WIDTH/2+50,10,100, 15);
    text_chef.text = @"Chefs";
    text_chef.font = [UIFont fontWithName:kFontBold size:15];
    // text_chef.textColor = [UIColor redColor];
    text_chef.userInteractionEnabled = YES;
    text_chef.backgroundColor = [UIColor clearColor];
    [view_favorites addSubview:text_chef];
    
    img_strip_for_chef = [[UIImageView alloc]init];
    //img_strip_for_chef.frame = CGRectMake(WIDTH/2+30, CGRectGetMaxY(text_chef.frame)+5,WIDTH/2-80, 3);
    [img_strip_for_chef setImage:[UIImage imageNamed:@"black strip@2x.png"]];
    [img_strip_for_chef setUserInteractionEnabled:YES];
    [view_favorites addSubview:img_strip_for_chef];
    img_strip_for_chef.hidden = YES;
    
    UIButton * btn_on_chef_labl = [UIButton buttonWithType:UIButtonTypeCustom];
    //btn_on_chef_labl.frame = CGRectMake(WIDTH/2,0, WIDTH/2, 40);
    btn_on_chef_labl .backgroundColor = [UIColor clearColor];
    [btn_on_chef_labl  addTarget:self action:@selector(click_on_labl_chef_btn:) forControlEvents:UIControlEventTouchUpInside];
    [view_favorites   addSubview:btn_on_chef_labl];
    
#pragma sub_view_chef
    
    
    view_for_chef_side = [[UIView alloc]init];
    //view_for_chef_side.frame=CGRectMake(0,CGRectGetMaxY(btn_items.frame),WIDTH,250);
    view_for_chef_side.backgroundColor=[UIColor clearColor];
    [view_favorites addSubview: view_for_chef_side];
    view_for_chef_side.hidden = YES;
    
#pragma mark Tableview-for-chef
    
    table_for_chef_in_favorites = [[UITableView alloc] init ];
    //table_for_chef_in_favorites.frame  = CGRectMake(13,CGRectGetMaxY(img_rect.frame),296,175);
    [table_for_chef_in_favorites setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_chef_in_favorites.delegate = self;
    table_for_chef_in_favorites.dataSource = self;
    table_for_chef_in_favorites.showsVerticalScrollIndicator = NO;
    table_for_chef_in_favorites.backgroundColor = [UIColor clearColor];
    [view_for_chef_side addSubview: table_for_chef_in_favorites];
    
    
    
    
    
    if (IS_IPHONE_6Plus)
    {
        
        scroll.frame = CGRectMake(0,0, WIDTH, 800);
        
        
        img_bg .frame = CGRectMake(0,0, WIDTH, 285);
        user_img.frame = CGRectMake(130,CGRectGetMaxY( img_header.frame)+60,115,115);
        lbl_user_name.frame = CGRectMake(155,CGRectGetMaxY(user_img.frame)-5,300,45);
        img_pencil.frame = CGRectMake(365,CGRectGetMaxY(img_header.frame)+65,25,25);
        
        icon_user_info.frame = CGRectMake(40,CGRectGetMaxY(user_img.frame)+35,40,40);
        lbl_personal_info.frame = CGRectMake(25,CGRectGetMaxY(icon_user_info.frame)-10, 150,45);
        img_strip.frame = CGRectMake(0,CGRectGetMaxY(lbl_personal_info.frame)-4,100, 4);
        btn_personal.frame = CGRectMake(0,CGRectGetMaxY(user_img.frame)+35,110,74);
        
        view_personal.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,700);
        img_bg1.frame = CGRectMake(0,10,WIDTH, 440);
        txtview_adddescription.frame = CGRectMake(20,20, 360,60);
        img_line .frame = CGRectMake(25,CGRectGetMaxY(txtview_adddescription.frame)+5, 350, 0.5);
        img_table.frame  = CGRectMake(13,CGRectGetMaxY(img_line.frame),WIDTH,330);
        
        
        icon_address.frame = CGRectMake(CGRectGetMaxX(icon_user_info.frame)+60,CGRectGetMaxY(user_img.frame)+35,40,40);
        lbl_address_info.frame = CGRectMake(CGRectGetMidX(lbl_personal_info.frame)+40,CGRectGetMaxY(icon_address.frame)-10, 150,45);
        img_strip2.frame = CGRectMake(125,CGRectGetMaxY(lbl_address_info.frame)-4, 70, 4);
        btn_on_adress.frame = CGRectMake(CGRectGetMaxX(btn_personal.frame),CGRectGetMaxY(user_img.frame)+35,90,74);
        view_for_address.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,500);
        img_bg_for_address .frame = CGRectMake(0,5,WIDTH,140);
        // lbl_delever.frame = CGRectMake(280,0, 100,45);
        table_for_address.frame  = CGRectMake(10,0,WIDTH-23,135);
        icon_add.frame = CGRectMake(170,CGRectGetMaxY(img_bg_for_address.frame)+10,75,75);
        
        
        icon_food_info.frame = CGRectMake(CGRectGetMaxX(icon_address.frame)+60,CGRectGetMaxY(user_img.frame)+35,40,40);
        lbl_food_info .frame = CGRectMake(CGRectGetMidX(lbl_address_info.frame)+25,CGRectGetMaxY(icon_address.frame)-10, 150,45);
        img_strip3.frame = CGRectMake(230,CGRectGetMaxY(lbl_food_info.frame)-4, 70, 4);
        btn_on_fodd_info.frame = CGRectMake(CGRectGetMaxX(btn_on_adress.frame),CGRectGetMaxY(user_img.frame)+35,85,74);
        view_for_foodinfo.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,400);
        img_bg_for_food_info .frame = CGRectMake(0,10, WIDTH,183);
        table_for_food_info.frame  = CGRectMake(10,5,WIDTH-23,170);
        
        
        
        icon_favorite.frame = CGRectMake(CGRectGetMaxX(icon_food_info.frame)+60,CGRectGetMaxY(user_img.frame)+35,40,40);
        lbl_favorite.frame = CGRectMake(CGRectGetMidX(lbl_food_info.frame)+25,CGRectGetMaxY(icon_address.frame)-10, 150,45);
        img_strip4.frame = CGRectMake(320,CGRectGetMaxY(lbl_food_info.frame)-4, 100, 4);
        btn_on_favorite.frame = CGRectMake(CGRectGetMaxX( btn_on_fodd_info.frame),CGRectGetMaxY(user_img.frame)+35,90,74);
        view_favorites.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame)+15,WIDTH,450);
        text_items  .frame = CGRectMake(80,10,200, 15);
        img_strip_for_items.frame = CGRectMake(60,CGRectGetMaxY(text_chef.frame)+34, 80, 4);
        btn_on_items.frame = CGRectMake(0,0, WIDTH/2, 40);
        
        view_for_items.frame=CGRectMake(0,CGRectGetMaxY(btn_on_items.frame),WIDTH,420);
        img_rect.frame = CGRectMake(-5,5, WIDTH+13, 60);
        text_distance.frame = CGRectMake(30,20,200, 15);
        icon_drop_dow.frame = CGRectMake(CGRectGetMaxX(text_distance.frame)+140,20,20,20);
        btn_on_distance .frame = CGRectMake(10,0, WIDTH-10, 60);
        table_short_favorite_items.frame  = CGRectMake(5,CGRectGetMaxY(img_rect.frame)-2,WIDTH-10,100);
        table_items_in_favorites.frame  = CGRectMake(8,CGRectGetMaxY(img_rect.frame),WIDTH-13,320);
        
        
        text_chef.frame = CGRectMake(WIDTH/2+70,10,100, 15);
        img_strip_for_chef.frame = CGRectMake(WIDTH/2+50, CGRectGetMaxY(text_chef.frame)+9,80, 4);
        btn_on_chef_labl.frame = CGRectMake(WIDTH/2,0, WIDTH/2, 40);
        view_for_chef_side.frame=CGRectMake(0,CGRectGetMaxY(btn_on_chef_labl.frame),WIDTH,400);
        table_for_chef_in_favorites.frame  = CGRectMake(5,10,WIDTH-10,360);
    }
    else if (IS_IPHONE_6)
    {
        
        scroll.frame = CGRectMake(0,0, WIDTH, 800);
        
        
        img_bg .frame = CGRectMake(0,0, WIDTH, 285);
        user_img.frame = CGRectMake(120,CGRectGetMaxY( img_header.frame)+60,115,115);
        lbl_user_name.frame = CGRectMake(145,CGRectGetMaxY(user_img.frame)-5,300,45);
        img_pencil.frame = CGRectMake(330,CGRectGetMaxY(img_header.frame)+65,25,25);
        
        icon_user_info.frame = CGRectMake(35,CGRectGetMaxY(user_img.frame)+35,40,40);
        lbl_personal_info.frame = CGRectMake(20,CGRectGetMaxY(icon_user_info.frame)-10, 150,45);
        img_strip.frame = CGRectMake(0,CGRectGetMaxY(lbl_personal_info.frame)-4,100, 4);
        btn_personal.frame = CGRectMake(0,CGRectGetMaxY(user_img.frame)+35,110,74);
        
        view_personal.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,700);
        img_bg1.frame = CGRectMake(0,0,WIDTH, 360);
        txtview_adddescription.frame = CGRectMake(20,20, 330,60);
        img_line .frame = CGRectMake(25,CGRectGetMaxY(txtview_adddescription.frame)+5, 320, 0.5);
        img_table.frame  = CGRectMake(13,CGRectGetMaxY(img_line.frame),WIDTH,260);
        
        
        icon_address.frame = CGRectMake(CGRectGetMaxX(icon_user_info.frame)+50,CGRectGetMaxY(user_img.frame)+35,40,40);
        lbl_address_info.frame = CGRectMake(CGRectGetMidX(lbl_personal_info.frame)+30,CGRectGetMaxY(icon_address.frame)-10, 150,45);
        img_strip2.frame = CGRectMake(120,CGRectGetMaxY(lbl_address_info.frame)-4, 70, 4);
        btn_on_adress.frame = CGRectMake(CGRectGetMaxX(btn_personal.frame),CGRectGetMaxY(user_img.frame)+35,90,74);
        view_for_address.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,400);
        img_bg_for_address .frame = CGRectMake(0,5,WIDTH,140);
        // lbl_delever.frame = CGRectMake(280,0, 100,45);
        table_for_address.frame  = CGRectMake(10,0,WIDTH-23,135);
        icon_add.frame = CGRectMake(140,CGRectGetMaxY(img_bg_for_address.frame)+10,75,75);
        
        
        icon_food_info.frame = CGRectMake(CGRectGetMaxX(icon_address.frame)+50,CGRectGetMaxY(user_img.frame)+35,40,40);
        lbl_food_info .frame = CGRectMake(CGRectGetMidX(lbl_address_info.frame)+15,CGRectGetMaxY(icon_address.frame)-10, 150,45);
        img_strip3.frame = CGRectMake(210,CGRectGetMaxY(lbl_food_info.frame)-4, 70, 4);
        btn_on_fodd_info.frame = CGRectMake(CGRectGetMaxX(btn_on_adress.frame),CGRectGetMaxY(user_img.frame)+35,85,74);
        view_for_foodinfo.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,400);
        img_bg_for_food_info .frame = CGRectMake(0,10, WIDTH,183);
        table_for_food_info.frame  = CGRectMake(10,5,WIDTH-23,170);
        
        
        
        icon_favorite.frame = CGRectMake(CGRectGetMaxX(icon_food_info.frame)+50,CGRectGetMaxY(user_img.frame)+35,40,40);
        lbl_favorite.frame = CGRectMake(CGRectGetMidX(lbl_food_info.frame)+15,CGRectGetMaxY(icon_address.frame)-10, 150,45);
        img_strip4.frame = CGRectMake(300,CGRectGetMaxY(lbl_food_info.frame)-4, 80, 4);
        btn_on_favorite.frame = CGRectMake(CGRectGetMaxX( btn_on_fodd_info.frame),CGRectGetMaxY(user_img.frame)+35,90,74);
        view_favorites.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame)+15,WIDTH,400);
        text_items  .frame = CGRectMake(70,10,200, 15);
        img_strip_for_items.frame = CGRectMake(50,CGRectGetMaxY(text_chef.frame)+34, 80, 4);
        btn_on_items.frame = CGRectMake(0,0, WIDTH/2, 40);
        
        view_for_items.frame=CGRectMake(0,CGRectGetMaxY(btn_on_items.frame),WIDTH,350);
        img_rect.frame = CGRectMake(-5,5, WIDTH+13, 60);
        text_distance.frame = CGRectMake(30,20,200, 15);
        icon_drop_dow.frame = CGRectMake(CGRectGetMaxX(text_distance.frame)+100,20,20,20);
        btn_on_distance .frame = CGRectMake(10,0, WIDTH-10, 60);
        table_short_favorite_items.frame  = CGRectMake(5,CGRectGetMaxY(img_rect.frame)-2,WIDTH-10,100);
        table_items_in_favorites.frame  = CGRectMake(5,CGRectGetMaxY(img_rect.frame),WIDTH-10,250);
        
        
        text_chef.frame = CGRectMake(WIDTH/2+50,10,100, 15);
        img_strip_for_chef.frame = CGRectMake(WIDTH/2+30, CGRectGetMaxY(text_chef.frame)+9,80, 4);
        btn_on_chef_labl.frame = CGRectMake(WIDTH/2,0, WIDTH/2, 40);
        view_for_chef_side.frame=CGRectMake(0,CGRectGetMaxY(btn_on_chef_labl.frame),WIDTH,350);
        table_for_chef_in_favorites.frame  = CGRectMake(5,10,WIDTH-10,350);
        
        
        
    }
    else
    {
        scroll.frame = CGRectMake(0,0, WIDTH, 800);
        
        
        img_bg .frame = CGRectMake(0,0, WIDTH, 285);
        user_img.frame = CGRectMake(100,CGRectGetMaxY( img_header.frame)+60,115,115);
        lbl_user_name.frame = CGRectMake(125,CGRectGetMaxY(user_img.frame)-5,300,45);
        img_pencil.frame = CGRectMake(275,CGRectGetMaxY(img_header.frame)+65,25,25);
        
        icon_user_info.frame = CGRectMake(25,CGRectGetMaxY(user_img.frame)+35,40,40);
        lbl_personal_info.frame = CGRectMake(12,CGRectGetMaxY(icon_user_info.frame)-10, 150,45);
        img_strip.frame = CGRectMake(0,CGRectGetMaxY(lbl_personal_info.frame)-4,85, 4);
        btn_personal.frame = CGRectMake(0,CGRectGetMaxY(user_img.frame)+35,85,74);
        
        view_personal.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,700);
        img_bg1.frame = CGRectMake(0,0,WIDTH, 360);
        txtview_adddescription.frame = CGRectMake(20,20, 275,50);
        img_line .frame = CGRectMake(20,CGRectGetMaxY(txtview_adddescription.frame)+5,270, 0.5);
        img_table.frame  = CGRectMake(13,CGRectGetMaxY(img_line.frame),WIDTH,260);
        
        
        icon_address.frame = CGRectMake(CGRectGetMaxX(icon_user_info.frame)+42,CGRectGetMaxY(user_img.frame)+35,40,40);
        lbl_address_info.frame = CGRectMake(CGRectGetMidX(lbl_personal_info.frame)+22,CGRectGetMaxY(icon_address.frame)-10, 150,45);
        img_strip2.frame = CGRectMake(105,CGRectGetMaxY(lbl_address_info.frame)-4, 60, 4);
        btn_on_adress.frame = CGRectMake(CGRectGetMaxX(btn_personal.frame),CGRectGetMaxY(user_img.frame)+35,90,74);
        view_for_address.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,400);
        img_bg_for_address .frame = CGRectMake(0,5,WIDTH,140);
        // lbl_delever.frame = CGRectMake(280,0, 100,45);
        table_for_address.frame  = CGRectMake(10,0,WIDTH-23,135);
        icon_add.frame = CGRectMake(120,CGRectGetMaxY(img_bg_for_address.frame)+10,75,75);
        
        
        icon_food_info.frame = CGRectMake(CGRectGetMaxX(icon_address.frame)+37,CGRectGetMaxY(user_img.frame)+35,40,40);
        lbl_food_info .frame = CGRectMake(CGRectGetMidX(lbl_address_info.frame)+3,CGRectGetMaxY(icon_address.frame)-10, 150,45);
        img_strip3.frame = CGRectMake(185,CGRectGetMaxY(lbl_food_info.frame)-4, 60, 4);
        btn_on_fodd_info.frame = CGRectMake(CGRectGetMaxX(btn_on_adress.frame),CGRectGetMaxY(user_img.frame)+35,74,74);
        view_for_foodinfo.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,400);
        img_bg_for_food_info .frame = CGRectMake(0,10, WIDTH,183);
        table_for_food_info.frame  = CGRectMake(10,5,WIDTH-23,170);
        
        
        
        icon_favorite.frame = CGRectMake(CGRectGetMaxX(icon_food_info.frame)+37,CGRectGetMaxY(user_img.frame)+35,40,40);
        lbl_favorite.frame = CGRectMake(CGRectGetMidX(lbl_food_info.frame),CGRectGetMaxY(icon_address.frame)-10, 150,45);
        img_strip4.frame = CGRectMake(260,CGRectGetMaxY(lbl_food_info.frame)-4, 80, 4);
        btn_on_favorite.frame = CGRectMake(CGRectGetMaxX( btn_on_fodd_info.frame),CGRectGetMaxY(user_img.frame)+35,90,74);
        view_favorites.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame)+15,WIDTH,470);
        text_items  .frame = CGRectMake(70,10,200, 15);
        img_strip_for_items.frame = CGRectMake(50,CGRectGetMaxY(text_chef.frame)+34, 80, 4);
        btn_on_items.frame = CGRectMake(0,0, WIDTH/2, 40);
        
        view_for_items.frame=CGRectMake(0,CGRectGetMaxY(btn_on_items.frame),WIDTH,470);
        img_rect.frame = CGRectMake(-5,5, WIDTH+13, 50);
        text_distance.frame = CGRectMake(30,20,200, 15);
        icon_drop_dow.frame = CGRectMake(CGRectGetMaxX(text_distance.frame)+60,20,20,20);
        btn_on_distance .frame = CGRectMake(10,0, WIDTH-10, 60);
        table_short_favorite_items.frame  = CGRectMake(5,CGRectGetMaxY(img_rect.frame)-2,WIDTH-10,100);
        table_items_in_favorites.frame  = CGRectMake(5,CGRectGetMaxY(img_rect.frame),WIDTH-10,250);
        
        
        text_chef.frame = CGRectMake(WIDTH/2+50,10,100, 15);
        img_strip_for_chef.frame = CGRectMake(WIDTH/2+30, CGRectGetMaxY(text_chef.frame)+9,80, 4);
        btn_on_chef_labl.frame = CGRectMake(WIDTH/2,0, WIDTH/2, 40);
        view_for_chef_side.frame=CGRectMake(0,CGRectGetMaxY(btn_on_chef_labl.frame),WIDTH,400);
        table_for_chef_in_favorites.frame  = CGRectMake(5,10,WIDTH-10,370);
    }
    [scroll setContentSize:CGSizeMake(0,1000)];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == img_table)
    {
        if ([aryray_text_in_cell count]==0)
        {
            return 0;
        }
        else{
            return [aryray_text_in_cell count]+6;
            
        }
        
        //         return   [aryray_text_in_cell count];
    }
    else if(tableView == table_for_address)
    {
        return   [ary_displayAddress count];
    }
    else if (tableView == table_for_food_info)
    {
        
        if ([ary_FoodinFo count]==0)
        {
            return 0;
        }
        else{
            return [ary_FoodinFo count]+2;
            
        }
        
        //        return [ary_FoodinFo count];
        
    }
    else if (tableView == table_short_favorite_items)
    {
        return [ array_short_items count];
    }
    else if (tableView == table_items_in_favorites)
    {
        return [ary_itemsinformation count];
    }
    else if (tableView == table_for_chef_in_favorites)
    {
        return [ary_Chefinformation count];
    }
    return 0;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == img_table)
    {
        return   1;
    }
    else if(tableView == table_for_address)
    {
        return   1;
    }
    else if (tableView == table_for_food_info)
    {
        return 1;
    }
    
    else if (tableView == table_short_favorite_items)
    {
        return 1;
    }
    else if (tableView == table_items_in_favorites)
    {
        return 1;
    }
    else if (tableView == table_for_chef_in_favorites)
    {
        return 1;
    }
    
    
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == img_table)
    {
        return   50;
    }
    else if(tableView == table_for_address)
    {
        return   75;
    }
    else if (tableView == table_for_food_info)
    {
        return 60;
    }
    else if (tableView == table_short_favorite_items)
    {
        return 50;
    }
    else if (tableView == table_items_in_favorites)
    {
        return 180;
    }
    
    else if (tableView == table_for_chef_in_favorites)
    {
        return 170;
    }
    
    
    
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    if (tableView == img_table)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        //   img_cellBackGnd.frame =  CGRectMake(0,2, 290, 50);
        //    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor whiteColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UIImageView *img_line = [[UIImageView alloc]init];
        //  img_line.frame =  CGRectMake(0,49, 290, 0.5);
        img_line.backgroundColor =[UIColor grayColor];
        [img_line setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line];
        
        
        UILabel *label_head_in_cell = [[UILabel alloc]init];
        //   label_head_in_cell.frame = CGRectMake(40,10,200, 15);
        
        label_head_in_cell.text = [NSString stringWithFormat:@"%@",[ary_displaynames objectAtIndex:indexPath.row]];
        label_head_in_cell.font = [UIFont fontWithName:kFontBold size:15];
        label_head_in_cell.textColor = [UIColor blackColor];
        label_head_in_cell.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:label_head_in_cell];
        
        UIImageView *info_imges_in_cell = [[UIImageView alloc]init];
        //   info_imges_in_cell.frame =  CGRectMake(0,10, 30, 30);
        [info_imges_in_cell setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_img objectAtIndex:indexPath.row]]]];
        info_imges_in_cell.backgroundColor =[UIColor clearColor];
        [info_imges_in_cell setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:info_imges_in_cell];
        
        
        UITextView *text_in_cell = [[UITextView alloc]init];
        //           text_in_cell.frame = CGRectMake(40,20,200, 20);
        
        
        if ([aryray_text_in_cell count]==0)
        {
            
        }
        else{
            if (indexPath.row == 0)
            {
                text_in_cell .text = [NSString stringWithFormat:@"%@",[[aryray_text_in_cell objectAtIndex:0] valueForKey:@"UserName"]];
                
            }
            else if (indexPath.row == 1)
            {
                
                text_in_cell .text = [NSString stringWithFormat:@"%@",[[aryray_text_in_cell objectAtIndex:0] valueForKey:@"Full_Name"]];
                
            }
            else if (indexPath.row == 2)
            {
                
                text_in_cell .text = [NSString stringWithFormat:@"%@",[[aryray_text_in_cell objectAtIndex:0] valueForKey:@"DateOfBirth"]];
                
            }
            else if (indexPath.row == 3)
            {
                
                text_in_cell .text = [NSString stringWithFormat:@"%@",[[aryray_text_in_cell objectAtIndex:0] valueForKey:@"Email_Address"]];
                
            }
            else if (indexPath.row == 4)
            {
                
                text_in_cell .text = [NSString stringWithFormat:@"%@",[[aryray_text_in_cell objectAtIndex:0] valueForKey:@"Mobile_Number"]];
                
            }
            else if (indexPath.row == 5)
            {
                
                text_in_cell .text = [NSString stringWithFormat:@"%@",[[aryray_text_in_cell objectAtIndex:0] valueForKey:@"Paypal_Account"]];
                
            }
            
            
            
        }
        text_in_cell.scrollEnabled = YES;
        text_in_cell.userInteractionEnabled = NO;
        text_in_cell.font = [UIFont fontWithName:kFont size:12];
        text_in_cell.backgroundColor = [UIColor clearColor];
        text_in_cell.delegate = self;
        text_in_cell.textColor = [UIColor blackColor];
        //        text_in_cell.text = [NSString stringWithFormat:@"%@",[ aryray_text_in_cell objectAtIndex:indexPath.row]];
        [img_cellBackGnd addSubview:text_in_cell];
        
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(10,2, WIDTH-45, 50);
            img_line.frame =  CGRectMake(0,49, 350, 0.5);
            label_head_in_cell.frame = CGRectMake(45,10,200, 15);
            info_imges_in_cell.frame =  CGRectMake(0,10, 30, 30);
            text_in_cell.frame = CGRectMake(40,20,200,30);
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(10,2, WIDTH-45, 50);
            img_line.frame =  CGRectMake(0,49, 320, 0.5);
            label_head_in_cell.frame = CGRectMake(45,10,200, 15);
            info_imges_in_cell.frame =  CGRectMake(0,10, 30, 30);
            text_in_cell.frame = CGRectMake(40,20,200, 23);
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, 290, 50);
            img_line.frame =  CGRectMake(6,49,270, 0.5);
            label_head_in_cell.frame = CGRectMake(40,10,200, 15);
            info_imges_in_cell.frame =  CGRectMake(10,10, 30, 30);
            text_in_cell.frame = CGRectMake(40,20,200, 20);
        }
        
    }
    else if(tableView == table_for_address)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        //  img_cellBackGnd.frame =  CGRectMake(0,2, 290, 50);
        //    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor whiteColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UIImageView *img_line = [[UIImageView alloc]init];
        //   img_line.frame =  CGRectMake(0,49, 290, 0.5);
        img_line.backgroundColor =[UIColor grayColor];
        [img_line setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line];
        
        //        ary_displayAddressary_displayAddress
        UILabel *lbl_home_r_work = [[UILabel alloc]init];
        //   lbl_home_r_work.frame = CGRectMake(40,10,200, 15);
        if (indexPath.row == 0)
        {
            lbl_home_r_work .text = [NSString stringWithFormat:@"%@",[[ary_displayAddress objectAtIndex:0] valueForKey:@"Address_Name"]];
            
        }
        else if (indexPath.row == 1)
        {
            
            lbl_home_r_work .text = [NSString stringWithFormat:@"%@",[[ary_displayAddress objectAtIndex:1] valueForKey:@"Address_Name"]];
            
        }
        
        //        lbl_home_r_work.text = [NSString stringWithFormat:@"%@",[array_home_work_address objectAtIndex:indexPath.row]];
        lbl_home_r_work.font = [UIFont fontWithName:kFontBold size:13];
        lbl_home_r_work.textColor = [UIColor blackColor];
        lbl_home_r_work.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_home_r_work];
        
        
        
        
        UIImageView *info_imges = [[UIImageView alloc]init];
        //   info_imges.frame =  CGRectMake(0,10, 30, 30);
        [info_imges setImage:[UIImage imageNamed:@"icon-location@2x.png"]];
        info_imges.backgroundColor =[UIColor clearColor];
        [info_imges setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:info_imges];
        
        UILabel *address = [[UILabel alloc]init];
        //   address.frame = CGRectMake(40,25,200, 15);
        
        address.text = [NSString stringWithFormat:@"%@,%@,%@,%@",[[ary_displayAddress objectAtIndex:indexPath.row] valueForKey:@"City"],[[ary_displayAddress objectAtIndex:indexPath.row] valueForKey:@"Country"],[[ary_displayAddress objectAtIndex:indexPath.row] valueForKey:@"Street_Address"],[[ary_displayAddress objectAtIndex:indexPath.row] valueForKey:@"Postal_Code"]];
        // address.numberOfLines = 2;
        address.font = [UIFont fontWithName:kFont size:13];
        address.textColor = [UIColor blackColor];
        address.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:address];
        
        UILabel *label_deliver = [[UILabel alloc]init];
        //  label_deliver.frame = CGRectMake(230,10,200, 15);
        label_deliver.text = @"Delivery";
        label_deliver.font = [UIFont fontWithName:kFontBold size:13];
        label_deliver.textColor = [UIColor blackColor];
        label_deliver.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:label_deliver];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, WIDTH,80);
            img_line.frame =  CGRectMake(0,64, WIDTH-20, 0.5);
            lbl_home_r_work.frame = CGRectMake(55,10,200, 15);
            //            lbl_home_r_Address.frame=CGRectMake(55, CGRectGetMaxY(info_imges.frame)+10, 200, 15);
            info_imges.frame =  CGRectMake(10,10, 30, 30);
            address.frame = CGRectMake(55,25,200, 15);
            label_deliver.frame = CGRectMake(320,5,200, 15);
        }
        else if (IS_IPHONE_6)
        {
            
            img_cellBackGnd.frame =  CGRectMake(0,2, WIDTH, 75);
            info_imges.frame =  CGRectMake(15,10, 35, 35);
            lbl_home_r_work.frame = CGRectMake(CGRectGetMaxX(info_imges.frame)+15,10,200, 15);
            //            lbl_home_r_Address.frame=CGRectMake(55, CGRectGetMaxY(info_imges.frame)+10, 200, 15);
            
            address.frame = CGRectMake(CGRectGetMaxX(info_imges.frame)+15,25,400, 33);
            label_deliver.frame = CGRectMake(280,10,200, 15);
            img_line.frame =  CGRectMake(15,70, WIDTH-30, 0.5);
        }
        
        else
            
        {
            img_cellBackGnd.frame =  CGRectMake(2,2, 290,75);
            img_line.frame =  CGRectMake(0,60, 295, 0.5);
            lbl_home_r_work.frame = CGRectMake(50,10,200, 15);
            //            lbl_home_r_Address.frame=CGRectMake(55, CGRectGetMaxY(info_imges.frame)+10, 200, 15);
            
            info_imges.frame =  CGRectMake(8,10, 30, 30);
            address.frame = CGRectMake(50,25,200, 15);
            label_deliver.frame = CGRectMake(230,10,200, 15);
        }
        
        
    }
    else if (tableView == table_for_food_info)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH, 70);
        //    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor whiteColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        UIImageView *icons_food_info = [[UIImageView alloc]init];
        icons_food_info.frame =  CGRectMake(10,10, 30, 30);
        [icons_food_info setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_icons_in_food_info objectAtIndex:indexPath.row]]]];
        icons_food_info.backgroundColor =[UIColor clearColor];
        [icons_food_info setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:icons_food_info];
        
        UILabel *head_lab_food_info = [[UILabel alloc]init];
        head_lab_food_info.frame = CGRectMake(CGRectGetMaxX(icons_food_info.frame)+15,10,200, 15);
        head_lab_food_info.text = [NSString stringWithFormat:@"%@",[array_lable_in_food_info objectAtIndex:indexPath.row]];
        head_lab_food_info.font = [UIFont fontWithName:kFontBold size:13];
        head_lab_food_info.textColor = [UIColor blackColor];
        head_lab_food_info.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:head_lab_food_info];
        
        
        UILabel *items_names = [[UILabel alloc]init];
        items_names.frame = CGRectMake(CGRectGetMaxX(icons_food_info.frame)+15,25,300, 25);
        
        
        if ([ary_FoodinFo count]==0)
        {
            
        }
        else{
            if (indexPath.row == 0)
            {
                items_names .text = [NSString stringWithFormat:@"%@",[[ary_FoodinFo objectAtIndex:0] valueForKey:@"Dietary_Restrictions"]];
                
            }
            else if (indexPath.row == 1)
            {
                
                items_names .text = [NSString stringWithFormat:@"%@",[[ary_FoodinFo objectAtIndex:0] valueForKey:@"FavoriteCuisines"]];
                
            }
            else if (indexPath.row == 2)
            {
                
                items_names .text = [NSString stringWithFormat:@"%@",[[ary_FoodinFo objectAtIndex:0] valueForKey:@"Food_Alergies"]];
                
            }
            
            
        }
        
        
        //        if (indexPath.row == 0)
        //        {
        //            head_lab_food_info .text = [NSString stringWithFormat:@"%@",[[ary_FoodinFo objectAtIndex:0] valueForKey:@"Dietary_Restrictions"]];
        //
        //        }
        //        else if (indexPath.row == 1)
        //        {
        //
        //            head_lab_food_info .text = [NSString stringWithFormat:@"%@",[[ary_FoodinFo objectAtIndex:1] valueForKey:@"FavoriteCuisines"]];
        //
        //        }
        //        else if (indexPath.row == 2)
        //        {
        //
        //            head_lab_food_info .text = [NSString stringWithFormat:@"%@",[[ary_FoodinFo objectAtIndex:2] valueForKey:@"Food_Alergies"]];
        //
        //        }
        
        //        items_names.text = [NSString stringWithFormat:@"%@",[array_items_in_food_info objectAtIndex:indexPath.row]];
        items_names.font = [UIFont fontWithName:kFont size:13];
        items_names.textColor = [UIColor blackColor];
        items_names.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:items_names];
        
        UIImageView *img_line = [[UIImageView alloc]init];
        img_line.frame =  CGRectMake(10,55, 330, 0.5);
        img_line.backgroundColor =[UIColor grayColor];
        [img_line setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line];
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH, 70);
            icons_food_info.frame =  CGRectMake(15,10, 30, 30);
            
            
            head_lab_food_info.frame = CGRectMake(CGRectGetMaxX(icons_food_info.frame)+20,10,200, 15);
            items_names.frame = CGRectMake(CGRectGetMaxX(icons_food_info.frame)+20,25,300, 25);
            img_line.frame =  CGRectMake(15,55, 350, 0.5);
            
        }
        else if(IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH, 70);
            icons_food_info.frame =  CGRectMake(10,10, 30, 30);
            
            
            head_lab_food_info.frame = CGRectMake(CGRectGetMaxX(icons_food_info.frame)+15,10,200, 15);
            items_names.frame = CGRectMake(CGRectGetMaxX(icons_food_info.frame)+15,25,300, 25);
            img_line.frame =  CGRectMake(10,55, 330, 0.5);
            
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH, 70);
            icons_food_info.frame =  CGRectMake(10,10, 30, 30);
            
            
            head_lab_food_info.frame = CGRectMake(CGRectGetMaxX(icons_food_info.frame)+15,10,200, 15);
            items_names.frame = CGRectMake(CGRectGetMaxX(icons_food_info.frame)+15,25,300, 25);
            img_line.frame =  CGRectMake(10,55, 330, 0.5);
            
            
        }
    }
    else if (tableView == table_short_favorite_items)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH, 60);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor whiteColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UILabel *labl_short_items = [[UILabel alloc]init];
        labl_short_items.frame = CGRectMake(20,10,200, 15);
        labl_short_items.text = [NSString stringWithFormat:@"%@",[array_short_items objectAtIndex:indexPath.row]];
        labl_short_items.font = [UIFont fontWithName:kFontBold size:13];
        labl_short_items.textColor = [UIColor blackColor];
        labl_short_items.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:labl_short_items];
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH, 60);
            labl_short_items.frame = CGRectMake(20,10,200, 15);
        }
        else if(IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH, 60);
            labl_short_items.frame = CGRectMake(20,10,200, 15);
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH, 60);
            labl_short_items.frame = CGRectMake(20,10,200, 15);
        }
        
    }
    else if (tableView == table_items_in_favorites)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH+5, 170);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-5, 170);
            
        }
        else
        {    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-10,170);
            
        }
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UIImageView *img_dish = [[UIImageView alloc] init];
        NSString *ImagePath1 =[[NSString stringWithFormat:@"%@",[[ary_itemsinformation objectAtIndex:indexPath.row]valueForKey:@"DishImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [img_dish setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@""]];
        [img_cellBackGnd addSubview:img_dish];
        
        UIButton *btn_dish = [UIButton buttonWithType:UIButtonTypeCustom];
        img_dish .backgroundColor = [UIColor clearColor];
        [btn_dish addTarget:self action:@selector(btn_imgDishclick:) forControlEvents:UIControlEventTouchUpInside];
        [img_dish   addSubview:btn_dish];
        
        UIButton *btn_favorite = [UIButton buttonWithType:UIButtonTypeCustom];
        //  btn_favorite.frame = CGRectMake(5,58, 35, 35);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [btn_favorite addTarget:self action:@selector(btn_favorite_click:) forControlEvents:UIControlEventTouchUpInside];
        [btn_favorite setImage:[UIImage imageNamed:@"favorite-icon@2x.png"] forState:UIControlStateNormal];
        [img_dish   addSubview:btn_favorite];
        UIImageView *img_round_red = [[UIImageView alloc]init];
        [img_round_red setUserInteractionEnabled:YES];
        img_round_red.image=[UIImage imageNamed:@"icon_round_red@2x.png"];
        [img_dish addSubview:img_round_red];
        
        UILabel *round_red_val  = [[UILabel alloc]init];
        //        round_red_val .text = [NSString stringWithFormat:@"%@",[array_round_red_val objectAtIndex:indexPath.row]];
        round_red_val .font = [UIFont fontWithName:kFontBold size:14];
        round_red_val .textColor = [UIColor whiteColor];
        round_red_val .backgroundColor = [UIColor clearColor];
        [img_round_red addSubview:round_red_val];
        
        
        
        UIButton *icon_delete = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_delete.frame = CGRectMake(WIDTH-70,20, 40, 40);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_delete addTarget:self action:@selector(btn_add_to_cart_click:) forControlEvents:UIControlEventTouchUpInside];
        [icon_delete setImage:[UIImage imageNamed:@"img-cart@2x.png"] forState:UIControlStateNormal];
        [cell.contentView   addSubview:icon_delete];
        
        UILabel *dish_name = [[UILabel alloc]init];
        //   dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
        dish_name.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"DishName"];
        //        dish_name.font = [UIFont fontWithName:kFontBold size:8];
        dish_name.textColor = [UIColor blackColor];
        dish_name.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:dish_name];
        
        
        UIImageView *icon_location = [[UIImageView alloc] init];
        //    icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        [icon_location setImage:[UIImage imageNamed:@"location-icon@2x.png"]];
        [img_cellBackGnd  addSubview:icon_location];
        
        UILabel *meters = [[UILabel alloc]init];
        //  meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        //        meters.text = @"5km";
        meters.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"kitchenDistance"];
        
        meters.font = [UIFont fontWithName:kFont size:15];
        meters.textColor = [UIColor blackColor];
        meters.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:meters];
        
        
        
        
        layout=[[UICollectionViewFlowLayout alloc] init];
        collView_serviceDirectory = [[UICollectionView alloc] initWithFrame:CGRectMake(30,0,WIDTH-65,70)
                                                       collectionViewLayout:layout];
        
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        [collView_serviceDirectory setDataSource:self];
        [collView_serviceDirectory setDelegate:self];
        collView_serviceDirectory.scrollEnabled = YES;
        collView_serviceDirectory.showsVerticalScrollIndicator = NO;
        collView_serviceDirectory.showsHorizontalScrollIndicator = NO;
        collView_serviceDirectory.pagingEnabled = NO;
        [collView_serviceDirectory registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [collView_serviceDirectory setBackgroundColor:[UIColor clearColor]];
        layout.minimumInteritemSpacing = 2;
        layout.minimumLineSpacing = 0;
        collView_serviceDirectory.userInteractionEnabled = YES;
        [img_cellBackGnd addSubview:collView_serviceDirectory];
        
        
        //        UIButton *icon_server = [UIButton buttonWithType:UIButtonTypeCustom];
        //        //   icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
        //        //icon_server .backgroundColor = [UIColor clearColor];
        //        [icon_server  addTarget:self action:@selector(click_on_icon_sever_btn:) forControlEvents:UIControlEventTouchUpInside];
        //        [icon_server setImage:[UIImage imageNamed:@"serving-icon@2x.png"] forState:UIControlStateNormal];
        //        [img_cellBackGnd   addSubview:icon_server];
        //
        //        UIButton *icon_halal = [UIButton buttonWithType:UIButtonTypeCustom];
        //        //   icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
        //        //icon_halal .backgroundColor = [UIColor clearColor];
        //        [icon_halal addTarget:self action:@selector(click_on_icon_halal_btn:) forControlEvents:UIControlEventTouchUpInside];
        //        [icon_halal setImage:[UIImage imageNamed:@"halal-icon@2x.png"] forState:UIControlStateNormal];
        //        [img_cellBackGnd   addSubview:icon_halal];
        //
        //        UIImageView *img_non_veg = [[UIImageView alloc] init];
        //        //  img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
        //        [img_non_veg setImage:[UIImage imageNamed:@"non-veg bt@2x.png"]];
        //        [img_cellBackGnd addSubview:img_non_veg];
        //
        //        UIButton *icon_cow = [UIButton buttonWithType:UIButtonTypeCustom];
        //        //  icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
        //        //icon_cow .backgroundColor = [UIColor clearColor];
        //        [icon_cow addTarget:self action:@selector(click_on_icon_cow_btn:) forControlEvents:UIControlEventTouchUpInside];
        //        [icon_cow setImage:[UIImage imageNamed:@"cow-icon@2x.png"] forState:UIControlStateNormal];
        //        [img_cellBackGnd   addSubview:icon_cow];
        //
        //        UIButton *icon_fronce = [UIButton buttonWithType:UIButtonTypeCustom];
        //        //   icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
        //        //icon_cow .backgroundColor = [UIColor clearColor];
        //        [icon_fronce addTarget:self action:@selector(click_on_fronce_btn:) forControlEvents:UIControlEventTouchUpInside];
        //        [icon_fronce setImage:[UIImage imageNamed:@"fronce-icon@2x.png"] forState:UIControlStateNormal];
        //        [img_cellBackGnd   addSubview:icon_fronce];
        
        UILabel *doller_rate = [[UILabel alloc]init];
        //   doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
        //        doller_rate.text = @"$14.90";
        doller_rate.font = [UIFont fontWithName:kFontBold size:16];
        doller_rate.text = [NSString stringWithFormat:@"$%@",[[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"Dishpprice"]];
        
        doller_rate.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        doller_rate.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:doller_rate];
        
        UIImageView *img_line = [[UIImageView alloc] init];
        //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
        [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
        [img_cellBackGnd addSubview:img_line];
        
        
        UIButton *icon_take = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        //icon_take .backgroundColor = [UIColor clearColor];
        [icon_take addTarget:self action:@selector(click_on_take_out_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_take setImage:[UIImage imageNamed:@"take-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_take];
        
        UIButton *icon_deliver = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //icon_deliver .backgroundColor = [UIColor clearColor];
        [icon_deliver addTarget:self action:@selector(click_on_icon_delivery_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_deliver setImage:[UIImage imageNamed:@"deliver-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_deliver];
        
        
        UIButton *img_btn_seving_Now = [UIButton buttonWithType:UIButtonTypeCustom];
        //        img_btn_seving_Now.text = [[array_items_name objectAtIndex:indexPath.row] valueForKey:@"likes"];
        [img_btn_seving_Now addTarget:self action:@selector(click_on_seving_now_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_seving_Now setImage:[UIImage imageNamed:@"now-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_seving_Now];
        
        
        UIButton *img_btn_chef_menu = [UIButton buttonWithType:UIButtonTypeCustom];
        //    img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
        [img_btn_chef_menu addTarget:self action:@selector(click_on_icon_chef_menu_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_chef_menu setImage:[UIImage imageNamed:@"chef-menu-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_chef_menu];
        
        
        UIButton *icon_thumb = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+5, 20, 20);
        //icon_thumb .backgroundColor = [UIColor clearColor];
        [icon_thumb addTarget:self action:@selector(click_on_icon_thumb_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_thumb setImage:[UIImage imageNamed:@"thumb-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_thumb];
        
        UILabel *likes = [[UILabel alloc]init];
        //  likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
        //        likes.text = @"87.4%";
        likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
        
        likes.font = [UIFont fontWithName:kFont size:10];
        likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        likes.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:likes];
        
        UILabel *labl_time_and_date = [[UILabel alloc]init];
        //labl_time_and_date.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
        //        labl_time_and_date.text = [NSString stringWithFormat:@"%@",[array_time_and_date objectAtIndex:indexPath.row]];
        labl_time_and_date.font = [UIFont fontWithName:kFont size:10];
        labl_time_and_date.textColor = [UIColor blackColor];
        labl_time_and_date.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:labl_time_and_date];
        
        
        if (IS_IPHONE_6Plus)
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_dish.frame=CGRectMake(0, 0, 90, 95);
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            img_round_red.frame = CGRectMake(70, 5, 25,25);
            round_red_val .frame = CGRectMake(10,0,25, 25);
            icon_delete.frame = CGRectMake(WIDTH-70,20, 40, 40);
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 50, 200, 50);
            //            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            //            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            //            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            //            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            //            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+4, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+9,128,100, 10);
            labl_time_and_date.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)-100,CGRectGetMaxY(img_cellBackGnd.frame)-3,300, 10);
            dish_name.font = [UIFont fontWithName:kFontBold size:18];
            
        }
        else if (IS_IPHONE_6)
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_dish.frame=CGRectMake(0, 0, 90, 95);
            
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            img_round_red.frame = CGRectMake(65, 5, 25,25);
            round_red_val .frame = CGRectMake(10,0,25, 25);
            icon_delete.frame = CGRectMake(WIDTH-62,20, 40, 40);
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 60, 180, 50);
            //            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            //            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            //            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            //            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            //            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(300,CGRectGetMaxY(img_line.frame)+5, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
            labl_time_and_date.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)-100,CGRectGetMaxY(img_cellBackGnd.frame)-3,300, 10);
            dish_name.font = [UIFont fontWithName:kFontBold size:18];
            
        }
        else
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            btn_dish.frame=CGRectMake(0, 0, 90, 95);
            
            btn_favorite.frame = CGRectMake(5,58, 35, 35);
            img_round_red.frame = CGRectMake(65, 5, 25,25);
            round_red_val .frame = CGRectMake(10,0,25, 25);
            icon_delete.frame = CGRectMake(WIDTH-65,20, 40, 40);
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 40, 150, 50);
            //            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            //            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            //            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            //            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            //            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(IS_IPHONE_5?WIDTH-75:WIDTH-80,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(IS_IPHONE_5?240:230,CGRectGetMaxY(img_line.frame)+5, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
            labl_time_and_date.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)-100,CGRectGetMaxY(img_cellBackGnd.frame)-3,300, 10);
            dish_name.font = [UIFont fontWithName:kFontBold size:IS_IPHONE_5?15:15];
            
        }
        
    }
    else if (tableView == table_for_chef_in_favorites)
    {
        
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH+5, 175);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-5, 160);
            
        }
        else
        {    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-10,170);
            
        }
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        
        UIImageView *img_chef = [[UIImageView alloc] init];
        //    img_chef.frame = CGRectMake(7,7, 90,  95 );
        NSString *ImagePath1 =[[NSString stringWithFormat:@"%@",[[ary_Chefinformation objectAtIndex:0] valueForKey:@"ChefProfilePic"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [img_chef setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@""]];
        [img_cellBackGnd addSubview:img_chef];
        
        
        UIButton *icon_hart = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_hart.frame = CGRectMake(WIDTH-70,20, 40, 40);
        //icon_hart .backgroundColor = [UIColor clearColor];
        [icon_hart addTarget:self action:@selector(btn_add_to_cart_click:) forControlEvents:UIControlEventTouchUpInside];
        [icon_hart setImage:[UIImage imageNamed:@"favorite-icon@2x.png"] forState:UIControlStateNormal];
        [cell.contentView   addSubview:icon_hart];
        
        UILabel *chef_names = [[UILabel alloc]init];
        // chef_names.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
        //        chef_names.text = [NSString stringWithFormat:@"%@",[array_chef_name objectAtIndex:indexPath.row]];
        //        chef_names.text = [[ary_Chefinformation objectAtIndex:indexPath.row] valueForKey:@"Full_Name"];
        //        chef_names.font = [UIFont fontWithName:kFontBold size:8];
        chef_names.textColor = [UIColor blackColor];
        chef_names.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:chef_names];
        
        
        UIImageView *icon_location = [[UIImageView alloc] init];
        //    icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        [icon_location setImage:[UIImage imageNamed:@"location-icon@2x.png"]];
        [img_cellBackGnd  addSubview:icon_location];
        
        UILabel *meters = [[UILabel alloc]init];
        //  meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        //        meters.text = @"5km";
        meters.text = [[ary_Chefinformation objectAtIndex:indexPath.row] valueForKey:@"kitchenDistance"];
        
        meters.font = [UIFont fontWithName:kFont size:15];
        meters.textColor = [UIColor blackColor];
        meters.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:meters];
        
        
        
        
        layout2=[[UICollectionViewFlowLayout alloc] init];
        collView_serviceDirectory2 = [[UICollectionView alloc] initWithFrame:CGRectMake(30,0,WIDTH-65,70)
                                                        collectionViewLayout:layout];
        
        [layout2 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        [collView_serviceDirectory2 setDataSource:self];
        [collView_serviceDirectory2 setDelegate:self];
        collView_serviceDirectory2.scrollEnabled = YES;
        collView_serviceDirectory2.showsVerticalScrollIndicator = NO;
        collView_serviceDirectory2.showsHorizontalScrollIndicator = NO;
        collView_serviceDirectory2.pagingEnabled = NO;
        [collView_serviceDirectory2 registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [collView_serviceDirectory2 setBackgroundColor:[UIColor clearColor]];
        layout2.minimumInteritemSpacing = 2;
        layout2.minimumLineSpacing = 0;
        collView_serviceDirectory2.userInteractionEnabled = YES;
        [img_cellBackGnd addSubview:collView_serviceDirectory2];
        
        
        
        
        UIImageView *img_line = [[UIImageView alloc] init];
        //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
        [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
        [img_cellBackGnd addSubview:img_line];
        
        
        UIButton *icon_take = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        //icon_take .backgroundColor = [UIColor clearColor];
        [icon_take addTarget:self action:@selector(click_on_take_out_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_take setImage:[UIImage imageNamed:@"take-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_take];
        
        UIButton *icon_deliver = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //icon_deliver .backgroundColor = [UIColor clearColor];
        [icon_deliver addTarget:self action:@selector(click_on_icon_delivery_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_deliver setImage:[UIImage imageNamed:@"deliver-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_deliver];
        
        
        UIButton *img_btn_seving_Now = [UIButton buttonWithType:UIButtonTypeCustom];
        //   img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //img_btn_deliver_Now .backgroundColor = [UIColor clearColor];
        [img_btn_seving_Now addTarget:self action:@selector(click_on_seving_now_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_seving_Now setImage:[UIImage imageNamed:@"now-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_seving_Now];
        
        
        UIButton *img_btn_chef_menu = [UIButton buttonWithType:UIButtonTypeCustom];
        //    img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
        [img_btn_chef_menu addTarget:self action:@selector(click_on_icon_chef_menu_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_chef_menu setImage:[UIImage imageNamed:@"chef-menu-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_chef_menu];
        
        
        
        UIButton *icon_thumb = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+5, 20, 20);
        //icon_thumb .backgroundColor = [UIColor clearColor];
        [icon_thumb addTarget:self action:@selector(click_on_icon_thumb_btn:) forControlEvents:UIControlEventTouchUpInside];
        [icon_thumb setImage:[UIImage imageNamed:@"thumb-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_thumb];
        
        UILabel *likes = [[UILabel alloc]init];
        //  likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
        //        likes.text = @"87.4%";
        likes.text = [[ary_Chefinformation objectAtIndex:indexPath.row] valueForKey:@"likes"];
        
        likes.font = [UIFont fontWithName:kFont size:10];
        likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        likes.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:likes];
        
        if (IS_IPHONE_6Plus)
        {
            img_chef.frame = CGRectMake(15,7, 90,  95 );
            icon_hart.frame = CGRectMake(WIDTH-50,10, 40, 40);
            
            chef_names.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,CGRectGetMaxY(chef_names.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+10,40,200, 15);
            collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_chef.frame)+5, 50, 200, 50);
            
            //            icon_halal.frame = CGRectMake( CGRectGetMaxX(img_chef.frame)+15,80, 25, 25);
            //            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            //            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_chef.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(190,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+9, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+9,140,100, 10);
            chef_names.font = [UIFont fontWithName:kFontBold size:16];
            
        }
        else if (IS_IPHONE_6)
        {
            img_chef.frame = CGRectMake(15,15, 70,  70 );
            icon_hart.frame = CGRectMake(WIDTH-47,10, 30, 30);
            chef_names.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,15,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+15,CGRectGetMaxY(chef_names.frame)+15,13, 20);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,48,200, 15);
            collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_chef.frame)+5, 50, 200, 50);
            
            //            icon_halal.frame = CGRectMake( CGRectGetMaxX(img_chef.frame)+15,75, 25, 25);
            //            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,75, 25, 25);
            //            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,75, 25, 25);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_chef.frame)+25, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(60,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(135,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(300,CGRectGetMaxY(img_line.frame)+5, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,122,100, 10);
            chef_names.font = [UIFont fontWithName:kFontBold size:15];
            
        }
        else
        {
            img_chef.frame = CGRectMake(15,15, 70,  70 );
            icon_hart.frame = CGRectMake(WIDTH-65,10, 40, 40);
            chef_names.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+10,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+10,CGRectGetMaxY(chef_names.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_chef.frame)+5, 50, 200, 50);
            
            //            icon_halal.frame = CGRectMake( CGRectGetMaxX(img_chef.frame)+10,80, 25, 25);
            //            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            //            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_chef.frame)+25, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            icon_thumb.frame = CGRectMake(IS_IPHONE_5?330:230,CGRectGetMaxY(img_line.frame)+5, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
            chef_names.font = [UIFont fontWithName:kFontBold size:IS_IPHONE_5?15:15];
            
        }
        
    }
    
    
    
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==table_short_favorite_items)
    {
        
        text_distance.text =[array_short_items objectAtIndex:indexPath.row];
        
        table_short_favorite_items.hidden = YES;
        
        
    }
}

#pragma mark UiCollectionView Delegate Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 4;
    //    return [ary_DishRestrictions count];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    if (collectionView == collView_serviceDirectory) {
        UIImageView *img_backGnd = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 180, 50)];
        //[img_backGnd setImage:[UIImage imageNamed:@"img_BackGnd@2x.png"]];
        img_backGnd.backgroundColor = [UIColor redColor];
        [cell.contentView addSubview:img_backGnd];
        
        
        UIImageView *img_non_veg = [[UIImageView alloc] init];
        //    NSString *ImagePath1 =  [[NSString stringWithFormat:@"%@",[[array_items_name objectAtIndex:indexPath.row] valueForKey:@"resImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //    [img_non_veg setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@"img_merchantPlaceholder@2x.png"]];
        img_non_veg.frame = CGRectMake(2,15, 30, 20 );
        img_non_veg.backgroundColor=[UIColor greenColor];
        [img_backGnd addSubview:img_non_veg];
        
        UIImageView  *img_swapImg = [[UIImageView alloc]initWithFrame: CGRectMake(0,-2,50,20)];
        [img_swapImg setImage:[UIImage imageNamed:@"bg_nonveg@2x.png"]];
        img_swapImg .backgroundColor = [UIColor clearColor];
        
        [img_swapImg setUserInteractionEnabled:YES];
        [img_backGnd addSubview:img_swapImg];
        
        
        img_swapImg.hidden=NO;
        
        if (indexPath.row==selectedindex)
        {
            img_swapImg.hidden=NO;
            UILabel *chef_titiles = [[UILabel alloc]init];
            chef_titiles .frame = CGRectMake(0,0,50, 15);
            chef_titiles .text = [NSString stringWithFormat:@"%@",[[[array_items_name objectAtIndex:0] objectAtIndex:indexPath.row] valueForKey:@"Restriction_name"]];
            chef_titiles .font = [UIFont fontWithName:kFontBold size:9];
            chef_titiles .textColor = [UIColor whiteColor];
            chef_titiles .backgroundColor = [UIColor clearColor];
            [img_swapImg addSubview:chef_titiles ];
            
            
            
            // img_MenuImg.hidden=NO;
        }
        else
        {
            img_swapImg.hidden=YES;
            // img_MenuImg.hidden=YES;
        }
        
    } else {
        UIImageView *img_backGnd = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 180, 50)];
        //[img_backGnd setImage:[UIImage imageNamed:@"img_BackGnd@2x.png"]];
        img_backGnd.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:img_backGnd];
        
        
        UIImageView *img_non_veg = [[UIImageView alloc] init];
        //    NSString *ImagePath1 =  [[NSString stringWithFormat:@"%@",[[array_items_name objectAtIndex:indexPath.row] valueForKey:@"resImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //    [img_non_veg setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@"img_merchantPlaceholder@2x.png"]];
        img_non_veg.frame = CGRectMake(2,15, 30, 20 );
        img_non_veg.backgroundColor=[UIColor clearColor];
        [img_backGnd addSubview:img_non_veg];
        
        UIImageView  *img_swapImg = [[UIImageView alloc]initWithFrame: CGRectMake(0,-2,50,20)];
        [img_swapImg setImage:[UIImage imageNamed:@"bg_nonveg@2x.png"]];
        img_swapImg .backgroundColor = [UIColor clearColor];
        
        [img_swapImg setUserInteractionEnabled:YES];
        [img_backGnd addSubview:img_swapImg];
        
        
        img_swapImg.hidden=NO;
        
        if (indexPath.row==selectedindex)
        {
            img_swapImg.hidden=NO;
            UILabel *chef_titiles = [[UILabel alloc]init];
            chef_titiles .frame = CGRectMake(0,0,50, 15);
            chef_titiles .text = [NSString stringWithFormat:@"%@",[[[array_items_name objectAtIndex:0] objectAtIndex:indexPath.row] valueForKey:@"Restriction_name"]];
            chef_titiles .font = [UIFont fontWithName:kFontBold size:9];
            chef_titiles .textColor = [UIColor whiteColor];
            chef_titiles .backgroundColor = [UIColor clearColor];
            [img_swapImg addSubview:chef_titiles ];
            
            
            
            // img_MenuImg.hidden=NO;
        }
        else
        {
            img_swapImg.hidden=YES;
            // img_MenuImg.hidden=YES;
        }
        
    }
    //    UIImageView *img_backGnd = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 180, 50)];
    //    //[img_backGnd setImage:[UIImage imageNamed:@"img_BackGnd@2x.png"]];
    //    img_backGnd.backgroundColor = [UIColor redColor];
    //    [cell.contentView addSubview:img_backGnd];
    //
    //
    //    UIImageView *img_non_veg = [[UIImageView alloc] init];
    ////    NSString *ImagePath1 =  [[NSString stringWithFormat:@"%@",[[array_items_name objectAtIndex:indexPath.row] valueForKey:@"resImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    ////    [img_non_veg setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@"img_merchantPlaceholder@2x.png"]];
    //    img_non_veg.frame = CGRectMake(2,15, 30, 20 );
    //    img_non_veg.backgroundColor=[UIColor greenColor];
    //    [img_backGnd addSubview:img_non_veg];
    //
    //    UIImageView  *img_swapImg = [[UIImageView alloc]initWithFrame: CGRectMake(0,-2,50,20)];
    //    [img_swapImg setImage:[UIImage imageNamed:@"bg_nonveg@2x.png"]];
    //    img_swapImg .backgroundColor = [UIColor clearColor];
    //
    //    [img_swapImg setUserInteractionEnabled:YES];
    //    [img_backGnd addSubview:img_swapImg];
    //
    //
    //    img_swapImg.hidden=NO;
    //
    //    if (indexPath.row==selectedindex)
    //    {
    //        img_swapImg.hidden=NO;
    //        UILabel *chef_titiles = [[UILabel alloc]init];
    //        chef_titiles .frame = CGRectMake(0,0,50, 15);
    //        chef_titiles .text = [NSString stringWithFormat:@"%@",[[[array_items_name objectAtIndex:0] objectAtIndex:indexPath.row] valueForKey:@"Restriction_name"]];
    //        chef_titiles .font = [UIFont fontWithName:kFontBold size:9];
    //        chef_titiles .textColor = [UIColor whiteColor];
    //        chef_titiles .backgroundColor = [UIColor clearColor];
    //        [img_swapImg addSubview:chef_titiles ];
    //
    //
    //
    //        // img_MenuImg.hidden=NO;
    //    }
    //    else
    //    {
    //        img_swapImg.hidden=YES;
    //        // img_MenuImg.hidden=YES;
    //    }
    //
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (IS_IPHONE_6Plus){
        return CGSizeMake(50, 25);
    }
    else if (IS_IPHONE_6){
        return CGSizeMake(50, 25);
    }
    else  {
        return CGSizeMake(50, 25);
    }
    
    return CGSizeMake(0, 0);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView== collView_serviceDirectory) {
        selectedindex= (int)indexPath.row;
        
        if (indexPath.row==selectedindex)
        {
            indexSelected = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
        }
        [collectionView reloadData];
        
    } else {
        selectedindex= (int)indexPath.row;
        
        if (indexPath.row==selectedindex)
        {
            indexSelected = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
        }
        [collectionView reloadData];
        
    }
    
    
    //    selectedindex= (int)indexPath.row;
    //
    //    if (indexPath.row==selectedindex)
    //    {
    //        indexSelected = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
    //    }
    //    [collectionView reloadData];
    
    
}




-(void)btn_back_click:(UIButton *)sender
{
    NSLog(@"icon_back_click Btn Click");
    [self dismissViewControllerAnimated:NO completion:nil];
    
}
-(void)btn_img_user_click:(UIButton *)sender
{
    NSLog(@"img_user_click Btn Click");
    
}
-(void)btn_img_pencil_click:(UIButton *)sender
{
    NSLog(@"img_pencil_click Btn Click");
    
}
-(void)btn_img_user_info_click:(UIButton *)sender
{
    NSLog(@"img_user_info_click Btn Click");
    
}
-(void)btn_imgDishclick:(UIButton *)sender
{
    AddToCartFoodNowVC *addCartfoodnow=[[AddToCartFoodNowVC alloc]init];
    [self presentViewController:addCartfoodnow animated:NO completion:nil];
    
    //    [self.navigationController pushViewController:chefsignup5 animated:NO];
    
    
}


-(void)btn_personal_info_strip_click:(UIButton *)sender
{
    
    NSLog(@" Btn_items_Click");
    img_strip.hidden = NO;
    img_strip2.hidden = YES;
    img_strip3.hidden = YES;
    img_strip4.hidden = YES;
    
    
    view_personal.hidden = NO;
    view_for_address.hidden = YES;
    view_for_foodinfo.hidden = YES;
    view_favorites.hidden = YES;
    
    //    [self UserProfileInfo];
}
-(void)btn_address_strip_click:(UIButton *)sender
{
    
    NSLog(@" Btn_chef_Click");
    img_strip.hidden = YES;
    img_strip2.hidden = NO;
    img_strip3.hidden = YES;
    img_strip4.hidden = YES;
    
    view_personal.hidden = YES;
    view_for_address.hidden = NO;
    view_for_foodinfo.hidden = YES;
    view_favorites.hidden = YES;
    //    [self UserProfileAddress];
    
    
    
}
-(void)btn_foodinfo_strip_click:(UIButton *)sender
{
    
    NSLog(@" Btn_chef_Click");
    img_strip.hidden = YES;
    img_strip2.hidden = YES;
    img_strip3.hidden = NO;
    img_strip4.hidden = YES;
    view_personal.hidden = YES;
    view_for_address.hidden = YES;
    view_for_foodinfo.hidden = NO;
    view_favorites.hidden = YES;
    
}
-(void)btn_favorites_strip_click:(UIButton *)sender
{
    
    
    NSLog(@" Btn_chef_Click");
    img_strip.hidden = YES;
    img_strip2.hidden = YES;
    img_strip3.hidden = YES;
    img_strip4.hidden = NO;
    view_personal.hidden = YES;
    view_for_address.hidden = YES;
    view_for_foodinfo.hidden = YES;
    view_favorites.hidden = NO;
    [self UserProfileFavorites];
    
}
-(void)click_on_add_btn:(UIButton *)sender
{
    NSLog(@"click_on_add_btn:");
    
}

//favavorites click events

-(void)click_on_items_btn:(UIButton *)sender
{
    NSLog(@"click_on_items_btn:");
    img_strip_for_chef.hidden = YES;
    view_for_chef_side.hidden = YES;
    img_strip_for_items .hidden = NO;
    view_for_items.hidden = NO;
    str_favorite_type=@"Dish";
    
    
    [self UserProfileFavorites];
    
    
}
-(void)click_on_labl_chef_btn:(UIButton *)sender
{
    NSLog(@"click_on_labl_chef_btn:");
    img_strip_for_items .hidden =YES;
    view_for_items.hidden = YES;
    img_strip_for_chef.hidden = NO;
    view_for_chef_side.hidden = NO;
    str_favorite_type=@"Chef";
    
    [self UserProfileFavorites];
    
    
}

-(void)click_on_dropdown_btn:(UIButton *)sender
{
    NSLog(@"click_on_dropdown_btn");
    
}
-(void)click_on_distance_btn:(UIButton *)sender
{
    NSLog(@"click_on_distance_btn");
    table_short_favorite_items.hidden = NO;
    
}



//clicl-events-home

-(void)btn_number_three_click:(UIButton *)sender
{
    NSLog(@"btn_number_three_click");
    
}
-(void)btn_add_to_cart_click:(UIButton *)sender
{
    NSLog(@"btn_add_to_cart_click:");
    
}

-(void)click_on_icon_sever_btn:(UIButton *)sender
{
    NSLog(@"click_on_icon_sever_btn:");
    
}

-(void)click_on_icon_halal_btn:(UIButton *)sender
{
    NSLog(@"click_on_icon_halal_btn:");
    
}

-(void)click_on_icon_cow_btn:(UIButton *)sender
{
    NSLog(@"click_on_icon_cow_btn:");
    
}
-(void)click_on_fronce_btn:(UIButton *)sender
{
    NSLog(@"click_on_fronce_btn:");
    
}
-(void)click_on_take_out_btn:(UIButton *)sender
{
    NSLog(@"click_on_take_out_btn:");
    
}
-(void)click_on_icon_delivery_btn:(UIButton *)sender
{
    NSLog(@"click_on_icon_delivery_btn:");
    
}
-(void)click_on_seving_now_btn:(UIButton *)sender
{
    NSLog(@"click_on_seving_now_btn:");
    
}
-(void)click_on_icon_chef_menu_btn:(UIButton *)sender
{
    NSLog(@"click_on_icon_chef_menu_btn:");
    
}
-(void)click_on_icon_thumb_btn:(UIButton *)sender
{
    NSLog(@"click_on_icon_thumb_btn:");
    
}




-(void)btn_address_click:(UIButton *)sender
{
    NSLog(@"img_address_click Btn Click");
    
}
-(void)btn_food_info_click:(UIButton *)sender
{
    NSLog(@"img_food_info_click Btn Click");
    
}

-(void)btn_favorite_click:(UIButton *)sender
{
    NSLog(@"img_favorite_click Btn Click");
    
    //    if () {
    //        <#statements#>
    //    } else {
    //        <#statements#>
    //    }
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}
#pragma user-profile-functionality

-(void)UserProfileInfo
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            
                            @"uid"                               :  @"572",
                            @"role_type"                         :  @"user_profile",
                            @"device_udid"                       :  UniqueAppID,
                            @"device_token"                      :  @"Dev",
                            @"device_type"                       :  @"1"
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:@"webservices/add-user-profile.json"  parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseUserProfileInfo:JSON];
    }
     
     
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error){
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self UserProfileInfo];
                                         }
                                     }];
    [operation start];
    
}
-(void)ResponseUserProfileInfo:(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        
        
        [aryray_text_in_cell addObject:[[TheDict valueForKey:@"profile_info"] valueForKey:@"Personal_Information"]];
        [ary_FoodinFo addObject:[[TheDict valueForKey:@"profile_info"] valueForKey:@"Food_Info"]];
        
        for (int i=0; i<[[[TheDict valueForKey:@"profile_info"] valueForKey:@"Address"] count]; i++)
        {
            [ary_displayAddress addObject:[[[TheDict valueForKey:@"profile_info"] valueForKey:@"Address"] objectAtIndex:i]];
            
        }
        txtview_adddescription.text =[[[TheDict valueForKey:@"profile_info"] valueForKey:@"Personal_Information"] valueForKey:@"About_Us"];
        
        //        [array_chef_details addObject:[[TheDict valueForKey:@"profile_info"] valueForKey:@"Personal_Information"]];
        
        
        [img_table reloadData];
        [table_for_food_info reloadData];
        [table_for_address reloadData];
        
        
        
        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
}


#pragma UserProfileUserFavorites

-(void)UserProfileFavorites
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            
                            @"uid"                               :  @"477",
                            @"lat"                               :  @"28.613939",
                            @"long"                               :  @"77.209021",
                            @"favorite_type"                      :  str_favorite_type,
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    //    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:@"webservices/add-user-profile.json"  parameters:params];
    
    
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kUserProfileFavorites
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseUserProfileFavorites:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error){
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self UserProfileFavorites];
                                         }
                                     }];
    [operation start];
    
}
-(void)ResponseUserProfileFavorites:(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        if ([str_favorite_type isEqualToString:@"Dish"])
        {
            
            
            
            NSArray *array = [TheDict objectForKey:@"Item_Information"];
            
            NSLog(@"array %@",array);
            
            for (NSDictionary *dict in array) {
                [ary_itemsinformation addObject:dict];
                
            }
            
            NSLog(@"ary_itemsinformation %@",array);
            
            
            
            
            
            
            //  [ary_itemsinformation addObject:[TheDict valueForKey:@"Item_Information"]];
            //            for (int i=0; i<[[TheDict valueForKey:@"Item_Information"] count]; i++)
            //            {
            //
            //                [ary_itemsinformation addObject:[[TheDict valueForKey:@"Item_Information"]objectAtIndex:i]];
            //            }
            
            
            //            for (int j=0; j<[[[TheDict valueForKey:@"Item_Information"] valueForKey:@"DietaryRestrictions"] count]; j++)
            //            {
            ////                               [array_items_name addObject:[[[TheDict valueForKey:@"Item_Information"] valueForKey:@"DietaryRestrictions"] objectAtIndex:j]];
            //            }
            
            
            
            [table_items_in_favorites reloadData];
            
        }
        else{
            
            NSArray *array = [TheDict objectForKey:@"Chef_Information"];
            
            NSLog(@"array %@",array);
            
            for (NSDictionary *dict in array) {
                [ary_Chefinformation addObject:dict];
                
            }
            
            NSLog(@"ary_Chefinformation %@",array);
            
            
            //            for (int i=0; i<[[TheDict valueForKey:@"Chef_Information"] count]; i++)
            //            {
            //              
            //               [ary_Chefinformation addObject:[[TheDict valueForKey:@"Chef_Information"]objectAtIndex:i]];
            //            }
            
            //           [ary_Chefinformation addObject:[TheDict valueForKey:@"Chef_Information"]];
            //            for (int j=0; j<[[[TheDict valueForKey:@"Chef_Information"] valueForKey:@"DietaryRestrictions"] count]; j++)
            //            {
            //                [array_chef_name addObject:[[[TheDict valueForKey:@"Chef_Information"] valueForKey:@"DietaryRestrictions"] objectAtIndex:j]];
            //                
            //            }
            
            [table_for_chef_in_favorites reloadData];
            
            
        }
        
        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
