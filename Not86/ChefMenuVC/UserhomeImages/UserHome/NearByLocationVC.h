//
//  NearByLocationVC.h
//  Joel
//
//  Created by User9 on 12/06/14.
//  Copyright (c) 2014 User1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"
#import <CoreLocation/CoreLocation.h>


@protocol LocationSelectionDelegate;

@class FSVenue;

@class AppDelegate;

@protocol ProductSelectionDelegate;


@interface NearByLocationVC : JWSlideMenuViewController
{
    CLLocationManager *locationManager;
    CLGeocoder *myGeoCoder;
}


@property(nonatomic,strong)NSString *str_Title;
@property(nonatomic,strong)NSString *str_Latitude;
@property(nonatomic,strong)NSString *str_Longitude;
@property (strong,nonatomic)FSVenue* selected;
@property (strong,nonatomic)NSArray* nearbyVenues;
@property(nonatomic,retain) id <ProductSelectionDelegate> piddelegate;
@property(nonatomic,retain) id <LocationSelectionDelegate> delegate;


//location
@property (strong, nonatomic) CLGeocoder *myGeoCoder;
@property (nonatomic, strong) CLLocation *mUserCurrentLocation;

@property (nonatomic, strong) CLLocationManager *locationManager;
@end


@protocol ProductSelectionDelegate <NSObject>

-(void) selectedLocationWithName:(NSString *) MapLocationName latitude:(double)maplatitude longitude:(double)maplongitude MapLocationChange:(NSString*)str_mapchange;

@end


@protocol LocationSelectionDelegate <NSObject>
-(void) setLocationWithName:(NSString *) LocationName lat:(double)latitude long:(double)longitude;




-(void) updateCurrentLocation;

-(CLLocation *) getCurrentLocation;
-(void)CurrentLocationIdentifier;

@end


