//
//  ProceedToPaymentVC.m
//  Not86
//
//  Created by Admin on 02/09/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ProceedToPaymentVC.h"
#import "AppDelegate.h"
#import "Define.h"
#import "PayPalVC.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"


@interface ProceedToPaymentVC ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    UIImageView *img_header;
    
    UIView  *view_for_my_orders;
    UIScrollView *scroll;
    NSMutableArray * array_order_no;
    
    UITableView *img_table;
    
    NSMutableArray * array_serving_time_date;
    NSMutableArray * array_dish_names;
    NSMutableArray *array_serving_type_imges;
    NSMutableArray * array_icon_dietary;
    NSMutableArray * array_icon_serving_type;
    NSMutableArray * array_quantity;
    NSMutableArray * array_serving_charge;
    NSMutableArray * array_sub_total;
    NSMutableArray * array_delivery_address;
    NSMutableArray *  array_request_and_remarks;
    
    UIView * view_for_popup;
    UITableView *img_table_for_popup;
    
    NSMutableArray*array_total_Items;
    NSMutableArray*array_Amount;
    AppDelegate*delegate;
    
    
    
    UILabel *food_bill_val;
    UILabel *not86_service_fee_val;
    UILabel *total_delivery_charge_val;
    UILabel *total_be_charged_val;
    
}

@end

@implementation ProceedToPaymentVC
@synthesize str_typeofserve;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
       array_total_Items = [NSMutableArray new];
    array_Amount = [NSMutableArray new];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    //[self popup_cancel_oeder];
    self.view.backgroundColor =[UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    
    array_serving_time_date = [[NSMutableArray alloc]initWithObjects:@"14/7/2015, 4:00:PM",@"14/7/2015, 4:00:PM",@"14/7/2015, 4:00:PM",@"14/7/2015, 4:00:PM",nil];
   array_serving_type_imges = [[NSMutableArray alloc]initWithObjects:@"deliver-icon@2x.png",@"deliver-icon@2x.png",@"deliver-icon@2x.png",@"deliver-icon@2x.png",nil];
    array_dish_names = [[NSMutableArray alloc]initWithObjects:@"Spicy Chicken Salad",@"Raspberry Custard",@"Spicy Chicken Salad",@"Raspberry Custard",nil];
    array_icon_dietary = [[NSMutableArray alloc]initWithObjects:@"halal-icon@2x.png",@"halal-icon@2x.png",@"halal-icon@2x.png",@"halal-icon@2x.png",nil];
    array_icon_serving_type = [[NSMutableArray alloc]initWithObjects:@"take-icon@2x.png",@"take-icon@2x.png",@"take-icon@2x.png",@"take-icon@2x.png",nil];
    array_quantity =  [[NSMutableArray alloc]initWithObjects:@"3",@"3",@"3",@"3",nil];
    array_serving_charge = [[NSMutableArray alloc]initWithObjects:@"$5.50",@"$5.50",@"$5.50",@"$5.50",nil];
    array_sub_total = [[NSMutableArray alloc]initWithObjects:@"$16.5",@"$16.5",@"$16.0",@"$16.5",nil];
    
    array_delivery_address = [[NSMutableArray alloc]initWithObjects: @"22 Montgomery Road, #17, NY, NY 10050",@"22 Montgomery Road, #17, NY, NY 10050",@"22 Montgomery Road, #17, NY, NY 10050",@"22 Montgomery Road, #17, NY, NY 10050",nil];
    array_request_and_remarks = [[NSMutableArray alloc]initWithObjects:@"NO Chilli and Cheese.",@"NO Chilli and Cheese.", @"NO Chilli and Cheese.",@"NO Chilli and Cheese.",nil];
    [self integrateHeader];
    [self integrateBodyDesign];

}
-(void)viewWillAppear:(BOOL)animated
{
    [self AFCartDetails];
    
}
-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [icon_back addTarget:self action:@selector(click_on_back_arrow:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:icon_back];
    
    
    UILabel *lbl_order_details = [[UILabel alloc]init];
    lbl_order_details.frame = CGRectMake(CGRectGetMaxX(icon_back .frame)+30,0, 300, 45);
    lbl_order_details.text = @"Proceed to Payment";
    lbl_order_details.font = [UIFont fontWithName:kFont size:20];
    lbl_order_details.textColor = [UIColor whiteColor];
    lbl_order_details.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_order_details];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}
-(void)integrateBodyDesign
{
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor whiteColor];
    scroll.frame = CGRectMake(05,50,WIDTH-10,HEIGHT-150);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    UIImageView *icon_now = [[UIImageView alloc]init];
    icon_now.frame = CGRectMake(20, 15, 30, 30);
    [icon_now setImage:[UIImage imageNamed:@"now@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_now setUserInteractionEnabled:YES];
    [scroll addSubview:icon_now];
    
    UILabel *lbl_food_now = [[UILabel alloc]init];
    lbl_food_now.frame = CGRectMake(CGRectGetMaxX(icon_now.frame)+10,05, 200, 45);
    lbl_food_now.text = @"Food Now";
    lbl_food_now.font = [UIFont fontWithName:kFontBold size:15];
    lbl_food_now.textColor = [UIColor blackColor];
    lbl_food_now.backgroundColor = [UIColor clearColor];
    [scroll addSubview:lbl_food_now];
    
    
    UIImageView *img_line = [[UIImageView alloc]init];
    img_line.frame = CGRectMake(20, CGRectGetMaxY(icon_now.frame)+8, WIDTH-40, 0.5);
    [img_line setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_line setUserInteractionEnabled:YES];
    [scroll addSubview:img_line];
    
#pragma mark Tableview
    
    img_table = [[UITableView alloc] init ];
    img_table.frame  = CGRectMake(5,CGRectGetMaxY(img_line.frame)+5,WIDTH-26,HEIGHT-310);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    img_table.backgroundColor = [UIColor clearColor];
    [scroll addSubview:img_table];
    
        UILabel *lbl_food_bill = [[UILabel alloc]init];
        lbl_food_bill.frame = CGRectMake(10,CGRectGetMaxY(img_table.frame), 150, 15);
        lbl_food_bill.text = @"Total food bill";
        lbl_food_bill.font = [UIFont fontWithName:kFont size:14];
        lbl_food_bill.textColor = [UIColor blackColor];
        lbl_food_bill.backgroundColor = [UIColor clearColor];
        [scroll addSubview:lbl_food_bill];
    
       food_bill_val = [[UILabel alloc]init];
        food_bill_val.frame = CGRectMake(WIDTH-70,CGRectGetMaxY(img_table.frame), 100, 15);
        
        food_bill_val.font = [UIFont fontWithName:kFontBold size:13];
        food_bill_val.textColor = [UIColor blackColor];
        food_bill_val.backgroundColor = [UIColor clearColor];
        [scroll addSubview:food_bill_val];
    
        UILabel *lbl_not86_sevice_fee = [[UILabel alloc]init];
        lbl_not86_sevice_fee.frame = CGRectMake(10,CGRectGetMaxY(lbl_food_bill.frame)+5, 200, 15);
        lbl_not86_sevice_fee.text = @"Total not86 service fee(7.5%)";
        lbl_not86_sevice_fee.font = [UIFont fontWithName:kFont size:14];
        lbl_not86_sevice_fee.textColor = [UIColor blackColor];
        lbl_not86_sevice_fee.backgroundColor = [UIColor clearColor];
        [scroll addSubview:lbl_not86_sevice_fee];
    
       not86_service_fee_val = [[UILabel alloc]init];
        not86_service_fee_val.frame = CGRectMake(WIDTH-70,CGRectGetMaxY(food_bill_val.frame)+5, 70, 15);
       
        not86_service_fee_val.font = [UIFont fontWithName:kFontBold size:13];
        not86_service_fee_val.textColor = [UIColor blackColor];
        not86_service_fee_val.backgroundColor = [UIColor clearColor];
        [scroll addSubview:not86_service_fee_val];
    
       UILabel* lbl_total_delivery_charge = [[UILabel alloc]init];
        lbl_total_delivery_charge.frame = CGRectMake(10,CGRectGetMaxY(lbl_not86_sevice_fee.frame)+5, 200, 15);
        lbl_total_delivery_charge.text = @"Total delivery charges";
        lbl_total_delivery_charge.font = [UIFont fontWithName:kFont size:14];
        lbl_total_delivery_charge.textColor = [UIColor blackColor];
        lbl_total_delivery_charge.backgroundColor = [UIColor clearColor];
        [scroll addSubview:lbl_total_delivery_charge];
    
        total_delivery_charge_val = [[UILabel alloc]init];
        total_delivery_charge_val.frame = CGRectMake(WIDTH-70,CGRectGetMaxY(not86_service_fee_val.frame)+5, 60, 15);
       
        total_delivery_charge_val.font = [UIFont fontWithName:kFontBold size:13];
        total_delivery_charge_val.textColor = [UIColor blackColor];
        total_delivery_charge_val.backgroundColor = [UIColor clearColor];
        [scroll addSubview:total_delivery_charge_val];
    
    UILabel *lbl_total_to_be_charge = [[UILabel alloc]init];
    lbl_total_to_be_charge.frame = CGRectMake(10,CGRectGetMaxY(lbl_total_delivery_charge.frame)+5, 200, 45);
    lbl_total_to_be_charge.text = @"Total to be charged";
    lbl_total_to_be_charge.font = [UIFont fontWithName:kFontBold size:14];
    lbl_total_to_be_charge.textColor = [UIColor blackColor];
    lbl_total_to_be_charge.backgroundColor = [UIColor clearColor];
    [scroll addSubview:lbl_total_to_be_charge];
    
    total_be_charged_val = [[UILabel alloc]init];
    total_be_charged_val.frame = CGRectMake(WIDTH-70,CGRectGetMaxY(total_delivery_charge_val.frame)+5, 50, 45);
    total_be_charged_val.font = [UIFont fontWithName:kFontBold size:16];
    total_be_charged_val.textColor = [UIColor blackColor];
    total_be_charged_val.backgroundColor = [UIColor clearColor];
    [scroll addSubview:total_be_charged_val];
    
    
    UIImageView *img_check_out = [[UIImageView alloc]init];
    img_check_out.frame = CGRectMake(25,CGRectGetMaxY(scroll.frame)+20, WIDTH-50,45);
    [img_check_out setImage:[UIImage imageNamed:@"button-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_check_out setUserInteractionEnabled:YES];
    [self.view addSubview:img_check_out];
    
    UILabel *lbl_check_out = [[UILabel alloc]init];
    lbl_check_out.frame = CGRectMake(0,0,  WIDTH-50, 45);
    lbl_check_out.text = @"CONFIRM REQUEST";
    [lbl_check_out setUserInteractionEnabled:YES];
    lbl_check_out.font = [UIFont fontWithName:kFont size:18];
    lbl_check_out.textColor = [UIColor whiteColor];
    lbl_check_out.backgroundColor = [UIColor clearColor];
    lbl_check_out.textAlignment = NSTextAlignmentCenter;
    [img_check_out addSubview:lbl_check_out];
    
    UIButton *btn_on_img_check_out = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_img_check_out.frame = CGRectMake(25,CGRectGetMaxY(scroll.frame)+20, WIDTH-50,45);
    btn_on_img_check_out .backgroundColor = [UIColor clearColor];
    // [btn_on_img_proced_to_payments setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [btn_on_img_check_out addTarget:self action:@selector(click_on_check_out_btn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:btn_on_img_check_out];
    

}


-(void)CallPayPal
{
    
    //  cartvalue(10:10:10:2009-10-11:2009-10-11|10:10:10:2009-10-11:2009-10-11:location:hours) (pid:qty:subtotal:fromrent:torent:location:hours)
    
    
    //AbXePxAzYq7ciO65Qg8YfolhTMYTR8ugWMeAfw7decui5sFHtwNwVkyD8dCv
    //    AZ3ZoBAYrGNTf15oKGtrOSrRb1zSEOwzpi-zwAyc1DLWCquqwx8mkoIoUeoG    old
    
    [PayPalMobile initializeWithClientIdsForEnvironments:@{
                                                           PayPalEnvironmentSandbox : @"AZ3ZoBAYrGNTf15oKGtrOSrRb1zSEOwzpi-zwAyc1DLWCquqwx8mkoIoUeoG"}];
    
    
    PayPalVC *vc = [[PayPalVC alloc]init];
    vc.str_Price = [NSString stringWithFormat:@"%@",[[array_Amount objectAtIndex:0]valueForKey:@"Grand_Total"]];
    vc.str_SERVINGTYPE = str_typeofserve;
    [self presentViewController:vc animated:NO completion:nil];
    
    //[self.navigationController pushViewController:vc];
    
}


#pragma table_view delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [array_total_Items count];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 170;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-20, 170);
        
    }
    else if (IS_IPHONE_6)
    {
        img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-5, 170);
        
    }
    else
    {    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-10,170);
        
    }
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    img_cellBackGnd.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [cell.contentView addSubview:img_cellBackGnd];
    
    
    UIImageView *img_serving_type = [[UIImageView alloc] init];
    img_serving_type.frame = CGRectMake(10,3,25,25);
   // [img_serving_type setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_serving_type_imges objectAtIndex:indexPath.row]]]];
    //[img_cellBackGnd addSubview:img_serving_type];

    UILabel *lbl_dish_name = [[UILabel alloc]init];
    lbl_dish_name.frame = CGRectMake(CGRectGetMaxX(img_serving_type.frame)+5,5,200,25);
    lbl_dish_name.text = [NSString stringWithFormat:@"%@",[[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"item_name"]];
    lbl_dish_name.font = [UIFont fontWithName:kFontBold size:12];
    lbl_dish_name.textColor = [UIColor blackColor];
    lbl_dish_name.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_dish_name];
    
    
    
        UILabel *lbl_serving_date_and_time = [[UILabel alloc]init];
        lbl_serving_date_and_time.frame = CGRectMake(10,CGRectGetMaxY(img_serving_type.frame)+3,160, 25);
        lbl_serving_date_and_time.text = @"Reruested Serving Date/Time:";
        lbl_serving_date_and_time.font = [UIFont fontWithName:kFont size:11];
        lbl_serving_date_and_time.textColor = [UIColor blackColor];
        lbl_serving_date_and_time.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_serving_date_and_time];
    
        UILabel *lbl_array_serving_date_and_time = [[UILabel alloc]init];
        lbl_array_serving_date_and_time.frame = CGRectMake(CGRectGetMaxX(lbl_serving_date_and_time.frame)+5,CGRectGetMaxY(img_serving_type.frame)+3,120, 25);
        lbl_array_serving_date_and_time.text = [NSString stringWithFormat:@"%@",[[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"requesting_datetime"]];
        lbl_array_serving_date_and_time.font = [UIFont fontWithName:kFontBold size:11];
        lbl_array_serving_date_and_time.textColor = [UIColor blackColor];
        lbl_array_serving_date_and_time.backgroundColor = [UIColor clearColor];
      lbl_array_serving_date_and_time.textAlignment = NSTextAlignmentLeft;
        [img_cellBackGnd addSubview:lbl_array_serving_date_and_time];
    
    
    
    
    //    UIImageView *img_dietary = [[UIImageView alloc] init];
    //    img_dietary.frame = CGRectMake(8,CGRectGetMaxY(lbl_dish_name.frame)+10,20,20);
    //    [img_dietary setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_icon_dietary objectAtIndex:indexPath.row]]]];
    //    [img_cellBackGnd addSubview:img_dietary];
    
    //    UIImageView *img_serving_type = [[UIImageView alloc] init];
    //    img_serving_type.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+255,CGRectGetMaxY(lbl_dish_name.frame)+10,20,20);
    //    [img_serving_type setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_icon_serving_type objectAtIndex:indexPath.row]]]];
    //    [img_cellBackGnd addSubview:img_serving_type];
    
    
    //    UIButton *img_cross = [UIButton buttonWithType:UIButtonTypeCustom];
    //    img_cross.frame = CGRectMake(CGRectGetMaxX(img_dietary.frame)+246,CGRectGetMaxY(lbl_dish_name.frame)-41,45,80);
    //    img_cross .backgroundColor = [UIColor clearColor];
    //    [img_cross setImage:[UIImage imageNamed:@"img-x@2x.png"] forState:UIControlStateNormal];
    //    [img_cross addTarget:self action:@selector(click_on_x_btn:) forControlEvents:UIControlEventTouchUpInside];
    //    [img_cellBackGnd   addSubview:img_cross];
    
    
    
    UIImageView *img_line3 = [[UIImageView alloc]init];
    img_line3.frame = CGRectMake(10, CGRectGetMaxY(lbl_serving_date_and_time.frame)+2, WIDTH-50, 0.5);
    [img_line3 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_line3 setUserInteractionEnabled:YES];
    [img_cellBackGnd addSubview:img_line3];
    
    UILabel *lbl_quantity = [[UILabel alloc]init];
    lbl_quantity.frame = CGRectMake(10,CGRectGetMaxY(img_line3.frame)+1,50, 30);
    lbl_quantity.text = @"Quantity:";
    lbl_quantity.font = [UIFont fontWithName:kFont size:11];
    lbl_quantity.textColor = [UIColor blackColor];
    lbl_quantity.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_quantity];
    
    UILabel *quantity_val = [[UILabel alloc]init];
    quantity_val.frame = CGRectMake(CGRectGetMaxX(lbl_quantity.frame),CGRectGetMaxY(img_line3.frame)+1,30,30);
    quantity_val.text = [NSString stringWithFormat:@"%@",[[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"quantity"]];
    quantity_val.font = [UIFont fontWithName:kFontBold size:11];
    quantity_val.textColor = [UIColor blackColor];
    quantity_val.textAlignment = NSTextAlignmentLeft;
    quantity_val.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:quantity_val];
    
    UILabel *serving_charg_val = [[UILabel alloc]init];
    serving_charg_val.frame = CGRectMake(CGRectGetMaxX(quantity_val.frame)+2,CGRectGetMaxY(img_line3.frame)+1,50,30);
    serving_charg_val.text = [NSString stringWithFormat:@"%@",[[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"service_fee"]];
    serving_charg_val.font = [UIFont fontWithName:kFontBold size:11];
    serving_charg_val.textColor = [UIColor blackColor];
    serving_charg_val.textAlignment = NSTextAlignmentCenter;
    serving_charg_val.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:serving_charg_val];
    
    
    UILabel *lbl_serving_charge = [[UILabel alloc]init];
    lbl_serving_charge.frame = CGRectMake(CGRectGetMaxX(serving_charg_val.frame),CGRectGetMaxY(img_line3.frame)+1,60, 30);
    lbl_serving_charge.text = @"/serving";
    lbl_serving_charge.font = [UIFont fontWithName:kFont size:11];
    lbl_serving_charge.textColor = [UIColor blackColor];
    lbl_serving_charge.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_serving_charge];
    
    
    UILabel *lbl_subtotal = [[UILabel alloc]init];
    lbl_subtotal.frame = CGRectMake(CGRectGetMaxX(lbl_serving_charge.frame)+5,CGRectGetMaxY(img_line3.frame)+1,50, 30);
    lbl_subtotal.text = @"Subtotal:";
    lbl_subtotal.font = [UIFont fontWithName:kFont size:11];
    lbl_subtotal.textColor = [UIColor blackColor];
    lbl_subtotal.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_subtotal];
    
    UILabel *subtotal_val = [[UILabel alloc]init];
    subtotal_val.frame = CGRectMake(CGRectGetMaxX(lbl_subtotal.frame)+6,CGRectGetMaxY(img_line3.frame)+1,50,25);
    subtotal_val.text = [NSString stringWithFormat:@"%@",[[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"sub_total"]];
    subtotal_val.font = [UIFont fontWithName:kFontBold size:11];
    subtotal_val.textAlignment = NSTextAlignmentLeft;

    subtotal_val.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    subtotal_val.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:subtotal_val];
    
    UIImageView *img_line4 = [[UIImageView alloc]init];
    img_line4.frame = CGRectMake(10, CGRectGetMaxY(lbl_quantity.frame)-5, WIDTH-50, 0.5);
    [img_line4 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_line4 setUserInteractionEnabled:YES];
    [img_cellBackGnd addSubview:img_line4];
    
    UILabel *lbl_delivery_address = [[UILabel alloc]init];
    lbl_delivery_address.frame = CGRectMake(10,CGRectGetMaxY(img_line4.frame)-8,200, 45);
    lbl_delivery_address.text = @"Delivery Address";
    lbl_delivery_address.font = [UIFont fontWithName:kFontBold size:11];
    lbl_delivery_address.textColor = [UIColor blackColor];
    lbl_delivery_address.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_delivery_address];
    
    UILabel *delivery_address_val = [[UILabel alloc]init];
    delivery_address_val.frame = CGRectMake(10,CGRectGetMaxY(lbl_delivery_address.frame)-28,300,45);
    delivery_address_val.text = [NSString stringWithFormat:@"%@",[[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"delivery_address"]];
    delivery_address_val.font = [UIFont fontWithName:kFont size:11];
    delivery_address_val.textColor = [UIColor blackColor];
    delivery_address_val.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:delivery_address_val];
    
    UILabel *lbl_request_and_remarks = [[UILabel alloc]init];
    lbl_request_and_remarks.frame = CGRectMake(10,CGRectGetMaxY(delivery_address_val.frame)-28,200, 45);
    lbl_request_and_remarks.text = @"Special Requests/Remarks";
    lbl_request_and_remarks.font = [UIFont fontWithName:kFontBold size:11];
    lbl_request_and_remarks.textColor = [UIColor blackColor];
    lbl_request_and_remarks.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_request_and_remarks];
    
    UILabel *request_and_remarks_val = [[UILabel alloc]init];
    request_and_remarks_val.frame = CGRectMake(10,CGRectGetMaxY(lbl_request_and_remarks.frame)-28,300,45);
    request_and_remarks_val.text = [NSString stringWithFormat:@"%@",[[array_total_Items objectAtIndex:indexPath.row]valueForKey:@"request_remarks"]];
    request_and_remarks_val.font = [UIFont fontWithName:kFont size:11];
    request_and_remarks_val.textColor = [UIColor blackColor];
    request_and_remarks_val.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:request_and_remarks_val];
    
    UIImageView *img_line5 = [[UIImageView alloc]init];
    img_line5.frame = CGRectMake(10, CGRectGetMaxY(request_and_remarks_val.frame)-13, WIDTH-50, 0.6);
    [img_line5 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_line5 setUserInteractionEnabled:YES];
    [img_cellBackGnd addSubview:img_line5];

    
    return cell;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma click_events
-(void)click_on_back_arrow:(UIButton *)sender
{
    NSLog(@"click_on_back_arrow:");
    [self dismissViewControllerAnimated:NO completion:nil];
}
-(void)click_on_check_out_btn:(UIButton *)sender
{
    NSLog(@"btn_on_img_check_out");
    
    [self CallPayPal];
}
#pragma mark servixcesfunctionality

-(void)AFCartDetails
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    NSDictionary *params =@{
                            
                            
                            @"uid"            :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"type"            :  @"0",
                            
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER  kChefAccountSaleSummary
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kUserDiningCart
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseCartDetailsList:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFCartDetails];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseCartDetailsList :(NSDictionary * )TheDict
{
    
    [array_total_Items removeAllObjects];
    [array_Amount removeAllObjects];
    
    NSLog(@"Response Dict: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"cart_details"] count]; i++)
        {
            [array_total_Items addObject:[[TheDict valueForKey:@"cart_details"] objectAtIndex:i]];
            
        }
        
        NSLog(@"array_for_accounts is: %@",array_total_Items);
        
        
        [array_Amount addObject:[TheDict valueForKey:@"Amount_details"]];
        
        food_bill_val.text = [NSString stringWithFormat: @"$%@", [[array_Amount objectAtIndex:0]valueForKey:@"grand_subtotal"]];
        not86_service_fee_val.text = [NSString stringWithFormat: @"$%@", [[array_Amount objectAtIndex:0]valueForKey:@"service_fee"]];
        total_delivery_charge_val.text = [NSString stringWithFormat: @"$%@", [[array_Amount objectAtIndex:0]valueForKey:@"grand_delivery_charge"]];
        total_be_charged_val.text = [NSString stringWithFormat: @"$%@", [[array_Amount objectAtIndex:0]valueForKey:@"Grand_Total"]];
        

        
        NSLog(@"array_Amount is: %@", array_Amount);
        
        //        for (int i = 0; i < [[[TheDict valueForKey:@"OrderData"]valueForKey:@"TotalitemsList"] count]; i++)
        //        {
        //            [array_total_Items addObject:[[[array_for_accounts objectAtIndex:i]valueForKey:@"TotalitemsList"] objectAtIndex:i]];
        //        }
        //        NSLog(@"array_total_Items is: %@",array_total_Items);
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        NSLog(@"Error 1 check response");
        
    }
    
    [img_table reloadData];
    
    //    [self AlergyList];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
