//
//  UserMessagesVC.m
//  Not86
//
//  Created by Admin on 05/09/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "UserMessagesVC.h"
#import "Define.h"
#import "AppDelegate.h"
#import "UIImageView+AFNetworking.h"


#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"



@interface UserMessagesVC ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
{
    UIImageView * img_header;
    UIView *view_for_notifications;
    UITextField *txt_for_search;
    UITableView * table_for_messages ;
    NSMutableArray * arra_chef_images;
    NSMutableArray * array_chef_names;
    NSMutableArray * array_order_val;
    NSMutableArray * array_subject;
    NSMutableArray * array_date_and_time;
    NSMutableArray * array_round_red_val;
    AppDelegate *delegate;

}

@end

@implementation UserMessagesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBodyDesign];
    
    arra_chef_images = [[NSMutableArray alloc]initWithObjects:@"chef1-img@2x.png",@"chef1-img@2x.png",@"chef1-img@2x.png",@"chef1-img@2x.png",nil];
    array_chef_names = [[NSMutableArray alloc]initWithObjects:@"John Doe",@"John Doe",@"John Doe",@"John Doe",nil];
    array_order_val = [[NSMutableArray alloc]initWithObjects:@"221411",@"221411",@"221411",@"221411", nil];
    array_subject = [[NSMutableArray alloc]initWithObjects:@"Lorem ipsum dolor sit",@"Lorem ipsum dolor sit",@"Lorem ipsum dolor sit",@"Lorem ipsum dolor sit",nil];
    array_date_and_time = [[NSMutableArray alloc]initWithObjects:@"18-9-2015, 2:30 PM",@"18-9-2015, 2:30 PM",@"18-9-2015, 2:30 PM",@"18-9-2015, 2:30 PM", nil];
    array_round_red_val = [[NSMutableArray alloc]initWithObjects: @"3",@"3",@"3",@"3",nil];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    [self UserAllMessages];
}


-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu.frame = CGRectMake(10,13,25,20);
    icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
    [icon_menu addTarget:self action:@selector(click_on_menu_btn:) forControlEvents:UIControlEventTouchUpInside];
   // [self.view   addSubview:icon_menu];
    
    
    UILabel *lbl_support_on_header = [[UILabel alloc]init];
    lbl_support_on_header.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 300, 45);
    lbl_support_on_header.text = @"Messages";
    lbl_support_on_header.font = [UIFont fontWithName:kFont size:20];
    lbl_support_on_header.textColor = [UIColor whiteColor];
    lbl_support_on_header.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_support_on_header];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}
-(void)integrateBodyDesign
{
    view_for_notifications = [[UIView alloc]init];
    view_for_notifications.frame = CGRectMake(0,CGRectGetMaxY(img_header.frame),WIDTH,HEIGHT+90);
    view_for_notifications.backgroundColor=[UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [view_for_notifications setUserInteractionEnabled:YES];
    [self.view addSubview:  view_for_notifications];

    
    UIImageView *bg_for_date = [[UIImageView alloc]init];
    bg_for_date.frame = CGRectMake(5, 5, WIDTH-10, 55);
    [bg_for_date setUserInteractionEnabled:YES];
    bg_for_date.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_notifications addSubview:bg_for_date];
    
    UIImageView *img_serch_bar = [[UIImageView alloc]init];
    img_serch_bar.frame = CGRectMake(5, 10, WIDTH-24, 35);
    [img_serch_bar setUserInteractionEnabled:YES];
    img_serch_bar.image=[UIImage imageNamed:@"search-bg@2x.png"];
    [bg_for_date addSubview:img_serch_bar];
    
    txt_for_search = [[UITextField alloc] init];
    txt_for_search.frame = CGRectMake(10,13.5,290,25);
    txt_for_search .borderStyle = UITextBorderStyleNone;
    txt_for_search .textColor = [UIColor lightGrayColor];
    txt_for_search .font = [UIFont fontWithName:kFont size:13];
    txt_for_search .placeholder = @"Search...";
    [txt_for_search  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_for_search  setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_for_search .leftView = padding1;
    txt_for_search .leftViewMode = UITextFieldViewModeAlways;
    txt_for_search .userInteractionEnabled=YES;
    txt_for_search .textAlignment = NSTextAlignmentLeft;
    txt_for_search .backgroundColor = [UIColor clearColor];
    txt_for_search .keyboardType = UIKeyboardTypeAlphabet;
    txt_for_search .delegate = self;
    [bg_for_date addSubview:txt_for_search ];
    
    UIButton *icon_search = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_search.frame = CGRectMake(CGRectGetMaxX(txt_for_search.frame)+5,-13,60,60);
    icon_search .backgroundColor = [UIColor clearColor];
    [icon_search setImage:[UIImage imageNamed:@"icon-search@2x.png"] forState:UIControlStateNormal];
   [icon_search addTarget:self action:@selector(click_on_search_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_serch_bar   addSubview:icon_search];
    
  
    table_for_messages = [[UITableView alloc] init ];
    table_for_messages .frame = CGRectMake(5,CGRectGetMaxY(bg_for_date.frame)+5,WIDTH-10,HEIGHT-150);
    [table_for_messages  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_messages .delegate = self;
    table_for_messages .dataSource = self;
    table_for_messages .showsVerticalScrollIndicator = NO;
    table_for_messages .backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [view_for_notifications addSubview: table_for_messages ];
    
}
#pragma  table_view_delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arra_chef_images count];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    UIView *SwipeView = [[UIView alloc]init];
    SwipeView.tag = -1001;
    [cell.contentView addSubview:SwipeView];
    
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    img_cellBackGnd.frame =  CGRectMake(-3,0, WIDTH+10, 115);
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [img_cellBackGnd  setContentMode:UIViewContentModeScaleAspectFill];
    [img_cellBackGnd  setClipsToBounds:YES];
    [SwipeView addSubview:img_cellBackGnd];
    
    UIImageView *img_chef = [[UIImageView alloc] init];
    img_chef.frame = CGRectMake(10,20,80,80 );
    [img_chef setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arra_chef_images objectAtIndex:indexPath.row]]]];
    [img_cellBackGnd addSubview:img_chef];
    
    UILabel *lbl_chef_name = [[UILabel alloc]init];
    lbl_chef_name.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+8,20,200, 15);
    lbl_chef_name.text = [NSString stringWithFormat:@"%@",[array_chef_names objectAtIndex:indexPath.row]];
    lbl_chef_name.font = [UIFont fontWithName:kFontBold size:18];
    lbl_chef_name.textColor = [UIColor blackColor];
    lbl_chef_name.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_chef_name];
    
    UILabel *lbl_order_number = [[UILabel alloc]init];
    lbl_order_number.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+8,CGRectGetMaxY(lbl_chef_name.frame)+4,200, 15);
    lbl_order_number.text = @"Order Number:";
    lbl_order_number.font = [UIFont fontWithName:kFont size:14];
    lbl_order_number.textColor = [UIColor blackColor];
    lbl_order_number.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:lbl_order_number];
    
    UILabel *order_val = [[UILabel alloc]init];
    order_val.frame = CGRectMake(CGRectGetMidX(lbl_order_number.frame)+8,CGRectGetMaxY(lbl_chef_name.frame)+4,200, 15);
    order_val.text = [NSString stringWithFormat:@"%@",[ array_order_val objectAtIndex:indexPath.row]];
    order_val.font = [UIFont fontWithName:kFontBold size:14];
    order_val.textColor = [UIColor blackColor];
    order_val.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:order_val];
    
    UILabel *lbl_subject = [[UILabel alloc]init];
    lbl_subject.frame = CGRectMake(CGRectGetMaxX(img_chef.frame)+8,CGRectGetMaxY(lbl_order_number.frame)+4,200, 15);
    lbl_subject.text = @"Subject:";
    lbl_subject.font = [UIFont fontWithName:kFont size:14];
    lbl_subject.textColor = [UIColor blackColor];
    lbl_subject.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd  addSubview:lbl_subject];
    
    UILabel *subject_val = [[UILabel alloc]init];
    subject_val.frame = CGRectMake(CGRectGetMinX(lbl_order_number.frame)+57,CGRectGetMaxY(lbl_order_number.frame)+4,200, 15);
    subject_val.text = [NSString stringWithFormat:@"%@",[ array_subject objectAtIndex:indexPath.row]];
    subject_val.font = [UIFont fontWithName:kFontBold size:14];
    subject_val.textColor = [UIColor blackColor];
    subject_val.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:subject_val];
    
    UILabel *date_and_time_val  = [[UILabel alloc]init];
    date_and_time_val.frame = CGRectMake(CGRectGetMinX(img_chef.frame)+86,CGRectGetMaxY(lbl_subject.frame)+4,200, 15);
    date_and_time_val.text = [NSString stringWithFormat:@"%@",[ array_date_and_time objectAtIndex:indexPath.row]];
    date_and_time_val.font = [UIFont fontWithName:kFont size:14];
    date_and_time_val.textColor = [UIColor blackColor];
    date_and_time_val.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:date_and_time_val];
    
    UIImageView *img_round_red = [[UIImageView alloc]init];
    img_round_red.frame = CGRectMake(325, 30, 25,25);
    [img_round_red setUserInteractionEnabled:YES];
    img_round_red.image=[UIImage imageNamed:@"icon_round_red@2x.png"];
    [img_cellBackGnd addSubview:img_round_red];
    
    UILabel *round_red_val  = [[UILabel alloc]init];
    round_red_val .frame = CGRectMake(8,2,20, 20);
    round_red_val .text = [NSString stringWithFormat:@"%@",[ array_round_red_val objectAtIndex:indexPath.row]];
    round_red_val .font = [UIFont fontWithName:kFontBold size:14];
    round_red_val .textColor = [UIColor whiteColor];
    round_red_val .backgroundColor = [UIColor clearColor];
    [img_round_red addSubview:round_red_val ];



    
    return cell;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)click_on_menu_btn:(UIButton *)sender
{
    NSLog(@"click_on_menu_btn");
}
-(void)click_on_search_btn:(UIButton *)sender
{
    NSLog(@"click_on_search_btn");
}


#pragma user-AllMessages-functionality

-(void)UserAllMessages
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            
                            @"login_uid"                               :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:kUserAllMessagesList  parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseUserAllMessages:JSON];
    }
     
     
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error){
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self UserAllMessages];
                                         }
                                     }];
    [operation start];
    
}
-(void)ResponseUserAllMessages:(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
//        
//        for (int i=0; i<[[[TheDict valueForKey:@"Notifications"] valueForKey:@"Dishdetail"] count]; i++)
//        {
//            //                    [ary_displaynames addObject:[[[TheDict valueForKey:@"profile_info"] valueForKey:@"Personal_Information"] objectAtIndex:i]];
//            
//        }
//        
//        
//        for (int j=0; j<[[[TheDict valueForKey:@"Notifications"] valueForKey:@"notifications"] count]; j++)
//        {
//            //                   [ary_FoodinFo addObject:[[[TheDict valueForKey:@"profile_info"] valueForKey:@"Food_Info"] objectAtIndex:i]];
//            
//        }
        
        
        
        
        
        
        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
