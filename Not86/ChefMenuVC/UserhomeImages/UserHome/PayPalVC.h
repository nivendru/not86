//
//  PayPalVC.h
//  LocoGalleria
//
//  Created by User9 on 09/07/14.
//  Copyright (c) 2014 User1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayPalMobile.h"
#import "JWSlideMenuViewController.h"

@interface PayPalVC : JWSlideMenuViewController<PayPalPaymentDelegate, UIPopoverControllerDelegate>


@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;
@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;
@property(nonatomic, strong, readwrite) NSString *resultText;
@property(nonatomic, strong, readwrite) NSString *str_CartInfo;
@property(nonatomic, strong, readwrite) NSString *str_Price;
@property(nonatomic, strong, readwrite) NSString *str_SERVINGTYPE;

@property(nonatomic) BOOL bool_Cart;

@end
