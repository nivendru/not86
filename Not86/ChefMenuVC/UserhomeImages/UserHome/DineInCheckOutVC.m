//
//  DineInCheckOutVC.m
//  Not86
//
//  Created by Interworld on 02/12/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "DineInCheckOutVC.h"
#import "AddressDetailsVC.h"

@interface DineInCheckOutVC ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIImageView *img_header;
    UITableView *img_table;
    NSMutableArray*ary_dummykithennames;
    
}
@end

@implementation DineInCheckOutVC
@synthesize ary_dinindetails,ary_amountdetails,str_servetype;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    ary_dummykithennames = [NSMutableArray new];
    
    [self integrateHeader];
    [self integrateBody];
    
}


-(BOOL)prefersStatusBarHidden
{
    return YES;
}


-(void)integrateHeader
{
    
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu .frame = CGRectMake(10, 13,20,20);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu  addTarget:self action:@selector(click_on_Back_Btn:)forControlEvents:UIControlEventTouchUpInside];
    [icon_menu setImage:[UIImage imageNamed:@"img_arrow@2x"] forState:UIControlStateNormal];
    //icon_menu.layer.borderWidth = 1.0;
    [icon_menu setBackgroundColor:[UIColor clearColor]];
    [img_header   addSubview:icon_menu ];
    
    
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 150, 45);
    lbl_User_Sign_Up.text = @"Checkout";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-35, 9, 27, 27);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
    
}

-(void)integrateBody
{
    
    UIImageView *img_bg = [[UIImageView alloc]init];
    img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 440);
    //    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    img_bg.backgroundColor = [UIColor clearColor];
    //img_bg.layer.borderWidth = 1.0;
    [img_bg setUserInteractionEnabled:YES];
    [self.view addSubview:img_bg];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(10, 10, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"iicon-on_reuest@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_bg addSubview:icon_user];
    
    
    UILabel *lbl_Head = [[UILabel alloc]init];
    lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
    lbl_Head.text = @"On Request";
    lbl_Head.font = [UIFont fontWithName:kFont size:20];
    lbl_Head.textColor = [UIColor blackColor];
    lbl_Head.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_Head];
    
    
    UIImageView *line = [[UIImageView alloc]init];
    line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
    line.backgroundColor = [UIColor grayColor];
    [line setUserInteractionEnabled:YES];
    [img_bg addSubview:line];
    
    
    img_table= [[UITableView alloc] init ];
    img_table.frame  = CGRectMake(0,CGRectGetMaxY(line.frame),WIDTH-10, HEIGHT-300);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    //   img_table.layer.borderWidth = 1.0;
    img_table.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:img_table];
    
    
    
    
    
    UIView*sectionView;
    sectionView = [[UIView alloc]init];
    sectionView.backgroundColor = [UIColor whiteColor];
    //    sectionView.layer.borderWidth = 1.0;
    [self.view addSubview:sectionView];
    
    
    UIImageView *img_Footer = [[UIImageView alloc]init];
    [img_Footer setUserInteractionEnabled:YES];
    [img_Footer setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    [sectionView addSubview:img_Footer];
    
    
    UILabel *firstLabel = [[UILabel alloc]init];
    
    firstLabel.backgroundColor = [UIColor clearColor];
    firstLabel.text = @"Total food bill";
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    //    firstLabel.textAlignment = NSTextAlignmentCenter;
    firstLabel.textColor = [UIColor blackColor];
    firstLabel.font = [UIFont fontWithName:kFont size:15];
    firstLabel.lineBreakMode = NSLineBreakByWordWrapping;
    firstLabel.numberOfLines = 0;
    [img_Footer addSubview:firstLabel];
    
    
    UILabel *fstLabel = [[UILabel alloc]init];
   
    fstLabel.backgroundColor = [UIColor clearColor];
    fstLabel.text = [NSString stringWithFormat:@"$%@",[[ary_amountdetails objectAtIndex:0]valueForKey:@"grand_subtotal"]];
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    fstLabel.textAlignment = NSTextAlignmentRight;
    fstLabel.textColor = [UIColor blackColor];
    fstLabel.font = [UIFont fontWithName:kFontBold size:15];
    fstLabel.lineBreakMode = NSLineBreakByWordWrapping;
    fstLabel.numberOfLines = 0;
    [img_Footer addSubview:fstLabel];
    
    
    UILabel *secondLabel = [[UILabel alloc]init];
    
    secondLabel.backgroundColor = [UIColor clearColor];
    secondLabel.text = @"not86 service fee (7.5%)";
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    //    firstLabel.textAlignment = NSTextAlignmentCenter;
    secondLabel.textColor = [UIColor blackColor];
    secondLabel.font = [UIFont fontWithName:kFont size:15];
    secondLabel.lineBreakMode = NSLineBreakByWordWrapping;
    secondLabel.numberOfLines = 0;
    [img_Footer addSubview:secondLabel];
    
    
    
    
    UILabel *secLabel = [[UILabel alloc]init];
  
    secLabel.backgroundColor = [UIColor clearColor];
    secLabel.text = [NSString stringWithFormat:@"%@",[[ary_amountdetails objectAtIndex:0]valueForKey:@"service_fee"]];
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    secLabel.textAlignment = NSTextAlignmentRight;
    secLabel.textColor = [UIColor blackColor];
    secLabel.font = [UIFont fontWithName:kFontBold size:15];
    secLabel.lineBreakMode = NSLineBreakByWordWrapping;
    secLabel.numberOfLines = 0;
    [img_Footer addSubview:secLabel];
    
    
    
    UILabel *thirdLabel = [[UILabel alloc]init];
   
    thirdLabel.backgroundColor = [UIColor clearColor];
    thirdLabel.text = @"Total";
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    //    firstLabel.textAlignment = NSTextAlignmentCenter;
    thirdLabel.textColor = [UIColor blackColor];
    thirdLabel.font = [UIFont fontWithName:kFont size:15];
    thirdLabel.lineBreakMode = NSLineBreakByWordWrapping;
    thirdLabel.numberOfLines = 0;
    [img_Footer addSubview:thirdLabel];
    
    
    UILabel *thrdLabel = [[UILabel alloc]init];
   
    thrdLabel.backgroundColor = [UIColor clearColor];
    thrdLabel.text = [NSString stringWithFormat:@"$%@",[[ary_amountdetails objectAtIndex:0]valueForKey:@"Grand_Total"]];
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    thrdLabel.textAlignment = NSTextAlignmentRight;
    thrdLabel.textColor = [UIColor blackColor];
    thrdLabel.font = [UIFont fontWithName:kFontBold size:16];
    thrdLabel.lineBreakMode = NSLineBreakByWordWrapping;
    thrdLabel.numberOfLines = 0;
    [img_Footer addSubview:thrdLabel];
    
    
    
    
    
    
    UIButton *btn_MoreFood = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_MoreFood.frame = CGRectMake(30,CGRectGetMaxY(img_bg.frame)+20,(WIDTH-60),45);
    btn_MoreFood.layer.cornerRadius=4.0f;
    [btn_MoreFood setTitle:@"ADDRESS DETAILS" forState:UIControlStateNormal];
    [btn_MoreFood setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_MoreFood setBackgroundColor:[UIColor colorWithRed:39.0/255.0 green:37.0/255.0 blue:48.0/255.0 alpha:1.0f]];
    btn_MoreFood.titleLabel.font=[UIFont fontWithName:kFont size:14];
    [btn_MoreFood setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_MoreFood addTarget:self action:@selector(btn_MoreFood_Method:) forControlEvents:UIControlEventTouchUpInside];
    [self.view  addSubview:btn_MoreFood];
    
    
    
    if (IS_IPHONE_6Plus)
    {
        img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 600);
        icon_user.frame = CGRectMake(10, 10, 30, 30);
        lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
        line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
        img_table.frame  = CGRectMake(0,CGRectGetMaxY(line.frame),WIDTH-10, HEIGHT-280);
        //
        sectionView.frame = CGRectMake(10, HEIGHT-160, WIDTH-10, 80);
        img_Footer.frame = CGRectMake(0, 0,WIDTH-10, 80);
        firstLabel.frame = CGRectMake(15, 2, (WIDTH-50)/1.5, 24);
        fstLabel.frame = CGRectMake(WIDTH-100, 2, 80, 24);
        secondLabel.frame = CGRectMake(15, CGRectGetMaxY(firstLabel.frame), (WIDTH-50)/1.5, 24);
        secLabel.frame = CGRectMake(WIDTH-100, CGRectGetMaxY(firstLabel.frame), 80, 24);
        thirdLabel.frame = CGRectMake(15, CGRectGetMaxY(secondLabel.frame), (WIDTH-50)/1.5, 24);
        thrdLabel.frame = CGRectMake(WIDTH-100, CGRectGetMaxY(secondLabel.frame), 80, 24);
        
        btn_MoreFood.frame = CGRectMake(30,HEIGHT-60,(WIDTH-60),40);
    }
    else if (IS_IPHONE_6)
    {
        img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, HEIGHT-60);
        icon_user.frame = CGRectMake(10, 10, 30, 30);
        lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
        line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
        img_table.frame  = CGRectMake(0,CGRectGetMaxY(line.frame),WIDTH-10, HEIGHT-280);
        //
        sectionView.frame = CGRectMake(0,HEIGHT-160, WIDTH-10, 80);
        img_Footer.frame = CGRectMake(0, 0,WIDTH-10, 80);
        firstLabel.frame = CGRectMake(15, 2, (WIDTH-50)/1.5, 24);
        fstLabel.frame = CGRectMake(WIDTH-100, 2, 80, 24);
        secondLabel.frame = CGRectMake(15, CGRectGetMaxY(firstLabel.frame), (WIDTH-50)/1.5, 24);
        secLabel.frame = CGRectMake(WIDTH-100, CGRectGetMaxY(firstLabel.frame), 80, 24);
        thirdLabel.frame = CGRectMake(15, CGRectGetMaxY(secondLabel.frame), (WIDTH-50)/1.5, 24);
        thrdLabel.frame = CGRectMake(WIDTH-100, CGRectGetMaxY(secondLabel.frame), 80, 24);
        
        btn_MoreFood.frame = CGRectMake(30,HEIGHT-60,(WIDTH-60),40);
    
    }
    else if (IS_IPHONE_5)
    {
        img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 440);
        icon_user.frame = CGRectMake(10, 10, 30, 30);
        lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
        line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
        img_table.frame  = CGRectMake(0,CGRectGetMaxY(line.frame),WIDTH-10, HEIGHT-300);
//
        sectionView.frame = CGRectMake(0,HEIGHT-160, WIDTH-10, 80);
        img_Footer.frame = CGRectMake(0, 0,WIDTH-10, 80);
        firstLabel.frame = CGRectMake(15, 2, (WIDTH-50)/1.5, 24);
        fstLabel.frame = CGRectMake(WIDTH-100, 2, 80, 24);
        secondLabel.frame = CGRectMake(15, CGRectGetMaxY(firstLabel.frame), (WIDTH-50)/1.5, 24);
        secLabel.frame = CGRectMake(WIDTH-100, CGRectGetMaxY(firstLabel.frame), 80, 24);
        thirdLabel.frame = CGRectMake(15, CGRectGetMaxY(secondLabel.frame), (WIDTH-50)/1.5, 24);
        thrdLabel.frame = CGRectMake(WIDTH-100, CGRectGetMaxY(secondLabel.frame), 80, 24);
        
        btn_MoreFood.frame = CGRectMake(30,HEIGHT-60,(WIDTH-60),40);

    }
    else
    {
        img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 360);
        icon_user.frame = CGRectMake(10, 10, 30, 30);
        lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
        line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
        img_table.frame  = CGRectMake(0,CGRectGetMaxY(line.frame),WIDTH-10, HEIGHT-300);
        //
        sectionView.frame = CGRectMake(0, HEIGHT-160, WIDTH-10, 80);
        img_Footer.frame = CGRectMake(0, 0,WIDTH-10, 80);
        firstLabel.frame = CGRectMake(15, 2, (WIDTH-50)/1.5, 24);
        fstLabel.frame = CGRectMake(WIDTH-100, 2, 80, 24);
        secondLabel.frame = CGRectMake(15, CGRectGetMaxY(firstLabel.frame), (WIDTH-50)/1.5, 24);
        secLabel.frame = CGRectMake(WIDTH-100, CGRectGetMaxY(firstLabel.frame), 80, 24);
        thirdLabel.frame = CGRectMake(15, CGRectGetMaxY(secondLabel.frame), (WIDTH-50)/1.5, 24);
        thrdLabel.frame = CGRectMake(WIDTH-100, CGRectGetMaxY(secondLabel.frame), 80, 24);
        
        btn_MoreFood.frame = CGRectMake(30,HEIGHT-60,(WIDTH-60),40);
    }
}


-(void)click_on_Back_Btn: (UIButton *)sender

{
    NSLog(@"click_on_Back_Btn Click");
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(void)btn_MoreFood_Method: (UIButton *)sender

{
    NSLog(@"btn_MoreFood_Method: Click");
//    [self dismissViewControllerAnimated:NO completion:nil];
    
    AddressDetailsVC *objDineInFN = [AddressDetailsVC new];
    objDineInFN.ary_dishdivary = ary_dinindetails;
    objDineInFN.ary_dishdivaryamont = ary_amountdetails;
    objDineInFN.str_Type = str_servetype;
    [self presentViewController:objDineInFN animated:NO completion:nil];
    
}


#pragma mark TableviewDelegate&Datasources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

//-(CGFloat)tableView: (UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 40.0;
//}
//
//
//-(UIView *)tableView : (UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    
//    UIView*sectionView;
//    sectionView = [[UIView alloc]init];
//    sectionView.frame = CGRectMake(0, 0, WIDTH-10, 40);
//    sectionView.backgroundColor = [UIColor whiteColor];
//    //    sectionView.layer.borderWidth = 1.0;
//    
//    UILabel *firstLabel = [[UILabel alloc]init];
//    firstLabel.frame = CGRectMake(15, 5, WIDTH-10, 35);
//    firstLabel.backgroundColor = [UIColor clearColor];
//    firstLabel.text = @"Doe's Kitchen";
//    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
//    //    firstLabel.textAlignment = NSTextAlignmentCenter;
//    firstLabel.textColor = [UIColor blackColor];
//    firstLabel.font = [UIFont fontWithName:kFontBold size:15];
//    firstLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    firstLabel.numberOfLines = 0;
//    [sectionView addSubview:firstLabel];
//    
//    
//    UIImageView *Line = [[UIImageView alloc]init];
//    Line.frame = CGRectMake(12, CGRectGetMaxY(firstLabel.frame)-5,(WIDTH-10)/3.0 ,2);
//    Line.backgroundColor = [UIColor blackColor];
//    [sectionView addSubview:Line];
//    
//    return sectionView;
//}
//

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    return 0.0;
//}
//
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    UIView*sectionView;
//    sectionView = [[UIView alloc]init];
//    sectionView.frame = CGRectMake(0, 0, WIDTH-10, 80);
//    sectionView.backgroundColor = [UIColor whiteColor];
//    //    sectionView.layer.borderWidth = 1.0;
//    
//    
//    UIImageView *img_Footer = [[UIImageView alloc]init];
//    img_Footer.frame = CGRectMake(0, 0,WIDTH-10, 80);
//    [img_Footer setUserInteractionEnabled:YES];
//    [img_Footer setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
//    [sectionView addSubview:img_Footer];
//    
//    
//    UILabel *firstLabel = [[UILabel alloc]init];
//    firstLabel.frame = CGRectMake(15, 2, (WIDTH-50)/1.5, 24);
//    firstLabel.backgroundColor = [UIColor clearColor];
//    firstLabel.text = @"Doe's Kitchen";
//    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
//    //    firstLabel.textAlignment = NSTextAlignmentCenter;
//    firstLabel.textColor = [UIColor blackColor];
//    firstLabel.font = [UIFont fontWithName:kFont size:15];
//    firstLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    firstLabel.numberOfLines = 0;
//    [img_Footer addSubview:firstLabel];
//    
//    
//    UILabel *fstLabel = [[UILabel alloc]init];
//    fstLabel.frame = CGRectMake(WIDTH-100, 2, 80, 24);
//    fstLabel.backgroundColor = [UIColor clearColor];
//    fstLabel.text = @"$ 1200.99";
//    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
//      fstLabel.textAlignment = NSTextAlignmentRight;
//    fstLabel.textColor = [UIColor blackColor];
//    fstLabel.font = [UIFont fontWithName:kFontBold size:15];
//    fstLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    fstLabel.numberOfLines = 0;
//    [img_Footer addSubview:fstLabel];
//
//    
//    UILabel *secondLabel = [[UILabel alloc]init];
//    secondLabel.frame = CGRectMake(15, CGRectGetMaxY(firstLabel.frame), (WIDTH-50)/1.5, 24);
//    secondLabel.backgroundColor = [UIColor clearColor];
//    secondLabel.text = @"Doe's Kitchen afrweyedrtuy";
//    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
//    //    firstLabel.textAlignment = NSTextAlignmentCenter;
//    secondLabel.textColor = [UIColor blackColor];
//    secondLabel.font = [UIFont fontWithName:kFont size:15];
//    secondLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    secondLabel.numberOfLines = 0;
//    [img_Footer addSubview:secondLabel];
//    
//    
//    
//    
//    UILabel *secLabel = [[UILabel alloc]init];
//    secLabel.frame = CGRectMake(WIDTH-100, CGRectGetMaxY(firstLabel.frame), 80, 24);
//    secLabel.backgroundColor = [UIColor clearColor];
//    secLabel.text = @"$ 1233.13";
//    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
//        secLabel.textAlignment = NSTextAlignmentRight;
//    secLabel.textColor = [UIColor blackColor];
//    secLabel.font = [UIFont fontWithName:kFontBold size:15];
//    secLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    secLabel.numberOfLines = 0;
//    [img_Footer addSubview:secLabel];
//    
//    
//    
//    UILabel *thirdLabel = [[UILabel alloc]init];
//    thirdLabel.frame = CGRectMake(15, CGRectGetMaxY(secondLabel.frame), (WIDTH-50)/1.5, 24);
//    thirdLabel.backgroundColor = [UIColor clearColor];
//    thirdLabel.text = @"Total";
//    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
//    //    firstLabel.textAlignment = NSTextAlignmentCenter;
//    thirdLabel.textColor = [UIColor blackColor];
//    thirdLabel.font = [UIFont fontWithName:kFont size:15];
//    thirdLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    thirdLabel.numberOfLines = 0;
//    [img_Footer addSubview:thirdLabel];
//    
//    
//    UILabel *thrdLabel = [[UILabel alloc]init];
//    thrdLabel.frame = CGRectMake(WIDTH-100, CGRectGetMaxY(secondLabel.frame), 80, 24);
//    thrdLabel.backgroundColor = [UIColor clearColor];
//    thrdLabel.text = @"$ 122.19";
//    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
//        thrdLabel.textAlignment = NSTextAlignmentRight;
//    thrdLabel.textColor = [UIColor blackColor];
//    thrdLabel.font = [UIFont fontWithName:kFontBold size:18];
//    thrdLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    thrdLabel.numberOfLines = 0;
//    [img_Footer addSubview:thrdLabel];
//    
//    
//    
//    return sectionView;
//
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ary_dinindetails count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 140;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
        for (UIView *view in cell.contentView.subviews)
        {
            [view removeFromSuperview];
        }
    
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-5, 140);
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    img_cellBackGnd.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [cell.contentView addSubview:img_cellBackGnd];
    
    
    UILabel *firstLabel = [[UILabel alloc]init];
    firstLabel.frame = CGRectMake(15, 5, WIDTH-10, 30);
    firstLabel.backgroundColor = [UIColor clearColor];
    firstLabel.textColor = [UIColor blackColor];
    firstLabel.font = [UIFont fontWithName:kFontBold size:15];
    firstLabel.lineBreakMode = NSLineBreakByWordWrapping;
    firstLabel.numberOfLines = 0;
    [img_cellBackGnd addSubview:firstLabel];
    
    
    UIImageView *Line = [[UIImageView alloc]init];
    Line.frame = CGRectMake(12, CGRectGetMaxY(firstLabel.frame)-5,(WIDTH-10)/3.0 ,2);
    Line.backgroundColor = [UIColor blackColor];
    [img_cellBackGnd addSubview:Line];
    
    
    
    if (![ary_dummykithennames containsObject:[[ary_dinindetails objectAtIndex:indexPath.row]valueForKey:@"kitchen_name"]])
    {
        firstLabel.text = [NSString stringWithFormat: @"%@", [[ary_dinindetails objectAtIndex:indexPath.row]valueForKey:@"kitchen_name"]];
        [ary_dummykithennames addObject:[[ary_dinindetails objectAtIndex:indexPath.row]valueForKey:@"kitchen_name"]];
        firstLabel.hidden = NO;
        Line.hidden= NO;
    }
    else
    {
        firstLabel.hidden = YES;
        Line.hidden= YES;
        
    }

    
    UILabel *cell_head  = [[UILabel alloc]init];
    cell_head.frame = CGRectMake(10, CGRectGetMaxY(Line.frame)+3, WIDTH-40,20);
    //        round_red_val .text = [NSString stringWithFormat:@"%@",[array_round_red_val objectAtIndex:indexPath.row]];
    cell_head .text = [NSString stringWithFormat: @"%@", [[ary_dinindetails objectAtIndex:indexPath.row]valueForKey:@"item_name" ]];
    //    cell_head.textAlignment = NSTextAlignmentCenter;
    cell_head .font = [UIFont fontWithName:kFontBold size:14];
    cell_head .textColor = [UIColor blackColor];
    cell_head .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:cell_head];
    
    
    
    UIImageView *img_line = [[UIImageView alloc] init];
    img_line.frame = CGRectMake(15,CGRectGetMaxY(cell_head.frame)+5, WIDTH-40, 0.5 );
    [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
    [img_cellBackGnd addSubview:img_line];
    
    
    
    UILabel *quantity_Lbl = [[UILabel alloc]init];
    quantity_Lbl.frame = CGRectMake(15,CGRectGetMaxY(img_line.frame),(WIDTH-50)/3.0, 25);
    //quantity_Lbl.text = @"87.4%";
    // likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
    quantity_Lbl.font = [UIFont fontWithName:kFont size:10];
    quantity_Lbl.textColor = [UIColor lightGrayColor];
    quantity_Lbl.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:quantity_Lbl];
    
    
    
    UILabel *serving_Lbl = [[UILabel alloc]init];
    serving_Lbl.frame = CGRectMake(CGRectGetMaxX(quantity_Lbl.frame),CGRectGetMaxY(img_line.frame),(WIDTH-50)/3.0, 25);
    // likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
    serving_Lbl.font = [UIFont fontWithName:kFont size:10];
    serving_Lbl.textColor = [UIColor lightGrayColor];
    serving_Lbl.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:serving_Lbl];
    
    
    UILabel *likes = [[UILabel alloc]init];
    likes.frame = CGRectMake(CGRectGetMaxX(serving_Lbl.frame),CGRectGetMaxY(img_line.frame),(WIDTH-50)/3.0, 25);
    // likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
    likes.font = [UIFont fontWithName:kFont size:10];
    likes.textColor = [UIColor lightGrayColor];
    likes.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:likes];
    
    quantity_Lbl.text = [NSString stringWithFormat: @"Quantity: %@", [[ary_dinindetails objectAtIndex:indexPath.row]valueForKey:@"quantity" ]];
    serving_Lbl.text = [NSString stringWithFormat: @"$%@/Serving", [[ary_dinindetails objectAtIndex:indexPath.row]valueForKey:@"price" ]];
    likes.text = [NSString stringWithFormat: @"Subtotal:$%@", [[ary_dinindetails objectAtIndex:indexPath.row]valueForKey:@"sub_total" ]];
    
    //        UILabel *labl_time_and_date = [[UILabel alloc]init];
    //        //labl_time_and_date.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
    //        //        labl_time_and_date.text = [NSString stringWithFormat:@"%@",[array_time_and_date objectAtIndex:indexPath.row]];
    //        labl_time_and_date.font = [UIFont fontWithName:kFont size:10];
    //        labl_time_and_date.textColor = [UIColor blackColor];
    //        labl_time_and_date.backgroundColor = [UIColor clearColor];
    //        [img_cellBackGnd addSubview:labl_time_and_date];
    
    
    
    
    
    
    
    
    
    
    
    //        if (IS_IPHONE_6Plus)
    //        {
    //            img_dish.frame = CGRectMake(5,10, 95,  95 );
    //            btn_dish.frame=CGRectMake(0, 0, 95, 95);
    //            img_favorite.frame = CGRectMake(3,62, 30, 30);
    //            img_round_red.frame = CGRectMake(62, 3, 30,30);
    //            round_red_val .frame = CGRectMake(0,0,30, 30);
    //            icon_delete.frame = CGRectMake(WIDTH-60,15, 40, 40);
    //            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,10,200, 20);
    //            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,CGRectGetMaxY(dish_name.frame)+7,15, 25);
    //            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,CGRectGetMaxY(dish_name.frame)+7,200, 20);
    //            collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+5, CGRectGetMaxY(meters.frame)+15, 140, 35);
    //            doller_rate.frame = CGRectMake(WIDTH-65,85,200, 15);
    //            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+17, WIDTH-40, 0.5 );
    //            icon_take.frame = CGRectMake(15, CGRectGetMaxY(img_line.frame)+5, 30, 30);
    //            icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //            img_btn_seving_Now.frame = CGRectMake(180,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //            img_btn_chef_menu.frame = CGRectMake(220,CGRectGetMaxY(img_line.frame)+12,  30, 20);
    //            icon_thumb.frame = CGRectMake(325,CGRectGetMaxY(img_line.frame)+10, 20, 20);
    //            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+7,CGRectGetMaxY(img_line.frame)+12,100, 20);
    //            add_Date_lbl.frame = CGRectMake((WIDTH-10)/2.0+30,168,(WIDTH-10)/2.0, 20);
    //
    //            dish_name.font = [UIFont fontWithName:kFontBold size:15];
    //
    //        }
    //        else if (IS_IPHONE_6)
    //        {
    //            img_dish.frame = CGRectMake(5,10, 95,  95 );
    //            btn_dish.frame=CGRectMake(0, 0, 95, 95);
    //            img_favorite.frame = CGRectMake(3,62, 30, 30);
    //            img_round_red.frame = CGRectMake(62, 3, 30,30);
    //            round_red_val .frame = CGRectMake(0,0,30, 30);
    //            icon_delete.frame = CGRectMake(WIDTH-60,15, 40, 40);
    //            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,10,200, 20);
    //            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,CGRectGetMaxY(dish_name.frame)+7,15, 25);
    //            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,CGRectGetMaxY(dish_name.frame)+7,200, 20);
    //            collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+5, CGRectGetMaxY(meters.frame)+15, 140, 35);
    //            doller_rate.frame = CGRectMake(WIDTH-65,85,200, 15);
    //            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+17, WIDTH-40, 0.5 );
    //            icon_take.frame = CGRectMake(15, CGRectGetMaxY(img_line.frame)+5, 30, 30);
    //            icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //            img_btn_seving_Now.frame = CGRectMake(150,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //            img_btn_chef_menu.frame = CGRectMake(190,CGRectGetMaxY(img_line.frame)+12,  30, 20);
    //            icon_thumb.frame = CGRectMake(290,CGRectGetMaxY(img_line.frame)+10, 20, 20);
    //            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+7,CGRectGetMaxY(img_line.frame)+12,100, 20);
    //            add_Date_lbl.frame = CGRectMake((WIDTH-10)/2.0+15,168,(WIDTH-10)/2.0, 20);
    //            //                labl_time_and_date.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)-100,CGRectGetMaxY(img_cellBackGnd.frame)-3,300, 10);
    //            dish_name.font = [UIFont fontWithName:kFontBold size:15];
    //
    //        }
    //        else if (IS_IPHONE_5)
    //        {
    //            img_dish.frame = CGRectMake(5,10, 95,  95 );
    //            btn_dish.frame=CGRectMake(0, 0, 95, 95);
    //            img_favorite.frame = CGRectMake(3,62, 30, 30);
    //            img_round_red.frame = CGRectMake(62, 3, 30,30);
    //            round_red_val .frame = CGRectMake(0,0,30, 30);
    //            icon_delete.frame = CGRectMake(WIDTH-60,15, 40, 40);
    //            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,10,200, 20);
    //            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,CGRectGetMaxY(dish_name.frame)+7,15, 25);
    //            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,CGRectGetMaxY(dish_name.frame)+7,200, 20);
    //            collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+5, CGRectGetMaxY(meters.frame)+15, 140, 35);
    //
    //            doller_rate.frame = CGRectMake(WIDTH-65,85,200, 15);
    //            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+17, WIDTH-40, 0.5 );
    //            icon_take.frame = CGRectMake(15, CGRectGetMaxY(img_line.frame)+5, 30, 30);
    //            icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //            img_btn_chef_menu.frame = CGRectMake(170,CGRectGetMaxY(img_line.frame)+12,  30, 20);
    //            icon_thumb.frame = CGRectMake(240,CGRectGetMaxY(img_line.frame)+10, 20, 20);
    //            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+7,CGRectGetMaxY(img_line.frame)+12,100, 20);
    //            add_Date_lbl.frame = CGRectMake((WIDTH-10)/2.0,168,(WIDTH-10)/2.0, 20);
    //
    //            dish_name.font = [UIFont fontWithName:kFontBold size:15];
    //
    //        }
    //        else
    //        {
    //            img_dish.frame = CGRectMake(5,10, 95,  95 );
    //            btn_dish.frame=CGRectMake(0, 0, 95, 95);
    //            img_favorite.frame = CGRectMake(3,62, 30, 30);
    //            img_round_red.frame = CGRectMake(62, 3, 30,30);
    //            round_red_val .frame = CGRectMake(0,0,30, 30);
    //            icon_delete.frame = CGRectMake(WIDTH-60,15, 40, 40);
    //            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,10,200, 20);
    //            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+7,CGRectGetMaxY(dish_name.frame)+7,15, 25);
    //            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,CGRectGetMaxY(dish_name.frame)+7,200, 20);
    //            collView_serviceDirectory.frame=CGRectMake(CGRectGetMaxX(img_dish.frame)+5, CGRectGetMaxY(meters.frame)+15, 140, 35);
    //            doller_rate.frame = CGRectMake(WIDTH-65,85,200, 15);
    //            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+17, WIDTH-40, 0.5 );
    //            icon_take.frame = CGRectMake(15, CGRectGetMaxY(img_line.frame)+5, 30, 30);
    //            icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
    //            img_btn_chef_menu.frame = CGRectMake(170,CGRectGetMaxY(img_line.frame)+12,  30, 20);
    //            icon_thumb.frame = CGRectMake(240,CGRectGetMaxY(img_line.frame)+10, 20, 20);
    //            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+7,CGRectGetMaxY(img_line.frame)+12,100, 20);
    //            add_Date_lbl.frame = CGRectMake((WIDTH-10)/2.0,168,(WIDTH-10)/2.0, 20);
    //                dish_name.font = [UIFont fontWithName:kFontBold size:15];
    //        }
    
    
    
    
    
    
    return cell;
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
