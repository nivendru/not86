//
//  DineInAddressDetailVC.m
//  Not86
//
//  Created by Interworld on 03/12/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "DineInAddressDetailVC.h"
#import "DineInPaymentVC.h"

@interface DineInAddressDetailVC ()<UITextFieldDelegate,UITextViewDelegate, UITableViewDataSource,UITableViewDelegate>

{
    UIImageView *img_header;
    UITableView *img_table;
}

@end

@implementation DineInAddressDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBody];
    
}


-(BOOL)prefersStatusBarHidden
{
    return YES;
}


-(void)integrateHeader
{
    
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu .frame = CGRectMake(10, 13,25,20);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu  addTarget:self action:@selector(click_on_Back_Btn:)forControlEvents:UIControlEventTouchUpInside];
    [icon_menu setImage:[UIImage imageNamed:@"img_arrow@2x"] forState:UIControlStateNormal];
    //    icon_menu.layer.borderWidth = 1.0;
    [icon_menu setBackgroundColor:[UIColor clearColor]];
    [img_header   addSubview:icon_menu ];
    
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+20,0, 150, 45);
    lbl_User_Sign_Up.text = @"Address Details";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-35, 9, 27, 27);
    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
    
}

-(void)integrateBody
{
    
    UIImageView *img_bg = [[UIImageView alloc]init];
    img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 440);
    //    [icon_user setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    img_bg.layer.borderWidth = 1.0;
    [img_bg setUserInteractionEnabled:YES];
    [self.view addSubview:img_bg];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(10, 10, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"iicon-on_reuest@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_bg addSubview:icon_user];
    
    
    UILabel *lbl_Head = [[UILabel alloc]init];
    lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
    lbl_Head.text = @"On Request";
    lbl_Head.font = [UIFont fontWithName:kFont size:20];
    lbl_Head.textColor = [UIColor blackColor];
    lbl_Head.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_Head];
    
    
    UIImageView *line = [[UIImageView alloc]init];
    line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
    line.backgroundColor = [UIColor grayColor];
    [line setUserInteractionEnabled:YES];
    [img_bg addSubview:line];
    
    
    img_table= [[UITableView alloc] init ];
    img_table.frame  = CGRectMake(0,CGRectGetMaxY(line.frame),WIDTH-10,390);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    img_table.layer.borderWidth = 1.0;
    img_table.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:img_table];
    
    
    UIButton *btn_ProceedPayment = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_ProceedPayment.frame = CGRectMake(30,CGRectGetMaxY(img_bg.frame)+20,(WIDTH-60),45);
    btn_ProceedPayment.layer.cornerRadius=4.0f;
    [btn_ProceedPayment setTitle:@"Proceed to Payment" forState:UIControlStateNormal];
    [btn_ProceedPayment setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_ProceedPayment setBackgroundColor:[UIColor colorWithRed:39.0/255.0 green:37.0/255.0 blue:48.0/255.0 alpha:1.0f]];
    btn_ProceedPayment.titleLabel.font=[UIFont fontWithName:kFont size:14];
    [btn_ProceedPayment setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_ProceedPayment addTarget:self action:@selector(btn_ProceedPayment_Method:) forControlEvents:UIControlEventTouchUpInside];
    [self.view  addSubview:btn_ProceedPayment];
    
    
    if (IS_IPHONE_6Plus)
    {
        img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 600);
        icon_user.frame = CGRectMake(10, 10, 30, 30);
        lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
        line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
        img_table.frame  = CGRectMake(0,CGRectGetMaxY(line.frame),WIDTH-10,550);
        btn_ProceedPayment.frame = CGRectMake(30,CGRectGetMaxY(img_bg.frame)+25,(WIDTH-60),50);
    }
    else if (IS_IPHONE_6)
    {
        img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 540);
        icon_user.frame = CGRectMake(10, 10, 30, 30);
        lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
        line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
        img_table.frame  = CGRectMake(0,CGRectGetMaxY(line.frame),WIDTH-10,490);
        btn_ProceedPayment.frame = CGRectMake(30,CGRectGetMaxY(img_bg.frame)+20,(WIDTH-60),50);
    }
    else if (IS_IPHONE_5)
    {
        img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 440);
        icon_user.frame = CGRectMake(10, 10, 30, 30);
        lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
        line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
        img_table.frame  = CGRectMake(0,CGRectGetMaxY(line.frame),WIDTH-10,390);
        btn_ProceedPayment.frame = CGRectMake(30,CGRectGetMaxY(img_bg.frame)+20,(WIDTH-60),45);
    }
    else
    {
        img_bg.frame = CGRectMake(5, CGRectGetMaxY(img_header.frame)+5, WIDTH-10, 360);
        icon_user.frame = CGRectMake(10, 10, 30, 30);
        lbl_Head.frame = CGRectMake(CGRectGetMaxX(icon_user.frame)+20, 10, 200, 30);
        line.frame = CGRectMake(10, CGRectGetMaxY(lbl_Head.frame)+10, WIDTH-40, 1.0);
        img_table.frame  = CGRectMake(0,CGRectGetMaxY(line.frame),WIDTH-10,310);
        btn_ProceedPayment.frame = CGRectMake(30,CGRectGetMaxY(img_bg.frame)+20,(WIDTH-60),40);
    }
}

#pragma mark ButtonSelector

-(void)click_on_Back_Btn: (UIButton *)sender

{
    NSLog(@"click_on_Back_Btn Click");
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(void)btn_ProceedPayment_Method: (UIButton *)sender

{
    NSLog(@"btn_ProceedPayment_Method: Click");
    DineInPaymentVC *obj = [DineInPaymentVC new];
   [self presentViewController:obj animated:NO completion:nil];
    
}

-(void)delibery_Btn_Method: (UIButton *)sender

{
    NSLog(@"delibery_Btn_Method Click");
    
}

-(void)apply_Btn_Method: (UIButton *)sender

{
    NSLog(@"apply_Btn_Method Click");
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
    }
    else
    {
        [sender setSelected:NO];
    }
    
}

#pragma mark TableviewDelegate&Datasources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(CGFloat)tableView: (UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}


-(UIView *)tableView : (UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView*sectionView;
    sectionView = [[UIView alloc]init];
    sectionView.frame = CGRectMake(0, 0, WIDTH-10, 40);
    sectionView.backgroundColor = [UIColor whiteColor];
    //    sectionView.layer.borderWidth = 1.0;
    
    UILabel *firstLabel = [[UILabel alloc]init];
    firstLabel.frame = CGRectMake(15, 5, WIDTH-10, 35);
    firstLabel.backgroundColor = [UIColor clearColor];
    firstLabel.text = @"Doe's Kitchen";
    //    firstLabel.text = [[ary_feedbacklist objectAtIndex:section] valueForKey:@"date"];
    //    firstLabel.textAlignment = NSTextAlignmentCenter;
    firstLabel.textColor = [UIColor blackColor];
    firstLabel.font = [UIFont fontWithName:kFontBold size:15];
    firstLabel.lineBreakMode = NSLineBreakByWordWrapping;
    firstLabel.numberOfLines = 0;
    [sectionView addSubview:firstLabel];
    
    
    UIImageView *Line = [[UIImageView alloc]init];
    Line.frame = CGRectMake(12, CGRectGetMaxY(firstLabel.frame)-5,(WIDTH-10)/3.0 ,2);
    Line.backgroundColor = [UIColor blackColor];
    [sectionView addSubview:Line];
    
    return sectionView;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 225;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    //    for (UIView *view in cell.contentView.subviews)
    //    {
    //        [view removeFromSuperview];
    //    }
    
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-5, 225);
    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    img_cellBackGnd.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    //    img_cellBackGnd.layer.borderWidth = 1.0;
    [cell.contentView addSubview:img_cellBackGnd];
    
    
    UILabel *cell_head  = [[UILabel alloc]init];
    cell_head.frame = CGRectMake(15, 5, WIDTH-80, 20. );
    //        round_red_val .text = [NSString stringWithFormat:@"%@",[array_round_red_val objectAtIndex:indexPath.row]];
    cell_head .text = @"Dish Name xyz";
    //    cell_head.textAlignment = NSTextAlignmentCenter;
    cell_head .font = [UIFont fontWithName:kFontBold size:14];
    cell_head .textColor = [UIColor blackColor];
    cell_head .backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:cell_head];
    
    
    
    UILabel *cell_head1  = [[UILabel alloc]init];
    cell_head1.frame = CGRectMake(WIDTH-50, 5, 40, 20. );
    //        round_red_val .text = [NSString stringWithFormat:@"%@",[array_round_red_val objectAtIndex:indexPath.row]];
    cell_head1 .text = @"x3";
    //    cell_head1.textAlignment = NSTextAlignmentCenter;
    cell_head1 .font = [UIFont fontWithName:kFont size:14];
    cell_head1 .textColor = [UIColor blackColor];
    cell_head1.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:cell_head1];
    
    
    UIImageView *img_line = [[UIImageView alloc] init];
    img_line.frame = CGRectMake(15,CGRectGetMaxY(cell_head.frame)+5, WIDTH-40, 0.5 );
    [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
    [img_cellBackGnd addSubview:img_line];
    
    
    
    UILabel *delAddress_Lbl = [[UILabel alloc]init];
    delAddress_Lbl.frame = CGRectMake(20,CGRectGetMaxY(img_line.frame),(WIDTH-50)/2.0, 25);
    delAddress_Lbl.text = @"Delivery Address";
    // likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
    delAddress_Lbl.font = [UIFont fontWithName:kFontBold size:13];
//    delAddress_Lbl.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    delAddress_Lbl.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:delAddress_Lbl];
    
    
    UIButton *delibery_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    delibery_Btn.frame = CGRectMake(20, CGRectGetMaxY(delAddress_Lbl.frame), WIDTH-50 , 30);
    [delibery_Btn addTarget:self action:@selector(delibery_Btn_Method:) forControlEvents:UIControlEventTouchUpInside];
    delibery_Btn.tag = 1;
    [delibery_Btn setBackgroundImage:[UIImage imageNamed:@"dietary-table-img@2x"] forState:UIControlStateNormal];
    [img_cellBackGnd addSubview: delibery_Btn];
    
    
    
    UILabel *companyNameLbl = [[UILabel alloc]init];
    companyNameLbl .frame = CGRectMake(5, 0, WIDTH-90 , 30);
    companyNameLbl .backgroundColor = [UIColor clearColor];
    companyNameLbl .text =@"Timothy's Delivery";
    companyNameLbl .textColor = [UIColor blackColor];
    //        nameLbl .textAlignment = NSTextAlignmentCenter;
    companyNameLbl .font = [UIFont fontWithName:kFont size:13];
    companyNameLbl.lineBreakMode = NSLineBreakByWordWrapping;
    companyNameLbl.numberOfLines = 0;
    [delibery_Btn addSubview:companyNameLbl];
    
    
    
    UIButton *apply_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    apply_Btn.frame = CGRectMake(WIDTH-120, CGRectGetMaxY(delibery_Btn.frame)+7, 15 , 15);
    [apply_Btn addTarget:self action:@selector(apply_Btn_Method:) forControlEvents:UIControlEventTouchUpInside];
    apply_Btn.tag = 1;
    [apply_Btn setBackgroundImage:[UIImage imageNamed:@"img-check@2x"] forState:UIControlStateNormal];
    [apply_Btn setBackgroundImage:[UIImage imageNamed:@"img-check-select@2x"] forState:UIControlStateSelected];
    [img_cellBackGnd addSubview: apply_Btn];
    
    
    
    UILabel *apply_Lbl = [[UILabel alloc]init];
    apply_Lbl.frame = CGRectMake(CGRectGetMaxX(apply_Btn.frame)+7, CGRectGetMaxY(delibery_Btn.frame)+7, 80 , 15);
    apply_Lbl.backgroundColor = [UIColor clearColor];
    apply_Lbl.text =@"Apply to All";
    apply_Lbl.textColor = [UIColor blackColor];
    //        apply_Lbl.textAlignment = NSTextAlignmentCenter;
    apply_Lbl.font = [UIFont fontWithName:kFontBold size:11];
    apply_Lbl.lineBreakMode = NSLineBreakByWordWrapping;
    apply_Lbl.numberOfLines = 0;
    [img_cellBackGnd addSubview:apply_Lbl];
    
    
    
    UILabel *servingRequest_Lbl = [[UILabel alloc]init];
    servingRequest_Lbl.frame = CGRectMake(20,CGRectGetMaxY(apply_Lbl.frame)+10,(WIDTH-80), 25);
    servingRequest_Lbl.text = @"Special Requset/Remarks";
    // likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
    servingRequest_Lbl.font = [UIFont fontWithName:kFontBold size:13];
//    servingRequest_Lbl.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    servingRequest_Lbl.backgroundColor = [UIColor clearColor];
    [img_cellBackGnd addSubview:servingRequest_Lbl];
//
    
    UITextView *txtview_adddescription = [[UITextView alloc]init];
    txtview_adddescription.frame = CGRectMake(20,CGRectGetMaxY(servingRequest_Lbl.frame),WIDTH-50,65);
    txtview_adddescription.scrollEnabled = YES;
    txtview_adddescription.userInteractionEnabled = YES;
    txtview_adddescription.font = [UIFont fontWithName:kFont size:12];
    txtview_adddescription.backgroundColor = [UIColor clearColor];
    txtview_adddescription.delegate = self;
    txtview_adddescription.textColor = [UIColor blackColor];
    txtview_adddescription.text =@"LOrem ipsumdotor sit amet.";
//    txtview_adddescription.text = [[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Personal_Information"] valueForKey:@"About_Us"];
    txtview_adddescription.layer.borderWidth = 1.0;
    [img_cellBackGnd addSubview:txtview_adddescription];
    
//
//    UILabel *likes = [[UILabel alloc]init];
//    likes.frame = CGRectMake(CGRectGetMaxX(servingRequest_Lbl.frame),CGRectGetMaxY(img_line.frame),(WIDTH-50)/3.0, 25);
//    likes.text = @"87.4%";
//    // likes.text = [[ary_itemsinformation  objectAtIndex:indexPath.row] valueForKey:@"likes"];
//    likes.font = [UIFont fontWithName:kFont size:10];
//    likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
//    likes.backgroundColor = [UIColor clearColor];
//    [img_cellBackGnd addSubview:likes];
    
    
    
    
    
    
    return cell;
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
