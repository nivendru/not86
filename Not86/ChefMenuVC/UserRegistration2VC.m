//
//  UserRegistration2VC.m
//  Not86
//
//  Created by Admin on 29/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "UserRegistration2VC.h"
#import "AppDelegate.h"
#import "UserLoginVC.h"
#import "AboutUS.h"

//#import "HomeVC.h"
#import "Define.h"
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"



@interface UserRegistration2VC ()<UITextFieldDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIImageView *img_topbar;
    UIImageView *img_BackgroundImg;
    UIScrollView *scrollview4;
    
    UITextField *txt_popular;
    
    UITableView *cuisines_table;
    UITableView *diatary_table;
    UITableView *allergies_table;
    
    NSMutableArray *arrCuisines;
    NSMutableArray *arrrestrictions;
    NSMutableArray *array_food_allergies;
    
    UITableView * img_table_for_popular;
    
    CGFloat	animatedDistance;
    
    NSMutableArray *aryPopularlist;
    NSString*str_cuisenCatId;
    NSMutableArray*ary_countrylist;
    NSMutableArray *Arr_temp;
    NSMutableArray *Arr_tempdiet;
    NSMutableArray *Arr_tempfoodalery;

    UITextField *txt_others;
    AppDelegate *delegate;
    UIView*alertviewBg;
     UITextField*txt_dietryothers;

     UITextField*txt_diewothers;
   }

@end

@implementation UserRegistration2VC


static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    str_cuisenCatId = [NSString new];
    ary_countrylist =[[NSMutableArray alloc]init];
    arrCuisines=[[NSMutableArray alloc]init];
    arrrestrictions=[[NSMutableArray alloc]init];
    array_food_allergies = [[NSMutableArray alloc]init];
    aryPopularlist= [[NSMutableArray alloc]init];
    
    Arr_temp=[[NSMutableArray alloc]init];
    Arr_tempdiet=[[NSMutableArray alloc]init];
    Arr_tempfoodalery=[[NSMutableArray alloc]init];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    
    [self IntegrateHeaderDesign];
    [self IntegrateBodyDesign];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
    [self popularList];
    [self countryList];
    
}
-(void)IntegrateHeaderDesign{
    
    
    img_topbar=[[UIImageView alloc]init];
    UIButton *btn_back = [[UIButton alloc] init];
    UILabel  * lbl_heading = [[UILabel alloc]init];
    UIImageView *img_logo=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus){
        
        img_topbar.frame=CGRectMake(0, 0,WIDTH,45);
        btn_back.frame = CGRectMake(10, 15, 15,15);
        lbl_heading.frame = CGRectMake(CGRectGetMaxX(btn_back.frame)+20,0, 220,50);
        img_logo.frame = CGRectMake(WIDTH-40, 10, 30, 30);
        scrollview4=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 50, WIDTH, HEIGHT+200)];
    }
    
    
    if (IS_IPHONE_5)
    {
        img_topbar.frame=CGRectMake(0, 0,WIDTH,45);
        btn_back.frame = CGRectMake(10, 13, 15,15);
        lbl_heading.frame =  CGRectMake(CGRectGetMaxX(btn_back.frame)+30,0, 150, 45);
        img_logo.frame = CGRectMake(WIDTH-40, 9, 30, 30);
        scrollview4=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 50, WIDTH, HEIGHT-80)];
    }
    else if (IS_IPHONE_6)
    {
        
        img_topbar.frame=CGRectMake(0, 0,WIDTH,45);
        btn_back.frame = CGRectMake(10, 13, 15,15);
        lbl_heading.frame =  CGRectMake(CGRectGetMaxX(btn_back.frame)+30,0, 150, 45);
        img_logo.frame = CGRectMake(WIDTH-40, 9, 30, 30);
        scrollview4=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 50, WIDTH, HEIGHT-80)];
    }
    else
    {
        img_topbar.frame=CGRectMake(0, 0,WIDTH,45);
        btn_back.frame = CGRectMake(10, 13, 15,15);
        lbl_heading.frame =  CGRectMake(CGRectGetMaxX(btn_back.frame)+30,0, 150, 45);
        img_logo.frame = CGRectMake(WIDTH-40, 9, 30, 30);
        scrollview4=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 50, WIDTH, HEIGHT-80)];
    }
    [img_topbar setUserInteractionEnabled:YES];
    img_topbar.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:img_topbar];
    
    btn_back.backgroundColor = [UIColor clearColor];
    UIImage *image=[UIImage imageNamed:@"arrow@2x.png"];
    [btn_back setBackgroundImage:image forState:UIControlStateNormal];
    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_topbar addSubview:btn_back];
    
    lbl_heading.text = @"User Sign Up";
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:kFont size:20];
    [img_topbar addSubview:lbl_heading];
    
    
    
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img_user_icon@2x.png"];
    [img_topbar addSubview:img_logo];
    
    scrollview4=[[UIScrollView alloc]init];
    [scrollview4 setShowsVerticalScrollIndicator:NO];
    scrollview4.delegate = self;
    scrollview4.scrollEnabled = YES;
    scrollview4.showsVerticalScrollIndicator = NO;
    [scrollview4 setUserInteractionEnabled:YES];
    scrollview4.backgroundColor =  [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [self.view addSubview:scrollview4];
    
    
    
    
}
-(void)Back_btnClick{
//    [self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];

}

-(void)IntegrateBodyDesign
{
    
    
    UILabel  *lbl_enjoycooking = [[UILabel alloc]init];
    UIImageView *line_img=[[UIImageView alloc]init];
    UIButton *next = [[UIButton alloc] init];
    txt_popular = [[UITextField alloc] init];
    UILabel  *lbl_cuisines = [[UILabel alloc]init];
    UIImageView *image=[[UIImageView alloc]init];
    cuisines_table= [[UITableView alloc] init ];
    
    UILabel  *lbl_Restrictions = [[UILabel alloc]init];
    diatary_table= [[UITableView alloc] init ];
    UIImageView *lineimage=[[UIImageView alloc]init];
    UILabel  * page4 = [[UILabel alloc]init];
    UIButton *Nextview = [[UIButton alloc] init];
    
    
    UIImageView *img_bg1 = [[UIImageView alloc]init];
    img_bg1.frame =  CGRectMake(5,5, WIDTH-10, 390);
    [img_bg1 setUserInteractionEnabled:YES];
    [img_bg1 setImage:[UIImage imageNamed:@"small-line@2x.png"]];
    [img_bg1 setUserInteractionEnabled:YES];
    [scrollview4 addSubview:img_bg1];
    

    
    
    lbl_enjoycooking.text = @"Favourite Cuisines(choose up to 5)";
    lbl_enjoycooking.backgroundColor=[UIColor clearColor];
    lbl_enjoycooking.textColor=[UIColor blackColor];
    lbl_enjoycooking.numberOfLines = 0;
    lbl_enjoycooking.font = [UIFont fontWithName:kFont size:13];
    [img_bg1 addSubview:lbl_enjoycooking];
    
    txt_popular.borderStyle = UITextBorderStyleNone;
    txt_popular.textColor = [UIColor blackColor];
    txt_popular.font = [UIFont fontWithName:kFont size:15];
    txt_popular.placeholder = @"Popular";
    [txt_popular setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_popular setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    txt_popular.leftViewMode = UITextFieldViewModeAlways;
    txt_popular.userInteractionEnabled=YES;
    txt_popular.textAlignment = NSTextAlignmentLeft;
    txt_popular.backgroundColor = [UIColor clearColor];
    txt_popular.keyboardType = UIKeyboardTypeAlphabet;
    
    txt_popular.delegate = self;
    [img_bg1 addSubview:txt_popular];
    
    [line_img setUserInteractionEnabled:YES];
    line_img.backgroundColor=[UIColor clearColor];
    line_img.image=[UIImage imageNamed:@"line1.png"];
    [img_bg1 addSubview:line_img];
    
    next.backgroundColor = [UIColor clearColor];
    [next setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [next setUserInteractionEnabled:YES];
    [next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [next addTarget:self action:@selector(drop_btnClick:) forControlEvents:UIControlEventTouchUpInside ];
    [img_bg1 addSubview:next];
    
    UIButton *btn_on_popular = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_popular .backgroundColor = [UIColor clearColor];
    [btn_on_popular  addTarget:self action:@selector(btn_action_on_popular:) forControlEvents:UIControlEventTouchUpInside];
   // [btn_on_popular setImage:[UIImage imageNamed:@"search-icon@2x.png"] forState:UIControlStateNormal];
    [img_bg1   addSubview:btn_on_popular ];

    
//#pragma mark Tableview
    
 
    
    lbl_cuisines.text = @"Cuisines";
    lbl_cuisines.backgroundColor=[UIColor clearColor];
    lbl_cuisines.textColor=[UIColor blackColor];
    lbl_cuisines.numberOfLines = 0;
    lbl_cuisines.font = [UIFont fontWithName:kFont size:16];
    [img_bg1 addSubview:lbl_cuisines];
    
    [image setUserInteractionEnabled:YES];
    image.backgroundColor=[UIColor whiteColor];
    image.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    image.layer.borderWidth=1.0f;
    image.clipsToBounds=YES;
   // image.image=[UIImage imageNamed:@"line1.png"];
    [img_bg1 addSubview:image];
    
    
    
#pragma mark Tableview
    
    [cuisines_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    cuisines_table.delegate = self;
    cuisines_table.dataSource = self;
    cuisines_table.showsVerticalScrollIndicator = NO;
    cuisines_table.backgroundColor = [UIColor clearColor];
    [image addSubview:cuisines_table];
   
    
    
    UIImageView *img_bg2 = [[UIImageView alloc]init];
    img_bg2.frame =  CGRectMake(5,CGRectGetMaxY(img_bg1.frame)+10, WIDTH-10, 390);
    [img_bg2 setUserInteractionEnabled:YES];
    [img_bg2 setImage:[UIImage imageNamed:@"small-line@2x.png"]];
    [img_bg2 setUserInteractionEnabled:YES];
    [scrollview4 addSubview:img_bg2];

    lbl_Restrictions.text = @"Dietary restrictions";
    lbl_Restrictions.backgroundColor=[UIColor clearColor];
    lbl_Restrictions.textColor=[UIColor blackColor];
    //lbl_Restrictions.numberOfLines = 0;
    lbl_Restrictions.font = [UIFont fontWithName:kFontBold size:12];
    [img_bg2 addSubview:lbl_Restrictions];
    
    [diatary_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    diatary_table.delegate = self;
    diatary_table.dataSource = self;
    diatary_table.showsVerticalScrollIndicator = NO;
    diatary_table.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    diatary_table.layer.borderWidth=1.0f;
    diatary_table.clipsToBounds = YES;
    diatary_table.backgroundColor = [UIColor whiteColor];
    [img_bg2 addSubview:diatary_table];
    
   
    
    txt_diewothers = [[UITextField alloc]init];
    txt_diewothers.borderStyle = UITextBorderStyleNone;
    txt_diewothers.textColor = [UIColor blackColor];
    txt_diewothers.font = [UIFont fontWithName:kFont size:15];
    txt_diewothers.placeholder = @"others Specify";
    [txt_diewothers setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_diewothers setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    txt_diewothers.leftViewMode = UITextFieldViewModeAlways;
    txt_diewothers.userInteractionEnabled=YES;
    txt_diewothers.textAlignment = NSTextAlignmentLeft;
    txt_diewothers.backgroundColor = [UIColor clearColor];
    txt_diewothers.keyboardType = UIKeyboardTypeAlphabet;
    txt_diewothers.delegate = self;
    [img_bg2 addSubview:txt_diewothers];
    

    
    UIImageView *line=[[UIImageView alloc]init];
    [line setUserInteractionEnabled:YES];
    line.backgroundColor=[UIColor clearColor];
    line.image=[UIImage imageNamed:@"line1@2x.png"];
    [img_bg2 addSubview:line];
    
    UIImageView *img_bg3 = [[UIImageView alloc]init];
    img_bg3.frame =  CGRectMake(5,CGRectGetMaxY(img_bg1.frame)+10, WIDTH-10, 390);
    [img_bg3 setUserInteractionEnabled:YES];
    [img_bg3 setImage:[UIImage imageNamed:@"small-line@2x.png"]];
    [img_bg3 setUserInteractionEnabled:YES];
    [scrollview4 addSubview:img_bg3];
    

    img_table_for_popular = [[UITableView alloc] init];
    [img_table_for_popular setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table_for_popular.delegate = self;
    img_table_for_popular.dataSource = self;
    img_table_for_popular.showsVerticalScrollIndicator = NO;
    img_table_for_popular.backgroundColor = [UIColor clearColor];
    img_table_for_popular.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    img_table_for_popular.layer.borderWidth = 1.0f;
    img_table_for_popular.clipsToBounds = YES;
    [img_bg1  addSubview: img_table_for_popular];
    img_table_for_popular.hidden =YES;
    
   
    
    UILabel *lbl_food_allergies = [[UILabel alloc]init];
    lbl_food_allergies.text = @"Food Allergies";
    lbl_food_allergies.backgroundColor=[UIColor clearColor];
    lbl_food_allergies.textColor=[UIColor blackColor];
    lbl_food_allergies.numberOfLines = 0;
    lbl_food_allergies.font = [UIFont fontWithName:kFontBold size:12];
    [img_bg3 addSubview: lbl_food_allergies];
    
    allergies_table = [[UITableView alloc]init];
    [allergies_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    allergies_table.delegate = self;
    allergies_table.dataSource = self;
    allergies_table.showsVerticalScrollIndicator = NO;
    allergies_table.backgroundColor = [UIColor clearColor];
    allergies_table.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    allergies_table.layer.borderWidth = 1.0f;
    allergies_table.clipsToBounds = YES;
    [img_bg3 addSubview:allergies_table];
    
   
    txt_dietryothers = [[UITextField alloc]init];
    txt_dietryothers.borderStyle = UITextBorderStyleNone;
    txt_dietryothers.textColor = [UIColor blackColor];
    txt_dietryothers.font = [UIFont fontWithName:kFont size:15];
    txt_dietryothers.placeholder = @"others Specify";
    [txt_dietryothers setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_dietryothers setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    txt_dietryothers.leftViewMode = UITextFieldViewModeAlways;
    txt_dietryothers.userInteractionEnabled=YES;
    txt_dietryothers.textAlignment = NSTextAlignmentLeft;
    txt_dietryothers.backgroundColor = [UIColor clearColor];
    txt_dietryothers.keyboardType = UIKeyboardTypeAlphabet;
    txt_dietryothers.delegate = self;
    [img_bg3 addSubview:txt_dietryothers];

    
    UIImageView *line2 = [[UIImageView alloc]init];
    [line2 setUserInteractionEnabled:YES];
    line2.backgroundColor=[UIColor clearColor];
    line2.image=[UIImage imageNamed:@"line1@2x.png"];
    [img_bg3 addSubview:line2];
    
    UIImageView *img_bg4 = [[UIImageView alloc]init];
    img_bg4.frame =  CGRectMake(-13, 660, WIDTH+35, 80);
    [img_bg4 setUserInteractionEnabled:YES];
    img_bg4.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [self.view addSubview:img_bg4];

    
    UIImageView *img_terms_and_conditions = [[UIImageView alloc]init];
    [img_terms_and_conditions setUserInteractionEnabled:YES];
    img_terms_and_conditions.backgroundColor=[UIColor clearColor];
    img_terms_and_conditions.image=[UIImage imageNamed:@"text-img@2x.png"];
    [img_bg4 addSubview:img_terms_and_conditions];
    
    
    UILabel *terms_and_conditions = [[UILabel alloc]init];
    terms_and_conditions.frame = CGRectMake(10 , 20, 250, 150);
    terms_and_conditions.text = @"   By creating a not86 account, you \n        have read and agree to the \n                                          governing \n                  the use of not86.";
    terms_and_conditions.backgroundColor=[UIColor clearColor];
    terms_and_conditions.textColor=[UIColor blackColor];
    terms_and_conditions.numberOfLines = 4;
    terms_and_conditions.font = [UIFont fontWithName:kFont size:17];
    [img_bg4 addSubview: terms_and_conditions];
    
    
    UILabel *terms_and_conditions_text = [[UILabel alloc]init];
    terms_and_conditions_text.frame = CGRectMake(10 , 20, 250, 150);
    terms_and_conditions_text.text = @"Terms and Conditions";
    terms_and_conditions_text.backgroundColor=[UIColor clearColor];
    terms_and_conditions_text.textColor = [UIColor colorWithRed:34/255.0f green:59/255.0f blue:94/255.0f alpha:1];
    terms_and_conditions_text.numberOfLines = 1;
    terms_and_conditions_text.font = [UIFont fontWithName:kFont size:16];
    [img_bg4 addSubview: terms_and_conditions_text];

    
    UIButton *btn_on_terms_and_conditions =[UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_terms_and_conditions.frame=CGRectMake(5,CGRectGetMaxY(terms_and_conditions.frame)+33, 158, 20);
    btn_on_terms_and_conditions.backgroundColor = [UIColor clearColor];
    [btn_on_terms_and_conditions addTarget:self action:@selector(btn_on_terms_and_condition:) forControlEvents:UIControlEventTouchUpInside];
    btn_on_terms_and_conditions.userInteractionEnabled = YES;
    // [btn_on_terms_and_conditions setImage:[UIImage imageNamed:@"icon-dropdown@2x.png"] forState:UIControlStateNormal];
    [terms_and_conditions_text  addSubview:btn_on_terms_and_conditions];
    
    
    
    
    page4.text = @"(Page2/2)";
    page4.backgroundColor=[UIColor clearColor];
    page4.textColor=[UIColor lightGrayColor];
    page4.numberOfLines = 0;
    page4.font = [UIFont fontWithName:kFont size:13];
    [img_bg4 addSubview:page4];
    
    Nextview.backgroundColor = [UIColor clearColor];
    //    [Nextview setTitle:@"Next" forState:UIControlStateNormal];
    [Nextview setImage:[UIImage imageNamed:@"sign-up@2x.png"] forState:UIControlStateNormal];
    [Nextview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [Nextview addTarget:self action:@selector(sign_up_btnClick:) forControlEvents:UIControlEventTouchUpInside ];
    Nextview.layer.cornerRadius=4.0f;
    [img_bg4 addSubview:Nextview];
    
    if (IS_IPHONE_6Plus){
        scrollview4.frame = CGRectMake(0, 45, WIDTH, HEIGHT-230);
        
        img_bg1.frame =  CGRectMake(5,5, WIDTH-10, 390);
        lbl_enjoycooking.frame = CGRectMake(20,0, 300,50);
        txt_popular.frame=CGRectMake(20,CGRectGetMaxY(lbl_enjoycooking.frame)+1, 350, 38);
        btn_on_popular .frame = CGRectMake(0, CGRectGetMaxY(lbl_enjoycooking.frame)+1, 400, 30);

        line_img.frame=CGRectMake(20, CGRectGetMaxY(txt_popular.frame)-10, self.view.frame.size.width-40, 0.5);
        img_table_for_popular.frame  = CGRectMake(20,CGRectGetMaxY(line_img.frame)+3,370,150);

        next.frame = CGRectMake(self.view.frame.size.width-50, CGRectGetMaxY(lbl_enjoycooking.frame)+15, 20, 10);
        lbl_cuisines.frame = CGRectMake(self.view.frame.size.width/2-50,CGRectGetMaxY(txt_popular.frame),300,30);
        image.frame=CGRectMake(20, CGRectGetMaxY(lbl_cuisines.frame)+3, self.view.frame.size.width-40, 250);
        cuisines_table.frame  = CGRectMake(0,0,400,250);
        
         img_bg2.frame =  CGRectMake(5,CGRectGetMaxY(img_bg1.frame)+10, WIDTH-10, 310);
         lbl_Restrictions.frame = CGRectMake(20,-10, 300,50);
         diatary_table.frame  = CGRectMake(20, CGRectGetMaxY(lbl_Restrictions.frame)-10,370,250);
        txt_diewothers.frame = CGRectMake(50, CGRectGetMaxY(diatary_table.frame),300,20);
         lineimage.frame=CGRectMake(50, CGRectGetMaxY(txt_diewothers.frame)+2, self.view.frame.size.width-70, 0.5);
         line.frame=CGRectMake(50, CGRectGetMaxY(txt_diewothers .frame)+2, 300, 0.5);

         img_bg3.frame =  CGRectMake(5,CGRectGetMaxY(img_bg2.frame)+10, WIDTH-10, 310);
         lbl_food_allergies.frame = CGRectMake(25,-10, 300,50);
          allergies_table.frame  = CGRectMake(20, CGRectGetMaxY(lbl_food_allergies.frame)-10,370,250);
        txt_dietryothers.frame = CGRectMake(50, CGRectGetMaxY(allergies_table.frame),300,20);
        line2.frame=CGRectMake(50, CGRectGetMaxY( txt_dietryothers .frame)+2, 300, 0.5);
        
        img_bg4.frame =  CGRectMake(-13, CGRectGetMaxY(scrollview4.frame), WIDTH+35,215);

        terms_and_conditions.frame=CGRectMake(40, 05, 350, 100);
        terms_and_conditions_text.frame = CGRectMake(65,58,185, 20);
        
        terms_and_conditions.font = [UIFont fontWithName:kFont size:18];
        terms_and_conditions_text.font = [UIFont fontWithName:kFont size:17];
         btn_on_terms_and_conditions.frame=CGRectMake(0, 0, 185, 25);
       page4.frame = CGRectMake(self.view.frame.size.width/2-20,CGRectGetMaxY( terms_and_conditions.frame)-5, 100,30);
        Nextview.frame = CGRectMake(13, CGRectGetMaxY(page4.frame), WIDTH,50);
        
    }
    else if (IS_IPHONE_6){
        scrollview4.frame = CGRectMake(0, 46, WIDTH, HEIGHT-230);
        
        img_bg1.frame =  CGRectMake(5,5, WIDTH-10, 390);
        img_bg2.frame =  CGRectMake(5,CGRectGetMaxY(img_bg1.frame)+10, WIDTH-10, 390);

        
        lbl_enjoycooking.frame = CGRectMake(20,0, 300,50);
        txt_popular.frame=CGRectMake(20,CGRectGetMaxY(lbl_enjoycooking.frame)+1, 350, 38);
        btn_on_popular .frame = CGRectMake(0, CGRectGetMaxY(lbl_enjoycooking.frame)+1, 390, 50);

        line_img.frame=CGRectMake(20, CGRectGetMaxY(txt_popular.frame)+1, self.view.frame.size.width-40, 0.5);
        img_table_for_popular.frame  = CGRectMake(20,CGRectGetMaxY(line_img.frame),330,150);

        next.frame = CGRectMake(self.view.frame.size.width-50, CGRectGetMaxY(lbl_enjoycooking.frame)+15, 20, 10);
        lbl_cuisines.frame = CGRectMake(self.view.frame.size.width/2-50,CGRectGetMaxY(txt_popular.frame)+10, 300,30);
        image.frame=CGRectMake(20, CGRectGetMaxY(lbl_cuisines.frame)+3, self.view.frame.size.width-40, 250);
        cuisines_table.frame  = CGRectMake(0,0,400,250);
        
         img_bg2.frame =  CGRectMake(5,CGRectGetMaxY(img_bg1.frame)+10, WIDTH-10, 310);
         lbl_Restrictions.frame = CGRectMake(20,-5, 300,50);
         diatary_table.frame  = CGRectMake(20, CGRectGetMaxY(lbl_Restrictions.frame)-10,300,250);
        txt_diewothers.frame = CGRectMake(50, CGRectGetMaxY(diatary_table.frame),300,20);
        lineimage.frame=CGRectMake(50, CGRectGetMaxY(txt_diewothers.frame)+2, self.view.frame.size.width-70, 0.5);
        line.frame=CGRectMake(50, CGRectGetMaxY(txt_diewothers .frame)+2, 300, 0.5);
      
         img_bg3.frame =  CGRectMake(5,CGRectGetMaxY(img_bg2.frame)+10, WIDTH-10, 310);
          lbl_food_allergies.frame = CGRectMake(25,-5, 300,50);
          allergies_table.frame  = CGRectMake(20, CGRectGetMaxY(lbl_food_allergies.frame)-10,300,250);
        txt_dietryothers.frame = CGRectMake(50, CGRectGetMaxY(allergies_table.frame),300,20);

          line2.frame=CGRectMake(50, CGRectGetMaxY( txt_dietryothers .frame)+2, 290, 0.5);
        
        img_bg4.frame =  CGRectMake(-13, CGRectGetMaxY(scrollview4.frame), WIDTH+35,210);
        
        terms_and_conditions.frame=CGRectMake(40, 05, 310, 100);
        terms_and_conditions_text.frame = CGRectMake(65,57,190, 20);
        
        terms_and_conditions.font = [UIFont fontWithName:kFont size:18];
        terms_and_conditions_text.font = [UIFont fontWithName:kFont size:17];

        
        
        btn_on_terms_and_conditions.frame=CGRectMake(0,0, 175, 30);
        
        page4.frame = CGRectMake(self.view.frame.size.width/2-30,CGRectGetMaxY( terms_and_conditions.frame)-5, 100,30);
        Nextview.frame = CGRectMake(30, CGRectGetMaxY(page4.frame), WIDTH-40,45);
        
    }
    else
    {
        scrollview4.frame = CGRectMake(0, 45, WIDTH, HEIGHT-230);
        
        img_bg1.frame =  CGRectMake(5,5, WIDTH-10, 360);
        
        lbl_enjoycooking.frame = CGRectMake(20,-10, 300,50);
        txt_popular.frame=CGRectMake(20,CGRectGetMaxY(lbl_enjoycooking.frame)-10, 300, 38);
        btn_on_popular .frame = CGRectMake(0, CGRectGetMaxY(lbl_enjoycooking.frame)+1, 300, 50);

        line_img.frame=CGRectMake(20, CGRectGetMaxY(txt_popular.frame)-10, self.view.frame.size.width-45, 0.5);
        img_table_for_popular.frame  = CGRectMake(20,CGRectGetMaxY(line_img.frame),330,150);

        next.frame = CGRectMake(self.view.frame.size.width-50, CGRectGetMaxY(lbl_enjoycooking.frame)+6, 20, 10);
        lbl_cuisines.frame = CGRectMake(self.view.frame.size.width/2-50,CGRectGetMaxY(txt_popular.frame),300,30);
        image.frame=CGRectMake(20, CGRectGetMaxY(lbl_cuisines.frame)+3, self.view.frame.size.width-40, 250);
        cuisines_table.frame  = CGRectMake(0,0,400,250);
        
         img_bg2.frame =  CGRectMake(5,CGRectGetMaxY(img_bg1.frame)+10, WIDTH-10, 320);
         lbl_Restrictions.frame = CGRectMake(20,0, 300,50);
         diatary_table.frame  = CGRectMake(20, CGRectGetMaxY(lbl_Restrictions.frame)-5,290,250);
        txt_diewothers.frame = CGRectMake(50, CGRectGetMaxY(diatary_table.frame),300,20);
        lineimage.frame=CGRectMake(50, CGRectGetMaxY(txt_diewothers.frame)+2, self.view.frame.size.width-70, 0.5);
        line.frame=CGRectMake(50, CGRectGetMaxY(txt_diewothers .frame)+2, 300, 0.5);

       
          img_bg3.frame =  CGRectMake(5,CGRectGetMaxY(img_bg2.frame)+10, WIDTH-10, 315);
          lbl_food_allergies.frame = CGRectMake(25,0, 300,50);
          allergies_table.frame  = CGRectMake(20, CGRectGetMaxY(lbl_food_allergies.frame)-5,290,250);
        txt_dietryothers.frame = CGRectMake(50, CGRectGetMaxY(allergies_table.frame),300,20);

          line2.frame=CGRectMake(50, CGRectGetMaxY( txt_dietryothers .frame)+2, 240, 0.5);
        
          img_bg4.frame =  CGRectMake(-13, CGRectGetMaxY(scrollview4.frame), WIDTH+35,210);
        terms_and_conditions.frame=CGRectMake(40, 05, 270, 100);
        terms_and_conditions_text.frame = CGRectMake(65,57,150, 20);
        terms_and_conditions.font = [UIFont fontWithName:kFont size:15];
        terms_and_conditions_text.font = [UIFont fontWithName:kFont size:14];


        
                 btn_on_terms_and_conditions.frame=CGRectMake(0,0, 150, 30);
    
         page4.frame = CGRectMake(self.view.frame.size.width/2-25,CGRectGetMaxY( terms_and_conditions.frame)-5, 100,30);
         Nextview.frame = CGRectMake(33, CGRectGetMaxY(page4.frame), WIDTH-40,45);
        
    }
    [scrollview4 setContentSize:CGSizeMake(0,1100)];
    
}

//-(void)Next_btnClick
//{
//    ChefSignup5 *chefsignup5=[[ChefSignup5 alloc]init];
//    [self.navigationController pushViewController:chefsignup5 animated:YES];
//}
-(void)drop_btnClick:(id)sender
{
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView== cuisines_table)
    {
        return [arrCuisines count];
        
    }
    else if (tableView == diatary_table )
    {
        return [ary_countrylist count];
        
    }
    else if(tableView == allergies_table)
    {
        return [array_food_allergies count];
    }
    else
    {
         return [aryPopularlist count];
    }
    return 0;
    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
 
    cell.backgroundColor = [UIColor whiteColor];

    if (tableView == cuisines_table)
    {
//        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
//        if (IS_IPHONE_6Plus)
//        {
//            img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH+5, 170);
//            
//        }
//        else if (IS_IPHONE_6)
//        {
//            img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-5,250);
//            
//        }
//        else
//        {    img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-10,170);
//            
//        }
//        //[img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
//        img_cellBackGnd.backgroundColor = [UIColor redColor];
//        [img_cellBackGnd setUserInteractionEnabled:YES];
//        [cell.contentView addSubview:img_cellBackGnd];
//        

        
        UIButton *btn_TableCell_CheckBox1 = [[UIButton alloc]initWithFrame:CGRectMake(2,2,15,15)];
        btn_TableCell_CheckBox1.backgroundColor=[UIColor clearColor];
        [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img.checkcorrect@2x.png"] forState:UIControlStateSelected];
        [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
        btn_TableCell_CheckBox1.tag = indexPath.row;
        [cell.contentView addSubview:btn_TableCell_CheckBox1];
        if([Arr_temp containsObject:[arrCuisines objectAtIndex:indexPath.row]])
        {
            [btn_TableCell_CheckBox1 setSelected:YES];
        }
        else
        {
            [btn_TableCell_CheckBox1 setSelected:NO];
        }
      [btn_TableCell_CheckBox1 addTarget:self action:@selector(click_selectObjectAt:) forControlEvents:UIControlEventTouchUpInside];
        
        
        UILabel  *lbl_Restrictions = [[UILabel alloc]initWithFrame:CGRectMake(23,-3,150,25)];
       lbl_Restrictions.text = [[arrCuisines objectAtIndex:indexPath.row] valueForKey:@"CuisineName"];
        lbl_Restrictions.backgroundColor=[UIColor clearColor];
        lbl_Restrictions.textColor=[UIColor blackColor];
        lbl_Restrictions.font = [UIFont fontWithName:kFont size:12];
        [cell.contentView addSubview:lbl_Restrictions];

        
    }
    else if (tableView == diatary_table)
    {
        UIButton *btn_TableCell_CheckBox2 = [[UIButton alloc]initWithFrame:CGRectMake(2,2,15,15)];
        btn_TableCell_CheckBox2.backgroundColor=[UIColor clearColor];
        [btn_TableCell_CheckBox2 setImage:[UIImage imageNamed:@"img.checkcorrect@2x.png"] forState:UIControlStateSelected];
        [btn_TableCell_CheckBox2 setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
        btn_TableCell_CheckBox2.tag = indexPath.row;
        [cell.contentView addSubview:btn_TableCell_CheckBox2];
        if([Arr_tempdiet containsObject:[ary_countrylist objectAtIndex:indexPath.row]])
        {
            [btn_TableCell_CheckBox2 setSelected:YES];
        }
        else{
            [btn_TableCell_CheckBox2 setSelected:NO];
        }
       [btn_TableCell_CheckBox2 addTarget:self action:@selector(click_selectObjectAt1:) forControlEvents:UIControlEventTouchUpInside];
        UILabel  *lbl_Restrictions = [[UILabel alloc]initWithFrame:CGRectMake(23,-3,150,25)];
        lbl_Restrictions.text = [[ary_countrylist objectAtIndex:indexPath.row] valueForKey:@"DietaryName"];
        lbl_Restrictions.backgroundColor=[UIColor clearColor];
        lbl_Restrictions.textColor=[UIColor blackColor];
        lbl_Restrictions.font = [UIFont fontWithName:kFont size:12];
        [cell.contentView addSubview:lbl_Restrictions];
        
    }
    else if(tableView == allergies_table)
    {
        
        UIButton *btn_TableCell_CheckBox2 = [[UIButton alloc]initWithFrame:CGRectMake(2,2,15,15)];
        btn_TableCell_CheckBox2.backgroundColor=[UIColor clearColor];
        [btn_TableCell_CheckBox2 setImage:[UIImage imageNamed:@"img.checkcorrect@2x.png"] forState:UIControlStateSelected];
        [btn_TableCell_CheckBox2 setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
        btn_TableCell_CheckBox2.tag = indexPath.row;
        [cell.contentView addSubview:btn_TableCell_CheckBox2];
        
        if([Arr_tempfoodalery containsObject:[array_food_allergies objectAtIndex:indexPath.row]])
        {
            [btn_TableCell_CheckBox2 setSelected:YES];
        }
        else{
            [btn_TableCell_CheckBox2 setSelected:NO];
        }
        
       [btn_TableCell_CheckBox2 addTarget:self action:@selector(click_selectObjectAt2:) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel  *lbl_Restrictions = [[UILabel alloc]initWithFrame:CGRectMake(23,-3,150,25)];
        lbl_Restrictions.text = [[array_food_allergies objectAtIndex:indexPath.row] valueForKey:@"DietaryName"];
        lbl_Restrictions.backgroundColor=[UIColor clearColor];
        lbl_Restrictions.textColor=[UIColor blackColor];
        lbl_Restrictions.font = [UIFont fontWithName:kFont size:12];
        [cell.contentView addSubview:lbl_Restrictions];
    }
    else
    {
        UILabel  *lbl_Restrictions = [[UILabel alloc]initWithFrame:CGRectMake(14,0,146,25)];
        lbl_Restrictions.text=[[aryPopularlist objectAtIndex:indexPath.row] valueForKey:@"CuisineCatName"];
        lbl_Restrictions.backgroundColor=[UIColor clearColor];
        lbl_Restrictions.textColor=[UIColor blackColor];
        lbl_Restrictions.font = [UIFont fontWithName:kFont size:12];
        [cell.contentView addSubview:lbl_Restrictions];
    }
//
//   
   // array_food_allergies
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView== cuisines_table)
    {
        return 25;
        
    }
    else if (tableView == diatary_table)
    {
        return 25;
        
    }
    else if(tableView == allergies_table)
    {
        return 25;
    }
    else
    {
        return  25;
    }
    return 0;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView== cuisines_table)
    {
       
        
    }
    else if (tableView == diatary_table )
    {
        
    }
    else if(tableView == allergies_table)
    {
        
    }
    else
    {
        txt_popular.text=[[aryPopularlist objectAtIndex:indexPath.row] valueForKey:@"CuisineCatName"];
        str_cuisenCatId =[[aryPopularlist objectAtIndex:indexPath.row] valueForKey:@"CuisineCatID"];
        [self cuisineList];
        [img_table_for_popular setHidden:YES];
    }
   
    
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}
-(void)btn_action_on_popular:(UIButton *)sender
{
    NSLog(@"btn_action_on_popular");
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        img_table_for_popular.hidden=NO;
    }
    else
    {
        [sender setSelected:NO];
        img_table_for_popular.hidden=YES;
    }

    //img_table_for_popular.hidden =NO;
    

}
-(void)btn_on_terms_and_condition:(UIButton *)sender
{
    NSLog(@"btn_on_terms_and_conditions:");
    AboutUS*vc = [AboutUS new];
    vc.str_Title = @"Terms & Conditions";
    vc.str_URL = @"ws/pages-contents.php?nid=67";
    [self presentViewController:vc animated:NO completion:nil];
    
}




-(void)sign_up_btnClick:(UIButton *)sender
{
    NSLog(@"btn_back_click:");
    
    UserLoginVC *vc = [[UserLoginVC alloc]init];
    [self presentViewController:vc animated:NO completion:nil];
    
      if (Arr_temp.count ==0)
        
    {
        [self popup_Alertview:@"Please enter cuisine id"];
    }
    
//    [self.navigationController pushViewController:vc animated:NO];
//    [self.navigationController pushViewController:vc animated:NO];
    
    
    
}

-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFontBold size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFontBold size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}


#pragma rerutrn event code

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

//-(BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    [self.view endEditing:YES];
//    return [textField resignFirstResponder];
//
//}
//-(void)drop_btnClick1:(UIButton *)sender
//{
//    NSLog(@"drop_btnClick1:");
//
//}
-(void) click_selectObjectAt:(UIButton *) sender
{
    
    if([Arr_temp containsObject:[arrCuisines objectAtIndex:sender.tag]])
    {
        [Arr_temp removeObject:[arrCuisines objectAtIndex:sender.tag]];
    }
    else
    {
        if (Arr_temp.count >4)
        {
            [self popup_Alertview:@"Select upto five cuisines"];
            
        }
        else{
            [Arr_temp addObject:[arrCuisines objectAtIndex:sender.tag]];
            
        }
    }
    
    [cuisines_table reloadData];
    
}


//-(void) click_selectObjectAt:(UIButton *) sender
//{
//    
//    
//    if (Arr_temp.count >4)
//    {
//        [self popup_Alertview:@"Select upto five cuisines"];
//        
//    }
//    else
//    {
//        if([Arr_temp containsObject:[arrCuisines objectAtIndex:sender.tag]])
//        {
//            [Arr_temp removeObject:[arrCuisines objectAtIndex:sender.tag]];
//        }
//        else
//        {
//            [Arr_temp addObject:[arrCuisines objectAtIndex:sender.tag]];
//        }
//        
//        [cuisines_table reloadData];
//        
//    }
//    
//}
-(void) click_selectObjectAt1:(UIButton *) sender
{
    if ([[[ary_countrylist objectAtIndex:sender.tag] valueForKey:@"DietaryName"]isEqualToString:@"None"])
    {
        [Arr_tempdiet removeAllObjects];
        [diatary_table reloadData];
        
    }
    else{
        if([Arr_tempdiet containsObject:[ary_countrylist objectAtIndex:sender.tag]])
        {
            [Arr_tempdiet removeObject:[ary_countrylist objectAtIndex:sender.tag]];
        }
        else
        {
            [Arr_tempdiet addObject:[ary_countrylist objectAtIndex:sender.tag]];
            
            if ([[[ary_countrylist objectAtIndex:sender.tag] valueForKey:@"DietaryName"]isEqualToString:@"Others"])
            {
                [txt_diewothers becomeFirstResponder];
                
                
            }
            
        }
         [diatary_table reloadData];
        
        
        
       
        
    }
    
    
}

-(void) click_selectObjectAt2:(UIButton *) sender
{
    
        if([Arr_tempfoodalery containsObject:[array_food_allergies objectAtIndex:sender.tag]])
        {
            [Arr_tempfoodalery removeObject:[array_food_allergies objectAtIndex:sender.tag]];
        }
        else
        {
            [Arr_tempfoodalery addObject:[array_food_allergies objectAtIndex:sender.tag]];
            
            if ([[[array_food_allergies objectAtIndex:sender.tag] valueForKey:@"DietaryName"]isEqualToString:@"Others (please specify)"])
            {
                [txt_dietryothers becomeFirstResponder];
                

            }
            
        }
        
        [allergies_table reloadData];
  
    
}


#pragma mark - method Sign Up
-(void)SignUpOne
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    
    NSDictionary *params =@{
                            
                            @"uid"                       : [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"registration_page"         : @"2",
                            @"role_type"           : @"user_profile",

                            
                            
                            };
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    
    //===============================SIMPLE REQUEST
    
        NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                                path:@"add-user-profile.json"
                                                          parameters:params];
    
        NSLog(@"request =%@",request);
    
    
    
   
    //====================================================RESPONSE
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        NSLog(@"JSON = %@",JSON);
        [delegate.activityIndicator stopAnimating];
        
        [self ResponseSignUpOne:JSON];
        
    }
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         [self.view setUserInteractionEnabled:YES];
                                         if([operation.response statusCode] == 406){
                                             
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             
                                             
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                         }
                                     }];
    [operation start];
    
}


-(void)ResponseSignUpOne:(NSDictionary * )TheDict
{
    NSLog(@"ResponseDefault: %@",TheDict);
    
    [self.view setUserInteractionEnabled:YES];
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        
         UserRegistration2VC*vc = [[UserRegistration2VC alloc]init];
        [self  presentViewController:vc animated:NO completion:nil];
        //            NSString *msg = [TheDict objectForKey:@"message"];
        dispatch_async(dispatch_get_main_queue(), ^{
        });

        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        NSString *msg = [TheDict objectForKey:@"message"];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        
    }
}




# pragma mark Popular method

-(void)popularList
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpCuisineCategoryList
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpPopular:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self popularList];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpPopular :(NSDictionary * )TheDict
{
    [aryPopularlist removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"CuisineCategoryList"] count]; i++)
        {
            [aryPopularlist addObject:[[TheDict valueForKey:@"CuisineCategoryList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        txt_popular.text=@"";
        
        
    }
    
    [img_table_for_popular reloadData];
    
    str_cuisenCatId = @"80";

    [self cuisineList];
   
    
}





# pragma mark  Cuisine list method

-(void)cuisineList
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    NSDictionary *params =@{
                            
                            
                            @"cuisine_category_id"            :  str_cuisenCatId,
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpCuisineList
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpCuisineList:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self cuisineList];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpCuisineList :(NSDictionary * )TheDict
{
    [arrCuisines removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"CuisineList"] count]; i++)
        {
            [arrCuisines addObject:[[TheDict valueForKey:@"CuisineList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        txt_popular.text=@"";
        
        
    }
    
    [cuisines_table reloadData];
    
     [self AlergyList];
}

# pragma mark Hcountry method

-(void)countryList
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpHcountry
                                                      parameters:nil];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefSignUpHcountry:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self countryList];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefSignUpHcountry :(NSDictionary * )TheDict
{
    [ary_countrylist removeAllObjects];
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DietarytList"] count]; i++)
        {
            [ary_countrylist addObject:[[TheDict valueForKey:@"DietarytList"] objectAtIndex:i]];
            
        }
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        
        
    }
    
    [diatary_table reloadData];
    
    
}


# pragma mark Hcountry method

-(void)AlergyList
{
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:KFoodAlery
                                                      parameters:nil];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        [self Responsealeries:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AlergyList];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsealeries :(NSDictionary * )TheDict
{
    [array_food_allergies removeAllObjects];
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
   {
        for (int i=0; i<[[TheDict valueForKey:@"Food_Alergies"] count]; i++)
        {
            [array_food_allergies addObject:[[TheDict valueForKey:@"Food_Alergies"] objectAtIndex:i]];
            
        }
       
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        
        
    }
    
    [allergies_table reloadData];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
