//
//  UserSignUpVC.h
//  Not86
//
//  Created by Admin on 29/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserRegistration1VC.h"

@interface UserSignUpVC : UIViewController

@property(nonatomic,strong)NSMutableArray *array_Facebook_Details;
@property(nonatomic,strong)UITextField *txt_Username,*txt_Password,*txt_ConfirmPassword;

@end
