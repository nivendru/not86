//
//  MenuScreenEditMealVC.m
//  Not86
//
//  Created by Interwld on 8/22/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "MenuScreenEditMealVC.h"
#import "AFNetworking.h"
#import "Define.h"
#import "ViewMenuVC.h"

@interface MenuScreenEditMealVC ()<UIScrollViewDelegate,UITextFieldDelegate,UITextViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UIImagePickerControllerDelegate ,UIGestureRecognizerDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIImageView *img_topbar;
    UIImageView *img_BackgroundImg;
    UIScrollView *scrollviewEditMeal;
    UITextField *txt_cheftype;
    UITextField *txt_others;
    NSMutableArray *collectionarrImages;
    
    UICollectionViewFlowLayout *layout;
    UICollectionView *collView_serviceDirectory;
    
    UICollectionViewFlowLayout * layout2;
    UICollectionView * collectionView_Keywords;
    
    
    NSMutableArray * array_dietary_restryctionlist;
    NSMutableArray * Arr_temp_collection;
    
    NSString    * selected_img;
    
    NSData * imgData;
    UIImageView *img_backround;
    
    AppDelegate * delegate;
    
    UITableView * table_for_category;
    
    NSMutableArray * aryPopularlist;
    
    UITextField * txt_Category;
    NSString * str_cataogyid;
    
    NSString * str_cuisenCatId;
    
    UITableView * table_for_cuisine;
    NSMutableArray * arrCuisines;
    NSString * str_cuisineid;
    
    UITextField *txt_Cuisine;
    
    UITableView * table_for_course;
    
    NSMutableArray * array_course_list;
    NSString * str_courseid;
    UITextField *txt_Course;
    
    NSMutableArray * Arr_temp;
    NSMutableArray * Arr_temp_cuisine;
    
    NSMutableArray * Arr_temp_courselist;
    
    NSData  * imageData;
    
    UITextField *txt_MealTitle;
    UITextField *txt_Price;
    
    UITextView * txt_view;
    
    NSMutableArray * ary_Keywords;
    
    CGFloat	animatedDistance;
    
    UIView*alertviewBg;
    
    
    int selectedindex;
    NSIndexPath * indexSelected;
    UITextField*txt_keyword;
    
    int int_SelectedKeyword;
    UIView*deletePopUpBg;
    
}


@end

@implementation MenuScreenEditMealVC
@synthesize ary_dishDetails,str_DishID;

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    array_dietary_restryctionlist = [[NSMutableArray alloc]init];
    Arr_temp_collection = [[NSMutableArray alloc]init];
    selected_img = [NSString new];
    str_cataogyid = [NSString new];
    
    aryPopularlist = [[NSMutableArray alloc]init];
    
    str_cuisenCatId = [NSString new];
    
    arrCuisines = [[NSMutableArray alloc]init];
    str_cuisineid = [NSString new];
    
    array_course_list = [[NSMutableArray alloc]init];
    str_courseid = [NSString new];
    
    Arr_temp = [[NSMutableArray alloc]init];
    
    Arr_temp_courselist = [[NSMutableArray alloc]init];
    
    ary_Keywords = [[NSMutableArray alloc]init];
    
    Arr_temp_cuisine = [[NSMutableArray alloc]init];
    
    
    
    indexSelected = nil;
    selectedindex=-1;
    
    [self IntegrateHeaderDesign];
    collectionarrImages=[NSMutableArray arrayWithObjects:@"img_halal@2x.png",@"img_Univer@2x.png",@"img_Organic@2x.png", nil];
    [self IntegrateBodyDesign];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self AFDietary_restrictions];
    [self popularList];
    [self cuisineList];
    [self AFcourse];
}
-(void)IntegrateHeaderDesign
{
    
    
    
    img_topbar=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,50)];
    [img_topbar setUserInteractionEnabled:YES];
    img_topbar.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:img_topbar];
    
    
    UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(10, 17, 12, 12)];
    [img_back setUserInteractionEnabled:YES];
    img_back.backgroundColor=[UIColor clearColor];
    img_back.image=[UIImage imageNamed:@"arrow@2x.png"];
    [img_topbar addSubview:img_back];
    UILabel  * lbl_heading = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(img_back.frame)+20,4, 220,40)];
    if ([_str_type  isEqualToString:@"Dish"])
    {
        lbl_heading.text = @"Edit Dish";
        
    }
    else{
        lbl_heading.text = @"Edit  Meal";
    }
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:kFont size:16];
    [img_topbar addSubview:lbl_heading];
    
    UIButton *btn_back = [[UIButton alloc] init];
    btn_back.frame = CGRectMake(10, 17, 15,15);
    btn_back.backgroundColor = [UIColor clearColor];
    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_topbar addSubview:btn_back];
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-38, 7, 30, 30)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-logo.png"];
    [img_topbar addSubview:img_logo];
    
    scrollviewEditMeal=[[UIScrollView alloc]init];
    scrollviewEditMeal.frame = CGRectMake(0, 50, WIDTH,HEIGHT);
    [scrollviewEditMeal setShowsVerticalScrollIndicator:NO];
    scrollviewEditMeal.delegate = self;
    scrollviewEditMeal.scrollEnabled = YES;
    scrollviewEditMeal.showsVerticalScrollIndicator = NO;
    [scrollviewEditMeal setUserInteractionEnabled:YES];
    scrollviewEditMeal.backgroundColor = [UIColor orangeColor];
    [scrollviewEditMeal setContentSize:CGSizeMake(0,900)];
    [self.view addSubview:scrollviewEditMeal];
    
}

-(void)Back_btnClick{
    [self dismissViewControllerAnimated:NO completion:nil];
    //    [self.navigationController popViewControllerAnimated:NO];
    
    
}
-(void)IntegrateBodyDesign{
    
    
    UIImageView *img_Backgroundimage=[[UIImageView alloc]init];
    
    
    
    if (IS_IPHONE_6Plus)
    {
        img_Backgroundimage.frame = CGRectMake(0, 0, WIDTH, 90);
    }
    else if (IS_IPHONE_6)
    {
        img_Backgroundimage.frame = CGRectMake(0, 0, WIDTH, 90);
    }
    else
    {
        img_Backgroundimage.frame = CGRectMake(0, 0, WIDTH, 80);
    }
    
    [img_Backgroundimage setUserInteractionEnabled:YES];
    img_Backgroundimage.backgroundColor=[UIColor clearColor];
    img_Backgroundimage.image=[UIImage imageNamed:@"bg1.png"];
    [scrollviewEditMeal addSubview:img_Backgroundimage];
    
    
    UIImageView *Arrow_left=[[UIImageView alloc]init];
    
    
    
    
    if (IS_IPHONE_6Plus)
    {
        Arrow_left.frame=CGRectMake(15, 25, 10, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        Arrow_left.frame=CGRectMake(5, 35, 10, 15);
    }
    else
    {
        Arrow_left.frame=CGRectMake(5, 25, 10, 15);
        
    }
    
    [Arrow_left setUserInteractionEnabled:YES];
    Arrow_left.backgroundColor=[UIColor clearColor];
    Arrow_left.image=[UIImage imageNamed:@"arrow_left.png"];
    [img_Backgroundimage addSubview:Arrow_left];
    
    UIButton *btn_Arrowleft = [[UIButton alloc] init];
    
    
    
    if (IS_IPHONE_6Plus)
    {
        btn_Arrowleft.frame = CGRectMake(15, 20, 10, 15);
        
    }
    else if (IS_IPHONE_6){
        btn_Arrowleft.frame = CGRectMake(5, 25, 10, 15);
        
    }
    else  {
        btn_Arrowleft.frame = CGRectMake(5, 25, 10, 15);
        
    }
    
    btn_Arrowleft.backgroundColor = [UIColor clearColor];
    [btn_Arrowleft setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    //    [btn_Arrowleft addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_Backgroundimage addSubview:btn_Arrowleft];
    
    
    layout=[[UICollectionViewFlowLayout alloc] init];
    if (IS_IPHONE_6Plus)
    {
        
        collView_serviceDirectory = [[UICollectionView alloc] initWithFrame:CGRectMake(30,0,WIDTH-65,70)
                                                       collectionViewLayout:layout];
        
    }
    else if (IS_IPHONE_6)
    {
        
        collView_serviceDirectory = [[UICollectionView alloc] initWithFrame:CGRectMake(30,10,WIDTH-65,70)
                                                       collectionViewLayout:layout];
        
    }
    else
    {
        
        collView_serviceDirectory = [[UICollectionView alloc] initWithFrame:CGRectMake(30,0,WIDTH-65,70)
                                                       collectionViewLayout:layout];
        
    }
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_serviceDirectory setDataSource:self];
    [collView_serviceDirectory setDelegate:self];
    collView_serviceDirectory.scrollEnabled = YES;
    collView_serviceDirectory.showsVerticalScrollIndicator = NO;
    collView_serviceDirectory.showsHorizontalScrollIndicator = NO;
    collView_serviceDirectory.pagingEnabled = NO;
    [collView_serviceDirectory registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_serviceDirectory setBackgroundColor:[UIColor redColor]];
    layout.minimumInteritemSpacing = 2;
    layout.minimumLineSpacing = 0;
    collView_serviceDirectory.userInteractionEnabled = YES;
    [img_Backgroundimage addSubview:collView_serviceDirectory];
    
    
    
    UIImageView *Arrow_right=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        Arrow_right.frame=CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+1, 25, 10, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        Arrow_right.frame=CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 35, 10, 15);
        
    }
    else
    {
        Arrow_right.frame=CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
        
    }
    [Arrow_right setUserInteractionEnabled:YES];
    Arrow_right.backgroundColor=[UIColor clearColor];
    Arrow_right.image=[UIImage imageNamed:@"arrow_right.png"];
    [img_Backgroundimage addSubview:Arrow_right];
    
    
    UIButton *btn_Arrowright = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus)
    {
        btn_Arrowright.frame = CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+1, 25, 10, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        btn_Arrowright.frame = CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 35, 10, 15);
        
    }
    else
    {
        btn_Arrowright.frame = CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
        
    }
    btn_Arrowright.backgroundColor = [UIColor clearColor];
    //    [btn_Arrowleft setImage:[UIImage imageNamed:@"arrow_left.png"] forState:UIControlStateNormal];
    [btn_Arrowright setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    //    [btn_Arrowleft addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_Backgroundimage addSubview:btn_Arrowright];
    
    
    
    UILabel  * lbl_Mealimage = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        lbl_Mealimage.frame = CGRectMake(120,80, 220,40);
        
    }
    else if (IS_IPHONE_6)
    {
        lbl_Mealimage.frame = CGRectMake(125,80, 220,40);
        
    }
    else
    {
        lbl_Mealimage.frame = CGRectMake(110,80, 220,40);
        
    }
    lbl_Mealimage.text = @"Upload Meal image ";
    lbl_Mealimage.backgroundColor=[UIColor clearColor];
    lbl_Mealimage.textColor=[UIColor blackColor];
    lbl_Mealimage.textAlignment=NSTextAlignmentLeft;
    lbl_Mealimage.font = [UIFont fontWithName:kFontBold size:12];
    [scrollviewEditMeal addSubview:lbl_Mealimage];
    
    
    
    UIImageView *img_Backgroundimage2 =[[UIImageView alloc]init];
    
    
    
    if (IS_IPHONE_6Plus)
    {
        img_Backgroundimage2.frame = CGRectMake(100,CGRectGetMaxY(lbl_Mealimage.frame)+1, 180, 190);
    }
    else if (IS_IPHONE_6)
    {
        img_Backgroundimage2.frame = CGRectMake(100,CGRectGetMaxY(lbl_Mealimage.frame)+1, 180,190);
    }
    else
    {
        img_Backgroundimage2.frame = CGRectMake(100, CGRectGetMaxY(lbl_Mealimage.frame)+1, 180, 190);
    }
    
    [img_Backgroundimage2 setUserInteractionEnabled:YES];
    img_Backgroundimage2.backgroundColor=[UIColor clearColor];
    img_Backgroundimage2.image=[UIImage imageNamed:@"bg1.png"];
    [scrollviewEditMeal addSubview:img_Backgroundimage2];
    
    
    
    img_backround = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        img_backround.frame=CGRectMake(100, CGRectGetMaxY(lbl_Mealimage.frame)+1, 200, 180);
        
    }
    else if (IS_IPHONE_6)
    {
        img_backround.frame=CGRectMake(5, 5, 170, 170);
        
    }
    else
    {
        img_backround.frame=CGRectMake(80, CGRectGetMaxY(lbl_Mealimage.frame)+1, 130, 130);
        
    }
    NSString *ImagePath = [[NSString stringWithFormat:@"%@",[[ary_dishDetails objectAtIndex:0]valueForKey:@"DishImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [img_backround setImageWithURL:[NSURL URLWithString:ImagePath] placeholderImage:[UIImage imageNamed:@"img-dish-bg@2x.png"]];
    //img_backround.image=[UIImage imageNamed:@"img-dish1@2x.png"];
    
    [img_backround setUserInteractionEnabled:YES];
    img_backround.backgroundColor=[UIColor clearColor];
    [img_Backgroundimage2 addSubview:img_backround];
    
    
    UIButton * edite_img_in_meal = [[UIButton alloc]init];
    if (IS_IPHONE_6Plus)
    {
        edite_img_in_meal.frame=CGRectMake(150, 150, 15, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        edite_img_in_meal.frame=CGRectMake(150, 150, 15, 15);
        
    }
    else
    {
        edite_img_in_meal.frame=CGRectMake(130, 130, 15, 15);
        
        
    }
    
    edite_img_in_meal .backgroundColor = [UIColor clearColor];
    [edite_img_in_meal addTarget:self action:@selector(btn_edite_in_meal_click:) forControlEvents:UIControlEventTouchUpInside];
    [edite_img_in_meal setImage:[UIImage imageNamed:@"icon 2.png"] forState:UIControlStateNormal];
    [img_backround   addSubview:edite_img_in_meal];
    
    UILabel  * lbl_Weidth = [[UILabel alloc]init];
    if (IS_IPHONE_6Plus)
    {
        lbl_Weidth.frame=CGRectMake(CGRectGetMaxX(img_Backgroundimage2.frame)+10,CGRectGetMaxY(lbl_Mealimage.frame)+100, 100,120);
    }
    else if (IS_IPHONE_6)
    {
        lbl_Weidth.frame=CGRectMake(CGRectGetMaxX(img_Backgroundimage2.frame)+10,CGRectGetMaxY(lbl_Mealimage.frame)+90, 100,120);
    }
    else
    {
        lbl_Weidth.frame=CGRectMake(CGRectGetMaxX(img_Backgroundimage2.frame)+5,CGRectGetMaxY(lbl_Mealimage.frame)+50, 90,100);
        
    }
    lbl_Weidth.text = @"Width:150px\nHeight:150px\nSize:5mb\n.jpg..png";
    lbl_Weidth.backgroundColor=[UIColor clearColor];
    lbl_Weidth.textColor=[UIColor lightGrayColor];
    lbl_Weidth.textAlignment=NSTextAlignmentLeft;
    lbl_Weidth.numberOfLines=0;
    lbl_Weidth.font = [UIFont fontWithName:kFontBold size:8];
    [scrollviewEditMeal addSubview:lbl_Weidth];
    
    
    UIImageView *img_backgroundimages=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        img_backgroundimages.frame=CGRectMake(5, CGRectGetMaxY(img_Backgroundimage2.frame)+2, WIDTH-4, 400);
    }
    else if (IS_IPHONE_6){
        img_backgroundimages.frame=CGRectMake(5, CGRectGetMaxY(img_Backgroundimage2.frame)+2, WIDTH-4, 400);
    }
    else  {
        img_backgroundimages.frame=CGRectMake(5, CGRectGetMaxY(img_Backgroundimage2.frame)+2, WIDTH-4, 400);
        
    }
    img_backgroundimages.image=[UIImage imageNamed:@"bg1.png"];
    
    [img_backgroundimages setUserInteractionEnabled:YES];
    img_backgroundimages.backgroundColor=[UIColor clearColor];
    [scrollviewEditMeal addSubview:img_backgroundimages];
    
    txt_MealTitle = [[UITextField alloc] init];
    if (IS_IPHONE_6Plus){
        txt_MealTitle.frame=CGRectMake(30, 10, 250, 30);
    }
    else if (IS_IPHONE_6){
        txt_MealTitle.frame=CGRectMake(30, 10, 250, 30);
    }
    else  {
        txt_MealTitle.frame=CGRectMake(20, 10, 250, 30);
    }
    txt_MealTitle.borderStyle = UITextBorderStyleNone;
    txt_MealTitle.font = [UIFont fontWithName:kFont size:13];
    if ([_str_type  isEqualToString:@"Dish"])
    {
        txt_MealTitle.placeholder = @"Dish Title";
        txt_MealTitle.text= [NSString stringWithFormat:@"%@",[[ary_dishDetails objectAtIndex:0]valueForKey:@"DishName"]];

        
    }
    else{
        txt_MealTitle.placeholder = @"Meal Title";
        txt_MealTitle.text= [NSString stringWithFormat:@"%@",[[ary_dishDetails objectAtIndex:0]valueForKey:@"MenuName"]];

    }
    [txt_MealTitle setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_MealTitle setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_MealTitle.leftViewMode = UITextFieldViewModeAlways;
    txt_MealTitle.userInteractionEnabled=YES;
    txt_MealTitle.textAlignment = NSTextAlignmentLeft;
    txt_MealTitle.backgroundColor = [UIColor clearColor];
    txt_MealTitle.keyboardType = UIKeyboardTypeAlphabet;
    txt_MealTitle.delegate = self;
    [img_backgroundimages addSubview:txt_MealTitle];
    
    UIImageView *line_img=[[UIImageView alloc]init];
    
    
    if (IS_IPHONE_6Plus){
        line_img.frame=CGRectMake(30, CGRectGetMaxY(txt_MealTitle.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6){
        line_img.frame=CGRectMake(30, CGRectGetMaxY(txt_MealTitle.frame)+1, WIDTH-60, 0.5);
    }
    else  {
        line_img.frame=CGRectMake(20, CGRectGetMaxY(txt_MealTitle.frame)+1, WIDTH-40, 0.5);
        
    }
    [line_img setUserInteractionEnabled:YES];
    line_img.backgroundColor=[UIColor clearColor];
    line_img.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:line_img];
    
    
    UILabel  * lbl_Maxchar = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus){
        lbl_Maxchar.frame=CGRectMake(330,CGRectGetMaxY(txt_MealTitle.frame)+4, 100,5);
        
    }
    else if (IS_IPHONE_6){
        lbl_Maxchar.frame=CGRectMake(300,CGRectGetMaxY(txt_MealTitle.frame)+4, 100,5);
        
    }
    else  {
        lbl_Maxchar.frame=CGRectMake(240,CGRectGetMaxY(txt_MealTitle.frame)+4, 100,5);
        
        
    }
    lbl_Maxchar.text = @"(Max. 50 char)";
    lbl_Maxchar.backgroundColor=[UIColor clearColor];
    lbl_Maxchar.textColor=[UIColor lightGrayColor];
    lbl_Maxchar.textAlignment=NSTextAlignmentLeft;
    lbl_Maxchar.numberOfLines=0;
    lbl_Maxchar.font = [UIFont fontWithName:kFontBold size:7];
    [img_backgroundimages addSubview:lbl_Maxchar];
    
    
    
    
    
    txt_Category= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus){
        txt_Category.frame=CGRectMake(30, CGRectGetMaxY(txt_MealTitle.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6){
        txt_Category.frame=CGRectMake(30, CGRectGetMaxY(txt_MealTitle.frame)+5, 200, 30);
    }
    else  {
        txt_Category.frame=CGRectMake(20, CGRectGetMaxY(txt_MealTitle.frame)+5, 200, 30);
    }
    txt_Category.borderStyle = UITextBorderStyleNone;
    txt_Category.font = [UIFont fontWithName:kFont size:13];
    txt_Category.placeholder = @"Asian";
    [txt_Category setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Category setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Category.leftViewMode = UITextFieldViewModeAlways;
    txt_Category.userInteractionEnabled=YES;
    txt_Category.textAlignment = NSTextAlignmentLeft;
    txt_Category.backgroundColor = [UIColor clearColor];
    txt_Category.keyboardType = UIKeyboardTypeAlphabet;
    txt_Category.delegate = self;
    txt_MealTitle.text= [NSString stringWithFormat:@"%@",[[ary_dishDetails objectAtIndex:0]valueForKey:@"DishName"]];

    [img_backgroundimages addSubview:txt_Category];
    
    UIImageView *img_lineimg=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        img_lineimg.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        img_lineimg.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+1, WIDTH-60, 0.5);
    }
    else
    {
        img_lineimg.frame=CGRectMake(20, CGRectGetMaxY(txt_Category.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_lineimg setUserInteractionEnabled:YES];
    img_lineimg.backgroundColor=[UIColor clearColor];
    img_lineimg.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_lineimg];
    
    
    
    UIButton *img_dropbox = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        img_dropbox.frame=CGRectMake(370, CGRectGetMaxY(txt_MealTitle.frame)+20, 15, 10);
    }
    else if (IS_IPHONE_6){
        img_dropbox.frame=CGRectMake(325, CGRectGetMaxY(txt_MealTitle.frame)+20, 15, 10);
    }
    else  {
        img_dropbox.frame=CGRectMake(280, CGRectGetMaxY(txt_MealTitle.frame)+20, 15, 10);
        
    }
    img_dropbox.backgroundColor = [UIColor clearColor];
    [img_dropbox setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [img_dropbox setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [img_dropbox addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimages addSubview:img_dropbox];
    
    
    UIButton * btn_on_category = [[UIButton alloc]init];
    if (IS_IPHONE_6Plus)
    {
        btn_on_category.frame=CGRectMake(20, CGRectGetMaxY(line_img.frame)+2,330, 35);
        
    }
    else if (IS_IPHONE_6)
    {
        btn_on_category.frame=CGRectMake(20, CGRectGetMaxY(line_img.frame)+2,330, 35);
        
    }
    else
    {
        btn_on_category.frame=CGRectMake(20, CGRectGetMaxY(line_img.frame)+2,330, 35);
        
    }
    
    btn_on_category .backgroundColor = [UIColor clearColor];
    [btn_on_category addTarget:self action:@selector(btn_category_click:) forControlEvents:UIControlEventTouchUpInside];
    //  [btn_on_dropdwon setImage:[UIImage imageNamed:@"icon 2.png"] forState:UIControlStateNormal];
    [img_backgroundimages   addSubview:btn_on_category];
    
    
    
    
    
    txt_Cuisine= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus){
        txt_Cuisine.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6){
        txt_Cuisine.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+5, 200, 30);
    }
    else  {
        txt_Cuisine.frame=CGRectMake(20, CGRectGetMaxY(txt_Category.frame)+5, 200, 30);
    }
    txt_Cuisine.borderStyle = UITextBorderStyleNone;
    txt_Cuisine.font = [UIFont fontWithName:kFont size:13];
    txt_Cuisine.placeholder = @"Malay";
    [txt_Cuisine setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Cuisine setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Cuisine.leftViewMode = UITextFieldViewModeAlways;
    txt_Cuisine.userInteractionEnabled=YES;
    txt_Cuisine.textAlignment = NSTextAlignmentLeft;
    txt_Cuisine.backgroundColor = [UIColor clearColor];
    txt_Cuisine.keyboardType = UIKeyboardTypeAlphabet;
    txt_Cuisine.delegate = self;
    [img_backgroundimages addSubview:txt_Cuisine];
    
    UIImageView *img_line=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus){
        img_line.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6){
        img_line.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    }
    else  {
        img_line.frame=CGRectMake(20, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_line setUserInteractionEnabled:YES];
    img_line.backgroundColor=[UIColor clearColor];
    img_line.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_line];
    
    
    
    UIButton *img_dropbox1mg = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        img_dropbox1mg.frame=CGRectMake(370, CGRectGetMaxY(txt_Category.frame)+20, 15, 10);
    }
    else if (IS_IPHONE_6){
        img_dropbox1mg.frame=CGRectMake(325, CGRectGetMaxY(txt_Category.frame)+20, 15, 10);
    }
    else  {
        img_dropbox1mg.frame=CGRectMake(280, CGRectGetMaxY(txt_Category.frame)+20, 15, 10);
        
    }
    img_dropbox1mg.backgroundColor = [UIColor clearColor];
    [img_dropbox1mg setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [img_dropbox1mg setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [img_dropbox1mg addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimages addSubview:img_dropbox1mg];
    
    
    UIButton * btn_on_cuisines = [[UIButton alloc]init];
    if (IS_IPHONE_6Plus)
    {
        btn_on_cuisines.frame=CGRectMake(20, CGRectGetMaxY(img_lineimg.frame)+2,330, 35);
        
    }
    else if (IS_IPHONE_6)
    {
        btn_on_cuisines.frame=CGRectMake(20, CGRectGetMaxY(img_lineimg.frame)+2,330, 35);
        
    }
    else
    {
        btn_on_cuisines.frame=CGRectMake(20, CGRectGetMaxY(img_lineimg.frame)+2,330, 35);
        
    }
    
    btn_on_cuisines .backgroundColor = [UIColor clearColor];
    [btn_on_cuisines addTarget:self action:@selector(btn_cuisine_click:) forControlEvents:UIControlEventTouchUpInside];
    //  [btn_on_dropdwon setImage:[UIImage imageNamed:@"icon 2.png"] forState:UIControlStateNormal];
    [img_backgroundimages   addSubview:btn_on_cuisines];
    
    
    txt_Course= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus){
        txt_Course.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+5, 250, 30);
    }
    else if (IS_IPHONE_6){
        txt_Course.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+5, 250, 30);
    }
    else  {
        txt_Course.frame=CGRectMake(20, CGRectGetMaxY(txt_Cuisine.frame)+5, 220, 30);
    }
    txt_Course.borderStyle = UITextBorderStyleNone;
    txt_Course.font = [UIFont fontWithName:kFont size:13];
    txt_Course.placeholder = @"Appetizer,Main Course,Breakfast";
    [txt_Course setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Course setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Course.leftViewMode = UITextFieldViewModeAlways;
    txt_Course.userInteractionEnabled=YES;
    txt_Course.textAlignment = NSTextAlignmentLeft;
    txt_Course.backgroundColor = [UIColor clearColor];
    txt_Course.keyboardType = UIKeyboardTypeAlphabet;
    txt_Course.delegate = self;
    [img_backgroundimages addSubview:txt_Course];
    
    UIImageView *img_lineimge=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus){
        img_lineimge.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6){
        img_lineimge.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+1, WIDTH-60, 0.5);
    }
    else  {
        img_lineimge.frame=CGRectMake(20, CGRectGetMaxY(txt_Course.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_lineimge setUserInteractionEnabled:YES];
    img_lineimge.backgroundColor=[UIColor clearColor];
    img_lineimge.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_lineimge];
    
    
    
    UIButton *img_dropbox1mgs = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        img_dropbox1mgs.frame=CGRectMake(370, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
    }
    else if (IS_IPHONE_6){
        img_dropbox1mgs.frame=CGRectMake(325, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
    }
    else  {
        img_dropbox1mgs.frame=CGRectMake(280, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
        
    }
    img_dropbox1mgs.backgroundColor = [UIColor clearColor];
    [img_dropbox1mgs setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [img_dropbox1mgs setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [img_dropbox1mgs addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimages addSubview:img_dropbox1mgs];
    
    
    
    UIButton * btn_on_Course = [[UIButton alloc]init];
    if (IS_IPHONE_6Plus)
    {
        btn_on_Course.frame=CGRectMake(20, CGRectGetMaxY(img_line.frame)+2,330, 35);
        
    }
    else if (IS_IPHONE_6)
    {
        btn_on_Course.frame=CGRectMake(20, CGRectGetMaxY(img_line.frame)+2,330, 35);
        
    }
    else
    {
        btn_on_Course.frame=CGRectMake(20, CGRectGetMaxY(img_line.frame)+2,330, 35);
        
    }
    
    btn_on_Course .backgroundColor = [UIColor clearColor];
    [btn_on_Course addTarget:self action:@selector(btn_course_click:) forControlEvents:UIControlEventTouchUpInside];
    //  [btn_on_dropdwon setImage:[UIImage imageNamed:@"icon 2.png"] forState:UIControlStateNormal];
    [img_backgroundimages   addSubview:btn_on_Course];
    
    
    
    
    txt_keyword= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus)
    {
        txt_keyword.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6)
    {
        txt_keyword.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+5, 200, 30);
    }
    else
    {
        txt_keyword.frame=CGRectMake(20, CGRectGetMaxY(txt_Course.frame)+5, 200, 30);
    }
    txt_keyword.borderStyle = UITextBorderStyleNone;
    txt_keyword.font = [UIFont fontWithName:kFont size:13];
    txt_keyword.placeholder = @"Keyword";
    [txt_keyword setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_keyword setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_keyword.leftViewMode = UITextFieldViewModeAlways;
    txt_keyword.userInteractionEnabled=YES;
    txt_keyword.textAlignment = NSTextAlignmentLeft;
    txt_keyword.backgroundColor = [UIColor clearColor];
    txt_keyword.keyboardType = UIKeyboardTypeAlphabet;
    txt_keyword.delegate = self;
    [img_backgroundimages addSubview:txt_keyword];
    
    UICollectionViewFlowLayout *layout_Keyword = [[UICollectionViewFlowLayout alloc] init];
    collectionView_Keywords = [[UICollectionView alloc] initWithFrame:CGRectMake(0,0 ,200,35)
                                                 collectionViewLayout:layout_Keyword];
    [layout_Keyword setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collectionView_Keywords setDataSource:self];
    [collectionView_Keywords setDelegate:self];
    collectionView_Keywords.scrollEnabled = YES;
    collectionView_Keywords.showsHorizontalScrollIndicator = NO;
    [collectionView_Keywords registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collectionView_Keywords setBackgroundColor:[UIColor clearColor]];
    layout_Keyword.minimumInteritemSpacing = 0;
    layout_Keyword.minimumLineSpacing = 0;
    collectionView_Keywords.userInteractionEnabled = YES;
    [collectionView_Keywords setHidden:YES];
    [txt_keyword addSubview:collectionView_Keywords];
    
    
    
    UIImageView *img_lineimges=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus){
        img_lineimges.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+5, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6){
        img_lineimges.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+5, WIDTH-60, 0.5);
    }
    else  {
        img_lineimges.frame=CGRectMake(20, CGRectGetMaxY(txt_keyword.frame)+5, WIDTH-40, 0.5);
        
    }
    [img_lineimges setUserInteractionEnabled:YES];
    img_lineimges.backgroundColor=[UIColor clearColor];
    img_lineimges.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_lineimges];
    
    
    
    UILabel  * lbl_uptolimit = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        lbl_uptolimit.frame=CGRectMake(350, CGRectGetMaxY(txt_Course.frame)+10, 35, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        lbl_uptolimit.frame=CGRectMake(310, CGRectGetMaxY(txt_Course.frame)+10, 35, 15);
        
    }
    else
    {
        lbl_uptolimit.frame=CGRectMake(260, CGRectGetMaxY(txt_Course.frame)+10, 35, 15);
        
        
    }
    lbl_uptolimit.text = @"Up to 5";
    lbl_uptolimit.backgroundColor=[UIColor clearColor];
    lbl_uptolimit.textColor=[UIColor blackColor];
    lbl_uptolimit.textAlignment=NSTextAlignmentLeft;
    lbl_uptolimit.numberOfLines=0;
    lbl_uptolimit.font = [UIFont fontWithName:kFont size:9];
    [img_backgroundimages addSubview:lbl_uptolimit];
    
    //    UIButton *btn_uptolimit = [[UIButton alloc] init];
    //
    //    if (IS_IPHONE_6Plus){
    //        btn_uptolimit.frame=CGRectMake(200, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    //    }
    //    else if (IS_IPHONE_6){
    //        btn_uptolimit.frame=CGRectMake(325, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
    //    }
    //    else  {
    //        btn_uptolimit.frame=CGRectMake(250, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    //
    //    }
    //    btn_uptolimit.backgroundColor = [UIColor clearColor];
    ////    [btn_uptolimit setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    //    [btn_uptolimit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    //    [btn_uptolimit addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    //    [img_backgroundimages addSubview:btn_uptolimit];
    //
    
    txt_Price = [[UITextField alloc] init];
    if (IS_IPHONE_6Plus){
        txt_Price.frame=CGRectMake(30, CGRectGetMaxY(img_lineimges.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6){
        txt_Price.frame=CGRectMake(30, CGRectGetMaxY(img_lineimges.frame)+5, 200, 30);
    }
    else  {
        txt_Price.frame=CGRectMake(20, CGRectGetMaxY(img_lineimges.frame)+5, 200, 30);
    }
    txt_Price.borderStyle = UITextBorderStyleNone;
    txt_Price.font = [UIFont fontWithName:kFont size:13];
    txt_Price.placeholder = @"$14.90";
    [txt_Price setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Price setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Price.leftViewMode = UITextFieldViewModeAlways;
    txt_Price.userInteractionEnabled=YES;
    txt_Price.textAlignment = NSTextAlignmentLeft;
    txt_Price.backgroundColor = [UIColor clearColor];
    txt_Price.keyboardType = UIKeyboardTypeAlphabet;
    txt_Price.delegate = self;
    [img_backgroundimages addSubview:txt_Price];
    
    UIImageView *img_linesimge=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus){
        img_linesimge.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6){
        img_linesimge.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+1, WIDTH-60, 0.5);
    }
    else  {
        img_linesimge.frame=CGRectMake(20, CGRectGetMaxY(txt_Price.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_linesimge setUserInteractionEnabled:YES];
    img_linesimge.backgroundColor=[UIColor clearColor];
    img_linesimge.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_linesimge];
    
    
    
    UILabel  * lbl_PerMeal = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        lbl_PerMeal.frame=CGRectMake(345, CGRectGetMaxY(img_lineimges.frame)+10, 40, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        lbl_PerMeal.frame=CGRectMake(300, CGRectGetMaxY(img_lineimges.frame)+10, 40, 15);
        
    }
    else
    {
        lbl_PerMeal.frame=CGRectMake(250, CGRectGetMaxY(img_lineimges.frame)+10, 40, 15);
        
        
    }
    lbl_PerMeal.text = @"Per Meal";
    lbl_PerMeal.backgroundColor=[UIColor clearColor];
    lbl_PerMeal.textColor=[UIColor blackColor];
    lbl_PerMeal.textAlignment=NSTextAlignmentLeft;
    lbl_PerMeal.numberOfLines=0;
    lbl_PerMeal.font = [UIFont fontWithName:kFont size:9];
    [img_backgroundimages addSubview:lbl_PerMeal];
    
    
    
    
    
    UILabel  * lbl_Description = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus){
        lbl_Description.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+10, 100, 25);
        
    }
    else if (IS_IPHONE_6){
        lbl_Description.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+10, 100, 25);
        
    }
    else  {
        lbl_Description.frame=CGRectMake(20, CGRectGetMaxY(txt_Price.frame)+10, 100, 25);
        
        
    }
    lbl_Description.text = @"Description";
    lbl_Description.backgroundColor=[UIColor clearColor];
    lbl_Description.textColor=[UIColor blackColor];
    lbl_Description.textAlignment=NSTextAlignmentLeft;
    lbl_Description.numberOfLines=0;
    lbl_Description.font = [UIFont fontWithName:kFont size:12];
    [img_backgroundimages addSubview:lbl_Description];
    
    
    
    txt_view =[[UITextView alloc]init];
    if (IS_IPHONE_6Plus){
        txt_view.frame=CGRectMake(30, CGRectGetMaxY(lbl_Description.frame)+5, WIDTH-65, 100);
        
    }
    else if (IS_IPHONE_6){
        txt_view.frame=CGRectMake(30, CGRectGetMaxY(lbl_Description.frame)+5, WIDTH-60, 100);
        
    }
    else  {
        txt_view.frame=CGRectMake(25, CGRectGetMaxY(lbl_Description.frame)+5, WIDTH-50, 100);
        
        
    }
    [txt_view setReturnKeyType:UIReturnKeyDone];
    txt_view.scrollEnabled=YES;
    [txt_view setDelegate:self];
    [txt_view setReturnKeyType:UIReturnKeyDone];
    txt_view.text=@"Lorem ipsum dolor sit amet,diceret euripi dis pro et. Eu veniam probatus has,comm ado fuisset apavtere id per. Vix aperiam ci vibus. Enim lorem cu pro. sit mazim arna tus fierent ea,eiqui odio quot Verterem.";
    [txt_view setFont:[UIFont fontWithName:kFont size:10]];
    [txt_view setTextColor:[UIColor lightTextColor]];
    txt_view.userInteractionEnabled=YES;
    txt_view.backgroundColor=[UIColor whiteColor];
    txt_view.delegate=self;
    txt_view.textColor=[UIColor blackColor];
    txt_view.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    txt_view.layer.borderWidth=1.0f;
    txt_view.clipsToBounds=YES;
    [img_backgroundimages addSubview:txt_view];
    
    UILabel  * lblmax = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus){
        lblmax.frame=CGRectMake(320, CGRectGetMaxY(txt_view.frame)+1, 70, 15);
        
    }
    else if (IS_IPHONE_6){
        lblmax.frame=CGRectMake(280, CGRectGetMaxY(txt_view.frame)+1, 70, 15);
        
    }
    else  {
        lblmax.frame=CGRectMake(225, CGRectGetMaxY(txt_view.frame)+1, 70, 15);
        
        
    }
    
    lblmax.text = @"(500 char. max.)";
    lblmax.backgroundColor=[UIColor clearColor];
    lblmax.textColor=[UIColor lightGrayColor];
    lblmax.numberOfLines = 0;
    lblmax.font = [UIFont fontWithName:kFont size:8];
    [img_backgroundimages addSubview:lblmax];
    
    
    UIButton *img_deletebutton = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus)
    {
        img_deletebutton.frame=CGRectMake(30, HEIGHT-50, 170, 40);
        
        
    }
    else if (IS_IPHONE_6)
    {
        img_deletebutton.frame=CGRectMake(25,CGRectGetMaxY(img_backgroundimages.frame)+10, 160, 40);
    }
    else
    {
        img_deletebutton.frame=CGRectMake(20, HEIGHT-50, 130, 35);
        
    }
    img_deletebutton.backgroundColor = [UIColor clearColor];
    [img_deletebutton setImage:[UIImage imageNamed:@"img_delete button.png"] forState:UIControlStateNormal];
    [img_deletebutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [img_deletebutton addTarget:self action:@selector(click_on_delete_btn:) forControlEvents:UIControlEventTouchUpInside ];
    [scrollviewEditMeal addSubview:img_deletebutton];
    
    
    
    UIButton *img_savebutton = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus)
    {
        img_savebutton.frame=CGRectMake(215, HEIGHT-50, 180, 40);
        
        
    }
    else if (IS_IPHONE_6)
    {
        
        img_savebutton.frame=CGRectMake(CGRectGetMaxX(img_deletebutton.frame)+10, CGRectGetMaxY(img_backgroundimages.frame)+10, 160, 40);
    }
    else
    {
        img_savebutton.frame=CGRectMake(170, HEIGHT-50, 130, 35);
        
    }
    img_savebutton.backgroundColor = [UIColor clearColor];
    [img_savebutton setImage:[UIImage imageNamed:@"img_savbutton.png"] forState:UIControlStateNormal];
    [img_savebutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [img_savebutton addTarget:self action:@selector(click_on_save_btn:) forControlEvents:UIControlEventTouchUpInside ];
    [scrollviewEditMeal addSubview:img_savebutton];
    
#pragma tableview
    
    table_for_category = [[UITableView alloc] init ];
    table_for_category.frame  = CGRectMake(30,CGRectGetMaxY(txt_Category.frame)+5,WIDTH-60,90);
    if (IS_IPHONE_6Plus)
    {
        table_for_category.frame  = CGRectMake(30,CGRectGetMaxY(txt_Category.frame)+5,WIDTH-60,90);
    }
    else if (IS_IPHONE_6)
    {
        table_for_category.frame  = CGRectMake(30,CGRectGetMaxY(txt_Category.frame)+5,WIDTH-60,90);
    }
    else
    {
        table_for_category.frame  = CGRectMake(20,CGRectGetMaxY(txt_Category.frame)+5,WIDTH-40,90);
        
    }
    
    [table_for_category setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_category.delegate = self;
    table_for_category.dataSource = self;
    table_for_category.showsVerticalScrollIndicator = NO;
    table_for_category.layer.borderColor = [[UIColor blackColor]CGColor];
    table_for_category.layer.borderWidth = 1.0f;
    table_for_category.clipsToBounds = YES;
    table_for_category.backgroundColor = [UIColor clearColor];
    [img_backgroundimages addSubview:table_for_category];
    table_for_category.hidden = YES;
    
    
    
    table_for_cuisine = [[UITableView alloc] init ];
    table_for_cuisine.frame  = CGRectMake(30,CGRectGetMaxY(txt_Cuisine.frame)+5,WIDTH-60,90);
    if (IS_IPHONE_6Plus)
    {
        table_for_cuisine.frame  = CGRectMake(30,CGRectGetMaxY(txt_Cuisine.frame)+5,WIDTH-60,90);
    }
    else if (IS_IPHONE_6)
    {
        table_for_cuisine.frame  = CGRectMake(30,CGRectGetMaxY(txt_Cuisine.frame)+5,WIDTH-60,90);
    }
    else
    {
        table_for_cuisine.frame  = CGRectMake(20,CGRectGetMaxY(txt_Cuisine.frame)+5,WIDTH-40,90);
        
    }
    
    [table_for_cuisine setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_cuisine.delegate = self;
    table_for_cuisine.dataSource = self;
    table_for_cuisine.showsVerticalScrollIndicator = NO;
    table_for_cuisine.layer.borderColor = [[UIColor blackColor]CGColor];
    table_for_cuisine.layer.borderWidth = 1.0f;
    table_for_cuisine.clipsToBounds = YES;
    table_for_cuisine.backgroundColor = [UIColor clearColor];
    [img_backgroundimages addSubview:table_for_cuisine];
    table_for_cuisine.hidden = YES;
    
    
    table_for_course = [[UITableView alloc] init ];
    table_for_course.frame  = CGRectMake(30,CGRectGetMaxY(txt_Course.frame)+5,WIDTH-60,90);
    if (IS_IPHONE_6Plus)
    {
        table_for_course.frame  = CGRectMake(30,CGRectGetMaxY(txt_Course.frame)+5,WIDTH-60,90);
    }
    else if (IS_IPHONE_6)
    {
        table_for_course.frame  = CGRectMake(30,CGRectGetMaxY(txt_Course.frame)+5,WIDTH-60,90);
    }
    else
    {
        table_for_course.frame  = CGRectMake(20,CGRectGetMaxY(txt_Course.frame)+5,WIDTH-40,90);
        
    }
    
    [table_for_course setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_course.delegate = self;
    table_for_course.dataSource = self;
    table_for_course.showsVerticalScrollIndicator = NO;
    table_for_course.layer.borderColor = [[UIColor blackColor]CGColor];
    table_for_course.layer.borderWidth = 1.0f;
    table_for_course.clipsToBounds = YES;
    table_for_course.backgroundColor = [UIColor clearColor];
    [img_backgroundimages addSubview:table_for_course];
    table_for_course.hidden = YES;
    
    
//
    
    
    
}




#pragma tableview delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == table_for_category)
    {
        return [aryPopularlist count];
        
    }
    else if (tableView == table_for_cuisine)
    {
        return [arrCuisines count];
    }
    else if (tableView == table_for_course)
    {
        return [array_course_list count];
    }
    return 0;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table_for_category)
    {
        return 40;
    }
    else if (tableView == table_for_cuisine)
    {
        return 39;
    }
    else if (tableView == table_for_course)
    {
        return 40;
    }
    return 0;
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews)
    {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    UILabel  *lbl_Restrictions = [[UILabel alloc]initWithFrame:CGRectMake(35,-3,150,25)];
    
    if (tableView == table_for_category)
    {
        UIImageView * img_bg_for_first_tbl = [[UIImageView alloc]init];
        img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
        img_bg_for_first_tbl.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_bg_for_first_tbl setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_first_tbl];
        
        UILabel  *lbl_Restrictions = [[UILabel alloc]init];
        lbl_Restrictions.frame  =CGRectMake(5,5,146,25);
        lbl_Restrictions.text=[[aryPopularlist objectAtIndex:indexPath.row] valueForKey:@"CuisineCatName"];
        lbl_Restrictions.backgroundColor=[UIColor clearColor];
        lbl_Restrictions.textColor=[UIColor blackColor];
        lbl_Restrictions.font = [UIFont fontWithName:kFont size:12];
        [cell.contentView addSubview:lbl_Restrictions];
        
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,0, WIDTH, 43);
            lbl_Restrictions.frame  =CGRectMake(5,10,146,25);
            
        }
        else if(IS_IPHONE_6)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
            lbl_Restrictions.frame  =CGRectMake(5,10,146,25);
            
        }
        else
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
            lbl_Restrictions.frame  =CGRectMake(5,5,146,25);
            
        }
        
        
    }
    else if (tableView == table_for_cuisine)
    {
        UIImageView * img_bg_for_first_tbl = [[UIImageView alloc]init];
        img_bg_for_first_tbl.frame =  CGRectMake(0,0,WIDTH, 40);
        img_bg_for_first_tbl.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_bg_for_first_tbl setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_first_tbl];
        
        
        //
        //
        //
        //        UILabel  *lbl_Restrictions = [[UILabel alloc]init];
        //        lbl_Restrictions.frame = CGRectMake(23,-3,150,25);
        //        lbl_Restrictions.text = [[arrCuisines objectAtIndex:indexPath.row] valueForKey:@"CuisineName"];
        //        lbl_Restrictions.backgroundColor=[UIColor clearColor];
        //        lbl_Restrictions.textColor=[UIColor blackColor];
        //        lbl_Restrictions.font = [UIFont fontWithName:kFont size:12];
        //        [cell.contentView addSubview:lbl_Restrictions];
        //
        
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,0, WIDTH, 43);
            // lbl_Restrictions.frame  =CGRectMake(5,10,146,25);
            
        }
        else if(IS_IPHONE_6)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
            // lbl_Restrictions.frame  =CGRectMake(5,10,146,25);
            
        }
        else
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
            //lbl_Restrictions.frame  =CGRectMake(5,5,146,25);
            
        }
        
        
        UIButton *btn_TableCell_CheckBox1 = [[UIButton alloc]initWithFrame:CGRectMake(10,2,15,15)];
        btn_TableCell_CheckBox1.backgroundColor=[UIColor clearColor];
        [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img.checkcorrect@2x.png"] forState:UIControlStateSelected];
        [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
        btn_TableCell_CheckBox1.tag = indexPath.row;
        [img_bg_for_first_tbl addSubview:btn_TableCell_CheckBox1];
        if([Arr_temp_cuisine containsObject:[arrCuisines objectAtIndex:indexPath.row]])
        {
            [btn_TableCell_CheckBox1 setSelected:YES];
        }
        else{
            [btn_TableCell_CheckBox1 setSelected:NO];
        }
        [btn_TableCell_CheckBox1 addTarget:self action:@selector(click_on_cuisines_btn:) forControlEvents:UIControlEventTouchUpInside];
        
        lbl_Restrictions.text = [[arrCuisines objectAtIndex:indexPath.row] valueForKey:@"CuisineName"];
        
        
        
        
        
        
    }
    
    else if (tableView == table_for_course)
    {
        
        UIImageView * img_bg_for_first_tbl = [[UIImageView alloc]init];
        img_bg_for_first_tbl.frame =  CGRectMake(0,5, WIDTH, 40);
        img_bg_for_first_tbl.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_bg_for_first_tbl setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_first_tbl];
        
        //            UILabel * lbl_array_course = [[UILabel alloc]init];
        //            lbl_array_course .frame = CGRectMake(5,10,200, 15);
        //            lbl_array_course .text = [[array_course_list objectAtIndex:indexPath.row] valueForKey:@"DishCourseName"];
        //            lbl_array_course .font = [UIFont fontWithName:kFont size:12];
        //            lbl_array_course .textColor = [UIColor blackColor];
        //            lbl_array_course .backgroundColor = [UIColor clearColor];
        //            [img_bg_for_first_tbl addSubview: lbl_array_course ];
        //
        
        
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,0, WIDTH, 42);
            //   lbl_array_course.frame  =CGRectMake(10,10,146,25);
            
        }
        else if(IS_IPHONE_6)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
            //   lbl_array_course.frame  =CGRectMake(10,10,146,25);
        }
        else
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
            //    lbl_array_course.frame  =CGRectMake(10,5,146,25);
            
        }
        
        UIButton *btn_TableCell_CheckBox1 = [[UIButton alloc]initWithFrame:CGRectMake(10,2,15,15)];
        btn_TableCell_CheckBox1.backgroundColor=[UIColor clearColor];
        [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img.checkcorrect@2x.png"] forState:UIControlStateSelected];
        [btn_TableCell_CheckBox1 setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
        btn_TableCell_CheckBox1.tag = indexPath.row;
        [img_bg_for_first_tbl addSubview:btn_TableCell_CheckBox1];
        if([Arr_temp_courselist containsObject:[array_course_list objectAtIndex:indexPath.row]])
        {
            [btn_TableCell_CheckBox1 setSelected:YES];
        }
        else{
            [btn_TableCell_CheckBox1 setSelected:NO];
        }
        [btn_TableCell_CheckBox1 addTarget:self action:@selector(click_on_courselist_btn:) forControlEvents:UIControlEventTouchUpInside];
        
        lbl_Restrictions.text = [[array_course_list objectAtIndex:indexPath.row] valueForKey:@"DishCourseName"];
        
        
        
    }
    
    lbl_Restrictions.backgroundColor=[UIColor clearColor];
    lbl_Restrictions.textColor=[UIColor blackColor];
    lbl_Restrictions.font = [UIFont fontWithName:kFont size:12];
    [cell.contentView addSubview:lbl_Restrictions];
    
    return cell;
    
}
#pragma table view

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == table_for_category)
    {
        
        txt_Category.text =[[aryPopularlist objectAtIndex:indexPath.row] valueForKey:@"CuisineCatName"];
        str_cataogyid  =[[aryPopularlist objectAtIndex:indexPath.row] valueForKey:@"CuisineCatID"];
        //  str_cuisenCatId =[[aryPopularlist objectAtIndex:indexPath.row] valueForKey:@"CuisineCatID"];
        [table_for_category setHidden:YES];
    }
    else if (tableView ==  table_for_cuisine)
    {
        txt_Cuisine.text =[[arrCuisines objectAtIndex:indexPath.row] valueForKey:@"CuisineName"];
        str_cuisineid = [[arrCuisines objectAtIndex:indexPath.row] valueForKey:@"CuisineID"];
        [table_for_cuisine setHidden:YES];
    }
    else if (tableView == table_for_course)
    {
        txt_Course.text =[[array_course_list objectAtIndex:indexPath.row] valueForKey:@"DishCourseName"];
        str_courseid = [[array_course_list objectAtIndex:indexPath.row] valueForKey:@"DishCourseID"];
        
        [table_for_course setHidden:YES];
        
    }
    
}







#pragma mark UiCollectionView Delegate Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == collView_serviceDirectory )
    {
        return [array_dietary_restryctionlist count];
        
    }
    else if (collectionView == collectionView_Keywords)
    {
        return [ary_Keywords count];
    }
    return 1;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView1 dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    if (collectionView1 == collView_serviceDirectory )
    {
        
        UIImageView *img_backGnd = [[UIImageView alloc]init];
        
        
        if (IS_IPHONE_6Plus)
        {
            img_backGnd.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        else if (IS_IPHONE_6){
            img_backGnd.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        else
        {
            img_backGnd.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        img_backGnd.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:img_backGnd];
        
        
        UIImageView *img_Images = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_Images.frame = CGRectMake(28, 0, 55,55);
            
        }
        else if (IS_IPHONE_6)
        {
            img_Images.frame = CGRectMake(28, 0,55,55);
            
        }
        else
        {
            img_Images.frame = CGRectMake(25, 0, 40,40);
            
        }
        // [img_Images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[collectionarrImages objectAtIndex:indexPath.row]]]];
        
        
        NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@",[[array_dietary_restryctionlist objectAtIndex:indexPath.row] valueForKey:@"DietaryImage"]]];
        
        NSData *myData = [NSData dataWithContentsOfURL:url];
        
        img_Images.image = [UIImage imageWithData:myData];
        //    NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[array_dietary_restryctionlist objectAtIndex:indexPath.row] valueForKey:@"DietaryImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //    [img_Images setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@""]];
        [img_Images setUserInteractionEnabled:YES];
        [img_Images setContentMode:UIViewContentModeScaleAspectFill];
        [img_Images setClipsToBounds:YES];
        [img_Images setUserInteractionEnabled:YES];
        [img_backGnd addSubview:img_Images];
        
        
        UILabel  * lbl_headings = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(img_Images.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        else if (IS_IPHONE_6)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(img_Images.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        else
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(img_Images.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        
        lbl_headings.text =[[array_dietary_restryctionlist objectAtIndex:indexPath.row] valueForKey:@"DietaryName"];
        
        lbl_headings.backgroundColor=[UIColor clearColor];
        lbl_headings.textColor=[UIColor blackColor];
        lbl_headings.textAlignment=NSTextAlignmentCenter;
        lbl_headings.font = [UIFont fontWithName:kFont size:9];
        [img_backGnd addSubview:lbl_headings];
        
        
        
        UIButton *btn_incell_tikmark = [[UIButton alloc]init];
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        else
        {
            btn_incell_tikmark.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        btn_incell_tikmark.backgroundColor=[UIColor clearColor];
        btn_incell_tikmark.layer.cornerRadius = 7.0;
        btn_incell_tikmark.layer.borderColor = [[UIColor whiteColor] CGColor];
        //   btn_TableCell_CheckBox1.layer.borderWidth = 1.0;
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,30,((WIDTH-65)/3)-20,0);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(65,52,20,0);
        }
        else
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,30,((WIDTH-65)/3)-20,0);
        }
        //btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,20,40,-40);
        //{top, left, bottom, right};
        //  btn_TableCell_CheckBox1.titleEdgeInsets = UIEdgeInsetsMake(0,-10,4.5,0);
        
        [btn_incell_tikmark addTarget:self action:@selector(click_selectObjectAt_dietary:) forControlEvents:UIControlEventTouchUpInside];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@"img_correct@2x.png"] forState:UIControlStateSelected];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        btn_incell_tikmark.tag = indexPath.row;
        [cell.contentView addSubview:btn_incell_tikmark];
        
        if (indexPath.row==selectedindex)
        {
            btn_incell_tikmark.hidden = NO;
            
            
        }
        else{
            btn_incell_tikmark.hidden = YES;
            
            
        }
        
        int selectedindex=(int)indexPath.row;
        NSLog(@"%d",selectedindex);
        
        
    }
    
    else if (collectionView1 ==collectionView_Keywords)
    {
        
        CGRect sizeRect = [[ary_Keywords  objectAtIndex:indexPath.row] boundingRectWithSize:CGSizeMake(2000, 25)
                                                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                                                 attributes:@{NSFontAttributeName:[UIFont fontWithName:kFont size:11.0f]}
                                                                                    context:nil];
        
        UILabel *lbl_Keyword = [[UILabel alloc]initWithFrame:CGRectMake(2, 5, sizeRect.size.width+8, 25)];
        lbl_Keyword.text = [ary_Keywords  objectAtIndex:indexPath.row];
        lbl_Keyword.layer.cornerRadius = 4.0f;
        lbl_Keyword.clipsToBounds = YES;
        lbl_Keyword.font = [UIFont fontWithName:kFont size:11];
        lbl_Keyword.textAlignment = NSTextAlignmentCenter;
        lbl_Keyword.textColor = [UIColor whiteColor];
        lbl_Keyword.backgroundColor = [UIColor colorWithRed:20/255.0f green:58/255.0f blue:102/255.0f alpha:1];
        [cell.contentView addSubview:lbl_Keyword];
        
    }
    
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == collView_serviceDirectory)
    {
        if (IS_IPHONE_6Plus)
        {
            return CGSizeMake((WIDTH-46)/3, 70);
        }
        else if (IS_IPHONE_6)
        {
            return CGSizeMake((WIDTH-46)/3, 70);
        }
        else
        {
            return CGSizeMake((WIDTH-46)/3, 60);
        }
        
    }
    else if (collectionView ==collectionView_Keywords)
    {
        CGRect sizeRect = [[ary_Keywords  objectAtIndex:indexPath.row] boundingRectWithSize:CGSizeMake(2000, 25)
                                                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                                                 attributes:@{NSFontAttributeName:[UIFont fontWithName:kFont size:11.0f]}
                                                                                    context:nil];
        
        
        return CGSizeMake(sizeRect.size.width+12, 35);
        
        
    }
    else
    {
        
        if (IS_IPHONE_6Plus)
        {
            return CGSizeMake((200)/5,30);
        }
        else if (IS_IPHONE_6)
        {
            return CGSizeMake((175)/4, 30);
        }
        else
        {
            return CGSizeMake((200)/5, 30);
        }
        //
        
        //        CGRect sizeRect = [[ary_Keywords  objectAtIndex:indexPath.row] boundingRectWithSize:CGSizeMake(2000, 25)
        //                                                                                    options:NSStringDrawingUsesLineFragmentOrigin
        //                                                                                 attributes:@{NSFontAttributeName:[UIFont fontWithName:kFont size:11.0f]}
        //                                                                                    context:nil];
        //
        //
        //        return CGSizeMake(sizeRect.size.width+12, 35);
        
        
        
    }
    
    return CGSizeMake(0, 0);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == collectionView_Keywords)
    {
        int_SelectedKeyword = (int)indexPath.row;
        
        [self popup_Alertviewwithboth:@"Do you want to delete this Keyword?"];
        
    }
    
    
    if (collectionView == collView_serviceDirectory)
    {
        selectedindex= (int)indexPath.row;
        
        if (indexPath.row==selectedindex)
        {
            indexSelected = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
            
        }
        
        [collView_serviceDirectory reloadData];

    }
    
    
    
}

-(void)popup_Alertviewwithboth:(NSString *)message
{
    [deletePopUpBg removeFromSuperview];
    deletePopUpBg=[[UIView alloc] init];
    deletePopUpBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    deletePopUpBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    deletePopUpBg.userInteractionEnabled=TRUE;
    [self.view addSubview:deletePopUpBg];
    
    
    UIImageView *img_bg_DeletePopUP = [[UIImageView alloc]init];
    img_bg_DeletePopUP.frame = CGRectMake(5,(HEIGHT-200)/2.0, WIDTH-10, 200);
    [img_bg_DeletePopUP  setImage:[UIImage imageNamed:@"img-bg@2x-1"]];
    [img_bg_DeletePopUP  setUserInteractionEnabled:YES];
    [deletePopUpBg addSubview:img_bg_DeletePopUP];
    //    img_bg_DeletePopUP.hidden = YES;
    
    
    UIButton   *btn_Cross = [[UIButton alloc] init];
    btn_Cross.frame = CGRectMake(WIDTH-41, 10, 15, 15);
    [btn_Cross setBackgroundColor:[UIColor clearColor]];
    [btn_Cross setImage:[UIImage imageNamed: @"icon-del"] forState:UIControlStateNormal];
    [btn_Cross addTarget:self action:@selector(btn_Cross_Method:) forControlEvents:UIControlEventTouchUpInside ];
    [img_bg_DeletePopUP addSubview:btn_Cross];
    
    
    UILabel *labl_Alert = [[UILabel alloc]init];
    labl_Alert.frame = CGRectMake(20,CGRectGetMaxY(btn_Cross.frame)+3, WIDTH-60,60);
    labl_Alert.text =message;
    //    labl_Alert.text = [NSString stringWithFormat:@"%@",[array_delivery_company_name objectAtIndex:indexPath.row]];
    labl_Alert.font = [UIFont fontWithName:kFontBold size:13];
    labl_Alert.textColor = [UIColor blackColor];
    labl_Alert. lineBreakMode = NSLineBreakByWordWrapping;
    labl_Alert. numberOfLines = 1.0;
    labl_Alert.backgroundColor = [UIColor clearColor];
    [img_bg_DeletePopUP addSubview:labl_Alert];
    
    
    
    UIButton   *btn_Yes = [[UIButton alloc] init];
    btn_Yes.frame = CGRectMake(30, CGRectGetMaxY(labl_Alert.frame)+15, (WIDTH-90)/2.0, 35);
    btn_Yes.layer.cornerRadius=4.0f;
    [btn_Yes setTitle:@"YES" forState:UIControlStateNormal];
    [btn_Yes setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Yes setBackgroundColor:[UIColor colorWithRed:152.0/255.0 green:0.0/255.0 blue:34.0/255.0 alpha:1.0f]];
    btn_Yes.titleLabel.font=[UIFont fontWithName:kFont size:16];
    [btn_Yes setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Yes addTarget:self action:@selector(btn_DeleteCellYes_Method:) forControlEvents:UIControlEventTouchUpInside ];
    btn_Yes.tag = selectedindex;
    [img_bg_DeletePopUP addSubview:btn_Yes];
    
    
    UIButton   *btn_No = [[UIButton alloc] init];
    btn_No.frame = CGRectMake(CGRectGetMaxX(btn_Yes.frame)+10, CGRectGetMaxY(labl_Alert.frame)+15, (WIDTH-90)/2.0, 35);
    btn_No.layer.cornerRadius=4.0f;
    [btn_No setTitle:@"NO" forState:UIControlStateNormal];
    [btn_No setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_No setBackgroundColor:[UIColor colorWithRed:39.0/255.0 green:37.0/255.0 blue:48.0/255.0 alpha:1.0f]];
    btn_No.titleLabel.font=[UIFont fontWithName:kFont size:16];
    [btn_No setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_No addTarget:self action:@selector(btn_No_Method:) forControlEvents:UIControlEventTouchUpInside ];
    [img_bg_DeletePopUP addSubview:btn_No];
    
}


-(void)btn_DeleteCellYes_Method:(UIButton *)sender
{
    [deletePopUpBg removeFromSuperview];
    
    [ary_Keywords removeObjectAtIndex:int_SelectedKeyword];
    
    if ([ary_Keywords count]==0)
    {
        txt_keyword.placeholder = @"Keywords (eg: bags,watch)";
    }
    
    if ([ary_Keywords count]*67 >= 200)
    {
        collectionView_Keywords.frame = CGRectMake(10,0 ,200,35);
    }
    else
    {
        collectionView_Keywords.frame = CGRectMake(10,0 ,[ary_Keywords count]*67,35);
    }
    
    [collectionView_Keywords reloadData];
    [deletePopUpBg removeFromSuperview];
    //    [self AFDeleteDishFromSchedule];
    
}

-(void)btn_No_Method:(UIButton *)sender
{
    NSLog(@"btn_No_Method clicked");
    [deletePopUpBg removeFromSuperview];
    
}
-(void)btn_Cross_Method:(UIButton *)sender
{
    NSLog(@"btn_Cross_Method clicked ");
    [deletePopUpBg removeFromSuperview] ;
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}


#pragma mark TextField Delegate methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == txt_keyword)
    {
        
        [collectionView_Keywords setHidden:YES];
        
        NSString *str_Keywords = @"";
        
        for (int i=0; i<[ary_Keywords count]; i++)
        {
            str_Keywords = [NSString stringWithFormat:@"%@,%@",str_Keywords,[ary_Keywords objectAtIndex:i]];
        }
        
        if ([str_Keywords hasPrefix:@","])
        {
            str_Keywords = [str_Keywords substringFromIndex:1];
        }
        if ([str_Keywords hasSuffix:@","])
        {
            str_Keywords = [str_Keywords substringToIndex:[str_Keywords length]-1];
        }
        
        txt_keyword.text = str_Keywords;
        
    }
  
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == txt_keyword)
    {
        NSString *str_Keywords = txt_keyword.text;
        
        if ([str_Keywords hasPrefix:@","])
        {
            str_Keywords = [str_Keywords substringFromIndex:1];
        }
        if ([str_Keywords hasSuffix:@","])
        {
            str_Keywords = [str_Keywords substringToIndex:[str_Keywords length]-1];
        }
        
        
        txt_keyword.text = str_Keywords;
        
        NSArray *ary_TempKeyword = [[NSArray alloc]init];
        
        if ([str_Keywords length]>0)
        {
            ary_TempKeyword = [str_Keywords componentsSeparatedByString:@","];
        }
        
        //        if ([ary_TempKeyword count]<6 && [ary_TempKeyword count]>0)
        //        {
        [ary_Keywords removeAllObjects];
        
        int total = (int)[ary_TempKeyword count];
        
        if ([ary_TempKeyword count]>5)
        {
            total = 5;
        }
        for (int i=0; i<total; i++)
        {
            [ary_Keywords addObject:[ary_TempKeyword objectAtIndex:i]];
        }
        //    }
        
        
        
        if ([ary_Keywords count]>0)
        {
            txt_keyword.text = @"";
            txt_keyword.placeholder = @"";
            
            if ([ary_TempKeyword count] > 5)
            {
                [self popup_Alertview:@"You can enter max 5 keywords."];
            }
            
            
            int width_CollectionView = 0;
            for (int i=0; i<[ary_Keywords count]; i++)
            {
                CGRect sizeRect = [[ary_Keywords  objectAtIndex:i] boundingRectWithSize:CGSizeMake(2000, 25)
                                                                                options:NSStringDrawingUsesLineFragmentOrigin
                                                                             attributes:@{NSFontAttributeName:[UIFont fontWithName:kFont size:11.0f]}
                                                                                context:nil];
                
                
                
                width_CollectionView = width_CollectionView + sizeRect.size.width + 12;
                
                
            }
            
            if (width_CollectionView >= 275)
            {
                collectionView_Keywords.frame = CGRectMake(10,0 ,200,35);
            }
            else
            {
                collectionView_Keywords.frame = CGRectMake(10,0 ,width_CollectionView,35);
            }
            
            [collectionView_Keywords setHidden:NO];
            [collectionView_Keywords reloadData];
            //   }
            
        }
        else
        {
            txt_keyword.placeholder = @"Keywords (eg: Tacos,fries)";
            [collectionView_Keywords setHidden:YES];
        }
        
    }
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == txt_keyword)
    {
        if ([string isEqualToString:@" "])
        {
            return NO;
        }
    }
    return YES;
}



#pragma mark - IMAGEPICKER DELEGATE METHODS
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *imageOriginal =  [info objectForKey:UIImagePickerControllerEditedImage];
    
    imgData =  UIImageJPEGRepresentation(imageOriginal,0.5);
    img_backround.image = imageOriginal;
    
    
    
    [picker dismissViewControllerAnimated:YES completion:Nil];
    
}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:Nil];
}


#pragma mark - ACTION SHEET DELEGATE

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 0)
    {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            UIImagePickerController*imgPicker = [[UIImagePickerController alloc] init];
            UIColor* color = [UIColor colorWithRed:46.0/255 green:127.0/255 blue:244.0/255 alpha:1];
            [imgPicker.navigationBar setTintColor:color];
            imgPicker.delegate = self;
            imgPicker.allowsEditing = YES;
            imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            [self presentViewController:imgPicker animated:NO completion:Nil];
        }
    }
    else if(buttonIndex == 1)
    {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController*imgPicker = [[UIImagePickerController alloc] init];
            UIColor* color = [UIColor colorWithRed:46.0/255 green:127.0/255 blue:244.0/255 alpha:1];
            [imgPicker.navigationBar setTintColor:color];
            imgPicker.delegate = self;
            imgPicker.allowsEditing = YES;
            imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:imgPicker animated:NO completion:Nil];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Not86" message:@"Device Does Not Support Camera" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
}


#pragma click events

-(void)btn_edite_in_meal_click:(UIButton *)sender
{
    NSLog(@"btn_edite_in_meal_click");
    //  selected_img = @"meal_image";
    
    UIActionSheet *actionSheet_popupQuery = [[UIActionSheet alloc] initWithTitle:@"Select A Image From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Camera", nil];
    actionSheet_popupQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet_popupQuery showInView:self.view];
    
    
    
    
}


-(void)btn_category_click:(UIButton *)sender
{
    NSLog(@"btn_category_click");
    table_for_category.hidden = NO;
    table_for_cuisine.hidden = YES;
    table_for_course.hidden = YES;
}
-(void)btn_cuisine_click:(UIButton *)sender
{
    NSLog(@"btn_cuisine_click");
    table_for_category.hidden = YES;
    table_for_cuisine.hidden = NO;
    table_for_course.hidden = YES;
}
-(void)btn_course_click:(UIButton *)sender
{
    NSLog(@"btn_course_click");
    table_for_category.hidden = YES;
    table_for_cuisine.hidden = YES;
    table_for_course.hidden = NO;
}

-(void)click_on_delete_btn:(UIButton *)sender
{
    NSLog(@"click_on_delete_btn");
    
    [self AFDeleteDish];
    
}
-(void)click_on_save_btn:(UIButton *)sender
{
    
    
    if ([Arr_temp count] == 0 )
    {
        
        [self popup_Alertview:@"please select dietary restriction"];
        
    }
    
    else  if (imgData == nil)
    {
        [self popup_Alertview:@"please select dish image"];
    }
    
    
    else if(txt_MealTitle.text.length == 0)
    {
        
        [self popup_Alertview:@"please enter Dish Title"];
        
    }
    else if(str_cataogyid.length == 0)
    {
        
        [self popup_Alertview:@"please select category"];
        
    }
    
    else if(str_cuisineid.length  == 0)
    {
        
        [self popup_Alertview:@"please select Cuisines"];
        
    }
    
    else if(str_courseid.length  == 0)
    {
        
        [self popup_Alertview:@"please select courses"];
        
    }
    
    
    else if ([ary_Keywords count] == 0)
    {
        [self popup_Alertview:@"Please enter keywords."];
    }
    
    else if(txt_Price.text.length == 0)
    {
        
        [self popup_Alertview:@"please enter price"];
        
    }
    
    else if(txt_view.text.length == 0)
    {
        
        [self popup_Alertview:@"please write description"];
        
    }
    else
    {
        
        [self AFforSingleDish_r_Meal];
        //     MenuScreenEditMealVC *menuScreenEdit = [[MenuScreenEditMealVC alloc]init];
        //     [self presentViewController:menuScreenEdit animated:NO completion:nil];
    }
    //    [self.navigationController pushViewController:menuScreenEdit animated:NO];
    
}


-(void) click_selectObjectAt_dietary:(UIButton *)sender
{
    
    
    
    
    if([Arr_temp containsObject:[array_dietary_restryctionlist objectAtIndex:sender.tag]])
    {
        [Arr_temp removeObject:[array_dietary_restryctionlist objectAtIndex:sender.tag]];
    }
    else
    {
        [Arr_temp addObject:[array_dietary_restryctionlist objectAtIndex:sender.tag]];
    }
    
    //[collectionView_FashionCategory reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:sender.tag inSection:0]]  withRowAnimation:UITableViewRowAnimationFade];
    [collView_serviceDirectory reloadData];
    
    
}




//-(void) click_selectObjectAt:(UIButton *) sender
//{
//
//    if([Arr_temp_collection containsObject:[array_dietary_restryctionlist objectAtIndex:sender.tag]])
//    {
//        [Arr_temp_collection removeObject:[array_dietary_restryctionlist objectAtIndex:sender.tag]];
//    }
//    else
//    {
//        [Arr_temp_collection addObject:[array_dietary_restryctionlist objectAtIndex:sender.tag]];
//    }
//
//    //[collectionView_FashionCategory reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:sender.tag inSection:0]]  withRowAnimation:UITableViewRowAnimationFade];
//    [collView_serviceDirectory reloadData];
//
//
//
//}





-(void) click_on_cuisines_btn:(UIButton *) sender
{
    
    
    
    if([Arr_temp_cuisine containsObject:[arrCuisines objectAtIndex:sender.tag]])
    {
        [Arr_temp_cuisine removeObject:[arrCuisines objectAtIndex:sender.tag]];
    }
    else
    {
        if (Arr_temp_cuisine.count >4)
        {
            // [self popup_Alertview:@"Select upto five cuisines"];
            
        }
        else{
            [Arr_temp_cuisine addObject:[arrCuisines objectAtIndex:sender.tag]];
            
        }
    }
    
    [table_for_cuisine reloadData];
    
    
}

-(void) click_on_courselist_btn:(UIButton *) sender
{
    
    if([Arr_temp_courselist containsObject:[array_course_list objectAtIndex:sender.tag]])
    {
        [Arr_temp_courselist removeObject:[array_course_list objectAtIndex:sender.tag]];
    }
    else
    {
        if (Arr_temp_courselist.count >4)
        {
            // [self popup_Alertview:@"Select upto five cuisines"];
            
        }
        else
        {
            [Arr_temp_courselist addObject:[array_course_list objectAtIndex:sender.tag]];
            
        }
    }
    
    [table_for_course reloadData];
    
}






# pragma mark ditary_ristrictions method

-(void)AFDietary_restrictions
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kDietary_restryctions
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsediet:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
         
         [delegate.activityIndicator stopAnimating];
         
         if([operation.response statusCode] == 406)
         {
             
             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
             return;
         }
         
         if([operation.response statusCode] == 403)
         {
             NSLog(@"Upload Failed");
             return;
         }
         if ([[operation error] code] == -1009)
         {
             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                          message:@"Please check your internet connection"
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
             [av show];
         }
         else if ([[operation error] code] == -1001)
         {
             
             NSLog(@"Successfully Registered");
             [self AFDietary_restrictions];
         }
     }];
    [operation start];
    
}
-(void) Responsediet :(NSDictionary * )TheDict
{
    [array_dietary_restryctionlist removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DietarytList"] count]; i++)
        {
            [array_dietary_restryctionlist addObject:[[TheDict valueForKey:@"DietarytList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //  txt_popular.text=@"";
        
        
    }
    
    [collView_serviceDirectory reloadData];
    
    
    
}



# pragma mark Popular method

-(void)popularList
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpCuisineCategoryList
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsepopularlist:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self popularList];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsepopularlist :(NSDictionary * )TheDict
{
    [aryPopularlist removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"CuisineCategoryList"] count]; i++)
        {
            [aryPopularlist addObject:[[TheDict valueForKey:@"CuisineCategoryList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //  txt_popular.text=@"";
        
        
    }
    
    [table_for_category reloadData];
    
    
}

# pragma mark  Cuisine list method

-(void)cuisineList
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    NSDictionary *params =@{
                            
                            
                            @"cuisine_category_id"            :  str_cuisenCatId,
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpCuisineList
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsecuisenlist:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self cuisineList];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsecuisenlist :(NSDictionary * )TheDict
{
    [arrCuisines removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"CuisineList"] count]; i++)
        {
            [arrCuisines addObject:[[TheDict valueForKey:@"CuisineList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        // txt_popular.text=@"";
        
        
    }
    
    [table_for_cuisine reloadData];
    
    
}


# pragma mark  Course list method

-(void)AFcourse
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kCourses
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsecousrelist:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFcourse];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsecousrelist :(NSDictionary * )TheDict
{
    [array_course_list removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DishCoursetList"] count]; i++)
        {
            [array_course_list addObject:[[TheDict valueForKey:@"DishCoursetList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        // txt_popular.text=@"";
        
        
    }
    
    [table_for_course reloadData];
    
}


# pragma mark  Course list method

-(void)AFDeleteDish
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    NSDictionary *params;
    
    params =@{
              
              @"uid"                            : [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
              @"product_id"                     :  str_DishID,
              
              };

    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:KDeletedish
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsedeletelist:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFDeleteDish];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsedeletelist :(NSDictionary * )TheDict
{
    [array_course_list removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
    
}


#pragma mark - method SingleDish_r_meal

-(void)AFforSingleDish_r_Meal
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    

    
    NSMutableString *str_dietryids = [[NSMutableString alloc] init];
    
    for (int i=0; i<[Arr_temp count]; i++)
    {
        
        
        if (i==[Arr_temp count]-1)
        {
            [str_dietryids  appendString:[NSString stringWithFormat:@"%@",[[Arr_temp objectAtIndex:i] valueForKey:@"DietaryID"]]];
        }
        else
        {
            [str_dietryids  appendString:[NSString stringWithFormat:@"%@||",[[Arr_temp objectAtIndex:i] valueForKey:@"DietaryID"]]];
        }
        
        
    }
    
//    NSMutableString *str_dietryidsmeal = [[NSMutableString alloc] init];
//    
//    for (int i=0; i<[Arr_temp_in_meal count]; i++)
//    {
//        
//        
//        if (i==[Arr_temp_in_meal count]-1)
//        {
//            [str_dietryidsmeal  appendString:[NSString stringWithFormat:@"%@",[[Arr_temp_in_meal objectAtIndex:i] valueForKey:@"DietaryID"]]];
//        }
//        else
//        {
//            [str_dietryidsmeal  appendString:[NSString stringWithFormat:@"%@||",[[Arr_temp_in_meal objectAtIndex:i] valueForKey:@"DietaryID"]]];
//        }
//        
//        
//    }
    
    
    NSString *str_Keywords = @"";
    
    for (int i=0; i<[ary_Keywords count]; i++)
    {
        str_Keywords = [NSString stringWithFormat:@"%@,%@",str_Keywords,[ary_Keywords objectAtIndex:i]];
    }
    
    if ([str_Keywords hasPrefix:@","])
    {
        str_Keywords = [str_Keywords substringFromIndex:1];
    }
    
//    NSString *str_Keywords_meal = @"";
//    
//    for (int i=0; i<[Ary_keywordmeal count]; i++)
//    {
//        str_Keywords_meal = [NSString stringWithFormat:@"%@,%@",str_Keywords_meal,[Ary_keywordmeal objectAtIndex:i]];
//    }
//    
//    if ([str_Keywords_meal hasPrefix:@","])
//    {
//        str_Keywords_meal = [str_Keywords_meal substringFromIndex:1];
//    }
    
    
    NSDictionary *params;
    params =@{
              
              @"uid"                       : [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
              @"title"                     :  txt_MealTitle.text,
              @"type"                      :  _str_type,
              
              @"dietary_restrictions"      :  str_dietryids,
              
              @"cuisine_category_id"       :  str_cataogyid,
              @"dish_cuisine_id"           :  str_cuisineid,
              @"course_id"                 :  str_courseid,
              
              @"dish_tags"                 :  str_Keywords,
              @"dish_price"                :  txt_Price.text,
              @"description"               :  txt_view.text,
              
              };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request;
    
    
    if (imgData != nil)
    {
        
        request = [httpClient multipartFormRequestWithMethod:@"POST" path:ksingledishandmeal  parameters:params constructingBodyWithBlock:^(id <AFMultipartFormData>formData)
                   {
                       NSTimeInterval timeInterval = [NSDate timeIntervalSinceReferenceDate];
                       [formData appendPartWithFileData:imgData name:@"dish_image" fileName:[NSString stringWithFormat:@"%lf-dishimage.png",timeInterval] mimeType:@"image/jpeg"];
                       
                   }];
    }
    
    else
    {
        request = [httpClient requestWithMethod:@"POST" path:ksingledishandmeal  parameters:params];
        
    }
    
    
    NSLog(@"request:%@",request);
    
    //====================================================RESPONSE
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        NSLog(@"JSON = %@",JSON);
        [delegate.activityIndicator stopAnimating];
        
        [self ResponseSingleDish_r_Meal:JSON];
        
    }
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         [self.view setUserInteractionEnabled:YES];
                                         if([operation.response statusCode] == 406){
                                             
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             
                                             
                                         }
                                         else if ([[operation error] code] == -1001)
                                         {
                                             NSLog(@"Successfully Registered");
                                             [self AFforSingleDish_r_Meal];
                                         }
                                     }];
    [operation start];
    
}


-(void)ResponseSingleDish_r_Meal:(NSDictionary * )TheDict
{
    NSLog(@"ResponseDefault: %@",TheDict);
    
    [self.view setUserInteractionEnabled:YES];
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        ViewMenuVC *menuScreenEdit = [[ViewMenuVC alloc]init];
        [self presentViewController:menuScreenEdit animated:NO completion:nil];
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        
    }
}






-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}









- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
