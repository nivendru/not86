//
//  MyScheduleScreenVC.m
//  Not86
//
//  Created by Interwld on 8/22/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "MyScheduleScreenVC.h"
#import "CKCalendarView.h"


@interface MyScheduleScreenVC ()<UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate,UITableViewDataSource,CKCalendarDelegate>
{
    UIImageView *img_topbar;
    UITableView *table_schedule;
    NSMutableArray *array_serving_img;
    NSMutableArray * array_serving_time;
    NSMutableArray * array_serving_items;
    UIScrollView *scrollview_Schedule;
    
}
@property(nonatomic, weak) CKCalendarView *calendar;
@property(nonatomic, strong) UILabel *dateLabel;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property(nonatomic, strong) NSDate *minimumDate;
@property(nonatomic, strong) NSArray *disabledDates;



@end

@implementation MyScheduleScreenVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self IntegrateHeaderDesign];
    [self IntegrateBodyDesign];
    // Do any additional setup after loading the view.
}
-(void)IntegrateHeaderDesign{
    
    
    img_topbar=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,45)];
    [img_topbar setUserInteractionEnabled:YES];
    img_topbar.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:img_topbar];
    
    
    UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(10, 17, 20, 16)];
    [img_back setUserInteractionEnabled:YES];
    img_back.backgroundColor=[UIColor clearColor];
    img_back.image=[UIImage imageNamed:@"icon_menu@2x.png"];
    [img_topbar addSubview:img_back];
    UILabel  * lbl_heading = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(img_back.frame)+20,4, 220,40)];
    lbl_heading.text = @"My Schedule ";
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:kFontBold size:20];
    [img_topbar addSubview:lbl_heading];
    
    UIButton *btn_back = [[UIButton alloc] init];
    btn_back.frame = CGRectMake(10, 17, 15,15);
    btn_back.backgroundColor = [UIColor clearColor];
    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_topbar addSubview:btn_back];
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-38, 7, 30, 30)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-logo.png"];
    [img_topbar addSubview:img_logo];
    
    scrollview_Schedule=[[UIScrollView alloc]init];
    scrollview_Schedule.frame=CGRectMake(0, 45, WIDTH, HEIGHT-100);
    [scrollview_Schedule setShowsVerticalScrollIndicator:NO];
    scrollview_Schedule.delegate = self;
    scrollview_Schedule.scrollEnabled = YES;
    scrollview_Schedule.showsVerticalScrollIndicator = NO;
    [scrollview_Schedule setUserInteractionEnabled:YES];
    scrollview_Schedule.backgroundColor=[UIColor clearColor];
//    scrollview_Schedule.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [self.view addSubview:scrollview_Schedule];
    
    
}
-(void)Back_btnClick{
    //    [self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}
-(void)IntegrateBodyDesign{
    UIButton *btn_Signaldish = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        btn_Signaldish.frame = CGRectMake(50, CGRectGetMaxY(img_topbar.frame)+15, 130,25);
        
    }
    else if (IS_IPHONE_6){
        btn_Signaldish.frame = CGRectMake(40, 10, 130,40);
    }
    else  {
        btn_Signaldish.frame = CGRectMake(40, CGRectGetMaxY(img_topbar.frame)+10, 120,30);
        
    }
    
    btn_Signaldish.backgroundColor = [UIColor blackColor];
    [btn_Signaldish setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Signaldish setTitle:@"Schedule" forState:UIControlStateNormal];
    btn_Signaldish.titleLabel.font = [UIFont fontWithName:kFontBold size:15];
    [btn_Signaldish addTarget:self action:@selector(Schedule_btnClick:) forControlEvents:UIControlEventTouchUpInside ];
    [scrollview_Schedule addSubview:btn_Signaldish];
    UIButton *btn_Meal = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        btn_Meal.frame = CGRectMake(180, CGRectGetMaxY(img_topbar.frame)+15, 130,25);
    }
    else if (IS_IPHONE_6){
        btn_Meal.frame = CGRectMake(170, 10, 130,40);
    }
    else  {
        btn_Meal.frame = CGRectMake(160, CGRectGetMaxY(img_topbar.frame)+10, 120,30);
    }
    
    btn_Meal.backgroundColor = [UIColor lightGrayColor];
    [btn_Meal setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Meal setTitle:@"On Request" forState:UIControlStateNormal];
    btn_Meal.titleLabel.font = [UIFont fontWithName:kFontBold size:15];
    [btn_Meal addTarget:self action:@selector(onRequest_btnClick:) forControlEvents:UIControlEventTouchUpInside ];
    [scrollview_Schedule addSubview:btn_Meal];
    
    
    
    array_serving_img = [[NSMutableArray alloc]initWithObjects:@"icon-serving@2x.png",@"icon-serving@2x.png",@"icon-serving@2x.png",nil];
    array_serving_time = [[NSMutableArray alloc]initWithObjects:@"Now-12:00PM",@"01:00PM-03:00PM",@"07:00PM-9:00PM", nil];
    array_serving_items =[[NSMutableArray alloc]initWithObjects:@"Chill Chicken,Spicy Nugget,Peac...",@"Chill Chicken,Spicy Nugget,Peac...",@"Chill Chicken,Spicy Nugget,Peac...", nil];
#pragma view_for_schedule
    
//    UIView *view_schedule = [[UIView alloc]init];
//    view_schedule.frame=CGRectMake(0,CGRectGetMaxY(btn_Meal.frame)+4,WIDTH,310);
//    view_schedule.backgroundColor=[UIColor clearColor];
//    [scrollview_Schedule addSubview:view_schedule];
//    
    UIImageView *img_calender = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus){
        img_calender.frame = CGRectMake(10,CGRectGetMaxY(btn_Meal.frame)+10, WIDTH-20, 305);
        
    }
    else if (IS_IPHONE_6){
        img_calender.frame = CGRectMake(10,CGRectGetMaxY(btn_Meal.frame)+10, WIDTH-20, 310);
    }
    else  {
        img_calender.frame = CGRectMake(10,CGRectGetMaxY(btn_Meal.frame)+10, WIDTH-20, 300);
        
    }

    [img_calender  setImage:[UIImage imageNamed:@"img_backgound.png"]];
       img_calender.backgroundColor = [UIColor whiteColor];
    [img_calender  setUserInteractionEnabled:YES];
    [ scrollview_Schedule addSubview:img_calender ];
    
    
    
    CKCalendarView *calendar = [[CKCalendarView alloc] initWithStartDay:startMonday];
    self.calendar = calendar;
    calendar.delegate = self;
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    self.minimumDate = [self.dateFormatter dateFromString:@"20/09/2012"];
    
    self.disabledDates = @[
                           [self.dateFormatter dateFromString:@"05/01/2013"],
                           [self.dateFormatter dateFromString:@"06/01/2013"],
                           [self.dateFormatter dateFromString:@"07/01/2013"]
                           ];
    
    calendar.onlyShowCurrentMonth = NO;
    calendar.adaptHeightToNumberOfWeeksInMonth = YES;
    
    calendar.frame = CGRectMake(10, 0, 330, 100);
    [img_calender addSubview:calendar];
    
    //    self.dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(calendar.frame) + 4, self.view.bounds.size.width, 24)];
    //    [self.view addSubview:self.dateLabel];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localeDidChange) name:NSCurrentLocaleDidChangeNotification object:nil];
    
    UIImageView *img_bg3 = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus){
        img_bg3.frame = CGRectMake(10,CGRectGetMaxY(img_calender.frame)+10, WIDTH-20, 178);
        
    }
    else if (IS_IPHONE_6){
        img_bg3.frame = CGRectMake(10,CGRectGetMaxY(img_calender.frame)+5, WIDTH-20, 178);
    }
    else  {
        img_bg3.frame = CGRectMake(10,CGRectGetMaxY(img_calender.frame)+10, WIDTH-20, 178);
        
    }
    [img_bg3  setImage:[UIImage imageNamed:@"img_backgound.png"]];
    img_bg3.backgroundColor = [UIColor whiteColor];
    [img_bg3  setUserInteractionEnabled:YES];
    [ scrollview_Schedule addSubview:img_bg3 ];
    
    
    UILabel *lbl_AddSchedule = [[UILabel alloc]init];
    if (IS_IPHONE_6Plus){
        lbl_AddSchedule.frame = CGRectMake(260,10, 20, 20);
        
    }
    else if (IS_IPHONE_6){
        lbl_AddSchedule.frame = CGRectMake(40,HEIGHT-65, 300, 55);
    }
    else  {
        lbl_AddSchedule.frame = CGRectMake(260,10, 20, 20);
        
    }
    

    lbl_AddSchedule .text = @"ADD SCHEDULE";
    lbl_AddSchedule .font = [UIFont fontWithName:kFont size:18];
    lbl_AddSchedule .textColor = [UIColor whiteColor];
    lbl_AddSchedule .backgroundColor = [UIColor clearColor];
    lbl_AddSchedule.layer.cornerRadius = 4.0f;
    lbl_AddSchedule.clipsToBounds = YES;
    lbl_AddSchedule.userInteractionEnabled=YES;
    lbl_AddSchedule.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lbl_AddSchedule];

    
    UIButton *btn_AddSchedule = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPHONE_6Plus){
        btn_AddSchedule.frame = CGRectMake(260,10, 20, 20);
        
    }
    else if (IS_IPHONE_6){
        btn_AddSchedule.frame = CGRectMake(40,HEIGHT-65, 300, 55);
    }
    else  {
        btn_AddSchedule.frame = CGRectMake(260,10, 20, 20);
        
    }

    [btn_AddSchedule addTarget:self action:@selector(clickScheduleBtn) forControlEvents:UIControlEventTouchUpInside];
    [btn_AddSchedule setImage:[UIImage imageNamed:@"bg_applybtn.png"] forState:UIControlStateNormal];
    btn_AddSchedule.userInteractionEnabled=YES;
    [self.view  addSubview:btn_AddSchedule];
    

    
#pragma mark Tableview
#pragma table_for_schedule
    
    table_schedule = [[UITableView alloc] init ];
    table_schedule.frame  = CGRectMake(12,0,WIDTH-25,180);
    [table_schedule setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_schedule.delegate = self;
    table_schedule.dataSource = self;
    table_schedule.showsVerticalScrollIndicator = NO;
    table_schedule.backgroundColor = [UIColor clearColor];
    [img_bg3 addSubview:table_schedule];
}
-(void)clickScheduleBtn{
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
    {
      
            return [array_serving_img count];
        
    }
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
    {
        return 1;
    }
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        return 60;
      
    }
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        
        NSString *kReuseIndentifier = @"myCell";
        
        UITableViewCell *cell;
        cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
            cell.selectionStyle = UITableViewCellSeparatorStyleNone;
            
        }
        
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
        
        cell.backgroundColor = [UIColor clearColor];
        
        if (tableView == table_schedule)
        {
            UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
//               img_cellBackGnd.frame =  CGRectMake(0,2, 320, 50);
               [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
            img_cellBackGnd.backgroundColor =[UIColor clearColor];
            [img_cellBackGnd setUserInteractionEnabled:YES];
            [cell.contentView addSubview:img_cellBackGnd];
            
            UIImageView *img_line = [[UIImageView alloc]init];
//               img_line.frame =  CGRectMake(7,49, WIDTH-60, 0.5);
            img_line.backgroundColor =[UIColor grayColor];
            [img_line setUserInteractionEnabled:YES];
            [img_cellBackGnd addSubview:img_line];
            
            UIImageView *servingimges = [[UIImageView alloc]init];
//              servingimges.frame =  CGRectMake(10,10, 30, 30);
            [servingimges setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_serving_img objectAtIndex:indexPath.row]]]];
            [servingimges setImage:[UIImage imageNamed:@"img_Timehandle.png"]];
            servingimges.backgroundColor =[UIColor clearColor];
            [servingimges setUserInteractionEnabled:YES];
            [img_cellBackGnd addSubview:servingimges];
            
            UILabel *serving_timeings = [[UILabel alloc]init];
//               serving_timeings .frame = CGRectMake(50,10,200, 15);
            serving_timeings .text = [NSString stringWithFormat:@"%@",[array_serving_time objectAtIndex:indexPath.row]];
            serving_timeings .font = [UIFont fontWithName:kFontBold size:12];
            serving_timeings .textColor = [UIColor blackColor];
            serving_timeings .backgroundColor = [UIColor clearColor];
            [img_cellBackGnd addSubview:serving_timeings ];
            
            UILabel *serving_items = [[UILabel alloc]init];
//              serving_items .frame = CGRectMake(50,30,200, 15);
            serving_items .text = [NSString stringWithFormat:@"%@",[ array_serving_items objectAtIndex:indexPath.row]];
            serving_items .font = [UIFont fontWithName:kFont size:14];
            serving_items .textColor = [UIColor blackColor];
            serving_items.backgroundColor = [UIColor clearColor];
            [img_cellBackGnd addSubview:serving_items ];
            
            
            UIButton *icon_right_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
//            icon_right_arrow.frame = CGRectMake(260,10, 20, 20);
            [icon_right_arrow addTarget:self action:@selector(icon_right_arrow_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
            [icon_right_arrow setImage:[UIImage imageNamed:@"arrow_right.png"] forState:UIControlStateNormal];
            [img_cellBackGnd   addSubview:icon_right_arrow];
            
            
            if (IS_IPHONE_6Plus)
            {
                img_cellBackGnd.frame =  CGRectMake(5,2, WIDTH-20, 50);
                img_line.frame =  CGRectMake(7,49, WIDTH-30, 0.5);
                servingimges.frame =  CGRectMake(10,10, 30, 30);
                serving_timeings .frame = CGRectMake(50,10,WIDTH-10, 10);
                serving_items .frame = CGRectMake(50,30,200, 20);
                icon_right_arrow.frame = CGRectMake(280,10, 20, 20);
                
            }
            else if (IS_IPHONE_6)
            {
                img_cellBackGnd.frame =  CGRectMake(0,2, WIDTH-25, 180);
                img_line.frame =  CGRectMake(15,49, WIDTH-65, 0.5);
                servingimges.frame =  CGRectMake(15,5, 30, 30);
                serving_timeings .frame = CGRectMake(55,5,250, 10);
                serving_items .frame = CGRectMake(55,20,300, 20);
                icon_right_arrow.frame = CGRectMake(WIDTH-70,10, 10, 15);
                
            }
            else
            {
                img_cellBackGnd.frame =  CGRectMake(0,2, 320, 50);
                img_line.frame =  CGRectMake(7,49, 280, 0.5);
                servingimges.frame =  CGRectMake(10,10, 30, 30);
                serving_timeings .frame = CGRectMake(50,10,200, 15);
                serving_items .frame = CGRectMake(50,30,200, 15);
                icon_right_arrow.frame = CGRectMake(260,10, 20, 20);
                
            }
            
        
}
         return cell;
    }


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

- (void)localeDidChange {
    [self.calendar setLocale:[NSLocale currentLocale]];
}

- (BOOL)dateIsDisabled:(NSDate *)date {
    for (NSDate *disabledDate in self.disabledDates) {
        if ([disabledDate isEqualToDate:date]) {
            return YES;
        }
    }
    return NO;
}

#pragma mark -
#pragma mark - CKCalendarDelegate

- (void)calendar:(CKCalendarView *)calendar configureDateItem:(CKDateItem *)dateItem forDate:(NSDate *)date {
    // TODO: play with the coloring if we want to...
    if ([self dateIsDisabled:date]) {
        dateItem.backgroundColor = [UIColor redColor];
        dateItem.textColor = [UIColor whiteColor];
    }
}

- (BOOL)calendar:(CKCalendarView *)calendar willSelectDate:(NSDate *)date {
    return ![self dateIsDisabled:date];
}

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date {
    self.dateLabel.text = [self.dateFormatter stringFromDate:date];
}

- (BOOL)calendar:(CKCalendarView *)calendar willChangeToMonth:(NSDate *)date {
    if ([date laterDate:self.minimumDate] == date) {
        self.calendar.backgroundColor = [UIColor blueColor];
        return YES;
    } else {
        self.calendar.backgroundColor = [UIColor redColor];
        return NO;
    }
}

- (void)calendar:(CKCalendarView *)calendar didLayoutInRect:(CGRect)frame {
    NSLog(@"calendar layout: %@", NSStringFromCGRect(frame));
}


#pragma mark - ButtonActions


-(void) Schedule_btnClick:(UIButton *) sender
{
    
}
-(void) onRequest_btnClick:(UIButton *) sender
{
    
}
-(void) icon_right_arrow_BtnClick:(UIButton *) sender
{
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end

