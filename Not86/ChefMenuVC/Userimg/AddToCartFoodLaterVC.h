//
//  AddToCartFoodLaterVC.h
//  Not86
//
//  Created by Admin on 30/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CKCalendarView.h"

@interface AddToCartFoodLaterVC : UIViewController
{
    
}

@property(nonatomic,strong)NSMutableArray*ary_details;
@property(nonatomic, strong) CKCalendarView *calendar;
@property(nonatomic, strong) NSMutableArray *enabledDates;
@property(nonatomic,strong) NSString*str_comefrom;


@end
