//
//  MoreFiltersInFoodForNowVC.m
//  Not86
//
//  Created by Admin on 30/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "MoreFiltersInFoodForNowVC.h"
//#import "LocationVC.h"
#import "Define.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"


@interface MoreFiltersInFoodForNowVC ()<UITextFieldDelegate,UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDataSource,UITableViewDelegate>

{
    CGFloat	animatedDistance;
    UIScrollView *scroll;
    UIImageView *img_header;
    UIView *view_serch_for_food_later;
    
    UIImageView *img_strip1;
    UIImageView *img_strip2;
    
    UIView * view_for_my_favorites;
    UIView *  view_for_all;
    
    //for Favorites
    UICollectionViewFlowLayout *layout;
    UICollectionView * collView_dish_types;
    UICollectionViewFlowLayout * layout2;
    UICollectionView * collView_for_course;
    UICollectionViewFlowLayout * layout3;
    UICollectionView * collView_for_dietary;
    
    NSMutableArray *array_imges;
    NSMutableArray *array_dish_names;
    NSMutableArray * array_course_images;
    NSMutableArray * array_course_names;
    NSMutableArray * array_dietary_images;
    NSMutableArray * array_names_in_dietary_table;
    
    UITableView * table_for_dietary;
    
    //for All
    UICollectionViewFlowLayout *layout4;
    UICollectionView * collView_dish_types2;
    UICollectionViewFlowLayout * layout5;
    UICollectionView * collView_for_course2;
    UICollectionViewFlowLayout * layout6;
    UICollectionView * collView_for_dietary2;
    
    NSMutableArray *array_imges_in_all;
    NSMutableArray *array_dish_names_in_all;
    NSMutableArray * array_course_images_in_all;
    NSMutableArray * array_dietary_images_in_all;
    NSMutableArray * array_names_in_dietary_table_in_all;
    
    UITableView *  table_for_dietary_in_all;
    
    
    
    
    
}

@end

@implementation MoreFiltersInFoodForNowVC
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateBodyDesign];
    [self integrateHeader];
    //array declaration in favorites
    array_imges = [[NSMutableArray alloc]initWithObjects:@"b-chines@2x.png",@"b-french@2x.png",@"b-japanes@2x.png",@"b-italian@2x.png", nil];
    array_dish_names = [[NSMutableArray alloc]initWithObjects:@"Chinese",@"French",@"Japanese",@"Italian", nil];
    
    array_course_images = [[NSMutableArray alloc]initWithObjects:@"b-breakfast@2x.png",@"main-couse-img@2x.png",@"b-dessert@2x.png",nil];
    
    array_course_names = [[NSMutableArray alloc]initWithObjects:@"Brackfast",@"Main Course",@"Dessert",nil];
    
    array_dietary_images = [[NSMutableArray alloc]initWithObjects:@"img-halal@2x.png",@"img-koser@2x.png",@"img-non-veg@2x.png",@"img-veg@2x.png", nil];
    //names in table view in favorites
    array_names_in_dietary_table = [[NSMutableArray alloc]initWithObjects:@"Rating (High-Low)",@"Price (Low-High)",@"Price (High-Low)", nil];
    
    
    //array declaration in all
    
    array_imges_in_all = [[NSMutableArray alloc]initWithObjects:@"b-chines@2x.png",@"b-french@2x.png",@"b-japanes@2x.png",@"b-italian@2x.png", nil];
    array_dish_names_in_all = [[NSMutableArray alloc]initWithObjects:@"Chinese",@"French",@"Japanese",@"Italian", nil];
    
    array_course_images_in_all = [[NSMutableArray alloc]initWithObjects:@"b-breakfast@2x.png",@"main-couse-img@2x.png",@"dessert-b@2x.png",nil];
    
    array_dietary_images_in_all = [[NSMutableArray alloc]initWithObjects:@"halal-img@2x.png",@"koser-img@2x.png",@"non-veg-img@2x.png",@"veg-img@2x.png", nil];
    
    //names in table view in all
    array_names_in_dietary_table_in_all = [[NSMutableArray alloc]initWithObjects:@"Rating (High-Low)",@"Price (Low-High)",@"Price (High-Low)", nil];
    
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}
-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu.frame = CGRectMake(10,13,20,20);
    icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    [icon_menu addTarget:self action:@selector(btn_menu_click:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:icon_menu];
    
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 150, 45);
    lbl_User_Sign_Up.text = @"Food Now";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    UIButton *icon_drop_down = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_drop_down.frame = CGRectMake(CGRectGetMaxX(lbl_User_Sign_Up.frame)-45,15,20,20);
    icon_drop_down .backgroundColor = [UIColor clearColor];
    [icon_drop_down addTarget:self action:@selector(btn_drop_down:) forControlEvents:UIControlEventTouchUpInside];
    [icon_drop_down setImage:[UIImage imageNamed:@"white-dropd-@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_drop_down];
    
    UIButton *btn_on_food_later_in_head = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_food_later_in_head.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0,150,45);
    btn_on_food_later_in_head .backgroundColor = [UIColor clearColor];
    [btn_on_food_later_in_head addTarget:self action:@selector(btn_on_food_later_label_click:) forControlEvents:UIControlEventTouchUpInside];
    //[icon_drop_down setImage:[UIImage imageNamed:@"drop-down@2x.png"] forState:UIControlStateNormal];
    [self.view   addSubview:btn_on_food_later_in_head];
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"logo@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}

-(void)integrateBodyDesign
{
    
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor clearColor];
    scroll.frame = CGRectMake(0, 46, WIDTH, HEIGHT-150);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    
    view_serch_for_food_later = [[UIView alloc]init];
    view_serch_for_food_later.frame=CGRectMake(0,0,WIDTH,1500);
    view_serch_for_food_later.backgroundColor=[UIColor yellowColor];
    [scroll  addSubview:  view_serch_for_food_later];
    
    UILabel *lbl_location = [[UILabel alloc]init];
    lbl_location.frame = CGRectMake(105,5, 150, 45);
    lbl_location.text = @"Location";
    lbl_location.font = [UIFont fontWithName:kFontBold size:18];
    lbl_location.textColor = [UIColor blackColor];
    lbl_location.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_location];
    
    UIImageView *img_pointer_location = [[UIImageView alloc]init];
    img_pointer_location.frame = CGRectMake(25,CGRectGetMaxY(lbl_location.frame), 30, 30);
    [img_pointer_location setImage:[UIImage imageNamed:@"location-pointer@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_pointer_location setUserInteractionEnabled:YES];
    [view_serch_for_food_later addSubview:img_pointer_location];
    
    
    //    UITextField *txt_set_location = [[UITextField alloc] init];
    //    txt_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+5,CGRectGetMaxY(lbl_location.frame), 250, 30);
    //    txt_set_location .borderStyle = UITextBorderStyleNone;
    //    txt_set_location .textColor = [UIColor grayColor];
    //    txt_set_location .font = [UIFont fontWithName:kFont size:15];
    //    txt_set_location .placeholder = @"Smith Street, 77 Block (5km)";
    //    [txt_set_location  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    //    [txt_set_location  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    //    UIView *padding2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    //    txt_set_location .leftView = padding2;
    //    txt_set_location .leftViewMode = UITextFieldViewModeAlways;
    //    txt_set_location .userInteractionEnabled=YES;
    //    txt_set_location .textAlignment = NSTextAlignmentLeft;
    //    txt_set_location .backgroundColor = [UIColor clearColor];
    //    txt_set_location .keyboardType = UIKeyboardTypeAlphabet;
    //    txt_set_location .delegate = self;
    //    [view_serch_for_food_later addSubview:txt_set_location ];
    
    UILabel *lbl_set_location = [[UILabel alloc]init];
    lbl_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+5,CGRectGetMaxY(lbl_location.frame), 250, 30);
    lbl_set_location.text = @"Smith Street, 77 Block (5km)";
    lbl_set_location.font = [UIFont fontWithName:kFont size:18];
    lbl_set_location.textColor = [UIColor blackColor];
    lbl_set_location.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_set_location];
    
    
    UIButton *btn_img_right = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_right.frame = CGRectMake(CGRectGetMidX(lbl_set_location.frame)+80,CGRectGetMaxY(lbl_location.frame),30,30);
    btn_img_right .backgroundColor = [UIColor clearColor];
    [btn_img_right addTarget:self action:@selector(btn_right_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_right setImage:[UIImage imageNamed:@"img-right@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_img_right];
    
    UIButton *btn_on_set_location = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_set_location.frame = CGRectMake(25,CGRectGetMaxY(lbl_location.frame),270,35);
    btn_on_set_location .backgroundColor = [UIColor clearColor];
    [btn_on_set_location addTarget:self action:@selector(btn_on_set_location_click:) forControlEvents:UIControlEventTouchUpInside];
    // [btn_set_date setImage:[UIImage imageNamed:@"menu-food-later@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_on_set_location];
    
    UILabel *lbl_dish_r_meal = [[UILabel alloc]init];
    // lbl_dish_r_meal.frame = CGRectMake(105,CGRectGetMaxY(txt_set_location.frame)+10, 150, 45);
    lbl_dish_r_meal.text = @"Dish/Meal";
    lbl_dish_r_meal.font = [UIFont fontWithName:kFontBold size:18];
    lbl_dish_r_meal.textColor = [UIColor blackColor];
    lbl_dish_r_meal.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_dish_r_meal];
    
    UIButton *btn_img_single_dish = [UIButton buttonWithType:UIButtonTypeCustom];
    //btn_img_single_dish.frame = CGRectMake(90,CGRectGetMaxY(lbl_dish_r_meal.frame),50,50);
    btn_img_single_dish .backgroundColor = [UIColor clearColor];
    [btn_img_single_dish addTarget:self action:@selector(btn_singel_dish_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_single_dish setImage:[UIImage imageNamed:@"single-dish-icon@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_img_single_dish];
    
    
    UIButton *btn_img_meal = [UIButton buttonWithType:UIButtonTypeCustom];
    //  btn_img_meal.frame = CGRectMake(CGRectGetMidX(btn_img_single_dish.frame)+65,CGRectGetMaxY(lbl_dish_r_meal.frame),50,50);
    btn_img_meal .backgroundColor = [UIColor clearColor];
    [btn_img_meal addTarget:self action:@selector(btn_meal_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_meal setImage:[UIImage imageNamed:@"iocn-meal@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_img_meal];
    
    UIImageView *img_red_tik1 = [[UIImageView alloc]init];
    //img_red_tik1.frame =  CGRectMake(CGRectGetMidX(btn_img_single_dish.frame)+135,CGRectGetMaxY(lbl_dish_r_meal.frame)+15,25,25);
    [img_red_tik1 setImage:[UIImage imageNamed:@"icon-red-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik1 setUserInteractionEnabled:YES];
    [view_serch_for_food_later addSubview:img_red_tik1];
    
    
    UILabel *lbl_seving_type = [[UILabel alloc]init];
    lbl_seving_type.frame = CGRectMake(105,CGRectGetMaxY(btn_img_single_dish.frame)+150, 150, 45);
    lbl_seving_type.text = @"Serving Type";
    lbl_seving_type.font = [UIFont fontWithName:kFontBold size:18];
    lbl_seving_type.textColor = [UIColor blackColor];
    lbl_seving_type.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_seving_type];
    
    
    UIButton *btn_img_dine_in = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_dine_in.frame = CGRectMake(45,CGRectGetMaxY(lbl_seving_type.frame),50,50);
    btn_img_dine_in .backgroundColor = [UIColor clearColor];
    [btn_img_dine_in addTarget:self action:@selector(btn_img_dine_in_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_dine_in setImage:[UIImage imageNamed:@"img-dine@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_img_dine_in];
    
    
    UIButton *btn_img_take_out = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_take_out.frame = CGRectMake(CGRectGetMidX(btn_img_dine_in.frame)+65,CGRectGetMaxY(lbl_seving_type.frame),50,50);
    btn_img_take_out .backgroundColor = [UIColor clearColor];
    [btn_img_take_out addTarget:self action:@selector(btn_img_take_out_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_take_out setImage:[UIImage imageNamed:@"img-take-out1@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_img_take_out];
    
    
    UIImageView *img_red_tik = [[UIImageView alloc]init];
    img_red_tik.frame =  CGRectMake(CGRectGetMidX(btn_img_dine_in.frame)+135,CGRectGetMaxY(lbl_seving_type.frame)+15,25,25);
    [img_red_tik setImage:[UIImage imageNamed:@"icon-red-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik setUserInteractionEnabled:YES];
    [view_serch_for_food_later addSubview:img_red_tik];
    
    
    //    UIButton *btn_red_tik = [UIButton buttonWithType:UIButtonTypeCustom];
    //    btn_red_tik.frame = CGRectMake(CGRectGetMidX(btn_img_dine_in.frame)+65,CGRectGetMaxY(lbl_seving_type.frame),50,50);
    //    btn_red_tik .backgroundColor = [UIColor clearColor];
    //    [btn_red_tik addTarget:self action:@selector(btn_img_take_out_click:) forControlEvents:UIControlEventTouchUpInside];
    //    [btn_red_tik setImage:[UIImage imageNamed:@" icon-red-tik@2x.png"] forState:UIControlStateNormal];
    //    [btn_img_take_out   addSubview:btn_red_tik];
    
    
    UIButton *btn_img_delivery = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_delivery.frame = CGRectMake(CGRectGetMidX(btn_img_take_out.frame)+65,CGRectGetMaxY(lbl_seving_type.frame),50,50);
    btn_img_delivery .backgroundColor = [UIColor clearColor];
    [btn_img_delivery addTarget:self action:@selector(btn_img_delivery_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_delivery setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [view_serch_for_food_later   addSubview:btn_img_delivery];
    
    UILabel *lbl_keywords = [[UILabel alloc]init];
    lbl_keywords.frame = CGRectMake(130,CGRectGetMaxY(btn_img_take_out.frame)+5, 150, 45);
    lbl_keywords.text = @"Keywords";
    lbl_keywords.font = [UIFont fontWithName:kFontBold size:18];
    lbl_keywords.textColor = [UIColor blackColor];
    lbl_keywords.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_keywords];
    
    UIImageView *img_serch_bar = [[UIImageView alloc]init];
    img_serch_bar.frame = CGRectMake(25,CGRectGetMaxY(lbl_keywords.frame), 290, 30);
    [img_serch_bar setImage:[UIImage imageNamed:@"serch-bar@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_serch_bar setUserInteractionEnabled:YES];
    [view_serch_for_food_later addSubview:img_serch_bar];
    
    //    UILabel *lbl_search = [[UILabel alloc]init];
    //    lbl_search.frame = CGRectMake(10,-4, 200, 45);
    //    lbl_search.text = @"Search...";
    //    lbl_search.font = [UIFont fontWithName:kFontHelvetica size:15];
    //    lbl_search.textColor = [UIColor blackColor];
    //    lbl_search.backgroundColor = [UIColor clearColor];
    //    [img_serch_bar addSubview:lbl_search];
    
    UITextField *txt_search = [[UITextField alloc] init];
    //  txt_search.frame = CGRectMake(10,-4, 200, 45);
    txt_search .borderStyle = UITextBorderStyleNone;
    txt_search .textColor = [UIColor grayColor];
    txt_search .font = [UIFont fontWithName:kFont size:15];
    txt_search .placeholder = @"Search...";
    [txt_search  setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [txt_search  setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_search .leftView = padding3;
    txt_search .leftViewMode = UITextFieldViewModeAlways;
    txt_search .userInteractionEnabled=YES;
    txt_search .textAlignment = NSTextAlignmentLeft;
    txt_search .backgroundColor = [UIColor clearColor];
    txt_search .keyboardType = UIKeyboardTypeAlphabet;
    txt_search .delegate = self;
    [img_serch_bar addSubview:txt_search ];
    
    
    UIImageView *icon_search = [[UIImageView alloc]init];
    //   icon_search.frame = CGRectMake(255,8, 15, 15);
    [icon_search setImage:[UIImage imageNamed:@"serach1@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_search setUserInteractionEnabled:YES];
    [img_serch_bar addSubview:icon_search];
    
    UIButton *btn_on_search_bar = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_search_bar.frame = CGRectMake(240,0, 30, 30);
    btn_on_search_bar .backgroundColor = [UIColor clearColor];
    [btn_on_search_bar addTarget:self action:@selector(btn_on_search_bar_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_search_bar setImage:[UIImage imageNamed:@"img-delivery@2x.png"] forState:UIControlStateNormal];
    [img_serch_bar   addSubview:btn_on_search_bar];
    
    UILabel *lbl_cuisines = [[UILabel alloc]init];
    lbl_cuisines.frame = CGRectMake(130,CGRectGetMaxY(img_serch_bar.frame)+5, 150, 45);
    lbl_cuisines.text = @"Cuisines";
    lbl_cuisines.font = [UIFont fontWithName:kFontBold size:18];
    lbl_cuisines.textColor = [UIColor blackColor];
    lbl_cuisines.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_cuisines];
    
    UILabel *lbl_my_favorites = [[UILabel alloc]init];
    lbl_my_favorites.frame = CGRectMake(130,CGRectGetMaxY(img_serch_bar.frame)+5, 150, 45);
    lbl_my_favorites.text = @"My Favorites";
    lbl_my_favorites.font = [UIFont fontWithName:kFontBold size:18];
    lbl_my_favorites.textColor = [UIColor blackColor];
    lbl_my_favorites.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_my_favorites];
    
    UIButton *btn_my_favorites = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_my_favorites.frame = CGRectMake(130,CGRectGetMaxY(img_serch_bar.frame)+5, 150, 45);
    btn_my_favorites .backgroundColor = [UIColor clearColor];
    [btn_my_favorites addTarget:self action:@selector(btn_on_my_favorites_click:) forControlEvents:UIControlEventTouchUpInside];
    [scroll   addSubview:btn_my_favorites];
    
    img_strip1 = [[UIImageView alloc]init];
    img_strip1.frame = CGRectMake(200, CGRectGetMaxY(lbl_my_favorites.frame), 57, 2);
    [img_strip1 setImage:[UIImage imageNamed:@"black strip@2x.png"]];
    [img_strip1 setUserInteractionEnabled:YES];
    [scroll addSubview:img_strip1];
    
    
    UILabel *lbl_all = [[UILabel alloc]init];
    lbl_all.frame = CGRectMake(130,CGRectGetMaxY(img_serch_bar.frame)+5, 150, 45);
    lbl_all.text = @"All";
    lbl_all.font = [UIFont fontWithName:kFontBold size:18];
    lbl_all.textColor = [UIColor blackColor];
    lbl_all.backgroundColor = [UIColor clearColor];
    [view_serch_for_food_later addSubview:lbl_all];
    
    UIButton *btn_on_all = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_all.frame = CGRectMake(130,CGRectGetMaxY(img_serch_bar.frame)+5, 150, 45);
    btn_on_all .backgroundColor = [UIColor clearColor];
    [btn_on_all addTarget:self action:@selector(btn_all_click:) forControlEvents:UIControlEventTouchUpInside];
    [scroll   addSubview:btn_on_all];
    
    img_strip2 = [[UIImageView alloc]init];
    img_strip2.frame = CGRectMake(200, CGRectGetMaxY(lbl_my_favorites.frame), 57, 2);
    [img_strip2 setImage:[UIImage imageNamed:@"black strip@2x.png"]];
    [img_strip2 setUserInteractionEnabled:YES];
    [scroll addSubview:img_strip2];
    
    img_strip2.hidden = YES;
    
    
    //view for MY FAVORITES
    
    view_for_my_favorites = [[UIView alloc]init];
    view_for_my_favorites.frame=CGRectMake(0,CGRectGetMaxY(lbl_my_favorites.frame)+92,WIDTH,HEIGHT+90);
    view_for_my_favorites.backgroundColor=[UIColor clearColor];
    [view_for_my_favorites setUserInteractionEnabled:YES];
    [view_serch_for_food_later addSubview:  view_for_my_favorites];
    
    
    // first collection view for dish
    
    layout=[[UICollectionViewFlowLayout alloc] init];
    collView_dish_types = [[UICollectionView alloc] initWithFrame:CGRectMake(15,10,WIDTH-30,90)
                                             collectionViewLayout:layout];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [ collView_dish_types setDataSource:self];
    [ collView_dish_types setDelegate:self];
    collView_dish_types.scrollEnabled = YES;
    collView_dish_types.showsVerticalScrollIndicator = NO;
    collView_dish_types.showsHorizontalScrollIndicator = NO;
    collView_dish_types.pagingEnabled = NO;
    [ collView_dish_types registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [ collView_dish_types setBackgroundColor:[UIColor redColor]];
    layout.minimumInteritemSpacing = 2;
    layout.minimumLineSpacing = 0;
    collView_dish_types.userInteractionEnabled = YES;
    [view_for_my_favorites addSubview: collView_dish_types];
    
    UIImageView *img_left_arrow = [[UIImageView alloc]init];
    img_left_arrow.frame = CGRectMake(5,35,10,13);
    [img_left_arrow setImage:[UIImage imageNamed:@"left-arrow-img@2x.png"]];
    img_left_arrow.backgroundColor = [UIColor clearColor];
    [img_left_arrow setUserInteractionEnabled:YES];
    [view_for_my_favorites addSubview:img_left_arrow];
    
    UIButton *btn_on_left_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_left_arrow.frame = CGRectMake(0,10, 14,75);
    btn_on_left_arrow .backgroundColor = [UIColor clearColor];
    [btn_on_left_arrow setUserInteractionEnabled:YES];
    [btn_on_left_arrow addTarget:self action:@selector(btn_on_left_arrow_click:)forControlEvents:UIControlEventTouchUpInside];
    [view_for_my_favorites addSubview:btn_on_left_arrow];
    
    UIImageView *img_right_arrow = [[UIImageView alloc]init];
    img_right_arrow.frame = CGRectMake(CGRectGetMaxX(collView_dish_types.frame),35, 10,13);
    [img_right_arrow setImage:[UIImage imageNamed:@"right-arrow-img@2x.png"]];
    img_right_arrow.backgroundColor = [UIColor clearColor];
    [img_right_arrow setUserInteractionEnabled:YES];
    [view_for_my_favorites addSubview:img_right_arrow];
    
    UIButton *btn_on_right_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_right_arrow.frame = CGRectMake(CGRectGetMaxX(collView_dish_types.frame),10,14, 75);
    btn_on_right_arrow .backgroundColor = [UIColor clearColor];
    [btn_on_right_arrow setUserInteractionEnabled:YES];
    [btn_on_right_arrow addTarget:self action:@selector(btn_on_right_arrow_click:)forControlEvents:UIControlEventTouchUpInside];
    [view_for_my_favorites addSubview:btn_on_right_arrow];
    
    
    
    
    
    UILabel *lbl_course = [[UILabel alloc]init];
    lbl_course.frame = CGRectMake(150,CGRectGetMaxY(collView_dish_types.frame)-10, 150, 45);
    lbl_course.text = @"Course";
    lbl_course.font = [UIFont fontWithName:kFontBold size:18];
    lbl_course.textColor = [UIColor blackColor];
    lbl_course.backgroundColor = [UIColor clearColor];
    [view_for_my_favorites addSubview:lbl_course];
    
    // second collection view for curse
    
    layout2 = [[UICollectionViewFlowLayout alloc] init];
    collView_for_course = [[UICollectionView alloc] initWithFrame:CGRectMake(15,CGRectGetMaxY(lbl_course.frame)-10,WIDTH-30,70)
                                             collectionViewLayout:layout2];
    [layout2 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [ collView_for_course setDataSource:self];
    [ collView_for_course setDelegate:self];
    collView_for_course.scrollEnabled = YES;
    collView_for_course.showsVerticalScrollIndicator = NO;
    collView_for_course.showsHorizontalScrollIndicator = NO;
    collView_for_course.pagingEnabled = NO;
    [ collView_for_course registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [ collView_for_course setBackgroundColor:[UIColor redColor]];
    layout2.minimumInteritemSpacing = 22;
    layout2.minimumLineSpacing = 0;
    collView_for_course.userInteractionEnabled = YES;
    [view_for_my_favorites addSubview: collView_for_course];
    
    
    UIImageView *img_left_arrow2 = [[UIImageView alloc]init];
    img_left_arrow2.frame = CGRectMake(5,CGRectGetMaxY(lbl_course.frame)+5,10,13);
    [img_left_arrow2 setImage:[UIImage imageNamed:@"left-arrow-img@2x.png"]];
    img_left_arrow2.backgroundColor = [UIColor clearColor];
    [img_left_arrow2 setUserInteractionEnabled:YES];
    [view_for_my_favorites addSubview:img_left_arrow2];
    
    UIButton *btn_on_left_arrow2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_left_arrow2.frame = CGRectMake(0,CGRectGetMaxY(lbl_course.frame)-10, 14,60);
    btn_on_left_arrow2 .backgroundColor = [UIColor clearColor];
    [btn_on_left_arrow2 setUserInteractionEnabled:YES];
    [btn_on_left_arrow2 addTarget:self action:@selector(btn_on_left_arrow2_click:)forControlEvents:UIControlEventTouchUpInside];
    [view_for_my_favorites addSubview:btn_on_left_arrow2];
    
    UIImageView *img_right_arrow2 = [[UIImageView alloc]init];
    img_right_arrow2.frame = CGRectMake(CGRectGetMaxX(collView_for_course.frame),CGRectGetMaxY(lbl_course.frame)+5,10,13);
    [img_right_arrow2 setImage:[UIImage imageNamed:@"right-arrow-img@2x.png"]];
    img_right_arrow2.backgroundColor = [UIColor clearColor];
    [img_right_arrow2 setUserInteractionEnabled:YES];
    [view_for_my_favorites addSubview:img_right_arrow2];
    
    UIButton *btn_on_right_arrow2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_right_arrow2.frame = CGRectMake(CGRectGetMaxX(collView_dish_types.frame),CGRectGetMaxY(lbl_course.frame)-10,14, 60);
    btn_on_right_arrow2 .backgroundColor = [UIColor clearColor];
    [btn_on_right_arrow2 setUserInteractionEnabled:YES];
    [btn_on_right_arrow2 addTarget:self action:@selector(btn_on_right_arrow2_click:)forControlEvents:UIControlEventTouchUpInside];
    [view_for_my_favorites addSubview:btn_on_right_arrow2];
    
    
    
    UILabel *lbl_dietary_restrictions = [[UILabel alloc]init];
    lbl_dietary_restrictions.frame = CGRectMake(100,CGRectGetMaxY(collView_for_course.frame)-10, 200, 45);
    lbl_dietary_restrictions.text = @"Dietary Restrictions";
    lbl_dietary_restrictions.font = [UIFont fontWithName:kFontBold size:18];
    lbl_dietary_restrictions.textColor = [UIColor blackColor];
    lbl_dietary_restrictions.backgroundColor = [UIColor clearColor];
    [view_for_my_favorites addSubview:lbl_dietary_restrictions];
    
    // thired collection for dietary
    
    layout3 = [[UICollectionViewFlowLayout alloc] init];
    collView_for_dietary = [[UICollectionView alloc] initWithFrame:CGRectMake(15,CGRectGetMaxY(lbl_dietary_restrictions.frame)-10,WIDTH-29,65)
                                              collectionViewLayout:layout3];
    [layout3 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [ collView_for_dietary setDataSource:self];
    [ collView_for_dietary setDelegate:self];
    collView_for_dietary.scrollEnabled = YES;
    collView_for_dietary.showsVerticalScrollIndicator = NO;
    collView_for_dietary.showsHorizontalScrollIndicator = NO;
    collView_for_dietary.pagingEnabled = NO;
    [ collView_for_dietary registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [ collView_for_dietary setBackgroundColor:[UIColor redColor]];
    layout3.minimumInteritemSpacing = 12;
    layout3.minimumLineSpacing = 0;
    collView_for_dietary.userInteractionEnabled = YES;
    [view_for_my_favorites addSubview: collView_for_dietary];
    
    UIImageView *img_left_arrow3 = [[UIImageView alloc]init];
    img_left_arrow3.frame = CGRectMake(5,CGRectGetMaxY(lbl_dietary_restrictions.frame)+15,10,13);
    [img_left_arrow3 setImage:[UIImage imageNamed:@"left-arrow-img@2x.png"]];
    img_left_arrow3.backgroundColor = [UIColor clearColor];
    [img_left_arrow3 setUserInteractionEnabled:YES];
    [view_for_my_favorites addSubview:img_left_arrow3];
    
    UIButton *btn_on_left_arrow3 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_left_arrow3.frame = CGRectMake(0,CGRectGetMaxY(lbl_dietary_restrictions.frame)-10, 14,65);
    btn_on_left_arrow3 .backgroundColor = [UIColor clearColor];
    [btn_on_left_arrow3 setUserInteractionEnabled:YES];
    [btn_on_left_arrow3 addTarget:self action:@selector(btn_on_left_arrow3_click:)forControlEvents:UIControlEventTouchUpInside];
    [view_for_my_favorites addSubview:btn_on_left_arrow3];
    
    UIImageView *img_right_arrow3 = [[UIImageView alloc]init];
    img_right_arrow3.frame = CGRectMake(CGRectGetMaxX(collView_for_dietary.frame),CGRectGetMaxY(lbl_dietary_restrictions.frame)+15,10,13);
    [img_right_arrow3 setImage:[UIImage imageNamed:@"right-arrow-img@2x.png"]];
    img_right_arrow3.backgroundColor = [UIColor clearColor];
    [img_right_arrow3 setUserInteractionEnabled:YES];
    [view_for_my_favorites addSubview:img_right_arrow3];
    
    UIButton *btn_on_right_arrow3 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_right_arrow3.frame = CGRectMake(CGRectGetMaxX( collView_for_dietary.frame),CGRectGetMaxY(lbl_dietary_restrictions.frame)-10,14, 65);
    btn_on_right_arrow3 .backgroundColor = [UIColor clearColor];
    [btn_on_right_arrow3 setUserInteractionEnabled:YES];
    [btn_on_right_arrow3 addTarget:self action:@selector(btn_on_right_arrow3_click:)forControlEvents:UIControlEventTouchUpInside];
    [view_for_my_favorites addSubview:btn_on_right_arrow3];
    
    
    UILabel *lbl_sort_by = [[UILabel alloc]init];
    lbl_sort_by.frame = CGRectMake(150,CGRectGetMaxY(collView_for_dietary.frame)+5, 150, 45);
    lbl_sort_by.text = @"Sort By";
    lbl_sort_by.font = [UIFont fontWithName:kFont size:18];
    lbl_sort_by.textColor = [UIColor lightGrayColor];
    lbl_sort_by.backgroundColor = [UIColor clearColor];
    [view_for_my_favorites addSubview:lbl_sort_by];
    
    
    UIImageView *img_ditery_table = [[UIImageView alloc]init];
    img_ditery_table.frame = CGRectMake(50,CGRectGetMaxY(lbl_sort_by.frame)+10, 280, 25);
    [img_ditery_table setImage:[UIImage imageNamed:@"dietary-table-img@2x.png"]];
    img_ditery_table.backgroundColor = [UIColor clearColor];
    [img_ditery_table setUserInteractionEnabled:YES];
    [view_for_my_favorites addSubview:img_ditery_table];
    
    UILabel *lbl_distance = [[UILabel alloc]init];
    lbl_distance.frame = CGRectMake(4,0, 150, 25);
    lbl_distance.text = @"Distance";
    lbl_distance.font = [UIFont fontWithName:kFont size:14];
    [lbl_distance setUserInteractionEnabled:YES];
    lbl_distance.textColor = [UIColor lightGrayColor];
    lbl_distance.backgroundColor = [UIColor clearColor];
    [img_ditery_table addSubview:lbl_distance];
    
    UIButton *btn_on_dietary = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_dietary.frame = CGRectMake(0,0, 280, 25);
    btn_on_dietary .backgroundColor = [UIColor clearColor];
    [btn_on_dietary setUserInteractionEnabled:YES];
    [btn_on_dietary addTarget:self action:@selector(btn_on_dietary_click:)forControlEvents:UIControlEventTouchUpInside];
    [img_ditery_table addSubview:btn_on_dietary];
    
    
#pragma mark Tableview
    
    table_for_dietary = [[UITableView alloc] init ];
    table_for_dietary.frame  = CGRectMake(47,CGRectGetMaxY(img_ditery_table.frame)-14,283,200);
    [table_for_dietary setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_dietary.delegate = self;
    table_for_dietary.dataSource = self;
    table_for_dietary.showsVerticalScrollIndicator = NO;
    table_for_dietary.backgroundColor = [UIColor clearColor];
    [view_for_my_favorites addSubview: table_for_dietary ];
    
    //view for ALL
    
    view_for_all = [[UIView alloc]init];
    view_for_all.frame=CGRectMake(0,CGRectGetMaxY(lbl_my_favorites.frame)+92,WIDTH,300);
    view_for_all.backgroundColor=[UIColor blueColor];
    [view_serch_for_food_later addSubview:view_for_all];
    
    // first collection view for dish in all
    
    layout4=[[UICollectionViewFlowLayout alloc] init];
    collView_dish_types2 = [[UICollectionView alloc] initWithFrame:CGRectMake(15,10,WIDTH-30,90)
                                              collectionViewLayout:layout4];
    [layout4 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [ collView_dish_types2 setDataSource:self];
    [ collView_dish_types2 setDelegate:self];
    collView_dish_types2.scrollEnabled = YES;
    collView_dish_types2.showsVerticalScrollIndicator = NO;
    collView_dish_types2.showsHorizontalScrollIndicator = NO;
    collView_dish_types2.pagingEnabled = NO;
    [ collView_dish_types2 registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [ collView_dish_types2 setBackgroundColor:[UIColor greenColor]];
    layout4.minimumInteritemSpacing = 2;
    layout4.minimumLineSpacing = 0;
    collView_dish_types2.userInteractionEnabled = YES;
    [view_for_all addSubview: collView_dish_types2];
    
    UIImageView *img_left_arrow_in_all = [[UIImageView alloc]init];
    img_left_arrow_in_all.frame = CGRectMake(5,35,10,13);
    [img_left_arrow_in_all setImage:[UIImage imageNamed:@"left-arrow-img@2x.png"]];
    img_left_arrow_in_all.backgroundColor = [UIColor clearColor];
    [img_left_arrow_in_all setUserInteractionEnabled:YES];
    [view_for_all addSubview:img_left_arrow_in_all];
    
    UIButton *btn_on_left_arrow_in_all = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_left_arrow_in_all.frame = CGRectMake(0,10, 14,75);
    btn_on_left_arrow_in_all .backgroundColor = [UIColor clearColor];
    [btn_on_left_arrow_in_all setUserInteractionEnabled:YES];
    [btn_on_left_arrow_in_all addTarget:self action:@selector(btn_on_left_arrow_click:)forControlEvents:UIControlEventTouchUpInside];
    [view_for_all addSubview:btn_on_left_arrow_in_all];
    
    UIImageView *img_right_arrow_in_all = [[UIImageView alloc]init];
    img_right_arrow_in_all.frame = CGRectMake(CGRectGetMaxX(collView_dish_types2.frame),35, 10,13);
    [img_right_arrow_in_all setImage:[UIImage imageNamed:@"right-arrow-img@2x.png"]];
    img_right_arrow_in_all.backgroundColor = [UIColor clearColor];
    [img_right_arrow_in_all setUserInteractionEnabled:YES];
    [view_for_all addSubview:img_right_arrow_in_all];
    
    UIButton *btn_on_right_arrow_in_all = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_right_arrow_in_all.frame = CGRectMake(CGRectGetMaxX(collView_dish_types2.frame),10,14, 75);
    btn_on_right_arrow_in_all .backgroundColor = [UIColor clearColor];
    [btn_on_right_arrow_in_all setUserInteractionEnabled:YES];
    [btn_on_right_arrow_in_all addTarget:self action:@selector(btn_on_right_arrow_click:)forControlEvents:UIControlEventTouchUpInside];
    [view_for_all addSubview:btn_on_right_arrow_in_all];
    
    
    
    
    
    UILabel *lbl_course_in_all = [[UILabel alloc]init];
    lbl_course_in_all.frame = CGRectMake(150,CGRectGetMaxY(collView_dish_types2.frame)-10, 150, 45);
    lbl_course_in_all.text = @"Course";
    lbl_course_in_all.font = [UIFont fontWithName:kFontBold size:18];
    lbl_course_in_all.textColor = [UIColor blackColor];
    lbl_course_in_all.backgroundColor = [UIColor clearColor];
    [view_for_all addSubview:lbl_course_in_all];
    
    // second collection view for curse
    
    layout5 = [[UICollectionViewFlowLayout alloc] init];
    collView_for_course2 = [[UICollectionView alloc] initWithFrame:CGRectMake(15,CGRectGetMaxY(lbl_course_in_all.frame)-10,WIDTH-30,70)
                                              collectionViewLayout:layout5];
    [layout5 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [ collView_for_course2 setDataSource:self];
    [ collView_for_course2 setDelegate:self];
    collView_for_course2.scrollEnabled = YES;
    collView_for_course2.showsVerticalScrollIndicator = NO;
    collView_for_course2.showsHorizontalScrollIndicator = NO;
    collView_for_course2.pagingEnabled = NO;
    [ collView_for_course2 registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [ collView_for_course2 setBackgroundColor:[UIColor greenColor]];
    layout5.minimumInteritemSpacing = 22;
    layout5.minimumLineSpacing = 0;
    collView_for_course2.userInteractionEnabled = YES;
    [view_for_all addSubview: collView_for_course2];
    
    
    UIImageView *img_left_arrow2_in_all = [[UIImageView alloc]init];
    img_left_arrow2_in_all.frame = CGRectMake(5,CGRectGetMaxY(lbl_course_in_all.frame)+5,10,13);
    [img_left_arrow2_in_all setImage:[UIImage imageNamed:@"left-arrow-img@2x.png"]];
    img_left_arrow2_in_all.backgroundColor = [UIColor clearColor];
    [img_left_arrow2_in_all setUserInteractionEnabled:YES];
    [view_for_all addSubview:img_left_arrow2_in_all];
    
    UIButton *btn_on_left_arrow2_in_all = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_left_arrow2_in_all.frame = CGRectMake(0,CGRectGetMaxY(lbl_course_in_all.frame)-10, 14,60);
    btn_on_left_arrow2_in_all .backgroundColor = [UIColor clearColor];
    [btn_on_left_arrow2_in_all setUserInteractionEnabled:YES];
    [btn_on_left_arrow2_in_all addTarget:self action:@selector(btn_on_left_arrow2_click:)forControlEvents:UIControlEventTouchUpInside];
    [view_for_all addSubview:btn_on_left_arrow2_in_all];
    
    UIImageView *img_right_arrow2_in_all = [[UIImageView alloc]init];
    img_right_arrow2_in_all.frame = CGRectMake(CGRectGetMaxX(collView_for_course2.frame),CGRectGetMaxY(lbl_course_in_all.frame)+5,10,13);
    [img_right_arrow2_in_all setImage:[UIImage imageNamed:@"right-arrow-img@2x.png"]];
    img_right_arrow2_in_all.backgroundColor = [UIColor clearColor];
    [img_right_arrow2_in_all setUserInteractionEnabled:YES];
    [view_for_all addSubview:img_right_arrow2_in_all];
    
    UIButton *btn_on_right_arrow2_in_all = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_right_arrow2_in_all.frame = CGRectMake(CGRectGetMaxX(collView_dish_types2.frame),CGRectGetMaxY(lbl_course_in_all.frame)-10,14, 60);
    btn_on_right_arrow2_in_all .backgroundColor = [UIColor clearColor];
    [btn_on_right_arrow2_in_all setUserInteractionEnabled:YES];
    [btn_on_right_arrow2_in_all addTarget:self action:@selector(btn_on_right_arrow2_click:)forControlEvents:UIControlEventTouchUpInside];
    [view_for_all addSubview:btn_on_right_arrow2_in_all];
    
    
    
    UILabel *lbl_dietary_restrictions_in_all = [[UILabel alloc]init];
    lbl_dietary_restrictions_in_all.frame = CGRectMake(100,CGRectGetMaxY(collView_for_course2.frame)-10, 200, 45);
    lbl_dietary_restrictions_in_all.text = @"Dietary Restrictions";
    lbl_dietary_restrictions_in_all.font = [UIFont fontWithName:kFontBold size:18];
    lbl_dietary_restrictions_in_all.textColor = [UIColor blackColor];
    lbl_dietary_restrictions_in_all.backgroundColor = [UIColor clearColor];
    [view_for_all addSubview:lbl_dietary_restrictions_in_all];
    
    // thired collection for dietary
    
    layout6 = [[UICollectionViewFlowLayout alloc] init];
    collView_for_dietary2 = [[UICollectionView alloc] initWithFrame:CGRectMake(15,CGRectGetMaxY(lbl_dietary_restrictions_in_all.frame)-10,WIDTH-29,65)
                                               collectionViewLayout:layout6];
    [layout5 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [ collView_for_dietary2 setDataSource:self];
    [ collView_for_dietary2 setDelegate:self];
    collView_for_dietary2.scrollEnabled = YES;
    collView_for_dietary2.showsVerticalScrollIndicator = NO;
    collView_for_dietary2.showsHorizontalScrollIndicator = NO;
    collView_for_dietary2.pagingEnabled = NO;
    [ collView_for_dietary2 registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [ collView_for_dietary2 setBackgroundColor:[UIColor greenColor]];
    layout6.minimumInteritemSpacing = 12;
    layout6.minimumLineSpacing = 0;
    collView_for_dietary2.userInteractionEnabled = YES;
    [view_for_all addSubview: collView_for_dietary2];
    
    UIImageView *img_left_arrow3_in_all = [[UIImageView alloc]init];
    img_left_arrow3_in_all.frame = CGRectMake(5,CGRectGetMaxY(lbl_dietary_restrictions_in_all.frame)+15,10,13);
    [img_left_arrow3_in_all setImage:[UIImage imageNamed:@"left-arrow-img@2x.png"]];
    img_left_arrow3_in_all.backgroundColor = [UIColor clearColor];
    [img_left_arrow3_in_all setUserInteractionEnabled:YES];
    [view_for_all addSubview:img_left_arrow3_in_all];
    
    UIButton *btn_on_left_arrow3_in_all = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_left_arrow3_in_all.frame = CGRectMake(0,CGRectGetMaxY(lbl_dietary_restrictions_in_all.frame)-10, 14,65);
    btn_on_left_arrow3_in_all .backgroundColor = [UIColor clearColor];
    [btn_on_left_arrow3_in_all setUserInteractionEnabled:YES];
    [btn_on_left_arrow3_in_all addTarget:self action:@selector(btn_on_left_arrow3_click:)forControlEvents:UIControlEventTouchUpInside];
    [view_for_all addSubview:btn_on_left_arrow3_in_all];
    
    UIImageView *img_right_arrow3_in_all = [[UIImageView alloc]init];
    img_right_arrow3_in_all.frame = CGRectMake(CGRectGetMaxX(collView_for_dietary2.frame),CGRectGetMaxY(lbl_dietary_restrictions_in_all.frame)+15,10,13);
    [img_right_arrow3_in_all setImage:[UIImage imageNamed:@"right-arrow-img@2x.png"]];
    img_right_arrow3_in_all.backgroundColor = [UIColor clearColor];
    [img_right_arrow3_in_all setUserInteractionEnabled:YES];
    [view_for_all addSubview:img_right_arrow3_in_all];
    
    UIButton *btn_on_right_arrow3_in_all = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_right_arrow3_in_all.frame = CGRectMake(CGRectGetMaxX( collView_for_dietary2.frame),CGRectGetMaxY(lbl_dietary_restrictions_in_all.frame)-10,14, 65);
    btn_on_right_arrow3_in_all .backgroundColor = [UIColor clearColor];
    [btn_on_right_arrow3_in_all setUserInteractionEnabled:YES];
    [btn_on_right_arrow3_in_all addTarget:self action:@selector(btn_on_right_arrow3_click:)forControlEvents:UIControlEventTouchUpInside];
    [view_for_all addSubview:btn_on_right_arrow3_in_all];
    
    
    UILabel *lbl_sort_by_in_all = [[UILabel alloc]init];
    lbl_sort_by_in_all.frame = CGRectMake(150,CGRectGetMaxY(collView_for_dietary2.frame)+5, 150, 45);
    lbl_sort_by_in_all.text = @"Sort By";
    lbl_sort_by_in_all.font = [UIFont fontWithName:kFont size:18];
    lbl_sort_by_in_all.textColor = [UIColor lightGrayColor];
    lbl_sort_by_in_all.backgroundColor = [UIColor clearColor];
    [view_for_all addSubview:lbl_sort_by_in_all];
    
    
    UIImageView *img_ditery_table_in_all = [[UIImageView alloc]init];
    img_ditery_table_in_all.frame = CGRectMake(50,CGRectGetMaxY(lbl_sort_by_in_all.frame)+10, 280, 25);
    [img_ditery_table_in_all setImage:[UIImage imageNamed:@"img-dietary-table@2x.png"]];
    img_ditery_table_in_all.backgroundColor = [UIColor clearColor];
    [img_ditery_table_in_all setUserInteractionEnabled:YES];
    [view_for_all addSubview:img_ditery_table_in_all];
    
    UILabel *lbl_distance_in_all = [[UILabel alloc]init];
    lbl_distance_in_all.frame = CGRectMake(4,0, 150, 25);
    lbl_distance_in_all.text = @"Distance";
    lbl_distance_in_all.font = [UIFont fontWithName:kFont size:14];
    [lbl_distance_in_all setUserInteractionEnabled:YES];
    lbl_distance_in_all.textColor = [UIColor lightGrayColor];
    lbl_distance_in_all.backgroundColor = [UIColor clearColor];
    [img_ditery_table_in_all addSubview:lbl_distance_in_all];
    
    UIButton *btn_on_dietary_in_all = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_dietary_in_all.frame = CGRectMake(0,0, 280, 25);
    btn_on_dietary_in_all .backgroundColor = [UIColor clearColor];
    [btn_on_dietary_in_all setUserInteractionEnabled:YES];
    [btn_on_dietary_in_all addTarget:self action:@selector(btn_on_dietary_click:)forControlEvents:UIControlEventTouchUpInside];
    [img_ditery_table_in_all addSubview:btn_on_dietary_in_all];
    
    
#pragma mark Tableview
    
    table_for_dietary_in_all = [[UITableView alloc] init ];
    table_for_dietary_in_all.frame  = CGRectMake(47,CGRectGetMaxY(img_ditery_table_in_all.frame)-14,283,200);
    [table_for_dietary_in_all setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_dietary_in_all.delegate = self;
    table_for_dietary_in_all.dataSource = self;
    table_for_dietary_in_all.showsVerticalScrollIndicator = NO;
    table_for_dietary_in_all.backgroundColor = [UIColor clearColor];
    [view_for_all addSubview:  table_for_dietary_in_all ];
    
    
    UIImageView *img_filter = [[UIImageView alloc]init];
    img_filter.frame = CGRectMake(40,HEIGHT-48, 308, 45);
    [img_filter setImage:[UIImage imageNamed:@"button-filter-img@2x.png"]];
    img_filter.backgroundColor = [UIColor clearColor];
    [img_filter setUserInteractionEnabled:YES];
    [self.view addSubview:img_filter];
    
    UILabel *lbl_filter = [[UILabel alloc]init];
    lbl_filter.frame = CGRectMake(130,13, 150, 25);
    lbl_filter.text = @"FILTER";
    lbl_filter.font = [UIFont fontWithName:kFont size:20];
    lbl_filter.textColor = [UIColor whiteColor];
    lbl_filter.backgroundColor = [UIColor clearColor];
    [img_filter addSubview:lbl_filter];
    
    UIButton *btn_on_filter = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_filter.frame = CGRectMake(40,HEIGHT-48, 308, 45);
    btn_on_filter .backgroundColor = [UIColor clearColor];
    [btn_on_filter addTarget:self action:@selector(btn_on_filter_click:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn_on_filter];
    
    
    
    
    
    if (IS_IPHONE_6Plus)
    {
        scroll.frame = CGRectMake(0, 46, WIDTH, HEIGHT+150);
        view_serch_for_food_later.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame)-50,WIDTH,HEIGHT+150);
        lbl_location.frame = CGRectMake(145,5, 150, 45);
        img_pointer_location.frame = CGRectMake(45,CGRectGetMaxY(lbl_location.frame), 30, 30);
        //txt_set_location .frame = CGRectMake(CGRectGetMaxX(img_pointer_location.frame)+20,CGRectGetMaxY(lbl_location.frame), 250, 30);
        lbl_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+5,CGRectGetMaxY(lbl_location.frame), 250, 30);
        
        btn_img_right.frame = CGRectMake(CGRectGetMidX(lbl_set_location.frame)+130,CGRectGetMaxY(lbl_location.frame),30,30);
        btn_on_set_location.frame = CGRectMake(25,CGRectGetMaxY(lbl_location.frame),270,35);
        
        lbl_dish_r_meal.frame = CGRectMake(150,CGRectGetMaxY(lbl_set_location.frame)+10, 150, 45);
        btn_img_single_dish.frame = CGRectMake(120,CGRectGetMaxY(lbl_dish_r_meal.frame),50,50);
        btn_img_meal.frame = CGRectMake(CGRectGetMidX(btn_img_single_dish.frame)+65,CGRectGetMaxY(lbl_dish_r_meal.frame),45,50);
        img_red_tik1.frame =  CGRectMake(CGRectGetMidX(btn_img_single_dish.frame)+110,CGRectGetMaxY(lbl_dish_r_meal.frame)+40,15,10);
        
        
        lbl_seving_type.frame = CGRectMake(150,CGRectGetMaxY(btn_img_single_dish.frame)+10, 150, 45);
        btn_img_dine_in.frame = CGRectMake(95,CGRectGetMaxY(lbl_seving_type.frame),50,50);
        btn_img_take_out.frame = CGRectMake(CGRectGetMidX(btn_img_dine_in.frame)+65,CGRectGetMaxY(lbl_seving_type.frame),50,50);
        img_red_tik.frame =  CGRectMake(CGRectGetMidX(btn_img_dine_in.frame)+90,CGRectGetMaxY(lbl_seving_type.frame)+23,25,25);
        btn_img_delivery.frame = CGRectMake(CGRectGetMidX(btn_img_take_out.frame)+65,CGRectGetMaxY(lbl_seving_type.frame),50,50);
        lbl_keywords.frame = CGRectMake(170,CGRectGetMaxY(btn_img_take_out.frame)+5, 150, 45);
        img_serch_bar.frame = CGRectMake(30,CGRectGetMaxY(lbl_keywords.frame), WIDTH-60, 30);
        txt_search.frame = CGRectMake(0,-4, 200, 45);
        icon_search.frame = CGRectMake(330,8, 15, 15);
        btn_on_search_bar.frame = CGRectMake(330,0, 30, 30);
        
        lbl_cuisines.frame = CGRectMake(170,CGRectGetMaxY(img_serch_bar.frame)+5, 150, 45);
        lbl_my_favorites.frame = CGRectMake(130,CGRectGetMaxY(img_serch_bar.frame)+5, 150, 45);
        btn_my_favorites.frame = CGRectMake(130,CGRectGetMaxY(img_serch_bar.frame)+5, 150, 25);
        img_strip1.frame = CGRectMake(200, CGRectGetMaxY(lbl_my_favorites.frame), 57, 2);
        lbl_all.frame = CGRectMake(130,CGRectGetMaxY(img_serch_bar.frame)+5, 150, 45);
        btn_on_all.frame = CGRectMake(130,CGRectGetMaxY(img_serch_bar.frame)+5, 150, 25);
        img_strip2.frame = CGRectMake(200, CGRectGetMaxY(lbl_my_favorites.frame), 57, 2);
        
    }
    else if (IS_IPHONE_6)
    {
        scroll.frame = CGRectMake(0, 46, WIDTH, HEIGHT+150);
        view_serch_for_food_later.frame=CGRectMake(0,0,WIDTH,HEIGHT+150);
        
        lbl_location.frame = CGRectMake(145,5, 150, 45);
        img_pointer_location.frame = CGRectMake(30,CGRectGetMaxY(lbl_location.frame), 30, 30);
        // txt_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+20,CGRectGetMaxY(lbl_location.frame), 250, 30);
        lbl_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+5,CGRectGetMaxY(lbl_location.frame), 250, 30);
        
        btn_img_right.frame = CGRectMake(CGRectGetMidX(lbl_set_location.frame)+130,CGRectGetMaxY(lbl_location.frame),30,30);
        btn_on_set_location.frame = CGRectMake(25,CGRectGetMaxY(lbl_location.frame),270,35);
        lbl_dish_r_meal.frame = CGRectMake(140,CGRectGetMaxY(lbl_set_location.frame)+10, 150, 45);
        btn_img_single_dish.frame = CGRectMake(110,CGRectGetMaxY(lbl_dish_r_meal.frame),55,50);
        btn_img_meal.frame = CGRectMake(CGRectGetMidX(btn_img_single_dish.frame)+75,CGRectGetMaxY(lbl_dish_r_meal.frame),40,50);
        img_red_tik1.frame =  CGRectMake(CGRectGetMidX(btn_img_single_dish.frame)+110,CGRectGetMaxY(lbl_dish_r_meal.frame)+40,15,10);
        
        lbl_seving_type.frame = CGRectMake(135,CGRectGetMaxY(btn_img_single_dish.frame)+10, 150, 45);
        
        btn_img_dine_in.frame = CGRectMake(55,CGRectGetMaxY(lbl_seving_type.frame),50,50);
        btn_img_take_out.frame = CGRectMake(CGRectGetMidX(btn_img_dine_in.frame)+85,CGRectGetMaxY(lbl_seving_type.frame),50,50);
        img_red_tik.frame =  CGRectMake(CGRectGetMidX(btn_img_dine_in.frame)+110,CGRectGetMaxY(lbl_seving_type.frame)+18,25,25);
        btn_img_delivery.frame = CGRectMake(CGRectGetMidX(btn_img_take_out.frame)+75,CGRectGetMaxY(lbl_seving_type.frame),50,50);
        lbl_keywords.frame = CGRectMake(155,CGRectGetMaxY(btn_img_take_out.frame)+5, 150, 45);
        img_serch_bar.frame = CGRectMake(30,CGRectGetMaxY(lbl_keywords.frame), WIDTH-60, 30);
        txt_search.frame = CGRectMake(0,-4, 200, 45);
        icon_search.frame = CGRectMake(290,8, 15, 15);
        btn_on_search_bar.frame = CGRectMake(290,0, 30, 30);
        
        lbl_cuisines.frame = CGRectMake(155,CGRectGetMaxY(img_serch_bar.frame)+5, 150, 45);
        lbl_my_favorites.frame = CGRectMake(30,CGRectGetMaxY(lbl_cuisines.frame)-10, 150, 45);
        btn_my_favorites.frame = CGRectMake(30,CGRectGetMaxY(lbl_cuisines.frame)+5, 120, 35);
        img_strip1.frame = CGRectMake(30, CGRectGetMaxY(lbl_cuisines.frame)+29, 107, 2);
        lbl_all.frame = CGRectMake(280,CGRectGetMaxY( lbl_cuisines.frame)-10, 80, 45);
        btn_on_all.frame = CGRectMake(270,CGRectGetMaxY( lbl_cuisines.frame)+5, 80, 35);
        img_strip2.frame = CGRectMake(280, CGRectGetMaxY( lbl_cuisines.frame)+29, 37, 2);
        
    }
    else
    {
        scroll.frame = CGRectMake(0, 46, WIDTH, HEIGHT+150);
        view_serch_for_food_later.frame=CGRectMake(0,CGRectGetMaxY(img_header.frame)-45,WIDTH,HEIGHT+350);
        
        lbl_location.frame = CGRectMake(120,5, 150, 45);
        img_pointer_location.frame = CGRectMake(25,CGRectGetMaxY(lbl_location.frame), 30, 30);
        //txt_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+5,CGRectGetMaxY(lbl_location.frame), 250, 30);
        lbl_set_location .frame = CGRectMake(CGRectGetMaxX( img_pointer_location.frame)+5,CGRectGetMaxY(lbl_location.frame), 250, 30);
        lbl_set_location.font = [UIFont fontWithName:kFont size:15];
        
        btn_img_right.frame = CGRectMake(CGRectGetMidX(lbl_set_location.frame)+80,CGRectGetMaxY(lbl_location.frame),30,30);
        btn_on_set_location.frame = CGRectMake(25,CGRectGetMaxY(lbl_location.frame),270,35);
        lbl_dish_r_meal.frame = CGRectMake(110,CGRectGetMaxY(lbl_set_location.frame)+10, 150, 45);
        btn_img_single_dish.frame = CGRectMake(90,CGRectGetMaxY(lbl_dish_r_meal.frame),50,50);
        btn_img_meal.frame = CGRectMake(CGRectGetMidX(btn_img_single_dish.frame)+65,CGRectGetMaxY(lbl_dish_r_meal.frame),45,50);
        img_red_tik1.frame =  CGRectMake(CGRectGetMidX(btn_img_single_dish.frame)+110,CGRectGetMaxY(lbl_dish_r_meal.frame)+40,15,10);
        
        lbl_seving_type.frame = CGRectMake(105,CGRectGetMaxY(btn_img_single_dish.frame)+10, 150, 45);
        
        btn_img_dine_in.frame = CGRectMake(45,CGRectGetMaxY(lbl_seving_type.frame),50,50);
        btn_img_take_out.frame = CGRectMake(CGRectGetMidX(btn_img_dine_in.frame)+65,CGRectGetMaxY(lbl_seving_type.frame),50,50);
        img_red_tik.frame =  CGRectMake(CGRectGetMidX(btn_img_dine_in.frame)+90,CGRectGetMaxY(lbl_seving_type.frame)+18,25,25);
        btn_img_delivery.frame = CGRectMake(CGRectGetMidX(btn_img_take_out.frame)+65,CGRectGetMaxY(lbl_seving_type.frame),50,50);
        lbl_keywords.frame = CGRectMake(130,CGRectGetMaxY(btn_img_take_out.frame)+5, 150, 45);
        img_serch_bar.frame = CGRectMake(25,CGRectGetMaxY(lbl_keywords.frame), 290, 30);
        txt_search.frame = CGRectMake(10,-4, 200, 45);
        icon_search.frame = CGRectMake(255,8, 15, 15);
        btn_on_search_bar.frame = CGRectMake(255,0, 30, 30);
        
        lbl_cuisines.frame = CGRectMake(130,CGRectGetMaxY(img_serch_bar.frame)+5, 150, 45);
        lbl_my_favorites.frame = CGRectMake(130,CGRectGetMaxY(img_serch_bar.frame)+5, 150, 45);
        btn_my_favorites.frame = CGRectMake(130,CGRectGetMaxY(img_serch_bar.frame)+5, 150, 45);
        img_strip1.frame = CGRectMake(200, CGRectGetMaxY(lbl_my_favorites.frame), 57, 2);
        lbl_all.frame = CGRectMake(130,CGRectGetMaxY(img_serch_bar.frame)+5, 150, 45);
        btn_on_all.frame = CGRectMake(130,CGRectGetMaxY(img_serch_bar.frame)+5, 150, 45);
        img_strip2.frame = CGRectMake(200, CGRectGetMaxY(lbl_my_favorites.frame), 57, 2);
        
    }
    
    //    view_for_reviews.hidden = YES;
    //    view_for_chef.hidden = NO;
    
    
    [scroll setContentSize:CGSizeMake(0,1200)];
}

#pragma table view delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == table_for_dietary)
    {
        return [array_names_in_dietary_table count];
    }
    else
    {
        return [array_names_in_dietary_table_in_all count];
    }
    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    if (tableView == table_for_dietary)
    {
        return 1;
    }
    else
    {
        return 1;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table_for_dietary)
    {
        return 20;
    }
    else
    {
        return 20;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews)
    {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    
    
    if (tableView == table_for_dietary)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-10, 170);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-20, 25);
            
        }
        else
        {    img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-20,170);
            
        }
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UILabel *names_in_table = [[UILabel alloc]init];
        names_in_table.frame = CGRectMake(7,5,200, 15);
        names_in_table.text = [NSString stringWithFormat:@"%@",[array_names_in_dietary_table objectAtIndex:indexPath.row]];
        names_in_table.font = [UIFont fontWithName:kFont size:12];
        names_in_table.textColor = [UIColor blackColor];
        names_in_table.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:names_in_table];
    }
    else
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-10, 170);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-20, 25);
            
        }
        else
        {    img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-20,170);
            
        }
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UILabel *names_in_table = [[UILabel alloc]init];
        names_in_table.frame = CGRectMake(7,5,200, 15);
        names_in_table.text = [NSString stringWithFormat:@"%@",[array_names_in_dietary_table_in_all objectAtIndex:indexPath.row]];
        names_in_table.font = [UIFont fontWithName:kFont size:12];
        names_in_table.textColor = [UIColor blackColor];
        names_in_table.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:names_in_table];
    }
    
    
    
    
    
    return  cell;
    
}

#pragma mark UiCollectionView Delegate Methods for dish

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    
    
    
    if (collectionView == collView_dish_types)
    {
        return [array_imges count];
        
    }
    else if (collectionView == collView_for_course)
    {
        return [array_course_images count];
    }
    else if (collectionView == collView_for_dietary)
    {
        return [array_dietary_images count];
    }
    
    else if (collectionView == collView_dish_types2)
    {
        return [array_imges_in_all count];
        
    }
    else if (collectionView == collView_for_course2)
    {
        return [array_course_images_in_all count];
    }
    else if (collectionView == collView_for_dietary2)
    {
        return [array_dietary_images_in_all count];
    }
    
    
    
    return 4;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView1 dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    
    
    UIImageView *img_bg_on_dish_cell = [[UIImageView alloc]init];
    img_bg_on_dish_cell.frame =  CGRectMake(9, 0, 80, 90);
    img_bg_on_dish_cell.backgroundColor = [UIColor purpleColor];
    [cell.contentView addSubview:img_bg_on_dish_cell];
    
    UIImageView *dish_type_images = [[UIImageView alloc]init];
    
    UILabel *lbl_dish_names = [[UILabel alloc]init];
    lbl_dish_names.font = [UIFont fontWithName:kFont size:10];
    lbl_dish_names.textColor = [UIColor lightGrayColor];
    lbl_dish_names.backgroundColor = [UIColor clearColor];
    [img_bg_on_dish_cell addSubview:lbl_dish_names];
    
    
    
    UILabel *lbl_coures_names = [[UILabel alloc]init];
    lbl_coures_names.font = [UIFont fontWithName:kFont size:9];
    lbl_coures_names.textColor = [UIColor lightGrayColor];
    lbl_coures_names.backgroundColor = [UIColor clearColor];
    [img_bg_on_dish_cell addSubview:lbl_coures_names];
    
    
    
    
    
    
    if (collectionView1 == collView_dish_types)
    {
        dish_type_images.frame=CGRectMake(10, 8, 65,65);
        [dish_type_images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_imges objectAtIndex:indexPath.row]]]];
        
        lbl_coures_names.frame = CGRectMake(22,74, 150, 15);
        lbl_coures_names.text = [NSString stringWithFormat:@"%@",[ array_dish_names objectAtIndex:indexPath.row]];
        
    }
    else if (collectionView1 == collView_for_course)
    {
        dish_type_images.frame=CGRectMake(25, 15, 35,30);
        [dish_type_images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_course_images objectAtIndex:indexPath.row]]]];
        
        lbl_coures_names.frame = CGRectMake(22,50, 100, 15);
        lbl_coures_names.text = [NSString stringWithFormat:@"%@",[ array_course_names objectAtIndex:indexPath.row]];
        
    }
    else if (collectionView1 == collView_for_dietary)
    {
        dish_type_images.frame=CGRectMake(13, 5, 50,55);
        [dish_type_images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_dietary_images objectAtIndex:indexPath.row]]]];
    }
    
    else if (collectionView1 == collView_dish_types2)
    {
        dish_type_images.frame=CGRectMake(10, 8, 65,65);
        [dish_type_images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_imges_in_all objectAtIndex:indexPath.row]]]];
        
        lbl_coures_names.frame = CGRectMake(22,74, 150, 15);
        lbl_coures_names.text = [NSString stringWithFormat:@"%@",[ array_dish_names_in_all objectAtIndex:indexPath.row]];
        
    }
    else if (collectionView1 == collView_for_course2)
    {
        dish_type_images.frame=CGRectMake(25, 15, 35,30);
        [dish_type_images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_course_images_in_all objectAtIndex:indexPath.row]]
                                    ]];
        
        lbl_coures_names.frame = CGRectMake(22,50, 100, 15);
        lbl_coures_names.text = [NSString stringWithFormat:@"%@",[ array_course_names objectAtIndex:indexPath.row]];
        
        
    }
    else if (collectionView1 == collView_for_dietary2)
    {
        dish_type_images.frame=CGRectMake(13, 5, 50,55);
        [dish_type_images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_dietary_images_in_all objectAtIndex:indexPath.row]]]];
    }
    
    
    
    [dish_type_images setUserInteractionEnabled:YES];
    //    [img_Images setContentMode:UIViewContentModeScaleAspectFill];
    //    [img_Images setClipsToBounds:YES];
    [dish_type_images setUserInteractionEnabled:YES];
    [img_bg_on_dish_cell addSubview:dish_type_images];
    
    
    
    
    
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == collView_dish_types)
    {
        return CGSizeMake((WIDTH-33)/4, 88);
    }
    else if (collectionView == collView_for_course)
    {
        return CGSizeMake((WIDTH)/3, 80);
        
    }
    else if (collectionView == collView_for_dietary)
    {
        return CGSizeMake((WIDTH-68)/4, 90);
    }
    else if (collectionView == collView_dish_types2)
    {
        return CGSizeMake((WIDTH-33)/4, 88);
        
    }
    else if (collectionView == collView_for_course2)
    {
        return CGSizeMake((WIDTH)/3, 80);
    }
    else if (collectionView == collView_for_dietary2)
    {
        return CGSizeMake((WIDTH-68)/4, 90);
    }
    return CGSizeMake((WIDTH-56)/4, 90);}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
}




#pragma scroll View Delegates

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
}


-(void)btn_on_my_favorites_click:(UIButton *)sender
{
    NSLog(@"btn_on_my_favorites_click");
    img_strip1.hidden = NO;
    img_strip2.hidden = YES;
    
    view_for_my_favorites.hidden = NO;
    view_for_all.hidden = YES;
    
}
-(void)btn_all_click:(UIButton *)sender
{
    NSLog(@"btn_all_click");
    img_strip1.hidden = YES;
    img_strip2.hidden = NO;
    
    view_for_all.hidden = NO;
    view_for_my_favorites.hidden = YES;
    
    
    
}



#pragma mark Click Events
-(void)btn_menu_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click");
    [self dismissViewControllerAnimated:NO completion:nil];
//    [self.navigationController popViewControllerAnimated:NO];
}
-(void)btn_on_food_later_label_click:(UIButton *)sender
{
    NSLog(@"btn_on_food_later_label_click");
    
}
-(void)btn_drop_down:(UIButton *)sender
{
    NSLog(@"btn_on_food_later_label_click");
    
}
-(void)btn_set_date_click:(UIButton *)sender
{
    NSLog(@"img_fb_click Btn Click");
    
}
-(void)btn_set_time_click:(UIButton *)sender
{
    NSLog(@"btn_set_time_click:");
    
}
-(void)btn_right_click:(UIButton *)sender
{
    NSLog(@"btn_right_click:");
    
}
-(void)btn_on_set_location_click:(UIButton *)sender
{
    NSLog(@"btn_on_set_location_click::");
    //LocationVC*vc = [[LocationVC alloc]init];
    //[self.navigationController pushViewController:vc animated:NO];
    
    
}

-(void)btn_singel_dish_click:(UIButton *)sender
{
    NSLog(@"btn_singel_dish_click:");
    
}
-(void)btn_meal_click:(UIButton *)sender
{
    NSLog(@"btn_meal_click:");
    
}
-(void)btn_img_dine_in_click:(UIButton *)sender
{
    NSLog(@"btn_img_dine_in_click:");
    
}
-(void)btn_img_take_out_click:(UIButton *)sender
{
    NSLog(@"btn_img_take_out_click:");
    
}
-(void)btn_img_delivery_click:(UIButton *)sender
{
    NSLog(@"btn_img_delivery_click:");
    
}
-(void)btn_on_search_bar_click:(UIButton *)sender
{
    NSLog(@"btn_on_search_bar_click:");
    
}
-(void)btn_on_dietary_click:(UIButton *)sender
{
    NSLog(@"btn_on_dietary_click:");
    
}
-(void)btn_on_filter_click:(UIButton *)sender
{
    NSLog(@"btn_on_filter_click:");
    
}
-(void)btn_on_left_arrow_click:(UIButton *)sender
{
    NSLog(@"btn_on_left_arrow_click:");
    
}
-(void)btn_on_right_arrow_click:(UIButton *)sender
{
    NSLog(@"btn_on_right_arrow2_click:");
    
}
-(void)btn_on_left_arrow2_click:(UIButton *)sender
{
    NSLog(@"btn_on_left_arrow_click:");
    
}
-(void)btn_on_right_arrow2_click:(UIButton *)sender
{
    NSLog(@"btn_on_right_arrow2_click:");
    
}
-(void)btn_on_left_arrow3_click:(UIButton *)sender
{
    NSLog(@"btn_on_left_arrow3_click:");
    
}
-(void)btn_on_right_arrow3_click:(UIButton *)sender
{
    NSLog(@"btn_on_right_arrow3_click:");
    
}

#pragma return action

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}



- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
