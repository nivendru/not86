//
//  AddToCartFoodNowVC.m
//  Not86
//
//  Created by Admin on 30/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "AddToCartFoodNowVC.h"
#import "Define.h"
#import "AppDelegate.h"
#import "UIImageView+AFNetworking.h"
#import "DiningCartVC.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"

@interface AddToCartFoodNowVC ()<UITabBarControllerDelegate,UITableViewDataSource,UITableViewDelegate>

@end

@implementation AddToCartFoodNowVC

{
    UIImageView *img_header;
    UIImageView *img_bg;
    AppDelegate *delegate;
    
    UIButton *  btn_on_dine_in;
    UIImageView * img_red_tik_on_dine_in;
    UIButton *  btn_on_take_out;
    UIImageView * img_red_tik_on_take_out;
    UIButton * btn_on_delever;
    UIImageView * img_red_tik_on_deliver;
    int  DEF;
    NSMutableArray *  array_temp2;
    UITableView*tableview_Kcountry;
    UILabel *lbl_qty_valu_inrect;
    UIView*alertviewBg;
    NSString*str_takoutbody;
    
    

}
@synthesize ary_details,str_camefrom;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    // Do any additional setup after loading the view.
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    array_temp2 = [NSMutableArray new];
    str_takoutbody = [NSString new];
    str_takoutbody = @"3";
    
    [self integrateHeader];
    [self integrateBody];
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
   
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)integrateHeader
{
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 50);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,5,35,35);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back addTarget:self action:@selector(btn_back_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_back];
    
    
    UILabel *lbl_dish_detail = [[UILabel alloc]init];
    lbl_dish_detail.frame = CGRectMake(CGRectGetMaxX(icon_back.frame)+30,0, WIDTH-70, 45);
    lbl_dish_detail.text = [NSString stringWithFormat:@"%@",[[ary_details objectAtIndex:0] valueForKey:@"DishName"]];
    lbl_dish_detail.font = [UIFont fontWithName:kFont size:14];
    lbl_dish_detail.textColor = [UIColor whiteColor];
    lbl_dish_detail.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_dish_detail];
    
    
    
    
    UIImageView *icon_logo = [[UIImageView alloc]init];
    icon_logo .frame = CGRectMake(WIDTH-40, 9, 30, 30);
    [icon_logo  setImage:[UIImage imageNamed:@"img_logo@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_logo  setUserInteractionEnabled:YES];
    [img_header addSubview:icon_logo ];
    
    
}
-(void)integrateBody
{
    
    
    img_bg = [[UIImageView alloc]init];
    //  img_bg .frame = CGRectMake(0,CGRectGetMaxY(img_header.frame)+10, WIDTH+10, 350);
    [img_bg  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg  setUserInteractionEnabled:YES];
    [ self.view addSubview:img_bg ];
    ;
    
    UILabel *lbl_kitchen_name = [[UILabel alloc]init];
    // lbl_kitchen_name.frame = CGRectMake(18,20, 350, 20);
    
    if ([NSString stringWithFormat:@"%@",[[[ary_details objectAtIndex:0] valueForKey:@"ChefDetail"] valueForKey:@"Kitchen_Name"]] ==(NSString*) [NSNull null])
    {
        lbl_kitchen_name.text = [NSString stringWithFormat:@"Name of Kitchen: %@",@""];
    }
    else
    {
      lbl_kitchen_name.text = [NSString stringWithFormat:@"Name of Kitchen: %@",[[[ary_details objectAtIndex:0] valueForKey:@"ChefDetail"] valueForKey:@"Kitchen_Name"]];
    }
    
    
    lbl_kitchen_name.font = [UIFont fontWithName:kFont size:15];
    lbl_kitchen_name.textColor = [UIColor blackColor];
    lbl_kitchen_name.textAlignment = NSTextAlignmentLeft;
    lbl_kitchen_name.backgroundColor = [UIColor clearColor];
    [ img_bg addSubview:lbl_kitchen_name];
    
    
    
    UILabel *lbl_dish_name = [[UILabel alloc]init];
    //lbl_dish_name.frame = CGRectMake(18,CGRectGetMaxY(lbl_kitchen_name.frame)+10, 350, 20);
     lbl_dish_name.text= [NSString stringWithFormat:@"Dish/Meal Name: %@",[[ary_details objectAtIndex:0] valueForKey:@"DishName"]];
    lbl_dish_name.font = [UIFont fontWithName:kFont size:15];
    lbl_dish_name.textAlignment = NSTextAlignmentLeft;

    lbl_dish_name.textColor = [UIColor blackColor];
    lbl_dish_name.backgroundColor = [UIColor clearColor];
    [ img_bg addSubview:lbl_dish_name];
    
    
    UILabel *lbl_seving_time = [[UILabel alloc]init];
    //lbl_seving_time .frame = CGRectMake(18,CGRectGetMaxY(lbl_dish_name.frame)+10, 350, 20);
    lbl_seving_time .text = @"Serving Time: Now ";
    lbl_seving_time .font = [UIFont fontWithName:kFont size:15];
    lbl_seving_time .textColor = [UIColor blackColor];
    lbl_seving_time .backgroundColor = [UIColor clearColor];
    [ img_bg addSubview:lbl_seving_time ];
    
    
    UILabel *lbl_seving_qty = [[UILabel alloc]init];
    // lbl_seving_qty .frame = CGRectMake(18,CGRectGetMaxY(lbl_seving_time.frame)+10, 350, 20);
    
    if ([NSString stringWithFormat:@"%@",[[ary_details objectAtIndex:0] valueForKey:@"quantity"]] ==(NSString*) [NSNull null])
    {
        lbl_seving_qty .text = [NSString stringWithFormat:@"Available Serving Qty Now: %@",@""];
    }
    else
    {
        lbl_seving_qty .text = [NSString stringWithFormat:@"Available Serving Qty Now: %@",[[ary_details objectAtIndex:0] valueForKey:@"quantity"]];
        
    }
    lbl_seving_qty .font = [UIFont fontWithName:kFont size:15];
    lbl_seving_qty .textColor = [UIColor blackColor];
    lbl_seving_qty.textAlignment = NSTextAlignmentLeft;

    lbl_seving_qty .backgroundColor = [UIColor clearColor];
    [ img_bg addSubview:lbl_seving_qty ];
    
    UILabel *lbl_price = [[UILabel alloc]init];
    //lbl_price  .frame = CGRectMake(18,CGRectGetMaxY(lbl_seving_qty.frame)+10, 350, 20);
  
    
     lbl_price.text =[NSString stringWithFormat:@"Price/Serving: $%@",[[ary_details objectAtIndex:0] valueForKey:@"DishPrice"]];
    lbl_price .font = [UIFont fontWithName:kFont size:15];
    lbl_price  .textColor = [UIColor blackColor];
    lbl_price  .backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_price  ];
    
    
    UIImageView *img_line = [[UIImageView alloc]init];
    //img_line .frame = CGRectMake(13,CGRectGetMaxY(lbl_price.frame)+10, 275, 0.5);
    [img_line setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_line setUserInteractionEnabled:YES];
    [img_bg addSubview:img_line ];
    
    
    UILabel *lbl_serving_qty_order = [[UILabel alloc]init];
    //lbl_serving_qty_order  .frame = CGRectMake(18,CGRectGetMaxY(img_line.frame)+10, 350, 20);
    lbl_serving_qty_order  .text = @"Serving Quantity Ordered:";
    lbl_serving_qty_order .font = [UIFont fontWithName:kFontBold size:15];
    lbl_serving_qty_order  .textColor = [UIColor blackColor];
    lbl_serving_qty_order  .backgroundColor = [UIColor clearColor];
    [ img_bg addSubview:lbl_serving_qty_order ];
    
    UIImageView *img_rect = [[UIImageView alloc]init];
    //img_rect .frame = CGRectMake(210,CGRectGetMaxY(img_line.frame)+10, 50, 20);
    [img_rect setImage:[UIImage imageNamed:@"rectangle@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_rect setUserInteractionEnabled:YES];
    [img_bg addSubview:img_rect ];
    
    
    UIImageView *img_rect1 = [[UIImageView alloc]init];
    //img_rect .frame = CGRectMake(210,CGRectGetMaxY(img_line.frame)+10, 50, 20);
    [img_rect1 setImage:[UIImage imageNamed:@"drop-down-icon@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_rect1 setUserInteractionEnabled:YES];
    [img_rect addSubview:img_rect1];

    
    UIButton *icon_drop_down = [UIButton buttonWithType:UIButtonTypeCustom];
    //icon_drop_down .frame = CGRectMake(32,7,12,10);
    icon_drop_down  .backgroundColor = [UIColor clearColor];
    [icon_drop_down  addTarget:self action:@selector(btn_drop_down_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_rect   addSubview:icon_drop_down ];
    
    
    lbl_qty_valu_inrect = [[UILabel alloc]init];
    // lbl_qty_valu_inrect  .frame = CGRectMake(10,5,12,12);
    lbl_qty_valu_inrect.font = [UIFont fontWithName:kFont size:15];
    lbl_qty_valu_inrect  .textColor = [UIColor blackColor];
    lbl_qty_valu_inrect .backgroundColor = [UIColor clearColor];
    [ img_rect  addSubview:lbl_qty_valu_inrect];
    
    UILabel *lbl_serving_type = [[UILabel alloc]init];
    // lbl_serving_type .frame = CGRectMake(75,CGRectGetMaxY(lbl_serving_qty_order .frame)+10, 350, 20);
    lbl_serving_type .text = @"Select Serving Type";
    lbl_serving_type .font = [UIFont fontWithName:kFontBold size:15];
    lbl_serving_type  .textColor = [UIColor blackColor];
    lbl_serving_type  .backgroundColor = [UIColor clearColor];
    [ img_bg addSubview:lbl_serving_type];
    
    btn_on_dine_in  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_dine_in.frame = CGRectMake(35,CGRectGetMaxY(lbl_serving_type .frame)+10, 60, 60);
    //[ btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in-bl@2x.png"] forState:UIControlStateSelected];
    [ btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in@2x.png"] forState:UIControlStateNormal];
     btn_on_dine_in.tag = 0;
    [ btn_on_dine_in addTarget:self action:@selector(btn_serving1_click:)forControlEvents:UIControlEventTouchUpInside];
    [img_bg addSubview:   btn_on_dine_in];
    
    img_red_tik_on_dine_in = [[UIImageView alloc]init];
    img_red_tik_on_dine_in.frame = CGRectMake(CGRectGetMidX(btn_on_dine_in.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+10,25,20);
    [img_red_tik_on_dine_in setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik_on_dine_in setUserInteractionEnabled:YES];
    [img_bg addSubview:img_red_tik_on_dine_in];
    img_red_tik_on_dine_in.hidden = YES;
    
    
    btn_on_take_out  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_take_out.frame = CGRectMake(CGRectGetMaxX(btn_on_dine_in.frame)+70,CGRectGetMaxY(lbl_serving_type .frame)+10,50,50);
    //[btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out-bl@2x.png"]forState:UIControlStateSelected];
    [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out@2x.png"]forState:UIControlStateNormal];
    btn_on_take_out.tag = 1;
    [btn_on_take_out addTarget:self action:@selector(btn_serving2_click:)forControlEvents:UIControlEventTouchUpInside];
    [img_bg addSubview:btn_on_take_out];
    
    img_red_tik_on_take_out = [[UIImageView alloc]init];
    img_red_tik_on_take_out.frame = CGRectMake(CGRectGetMidX(btn_on_take_out.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+10,25,20);
    [img_red_tik_on_take_out setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik_on_take_out setUserInteractionEnabled:YES];
    [img_bg addSubview:img_red_tik_on_take_out];
    img_red_tik_on_take_out.hidden = YES;
    
    
    btn_on_delever  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_delever.frame = CGRectMake(CGRectGetMaxX( btn_on_take_out.frame)+70,CGRectGetMaxY(lbl_serving_type .frame)+10,50,50);
    //[btn_on_delever setImage:[UIImage imageNamed:@"img-delever-bl@2x.png"] forState:UIControlStateSelected];
    [btn_on_delever setImage:[UIImage imageNamed:@"img-delever@2x.png"] forState:UIControlStateNormal];
    btn_on_delever.tag = 2;
    [btn_on_delever addTarget:self action:@selector(btn_serving3_click:)forControlEvents:UIControlEventTouchUpInside];
    [img_bg addSubview:btn_on_delever];
    
    img_red_tik_on_deliver = [[UIImageView alloc]init];
    img_red_tik_on_deliver.frame = CGRectMake(CGRectGetMidX(btn_on_delever.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+10,25,20);
    [img_red_tik_on_deliver setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik_on_deliver setUserInteractionEnabled:YES];
    [img_bg addSubview:img_red_tik_on_deliver];
    img_red_tik_on_deliver.hidden = YES;
    
    
    UIButton *img_add_to_cart = [UIButton buttonWithType:UIButtonTypeCustom];
    //img_add_to_cart .frame = CGRectMake(-10,CGRectGetMaxY( img_bg .frame)+72, WIDTH+19, 35);
    img_add_to_cart .backgroundColor = [UIColor colorWithRed:40/255.0f green:30/255.0f blue:48/255.0f alpha:1];
    [img_add_to_cart  addTarget:self action:@selector(btn_add_to_cart_click:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:img_add_to_cart];
    
    
    UILabel *lbl_add_to_cart = [[UILabel alloc]init];
    //lbl_add_to_cart.frame = CGRectMake(100,5, 300, 20);
    lbl_add_to_cart .text = @"ADD TO CART";
    lbl_add_to_cart.font = [UIFont fontWithName:kFontBold size:20];
    lbl_add_to_cart .textColor = [UIColor whiteColor];
    lbl_add_to_cart .backgroundColor = [UIColor clearColor];
    lbl_add_to_cart.textAlignment = NSTextAlignmentCenter;
    [img_add_to_cart addSubview:lbl_add_to_cart];
    
    //    UIButton *btn_on_add_to_cart = [UIButton buttonWithType:UIButtonTypeCustom];
    //    //btn_on_add_to_cart .frame = CGRectMake(10,CGRectGetMaxY( img_bg .frame)+72, WIDTH+19, 35);
    //    btn_on_add_to_cart .backgroundColor = [UIColor redColor];
    //    [btn_on_add_to_cart  addTarget:self action:@selector(btn_add_to_cart_click:) forControlEvents:UIControlEventTouchUpInside];
    //   // [img_add_to_cart setImage:[UIImage imageNamed:@"img-cart@2x.png"] forState:UIControlStateNormal];
    //    [self.view  addSubview:btn_on_add_to_cart];
    
    
    
    tableview_Kcountry = [[UITableView alloc]init];
    [tableview_Kcountry setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tableview_Kcountry.delegate = self;
    tableview_Kcountry.dataSource = self;
    tableview_Kcountry.showsVerticalScrollIndicator = NO;
    tableview_Kcountry.backgroundColor = [UIColor whiteColor];
    tableview_Kcountry.layer.borderColor = [[UIColor blackColor]CGColor];
    tableview_Kcountry.layer.borderWidth = 1.0f;
    tableview_Kcountry.clipsToBounds = YES;
    tableview_Kcountry.hidden =YES;
    [img_bg addSubview:tableview_Kcountry];
    
    if ([str_camefrom isEqualToString:@"NOW"])
    {
        if ([[NSString stringWithFormat:@"%@",[[[ary_details objectAtIndex:0] valueForKey:@"ChefDetail"]valueForKey:@"ChefStatus"]]isEqualToString:@"On Schedule"])
        {
            
            if ([NSString stringWithFormat:@"%@",[[[[ary_details objectAtIndex:0] valueForKey:@"ChefDetail"]valueForKey:@"ServeType"] valueForKey:@"delivery"]] ==(NSString*) [NSNull null])
            {
                btn_on_delever.hidden = NO;
                //img_red_tik_on_deliver.hidden=NO;
            }
            else
            {
                if ([[NSString stringWithFormat:@"%@",[[[[ary_details objectAtIndex:0] valueForKey:@"ChefDetail"]valueForKey:@"ServeType"] valueForKey:@"delivery"]]isEqualToString:@"1"])
                {
                   
                    btn_on_delever.hidden = NO;
                    //img_red_tik_on_deliver.hidden=NO;
                }
                else{
                    btn_on_delever.hidden = YES;
                    img_red_tik_on_deliver.hidden=YES;
                }
            }
            
            
            if ([NSString stringWithFormat:@"%@",[[[[ary_details objectAtIndex:0] valueForKey:@"ChefDetail"]valueForKey:@"ServeType"] valueForKey:@"dining"]] ==(NSString*) [NSNull null])
            {
                btn_on_dine_in.hidden = NO;
                //img_red_tik_on_dine_in.hidden = NO;

            }
            else
            {
                if ([[NSString stringWithFormat:@"%@",[[[[ary_details objectAtIndex:0] valueForKey:@"ChefDetail"]valueForKey:@"ServeType"] valueForKey:@"dining"]]isEqualToString:@"1"])
                {
                    btn_on_dine_in.hidden = NO;
                    //img_red_tik_on_dine_in.hidden = NO;
                    
                }
                else{
                    btn_on_dine_in.hidden = YES;
                    img_red_tik_on_dine_in.hidden = YES;
                }
            }
            
            
            
            if ([NSString stringWithFormat:@"%@",[[[[ary_details objectAtIndex:0] valueForKey:@"ChefDetail"]valueForKey:@"ServeType"] valueForKey:@"takeout"]] ==(NSString*) [NSNull null])
            {
                btn_on_take_out.hidden = NO;
                img_red_tik_on_take_out.hidden = NO;
            }
            else
            {
                if ([[NSString stringWithFormat:@"%@",[[[[ary_details objectAtIndex:0] valueForKey:@"ChefDetail"]valueForKey:@"ServeType"] valueForKey:@"takeout"]]isEqualToString:@"1"])
                {
                    
                    btn_on_take_out.hidden = NO;
                   // img_red_tik_on_take_out.hidden = NO;
                    
                }
                else{
                    btn_on_take_out.hidden = YES;
                   // img_red_tik_on_take_out.hidden = YES;
                }
            }
            
            
        }
        else
        {
            
            
        }
    }
    else{
        
        lbl_kitchen_name.text = [NSString stringWithFormat:@"Name of Kitchen: %@",@""];
        lbl_seving_qty .text = [NSString stringWithFormat:@"Available Serving Qty Now: %@",@""];


        btn_on_delever.hidden = NO;
       // img_red_tik_on_deliver.hidden=NO;
        btn_on_dine_in.hidden = NO;
        //img_red_tik_on_dine_in.hidden = NO;
        btn_on_take_out.hidden = NO;
       // img_red_tik_on_take_out.hidden = NO;
        
//        if ([[NSString stringWithFormat:@"%@",[[[ary_details objectAtIndex:0] valueForKey:@"ChefDetail"]valueForKey:@"Dishservetype"]]isEqualToString:@"on_request"])
//        {
//            
//        }
//        else
//        {
//            
//            
//        }
    }
    
    
    if (IS_IPHONE_6Plus)
    {
        img_bg .frame = CGRectMake(0,CGRectGetMaxY(img_header.frame)+10, WIDTH+10, 370);
        lbl_kitchen_name.frame = CGRectMake(35,30, 350, 20);
        lbl_dish_name.frame = CGRectMake(35,CGRectGetMaxY(lbl_kitchen_name.frame)+10, WIDTH-70, 20);
        lbl_seving_time .frame = CGRectMake(35,CGRectGetMaxY(lbl_dish_name.frame)+10, WIDTH-70, 20);
        lbl_seving_qty .frame = CGRectMake(35,CGRectGetMaxY(lbl_seving_time.frame)+10, WIDTH-70, 20);
        lbl_price  .frame = CGRectMake(35,CGRectGetMaxY(lbl_seving_qty.frame)+10, WIDTH-70, 20);
        img_line .frame = CGRectMake(18,CGRectGetMaxY(lbl_price.frame)+10, WIDTH-50, 0.5);
        lbl_serving_qty_order  .frame = CGRectMake(35,CGRectGetMaxY(img_line.frame)+10, 350, 20);
        img_rect .frame = CGRectMake(230,CGRectGetMaxY(img_line.frame)+10, 50, 20);
        img_rect1.frame = CGRectMake(32,7,12,10);
        icon_drop_down .frame = CGRectMake(0,0, 50, 20);
        lbl_qty_valu_inrect  .frame = CGRectMake(10,5,12,12);
        lbl_serving_type .frame = CGRectMake(130,CGRectGetMaxY(lbl_serving_qty_order .frame)+20, 350, 20);
        img_add_to_cart .frame = CGRectMake(0,HEIGHT-60, WIDTH, 60);
        btn_on_dine_in.frame = CGRectMake(35,CGRectGetMaxY(lbl_serving_type .frame)+10, 50, 50);
        img_red_tik_on_dine_in.frame = CGRectMake(CGRectGetMidX(btn_on_dine_in.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+20,25,20);
        btn_on_take_out.frame = CGRectMake(CGRectGetMaxX(btn_on_dine_in.frame)+70,CGRectGetMaxY(lbl_serving_type .frame)+10,50,50);
        img_red_tik_on_take_out.frame = CGRectMake(CGRectGetMidX(btn_on_take_out.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+20,25,20);
        btn_on_delever.frame = CGRectMake(CGRectGetMaxX( btn_on_take_out.frame)+70,CGRectGetMaxY(lbl_serving_type .frame)+10,50,50);
        img_red_tik_on_deliver.frame = CGRectMake(CGRectGetMidX(btn_on_delever.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+20,25,20);
        tableview_Kcountry.frame=CGRectMake(230, CGRectGetMaxY(img_rect.frame)+5, 70, 140);
        lbl_add_to_cart.frame = CGRectMake(0,5, img_add_to_cart.frame.size.width, 50);
        // btn_on_add_to_cart .frame = CGRectMake(0,CGRectGetMaxY( img_bg .frame)+60, WIDTH+19, 39);
        
    }
    else if (IS_IPHONE_6)
    {
        img_bg .frame = CGRectMake(0,CGRectGetMaxY(img_header.frame)+10, WIDTH+7, 370);
        lbl_kitchen_name.frame = CGRectMake(35,30, WIDTH-70, 20);
        lbl_dish_name.frame = CGRectMake(35,CGRectGetMaxY(lbl_kitchen_name.frame)+10, WIDTH-70, 20);
        lbl_seving_time .frame = CGRectMake(35,CGRectGetMaxY(lbl_dish_name.frame)+10, WIDTH-70, 20);
        lbl_seving_qty .frame = CGRectMake(35,CGRectGetMaxY(lbl_seving_time.frame)+10, WIDTH-70, 20);
        lbl_price  .frame = CGRectMake(35,CGRectGetMaxY(lbl_seving_qty.frame)+10, WIDTH-70, 20);
        img_line .frame = CGRectMake(18,CGRectGetMaxY(lbl_price.frame)+10, WIDTH-50, 0.5);
        lbl_serving_qty_order  .frame = CGRectMake(35,CGRectGetMaxY(img_line.frame)+10, 350, 20);
        img_rect .frame = CGRectMake(230,CGRectGetMaxY(img_line.frame)+10, 50, 20);
        img_rect1.frame = CGRectMake(32,7,12,10);

        icon_drop_down .frame = CGRectMake(0,0, 50, 20);
        lbl_qty_valu_inrect  .frame = CGRectMake(10,5,12,12);
        lbl_serving_type .frame = CGRectMake(130,CGRectGetMaxY(lbl_serving_qty_order .frame)+20, 350, 20);
        btn_on_dine_in.frame = CGRectMake(35,CGRectGetMaxY(lbl_serving_type .frame)+10, 50, 50);
        img_red_tik_on_dine_in.frame = CGRectMake(CGRectGetMidX(btn_on_dine_in.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+20,25,20);
        btn_on_take_out.frame = CGRectMake(CGRectGetMaxX(btn_on_dine_in.frame)+70,CGRectGetMaxY(lbl_serving_type .frame)+10,50,50);
        img_red_tik_on_take_out.frame = CGRectMake(CGRectGetMidX(btn_on_take_out.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+20,25,20);
        btn_on_delever.frame = CGRectMake(CGRectGetMaxX( btn_on_take_out.frame)+70,CGRectGetMaxY(lbl_serving_type .frame)+10,50,50);
        img_red_tik_on_deliver.frame = CGRectMake(CGRectGetMidX(btn_on_delever.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+20,25,20);
        tableview_Kcountry.frame=CGRectMake(230, CGRectGetMaxY(img_rect.frame)+25, 70, 140);

        
        img_add_to_cart .frame = CGRectMake(0,HEIGHT-60, WIDTH, 60);
        lbl_add_to_cart.frame = CGRectMake(0,0, WIDTH, 60);
        // btn_on_add_to_cart .frame = CGRectMake(0,CGRectGetMaxY(img_bg .frame)+140, WIDTH+19, 39);
        
        
    }
    else
    {
        img_bg .frame = CGRectMake(0,CGRectGetMaxY(img_header.frame)+10, WIDTH, 350);
        lbl_kitchen_name.frame = CGRectMake(28,20, WIDTH-56, 20);
        lbl_dish_name.frame = CGRectMake(28,CGRectGetMaxY(lbl_kitchen_name.frame)+10, WIDTH-56, 20);
        lbl_seving_time .frame = CGRectMake(28,CGRectGetMaxY(lbl_dish_name.frame)+10, WIDTH-56, 20);
        lbl_seving_qty .frame = CGRectMake(28,CGRectGetMaxY(lbl_seving_time.frame)+10,WIDTH-56, 20);
        lbl_price  .frame = CGRectMake(28,CGRectGetMaxY(lbl_seving_qty.frame)+10, WIDTH-56, 20);
        img_line .frame = CGRectMake(22,CGRectGetMaxY(lbl_price.frame)+10, 275, 0.5);
        lbl_serving_qty_order  .frame = CGRectMake(28,CGRectGetMaxY(img_line.frame)+10, 350, 20);
        img_rect .frame = CGRectMake(220,CGRectGetMaxY(img_line.frame)+10, 50, 20);
        img_rect1 .frame = CGRectMake(32,7,12,10);
        icon_drop_down .frame = CGRectMake(0,0, 50, 20);

        lbl_qty_valu_inrect  .frame = CGRectMake(10,5,12,12);
        lbl_serving_type .frame = CGRectMake(100,CGRectGetMaxY(lbl_serving_qty_order .frame)+10, 350, 20);
        btn_on_dine_in.frame = CGRectMake(35,CGRectGetMaxY(lbl_serving_type .frame)+10, 50, 50);
        img_red_tik_on_dine_in.frame = CGRectMake(CGRectGetMidX(btn_on_dine_in.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+20,25,20);
        btn_on_take_out.frame = CGRectMake(CGRectGetMaxX(btn_on_dine_in.frame)+50,CGRectGetMaxY(lbl_serving_type .frame)+10,50,50);
        img_red_tik_on_take_out.frame = CGRectMake(CGRectGetMidX(btn_on_take_out.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+20,25,20);
        btn_on_delever.frame = CGRectMake(CGRectGetMaxX( btn_on_take_out.frame)+50,CGRectGetMaxY(lbl_serving_type .frame)+10,50,50);
        img_red_tik_on_deliver.frame = CGRectMake(CGRectGetMidX(btn_on_delever.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+20,25,20);

        img_add_to_cart .frame = CGRectMake(0,HEIGHT-43, WIDTH+100, 50);
        lbl_add_to_cart.frame = CGRectMake(0,9, WIDTH, 20);
        // btn_on_add_to_cart .frame = CGRectMake(0,IS_IPHONE_5?CGRectGetMaxY( img_bg .frame)+120:CGRectGetMaxY( img_bg .frame)+30, WIDTH+19, 39);
        
        tableview_Kcountry.frame=CGRectMake(220, CGRectGetMaxY(img_rect.frame)+5, 70, 140);

    }
    
    
}

-(void)btn_back_click:(UIButton *)sender
{
    NSLog(@"icon_back_click Btn Click");
    [self dismissViewControllerAnimated:NO completion:nil];
//    [self.navigationController popViewControllerAnimated:NO];
    
    
}

-(void)btn_drop_down_click:(UIButton *)sender
{
    NSLog(@"icon_drop_down_click Btn Click");
    
    
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        tableview_Kcountry.hidden=NO;
        
    }
    else
    {
        [sender setSelected:NO];
        tableview_Kcountry.hidden=YES;
    }
    
}
//-(void)btn_serving1_click:(UIButton *)sender
//{
//    NSLog(@"icon_drop_down_click Btn Click");
//    
//}
//
//-(void)btn_serving2_click:(UIButton *)sender
//{
//    NSLog(@"icon_drop_down_click Btn Click");
//    
//}
//
//-(void)btn_serving3_click:(UIButton *)sender
//{
//    NSLog(@"icon_drop_down_click Btn Click");
//    
//}



-(void)btn_serving1_click:(UIButton *)sender
{
    if([sender isSelected])
    {
        [sender setSelected:NO];
        str_takoutbody = @"3";
        
        // [btn_on_dine_in setBackgroundImage:[UIImage imageNamed:@"img-dine-in@2x"] forState:UIControlStateNormal];
        [btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in@2x"] forState:UIControlStateNormal];
        img_red_tik_on_dine_in.hidden = YES;
        
    }
    else
    {
        
        str_takoutbody = @"0";
        [sender setSelected:YES];
        img_red_tik_on_take_out.hidden = YES;
        img_red_tik_on_deliver.hidden = YES;
        img_red_tik_on_dine_in.hidden = NO;
        
        [btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in-bl@2x"] forState:UIControlStateNormal];
        
        [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out@2x"] forState:UIControlStateNormal];
        
        [btn_on_delever setImage:[UIImage imageNamed:@"img-delever@2x.png"] forState:UIControlStateNormal];
        
        
        // [btn_on_dine_in setBackgroundImage:[UIImage imageNamed:@"img-dine-in-bl@2x"] forState:UIControlStateNormal];
        //[btn_on_take_out setBackgroundImage:[UIImage imageNamed:@"img-take-out@2x"] forState:UIControlStateNormal];
        // [btn_on_delever setBackgroundImage:[UIImage imageNamed:@"img-delever@2x"] forState:UIControlStateNormal];
        
    }
    
    
}
-(void)btn_serving2_click:(UIButton *)sender
{
    
    NSLog(@"TakeOut Button Clicked");
    if([sender isSelected])
    {
        [sender setSelected:NO];
        str_takoutbody = @"3";
        
        // [btn_on_take_out setBackgroundImage:[UIImage imageNamed:@"img-take-out@2x"] forState:UIControlStateNormal];
        
        [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out@2x"] forState:UIControlStateNormal];
        img_red_tik_on_take_out.hidden = YES;
        
        
    }
    else
    {
        img_red_tik_on_take_out.hidden = NO;
        img_red_tik_on_deliver.hidden = YES;
        img_red_tik_on_dine_in.hidden = YES;
        
        str_takoutbody = @"1";
        [btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in@2x"] forState:UIControlStateNormal];
        
        [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out-bl@2x"] forState:UIControlStateNormal];
        
        [btn_on_delever setImage:[UIImage imageNamed:@"img-delever@2x"] forState:UIControlStateNormal];
        
        //  [btn_on_dine_in setBackgroundImage:[UIImage imageNamed:@"img-dine-in@2x"] forState:UIControlStateNormal];
        //[btn_on_take_out setBackgroundImage:[UIImage imageNamed:@"img-take-out-bl@2x"] forState:UIControlStateNormal];
        // [btn_on_delever setBackgroundImage:[UIImage imageNamed:@"img-delever@2x"] forState:UIControlStateNormal];
        [sender setSelected:YES];
        
    }
    
}

-(void)btn_serving3_click:(UIButton *)sender
{
    
    NSLog(@"Delevery Button Clicked");
    //    SelectAll_btnClick
    
    if([sender isSelected])
    {
        [sender setSelected:NO];
        str_takoutbody = @"3";
        
        //  [btn_on_delever setBackgroundImage:[UIImage imageNamed:@"img-delever@2x"] forState:UIControlStateNormal];
        [btn_on_delever setImage:[UIImage imageNamed:@"img-delever@2x"] forState:UIControlStateNormal];
        img_red_tik_on_deliver.hidden = YES;
        
    }
    else
    {
        
        str_takoutbody = @"2";
        
        img_red_tik_on_take_out.hidden = YES;
        img_red_tik_on_deliver.hidden = NO;
        img_red_tik_on_dine_in.hidden = YES;
        
        [btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in@2x"] forState:UIControlStateNormal];
        
        [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out@2x"] forState:UIControlStateNormal];
        
        [btn_on_delever setImage:[UIImage imageNamed:@"img-delever-bl@2x"] forState:UIControlStateNormal];
        
        // [btn_on_dine_in setBackgroundImage:[UIImage imageNamed:@"img-dine-in@2x"] forState:UIControlStateNormal];
        // [btn_on_take_out setBackgroundImage:[UIImage imageNamed:@"img-take-out@2x"] forState:UIControlStateNormal];
        //  [btn_on_delever setBackgroundImage:[UIImage imageNamed:@"img-delever-bl@2x"] forState:UIControlStateNormal];
        
        [sender setSelected:YES];
        
    }
    
}
//
-(void)btn_add_to_cart_click:(UIButton *)sender
{
    NSLog(@"icon_add_to_cart_click Btn Click");
    
    
    if (lbl_qty_valu_inrect.text.length<=0)
    {
        [self popup_Alertview:@"plesae select quantity"];
        
    }
    else if ([[NSString stringWithFormat:@"%@",str_takoutbody]isEqualToString:@"3"])
    {
        [self popup_Alertview:@"plesae select servingtype"];

    }
    else
    {
        [self AFUserAddtoCart];
        
    }
//    DiningCartVC*vc = [[DiningCartVC alloc]init];
//    [self presentViewController:vc animated:NO completion:nil];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}
-(void)click_on_serving_type:(UIButton *)sender
{
    NSLog(@"btn_img_dine_in_click:");
    DEF=(int)sender.tag;
    
    if([ array_temp2 containsObject:[NSString stringWithFormat:@"%d",DEF]])
    {
        [array_temp2 removeObject:[NSString stringWithFormat:@"%d",DEF]];
        
        if (sender.tag == 0)
        {
            
            [btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in@2x.png"]forState:UIControlStateNormal];
            img_red_tik_on_dine_in.hidden=YES;
            
            
        }
        else if (sender.tag == 1)
        {
            img_red_tik_on_take_out.hidden=YES;
            [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out@2x.png"] forState:UIControlStateNormal];
        }
        else if (sender.tag == 2)
        {
            img_red_tik_on_deliver.hidden=YES;
            [btn_on_delever setImage:[UIImage imageNamed:@"img-delever@2x.png"] forState:UIControlStateNormal];
            
        }
    }
    else
    {
        [array_temp2 addObject:[NSString stringWithFormat:@"%d",DEF]];
        if (sender.tag == 0)
        {
            [btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in-bl@2x.png"]forState:UIControlStateNormal];
            img_red_tik_on_dine_in.hidden=NO;
        }
        else if (sender.tag == 1)
        {
            [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out-bl@2x.png"]forState:UIControlStateNormal];
            img_red_tik_on_take_out.hidden=NO;
            
        }
        else if (sender.tag == 2)
        {
            [btn_on_delever setImage:[UIImage imageNamed:@"img-delever-bl@2x.png"]forState:UIControlStateNormal];
            img_red_tik_on_deliver.hidden=NO;
            
        }
    }
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tableview_Kcountry)
    {
        
        
        int total = [[[ary_details objectAtIndex:0] valueForKey:@"Total_Serving"] intValue];
        
        if (total>0)
        {
            return total;
        }
        else{
            return 11;
        }
        
        
    }else {
        return 0;
    }
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"LazyTableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor=[UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    
    if (tableView==tableview_Kcountry)
    {
        UILabel  * lbl_list = [[UILabel alloc]init];
        lbl_list.frame=CGRectMake(0, 0, 70, 25);
        lbl_list.backgroundColor=[UIColor clearColor];
        lbl_list.textColor=[UIColor blackColor];
        lbl_list.textAlignment= NSTextAlignmentCenter;
        lbl_list.font = [UIFont fontWithName:kFont size:12];
        lbl_list.text=[NSString stringWithFormat:@"%ld",indexPath.row+1];
        [cell.contentView addSubview:lbl_list];
        
        
    }
    else{
        
        
    }
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 25;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tableview_Kcountry) {
        
        lbl_qty_valu_inrect.text =[NSString stringWithFormat:@"%ld",indexPath.row+1];
        
        if (IS_IPHONE_6Plus)
        {
            lbl_qty_valu_inrect.font = [UIFont fontWithName:kFont size:9];

        }
        else if (IS_IPHONE_6)
        {
            lbl_qty_valu_inrect.font = [UIFont fontWithName:kFont size:9];

        }
        else
        {
            lbl_qty_valu_inrect.font = [UIFont fontWithName:kFont size:6];

        }
        
        [tableview_Kcountry setHidden:YES];
        
        
    }
}


#pragma UserAddtoCart-functionality

-(void)AFUserAddtoCart
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    NSMutableString *str_servingtype = [[NSMutableString alloc] init];
    
    for (int i=0; i<[array_temp2 count]; i++)
    {
        
        
        if (i==[array_temp2 count]-1)
        {
            [str_servingtype  appendString:[NSString stringWithFormat:@"%@",[array_temp2 objectAtIndex:i]]];
        }
        else
        {
            [str_servingtype  appendString:[NSString stringWithFormat:@"%@,",[array_temp2 objectAtIndex:i]]];
        }
        
        
    }

  
    
    NSDictionary *params =@{
                            
                                                        @"uid"                          :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                                                        @"item_id"                        :  [[ary_details objectAtIndex:0] valueForKey:@"DishID"],
                                                        @"quantity"                       :  lbl_qty_valu_inrect.text,
                                                        @"price"                          :  [[ary_details objectAtIndex:0] valueForKey:@"DishPrice"],
                                                        @"serving_type"                   :  str_takoutbody,
                                                        @"type"                           :  @"0"
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:kUserAddtoCart  parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseUserAddtoCart:JSON];
    }
     
     
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error){
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFUserAddtoCart];
                                         }
                                     }];
    [operation start];
    
}
-(void)ResponseUserAddtoCart:(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
         [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
         [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
}



#pragma mark Alertview Popup


-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
    
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
