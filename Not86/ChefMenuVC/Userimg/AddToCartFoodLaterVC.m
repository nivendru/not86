//
//  AddToCartFoodLaterVC.m
//  Not86
//
//  Created by Admin on 30/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "AddToCartFoodLaterVC.h"
#import "Define.h"
#import "CKCalendarView.h"

#import "AppDelegate.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)




@interface AddToCartFoodLaterVC ()<UIScrollViewDelegate,CKCalendarDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    UIImageView *img_header;
    
    UIScrollView *scroll;
    UITableView*tableview_Kcountry;
     UILabel *lbl_qty_valu_inrect;
    UIButton*btn_on_dine_in;
    UIButton*btn_on_take_out;
    UIButton*btn_on_delever;
    
    UIImageView*img_red_tik_on_dine_in;
    UIImageView*img_red_tik_on_take_out;
    UIImageView*img_red_tik_on_deliver;
    UITextField*txt_date;
    UIToolbar*keyboardToolbar_Date;
    NSDateFormatter*formatter;
    UIDatePicker*datePicker;
    UITextField*txt_time;
    
    
    UIToolbar*keyboardToolbar_time;
    NSDateFormatter*formattertime;
    UIDatePicker*datePickertime;
    AppDelegate*delegate;
    
    
    NSString*str_takoutbody;
    
    UIView*alertviewBg;
    
    NSMutableArray*array_temp2;
    
}


@end

@implementation AddToCartFoodLaterVC
@synthesize ary_details,calendar,enabledDates,str_comefrom;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    str_takoutbody = [NSString new];
    str_takoutbody = @"3";
    array_temp2 = [NSMutableArray new];
    
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBody];
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}
-(void)integrateHeader
{
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back addTarget:self action:@selector(btn_back_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_back];
    
    
    UILabel *lbl_dish_detail = [[UILabel alloc]init];
    lbl_dish_detail.frame = CGRectMake(CGRectGetMaxX(icon_back.frame)+5,0, WIDTH-90, 45);
    lbl_dish_detail.text = [NSString stringWithFormat:@"%@",[[ary_details objectAtIndex:0] valueForKey:@"DishName"]];
    lbl_dish_detail.font = [UIFont fontWithName:kFont size:13];
    lbl_dish_detail.textColor = [UIColor whiteColor];
    lbl_dish_detail.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_dish_detail];
    
    
    
    
    UIImageView *icon_logo = [[UIImageView alloc]init];
    icon_logo .frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_logo  setImage:[UIImage imageNamed:@"img_logo@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_logo  setUserInteractionEnabled:YES];
    [img_header addSubview:icon_logo ];
    
    
}
-(void)integrateBody
{
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor clearColor];
    //   scroll.frame = CGRectMake(0, 100, WIDTH, 450);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    
    [self.view addSubview:scroll];
    
    
    UIImageView *img_bg1 = [[UIImageView alloc]init];
    //   img_bg1 .frame = CGRectMake(0,CGRectGetMaxY(img_header.frame)+10, WIDTH, 250);
    //[img_bg1 setImage:[UIImage imageNamed:@"bg1@2x.png"]];
    img_bg1.backgroundColor = [UIColor clearColor];
    [img_bg1 setUserInteractionEnabled:YES];
    [ scroll addSubview:img_bg1 ];
    
    
    calendar = [[CKCalendarView alloc] initWithStartDay:startSunday];
    calendar.frame = CGRectMake(10, 0, WIDTH-20, 150);
    if (IS_IPHONE_6Plus)
    {
        calendar.frame = CGRectMake(0, 0, WIDTH-24, 150);
    }
    else if (IS_IPHONE_6)
    {
        calendar.frame = CGRectMake(0, 0, WIDTH-24, 150);
    }
    else if (IS_IPHONE_5)
    {
        calendar.frame = CGRectMake(0, 0, WIDTH-24, 150);
    }
    else
    {
        calendar.frame = CGRectMake(0, 0, WIDTH-24, 150);
    }
    self.calendar = calendar;
    calendar.delegate = self;
    calendar.backgroundColor = [UIColor clearColor];
    calendar.titleColor = [UIColor blackColor];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    //     minimumDate = [dateFormatter dateFromString:dateString];
    //    minimumDate = [dateFormatter dateFromString:@"22/02/1990"];
    calendar.onlyShowCurrentMonth = NO;
    calendar.adaptHeightToNumberOfWeeksInMonth = NO;
    calendar.titleFont = [UIFont fontWithName:kFont size:17.0];
    calendar.layer.borderWidth = 1.0;
    calendar.clipsToBounds=YES;
    [scroll addSubview:calendar];
    [calendar selectDate:[NSDate date] makeVisible:NO];
    //    ary_AvailableDates = self.enabledDates;
    [calendar reloadData];

   
    
    UILabel *lbl_serving_time = [[UILabel alloc]init];
    lbl_serving_time.text = @"Serving Time";
    lbl_serving_time.font = [UIFont fontWithName:kFont size:14];
    lbl_serving_time.textColor = [UIColor blackColor];
    lbl_serving_time.backgroundColor = [UIColor clearColor];
    [ scroll addSubview:lbl_serving_time];
    
    
    UILabel *lbl_serving_time1 = [[UILabel alloc]init];
    lbl_serving_time1.text = @"03:00:pm - 05:00:pm";
    lbl_serving_time1.font = [UIFont fontWithName:kFontBold size:14];
    lbl_serving_time1.textColor = [UIColor blackColor];
    lbl_serving_time1.backgroundColor = [UIColor clearColor];
    [ scroll addSubview:lbl_serving_time1];
    
    UILabel *lbl_serving_time2 = [[UILabel alloc]init];
    lbl_serving_time2.text = @"06:00:pm - 09:00:pm";
    lbl_serving_time2.font = [UIFont fontWithName:kFontBold size:14];
    lbl_serving_time2.textColor = [UIColor blackColor];
    lbl_serving_time2.backgroundColor = [UIColor clearColor];
    [scroll addSubview:lbl_serving_time2];
    
    
    UIImageView *img_bg2 = [[UIImageView alloc]init];
    //[img_bg2  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    img_bg2.backgroundColor = [UIColor clearColor];
    [img_bg2  setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg2 ];
    
    
    UILabel *lbl_kitchen_name = [[UILabel alloc]init];
    lbl_kitchen_name.text = @"Name of Kitchen :";
    lbl_kitchen_name.font = [UIFont fontWithName:kFont size:14];
    lbl_kitchen_name.textColor = [UIColor blackColor];
    lbl_kitchen_name.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:lbl_kitchen_name];

    
    UILabel *lbl_kitchen_nameval = [[UILabel alloc]init];
    lbl_kitchen_nameval.text = [NSString stringWithFormat:@"%@",[[[ary_details objectAtIndex:0] valueForKey:@"ChefDetail"] valueForKey:@"Kitchen_Name"]];
    lbl_kitchen_nameval.font = [UIFont fontWithName:kFontBold size:14];
    lbl_kitchen_nameval.textColor = [UIColor blackColor];
    lbl_kitchen_nameval.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:lbl_kitchen_nameval];
    
    
    UILabel *lbl_dish_name = [[UILabel alloc]init];
    lbl_dish_name.text= @"Dish/Meal Name:";
     lbl_dish_name.font = [UIFont fontWithName:kFont size:14];
    lbl_dish_name.textColor = [UIColor blackColor];
    lbl_dish_name.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:lbl_dish_name];
    
    
    UILabel *lbl_dish_nameval = [[UILabel alloc]init];
    lbl_dish_nameval.text= [NSString stringWithFormat:@"%@",[[ary_details objectAtIndex:0] valueForKey:@"DishName"]];
    lbl_dish_nameval.font = [UIFont fontWithName:kFont size:14];
    lbl_dish_nameval.textColor = [UIColor blackColor];
    lbl_dish_nameval.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:lbl_dish_nameval];

    
    UILabel *lbl_seving_qty = [[UILabel alloc]init];
    lbl_seving_qty .text = @"Available Serving Qty Now:";
    lbl_seving_qty .font = [UIFont fontWithName:kFont size:14];
    lbl_seving_qty .textColor = [UIColor blackColor];
    lbl_seving_qty .backgroundColor = [UIColor clearColor];
    [ img_bg2 addSubview:lbl_seving_qty ];
    
    UILabel *lbl_seving_qtyval = [[UILabel alloc]init];
    lbl_seving_qtyval .text = [NSString stringWithFormat:@"%@",[[ary_details objectAtIndex:0] valueForKey:@"quantity"]];
    lbl_seving_qtyval .font = [UIFont fontWithName:kFontBold size:14];
    lbl_seving_qtyval .textColor = [UIColor blackColor];
    lbl_seving_qtyval .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:lbl_seving_qtyval];
    
    
    UILabel *lbl_price = [[UILabel alloc]init];
    lbl_price  .text =@"Price/Serving: $"; ;
    lbl_price .font = [UIFont fontWithName:kFont size:14];
    lbl_price  .textColor = [UIColor blackColor];
    lbl_price  .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:lbl_price];
    
    UILabel *lbl_priceval = [[UILabel alloc]init];
    lbl_priceval .text =[NSString stringWithFormat:@"%@",[[ary_details objectAtIndex:0] valueForKey:@"DishPrice"]]; ;
    lbl_priceval.font = [UIFont fontWithName:kFontBold size:14];
    lbl_priceval.textColor = [UIColor blackColor];
    lbl_priceval.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:lbl_priceval];
    
    
    UIImageView *img_line = [[UIImageView alloc]init];
    [img_line setImage:[UIImage imageNamed:@"line2-img@2x.png"]];
    [img_line setUserInteractionEnabled:YES];
    [img_bg2 addSubview:img_line ];
    
    
    UILabel *lbl_serving_qty_order = [[UILabel alloc]init];
    lbl_serving_qty_order  .text = @"Serving Quantity Orderd:";
    lbl_serving_qty_order .font = [UIFont fontWithName:kFont size:14];
    lbl_serving_qty_order  .textColor = [UIColor blackColor];
    lbl_serving_qty_order  .backgroundColor = [UIColor clearColor];
    [ img_bg2 addSubview:lbl_serving_qty_order ];
    
    UIImageView *img_rect = [[UIImageView alloc]init];
    [img_rect setImage:[UIImage imageNamed:@"rectangle@2x.png"]];
    [img_rect setUserInteractionEnabled:YES];
    [img_bg2 addSubview:img_rect ];
    
    
    UIImageView *img_rect1 = [[UIImageView alloc]init];
    [img_rect1 setImage:[UIImage imageNamed:@"drop-down-icon@2x.png"]];
    [img_rect1 setUserInteractionEnabled:YES];
    [img_rect addSubview:img_rect1];
    
    
    UIButton *icon_drop_down = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_drop_down  .backgroundColor = [UIColor clearColor];
    [icon_drop_down  addTarget:self action:@selector(btn_drop_down_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_rect   addSubview:icon_drop_down ];
    
    
    lbl_qty_valu_inrect = [[UILabel alloc]init];
    //   lbl_qty_valu_inrect  .frame = CGRectMake(10,5,12,12);
    lbl_qty_valu_inrect.font = [UIFont fontWithName:kFont size:6];
    lbl_qty_valu_inrect  .textColor = [UIColor blackColor];
    lbl_qty_valu_inrect .backgroundColor = [UIColor clearColor];
    [img_rect  addSubview:lbl_qty_valu_inrect];
    
    UILabel *lbl_serving_type = [[UILabel alloc]init];
    lbl_serving_type .text = @"Select Serving Type";
    lbl_serving_type .font = [UIFont fontWithName:kFont size:14];
    lbl_serving_type  .textColor = [UIColor blackColor];
    lbl_serving_type.textAlignment = NSTextAlignmentCenter;
    lbl_serving_type  .backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:lbl_serving_type];
    
    btn_on_dine_in  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_dine_in.frame = CGRectMake(35,CGRectGetMaxY(lbl_serving_type .frame)+10, 60, 60);
    //[ btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in-bl@2x.png"] forState:UIControlStateSelected];
    [ btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in@2x.png"] forState:UIControlStateNormal];
    btn_on_dine_in.tag = 0;
    [ btn_on_dine_in addTarget:self action:@selector(btn_serving1_click:)forControlEvents:UIControlEventTouchUpInside];
    [img_bg2 addSubview:   btn_on_dine_in];
    
    img_red_tik_on_dine_in = [[UIImageView alloc]init];
    img_red_tik_on_dine_in.frame = CGRectMake(CGRectGetMidX(btn_on_dine_in.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+10,25,20);
    [img_red_tik_on_dine_in setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik_on_dine_in setUserInteractionEnabled:YES];
    [img_bg2 addSubview:img_red_tik_on_dine_in];
    img_red_tik_on_dine_in.hidden = YES;
    
    
    btn_on_take_out  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_take_out.frame = CGRectMake(CGRectGetMaxX(btn_on_dine_in.frame)+70,CGRectGetMaxY(lbl_serving_type .frame)+10,50,50);
    //[btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out-bl@2x.png"]forState:UIControlStateSelected];
    [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out@2x.png"]forState:UIControlStateNormal];
    btn_on_take_out.tag = 1;
    [btn_on_take_out addTarget:self action:@selector(btn_serving2_click:)forControlEvents:UIControlEventTouchUpInside];
    [img_bg2 addSubview:btn_on_take_out];
    
    img_red_tik_on_take_out = [[UIImageView alloc]init];
    img_red_tik_on_take_out.frame = CGRectMake(CGRectGetMidX(btn_on_take_out.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+10,25,20);
    [img_red_tik_on_take_out setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik_on_take_out setUserInteractionEnabled:YES];
    [img_bg2 addSubview:img_red_tik_on_take_out];
    img_red_tik_on_take_out.hidden = YES;
    
    
    btn_on_delever  = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_delever.frame = CGRectMake(CGRectGetMaxX( btn_on_take_out.frame)+70,CGRectGetMaxY(lbl_serving_type .frame)+10,50,50);
    //[btn_on_delever setImage:[UIImage imageNamed:@"img-delever-bl@2x.png"] forState:UIControlStateSelected];
    [btn_on_delever setImage:[UIImage imageNamed:@"img-delever@2x.png"] forState:UIControlStateNormal];
    btn_on_delever.tag = 2;
    [btn_on_delever addTarget:self action:@selector(btn_serving3_click:)forControlEvents:UIControlEventTouchUpInside];
    [img_bg2 addSubview:btn_on_delever];
    
    img_red_tik_on_deliver = [[UIImageView alloc]init];
    img_red_tik_on_deliver.frame = CGRectMake(CGRectGetMidX(btn_on_delever.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+10,25,20);
    [img_red_tik_on_deliver setImage:[UIImage imageNamed:@"icon-right-tik@2x.png"]];
    //   img_red_tik.backgroundColor = [UIColor redColor];
    [img_red_tik_on_deliver setUserInteractionEnabled:YES];
    [img_bg2 addSubview:img_red_tik_on_deliver];
    img_red_tik_on_deliver.hidden = YES;
    
    
    UILabel *lbl_serving_date_time = [[UILabel alloc]init];
    //   lbl_serving_date_time .frame = CGRectMake(40,CGRectGetMaxY(img_take_out .frame), 350, 40);
    lbl_serving_date_time .text = @"Select Serving Date and Time";
    lbl_serving_date_time .font = [UIFont fontWithName:kFont size:14];
    lbl_serving_date_time  .textColor = [UIColor blackColor];
    lbl_serving_date_time.textAlignment = NSTextAlignmentCenter;
    lbl_serving_date_time .backgroundColor = [UIColor clearColor];
    [ img_bg2 addSubview:lbl_serving_date_time];
    
    
//    UIButton *img_date = [UIButton buttonWithType:UIButtonTypeCustom];
//    //   img_date  .frame = CGRectMake(50,CGRectGetMaxY(lbl_serving_date_time .frame), 25, 25);
//    img_date .backgroundColor = [UIColor clearColor];
//    [img_date  addTarget:self action:@selector(btn_serving3_click:) forControlEvents:UIControlEventTouchUpInside];
//    [img_date  setImage:[UIImage imageNamed:@"date1-icon@2x.png"] forState:UIControlStateNormal];
//    [img_bg2   addSubview:img_date ];
//    
//    UIButton *img_time = [UIButton buttonWithType:UIButtonTypeCustom];
//    //    img_time.frame = CGRectMake(200,CGRectGetMaxY(lbl_serving_date_time.frame), 25, 25);
//    img_time.backgroundColor = [UIColor clearColor];
//    [img_time addTarget:self action:@selector(btn_serving3_click:) forControlEvents:UIControlEventTouchUpInside];
//    [img_time setImage:[UIImage imageNamed:@"time-icon@2x.png"] forState:UIControlStateNormal];
//    [img_bg2   addSubview:img_time];
//    
//    UIImageView *img_line1 = [[UIImageView alloc]init];
//    //   img_line1 .frame = CGRectMake(85,CGRectGetMaxY(lbl_serving_date_time.frame)+21, 50, 0.5);
//    [img_line1 setImage:[UIImage imageNamed:@"line2@2x.png"]];
//    //   icon_user.backgroundColor = [UIColor redColor];
//    [img_line1 setUserInteractionEnabled:YES];
//    [img_bg2 addSubview:img_line1 ];
//    
//    UIImageView *img_line2 = [[UIImageView alloc]init];
//    //   img_line2 .frame = CGRectMake(235,CGRectGetMaxY(lbl_serving_date_time.frame)+21, 45, 0.5);
//    [img_line2 setImage:[UIImage imageNamed:@"line2@2x.png"]];
//    //   icon_user.backgroundColor = [UIColor redColor];
//    [img_line2 setUserInteractionEnabled:YES];
//    [img_bg2 addSubview:img_line2 ];
//    
//    UILabel *lbl_serving_date = [[UILabel alloc]init];
//    //   lbl_serving_date .frame = CGRectMake(CGRectGetMaxX(img_date .frame)+5,CGRectGetMaxY(lbl_serving_date_time.frame)-10, 350, 40);
//    lbl_serving_date .text = @"19/07/2015";
//    lbl_serving_date.font = [UIFont fontWithName:kFont size:2];
//    lbl_serving_date  .textColor = [UIColor blackColor];
//    lbl_serving_date .backgroundColor = [UIColor clearColor];
//    [ img_bg2 addSubview:lbl_serving_date];
//    
//    UILabel *lbl_serve_time = [[UILabel alloc]init];
//    //    lbl_serve_time .frame = CGRectMake(CGRectGetMaxX(img_time .frame)+5,CGRectGetMaxY(lbl_serving_date_time.frame)-10, 350, 40);
//    lbl_serve_time.text = @"17:00";
//    lbl_serve_time .font = [UIFont fontWithName:@"Howelvitica" size:5];
//    lbl_serve_time .textColor = [UIColor blackColor];
//    lbl_serve_time .backgroundColor = [UIColor clearColor];
//    [img_bg2 addSubview:lbl_serve_time];
    
    
    
    
    
    
    UIImageView *img_calender = [[UIImageView alloc]init];
    img_calender.frame = CGRectMake(40,5, 30, 30);
    [img_calender setImage:[UIImage imageNamed:@"img-calender@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_calender setUserInteractionEnabled:YES];
    [img_bg2 addSubview:img_calender];
    
    
    txt_date = [[UITextField alloc] init];
    txt_date.borderStyle = UITextBorderStyleNone;
    txt_date.placeholder = @"dd-mm-yyyy";
    txt_date.font = [UIFont systemFontOfSize:14.0];
    [txt_date setValue:[UIFont fontWithName:kFont size: 14] forKeyPath:@"_placeholderLabel.font"];
    txt_date.textColor = [UIColor blackColor];
    // txt_Date.font = [UIFont systemFontOfSize:15.0];
    //txt_date.background = [UIColor redColor];
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 4, 20)];
    txt_date.leftView = paddingView1;
    txt_date.leftViewMode = UITextFieldViewModeAlways;
    txt_date.autocorrectionType = UITextAutocorrectionTypeNo;
    //   txt_Date.keyboardType = UIKeyboardTypeDefault;
    txt_date.clearButtonMode = UITextFieldViewModeWhileEditing;
    txt_date.delegate = self;
    txt_date.userInteractionEnabled = YES;
    [txt_date setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    // [txt_Date setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [img_bg2 addSubview:txt_date];
    
    if (keyboardToolbar_Date == nil)
    {
        keyboardToolbar_Date = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar_Date setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_DoneDate:)];
        [keyboardToolbar_Date setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    txt_date.inputAccessoryView = keyboardToolbar_Date;
    txt_date.backgroundColor=[UIColor clearColor];
    
    //   txt_DateBirth = txt_date_of_birth;
    
    formatter = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    //datePicker.minimumDate = [formatter dateFromString:@"1900-01-01"];
    
    
    NSDate *currentDate = [NSDate date];
    NSCalendar *calendard = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendard components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate];
    components.day=components.day;
    components.month=components.month;
    components.year = components.year;
    //[components setYear:0];
    
    //NSString *temp=[NSString stringWithFormat:@" Day:%d Months:%d Year:%ld",(long)components.day,(long)components.month,(long)components.year];
    //    NSLog(@"%@",temp);
    //    NSString *string = [NSString stringWithFormat:@"%ld.%ld.%ld", (long)components.day, (long)components.month, (long)components.year];
    // NSDate *minDate=[calendar dateByAddingComponents:components toDate:currentDate  options:0];
    NSDate *minDate=  [formatter dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components.year, (long)components.month,(long)components.day]];
    
    NSLog(@"Minimum date is :: %@",minDate);
    
    //[components setYear:20];
    
    //NSDate *maxDate = [calendar dateByAddingComponents:components toDate:currentDate  options:0];
    
    NSDate *maxDate=  [formatter dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components.year+20, (long)components.month,(long)components.day]];
    
    //   datePicker.maximumDate= [formatter dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components.year-20, (long)components.month,(long)components.day]];
    
    NSLog(@"Maximum date is :: %@",maxDate);
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [datePicker setMinimumDate:currentDate];
    [datePicker setMaximumDate:maxDate];
    [datePicker setDate:currentDate];
    txt_date.inputView = datePicker;
    
    
    
    UIImageView *img_clock = [[UIImageView alloc]init];
    img_clock.frame = CGRectMake(CGRectGetMaxX(txt_date.frame)+10,5, 30, 30);
    [img_clock setImage:[UIImage imageNamed:@"img-clock@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_clock setUserInteractionEnabled:YES];
    [img_bg2 addSubview:img_clock];
    
    
    NSDate *currDate = [NSDate date];
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
    [dateFormatter1 setDateFormat:@"hh:mm"];
    [dateFormatter1 setAMSymbol:@"am"];
    [dateFormatter1 setPMSymbol:@"pm"];
    [dateFormatter1 setTimeStyle: NSDateFormatterShortStyle];
    NSString*dateString;
    
    dateString = [dateFormatter1 stringFromDate:currDate];
    
    txt_time = [[UITextField alloc] init];
    txt_time.borderStyle = UITextBorderStyleNone;
    txt_time.placeholder = @"time";
    txt_time.textColor = [UIColor blackColor];
    txt_time.font = [UIFont systemFontOfSize:13.0];
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 4, 20)];
    txt_time.leftView = paddingView2;
    txt_time.leftViewMode = UITextFieldViewModeAlways;
    txt_time.autocorrectionType = UITextAutocorrectionTypeNo;
    //   txt_Date.keyboardType = UIKeyboardTypeDefault;
    txt_time.clearButtonMode = UITextFieldViewModeWhileEditing;
    txt_time.delegate = self;
    txt_time.userInteractionEnabled = YES;
    txt_time.text = dateString;
    
    [txt_time setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    [txt_time setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [img_bg2 addSubview:txt_time];
    
    if (keyboardToolbar_time == nil)
    {
        keyboardToolbar_time = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar_time setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_Donetime:)];
        [keyboardToolbar_time setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    txt_time.inputAccessoryView = keyboardToolbar_time;
    txt_time.backgroundColor=[UIColor clearColor];
    
    datePickertime = [[UIDatePicker alloc] init];
    datePickertime.datePickerMode = UIDatePickerModeTime;
    [datePickertime addTarget:self action:@selector(timedatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    txt_time.inputView = datePickertime;
    
    formattertime = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"yyyy-MM-dd"];
    [formattertime setDateFormat:@"hh:mm a"];
    //datePicker.minimumDate = [formatter dateFromString:@"1900-01-01"];
    
    NSDate *now1 = [NSDate date];
    NSCalendar *calendar1 = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components1 = [calendar1 components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now1];
    components1.year = components1.year  ;
    
    datePickertime.minimumDate = now1;
    
    datePickertime.maximumDate = [formattertime dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components1.year+20, (long)components1.month,(long)components1.day]];
    
    
    
    
    UIButton *img_add_to_cart = [UIButton buttonWithType:UIButtonTypeCustom];
    //   img_add_to_cart .frame = CGRectMake(-10,CGRectGetMaxY( img_bg2 .frame), WIDTH+19, 35);
    img_add_to_cart .backgroundColor = [UIColor clearColor];
    [img_add_to_cart  addTarget:self action:@selector(btn_add_to_cart_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_add_to_cart setImage:[UIImage imageNamed:@"cart-img@2x.png"] forState:UIControlStateNormal];
    [scroll  addSubview:img_add_to_cart];
    UILabel *lbl_add_to_cart = [[UILabel alloc]init];
    lbl_add_to_cart.frame = CGRectMake(100,5, 300, 20);
    lbl_add_to_cart .text = @"ADD TO CART";
    lbl_add_to_cart.textAlignment = NSTextAlignmentCenter;
    lbl_add_to_cart.font = [UIFont fontWithName:kFont size:15];
    lbl_add_to_cart .textColor = [UIColor whiteColor];
    lbl_add_to_cart .backgroundColor = [UIColor clearColor];
    [ img_add_to_cart addSubview:lbl_add_to_cart];
    
    
    tableview_Kcountry = [[UITableView alloc]init];
    [tableview_Kcountry setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tableview_Kcountry.delegate = self;
    tableview_Kcountry.dataSource = self;
    tableview_Kcountry.showsVerticalScrollIndicator = NO;
    tableview_Kcountry.backgroundColor = [UIColor whiteColor];
    tableview_Kcountry.layer.borderColor = [[UIColor blackColor]CGColor];
    tableview_Kcountry.layer.borderWidth = 1.0f;
    tableview_Kcountry.clipsToBounds = YES;
    tableview_Kcountry.hidden =YES;
    [img_bg2 addSubview:tableview_Kcountry];

    
    if (IS_IPHONE_6Plus)
    {
        scroll.frame = CGRectMake(0, 50, WIDTH, HEIGHT);
        img_bg1 .frame = CGRectMake(0,0, WIDTH, 320);
        calendar.frame = CGRectMake(12,0, WIDTH-24, 300);
        lbl_serving_time.frame = CGRectMake(25,CGRectGetMaxY(img_bg1.frame)+10, 100, 20);
        lbl_serving_time1.frame = CGRectMake(CGRectGetMaxX(lbl_serving_time.frame),CGRectGetMaxY(img_bg1.frame)+10, WIDTH-135, 20);
        lbl_serving_time2.frame = CGRectMake(CGRectGetMaxX(lbl_serving_time.frame),CGRectGetMaxY(lbl_serving_time.frame),WIDTH-135,20);
        
        img_bg2 .frame = CGRectMake(0,CGRectGetMaxY(lbl_serving_time.frame)+20, WIDTH+15, 370);
        lbl_kitchen_name.frame = CGRectMake(35,30, 140, 20);
        lbl_kitchen_nameval.frame = CGRectMake(CGRectGetMaxX(lbl_kitchen_name.frame),30, WIDTH-180, 20);
        
        lbl_dish_name.frame = CGRectMake(35,CGRectGetMaxY(lbl_kitchen_name.frame)+10, 120, 20);
        lbl_dish_nameval.frame = CGRectMake(CGRectGetMaxX(lbl_dish_name.frame),CGRectGetMaxY(lbl_kitchen_name.frame)+10, WIDTH-160, 20);
        
        lbl_seving_qty .frame = CGRectMake(35,CGRectGetMaxY(lbl_dish_nameval.frame)+10, 190, 20);
        lbl_seving_qtyval.frame = CGRectMake(CGRectGetMaxX(lbl_seving_qty.frame),CGRectGetMaxY(lbl_dish_nameval.frame)+10, WIDTH-230, 20);
        
        lbl_price.frame = CGRectMake(35,CGRectGetMaxY(lbl_seving_qty.frame)+10, 120, 20);
        lbl_priceval.frame = CGRectMake(CGRectGetMaxX(lbl_price.frame),CGRectGetMaxY(lbl_seving_qty.frame)+10, WIDTH-170, 20);
        
        img_line .frame = CGRectMake(18,CGRectGetMaxY(lbl_price.frame)+10, WIDTH-35, 0.5);
        lbl_serving_qty_order  .frame = CGRectMake(35,CGRectGetMaxY(img_line.frame)+10, 350, 20);
        img_rect .frame = CGRectMake(230,CGRectGetMaxY(img_line.frame)+10, 50, 20);
        img_rect1.frame = CGRectMake(32,7,12,10);
        
        icon_drop_down .frame = CGRectMake(0,0, 50, 20);
        lbl_qty_valu_inrect  .frame = CGRectMake(10,5,12,12);
        lbl_serving_type .frame = CGRectMake(0,CGRectGetMaxY(lbl_serving_qty_order .frame)+20, WIDTH, 20);
        btn_on_dine_in.frame = CGRectMake(70,CGRectGetMaxY(lbl_serving_type .frame)+10, 50, 50);
        img_red_tik_on_dine_in.frame = CGRectMake(CGRectGetMidX(btn_on_dine_in.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+20,25,20);
        btn_on_take_out.frame = CGRectMake(CGRectGetMaxX(btn_on_dine_in.frame)+50,CGRectGetMaxY(lbl_serving_type .frame)+10,50,50);
        img_red_tik_on_take_out.frame = CGRectMake(CGRectGetMidX(btn_on_take_out.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+20,25,20);
        btn_on_delever.frame = CGRectMake(CGRectGetMaxX( btn_on_take_out.frame)+50,CGRectGetMaxY(lbl_serving_type .frame)+10,50,50);
        img_red_tik_on_deliver.frame = CGRectMake(CGRectGetMidX(btn_on_delever.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+20,25,20);
        
        
        lbl_serving_date_time .frame = CGRectMake(0,CGRectGetMaxY(btn_on_delever .frame), WIDTH, 40);
        
        img_calender.frame = CGRectMake(40,CGRectGetMaxY(lbl_serving_date_time .frame)+10, 18, 18);
        txt_date .frame = CGRectMake(CGRectGetMaxX(img_calender.frame),CGRectGetMaxY(lbl_serving_date_time .frame)+5,WIDTH/2-60, 30);
        img_clock.frame = CGRectMake(CGRectGetMaxX(txt_date.frame)+10,CGRectGetMaxY(lbl_serving_date_time .frame)+10, 18, 18);
        txt_time .frame = CGRectMake(CGRectGetMaxX(img_clock.frame),CGRectGetMaxY(lbl_serving_date_time .frame)+5, WIDTH/2-70, 30);
        
        img_add_to_cart .frame = CGRectMake(0,CGRectGetMaxY( img_bg2 .frame)+30, WIDTH, 60);
        lbl_add_to_cart.frame = CGRectMake(0,5, WIDTH, 50);
        tableview_Kcountry.frame=CGRectMake(220, CGRectGetMaxY(img_rect.frame)+5, 70, 140);
        [scroll setContentSize:CGSizeMake(0,HEIGHT+120)];

    }
    else{
        scroll.frame = CGRectMake(0, 50, WIDTH, HEIGHT);
        img_bg1 .frame = CGRectMake(0,0, WIDTH, 290);
        calendar.frame = CGRectMake(12,0, WIDTH-24, 300);
        lbl_serving_time.frame = CGRectMake(25,CGRectGetMaxY(img_bg1.frame)+10, 100, 20);
        lbl_serving_time1.frame = CGRectMake(CGRectGetMaxX(lbl_serving_time.frame),CGRectGetMaxY(img_bg1.frame)+10, WIDTH-135, 20);
        lbl_serving_time2.frame = CGRectMake(CGRectGetMaxX(lbl_serving_time.frame),CGRectGetMaxY(lbl_serving_time.frame),WIDTH-135,20);
        
        img_bg2 .frame = CGRectMake(0,CGRectGetMaxY(lbl_serving_time.frame)+20, WIDTH+15, 370);
        lbl_kitchen_name.frame = CGRectMake(35,30, 140, 20);
        lbl_kitchen_nameval.frame = CGRectMake(CGRectGetMaxX(lbl_kitchen_name.frame),30, WIDTH-180, 20);
        
        lbl_dish_name.frame = CGRectMake(35,CGRectGetMaxY(lbl_kitchen_name.frame)+10, 120, 20);
        lbl_dish_nameval.frame = CGRectMake(CGRectGetMaxX(lbl_dish_name.frame),CGRectGetMaxY(lbl_kitchen_name.frame)+10, WIDTH-160, 20);
        
        lbl_seving_qty .frame = CGRectMake(35,CGRectGetMaxY(lbl_dish_nameval.frame)+10, 190, 20);
        lbl_seving_qtyval.frame = CGRectMake(CGRectGetMaxX(lbl_seving_qty.frame),CGRectGetMaxY(lbl_dish_nameval.frame)+10, WIDTH-230, 20);
        
        lbl_price.frame = CGRectMake(35,CGRectGetMaxY(lbl_seving_qty.frame)+10, 120, 20);
        lbl_priceval.frame = CGRectMake(CGRectGetMaxX(lbl_price.frame),CGRectGetMaxY(lbl_seving_qty.frame)+10, WIDTH-170, 20);
        
        img_line .frame = CGRectMake(18,CGRectGetMaxY(lbl_price.frame)+10, WIDTH-35, 0.5);
        lbl_serving_qty_order  .frame = CGRectMake(35,CGRectGetMaxY(img_line.frame)+10, 350, 20);
        img_rect .frame = CGRectMake(230,CGRectGetMaxY(img_line.frame)+10, 50, 20);
        img_rect1.frame = CGRectMake(32,7,12,10);
        
        icon_drop_down .frame = CGRectMake(0,0, 50, 20);
        lbl_qty_valu_inrect  .frame = CGRectMake(10,5,12,12);
        lbl_serving_type .frame = CGRectMake(0,CGRectGetMaxY(lbl_serving_qty_order .frame)+20, WIDTH, 20);
        btn_on_dine_in.frame = CGRectMake(35,CGRectGetMaxY(lbl_serving_type .frame)+10, 50, 50);
        img_red_tik_on_dine_in.frame = CGRectMake(CGRectGetMidX(btn_on_dine_in.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+20,25,20);
        btn_on_take_out.frame = CGRectMake(CGRectGetMaxX(btn_on_dine_in.frame)+50,CGRectGetMaxY(lbl_serving_type .frame)+10,50,50);
        img_red_tik_on_take_out.frame = CGRectMake(CGRectGetMidX(btn_on_take_out.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+20,25,20);
        btn_on_delever.frame = CGRectMake(CGRectGetMaxX( btn_on_take_out.frame)+50,CGRectGetMaxY(lbl_serving_type .frame)+10,50,50);
        img_red_tik_on_deliver.frame = CGRectMake(CGRectGetMidX(btn_on_delever.frame)+5,CGRectGetMaxY(lbl_serving_type .frame)+20,25,20);
        
        
        lbl_serving_date_time .frame = CGRectMake(0,CGRectGetMaxY(btn_on_delever .frame), WIDTH, 40);
        
        img_calender.frame = CGRectMake(40,CGRectGetMaxY(lbl_serving_date_time .frame)+10, 18, 18);
        txt_date .frame = CGRectMake(CGRectGetMaxX(img_calender.frame),CGRectGetMaxY(lbl_serving_date_time .frame)+5,WIDTH/2-60, 30);
        img_clock.frame = CGRectMake(CGRectGetMaxX(txt_date.frame)+10,CGRectGetMaxY(lbl_serving_date_time .frame)+10, 18, 18);
        txt_time .frame = CGRectMake(CGRectGetMaxX(img_clock.frame),CGRectGetMaxY(lbl_serving_date_time .frame)+5, WIDTH/2-70, 30);
        
        img_add_to_cart .frame = CGRectMake(0,CGRectGetMaxY( img_bg2 .frame)+30, WIDTH, 60);
        lbl_add_to_cart.frame = CGRectMake(0,5, WIDTH, 50);
        tableview_Kcountry.frame=CGRectMake(220, CGRectGetMaxY(img_rect.frame)+5, 70, 140);
        [scroll setContentSize:CGSizeMake(0,HEIGHT+300)];


    }
    
}

//    else if (IS_IPHONE_6)
//    {


//    else
//    {
//        scroll.frame = CGRectMake(0, 100, WIDTH, 450);
//        img_bg1 .frame = CGRectMake(0,CGRectGetMaxY(img_header.frame)+10, WIDTH, 250);
//        calendar.frame = CGRectMake(12, CGRectGetMaxY(img_header.frame)+10, WIDTH-24, 200);
//
//        lbl_serving_time.frame = CGRectMake(25,CGRectGetMaxY(img_bg1.frame)+10, 350, 20);
//        lbl_serving_time2.frame = CGRectMake(130,CGRectGetMaxY(lbl_serving_time.frame), 350, 20);
//        img_bg2 .frame = CGRectMake(0,CGRectGetMaxY(lbl_serving_time.frame)+20,IS_IPHONE_5? WIDTH+10:WIDTH, 370);
//        lbl_kitchen_name.frame = CGRectMake(18,20, 350, 20);
//        lbl_dish_name.frame = CGRectMake(18,CGRectGetMaxY(lbl_kitchen_name.frame)+5, 350, 20);
//        lbl_seving_time .frame = CGRectMake(18,CGRectGetMaxY(lbl_dish_name.frame)+5, 350, 20);
//        lbl_seving_qty .frame = CGRectMake(18,CGRectGetMaxY(lbl_seving_time.frame)+5, 350, 20);
//        lbl_price  .frame = CGRectMake(18,CGRectGetMaxY(lbl_seving_time.frame)+5, 350, 20);
//        img_line .frame = CGRectMake(13,CGRectGetMaxY(lbl_price.frame)+5, 275, 0.5);
//        lbl_serving_qty_order  .frame = CGRectMake(18,CGRectGetMaxY(img_line.frame)+5, 350, 20);
//        img_rect .frame = CGRectMake(220,CGRectGetMaxY(img_line.frame)+10, 50, 20);
//        img_rect1 .frame = CGRectMake(32,7,12,10);
//        icon_drop_down .frame = CGRectMake(0,0, 50, 20);
//        
//        lbl_qty_valu_inrect  .frame = CGRectMake(10,5,12,12);
//        lbl_serving_type .frame = CGRectMake(75,CGRectGetMaxY(lbl_serving_qty_order .frame)+10, 350, 20);
//        img_dine_in .frame = CGRectMake(45,CGRectGetMaxY(lbl_serving_type .frame)+10, 60, 60);
//        img_take_out .frame = CGRectMake(125,CGRectGetMaxY(lbl_serving_type .frame)+10, 60, 60);
//        img_tick .frame = CGRectMake(140,CGRectGetMaxY(lbl_serving_type .frame)+40, 30, 30);
//        img_deliver .frame = CGRectMake(210,CGRectGetMaxY(lbl_serving_type .frame)+10, 60, 60);
//        lbl_serving_date_time .frame = CGRectMake(40,CGRectGetMaxY(img_take_out .frame), 350, 40);
//        img_date  .frame = CGRectMake(50,CGRectGetMaxY(lbl_serving_date_time .frame), 25, 25);
//        img_time.frame = CGRectMake(200,CGRectGetMaxY(lbl_serving_date_time.frame), 25, 25);
//        img_line1 .frame = CGRectMake(85,CGRectGetMaxY(lbl_serving_date_time.frame)+21, 50, 0.5);
//        img_line2 .frame = CGRectMake(235,CGRectGetMaxY(lbl_serving_date_time.frame)+21, 45, 0.5);
//        lbl_serving_date .frame = CGRectMake(CGRectGetMaxX(img_date .frame)+5,CGRectGetMaxY(lbl_serving_date_time.frame)-10, 350, 40);
//        lbl_serve_time .frame = CGRectMake(CGRectGetMaxX(img_time .frame)+5,CGRectGetMaxY(lbl_serving_date_time.frame)-10, 350, 40);
//        img_add_to_cart .frame = CGRectMake(-10,CGRectGetMaxY( img_bg2 .frame), WIDTH+19, 35);
//        tableview_Kcountry.frame=CGRectMake(220, CGRectGetMaxY(img_rect.frame)+5, 70, 140);
//
//    }
    

#pragma mark --CalenderMethod--

- (void)localeDidChange {
    [self.calendar setLocale:[NSLocale currentLocale]];
}

- (BOOL)dateIsDisabled:(NSDate *)date
{
    for (NSDate *disabledDate in self.enabledDates)
    {
        if ([disabledDate isEqualToDate:date])
        {
            return YES;
        }
    }
    return NO;
}



#pragma mark - CKCalendarDelegate
- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)myDate
{
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd/MM/yyyy"];
    
    NSString *str_SelectedDate = [format stringFromDate:myDate];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tableview_Kcountry) {
        
        int total = [[[ary_details objectAtIndex:0] valueForKey:@"Total_Serving"] intValue];
        
        if (total>0)
        {
             return total;
        }
        else{
             return 11;
        }
        
       
        
    }else {
        return 0;
    }
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"LazyTableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor=[UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    
    if (tableView==tableview_Kcountry)
    {
        UILabel  * lbl_list = [[UILabel alloc]init];
        lbl_list.frame=CGRectMake(0, 0, 70, 25);
        lbl_list.backgroundColor=[UIColor clearColor];
        lbl_list.textColor=[UIColor blackColor];
        lbl_list.textAlignment= NSTextAlignmentCenter;
        lbl_list.font = [UIFont fontWithName:kFont size:12];
        lbl_list.text=[NSString stringWithFormat:@"%ld",indexPath.row+1];
        [cell.contentView addSubview:lbl_list];
        
        
    }
    else{
        
        
    }
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 25;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tableview_Kcountry) {
        
        lbl_qty_valu_inrect.text =[NSString stringWithFormat:@"%d",indexPath.row+1];
        [tableview_Kcountry setHidden:YES];
        
        
    }
}

-(void) click_DoneDate:(id) sender
{
    [formatter setDateFormat:@"dd/MM/yyyy"];
    txt_date.font = [UIFont fontWithName:kFont size:14];
    txt_date.textColor = [UIColor blackColor];
    
    [txt_date setText:[formatter stringFromDate:datePicker.date]];
    [self.view endEditing:YES];
}

- (void)datePickerValueChanged:(id)sender
{
    [formatter setDateFormat:@"dd/MM/yyyy"];
    txt_date.font = [UIFont fontWithName:kFont size:14];
    txt_date.textColor = [UIColor blackColor];
    
    [txt_date setText:[formatter stringFromDate:datePicker.date]];
    [self.view endEditing:YES];
}
#pragma mark - Time picker

- (void)timedatePickerValueChanged:(id)sender
{
    [formattertime setDateFormat:@"hh:mm a"];
    [formattertime setTimeStyle: NSDateFormatterShortStyle];
    NSString*dateString;
    dateString = [formattertime stringFromDate:datePickertime.date];
    [txt_time setText:dateString];
    
}

-(void) click_Donetime:(id) sender
{
    if (txt_time.text.length == 0)
        [formattertime setDateFormat:@"hh:mm a"];
    [formattertime setTimeStyle: NSDateFormatterShortStyle];
    NSString*dateString;
    dateString = [formattertime stringFromDate:datePickertime.date];
    [txt_time setText:dateString];
    
    [self.view endEditing:YES];
}



-(void)btn_back_click:(UIButton *)sender
{
    NSLog(@"icon_back_click Btn Click");
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)btn_drop_down_click:(UIButton *)sender
{
    NSLog(@"icon_drop_down_click Btn Click");
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        tableview_Kcountry.hidden=NO;
        
    }
    else
    {
        [sender setSelected:NO];
        tableview_Kcountry.hidden=YES;
    }

    
}
-(void)btn_serving1_click:(UIButton *)sender
{
    if([sender isSelected])
    {
        [sender setSelected:NO];
        str_takoutbody = @"3";
        
        // [btn_on_dine_in setBackgroundImage:[UIImage imageNamed:@"img-dine-in@2x"] forState:UIControlStateNormal];
        [btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in@2x"] forState:UIControlStateNormal];
        img_red_tik_on_dine_in.hidden = YES;
        
    }
    else
    {
        
        str_takoutbody = @"0";
        [sender setSelected:YES];
        img_red_tik_on_take_out.hidden = YES;
        img_red_tik_on_deliver.hidden = YES;
        img_red_tik_on_dine_in.hidden = NO;
        
        [btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in-bl@2x"] forState:UIControlStateNormal];
        
        [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out@2x"] forState:UIControlStateNormal];
        
        [btn_on_delever setImage:[UIImage imageNamed:@"img-delever@2x.png"] forState:UIControlStateNormal];
        
        
        // [btn_on_dine_in setBackgroundImage:[UIImage imageNamed:@"img-dine-in-bl@2x"] forState:UIControlStateNormal];
        //[btn_on_take_out setBackgroundImage:[UIImage imageNamed:@"img-take-out@2x"] forState:UIControlStateNormal];
        // [btn_on_delever setBackgroundImage:[UIImage imageNamed:@"img-delever@2x"] forState:UIControlStateNormal];
        
    }
    
    
}
-(void)btn_serving2_click:(UIButton *)sender
{
    
    NSLog(@"TakeOut Button Clicked");
    if([sender isSelected])
    {
        [sender setSelected:NO];
        str_takoutbody = @"3";
        
        // [btn_on_take_out setBackgroundImage:[UIImage imageNamed:@"img-take-out@2x"] forState:UIControlStateNormal];
        
        [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out@2x"] forState:UIControlStateNormal];
        img_red_tik_on_take_out.hidden = YES;
        
        
    }
    else
    {
        img_red_tik_on_take_out.hidden = NO;
        img_red_tik_on_deliver.hidden = YES;
        img_red_tik_on_dine_in.hidden = YES;

        str_takoutbody = @"1";
        [btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in@2x"] forState:UIControlStateNormal];
        
        [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out-bl@2x"] forState:UIControlStateNormal];
        
        [btn_on_delever setImage:[UIImage imageNamed:@"img-delever@2x"] forState:UIControlStateNormal];
        
        //  [btn_on_dine_in setBackgroundImage:[UIImage imageNamed:@"img-dine-in@2x"] forState:UIControlStateNormal];
        //[btn_on_take_out setBackgroundImage:[UIImage imageNamed:@"img-take-out-bl@2x"] forState:UIControlStateNormal];
        // [btn_on_delever setBackgroundImage:[UIImage imageNamed:@"img-delever@2x"] forState:UIControlStateNormal];
        [sender setSelected:YES];
        
    }
    
}

-(void)btn_serving3_click:(UIButton *)sender
{
    
    NSLog(@"Delevery Button Clicked");
    //    SelectAll_btnClick
    
    if([sender isSelected])
    {
        [sender setSelected:NO];
        str_takoutbody = @"3";
        
        //  [btn_on_delever setBackgroundImage:[UIImage imageNamed:@"img-delever@2x"] forState:UIControlStateNormal];
        [btn_on_delever setImage:[UIImage imageNamed:@"img-delever@2x"] forState:UIControlStateNormal];
        img_red_tik_on_deliver.hidden = YES;
        
    }
    else
    {
        
        str_takoutbody = @"2";
        
        img_red_tik_on_take_out.hidden = YES;
        img_red_tik_on_deliver.hidden = NO;
        img_red_tik_on_dine_in.hidden = YES;
        
        [btn_on_dine_in setImage:[UIImage imageNamed:@"img-dine-in@2x"] forState:UIControlStateNormal];
        
        [btn_on_take_out setImage:[UIImage imageNamed:@"img-take-out@2x"] forState:UIControlStateNormal];
        
        [btn_on_delever setImage:[UIImage imageNamed:@"img-delever-bl@2x"] forState:UIControlStateNormal];
        
        // [btn_on_dine_in setBackgroundImage:[UIImage imageNamed:@"img-dine-in@2x"] forState:UIControlStateNormal];
        // [btn_on_take_out setBackgroundImage:[UIImage imageNamed:@"img-take-out@2x"] forState:UIControlStateNormal];
        //  [btn_on_delever setBackgroundImage:[UIImage imageNamed:@"img-delever-bl@2x"] forState:UIControlStateNormal];
        
        [sender setSelected:YES];
        
    }
    
}
//
-(void)btn_add_to_cart_click:(UIButton *)sender
{
    NSLog(@"icon_add_to_cart_click Btn Click");
    
    
    if (lbl_qty_valu_inrect.text.length<=0)
    {
        [self popup_Alertview:@"plesae select quantity"];
        
    }
    else if ([[NSString stringWithFormat:@"%@",str_takoutbody]isEqualToString:@"3"])
    {
        [self popup_Alertview:@"plesae select servingtype"];
        
    }
    else
    {
        [self AFUserAddtoCart];
        
    }
    //    DiningCartVC*vc = [[DiningCartVC alloc]init];
    //    [self presentViewController:vc animated:NO completion:nil];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}


#pragma UserAddtoCart-functionality

-(void)AFUserAddtoCart
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    NSMutableString *str_servingtype = [[NSMutableString alloc] init];
    
    for (int i=0; i<[array_temp2 count]; i++)
    {
        
        
        if (i==[array_temp2 count]-1)
        {
            [str_servingtype  appendString:[NSString stringWithFormat:@"%@",[array_temp2 objectAtIndex:i]]];
        }
        else
        {
            [str_servingtype  appendString:[NSString stringWithFormat:@"%@,",[array_temp2 objectAtIndex:i]]];
        }
        
        
    }
    
    
    
    NSDictionary *params =@{
                            
                            @"uid"                          :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"item_id"                        :  [[ary_details objectAtIndex:0] valueForKey:@"DishID"],
                            @"quantity"                       :  lbl_qty_valu_inrect.text,
                            @"price"                          :  [[ary_details objectAtIndex:0] valueForKey:@"DishPrice"],
                            @"serving_type"                   :  str_takoutbody,
                            @"type"                           :  @"1"
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:kUserAddtoCart  parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseUserAddtoCart:JSON];
    }
     
     
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error){
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFUserAddtoCart];
                                         }
                                     }];
    [operation start];
    
}
-(void)ResponseUserAddtoCart:(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
}



#pragma mark Alertview Popup


-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
