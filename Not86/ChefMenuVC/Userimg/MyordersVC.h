//
//  MyordersVC.h
//  Not86
//
//  Created by Admin on 30/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"


@interface MyordersVC : JWSlideMenuViewController
@property UICollectionView * collView_order_on_request;
@property UICollectionView * collView_in_progress;
@property UICollectionView * collView_order_cancelled;
@property UICollectionView * collView_order_completed;
@end
