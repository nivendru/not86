//
//  MyordersVC.m
//  Not86
//
//  Created by Admin on 30/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "MyordersVC.h"
#import "Define.h"
#import "UserSupportVC.h"
#import "JWSlideMenuViewController.h"
#import "JWSlideMenuController.h"
#import "AppDelegate.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"


@interface MyordersVC ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UITextFieldDelegate>
{
    UITableView*img_table;
    UIImageView * img_header;
    UITableView * table_on_drop_down;
    UILabel *lbl_User_Sign_Up;
    NSMutableArray * array_names_in_orders;
    UIImageView * img_bg_for_first_tbl;
    UILabel * lbl_orders;
    
    UIScrollView *scroll;
    UIView  *view_for_orderonrequest;
    
    NSMutableArray * array_requested_on;
    NSMutableArray * array_Request_date_time;
    NSMutableArray * array_dish_imges;
    
    NSMutableArray * array_cuisine_imges;
    
    UIView * view_for_orders_in_progress;
    UITableView *  table_in_order_in_progress;
    NSMutableArray * array_order_no;
    NSMutableArray * array_serving_date_time;
    NSMutableArray * array_status;
    
    UIView * view_for_orders_cancelled;
    UITableView * table_for_cancelled_items;
    NSMutableArray * array_order_no_in_cancelled;
    NSMutableArray * array_serving_time_in_cancelled;
    NSMutableArray * array_time_of_ancelled_value;
    NSMutableArray * array_by_me_r_chef;
    NSMutableArray * array_refund_amount;
    NSMutableArray * array_status_in_cancelled;
    
    
    UIView * view_for_orders_completed;
    UITableView * table_for_order_completion;
    NSMutableArray * array_serving_location;
    
    
    UITextField * txt_from_date;
    UIToolbar * keyboardToolbar_Date;
    NSDateFormatter *formatter;
    UIDatePicker *datePicker;
    UIDatePicker *pickerView;
    NSString*str_DOBfinal;
    
    
    UITextField     * txt_to_date_onrequest;
    UIToolbar       * keyboardToolbar_Date2;
    NSDateFormatter * formatter2;
    UIDatePicker    * datePicker2;
    
    NSString        * str_TO_date;
    
    
    AppDelegate * delegate;
    
    NSMutableArray * array_orderonrequest;
    NSMutableArray * array_orderinprogress;
    
    
    UITextField * txt_from_date_inprogress;
    UIToolbar * keyboardToolbar_Date_inprogress;
    NSDateFormatter * formatter_inprogress;
    UIDatePicker * datePicker_inprogress;
    
    UITextField * txt_to_date_inprogress;
    UIToolbar * keyboardToolbar_toDate_inprogress;
    NSDateFormatter * formatter_inprogress2;
    UIDatePicker * datePicker_inprogress2;
    
    
    
}

@end

@implementation MyordersVC
@synthesize   collView_order_on_request;
@synthesize   collView_in_progress;
@synthesize collView_order_cancelled;
@synthesize collView_order_completed;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBodyDesign];
    
    array_orderonrequest = [[NSMutableArray alloc]init];
    array_orderinprogress =[[NSMutableArray alloc]init];
    
    
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    
    array_names_in_orders = [[NSMutableArray alloc]initWithObjects:@"Orders On Request",@"Orders In Progress",@"Orders Cancelled",@"Orders Completed",nil];
    
    
    array_requested_on = [[NSMutableArray alloc]initWithObjects:@"Requestd on:", nil];
    array_Request_date_time = [[NSMutableArray alloc]initWithObjects:@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015,4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM", nil];
    array_dish_imges = [[NSMutableArray alloc]initWithObjects:@"img-dish4@2x.png",@"img-dish2@2x.png",@"img-dish1@2x.png",@"img-dish4@2x.png",@"img-dish2@2x.png", nil];
    
    array_cuisine_imges = [[NSMutableArray alloc]initWithObjects:@"dish1-img@2x.png",@"dish1-img@2x.png",@"dish1-img@2x.png",@"dish1-img@2x.png",@"dish1-img@2x.png",@"dish1-img@2x.png",@"dish1-img@2x.png",nil];
    
    // ORDERS IN PROGRESS
    
    array_order_no = [[NSMutableArray alloc]initWithObjects:@"10847",@"10847",@"10847",@"10847",@"10847",nil];
    array_serving_date_time = [[NSMutableArray alloc]initWithObjects:@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015,4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM", nil];
    // array_dish_imges = [[NSMutableArray alloc]initWithObjects:@"img-dish4@2x.png",@"img-dish2@2x.png",@"img-dish1@2x.png",@"img-dish4@2x.png",@"img-dish2@2x.png", nil];
    array_status = [[NSMutableArray alloc]initWithObjects:@"In Progress",@"In Progress",@"In Progress",@"In Progress",@"In Progress",nil];
    
    // ORDERS CANCELLED
    
    array_order_no_in_cancelled= [[NSMutableArray alloc]initWithObjects:@"10847",@"10847",@"10847",@"10847",@"10847", nil];
    //    array_serving_time_in_cancelled = [[NSMutableArray alloc]initWithObjects:@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",@"16/07/2015, 4:00:PM",nil];
    //
    array_time_of_ancelled_value = [[NSMutableArray alloc]initWithObjects:@"16/07/2015,4:00:PM",@"16/07/2015,4:00:PM",@"16/07/2015,4:00:PM",@"16/07/2015,4:00:PM",@"16/07/2015,4:00:PM", nil];
    array_by_me_r_chef = [[NSMutableArray alloc]initWithObjects:@"Me",@"Chef",@"Me",@"Chef",@"Me", nil];
    array_refund_amount = [[NSMutableArray alloc]initWithObjects:@"$0",@"$15.90",@"$0",@"$15.90",@"$0", nil];
    array_status_in_cancelled = [[NSMutableArray alloc]initWithObjects:@"Cancelled",@"Cancelled",@"Cancelled",@"Cancelled",@"Cancelled",nil];
    
    
    array_dish_imges = [[NSMutableArray alloc]initWithObjects:@"img-dish4@2x.png",@"img-dish2@2x.png",@"img-dish1@2x.png",@"img-dish4@2x.png",@"img-dish2@2x.png", nil];
    
    //ORDER COMPLETED
    array_serving_location = [[NSMutableArray alloc]initWithObjects:@"Smith st, Paris 37742",@"Smith st, Paris 37742",@"Smith st, Paris 37742",@"Smith st, Paris 37742",@"Smith st, Paris 37742", nil];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self AForderonrequest];
}

-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    img_header.userInteractionEnabled = YES;
    [self.view addSubview:img_header];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu.frame = CGRectMake(10,13,20,20);
    icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
    [icon_menu addTarget:self action:@selector(btn_menu_click:) forControlEvents:UIControlEventTouchUpInside];
    // [self.view   addSubview:icon_menu];
    
    
    lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 200, 45);
    lbl_User_Sign_Up.text = @"Orders On Request";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    UIButton *icon_drop_down = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_drop_down.frame = CGRectMake(CGRectGetMaxX(lbl_User_Sign_Up.frame)-10,15,20,20);
    icon_drop_down .backgroundColor = [UIColor clearColor];
    [icon_drop_down addTarget:self action:@selector(btn_dropdown_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_drop_down setImage:[UIImage imageNamed:@"white-dropd-@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_drop_down];
    
    UIButton *btn_on_dropdown = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_dropdown.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0,227,45);
    btn_on_dropdown .backgroundColor = [UIColor clearColor];
    [btn_on_dropdown addTarget:self action:@selector(btn_on_dropdown_click:) forControlEvents:UIControlEventTouchUpInside];
    //[icon_drop_down setImage:[UIImage imageNamed:@"drop-down@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:btn_on_dropdown];
    
    
    
    
    UIButton *icon_logo = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_logo .frame = CGRectMake(WIDTH-35, 9, 27, 27);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_logo  addTarget:self action:@selector(click_on_logo_btn:) forControlEvents:UIControlEventTouchUpInside];
    [icon_logo setImage:[UIImage imageNamed:@"logo-img@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_logo ];
    
    
}

-(void)integrateBodyDesign
{
    
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor clearColor];
    scroll.frame = CGRectMake(0, 45, WIDTH, 610);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    
    
    view_for_orderonrequest = [[UIView alloc]init];
    view_for_orderonrequest.frame = CGRectMake(0,0,WIDTH,HEIGHT+90);
    view_for_orderonrequest.backgroundColor=[UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    view_for_orderonrequest.backgroundColor = [UIColor clearColor];
    [view_for_orderonrequest setUserInteractionEnabled:YES];
    [scroll addSubview:  view_for_orderonrequest];
    
    UILabel *lbl_date = [[UILabel alloc]init];
    lbl_date.frame = CGRectMake(WIDTH/2-40,0, 100, 45);
    lbl_date.text = @"Date";
    lbl_date.font = [UIFont fontWithName:kFontBold size:18];
    lbl_date.textColor = [UIColor blackColor];
    lbl_date.backgroundColor = [UIColor clearColor];
    [view_for_orderonrequest addSubview:lbl_date];
    
    UIImageView *bg_for_date = [[UIImageView alloc]init];
    bg_for_date.frame = CGRectMake(0, CGRectGetMaxY(lbl_date.frame)-5, WIDTH, 50);
    [bg_for_date setUserInteractionEnabled:YES];
    bg_for_date.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_orderonrequest addSubview:bg_for_date];
    
    UILabel *lbl_from = [[UILabel alloc]init];
    lbl_from.frame = CGRectMake(30,0, 60, 45);
    lbl_from.text = @"From:";
    lbl_from.font = [UIFont fontWithName:kFont size:15];
    lbl_from.textColor = [UIColor blackColor];
    lbl_from.backgroundColor = [UIColor clearColor];
    [bg_for_date addSubview:lbl_from];
    
    UIImageView *line_img = [[UIImageView alloc]init];
    line_img.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 35,110, 0.5);
    [line_img setUserInteractionEnabled:YES];
    line_img.image=[UIImage imageNamed:@"line1@2x.png"];
    [bg_for_date addSubview:line_img];
    
    
    
    
    txt_from_date = [[UITextField alloc] init];
    txt_from_date.borderStyle = UITextBorderStyleNone;
    txt_from_date.placeholder = @"dd/mm/yyyy";//@"__ __ / __ __ / __ __ __ __";
    
    if(IS_IPHONE_6Plus)
    {
        txt_from_date.frame = CGRectMake(5, 5,100, 45);
        txt_from_date.placeholder = @"dd/mm/yyyy";
        txt_from_date.font = [UIFont systemFontOfSize:17.0];
        [txt_from_date setValue:[UIFont fontWithName:kFont size: 17] forKeyPath:@"_placeholderLabel.font"];
        
        
        
    }
    else if(IS_IPHONE_6)
    {
        txt_from_date.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 5, 120,35);
        txt_from_date.placeholder = @"dd/mm/yyyy";
        txt_from_date.font = [UIFont systemFontOfSize:15.0];
        [txt_from_date setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
        
    }
    else
    {
        txt_from_date.frame = CGRectMake(CGRectGetMaxX(lbl_from.frame), 5, 100, 45);
        txt_from_date.placeholder = @"dd/mm/yyyy";
        txt_from_date.font = [UIFont systemFontOfSize:14.0];
        [txt_from_date setValue:[UIFont fontWithName:kFont size: 14] forKeyPath:@"_placeholderLabel.font"];
        
    }
    txt_from_date.textColor = [UIColor colorWithRed:167.0/255.0 green:167.0/255.0 blue:167.0/255.0 alpha:1.0];
    // txt_Date.font = [UIFont systemFontOfSize:15.0];
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 4, 20)];
    txt_from_date.leftView = paddingView1;
    txt_from_date.leftViewMode = UITextFieldViewModeAlways;
    txt_from_date.autocorrectionType = UITextAutocorrectionTypeNo;
    //   txt_Date.keyboardType = UIKeyboardTypeDefault;
    txt_from_date.clearButtonMode = UITextFieldViewModeWhileEditing;
    txt_from_date.delegate = self;
    txt_from_date.userInteractionEnabled = YES;
    [txt_from_date setValue:[UIColor colorWithRed:167.0/255.0 green:167.0/255.0 blue:167.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    // [txt_Date setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [bg_for_date addSubview:txt_from_date];
    
    if (keyboardToolbar_Date == nil)
    {
        keyboardToolbar_Date = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar_Date setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_DoneDate:)];
        [keyboardToolbar_Date setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    txt_from_date.inputAccessoryView = keyboardToolbar_Date;
    txt_from_date.backgroundColor=[UIColor clearColor];
    
    //   txt_DateBirth = txt_date_of_birth;
    
    formatter = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    //datePicker.minimumDate = [formatter dateFromString:@"1900-01-01"];
    
    
    NSDate *currentDate = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate];
    components.day=components.day;
    components.month=components.month;
    components.year = components.year;
    [components setYear:-19];
    
    //    NSString *temp=[NSString stringWithFormat:@" Day:%d Months:%d Year:%ld",(long)components.day,(long)components.month,(long)components.year];
    //    NSLog(@"%@",temp);
    //    NSString *string = [NSString stringWithFormat:@"%ld.%ld.%ld", (long)components.day, (long)components.month, (long)components.year];
    NSDate *minDate=[calendar dateByAddingComponents:components toDate:currentDate  options:0];
    
    NSLog(@"Minimum date is :: %@",minDate);
    
    [components setYear:-150];
    
    NSDate *maxDate = [calendar dateByAddingComponents:components toDate:currentDate  options:0];
    
    //   NSDate *maxDate=  [formatter dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components.year, (long)components.month,(long)components.day]];
    
    //   datePicker.maximumDate= [formatter dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components.year-20, (long)components.month,(long)components.day]];
    
    NSLog(@"Maximum date is :: %@",maxDate);
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [datePicker setMinimumDate:maxDate];
    [datePicker setMaximumDate:minDate];
    [datePicker setDate:minDate];
    txt_from_date.inputView = datePicker;
    
    
    
    
    UIImageView *img_calender = [[UIImageView alloc]init];
    img_calender.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+85,9.5,25, 25);
    [img_calender setUserInteractionEnabled:YES];
    img_calender.image=[UIImage imageNamed:@"img-calender@2x.png"];
    [bg_for_date addSubview:img_calender];
    
    UILabel *lbl_to = [[UILabel alloc]init];
    lbl_to.frame = CGRectMake(CGRectGetMidX(img_calender.frame)+30,0, 50, 45);
    lbl_to.text = @"to";
    lbl_to.font = [UIFont fontWithName:kFont size:15];
    lbl_to.textColor = [UIColor blackColor];
    lbl_to.backgroundColor = [UIColor clearColor];
    [bg_for_date addSubview:lbl_to];
    
    UIImageView *line_img2 = [[UIImageView alloc]init];
    line_img2.frame = CGRectMake(CGRectGetMidX(lbl_to.frame)-20, 35,110, 0.5);
    [line_img2 setUserInteractionEnabled:YES];
    line_img2.image=[UIImage imageNamed:@"line1@2x.png"];
    [bg_for_date addSubview:line_img2];
    
    
    
    txt_to_date_onrequest = [[UITextField alloc] init];
    txt_to_date_onrequest.borderStyle = UITextBorderStyleNone;
    txt_to_date_onrequest.placeholder = @"dd/mm/yyyy";//@"__ __ / __ __ / __ __ __ __";
    
    if(IS_IPHONE_6Plus)
    {
        txt_to_date_onrequest.frame = CGRectMake(5, 5,100, 45);
        txt_to_date_onrequest.placeholder = @"dd/mm/yyyy";
        txt_to_date_onrequest.font = [UIFont systemFontOfSize:17.0];
        [txt_to_date_onrequest setValue:[UIFont fontWithName:kFont size: 17] forKeyPath:@"_placeholderLabel.font"];
        
        
        
    }
    else if(IS_IPHONE_6)
    {
        txt_to_date_onrequest.frame = CGRectMake(CGRectGetMidX(lbl_to.frame), 5, 120,35);
        txt_to_date_onrequest.placeholder = @"dd/mm/yyyy";
        txt_to_date_onrequest.font = [UIFont systemFontOfSize:15.0];
        [txt_to_date_onrequest setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
        
    }
    else
    {
        txt_to_date_onrequest.frame = CGRectMake(CGRectGetMidX(img_calender.frame)+50, 5, 100, 45);
        txt_to_date_onrequest.placeholder = @"dd/mm/yyyy";
        txt_to_date_onrequest.font = [UIFont systemFontOfSize:14.0];
        [txt_to_date_onrequest setValue:[UIFont fontWithName:kFont size: 14] forKeyPath:@"_placeholderLabel.font"];
        
    }
    txt_to_date_onrequest.textColor = [UIColor colorWithRed:167.0/255.0 green:167.0/255.0 blue:167.0/255.0 alpha:1.0];
    // txt_Date.font = [UIFont systemFontOfSize:15.0];
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 4, 20)];
    txt_to_date_onrequest.leftView = paddingView2;
    txt_to_date_onrequest.leftViewMode = UITextFieldViewModeAlways;
    txt_to_date_onrequest.autocorrectionType = UITextAutocorrectionTypeNo;
    //   txt_Date.keyboardType = UIKeyboardTypeDefault;
    txt_to_date_onrequest.clearButtonMode = UITextFieldViewModeWhileEditing;
    txt_to_date_onrequest.delegate = self;
    txt_to_date_onrequest.userInteractionEnabled = YES;
    [txt_to_date_onrequest setValue:[UIColor colorWithRed:167.0/255.0 green:167.0/255.0 blue:167.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    // [txt_Date setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [bg_for_date addSubview:txt_to_date_onrequest];
    
    if (keyboardToolbar_Date2 == nil)
    {
        keyboardToolbar_Date2 = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar_Date2 setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_Done_to_Date:)];
        [keyboardToolbar_Date2 setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    txt_to_date_onrequest.inputAccessoryView = keyboardToolbar_Date2;
    txt_to_date_onrequest.backgroundColor=[UIColor clearColor];
    
    //   txt_DateBirth = txt_date_of_birth;
    
    formatter2 = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter2 setDateFormat:@"dd-MM-yyyy"];
    //datePicker.minimumDate = [formatter dateFromString:@"1900-01-01"];
    
    
    
    
    
    NSDate *currentDate2 = [NSDate date];
    NSCalendar *calendar2 = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components2 = [calendar2 components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate2];
    components2.day=components2.day;
    components2.month=components2.month;
    components2.year = components2.year;
    [components2 setYear:-19];
    
    NSDate *minDate2=[calendar2 dateByAddingComponents:components2 toDate:currentDate2  options:0];
    NSLog(@"Minimum date is :: %@",minDate2);
    
    [components2 setYear:-150];
    
    NSDate *maxDate2 = [calendar2 dateByAddingComponents:components2 toDate:currentDate2  options:0];
    NSLog(@"Maximum date is :: %@",maxDate2);
    
    datePicker2 = [[UIDatePicker alloc] init];
    datePicker2.datePickerMode = UIDatePickerModeDate;
    [datePicker2 addTarget:self action:@selector(datePicker_to_ValueChanged:) forControlEvents:UIControlEventValueChanged];
    [datePicker2 setMinimumDate:maxDate2];
    [datePicker2 setMaximumDate:minDate2];
    [datePicker2 setDate:minDate2];
    txt_to_date_onrequest.inputView = datePicker2;
    
    
    
    
    UIImageView *img_calender2 = [[UIImageView alloc]init];
    img_calender2.frame = CGRectMake(CGRectGetMidX(lbl_to.frame)+65,9.5,25, 25);
    [img_calender2 setUserInteractionEnabled:YES];
    img_calender2.image=[UIImage imageNamed:@"img-calender@2x.png"];
    [bg_for_date addSubview:img_calender2];
    
    img_table = [[UITableView alloc] init ];
    img_table.frame = CGRectMake(0, CGRectGetMaxY(bg_for_date.frame)+5, WIDTH, HEIGHT-150);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [img_table setUserInteractionEnabled:YES];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    img_table.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [view_for_orderonrequest addSubview:img_table];
    
    view_for_orders_in_progress = [[UIView alloc]init];
    view_for_orders_in_progress.frame = CGRectMake(0,0,WIDTH,HEIGHT+90);
    view_for_orders_in_progress.backgroundColor=[UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    view_for_orders_in_progress.backgroundColor = [UIColor clearColor];
    [view_for_orders_in_progress setUserInteractionEnabled:YES];
    [scroll addSubview:view_for_orders_in_progress];
    view_for_orders_in_progress.hidden = YES;
    
    
    UILabel *lbl_date_in_progress = [[UILabel alloc]init];
    lbl_date_in_progress.frame = CGRectMake(WIDTH/2-40,0, 100, 45);
    lbl_date_in_progress.text = @"Date";
    lbl_date_in_progress.font = [UIFont fontWithName:kFontBold size:18];
    lbl_date_in_progress.textColor = [UIColor blackColor];
    lbl_date_in_progress.backgroundColor = [UIColor clearColor];
    [view_for_orders_in_progress addSubview:lbl_date_in_progress];
    
    UIImageView *bg_for_date_in_progress = [[UIImageView alloc]init];
    bg_for_date_in_progress.frame = CGRectMake(-5, CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
    [bg_for_date_in_progress setUserInteractionEnabled:YES];
    bg_for_date_in_progress.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_orders_in_progress addSubview:bg_for_date_in_progress];
    
    UILabel *lbl_from_in_progress = [[UILabel alloc]init];
    lbl_from_in_progress.frame = CGRectMake(30,0, 100, 45);
    lbl_from_in_progress.text = @"From:";
    lbl_from_in_progress.font = [UIFont fontWithName:kFont size:15];
    lbl_from_in_progress.textColor = [UIColor blackColor];
    lbl_from_in_progress.backgroundColor = [UIColor clearColor];
    [bg_for_date_in_progress addSubview:lbl_from_in_progress];
    
    UIImageView *line_img_in_progress = [[UIImageView alloc]init];
    line_img_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 35,110, 0.5);
    [line_img_in_progress setUserInteractionEnabled:YES];
    line_img_in_progress.image=[UIImage imageNamed:@"line1@2x.png"];
    [bg_for_date_in_progress addSubview:line_img_in_progress];
    
    
    txt_from_date_inprogress = [[UITextField alloc] init];
    txt_from_date_inprogress.borderStyle = UITextBorderStyleNone;
    txt_from_date_inprogress.placeholder = @"dd/mm/yyyy";//@"__ __ / __ __ / __ __ __ __";
    
    if(IS_IPHONE_6Plus)
    {
        txt_from_date_inprogress.frame = CGRectMake(5, 5,100, 45);
        txt_from_date_inprogress.placeholder = @"dd/mm/yyyy";
        txt_from_date_inprogress.font = [UIFont systemFontOfSize:17.0];
        [txt_from_date_inprogress setValue:[UIFont fontWithName:kFont size: 17] forKeyPath:@"_placeholderLabel.font"];
        
        
        
    }
    else if(IS_IPHONE_6)
    {
        txt_from_date_inprogress.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 5, 120,35);
        txt_from_date_inprogress.placeholder = @"dd/mm/yyyy";
        txt_from_date_inprogress.font = [UIFont systemFontOfSize:15.0];
        [txt_from_date_inprogress setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
        
    }
    else
    {
        txt_from_date_inprogress.frame = CGRectMake(5, 5, 100, 45);
        txt_from_date_inprogress.placeholder = @"dd/mm/yyyy";
        txt_from_date_inprogress.font = [UIFont systemFontOfSize:14.0];
        [txt_from_date_inprogress setValue:[UIFont fontWithName:kFont size: 14] forKeyPath:@"_placeholderLabel.font"];
        
    }
    txt_from_date_inprogress.textColor = [UIColor colorWithRed:167.0/255.0 green:167.0/255.0 blue:167.0/255.0 alpha:1.0];
    // txt_Date.font = [UIFont systemFontOfSize:15.0];
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 4, 20)];
    txt_from_date_inprogress.leftView = paddingView3;
    txt_from_date_inprogress.leftViewMode = UITextFieldViewModeAlways;
    txt_from_date_inprogress.autocorrectionType = UITextAutocorrectionTypeNo;
    //   txt_Date.keyboardType = UIKeyboardTypeDefault;
    txt_from_date_inprogress.clearButtonMode = UITextFieldViewModeWhileEditing;
    txt_from_date_inprogress.delegate = self;
    txt_from_date_inprogress.userInteractionEnabled = YES;
    [txt_from_date_inprogress setValue:[UIColor colorWithRed:167.0/255.0 green:167.0/255.0 blue:167.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    // [txt_Date setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [bg_for_date_in_progress addSubview:txt_from_date_inprogress];
    
    if (keyboardToolbar_Date_inprogress == nil)
    {
        keyboardToolbar_Date_inprogress = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar_Date_inprogress setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace_inprogres = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept_inprogress = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_DoneDate_inprogress:)];
        [keyboardToolbar_Date_inprogress setItems:[[NSArray alloc] initWithObjects: extraSpace_inprogres, accept_inprogress, nil]];
    }
    txt_from_date_inprogress.inputAccessoryView = keyboardToolbar_Date;
    txt_from_date_inprogress.backgroundColor=[UIColor clearColor];
    
    
    formatter_inprogress = [[NSDateFormatter alloc] init];
    [formatter_inprogress setDateFormat:@"dd-MM-yyyy"];
    
    NSDate *currentDate_inprogress = [NSDate date];
    NSCalendar *calendar_inprogress = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components_inprogress = [calendar_inprogress components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate_inprogress];
    components_inprogress.day=components_inprogress.day;
    components_inprogress.month=components_inprogress.month;
    components_inprogress.year = components_inprogress.year;
    [components_inprogress setYear:-19];
    
    NSDate *minDate_inprogress =[calendar_inprogress dateByAddingComponents:components_inprogress toDate:currentDate_inprogress  options:0];
    NSLog(@"Minimum date is :: %@",minDate_inprogress);
    
    [components_inprogress setYear:-150];
    
    NSDate *maxDate_inprogress = [calendar_inprogress dateByAddingComponents:components_inprogress toDate:currentDate_inprogress  options:0];
    NSLog(@"Maximum date is :: %@",maxDate_inprogress);
    
    datePicker_inprogress = [[UIDatePicker alloc] init];
    datePicker_inprogress.datePickerMode = UIDatePickerModeDate;
    [datePicker_inprogress addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [datePicker_inprogress setMinimumDate:maxDate_inprogress];
    [datePicker_inprogress setMaximumDate:minDate_inprogress];
    [datePicker_inprogress setDate:minDate_inprogress];
    txt_from_date_inprogress.inputView = datePicker_inprogress;
    
    
    
    UIImageView *img_calender_in_progress = [[UIImageView alloc]init];
    img_calender_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+85,9.5,25, 25);
    [img_calender_in_progress setUserInteractionEnabled:YES];
    img_calender_in_progress.image=[UIImage imageNamed:@"img-calender@2x.png"];
    [bg_for_date_in_progress addSubview:img_calender_in_progress];
    
    UILabel *lbl_to_in_progress = [[UILabel alloc]init];
    lbl_to_in_progress.frame = CGRectMake(CGRectGetMidX(img_calender.frame)+30,0, 100, 45);
    lbl_to_in_progress.text = @"to";
    lbl_to_in_progress.font = [UIFont fontWithName:kFont size:15];
    lbl_to_in_progress.textColor = [UIColor blackColor];
    lbl_to_in_progress.backgroundColor = [UIColor clearColor];
    [bg_for_date_in_progress addSubview:lbl_to_in_progress];
    
    UIImageView *line_img2_in_progress = [[UIImageView alloc]init];
    line_img2_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_to_in_progress.frame)-20, 35,110, 0.5);
    [line_img2_in_progress setUserInteractionEnabled:YES];
    line_img2_in_progress.image=[UIImage imageNamed:@"line1@2x.png"];
    [bg_for_date_in_progress addSubview:line_img2_in_progress];
    
    
    txt_to_date_inprogress = [[UITextField alloc] init];
    txt_to_date_inprogress.borderStyle = UITextBorderStyleNone;
    txt_to_date_inprogress.placeholder = @"dd/mm/yyyy";//@"__ __ / __ __ / __ __ __ __";
    
    if(IS_IPHONE_6Plus)
    {
        txt_to_date_inprogress.frame = CGRectMake(5, 5,100, 45);
        txt_to_date_inprogress.placeholder = @"dd/mm/yyyy";
        txt_to_date_inprogress.font = [UIFont systemFontOfSize:17.0];
        [txt_to_date_inprogress setValue:[UIFont fontWithName:kFont size: 17] forKeyPath:@"_placeholderLabel.font"];
        
        
        
    }
    else if(IS_IPHONE_6)
    {
        txt_to_date_inprogress.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 5, 120,35);
        txt_to_date_inprogress.placeholder = @"dd/mm/yyyy";
        txt_to_date_inprogress.font = [UIFont systemFontOfSize:15.0];
        [txt_to_date_inprogress setValue:[UIFont fontWithName:kFont size: 12] forKeyPath:@"_placeholderLabel.font"];
        
    }
    else
    {
        txt_to_date_inprogress.frame = CGRectMake(5, 5, 100, 45);
        txt_to_date_inprogress.placeholder = @"dd/mm/yyyy";
        txt_to_date_inprogress.font = [UIFont systemFontOfSize:14.0];
        [txt_to_date_inprogress setValue:[UIFont fontWithName:kFont size: 14] forKeyPath:@"_placeholderLabel.font"];
        
    }
    txt_to_date_inprogress.textColor = [UIColor colorWithRed:167.0/255.0 green:167.0/255.0 blue:167.0/255.0 alpha:1.0];
    // txt_Date.font = [UIFont systemFontOfSize:15.0];
    UIView *paddingView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 4, 20)];
    txt_to_date_inprogress.leftView = paddingView4;
    txt_to_date_inprogress.leftViewMode = UITextFieldViewModeAlways;
    txt_to_date_inprogress.autocorrectionType = UITextAutocorrectionTypeNo;
    //   txt_Date.keyboardType = UIKeyboardTypeDefault;
    txt_to_date_inprogress.clearButtonMode = UITextFieldViewModeWhileEditing;
    txt_to_date_inprogress.delegate = self;
    txt_to_date_inprogress.userInteractionEnabled = YES;
    [txt_to_date_inprogress setValue:[UIColor colorWithRed:167.0/255.0 green:167.0/255.0 blue:167.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    // [txt_Date setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [bg_for_date_in_progress addSubview:txt_to_date_inprogress];
    
    if (keyboardToolbar_toDate_inprogress == nil)
    {
        keyboardToolbar_toDate_inprogress = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar_toDate_inprogress setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace_inprogres2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept_inprogress2 = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_DonetoDate_inprogress:)];
        [keyboardToolbar_toDate_inprogress setItems:[[NSArray alloc] initWithObjects: extraSpace_inprogres2, accept_inprogress2, nil]];
    }
    txt_to_date_inprogress.inputAccessoryView = keyboardToolbar_toDate_inprogress;
    txt_to_date_inprogress.backgroundColor=[UIColor clearColor];
    
    
    formatter_inprogress2 = [[NSDateFormatter alloc] init];
    [formatter_inprogress2 setDateFormat:@"dd-MM-yyyy"];
    
    NSDate *currentDate_inprogress2 = [NSDate date];
    NSCalendar *calendar_inprogress2 = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components_inprogress2 = [calendar_inprogress2 components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate_inprogress2];
    components_inprogress2.day=components_inprogress2.day;
    components_inprogress2.month=components_inprogress2.month;
    components_inprogress2.year = components_inprogress2.year;
    [components_inprogress2 setYear:-19];
    
    NSDate *minDate_inprogress2 =[calendar_inprogress2 dateByAddingComponents:components_inprogress2 toDate:currentDate_inprogress2  options:0];
    NSLog(@"Minimum date is :: %@",minDate_inprogress2);
    
    [components_inprogress2 setYear:-150];
    
    NSDate *maxDate_inprogress2 = [calendar_inprogress2 dateByAddingComponents:components_inprogress2 toDate:currentDate_inprogress2  options:0];
    NSLog(@"Maximum date is :: %@",maxDate_inprogress2);
    
    datePicker_inprogress2 = [[UIDatePicker alloc] init];
    datePicker_inprogress2.datePickerMode = UIDatePickerModeDate;
    [datePicker_inprogress2 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [datePicker_inprogress2 setMinimumDate:maxDate_inprogress2];
    [datePicker_inprogress2 setMaximumDate:minDate_inprogress2];
    [datePicker_inprogress2 setDate:minDate_inprogress2];
    txt_to_date_inprogress.inputView = datePicker_inprogress2;
    
    
    UIImageView *img_calender2_in_progress = [[UIImageView alloc]init];
    img_calender2_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_to.frame)+65,9.5,25, 25);
    [img_calender2_in_progress setUserInteractionEnabled:YES];
    img_calender2_in_progress.image=[UIImage imageNamed:@"img-calender@2x.png"];
    [bg_for_date_in_progress addSubview:img_calender2_in_progress];
    
    table_in_order_in_progress = [[UITableView alloc] init ];
    table_in_order_in_progress.frame = CGRectMake(5, CGRectGetMaxY(bg_for_date.frame)+5, WIDTH-10, HEIGHT-150);
    [table_in_order_in_progress setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_in_order_in_progress.delegate = self;
    [table_in_order_in_progress setUserInteractionEnabled:YES];
    table_in_order_in_progress.dataSource = self;
    table_in_order_in_progress.showsVerticalScrollIndicator = NO;
    table_in_order_in_progress.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [view_for_orders_in_progress addSubview:table_in_order_in_progress];
    
    
    
    
    view_for_orders_cancelled = [[UIView alloc]init];
    view_for_orders_cancelled.frame = CGRectMake(0,0,WIDTH,HEIGHT+90);
    view_for_orders_cancelled.backgroundColor=[UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [view_for_orders_cancelled setUserInteractionEnabled:YES];
    [scroll addSubview:  view_for_orders_cancelled];
    view_for_orders_cancelled.hidden = YES;
    
    
    UILabel *lbl_date_in_cancelled = [[UILabel alloc]init];
    lbl_date_in_cancelled.frame = CGRectMake(WIDTH/2-40,0, 100, 45);
    lbl_date_in_cancelled.text = @"Date";
    lbl_date_in_cancelled.font = [UIFont fontWithName:kFontBold size:18];
    lbl_date_in_cancelled.textColor = [UIColor blackColor];
    lbl_date_in_cancelled.backgroundColor = [UIColor clearColor];
    [view_for_orders_cancelled addSubview:lbl_date_in_cancelled];
    
    UIImageView *bg_for_date_cancelled = [[UIImageView alloc]init];
    bg_for_date_cancelled.frame = CGRectMake(-5, CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
    [bg_for_date_cancelled setUserInteractionEnabled:YES];
    bg_for_date_cancelled.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_orders_cancelled addSubview:bg_for_date_cancelled];
    
    UILabel *lbl_from_in_cancelled = [[UILabel alloc]init];
    lbl_from_in_cancelled.frame = CGRectMake(30,0, 100, 45);
    lbl_from_in_cancelled.text = @"From:";
    lbl_from_in_cancelled.font = [UIFont fontWithName:kFont size:15];
    lbl_from_in_cancelled.textColor = [UIColor blackColor];
    lbl_from_in_cancelled.backgroundColor = [UIColor clearColor];
    [bg_for_date_cancelled addSubview:lbl_from_in_cancelled];
    
    UIImageView *line_img_in_cancelled = [[UIImageView alloc]init];
    line_img_in_cancelled.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 35,110, 0.5);
    [line_img_in_cancelled setUserInteractionEnabled:YES];
    line_img_in_cancelled.image=[UIImage imageNamed:@"line1@2x.png"];
    [bg_for_date_cancelled addSubview:line_img_in_cancelled];
    
    UIImageView *img_calender_in_cancelled = [[UIImageView alloc]init];
    img_calender_in_cancelled.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+85,9.5,25, 25);
    [img_calender_in_cancelled setUserInteractionEnabled:YES];
    img_calender_in_cancelled.image=[UIImage imageNamed:@"img-calender@2x.png"];
    [bg_for_date_cancelled addSubview:img_calender_in_cancelled];
    
    UILabel *lbl_to_in_cancelled = [[UILabel alloc]init];
    lbl_to_in_cancelled.frame = CGRectMake(CGRectGetMidX(img_calender.frame)+30,0, 100, 45);
    lbl_to_in_cancelled.text = @"to";
    lbl_to_in_cancelled.font = [UIFont fontWithName:kFont size:15];
    lbl_to_in_cancelled.textColor = [UIColor blackColor];
    lbl_to_in_cancelled.backgroundColor = [UIColor clearColor];
    [bg_for_date_cancelled addSubview:lbl_to_in_cancelled];
    
    UIImageView *line_img2_in_cancelled = [[UIImageView alloc]init];
    line_img2_in_cancelled.frame = CGRectMake(CGRectGetMidX(lbl_to_in_cancelled.frame)-20, 35,110, 0.5);
    [line_img2_in_cancelled setUserInteractionEnabled:YES];
    line_img2_in_cancelled.image=[UIImage imageNamed:@"line1@2x.png"];
    [bg_for_date_cancelled addSubview:line_img2_in_cancelled];
    
    UIImageView *img_calender2_in_cancelled = [[UIImageView alloc]init];
    img_calender2_in_cancelled.frame = CGRectMake(CGRectGetMidX(lbl_to_in_cancelled.frame)+65,9.5,25, 25);
    [img_calender2_in_cancelled setUserInteractionEnabled:YES];
    img_calender2_in_cancelled.image=[UIImage imageNamed:@"img-calender@2x.png"];
    [bg_for_date_cancelled addSubview:img_calender2_in_cancelled];
    
    table_for_cancelled_items = [[UITableView alloc] init ];
    table_for_cancelled_items.frame = CGRectMake(5,CGRectGetMaxY(bg_for_date.frame)+5, WIDTH-10, HEIGHT-150);
    [table_for_cancelled_items setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [table_for_cancelled_items setUserInteractionEnabled:YES];
    table_for_cancelled_items.delegate = self;
    table_for_cancelled_items.dataSource = self;
    table_for_cancelled_items.showsVerticalScrollIndicator = NO;
    table_for_cancelled_items.backgroundColor=[UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [view_for_orders_cancelled addSubview:table_for_cancelled_items];
    
    
    
    
    view_for_orders_completed = [[UIView alloc]init];
    view_for_orders_completed.frame = CGRectMake(0,0,WIDTH,HEIGHT+90);
    view_for_orders_completed.backgroundColor=[UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    view_for_orders_completed.backgroundColor = [UIColor clearColor];
    [view_for_orders_completed setUserInteractionEnabled:YES];
    [scroll addSubview:  view_for_orders_completed];
    view_for_orders_completed.hidden = YES;
    
    UILabel *lbl_date_in_completed = [[UILabel alloc]init];
    lbl_date_in_completed.frame = CGRectMake(WIDTH/2-40,0, 100, 45);
    lbl_date_in_completed.text = @"Date";
    lbl_date_in_completed.font = [UIFont fontWithName:kFontBold size:18];
    lbl_date_in_completed.textColor = [UIColor blackColor];
    lbl_date_in_completed.backgroundColor = [UIColor clearColor];
    [view_for_orders_completed addSubview:lbl_date_in_completed];
    
    UIImageView *bg_for_date_in_completed = [[UIImageView alloc]init];
    bg_for_date_in_completed.frame = CGRectMake(-5, CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
    [bg_for_date_in_completed setUserInteractionEnabled:YES];
    bg_for_date_in_completed.image=[UIImage imageNamed:@"wht-popup@2x.png"];
    [view_for_orders_completed addSubview:bg_for_date_in_completed];
    
    UILabel *lbl_from_in_completed = [[UILabel alloc]init];
    lbl_from_in_completed.frame = CGRectMake(30,0, 100, 45);
    lbl_from_in_completed.text = @"From:";
    lbl_from_in_completed.font = [UIFont fontWithName:kFont size:15];
    lbl_from_in_completed.textColor = [UIColor blackColor];
    lbl_from_in_completed.backgroundColor = [UIColor clearColor];
    [bg_for_date_in_completed addSubview:lbl_from_in_completed];
    
    UIImageView *line_img_in_completed = [[UIImageView alloc]init];
    line_img_in_completed.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 35,110, 0.5);
    [line_img_in_completed setUserInteractionEnabled:YES];
    line_img_in_completed.image=[UIImage imageNamed:@"line1@2x.png"];
    [bg_for_date_in_completed addSubview:line_img_in_completed];
    
    UIImageView *img_calender_in_completed = [[UIImageView alloc]init];
    img_calender_in_completed.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+85,9.5,25, 25);
    [img_calender_in_completed setUserInteractionEnabled:YES];
    img_calender_in_completed.image=[UIImage imageNamed:@"img-calender@2x.png"];
    [bg_for_date_in_completed addSubview:img_calender_in_completed];
    
    UILabel *lbl_to_in_completed = [[UILabel alloc]init];
    lbl_to_in_completed.frame = CGRectMake(CGRectGetMidX(img_calender.frame)+30,0, 100, 45);
    lbl_to_in_completed.text = @"to";
    lbl_to_in_completed.font = [UIFont fontWithName:kFont size:15];
    lbl_to_in_completed.textColor = [UIColor blackColor];
    lbl_to_in_completed.backgroundColor = [UIColor clearColor];
    [bg_for_date_in_completed addSubview:lbl_to_in_completed];
    
    UIImageView *line_img2_in_completed = [[UIImageView alloc]init];
    line_img2_in_completed.frame = CGRectMake(CGRectGetMidX(lbl_to_in_completed.frame)-20, 35,110, 0.5);
    [line_img2_in_completed setUserInteractionEnabled:YES];
    line_img2_in_completed.image=[UIImage imageNamed:@"line1@2x.png"];
    [bg_for_date_in_completed addSubview:line_img2_in_completed];
    
    UIImageView *img_calender2_in_completed = [[UIImageView alloc]init];
    img_calender2_in_completed.frame = CGRectMake(CGRectGetMidX(lbl_to_in_completed.frame)+65,9.5,25, 25);
    [img_calender2_in_completed setUserInteractionEnabled:YES];
    img_calender2_in_completed.image=[UIImage imageNamed:@"img-calender@2x.png"];
    [bg_for_date_in_completed addSubview:img_calender2_in_completed];
    
    table_for_order_completion = [[UITableView alloc] init ];
    table_for_order_completion.frame = CGRectMake(5, CGRectGetMaxY(bg_for_date.frame)+5, WIDTH-10, HEIGHT-150);
    [table_for_order_completion setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [table_for_order_completion setUserInteractionEnabled:YES];
    table_for_order_completion.delegate = self;
    table_for_order_completion.dataSource = self;
    table_for_order_completion.showsVerticalScrollIndicator = NO;
    table_for_order_completion.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [view_for_orders_completed addSubview:table_for_order_completion];
    
    
#pragma mark Tableview
    
    table_on_drop_down = [[UITableView alloc] init ];
    table_on_drop_down .frame  = CGRectMake(60,45,210,100);
    [table_on_drop_down  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_on_drop_down .delegate = self;
    table_on_drop_down .dataSource = self;
    table_on_drop_down.layer.borderColor = [[UIColor blackColor]CGColor];
    table_on_drop_down.layer.borderWidth = 1.0f;
    table_on_drop_down.clipsToBounds = YES;
    table_on_drop_down .showsVerticalScrollIndicator = NO;
    table_on_drop_down .backgroundColor = [UIColor clearColor];
    table_on_drop_down.hidden = YES;
    [self.view addSubview: table_on_drop_down ];
    
    if (IS_IPHONE_6Plus)
    {
        
        scroll.frame = CGRectMake(0, 45, WIDTH, 680);
        
        view_for_orderonrequest.frame = CGRectMake(0,0,WIDTH,HEIGHT+90);
        lbl_date.frame = CGRectMake(WIDTH/2-40,0, 100, 45);
        bg_for_date.frame = CGRectMake(-5, CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
        lbl_from.frame = CGRectMake(30,0, 100, 45);
        line_img.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 35,110, 0.5);
        img_calender.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+85,9.5,25, 25);
        lbl_to.frame = CGRectMake(CGRectGetMidX(img_calender.frame)+30,0, 30, 45);
        line_img2.frame = CGRectMake(CGRectGetMidX(lbl_to.frame)-20, 35,110, 0.5);
        img_calender2.frame = CGRectMake(CGRectGetMidX(lbl_to.frame)+65,9.5,25, 25);
        img_table.frame = CGRectMake(0, CGRectGetMaxY(bg_for_date.frame)+5, WIDTH, HEIGHT-150);
        
        view_for_orders_in_progress.frame = CGRectMake(0,0,WIDTH,HEIGHT+90);
        lbl_date_in_progress.frame = CGRectMake(WIDTH/2-40,0, 100, 45);
        bg_for_date_in_progress.frame = CGRectMake(-5, CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
        lbl_from_in_progress.frame = CGRectMake(30,0, 100, 45);
        line_img_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 35,110, 0.5);
        img_calender_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+85,9.5,25, 25);
        lbl_to_in_progress.frame = CGRectMake(CGRectGetMidX(img_calender.frame)+30,0, 30, 45);
        line_img2_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_to_in_progress.frame)-20, 35,110, 0.5);
        img_calender2_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_to_in_progress.frame)+65,9.5,25, 25);
        table_in_order_in_progress.frame = CGRectMake(5, CGRectGetMaxY(bg_for_date.frame)+5, WIDTH-10, HEIGHT-150);
        
        view_for_orders_cancelled.frame = CGRectMake(0,0,WIDTH,HEIGHT+90);
        lbl_date_in_cancelled.frame = CGRectMake(WIDTH/2-40,0, 100, 45);
        bg_for_date_cancelled.frame = CGRectMake(-5, CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
        lbl_from_in_cancelled.frame = CGRectMake(30,0, 100, 45);
        line_img_in_cancelled.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 35,110, 0.5);
        img_calender_in_cancelled.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+85,9.5,25, 25);
        lbl_to_in_cancelled.frame = CGRectMake(CGRectGetMidX(img_calender.frame)+30,0, 30, 45);
        line_img2_in_cancelled.frame = CGRectMake(CGRectGetMidX(lbl_to_in_cancelled.frame)-20, 35,110, 0.5);
        img_calender2_in_cancelled.frame = CGRectMake(CGRectGetMidX(lbl_to_in_cancelled.frame)+65,9.5,25, 25);
        table_for_cancelled_items.frame = CGRectMake(5, CGRectGetMaxY(bg_for_date.frame)+5, WIDTH-10, HEIGHT-150);
        
        view_for_orders_completed.frame = CGRectMake(0,0,WIDTH,HEIGHT+90);
        lbl_date_in_completed.frame = CGRectMake(WIDTH/2-40,0, 100, 45);
        bg_for_date_in_completed.frame = CGRectMake(-5, CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
        lbl_from_in_completed.frame = CGRectMake(30,0, 100, 45);
        line_img_in_completed.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 35,110, 0.5);
        img_calender_in_completed.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+85,9.5,25, 25);
        lbl_to_in_completed.frame = CGRectMake(CGRectGetMidX(img_calender.frame)+30,0,50, 45);
        line_img2_in_completed.frame = CGRectMake(CGRectGetMidX(lbl_to_in_completed.frame)-20, 35,110, 0.5);
        img_calender2_in_completed.frame = CGRectMake(CGRectGetMidX(lbl_to_in_completed.frame)+65,9.5,25, 25);
        table_for_order_completion.frame = CGRectMake(5, CGRectGetMaxY(bg_for_date.frame)+5, WIDTH-10, HEIGHT-150);
        table_on_drop_down .frame  = CGRectMake(60,45,270,100);
        
        
    }
    else if (IS_IPHONE_6)
    {
        scroll.frame = CGRectMake(0, 45, WIDTH, 610);
        
        view_for_orderonrequest.frame = CGRectMake(0,0,WIDTH,HEIGHT+90);
        lbl_date.frame = CGRectMake(WIDTH/2-40,0, 100, 45);
        bg_for_date.frame = CGRectMake(-5, CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
        lbl_from.frame = CGRectMake(30,0, 100, 45);
        line_img.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 35,110, 0.5);
        img_calender.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+85,9.5,25, 25);
        lbl_to.frame = CGRectMake(CGRectGetMidX(img_calender.frame)+30,0, 30, 45);
        line_img2.frame = CGRectMake(CGRectGetMidX(lbl_to.frame)+5, 35,110, 0.5);
        img_calender2.frame = CGRectMake(CGRectGetMidX(lbl_to.frame)+90,9.5,25, 25);
        img_table.frame = CGRectMake(0, CGRectGetMaxY(bg_for_date.frame)+5, WIDTH, HEIGHT-150);
        
        view_for_orders_in_progress.frame = CGRectMake(0,0,WIDTH,HEIGHT+90);
        lbl_date_in_progress.frame = CGRectMake(WIDTH/2-40,0, 100, 45);
        bg_for_date_in_progress.frame = CGRectMake(-5, CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
        lbl_from_in_progress.frame = CGRectMake(30,0, 100, 45);
        line_img_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 35,110, 0.5);
        img_calender_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+85,9.5,25, 25);
        lbl_to_in_progress.frame = CGRectMake(CGRectGetMidX(img_calender.frame)+30,0, 100, 45);
        line_img2_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_to_in_progress.frame)-20, 35,110, 0.5);
        img_calender2_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_to_in_progress.frame)+65,9.5,25, 25);
        table_in_order_in_progress.frame = CGRectMake(5, CGRectGetMaxY(bg_for_date.frame)+5, WIDTH-10, HEIGHT-150);
        
        view_for_orders_cancelled.frame = CGRectMake(0,0,WIDTH,HEIGHT+90);
        lbl_date_in_cancelled.frame = CGRectMake(WIDTH/2-40,0, 100, 45);
        bg_for_date_cancelled.frame = CGRectMake(-5, CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
        lbl_from_in_cancelled.frame = CGRectMake(30,0, 100, 45);
        line_img_in_cancelled.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 35,110, 0.5);
        img_calender_in_cancelled.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+85,9.5,25, 25);
        lbl_to_in_cancelled.frame = CGRectMake(CGRectGetMidX(img_calender.frame)+30,0, 50, 45);
        line_img2_in_cancelled.frame = CGRectMake(CGRectGetMidX(lbl_to_in_cancelled.frame)-20, 35,110, 0.5);
        img_calender2_in_cancelled.frame = CGRectMake(CGRectGetMidX(lbl_to_in_cancelled.frame)+65,9.5,25, 25);
        table_for_cancelled_items.frame = CGRectMake(5, CGRectGetMaxY(bg_for_date.frame)+5, WIDTH-10, HEIGHT-150);
        
        view_for_orders_completed.frame = CGRectMake(0,0,WIDTH,HEIGHT+90);
        lbl_date_in_completed.frame = CGRectMake(WIDTH/2-40,0, 100, 45);
        bg_for_date_in_completed.frame = CGRectMake(-5, CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
        lbl_from_in_completed.frame = CGRectMake(30,0, 100, 45);
        line_img_in_completed.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 35,110, 0.5);
        img_calender_in_completed.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+85,9.5,25, 25);
        lbl_to_in_completed.frame = CGRectMake(CGRectGetMidX(img_calender.frame)+30,0, 50, 45);
        line_img2_in_completed.frame = CGRectMake(CGRectGetMidX(lbl_to_in_completed.frame)-20, 35,110, 0.5);
        img_calender2_in_completed.frame = CGRectMake(CGRectGetMidX(lbl_to_in_completed.frame)+65,9.5,25, 25);
        table_for_order_completion.frame = CGRectMake(5, CGRectGetMaxY(bg_for_date.frame)+5, WIDTH-10, HEIGHT-150);
        table_on_drop_down .frame  = CGRectMake(60,45,270,100);
        
    }
    else if (IS_IPHONE_5)
    {
        scroll.frame = CGRectMake(0, 45, WIDTH, 610);
        view_for_orderonrequest.frame = CGRectMake(0,0,WIDTH,HEIGHT+90);
        lbl_date.frame = CGRectMake(WIDTH/2-30,0, 100, 45);
        bg_for_date.frame = CGRectMake(-5, CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
        lbl_from.frame = CGRectMake(30,0, 100, 45);
        line_img.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 35,90, 0.5);
        img_calender.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+65,9.5,25, 25);
        lbl_to.frame = CGRectMake(CGRectGetMidX(img_calender.frame)+30,0,50, 45);
        line_img2.frame = CGRectMake(CGRectGetMidX(lbl_to.frame)-20, 35,90, 0.5);
        img_calender2.frame = CGRectMake(CGRectGetMidX(lbl_to.frame)+45,9.5,25, 25);
        img_table.frame = CGRectMake(0, CGRectGetMaxY(bg_for_date.frame)+5, WIDTH, HEIGHT-150);
        
        view_for_orders_in_progress.frame = CGRectMake(0,0,WIDTH,HEIGHT+90);
        lbl_date_in_progress.frame = CGRectMake(WIDTH/2-40,0, 100, 45);
        bg_for_date_in_progress.frame = CGRectMake(-5, CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
        lbl_from_in_progress.frame = CGRectMake(30,0, 100, 45);
        line_img_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 35,90, 0.5);
        img_calender_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+65,9.5,25, 25);
        lbl_to_in_progress.frame = CGRectMake(CGRectGetMidX(img_calender.frame)+30,0, 50, 45);
        line_img2_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_to_in_progress.frame)-20, 35,90, 0.5);
        img_calender2_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_to_in_progress.frame)+45,9.5,25, 25);
        table_in_order_in_progress.frame = CGRectMake(5, CGRectGetMaxY(bg_for_date.frame)+5, WIDTH-10, HEIGHT-150);
        
        view_for_orders_cancelled.frame = CGRectMake(0,0,WIDTH,HEIGHT+90);
        lbl_date_in_cancelled.frame = CGRectMake(WIDTH/2-40,0, 100, 45);
        bg_for_date_cancelled.frame = CGRectMake(-5, CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
        lbl_from_in_cancelled.frame = CGRectMake(30,0, 100, 45);
        line_img_in_cancelled.frame = CGRectMake(CGRectGetMidX(lbl_from.frame),35,90, 0.5);
        img_calender_in_cancelled.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+65,9.5,25, 25);
        lbl_to_in_cancelled.frame = CGRectMake(CGRectGetMidX(img_calender.frame)+30,0, 50, 45);
        line_img2_in_cancelled.frame = CGRectMake(CGRectGetMidX(lbl_to_in_cancelled.frame)-20, 35,90, 0.5);
        img_calender2_in_cancelled.frame = CGRectMake(CGRectGetMidX(lbl_to_in_cancelled.frame)+45,9.5,25, 25);
        table_for_cancelled_items.frame = CGRectMake(5, CGRectGetMaxY(bg_for_date.frame)+5, WIDTH-10, HEIGHT-150);
        
        view_for_orders_completed.frame = CGRectMake(0,0,WIDTH,HEIGHT+90);
        lbl_date_in_completed.frame = CGRectMake(WIDTH/2-40,0, 100, 45);
        bg_for_date_in_completed.frame = CGRectMake(-5, CGRectGetMaxY(lbl_date.frame)-5, WIDTH+20, 50);
        lbl_from_in_completed.frame = CGRectMake(30,0, 100, 45);
        line_img_in_completed.frame = CGRectMake(CGRectGetMidX(lbl_from.frame), 35,90, 0.5);
        img_calender_in_completed.frame = CGRectMake(CGRectGetMidX(lbl_from.frame)+65,9.5,25, 25);
        lbl_to_in_completed.frame = CGRectMake(CGRectGetMidX(img_calender.frame)+30,0,50, 45);
        line_img2_in_completed.frame = CGRectMake(CGRectGetMidX(lbl_to_in_completed.frame)-20, 35,90, 0.5);
        img_calender2_in_completed.frame = CGRectMake(CGRectGetMidX(lbl_to_in_completed.frame)+45,9.5,25, 25);
        table_for_order_completion.frame = CGRectMake(5, CGRectGetMaxY(bg_for_date.frame)+5, WIDTH-10, HEIGHT-150);
        table_on_drop_down .frame  = CGRectMake(60,45,250,100);
        
    }
    
    [scroll setContentSize:CGSizeMake(0, 1000)];
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == table_on_drop_down)
    {
        return [array_names_in_orders count];
    }
    
    else if(tableView == img_table)
    {
        return [array_orderonrequest count];
    }
    else if (tableView ==  table_in_order_in_progress)
    {
        return [array_orderinprogress count];
    }
    else if (tableView == table_for_cancelled_items)
    {
        return 5;
    }
    else if (tableView == table_for_order_completion)
    {
        return 5;
    }
    return 0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == table_on_drop_down)
    {
        return 1;
    }
    
    else if(tableView == img_table)
    {
        return 1;
    }
    else if (tableView == table_in_order_in_progress)
    {
        return 1;
    }
    else if (tableView == table_for_cancelled_items)
    {
        return 1;
    }
    else if (tableView == table_for_order_completion)
    {
        return 1;
    }
    
    
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == table_on_drop_down)
    {
        return 40;
    }
    else if(tableView == img_table)
    {
        return 160;
    }
    else if (tableView == table_in_order_in_progress)
    {
        return 160;
    }
    else if (tableView == table_for_cancelled_items)
    {
        return 215;
    }
    else if (tableView == table_for_order_completion)
    {
        return 210;
    }
    
    
    return 0;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    if (tableView == table_on_drop_down)
    {
        img_bg_for_first_tbl = [[UIImageView alloc]init];
        img_bg_for_first_tbl.frame =  CGRectMake(0,5, 200, 40);
        //img_bg_for_first_tbl.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_bg_for_first_tbl setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_first_tbl];
        
        lbl_orders = [[UILabel alloc]init];
        lbl_orders .frame = CGRectMake(5,10,200, 15);
        lbl_orders .text = [NSString stringWithFormat:@"%@",[array_names_in_orders objectAtIndex:indexPath.row]];
        lbl_orders .font = [UIFont fontWithName:kFontBold size:15];
        lbl_orders .textColor = [UIColor blackColor];
        lbl_orders .backgroundColor = [UIColor clearColor];
        [img_bg_for_first_tbl addSubview: lbl_orders];
        
    }
    
    else if(tableView == img_table)
    {
        
        cell.backgroundColor = [UIColor clearColor];
        
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        img_cellBackGnd.frame =  CGRectMake(5,0, WIDTH-10, 150);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [img_cellBackGnd  setContentMode:UIViewContentModeScaleAspectFill];
        [img_cellBackGnd  setClipsToBounds:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UILabel *lbl_requeste_on = [[UILabel alloc]init];
        lbl_requeste_on.frame = CGRectMake(15,-5, 200, 45);
        lbl_requeste_on.text = @"Requested on:";
        lbl_requeste_on.font = [UIFont fontWithName:kFont size:15];
        lbl_requeste_on.textColor = [UIColor blackColor];
        lbl_requeste_on.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_requeste_on];
        
        UILabel *lbl_date_time = [[UILabel alloc]init];
        lbl_date_time.frame = CGRectMake(CGRectGetMidX(lbl_requeste_on.frame)+9,-4, 200, 45);
        //lbl_date_time.text =[NSString stringWithFormat:@"%@",[array_orderonrequest objectAtIndex:indexPath.row]];
        lbl_date_time.text = [[array_orderonrequest  objectAtIndex:indexPath.row] valueForKey:@"RequestedOn"];
        lbl_date_time.font = [UIFont fontWithName:kFontBold size:14];
        lbl_date_time.textColor = [UIColor blackColor];
        lbl_date_time.textAlignment = NSTextAlignmentLeft;
        lbl_date_time.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_date_time];
        
        UILabel *lbl_status = [[UILabel alloc]init];
        lbl_status.frame = CGRectMake(76,CGRectGetMidY(lbl_date_time.frame)-5, 100, 45);
        lbl_status.text = @"Status:";
        lbl_status.font = [UIFont fontWithName:kFont size:15];
        lbl_status.textColor = [UIColor blackColor];
        lbl_status.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_status];
        
        UILabel *lbl_on_request = [[UILabel alloc]init];
        lbl_on_request.frame = CGRectMake(CGRectGetMidX(lbl_status.frame),CGRectGetMidY(lbl_date_time.frame)-5, 100 , 45);
        //lbl_on_request.text = @"On Request";
        lbl_on_request.text = [[array_orderonrequest  objectAtIndex:indexPath.row] valueForKey:@"Status"];
        lbl_on_request.font = [UIFont fontWithName:kFont size:14];
        lbl_on_request.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        lbl_on_request.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_on_request];
        
        UIImageView *img_left_arrow = [[UIImageView alloc]init];
        img_left_arrow.frame = CGRectMake(05,85,11,15);
        [img_left_arrow setImage:[UIImage imageNamed:@"left-arrow-img@2x.png"]];
        img_left_arrow.backgroundColor = [UIColor clearColor];
        [img_left_arrow setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_left_arrow];
        
        UIButton *btn_on_left_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
        btn_on_left_arrow.frame = CGRectMake(4,60, 14,85);
        btn_on_left_arrow .backgroundColor = [UIColor clearColor];
        [btn_on_left_arrow setUserInteractionEnabled:YES];
        [btn_on_left_arrow addTarget:self action:@selector(btn_on_left_arrow_click:)forControlEvents:UIControlEventTouchUpInside];
        [img_cellBackGnd addSubview:btn_on_left_arrow];
        
        
        UIImageView *img_right_arrow = [[UIImageView alloc]init];
        img_right_arrow.frame = CGRectMake(345,85, 11,15);
        [img_right_arrow setImage:[UIImage imageNamed:@"right-arrow-icon@2x.png"]];
        img_right_arrow.backgroundColor = [UIColor clearColor];
        [img_right_arrow setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_right_arrow];
        
        UIButton *btn_on_right_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
        btn_on_right_arrow.frame = CGRectMake(340,60,19,85);
        btn_on_right_arrow .backgroundColor = [UIColor clearColor];
        [btn_on_right_arrow setUserInteractionEnabled:YES];
        [btn_on_right_arrow addTarget:self action:@selector(btn_on_right_arrow_click:)forControlEvents:UIControlEventTouchUpInside];
        [img_cellBackGnd addSubview:btn_on_right_arrow];
        
        UICollectionViewFlowLayout *layout;
        
        layout=[[UICollectionViewFlowLayout alloc] init];
        collView_order_on_request = [[UICollectionView alloc] initWithFrame:CGRectMake(25,53,WIDTH-55,90)
                                                       collectionViewLayout:layout];
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        [collView_order_on_request setDataSource:self];
        [collView_order_on_request setDelegate:self];
        collView_order_on_request.scrollEnabled = YES;
        collView_order_on_request.showsVerticalScrollIndicator = NO;
        collView_order_on_request.showsHorizontalScrollIndicator = NO;
        collView_order_on_request.pagingEnabled = NO;
        [collView_order_on_request registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [collView_order_on_request setBackgroundColor:[UIColor clearColor]];
        layout.minimumInteritemSpacing = 2;
        layout.minimumLineSpacing = 0;
        collView_order_on_request.userInteractionEnabled = YES;
        [cell.contentView addSubview:collView_order_on_request];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(5,0, WIDTH-10, 150);
            lbl_requeste_on.frame = CGRectMake(15,-5, 200, 45);
            lbl_date_time.frame = CGRectMake(CGRectGetMidX(lbl_requeste_on.frame)+9,-4, 200, 45);
            lbl_status.frame = CGRectMake(76,CGRectGetMidY(lbl_date_time.frame)-5, 100, 45);
            lbl_on_request.frame = CGRectMake(CGRectGetMidX(lbl_status.frame),CGRectGetMidY(lbl_date_time.frame)-5, 100 , 45);
            img_left_arrow.frame = CGRectMake(05,85,11,15);
            btn_on_left_arrow.frame = CGRectMake(4,60, 14,85);
            img_right_arrow.frame = CGRectMake(385,85, 11,15);
            btn_on_right_arrow.frame = CGRectMake(380,60,19,85);
            
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(7,0, WIDTH-10, 150);
            lbl_requeste_on.frame = CGRectMake(15,-5, 200, 45);
            lbl_date_time.frame = CGRectMake(CGRectGetMidX(lbl_requeste_on.frame)+9,-4, 200, 45);
            lbl_status.frame = CGRectMake(76,CGRectGetMidY(lbl_date_time.frame)-5, 100, 45);
            lbl_on_request.frame = CGRectMake(CGRectGetMidX(lbl_status.frame),CGRectGetMidY(lbl_date_time.frame)-5, 100 , 45);
            img_left_arrow.frame = CGRectMake(05,85,11,15);
            btn_on_left_arrow.frame = CGRectMake(4,60, 14,85);
            img_right_arrow.frame = CGRectMake(345,85, 11,15);
            btn_on_right_arrow.frame = CGRectMake(340,60,19,85);
            
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(5,0, WIDTH-10, 150);
            lbl_requeste_on.frame = CGRectMake(15,-5, 200, 45);
            lbl_date_time.frame = CGRectMake(CGRectGetMidX(lbl_requeste_on.frame)+9,-4, 200, 45);
            lbl_status.frame = CGRectMake(76,CGRectGetMidY(lbl_date_time.frame)-5, 100, 45);
            lbl_on_request.frame = CGRectMake(CGRectGetMidX(lbl_status.frame),CGRectGetMidY(lbl_date_time.frame)-5, 100 , 45);
            img_left_arrow.frame = CGRectMake(05,85,11,15);
            btn_on_left_arrow.frame = CGRectMake(4,60, 14,85);
            img_right_arrow.frame = CGRectMake(290,85, 11,15);
            btn_on_right_arrow.frame = CGRectMake(290,60,19,85);
            
        }
        
    }
    else if (tableView == table_in_order_in_progress)
    {
        
        cell.backgroundColor = [UIColor clearColor];
        
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        img_cellBackGnd.frame =  CGRectMake(3,0,WIDTH-10,150);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        [img_cellBackGnd  setContentMode:UIViewContentModeScaleAspectFill];
        [img_cellBackGnd  setClipsToBounds:YES];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        
        [cell.contentView addSubview:img_cellBackGnd];
        
        UILabel *text_order_no = [[UILabel alloc]init];
        text_order_no.frame = CGRectMake(19,-5, 200, 45);
        text_order_no.text = @"Order no.:";
        text_order_no.font = [UIFont fontWithName:kFont size:18];
        text_order_no.textColor = [UIColor blackColor];
        text_order_no.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:text_order_no];
        
        UILabel *order_no_value = [[UILabel alloc]init];
        order_no_value.frame = CGRectMake(CGRectGetMidX(text_order_no.frame),-4, 200, 45);
        //   order_no_value.text =[NSString stringWithFormat:@"%@",[ array_order_no objectAtIndex:indexPath.row]];
        order_no_value.text = [[array_orderinprogress objectAtIndex:indexPath.row] valueForKey:@""];
        order_no_value.font = [UIFont fontWithName:kFontBold size:15];
        order_no_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        order_no_value.textAlignment = NSTextAlignmentLeft;
        order_no_value.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:order_no_value];
        
        
        
        UILabel *lbl_requeste_on = [[UILabel alloc]init];
        lbl_requeste_on.frame = CGRectMake(15,CGRectGetMidY(text_order_no.frame)-5, 200, 45);
        lbl_requeste_on.text = @"Serving Time:";
        lbl_requeste_on.font = [UIFont fontWithName:kFont size:15];
        lbl_requeste_on.textColor = [UIColor blackColor];
        lbl_requeste_on.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_requeste_on];
        
        UILabel *lbl_date_time = [[UILabel alloc]init];
        lbl_date_time.frame = CGRectMake(CGRectGetMaxY(text_order_no.frame)+75,12, 200, 45);
        //lbl_date_time.text =[NSString stringWithFormat:@"%@",[array_serving_date_time objectAtIndex:indexPath.row]];
        lbl_date_time.text = [[array_orderinprogress objectAtIndex:indexPath.row] valueForKey:@""];
        lbl_date_time.font = [UIFont fontWithName:kFont size:14];
        lbl_date_time.textColor = [UIColor blackColor];
        lbl_date_time.textAlignment = NSTextAlignmentLeft;
        lbl_date_time.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_date_time];
        
        UILabel *lbl_status = [[UILabel alloc]init];
        lbl_status.frame = CGRectMake(61,CGRectGetMidY(lbl_date_time.frame)-5, 100, 45);
        lbl_status.text = @"Status:";
        lbl_status.font = [UIFont fontWithName:kFont size:15];
        lbl_status.textColor = [UIColor blackColor];
        lbl_status.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_status];
        
        UILabel *lbl_in_progress = [[UILabel alloc]init];
        lbl_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_status.frame)+3,CGRectGetMidY(lbl_date_time.frame)-5,100, 45);
        //lbl_in_progress.text =[NSString stringWithFormat:@"%@",[array_status objectAtIndex:indexPath.row]];
        lbl_status.text = [[array_orderinprogress objectAtIndex:indexPath.row] valueForKey:@""];
        lbl_in_progress.font = [UIFont fontWithName:kFont size:14];
        lbl_in_progress.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        lbl_in_progress.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_in_progress];
        
        
        
        
        UIImageView *img_left_arrow = [[UIImageView alloc]init];
        img_left_arrow.frame = CGRectMake(8,95,11,15);
        [img_left_arrow setImage:[UIImage imageNamed:@"left-arrow-img@2x.png"]];
        img_left_arrow.backgroundColor = [UIColor clearColor];
        [img_left_arrow setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_left_arrow];
        
        UIButton *btn_on_left_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
        btn_on_left_arrow.frame = CGRectMake(4,65, 18,75);
        btn_on_left_arrow .backgroundColor = [UIColor clearColor];
        [btn_on_left_arrow setUserInteractionEnabled:YES];
        [btn_on_left_arrow addTarget:self action:@selector(btn_on_left_arrow_inprogress_click:)forControlEvents:UIControlEventTouchUpInside];
        [img_cellBackGnd addSubview:btn_on_left_arrow];
        
        UIImageView *img_right_arrow = [[UIImageView alloc]init];
        img_right_arrow.frame = CGRectMake(345,95, 11,15);
        [img_right_arrow setImage:[UIImage imageNamed:@"right-arrow-icon@2x.png"]];
        img_right_arrow.backgroundColor = [UIColor clearColor];
        [img_right_arrow setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_right_arrow];
        
        UIButton *btn_on_right_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
        btn_on_right_arrow.frame = CGRectMake(343,65,18, 75);
        btn_on_right_arrow .backgroundColor = [UIColor clearColor];
        [btn_on_right_arrow setUserInteractionEnabled:YES];
        [btn_on_right_arrow addTarget:self action:@selector(btn_on_right_arrow_inprogress_click:)forControlEvents:UIControlEventTouchUpInside];
        [img_cellBackGnd addSubview:btn_on_right_arrow];
        
        UICollectionViewFlowLayout * layout2;
        //  UICollectionView * collView_in_progress;
        
        layout2=[[UICollectionViewFlowLayout alloc] init];
        collView_in_progress = [[UICollectionView alloc] initWithFrame:CGRectMake(25,60,WIDTH-55,90)
                                                  collectionViewLayout:layout2];
        [layout2 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        [collView_in_progress setDataSource:self];
        [collView_in_progress setDelegate:self];
        collView_in_progress.scrollEnabled = YES;
        collView_in_progress.showsVerticalScrollIndicator = NO;
        collView_in_progress.showsHorizontalScrollIndicator = NO;
        collView_in_progress.pagingEnabled = NO;
        [collView_in_progress registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [collView_in_progress setBackgroundColor:[UIColor clearColor]];
        layout2.minimumInteritemSpacing = 2;
        layout2.minimumLineSpacing = 0;
        collView_in_progress.userInteractionEnabled = YES;
        [cell.contentView addSubview:collView_in_progress];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            
            img_cellBackGnd.frame =  CGRectMake(3,0,WIDTH-10,150);
            text_order_no.frame = CGRectMake(19,-5, 200, 45);
            order_no_value.frame = CGRectMake(CGRectGetMidX(text_order_no.frame),-4, 200, 45);
            lbl_requeste_on.frame = CGRectMake(15,CGRectGetMidY(text_order_no.frame)-5, 200, 45);
            lbl_date_time.frame = CGRectMake(CGRectGetMaxY(text_order_no.frame)+75,12, 200, 45);
            lbl_status.frame = CGRectMake(61,CGRectGetMidY(lbl_date_time.frame)-5, 100, 45);
            lbl_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_status.frame)+3,CGRectGetMidY(lbl_date_time.frame)-5,100, 45);
            img_left_arrow.frame = CGRectMake(8,95,11,15);
            btn_on_left_arrow.frame = CGRectMake(4,65, 18,75);
            img_right_arrow.frame = CGRectMake(385,95, 11,15);
            btn_on_right_arrow.frame = CGRectMake(383,65,18, 75);
            
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(3,0,WIDTH-10,150);
            text_order_no.frame = CGRectMake(19,-5, 200, 45);
            order_no_value.frame = CGRectMake(CGRectGetMidX(text_order_no.frame),-4, 200, 45);
            lbl_requeste_on.frame = CGRectMake(15,CGRectGetMidY(text_order_no.frame)-5, 200, 45);
            lbl_date_time.frame = CGRectMake(CGRectGetMaxY(text_order_no.frame)+75,12, 200, 45);
            lbl_status.frame = CGRectMake(61,CGRectGetMidY(lbl_date_time.frame)-5, 100, 45);
            lbl_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_status.frame)+3,CGRectGetMidY(lbl_date_time.frame)-5,100, 45);
            img_left_arrow.frame = CGRectMake(8,95,11,15);
            btn_on_left_arrow.frame = CGRectMake(4,65, 18,75);
            img_right_arrow.frame = CGRectMake(345,95, 11,15);
            btn_on_right_arrow.frame = CGRectMake(343,65,18, 75);
            
            
        }
        else
        {
            
            img_cellBackGnd.frame =  CGRectMake(3,0,WIDTH-10,150);
            text_order_no.frame = CGRectMake(19,-5, 200, 45);
            order_no_value.frame = CGRectMake(CGRectGetMidX(text_order_no.frame),-4, 200, 45);
            lbl_requeste_on.frame = CGRectMake(15,CGRectGetMidY(text_order_no.frame)-5, 200, 45);
            lbl_date_time.frame = CGRectMake(CGRectGetMaxY(text_order_no.frame)+75,12, 200, 45);
            lbl_status.frame = CGRectMake(61,CGRectGetMidY(lbl_date_time.frame)-5, 100, 45);
            lbl_in_progress.frame = CGRectMake(CGRectGetMidX(lbl_status.frame)+3,CGRectGetMidY(lbl_date_time.frame)-5,100, 45);
            img_left_arrow.frame = CGRectMake(8,95,11,15);
            btn_on_left_arrow.frame = CGRectMake(4,65, 18,75);
            img_right_arrow.frame = CGRectMake(290,95, 11,15);
            btn_on_right_arrow.frame = CGRectMake(290,65,18, 75);
            
            
        }
        
        
    }
    else if (tableView == table_for_cancelled_items)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        img_cellBackGnd.frame =  CGRectMake(5,0, WIDTH-20,195);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [img_cellBackGnd  setContentMode:UIViewContentModeScaleAspectFill];
        [img_cellBackGnd  setClipsToBounds:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UILabel *text_order_no = [[UILabel alloc]init];
        text_order_no.frame = CGRectMake(19,-5, 200, 45);
        text_order_no.text = @"Order no.:";
        text_order_no.font = [UIFont fontWithName:kFontBold size:18];
        text_order_no.textColor = [UIColor blackColor];
        text_order_no.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:text_order_no];
        
        UILabel *order_no_value = [[UILabel alloc]init];
        order_no_value.frame = CGRectMake(CGRectGetMidX(text_order_no.frame)-10,-4, 200, 45);
        order_no_value.text =[NSString stringWithFormat:@"%@",[ array_order_no objectAtIndex:indexPath.row]];
        order_no_value.font = [UIFont fontWithName:kFontBold size:16];
        order_no_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        order_no_value.textAlignment = NSTextAlignmentLeft;
        order_no_value.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:order_no_value];
        
        
        UILabel *text_serving_time = [[UILabel alloc]init];
        text_serving_time.frame = CGRectMake(30,CGRectGetMidY(text_order_no.frame)-5, 200, 45);
        text_serving_time.text = @"Serving Time:";
        text_serving_time.font = [UIFont fontWithName:kFont size:15];
        text_serving_time.textColor = [UIColor blackColor];
        text_serving_time.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:text_serving_time];
        
        UILabel *lbl_serving_date_time = [[UILabel alloc]init];
        lbl_serving_date_time.frame = CGRectMake(CGRectGetMaxY(text_order_no.frame)+85,12, 200, 45);
        lbl_serving_date_time.text =[NSString stringWithFormat:@"%@",[array_Request_date_time objectAtIndex:indexPath.row]];
        lbl_serving_date_time.font = [UIFont fontWithName:kFont size:14];
        lbl_serving_date_time.textColor = [UIColor blackColor];
        lbl_serving_date_time.textAlignment = NSTextAlignmentLeft;
        lbl_serving_date_time.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_serving_date_time];
        
        UILabel *text_time_of_cancellation = [[UILabel alloc]init];
        text_time_of_cancellation.frame = CGRectMake(30,CGRectGetMidY(text_serving_time.frame)-5, 200, 45);
        text_time_of_cancellation.text = @"Time of Cancellation:";
        text_time_of_cancellation.font = [UIFont fontWithName:kFont size:15];
        text_time_of_cancellation.textColor = [UIColor blackColor];
        text_time_of_cancellation.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:text_time_of_cancellation];
        
        UILabel *cancelled_time_date_value = [[UILabel alloc]init];
        cancelled_time_date_value.frame = CGRectMake(185,CGRectGetMaxY(text_serving_time.frame)-27,200, 45);
        cancelled_time_date_value.text =[NSString stringWithFormat:@"%@",[array_time_of_ancelled_value objectAtIndex:indexPath.row]];
        cancelled_time_date_value.font = [UIFont fontWithName:kFont size:14];
        cancelled_time_date_value.textColor = [UIColor blackColor];
        cancelled_time_date_value.textAlignment = NSTextAlignmentLeft;
        cancelled_time_date_value.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:cancelled_time_date_value];
        
        
        UILabel *text_by = [[UILabel alloc]init];
        text_by.frame = CGRectMake(30,CGRectGetMidY(text_time_of_cancellation.frame)-5, 200, 45);
        text_by.text = @"By:";
        text_by.font = [UIFont fontWithName:kFont size:15];
        text_by.textColor = [UIColor blackColor];
        text_by.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:text_by];
        
        UILabel *by_me_r_chef_text = [[UILabel alloc]init];
        by_me_r_chef_text.frame = CGRectMake(55,CGRectGetMaxY(text_time_of_cancellation.frame)-27, 200, 45);
        by_me_r_chef_text.text =[NSString stringWithFormat:@"%@",[array_by_me_r_chef objectAtIndex:indexPath.row]];
        by_me_r_chef_text.font = [UIFont fontWithName:kFont size:14];
        by_me_r_chef_text.textColor = [UIColor blackColor];
        by_me_r_chef_text.textAlignment = NSTextAlignmentLeft;
        by_me_r_chef_text.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:by_me_r_chef_text];
        
        
        UILabel *text_refund_amount = [[UILabel alloc]init];
        text_refund_amount.frame = CGRectMake(30,CGRectGetMidY(text_by.frame)-5, 200, 45);
        text_refund_amount.text = @"Refund Amount:";
        text_refund_amount.font = [UIFont fontWithName:kFont size:15];
        text_refund_amount.textColor = [UIColor blackColor];
        text_refund_amount.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:text_refund_amount];
        
        UILabel *refund_amount_value = [[UILabel alloc]init];
        refund_amount_value.frame = CGRectMake(149,CGRectGetMaxY(text_by.frame)-27, 200, 45);
        refund_amount_value.text =[NSString stringWithFormat:@"%@",[array_refund_amount objectAtIndex:indexPath.row]];
        refund_amount_value.font = [UIFont fontWithName:kFont size:14];
        refund_amount_value.textColor = [UIColor blackColor];
        refund_amount_value.textAlignment = NSTextAlignmentLeft;
        refund_amount_value.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:refund_amount_value];
        
        
        UILabel *lbl_status = [[UILabel alloc]init];
        lbl_status.frame = CGRectMake(30,CGRectGetMidY(text_refund_amount.frame)-5, 100, 45);
        lbl_status.text = @"Status:";
        lbl_status.font = [UIFont fontWithName:kFont size:15];
        lbl_status.textColor = [UIColor blackColor];
        lbl_status.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_status];
        
        UILabel *lbl_on_request = [[UILabel alloc]init];
        lbl_on_request.frame = CGRectMake(CGRectGetMidX(lbl_status.frame),CGRectGetMidY(text_refund_amount.frame)-5, 100, 45);
        lbl_on_request.text = @"Cancelld";
        lbl_on_request.font = [UIFont fontWithName:kFont size:14];
        lbl_on_request.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        lbl_on_request.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_on_request];
        
        
        //  UIImageView *img_pdf = [[UIImageView alloc]init];
        //  img_pdf.frame = CGRectMake(361,10,15,20);
        //  [img_pdf setImage:[UIImage imageNamed:@"icon-pdf@2x.png"]];
        //  img_pdf.backgroundColor = [UIColor clearColor];
        //  [img_pdf setUserInteractionEnabled:YES];
        //  [img_cellBackGnd addSubview:img_pdf];
        
        
        UIButton *img_pdf = [UIButton buttonWithType:UIButtonTypeCustom];
        img_pdf.frame = CGRectMake(361,10,15,20);
        img_pdf .backgroundColor = [UIColor clearColor];
        [img_pdf  addTarget:self action:@selector(click_on_pdf_in_cancell_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_pdf setImage:[UIImage imageNamed:@"icon-pdf@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_pdf ];
        
        
        
        UIImageView *img_left_arrow = [[UIImageView alloc]init];
        img_left_arrow.frame = CGRectMake(3,138,11,15);
        [img_left_arrow setImage:[UIImage imageNamed:@"left-arrow-img@2x.png"]];
        img_left_arrow.backgroundColor = [UIColor clearColor];
        [img_left_arrow setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_left_arrow];
        
        UIButton *btn_on_left_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
        btn_on_left_arrow.frame = CGRectMake(0,120, 14,75);
        btn_on_left_arrow .backgroundColor = [UIColor clearColor];
        [btn_on_left_arrow setUserInteractionEnabled:YES];
        [btn_on_left_arrow addTarget:self action:@selector(btn_on_left_arrow_in_cancelled:)forControlEvents:UIControlEventTouchUpInside];
        [img_cellBackGnd addSubview:btn_on_left_arrow];
        
        UIImageView *img_right_arrow = [[UIImageView alloc]init];
        img_right_arrow.frame = CGRectMake(340,138,11,15);
        [img_right_arrow setImage:[UIImage imageNamed:@"right-arrow-icon@2x.png"]];
        img_right_arrow.backgroundColor = [UIColor clearColor];
        [img_right_arrow setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_right_arrow];
        
        UIButton *btn_on_right_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
        btn_on_right_arrow.frame = CGRectMake(340,120,14, 75);
        btn_on_right_arrow .backgroundColor = [UIColor clearColor];
        [btn_on_right_arrow setUserInteractionEnabled:YES];
        [btn_on_right_arrow addTarget:self action:@selector(btn_on_right_arrow_in_cancelld:)forControlEvents:UIControlEventTouchUpInside];
        [img_cellBackGnd addSubview:btn_on_right_arrow];
        
        
        UICollectionViewFlowLayout *layout;
        // UICollectionView *collView_order_cancelled;
        
        
        layout=[[UICollectionViewFlowLayout alloc] init];
        collView_order_cancelled = [[UICollectionView alloc] initWithFrame:CGRectMake(19,113,WIDTH-55,90)
                                                      collectionViewLayout:layout];
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        [collView_order_cancelled setDataSource:self];
        [collView_order_cancelled setDelegate:self];
        collView_order_cancelled.scrollEnabled = YES;
        collView_order_cancelled.showsVerticalScrollIndicator = NO;
        collView_order_cancelled.showsHorizontalScrollIndicator = NO;
        collView_order_cancelled.pagingEnabled = NO;
        [collView_order_cancelled registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [collView_order_cancelled setBackgroundColor:[UIColor clearColor]];
        layout.minimumInteritemSpacing = 2;
        layout.minimumLineSpacing = 0;
        collView_order_cancelled.userInteractionEnabled = YES;
        [cell.contentView addSubview:collView_order_cancelled];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            
            img_cellBackGnd.frame =  CGRectMake(5,0, WIDTH-20,195);
            text_order_no.frame = CGRectMake(19,-5, 200, 45);
            order_no_value.frame = CGRectMake(CGRectGetMidX(text_order_no.frame)-10,-4, 200, 45);
            text_serving_time.frame = CGRectMake(30,CGRectGetMidY(text_order_no.frame)-5, 200, 45);
            lbl_serving_date_time.frame = CGRectMake(CGRectGetMaxY(text_order_no.frame)+85,12, 200, 45);
            text_time_of_cancellation.frame = CGRectMake(30,CGRectGetMidY(text_serving_time.frame)-5, 200, 45);
            cancelled_time_date_value.frame = CGRectMake(185,CGRectGetMaxY(text_serving_time.frame)-27,200, 45);
            text_by.frame = CGRectMake(30,CGRectGetMidY(text_time_of_cancellation.frame)-5, 200, 45);
            by_me_r_chef_text.frame = CGRectMake(55,CGRectGetMaxY(text_time_of_cancellation.frame)-27, 200, 45);
            text_refund_amount.frame = CGRectMake(30,CGRectGetMidY(text_by.frame)-5, 200, 45);
            refund_amount_value.frame = CGRectMake(149,CGRectGetMaxY(text_by.frame)-27, 200, 45);
            lbl_status.frame = CGRectMake(30,CGRectGetMidY(text_refund_amount.frame)-5, 100, 45);
            lbl_on_request.frame = CGRectMake(CGRectGetMidX(lbl_status.frame),CGRectGetMidY(text_refund_amount.frame)-5, 100, 45);
            img_left_arrow.frame = CGRectMake(3,138,11,15);
            btn_on_left_arrow.frame = CGRectMake(0,120, 14,75);
            img_right_arrow.frame = CGRectMake(375,138,11,15);
            btn_on_right_arrow.frame = CGRectMake(375,120,14, 75);
            
            img_pdf.frame = CGRectMake(340,10,23,25);
            
            
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(5,0, WIDTH-20,215);
            text_order_no.frame = CGRectMake(19,-5, 200, 45);
            order_no_value.frame = CGRectMake(CGRectGetMidX(text_order_no.frame)-10,-4, 200, 45);
            text_serving_time.frame = CGRectMake(30,CGRectGetMidY(text_order_no.frame)-5, 200, 45);
            lbl_serving_date_time.frame = CGRectMake(CGRectGetMaxY(text_order_no.frame)+85,12, 200, 45);
            text_time_of_cancellation.frame = CGRectMake(30,CGRectGetMidY(text_serving_time.frame)-5, 200, 45);
            cancelled_time_date_value.frame = CGRectMake(185,CGRectGetMaxY(text_serving_time.frame)-27,200, 45);
            text_by.frame = CGRectMake(30,CGRectGetMidY(text_time_of_cancellation.frame)-5, 200, 45);
            by_me_r_chef_text.frame = CGRectMake(55,CGRectGetMaxY(text_time_of_cancellation.frame)-27, 200, 45);
            text_refund_amount.frame = CGRectMake(30,CGRectGetMidY(text_by.frame)-5, 200, 45);
            refund_amount_value.frame = CGRectMake(149,CGRectGetMaxY(text_by.frame)-27, 200, 45);
            lbl_status.frame = CGRectMake(30,CGRectGetMidY(text_refund_amount.frame)-5, 100, 45);
            lbl_on_request.frame = CGRectMake(CGRectGetMidX(lbl_status.frame),CGRectGetMidY(text_refund_amount.frame)-5, 100, 45);
            img_left_arrow.frame = CGRectMake(3,138,11,15);
            btn_on_left_arrow.frame = CGRectMake(0,120, 14,75);
            img_right_arrow.frame = CGRectMake(340,138,11,15);
            btn_on_right_arrow.frame = CGRectMake(340,120,14, 75);
            
            img_pdf.frame = CGRectMake(310,10,22,25);
            
            
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(5,0, WIDTH-20,195);
            text_order_no.frame = CGRectMake(19,-5, 200, 45);
            order_no_value.frame = CGRectMake(CGRectGetMidX(text_order_no.frame)-10,-4, 200, 45);
            text_serving_time.frame = CGRectMake(30,CGRectGetMidY(text_order_no.frame)-5, 200, 45);
            lbl_serving_date_time.frame = CGRectMake(CGRectGetMaxY(text_order_no.frame)+85,12, 200, 45);
            text_time_of_cancellation.frame = CGRectMake(30,CGRectGetMidY(text_serving_time.frame)-5, 200, 45);
            cancelled_time_date_value.frame = CGRectMake(185,CGRectGetMaxY(text_serving_time.frame)-27,200, 45);
            text_by.frame = CGRectMake(30,CGRectGetMidY(text_time_of_cancellation.frame)-5, 200, 45);
            by_me_r_chef_text.frame = CGRectMake(55,CGRectGetMaxY(text_time_of_cancellation.frame)-27, 200, 45);
            text_refund_amount.frame = CGRectMake(30,CGRectGetMidY(text_by.frame)-5, 200, 45);
            refund_amount_value.frame = CGRectMake(149,CGRectGetMaxY(text_by.frame)-27, 200, 45);
            lbl_status.frame = CGRectMake(30,CGRectGetMidY(text_refund_amount.frame)-5, 100, 45);
            lbl_on_request.frame = CGRectMake(CGRectGetMidX(lbl_status.frame),CGRectGetMidY(text_refund_amount.frame)-5, 100, 45);
            img_left_arrow.frame = CGRectMake(3,138,11,15);
            btn_on_left_arrow.frame = CGRectMake(0,120, 14,75);
            img_right_arrow.frame = CGRectMake(286,138,11,15);
            btn_on_right_arrow.frame = CGRectMake(290,120,14, 75);
            
            img_pdf.frame = CGRectMake(270,10,23,25);
            
            
            
            
        }
        
        
    }
    
    else if (tableView == table_for_order_completion)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        img_cellBackGnd.frame =  CGRectMake(5,0, WIDTH-20, 210);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [img_cellBackGnd  setContentMode:UIViewContentModeScaleAspectFill];
        [img_cellBackGnd  setClipsToBounds:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UILabel *text_order_no = [[UILabel alloc]init];
        text_order_no.frame = CGRectMake(19,-5, 200, 45);
        text_order_no.text = @"Order no.:";
        text_order_no.font = [UIFont fontWithName:kFontBold size:20];
        text_order_no.textColor = [UIColor blackColor];
        text_order_no.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:text_order_no];
        
        UILabel *order_no_value = [[UILabel alloc]init];
        order_no_value.frame = CGRectMake(CGRectGetMidX(text_order_no.frame),-4, 200, 45);
        order_no_value.text =[NSString stringWithFormat:@"%@",[ array_order_no objectAtIndex:indexPath.row]];
        order_no_value.font = [UIFont fontWithName:kFont size:20];
        order_no_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        order_no_value.textAlignment = NSTextAlignmentLeft;
        order_no_value.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:order_no_value];
        
        
        UILabel *text_serving_time = [[UILabel alloc]init];
        text_serving_time.frame = CGRectMake(19,CGRectGetMidY(text_order_no.frame), 200, 45);
        text_serving_time.text = @"Serving Time:";
        text_serving_time.font = [UIFont fontWithName:kFont size:15];
        text_serving_time.textColor = [UIColor blackColor];
        text_serving_time.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:text_serving_time];
        
        UILabel *lbl_serving_date_time = [[UILabel alloc]init];
        lbl_serving_date_time.frame = CGRectMake(115,CGRectGetMaxY(text_order_no.frame)-22,200, 45);
        lbl_serving_date_time.text =[NSString stringWithFormat:@"%@",[array_Request_date_time objectAtIndex:indexPath.row]];
        lbl_serving_date_time.font = [UIFont fontWithName:kFont size:15];
        lbl_serving_date_time.textColor = [UIColor blackColor];
        lbl_serving_date_time.textAlignment = NSTextAlignmentLeft;
        lbl_serving_date_time.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_serving_date_time];
        
        UILabel *text_serving_location = [[UILabel alloc]init];
        text_serving_location.frame = CGRectMake(19,CGRectGetMidY(text_serving_time.frame), 200, 45);
        text_serving_location.text = @"Serving Location:";
        text_serving_location.font = [UIFont fontWithName:kFont size:15];
        text_serving_location.textColor = [UIColor blackColor];
        text_serving_location.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:text_serving_location];
        
        UILabel *location_name = [[UILabel alloc]init];
        location_name.frame = CGRectMake(148,CGRectGetMaxY(text_serving_time.frame)-22, 200, 45);
        location_name.text =[NSString stringWithFormat:@"%@",[array_serving_location objectAtIndex:indexPath.row]];
        location_name.font = [UIFont fontWithName:kFont size:15];
        location_name.textColor = [UIColor blackColor];
        location_name.textAlignment = NSTextAlignmentLeft;
        location_name.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:location_name];
        
        
        UILabel *lbl_status = [[UILabel alloc]init];
        lbl_status.frame = CGRectMake(19,CGRectGetMidY(text_serving_location.frame), 100, 45);
        lbl_status.text = @"Status:";
        lbl_status.font = [UIFont fontWithName:kFont size:15];
        lbl_status.textColor = [UIColor blackColor];
        lbl_status.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_status];
        
        UILabel *lbl_on_request = [[UILabel alloc]init];
        lbl_on_request.frame = CGRectMake(CGRectGetMidX(lbl_status.frame),CGRectGetMidY(text_serving_location.frame), 100, 45);
        lbl_on_request.text = @"Completed";
        lbl_on_request.font = [UIFont fontWithName:kFont size:15];
        lbl_on_request.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        lbl_on_request.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_on_request];
        
        UIButton *img_pdf = [UIButton buttonWithType:UIButtonTypeCustom];
        img_pdf.frame = CGRectMake(361,10,15,20);
        img_pdf .backgroundColor = [UIColor clearColor];
        [img_pdf  addTarget:self action:@selector(click_on_pdf_in_order_complet_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_pdf setImage:[UIImage imageNamed:@"icon-pdf@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_pdf ];
        
        
        UIButton *img_edit = [UIButton buttonWithType:UIButtonTypeCustom];
        img_edit.frame = CGRectMake(363,45,18,18);
        img_edit .backgroundColor = [UIColor clearColor];
        [img_edit  addTarget:self action:@selector(click_on_edit_in_order_complet_btn:) forControlEvents:UIControlEventTouchUpInside];
        [img_edit setImage:[UIImage imageNamed:@"img-edit-text@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_edit ];
        
        
        
        UIImageView *img_left_arrow = [[UIImageView alloc]init];
        img_left_arrow.frame = CGRectMake(2,138,11,15);
        [img_left_arrow setImage:[UIImage imageNamed:@"left-arrow-img@2x.png"]];
        img_left_arrow.backgroundColor = [UIColor clearColor];
        [img_left_arrow setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_left_arrow];
        
        UIButton *btn_on_left_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
        btn_on_left_arrow.frame = CGRectMake(4,80, 17,65);
        btn_on_left_arrow .backgroundColor = [UIColor clearColor];
        [btn_on_left_arrow setUserInteractionEnabled:YES];
        [btn_on_left_arrow addTarget:self action:@selector(btn_on_left_arrow_on_ordercmp:)forControlEvents:UIControlEventTouchUpInside];
        [img_cellBackGnd addSubview:btn_on_left_arrow];
        
        UIImageView *img_right_arrow = [[UIImageView alloc]init];
        img_right_arrow.frame = CGRectMake(340,138,11,15);
        [img_right_arrow setImage:[UIImage imageNamed:@"right-arrow-icon@2x.png"]];
        img_right_arrow.backgroundColor = [UIColor clearColor];
        [img_right_arrow setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_right_arrow];
        
        UIButton *btn_on_right_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
        btn_on_right_arrow.frame = CGRectMake(391,80,17, 65);
        btn_on_right_arrow .backgroundColor = [UIColor clearColor];
        [btn_on_right_arrow setUserInteractionEnabled:YES];
        [btn_on_right_arrow addTarget:self action:@selector(btn_on_right_arrow_on_ordercmp:)forControlEvents:UIControlEventTouchUpInside];
        [img_cellBackGnd addSubview:btn_on_right_arrow];
        
        
        UICollectionViewFlowLayout *layout;
        // UICollectionView *collView_order_completed;
        
        
        layout=[[UICollectionViewFlowLayout alloc] init];
        collView_order_completed = [[UICollectionView alloc] initWithFrame:CGRectMake(20,105,WIDTH-55,90)
                                                      collectionViewLayout:layout];
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        [collView_order_completed setDataSource:self];
        [collView_order_completed setDelegate:self];
        collView_order_completed.scrollEnabled = YES;
        collView_order_completed.showsVerticalScrollIndicator = NO;
        collView_order_completed.showsHorizontalScrollIndicator = NO;
        collView_order_completed.pagingEnabled = NO;
        [collView_order_completed registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [collView_order_completed setBackgroundColor:[UIColor clearColor]];
        layout.minimumInteritemSpacing = 2;
        layout.minimumLineSpacing = 0;
        collView_order_completed.userInteractionEnabled = YES;
        [cell.contentView addSubview:collView_order_completed];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            
            img_cellBackGnd.frame =  CGRectMake(5,0, WIDTH-20, 210);
            text_order_no.frame = CGRectMake(19,-5, 200, 45);
            order_no_value.frame = CGRectMake(CGRectGetMidX(text_order_no.frame),-4, 200, 45);
            text_serving_time.frame = CGRectMake(19,CGRectGetMidY(text_order_no.frame), 200, 45);
            lbl_serving_date_time.frame = CGRectMake(115,CGRectGetMaxY(text_order_no.frame)-22,200, 45);
            text_serving_location.frame = CGRectMake(19,CGRectGetMidY(text_serving_time.frame), 200, 45);
            location_name.frame = CGRectMake(148,CGRectGetMaxY(text_serving_time.frame)-22, 200, 45);
            lbl_status.frame = CGRectMake(19,CGRectGetMidY(text_serving_location.frame), 100, 45);
            lbl_on_request.frame = CGRectMake(CGRectGetMidX(lbl_status.frame),CGRectGetMidY(text_serving_location.frame), 100, 45);
            img_pdf.frame = CGRectMake(361,10,15,20);
            img_edit.frame = CGRectMake(363,45,18,18);
            img_left_arrow.frame = CGRectMake(2,138,11,15);
            btn_on_left_arrow.frame = CGRectMake(0,120, 17,65);
            img_right_arrow.frame = CGRectMake(375,138,11,15);
            btn_on_right_arrow.frame = CGRectMake(370,120,17, 65);
            
            img_pdf.frame = CGRectMake(340,10,23,25);
            img_edit.frame = CGRectMake(342,45,25,25);
            
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(5,0, WIDTH-20, 210);
            text_order_no.frame = CGRectMake(19,-5, 200, 45);
            order_no_value.frame = CGRectMake(CGRectGetMidX(text_order_no.frame),-4, 200, 45);
            text_serving_time.frame = CGRectMake(19,CGRectGetMidY(text_order_no.frame), 200, 45);
            lbl_serving_date_time.frame = CGRectMake(115,CGRectGetMaxY(text_order_no.frame)-22,200, 45);
            text_serving_location.frame = CGRectMake(19,CGRectGetMidY(text_serving_time.frame), 200, 45);
            location_name.frame = CGRectMake(148,CGRectGetMaxY(text_serving_time.frame)-22, 200, 45);
            lbl_status.frame = CGRectMake(19,CGRectGetMidY(text_serving_location.frame), 100, 45);
            lbl_on_request.frame = CGRectMake(CGRectGetMidX(lbl_status.frame),CGRectGetMidY(text_serving_location.frame), 100, 45);
            img_pdf.frame = CGRectMake(361,10,15,20);
            img_edit.frame = CGRectMake(363,45,18,18);
            img_left_arrow.frame = CGRectMake(2,138,11,15);
            btn_on_left_arrow.frame = CGRectMake(0,120, 17,65);
            img_right_arrow.frame = CGRectMake(340,138,11,15);
            btn_on_right_arrow.frame = CGRectMake(340,120,17, 65);
            
            img_pdf.frame = CGRectMake(310,10,23,25);
            img_edit.frame = CGRectMake(312,45,25,25);
            
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(5,0, WIDTH-20, 210);
            text_order_no.frame = CGRectMake(19,-5, 200, 45);
            order_no_value.frame = CGRectMake(CGRectGetMidX(text_order_no.frame)-10,-4, 200, 45);
            text_serving_time.frame = CGRectMake(19,CGRectGetMidY(text_order_no.frame), 200, 45);
            lbl_serving_date_time.frame = CGRectMake(105,CGRectGetMaxY(text_order_no.frame)-22,200, 45);
            text_serving_location.frame = CGRectMake(20,CGRectGetMidY(text_serving_time.frame), 200, 45);
            location_name.frame = CGRectMake(135,CGRectGetMaxY(text_serving_time.frame)-22, 200, 45);
            lbl_status.frame = CGRectMake(19,CGRectGetMidY(text_serving_location.frame), 100, 45);
            lbl_on_request.frame = CGRectMake(CGRectGetMidX(lbl_status.frame)-8,CGRectGetMidY(text_serving_location.frame), 100, 45);
            img_pdf.frame = CGRectMake(361,10,15,20);
            img_edit.frame = CGRectMake(363,45,18,18);
            img_left_arrow.frame = CGRectMake(2,138,11,15);
            btn_on_left_arrow.frame = CGRectMake(0,115,17,75);
            img_right_arrow.frame = CGRectMake(290,138,11,15);
            btn_on_right_arrow.frame = CGRectMake(283,115,17, 75);
            
            img_pdf.frame = CGRectMake(270,10,23,25);
            img_edit.frame = CGRectMake(272,45,25,25);
            
            
            
            text_order_no.font = [UIFont fontWithName:kFontBold size:18];
            order_no_value.font = [UIFont fontWithName:kFont size:18];
            
            text_serving_time.font = [UIFont fontWithName:kFont size:13];
            lbl_serving_date_time.font = [UIFont fontWithName:kFont size:13];
            text_serving_location.font = [UIFont fontWithName:kFont size:13];
            location_name.font = [UIFont fontWithName:kFont size:13];
            lbl_status.font = [UIFont fontWithName:kFont size:13];
            lbl_on_request.font = [UIFont fontWithName:kFont size:13];
            
            
            
            
        }
        
    }
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView == table_on_drop_down)
    {
        if (indexPath.row == 0)
        {
            view_for_orderonrequest.hidden = NO;
            view_for_orders_in_progress.hidden = YES;
            view_for_orders_cancelled.hidden = YES;
            view_for_orders_completed.hidden = YES;
            
        }
        else if (indexPath.row == 1)
        {
            view_for_orderonrequest.hidden = YES;
            view_for_orders_in_progress.hidden = NO;
            view_for_orders_cancelled.hidden = YES;
            view_for_orders_completed.hidden = YES;
            
        }
        else if (indexPath.row == 2)
        {
            view_for_orderonrequest.hidden = YES;
            view_for_orders_in_progress.hidden = YES;
            view_for_orders_cancelled.hidden = NO;
            view_for_orders_completed.hidden = YES;
        }
        else if(indexPath.row == 3)
        {
            view_for_orderonrequest.hidden = YES;
            view_for_orders_in_progress.hidden = YES;
            view_for_orders_cancelled.hidden = YES;
            view_for_orders_completed.hidden = NO;
        }
        
        lbl_User_Sign_Up.text =[array_names_in_orders objectAtIndex:indexPath.row];
        [table_on_drop_down setHidden:YES];
    }
    
    else if (tableView == img_table)
    {
        if (indexPath.row == 0)
        {
            
        }
    }
    
    else if (tableView == table_in_order_in_progress)
    {
        
    }
    
}


//table complete

#pragma mark UiCollectionView Delegate Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.collView_order_on_request)
    {
        return [array_orderonrequest count];
        
    }
    else if (collectionView == self.collView_in_progress )
    {
        return [array_orderinprogress  count];
    }
    else if (collectionView == self.collView_order_cancelled)
    {
        return 10;
        
    }
    else if (collectionView == self.collView_order_completed)
    {
        return 10;
        
    }
    return 1;
    
    
    
    
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView1 dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    if (collectionView1 == self.collView_order_on_request)
    {
        
        
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,-20,85,103);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        
        UIImageView *cuisines_icon = [[UIImageView alloc]init];
        cuisines_icon .frame = CGRectMake(5,8,70,70);
        // [cuisines_icon setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_cuisine_imges objectAtIndex:indexPath.row]]]];
        //    NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[array_orderonrequest objectAtIndex:indexPath.row] valueForKey:@"Dish_Meal_Image"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //    [cuisines_icon setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@""]];
        //
        NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@",[[array_orderonrequest objectAtIndex:indexPath.row] valueForKey:@"Dish_Meal_Image"]]];
        
        NSData *myData = [NSData dataWithContentsOfURL:url];
        
        cuisines_icon.image = [UIImage imageWithData:myData];
        // [chef_pro_icons  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        cuisines_icon .backgroundColor = [UIColor clearColor];
        [cuisines_icon  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:cuisines_icon];
        cuisines_icon.hidden = NO;
    }
    
    else if (collectionView1 == self.collView_in_progress )
    {
        
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,-20,85,103);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        
        UIImageView *cuisines_icon = [[UIImageView alloc]init];
        cuisines_icon .frame = CGRectMake(5,8,70,70);
        // [cuisines_icon setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_cuisine_imges objectAtIndex:indexPath.row]]]];
        NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@",[[array_orderinprogress objectAtIndex:indexPath.row] valueForKey:@""]]];
        
        NSData *myData = [NSData dataWithContentsOfURL:url];
        
        cuisines_icon.image = [UIImage imageWithData:myData];
        // [chef_pro_icons  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        cuisines_icon .backgroundColor = [UIColor clearColor];
        [cuisines_icon  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:cuisines_icon];
        cuisines_icon.hidden = NO;
        
    }
    
    else if (collectionView1 == self.collView_order_cancelled )
    {
        
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,-20,85,103);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        
        UIImageView *cuisines_icon = [[UIImageView alloc]init];
        cuisines_icon .frame = CGRectMake(5,8,70,70);
        [cuisines_icon setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_cuisine_imges objectAtIndex:indexPath.row]]]];
        //        NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@",[[array_orderonrequest objectAtIndex:indexPath.row] valueForKey:@"Dish_Meal_Image"]]];
        //
        //        NSData *myData = [NSData dataWithContentsOfURL:url];
        //
        //        cuisines_icon.image = [UIImage imageWithData:myData];
        // [chef_pro_icons  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        cuisines_icon .backgroundColor = [UIColor clearColor];
        [cuisines_icon  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:cuisines_icon];
        cuisines_icon.hidden = NO;
        
    }
    else if (collectionView1 == self.collView_order_completed )
    {
        
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,-20,85,103);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        
        UIImageView *cuisines_icon = [[UIImageView alloc]init];
        cuisines_icon .frame = CGRectMake(5,8,70,70);
        [cuisines_icon setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_cuisine_imges objectAtIndex:indexPath.row]]]];
        //        NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@",[[array_orderonrequest objectAtIndex:indexPath.row] valueForKey:@"Dish_Meal_Image"]]];
        //
        //        NSData *myData = [NSData dataWithContentsOfURL:url];
        //
        //        cuisines_icon.image = [UIImage imageWithData:myData];
        // [chef_pro_icons  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        cuisines_icon .backgroundColor = [UIColor clearColor];
        [cuisines_icon  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:cuisines_icon];
        cuisines_icon.hidden = NO;
        
    }
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == self.collView_order_on_request)
    {
        
        return CGSizeMake((WIDTH)/5, 50);
    }
    else if (collectionView == self.collView_in_progress )
    {
        return CGSizeMake((WIDTH)/5, 50);
    }
    else if (collectionView == self.collView_order_cancelled)
    {
        return CGSizeMake((WIDTH)/5, 50);
    }
    else if (collectionView == self.collView_order_completed)
    {
        return CGSizeMake((WIDTH)/5, 50);
    }
    
    return CGSizeMake((WIDTH)/5, 50);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
}



#pragma buttne actions

#pragma mark - Date Picker frome date in order request

- (void)datePickerValueChanged:(id)sender
{
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *str=[formatter stringFromDate:datePicker.date];
    
    
    txt_from_date.font = [UIFont fontWithName:kFont size:14];
    txt_from_date.textColor = [UIColor blackColor];
    
    NSLog(@"gdj%@", [str substringToIndex:2]);
    
    [txt_from_date setText:str_DOBfinal];
    
}

-(void) click_DoneDate:(id) sender
{
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *str=[formatter stringFromDate:datePicker.date];
    
    [txt_from_date setText:str];
    [self.view endEditing:YES];
}


- (void)datePicker_to_ValueChanged:(id)sender
{
    [formatter2 setDateFormat:@"yyyy-MM-dd"];
    NSString *str=[formatter2 stringFromDate:datePicker2.date];
    txt_to_date_onrequest.font = [UIFont fontWithName:kFont size:14];
    txt_to_date_onrequest.textColor = [UIColor blackColor];
    
    NSLog(@"gdj%@", [str substringToIndex:2]);
    
    [txt_to_date_onrequest setText:str];
    
}

-(void) click_Done_to_Date:(id) sender
{
    [formatter2 setDateFormat:@"yyyy-MM-dd"];
    NSString *str=[formatter2 stringFromDate:datePicker2.date];
    
    [txt_to_date_onrequest setText:str];
    [self.view endEditing:YES];
}

-(void) click_DoneDate_inprogress:(id) sender
{
    [formatter_inprogress setDateFormat:@"yyyy-MM-dd"];
    NSString *str=[formatter_inprogress stringFromDate:datePicker_inprogress.date];
    
    [txt_from_date_inprogress setText:str];
    [self.view endEditing:YES];
}

-(void)click_DonetoDate_inprogress:(id) sender
{
    [formatter_inprogress2 setDateFormat:@"yyyy-MM-dd"];
    NSString *str=[formatter_inprogress2 stringFromDate:datePicker_inprogress2.date];
    
    [txt_to_date_inprogress setText:str];
    [self.view endEditing:YES];
}





-(void)btn_menu_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click");
    [self dismissViewControllerAnimated:NO completion:nil];
    //    [self.navigationController popViewControllerAnimated:NO];
    
    
}
-(void)btn_dropdown_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click");
    
    
    
    
}
-(void)click_on_logo_btn:(UIButton *)sender
{
    NSLog(@"click_on_logo_btn");
    UserSupportVC *vc = [[UserSupportVC alloc]init];
    [self presentViewController:vc animated:NO completion:nil];
}

-(void)btn_on_dropdown_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click");
    table_on_drop_down.hidden = NO;
    
    
}
-(void)btn_on_left_arrow_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click");
    
    [self.collView_order_on_request scrollToItemAtIndexPath:0 atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:NO];
    
    
    
}
-(void)btn_on_right_arrow_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click");
    
    
}
-(void)click_on_pdf_in_cancell_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_pdf_in_order_complet_btn:(UIButton *)sender
{
    NSLog(@"");
}
-(void)click_on_edit_in_order_complet_btn:(UIButton *)sender
{
    NSLog(@"");
}

#pragma  Functionality


-(void)AForderonrequest
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    
    
    NSDictionary *params =@{
                            
                            @"uid"                              :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"startDate"                        :  txt_from_date.text,
                            @"endDate"                          :  txt_to_date_onrequest.text,
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:korderonrequest
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responseorderonrequest :JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AForderonrequest];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responseorderonrequest :(NSDictionary * )TheDict
{
    NSLog(@"foodlist: %@",TheDict);
    
    [array_orderonrequest removeAllObjects];
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"OrderOnRequest"] count]; i++)
        {
            
            [array_orderonrequest addObject:[[TheDict valueForKey:@"OrderOnRequest"] objectAtIndex:i]];
            
        }
        
        [img_table reloadData];
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
    
    
}


-(void)AForderinprogress
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    
    
    NSDictionary *params =@{
                            
                            @"uid"                              :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"startDate"                        :  txt_from_date.text,
                            @"endDate"                          :  txt_to_date_onrequest.text,
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:korderinprogress
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responseorderinprogress :JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AForderinprogress];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responseorderinprogress :(NSDictionary * )TheDict
{
    NSLog(@"foodlist: %@",TheDict);
    
    [array_orderinprogress removeAllObjects];
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"OrderOnRequest"] count]; i++)
        {
            
            [array_orderinprogress addObject:[[TheDict valueForKey:@"OrderOnRequest"] objectAtIndex:i]];
            
        }
        
        [table_in_order_in_progress reloadData];
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
    
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end

