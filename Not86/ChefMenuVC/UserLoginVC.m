//
//  UserLoginVC.m
//  Not86
//
//  Created by Admin on 29/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "UserLoginVC.h"
#import "ChefHomeViewController.h"
#import "HomeVC.h"
#import "AppDelegate.h"
#import "Define.h"
#import "ForgetpasswordVC.h"
#import "JWSlideMenuViewController.h"
#import "JWSlideMenuController.h"
#import "HomeViewController1.h"

#import "MessagesVC.h"
#import "SignupViewController.h"
#import "ChefSignup.h"
#import "ChefSignup2.h"
#import "Chefsignup3.h"
#import "ChefHomeViewController.h"
#import "HomeVC.h"
#import "MenuScreenVC.h"
#import "MyScheduleScreenVC.h"
#import "MyProfileVC.h"
#import "MyHomescreenVC.h"
#import "MyScheduleVC.h"
#import "ViewMenuVC.h"

#import "MyMenuVC.h"
#import "MyAccountVC.h"
#import "MessagesVC.h"
#import "SwitchtoUser.h"
#import "MessageListVC.h"
#import "SerchForFoodNowVC.h"
#import "DiningCartVC.h"
#import "ChefProfileVC.h"
#import "NotificationsVC.h"
#import "UserLoginVC.h"
//#import "HomeVC.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "Define.h"
#import "MyordersVC.h"
#import "SSKeychain.h"
static NSString *kSSToolkitTestsServiceName =@"KIVSSToolkitTestService";


//#import "UserForgotPasswordVC.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"
#import "AppDelegate.h"



@interface UserLoginVC ()<UITextFieldDelegate>
{
    UIImageView *img_header;
    UIImageView *img_background;
    CGFloat	animatedDistance;
    
    AppDelegate *delegate;
    UIView *view_popup;
    UIImageView *img_alertpop;
    
    UITextField *txt_user_Name;
    UITextField *txt_password;
    
    UIView*alertviewBg;
    UIButton *img_chek_box;
    // NSString * rememberMe;
    UIView * view_Forgetpassword;
    UITextField * txt_Emailid;
    UIButton * btn_Submittl;
    UIButton * btn_Cancel;
    UIButton * btn_Submitt;
    BOOL rememberMe;
}


@end

@implementation UserLoginVC

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;



-(void)viewWillAppear:(BOOL)animated{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    
    if([defaults valueForKey:@"loginKey"])
    {
        NSArray *accounts = [SSKeychain allAccounts];
        accounts = [SSKeychain accountsForService:kSSToolkitTestsServiceName];
        if(accounts.count>0)
        {
            txt_user_Name.text=[[accounts lastObject] valueForKey:@"acct"];
            NSString *password = [SSKeychain passwordForService:kSSToolkitTestsServiceName account:txt_user_Name.text];
            txt_password.text=password;
        }
        [img_chek_box setImage:[UIImage imageNamed:@"img_checkok@2x.png"] forState:UIControlStateNormal];
        
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // rememberMe =@"YES";
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBodyDesign];
    
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}


-(void)integrateHeader
{
    img_header=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 45)];
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,18,18);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back addTarget:self action:@selector(btn_back_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_back];
    
    UILabel *lbl_Login = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(icon_back.frame)+30,0,200, 45)];
    lbl_Login.text = @"Login";
    lbl_Login.font = [UIFont fontWithName:kFont size:20];
    lbl_Login.textColor = [UIColor whiteColor];
    lbl_Login.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_Login];
    
    UIImageView *img_circle = [[UIImageView alloc]init];
    img_circle.frame =CGRectMake(WIDTH-40, 8, 30, 30);
    [img_circle setImage:[UIImage imageNamed:@"img_user_icon@2x.png"]];
    //img_circle.backgroundColor = [UIColor whiteColor];
    [img_circle setUserInteractionEnabled:YES];
    [img_header addSubview:img_circle];
    
    
}
-(void)integrateBodyDesign
{
    img_background = [[UIImageView alloc]init];
    // img_background.frame = CGRectMake(30, CGRectGetMaxY(img_header.frame)+30, WIDTH-55, 85);
    [img_background setImage:[UIImage imageNamed:@"img_logo_name@2x.png"]];
    //    img_eat.backgroundColor = [UIColor redColor];
    [img_background setUserInteractionEnabled:YES];
    [self.view addSubview:img_background];
    
    
    
    txt_user_Name = [[UITextField alloc] init];
    //txt_user_Name .frame=CGRectMake(10, CGRectGetMaxY( img_background.frame)-20, WIDTH, 30);
    txt_user_Name .borderStyle = UITextBorderStyleNone;
    txt_user_Name .textColor = [UIColor blackColor];
    txt_user_Name .font = [UIFont fontWithName:kFont size:13];
    txt_user_Name .placeholder = @"Username";
    [txt_user_Name  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_user_Name  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_user_Name .leftView = padding1;
    txt_user_Name .leftViewMode = UITextFieldViewModeAlways;
    txt_user_Name .userInteractionEnabled=YES;
    txt_user_Name .textAlignment = NSTextAlignmentLeft;
    txt_user_Name .backgroundColor = [UIColor clearColor];
    txt_user_Name .keyboardType = UIKeyboardTypeAlphabet;
    txt_user_Name .delegate = self;
    [self.view addSubview:txt_user_Name ];
    
    UIImageView * img_line = [[UIImageView alloc]init];
    // img_line .frame = CGRectMake(10, CGRectGetMaxY(  txt_user_Name.frame)+5, WIDTH-60, 0.5);
    [img_line setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_line setUserInteractionEnabled:YES];
    [img_background addSubview:img_line];
    
    txt_password = [[UITextField alloc] init];
    //  txt_password .frame=CGRectMake(10, CGRectGetMaxY( img_line.frame)+10, WIDTH, 30);
    txt_password .borderStyle = UITextBorderStyleNone;
    txt_password .textColor = [UIColor blackColor];
    txt_password .font = [UIFont fontWithName:kFont size:13];
    txt_password .placeholder = @"Password";
    [txt_password  setValue:[UIFont fontWithName:kFont size:16]forKeyPath:@"_placeholderLabel.font"];
    [txt_password  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    txt_password .secureTextEntry = YES;
    UIView *padding2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_password .leftView = padding2;
    txt_password .leftViewMode = UITextFieldViewModeAlways;
    txt_password .userInteractionEnabled=YES;
    txt_password .textAlignment = NSTextAlignmentLeft;
    txt_password .backgroundColor = [UIColor clearColor];
    txt_password .keyboardType = UIKeyboardTypeAlphabet;
    txt_password .delegate = self;
    [self.view addSubview:txt_password ];
    
    UIImageView * img_line1 = [[UIImageView alloc]init];
    // img_line1 .frame = CGRectMake(10, CGRectGetMaxY(  txt_password.frame)+5, WIDTH-60, 0.5);
    [img_line1 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_line1 setUserInteractionEnabled:YES];
    [img_background addSubview:img_line1];
    
    
    img_chek_box = [UIButton buttonWithType:UIButtonTypeCustom];
    // img_chek_box.frame = CGRectMake(10,CGRectGetMaxY(  img_line1.frame)+15,20,20);
    img_chek_box .backgroundColor = [UIColor clearColor];
    [img_chek_box addTarget:self action:@selector(btn_check_box_click:) forControlEvents:UIControlEventTouchUpInside];
    img_chek_box.userInteractionEnabled = YES;
    [img_chek_box setImage:[UIImage imageNamed:@"img-check@2x.png"] forState:UIControlStateNormal];
    [img_background   addSubview:img_chek_box];
    
    
    UILabel *lbl_remember_me = [[UILabel alloc]init ];
    //   lbl_remember_me.frame =  CGRectMake( CGRectGetMaxX( img_chek_box.frame)+65, CGRectGetMaxY( img_line1.frame)+270, 100, 20);
    lbl_remember_me.text = @"Remember me";
    lbl_remember_me.font = [UIFont fontWithName:kFont size:13];
    lbl_remember_me.textColor = [UIColor blackColor];
    lbl_remember_me.backgroundColor = [UIColor clearColor];
    [self.view addSubview:lbl_remember_me];
    
    UIButton *btn_on_remember_me = [UIButton buttonWithType:UIButtonTypeCustom];
    // btn_on_remember_me.frame = CGRectMake( CGRectGetMaxX( img_chek_box.frame)+5, CGRectGetMaxY( img_line1.frame)+85, 130, 20);
    btn_on_remember_me .backgroundColor = [UIColor clearColor];
    [btn_on_remember_me addTarget:self action:@selector(btn_on_remember_me_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_remember_me setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [self.view   addSubview:btn_on_remember_me];
    
    
    UILabel *lbl_forgot_password = [[UILabel alloc]init ];
    //    lbl_forgot_password.frame =  CGRectMake( CGRectGetMaxX( img_chek_box.frame)+70, CGRectGetMaxY( img_line1.frame)+270, 100, 20);
    lbl_forgot_password.text = @"Forgot password?";
    lbl_forgot_password.font = [UIFont fontWithName:kFont size:13];
    lbl_forgot_password.textColor = [UIColor blackColor];
    lbl_forgot_password.backgroundColor = [UIColor clearColor];
    [self.view addSubview:lbl_forgot_password];
    
    UIButton *btn_on_forgot_pass = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_on_forgot_pass.frame = CGRectMake( CGRectGetMaxX( img_chek_box.frame)+150, CGRectGetMaxY( img_line1.frame)+85, 110, 20);
    btn_on_forgot_pass .backgroundColor = [UIColor clearColor];
    [btn_on_forgot_pass addTarget:self action:@selector(btn_on_forgot_pass_click:) forControlEvents:UIControlEventTouchUpInside];
    //[btn_on_remember_me setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [self.view   addSubview:btn_on_forgot_pass];
    
    
    
    UIButton *img_login = [UIButton buttonWithType:UIButtonTypeCustom];
    // img_login.frame = CGRectMake(10,CGRectGetMaxY(  img_chek_box.frame)+20,WIDTH-55,55);
    img_login .backgroundColor = [UIColor clearColor];
    img_login.userInteractionEnabled = YES;
    [img_login setImage:[UIImage imageNamed:@"login-bg-text@2x.png"] forState:UIControlStateNormal];
    [img_login addTarget:self action:@selector(btn_login_click:) forControlEvents:UIControlEventTouchUpInside];
    [self.view  addSubview:img_login];
    
    NSArray *accounts = [SSKeychain allAccounts];
    accounts = [SSKeychain accountsForService:kSSToolkitTestsServiceName];
    if(accounts.count>0)
    {
        txt_user_Name.text=[[accounts lastObject] valueForKey:@"acct"];
        NSString *password = [SSKeychain passwordForService:kSSToolkitTestsServiceName account:txt_user_Name.text];
        txt_password.text=password;
        rememberMe=YES;
    }
    
    
    if (IS_IPHONE_6Plus)
    {
        img_background.frame = CGRectMake(40, CGRectGetMaxY(img_header.frame)+30, WIDTH-130, 85);
        txt_user_Name .frame=CGRectMake(25, CGRectGetMaxY( img_background.frame)+20, WIDTH, 50);
        img_line .frame = CGRectMake(-10, CGRectGetMaxY(  txt_user_Name.frame)-85, WIDTH-60, 0.5);
        txt_password .frame=CGRectMake(25, CGRectGetMaxY( img_line.frame)+95, WIDTH, 55);
        img_line1 .frame = CGRectMake(-10, CGRectGetMaxY(  txt_password.frame)-85, WIDTH-60, 0.5);
        img_chek_box.frame = CGRectMake(-10,CGRectGetMaxY(  img_line1.frame)+25,20,20);
        
        lbl_remember_me.frame =  CGRectMake( CGRectGetMaxX( img_chek_box.frame)+48, CGRectGetMaxY( img_line1.frame)+100, 200, 20);
        btn_on_remember_me.frame = CGRectMake(CGRectGetMaxX( img_chek_box.frame)+5, CGRectGetMaxY( img_line1.frame)+100, 130, 20);
        
        lbl_forgot_password.frame =  CGRectMake(CGRectGetMaxX( img_chek_box.frame)+269, CGRectGetMaxY( img_line1.frame)+100, 200, 20);
        btn_on_forgot_pass.frame = CGRectMake(CGRectGetMaxX( img_chek_box.frame)+250, CGRectGetMaxY( img_line1.frame)+100, 110, 20);
        img_login.frame = CGRectMake(-5,CGRectGetMaxY(img_chek_box.frame)+110,WIDTH+10,60);
        
    }
    else if (IS_IPHONE_6)
    {
        img_background.frame = CGRectMake(30, CGRectGetMaxY(img_header.frame)+30, WIDTH-85, 85);
        txt_user_Name .frame=CGRectMake(25, CGRectGetMaxY( img_background.frame)+25, WIDTH, 50);
        img_line .frame = CGRectMake(0, CGRectGetMaxY(  txt_user_Name.frame)-85, WIDTH-60, 0.5);
        txt_password .frame=CGRectMake(25, CGRectGetMaxY( img_line.frame)+90, WIDTH, 55);
        img_line1 .frame = CGRectMake(0, CGRectGetMaxY(  txt_password.frame)-85, WIDTH-60, 0.5);
        img_chek_box.frame = CGRectMake(0,CGRectGetMaxY(  img_line1.frame)+29,20,20);
        
        lbl_remember_me.frame =  CGRectMake( CGRectGetMaxX( img_chek_box.frame)+38, CGRectGetMaxY( img_line1.frame)+105, 200, 20);
        btn_on_remember_me.frame = CGRectMake(CGRectGetMaxX( img_chek_box.frame)+5, CGRectGetMaxY( img_line1.frame)+94, 130, 20);
        
        lbl_forgot_password.frame =  CGRectMake(CGRectGetMaxX( img_chek_box.frame)+207, CGRectGetMaxY( img_line1.frame)+105, 200, 20);
        btn_on_forgot_pass.frame = CGRectMake(CGRectGetMaxX( img_chek_box.frame)+207, CGRectGetMaxY( img_line1.frame)+95, 110, 20);
        img_login.frame = CGRectMake(25,CGRectGetMaxY(img_chek_box.frame)+110,WIDTH-50,55);
        
        lbl_remember_me.font = [UIFont fontWithName:kFont size:14];
        lbl_forgot_password.font = [UIFont fontWithName:kFont size:14];
    }
    else
    {
        img_background.frame = CGRectMake(30, CGRectGetMaxY(img_header.frame)+30, WIDTH-75,IS_IPHONE_5?75:70);
        txt_user_Name .frame=CGRectMake(25,IS_IPHONE_5?CGRectGetMaxY( img_background.frame)+20:CGRectGetMaxY( img_background.frame)+20, WIDTH, 50);
        img_line .frame = CGRectMake(0, CGRectGetMaxY(  txt_user_Name.frame)-85, WIDTH-60, 0.5);
        txt_password .frame=CGRectMake(25, CGRectGetMaxY( img_line.frame)+80, WIDTH, 55);
        img_line1 .frame = CGRectMake(0, CGRectGetMaxY(  txt_password.frame)-85, WIDTH-60, 0.5);
        img_chek_box.frame = CGRectMake(0,CGRectGetMaxY(  img_line1.frame)+25,20,20);
        
        lbl_remember_me.frame =  CGRectMake( CGRectGetMaxX( img_chek_box.frame)+38, CGRectGetMaxY( img_line1.frame)+100, 200, 20);
        btn_on_remember_me.frame = CGRectMake(CGRectGetMaxX( img_chek_box.frame)+5, CGRectGetMaxY( img_line1.frame)+104, 130, 20);
        
        lbl_forgot_password.frame =  CGRectMake(CGRectGetMaxX( img_chek_box.frame)+160, CGRectGetMaxY( img_line1.frame)+100, 200, 20);
        btn_on_forgot_pass.frame = CGRectMake(CGRectGetMaxX( img_chek_box.frame)+160, CGRectGetMaxY( img_line1.frame)+104, 110, 20);
        img_login.frame = CGRectMake(25,CGRectGetMaxY(img_chek_box.frame)+110,WIDTH-50,55);
        lbl_remember_me.font = [UIFont fontWithName:kFont size:13];
        lbl_forgot_password.font = [UIFont fontWithName:kFont size:13];
        
    }
    
}

-(void)popup_Forgetpassword
{
    
    
    [view_Forgetpassword removeFromSuperview];
    view_Forgetpassword=[[UIView alloc] init];
    view_Forgetpassword.frame=CGRectMake(0 , 0, WIDTH, HEIGHT);
    view_Forgetpassword.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.7];
    view_Forgetpassword.userInteractionEnabled=TRUE;
    [self.view addSubview: view_Forgetpassword];
    
    UIImageView *alertView_Body =[[UIImageView alloc] init];
    if (IS_IPHONE_6Plus) {
        alertView_Body.frame=CGRectMake(50, 220, WIDTH-100, 180);
    }
    else if (IS_IPHONE_6) {
        alertView_Body.frame=CGRectMake(50, 220, WIDTH-100, 180);
    }
    else
    {
        alertView_Body.frame=CGRectMake(50, 200, WIDTH-100, 155);
    }
    
    
    //    [alertViewBody setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    alertView_Body.backgroundColor=[UIColor whiteColor];
    alertView_Body.userInteractionEnabled = YES;
    [ view_Forgetpassword addSubview:alertView_Body];
    
    
    
    UILabel  * lbl_forgetpassword = [[UILabel alloc]init];
    
    //    lbl_forget.frame= CGRectMake(WIDTH-150,CGRectGetMaxY(img_pass.frame)+25, WIDTH, 18);
    if (IS_IPHONE_6Plus) {
        lbl_forgetpassword.frame= CGRectMake(70,15, 230, 40);
    }
    else if (IS_IPHONE_6) {
        lbl_forgetpassword.frame= CGRectMake(50,15, 230, 40);
    }
    else
    {
        lbl_forgetpassword.frame= CGRectMake(25,15, 180, 30);
    }
    
    
    lbl_forgetpassword.text = @"Forgot Password";
    lbl_forgetpassword.backgroundColor=[UIColor clearColor];
    lbl_forgetpassword.textColor=[UIColor blackColor];
    lbl_forgetpassword.textAlignment=NSTextAlignmentLeft;
    lbl_forgetpassword.font = [UIFont fontWithName:kFontBold size:22];
    [alertView_Body addSubview:lbl_forgetpassword];
    
    
    
    txt_Emailid = [[UITextField alloc] init];
    if (IS_IPHONE_6Plus) {
        txt_Emailid.frame=CGRectMake(10, CGRectGetMaxY(lbl_forgetpassword.frame)+10, 295, 50);
    }
    else if (IS_IPHONE_6) {
        txt_Emailid.frame=CGRectMake(10, CGRectGetMaxY(lbl_forgetpassword.frame)+10, 255, 50);
    }
    else
    {
        txt_Emailid.frame=CGRectMake(10, CGRectGetMaxY(lbl_forgetpassword.frame)+10, 200, 40);
    }
    
    
    txt_Emailid.borderStyle = UITextBorderStyleNone;
    txt_Emailid.font = [UIFont fontWithName:kFont size:14];
    txt_Emailid.placeholder = @"      Email Address";
    [txt_Emailid setValue:[UIFont fontWithName: kFont size: 18] forKeyPath:@"_placeholderLabel.font"];
    [txt_Emailid setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Emailid.leftViewMode = UITextFieldViewModeAlways;
    txt_Emailid.userInteractionEnabled=YES;
    txt_Emailid.textAlignment = NSTextAlignmentLeft;
    txt_Emailid.backgroundColor = [UIColor lightGrayColor];
    txt_Emailid.keyboardType = UIKeyboardTypeAlphabet;
    txt_Emailid.textColor = [UIColor blackColor];
    txt_Emailid.delegate = self;
    [alertView_Body addSubview:txt_Emailid];
    
    
    
    UIImageView *img_Line=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus) {
        img_Line.frame=CGRectMake(10, CGRectGetMaxY(txt_Emailid.frame)+10, 295, 0.5);
    }
    else if (IS_IPHONE_6) {
        img_Line.frame=CGRectMake(10, CGRectGetMaxY(txt_Emailid.frame)+10, 255, 0.5);
    }
    else
    {
        img_Line.frame=CGRectMake(10, CGRectGetMaxY(txt_Emailid.frame)+10, 200, 0.5);
    }
    
    
    [img_Line setUserInteractionEnabled:YES];
    img_Line.backgroundColor=[UIColor lightGrayColor];
    img_Line.image=[UIImage imageNamed:@"line1.png"];
    [alertView_Body addSubview:img_Line];
    
    
    
    UILabel  * lbl_Cancel = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus) {
        lbl_Cancel.frame= CGRectMake(10, CGRectGetMaxY(img_Line.frame)+2, 147, 60);
    }
    else if (IS_IPHONE_6) {
        lbl_Cancel.frame= CGRectMake(10, CGRectGetMaxY(img_Line.frame)+2, 130, 50);
    }
    else
    {
        lbl_Cancel.frame= CGRectMake(10, CGRectGetMaxY(img_Line.frame)+2, 100, 40);
    }
    
    
    lbl_Cancel.text = @" Cancel";
    lbl_Cancel.backgroundColor=[UIColor clearColor];
    lbl_Cancel.textColor=[UIColor blackColor];
    lbl_Cancel.textAlignment=NSTextAlignmentCenter;
    lbl_Cancel.userInteractionEnabled=YES;
    lbl_Cancel.font = [UIFont fontWithName:kFontBold size:20];
    [alertView_Body addSubview:lbl_Cancel];
    
    
    
    btn_Cancel =[UIButton buttonWithType:UIButtonTypeCustom];
    //    [btn_login setImage:[UIImage imageNamed:@"img-login@2x.png"] forState:UIControlStateNormal];
    if (IS_IPHONE_6Plus) {
        btn_Cancel.frame=CGRectMake(10, CGRectGetMaxY(img_Line.frame)+2, 147, 60);
    }
    else if (IS_IPHONE_6) {
        btn_Cancel.frame=CGRectMake(10, CGRectGetMaxY(img_Line.frame)+2, 130, 50);
    }
    else
    {
        btn_Cancel.frame=CGRectMake(10, CGRectGetMaxY(img_Line.frame)+2, 100, 50);
    }
    [btn_Cancel addTarget:self action:@selector(click_Cancelbtn:) forControlEvents:UIControlEventTouchUpInside];
    
    btn_Cancel.backgroundColor=[UIColor clearColor];
    [alertView_Body addSubview:btn_Cancel];
    
    
    
    
    UIImageView *img_straightLine=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus) {
        img_straightLine.frame=CGRectMake(CGRectGetMaxX(lbl_Cancel.frame)+1, CGRectGetMaxY(img_Line.frame), 1, 50);
    }
    else if (IS_IPHONE_6) {
        img_straightLine.frame=CGRectMake(CGRectGetMaxX(lbl_Cancel.frame)+1, CGRectGetMaxY(img_Line.frame), 1, 50);
    }
    else
    {
        img_straightLine.frame=CGRectMake(CGRectGetMaxX(lbl_Cancel.frame)+1, CGRectGetMaxY(img_Line.frame), 1, 40);
    }
    
    
    [img_straightLine setUserInteractionEnabled:YES];
    img_straightLine.backgroundColor=[UIColor lightGrayColor];
    img_straightLine.image=[UIImage imageNamed:@"line_image@2.png"];
    [alertView_Body addSubview:img_straightLine];
    
    
    
    
    
    
    
    
    UILabel  * lbl_Submitt = [[UILabel alloc]init];
    
    //    lbl_forget.frame= CGRectMake(WIDTH-150,CGRectGetMaxY(img_pass.frame)+25, WIDTH, 18);
    if (IS_IPHONE_6Plus) {
        lbl_Submitt.frame= CGRectMake(170, CGRectGetMaxY(img_Line.frame)+2, 130, 60);
    }
    else if (IS_IPHONE_6) {
        lbl_Submitt.frame= CGRectMake(140, CGRectGetMaxY(img_Line.frame)+2, 130, 50);
    }
    else
    {
        lbl_Submitt.frame= CGRectMake(110, CGRectGetMaxY(img_Line.frame)+2, 100, 40);
        
    }
    
    
    lbl_Submitt.text = @" Send";
    lbl_Submitt.backgroundColor=[UIColor clearColor];
    lbl_Submitt.textColor=[UIColor blackColor];
    lbl_Submitt.textAlignment=NSTextAlignmentCenter;
    lbl_Submitt.userInteractionEnabled=YES;
    lbl_Submitt.font = [UIFont fontWithName:kFontBold size:20];
    [alertView_Body addSubview:lbl_Submitt];
    
    
    
    
    
    btn_Submitt =[UIButton buttonWithType:UIButtonTypeCustom];
    //    [btn_login setImage:[UIImage imageNamed:@"img-login@2x.png"] forState:UIControlStateNormal];
    if (IS_IPHONE_6Plus) {
        btn_Submitt.frame=CGRectMake(170, CGRectGetMaxY(img_Line.frame)+2, 130, 60);
    }
    else if (IS_IPHONE_6) {
        btn_Submitt.frame=CGRectMake(140, CGRectGetMaxY(img_Line.frame)+2, 130, 50);
    }
    else
    {
        btn_Submitt.frame=CGRectMake(110, CGRectGetMaxY(img_Line.frame)+2, 100, 50);
    }
    [btn_Submitt addTarget:self action:@selector(click_Sendbtn:) forControlEvents:UIControlEventTouchUpInside];
    
    btn_Submitt.backgroundColor=[UIColor clearColor];
    [alertView_Body addSubview:btn_Submitt];
    view_Forgetpassword.hidden = YES;
}


-(void)click_Cancelbtn:(UIButton *)sender
{
    
    
    view_Forgetpassword.hidden = YES;
    
    
    
}
-(void)click_Sendbtn:(UIButton *)sender
{
    
    
    if ([txt_Emailid.text isEqualToString:@""])
    {
        
        [self popup_Alertview:@"Please enter Email Id"];
        
    }
    
    else
    {
        [self AFForgetPass];
    }
    
    
}
#pragma mark Click Events
//-(void)click_Sendbtn:(UIButton *)sender
//{
//
//
//    if ([txt_Emailid.text isEqualToString:@""])
//    {
//
//        [self popup_Alertview:@"Please enter Emailid"];
//
//    }
//
//    else
//    {
//        [self AFForgetPass];
//    }
//
//
//}


-(void)btn_back_click:(UIButton *)sender
{
    NSLog(@"btn_back_click:");
    [self dismissViewControllerAnimated:NO completion:nil];
    //[self.navigationController popViewControllerAnimated:NO];
    
    
}
-(void)btn_on_remember_me_click:(UIButton *)sender
{
    NSLog(@"btn_on_remember_me_click:");
    
    if (rememberMe==YES)
    {
        rememberMe=NO;
        // [img_chek_box setImage:[UIImage imageNamed:@"img_checkbox@2x.png"]];
        
        
        [img_chek_box setImage:[UIImage imageNamed:@"img-checkbox@2x.png"] forState:UIControlStateNormal];
        
        NSArray *accounts = [SSKeychain allAccounts];
        accounts = [SSKeychain accountsForService:kSSToolkitTestsServiceName];
        
        int count = (int)[accounts count];
        
        for (int i = 0; i < count ; i ++) {
            
            [SSKeychain deletePasswordForService:kSSToolkitTestsServiceName account:[[accounts lastObject] valueForKey:@"acct"]];
        }
        
    }
    else
    {
        
        
        [img_chek_box setImage:[UIImage imageNamed:@"img_checkok@2x.png"] forState:UIControlStateNormal];
        
        rememberMe=YES;
        [SSKeychain setPassword:txt_password.text forService:kSSToolkitTestsServiceName account:txt_user_Name.text];
    }
    
    
    
    [txt_user_Name resignFirstResponder];
    [txt_password resignFirstResponder];
    
    
    
}
-(void)btn_on_forgot_pass_click:(UIButton *)sender
{
    NSLog(@"btn_on_remember_me_click:");
    [self popup_Forgetpassword];
    
    
    view_Forgetpassword.hidden = NO;
    
    //    UserForgotPasswordVC *forgetpassword=[[UserForgotPasswordVC alloc]init];
    //    [self.navigationController pushViewController:forgetpassword animated:YES];
    
    
    
    NSLog(@"click_BtnForgotPass");
    // [self AFForgotPass];
    
    
}

-(void)btn_check_box_click:(UIButton *)sender
{
    //    NSLog(@"btn_check_box_click:");
    //
    //    [img_chek_box setImage:[UIImage imageNamed:@"img-checkbox@2x.png"] forState:UIControlStateNormal];
    //
    //    if([sender isSelected])
    //    {
    //        [sender setSelected:NO];
    //        str_Rememberme=@"YES";
    //        [img_chek_box setImage:[UIImage imageNamed:@"img_checkok@2x.png"] forState:UIControlStateNormal];
    //    }
    //    else
    //    {
    //
    //        [sender setSelected:YES];
    //        [img_chek_box setImage:[UIImage imageNamed:@"img-checkbox@2x.png"] forState:UIControlStateNormal];
    //    }
    
    
    if (rememberMe==YES)
    {
        rememberMe=NO;
        // [img_chek_box setImage:[UIImage imageNamed:@"img_checkbox@2x.png"]];
        
        
        [img_chek_box setImage:[UIImage imageNamed:@"img-checkbox@2x.png"] forState:UIControlStateNormal];
        
        NSArray *accounts = [SSKeychain allAccounts];
        accounts = [SSKeychain accountsForService:kSSToolkitTestsServiceName];
        
        int count = (int)[accounts count];
        
        for (int i = 0; i < count ; i ++) {
            
            [SSKeychain deletePasswordForService:kSSToolkitTestsServiceName account:[[accounts lastObject] valueForKey:@"acct"]];
        }
        
    }
    else
    {
        
        
        [img_chek_box setImage:[UIImage imageNamed:@"img_checkok@2x.png"] forState:UIControlStateNormal];
        
        rememberMe=YES;
        [SSKeychain setPassword:txt_password.text forService:kSSToolkitTestsServiceName account:txt_user_Name.text];
    }
    
    
    
    [txt_user_Name resignFirstResponder];
    [txt_password resignFirstResponder];
    
    
    
    
}
-(void)btn_login_click:(UIButton *)sender
{
    NSLog(@"btn_login_click:");
    
    
    if ([txt_user_Name.text isEqualToString:@""])
    {
        
        [self popup_Alertview:@"Please enter Username"];
        
    }
    else if ([txt_password.text isEqualToString:@""])
    {
        [self popup_Alertview:@"Please enter Password"];
    }
    
    else if ([txt_password.text length]<8)
    {
        txt_password.text=@"";
        [self popup_Alertview:@"Password should be minimum of 8 characters"];
    }
    else
    {
        [self AFLogIn];
    }
    
}







#pragma rerutrn event code

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}




- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFontBold size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFontBold size:16.0f]];
}



#pragma mark -integrateJWSlider
-(void)integrateJWSlider
{
    
    if ([[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Role"]]isEqualToString:@"1"])
    {//user
        JWSlideMenuController *slideMenu=[[JWSlideMenuController alloc] init];
        JWSlideMenuViewController *vc_Home=[[ChefHomeViewController alloc] init];
        [slideMenu addViewController:vc_Home  withTitle:@"Home" andImage:[UIImage imageNamed:@"img_home@2x.png"]];
        
        JWSlideMenuViewController *vc_Appointment=[MyScheduleVC new];
        [slideMenu addViewController:vc_Appointment withTitle:@"My\n Schedule" andImage:[UIImage imageNamed:@"img_schedule@2x.png"]];
        
        JWSlideMenuViewController *vc_MyVeh=[MyordersVC new];
        [slideMenu addViewController:vc_MyVeh withTitle:@"Orders" andImage:[UIImage imageNamed:@"img_order@2x.png"]];
        
        JWSlideMenuViewController *vc_Accessories=[ViewMenuVC new];
        [slideMenu addViewController:vc_Accessories withTitle:@"My Menu" andImage:[UIImage imageNamed:@"img_Menusrceen@2x.png"]];
        
        JWSlideMenuViewController *vc_Notification=[MyAccountVC new];
        [slideMenu addViewController:vc_Notification withTitle:@"Account" andImage:[UIImage imageNamed:@"img_account@2x.png"]];
        
        JWSlideMenuViewController *vc_Promo=[MessagesVC new];
        [slideMenu addViewController:vc_Promo withTitle:@"Notification" andImage:[UIImage imageNamed:@"img_notification@2x.png"]];
        
        JWSlideMenuViewController *vc_Promo1=[MessageListVC new];
        [slideMenu addViewController:vc_Promo1 withTitle:@"Messages" andImage:[UIImage imageNamed:@"img_msg@2x.png"]];
        
        
        JWSlideMenuViewController *vc_Promo2=[SwitchtoUser new];
        [slideMenu addViewController:vc_Promo2 withTitle:@"Switch to\n User" andImage:[UIImage imageNamed:@"img_swtich@2x.png"]];
        
        [self presentViewController:slideMenu animated:NO completion:nil];
    }
    else {
        //chef
        
        
        JWSlideMenuController *slideMenu=[[JWSlideMenuController alloc] init];
        JWSlideMenuViewController *vc_Home=[[HomeVC alloc] init];
        [slideMenu addViewController:vc_Home  withTitle:@"Home" andImage:[UIImage imageNamed:@"img_home@2x.png"]];
        
        JWSlideMenuViewController *vc_Appointment=[SerchForFoodNowVC new];
        [slideMenu addViewController:vc_Appointment withTitle:@"Food Now\n Later" andImage:[UIImage imageNamed:@"img_schedule@2x.png"]];
        
        JWSlideMenuViewController *vc_MyVeh=[MyordersVC new];
        [slideMenu addViewController:vc_MyVeh withTitle:@"My Orders" andImage:[UIImage imageNamed:@"img_order@2x.png"]];
        
        JWSlideMenuViewController *vc_Notification=[DiningCartVC new];
        [slideMenu addViewController:vc_Notification withTitle:@"Dining\n Cart" andImage:[UIImage imageNamed:@"img_account@2x.png"]];
        
        JWSlideMenuViewController *vc_Promo=[ChefProfileVC new];
        [slideMenu addViewController:vc_Promo withTitle:@"Favorites" andImage:[UIImage imageNamed:@"icon-fav.png"]];
        
        JWSlideMenuViewController *vc_Promo1=[NotificationsVC new];
        [slideMenu addViewController:vc_Promo1 withTitle:@"Notification" andImage:[UIImage imageNamed:@"img_notification@2x.png"]];
        JWSlideMenuViewController *vc_Promo2=[MessageListVC new];
        
        [slideMenu addViewController:vc_Promo2 withTitle:@"Messages" andImage:[UIImage imageNamed:@"img_msg@2x.png"]];
        JWSlideMenuViewController *vc_Promo3=[SwitchtoUser new];
        [slideMenu addViewController:vc_Promo3 withTitle:@"Switch to\n Chef" andImage:[UIImage imageNamed:@"img_slidercap@2x.png"]];
        
        [self presentViewController:slideMenu animated:NO completion:nil];
        
        
    }
    
}


-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}



-(void) AFLogIn
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            @"username"                         :   txt_user_Name.text,
                            @"password"                         :   txt_password.text,
                            @"device_udid"                      :   UniqueAppID,
                            @"device_token"                     :   str_device_token,
                            @"role_type"                       :   @"user_profile",
                            @"device_type"                      :   @"1",
                            
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kUserLogin
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseLogin:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully login");
                                             [self AFLogIn];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseLogin :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSMutableDictionary *dictMutable = [[TheDict valueForKey:@"profile_info"] mutableCopy];
        [dictMutable removeObjectsForKeys:[[TheDict valueForKey:@"profile_info"] allKeysForObject:[NSNull null]]];
        
        [defaults setObject:dictMutable  forKey:@"UserInfo"];
        [defaults synchronize];
        
        NSLog(@"defaults %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"]);
        
        
        [self integrateJWSlider];
        
        //        if ([[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"field_last_name"]isEqualToString:@""]||[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"field_mobile_number"]isEqualToString:@""]||[[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"field_date_of_birth"]isEqualToString:@""])
        {
            HomeVC *chefhome=[[HomeVC alloc]init];
            //    [self.navigationController pushViewController:chefhome animated:NO];
            [self presentViewController:chefhome animated:NO completion:nil];
            
        }
        //        else
        //        {
        
        //        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        txt_user_Name.text = @"";
        txt_password.text = @"";
        
        //        [alertViewBody setHidden:NO];
        
        
    }
    
}


-(void) AFForgetPass
{
    
    [btn_Cancel setUserInteractionEnabled:NO];
    [btn_Submitt setUserInteractionEnabled:NO];
    
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    NSDictionary *params =@{
                            @"email"                  :   txt_Emailid.text,
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kForgetPassword
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseForgetPass:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully login");
                                             [self AFForgetPass];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseForgetPass :(NSDictionary * )TheDict
{
    NSLog(@"the dict %@",TheDict);
    
    [btn_Cancel setUserInteractionEnabled:YES];
    [btn_Submitt setUserInteractionEnabled:YES];
    
    
    if([[TheDict valueForKey:@"error"] intValue] == 0)
    {
        [self resignKeyboard];
        
        [self popup_Alertview:[TheDict valueForKey:@"message"]];
        [view_Forgetpassword removeFromSuperview];
        
    }
    
    else{
        
        
        txt_Emailid.text = @"";
        [self resignKeyboard];
        [view_Forgetpassword removeFromSuperview];
        
        
        [self popup_Alertview:[TheDict valueForKey:@"Invalid username or password"]];
        
    }
    
    
}



-(void)resignKeyboard
{
    [txt_Emailid resignFirstResponder];
    [txt_password resignFirstResponder];
    [txt_user_Name resignFirstResponder];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

//# pragma mark forgotpass method
//
//-(void)AFForgotPass{
//
//    [self.view addSubview:delegate.activityIndicator];
//    [delegate.activityIndicator startAnimating];
//
//    //=================================================================BASE URL
//
//    NSURL *url = [NSURL URLWithString:kBaseUrl];
//
//    //=================================================================USED PARAMETERS(ONLY TEXT)
//
//    if (TARGET_IPHONE_SIMULATOR)
//    {
//        delegate.devicestr = @"";
//    }
//
//
//    UIDevice *myDevice=[UIDevice currentDevice];
//    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
//
//    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
//
//    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
//        str_device_token = @"dev-signup";
//
//    }
//
//    NSDictionary *params =@{
//                            @"email"                            :   txt_user_Name.text
//
//                            };
//
//
//    //===========================================AFNETWORKING HEADER
//
//    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
//    httpClient.parameterEncoding = AFFormURLParameterEncoding;
//    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
//    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
//
//    //===============================SIMPLE REQUEST
//
//    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
//                                                            path:kForgetPassword
//                                                      parameters:params];
//
//
//
//
//    //====================================================RESPONSE
//
//
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
//
//    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
//
//    }];
//    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSError *error = nil;
//        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
//
//        [delegate.activityIndicator stopAnimating];
//        [self ResponseForgetpass:JSON];
//    }
//
//     //==================================================ERROR
//                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//
//
//                                         [delegate.activityIndicator stopAnimating];
//
//                                         if([operation.response statusCode] == 406){
//
//                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
//                                             return;
//                                         }
//
//                                         if([operation.response statusCode] == 403){
//                                             NSLog(@"Upload Failed");
//                                             return;
//                                         }
//                                         if ([[operation error] code] == -1009) {
//                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
//                                                                                          message:@"Please check your internet connection"
//                                                                                         delegate:nil
//                                                                                cancelButtonTitle:@"OK"
//                                                                                otherButtonTitles:nil];
//                                             [av show];
//                                         }
//                                         else if ([[operation error] code] == -1001) {
//
//                                             NSLog(@"Successfully login");
//                                             [self AFForgotPass];
//                                         }
//                                     }];
//    [operation start];
//
//
//
//}
//-(void) ResponseForgetpass :(NSDictionary * )TheDict
//{
//    NSLog(@"Login: %@",TheDict);
//
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
//    {
//
//        [defaults setObject:[TheDict valueForKey:@"user_info"]  forKey:@"UserInfo"];
//        [defaults setObject:[[TheDict valueForKey:@"user_info"] valueForKey:@"default_car_id"] forKey:@"defaultCarId"];
//        if ([[NSUserDefaults standardUserDefaults]valueForKey:@"defaultCarId"]) {
//
//        }
//        else
//            [defaults setObject:@"0" forKey:@"defaultCarId"];
//        [defaults synchronize];
//        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
//
//
//
//    }
//    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
//    {
//        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
//        txt_user_Name.text = @"";
//
//        //        [alertViewBody setHidden:NO];
//
//
//    }
//
//}
//
//# pragma mark login method
//
//-(void) AFUserSignUp
//{
//
//    [self.view addSubview:delegate.activityIndicator];
//    [delegate.activityIndicator startAnimating];
//
//    //=================================================================BASE URL
//
//    NSURL *url = [NSURL URLWithString:kBaseUrl];
//
//    //=================================================================USED PARAMETERS(ONLY TEXT)
//
//    if (TARGET_IPHONE_SIMULATOR)
//    {
//        delegate.devicestr = @"";
//    }
//
//
//    UIDevice *myDevice=[UIDevice currentDevice];
//    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
//
//    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
//
//    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
//        str_device_token = @"dev-signup";
//
//    }
//
//    NSDictionary *params =@{
//                            @"username"                         :   txt_user_Name.text,
//                            @"password"                         :   txt_password.text,
//                            @"device_udid"                      :   UniqueAppID,
//                            @"device_token"                     :   str_device_token,
//                            @"device_type"                      :   @"1"
//
//                            };
//
//
//    //===========================================AFNETWORKING HEADER
//
//    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
//    httpClient.parameterEncoding = AFFormURLParameterEncoding;
//    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
//    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
//
//    //===============================SIMPLE REQUEST
//
//    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
//                                                            path:kUserLogin
//                                                      parameters:params];
//
//
//
//
//    //====================================================RESPONSE
//
//
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
//
//    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
//
//    }];
//    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSError *error = nil;
//        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
//
//        [delegate.activityIndicator stopAnimating];
//        [self ResponseLogin:JSON];
//    }
//
//     //==================================================ERROR
//                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//
//
//                                         [delegate.activityIndicator stopAnimating];
//
//                                         if([operation.response statusCode] == 406){
//
//                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
//                                             return;
//                                         }
//
//                                         if([operation.response statusCode] == 403){
//                                             NSLog(@"Upload Failed");
//                                             return;
//                                         }
//                                         if ([[operation error] code] == -1009) {
//                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
//                                                                                          message:@"Please check your internet connection"
//                                                                                         delegate:nil
//                                                                                cancelButtonTitle:@"OK"
//                                                                                otherButtonTitles:nil];
//                                             [av show];
//                                         }
//                                         else if ([[operation error] code] == -1001) {
//
//                                             NSLog(@"Successfully login");
//                                             [self AFUserSignUp];
//                                         }
//                                     }];
//    [operation start];
//
//}
//-(void) ResponseLogin :(NSDictionary * )TheDict
//{
//    NSLog(@"Login: %@",TheDict);
//
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
//    {
//
//        [defaults setObject:[TheDict valueForKey:@"user_info"]  forKey:@"UserInfo"];
//        [defaults setObject:[[TheDict valueForKey:@"user_info"] valueForKey:@"default_car_id"] forKey:@"defaultCarId"];
//        if ([[NSUserDefaults standardUserDefaults]valueForKey:@"defaultCarId"]) {
//
//        }
//        else
//            [defaults setObject:@"0" forKey:@"defaultCarId"];
//        [defaults synchronize];
//        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
//
//
//
//    }
//    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
//    {
//        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
//        txt_user_Name.text = @"";
//        txt_password.text = @"";
//
//        //        [alertViewBody setHidden:NO];
//
//
//    }
//
//}
//
//
//
///*
//#pragma mark - Navigation
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}
//*/

@end
