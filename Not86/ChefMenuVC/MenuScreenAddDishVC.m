//
//  MenuScreenAddDishVC.m
//  Not86
//
//  Created by Interwld on 8/22/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "MenuScreenAddDishVC.h"

@interface MenuScreenAddDishVC ()<UIScrollViewDelegate,UITextFieldDelegate,UITextViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
{
    UIImageView *img_topbar;
    UIImageView *img_BackgroundImg;
    UIScrollView *scrollviewMenu;
    UITextField *txt_cheftype;
    UITextField *txt_others;
    NSMutableArray *collectionarrImages;
    
    UICollectionViewFlowLayout *layout;
    UICollectionView *collView_serviceDirectory;
    
    
}


@end

@implementation MenuScreenAddDishVC

- (void)viewDidLoad {
    [super viewDidLoad];
    collectionarrImages=[NSMutableArray arrayWithObjects:@"img_halal@2x.png",@"img_Univer@2x.png",@"img_Organic@2x.png", nil];
    [self IntegrateHeaderDesign];
    [self IntegrateBodyDesign];

    // Do any additional setup after loading the view.
}

-(void)IntegrateHeaderDesign{
    
    
    img_topbar=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,50)];
    [img_topbar setUserInteractionEnabled:YES];
    img_topbar.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:img_topbar];
    
    
    UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(10, 17, 15, 15)];
    [img_back setUserInteractionEnabled:YES];
    img_back.backgroundColor=[UIColor clearColor];
    img_back.image=[UIImage imageNamed:@"arrow@2x.png"];
    [img_topbar addSubview:img_back];
    UILabel  * lbl_heading = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(img_back.frame)+20,4, 220,40)];
    lbl_heading.text = @"Add Dish/Meal ";
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:kFontBold size:16];
    [img_topbar addSubview:lbl_heading];
    
    UIButton *btn_back = [[UIButton alloc] init];
    btn_back.frame = CGRectMake(10, 10, 20,20);
    btn_back.backgroundColor = [UIColor clearColor];
    //    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_topbar addSubview:btn_back];
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 10, 30, 30)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-logo.png"];
    [img_topbar addSubview:img_logo];
    
    scrollviewMenu=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 50, WIDTH, HEIGHT-80)];
    [scrollviewMenu setShowsVerticalScrollIndicator:NO];
    scrollviewMenu.delegate = self;
    scrollviewMenu.scrollEnabled = YES;
    scrollviewMenu.showsVerticalScrollIndicator = NO;
    [scrollviewMenu setUserInteractionEnabled:YES];
    scrollviewMenu.backgroundColor = [UIColor clearColor];
    
    [scrollviewMenu setContentSize:CGSizeMake(0,HEIGHT)];
    [self.view addSubview:scrollviewMenu];
    
    
    
    
    
}
-(void)IntegrateBodyDesign{
    
    UIButton *btn_Signaldish = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        btn_Signaldish.frame = CGRectMake(60, 15, 150,25);
        
    }
    else if (IS_IPHONE_6){
        btn_Signaldish.frame = CGRectMake(55, 15, 135,30);
    }
    else  {
        btn_Signaldish.frame = CGRectMake(50, 15, 100,25);
        
    }
    
    btn_Signaldish.backgroundColor = [UIColor blackColor];
    [btn_Signaldish setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Signaldish setTitle:@"Single Dish" forState:UIControlStateNormal];
    btn_Signaldish.titleLabel.font = [UIFont fontWithName:kFontBold size:10];
   // [btn_Signaldish addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [scrollviewMenu addSubview:btn_Signaldish];
    
    UIButton *btn_Meal = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        btn_Meal.frame = CGRectMake(210, 15, 150,25);
    }
    else if (IS_IPHONE_6){
        btn_Meal.frame = CGRectMake(190, 15, 135,30);
    }
    else  {
        btn_Meal.frame = CGRectMake(150, 15, 100,25);
    }
    
    btn_Meal.backgroundColor = [UIColor lightGrayColor];
    [btn_Meal setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Meal setTitle:@"Meal" forState:UIControlStateNormal];
    btn_Meal.titleLabel.font = [UIFont fontWithName:kFontBold size:10];
  //  [btn_Meal addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [scrollviewMenu addSubview:btn_Meal];
    
    UIImageView *img_Backgroundimage=[[UIImageView alloc]init];
    
    
    
    if (IS_IPHONE_6Plus){
        img_Backgroundimage.frame = CGRectMake(0, CGRectGetMaxY(btn_Signaldish.frame)+10, WIDTH, 80);
    }
    else if (IS_IPHONE_6){
        img_Backgroundimage.frame = CGRectMake(0, CGRectGetMaxY(btn_Signaldish.frame)+10, WIDTH, 80);
    }
    else  {
        img_Backgroundimage.frame = CGRectMake(0, CGRectGetMaxY(btn_Signaldish.frame)+10, WIDTH, 80);
    }
    
    [img_Backgroundimage setUserInteractionEnabled:YES];
    img_Backgroundimage.backgroundColor=[UIColor clearColor];
    img_Backgroundimage.image=[UIImage imageNamed:@"bg1.png"];
    [scrollviewMenu addSubview:img_Backgroundimage];
    
    
    UIImageView *Arrow_left=[[UIImageView alloc]init];
    
    
    
    
    if (IS_IPHONE_6Plus){
        Arrow_left.frame=CGRectMake(15, 20, 10, 15);
        
    }
    else if (IS_IPHONE_6){
        Arrow_left.frame=CGRectMake(5, 25, 10, 15);
    }
    else  {
        Arrow_left.frame=CGRectMake(5, 25, 10, 15);
        
    }
    
    [Arrow_left setUserInteractionEnabled:YES];
    Arrow_left.backgroundColor=[UIColor clearColor];
    Arrow_left.image=[UIImage imageNamed:@"arrow_left.png"];
    [img_Backgroundimage addSubview:Arrow_left];
    
    UIButton *btn_Arrowleft = [[UIButton alloc] init];
    
    
    
    if (IS_IPHONE_6Plus){
        btn_Arrowleft.frame = CGRectMake(15, 20, 10, 15);
        
    }
    else if (IS_IPHONE_6){
        btn_Arrowleft.frame = CGRectMake(5, 25, 10, 15);
        
    }
    else  {
        btn_Arrowleft.frame = CGRectMake(5, 25, 10, 15);
        
    }
    
    btn_Arrowleft.backgroundColor = [UIColor clearColor];
    [btn_Arrowleft setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    //    [btn_Arrowleft addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_Backgroundimage addSubview:btn_Arrowleft];
    
    
    layout=[[UICollectionViewFlowLayout alloc] init];
    collView_serviceDirectory = [[UICollectionView alloc] initWithFrame:CGRectMake(30,0,WIDTH-65,70)
                                                   collectionViewLayout:layout];
    
    
    
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_serviceDirectory setDataSource:self];
    [collView_serviceDirectory setDelegate:self];
    collView_serviceDirectory.scrollEnabled = YES;
    collView_serviceDirectory.showsVerticalScrollIndicator = NO;
    collView_serviceDirectory.showsHorizontalScrollIndicator = NO;
    collView_serviceDirectory.pagingEnabled = NO;
    [collView_serviceDirectory registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_serviceDirectory setBackgroundColor:[UIColor clearColor]];
    layout.minimumInteritemSpacing = 2;
    layout.minimumLineSpacing = 0;
    collView_serviceDirectory.userInteractionEnabled = YES;
    [img_Backgroundimage addSubview:collView_serviceDirectory];
    
    
    
    
    
    
    
    
    UIImageView *Arrow_right=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus){
        Arrow_right.frame=CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+1, 25, 10, 15);
        
    }
    else if (IS_IPHONE_6){
        Arrow_right.frame=CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
        
    }
    else  {
        Arrow_right.frame=CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
        
    }
    [Arrow_right setUserInteractionEnabled:YES];
    Arrow_right.backgroundColor=[UIColor clearColor];
    Arrow_right.image=[UIImage imageNamed:@"arrow_right.png"];
    [img_Backgroundimage addSubview:Arrow_right];
    
    
    UIButton *btn_Arrowright = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        btn_Arrowright.frame = CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+1, 25, 10, 15);
        
    }
    else if (IS_IPHONE_6){
        btn_Arrowright.frame = CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
        
    }
    else  {
        btn_Arrowright.frame = CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
        
    }
    btn_Arrowright.backgroundColor = [UIColor clearColor];
    //    [btn_Arrowleft setImage:[UIImage imageNamed:@"arrow_left.png"] forState:UIControlStateNormal];
    [btn_Arrowright setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    //    [btn_Arrowleft addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_Backgroundimage addSubview:btn_Arrowright];
    
    
    
    UILabel  * lbl_Mealimage = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus){
        lbl_Mealimage.frame = CGRectMake(100,120, 220,40);
        
    }
    else if (IS_IPHONE_6){
        lbl_Mealimage.frame = CGRectMake(100,120, 220,40);
        
    }
    else  {
        lbl_Mealimage.frame = CGRectMake(80,120, 220,40);
        
    }
    lbl_Mealimage.text = @"Upload Dish image ";
    lbl_Mealimage.backgroundColor=[UIColor clearColor];
    lbl_Mealimage.textColor=[UIColor blackColor];
    lbl_Mealimage.textAlignment=NSTextAlignmentLeft;
    lbl_Mealimage.font = [UIFont fontWithName:kFontBold size:13];
    [scrollviewMenu addSubview:lbl_Mealimage];
    
    
    UIImageView *img_backround=[[UIImageView alloc]init];
    
    
    if (IS_IPHONE_6Plus){
        img_backround.frame=CGRectMake(80, CGRectGetMaxY(lbl_Mealimage.frame)+1, 240, 180);
        
    }
    else if (IS_IPHONE_6){
        img_backround.frame=CGRectMake(80, CGRectGetMaxY(lbl_Mealimage.frame)+1, 210, 180);
        
    }
    else  {
        img_backround.frame=CGRectMake(60, CGRectGetMaxY(lbl_Mealimage.frame)+1, 180, 180);
        
    }
    img_backround.image=[UIImage imageNamed:@"img_backgound.png"];
    
    [img_backround setUserInteractionEnabled:YES];
    img_backround.backgroundColor=[UIColor clearColor];
    [scrollviewMenu addSubview:img_backround];
    
    
    UIImageView *img_Dropimage=[[UIImageView alloc]init];
    
    
    if (IS_IPHONE_6Plus){
        img_Dropimage.frame=CGRectMake(210, 150, 15, 15);
        
    }
    else if (IS_IPHONE_6){
        img_Dropimage.frame=CGRectMake(180, 150, 15, 15);
        
    }
    else  {
        img_Dropimage.frame=CGRectMake(180, 150, 15, 15);
        
        
    }
    img_Dropimage.image=[UIImage imageNamed:@"icon 2.png"];
    
    [img_Dropimage setUserInteractionEnabled:YES];
    img_Dropimage.backgroundColor=[UIColor clearColor];
    [img_backround addSubview:img_Dropimage];
    
    UILabel  * lbl_Weidth = [[UILabel alloc]init];
    
    
    if (IS_IPHONE_6Plus){
        lbl_Weidth.frame=CGRectMake(CGRectGetMaxX(img_backround.frame)+3,CGRectGetMaxY(lbl_Mealimage.frame)+80, 100,120);
    }
    else if (IS_IPHONE_6){
        lbl_Weidth.frame=CGRectMake(CGRectGetMaxX(img_backround.frame)+2,CGRectGetMaxY(lbl_Mealimage.frame)+90, 90,110);
    }
    else  {
        lbl_Weidth.frame=CGRectMake(CGRectGetMaxX(img_backround.frame)+2,CGRectGetMaxY(lbl_Mealimage.frame)+50, 100,120);
        
    }
    
    lbl_Weidth.text = @"Width:150px\nHeight:150px\nSize:5mb\n.jpg..png";
    lbl_Weidth.backgroundColor=[UIColor clearColor];
    lbl_Weidth.textColor=[UIColor lightGrayColor];
    lbl_Weidth.textAlignment=NSTextAlignmentLeft;
    lbl_Weidth.numberOfLines=0;
    lbl_Weidth.font = [UIFont fontWithName:kFontBold size:10];
    [scrollviewMenu addSubview:lbl_Weidth];
    
    
    UIImageView *img_backgroundimages=[[UIImageView alloc]init];
    
    
    if (IS_IPHONE_6Plus){
        img_backgroundimages.frame=CGRectMake(5, CGRectGetMaxY(img_backround.frame)+2, WIDTH-4, 400);
    }
    else if (IS_IPHONE_6){
        img_backgroundimages.frame=CGRectMake(5, CGRectGetMaxY(img_backround.frame)+2, WIDTH-4, 400);
    }
    else  {
        img_backgroundimages.frame=CGRectMake(5, CGRectGetMaxY(img_backround.frame)+2, WIDTH-4, 350);
        
    }
    img_backgroundimages.image=[UIImage imageNamed:@"img_backgound.png"];
    
    [img_backgroundimages setUserInteractionEnabled:YES];
    img_backgroundimages.backgroundColor=[UIColor clearColor];
    [scrollviewMenu addSubview:img_backgroundimages];
    
    UITextField *txt_MealTitle = [[UITextField alloc] init];
    if (IS_IPHONE_6Plus){
        txt_MealTitle.frame=CGRectMake(30, 10, 250, 30);
    }
    else if (IS_IPHONE_6){
        txt_MealTitle.frame=CGRectMake(30, 10, 250, 30);
    }
    else  {
        txt_MealTitle.frame=CGRectMake(30, 10, 250, 30);
    }
    txt_MealTitle.borderStyle = UITextBorderStyleNone;
    txt_MealTitle.font = [UIFont fontWithName:kFont size:13];
    txt_MealTitle.placeholder = @"Dish Title";
    [txt_MealTitle setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_MealTitle setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_MealTitle.leftViewMode = UITextFieldViewModeAlways;
    txt_MealTitle.userInteractionEnabled=YES;
    txt_MealTitle.textAlignment = NSTextAlignmentLeft;
    txt_MealTitle.backgroundColor = [UIColor clearColor];
    txt_MealTitle.keyboardType = UIKeyboardTypeAlphabet;
    txt_MealTitle.delegate = self;
    [img_backgroundimages addSubview:txt_MealTitle];
    
    UIImageView *line_img=[[UIImageView alloc]init];
    
    
    if (IS_IPHONE_6Plus){
        line_img.frame=CGRectMake(30, CGRectGetMaxY(txt_MealTitle.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6){
        line_img.frame=CGRectMake(30, CGRectGetMaxY(txt_MealTitle.frame)+1, WIDTH-60, 0.5);
    }
    else  {
        line_img.frame=CGRectMake(20, CGRectGetMaxY(txt_MealTitle.frame)+1, WIDTH-40, 0.5);
        
    }
    [line_img setUserInteractionEnabled:YES];
    line_img.backgroundColor=[UIColor clearColor];
    line_img.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:line_img];
    
    
    UILabel  * lbl_Maxchar = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus){
        lbl_Maxchar.frame=CGRectMake(330,CGRectGetMaxY(txt_MealTitle.frame)+4, 100,5);
        
    }
    else if (IS_IPHONE_6){
        lbl_Maxchar.frame=CGRectMake(300,CGRectGetMaxY(txt_MealTitle.frame)+4, 100,5);
        
    }
    else  {
        lbl_Maxchar.frame=CGRectMake(240,CGRectGetMaxY(txt_MealTitle.frame)+4, 100,5);
        
        
    }
    lbl_Maxchar.text = @"(Max. 50 char)";
    lbl_Maxchar.backgroundColor=[UIColor clearColor];
    lbl_Maxchar.textColor=[UIColor lightGrayColor];
    lbl_Maxchar.textAlignment=NSTextAlignmentLeft;
    lbl_Maxchar.numberOfLines=0;
    lbl_Maxchar.font = [UIFont fontWithName:kFontBold size:7];
    [img_backgroundimages addSubview:lbl_Maxchar];
    
    
    
    
    
    UITextField *txt_Category= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus){
        txt_Category.frame=CGRectMake(30, CGRectGetMaxY(txt_MealTitle.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6){
        txt_Category.frame=CGRectMake(30, CGRectGetMaxY(txt_MealTitle.frame)+5, 200, 30);
    }
    else  {
        txt_Category.frame=CGRectMake(20, CGRectGetMaxY(txt_MealTitle.frame)+5, 200, 30);
    }
    txt_Category.borderStyle = UITextBorderStyleNone;
    txt_Category.font = [UIFont fontWithName:kFont size:13];
    txt_Category.placeholder = @"Category";
    [txt_Category setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Category setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Category.leftViewMode = UITextFieldViewModeAlways;
    txt_Category.userInteractionEnabled=YES;
    txt_Category.textAlignment = NSTextAlignmentLeft;
    txt_Category.backgroundColor = [UIColor clearColor];
    txt_Category.keyboardType = UIKeyboardTypeAlphabet;
    txt_Category.delegate = self;
    [img_backgroundimages addSubview:txt_Category];
    
    UIImageView *img_lineimg=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus){
        img_lineimg.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6){
        img_lineimg.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+1, WIDTH-60, 0.5);
    }
    else  {
        img_lineimg.frame=CGRectMake(20, CGRectGetMaxY(txt_Category.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_lineimg setUserInteractionEnabled:YES];
    img_lineimg.backgroundColor=[UIColor clearColor];
    img_lineimg.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_lineimg];
    
    
    
    UIButton *img_dropbox = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        img_dropbox.frame=CGRectMake(370, CGRectGetMaxY(txt_MealTitle.frame)+20, 15, 10);
    }
    else if (IS_IPHONE_6){
        img_dropbox.frame=CGRectMake(325, CGRectGetMaxY(txt_MealTitle.frame)+20, 15, 10);
    }
    else  {
        img_dropbox.frame=CGRectMake(275, CGRectGetMaxY(txt_MealTitle.frame)+20, 15, 10);
        
    }
    img_dropbox.backgroundColor = [UIColor clearColor];
    [img_dropbox setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [img_dropbox setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [img_dropbox addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimages addSubview:img_dropbox];
    
    
    
    UITextField *txt_Cuisine= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus){
        txt_Cuisine.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6){
        txt_Cuisine.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+5, 200, 30);
    }
    else  {
        txt_Cuisine.frame=CGRectMake(20, CGRectGetMaxY(txt_Category.frame)+5, 200, 30);
    }
    txt_Cuisine.borderStyle = UITextBorderStyleNone;
    txt_Cuisine.font = [UIFont fontWithName:kFont size:13];
    txt_Cuisine.placeholder = @"Cuisine";
    [txt_Cuisine setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Cuisine setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Cuisine.leftViewMode = UITextFieldViewModeAlways;
    txt_Cuisine.userInteractionEnabled=YES;
    txt_Cuisine.textAlignment = NSTextAlignmentLeft;
    txt_Cuisine.backgroundColor = [UIColor clearColor];
    txt_Cuisine.keyboardType = UIKeyboardTypeAlphabet;
    txt_Cuisine.delegate = self;
    [img_backgroundimages addSubview:txt_Cuisine];
    
    UIImageView *img_line=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus){
        img_line.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6){
        img_line.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    }
    else  {
        img_line.frame=CGRectMake(20, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_line setUserInteractionEnabled:YES];
    img_line.backgroundColor=[UIColor clearColor];
    img_line.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_line];
    
    
    
    UIButton *img_dropbox1mg = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        img_dropbox1mg.frame=CGRectMake(370, CGRectGetMaxY(txt_Category.frame)+20, 15, 10);
    }
    else if (IS_IPHONE_6){
        img_dropbox1mg.frame=CGRectMake(325, CGRectGetMaxY(txt_Category.frame)+20, 15, 10);
    }
    else  {
        img_dropbox1mg.frame=CGRectMake(275, CGRectGetMaxY(txt_Category.frame)+20, 15, 10);
        
    }
    img_dropbox1mg.backgroundColor = [UIColor clearColor];
    [img_dropbox1mg setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [img_dropbox1mg setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [img_dropbox1mg addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimages addSubview:img_dropbox1mg];
    
    UITextField *txt_Course= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus){
        txt_Course.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6){
        txt_Course.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+5, 200, 30);
    }
    else  {
        txt_Course.frame=CGRectMake(20, CGRectGetMaxY(txt_Cuisine.frame)+5, 200, 30);
    }
    txt_Course.borderStyle = UITextBorderStyleNone;
    txt_Course.font = [UIFont fontWithName:kFont size:13];
    txt_Course.placeholder = @"Course";
    [txt_Course setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Course setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Course.leftViewMode = UITextFieldViewModeAlways;
    txt_Course.userInteractionEnabled=YES;
    txt_Course.textAlignment = NSTextAlignmentLeft;
    txt_Course.backgroundColor = [UIColor clearColor];
    txt_Course.keyboardType = UIKeyboardTypeAlphabet;
    txt_Course.delegate = self;
    [img_backgroundimages addSubview:txt_Course];
    
    UIImageView *img_lineimge=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus){
        img_lineimge.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6){
        img_lineimge.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+1, WIDTH-60, 0.5);
    }
    else  {
        img_lineimge.frame=CGRectMake(20, CGRectGetMaxY(txt_Course.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_lineimge setUserInteractionEnabled:YES];
    img_lineimge.backgroundColor=[UIColor clearColor];
    img_lineimge.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_lineimge];
    
    
    
    UIButton *img_dropbox1mgs = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        img_dropbox1mgs.frame=CGRectMake(370, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
    }
    else if (IS_IPHONE_6){
        img_dropbox1mgs.frame=CGRectMake(325, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
    }
    else  {
        img_dropbox1mgs.frame=CGRectMake(275, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
        
    }
    img_dropbox1mgs.backgroundColor = [UIColor clearColor];
    [img_dropbox1mgs setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [img_dropbox1mgs setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [img_dropbox1mgs addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimages addSubview:img_dropbox1mgs];
    
    
    
    
    UITextField *txt_keyword= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus){
        txt_keyword.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6){
        txt_keyword.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+5, 200, 30);
    }
    else  {
        txt_keyword.frame=CGRectMake(20, CGRectGetMaxY(txt_Course.frame)+5, 200, 30);
    }
    txt_keyword.borderStyle = UITextBorderStyleNone;
    txt_keyword.font = [UIFont fontWithName:kFont size:13];
    txt_keyword.placeholder = @"Keyword(Eg. Tacos, Fries)";
    [txt_keyword setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_keyword setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_keyword.leftViewMode = UITextFieldViewModeAlways;
    txt_keyword.userInteractionEnabled=YES;
    txt_keyword.textAlignment = NSTextAlignmentLeft;
    txt_keyword.backgroundColor = [UIColor clearColor];
    txt_keyword.keyboardType = UIKeyboardTypeAlphabet;
    txt_keyword.delegate = self;
    [img_backgroundimages addSubview:txt_keyword];
    
    UIImageView *img_lineimges=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus){
        img_lineimges.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6){
        img_lineimges.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+1, WIDTH-60, 0.5);
    }
    else  {
        img_lineimges.frame=CGRectMake(20, CGRectGetMaxY(txt_keyword.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_lineimges setUserInteractionEnabled:YES];
    img_lineimges.backgroundColor=[UIColor clearColor];
    img_lineimges.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_lineimges];
    
    
    
    UILabel  * lbl_uptolimit = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus){
        lbl_uptolimit.frame=CGRectMake(350, CGRectGetMaxY(txt_Course.frame)+20, 35, 15);
        
    }
    else if (IS_IPHONE_6){
        lbl_uptolimit.frame=CGRectMake(310, CGRectGetMaxY(txt_Course.frame)+20, 35, 15);
        
    }
    else  {
        lbl_uptolimit.frame=CGRectMake(260, CGRectGetMaxY(txt_Course.frame)+20, 35, 15);
        
        
    }
    lbl_uptolimit.text = @"Up to 5";
    lbl_uptolimit.backgroundColor=[UIColor clearColor];
    lbl_uptolimit.textColor=[UIColor lightGrayColor];
    lbl_uptolimit.textAlignment=NSTextAlignmentLeft;
    lbl_uptolimit.numberOfLines=0;
    lbl_uptolimit.font = [UIFont fontWithName:kFontBold size:9];
    [img_backgroundimages addSubview:lbl_uptolimit];
    
    //    UIButton *btn_uptolimit = [[UIButton alloc] init];
    //
    //    if (IS_IPHONE_6Plus){
    //        btn_uptolimit.frame=CGRectMake(200, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    //    }
    //    else if (IS_IPHONE_6){
    //        btn_uptolimit.frame=CGRectMake(325, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
    //    }
    //    else  {
    //        btn_uptolimit.frame=CGRectMake(250, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    //
    //    }
    //    btn_uptolimit.backgroundColor = [UIColor clearColor];
    ////    [btn_uptolimit setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    //    [btn_uptolimit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    //    [btn_uptolimit addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    //    [img_backgroundimages addSubview:btn_uptolimit];
    //
    
    UITextField *txt_Price= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus){
        txt_Price.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6){
        txt_Price.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+5, 200, 30);
    }
    else  {
        txt_Price.frame=CGRectMake(20, CGRectGetMaxY(txt_keyword.frame)+5, 200, 30);
    }
    txt_Price.borderStyle = UITextBorderStyleNone;
    txt_Price.font = [UIFont fontWithName:kFont size:13];
    txt_Price.placeholder = @"Price";
    [txt_Price setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Price setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Price.leftViewMode = UITextFieldViewModeAlways;
    txt_Price.userInteractionEnabled=YES;
    txt_Price.textAlignment = NSTextAlignmentLeft;
    txt_Price.backgroundColor = [UIColor clearColor];
    txt_Price.keyboardType = UIKeyboardTypeAlphabet;
    txt_Price.delegate = self;
    [img_backgroundimages addSubview:txt_Price];
    
    UIImageView *img_linesimge=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus){
        img_linesimge.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6){
        img_linesimge.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+1, WIDTH-60, 0.5);
    }
    else  {
        img_linesimge.frame=CGRectMake(20, CGRectGetMaxY(txt_Price.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_linesimge setUserInteractionEnabled:YES];
    img_linesimge.backgroundColor=[UIColor clearColor];
    img_linesimge.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_linesimge];
    
    
    
    UILabel  * lbl_PerMeal = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus){
        lbl_PerMeal.frame=CGRectMake(345, CGRectGetMaxY(txt_keyword.frame)+20, 40, 15);
        
    }
    else if (IS_IPHONE_6){
        lbl_PerMeal.frame=CGRectMake(290, CGRectGetMaxY(txt_keyword.frame)+20, 60, 15);
        
    }
    else  {
        lbl_PerMeal.frame=CGRectMake(250, CGRectGetMaxY(txt_keyword.frame)+20, 40, 15);
        
        
    }
    lbl_PerMeal.text = @"Per Serving";
    lbl_PerMeal.backgroundColor=[UIColor clearColor];
    lbl_PerMeal.textColor=[UIColor lightGrayColor];
    lbl_PerMeal.textAlignment=NSTextAlignmentLeft;
    lbl_PerMeal.numberOfLines=0;
    lbl_PerMeal.font = [UIFont fontWithName:kFontBold size:9];
    [img_backgroundimages addSubview:lbl_PerMeal];
    
    
    
    
    
    UILabel  * lbl_Description = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus){
        lbl_Description.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+10, 100, 25);
        
    }
    else if (IS_IPHONE_6){
        lbl_Description.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+10, 100, 25);
        
    }
    else  {
        lbl_Description.frame=CGRectMake(20, CGRectGetMaxY(txt_Price.frame)+10, 100, 25);
        
        
    }
    lbl_Description.text = @"Description";
    lbl_Description.backgroundColor=[UIColor clearColor];
    lbl_Description.textColor=[UIColor blackColor];
    lbl_Description.textAlignment=NSTextAlignmentLeft;
    lbl_Description.numberOfLines=0;
    lbl_Description.font = [UIFont fontWithName:kFont size:14];
    [img_backgroundimages addSubview:lbl_Description];
    
    
    
    UITextView * txt_view=[[UITextView alloc]init];
    if (IS_IPHONE_6Plus){
        txt_view.frame=CGRectMake(30, CGRectGetMaxY(lbl_Description.frame)+5, WIDTH-60, 100);
        
    }
    else if (IS_IPHONE_6){
        txt_view.frame=CGRectMake(30, CGRectGetMaxY(lbl_Description.frame)+5, WIDTH-60, 100);
        
    }
    else  {
        txt_view.frame=CGRectMake(20, CGRectGetMaxY(lbl_Description.frame)+5, WIDTH-40, 100);
        
        
    }
    [txt_view setReturnKeyType:UIReturnKeyDone];
    txt_view.scrollEnabled=YES;
    [txt_view setDelegate:self];
    [txt_view setReturnKeyType:UIReturnKeyDone];
    [txt_view setFont:[UIFont fontWithName:kFont size:10]];
    [txt_view setTextColor:[UIColor lightTextColor]];
    txt_view.userInteractionEnabled=YES;
    txt_view.backgroundColor=[UIColor whiteColor];
    txt_view.delegate=self;
    txt_view.textColor=[UIColor blackColor];
    txt_view.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    txt_view.layer.borderWidth=1.0f;
    txt_view.clipsToBounds=YES;
    [img_backgroundimages addSubview:txt_view];
    
    UILabel  * lblmax = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus){
        lblmax.frame=CGRectMake(320, CGRectGetMaxY(txt_view.frame)+1, 70, 15);
        
    }
    else if (IS_IPHONE_6){
        lblmax.frame=CGRectMake(280, CGRectGetMaxY(txt_view.frame)+1, 70, 15);
        
    }
    else  {
        lblmax.frame=CGRectMake(250, CGRectGetMaxY(txt_view.frame)+1, 70, 15);
        
        
    }
    
    lblmax.text = @"(500 char. max.)";
    lblmax.backgroundColor=[UIColor clearColor];
    lblmax.textColor=[UIColor lightGrayColor];
    lblmax.numberOfLines = 0;
    lblmax.font = [UIFont fontWithName:kFont size:8];
    [img_backgroundimages addSubview:lblmax];
    
    
    
    
    
    UIButton *btn_Save = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus){
        btn_Save.frame=CGRectMake(50, HEIGHT-50, WIDTH-80, 40);
    }
    else if (IS_IPHONE_6){
        btn_Save.frame=CGRectMake(50, HEIGHT-50, WIDTH-80, 40);
    }
    else  {
        btn_Save.frame=CGRectMake(30, HEIGHT-50, WIDTH-60, 40);
        
    }
    btn_Save.backgroundColor = [UIColor clearColor];
    [btn_Save setImage:[UIImage imageNamed:@"img_savebutton.png"] forState:UIControlStateNormal];
    [btn_Save setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [btn_uptolimit addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [self.view addSubview:btn_Save];
    
    
    
    
    
    
    
    
    
    
}


#pragma mark UiCollectionView Delegate Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [collectionarrImages count];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView1 dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    UIImageView *img_backGnd = [[UIImageView alloc]init];
    
    
    if (IS_IPHONE_6Plus){
        img_backGnd.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
    }
    else if (IS_IPHONE_6){
        img_backGnd.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
    }
    else  {
        img_backGnd.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
    }
    img_backGnd.backgroundColor = [UIColor clearColor];
    [cell.contentView addSubview:img_backGnd];
    
    
    UIImageView *img_Images = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus){
        img_Images.frame = CGRectMake(28, 0, 55,55);
        
    }
    else if (IS_IPHONE_6){
        img_Images.frame = CGRectMake(28, 0,55,55);
        
    }
    else  {
        img_Images.frame = CGRectMake(25, 0, 40,40);
        
    }
    
    [img_Images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[collectionarrImages objectAtIndex:indexPath.row]]]];
    [img_Images setUserInteractionEnabled:YES];
    [img_Images setContentMode:UIViewContentModeScaleAspectFill];
    [img_Images setClipsToBounds:YES];
    [img_Images setUserInteractionEnabled:YES];
    [img_backGnd addSubview:img_Images];
    
    
    UILabel  * lbl_headings = [[UILabel alloc]init];
    if (IS_IPHONE_6Plus){
        lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(img_Images.frame)+1, ((WIDTH-65)/3)-10,10);
        
    }
    else if (IS_IPHONE_6){
        lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(img_Images.frame)+1, ((WIDTH-65)/3)-10,10);
        
    }
    else  {
        lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(img_Images.frame)+1, ((WIDTH-65)/3)-10,10);
        
    }
    
    if (indexPath.row == 0) {
        lbl_headings.text = @"Halal";
    }else if (indexPath.row==1){
        lbl_headings.text=@"Kasher";
    }else{
        lbl_headings.text = @"Organic";
    }
    lbl_headings.backgroundColor=[UIColor clearColor];
    lbl_headings.textColor=[UIColor blackColor];
    lbl_headings.textAlignment=NSTextAlignmentCenter;
    lbl_headings.font = [UIFont fontWithName:kFont size:9];
    [img_backGnd addSubview:lbl_headings];
    
    UIImageView *img_Tikmark=[[UIImageView alloc]init];
    if (indexPath.row ==2)
    {
        img_Tikmark.image=[UIImage imageNamed:@""];
        
    }
    else{
        if (IS_IPHONE_6Plus){
            img_Tikmark.frame=CGRectMake(CGRectGetMaxX(img_Images.frame)-10, CGRectGetMaxY(img_Images.frame)+2, 12, 10);
        }
        else if (IS_IPHONE_6){
            img_Tikmark.frame=CGRectMake(CGRectGetMaxX(img_Images.frame)-10, CGRectGetMaxY(img_Images.frame)+2, 12, 10);
        }
        else  {
            img_Tikmark.frame=CGRectMake(CGRectGetMaxX(lbl_headings.frame)-20, CGRectGetMaxY(img_Images.frame)+2, 12, 10);
        }
        
        
        img_Tikmark.image=[UIImage imageNamed:@"img_correct@2x.png"];
        
    }
    [img_Tikmark setUserInteractionEnabled:YES];
    img_Tikmark.backgroundColor=[UIColor clearColor];
    
    [img_backGnd addSubview:img_Tikmark];
    
    
    
    
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (IS_IPHONE_6Plus){
        return CGSizeMake((WIDTH-46)/3, 70);
    }
    else if (IS_IPHONE_6){
        return CGSizeMake((WIDTH-46)/3, 70);
    }
    else  {
        return CGSizeMake((WIDTH-46)/3, 60);
    }
    
    return CGSizeMake(0, 0);
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
