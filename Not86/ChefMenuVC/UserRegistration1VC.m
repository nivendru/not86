//
//  UserRegistration1VC.m
//  Not86
//
//  Created by Admin on 29/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "UserRegistration1VC.h"
#import "Define.h"
#import "UserRegistration2VC.h"
#import "UserSignUpVC.h"
#import "AppDelegate.h"


#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"



@interface UserRegistration1VC ()<UITextFieldDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>
{
    UIImageView *img_header;
    UIImageView *img_background;
    CGFloat	animatedDistance;
    
    UIImageView *  user_img ;
    UIScrollView *scroll;
    
    
    UITableView *img_table_for_contry_code;
    
    NSMutableArray *arrayCountry;
    
    NSData *imageData;
    UIView*alertviewBg;
    AppDelegate *delegate;
    UIButton *profileimg_button;
    NSString*str_pickingImage;
    NSData*imgData;
    UIDatePicker*datePicker;
    UITextField *txt_Date;
    NSString * str_countrycodetosend;
    
    NSString*str_DOBfinal;
   
    UITextField *txt_Tel_Code;
    UITextField *txt_Mobile_No;
    UIButton *icon_dropdown;
    UIButton *btn_on_drop_down;
    UILabel *Date_Of_Birth;
    UIImageView *line5;
    UIImageView *line4;
    
    UITextField *txt_First_Name;
    UITextField *txt_Middle_Name;
    UITextField *txt_Last_Name;
    UITextField *txt_Gmail;
    
    
}

@end

@implementation UserRegistration1VC
@synthesize array_Facebook_Details;

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;



#pragma mark - UIAction
-(void)doneClicked{
    [self.txt_MobileNo resignFirstResponder];
}

-(void)user_img_click:(UIGestureRecognizer *)recognizer
{
    NSLog(@"-------- tap Image --------%li",  (long)[recognizer view].tag);
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Add Photo From " delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera", @"Photo", nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    actionSheet.backgroundColor = [UIColor clearColor];
    actionSheet.frame=CGRectMake(8, 90, 304, 216);
    
    // actionSheet.alpha=0.90;
    actionSheet.tag = 1000;
    [actionSheet showInView:self.view];
    
    

    
}


#pragma mark - Date Picker
- (void)datePickerValueChanged:(id)sender
{
    [formatter setDateFormat:@"dd/MM/yyyy"];
    //    @"d  d   m  m   y  y  y  y"
    NSString *str=[formatter stringFromDate:datePicker.date];
    
    
    if(IS_IPHONE_6Plus)
    {
        str_DOBfinal = [NSString stringWithFormat:@"%@      %@          %@     %@          %@      %@      %@     %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
        
    }
    else if(IS_IPHONE_6)
    {
        str_DOBfinal = [NSString stringWithFormat:@"%@    %@         %@     %@        %@     %@    %@     %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
    }
    else
    {
        str_DOBfinal = [NSString stringWithFormat:@"%@    %@     %@   %@      %@   %@   %@   %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
    }
    
    //    str_DOBfinal = [NSString stringWithFormat:@"  %@     %@          %@     %@        %@      %@     %@      %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
    //    NSLog(@"fdyhsag %@",str_DOBfinal);
    
    txt_Date.font = [UIFont fontWithName:kFont size:14];
    txt_Date.textColor = [UIColor blackColor];
    
    NSLog(@"gdj%@", [str substringToIndex:2]);
    
    [txt_Date setText:str_DOBfinal];
    
}

-(void) click_DoneDate:(id) sender
{
    [formatter setDateFormat:@"dd/MM/yyyy"];
    //    @"d  d   m  m   y  y  y  y"
    NSString *str=[formatter stringFromDate:datePicker.date];
    
   
    
    if(IS_IPHONE_6Plus)
    {
        str_DOBfinal = [NSString stringWithFormat:@"%@      %@          %@     %@          %@      %@      %@     %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
        
    }
    else if(IS_IPHONE_6)
    {
        str_DOBfinal = [NSString stringWithFormat:@"%@    %@         %@     %@        %@     %@    %@     %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
    }
    else
    {
        str_DOBfinal = [NSString stringWithFormat:@"%@   %@      %@   %@      %@   %@   %@   %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
        NSLog(@"fdyhsag %@",str_DOBfinal);
        
        
    }
    
    //    str_DOBfinal = [NSString stringWithFormat:@"  %@     %@          %@     %@        %@      %@     %@      %@",[str substringToIndex:1],[str substringWithRange:NSMakeRange(1, 1)],[str substringWithRange:NSMakeRange(3, 1)],[str substringWithRange:NSMakeRange(4, 1)],[str substringWithRange:NSMakeRange(6, 1)],[str substringWithRange:NSMakeRange(7, 1)],[str substringWithRange:NSMakeRange(8, 1)],[str substringWithRange:NSMakeRange(9, 1)]];
    //    NSLog(@"fdyhsag %@",str_DOBfinal);
    
    txt_Date.font = [UIFont fontWithName:kFont size:14];
    txt_Date.textColor = [UIColor blackColor];
    
    NSLog(@"gdj%@", [str substringToIndex:2]);
    
    [txt_Date setText:str_DOBfinal];
    [self.view endEditing:YES];
}



//- (void)datePickerValueChanged:(id)sender
//{
//    [formatter setDateFormat:@"dd/MM/yyyy"];
//    txt_Date.textColor = [UIColor blackColor];
//    [txt_Date setText:[formatter stringFromDate:datePicker.date]];
//}
//
//-(void) click_DoneDate:(id) sender
//{
//    [formatter setDateFormat:@"dd/MM/yyyy"];
//    txt_Date.textColor = [UIColor blackColor];
//    [txt_Date setText:[formatter stringFromDate:datePicker.date]];
//    [self.view endEditing:YES];
//}

#pragma mark - UIView LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    str_pickingImage = [NSString new];
    
     str_DOBfinal = [NSString new];

    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    
    arrayCountry = [NSMutableArray new];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    str_countrycodetosend = [NSString new];
    
    
    [self integrateHeader];
    [self integrateBodyDesign];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self countryList];
    });
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)integrateHeader
{
    
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [ self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back addTarget:self action:@selector(btn_back_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_back];
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init ];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_back.frame)+30,0, 150, 45);
    lbl_User_Sign_Up.text = @"User Sign Up";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    UIImageView *icon_user = [[UIImageView alloc]init ];
    icon_user .frame = CGRectMake(WIDTH-40, 8, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"img_user_icon@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
}

-(void)integrateBodyDesign
{
    
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor whiteColor];
    //  scroll.frame = CGRectMake(0, 51, WIDTH, 450);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    user_img = [[UIImageView alloc]init];
    // user_img.frame = CGRectMake(95, CGRectGetMaxY(img_header.frame)+10, 120, 120);
    user_img .backgroundColor = [UIColor clearColor];
    user_img.layer.cornerRadius = 120/2;
    user_img.clipsToBounds = YES;
    //[user_img setImage:[UIImage imageNamed:@"img-user@2x.png"] forState:UIControlStateNormal];
    user_img.image = [UIImage imageNamed:@"img-defaltpic@2x.png"];
    user_img.userInteractionEnabled = YES;
    [scroll   addSubview:user_img];
    
   
    profileimg_button = [[UIButton alloc] init];
    profileimg_button.backgroundColor=[UIColor clearColor];
    [profileimg_button addTarget:self action:@selector(profilepic_clickbtn:) forControlEvents:UIControlEventTouchUpInside ];
    [user_img addSubview:profileimg_button];

//    UITapGestureRecognizer *tapRecognizer1;
//    tapRecognizer1=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(user_img_click:)];
//    tapRecognizer1.numberOfTapsRequired=1;
//    tapRecognizer1.numberOfTouchesRequired=1;
//    tapRecognizer1.delegate = self;
//    [user_img addGestureRecognizer:tapRecognizer1];
//    user_img.userInteractionEnabled = YES;
    
    UILabel *lbl_User_Info = [[UILabel alloc]init ];
    //  lbl_User_Info.frame = CGRectMake(-10,CGRectGetMaxY(user_img.frame)-45,200, 15);
    lbl_User_Info.text = @"Personal Information";
    lbl_User_Info.font = [UIFont fontWithName:kFontBold size:15];
    lbl_User_Info.textColor = [UIColor blackColor];
    lbl_User_Info.backgroundColor = [UIColor clearColor];
    [ scroll addSubview:lbl_User_Info];
    
    txt_First_Name = [[UITextField alloc] init];
    //   txt_First_Name .frame=CGRectMake(-70, CGRectGetMaxY(lbl_User_Info.frame)+10, WIDTH-80, 38);
    txt_First_Name .borderStyle = UITextBorderStyleNone;
    txt_First_Name .textColor = [UIColor blackColor];
    txt_First_Name .font = [UIFont fontWithName:kFont size:16];
    txt_First_Name .placeholder = @"First Name";
    [txt_First_Name  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_First_Name  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_First_Name .leftView = padding1;
    txt_First_Name .leftViewMode = UITextFieldViewModeAlways;
    txt_First_Name .userInteractionEnabled=YES;
    txt_First_Name .textAlignment = NSTextAlignmentLeft;
    txt_First_Name .backgroundColor = [UIColor clearColor];
    txt_First_Name .keyboardType = UIKeyboardTypeAlphabet;
    txt_First_Name .delegate = self;
    [ scroll addSubview:txt_First_Name ];
    self.txt_FirstName = txt_First_Name;
    
    UIImageView *line=[[UIImageView alloc]init];
    //  line.frame=CGRectMake(-65, CGRectGetMaxY(txt_First_Name .frame)-5, 270, 0.5);
    [line setUserInteractionEnabled:YES];
    line.backgroundColor=[UIColor clearColor];
    line.image=[UIImage imageNamed:@"img-line@2x.png"];
    [ scroll addSubview:line];
    
    
    txt_Middle_Name = [[UITextField alloc] init];
    //   txt_Middle_Name.frame=CGRectMake(-70, CGRectGetMaxY(line.frame)+10, WIDTH-80, 38);
    txt_Middle_Name.borderStyle = UITextBorderStyleNone;
    txt_Middle_Name.textColor = [UIColor blackColor];
    txt_Middle_Name.font = [UIFont fontWithName:kFont size:16];
    txt_Middle_Name.placeholder = @"Middle Name";
    [txt_Middle_Name setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_Middle_Name setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_Middle_Name.leftView = padding2;
    txt_Middle_Name.leftViewMode = UITextFieldViewModeAlways;
    txt_Middle_Name.userInteractionEnabled=YES;
    txt_Middle_Name.textAlignment = NSTextAlignmentLeft;
    txt_Middle_Name.backgroundColor = [UIColor clearColor];
    txt_Middle_Name.keyboardType = UIKeyboardTypeAlphabet;
    txt_Middle_Name.delegate = self;
    [ scroll addSubview:txt_Middle_Name];
    self.txt_MiddleName = txt_Middle_Name;
    
    
    UIImageView *line2=[[UIImageView alloc]init];
    //   line2.frame=CGRectMake(-65, CGRectGetMaxY(txt_Middle_Name.frame)-5, 270, 0.5);
    [line2 setUserInteractionEnabled:YES];
    line2.backgroundColor=[UIColor clearColor];
    line2.image=[UIImage imageNamed:@"img-line@2x.png"];
    [ scroll addSubview:line2];
    
    UILabel *lbl_optional = [[UILabel alloc]init ];
    // lbl_optional.frame = CGRectMake(150,CGRectGetMaxY(line2.frame)+230,100, 15);
    lbl_optional.text = @"(optional)";
    lbl_optional.font = [UIFont fontWithName:kFont size:12];
    lbl_optional.textColor = [UIColor blackColor];
    lbl_optional.backgroundColor = [UIColor clearColor];
    [ scroll addSubview:lbl_optional];
    
    
    
    txt_Last_Name = [[UITextField alloc] init];
    //   txt_Last_Name .frame=CGRectMake(-70, CGRectGetMaxY(line2.frame)+10, WIDTH-80, 38);
    txt_Last_Name .borderStyle = UITextBorderStyleNone;
    txt_Last_Name .textColor = [UIColor blackColor];
    txt_Last_Name .font = [UIFont fontWithName:kFont size:16];
    txt_Last_Name .placeholder = @"Last Name";
    [txt_Last_Name  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_Last_Name  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_Last_Name .leftView = padding3;
    txt_Last_Name .leftViewMode = UITextFieldViewModeAlways;
    txt_Last_Name .userInteractionEnabled=YES;
    txt_Last_Name .textAlignment = NSTextAlignmentLeft;
    txt_Last_Name .backgroundColor = [UIColor clearColor];
    txt_Last_Name .keyboardType = UIKeyboardTypeAlphabet;
    txt_Last_Name .delegate = self;
    [ scroll addSubview:txt_Last_Name ];
    self.txt_LastName = txt_Last_Name;
    
    UIImageView *line3=[[UIImageView alloc]init];
    //   line3.frame=CGRectMake(-65, CGRectGetMaxY(txt_Last_Name.frame)-5, 270, 0.5);
    [line3 setUserInteractionEnabled:YES];
    line3.backgroundColor=[UIColor clearColor];
    line3.image=[UIImage imageNamed:@"img-line@2x.png"];
    [ scroll addSubview:line3];
    
    
    Date_Of_Birth = [[UILabel alloc]init];
    //   Date_Of_Birth.frame = CGRectMake(-65, CGRectGetMaxY(line3.frame)+10, WIDTH, 38);
   // Date_Of_Birth.text = @"Date of Birth __ __ / __ __ / __ __ __ __";
    Date_Of_Birth.text = @"Date of Birth";

    Date_Of_Birth.font = [UIFont fontWithName:kFont size:15];
    Date_Of_Birth.textColor = [UIColor blackColor];
    Date_Of_Birth.backgroundColor = [UIColor clearColor];
    [ scroll addSubview:Date_Of_Birth];
    
    
    
    UILabel *  lbl_lines = [[UILabel alloc]init];
    lbl_lines.frame = CGRectMake(CGRectGetMaxX(Date_Of_Birth.frame)+60, CGRectGetMaxY(line3.frame)+200, 150,20);
    lbl_lines.text = @"__ __ / __ __ / __ __ __ __";
    lbl_lines.backgroundColor=[UIColor clearColor];
    [lbl_lines setUserInteractionEnabled:YES];
    lbl_lines.textColor=[UIColor blackColor];
    lbl_lines.font = [UIFont fontWithName:kFont size:13];
    [scroll addSubview:lbl_lines];
    
    txt_Date = [[UITextField alloc] init];
    txt_Date.borderStyle = UITextBorderStyleNone;
    txt_Date.placeholder = @"dd/mm/yyyy";//@"__ __ / __ __ / __ __ __ __";
    
    if(IS_IPHONE_6Plus)
    {
//        txt_Date.frame = CGRectMake(5, 5, 200, 20);
        txt_Date.placeholder = @"d    d       m   m      y    y     y     y";
         txt_Date.font = [UIFont systemFontOfSize:17.0];
         [txt_Date setValue:[UIFont fontWithName:kFont size: 17] forKeyPath:@"_placeholderLabel.font"];
        
        
        
    }
    else if(IS_IPHONE_6)
    {
//         txt_Date.frame = CGRectMake(5, 5, 200, 20);
        txt_Date.placeholder = @"d    d      m   m       y    y     y    y";
         txt_Date.font = [UIFont systemFontOfSize:15.0];
         [txt_Date setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
        
    }
    else
    {
//         txt_Date.frame = CGRectMake(5, 5, 200, 20);
        txt_Date.placeholder = @"d   d     m  m     y   y   y   y";
         txt_Date.font = [UIFont systemFontOfSize:14.0];
         [txt_Date setValue:[UIFont fontWithName:kFont size: 14] forKeyPath:@"_placeholderLabel.font"];
        
    }
    txt_Date.textColor = [UIColor colorWithRed:167.0/255.0 green:167.0/255.0 blue:167.0/255.0 alpha:1.0];
   // txt_Date.font = [UIFont systemFontOfSize:15.0];
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 4, 20)];
    txt_Date.leftView = paddingView1;
    txt_Date.leftViewMode = UITextFieldViewModeAlways;
    txt_Date.autocorrectionType = UITextAutocorrectionTypeNo;
    //   txt_Date.keyboardType = UIKeyboardTypeDefault;
    txt_Date.clearButtonMode = UITextFieldViewModeWhileEditing;
    txt_Date.delegate = self;
    txt_Date.userInteractionEnabled = YES;
    [txt_Date setValue:[UIColor colorWithRed:167.0/255.0 green:167.0/255.0 blue:167.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
   // [txt_Date setValue:[UIFont fontWithName:kFont size: 15] forKeyPath:@"_placeholderLabel.font"];
    [scroll addSubview:txt_Date];
    
    if (self.keyboardToolbar_Date == nil)
    {
        self.keyboardToolbar_Date = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [self.keyboardToolbar_Date setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_DoneDate:)];
        [self.keyboardToolbar_Date setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    txt_Date.inputAccessoryView = self.keyboardToolbar_Date;
    txt_Date.backgroundColor=[UIColor clearColor];
    
    self.txt_DateBirth = txt_Date;
    
    formatter = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    //datePicker.minimumDate = [formatter dateFromString:@"1900-01-01"];
    
    
    NSDate *currentDate = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate];
    components.day=components.day;
    components.month=components.month;
    components.year = components.year;
    [components setYear:-19];
    
//    NSString *temp=[NSString stringWithFormat:@" Day:%d Months:%d Year:%ld",(long)components.day,(long)components.month,(long)components.year];
//    NSLog(@"%@",temp);
//    NSString *string = [NSString stringWithFormat:@"%ld.%ld.%ld", (long)components.day, (long)components.month, (long)components.year];
    NSDate *minDate=[calendar dateByAddingComponents:components toDate:currentDate  options:0];
    
    NSLog(@"Minimum date is :: %@",minDate);
    
    [components setYear:-150];
    
  NSDate *maxDate = [calendar dateByAddingComponents:components toDate:currentDate  options:0];
    
//   NSDate *maxDate=  [formatter dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components.year, (long)components.month,(long)components.day]];
    
//   datePicker.maximumDate= [formatter dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)components.year-20, (long)components.month,(long)components.day]];
    
    NSLog(@"Maximum date is :: %@",maxDate);
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [datePicker setMinimumDate:maxDate];
    [datePicker setMaximumDate:minDate];
    [datePicker setDate:minDate];
    txt_Date.inputView = datePicker;
    
    
    
    
    
    
    
    txt_Tel_Code = [[UITextField alloc] init];
    //   txt_Tel_Code  .frame=CGRectMake(-70, CGRectGetMaxY(Date_Of_Birth.frame)+10, WIDTH-80, 38);
    txt_Tel_Code  .borderStyle = UITextBorderStyleNone;
    txt_Tel_Code  .textColor = [UIColor grayColor];
    txt_Tel_Code  .font = [UIFont fontWithName:kFont size:14];
    txt_Tel_Code  .placeholder = @"+65";
    [txt_Tel_Code   setValue:[UIFont fontWithName:kFont size: 14] forKeyPath:@"_placeholderLabel.font"];
    [txt_Tel_Code   setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding5 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 10)];
    txt_Tel_Code  .leftView = padding5;
    txt_Tel_Code  .leftViewMode = UITextFieldViewModeAlways;
    txt_Tel_Code  .userInteractionEnabled=YES;
    txt_Tel_Code  .textAlignment = NSTextAlignmentLeft;
    txt_Tel_Code .backgroundColor = [UIColor whiteColor];
    txt_Tel_Code  .keyboardType = UIKeyboardTypeAlphabet;
    txt_Tel_Code  .delegate = self;
    [ scroll addSubview:txt_Tel_Code ];
    txt_Tel_Code.enabled = NO;
    self.txt_Phone_Code = txt_Tel_Code;
    
    txt_Mobile_No = [[UITextField alloc] init];
    //    txt_Mobile_No   .frame=CGRectMake(-10, CGRectGetMaxY(Date_Of_Birth.frame)+10, WIDTH-80, 38);
    txt_Mobile_No   .borderStyle = UITextBorderStyleNone;
    txt_Mobile_No  .textColor = [UIColor blackColor];
    txt_Mobile_No .font = [UIFont fontWithName:kFont size:16];
    txt_Mobile_No  .placeholder = @"Mobile no.";
    [txt_Mobile_No   setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_Mobile_No   setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_Mobile_No   .leftView = padding6;
    txt_Mobile_No   .leftViewMode = UITextFieldViewModeAlways;
    txt_Mobile_No   .userInteractionEnabled=YES;
    txt_Mobile_No  .textAlignment = NSTextAlignmentLeft;
    txt_Mobile_No  .backgroundColor = [UIColor clearColor];
    txt_Mobile_No   .keyboardType = UIKeyboardTypeNumberPad;

    txt_Mobile_No  .delegate = self;
    [ scroll addSubview:txt_Mobile_No];
    self.txt_MobileNo = txt_Mobile_No;
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked)],
                           nil];
    [numberToolbar sizeToFit];
    txt_Mobile_No.inputAccessoryView = numberToolbar;

    
    
    
    icon_dropdown =[[UIButton alloc]init];
    //  icon_dropdown.frame=CGRectMake(CGRectGetMaxX(txt_Tel_Code.frame)-203, CGRectGetMaxY(Date_Of_Birth.frame)+25, 15, 10);
    //icon_dropdown.backgroundColor = [UIColor clearColor];
    [icon_dropdown addTarget:self action:@selector(icon_drop_down_click:)forControlEvents:UIControlEventTouchUpInside];
    icon_dropdown.userInteractionEnabled = YES;
    [icon_dropdown setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [ scroll addSubview:icon_dropdown];
    
   btn_on_drop_down =[[UIButton alloc]init];
    //  btn_on_drop_down.frame=CGRectMake(CGRectGetMaxX(txt_Tel_Code.frame)-203, CGRectGetMaxY(Date_Of_Birth.frame)+25, 15, 10);
    btn_on_drop_down.backgroundColor = [UIColor clearColor];
    [btn_on_drop_down addTarget:self action:@selector(icon_drop_down_click:)forControlEvents:UIControlEventTouchUpInside];
    btn_on_drop_down.userInteractionEnabled = YES;
    //[icon_dropdown setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [ scroll addSubview:btn_on_drop_down];

    
      
    
    
    
    line4=[[UIImageView alloc]init];
    //   line4.frame=CGRectMake(-65, CGRectGetMaxY(txt_Tel_Code.frame)-5, 50, 0.5);
    [line4 setUserInteractionEnabled:YES];
    line4.backgroundColor=[UIColor clearColor];
    line4.image=[UIImage imageNamed:@"img-line@2x.png"];
    [ scroll addSubview:line4];
    
   line5=[[UIImageView alloc]init];
    //  line5.frame=CGRectMake(CGRectGetMaxX(line4.frame)+10, CGRectGetMaxY(txt_Tel_Code.frame)-5, 210, 0.5);
    [line5 setUserInteractionEnabled:YES];
    line5.backgroundColor=[UIColor clearColor];
    line5.image=[UIImage imageNamed:@"img-line@2x.png"];
    [ scroll addSubview:line5];
    
    
    txt_Gmail = [[UITextField alloc] init];
    //   txt_Gmail .frame=CGRectMake(-70, CGRectGetMaxY(line5.frame)+10, WIDTH-30, 38);
    txt_Gmail .borderStyle = UITextBorderStyleNone;
    txt_Gmail .textColor = [UIColor blackColor];
    txt_Gmail .font = [UIFont fontWithName:kFont size:16];
    txt_Gmail .placeholder = @"Email Address(e.g. Chef@gmail.com)";
    [txt_Gmail  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_Gmail  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding7 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_Gmail .leftView = padding7;
    txt_Gmail .leftViewMode = UITextFieldViewModeAlways;
    txt_Gmail .userInteractionEnabled=YES;
    txt_Gmail .textAlignment = NSTextAlignmentLeft;
    txt_Gmail .backgroundColor = [UIColor clearColor];
    txt_Gmail .keyboardType = UIKeyboardTypeEmailAddress;
    txt_Gmail .delegate = self;
    txt_Gmail.autocapitalizationType = UITextAutocorrectionTypeNo;
    [ scroll addSubview:txt_Gmail ];
    self.txt_Email = txt_Gmail;
    
    UIImageView *line6=[[UIImageView alloc]init];
    //   line6.frame=CGRectMake(-65, CGRectGetMaxY(txt_Gmail.frame), 270, 0.5);
    [line6 setUserInteractionEnabled:YES];
    line6.backgroundColor=[UIColor clearColor];
    line6.image=[UIImage imageNamed:@"img-line@2x.png"];
    [ scroll addSubview:line6];
    
    UILabel *page = [[UILabel alloc]init];
    //   page.frame = CGRectMake(24, CGRectGetMaxY(line6.frame)+11, WIDTH-80, 38);
    page.text = @" Page 1/2";
    page.font = [UIFont fontWithName:kFont size:15];
    page.textColor = [UIColor lightGrayColor];
    page.backgroundColor = [UIColor clearColor];
    [ self.view addSubview:page];
    
    
    UIButton *btn_Next =[UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_Next.frame=CGRectMake(-80,CGRectGetMaxY(line6.frame)+49, 300, 45);
    [btn_Next setTitle:@"NEXT" forState:UIControlStateNormal];
    [btn_Next setTitleColor:[UIColor whiteColor ] forState:UIControlStateNormal];
    [btn_Next addTarget:self action:@selector(btn_next_click:) forControlEvents:UIControlEventTouchUpInside];
    btn_Next.layer.cornerRadius = 5.0f;
    btn_Next.userInteractionEnabled = YES;
    btn_Next.backgroundColor = [UIColor colorWithRed:40/255.0f green:30/255.0f blue:48/255.0f alpha:1];
    [btn_Next setImage:[UIImage imageNamed:@"text buuton.png"] forState:UIControlStateNormal];
    [self.view   addSubview:btn_Next];
    
    
#pragma mark Tableview
    
    img_table_for_contry_code = [[UITableView alloc] init ];
    //  img_table_for_contry_code.frame  = CGRectMake(13,290,296,370);
    [img_table_for_contry_code setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table_for_contry_code.delegate = self;
    img_table_for_contry_code.dataSource = self;
    img_table_for_contry_code.showsVerticalScrollIndicator = NO;
    img_table_for_contry_code.backgroundColor = [UIColor whiteColor];
    img_table_for_contry_code.layer.borderColor = [[UIColor blackColor]CGColor];
    img_table_for_contry_code.layer.borderWidth = 1.0f;
    img_table_for_contry_code.clipsToBounds = YES;
    [scroll addSubview:img_table_for_contry_code];
    
    img_table_for_contry_code.hidden =YES;

    
    if (IS_IPHONE_6Plus)
    {
        scroll.frame = CGRectMake(05, 51, WIDTH-10, 510);
        
        user_img.frame = CGRectMake(140, CGRectGetMaxY(img_header.frame)-38, 120, 120);
        profileimg_button.frame=CGRectMake(0, 0, 120, 120);

        lbl_User_Info.frame = CGRectMake(130,CGRectGetMaxY(user_img.frame)+15,200, 15);
        
        txt_First_Name .frame=CGRectMake(15, CGRectGetMaxY(lbl_User_Info.frame)+15, WIDTH-80, 38);
        line.frame=CGRectMake(18, CGRectGetMaxY(txt_First_Name .frame), WIDTH-46, 0.5);
        
        txt_Middle_Name.frame=CGRectMake(15, CGRectGetMaxY(line.frame)+15, WIDTH-80, 38);
        line2.frame=CGRectMake(18, CGRectGetMaxY(txt_Middle_Name.frame), WIDTH-46, 0.5);
        lbl_optional.frame = CGRectMake(330,CGRectGetMaxY(line2.frame),100, 15);
        
        txt_Last_Name .frame=CGRectMake(15, CGRectGetMaxY(line2.frame)+15, WIDTH-80, 38);
        line3.frame=CGRectMake(18, CGRectGetMaxY(txt_Last_Name.frame), WIDTH-46, 0.5);
        
        Date_Of_Birth.frame = CGRectMake(17, CGRectGetMaxY(line3.frame)+15, 100, 38);
        
        self.txt_DateBirth.frame = CGRectMake(CGRectGetMaxX(Date_Of_Birth.frame), CGRectGetMaxY(line3.frame)+15, WIDTH-100, 38);

        
        txt_Tel_Code  .frame=CGRectMake(15, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
        txt_Mobile_No   .frame=CGRectMake(80, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
        icon_dropdown.frame=CGRectMake(CGRectGetMaxX(txt_Tel_Code.frame)-295, CGRectGetMaxY(Date_Of_Birth.frame)+30, 15, 10);
        btn_on_drop_down.frame=CGRectMake(CGRectGetMaxX(txt_Tel_Code.frame)-330, CGRectGetMaxY(Date_Of_Birth.frame)+25, 60, 30);
        img_table_for_contry_code.frame  = CGRectMake(18,CGRectGetMaxY(txt_Mobile_No.frame)+2,120,90);
        line4.frame=CGRectMake(18, CGRectGetMaxY(txt_Tel_Code.frame), 50, 0.5);
        line5.frame=CGRectMake(CGRectGetMaxX(line4.frame)+10, CGRectGetMaxY(txt_Tel_Code.frame), WIDTH-106, 0.5);
        
        txt_Gmail .frame=CGRectMake(15, CGRectGetMaxY(line5.frame)+15, WIDTH-30, 38);
        line6.frame=CGRectMake(18, CGRectGetMaxY(txt_Gmail.frame), WIDTH-46, 0.5);
        
        page.frame = CGRectMake(WIDTH/2-40, CGRectGetMaxY(line6.frame)+105, WIDTH-80, 38);
        btn_Next.frame=CGRectMake(20,HEIGHT-90, WIDTH-42, 55);
        
        
        lbl_lines.frame = CGRectMake(CGRectGetMaxX(Date_Of_Birth.frame), CGRectGetMaxY(line3.frame)+27, 270,25);
        lbl_lines.font = [UIFont fontWithName:kFont size:23];
        
        
    }
    else if (IS_IPHONE_6)
    {
        scroll.frame = CGRectMake(5, 51, WIDTH-10, 490);
        
        user_img.frame = CGRectMake(120, CGRectGetMaxY(img_header.frame)-38, 120, 120);
        profileimg_button.frame=CGRectMake(0, 0, 120, 120);
        lbl_User_Info.frame = CGRectMake(120,CGRectGetMaxY(user_img.frame)+10,200, 15);
        
        txt_First_Name .frame=CGRectMake(20, CGRectGetMaxY(lbl_User_Info.frame)+25, WIDTH-80, 38);
        line.frame=CGRectMake(23, CGRectGetMaxY(txt_First_Name .frame), WIDTH-40, 0.5);
        
        txt_Middle_Name.frame=CGRectMake(20, CGRectGetMaxY(line.frame)+15, WIDTH-80, 38);
        line2.frame=CGRectMake(23, CGRectGetMaxY(txt_Middle_Name.frame), WIDTH-40, 0.5);
        lbl_optional.frame = CGRectMake(290,CGRectGetMaxY(line2.frame),100, 15);
        
        txt_Last_Name .frame=CGRectMake(20, CGRectGetMaxY(line2.frame)+15, WIDTH-80, 38);
        line3.frame=CGRectMake(23, CGRectGetMaxY(txt_Last_Name.frame), WIDTH-46, 0.5);
        
        Date_Of_Birth.frame = CGRectMake(20, CGRectGetMaxY(line3.frame)+15, WIDTH-280, 38);
        
         self.txt_DateBirth.frame = CGRectMake(CGRectGetMaxX(Date_Of_Birth.frame), CGRectGetMaxY(line3.frame)+15, WIDTH-100, 38);

        
        txt_Tel_Code  .frame=CGRectMake(23, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
        txt_Mobile_No   .frame=CGRectMake(88, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-95, 38);
        icon_dropdown.frame=CGRectMake(CGRectGetMaxX(txt_Tel_Code.frame)-253, CGRectGetMaxY(Date_Of_Birth.frame)+30, 15, 10);
        btn_on_drop_down.frame=CGRectMake(CGRectGetMaxX(txt_Tel_Code.frame)-253, CGRectGetMaxY(Date_Of_Birth.frame)+25, 60, 10);
        img_table_for_contry_code.frame  = CGRectMake(18,CGRectGetMaxY(txt_Mobile_No.frame)+2,120,75);
        
        line4.frame=CGRectMake(23, CGRectGetMaxY(txt_Tel_Code.frame), 56, 0.5);
        line5.frame=CGRectMake(CGRectGetMaxX(line4.frame)+12, CGRectGetMaxY(txt_Tel_Code.frame), WIDTH-116, 0.5);
        
        txt_Gmail .frame=CGRectMake(20, CGRectGetMaxY(line5.frame)+10, WIDTH-30, 38);
        line6.frame=CGRectMake(23, CGRectGetMaxY(txt_Gmail.frame), WIDTH-46, 0.5);
        
        page.frame = CGRectMake(WIDTH/2-40, CGRectGetMaxY(line6.frame)+75, WIDTH-80, 38);
        btn_Next.frame=CGRectMake(20,HEIGHT-70, WIDTH-42, 55);
        
        
         lbl_lines.frame = CGRectMake(CGRectGetMaxX(Date_Of_Birth.frame), CGRectGetMaxY(line3.frame)+30, 250,20);
          lbl_lines.font = [UIFont fontWithName:kFont size:20];
        
    }
    else if(IS_IPHONE_5)
    {
        scroll.frame = CGRectMake(5, 51, WIDTH-10, 400);
        
        user_img.frame = CGRectMake(95, CGRectGetMaxY(img_header.frame)-38, 120, 120);
        profileimg_button.frame=CGRectMake(0, 0, 120, 120);

        lbl_User_Info.frame = CGRectMake(90,CGRectGetMaxY(user_img.frame)+10,200, 15);
        
        txt_First_Name .frame=CGRectMake(20, CGRectGetMaxY(lbl_User_Info.frame)+15, WIDTH-80, 38);
        line.frame=CGRectMake(23, CGRectGetMaxY(txt_First_Name .frame), 270, 0.5);
        
        txt_Middle_Name.frame=CGRectMake(20, CGRectGetMaxY(line.frame)+15, WIDTH-80, 38);
        line2.frame=CGRectMake(23, CGRectGetMaxY(txt_Middle_Name.frame), 270, 0.5);
        lbl_optional.frame = CGRectMake(230,CGRectGetMaxY(line2.frame),100, 15);
        
        txt_Last_Name .frame=CGRectMake(20, CGRectGetMaxY(line2.frame)+15, WIDTH-80, 38);
        line3.frame=CGRectMake(23, CGRectGetMaxY(txt_Last_Name.frame), 270, 0.5);
        
        Date_Of_Birth.frame = CGRectMake(20, CGRectGetMaxY(line3.frame)+15, 100, 38);
        self.txt_DateBirth.frame = CGRectMake(CGRectGetMaxX(Date_Of_Birth.frame)-4, CGRectGetMaxY(line3.frame)+15, WIDTH-100, 38);
        

        
        txt_Tel_Code  .frame=CGRectMake(20, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
        txt_Mobile_No   .frame=CGRectMake(80, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
        icon_dropdown.frame=CGRectMake(CGRectGetMaxX(txt_Tel_Code.frame)-203, CGRectGetMaxY(Date_Of_Birth.frame)+30, 15, 10);
        img_table_for_contry_code.frame  = CGRectMake(18,CGRectGetMaxY(icon_dropdown.frame)+11,120,100);
        btn_on_drop_down.frame=CGRectMake(CGRectGetMaxX(txt_Tel_Code.frame)-203, CGRectGetMaxY(Date_Of_Birth.frame)+25, 50, 10);
        line4.frame=CGRectMake(23, CGRectGetMaxY(txt_Tel_Code.frame), 50, 0.5);
        line5.frame=CGRectMake(CGRectGetMaxX(line4.frame)+10, CGRectGetMaxY(txt_Tel_Code.frame), 210, 0.5);
        
        txt_Gmail .frame=CGRectMake(20, CGRectGetMaxY(line5.frame)+15, WIDTH-10, 38);
        line6.frame=CGRectMake(23, CGRectGetMaxY(txt_Gmail.frame), 270, 0.5);
        
        page.frame = CGRectMake(125, CGRectGetMaxY(line6.frame)-10, WIDTH-80, 38);
        btn_Next.frame=CGRectMake(15,HEIGHT-65, WIDTH-30, 50);
        
        txt_Gmail .font = [UIFont fontWithName:kFont size:13];
        [txt_Gmail  setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
        
        lbl_lines.frame = CGRectMake(CGRectGetMaxX(Date_Of_Birth.frame), CGRectGetMaxY(line3.frame)+30, 250,20);
        lbl_lines.font = [UIFont fontWithName:kFont size:15];
        
    }
    else
    {
        scroll.frame = CGRectMake(5, 51, WIDTH-10, 320);
        
        user_img.frame = CGRectMake(95, CGRectGetMaxY(img_header.frame)-38, 120, 120);
        profileimg_button.frame=CGRectMake(0, 0, 120, 120);

        lbl_User_Info.frame = CGRectMake(90,CGRectGetMaxY(user_img.frame)+10,200, 15);
        
        txt_First_Name .frame=CGRectMake(20, CGRectGetMaxY(lbl_User_Info.frame)+15, WIDTH-80, 38);
        line.frame=CGRectMake(23, CGRectGetMaxY(txt_First_Name .frame), 270, 0.5);
        
        txt_Middle_Name.frame=CGRectMake(20, CGRectGetMaxY(line.frame)+15, WIDTH-80, 38);
        line2.frame=CGRectMake(23, CGRectGetMaxY(txt_Middle_Name.frame), 270, 0.5);
        lbl_optional.frame = CGRectMake(230,CGRectGetMaxY(line2.frame),100, 15);
        
        txt_Last_Name .frame=CGRectMake(20, CGRectGetMaxY(line2.frame)+15, WIDTH-80, 38);
        line3.frame=CGRectMake(23, CGRectGetMaxY(txt_Last_Name.frame), 270, 0.5);
        
        Date_Of_Birth.frame = CGRectMake(20, CGRectGetMaxY(line3.frame)+15, 100, 38);
        self.txt_DateBirth.frame = CGRectMake(CGRectGetMaxX(Date_Of_Birth.frame)-5, CGRectGetMaxY(line3.frame)+15, WIDTH-100, 38);
        

        
        txt_Tel_Code  .frame=CGRectMake(20, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
        txt_Mobile_No   .frame=CGRectMake(80, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
        icon_dropdown.frame=CGRectMake(CGRectGetMaxX(txt_Tel_Code.frame)-203, CGRectGetMaxY(Date_Of_Birth.frame)+30, 15, 10);
        btn_on_drop_down.frame=CGRectMake(CGRectGetMaxX(txt_Tel_Code.frame)-203,CGRectGetMaxY(Date_Of_Birth.frame)+25,50, 10);
        img_table_for_contry_code.frame  = CGRectMake(18,CGRectGetMaxY(txt_Mobile_No.frame)+1,120,100);
        line4.frame=CGRectMake(23, CGRectGetMaxY(txt_Tel_Code.frame), 50, 0.5);
        line5.frame=CGRectMake(CGRectGetMaxX(line4.frame)+10, CGRectGetMaxY(txt_Tel_Code.frame), 210, 0.5);
        
        txt_Gmail .frame=CGRectMake(20, CGRectGetMaxY(line5.frame)+15, WIDTH-10, 38);
        line6.frame=CGRectMake(23, CGRectGetMaxY(txt_Gmail.frame), 270, 0.5);
        
        page.frame = CGRectMake(125, CGRectGetMaxY(line6.frame)-100, WIDTH-80, 38);
        btn_Next.frame=CGRectMake(15,HEIGHT-65, WIDTH-30, 50);
        
        txt_Gmail .font = [UIFont fontWithName:kFont size:13];
        [txt_Gmail  setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
        
        
        lbl_lines.frame = CGRectMake(CGRectGetMaxX(Date_Of_Birth.frame), CGRectGetMaxY(line3.frame)+30, 250,20);
        lbl_lines.font = [UIFont fontWithName:kFont size:15];

    }
    
    if (IS_IPHONE_6Plus)
    {
        [scroll setContentSize:CGSizeMake(0,CGRectGetMaxY(txt_Gmail.frame)+10)];
    }
    else if (IS_IPHONE_6)
    {
        [scroll setContentSize:CGSizeMake(0,CGRectGetMaxY(txt_Gmail.frame)+10)];
    }
    else if(IS_IPHONE_5)
    {
        [scroll setContentSize:CGSizeMake(0,CGRectGetMaxY(page.frame)+80)];
    }
    else{
        [scroll setContentSize:CGSizeMake(0,CGRectGetMaxY(page.frame)+80)];
    }
    
}
-(void)keyboard_returnmethod
{
    [txt_First_Name resignFirstResponder];
    [txt_Middle_Name resignFirstResponder];
    [txt_Last_Name resignFirstResponder];
    [txt_Date resignFirstResponder];
    [txt_Gmail resignFirstResponder];
    [txt_Mobile_No resignFirstResponder];
}

#pragma table view delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayCountry.count;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 25;
    
//    if (IS_IPHONE_6Plus)
//    {
//        return 28;
//        
//    }
//    else if (IS_IPHONE_6)
//    {
//        return 20;
//        
//    }
//    else if(IS_IPHONE_5)
//    {
//        return 35;
//        
//    }
//    else
//    {
//        return 20;
//    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews)
    {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    img_cellBackGnd.frame =  CGRectMake(0,0, 120, 25);
//    [img_cellBackGnd setImage:[UIImage imageNamed:@"img-white-bg1@2x.png"]];
    img_cellBackGnd.backgroundColor = [UIColor whiteColor];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    img_cellBackGnd.tag=100;
    [cell.contentView addSubview:img_cellBackGnd];
    
    UIImageView *img_bg = [[UIImageView alloc]init];
    img_bg.frame =  CGRectMake(0,0, 120, 25);
    img_bg.backgroundColor = [UIColor whiteColor];
    [img_bg setUserInteractionEnabled:YES];
    img_bg.tag=100;
    [img_cellBackGnd addSubview:img_bg];

    
    UILabel *cuntry_code = [[UILabel alloc]init];
    cuntry_code.frame = CGRectMake(0, 2, 120, 21);
    cuntry_code.text = [[arrayCountry objectAtIndex:indexPath.row] valueForKey:@"code"];
    cuntry_code.font = [UIFont fontWithName:kFont size:15];
    cuntry_code.textColor = [UIColor blackColor];
    cuntry_code.backgroundColor = [UIColor whiteColor];
    cuntry_code.textAlignment=NSTextAlignmentLeft;
    cuntry_code.tag=200;
    [img_bg addSubview:cuntry_code];
    

    
   /* UIImageView *img_cellBackGnd = (UIImageView *)[cell viewWithTag:100];
    
    UILabel *cuntry_code = (UILabel *)[cell viewWithTag:200];
    
    for (UIView *view in cell.contentView.subviews)
    {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor redColor];
    
    
    UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
    //[img_cellBackGnd setImage:[UIImage imageNamed:@"img-white-bg1@2x.png"]];
    img_cellBackGnd.backgroundColor = [UIColor purpleColor];
    [img_cellBackGnd setUserInteractionEnabled:YES];
    [cell.contentView addSubview:img_cellBackGnd];
    
    UILabel *cuntry_code = [[UILabel alloc]init];
    cuntry_code.text = [arrayCountry objectAtIndex:indexPath.row];
    cuntry_code.font = [UIFont fontWithName:kFont size:15];
    cuntry_code.textColor = [UIColor blackColor];
    cuntry_code.backgroundColor = [UIColor clearColor];
    cuntry_code.textAlignment=1;
    [ img_cellBackGnd addSubview:cuntry_code];
    
    
    
    
    if (IS_IPHONE_6Plus)
    {
        img_cellBackGnd.frame =  CGRectMake(0,3, 50, 30);
        cuntry_code.frame = CGRectMake(0, 5, 50, 20);
        
        // white_bg.frame =  CGRectMake(0,0, 40,30);
    }
    else if (IS_IPHONE_6)
    {
        img_cellBackGnd.frame =  CGRectMake(0,10, 50, 30);
        cuntry_code.frame = CGRectMake(0, 5, 50, 20);

        
    }
    else if(IS_IPHONE_5)
    {
        img_cellBackGnd.frame =  CGRectMake(0,3, 50, 60);
        cuntry_code.frame = CGRectMake(0, 5, 50, 20);

    }
    else
    {
        img_cellBackGnd.frame =  CGRectMake(0,3, 50, 30);
        cuntry_code.frame = CGRectMake(0, 5, 50, 20);

        
    }*/
    
    
    cell.backgroundColor = [UIColor whiteColor];
    cell.contentView.backgroundColor = [UIColor whiteColor];
    
    return cell;
    
}




-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"didSelectRowAtIndexPath");
    
    self.txt_Phone_Code.text = [[arrayCountry objectAtIndex:indexPath.row] valueForKey:@"code"];

    CGSize constraint1 = CGSizeMake(210 ,10000.0f);
    CGSize chatText_size1 = [self.txt_Phone_Code.text  sizeWithFont:[UIFont fontWithName:kFont size:15.0f] constrainedToSize:constraint1 lineBreakMode:NSLineBreakByWordWrapping];
    
    
    if (IS_IPHONE_6Plus)
    {
        txt_Tel_Code  .frame=CGRectMake(15, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
        txt_Mobile_No   .frame=CGRectMake(chatText_size1.width+40, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-chatText_size1.width+40, 38);
        icon_dropdown.frame=CGRectMake(chatText_size1.width+20, CGRectGetMaxY(Date_Of_Birth.frame)+30, 15, 10);
        btn_on_drop_down.frame=CGRectMake(chatText_size1.width+10, CGRectGetMaxY(Date_Of_Birth.frame)+25, 60, 30);
        img_table_for_contry_code.frame  = CGRectMake(18,CGRectGetMaxY(txt_Mobile_No.frame)+2,120,90);
        line4.frame=CGRectMake(18, CGRectGetMaxY(txt_Tel_Code.frame), chatText_size1.width, 0.5);
        line5.frame=CGRectMake(CGRectGetMaxX(line4.frame)+10, CGRectGetMaxY(txt_Tel_Code.frame), WIDTH-106, 0.5);
       
       
        
    }
        else if (IS_IPHONE_6)
        {
            txt_Tel_Code.frame=CGRectMake(23, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
            txt_Mobile_No.frame=CGRectMake(chatText_size1.width+40, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-chatText_size1.width+40, 38);
            icon_dropdown.frame=CGRectMake(chatText_size1.width+20, CGRectGetMaxY(Date_Of_Birth.frame)+30, 15, 10);
            btn_on_drop_down.frame=CGRectMake(chatText_size1.width+10, CGRectGetMaxY(Date_Of_Birth.frame)+25, 60, 10);
            img_table_for_contry_code.frame  = CGRectMake(18,CGRectGetMaxY(txt_Mobile_No.frame)+2,120,75);
            line4.frame=CGRectMake(23, CGRectGetMaxY(txt_Tel_Code.frame), chatText_size1.width, 0.5);
            line5.frame=CGRectMake(CGRectGetMaxX(line4.frame)+12, CGRectGetMaxY(txt_Tel_Code.frame), WIDTH-116, 0.5);

        }
        else if(IS_IPHONE_5)
        {
            txt_Tel_Code  .frame=CGRectMake(20, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
            txt_Mobile_No   .frame=CGRectMake(chatText_size1.width+40, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-chatText_size1.width+40, 38);
            icon_dropdown.frame=CGRectMake(chatText_size1.width+20, CGRectGetMaxY(Date_Of_Birth.frame)+30, 15, 10);
            img_table_for_contry_code.frame  = CGRectMake(18,CGRectGetMaxY(icon_dropdown.frame)+11,120,100);
            btn_on_drop_down.frame=CGRectMake(chatText_size1.width+10, CGRectGetMaxY(Date_Of_Birth.frame)+25, 50, 10);
            line4.frame=CGRectMake(23, CGRectGetMaxY(txt_Tel_Code.frame), chatText_size1.width, 0.5);
            line5.frame=CGRectMake(CGRectGetMaxX(line4.frame)+10, CGRectGetMaxY(txt_Tel_Code.frame), 210, 0.5);
            
        }
        else
        {
            txt_Tel_Code  .frame=CGRectMake(20, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
            txt_Mobile_No   .frame=CGRectMake(chatText_size1.width+40, CGRectGetMaxY(Date_Of_Birth.frame)+15, WIDTH-80, 38);
            icon_dropdown.frame=CGRectMake(chatText_size1.width+20, CGRectGetMaxY(Date_Of_Birth.frame)+30, 15, 10);
            btn_on_drop_down.frame=CGRectMake(chatText_size1.width+10,CGRectGetMaxY(Date_Of_Birth.frame)+25,50, 10);
            img_table_for_contry_code.frame  = CGRectMake(18,CGRectGetMaxY(txt_Mobile_No.frame)+1,120,100);
            line4.frame=CGRectMake(23, CGRectGetMaxY(txt_Tel_Code.frame), chatText_size1.width, 0.5);
            line5.frame=CGRectMake(CGRectGetMaxX(line4.frame)+10, CGRectGetMaxY(txt_Tel_Code.frame), 210, 0.5);

        }
    
    //if (chatText_size1.width >150)

    
    str_countrycodetosend =[[arrayCountry objectAtIndex:indexPath.row] valueForKey:@"id"];
    self.txt_Phone_Code.textColor = [UIColor blackColor];
    img_table_for_contry_code.hidden = YES;
}

#pragma mark Validate Email

-(BOOL)ValidateEmail :(NSString *) strToEvaluate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL b = [emailTest evaluateWithObject:strToEvaluate];
    return b;
}
- (NSMutableArray *)validateEmailWithString:(NSString*)emails
{
    NSMutableArray *validEmails = [[NSMutableArray alloc] init];
    NSArray *emailArray = [emails componentsSeparatedByString:@","];
    for (NSString *email in emailArray)
    {
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        if ([emailTest evaluateWithObject:email])
            [validEmails addObject:email];
    }
    return validEmails;
}


-(void)btn_back_click:(UIButton *)sender
{
    NSLog(@"btn_back_click:");
    UserSignUpVC *vc = [[UserSignUpVC alloc]init];
    [self dismissViewControllerAnimated:vc completion:nil];
//    [self.navigationController popViewControllerAnimated:NO];
    
}



/*-(void)user_img_click:(UIButton *)sender
{
    NSLog(@"user_img_click:");
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Add Photo From " delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera", @"Photo", nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    actionSheet.backgroundColor = [UIColor clearColor];
    actionSheet.frame=CGRectMake(8, 90, 304, 216);
    
    // actionSheet.alpha=0.90;
    actionSheet.tag = 1000;
    [actionSheet showInView:self.view];
    
    
}*/

-(void)profilepic_clickbtn:(UIButton *) sender
{
    str_pickingImage = @"Profile";
    
    UIActionSheet *actionSheet_popupQuery = [[UIActionSheet alloc] initWithTitle:@"Select A Image From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Camera", nil];
    actionSheet_popupQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet_popupQuery showInView:self.view];
    
}

#pragma mark - ACTION SHEET DELEGATE

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 0)
    {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            UIImagePickerController*imgPicker = [[UIImagePickerController alloc] init];
            UIColor* color = [UIColor colorWithRed:46.0/255 green:127.0/255 blue:244.0/255 alpha:1];
            [imgPicker.navigationBar setTintColor:color];
            imgPicker.delegate = self;
            imgPicker.allowsEditing = YES;
            imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            [self presentViewController:imgPicker animated:NO completion:Nil];
        }
    }
    else if(buttonIndex == 1)
    {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController*imgPicker = [[UIImagePickerController alloc] init];
            UIColor* color = [UIColor colorWithRed:46.0/255 green:127.0/255 blue:244.0/255 alpha:1];
            [imgPicker.navigationBar setTintColor:color];
            imgPicker.delegate = self;
            imgPicker.allowsEditing = YES;
            imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:imgPicker animated:NO completion:Nil];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Not86" message:@"Device Does Not Support Camera" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
}

#pragma mark - IMAGEPICKER DELEGATE METHODS
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *imageOriginal =  [info objectForKey:UIImagePickerControllerEditedImage];
    
    if ( [[NSString stringWithFormat:@"%@",str_pickingImage]isEqualToString:@"Profile"])
    {
        imgData =  UIImageJPEGRepresentation(imageOriginal,0.5);
        user_img.image = imageOriginal;
        
    }
    
    [picker dismissViewControllerAnimated:YES completion:Nil];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:Nil];
}





-(void)icon_drop_down_click:(UIButton *)sender
{
    NSLog(@"icon_drop_down_click:");
    [self keyboard_returnmethod];
    
    if ([img_table_for_contry_code isHidden])
    {
        [sender setSelected:YES];
        img_table_for_contry_code.hidden=NO;
        
    }
    else
    {
        [sender setSelected:NO];
        img_table_for_contry_code.hidden=YES;
    }

    
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //[self countryList];
    });
    
}
-(void)btn_next_click:(UIButton *)sender
{
    NSLog(@"btn_nextclick:");
//    UserRegistration2VC*vc = [[UserRegistration2VC alloc]init];
//    [self  presentViewController:vc animated:NO completion:nil];
    
//        [self.navigationController pushViewController:vc animated:NO];
    
    
//    NSDate *selectedDate =  self.datePicker.date;
//    NSDate* now = [NSDate date];
//    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
//                                       components:NSYearCalendarUnit
//                                       fromDate:selectedDate
//                                       toDate:now
//                                       options:0];
//    NSInteger age = [ageComponents year];
//
    
    
    if (self.txt_FirstName.text.length == 0)
    {
        
        [self popup_Alertview:@"please enter First name"];
        
    }
//    else if (![self Validateusername])
//    {
//        [self popup_Alertview:@"should be alpha numeric"];
//    }

    else if (imgData== nil)
    {
        [self popup_Alertview:@"Please select User Profile Pic"];
    }
   else if (self.txt_LastName.text.length == 0){
        
        [self popup_Alertview:@"please enter last name"];
        
    }else if (self.txt_Phone_Code.text.length == 0){
        
         [self popup_Alertview:@"please enter country code"];
        
        
    }
    else if (self.txt_MobileNo.text.length == 0)
    {
        
        [self popup_Alertview:@" please enter mobile number"];

        
    }
    else if ((_txt_MobileNo.text.length>0&&[_txt_MobileNo.text length]<10 ))
            {
                _txt_MobileNo.text=@"";
                [self popup_Alertview:@"Please enter valid mobile number"];
            }
    else if (self.txt_DateBirth.text.length == 0){
        
        [self popup_Alertview:@"please select date of birth"];
        
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"date of birth required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alertView show];
    }
    else if (_txt_Email.text.length==0)
    {
        [self popup_Alertview:@"Please enter email address"];
    }
    else if(_txt_Email.text.length>0 && ![self ValidateEmail:_txt_Email.text])
    {
        _txt_Email.text=@"";
        [self popup_Alertview:@"Please Enter valid email address"];
        _txt_Email.text = @"";
    }
   

    
    else{
        
        
        [self SignUpOne];

        
//        if (age < 18) {
//            
////            [self popup_Alertview:@"You must be 18 years of age"];
//
////            
////            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"" message:@"You must be 18 years of age" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
////            [alertView show];
//        }
////        else if (_txt_Email.text.length==0)
////        {
////            [self popup_Alertview:@"Please enter email address"];
////        }
////        else if(_txt_Email.text.length>0 && ![self ValidateEmail:_txt_Email.text])
////        {
////            _txt_Email.text=@"";
////            [self popup_Alertview:@"Please Enter valid email address"];
////            _txt_Email.text = @"";
////        }
//       else{
//            NSLog(@"Sign uPpp");
//            [self SignUpOne];
//
//        }
    }
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}

-(BOOL)Validateusername
{
    [self.view endEditing:YES];
    BOOL letterCharacter = 0;
    BOOL numberCharacter = 0;
    if([self.txt_FirstName .text length] >= 4)
    {
        for (int i = 0; i < [self.txt_FirstName .text length]; i++)
        {
            unichar c = [self.txt_FirstName .text characterAtIndex:i];
            
            if(!letterCharacter)
            {
                letterCharacter = [[NSCharacterSet letterCharacterSet] characterIsMember:c];
            }
            if(!numberCharacter)
            {
                NSString *numberCharacterString = @"0123456789'";
                numberCharacter = [[NSCharacterSet characterSetWithCharactersInString:numberCharacterString] characterIsMember:c];
            }
        }
        if(letterCharacter && numberCharacter)
        {
            return YES;
        }
        else
        {
            self.txt_FirstName.text = @"";
            
            [self popup_Alertview:@"Password should be alphanumeric and minimum eight characters"];
            
        }
    }
    else
    {
        self.txt_FirstName .text = @"";
        [self popup_Alertview:@"Password should be alphanumeric and minimum eight characters"];
    }
    
    return NO;
}
#pragma mark TextField Delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //    if(textField == txt_username)
    //    {
    //        if ([string isEqualToString:@" "])
    //        {
    //            return NO;
    //        }
    //    }
    
    if (textField==_txt_MobileNo)
    {
        NSString *stringVal = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered =[[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        if (stringVal.length<11 && [string isEqualToString:filtered])
        {
            return YES;
        }
        else{
            return NO;
        }
        return [string isEqualToString:filtered];
    }
    else if (textField==_txt_Email)
    {
        textField.text = [_txt_Email.text stringByReplacingCharactersInRange:range withString:[string lowercaseString]];
        return NO;
    }
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    img_table_for_contry_code.hidden=YES;
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFontBold size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}



#pragma mark - method Sign Up
-(void)SignUpOne
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    NSString *firstName = self.txt_FirstName.text;
    NSString *middleName = self.txt_MiddleName.text;
    NSString *lastName = self.txt_LastName.text;
    NSString *mobileCodeNo = str_countrycodetosend;
    NSString *mobileNo = self.txt_MobileNo.text;
    NSString *emailAddress = self.txt_Email.text;
    
    [formatter setDateFormat:@"yyyy/MM/dd"];
    //    @"d  d   m  m   y  y  y  y"
    NSString *str=[formatter stringFromDate:datePicker.date];
    


   // NSString *dateOfBirth = self.txt_DateBirth.text;
    
    
    NSDictionary *params =@{
                            
                            @"uid"                       : [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"registration_page"         : @"1",
                            @"field_first_name"          : firstName,
                            @"field_middle_name"         : middleName,
                            @"field_last_name"           : lastName,
                            @"field_mobile_code_number"  : mobileCodeNo,
                            @"field_mobile_number"       : mobileNo,
                            @"field_email_address"       : emailAddress,
                            @"field_date_of_birth"       : str,
                            @"role_type"                 : @"user_profile",

                            
                            
                            };
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    
    //===============================SIMPLE REQUEST
    
    //    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
    //                                                            path:@"add-user-profile.json"
    //                                                      parameters:params];
    //
    //    NSLog(@"request =%@",request);
    //
    
    
    NSMutableURLRequest *request;
    
    if (imageData != nil)
    {
        
        request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"webservices/add-user-profile.json"  parameters:params constructingBodyWithBlock:^(id <AFMultipartFormData>formData)
                   {
                       NSTimeInterval timeInterval = [NSDate timeIntervalSinceReferenceDate];
                       [formData appendPartWithFileData:imageData name:@"profile_img" fileName:[NSString stringWithFormat:@"%lf-Userimage.png",timeInterval] mimeType:@"image/jpeg"];
                       
                   }];
    }
    
    else{
        request = [httpClient requestWithMethod:@"POST" path:@"webservices/add-user-profile.json"  parameters:params];
        
    }
    NSLog(@"request:%@",request);
    
    //====================================================RESPONSE
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        NSLog(@"JSON = %@",JSON);
        [delegate.activityIndicator stopAnimating];
        
        [self ResponseSignUpOne:JSON];
        
    }
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         [self.view setUserInteractionEnabled:YES];
                                         if([operation.response statusCode] == 406){
                                             
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             
                                             
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                         }
                                     }];
    [operation start];
    
}


-(void)ResponseSignUpOne:(NSDictionary * )TheDict
{
    NSLog(@"ResponseDefault: %@",TheDict);
    
    [self.view setUserInteractionEnabled:YES];
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        UserRegistration2VC*vc = [[UserRegistration2VC alloc]init];
        [self  presentViewController:vc animated:NO completion:nil];
        //            NSString *msg = [TheDict objectForKey:@"message"];
        dispatch_async(dispatch_get_main_queue(), ^{
        });
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        NSString *msg = [TheDict objectForKey:@"message"];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        
    }
}






# pragma mark Hcountry method

-(void)countryList
{
    
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
   
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpHcountry
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [self ResponseChefSignUpHcountry:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self countryList];
                                         }
                                     }];
    [operation start];
    
}

-(void) ResponseChefSignUpHcountry :(NSDictionary * )TheDict
{
    [arrayCountry removeAllObjects];
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"CountryMobileCode"] count]; i++)
        {
            [arrayCountry addObject:[[TheDict valueForKey:@"CountryMobileCode"] objectAtIndex:i]];
            
        }
        [img_table_for_contry_code reloadData];
       

        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
          self.txt_Phone_Code.text=@"";
        
        
    }
    
    [img_table_for_contry_code reloadData];
    
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
