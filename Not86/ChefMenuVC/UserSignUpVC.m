//
//  UserSignUpVC.m
//  Not86
//
//  Created by Admin on 29/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "UserSignUpVC.h"
#import "Define.h"
#import "UserRegistration1VC.h"
#import "AppDelegate.h"

//#import "SignupViewController.h"
//#import "UserRegitrationStep1VC.h"


#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"


@interface UserSignUpVC ()<UITextFieldDelegate>
{
    UIImageView * img_header;
    UIImageView *img_background;
    CGFloat	animatedDistance;
    AppDelegate *delegate;

    NSString *strFBIDs,*strFacebookName;
    UIView*alertviewBg;
    
    UIImageView *  imgView_PasswordStatus;
    UIImageView * imgView_PasswordStatus2;
    NSString*str_showsucess;
    UITextField *txt_call_referal_id;
    UIToolbar*keyboardToolbar_Date;
    
    
    

}

@end

@implementation UserSignUpVC
@synthesize array_Facebook_Details;

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"ArrayFacebookDetail =%@",self.array_Facebook_Details);
    str_showsucess = [NSString new];
   
    
    
//    UIDevice *myDevice=[UIDevice currentDevice];
//    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
//    NSString *device_token = appDelegate().devicestr;
//    
    
//    NSLog(@"UniqueAppID =%@",UniqueAppID);
//    NSLog(@"device_token =%@",device_token);
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    

//
    
    
    for (NSDictionary *dict in self.array_Facebook_Details) {
        NSString *fbID = [dict objectForKey:@"id"];
        NSString *name = [dict objectForKey:@"name"];
        
        strFBIDs = fbID;
        strFacebookName = name;

    }
    
    // Do any additional setup after loading the view.
    [self integrateHeader];
    [self integrateBodyDesign];
    
    
    
}


-(void)viewDidDisappear:(BOOL)animated
{
    str_showsucess = @"NO";
    
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}


-(void)integrateHeader
{
    img_header=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH,45)];
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,15,15,15);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back addTarget:self action:@selector(btn_back_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_back];
    
    UILabel *lbl_Login = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(icon_back.frame)+30,0, 100, 45)];
    lbl_Login.text = @"Sign Up";
    lbl_Login.font = [UIFont fontWithName:kFont size:18];
    lbl_Login.textColor = [UIColor whiteColor];
    lbl_Login.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_Login];
    
    UIImageView *img_circle = [[UIImageView alloc]init];
    img_circle.frame =CGRectMake(WIDTH-40, 8, 30, 30);
    [img_circle setImage:[UIImage imageNamed:@"img_user_icon@2x.png"]];
    //img_circle.backgroundColor = [UIColor whiteColor];
    [img_circle setUserInteractionEnabled:YES];
    [img_header addSubview:img_circle];
    
    
}

-(void)integrateBodyDesign
{
    img_background = [[UIImageView alloc]init];
    //  img_background.frame = CGRectMake(30, CGRectGetMaxY(img_header.frame)+30, WIDTH-50, 85);
    [img_background setImage:[UIImage imageNamed:@"img_logo_name@2x.png"]];
    //    img_eat.backgroundColor = [UIColor redColor];
    [img_background setUserInteractionEnabled:YES];
    [self.view addSubview:img_background];
    
    
    
    UITextField *txt_user_Name = [[UITextField alloc] init];
    //   txt_user_Name .frame=CGRectMake(10, CGRectGetMaxY( img_background.frame)-20, WIDTH, 30);
    txt_user_Name .borderStyle = UITextBorderStyleNone;
    txt_user_Name .textColor = [UIColor blackColor];
    txt_user_Name .font = [UIFont fontWithName:kFont size:13];
    txt_user_Name .placeholder = @"Username";
    if ([[NSString stringWithFormat:@"%@",delegate.login_type]isEqualToString:@"FACEBOOK"])
    {
        txt_user_Name.text = [[array_Facebook_Details objectAtIndex:0] valueForKey:@"name"];
    }
    
    [txt_user_Name  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_user_Name  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_user_Name .leftView = padding1;
    txt_user_Name .leftViewMode = UITextFieldViewModeAlways;
    txt_user_Name .userInteractionEnabled=YES;
    txt_user_Name .textAlignment = NSTextAlignmentLeft;
    txt_user_Name .backgroundColor = [UIColor clearColor];
    txt_user_Name .keyboardType = UIKeyboardTypeAlphabet;
    txt_user_Name .delegate = self;
    [self.view addSubview:txt_user_Name ];
    self.txt_Username = txt_user_Name;
    
    UIImageView * img_line = [[UIImageView alloc]init];
    //   img_line .frame = CGRectMake(10, CGRectGetMaxY(  txt_user_Name.frame)+5, WIDTH-60, 0.5);
    [img_line setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_line setUserInteractionEnabled:YES];
    [self.view addSubview:img_line];
    
    UITextField *txt_password = [[UITextField alloc] init];
    //    txt_password .frame=CGRectMake(10, CGRectGetMaxY( img_line.frame)+10, WIDTH, 30);
    txt_password .borderStyle = UITextBorderStyleNone;
    txt_password .textColor = [UIColor blackColor];
    txt_password .font = [UIFont fontWithName:kFont size:13];
    txt_password .placeholder = @"Password";
    [txt_password  setValue:[UIFont fontWithName:kFont size:16] forKeyPath:@"_placeholderLabel.font"];
    [txt_password  setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_password .leftView = padding2;
    txt_password .leftViewMode = UITextFieldViewModeAlways;
    txt_password .userInteractionEnabled=YES;
    txt_password .textAlignment = NSTextAlignmentLeft;
    txt_password .backgroundColor = [UIColor clearColor];
    txt_password .keyboardType = UIKeyboardTypeAlphabet;
    txt_password.secureTextEntry = YES;
    txt_password .delegate = self;
    [self.view addSubview:txt_password ];
    self.txt_Password = txt_password;
    
//    UIImageView * img_cross = [[UIImageView alloc]init];
//    //   img_cross .frame = CGRectMake(235,CGRectGetMaxY(img_line.frame)+15, 25, 25);
//    [img_cross setImage:[UIImage imageNamed:@"icon-cross@2x.png"]];
//    //img_background.backgroundColor = [UIColor redColor];
//    [img_cross setUserInteractionEnabled:YES];
//    [self.view addSubview:img_cross];
    
    imgView_PasswordStatus = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        imgView_PasswordStatus.frame = CGRectMake(CGRectGetMaxX(txt_password.frame)+2,CGRectGetMaxY(img_line.frame)+35,20,20);
    }
    else if (IS_IPHONE_6)
    {
        imgView_PasswordStatus.frame = CGRectMake(CGRectGetMaxX(txt_password.frame)+2,CGRectGetMaxY(img_line.frame)+35,20,20);
        
    }
    else
    {
        imgView_PasswordStatus.frame = CGRectMake(CGRectGetMaxX(txt_password.frame)+2,CGRectGetMaxY(img_line.frame)+35,20,20);
        
    }
    
    [imgView_PasswordStatus setImage:[UIImage imageNamed:@""]];
    imgView_PasswordStatus.backgroundColor = [UIColor clearColor];
    imgView_PasswordStatus.userInteractionEnabled=YES;
    [self.view addSubview:imgView_PasswordStatus];

    
    UIImageView * img_line1 = [[UIImageView alloc]init];
    //  img_line1 .frame = CGRectMake(10, CGRectGetMaxY(  txt_password.frame)+5, WIDTH-60, 0.5);
    [img_line1 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_line1 setUserInteractionEnabled:YES];
    [self.view addSubview:img_line1];
    
    
    UITextField *txt_conform_password = [[UITextField alloc] init];
    //    txt_conform_password .frame=CGRectMake(10, CGRectGetMaxY( img_line1.frame)+10, WIDTH, 30);
    txt_conform_password .borderStyle = UITextBorderStyleNone;
    txt_conform_password .textColor = [UIColor blackColor];
    txt_conform_password .font = [UIFont fontWithName:kFont size:13];
    txt_conform_password .placeholder = @"Confirm Password";
    [txt_conform_password  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_conform_password   setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_conform_password .leftView = padding3;
    txt_conform_password .leftViewMode = UITextFieldViewModeAlways;
    txt_conform_password .userInteractionEnabled=YES;
    txt_conform_password.secureTextEntry = YES;
    txt_conform_password .textAlignment = NSTextAlignmentLeft;
    txt_conform_password .backgroundColor = [UIColor clearColor];
    txt_conform_password .keyboardType = UIKeyboardTypeAlphabet;
    txt_conform_password .delegate = self;
    
    [self.view addSubview:txt_conform_password ];
    self.txt_ConfirmPassword = txt_conform_password;

    
//    UIImageView * img_currect = [[UIImageView alloc]init];
//    //   img_currect .frame = CGRectMake(235,CGRectGetMaxY(img_line1.frame)+15, 25, 25);
//    [img_currect setImage:[UIImage imageNamed:@"icon-currect@2x.png"]];
//    //img_background.backgroundColor = [UIColor redColor];
//    [img_currect setUserInteractionEnabled:YES];
//    [self.view addSubview:img_currect];
    imgView_PasswordStatus2 = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        imgView_PasswordStatus2.frame = CGRectMake(CGRectGetMaxX(txt_password.frame)+2,CGRectGetMaxY(img_line.frame)+35,20,20);
    }
    else if (IS_IPHONE_6)
    {
        imgView_PasswordStatus2.frame = CGRectMake(CGRectGetMaxX(txt_password.frame)+2,CGRectGetMaxY(img_line.frame)+35,20,20);
        
    }
    else
    {
        imgView_PasswordStatus2.frame = CGRectMake(CGRectGetMaxX(txt_password.frame)+2,CGRectGetMaxY(img_line.frame)+35,20,20);
        
    }
    
    [imgView_PasswordStatus2 setImage:[UIImage imageNamed:@""]];
    imgView_PasswordStatus2.backgroundColor = [UIColor clearColor];
    imgView_PasswordStatus2.userInteractionEnabled=YES;
    [self.view addSubview:imgView_PasswordStatus2];

    
    
    UIImageView * img_line2 = [[UIImageView alloc]init];
    //    img_line2 .frame = CGRectMake(10, CGRectGetMaxY(  txt_conform_password.frame)+5, WIDTH-60, 0.5);
    [img_line2 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_line2 setUserInteractionEnabled:YES];
    [self.view addSubview:img_line2];
    
   txt_call_referal_id = [[UITextField alloc] init];
    txt_call_referal_id .frame=CGRectMake(10, CGRectGetMaxY( img_line2.frame)+10, WIDTH, 30);
    txt_call_referal_id .borderStyle = UITextBorderStyleNone;
    txt_call_referal_id .textColor = [UIColor blackColor];
    txt_call_referal_id .font = [UIFont fontWithName:kFont size:13];
    txt_call_referal_id .placeholder = @"Call referral ID";
    [txt_call_referal_id  setValue:[UIFont fontWithName:kFont size: 16] forKeyPath:@"_placeholderLabel.font"];
    [txt_call_referal_id   setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *padding4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
    txt_call_referal_id .leftView = padding4;
    txt_call_referal_id .leftViewMode = UITextFieldViewModeAlways;
    txt_call_referal_id .userInteractionEnabled=YES;
    txt_call_referal_id .textAlignment = NSTextAlignmentLeft;
    txt_call_referal_id .backgroundColor = [UIColor clearColor];
    txt_call_referal_id .keyboardType = UIKeyboardTypeNumberPad;
    txt_call_referal_id .delegate = self;
    [self.view addSubview:txt_call_referal_id ];
    
    if (keyboardToolbar_Date == nil)
    {
        keyboardToolbar_Date = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, IS_IPHONE_5?45:45)];
        [keyboardToolbar_Date setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_Done)];
        [keyboardToolbar_Date setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    txt_call_referal_id.inputAccessoryView = keyboardToolbar_Date;

    
    UIImageView * img_line3 = [[UIImageView alloc]init];
    img_line3 .frame = CGRectMake(10, CGRectGetMaxY(txt_call_referal_id.frame)+5, WIDTH-60, 0.5);
    [img_line3 setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //img_line3.backgroundColor = [UIColor redColor];
    [img_line3 setUserInteractionEnabled:YES];
    [self.view addSubview:img_line3];

    
    UIButton *btn_sign_up = [UIButton buttonWithType:UIButtonTypeCustom];
    //    btn_sign_up.frame = CGRectMake(10,CGRectGetMaxY(  img_line2.frame)+30,WIDTH-55,50);
    btn_sign_up .backgroundColor = [UIColor clearColor];
    [btn_sign_up setImage:[UIImage imageNamed:@"img-signup@2x.png"] forState:UIControlStateNormal];
    
    [btn_sign_up addTarget:self action:@selector(btn_sign_up_click:) forControlEvents:UIControlEventTouchUpInside];
    btn_sign_up.userInteractionEnabled = YES;
    [self.view   addSubview:btn_sign_up];
    
    
    
    if (IS_IPHONE_6Plus)
    {
        img_background.frame = CGRectMake(40, CGRectGetMaxY(img_header.frame)+30, WIDTH-130, 85);
        txt_user_Name .frame=CGRectMake(35, CGRectGetMaxY( img_background.frame)+50, WIDTH-40, 30);
        img_line .frame = CGRectMake(40, CGRectGetMaxY(  txt_user_Name.frame)+5, WIDTH-80, 0.5);
        txt_password .frame=CGRectMake(35, CGRectGetMaxY( img_line.frame)+20, WIDTH-100, 30);
        imgView_PasswordStatus.frame = CGRectMake(CGRectGetMaxX(txt_password.frame)+2,CGRectGetMaxY(img_line.frame)+25,20,20);
        img_line1 .frame = CGRectMake(40, CGRectGetMaxY(  txt_password.frame)+5, WIDTH-80, 0.5);
        txt_conform_password .frame=CGRectMake(35, CGRectGetMaxY( img_line1.frame)+20, WIDTH-100, 30);
        imgView_PasswordStatus2.frame = CGRectMake(CGRectGetMaxX(txt_password.frame)+2,CGRectGetMaxY(img_line1.frame)+25,20,20);
        //img_line2 .frame = CGRectMake(40, CGRectGetMaxY(  txt_conform_password.frame)+5, WIDTH-80, 0.5);
        img_line2 .frame = CGRectMake(40, CGRectGetMaxY(  txt_conform_password.frame)+5, WIDTH-80, 0.5);
        
        txt_call_referal_id .frame=CGRectMake(35, CGRectGetMaxY( img_line2.frame)+20, WIDTH, 30);
        img_line3 .frame = CGRectMake(40, CGRectGetMaxY(txt_call_referal_id.frame)+5, WIDTH-80, 0.5);
        btn_sign_up.frame = CGRectMake(37,CGRectGetMaxY(  img_line3.frame)+35,WIDTH-74,55);
        
    }
    else if (IS_IPHONE_6)
    {
        img_background.frame = CGRectMake(30, CGRectGetMaxY(img_header.frame)+30, WIDTH-80, 90);
        txt_user_Name .frame=CGRectMake(35, CGRectGetMaxY( img_background.frame)+50, WIDTH-40, 30);
        img_line .frame = CGRectMake(40, CGRectGetMaxY(  txt_user_Name.frame)+5, WIDTH-80, 0.5);
        txt_password .frame=CGRectMake(35, CGRectGetMaxY( img_line.frame)+20, WIDTH-100, 30);
        imgView_PasswordStatus.frame = CGRectMake(CGRectGetMaxX(txt_password.frame)+2,CGRectGetMaxY(img_line.frame)+25,20,20);
        img_line1 .frame = CGRectMake(40, CGRectGetMaxY(  txt_password.frame)+5, WIDTH-80, 0.5);
        txt_conform_password .frame=CGRectMake(35, CGRectGetMaxY( img_line1.frame)+20, WIDTH-100, 30);
        imgView_PasswordStatus2.frame = CGRectMake(CGRectGetMaxX(txt_password.frame)+2,CGRectGetMaxY(img_line1.frame)+25,20,20);
       // img_line2 .frame = CGRectMake(40, CGRectGetMaxY(  txt_conform_password.frame)+5, WIDTH-80, 0.5);
        img_line2 .frame = CGRectMake(40, CGRectGetMaxY(  txt_conform_password.frame)+5, WIDTH-80, 0.5);
        
        txt_call_referal_id .frame=CGRectMake(35, CGRectGetMaxY( img_line2.frame)+20, WIDTH, 30);
        img_line3 .frame = CGRectMake(40, CGRectGetMaxY(txt_call_referal_id.frame)+5, WIDTH-80, 0.5);
        btn_sign_up.frame = CGRectMake(37,CGRectGetMaxY(  img_line3.frame)+35,WIDTH-74,55);
        
        
    }
    else
    {
        img_background.frame = CGRectMake(30, CGRectGetMaxY(img_header.frame)+30, WIDTH-80, 70);
        txt_user_Name .frame=CGRectMake(35, CGRectGetMaxY( img_background.frame)+50, WIDTH-40, 30);
        img_line .frame = CGRectMake(40, CGRectGetMaxY(  txt_user_Name.frame)+5, WIDTH-80, 0.5);
        txt_password .frame=CGRectMake(35, CGRectGetMaxY( img_line.frame)+20, WIDTH-100, 30);
        imgView_PasswordStatus.frame = CGRectMake(CGRectGetMaxX(txt_password.frame)+2,CGRectGetMaxY(img_line.frame)+25,20,20);
        img_line1 .frame = CGRectMake(40, CGRectGetMaxY(  txt_password.frame)+5, WIDTH-80, 0.5);
        txt_conform_password .frame=CGRectMake(35, CGRectGetMaxY( img_line1.frame)+20, WIDTH-100, 30);
        imgView_PasswordStatus2.frame = CGRectMake(CGRectGetMaxX(txt_password.frame)+2,CGRectGetMaxY(img_line1.frame)+25,20,20);
       // img_line2 .frame = CGRectMake(40, CGRectGetMaxY(  txt_conform_password.frame)+5, WIDTH-80, 0.5);
        img_line2 .frame = CGRectMake(40, CGRectGetMaxY(  txt_conform_password.frame)+5, WIDTH-80, 0.5);
        
        txt_call_referal_id .frame=CGRectMake(35, CGRectGetMaxY( img_line2.frame)+20, WIDTH, 30);
        img_line3 .frame = CGRectMake(40, CGRectGetMaxY(txt_call_referal_id.frame)+5, WIDTH-80, 0.5);
        btn_sign_up.frame = CGRectMake(37,CGRectGetMaxY(  img_line3.frame)+35,WIDTH-74,50);
        
        
    }
    
}

-(void)click_Done
{
    [txt_call_referal_id resignFirstResponder];
    
}
#pragma mark Click Events


-(void)SignUpWithFacebook{
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            
                            @"device_udid"           : UniqueAppID,
                            @"device_type"           : @"1",
                            @"device_token"           : str_device_token,
                            @"role_type"           : @"user_profile",
                            @"username"           : strFacebookName,
                            @"fbid"           : strFBIDs,
                            @"push_notificaiton"           : @"1",


                            };
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:@"webservices/fb-login.json"
                                                      parameters:params];
    
    NSLog(@"request =%@",request);
    
    //====================================================RESPONSE
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        NSLog(@"JSON = %@",JSON);
        
        [self ResponsefbSignUp:JSON];
        
    }
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         [self.view setUserInteractionEnabled:YES];
                                         if([operation.response statusCode] == 406){
                                             
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                            
                                             
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                         }
                                     }];
    [operation start];

}


-(void)ResponsefbSignUp:(NSDictionary * )TheDict
{
    NSLog(@"ResponseDefault: %@",TheDict);
    
    [self.view setUserInteractionEnabled:YES];
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        if (![[TheDict valueForKey:@"profile_info"] isKindOfClass:[NSNull class]])
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[TheDict valueForKey:@"profile_info"]  forKey:@"UserInfo"];
            [defaults synchronize];
            self.txt_Username.text = @"";
            self.txt_Password.text = @"";
            self.txt_ConfirmPassword.text = @"";
            txt_call_referal_id.text = @"";
            imgView_PasswordStatus.hidden = YES;
            imgView_PasswordStatus2.hidden = YES;

            
            str_showsucess = @"YES";
            [self popup_Alertview:[TheDict valueForKey:@"message"]];
            
            
            
            //            [self.navigationController pushViewController:vcc animated:NO];
        }
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        NSString *msg = [TheDict objectForKey:@"message"];
        //        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //        [alertView show];
        
        self.txt_Username.text = @"";
        self.txt_Password.text = @"";
        self.txt_ConfirmPassword.text = @"";
        txt_call_referal_id.text = @"";
        imgView_PasswordStatus.hidden = YES;
        imgView_PasswordStatus2.hidden = YES;

        [self popup_Alertview:[TheDict valueForKey:@"message"]];
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([string isEqualToString:@"\n"])
    {
        [textField resignFirstResponder];
    }
    if(textField ==  _txt_Username )
    {
        if ([string isEqualToString:@" "])
        {
            return NO;
        }
    }
    if(textField ==  _txt_Password )
    {
        if ([string isEqualToString:@" "])
        {
            return NO;
        }
    }
    if(textField ==   _txt_ConfirmPassword )
    {
        if ([string isEqualToString:@" "])
        {
            return NO;
        }
    }
    else if (textField == txt_call_referal_id)
    {
        [imgView_PasswordStatus setImage:[UIImage imageNamed:@""]];
        NSString *stringVal = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered =[[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        if (stringVal.length<17 && [string isEqualToString:filtered])
        {
            return YES;
        }
        else{
            return NO;
        }
        return [string isEqualToString:filtered];
        
    }
    
    
    return YES;
}

-(void)SignUpWithUser{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    

    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            
                            @"username"           : self.txt_Username.text,
                            @"password"           : self.txt_Password.text,
                            @"confirm_password"   : self.txt_ConfirmPassword.text,
                            @"role_type"          : @"user_profile",
                            @"device_udid"        : UniqueAppID,
                            @"device_token"       : @"dev",
                            @"device_type"        : @"1",
                            
                            
                            };
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:@"webservices/user-register.json"
                                                      parameters:params];
    
    NSLog(@"request =%@",request);
    
    //====================================================RESPONSE
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
     [delegate.activityIndicator stopAnimating];
        [self ResponseSignUp:JSON];
        
    }
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [delegate.activityIndicator stopAnimating];
                                         [self.view setUserInteractionEnabled:YES];

                                         if([operation.response statusCode] == 406){
                                             
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             
                                             
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                         }
                                     }];
    [operation start];
    
}



-(void)ResponseSignUp:(NSDictionary * )TheDict
{
       NSLog(@"ResponseDefault: %@",TheDict);
    
    [self.view setUserInteractionEnabled:YES];
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        if (![[TheDict valueForKey:@"profile_info"] isKindOfClass:[NSNull class]])
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[TheDict valueForKey:@"profile_info"]  forKey:@"UserInfo"];
            [defaults synchronize];
            
            self.txt_Username.text = @"";
            self.txt_Password.text = @"";
            self.txt_ConfirmPassword.text = @"";
            txt_call_referal_id.text = @"";
            imgView_PasswordStatus.hidden = YES;
            imgView_PasswordStatus2.hidden = YES;
            
            str_showsucess = @"YES";
            [self popup_Alertview:[TheDict valueForKey:@"message"]];
            
            
            
//            [self.navigationController pushViewController:vcc animated:NO];
        }
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
//        NSString *msg = [TheDict objectForKey:@"message"];
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alertView show];
        
        self.txt_Username.text = @"";
        self.txt_Password.text = @"";
        self.txt_ConfirmPassword.text = @"";
        txt_call_referal_id.text = @"";
        imgView_PasswordStatus.hidden = YES;
        imgView_PasswordStatus2.hidden = YES;
        [self popup_Alertview:[TheDict valueForKey:@"message"]];
    }
}


-(void)btn_back_click:(UIButton *)sender
{
    NSLog(@"btn_back_click:");
    [self dismissViewControllerAnimated:NO completion:nil];
    
//    [self.navigationController popViewControllerAnimated:NO];
    
    
}
-(void)btn_sign_up_click:(UIButton *)sender
{
    NSLog(@"btn_back_click:");
    
//        UserRegistration1VC *vcc = [[UserRegistration1VC alloc]init];
//    [self presentViewController:vcc animated:NO completion:nil];
    
       // [self.navigationController pushViewController:vcc animated:NO];
    
    if (self.txt_Username.text.length==0)
    {
        
        [self popup_Alertview:@"Please Enter Username"];
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Username" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alertView show];
    }
    else if (self.txt_Username.text.length<4)
    {
        self.txt_Username.text=@"";
        
        [self popup_Alertview:@"Username minimum 4 characters required"];
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Username minimum 4 characters required" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alertView show];
    }
    else if (self.txt_Password.text.length==0)
    {
        [self popup_Alertview:@"Please Enter Password"];
    }
    else if (self.txt_Password.text.length>0 &&![self ValidatePassword])
    {
        self.txt_Password.text = @"";
        [self ValidatePassword];
    }
    else if (self.txt_Password.text.length>0&& self.txt_ConfirmPassword.text.length==0)
    {
        self.txt_ConfirmPassword.text=@"";
        [self popup_Alertview:@"Please Enter Confirm Password"];
    }
    else  if (self.txt_ConfirmPassword.text.length>0 &&![self ValidateConfirmPassword])
    {
        [self ValidateConfirmPassword];
    }
    else
    {
        if ([[NSString stringWithFormat:@"%@",delegate.login_type]isEqualToString:@"FACEBOOK"])
        {
            [self SignUpWithFacebook];
        }
        else{
            [self SignUpWithUser];
            
        }

    }
//    else if (self.txt_Password.text.length==0)
//    {
//        
//        [self popup_Alertview:@"Please Enter Password"];
//
////        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
////        [alertView show];
//    }
//    else if (self.txt_Password.text.length>0 &&![self ValidatePassword])
//    {
//        self.txt_Password.text = @"";
//        [self ValidatePassword];
//    }
//    else if (self.txt_Password.text.length>0&& self.txt_ConfirmPassword.text.length==0)
//    {
//        self.txt_ConfirmPassword.text=@"";
//        
//        [self popup_Alertview:@"Please Enter Confirm Password"];
//
//    }
//    
//    
//    else  if (self.txt_ConfirmPassword.text.length>0 &&![self ValidateConfirmPassword])
//    {
//        [self ValidateConfirmPassword];
//    }
//    else
//    {
//        
//        if ([[NSString stringWithFormat:@"%@",delegate.login_type]isEqualToString:@"FACEBOOK"])
//        {
//            [self SignUpWithFacebook];
//        }
//        else{
//            [self SignUpWithUser];
// 
//        }
//        
////        if (appDelegate().isFBLogin) {
////            NSLog(@"facebook");
////            [self SignUpWithFacebook];
////
////        }else{
////            NSLog(@"simple");
////            [self SignUpWithUser];
////            
////        }
////
//        
//    }

}



 #pragma mark VALIDATION METHODS
-(BOOL)ValidatePassword
{
    [self.view endEditing:YES];
    BOOL letterCharacter = 0;
    BOOL numberCharacter = 0;
    if([self.txt_Password .text length] >= 8)
    {
        for (int i = 0; i < [self.txt_Password .text length]; i++)
        {
            unichar c = [self.txt_Password .text characterAtIndex:i];
            
            if(!letterCharacter)
            {
                letterCharacter = [[NSCharacterSet letterCharacterSet] characterIsMember:c];
            }
            if(!numberCharacter)
            {
                NSString *numberCharacterString = @"0123456789'";
                numberCharacter = [[NSCharacterSet characterSetWithCharactersInString:numberCharacterString] characterIsMember:c];
            }
        }
        if(letterCharacter && numberCharacter)
        {
            return YES;
        }
        else
        {
            self.txt_Password.text = @"";
            self.txt_ConfirmPassword.text = @"";
            
             [self popup_Alertview:@"Password should be alphanumeric and minimum eight characters"];
            
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:                                              @"Password should be minimum of 8 alphanumerical characters" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//            [alertView show];

            
           // [self popup_Alertview:@"Password should be minimum of 8 alphanumerical characters"];
        }
    }
    else
    {
        self.txt_Password .text = @"";
         self.txt_ConfirmPassword.text = @"";
        [self popup_Alertview:@"Password should be alphanumeric and minimum eight characters"];

        
        
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Password should be minimum of 8 alphanumerical characters" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alertView show];

     //   [self popup_Alertview:@"Password should be minimum of 8 alphanumerical characters"];
    }
    return NO;
}

-(BOOL)ValidateConfirmPassword
{
    [self.view endEditing:YES];
    
    BOOL Password_Flag = FALSE;
    
    if(!([[self.txt_Password text] length] == [[self.txt_ConfirmPassword text] length]))
    {
       // imgView_PWDStatus.hidden=YES;
       // imgView_confPWDStatus.hidden=YES;
        [self.txt_Password setText:@""];
        [self.txt_ConfirmPassword setText:@""];
        
        [self popup_Alertview:@"password and confirm password doesn't match"];

        
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Your password and confirm password does not match" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alertView show];
        
        
       // [self popup_Alertview:@"Your password and confirm password does not match"];
    }
    else {
        for(int x = 0; x < [[ self.txt_Password text] length];x++)
        {
            if([self.txt_Password.text characterAtIndex:x] != [ self.txt_ConfirmPassword.text characterAtIndex:x])
            {
//                imgView_PasswordStatus.hidden=YES;
//               imgView_PasswordStatus2.hidden=YES;
                [self.txt_Password setText:@""];
                [self.txt_ConfirmPassword setText:@""];
                
                [self popup_Alertview:@"Your password and confirm password does not match"];

//                
//                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Your password and confirm password does not match" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//                [alertView show];
//                
                
               // [self popup_Alertview:@"Your password and confirm password does not match"];
                Password_Flag = NO;
                break;
            }
            else
            {
                Password_Flag = YES;
            }
        }
    }
    if (Password_Flag == YES)
    {
        [imgView_PasswordStatus2 setImage:[UIImage imageNamed:@"icon-currect@2x.png"]];
        [imgView_PasswordStatus setImage:[UIImage imageNamed:@"icon-currect@2x.png"]];
    }
    else
    {
        [imgView_PasswordStatus2 setImage:[UIImage imageNamed:@"icon-cross@2x.png"]];
        [imgView_PasswordStatus setImage:[UIImage imageNamed:@"icon-cross@2x.png"]];
    }
    return  Password_Flag;
}






-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}

#pragma rerutrn event code

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [imgView_PasswordStatus2 setImage:[UIImage imageNamed:@""]];
    [imgView_PasswordStatus setImage:[UIImage imageNamed:@""]];

    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (textField==_txt_Password&&_txt_Password.text.length>0)
    {
        if ([self ValidatePassword])
        {
            imgView_PasswordStatus.hidden = NO;
            
        }
        if (_txt_ConfirmPassword.text.length>0)
        {
            [self ValidateConfirmPassword];
        }
    }
    else if (textField == _txt_ConfirmPassword && _txt_ConfirmPassword.text.length>0 && _txt_Password.text.length>0)
    {
        if ([self ValidateConfirmPassword])
        {
            imgView_PasswordStatus.hidden = NO;
        }
    }

    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFontBold size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFontBold size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    if ([str_showsucess isEqualToString:@"YES"])
    {
        UserRegistration1VC *vcc = [[UserRegistration1VC alloc]init];
        [self presentViewController:vcc animated:NO completion:nil];
 
    }
    [alertviewBg removeFromSuperview];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
