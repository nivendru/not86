//
//  UserLogonVC.m
//
//
//  Created by Admin on 29/08/15.
//
//

#import "UserLogonVC.h"
#import "UserLoginVC.h"
#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>

//#import <FBSDKLoginKit/FBSDKLoginKit.h>
//#import <FBSDKCoreKit/FBSDKCoreKit.h>



#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"
#define kFont @"CenturyGothic"


@interface UserLogonVC ()<FBLoginViewDelegate,GPPSignInDelegate, GPPDeepLinkDelegate>
{
    UIImageView *img_header;
    UIImageView *img_topbar;
    NSMutableArray *ary_SocialInfo;
    
    NSString*str_fblogin;
    AppDelegate *delegate;
    
    UIImageView * img_background;
    
    CGFloat	animatedDistance;
    
    // facebook
    FBLoginView *FBloginVC;
    
}

@end

@implementation UserLogonVC


static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;

- (void)viewDidLoad {
    [super viewDidLoad];
    ary_SocialInfo = [NSMutableArray new];
    str_fblogin = [NSString new];
    [self integrateHeader];
    [self integrateBody];
    
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
    
    //   appDelegate().isFBLogin = NO;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)integrateHeader
{
    img_background = [[UIImageView alloc]init];
    img_background .frame = CGRectMake(0, 0, WIDTH, HEIGHT);
    [img_background setImage:[UIImage imageNamed:@"img_background@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_background setUserInteractionEnabled:YES];
    [self.view addSubview:img_background];
    
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [ self.view addSubview:img_header];
    
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(5,9,25,25);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back addTarget:self action:@selector(btn_back_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_back];
    
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_back.frame)+30,0, 150, 45);
    lbl_User_Sign_Up.text = @"Eat";
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    
    UIImageView *icon_user = [[UIImageView alloc]init];
    icon_user.frame = CGRectMake(WIDTH-40, 7, 30, 30);
    [icon_user setImage:[UIImage imageNamed:@"img_logo@2x.png"]];
    // icon_user.backgroundColor = [UIColor redColor];
    [icon_user setUserInteractionEnabled:YES];
    [img_header addSubview:icon_user];
    
    
}

-(void)integrateBody
{
    
    
    UIButton *btn_fb = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_fb.frame = CGRectMake(45,CGRectGetMaxY( img_header.frame)+310,60,60);
    btn_fb.backgroundColor = [UIColor clearColor];
    [btn_fb addTarget:self action:@selector(btn_fb_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_fb setImage:[UIImage imageNamed:@"img_facebook@2x.png"] forState:UIControlStateNormal];
    [img_background  addSubview:btn_fb];
    
    UIButton *btn_google_pluse = [UIButton buttonWithType:UIButtonTypeCustom];
    //   btn_google_pluse.frame = CGRectMake(207,CGRectGetMaxY( img_header.frame)+310,60,60);
    btn_google_pluse.backgroundColor = [UIColor clearColor];
    [btn_google_pluse addTarget:self action:@selector(btn_google_pluse_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_google_pluse setImage:[UIImage imageNamed:@"img_g plus@2x.png"] forState:UIControlStateNormal];
    [img_background  addSubview:btn_google_pluse];
    
    UIImageView * img_line = [[UIImageView alloc]init];
    //   img_line .frame = CGRectMake(157, CGRectGetMaxY( img_header.frame)+300, 0.5, 85);
    [img_line setImage:[UIImage imageNamed:@"img_small line@2x.png"]];
    //img_background.backgroundColor = [UIColor redColor];
    [img_line setUserInteractionEnabled:YES];
    [img_background addSubview:img_line];
    
    
    
    UIImageView * img_text_login = [[UIImageView alloc]init];
    img_text_login .frame = CGRectMake(10, CGRectGetMaxY( btn_fb.frame)+35, WIDTH-20,15);
    [img_text_login setImage:[UIImage imageNamed:@"img-log-in@2x.png"]];
    img_text_login.backgroundColor = [UIColor clearColor];
    [img_text_login setUserInteractionEnabled:YES];
    [self.view addSubview:img_text_login];
    
    UIImageView * img_login = [[UIImageView alloc]init];
    img_login .frame = CGRectMake(10, CGRectGetMaxY( btn_fb.frame)+35, WIDTH-20,15);
    [img_login setImage:[UIImage imageNamed:@"img_login button@2x.png"]];
    img_login.backgroundColor = [UIColor clearColor];
    [img_login setUserInteractionEnabled:YES];
    [self.view addSubview:img_login];
    
    
    UIButton *btn_login = [UIButton buttonWithType:UIButtonTypeCustom];
    //btn_login.frame = CGRectMake(10,CGRectGetMaxY(  img_login.frame)+15,150,55);
    btn_login.backgroundColor = [UIColor clearColor];
    [btn_login addTarget:self action:@selector(btn_login_click:) forControlEvents:UIControlEventTouchUpInside];
    // [btn_login setImage:[UIImage imageNamed:@"btn-login2@2x.png"] forState:UIControlStateNormal];
    [self.view  addSubview:btn_login];
    
    UILabel *lbl_text = [[UILabel alloc]init];
    // lbl_text.frame = CGRectMake(20,CGRectGetMaxY(btn_login.frame), 150, 20);
    lbl_text.text = @"Sign in to your not86 account";
    lbl_text.font = [UIFont fontWithName:kFont size:10];
    lbl_text.textColor = [UIColor blackColor];
    lbl_text.backgroundColor = [UIColor clearColor];
    [img_background addSubview:lbl_text];
    
    
    UIImageView * img_sign_up = [[UIImageView alloc]init];
    img_sign_up .frame = CGRectMake(10, CGRectGetMaxY( btn_fb.frame)+35, WIDTH-20,15);
    [img_sign_up setImage:[UIImage imageNamed:@"btn-signup2@2x.png"]];
    img_sign_up.backgroundColor = [UIColor clearColor];
    [img_sign_up setUserInteractionEnabled:YES];
    [self.view addSubview:img_sign_up];
    
    UIButton *btn_sign_up = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_sign_up.frame = CGRectMake(CGRectGetMaxX(btn_login.frame)+3,CGRectGetMaxY(  img_login.frame)+15,150,55);
    btn_sign_up.backgroundColor = [UIColor clearColor];
    [btn_sign_up addTarget:self action:@selector(btn_sign_up_click:) forControlEvents:UIControlEventTouchUpInside];
    // [btn_sign_up setImage:[UIImage imageNamed:@"btn-signup2@2x.png"] forState:UIControlStateNormal];
    [self.view addSubview:btn_sign_up];
    
    UILabel *lbl_text1 = [[UILabel alloc]init];
    // lbl_text1.frame = CGRectMake(CGRectGetMaxX(lbl_text.frame),CGRectGetMaxY(btn_sign_up.frame), 150, 20);
    lbl_text1.text = @"Apply to become a not86 user";
    lbl_text1.font = [UIFont fontWithName:kFont size:10];
    lbl_text1.textColor = [UIColor blackColor];
    lbl_text1.backgroundColor = [UIColor clearColor];
    [img_background addSubview:lbl_text1];
    
    if (IS_IPHONE_6Plus)
    {
        //        [img_background setImage:[UIImage imageNamed:@"6+-img-bg@2x.png"]];
        //        [btn_fb setImage:[UIImage imageNamed:@"6+-icon-fb.png"] forState:UIControlStateNormal];
        //        [btn_google_pluse setImage:[UIImage imageNamed:@"6+-icon-G+.png"] forState:UIControlStateNormal];
        //        [img_text_login setImage:[UIImage imageNamed:@"img-log-in@2x.png"]];
        //        [img_login setImage:[UIImage imageNamed:@"6+-login-img@2x.png"]];
        //        [img_sign_up setImage:[UIImage imageNamed:@"6+-icon-signup@2x.png"]];
        
        btn_fb.frame = CGRectMake(62,450,77,77);
        btn_google_pluse.frame = CGRectMake(267,450,77,77);
        img_line .frame = CGRectMake(205, 450, 1, 100);
        img_text_login .frame = CGRectMake(10, CGRectGetMaxY( btn_fb.frame)+50, WIDTH-20,20);
        
        img_login .frame = CGRectMake(10, CGRectGetMaxY( btn_fb.frame)+85, 190,70);
        btn_login.frame = CGRectMake(10,CGRectGetMaxY(  btn_fb.frame)+65,190,70);
        lbl_text.frame = CGRectMake(20,CGRectGetMaxY(btn_login.frame)+28, 200, 20);
        img_sign_up .frame = CGRectMake(215, CGRectGetMaxY( btn_fb.frame)+85, 190,70);
        btn_sign_up.frame = CGRectMake(215, CGRectGetMaxY( btn_fb.frame)+65, 190,70);
        lbl_text1.frame = CGRectMake(CGRectGetMaxX(lbl_text.frame),CGRectGetMaxY(btn_sign_up.frame)+28, 200, 20);
        
        lbl_text.font = [UIFont fontWithName:kFont size:12];
        lbl_text1.font = [UIFont fontWithName:kFont size:12];
        
    }
    else if (IS_IPHONE_6)
    {
        //        [img_background setImage:[UIImage imageNamed:@"6+-img-bg@2x.png"]];
        //        [btn_fb setImage:[UIImage imageNamed:@"6+-icon-fb.png"] forState:UIControlStateNormal];
        //        [btn_google_pluse setImage:[UIImage imageNamed:@"6+-icon-G+.png"] forState:UIControlStateNormal];
        //        [img_text_login setImage:[UIImage imageNamed:@"img-log-in@2x.png"]];
        //        [img_login setImage:[UIImage imageNamed:@"6+-login-img@2x.png"]];
        //        [img_sign_up setImage:[UIImage imageNamed:@"6+-icon-signup@2x.png"]];
        
        btn_fb.frame = CGRectMake(58,400,68,77);
        btn_google_pluse.frame = CGRectMake(235,400,68,77);
        img_line .frame = CGRectMake(184, 400, 1, 100);
        img_text_login .frame = CGRectMake(10, CGRectGetMaxY( btn_fb.frame)+45, WIDTH-20,20);
        
        img_login .frame = CGRectMake(15, CGRectGetMaxY( btn_fb.frame)+80, 165,70);
        btn_login.frame = CGRectMake(15,CGRectGetMaxY(  btn_fb.frame)+80,165,70);
        lbl_text.frame = CGRectMake(24,CGRectGetMaxY(btn_login.frame)+5, 150, 20);
        img_sign_up .frame = CGRectMake(195, CGRectGetMaxY( btn_fb.frame)+80, 165,70);
        btn_sign_up.frame = CGRectMake(195, CGRectGetMaxY( btn_fb.frame)+80, 165,70);
        lbl_text1.frame = CGRectMake(CGRectGetMaxX(lbl_text.frame)+30,CGRectGetMaxY(btn_sign_up.frame)+5, 150, 20);
        
        
        
    }
    else if (IS_IPHONE_5)
    {
        btn_fb.frame = CGRectMake(45,IS_IPHONE_5?CGRectGetMaxY( img_header.frame)+310: CGRectGetMaxY( img_header.frame)+240,60,60);
        btn_google_pluse.frame = CGRectMake(207,IS_IPHONE_5? CGRectGetMaxY( img_header.frame)+310: CGRectGetMaxY( img_header.frame)+240,60,60);
        //        [img_login setImage:[UIImage imageNamed:@"6+-login-img@2x.png"]];
        img_line .frame = CGRectMake(IS_IPHONE_5?156:157,IS_IPHONE_5?350:290, 1, 73);
        //        [img_text_login setImage:[UIImage imageNamed:@"img-log-in@2x.png"]];
        img_text_login .frame = CGRectMake(10, CGRectGetMaxY( btn_fb.frame)+30, WIDTH-20,15);
        
        img_login .frame = CGRectMake(15, CGRectGetMaxY(btn_fb.frame)+58, 140,55);
        btn_login.frame = CGRectMake(15,CGRectGetMaxY(btn_fb.frame)+58,140,55);
        lbl_text.frame = CGRectMake(19,CGRectGetMaxY(btn_login.frame)+6, 150, 20);
        img_sign_up .frame = CGRectMake(167, CGRectGetMaxY( btn_fb.frame)+58, 140,55);
        btn_sign_up.frame = CGRectMake(167, CGRectGetMaxY( btn_fb.frame)+58, 140,55);
        lbl_text1.frame = CGRectMake(CGRectGetMaxX(lbl_text.frame)+3,CGRectGetMaxY(btn_sign_up.frame)+6, 150, 18);
        
        lbl_text.font = [UIFont fontWithName:kFont size:9];
        lbl_text1.font = [UIFont fontWithName:kFont size:9];
        
        
    }
    else
    {
        btn_fb.frame = CGRectMake(45,IS_IPHONE_5?CGRectGetMaxY( img_header.frame)+310: CGRectGetMaxY( img_header.frame)+240,60,60);
        btn_google_pluse.frame = CGRectMake(207,IS_IPHONE_5? CGRectGetMaxY( img_header.frame)+310: CGRectGetMaxY( img_header.frame)+240,60,60);
        //        [img_login setImage:[UIImage imageNamed:@"6+-login-img@2x.png"]];
        img_line .frame = CGRectMake(IS_IPHONE_5?156:157,IS_IPHONE_5?350:290, 1, 73);
        //        [img_text_login setImage:[UIImage imageNamed:@"img-log-in@2x.png"]];
        img_text_login .frame = CGRectMake(10, CGRectGetMaxY( btn_fb.frame)+30, WIDTH-20,15);
        
        img_login .frame = CGRectMake(15, CGRectGetMaxY(btn_fb.frame)+53, 140,55);
        btn_login.frame = CGRectMake(15,CGRectGetMaxY(btn_fb.frame)+53,140,55);
        lbl_text.frame = CGRectMake(19,CGRectGetMaxY(btn_login.frame)+3, 150, 20);
        img_sign_up .frame = CGRectMake(167, CGRectGetMaxY( btn_fb.frame)+53, 140,55);
        btn_sign_up.frame = CGRectMake(167, CGRectGetMaxY( btn_fb.frame)+53, 140,55);
        lbl_text1.frame = CGRectMake(CGRectGetMaxX(lbl_text.frame)+3,CGRectGetMaxY(btn_sign_up.frame)+3, 150, 18);
        
        lbl_text.font = [UIFont fontWithName:kFont size:9];
        lbl_text1.font = [UIFont fontWithName:kFont size:9];
    }
}
-(void)btn_fb_click:(UIButton *)sender
{
    NSLog(@"img_fb_click Btn Click");
    
    [FBSession.activeSession close];
    [FBSession.activeSession  closeAndClearTokenInformation];
    FBSession.activeSession=nil;
    
    FBloginVC = [[FBLoginView alloc] init];
    
    FBloginVC.frame = CGRectMake(0, 0, 0, 0);
    
    FBloginVC.delegate = self;
    FBloginVC.readPermissions = @[@"public_profile", @"email"];
    
    [self.view addSubview:FBloginVC];
    
    for (id obj in FBloginVC.subviews)
    {
        if ([obj isKindOfClass:[UIButton class]])
        {
            UIButton * loginButton =  obj;
            loginButton.frame =  CGRectMake(0, 0, 0, 0);
            
            [loginButton sendActionsForControlEvents: UIControlEventTouchUpInside];
            
        }
        if ([obj isKindOfClass:[UILabel class]])
        {
            UILabel * loginLabel =  obj;
            loginLabel.frame = CGRectMake(0, 0, 0, 0);
        }
    }
    
    
    
    // [self.navigationController popViewControllerAnimated:NO];
    
}
#pragma mark ------------------------- GOOGLE PLUS DELEGATE METHODS INTEGRATION ---START

-(void)btn_google_pluse_click:(UIButton *)sender
{
    NSLog(@"img_btn_google_pluse_click:");
    
    //    delegate.login_type = @"google";
    //    [self.view addSubview:delegate.activityIndicator];
    //    [delegate.activityIndicator startAnimating];
    //
    //
    //    [GPPSignInButton class];
    //
    //
    //    GPPSignIn *signIn = [GPPSignIn sharedInstance];
    //    signIn.delegate = self;
    //    signIn.shouldFetchGoogleUserID = YES;
    //    signIn.shouldFetchGoogleUserEmail = YES;
    //    signIn.clientID = kClientId;
    //    signIn.scopes = [NSArray arrayWithObjects:kGTLAuthScopePlusLogin,kGTLAuthScopePlusMe,nil];
    //
    ////    NSString *schema =
    ////    [NSString stringWithFormat:@"http://schemas.google.com/%@",
    ////     appActivity];
    ////    [supportedAppActivities addObject:schema];
    //
    //    signIn.actions = [NSArray arrayWithObjects:@"http://schemas.google.com/ListenActivity",nil];
    //    [GPPDeepLink setDelegate:self];
    //    [GPPDeepLink readDeepLinkAfterInstall];
    //    [signIn authenticate];
    
}

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth error: (NSError *) error
{
    [delegate.activityIndicator stopAnimating];
    
    if(!error) {
        
        [self GetLoggedInUserInfoWithAccessToken:auth.accessToken];
        
    }
    else {
    }
}

- (void)didReceiveDeepLink:(GPPDeepLink *)deepLink
{
    //NSLog(@"%@", deepLink);
}

- (void)didDisconnectWithError:(NSError *)error {
    if (error) {
        //        _signInAuthStatus.text =
        NSLog(@"-- %@",[NSString stringWithFormat:@"Status: Failed to disconnect: %@", error]);
    } else {
        //        _signInAuthStatus.text =
        NSLog(@"-- %@",[NSString stringWithFormat:@"Status: Disconnected"]);
    }
    if (error) {
        NSLog(@"Received error %@", error);
    } else {
        // The user is signed out and disconnected.
        // Clean up user data as specified by the Google+ terms.
        //        [self refreshInterfaceBasedOnSignIn];
    }
    
}

-(void) GetLoggedInUserInfoWithAccessToken:(NSString *) strToken{
    
    
    NSString *PeopleMe = [NSString stringWithFormat:@"https://www.googleapis.com/plus/v1/people/me"];
    NSURL *url = [NSURL URLWithString:PeopleMe];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [[GPPSignIn sharedInstance].authentication  authorizeRequest:request
                                               completionHandler:^(NSError *error) {
                                                   
                                                   
                                                   
                                                   NSString *output = nil;
                                                   if (error)
                                                   {
                                                       output = [error description];
                                                   } else {
                                                       // Synchronous fetches like this are a really bad idea in Cocoa applications
                                                       //
                                                       // For a very easy async alternative, we could use GTMHTTPFetcher
                                                       NSURLResponse *response = nil;
                                                       NSData *data = [NSURLConnection sendSynchronousRequest:request
                                                                                            returningResponse:&response
                                                                                                        error:&error];
                                                       if (data) {
                                                           // API fetch succeeded
                                                           //                          output = [[NSString alloc] initWithData:data
                                                           //                                                          encoding:NSUTF8StringEncoding];
                                                           
                                                           //[indicator stopAnimating];
                                                           NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                           
                                                           
                                                           NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
                                                           
                                                           [dict setObject:[JSON objectForKey:@"id"] forKey:@"id"];
                                                           [dict setObject:[JSON objectForKey:@"displayName"] forKey:@"username"];
                                                           [dict setObject:[[[JSON valueForKey:@"emails"]valueForKey:@"value"]objectAtIndex:0] forKey:@"email"];
                                                           [dict setObject:[[JSON valueForKey:@"image"] valueForKey:@"url"] forKey:@"profilepic"];
                                                           
                                                           
                                                           [ary_SocialInfo removeAllObjects];
                                                           
                                                           [ary_SocialInfo addObject:dict];
                                                           
                                                           
                                                           
                                                           
                                                           
                                                           
                                                       } else {
                                                           
                                                           //[indicator stopAnimating];
                                                           // fetch failed
                                                           //  NSLog(@"error:%@",[error description]);
                                                       }
                                                   }
                                                   
                                               }];
}

#pragma mark ------------------------- GOOGLE PLUS DELEGATE METHODS INTEGRATION ---END


#pragma mark ------------------------- FACEBOOK INTEGRATION ------------------------Start

#pragma mark - FBLoginViewDelegate

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    
    
    // "Post Status" available when logged on and potentially when logged off.  Differentiate in the label.
}


- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    // here we use helper properties of FBGraphUser to dot-through to first_name and
    // id properties of the json response from the server; alternatively we could use
    // NSDictionary methods such as objectForKey to get values from the my json object
    
    NSLog(@"usr_id::%@",user.id);
    NSLog(@"usr_first_name::%@",user.first_name);
    NSLog(@"usr_middle_name::%@",user.middle_name);
    NSLog(@"usr_last_nmae::%@",user.last_name);
    NSLog(@"usr_Username::%@",user.username);
    NSLog(@"usr_b_day::%@",user.birthday);
    
    NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [user objectForKey:@"id"]];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [dict setObject:[user objectForKey:@"id"] forKey:@"id"];
    [dict setObject:[user objectForKey:@"name"] forKey:@"name"];
    //    NSString *displayName = [NSString stringWithFormat:@"%@%@",[user objectForKey:@"first_name"],[user objectForKey:@"id"]];
    //    [dict setObject:displayName forKey:@"username"];
    //    [dict setObject:[user objectForKey:@"gender"] forKey:@"gender"];
    //    [dict setObject:[user objectForKey:@"email"] forKey:@"email"];
    //    [dict setObject:userImageURL forKey:@"profilepic"];
    [dict setObject:@"facebook" forKey:@"type"];
    
    
    [ary_SocialInfo removeAllObjects];
    [ary_SocialInfo addObject:dict];
    [FBloginVC removeFromSuperview];
    
    delegate.login_type = @"FACEBOOK";
    UserRegistration1VC *userSignUp = [[UserRegistration1VC alloc]init];
    userSignUp.array_Facebook_Details = ary_SocialInfo;
    [self.navigationController pushViewController:userSignUp animated:YES];
    
    
    //    appDelegate().isFBLogin = YES;
    //
    //    if (appDelegate().isFBLogin) {
    //        UserSignUpVC *userSignUp = [[UserSignUpVC alloc]init];
    //        [self.navigationController pushViewController:userSignUp animated:YES];
    //        userSignUp.array_Facebook_Details = ary_SocialInfo;
    //
    //    }
    
}
- (void)loginView:(FBLoginView *)loginView
      handleError:(NSError *)error
{
    //    [delegate.load removeFromSuperview];
}

#pragma mark ------------------------- FACEBOOK INTEGRATION ------------------------END


/*-(void)facebook{
 [FBSession openActiveSessionWithReadPermissions:@[@"email",@"user_location",@"user_birthday",@"user_hometown"]
 allowLoginUI:YES
 completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
 
 switch (state) {
 case FBSessionStateOpen:
 [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
 if (error) {
 NSLog(@"error:%@",error);
 }
 else
 {
 // retrive user's details at here as shown below
 NSLog(@"FB user first name:%@",user.first_name);
 NSLog(@"FB user last name:%@",user.last_name);
 NSLog(@"FB user birthday:%@",user.birthday);
 NSLog(@"FB user location:%@",user.location);
 NSLog(@"FB user username:%@",user.username);
 NSLog(@"FB user gender:%@",[user objectForKey:@"gender"]);
 NSLog(@"email id:%@",[user objectForKey:@"email"]);
 NSLog(@"location:%@", [NSString stringWithFormat:@"Location: %@\n\n",
 user.location[@"name"]]);
 
 }
 }];
 break;
 
 case FBSessionStateClosed:
 case FBSessionStateClosedLoginFailed:
 [FBSession.activeSession closeAndClearTokenInformation];
 break;
 
 default:
 break;
 }
 
 } ];
 
 
 }*/


-(void)btn_login_click:(UIButton *)sender
{
    NSLog(@"btn_login_click:");
    UserLoginVC *vc=[[UserLoginVC alloc]init];
    [self presentViewController:vc animated:NO completion:nil];
    
    //    [self.navigationController pushViewController:vc animated:NO];
}
-(void)btn_sign_up_click:(UIButton *)sender
{
    NSLog(@"btn_sign_up_click:");
    UserSignUpVC *vc=[[UserSignUpVC alloc] init];
    [self presentViewController:vc animated:NO completion:nil];
    
    //    [self.navigationController pushViewController:vc animated:NO];
    
    // UserRegistration2VC *vc = [[UserRegistration2VC alloc]init];
    //  [self.navigationController pushViewController:vc animated:NO];
    //
}


-(void)btn_back_click:(UIButton *)sender
{
    NSLog(@"btn_back_click:");
    [self.navigationController popViewControllerAnimated:NO];
    
    //    [self dismissViewControllerAnimated:NO completion:nil];
    
}

-(void)Click_fbbtn
{
    
    [FBSession.activeSession close];
    [FBSession.activeSession  closeAndClearTokenInformation];
    FBSession.activeSession=nil;
    
    FBloginVC = [[FBLoginView alloc] init];
    FBloginVC.frame = CGRectMake(0, 0, 0, 0);
    
    FBloginVC.delegate = self;
    FBloginVC.readPermissions = @[@"public_profile", @"email"];
    
    [self.view addSubview:FBloginVC];
    
    for (id obj in FBloginVC.subviews)
    {
        if ([obj isKindOfClass:[UIButton class]])
        {
            UIButton * loginButton =  obj;
            loginButton.frame =  CGRectMake(0, 0, 0, 0);
            
            [loginButton sendActionsForControlEvents: UIControlEventTouchUpInside];
            
        }
        if ([obj isKindOfClass:[UILabel class]])
        {
            UILabel * loginLabel =  obj;
            loginLabel.frame = CGRectMake(0, 0, 0, 0);
        }
    }
    
}
//-(void)Click_googlebtn
//{
//
//}

-(void)Click_loginbtn
{
    //    UserLoginVC *vc=[[UserLoginVC alloc]init];
    //    [self.navigationController pushViewController:vc animated:NO];
    //
}
-(void)Click_signupbtn
{
    //    UserSignUpVC *vc=[[UserSignUpVC alloc] init];
    //    [self.navigationController pushViewController:vc animated:NO];
}


#pragma rerutrn event code

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}




//#pragma mark - AF
//
//-(void) AFSocialGoogleLogin
//{
//
//    UIDevice *myDevice=[UIDevice currentDevice];
//    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
//
//
//
//    //=================================================================BASE URL
//    NSURL *url = [NSURL URLWithString:kBaseUrl];
//
//    //=================================================================USED PARAMETERS(ONLY TEXT)
//
//
//    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
//
//    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
//        str_device_token = @"";
//
//    }
//
//    NSDictionary *params =@{
//
//                            @"register_from"         :   @"2",
//                            @"socialsite_udid"    :   [[ary_SocialInfo objectAtIndex:0]valueForKey:@"id"],
//                            @"devicetoken"    :    str_device_token,
//                            @"deviceudid"    :    UniqueAppID,
//                            @"email"      :   [[ary_SocialInfo objectAtIndex:0]valueForKey:@"email"],
//                            @"username"      :  [[ary_SocialInfo objectAtIndex:0]valueForKey:@"username"],
//                            @"devicetype"    :@"1"
//
//                            };
//
//    //  =================================================================AFNETWORKING HEADER
//
//    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
//    httpClient.parameterEncoding = AFFormURLParameterEncoding;
//    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
//    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
//
//    //=================================================================USED FOR UNIQUE FILE NAME AND CONVERT TO DATA
//
//
//    //REQUEST WITH FILE
//
//    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:kFacebookLogin parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
//
//        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[ary_SocialInfo objectAtIndex:0]valueForKey:@"profilepic"]]];
//
//        NSTimeInterval timeInterval = [NSDate timeIntervalSinceReferenceDate];
//        NSString *photoName=[NSString stringWithFormat:@"%lf-Photo.jpeg",timeInterval];
//
//        [formData appendPartWithFileData:data name:@"image" fileName:photoName mimeType:@"image/jpeg"];
//    }];
//
//    //============================================================RESPONSE
//
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
//    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
//     {
//         NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
//     }];
//
//    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSError *error = nil;
//        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
//
//        [delegate.activityIndicator stopAnimating];
//        [self ResponseAFSocialGoogleLogin:JSON];
//    }
//     //=================================================================ERROR
//                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//
//                                         [delegate.activityIndicator stopAnimating];
//
//                                         if([operation.response statusCode] == 403){
//                                             NSLog(@"Upload Failed");
//                                             return;
//                                         }
//
//                                         if ([[operation error] code] == -1009) {
//                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"The List"
//                                                                                          message:@"Please check your internet connection"
//                                                                                         delegate:nil
//                                                                                cancelButtonTitle:@"OK"
//                                                                                otherButtonTitles:nil];
//                                             [av show];
//                                         }
//                                         else if ([[operation error] code] == -1001)
//                                         {
//                                             [self AFSocialGoogleLogin];
//                                         }
//                                     }];
//
//    [operation start];
//}
//
//
//-(void) ResponseAFSocialGoogleLogin :(NSDictionary * ) TheDict{
//
//
//    NSLog(@"%@",TheDict);
//
//
//    if ([[TheDict valueForKey:@"error"] intValue] == 0) {
//
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        [defaults setObject:[TheDict valueForKey:@"UserInfo"]  forKey:@"UserInfo"];
//        [defaults synchronize];
//
//
//        NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
//        [defaults1 setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"UserInfo"] valueForKey:@"second_degree_bookmarked_view"]  forKey:@"BookmarkSecondDegree"];
//        [defaults1 synchronize];
//
//        [self integrateJWSlider];
//
//    }
//    else
//    {
//        [SVProgressHUD showErrorWithStatus:[TheDict valueForKey:@"Message"]];
//
//    }
//}
//
//
//



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
