//
//  UserRegistration1VC.h
//  Not86
//
//  Created by Admin on 29/08/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserRegistration1VC : UIViewController<UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>{
    NSDateFormatter *formatter;
}
@property(nonatomic,strong)NSMutableArray *array_Facebook_Details;

@property(nonatomic,strong)UITextField *txt_FirstName,*txt_MiddleName,*txt_LastName,*txt_Email,*txt_DateBirth;
@property(nonatomic,strong)UITextField *txt_MobileNo,*txt_Phone_Code;
@property(nonatomic,strong)UIToolbar *keyboardToolbar_Date;
@property(nonatomic,strong)UIDatePicker *datePicker;
@end
