
//  ViewDishDetailsVC.m
//  Not86
//
//  Created by User on 01/12/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ViewDishDetailsVC.h"
#import "Define.h"
#import "AppDelegate.h"
#import <Social/Social.h>
#import "MGInstagram.h"
#import "MenuScreenEditMealVC.h"

@interface ViewDishDetailsVC ()<UIScrollViewDelegate,UITextViewDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIImageView * img_header;
    UIImageView * img_bg;
    
    UIScrollView * scroll;
    UIImageView  * img_strip;
    UIImageView * img_strip1;
    UIView*alertviewBg;
    
    
    UIView * view_for_reviews;
    UIView * view_for_food_details;
    UIImageView * img_rect;
    
    UITextField * txtview_over_all_value;
    
    UITextView * txtview_taste_value;
    
    UITableView * img_table;
    
    UIImageView * img_rect1;
    AppDelegate*delegate;
    NSMutableArray*ary_dishdetaillist;
    NSMutableArray*ary_UserReview;
    NSMutableArray*ary_reviewslist;
    
    UILabel *reviews_in_rat;
    UILabel *label_over_all_rateing;
    UILabel *label_taste_val;
    UILabel *label_appeal_value;
    UILabel *label_value_value;
    
    UITableView*table_on_drop_down;
    
    NSMutableArray*array_head_names;
    
    
    
    
}

@end

@implementation ViewDishDetailsVC
@synthesize str_dishID,str_dishUID,str_TYPE;


- (void)viewDidLoad
{
    [super viewDidLoad];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    array_head_names = [[NSMutableArray alloc]initWithObjects:@"Facebook",@"Instagram",@"Twitter", nil];
    
    
    ary_dishdetaillist = [NSMutableArray new];
    
    ary_UserReview= [NSMutableArray new];
    
    [self integrateHeader];
    
    // Do any additional setup after loading the view.
}

-(void)integrateHeader
{
    
    img_header=[[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    //    UIButton *icon_back= [UIButton buttonWithType:UIButtonTypeCustom];
    //    icon_back .frame = CGRectMake(10, 13,20,20);
    //    //icon_menu .backgroundColor = [UIColor clearColor];
    //    [icon_back  addTarget:self action:@selector(click_on_back_btn:) forControlEvents:UIControlEventTouchUpInside];
    //    [icon_back setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
    //    [img_header   addSubview:icon_back];
    
    
    UIButton *icon_back = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_back.frame = CGRectMake(10,13,20,20);
    icon_back .backgroundColor = [UIColor clearColor];
    [icon_back setImage:[UIImage imageNamed:@"img_arrow@2x.png"] forState:UIControlStateNormal];
    [icon_back addTarget:self action:@selector(click_on_back_arrow:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:icon_back];
    
    UILabel *lbl_User_Sign_Up = [[UILabel alloc]init];
    lbl_User_Sign_Up.frame = CGRectMake(CGRectGetMaxX(icon_back.frame)+30,0, 150, 45);
    
    if ([str_TYPE  isEqualToString:@"Dish"])
    {
        lbl_User_Sign_Up.text = @"Dish Detail";
        
    }
    else{
        lbl_User_Sign_Up.text = @"Menu Detail";
    }
    
    lbl_User_Sign_Up.font = [UIFont fontWithName:kFont size:20];
    lbl_User_Sign_Up.textColor = [UIColor whiteColor];
    lbl_User_Sign_Up.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_User_Sign_Up];
    
    
    
    /*UIImageView *imgback=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-70, 10, 20, 25)];
    [imgback setUserInteractionEnabled:YES];
    imgback.backgroundColor=[UIColor clearColor];
    imgback.image=[UIImage imageNamed:@"img_icon.png"];
    [img_header addSubview:imgback];
    
    
    UIButton *icon_share = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_share .frame = CGRectMake(WIDTH-70, 10, 20, 25);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_share  addTarget:self action:@selector(click_on_share_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_header   addSubview:icon_share ];*/
    
    
    
    UIButton *icon_logo = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_logo .frame = CGRectMake(WIDTH-35, 9, 27, 27);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_logo  addTarget:self action:@selector(click_on_logo_btn:) forControlEvents:UIControlEventTouchUpInside];
    [icon_logo setImage:[UIImage imageNamed:@"logo-img@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_logo ];
    
    table_on_drop_down = [[UITableView alloc] init ];
    table_on_drop_down .frame  = CGRectMake(WIDTH-160,45,120,120);
    [ table_on_drop_down  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_on_drop_down .delegate = self;
    table_on_drop_down .dataSource = self;
    table_on_drop_down .showsVerticalScrollIndicator = NO;
    table_on_drop_down .backgroundColor = [UIColor clearColor];
    table_on_drop_down.hidden = YES;
    [self.view addSubview: table_on_drop_down ];
    
    
    
    
    
}

-(void)click_on_share_btn:(UIButton *)sender
{
    table_on_drop_down.hidden=YES;
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        table_on_drop_down.hidden=NO;
        
        
    }
    else
    {
        [sender setSelected:NO];
        table_on_drop_down.hidden=YES;
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    [self AFUserDishDetails];
}
-(void)integratebody
{
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor whiteColor];
    scroll.frame = CGRectMake(0, 48, WIDTH,HEIGHT);
    scroll.scrollEnabled = YES;
    
    if(IS_IPHONE_6Plus){
      [scroll setContentSize:CGSizeMake(0,HEIGHT+110)];
    }
    else if(IS_IPHONE_6){
        [scroll setContentSize:CGSizeMake(0,HEIGHT+180)];
    }
    else {
        [scroll setContentSize:CGSizeMake(0,HEIGHT+125)];
    }
    
    
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    
    
    
    img_bg = [[UIImageView alloc]init];
    img_bg .frame = CGRectMake(0,0, WIDTH, 250);
    NSString *ImagePath = [NSString stringWithFormat:@"%@",[[ary_dishdetaillist objectAtIndex:0]valueForKey:@"DishImage"]];
    [img_bg setImageWithURL:[NSURL URLWithString:ImagePath] placeholderImage:[UIImage imageNamed:@"img-dish-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg  setUserInteractionEnabled:YES];
    [scroll addSubview:img_bg ];
    
    
    
    
    UIImageView *img_edit = [[UIImageView alloc]init];
    img_edit.frame = CGRectMake(WIDTH-50,20, 30, 30);
    [img_edit setImage:[UIImage imageNamed:@"icon-edit.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_edit  setUserInteractionEnabled:YES];
    [img_bg addSubview:img_edit];
    
    
    UIButton *btn_img_edit = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_edit.frame = CGRectMake(WIDTH-60,0, 60, 60);
    btn_img_edit .backgroundColor = [UIColor clearColor];
    [btn_img_edit addTarget:self action:@selector(btn_edit_click:) forControlEvents:UIControlEventTouchUpInside];
    [scroll   addSubview:btn_img_edit ];
    
    
    
    UILabel *text_dish_name = [[UILabel alloc]init];
    text_dish_name  .frame = CGRectMake(105,150,200,30);
    text_dish_name .text = [NSString stringWithFormat:@"%@",[[ary_dishdetaillist objectAtIndex:0] valueForKey:@"DishName"]];
    text_dish_name  .font = [UIFont fontWithName:kFontBold size:15];
    text_dish_name  .font = [UIFont fontWithName:kFontBold size:24];
    text_dish_name .textColor = [UIColor whiteColor];
    text_dish_name  .backgroundColor = [UIColor clearColor];
    [img_bg addSubview:text_dish_name];
    
    
    
    UIButton *btn_fb = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_fb.frame = CGRectMake(20,210,40,40);
    btn_fb .backgroundColor = [UIColor clearColor];
    [btn_fb  addTarget:self action:@selector(btn_fb_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_fb setImage:[UIImage imageNamed:@"fb-icon@2x.png"] forState:UIControlStateNormal];
    [img_bg   addSubview:btn_fb];
    
    UIButton *btn_tw = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_tw.frame = CGRectMake(CGRectGetMaxX(btn_fb.frame)+5,210,40,40);
    btn_tw .backgroundColor = [UIColor clearColor];
    [btn_tw  addTarget:self action:@selector(btn_tw_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_tw setImage:[UIImage imageNamed:@"tw-icon@2x.png"] forState:UIControlStateNormal];
    [img_bg   addSubview:btn_tw];
    
    UIButton *btn_cm = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_cm.frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+5,210,40,40);
    btn_cm .backgroundColor = [UIColor clearColor];
    [btn_cm  addTarget:self action:@selector(btn_cm_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_cm setImage:[UIImage imageNamed:@"cm-icon@2x.png"] forState:UIControlStateNormal];
    [img_bg   addSubview:btn_cm];
    
    UILabel *text_rate = [[UILabel alloc]init];
    text_rate  .frame = CGRectMake(WIDTH-60,215,200,30);
    text_rate .text = [NSString stringWithFormat:@"$%@",[[ary_dishdetaillist objectAtIndex:0] valueForKey:@"DishPrice"]];
    text_rate  .font = [UIFont fontWithName:kFontBold size:20];
    text_rate .textColor = [UIColor whiteColor];
    text_rate  .backgroundColor = [UIColor clearColor];
    [img_bg addSubview:text_rate];
    
    UILabel *text_dish_details = [[UILabel alloc]init];
    text_dish_details.frame = CGRectMake(40,CGRectGetMaxY(img_bg.frame)+20,200, 15);
    text_dish_details.text = @"Food Detail";
    text_dish_details.font= [UIFont fontWithName:kFont size:15];
    // text_chef.textColor = [UIColor redColor];
    text_dish_details.backgroundColor = [UIColor clearColor];
    [scroll addSubview:text_dish_details];
    
    img_strip = [[UIImageView alloc]init];
    img_strip.frame = CGRectMake(40, CGRectGetMaxY(text_dish_details.frame)+5, 80, 2);
    [img_strip setImage:[UIImage imageNamed:@"black strip@2x.png"]];
    [img_strip setUserInteractionEnabled:YES];
    [scroll addSubview:img_strip];
    
    
    
    UIButton *btn_food_details = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_food_details.frame = CGRectMake(0,CGRectGetMaxY(img_bg.frame), WIDTH/2,50);
    btn_food_details .backgroundColor = [UIColor clearColor];
    [btn_food_details addTarget:self action:@selector(btn_food_details_click:) forControlEvents:UIControlEventTouchUpInside];
    [scroll   addSubview:btn_food_details ];
    
    
    
    UILabel *text_reviews = [[UILabel alloc]init];
    text_reviews .frame = CGRectMake(CGRectGetMaxX(btn_food_details.frame)+40,CGRectGetMaxY(img_bg.frame)+20,70, 15);
    text_reviews.text = @"Reviews";
    text_reviews .font = [UIFont fontWithName:kFont size:15];
    //text_cuisen .textColor = [UIColor redColor];
    text_reviews .backgroundColor = [UIColor clearColor];
    [scroll addSubview:text_reviews ];
    
    UILabel *number_of_reviews = [[UILabel alloc]init];
    number_of_reviews.frame = CGRectMake(CGRectGetMaxX(text_reviews.frame)-5,CGRectGetMaxY(img_bg.frame)+20,50, 15);
    number_of_reviews.text = [NSString stringWithFormat:@"[%@]",[[ary_dishdetaillist objectAtIndex:0]valueForKey:@"total_revies"]]; //@"[123]";
    number_of_reviews.font = [UIFont fontWithName:kFont size:10];
    number_of_reviews .textColor = [UIColor blackColor];
    number_of_reviews .backgroundColor = [UIColor clearColor];
    [scroll addSubview:number_of_reviews ];
    
    
    UIImageView *img_percentage_tag = [[UIImageView alloc]init];
    img_percentage_tag.frame = CGRectMake(CGRectGetMidX(number_of_reviews.frame),CGRectGetMaxY(img_bg.frame)+10, 30, 18);
    [img_percentage_tag setImage:[UIImage imageNamed:@"icon_round_red@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_percentage_tag  setUserInteractionEnabled:YES];
    [img_bg addSubview:img_percentage_tag ];
    
    
    UILabel *likes_reviews = [[UILabel alloc]init];
    likes_reviews.frame = CGRectMake(CGRectGetMidX(number_of_reviews.frame),img_percentage_tag.frame.origin.y, 30, 18);
    likes_reviews.text = [NSString stringWithFormat:@"%@%%", [[ary_dishdetaillist objectAtIndex:0]valueForKey:@"overall_rating"]];
    [likes_reviews setTextAlignment:NSTextAlignmentCenter];
    likes_reviews.font = [UIFont fontWithName:kFont size:10];
    likes_reviews .textColor = [UIColor blackColor];
    likes_reviews .backgroundColor = [UIColor clearColor];
    [img_bg addSubview:likes_reviews];
    
    img_strip1 = [[UIImageView alloc]init];
    img_strip1.frame = CGRectMake(CGRectGetMaxX(img_strip.frame)+110, CGRectGetMaxY(text_reviews.frame)+5, 57, 2);
    [img_strip1 setImage:[UIImage imageNamed:@"black strip@2x.png"]];
    [img_strip1 setUserInteractionEnabled:YES];
    [scroll addSubview:img_strip1];
    img_strip1.hidden = YES;
    
    
    
    UIButton *btn_reviews = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_reviews.frame = CGRectMake(WIDTH/2+10,CGRectGetMaxY(img_bg.frame),WIDTH/2,50);
    btn_reviews .backgroundColor = [UIColor clearColor];
    [btn_reviews addTarget:self action:@selector(btn_reviews_click:) forControlEvents:UIControlEventTouchUpInside];
    [scroll   addSubview:btn_reviews];
    
    view_for_food_details = [[UIView alloc]init];
    view_for_food_details.frame=CGRectMake(0,CGRectGetMaxY(btn_reviews.frame),WIDTH,HEIGHT-210);
    view_for_food_details.backgroundColor=[UIColor clearColor];
    [view_for_food_details setUserInteractionEnabled:YES];
    [scroll  addSubview: view_for_food_details];
    
    
    
    
    img_rect1 = [[UIImageView alloc]init];
    img_rect1 .frame = CGRectMake(10,10,WIDTH-20, 450);
    [img_rect1 setImage:[UIImage imageNamed:@"img_bg@2x.png"]];
    img_rect1.backgroundColor = [UIColor whiteColor];
    [img_rect1 setUserInteractionEnabled:YES];
    [view_for_food_details addSubview:img_rect1 ];
    
    
    
    UITextView * txtview_message = [[UITextView alloc]init];
    txtview_message.frame = CGRectMake(10,5, WIDTH-40,65);
    txtview_message.scrollEnabled = YES;
    txtview_message.userInteractionEnabled = YES;
    txtview_message.font = [UIFont fontWithName:kFont size:13];
    txtview_message.backgroundColor = [UIColor clearColor];
    txtview_message.delegate = self;
    txtview_message.textColor = [UIColor blackColor];
    txtview_message.text =[NSString stringWithFormat:@"%@",[[ary_dishdetaillist objectAtIndex:0] valueForKey:@"Description"]];
    [img_rect1 addSubview:txtview_message];
    
    UIImageView *img_line2 = [[UIImageView alloc]init];
    img_line2 .frame = CGRectMake(15,CGRectGetMaxY(txtview_message.frame)+10, WIDTH-45,1);
    [img_line2  setImage:[UIImage imageNamed:@"line@2x.png"]];
    img_line2.backgroundColor = [UIColor lightGrayColor];
    [img_line2   setUserInteractionEnabled:YES];
    [img_rect1 addSubview:img_line2];
    
    UIImageView *img_key = [[UIImageView alloc]init];
    img_key .frame = CGRectMake(15,CGRectGetMaxY(img_line2.frame)+10,40,40);
    [img_key  setImage:[UIImage imageNamed:@"food-icon@2x.png"]];
    img_key.backgroundColor = [UIColor clearColor];
    [img_key   setUserInteractionEnabled:YES];
    [img_rect1 addSubview:img_key];
    
    
    UILabel *lbl_keyword = [[UILabel alloc]init];
    lbl_keyword .frame = CGRectMake(CGRectGetMaxX(img_key.frame)+10,CGRectGetMaxY(img_line2.frame)+10,150, 20);
    lbl_keyword.text = @"Keywords";
    lbl_keyword .font = [UIFont fontWithName:kFontBold size:15];
    lbl_keyword .textColor = [UIColor blackColor];
    lbl_keyword .backgroundColor = [UIColor clearColor];
    [img_rect1 addSubview:lbl_keyword];
    
    
    UILabel *lbl_keyword_items = [[UILabel alloc]init];
    lbl_keyword_items .frame = CGRectMake(CGRectGetMaxX(img_key.frame)+10,CGRectGetMaxY(lbl_keyword.frame),280, 25);
    lbl_keyword_items.text = [NSString stringWithFormat:@"%@",[[ary_dishdetaillist objectAtIndex:0] valueForKey:@"DishKeyword"]];
    lbl_keyword_items .font = [UIFont fontWithName:kFont size:13];
    lbl_keyword_items .textColor = [UIColor blackColor];
    lbl_keyword_items .backgroundColor = [UIColor clearColor];
    [img_rect1 addSubview:lbl_keyword_items];
    
    
    UIImageView *img_line3 = [[UIImageView alloc]init];
    img_line3 .frame = CGRectMake(15,CGRectGetMaxY(img_key.frame)+10, WIDTH-45,1);
    [img_line3  setImage:[UIImage imageNamed:@"line@2x.png"]];
    img_line3.backgroundColor = [UIColor lightGrayColor];
    [img_line3   setUserInteractionEnabled:YES];
    [img_rect1 addSubview:img_line3];
    
    UIImageView *img_courseicon = [[UIImageView alloc]init];
    img_courseicon .frame = CGRectMake(15,CGRectGetMaxY(img_line3.frame)+10,40,40);
    [img_courseicon  setImage:[UIImage imageNamed:@"food-icon@2x.png"]];
    img_courseicon.backgroundColor = [UIColor clearColor];
    [img_courseicon   setUserInteractionEnabled:YES];
    [img_rect1 addSubview:img_courseicon];
    
    
    UILabel *lbl_dietary_ristriction = [[UILabel alloc]init];
    lbl_dietary_ristriction .frame = CGRectMake(CGRectGetMaxX(img_courseicon.frame)+10,CGRectGetMaxY(img_line3.frame)+10,150, 20);
    lbl_dietary_ristriction.text = @"Dietary Restrictions";
    lbl_dietary_ristriction .font = [UIFont fontWithName:kFontBold size:15];
    lbl_dietary_ristriction .textColor = [UIColor blackColor];
    lbl_dietary_ristriction .backgroundColor = [UIColor clearColor];
    [img_rect1 addSubview:lbl_dietary_ristriction];
    
    
    UILabel *lbl_diet_items = [[UILabel alloc]init];
    lbl_diet_items .frame = CGRectMake(CGRectGetMaxX(img_courseicon.frame)+10,CGRectGetMaxY(lbl_dietary_ristriction.frame),240, 50);
    lbl_diet_items.numberOfLines = 2;
    lbl_diet_items.text = [NSString stringWithFormat:@"%@",[[[ary_dishdetaillist objectAtIndex:0] valueForKey:@"ChefDetail"] valueForKey:@"Dietary_Restrictions"]];
    lbl_diet_items .font = [UIFont fontWithName:kFont size:13];
    lbl_diet_items .textColor = [UIColor blackColor];
    lbl_diet_items .backgroundColor = [UIColor clearColor];
    [img_rect1 addSubview:lbl_diet_items];
    
    UIImageView *img_line4 = [[UIImageView alloc]init];
    img_line4 .frame = CGRectMake(15,CGRectGetMaxY(lbl_diet_items.frame)+10, WIDTH-45,1);
    [img_line4  setImage:[UIImage imageNamed:@"line@2x.png"]];
    img_line4.backgroundColor = [UIColor lightGrayColor];
    [img_line4   setUserInteractionEnabled:YES];
    [img_rect1 addSubview:img_line4];
    
    UIImageView *img_courseicon2 = [[UIImageView alloc]init];
    img_courseicon2 .frame = CGRectMake(15,CGRectGetMaxY(img_line4.frame)+10,40,40);
    [img_courseicon2  setImage:[UIImage imageNamed:@"food-icon@2x.png"]];
    img_courseicon2.backgroundColor = [UIColor clearColor];
    [img_courseicon2   setUserInteractionEnabled:YES];
    [img_rect1 addSubview:img_courseicon2];
    
    
    UILabel *lbl_cuisines = [[UILabel alloc]init];
    lbl_cuisines .frame = CGRectMake(CGRectGetMaxX(img_courseicon.frame)+10,CGRectGetMaxY(img_line4.frame)+10,150, 20);
    lbl_cuisines.text = @"Cuisines";
    lbl_cuisines .font = [UIFont fontWithName:kFontBold size:15];
    lbl_cuisines .textColor = [UIColor blackColor];
    lbl_cuisines .backgroundColor = [UIColor clearColor];
    [img_rect1 addSubview:lbl_cuisines];
    
    
    UILabel *lbl_cuisines_items = [[UILabel alloc]init];
    lbl_cuisines_items .frame = CGRectMake(CGRectGetMaxX(img_courseicon.frame)+10,CGRectGetMaxY(lbl_cuisines.frame),240, 50);
    lbl_cuisines_items.numberOfLines = 2;
    lbl_cuisines_items.text = [NSString stringWithFormat:@"%@",[[[ary_dishdetaillist objectAtIndex:0] valueForKey:@"ChefDetail"] valueForKey:@"Cuisine_Cooked"]];
    lbl_cuisines_items .font = [UIFont fontWithName:kFont size:13];
    lbl_cuisines_items .textColor = [UIColor blackColor];
    lbl_cuisines_items .backgroundColor = [UIColor clearColor];
    [img_rect1 addSubview:lbl_cuisines_items];
    
    
    UIImageView *img_line5 = [[UIImageView alloc]init];
    img_line5 .frame = CGRectMake(15,CGRectGetMaxY(lbl_cuisines_items.frame)+10, WIDTH-45,1);
    [img_line5  setImage:[UIImage imageNamed:@"line@2x.png"]];
    img_line5.backgroundColor = [UIColor lightGrayColor];
    [img_line5   setUserInteractionEnabled:YES];
    [img_rect1 addSubview:img_line5];
    
    UIImageView *img_calender = [[UIImageView alloc]init];
    img_calender .frame = CGRectMake(15,CGRectGetMaxY(img_line5.frame)+10,40,40);
    [img_calender  setImage:[UIImage imageNamed:@"img-calender@2x.png"]];
    img_calender.backgroundColor = [UIColor clearColor];
    [img_calender   setUserInteractionEnabled:YES];
    [img_rect1 addSubview:img_calender];
    
    
    UILabel *lbl_date = [[UILabel alloc]init];
    lbl_date .frame = CGRectMake(CGRectGetMaxX(img_calender.frame)+10,CGRectGetMaxY(img_line5.frame)+10,250, 20);
    lbl_date.text = @"Availabile Date & Time";
    lbl_date .font = [UIFont fontWithName:kFontBold size:15];
    lbl_date .textColor = [UIColor blackColor];
    lbl_date .backgroundColor = [UIColor clearColor];
    [img_rect1 addSubview:lbl_date];
    
    
    UILabel *lbl_available_dates = [[UILabel alloc]init];
    lbl_available_dates .frame = CGRectMake(CGRectGetMaxX(img_calender.frame)+10,CGRectGetMaxY(lbl_date.frame),280, 25);
    lbl_available_dates.text = @"02/07/15-05/07/15 2pm-4pm";
    lbl_available_dates .font = [UIFont fontWithName:kFont size:13];
    lbl_available_dates .textColor = [UIColor blackColor];
    lbl_available_dates .backgroundColor = [UIColor clearColor];
    [img_rect1 addSubview:lbl_available_dates];
    
    
    
    
    
    
    
    
    
    
    
    
    
    view_for_reviews = [[UIView alloc]init];
    view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(btn_reviews.frame),WIDTH,HEIGHT-350);
    view_for_reviews.backgroundColor=[UIColor orangeColor];
    [view_for_reviews setUserInteractionEnabled:YES];
    [scroll  addSubview: view_for_reviews];
    view_for_reviews.hidden = YES;
    
    
    img_rect = [[UIImageView alloc]init];
    img_rect .frame = CGRectMake(10,10,WIDTH-20, 75);
    [img_rect setImage:[UIImage imageNamed:@"img_bg@2x.png"]];
    img_rect.backgroundColor = [UIColor whiteColor];
    [img_rect setUserInteractionEnabled:YES];
    [view_for_reviews addSubview:img_rect ];
    
    reviews_in_rat = [[UILabel alloc]init];
    reviews_in_rat .frame = CGRectMake(10,7,70, 13);
    reviews_in_rat.text = @"177";
    reviews_in_rat .font = [UIFont fontWithName:kFontBold size:13];
    //text_cuisen .textColor = [UIColor redColor];
    reviews_in_rat .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:reviews_in_rat];
    
    
    UILabel *lbl_reviews_rate = [[UILabel alloc]init];
    lbl_reviews_rate .frame = CGRectMake(CGRectGetMidX(reviews_in_rat.frame),7,100, 13);
    lbl_reviews_rate.text = @"Reviews";
    lbl_reviews_rate .font = [UIFont fontWithName:kFont size:11];
    //text_cuisen .textColor = [UIColor redColor];
    lbl_reviews_rate .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:lbl_reviews_rate];
    
    
    UIImageView *img_thumb = [[UIImageView alloc]init];
    img_thumb.frame = CGRectMake(CGRectGetMaxX(lbl_reviews_rate.frame)+60,5, 20,20);
    [img_thumb setImage:[UIImage imageNamed:@"thumb-icon@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_thumb  setUserInteractionEnabled:YES];
    [img_rect addSubview:img_thumb ];
    
    UILabel *label_over_all = [[UILabel alloc]init];
    label_over_all .frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+10,9,50, 15);
    label_over_all .text = @"Overall:";
    label_over_all  .font = [UIFont fontWithName:kFont size:13];
    label_over_all   .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    label_over_all  .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:label_over_all ];
    
    
    label_over_all_rateing = [[UILabel alloc]init];
    label_over_all_rateing .frame = CGRectMake(CGRectGetMaxX(label_over_all.frame)+5,9,40, 15);
    label_over_all_rateing .text = @"80";
    label_over_all_rateing  .font = [UIFont fontWithName:kFontBold size:13];
    label_over_all_rateing   .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    label_over_all_rateing  .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:label_over_all_rateing];
    
    
    
    UIImageView *img_line = [[UIImageView alloc]init];
    img_line .frame = CGRectMake(10,35, WIDTH-40,0.3);
    [img_line  setImage:[UIImage imageNamed:@"line@2x.png"]];
    img_line.backgroundColor = [UIColor lightGrayColor];
    [img_line   setUserInteractionEnabled:YES];
    [img_rect addSubview:img_line  ];
    
    UILabel *label_test_in_rect = [[UILabel alloc]init];
    label_test_in_rect .frame = CGRectMake(10,CGRectGetMaxY(img_line.frame)+20,40, 15);
    label_test_in_rect.text = @"Taste:";
    label_test_in_rect .font = [UIFont fontWithName:kFont size:13];
    label_test_in_rect  .textColor = [UIColor blackColor];
    label_test_in_rect .backgroundColor = [UIColor redColor];
    [img_rect addSubview:label_test_in_rect ];
    
    
    
    label_taste_val = [[UILabel alloc]init];
    label_taste_val .frame = CGRectMake(CGRectGetMidY(label_test_in_rect.frame)-10,CGRectGetMaxY(img_line.frame)+13,30, 30);
    label_taste_val .text = @"75%";
    label_taste_val   .font = [UIFont fontWithName:kFontBold size:13];
    label_taste_val   .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    label_taste_val  .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:label_taste_val];
    
    
    
    
    UILabel *label_appeal = [[UILabel alloc]init];
    label_appeal .frame = CGRectMake(CGRectGetMaxX(label_taste_val.frame)+40,CGRectGetMaxY(img_line.frame)+20,60,15);
    label_appeal .text = @"Appeal:";
    label_appeal   .font = [UIFont fontWithName:kFont size:13];
    label_appeal   .textColor = [UIColor blackColor];
    label_appeal  .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:label_appeal  ];
    
    
    label_appeal_value = [[UILabel alloc]init];
    label_appeal_value .frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-5,CGRectGetMaxY(img_line.frame)+13,30, 30);
    label_appeal_value .text = @"85%";
    label_appeal_value   .font = [UIFont fontWithName:kFontBold size:13];
    label_appeal_value   .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    label_appeal_value  .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:label_appeal_value];
    
    
    UILabel *label_value = [[UILabel alloc]init];
    label_value .frame = CGRectMake(CGRectGetMaxX(label_appeal_value.frame)+40,CGRectGetMaxY(img_line.frame)+20,50,15);
    label_value .text = @"Value:";
    label_value .font = [UIFont fontWithName:kFont size:13];
    label_value .textColor = [UIColor blackColor];
    label_value .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:label_value];
    
    
    
    label_value_value = [[UILabel alloc]init];
    label_value_value .frame = CGRectMake(CGRectGetMaxX(label_value.frame),CGRectGetMaxY(img_line.frame)+13,30, 30);
    label_value_value .text = @"85%";
    label_value_value   .font = [UIFont fontWithName:kFontBold size:13];
    label_value_value   .textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    label_value_value  .backgroundColor = [UIColor clearColor];
    [img_rect addSubview:label_value_value];
    
    
    
#pragma mark Tableview
    
    img_table= [[UITableView alloc] init ];
    img_table.frame  = CGRectMake(10,CGRectGetMaxY(img_rect.frame)+10,WIDTH-20,HEIGHT-450);
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    img_table.backgroundColor = [UIColor brownColor];
    [view_for_reviews addSubview:img_table];
    
    
    
    
    
    if (IS_IPHONE_6Plus)
    {
        
        scroll.frame = CGRectMake(0, 48, WIDTH,HEIGHT);
        img_bg .frame = CGRectMake(0,0, WIDTH, 250);
        text_dish_name  .frame = CGRectMake(120,150,200,30);
        btn_fb.frame = CGRectMake(20,210,40,40);
        
        btn_tw.frame = CGRectMake(CGRectGetMaxX(btn_fb.frame)+5,210,40,40);
        btn_cm.frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+5,210,40,40);
        text_rate  .frame = CGRectMake(WIDTH-80,215,200,30);
        text_dish_details.frame = CGRectMake(40,CGRectGetMaxY(img_bg.frame)+20,200, 15);
        img_strip.frame = CGRectMake(40, CGRectGetMaxY(text_dish_details.frame)+5, 80, 2);
        
        btn_food_details.frame = CGRectMake(0,CGRectGetMaxY(img_bg.frame), WIDTH/2,50);
        text_reviews .frame = CGRectMake(CGRectGetMaxX(btn_food_details.frame)+40,CGRectGetMaxY(img_bg.frame)+20,70, 15);
        number_of_reviews.frame = CGRectMake(CGRectGetMaxX(text_reviews.frame)-5,CGRectGetMaxY(img_bg.frame)+15,50, 25);
        img_percentage_tag.frame = CGRectMake(CGRectGetMidX(number_of_reviews.frame),CGRectGetMaxY(img_bg.frame)+10, 30, 18);
        img_strip1.frame = CGRectMake(CGRectGetMaxX(img_strip.frame)+125, CGRectGetMaxY(text_reviews.frame)+5, 85, 2);
        btn_reviews.frame = CGRectMake(WIDTH/2+10,CGRectGetMaxY(img_bg.frame),WIDTH/2,50);
        
        
        view_for_food_details.frame=CGRectMake(0,CGRectGetMaxY(btn_reviews.frame),WIDTH,HEIGHT-250);
        img_rect1 .frame = CGRectMake(10,10,WIDTH-20, 450);
        txtview_message.frame = CGRectMake(10,5, WIDTH-40,55);
        img_line2 .frame = CGRectMake(15,CGRectGetMaxY(txtview_message.frame)+10, WIDTH-45,1);
        img_key .frame = CGRectMake(15,CGRectGetMaxY(img_line2.frame)+10,40,40);
        lbl_keyword .frame = CGRectMake(CGRectGetMaxX(img_key.frame)+10,CGRectGetMaxY(img_line2.frame)+10,150, 20);
        lbl_keyword_items .frame = CGRectMake(CGRectGetMaxX(img_key.frame)+10,CGRectGetMaxY(lbl_keyword.frame),280, 25);
        img_line3 .frame = CGRectMake(15,CGRectGetMaxY(img_key.frame)+10, WIDTH-45,1);
        img_courseicon .frame = CGRectMake(15,CGRectGetMaxY(img_line3.frame)+10,40,40);
        lbl_dietary_ristriction .frame = CGRectMake(CGRectGetMaxX(img_courseicon.frame)+10,CGRectGetMaxY(img_line3.frame)+10,150, 20);
        lbl_diet_items .frame = CGRectMake(CGRectGetMaxX(img_courseicon.frame)+10,CGRectGetMaxY(lbl_dietary_ristriction.frame),300, 50);
        img_line4 .frame = CGRectMake(15,CGRectGetMaxY(lbl_diet_items.frame)+10, WIDTH-45,1);
        img_courseicon2 .frame = CGRectMake(15,CGRectGetMaxY(img_line4.frame)+10,40,40);
        lbl_cuisines .frame = CGRectMake(CGRectGetMaxX(img_courseicon.frame)+10,CGRectGetMaxY(img_line4.frame)+10,150, 20);
        lbl_cuisines_items .frame = CGRectMake(CGRectGetMaxX(img_courseicon.frame)+10,CGRectGetMaxY(lbl_cuisines.frame),300, 50);
        img_line5 .frame = CGRectMake(15,CGRectGetMaxY(lbl_cuisines_items.frame)+10, WIDTH-45,1);
        img_calender .frame = CGRectMake(15,CGRectGetMaxY(img_line5.frame)+10,40,40);
        lbl_date .frame = CGRectMake(CGRectGetMaxX(img_calender.frame)+10,CGRectGetMaxY(img_line5.frame)+10,250, 20);
        lbl_available_dates .frame = CGRectMake(CGRectGetMaxX(img_calender.frame)+10,CGRectGetMaxY(lbl_date.frame),280, 25);
        
        
        view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(btn_reviews.frame),WIDTH,HEIGHT-350);
        img_rect .frame = CGRectMake(10,10,WIDTH-20, 75);
        reviews_in_rat .frame = CGRectMake(10,7,70, 13);
        lbl_reviews_rate .frame = CGRectMake(CGRectGetMidX(reviews_in_rat.frame),7,100, 13);
        img_thumb.frame = CGRectMake(CGRectGetMaxX(lbl_reviews_rate.frame)+110,5, 20,20);
        label_over_all .frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+10,9,50, 15);
        label_over_all_rateing .frame = CGRectMake(CGRectGetMaxX(label_over_all.frame)+5,9,40, 15);
        img_line .frame = CGRectMake(10,35, WIDTH-40,0.6);
        label_test_in_rect .frame = CGRectMake(10,CGRectGetMaxY(img_line.frame)+20,40, 15);
        label_taste_val .frame = CGRectMake(CGRectGetMidY(label_test_in_rect.frame)-10,CGRectGetMaxY(img_line.frame)+13,30, 30);
        label_appeal .frame = CGRectMake(CGRectGetMaxX(label_taste_val.frame)+70,CGRectGetMaxY(img_line.frame)+20,60,15);
        label_appeal_value .frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-5,CGRectGetMaxY(img_line.frame)+13,30, 30);
        label_value .frame = CGRectMake(CGRectGetMaxX(label_appeal_value.frame)+40,CGRectGetMaxY(img_line.frame)+20,50,15);
        label_value_value .frame = CGRectMake(CGRectGetMaxX(label_value.frame),CGRectGetMaxY(img_line.frame)+13,30, 30);
        img_table.frame  = CGRectMake(10,CGRectGetMaxY(img_rect.frame)+10,WIDTH-20,HEIGHT-450);
        
    }
    else if (IS_IPHONE_6)
    {
        
        scroll.frame = CGRectMake(0, 48, WIDTH,HEIGHT);
        img_bg .frame = CGRectMake(0,0, WIDTH, 250);
        text_dish_name  .frame = CGRectMake(105,150,200,30);
        btn_fb.frame = CGRectMake(20,210,40,40);
        
        btn_tw.frame = CGRectMake(CGRectGetMaxX(btn_fb.frame)+5,210,40,40);
        btn_cm.frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+5,210,40,40);
        text_rate  .frame = CGRectMake(WIDTH-80,215,200,30);
        text_dish_details.frame = CGRectMake(40,CGRectGetMaxY(img_bg.frame)+20,200, 15);
        img_strip.frame = CGRectMake(40, CGRectGetMaxY(text_dish_details.frame)+5, 80, 2);
        btn_food_details.frame = CGRectMake(0,CGRectGetMaxY(img_bg.frame), WIDTH/2,50);
        text_reviews .frame = CGRectMake(CGRectGetMaxX(btn_food_details.frame)+40,CGRectGetMaxY(img_bg.frame)+20,70, 15);
        number_of_reviews.frame = CGRectMake(CGRectGetMaxX(text_reviews.frame)-5,CGRectGetMaxY(img_bg.frame)+20,50, 15);
        img_percentage_tag.frame = CGRectMake(CGRectGetMidX(number_of_reviews.frame),CGRectGetMaxY(img_bg.frame)+10, 30, 18);
        img_strip1.frame = CGRectMake(CGRectGetMaxX(img_strip.frame)+110, CGRectGetMaxY(text_reviews.frame)+5, 57, 2);
        btn_reviews.frame = CGRectMake(WIDTH/2+10,CGRectGetMaxY(img_bg.frame),WIDTH/2,50);
        
        
        view_for_food_details.frame=CGRectMake(0,CGRectGetMaxY(btn_reviews.frame),WIDTH,HEIGHT-250);
        img_rect1 .frame = CGRectMake(10,10,WIDTH-20, 450);
        txtview_message.frame = CGRectMake(10,5, WIDTH-40,55);
        img_line2 .frame = CGRectMake(15,CGRectGetMaxY(txtview_message.frame)+10, WIDTH-45,1);
        img_key .frame = CGRectMake(15,CGRectGetMaxY(img_line2.frame)+10,40,40);
        lbl_keyword .frame = CGRectMake(CGRectGetMaxX(img_key.frame)+10,CGRectGetMaxY(img_line2.frame)+10,150, 20);
        lbl_keyword_items .frame = CGRectMake(CGRectGetMaxX(img_key.frame)+10,CGRectGetMaxY(lbl_keyword.frame),280, 25);
        img_line3 .frame = CGRectMake(15,CGRectGetMaxY(img_key.frame)+10, WIDTH-45,1);
        img_courseicon .frame = CGRectMake(15,CGRectGetMaxY(img_line3.frame)+10,40,40);
        lbl_dietary_ristriction .frame = CGRectMake(CGRectGetMaxX(img_courseicon.frame)+10,CGRectGetMaxY(img_line3.frame)+10,150, 20);
        lbl_diet_items .frame = CGRectMake(CGRectGetMaxX(img_courseicon.frame)+10,CGRectGetMaxY(lbl_dietary_ristriction.frame),240, 50);
        img_line4 .frame = CGRectMake(15,CGRectGetMaxY(lbl_diet_items.frame)+10, WIDTH-45,1);
        img_courseicon2 .frame = CGRectMake(15,CGRectGetMaxY(img_line4.frame)+10,40,40);
        lbl_cuisines .frame = CGRectMake(CGRectGetMaxX(img_courseicon.frame)+10,CGRectGetMaxY(img_line4.frame)+10,150, 20);
        lbl_cuisines_items .frame = CGRectMake(CGRectGetMaxX(img_courseicon.frame)+10,CGRectGetMaxY(lbl_cuisines.frame),270, 50);
        img_line5 .frame = CGRectMake(15,CGRectGetMaxY(lbl_cuisines_items.frame)+10, WIDTH-45,1);
        img_calender .frame = CGRectMake(15,CGRectGetMaxY(img_line5.frame)+10,40,40);
        lbl_date .frame = CGRectMake(CGRectGetMaxX(img_calender.frame)+10,CGRectGetMaxY(img_line5.frame)+10,250, 20);
        lbl_available_dates .frame = CGRectMake(CGRectGetMaxX(img_calender.frame)+10,CGRectGetMaxY(lbl_date.frame),280, 25);
        
        
        view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(btn_reviews.frame),WIDTH,HEIGHT-350);
        img_rect .frame = CGRectMake(10,10,WIDTH-20, 75);
        reviews_in_rat .frame = CGRectMake(10,7,70, 13);
        lbl_reviews_rate .frame = CGRectMake(CGRectGetMidX(reviews_in_rat.frame),7,100, 13);
        img_thumb.frame = CGRectMake(CGRectGetMaxX(lbl_reviews_rate.frame)+60,5, 20,20);
        label_over_all .frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+10,9,50, 15);
        label_over_all_rateing .frame = CGRectMake(CGRectGetMaxX(label_over_all.frame)+5,9,40, 15);
        img_line .frame = CGRectMake(10,35, WIDTH-40,0.3);
        label_test_in_rect .frame = CGRectMake(10,CGRectGetMaxY(img_line.frame)+20,40, 15);
        label_taste_val .frame = CGRectMake(CGRectGetMidY(label_test_in_rect.frame)-10,CGRectGetMaxY(img_line.frame)+13,30, 30);
        label_appeal .frame = CGRectMake(CGRectGetMaxX(label_taste_val.frame)+40,CGRectGetMaxY(img_line.frame)+20,60,15);
        label_appeal_value .frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-5,CGRectGetMaxY(img_line.frame)+13,30, 30);
        label_value .frame = CGRectMake(CGRectGetMaxX(label_appeal_value.frame)+40,CGRectGetMaxY(img_line.frame)+20,50,15);
        label_value_value .frame = CGRectMake(CGRectGetMaxX(label_value.frame),CGRectGetMaxY(img_line.frame)+13,30, 30);
        img_table.frame  = CGRectMake(10,CGRectGetMaxY(img_rect.frame)+10,WIDTH-20,HEIGHT-450);
        
    }
    
    else
    {
        
        scroll.frame = CGRectMake(0, 45, WIDTH,500);
        img_bg .frame = CGRectMake(0,0, WIDTH, 150);
        text_dish_name  .frame = CGRectMake(5,80,WIDTH-10, 20);
        btn_fb.frame = CGRectMake(20,118,25,25);
        btn_tw.frame = CGRectMake(CGRectGetMaxX(btn_fb.frame)+5,118,25,25);
        btn_cm.frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+5,118,25,25);
        
        text_rate  .frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+167,118,200,25);
        //text_rate .frame = CGRectMake(WIDTH-60,215,200,30);
        text_dish_details.frame = CGRectMake(40,CGRectGetMaxY(img_bg.frame)+20,200, 15);
        img_strip.frame = CGRectMake(40, CGRectGetMaxY(text_dish_details.frame)+5, 80, 2);
        btn_food_details.frame = CGRectMake(0,CGRectGetMaxY(img_bg.frame)+10, WIDTH/2, 25);
        text_reviews .frame = CGRectMake(CGRectGetMaxX(btn_food_details.frame)+40,CGRectGetMaxY(img_bg.frame)+20,70, 15);
        number_of_reviews.frame = CGRectMake(CGRectGetMaxX(text_reviews.frame)-5,CGRectGetMaxY(img_bg.frame)+20,50, 15);
        img_percentage_tag.frame = CGRectMake(CGRectGetMidX(number_of_reviews.frame),CGRectGetMaxY(img_bg.frame)+10, 30, 18);
        img_strip1.frame = CGRectMake(CGRectGetMaxX(img_strip.frame)+80, CGRectGetMaxY(text_reviews.frame)+5, 62, 2);
        btn_reviews.frame = CGRectMake(WIDTH/2+10,CGRectGetMaxY(img_bg.frame),WIDTH/2,50);
        
        
        
        //        scroll.frame = CGRectMake(0, 45, WIDTH,500);
        //        img_bg .frame = CGRectMake(0,0, WIDTH, 150);
        //        text_dish_name  .frame = CGRectMake(5,88,WIDTH-10, 15);
        //        img_black .frame = CGRectMake(WIDTH-150,CGRectGetMaxY( img_header.frame)-30,150,30);
        //        icon_location .frame = CGRectMake(0,0,30,30);
        //        lbl_km.frame = CGRectMake(25,-5, 150,45);
        //        btn_favorite.frame = CGRectMake(18,118,25,25);
        //        btn_fb.frame = CGRectMake(CGRectGetMaxX(btn_favorite.frame)+15,118,25,25);
        //        btn_tw.frame = CGRectMake(CGRectGetMaxX(btn_fb.frame)+5,118,25,25);
        //        btn_cm.frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+5,118,25,25);
        //        text_rate  .frame = CGRectMake(CGRectGetMaxX(btn_tw.frame)+150,118,200,25);
        //        text_food_details.frame = CGRectMake(40,CGRectGetMaxY(img_bg.frame)+10,200, 15);
        //        btn_food_details.frame = CGRectMake(0,CGRectGetMaxY(img_bg.frame)+10, WIDTH/2, 25);
        //        img_strip.frame = CGRectMake(40, CGRectGetMaxY(img_bg.frame)+29, 80, 2);
        //        text_reviews .frame = CGRectMake(200,CGRectGetMaxY(img_bg.frame)+10,200, 15);
        //        btn_reviews.frame = CGRectMake(WIDTH/2+10,CGRectGetMaxY(img_bg.frame)+10,WIDTH/2, 25);
        //        img_strip1.frame = CGRectMake(200, CGRectGetMaxY(img_bg.frame)+29, 57, 2);
        //        number_of_reviews.frame = CGRectMake(260,165,200, 10);
        //        img_percentage_tag.frame = CGRectMake(273,151, 25, 13);
        
        
        view_for_food_details.frame=CGRectMake(0,CGRectGetMaxY(btn_reviews.frame),WIDTH,HEIGHT-200);
        img_rect1 .frame = CGRectMake(10,10,WIDTH-20, 450);
        txtview_message.frame = CGRectMake(10,5, WIDTH-40,55);
        img_line2 .frame = CGRectMake(15,CGRectGetMaxY(txtview_message.frame)+10, WIDTH-45,1);
        img_key .frame = CGRectMake(15,CGRectGetMaxY(img_line2.frame)+10,40,40);
        lbl_keyword .frame = CGRectMake(CGRectGetMaxX(img_key.frame)+10,CGRectGetMaxY(img_line2.frame)+10,150, 20);
        lbl_keyword_items .frame = CGRectMake(CGRectGetMaxX(img_key.frame)+10,CGRectGetMaxY(lbl_keyword.frame),280, 25);
        img_line3 .frame = CGRectMake(15,CGRectGetMaxY(img_key.frame)+10, WIDTH-45,1);
        img_courseicon .frame = CGRectMake(15,CGRectGetMaxY(img_line3.frame)+10,40,40);
        lbl_dietary_ristriction .frame = CGRectMake(CGRectGetMaxX(img_courseicon.frame)+10,CGRectGetMaxY(img_line3.frame)+10,150, 20);
        lbl_diet_items .frame = CGRectMake(CGRectGetMaxX(img_courseicon.frame)+10,CGRectGetMaxY(lbl_dietary_ristriction.frame),240, 50);
        img_line4 .frame = CGRectMake(15,CGRectGetMaxY(lbl_diet_items.frame)+10, WIDTH-45,1);
        img_courseicon2 .frame = CGRectMake(15,CGRectGetMaxY(img_line4.frame)+10,40,40);
        lbl_cuisines .frame = CGRectMake(CGRectGetMaxX(img_courseicon.frame)+10,CGRectGetMaxY(img_line4.frame)+10,150, 20);
        lbl_cuisines_items .frame = CGRectMake(CGRectGetMaxX(img_courseicon.frame)+10,CGRectGetMaxY(lbl_cuisines.frame),240, 50);
        img_line5 .frame = CGRectMake(15,CGRectGetMaxY(lbl_cuisines_items.frame)+10, WIDTH-45,1);
        img_calender .frame = CGRectMake(15,CGRectGetMaxY(img_line5.frame)+10,40,40);
        lbl_date .frame = CGRectMake(CGRectGetMaxX(img_calender.frame)+10,CGRectGetMaxY(img_line5.frame)+10,250, 20);
        lbl_available_dates .frame = CGRectMake(CGRectGetMaxX(img_calender.frame)+10,CGRectGetMaxY(lbl_date.frame),280, 25);
        
        
        view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(btn_reviews.frame),WIDTH,HEIGHT-300);
        img_rect .frame = CGRectMake(10,10,WIDTH-20, 75);
        reviews_in_rat .frame = CGRectMake(10,7,70, 13);
        lbl_reviews_rate .frame = CGRectMake(CGRectGetMidX(reviews_in_rat.frame),7,100, 13);
        img_thumb.frame = CGRectMake(CGRectGetMaxX(lbl_reviews_rate.frame)+50,5, 20,20);
        label_over_all .frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+10,9,50, 15);
        label_over_all_rateing .frame = CGRectMake(CGRectGetMaxX(label_over_all.frame)+5,9,40, 15);
        img_line .frame = CGRectMake(10,35, WIDTH-40,0.3);
        label_test_in_rect .frame = CGRectMake(10,CGRectGetMaxY(img_line.frame)+20,40, 15);
        label_taste_val .frame = CGRectMake(CGRectGetMidY(label_test_in_rect.frame)-10,CGRectGetMaxY(img_line.frame)+13,30, 30);
        label_appeal .frame = CGRectMake(CGRectGetMaxX(label_taste_val.frame)+25,CGRectGetMaxY(img_line.frame)+20,60,15);
        label_appeal_value .frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-5,CGRectGetMaxY(img_line.frame)+13,30, 30);
        label_value .frame = CGRectMake(CGRectGetMaxX(label_appeal_value.frame)+20,CGRectGetMaxY(img_line.frame)+20,50,15);
        label_value_value .frame = CGRectMake(CGRectGetMaxX(label_value.frame),CGRectGetMaxY(img_line.frame)+13,30, 30);
        img_table.frame  = CGRectMake(10,CGRectGetMaxY(img_rect.frame)+10,WIDTH-20,HEIGHT-400);
        
    }
    
    
    
    
    
    
    
    
}


#pragma tableview delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == table_on_drop_down)
    {
        return 3;
    }
    return [ary_reviewslist count];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table_on_drop_down)
    {
        return 40;
    }
    
    return 170;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    if (tableView ==img_table)
    {
        cell.backgroundColor = [UIColor clearColor];
        
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        //img_cellBackGnd.frame =  CGRectMake(0,40, 300, 150);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        UIImageView *img_user = [[UIImageView alloc] init];
        //   img_user.frame = CGRectMake(20,0, 50,  50 );
        [img_user setImage:[UIImage imageNamed:@"user-img@2x.png"]];
        
        [img_cellBackGnd addSubview:img_user];
        
        
        UIImageView *img_flag = [[UIImageView alloc] init];
        //   img_flag.frame = CGRectMake(260,10, 20,  25 );
        [img_flag setImage:[UIImage imageNamed:@"icon-flag@2x.png"]];
        [img_cellBackGnd addSubview:img_flag];
        
        
        
        //        label_favorite_value.text = [NSString stringWithFormat:@"%@",[[ary_ReviewsList objectAtIndex:0] valueForKey:@"favorite_by_user"]];
        //        label_total_reviews_value.text = [NSString stringWithFormat:@"%@",[[ary_ReviewsList objectAtIndex:0] valueForKey:@"total_revies"]];
        //        label_overall_rating_value.text = [NSString stringWithFormat:@"%@",[[ary_ReviewsList objectAtIndex:0] valueForKey:@"overall_rating"]];
        
        
        //        ary_UserReview
        
        UILabel *user_name = [[UILabel alloc]init];
        //    user_name  .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,5,200,25);
        user_name.text = [NSString stringWithFormat:@"%@",[[ary_reviewslist objectAtIndex:indexPath.row] valueForKey:@"chefname"]];
        //        user_name .text = @"Jonathan Timothy";
        user_name .font = [UIFont fontWithName:kFont size:13];
        user_name.textColor = [UIColor blackColor];
        user_name .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:user_name];
        
        UILabel *taste = [[UILabel alloc]init];
        //  taste .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,20,200,25);
        taste .text = @"Taste:";
        taste .font = [UIFont fontWithName:kFont size:10];
        taste.textColor = [UIColor blackColor];
        taste .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:taste];
        
        UILabel *taste_value = [[UILabel alloc]init];
        //   taste_value .frame = CGRectMake(CGRectGetMaxX(taste.frame)-170,20,200,25);
        //        taste_value .text = @"7";
        taste_value.text = [NSString stringWithFormat:@"%@",[[ary_reviewslist objectAtIndex:indexPath.row] valueForKey:@"taste_rating"]];
        
        taste_value .font = [UIFont fontWithName:kFont size:10];
        taste_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        taste_value .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:taste_value];
        
        
        UILabel *text_appeal = [[UILabel alloc]init];
        //   text_appeal .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+70,20,200,25);
        text_appeal.text = @"Appeal:";
        text_appeal .font = [UIFont fontWithName:kFont size:10];
        text_appeal.textColor = [UIColor blackColor];
        text_appeal .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:text_appeal];
        
        UILabel *appeal_value = [[UILabel alloc]init];
        //    appeal_value .frame = CGRectMake(CGRectGetMaxX(text_appeal .frame)-160,20,200,25);
        //        appeal_value .text = @"8";
        appeal_value.text = [NSString stringWithFormat:@"%@",[[ary_reviewslist objectAtIndex:indexPath.row] valueForKey:@"total_revies"]];
        
        appeal_value .font = [UIFont fontWithName:kFont size:10];
        appeal_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];       appeal_value .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:appeal_value];
        
        
        UILabel *text_value = [[UILabel alloc]init];
        //   text_value .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+135,20,200,25);
        text_value.text = @"Value:";
        text_value .font = [UIFont fontWithName:kFont size:10];
        text_value.textColor = [UIColor blackColor];
        text_value .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:text_value];
        
        UILabel *value_value = [[UILabel alloc]init];
        //  value_value.frame = CGRectMake(CGRectGetMaxX(text_value.frame)-165,20,200,25);
        //        value_value .text = @"9";
        value_value.text = [NSString stringWithFormat:@"%@",[[ary_reviewslist objectAtIndex:indexPath.row] valueForKey:@"value_rating"]];
        
        value_value .font = [UIFont fontWithName:kFont size:10];
        value_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        value_value .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:value_value];
        
        
        
        UILabel *text_date = [[UILabel alloc]init];
        //   text_date .frame = CGRectMake(20,CGRectGetMaxY(taste_value .frame)+10,200,25);
        text_date.text = @"18 May 2015";
        text_date .font = [UIFont fontWithName:kFont size:10];
        text_date.textColor = [UIColor blackColor];
        text_date.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:text_date];
        
        
        UILabel *text_time = [[UILabel alloc]init];
        //   text_time .frame = CGRectMake(250,CGRectGetMaxY(text_date  .frame)-25,200,25);
        text_time.text = @"2:30pm";
        text_time .font = [UIFont fontWithName:kFont size:10];
        text_time.textColor = [UIColor blackColor];
        text_time.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:text_time];
        
        UITextView *txtview_message = [[UITextView alloc]init];
        //  txtview_message.frame = CGRectMake(5,CGRectGetMaxY(img_user.frame)+25, 295,50);
        txtview_message.scrollEnabled = YES;
        txtview_message.userInteractionEnabled = YES;
        txtview_message.font = [UIFont fontWithName:kFont size:10];
        txtview_message.backgroundColor = [UIColor clearColor];
        txtview_message.delegate = self;
        txtview_message.textColor = [UIColor blackColor];
        txtview_message.text =@"Lorem ipsumdotor sit amet.ne dicit virits corpor a per,ut illum liberavisse vet.At mei vidit conse cteture.in vitae definitionem per.tate vetear cliquado ei vim.";
        [img_cellBackGnd addSubview:txtview_message];
        
        
        UILabel *text_replied = [[UILabel alloc]init];
        //   text_replied .frame = CGRectMake(250,CGRectGetMaxY(txtview_message  .frame)-3,200,25);
        text_replied.text = @"Replied";
        text_replied .font = [UIFont fontWithName:kFont size:10];
        text_replied.textColor = [UIColor blackColor];
        text_replied.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd  addSubview:text_replied];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,40, WIDTH-20, 165);
            img_user.frame = CGRectMake(20,10, 50,  50 );
            img_flag.frame = CGRectMake(350,10, 20,  25 );
            user_name  .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,5,200,25);
            taste .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,20,200,25);
            
            
            taste_value .frame = CGRectMake(CGRectGetMaxX(taste.frame)-170,20,200,25);
            text_appeal .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+95,20,200,25);
            appeal_value .frame = CGRectMake(CGRectGetMaxX(text_appeal .frame)-160,20,200,25);
            text_value .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+185,20,200,25);
            value_value.frame = CGRectMake(CGRectGetMaxX(text_value.frame)-165,20,200,25);
            text_date .frame = CGRectMake(20,CGRectGetMaxY(taste_value .frame)+20,200,25);
            
            text_time .frame = CGRectMake(340,CGRectGetMaxY(text_date  .frame)-25,200,25);
            txtview_message.frame = CGRectMake(5,CGRectGetMaxY(img_user.frame)+30, WIDTH-40,55);
            text_replied .frame = CGRectMake(340,CGRectGetMaxY(txtview_message  .frame),200,25);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,40, WIDTH-20, 165);
            img_user.frame = CGRectMake(20,10, 50,  50 );
            img_flag.frame = CGRectMake(320,10, 20,  25 );
            user_name  .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,5,200,25);
            taste .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,20,200,25);
            
            
            taste_value .frame = CGRectMake(CGRectGetMaxX(taste.frame)-170,20,200,25);
            text_appeal .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+95,20,200,25);
            appeal_value .frame = CGRectMake(CGRectGetMaxX(text_appeal .frame)-160,20,200,25);
            text_value .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+185,20,200,25);
            value_value.frame = CGRectMake(CGRectGetMaxX(text_value.frame)-165,20,200,25);
            text_date .frame = CGRectMake(20,CGRectGetMaxY(taste_value .frame)+20,200,25);
            
            text_time .frame = CGRectMake(305,CGRectGetMaxY(text_date  .frame)-25,200,25);
            txtview_message.frame = CGRectMake(5,CGRectGetMaxY(img_user.frame)+30, WIDTH-40,55);
            text_replied .frame = CGRectMake(305,CGRectGetMaxY(txtview_message.frame)-5,200,25);
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,40, 300, 150);
            img_user.frame = CGRectMake(20,0, 50,  50 );
            img_flag.frame = CGRectMake(260,10, 20,  25 );
            user_name  .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,5,200,25);
            taste .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+10,20,200,25);
            
            
            taste_value .frame = CGRectMake(CGRectGetMaxX(taste.frame)-170,20,200,25);
            text_appeal .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+70,20,200,25);
            appeal_value .frame = CGRectMake(CGRectGetMaxX(text_appeal .frame)-160,20,200,25);
            text_value .frame = CGRectMake(CGRectGetMaxX(img_user .frame)+135,20,200,25);
            value_value.frame = CGRectMake(CGRectGetMaxX(text_value.frame)-165,20,200,25);
            text_date .frame = CGRectMake(20,CGRectGetMaxY(taste_value .frame)+10,200,25);
            
            text_time .frame = CGRectMake(250,CGRectGetMaxY(text_date  .frame)-25,200,25);
            txtview_message.frame = CGRectMake(5,CGRectGetMaxY(img_user.frame)+25, 295,55);
            text_replied .frame = CGRectMake(250,CGRectGetMaxY(txtview_message  .frame)-3,200,25);
            
            
        }
        
        
        
    }
    
    else if (tableView == table_on_drop_down)
    {
        UIImageView*img_bg_for_first_tbl;
        
        img_bg_for_first_tbl = [[UIImageView alloc]init];
        img_bg_for_first_tbl.frame =  CGRectMake(0,5, 150, 40);
        img_bg_for_first_tbl.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_bg_for_first_tbl setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_first_tbl];
        
        UILabel* lbl_food_now_r_later;
        
        lbl_food_now_r_later = [[UILabel alloc]init];
        lbl_food_now_r_later .frame = CGRectMake(5,10,200, 15);
        lbl_food_now_r_later .text = [NSString stringWithFormat:@"%@",[array_head_names objectAtIndex:indexPath.row]];
        lbl_food_now_r_later .font = [UIFont fontWithName:kFontBold size:15];
        lbl_food_now_r_later .textColor = [UIColor blackColor];
        lbl_food_now_r_later .backgroundColor = [UIColor clearColor];
        [img_bg_for_first_tbl addSubview: lbl_food_now_r_later ];
        
    }
    
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView == table_on_drop_down)
    {
        if (indexPath.row == 0)
        {
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
            {
                SLComposeViewController *facebook = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                
                SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result)
                {
                    if (result == SLComposeViewControllerResultDone)
                    {
                    }
                    [facebook dismissViewControllerAnimated:YES completion:Nil];
                };
                facebook.completionHandler =myBlock;
                
                
                [facebook addImage:[UIImage imageNamed:@"57x57.png"]];
                [facebook setInitialText:@"Not86"];
                [self presentViewController:facebook animated:YES completion:nil];
                
            }
            else
            {
                UIAlertView*alert=   [[UIAlertView alloc] initWithTitle:@"Alert" message:@"You can't send a share right now, make sure your device has an internet connection and you have at least one facebook account setup" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                
                [alert show];
                
                //            [SVProgressHUD showErrorWithStatus:@"You can't send a share right now, make sure your device has an internet connection and you have at least one facebook account setup"];
                
            }
            
            
        }
        else if (indexPath.row == 1)
        {
            
            
            if ([ViewDishDetailsVC isAppInstalled])
            {
                
                UIImageView * imageView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 200,200)];
                [imageView setUserInteractionEnabled:TRUE];
                //        [imageView setImageWithURL:[NSURL URLWithString:[[ary_dishdetaillist objectAtIndex:0] valueForKey:@"DishImage"]]];
                
                imageView.image =  [UIImage imageNamed:@"57x57.png"];
                
                [self.view addSubview:imageView];
                imageView.hidden=YES;
                
                
                
                
                UIImage *image = imageView.image;
                
                NSString *description=  @"Not86";
                
                if ([MGInstagram isAppInstalled])
                {
                    //[self AFProductShare:str_productID];
                    [MGInstagram postImage:image withCaption:description inView:self.view];
                }
                else
                    [self.notInstalledAlert show];
                
                
                
                
            }
            else
            {
                [self.notInstalledAlert show];
                NSLog(@"app not installed");
                //[self InstagramPopUp];
                
                
                //[SVProgressHUD showErrorWithStatus:@"Your Device should have the Instagrm Application"];
            }
            
        }
        else if (indexPath.row ==2)
        {
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
            {
                // Create a compose view controller for the service type Twitter
                SLComposeViewController *mulitpartPost = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result)
                {
                    if (result == SLComposeViewControllerResultDone)
                    {
                    }
                    [mulitpartPost dismissViewControllerAnimated:YES completion:Nil];
                };
                mulitpartPost.completionHandler =myBlock;
                
                
                
                
                // Set the text of the tweet
                [mulitpartPost setInitialText:@"Not86"];
                
                [mulitpartPost addImage:[UIImage imageNamed:@"57x57.png"]];
                
                
                
                
                // Display the tweet sheet to the user
                [self presentViewController:mulitpartPost animated:YES completion:nil];
                
            }
            else
            {
                
                UIAlertView*alert=   [[UIAlertView alloc] initWithTitle:@"Alert" message:@"You can't send a share right now, make sure your device has an internet connection and you have at least one Twitter account setup" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                
                [alert show];
                
                //        [SVProgressHUD showErrorWithStatus:@"You can't send a share right now, make sure your device has an internet connection and you have at least one Twitter account setup"];
                
                //        [self callAlertView:@"Sorry" message:@"You can't send a share right now, make sure your device has an internet connection and you have at least one Twitter account setup"];
            }
            
        }
        
        
        [table_on_drop_down setHidden:YES];
    }
    
}

-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFontBold size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFontBold size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFontBold size:16.0f]];
}
-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}



#pragma mark Instagram

/////instagram///
-(void)getimageBtnClick
{
    
    UIImagePickerController* imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

+ (BOOL) isAppInstalled
{
    NSURL *appURL = [NSURL URLWithString:kInstagramAppURLString];
    return [[UIApplication sharedApplication] canOpenURL:appURL];
}


-(UIImage*)thumbnailFromView:(UIView*)_myView{
    return [self thumbnailFromView:_myView withSize:_myView.frame.size];
}

-(UIImage*)thumbnailFromView:(UIView*)_myView withSize:(CGSize)viewsize{
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0)) {
        // Retina display
        CGSize newSize = viewsize;
        newSize.height=newSize.height*2;
        newSize.width=newSize.width*2;
        viewsize=newSize;
    }
    
    UIGraphicsBeginImageContext(_myView.bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGContextSetShouldAntialias(context, YES);
    [_myView.layer renderInContext: context];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    CGSize size = _myView.frame.size;
    CGFloat scale = MAX(viewsize.width / size.width, viewsize.height / size.height);
    
    UIGraphicsBeginImageContext(viewsize);
    CGFloat width = size.width * scale;
    CGFloat height = size.height * scale;
    float dwidth = ((viewsize.width - width) / 2.0f);
    float dheight = ((viewsize.height - height) / 2.0f);
    CGRect rect = CGRectMake(dwidth, dheight, size.width * scale, size.height * scale);
    [image drawInRect:rect];
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newimg;
}

- (UIAlertView*) notInstalledAlert
{
    return [[UIAlertView alloc] initWithTitle:@"Instagram Not Installed!" message:@"Instagram must be installed on the device in order to post images" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
}




#pragma click events

-(void)click_on_back_arrow:(UIButton *)sender
{
    NSLog(@"click_on_back_btn");
    [self dismissViewControllerAnimated:NO completion:nil];
    
}
-(void)click_on_logo_btn:(UIButton *)sender
{
    NSLog(@"click_on_logo_btn");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)btn_edit_click:(UIButton *)sender
{
    
    if ([[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"dish_type"] isEqualToString:@"active"])
    {
        MenuScreenEditMealVC*vc = [MenuScreenEditMealVC new];
        vc.str_DishID = str_dishID;
        vc.ary_dishDetails = ary_dishdetaillist;
        vc.str_type = str_TYPE;
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    else{
        [self popup_Alertview:@"You are unable to edit or delete this dish/meal as you have a current active order on this dish/meal. Once you have served this item, you may edit/delete this dish/meal"];
    }
    
    
}
-(void)btn_fb_click:(UIButton *)sender
{
    NSLog(@"icon_fb Btn Click");
    
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *facebook = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result)
        {
            if (result == SLComposeViewControllerResultDone)
            {
            }
            [facebook dismissViewControllerAnimated:YES completion:Nil];
        };
        facebook.completionHandler =myBlock;
        
        
        [facebook addImage:[UIImage imageNamed:@"57x57.png"]];
        [facebook setInitialText:@"Not86"];
        [self presentViewController:facebook animated:YES completion:nil];
        
    }
    else
    {
        
        //            [SVProgressHUD showErrorWithStatus:@"You can't send a share right now, make sure your device has an internet connection and you have at least one facebook account setup"];
        
    }
    
}


-(void)btn_tw_click:(UIButton *)sender
{
    NSLog(@"icon_tw Btn Click");
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        // Create a compose view controller for the service type Twitter
        SLComposeViewController *mulitpartPost = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result)
        {
            if (result == SLComposeViewControllerResultDone)
            {
            }
            [mulitpartPost dismissViewControllerAnimated:YES completion:Nil];
        };
        mulitpartPost.completionHandler =myBlock;
        
        
        
        
        // Set the text of the tweet
        [mulitpartPost setInitialText:@"Not86"];
        
        [mulitpartPost addImage:[UIImage imageNamed:@"57x57.png"]];
        
        
        
        
        // Display the tweet sheet to the user
        [self presentViewController:mulitpartPost animated:YES completion:nil];
        
    }
    else
    {
        //        [SVProgressHUD showErrorWithStatus:@"You can't send a share right now, make sure your device has an internet connection and you have at least one Twitter account setup"];
        
        //        [self callAlertView:@"Sorry" message:@"You can't send a share right now, make sure your device has an internet connection and you have at least one Twitter account setup"];
    }
    
    
}


-(void)btn_cm_click:(UIButton *)sender
{
    NSLog(@"icon_cm Btn Click");
    
    if ([ViewDishDetailsVC isAppInstalled])
    {
        
        UIImageView * imageView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 200,200)];
        [imageView setUserInteractionEnabled:TRUE];
        [imageView setImageWithURL:[NSURL URLWithString:[[ary_dishdetaillist objectAtIndex:0] valueForKey:@"DishImage"]]];
        [self.view addSubview:imageView];
        imageView.hidden=YES;
        
        
        
        
        UIImage *image = imageView.image;
        
        NSString *description=  [[ary_dishdetaillist objectAtIndex:0] valueForKey:@"DishName"];
        
        if ([MGInstagram isAppInstalled])
        {
            //[self AFProductShare:str_productID];
            [MGInstagram postImage:image withCaption:description inView:self.view];
        }
        else
            [self.notInstalledAlert show];
        
        
        
        
    }
    else
    {
        [self.notInstalledAlert show];
        NSLog(@"app not installed");
        //[self InstagramPopUp];
        
        
        //[SVProgressHUD showErrorWithStatus:@"Your Device should have the Instagrm Application"];
    }
    
    
}


-(void)btn_food_details_click:(UIButton *)sender
{
    NSLog(@"btn_food_details_click");
    
    img_strip.hidden = NO;
    img_strip1.hidden = YES;
    
    view_for_food_details.hidden = NO;
    view_for_reviews.hidden = YES;
    [self AFUserDishDetails];
    
}

-(void)btn_reviews_click:(UIButton *)sender
{
    NSLog(@"btn_food_details_click");
    
    img_strip.hidden = YES;
    img_strip1.hidden = NO;
    
    view_for_food_details.hidden = YES;
    view_for_reviews.hidden = NO;
    
    [self AFDishDetailUserReviews];
    
    
}


#pragma UserDishDetails-functionality

-(void)AFUserDishDetails
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    NSDictionary *params =@{
                            
                            @"dish_id"                           :  str_dishID,
                            @"user_id"                          :[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"]
                            //                            @"page_no"                           :  @"1",
                            //                            @"role_type"                         :  @"user_profile",
                            //                            @"device_udid"                       :  UniqueAppID,
                            //                            @"device_token"                      :  @"Dev",
                            //                            @"device_type"                       :  @"1"
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:kUserDishDetails  parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseUserDishDetails:JSON];
    }
     
     
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error){
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFUserDishDetails];
                                         }
                                     }];
    [operation start];
    
}
-(void)ResponseUserDishDetails:(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    [ary_dishdetaillist removeAllObjects];
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        //text_rate.text =[NSString stringWithFormat:@"$%@", [[TheDict valueForKey:@"FoodDetail"] valueForKey:@"DishPrice"]];
        
        
        for (int i=0; i<[[TheDict valueForKey:@"FoodDetail"] count]; i++)
        {
            [ary_dishdetaillist addObject:[[TheDict valueForKey:@"FoodDetail"] objectAtIndex:i]];
        }
        
        [self integratebody];
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
    
}


# pragma mark UserReviews method

-(void)AFDishDetailUserReviews
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    NSDictionary *params =@{
                            
                            @"uid"                               :   [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"chef_id"                           :   str_dishUID,
                            @"lat"                               :   @"",
                            @"long"                              :   @"",
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kChefProfileReviews
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseDishDetailUserReviews:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFDishDetailUserReviews];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseDishDetailUserReviews :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        [ary_UserReview addObject:[TheDict valueForKey:@"ChefDetails"]];
        
        reviews_in_rat.text = [NSString stringWithFormat:@"%@",[[ary_UserReview objectAtIndex:0] valueForKey:@"total_revies"]];
        label_over_all_rateing .text = [NSString stringWithFormat:@"%@",[[ary_UserReview objectAtIndex:0] valueForKey:@"overall_rating"]];
        label_taste_val.text =[NSString stringWithFormat:@"%@",[[ary_UserReview objectAtIndex:0] valueForKey:@"taste_rating"]];
        label_appeal_value.text =[NSString stringWithFormat:@"%@",[[ary_UserReview objectAtIndex:0] valueForKey:@"appeal_rating"]];
        label_value_value.text =[NSString stringWithFormat:@"%@ ",[[ary_UserReview objectAtIndex:0] valueForKey:@"value_rating"]];
        
        
        for (int i=0; i<[[TheDict valueForKey:@"reviewList"] count]; i++)
        {
            [ary_reviewslist addObject:[[TheDict valueForKey:@"reviewList"] objectAtIndex:i]];
        }
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    [img_table reloadData];
    
    
    
}

# pragma mark UserReviews method

-(void)AFFlag
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    NSDictionary *params =@{
                            
                            @"uid"                               :   [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"dish_id"                           :   str_dishUID,
                            @"review_id"                         :   @"",
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kChefProfileReviews
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseDishDetailUserReviews:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFFlag];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseFlag :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
        
    }
    [img_table reloadData];
    
    
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
