//
//  ChefServeLaterVC.h
//  Not86
//
//  Created by Interwld on 8/22/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CKCalendarView.h"

@interface ChefServeLaterVC : UIViewController
@property(nonatomic, strong) CKCalendarView *calendar;
@property(nonatomic, strong) NSMutableArray *enabledDates;
@end
