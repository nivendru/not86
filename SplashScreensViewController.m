//
//  SplashScreensViewController.m
//  Not86
//
//  Created by Interwld on 7/29/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "SplashScreensViewController.h"
#import "HomeViewController1.h"
#import "SMPageControl.h"
#import "UserLogonVC.h"

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"

@interface SplashScreensViewController ()<UIScrollViewDelegate>
{
    UIScrollView  *scrollView_splash;
    UIImageView*img_swapImg;
    NSArray *images;
    NSArray *images1;
    NSArray *images2;
    
    
    UIImageView *imageView;
    SMPageControl *pageControl;
    NSTimer*timer;
    BOOL pageControlBeingUsed;
    
    
}

@end

@implementation SplashScreensViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self integrate_View];
    images=[[NSArray alloc ]initWithObjects:
            [UIImage imageNamed:@"splash_1@2x.png"],
            [UIImage imageNamed:@"splash_2@2x.png"],
            [UIImage imageNamed:@"splash_3@2x.png"],
            [UIImage imageNamed:@"splash_4@2x.png"],
            [UIImage imageNamed:@"splash_5@2x.png"],
            [UIImage imageNamed:@"splash_6@2x.png"],nil];
    images1=[[NSArray alloc ]initWithObjects:
             [UIImage imageNamed:@"splash_1@2x.png"],
             [UIImage imageNamed:@"splash_2@2x.png"],
             [UIImage imageNamed:@"splash_3@2x.png"],
             [UIImage imageNamed:@"splash_4@2x.png"],
             [UIImage imageNamed:@"splash_5@2x.png"],
             [UIImage imageNamed:@"splash_6@2x.png"],nil];
    images2=[[NSArray alloc ]initWithObjects:
             [UIImage imageNamed:@"splash_1@2x.png"],
             [UIImage imageNamed:@"splash_2@2x.png"],
             [UIImage imageNamed:@"splash_3@2x.png"],
             [UIImage imageNamed:@"splash_4@2x.png"],
             [UIImage imageNamed:@"splash_5@2x.png"],
             [UIImage imageNamed:@"splash_6@2x.png"],nil];
    
    
    
    
    timer = [NSTimer scheduledTimerWithTimeInterval:6 target:self selector:@selector(ScrollImage) userInfo:nil repeats:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    pageControlBeingUsed = YES;
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}
-(void)integrate_View
{
    
    for (UIView *view in scrollView_splash.subviews)
        [view removeFromSuperview];

    scrollView_splash = [[UIScrollView alloc]init];
    if (IS_IPHONE_6) {
        scrollView_splash.frame = CGRectMake(0,0,WIDTH,HEIGHT);
    }
    else if (IS_IPHONE_6Plus) {
        scrollView_splash.frame = CGRectMake(0,0,WIDTH,HEIGHT);
    }
    else
    {
        scrollView_splash.frame = CGRectMake(0,0,WIDTH,HEIGHT);
    }
    [scrollView_splash setShowsVerticalScrollIndicator:NO];
    scrollView_splash.delegate = self;
    scrollView_splash.scrollEnabled = YES;
    scrollView_splash.showsVerticalScrollIndicator = NO;
    [scrollView_splash setUserInteractionEnabled:YES];
    scrollView_splash.pagingEnabled= YES;
    scrollView_splash.backgroundColor = [UIColor clearColor];
    
    
//    // here give you image name is proper so we can access it by for loop.
//    NSInteger numberOfViews =6;
//    for (int i=0; i<numberOfViews; i++)
//    {
//        //        CGFloat xOrigin = i * self.view.frame.size.width;
//        imageView = [[UIImageView alloc] initWithFrame: CGRectMake(WIDTH*i, 0, WIDTH, HEIGHT)] ;
//        if (IS_IPHONE_6) {
//            
//            
////            NSMutableArray * array_splase_screens_images;
////            array_splase_screens_images = [[NSMutableArray alloc]initWithObjects:@"splashone@2x.png",@"splashtwo@2x.png",@"splashthree@2x.png",@"splashfour@2x.png",@"splashfive@2x.png",@"splashsix@2x.png",nil];
////            imageView.image = [UIImage imageNamed:[array_splase_screens_images objectAtIndex:arc4random_uniform((uint32_t)[array_splase_screens_images count])]];
//            
//            
//        
//        }
//        else if (IS_IPHONE_6Plus) {
//            //            imageView.image = [UIImage imageNamed:[NSString stringWithFormat:
//            //                                                   @"splash_%d@2x", arc4random_uniform((uint32_t)[images count])]];
//            NSMutableArray * array_splase_screens_images;
//            array_splase_screens_images = [[NSMutableArray alloc]initWithObjects:@"splashone@2x.png",@"splashtwo@2x.png",@"splashthree@2x.png",@"splashfour@2x.png",@"splashfive@2x.png",@"splashsix@2x.png",nil];
//            imageView.image = [UIImage imageNamed:[array_splase_screens_images objectAtIndex:arc4random_uniform((uint32_t)[array_splase_screens_images count])]];
//        }
//        else
//        {
//            NSMutableArray * array_splase_screens_images;
//            array_splase_screens_images = [[NSMutableArray alloc]initWithObjects:@"splashone@2x.png",@"splashtwo@2x.png",@"splashthree@2x.png",@"splashfour@2x.png",@"splashfive@2x.png",@"splashsix@2x.png",nil];
//            imageView.image = [UIImage imageNamed:[array_splase_screens_images objectAtIndex:arc4random_uniform((uint32_t)[array_splase_screens_images count])]];
//        }
//        
//        [imageView setUserInteractionEnabled:YES];
//        imageView.contentMode = UIViewContentModeScaleAspectFill;
//        imageView.clipsToBounds = YES;
//        [scrollView_splash addSubview: imageView];
//        
//        
    
        // here give you image name is proper so we can access it by for loop.
        NSInteger numberOfViews =6;
        for (int i=0; i<numberOfViews; i++)
        {
            CGFloat xOrigin = i * self.view.frame.size.width;
            imageView = [[UIImageView alloc] initWithFrame: CGRectMake(xOrigin, 0, WIDTH, HEIGHT)] ;
            if (IS_IPHONE_6) {
                imageView.image = [UIImage imageNamed:[NSString stringWithFormat:
                                                       @"splash_%d@2x", i+1]];
            }
            else if (IS_IPHONE_6Plus) {
                imageView.image = [UIImage imageNamed:[NSString stringWithFormat:
                                                       @"splash_%d@2x", i+1]];
            }
            else
            {
                imageView.image = [UIImage imageNamed:[NSString stringWithFormat:
                                                       @"splash_%d@2x", i+1]];
            }
            
            [imageView setUserInteractionEnabled:YES];
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            imageView.clipsToBounds = YES;
            [scrollView_splash addSubview: imageView];
            
            
           
//            
//            UIImageView *imageView1 = [[UIImageView alloc] init];
//            if(IS_IPHONE_6)
//            {
//                imageView1.frame=CGRectMake(15,HEIGHT-70, WIDTH-30,60);
//                
//
//            }
//            else if (IS_IPHONE_6Plus)
//            {
//                imageView1.frame=CGRectMake(15,HEIGHT-73, WIDTH-30,60);
//            }
//            else
//            {
//                imageView1.frame=CGRectMake(10,HEIGHT-60, IS_IPHONE_5?WIDTH-20:WIDTH-20,IS_IPHONE_5?50:50);
//            }
//            [imageView1 setUserInteractionEnabled:YES];
//            [imageView addSubview:imageView1];

            UIImageView *  img_cook_eat =[[UIImageView alloc]init];
            if (IS_IPHONE_6Plus)
            {
                img_cook_eat.frame = CGRectMake(15, HEIGHT-73, WIDTH-30, 60);
            }
            if (IS_IPHONE_6)
            {
                img_cook_eat.frame = CGRectMake(15, HEIGHT-70, WIDTH-30, 60);
            }
            else
            {
                img_cook_eat.frame = CGRectMake(10, HEIGHT-60, IS_IPHONE_5?WIDTH-20:WIDTH-20,IS_IPHONE_5?50:50);
            }
            img_cook_eat.frame = CGRectMake(15, HEIGHT-70, WIDTH-30, 60);
            [img_cook_eat setUserInteractionEnabled:YES];
            img_cook_eat.image=[UIImage imageNamed:@"img-cook@2x.png"];
            [ imageView addSubview:img_cook_eat];

        
            UIButton *cookbtn = [[UIButton alloc] init];
            cookbtn.frame = CGRectMake(0,0, WIDTH/2-10,60);
            cookbtn.backgroundColor = [UIColor clearColor];
            [cookbtn addTarget:self action:@selector(Click_cookbtn) forControlEvents:UIControlEventTouchUpInside ];
            [cookbtn setUserInteractionEnabled:YES];
            [img_cook_eat addSubview:cookbtn];
            
            
            UIButton *eatbtn = [[UIButton alloc] init];
            eatbtn.frame = CGRectMake(WIDTH/2-10,0, WIDTH/2-15,60);
            eatbtn.backgroundColor = [UIColor clearColor];
            [eatbtn setUserInteractionEnabled:YES];
            [eatbtn addTarget:self action:@selector(Click_eatbtn) forControlEvents:UIControlEventTouchUpInside ];
            [img_cook_eat addSubview:eatbtn];

            
            
        pageControl = [[SMPageControl alloc] init];
        if (IS_IPHONE_6) {
            pageControl.frame = CGRectMake(30,HEIGHT-120,WIDTH-60,60);
        }
        else if (IS_IPHONE_6Plus) {
            pageControl.frame = CGRectMake(30,HEIGHT-120,WIDTH-60,60);
        }
        else
        {
            pageControl.frame = CGRectMake(20,HEIGHT-120,WIDTH-40,60);
        }
        
        if([pageControl respondsToSelector:@selector(setTintColor:)])
            //            [pageControl setPageIndicatorImage:[UIImage imageNamed:@"img_dotunselected@2x.png"]];
            //        [pageControl setCurrentPageIndicatorImage:[UIImage imageNamed:@"img_dotselected@2x.png"]];
            pageControl.backgroundColor=[UIColor clearColor];
        //    pageControl.transform = CGAffineTransformMakeScale(0.7, 0.7);
        //    [pageControl setIndicatorDiameter: 18.0f] ;
        //    [pageControl setIndicatorSpace: 11.0f] ;
        pageControl.userInteractionEnabled=YES;
        //        pageControl.numberOfPages=6;
        //        pageControl.currentPage=0;
        [pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
//        [imageView addSubview:pageControl];
        
    }
    
    
    scrollView_splash.contentSize = CGSizeMake(WIDTH*numberOfViews, HEIGHT-20);
    [self.view  addSubview:scrollView_splash];
}


-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlBeingUsed = YES;
    
}



-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlBeingUsed = NO;
    
}
#pragma mark ScrollImage
-(void)ScrollImage
{
    
    CGFloat pageWidth = scrollView_splash.frame.size.width;
    
    if([images  count]>pageControl.currentPage+1)
    {
        pageControl.numberOfPages=images.count;
        int page = floor((scrollView_splash.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        pageControl.currentPage=page+1;
        [scrollView_splash setContentOffset:CGPointMake(WIDTH*(page+1), 0) animated:YES];
        
        
    }
    else if([images  count]==pageControl.currentPage+1)
    {
//        pageControl.numberOfPages=images.count;
        pageControl.currentPage=0;
        [scrollView_splash setContentOffset:CGPointMake(0, 0) animated:YES];
        
        [self integrate_View];
        
        
    }
    
}






-(void)Click_cookbtn
{
    HomeViewController1 *vc=[[HomeViewController1 alloc] init];
    [self presentViewController:vc animated:NO completion:nil];
//    [self.navigationController pushViewController:vc animated:NO];
    
    
}
-(void)Click_eatbtn
{
    
    
    UserLogonVC *vc=[[UserLogonVC alloc] init];
    [self.navigationController pushViewController:vc animated:NO];
    
    //    BeaVolunteerVC *vc = [[BeaVolunteerVC alloc] init];
    //    [self.navigationController pushViewController:vc animated:NO];
}

- (void)changePage:(UIButton *)sender
{
    
}

@end
