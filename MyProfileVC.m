//
//  MyProfileVC.m
//  Not86
//
//  Created by Interwld on 9/1/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "MyProfileVC.h"
#import "AppDelegate.h"
#import "UIImageView+AFNetworking.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>




#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667  ) < DBL_EPSILON )
#define IS_IPHONE_6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736  ) < DBL_EPSILON )

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kFont @"CenturyGothic"
#define kFontBold @"CenturyGothic-Bold"
#define kFontHelvetica @"Helvetica"


@interface MyProfileVC ()
<UIScrollViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UITextViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIImageView * img_header;
    UIScrollView * scroll;
    UIImageView *  img_bg;
    UICollectionViewFlowLayout *  layout;
    UICollectionView * collView_for_icons;
    NSMutableArray * array_for_icons;
    NSMutableArray * array_icons_name;
    UIImageView * img_strip;
    UIView * view_personal;
    UITableView *  table_for_personal_info;
    NSMutableArray * ary_chef_info_titles;
    NSMutableArray * array_img;
    NSMutableArray * array_chef_details;
    UIImageView * img_strip1 ;
    UIView * view_for_kitchen;
    UITableView * table_for_kitchen_info;
    NSMutableArray * array_icon_kitchen;
    NSMutableArray * array_head_names_in_kitchen;
    NSMutableArray * array_kitchen_name;
    NSMutableArray * array_icon_storages_hygiene;
    UICollectionViewFlowLayout *  layout2;
    UICollectionView * collView_for_kitchen_imgs;
    NSMutableArray * array_kitchen_imgs;
    UITableView * table_for_food_storage;
    NSMutableArray * array_head_names_in_food_storage;
    UIImageView *  img_strip2;
    UIView * view_for_dine_in;
    UIImageView * img_strip3;
    UIView * view_for_menu;
    UITableView * table_for_menu;
    NSMutableArray *  array_dietary_icons;
    NSMutableArray * array_head_names_in_dietary;
    NSMutableArray * array_text_in_dietary_cell;
    UICollectionViewFlowLayout *  layout3;
    UICollectionView * collView_for_menu_items;
    NSMutableArray * array_menu_items;
    UIView *  view_for_schedule;
    UITableView * table_for_schedule;
    NSMutableArray * array_serving_img;
    NSMutableArray * array_serving_time;
    NSMutableArray * array_serving_items;
    UIView * view_for_schedule_items;
    UITableView * table_for_selected_schedule_deate;
    NSMutableArray * array_icon_left_arrow;
    NSMutableArray* array_serving_date_time;
    UITableView * table_for_dish_items;
    NSMutableArray*ary_displaynames;
    NSMutableArray * array_dish_img;
    NSMutableArray * arraY_serving_val;
    UIView * view_for_on_request;
    UICollectionViewFlowLayout *layout4;
    UICollectionView *collView_serviceDirectory;
    UIView *view_for_reviews;
    UIView * view_for_delivery;
    UIView * view_for_note;
    UILabel *label_favorite_value ;
    AppDelegate *delegate;
    UITextView * txtview_adddescription;
    NSMutableArray *ary_Chefprofile_info;
    NSMutableArray *ary_Personal_Information;
    NSMutableArray *ary_Serving_Type;
    NSMutableArray *ary_Kitchen_info;
    NSMutableArray *ary_Dine_In;
    NSMutableArray *ary_Menu;
    NSMutableArray *ary_ReviewsList;
    NSMutableArray *ary_notes;
    NSMutableArray *ary_KitchenImages;
    UITextView * txtview_note;
    UILabel *label_total_reviews_value;
    UILabel *label_overall_rating_value;
    UILabel *taste_value;
    UILabel *appeal_value;
    UILabel *value_value;
    UILabel *lbl_parking_val;
    UILabel *lbl_no_of_seats_val;
    UIImageView *img_din_in_area;
    UIImageView * img_video;
    NSMutableArray *ary_Menuimages;

    
    
    
    
    
}


@end

@implementation MyProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self integrateHeader];
    [self integrateBody];
    
    ary_Chefprofile_info=[NSMutableArray new];
    ary_Personal_Information=[NSMutableArray new];
    ary_Serving_Type=[NSMutableArray new];
    ary_Kitchen_info=[NSMutableArray new];
    ary_Dine_In=[NSMutableArray new];
    ary_Menu=[NSMutableArray new];
    ary_ReviewsList=[NSMutableArray new];
    ary_notes=[NSMutableArray new];
    ary_KitchenImages=[NSMutableArray new];
    ary_Menuimages=[NSMutableArray new];
//    NSMutableDictionary *dict_exp=[[NSMutableDictionary alloc] init];
//    [dict_exp setValue:@"" forKey:@"DishImage"];
//    [ary_Menuimages addObject:dict_exp];
    
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    
    array_for_icons = [[NSMutableArray alloc]initWithObjects:@"icon-personal-info-w@2x.png",@"icon-kichan-w@2x.png",@"icon-dine-in-w@2x.png",@"icon-menu-w@2x.png",@"calender-w-icon@2x.png",@"reviews-w-icon@2x.png",@"img-delivery-w@2x.png",@"icon-note@2x.png",nil];
    
    array_icons_name = [[NSMutableArray alloc]initWithObjects:@"Personal Info",@"   Kitchen",@"   Dine-in",@"     Menu",@"   Schedule",@"   Reviews",@"Delivert Info",@"    Note", nil];
    
    // personal info array declaration
    
    ary_chef_info_titles =[[NSMutableArray alloc]initWithObjects:@"Username",@"Full Name",@"Chef Type",@"Date of Birth",@"Social Security no./National ID",@"Email address",@"Mobile no.",@"Home Address",@"Serving Type",@"Cooking Qualification",@"Cooking Experience", @"Paypal Account",@"Payment Received",nil];
    array_img = [[NSMutableArray alloc]initWithObjects:@"user1-icon@2x.png",@"user1-icon@2x.png",@"food-icon@2x.png",@"icon-date-of-birth@2x.png",@"icon-social@2x.png",@"icon-email@2x.png",@"icon-phone2x.png",@"icon-Home@2x.png",@"seving-type-icon@2x.png",@"cooking-Q-icon@2x.png",@"cooking-Ex-icon@2x.png",@"icon-paypal@2x.png",@"icon-paypal@2x.png",nil];
    
    
    array_chef_details = [[NSMutableArray alloc]initWithObjects:@"Charles1990",@"Charles dan",@"Home Chef",@"29 july 1974",@"s123456789",@"Charlesdan@gmail.com",@"+01 643 124 5681",@"Smith St, Suntec City, Paris 743844",@"Dine-in,Takaway,Deliver",@"Diploma in Cooking Industry" ,@"6-10 Years",@"Charles@paypal.com",@"End of Month",nil];
    
    // kitchen info array declaration
    
    array_icon_kitchen = [[NSMutableArray alloc]initWithObjects:@"icon-kichan1@2x.png",@"icon-kichan1@2x.png", nil];
    array_head_names_in_kitchen = [[NSMutableArray alloc]initWithObjects:@"Kitchen Name",@"Kitchen Address",nil];
    array_kitchen_name = [[NSMutableArray alloc]initWithObjects:@"Charles X Restaruant",@"Cartel St, Sams City, Paris\n743659", nil];
    array_kitchen_imgs = [[NSMutableArray alloc]initWithObjects:@"img-kitchen1@2x.png",@"img-kitchen2@2x.png",@"img-kitchen3@2x.png",@"img-kitchen4@2x.png",@"img-kitchen5@2x.png",@"img-kitchen6@2x.png",@"img-kitchen7@2x.png",@"img-kitchen8@2x.png",@"img-kitchen1@2x.png",@"img-kitchen1@2x.png",nil];
    array_icon_storages_hygiene = [[NSMutableArray alloc]initWithObjects:@"icon-storage@2x.png",@"icon-hygiene@2x.png", nil];
    array_head_names_in_food_storage = [[NSMutableArray alloc]initWithObjects:@"Food Storage",@"Kitchen Hygiene Standards", nil];
    
    // array for menu
    array_dietary_icons = [[NSMutableArray alloc]initWithObjects:@"icon-order-food@2x.png",@"icon-allo_food@2x.png",nil];
    array_head_names_in_dietary = [[NSMutableArray alloc]initWithObjects:@"Cuisines Cooked",@"Dietary Restrictions followed",nil];
    array_text_in_dietary_cell = [[NSMutableArray alloc]initWithObjects:@"Chinese, Mexican, Italian, Korean",@"Vegetarian, Kosher", nil];
    //collections view array
    array_menu_items = [[NSMutableArray alloc]initWithObjects:@"img-item1@2x.png",@"img-item2@2x.png",@"img-item3@x.png",@"img-item4@2x.png",@"img-item5@2x.png",@"img-item6@2x.png",@"img-item7@2x.png",@"img-item8@2x.png",@"img-item9@2x.png",@"img-item10@2x.png",@"img-add@2x.png", nil];
    
    //array declaration for schedule
    
    array_serving_img = [[NSMutableArray alloc]initWithObjects:@"seving-type-icon@2x.png", @"seving-type-icon@2x.png",@"seving-type-icon@2x.png",nil];
    array_serving_time = [[NSMutableArray alloc]initWithObjects:@"10:00AM-12:00PM",@"02:00PM-05:00PM",@"06:00PM-10:00PM", nil];
    array_serving_items =[[NSMutableArray alloc]initWithObjects:@"Chicken 65,Chill Chicken",@"Chicken 65,Chill Chicken",@"Chicken 65,Chill Chicken", nil];
    //array for serving date and time
    array_icon_left_arrow = [[NSMutableArray alloc]initWithObjects:@"left-arrow-img@2x.png", nil];
    array_serving_date_time = [[NSMutableArray alloc]initWithObjects:@"22 july 12.00pm to 5.30pm", nil];
    //arraydeclaration for dish items
    ary_displaynames =[[NSMutableArray alloc]initWithObjects:@"Steamed Thai otah",@"Rasberry custored", nil];
    array_dish_img = [[NSMutableArray alloc]initWithObjects:@"dish1-img@2x.png",@"dish1-img@2x.png", nil];
    arraY_serving_val = [[NSMutableArray alloc]initWithObjects:@"30",@"30", nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];
    
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
//   [self AFChefMyProfileDishimages];
   [self ChefProfileInfo];
}
#pragma mark - Add Methods

-(void)integrateHeader
{
    
    img_header = [[UIImageView alloc]init];
    img_header.frame = CGRectMake(0, 0, WIDTH, 45);
    [img_header setUserInteractionEnabled:YES];
    img_header.image=[UIImage imageNamed:@"img_header@2x.png"];
    [self.view addSubview:img_header];
    
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu.frame = CGRectMake(10,13,20,20);
    icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu addTarget:self action:@selector(btn_menu_click:) forControlEvents:UIControlEventTouchUpInside];
    [icon_menu setImage:[UIImage imageNamed:@"menu-icon@2x.png"] forState:UIControlStateNormal];
    [img_header   addSubview:icon_menu];
    
    
    UILabel *lbl_my_profile = [[UILabel alloc]init];
    lbl_my_profile.frame = CGRectMake(CGRectGetMaxX(icon_menu.frame)+30,0, 150, 45);
    lbl_my_profile.text = @"My Profile";
    lbl_my_profile.font = [UIFont fontWithName:kFont size:20];
    lbl_my_profile.textColor = [UIColor whiteColor];
    lbl_my_profile.backgroundColor = [UIColor clearColor];
    [img_header addSubview:lbl_my_profile];
    
    UIImageView *icon_logo = [[UIImageView alloc]init];
    icon_logo.frame = CGRectMake(WIDTH-40, 9, 30, 30);
    [icon_logo setImage:[UIImage imageNamed:@"logo-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [icon_logo setUserInteractionEnabled:YES];
    [img_header addSubview:icon_logo];
    
}
-(void)integrateBody
{
    
    scroll = [[UIScrollView alloc]init];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    scroll.frame = CGRectMake(0,0, WIDTH, 900);
    scroll.scrollEnabled = YES;
    scroll.pagingEnabled = YES;
    [self.view addSubview:scroll];
    
    img_bg = [[UIImageView alloc]init];
    img_bg .frame = CGRectMake(0,25, WIDTH, 210);
    [img_bg  setImage:[UIImage imageNamed:@"bg2-img@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg  setUserInteractionEnabled:YES];
    [ scroll addSubview:img_bg ];
    
    UIButton *chef_img = [UIButton buttonWithType:UIButtonTypeCustom];
    chef_img.frame = CGRectMake(110,30,80,80);
    chef_img.backgroundColor = [UIColor clearColor];
    [chef_img addTarget:self action:@selector(btn_img_user_click:) forControlEvents:UIControlEventTouchUpInside];
    [chef_img setImage:[UIImage imageNamed:@"img-chef@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:chef_img];
    
    
    UIImageView * img_black = [[UIImageView alloc]init];
    img_black.frame = CGRectMake(0,67,80,20);
    [img_black setUserInteractionEnabled:YES];
    img_black.image=[UIImage imageNamed:@"img-black@2x.png"];
    [chef_img addSubview:img_black];
    
    UILabel *lbl_on_reuest = [[UILabel alloc]init];
    lbl_on_reuest.frame = CGRectMake(9,0, 100,20);
    lbl_on_reuest.text = @"On Request";
    lbl_on_reuest.font = [UIFont fontWithName:kFontBold size:11];
    lbl_on_reuest.textColor = [UIColor colorWithRed:61/255.0f green:214/255.0f blue:237/255.0f alpha:1];
    lbl_on_reuest.backgroundColor = [UIColor clearColor];
    [ img_black addSubview:lbl_on_reuest];
    
    
    
    UILabel *lbl_chef_name = [[UILabel alloc]init];
    lbl_chef_name.frame = CGRectMake(110,CGRectGetMaxY(chef_img.frame), 200,45);
    lbl_chef_name.text = @"Charles Dan";
    lbl_chef_name.font = [UIFont fontWithName:kFontBold size:15];
    lbl_chef_name.textColor = [UIColor whiteColor];
    lbl_chef_name.backgroundColor = [UIColor clearColor];
    [img_bg addSubview:lbl_chef_name];
    
    UIButton *img_pencil = [UIButton buttonWithType:UIButtonTypeCustom];
    img_pencil.frame = CGRectMake(280,35,20,20);
    img_pencil.backgroundColor = [UIColor clearColor];
    [img_pencil addTarget:self action:@selector(btn_img_pencil_click:) forControlEvents:UIControlEventTouchUpInside];
    [img_pencil setImage:[UIImage imageNamed:@"icon-edit@2x.png"] forState:UIControlStateNormal];
    [img_bg  addSubview:img_pencil];
    
#pragma collection view
    
    layout = [[UICollectionViewFlowLayout alloc] init];
    collView_for_icons = [[UICollectionView alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(chef_img.frame)+45,WIDTH,50)
                                            collectionViewLayout:layout];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_icons setDataSource:self];
    [collView_for_icons setDelegate:self];
    collView_for_icons.scrollEnabled = YES;
    collView_for_icons.showsVerticalScrollIndicator = NO;
    collView_for_icons.showsHorizontalScrollIndicator = NO;
    collView_for_icons.pagingEnabled = YES;
    [collView_for_icons registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_icons setBackgroundColor:[UIColor clearColor]];
    layout.minimumInteritemSpacing = 12;
    layout.minimumLineSpacing = 0;
    collView_for_icons.userInteractionEnabled = YES;
    [img_bg  addSubview: collView_for_icons];
    
#pragma BODY FOR INFO
    
    img_strip = [[UIImageView alloc]init];
    //   img_strip.frame = CGRectMake(0,CGRectGetMaxY(collView_for_icons.frame)+2,78, 3);
    [img_strip setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    //img_strip.backgroundColor = [UIColor redColor];
    [img_strip setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip];
    
    //view for info
    
    view_personal = [[UIView alloc]init];
    view_personal.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,800);
    view_personal.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_personal];
    
    //    UIImageView *img_bg1 = [[UIImageView alloc]init];
    //    //   img_bg1.frame = CGRectMake(0,10, WIDTH+5, 300);
    //    [img_bg1  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //    //   icon_user.backgroundColor = [UIColor redColor];
    //    [img_bg1  setUserInteractionEnabled:YES];
    //    [ view_personal addSubview:img_bg1 ];
    
    UIImageView *img_bg1 = [[UIImageView alloc]init];
    img_bg1.frame = CGRectMake(0,10, WIDTH+5, 500);
    [img_bg1  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg1  setUserInteractionEnabled:YES];
    [view_personal addSubview:img_bg1 ];
    
    txtview_adddescription = [[UITextView alloc]init];
    txtview_adddescription.frame = CGRectMake(20,20, 295,60);
    txtview_adddescription.scrollEnabled = YES;
    txtview_adddescription.userInteractionEnabled = NO;
    txtview_adddescription.font = [UIFont fontWithName:kFont size:12];
    txtview_adddescription.backgroundColor = [UIColor clearColor];
    txtview_adddescription.delegate = self;
    txtview_adddescription.textColor = [UIColor blackColor];
    //    txtview_adddescription.text =@"LOrem ipsumdotor sit amet.ne dicit virits corpor a per,ut illum liberavisse vet.At mei vidit conse cteture.in vitae definitionem per.tate vetear cliquado ei vim.";
    [img_bg1 addSubview:txtview_adddescription];
    
    UIImageView *img_line = [[UIImageView alloc]init];
    img_line .frame = CGRectMake(20,CGRectGetMaxY(txtview_adddescription.frame)+5, 280, 0.5);
    [img_line setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_line setUserInteractionEnabled:YES];
    [img_bg1 addSubview:img_line ];
    
    
#pragma mark personal_Tableview
    
    table_for_personal_info = [[UITableView alloc] init ];
    table_for_personal_info.frame  = CGRectMake(13,CGRectGetMaxY(img_line.frame),294,300);
    [table_for_personal_info setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_personal_info.delegate = self;
    table_for_personal_info.dataSource = self;
    table_for_personal_info.showsVerticalScrollIndicator = NO;
    table_for_personal_info.backgroundColor = [UIColor clearColor];
    [img_bg1 addSubview:table_for_personal_info];
    
#pragma BODY OF KITCHEN
    
    img_strip1 = [[UIImageView alloc]init];
    //   img_strip1.frame = CGRectMake(CGRectGetMaxX(img_strip.frame),CGRectGetMaxY(collView_for_icons.frame)+2,77, 3);
    [img_strip1 setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    //img_strip1.backgroundColor = [UIColor redColor];
    [img_strip1 setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip1];
    img_strip1.hidden = YES;
    
    //view for kitchen
    
    view_for_kitchen = [[UIView alloc]init];
    view_for_kitchen.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_kitchen.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_for_kitchen];
    view_for_kitchen.hidden = YES;
    
    
    UIImageView *img_bg2 = [[UIImageView alloc]init];
    img_bg2.frame = CGRectMake(0,10, WIDTH+2, 150);
    [img_bg2  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //icon_user.backgroundColor = [UIColor redColor];
    [img_bg2  setUserInteractionEnabled:YES];
    [view_for_kitchen addSubview:img_bg2 ];
    
#pragma mark table for kitchen info
    
    table_for_kitchen_info = [[UITableView alloc] init ];
    table_for_kitchen_info.frame  = CGRectMake(8,4,305,140);
    [table_for_kitchen_info setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_kitchen_info.delegate = self;
    table_for_kitchen_info.dataSource = self;
    table_for_kitchen_info.showsVerticalScrollIndicator = NO;
    table_for_kitchen_info.backgroundColor = [UIColor clearColor];
    [img_bg2 addSubview:table_for_kitchen_info];
    
    UIImageView *img_bg_for_kitchen_imgs = [[UIImageView alloc]init];
    img_bg_for_kitchen_imgs.frame = CGRectMake(0,CGRectGetMaxY(img_bg2.frame)+5, WIDTH+2, 250);
    [img_bg_for_kitchen_imgs  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //img_bg_for_kitchen_imgs.backgroundColor = [UIColor redColor];
    [img_bg_for_kitchen_imgs  setUserInteractionEnabled:YES];
    [view_for_kitchen addSubview:img_bg_for_kitchen_imgs];
    
    img_video = [[UIImageView alloc]init];
    img_video.frame = CGRectMake(8,10,330,100);
    [img_video setUserInteractionEnabled:YES];
    img_video.image=[UIImage imageNamed:@"img_video@2x.png"];
    [img_bg_for_kitchen_imgs addSubview:img_video];
    
    
    self.moviePlayer = [[MPMoviePlayerController alloc] init];
    self.moviePlayer.movieSourceType = MPMovieSourceTypeFile;
    self.moviePlayer.scalingMode  = MPMovieScalingModeAspectFill;
    self.moviePlayer.view.frame = CGRectMake(0,0,330,100);
    self.moviePlayer.controlStyle = MPMovieControlStyleEmbedded;
    [img_video addSubview:self.moviePlayer.view];
    
    UIButton *btn_VideoPLay = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_VideoPLay.frame = CGRectMake(0,0,330,100);
    btn_VideoPLay.backgroundColor = [UIColor clearColor];
    [btn_VideoPLay addTarget:self action:@selector(btn_Videoplay_click:) forControlEvents:UIControlEventTouchUpInside];
    //    [btn_VideoPLay setImage:[UIImage imageNamed:@"img-chef@2x.png"] forState:UIControlStateNormal];
    [img_video  addSubview:btn_VideoPLay];

    
    layout2 = [[UICollectionViewFlowLayout alloc] init];
    collView_for_kitchen_imgs = [[UICollectionView alloc] initWithFrame:CGRectMake(10,CGRectGetMaxY(img_video.frame),300,100)
                                                   collectionViewLayout:layout2];
    [layout2 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_kitchen_imgs setDataSource:self];
    [collView_for_kitchen_imgs setDelegate:self];
    collView_for_kitchen_imgs.scrollEnabled = YES;
    collView_for_kitchen_imgs.showsVerticalScrollIndicator = YES;
    collView_for_kitchen_imgs.showsHorizontalScrollIndicator = NO ;
    collView_for_kitchen_imgs.pagingEnabled = YES;
    [collView_for_kitchen_imgs registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_kitchen_imgs setBackgroundColor:[UIColor clearColor]];
    layout2.minimumInteritemSpacing = 4;
    layout2.minimumLineSpacing = 2;
    collView_for_kitchen_imgs.userInteractionEnabled = YES;
    [img_bg_for_kitchen_imgs  addSubview: collView_for_kitchen_imgs];
    
    
    
    UIImageView *img_bg_for_food_storage = [[UIImageView alloc]init];
    img_bg_for_food_storage.frame = CGRectMake(0,CGRectGetMaxY(img_bg_for_kitchen_imgs.frame)+5, WIDTH+2, 150);
    [img_bg_for_food_storage  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //img_bg_for_food_storage.backgroundColor = [UIColor redColor];
    [img_bg_for_food_storage  setUserInteractionEnabled:YES];
    [view_for_kitchen addSubview:img_bg_for_food_storage];
    
#pragma mark table for food storage
    
    table_for_food_storage = [[UITableView alloc] init ];
    table_for_food_storage.frame  = CGRectMake(8,4,305,140);
    [table_for_food_storage setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_food_storage.delegate = self;
    table_for_food_storage.dataSource = self;
    table_for_food_storage.showsVerticalScrollIndicator = NO;
    table_for_food_storage.backgroundColor = [UIColor clearColor];
    [img_bg_for_food_storage addSubview:table_for_food_storage];
    
   
#pragma BODY FOR DINE_IN
    
    img_strip2 = [[UIImageView alloc]init];
    //   img_strip2.frame = CGRectMake(CGRectGetMaxX(img_strip1.frame)+7,CGRectGetMaxY(collView_for_icons.frame)+2,77, 3);
    [img_strip2 setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    //img_strip2.backgroundColor = [UIColor redColor];
    [img_strip2 setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip2];
    img_strip2.hidden = YES;
    
    //view for dine-in
    
    view_for_dine_in = [[UIView alloc]init];
    view_for_dine_in.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_dine_in.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_for_dine_in];
    view_for_dine_in.hidden = YES;
    
    UIImageView *img_bg_for_din_in_area = [[UIImageView alloc]init];
    img_bg_for_din_in_area.frame = CGRectMake(0,10, WIDTH+2,270);
    [img_bg_for_din_in_area  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //img_bg_for_food_storage.backgroundColor = [UIColor redColor];
    [img_bg_for_din_in_area  setUserInteractionEnabled:YES];
    [view_for_dine_in addSubview:img_bg_for_din_in_area];
    
    UILabel *lbl_dine_in_area = [[UILabel alloc]init];
    lbl_dine_in_area.frame = CGRectMake(20,0, 200,45);
    lbl_dine_in_area.text = @"Dine-in Area";
    lbl_dine_in_area.font = [UIFont fontWithName:kFontBold size:15];
    lbl_dine_in_area.textColor = [UIColor blackColor];
    lbl_dine_in_area.backgroundColor = [UIColor clearColor];
    [img_bg_for_din_in_area addSubview:lbl_dine_in_area];
    
    img_din_in_area = [[UIImageView alloc]init];
    img_din_in_area.frame = CGRectMake(15,CGRectGetMaxY(lbl_dine_in_area.frame)-5, WIDTH-33,100);
//    [img_din_in_area  setImage:[UIImage imageNamed:@"img-dine-area@2x.png"]];
    //img_bg_for_food_storage.backgroundColor = [UIColor redColor];
    [img_din_in_area  setUserInteractionEnabled:YES];
    [img_bg_for_din_in_area addSubview:img_din_in_area];
    
    UIImageView *icon_no_of_seats = [[UIImageView alloc]init];
    icon_no_of_seats.frame = CGRectMake(25,CGRectGetMaxY(img_din_in_area.frame)+25, 35,20);
    [icon_no_of_seats  setImage:[UIImage imageNamed:@"icon-No-of-seats@2x.png"]];
    //img_bg_for_food_storage.backgroundColor = [UIColor redColor];
    [icon_no_of_seats  setUserInteractionEnabled:YES];
    [img_bg_for_din_in_area addSubview:icon_no_of_seats];
    
    UILabel *lbl_no_of_seats = [[UILabel alloc]init];
    lbl_no_of_seats.frame = CGRectMake(CGRectGetMaxX(icon_no_of_seats.frame)+10,CGRectGetMaxY(img_din_in_area.frame)+5, 200,45);
    lbl_no_of_seats.text = @"NO. of Seats";
    lbl_no_of_seats.font = [UIFont fontWithName:kFontBold size:12];
    lbl_no_of_seats.textColor = [UIColor blackColor];
    lbl_no_of_seats.backgroundColor = [UIColor clearColor];
    [img_bg_for_din_in_area addSubview:lbl_no_of_seats];
    
    lbl_no_of_seats_val = [[UILabel alloc]init];
    lbl_no_of_seats_val.frame = CGRectMake(CGRectGetMaxX(icon_no_of_seats.frame)+10,CGRectGetMaxY(lbl_no_of_seats.frame)-25, 200,45);
//    lbl_no_of_seats_val.text = @"10";
    lbl_no_of_seats_val.font = [UIFont fontWithName:kFont size:12];
    lbl_no_of_seats_val.textColor = [UIColor blackColor];
    lbl_no_of_seats_val.backgroundColor = [UIColor clearColor];
    [img_bg_for_din_in_area addSubview:lbl_no_of_seats_val];
    
    UIImageView *img_line2 = [[UIImageView alloc]init];
    img_line2.frame =  CGRectMake(20,CGRectGetMaxY(lbl_no_of_seats_val.frame)-10, 280, 0.5);
    img_line2.backgroundColor =[UIColor grayColor];
    [img_line2 setUserInteractionEnabled:YES];
    [img_bg_for_din_in_area addSubview:img_line2];
    
    UIImageView *icon_parking = [[UIImageView alloc]init];
    icon_parking.frame = CGRectMake(25,CGRectGetMaxY(img_line2.frame)+10, 35,35);
    [icon_parking  setImage:[UIImage imageNamed:@"icon-parking@2x.png"]];
    //img_bg_for_food_storage.backgroundColor = [UIColor redColor];
    [icon_parking  setUserInteractionEnabled:YES];
    [img_bg_for_din_in_area addSubview:icon_parking];
    
    UILabel *lbl_parking = [[UILabel alloc]init];
    lbl_parking.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(img_line2.frame)-2, 200,45);
    lbl_parking.text = @"Parking Available";
    lbl_parking.font = [UIFont fontWithName:kFontBold size:12];
    lbl_parking.textColor = [UIColor blackColor];
    lbl_parking.backgroundColor = [UIColor clearColor];
    [img_bg_for_din_in_area addSubview:lbl_parking];
    
    lbl_parking_val = [[UILabel alloc]init];
    lbl_parking_val.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-25, 200,45);
//    lbl_parking_val.text = @"Yes";
    lbl_parking_val.font = [UIFont fontWithName:kFont size:12];
    lbl_parking_val.textColor = [UIColor blackColor];
    lbl_parking_val.backgroundColor = [UIColor clearColor];
    [img_bg_for_din_in_area addSubview:lbl_parking_val];
    
    
#pragma MENU BODY
    
    img_strip3 = [[UIImageView alloc]init];
    //   img_strip3.frame = CGRectMake(CGRectGetMaxX(img_strip2.frame)+3,CGRectGetMaxY(collView_for_icons.frame)+2,80, 3);
    [img_strip3 setImage:[UIImage imageNamed:@"img-white-line@2x.png"]];
    //img_strip3.backgroundColor = [UIColor redColor];
    [img_strip3 setUserInteractionEnabled:YES];
    [img_bg addSubview:img_strip3];
    img_strip3.hidden = YES;
    
    //view for menu
    
    view_for_menu = [[UIView alloc]init];
    view_for_menu.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_menu.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_for_menu];
    view_for_menu.hidden = YES;
    
    UIImageView *img_bg_for_dietary = [[UIImageView alloc]init];
    img_bg_for_dietary.frame = CGRectMake(0,10, WIDTH+2,100);
    [img_bg_for_dietary  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //img_bg_for_food_storage.backgroundColor = [UIColor redColor];
    [img_bg_for_dietary  setUserInteractionEnabled:YES];
    [view_for_menu addSubview:img_bg_for_dietary];
    
    
#pragma mark table for food storage
    
    table_for_menu = [[UITableView alloc] init ];
    table_for_menu.frame  = CGRectMake(8,4,305,120);
    [table_for_menu setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_menu.delegate = self;
    table_for_menu.dataSource = self;
    table_for_menu.showsVerticalScrollIndicator = NO;
    table_for_menu.backgroundColor = [UIColor clearColor];
    [img_bg_for_dietary addSubview:table_for_menu];
    
    
    UIImageView *img_bg_for_menu_items = [[UIImageView alloc]init];
    img_bg_for_menu_items.frame = CGRectMake(0,CGRectGetMaxY(img_bg_for_dietary.frame)+35, WIDTH+2,200);
    [img_bg_for_menu_items  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    img_bg_for_menu_items.backgroundColor = [UIColor clearColor];
    [img_bg_for_menu_items  setUserInteractionEnabled:YES];
    [view_for_menu addSubview:img_bg_for_menu_items];
    
    layout3 = [[UICollectionViewFlowLayout alloc] init];
    collView_for_menu_items = [[UICollectionView alloc] initWithFrame:CGRectMake(10,5,300,50)
                                                 collectionViewLayout:layout3];
    [layout3 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_for_menu_items setDataSource:self];
    [collView_for_menu_items setDelegate:self];
    collView_for_menu_items.scrollEnabled = YES;
    collView_for_menu_items.showsVerticalScrollIndicator = YES;
    collView_for_menu_items.showsHorizontalScrollIndicator = NO;
    collView_for_menu_items.pagingEnabled = YES;
    [collView_for_menu_items registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_for_menu_items setBackgroundColor:[UIColor clearColor]];
    layout3.minimumInteritemSpacing = 12;
    layout3.minimumLineSpacing = 0;
    collView_for_menu_items.userInteractionEnabled = YES;
    [img_bg_for_menu_items  addSubview: collView_for_menu_items];
    
    
    
    
    
#pragma BODY FOR SCHEDULE
    
    view_for_schedule = [[UIView alloc]init];
    view_for_schedule.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,800);
    view_for_schedule.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_for_schedule];
    view_for_schedule.hidden = YES;
    
    UIImageView *img_calender = [[UIImageView alloc]init];
    img_calender.frame = CGRectMake(0,10, WIDTH, 200);
    [img_calender  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_calender  setUserInteractionEnabled:YES];
    [view_for_schedule addSubview:img_calender ];
    
    UIImageView *img_bg_for_schedule = [[UIImageView alloc]init];
    img_bg_for_schedule.frame = CGRectMake(0,CGRectGetMaxY(img_calender.frame), WIDTH, 155);
    [img_bg_for_schedule  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_schedule  setUserInteractionEnabled:YES];
    [view_for_schedule addSubview:img_bg_for_schedule ];
    
    
#pragma table_for_schedule
    
    table_for_schedule = [[UITableView alloc] init ];
    table_for_schedule.frame  = CGRectMake(10,0,300,155);
    [table_for_schedule setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_schedule.delegate = self;
    table_for_schedule.dataSource = self;
    table_for_schedule.showsVerticalScrollIndicator = NO;
    table_for_schedule.backgroundColor = [UIColor clearColor];
    [img_bg_for_schedule addSubview:table_for_schedule];
    
    UIButton *btn_on_schedule_img = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_schedule_img.frame = CGRectMake(20,CGRectGetMaxY(img_bg_for_schedule.frame)+10, WIDTH-40, 45);
    btn_on_schedule_img .backgroundColor = [UIColor clearColor];
    [btn_on_schedule_img addTarget:self action:@selector(click_on_add_schedule_btn:) forControlEvents:UIControlEventTouchUpInside];
    [btn_on_schedule_img setImage:[UIImage imageNamed:@"img-add-sche@2x.png"] forState:UIControlStateNormal];
    [view_for_schedule   addSubview:btn_on_schedule_img];
    
    
    
    // view for schedule items
    view_for_schedule_items = [[UIView alloc]init];
    view_for_schedule_items.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_schedule_items.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_for_schedule_items];
    view_for_schedule_items.hidden = YES;
    
    UIImageView *img_bg_for_selected_schedule_date = [[UIImageView alloc]init];
    img_bg_for_selected_schedule_date.frame = CGRectMake(0,05, WIDTH, 55);
    [img_bg_for_selected_schedule_date  setImage:[UIImage imageNamed:@"img-bg@2x.png"]];
    //   icon_user.backgroundColor = [UIColor redColor];
    [img_bg_for_selected_schedule_date  setUserInteractionEnabled:YES];
    [view_for_schedule_items addSubview:img_bg_for_selected_schedule_date ];
    
#pragma table_for_schedule
    
    table_for_selected_schedule_deate = [[UITableView alloc] init ];
    table_for_selected_schedule_deate.frame  = CGRectMake(10,0,300,55);
    [table_for_selected_schedule_deate setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_selected_schedule_deate.delegate = self;
    table_for_selected_schedule_deate.dataSource = self;
    table_for_selected_schedule_deate.showsVerticalScrollIndicator = NO;
    table_for_selected_schedule_deate.backgroundColor = [UIColor clearColor];
    [img_bg_for_selected_schedule_date addSubview:table_for_selected_schedule_deate];
    
#pragma table_for_dish_items
    
    table_for_dish_items = [[UITableView alloc] init ];
    table_for_dish_items.frame  = CGRectMake(10,CGRectGetMaxY(img_bg_for_selected_schedule_date.frame)+10,300,300);
    [table_for_dish_items setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_dish_items.delegate = self;
    table_for_dish_items.dataSource = self;
    table_for_dish_items.showsVerticalScrollIndicator = NO;
    table_for_dish_items.backgroundColor = [UIColor clearColor];
    [view_for_schedule_items addSubview:table_for_dish_items];
    
    
    UIButton *btn_img_edit = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_edit.frame = CGRectMake(10,CGRectGetMaxY(table_for_dish_items.frame)+10,140,40);
    btn_img_edit .backgroundColor = [UIColor clearColor];
    [btn_img_edit addTarget:self action:@selector(click_on_edite_btn:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_edit setImage:[UIImage imageNamed:@"img-edit@2x.png"] forState:UIControlStateNormal];
    [view_for_schedule_items   addSubview:btn_img_edit];
    
    UIButton *btn_img_delete = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_delete.frame = CGRectMake(CGRectGetMaxX(btn_img_edit.frame)+20,CGRectGetMaxY(table_for_dish_items.frame)+10,140,40);
    btn_img_delete .backgroundColor = [UIColor clearColor];
    [btn_img_delete addTarget:self action:@selector(click_on_delete_btn:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_delete setImage:[UIImage imageNamed:@"img-del@2x.png"] forState:UIControlStateNormal];
    [view_for_schedule_items   addSubview:btn_img_delete];
    
    
    //    layout4=[[UICollectionViewFlowLayout alloc] init];
    //    collView_serviceDirectory = [[UICollectionView alloc] initWithFrame:CGRectMake(30,0,WIDTH-65,70)
    //                                                   collectionViewLayout:layout4];
    //
    //    [layout4 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    //    [collView_serviceDirectory setDataSource:self];
    //    [collView_serviceDirectory setDelegate:self];
    //    collView_serviceDirectory.scrollEnabled = YES;
    //    collView_serviceDirectory.showsVerticalScrollIndicator = NO;
    //    collView_serviceDirectory.showsHorizontalScrollIndicator = NO;
    //    collView_serviceDirectory.pagingEnabled = NO;
    //    [collView_serviceDirectory registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    //    [collView_serviceDirectory setBackgroundColor:[UIColor clearColor]];
    //    layout4.minimumInteritemSpacing = 2;
    //    layout4.minimumLineSpacing = 0;
    //    collView_serviceDirectory.userInteractionEnabled = YES;
    //    [img_cellBackGnd addSubview:collView_serviceDirectory];
    
    
#pragma BODY FOR ON-REQUEST
    
    // view for on_request
    
    view_for_on_request = [[UIView alloc]init];
    view_for_on_request.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_on_request.backgroundColor=[UIColor clearColor];
    //  [scroll  addSubview: view_for_on_request];
    view_for_on_request.hidden = YES;
    
    
    UILabel *lbl_currently_accept_orders = [[UILabel alloc]init ];
    lbl_currently_accept_orders.frame = CGRectMake(90,02, 300, 45);
    lbl_currently_accept_orders.text = @"Currently accepting orders on request";
    lbl_currently_accept_orders.font = [UIFont fontWithName:kFontBold size:15];
    lbl_currently_accept_orders.textColor = [UIColor blackColor];
    lbl_currently_accept_orders.backgroundColor = [UIColor clearColor];
    [view_for_on_request addSubview:lbl_currently_accept_orders];
    
    UILabel *lbl_orders_accept = [[UILabel alloc]init ];
    lbl_orders_accept.frame = CGRectMake(90,CGRectGetMaxY(lbl_currently_accept_orders.frame)+5, 300, 45);
    lbl_orders_accept.text = @"% of Orders Accepted";
    lbl_orders_accept.font = [UIFont fontWithName:kFontBold size:15];
    lbl_orders_accept.textColor = [UIColor blackColor];
    lbl_orders_accept.backgroundColor = [UIColor clearColor];
    [view_for_on_request addSubview:lbl_orders_accept];
    
    UIImageView *img_green_percentage = [[UIImageView alloc]init];
    img_green_percentage .frame = CGRectMake(20,CGRectGetMaxY(lbl_orders_accept.frame),260,15);
    [img_green_percentage  setImage:[UIImage imageNamed:@"green-percentage@2x.png"]];
    img_green_percentage .backgroundColor = [UIColor clearColor];
    [img_green_percentage  setUserInteractionEnabled:YES];
    [view_for_on_request addSubview:img_green_percentage];
    
    UILabel *lbl_percentage_val = [[UILabel alloc]init ];
    lbl_percentage_val.frame = CGRectMake(CGRectGetMaxX(img_green_percentage.frame)+10,CGRectGetMaxY(lbl_orders_accept.frame)-15, 300, 45);
    lbl_percentage_val.text = @"56/100%";
    lbl_percentage_val.font = [UIFont fontWithName:kFontBold size:12];
    lbl_percentage_val.textColor = [UIColor blackColor];
    lbl_percentage_val.backgroundColor = [UIColor clearColor];
    [view_for_on_request addSubview:lbl_percentage_val];
    
    UILabel *lbl_avg_respond_time = [[UILabel alloc]init ];
    lbl_avg_respond_time.frame = CGRectMake(20,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
    lbl_avg_respond_time.text = @"Average Response Time:";
    lbl_avg_respond_time.font = [UIFont fontWithName:kFont size:15];
    lbl_avg_respond_time.textColor = [UIColor blackColor];
    lbl_avg_respond_time.backgroundColor = [UIColor clearColor];
    [view_for_on_request addSubview:lbl_avg_respond_time];
    
    UILabel *avg_respond_time_val = [[UILabel alloc]init ];
    avg_respond_time_val.frame = CGRectMake(CGRectGetMidX(lbl_avg_respond_time.frame)+35,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
    avg_respond_time_val.text = @"45 mins";
    avg_respond_time_val.font = [UIFont fontWithName:kFontBold size:15];
    avg_respond_time_val.textColor = [UIColor blackColor];
    avg_respond_time_val.backgroundColor = [UIColor clearColor];
    [view_for_on_request addSubview:avg_respond_time_val];
    
    
#pragma View_reviews
    
    view_for_reviews = [[UIView alloc]init];
    view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_reviews.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_for_reviews];
    view_for_reviews.hidden = YES;
    
    
    UIImageView *img_bg_in_reviews = [[UIImageView alloc]init];
    img_bg_in_reviews.frame = CGRectMake(10,20, WIDTH-20, 250);
    [img_bg_in_reviews  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    [img_bg_in_reviews  setUserInteractionEnabled:YES];
    [ view_for_reviews addSubview:img_bg_in_reviews ];
    
    UILabel *label_favorite_by_user = [[UILabel alloc]init];
    label_favorite_by_user.frame = CGRectMake(20,10, 200,30);
    label_favorite_by_user.text = @"Chef Favorite by Users";
    label_favorite_by_user.font = [UIFont fontWithName:kFont size:15];
    label_favorite_by_user.textColor = [UIColor blackColor];
    label_favorite_by_user.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_favorite_by_user];
    
    label_favorite_value = [[UILabel alloc]init];
    label_favorite_value.frame = CGRectMake(CGRectGetMaxX(label_favorite_by_user.frame)+3,-2, 295,60);
    //    label_favorite_value.text = @"1677";
    label_favorite_value.font = [UIFont fontWithName:kFontBold size:20];
    label_favorite_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:   1];
    label_favorite_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_favorite_value];
    
    UIImageView *img_line_in_reviews = [[UIImageView alloc]init];
    img_line_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_favorite_by_user.frame)+10, WIDTH-50, 0.5);
    [img_line_in_reviews  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line_in_reviews  setUserInteractionEnabled:YES];
    [img_bg_in_reviews addSubview:img_line_in_reviews];
    
    UILabel *label_total_reviews = [[UILabel alloc]init];
    label_total_reviews.frame = CGRectMake(20,CGRectGetMaxY(img_line_in_reviews.frame)+10, 200,30);
    label_total_reviews.text = @"Total Reviews";
    label_total_reviews.font = [UIFont fontWithName:kFont size:15];
    label_total_reviews.textColor = [UIColor blackColor];
    label_total_reviews.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_total_reviews];
    
    
    label_total_reviews_value = [[UILabel alloc]init];
    label_total_reviews_value.frame = CGRectMake(CGRectGetMaxX(label_total_reviews.frame)-5,CGRectGetMaxY(img_line_in_reviews.frame)+10, 150,30);
    //    label_total_reviews_value.text = @"118";
    label_total_reviews_value.font = [UIFont fontWithName:kFontBold size:20];
    label_total_reviews_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    label_total_reviews_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_total_reviews_value];
    
    
    UIButton *icon_red_right_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_red_right_arrow.frame = CGRectMake(CGRectGetMaxX(label_total_reviews_value.frame)-100,CGRectGetMaxY( img_line_in_reviews.frame)+15,20,20);
    icon_red_right_arrow.backgroundColor = [UIColor clearColor];
    [icon_red_right_arrow addTarget:self action:@selector(click_on_right_arrow:) forControlEvents:UIControlEventTouchUpInside];
    [icon_red_right_arrow setImage:[UIImage imageNamed:@"red-right-arrow@2x.png"] forState:UIControlStateNormal];
    [img_bg_in_reviews  addSubview:icon_red_right_arrow];
    
    UIButton *btn_on_right_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_on_right_arrow.frame = CGRectMake(4,CGRectGetMaxY(img_line_in_reviews.frame),290,50);
    btn_on_right_arrow.backgroundColor = [UIColor clearColor];
    [btn_on_right_arrow addTarget:self action:@selector(click_on_right_arrow:) forControlEvents:UIControlEventTouchUpInside];
    // [icon_red_right_arrow setImage:[UIImage imageNamed:@"red-right-arrow@2x.png"] forState:UIControlStateNormal];
    [img_bg_in_reviews  addSubview:btn_on_right_arrow];
    
    
    
    
    UIImageView *img_line2_in_reviews = [[UIImageView alloc]init];
    img_line2_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_total_reviews.frame)+10, WIDTH-50, 0.5);
    [img_line2_in_reviews  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line2_in_reviews  setUserInteractionEnabled:YES];
    [img_bg_in_reviews addSubview:img_line2_in_reviews];
    
    UILabel *label_overall_rating = [[UILabel alloc]init];
    label_overall_rating.frame = CGRectMake(20,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 200,30);
    label_overall_rating.text = @"Overall Ratings";
    label_overall_rating.font = [UIFont fontWithName:kFont size:15];
    label_overall_rating.textColor = [UIColor blackColor];
    label_overall_rating.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_overall_rating];
    
    
    UIImageView *img_thumb = [[UIImageView alloc]init];
    img_thumb .frame = CGRectMake(CGRectGetMaxX(label_overall_rating.frame)-15,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 25,25);
    [img_thumb   setImage:[UIImage imageNamed:@"thumb-icon@2x.png"]];
    [img_thumb   setUserInteractionEnabled:YES];
    [img_bg_in_reviews addSubview:img_thumb ];
    
    label_overall_rating_value = [[UILabel alloc]init];
    label_overall_rating_value.frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+5,CGRectGetMaxY(img_line2_in_reviews.frame)-5, 150,60);
    //    label_overall_rating_value.text = @"87.4%";
    label_overall_rating_value.font = [UIFont fontWithName:kFontBold size:17];
    label_overall_rating_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    label_overall_rating_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_overall_rating_value];
    
    
    UIImageView *img_line3_in_reviews = [[UIImageView alloc]init];
    img_line3_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_overall_rating.frame)+10, WIDTH-50, 0.5);
    [img_line3_in_reviews  setImage:[UIImage imageNamed:@"img-line@2x.png"]];
    [img_line3_in_reviews  setUserInteractionEnabled:YES];
    [img_bg_in_reviews addSubview:img_line3_in_reviews];
    
    UILabel *label_food_ratings = [[UILabel alloc]init];
    label_food_ratings.frame = CGRectMake(20,CGRectGetMaxY(img_line3_in_reviews.frame)+10, 250,30);
    label_food_ratings.text = @"Food Ratings";
    label_food_ratings.font = [UIFont fontWithName:kFont size:15];
    label_food_ratings.textColor = [UIColor blackColor];
    label_food_ratings.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_food_ratings];
    
    UILabel *label_taste = [[UILabel alloc]init];
    label_taste.frame = CGRectMake(10,CGRectGetMaxY(label_food_ratings.frame)+10, 50,30);
    label_taste.text = @"Taste:";
    label_taste.font = [UIFont fontWithName:kFont size:13];
    label_taste.textColor = [UIColor blackColor];
    label_taste.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_taste];
    
    taste_value = [[UILabel alloc]init];
    taste_value.frame = CGRectMake(CGRectGetMaxX(label_taste.frame)-10,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
    //    taste_value.text = @"75%";
    taste_value.font = [UIFont fontWithName:kFont size:13];
    taste_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];    taste_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:taste_value];
    
    UILabel *label_appeal = [[UILabel alloc]init];
    label_appeal.frame = CGRectMake(CGRectGetMaxX(taste_value.frame)-15,CGRectGetMaxY(label_food_ratings.frame)+10,70,30);
    label_appeal.text = @"Appeal:";
    label_appeal.font = [UIFont fontWithName:kFont size:13];
    label_appeal.textColor = [UIColor blackColor];
    label_appeal.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_appeal];
    
    appeal_value = [[UILabel alloc]init];
    appeal_value.frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-15,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
    //    appeal_value.text = @"85%";
    appeal_value.font = [UIFont fontWithName:@"Arial" size:13];
    appeal_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
    appeal_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:appeal_value];
    
    UILabel *label_value = [[UILabel alloc]init];
    label_value.frame = CGRectMake(CGRectGetMaxX(appeal_value.frame)-20,CGRectGetMaxY(label_food_ratings.frame)+10, 50,30);
    label_value.text = @"Value:";
    label_value.font = [UIFont fontWithName:kFont size:13];
    label_value.textColor = [UIColor blackColor];
    label_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:label_value];
    
    value_value = [[UILabel alloc]init];
    value_value.frame = CGRectMake(CGRectGetMaxX(label_value.frame)-5,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
    //    value_value.text = @"75%";
    value_value.font = [UIFont fontWithName:kFont size:13];
    value_value.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];    value_value.backgroundColor = [UIColor clearColor];
    [img_bg_in_reviews addSubview:value_value];
    
    
#pragma BODY FOR DELIVERY
    
    view_for_delivery = [[UIView alloc]init];
    view_for_delivery.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_delivery.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_for_delivery];
    view_for_delivery.hidden = YES;
    
#pragma BODY FOR NOTE
    
    view_for_note = [[UIView alloc]init];
    view_for_note.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
    view_for_note.backgroundColor=[UIColor clearColor];
    [scroll  addSubview: view_for_note];
    view_for_note.hidden = YES;
    
    UIImageView *img_bg_in_note = [[UIImageView alloc]init];
    img_bg_in_note.frame = CGRectMake(10,20, WIDTH-20, 240);
    [img_bg_in_note  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
    [img_bg_in_note  setUserInteractionEnabled:YES];
    [ view_for_note addSubview:img_bg_in_note];
    
    txtview_note = [[UITextView alloc]init];
    //    txtview_note .text = [NSString stringWithFormat:@"%@",[ary_notes objectAtIndex:0]];
    txtview_note.frame = CGRectMake(5,0,295,100);
    txtview_note.scrollEnabled = YES;
    txtview_note.userInteractionEnabled = NO;
    txtview_note.font = [UIFont fontWithName:kFont size:12];
    txtview_note.backgroundColor = [UIColor clearColor];
    txtview_note.delegate = self;
    txtview_note.textColor = [UIColor blackColor];
    
    
    
    //    txtview_note.text =@"LOrem ipsumdotor sit amet.ne dicit virits corpor a per,ut illum liberavisse vet.At mei vidit conse cteture.in vitae definitionem per.tate vetear cliquado ei vim.";
    [img_bg_in_note addSubview:txtview_note];
    
    UIButton *btn_img_write_note = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_img_write_note.frame = CGRectMake(20,CGRectGetMaxY(img_bg_in_note.frame)+15,WIDTH-40,50);
    btn_img_write_note .backgroundColor = [UIColor clearColor];
    [btn_img_write_note addTarget:self action:@selector(click_on_write_note_btn:) forControlEvents:UIControlEventTouchUpInside];
    [btn_img_write_note setImage:[UIImage imageNamed:@"img-write-note@2x.png"] forState:UIControlStateNormal];
    [view_for_note addSubview:btn_img_write_note];
    
    if (IS_IPHONE_6Plus)
    {scroll.frame = CGRectMake(0,0, WIDTH, 750);
        
        img_bg .frame = CGRectMake(0,25, WIDTH, 210);
        
        chef_img.frame = CGRectMake(150,30,80,80);
        img_black.frame = CGRectMake(0,67,80,20);
        lbl_on_reuest.frame = CGRectMake(9,0, 100,20);
        
        lbl_chef_name.frame = CGRectMake(150,CGRectGetMaxY(chef_img.frame), 200,45);
        img_pencil.frame = CGRectMake(380,35,20,20);
        //  collView_for_icons = [[UICollectionView alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(chef_img.frame)+45,WIDTH,50)
        //collectionViewLayout:layout];
        //      img_strip.frame = CGRectMake(0,CGRectGetMaxY(collView_for_icons.frame)+2,78, 3);
        
        view_personal.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg1.frame = CGRectMake(0,10, WIDTH+5, 600);
        txtview_adddescription.frame = CGRectMake(20,20, 375,60);
        img_line .frame = CGRectMake(20,CGRectGetMaxY(txtview_adddescription.frame)+5,360, 0.5);
        table_for_personal_info.frame  = CGRectMake(13,CGRectGetMaxY(img_line.frame),385,600);
        //       img_strip1.frame = CGRectMake(CGRectGetMaxX(img_strip.frame)+33,CGRectGetMaxY(collView_for_icons.frame)+2,55, 3);
        view_for_kitchen.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg2.frame = CGRectMake(0,10, WIDTH+2, 150);
        table_for_kitchen_info.frame  = CGRectMake(8,4,390,140);
        img_bg_for_kitchen_imgs.frame = CGRectMake(0,CGRectGetMaxY(img_bg2.frame)+5, WIDTH+2,300);
        img_video.frame = CGRectMake(16,10,380,100);
        //  collView_for_kitchen_imgs = [[UICollectionView alloc] initWithFrame:CGRectMake(10,CGRectGetMaxY(img_video.frame),300,50)
        //  collectionViewLayout:layout2];
        img_bg_for_food_storage.frame = CGRectMake(0,CGRectGetMaxY(img_bg_for_kitchen_imgs.frame)+5, WIDTH+2, 150);
        table_for_food_storage.frame  = CGRectMake(8,4,390,140);
        //       img_strip2.frame = CGRectMake(CGRectGetMaxX(img_strip1.frame)+7,CGRectGetMaxY(collView_for_icons.frame)+2,77, 3);
        
        view_for_dine_in.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_for_din_in_area.frame = CGRectMake(0,10, WIDTH+2,270);
        lbl_dine_in_area.frame = CGRectMake(20,0, 200,45);
        img_din_in_area.frame = CGRectMake(15,CGRectGetMaxY(lbl_dine_in_area.frame)-5, WIDTH-33,100);
        icon_no_of_seats.frame = CGRectMake(25,CGRectGetMaxY(img_din_in_area.frame)+25, 35,20);
        lbl_no_of_seats.frame = CGRectMake(CGRectGetMaxX(icon_no_of_seats.frame)+10,CGRectGetMaxY(img_din_in_area.frame)+5, 200,45);
        lbl_no_of_seats_val.frame = CGRectMake(CGRectGetMaxX(icon_no_of_seats.frame)+10,CGRectGetMaxY(lbl_no_of_seats.frame)-25, 200,45);
        img_line2.frame =  CGRectMake(20,CGRectGetMaxY(lbl_no_of_seats_val.frame)-10, 360, 0.5);
        icon_parking.frame = CGRectMake(25,CGRectGetMaxY(img_line2.frame)+10, 35,35);
        lbl_parking.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(img_line2.frame)-2, 200,45);
        lbl_parking_val.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-25, 200,45);
        
        //       img_strip3.frame = CGRectMake(CGRectGetMaxX(img_strip2.frame)+3,CGRectGetMaxY(collView_for_icons.frame)+2,80, 3);
        view_for_menu.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_for_dietary.frame = CGRectMake(0,10, WIDTH+2,140);
        table_for_menu.frame  = CGRectMake(8,4,390,130);
        img_bg_for_menu_items.frame = CGRectMake(0,CGRectGetMaxY(img_bg_for_dietary.frame)+35, WIDTH+2,200);
        //  collView_for_menu_items = [[UICollectionView alloc] initWithFrame:CGRectMake(10,5,300,50)
        //  collectionViewLayout:layout3];
        view_for_schedule.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_calender.frame = CGRectMake(0,10, WIDTH, 200);
        img_bg_for_schedule.frame = CGRectMake(0,CGRectGetMaxY(img_calender.frame), WIDTH, 155);
        table_for_schedule.frame  = CGRectMake(10,0,390,155);
        btn_on_schedule_img.frame = CGRectMake(20,CGRectGetMaxY(img_bg_for_schedule.frame)+10, WIDTH-40, 45);
        view_for_schedule_items.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_for_selected_schedule_date.frame = CGRectMake(0,05, WIDTH, 55);
        table_for_selected_schedule_deate.frame  = CGRectMake(10,0,390,55);
        table_for_dish_items.frame  = CGRectMake(10,CGRectGetMaxY(img_bg_for_selected_schedule_date.frame)+10,390,300);
        btn_img_edit.frame = CGRectMake(0,CGRectGetMaxY(table_for_dish_items.frame)+10,210,40);
        btn_img_delete.frame = CGRectMake(CGRectGetMaxX(btn_img_edit.frame),CGRectGetMaxY(table_for_dish_items.frame)+10,210,40);
        view_for_on_request.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        lbl_currently_accept_orders.frame = CGRectMake(90,02, 300, 45);
        lbl_orders_accept.frame = CGRectMake(90,CGRectGetMaxY(lbl_currently_accept_orders.frame)+5, 300, 45);
        img_green_percentage .frame = CGRectMake(20,CGRectGetMaxY(lbl_orders_accept.frame),260,15);
        lbl_percentage_val.frame = CGRectMake(CGRectGetMaxX(img_green_percentage.frame)+10,CGRectGetMaxY(lbl_orders_accept.frame)-15, 300, 45);
        lbl_avg_respond_time.frame = CGRectMake(20,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
        avg_respond_time_val.frame = CGRectMake(CGRectGetMidX(lbl_avg_respond_time.frame)+35,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
        
        view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_in_reviews.frame = CGRectMake(10,20, WIDTH-20, 250);
        label_favorite_by_user.frame = CGRectMake(20,10, 200,30);
        label_favorite_value.frame = CGRectMake(CGRectGetMaxX(label_favorite_by_user.frame)+95,-2, 295,60);
        img_line_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_favorite_by_user.frame)+10, WIDTH-50, 0.5);
        label_total_reviews.frame = CGRectMake(20,CGRectGetMaxY(img_line_in_reviews.frame)+10, 200,30);
        label_total_reviews_value.frame = CGRectMake(CGRectGetMaxX(label_total_reviews.frame)+80,CGRectGetMaxY(img_line_in_reviews.frame)+10, 150,30);
        icon_red_right_arrow.frame = CGRectMake(CGRectGetMaxX(label_total_reviews_value.frame)-100,CGRectGetMaxY( img_line_in_reviews.frame)+15,20,20);
        btn_on_right_arrow.frame = CGRectMake(4,CGRectGetMaxY(img_line_in_reviews.frame),290,50);
        img_line2_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_total_reviews.frame)+10, WIDTH-50, 0.5);
        
        label_overall_rating.frame = CGRectMake(20,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 200,30);
        img_thumb .frame = CGRectMake(CGRectGetMaxX(label_overall_rating.frame)+80,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 25,25);
        label_overall_rating_value.frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+5,CGRectGetMaxY(img_line2_in_reviews.frame)-5, 150,60);
        img_line3_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_overall_rating.frame)+10, WIDTH-50, 0.5);
        label_food_ratings.frame = CGRectMake(20,CGRectGetMaxY(img_line3_in_reviews.frame)+10, 250,30);
        label_taste.frame = CGRectMake(20,CGRectGetMaxY(label_food_ratings.frame)+10, 50,30);
        taste_value.frame = CGRectMake(CGRectGetMaxX(label_taste.frame)-10,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
        label_appeal.frame = CGRectMake(CGRectGetMaxX(taste_value.frame)+15,CGRectGetMaxY(label_food_ratings.frame)+10,70,30);
        appeal_value.frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-15,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
        label_value.frame = CGRectMake(CGRectGetMaxX(appeal_value.frame)+15,CGRectGetMaxY(label_food_ratings.frame)+10, 50,30);
        value_value.frame = CGRectMake(CGRectGetMaxX(label_value.frame)-5,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
        view_for_delivery.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        
        view_for_note.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_in_note.frame = CGRectMake(10,20, WIDTH-20, 240);
        txtview_note.frame = CGRectMake(5,0,390,100);
        btn_img_write_note.frame = CGRectMake(20,CGRectGetMaxY(img_bg_in_note.frame)+15,WIDTH-40,50);
        
        
    }
    else if(IS_IPHONE_6)
    {
        scroll.frame = CGRectMake(0,0, WIDTH,600);
        
        img_bg .frame = CGRectMake(0,25, WIDTH, 210);
        
        chef_img.frame = CGRectMake(150,30,80,80);
        img_black.frame = CGRectMake(0,67,80,20);
        lbl_on_reuest.frame = CGRectMake(9,0, 100,20);
        
        lbl_chef_name.frame = CGRectMake(150,CGRectGetMaxY(chef_img.frame), 200,45);
        img_pencil.frame = CGRectMake(340,35,20,20);
        //     collView_for_icons = [[UICollectionView alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(chef_img.frame)+45,WIDTH,50)
        //  collectionViewLayout:layout];
        //       img_strip.frame = CGRectMake(0,CGRectGetMaxY(collView_for_icons.frame)+2,78, 3);
        
        view_personal.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg1.frame = CGRectMake(0,10, WIDTH+5, 500);
        txtview_adddescription.frame = CGRectMake(20,20, 350,60);
        img_line .frame = CGRectMake(20,CGRectGetMaxY(txtview_adddescription.frame)+5,330, 0.5);
        table_for_personal_info.frame  = CGRectMake(13,CGRectGetMaxY(img_line.frame),350,450);
        //       img_strip1.frame = CGRectMake(CGRectGetMaxX(img_strip.frame)+33,CGRectGetMaxY(collView_for_icons.frame)+2,55, 3);
        view_for_kitchen.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg2.frame = CGRectMake(0,10, WIDTH+2, 150);
        table_for_kitchen_info.frame  = CGRectMake(8,4,355,140);
        img_bg_for_kitchen_imgs.frame = CGRectMake(0,CGRectGetMaxY(img_bg2.frame)+5, WIDTH+2, 250);
        img_video.frame = CGRectMake(16,10,340,100);
        //  collView_for_kitchen_imgs = [[UICollectionView alloc] initWithFrame:CGRectMake(10,CGRectGetMaxY(img_video.frame),300,50)
        //  collectionViewLayout:layout2];
        img_bg_for_food_storage.frame = CGRectMake(0,CGRectGetMaxY(img_bg_for_kitchen_imgs.frame)+5, WIDTH+2, 150);
        table_for_food_storage.frame  = CGRectMake(8,4,355,140);
        img_strip2.frame = CGRectMake(CGRectGetMaxX(img_strip1.frame)+7,CGRectGetMaxY(collView_for_icons.frame)+2,77, 3);
        
        view_for_dine_in.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_for_din_in_area.frame = CGRectMake(0,10, WIDTH+2,270);
        lbl_dine_in_area.frame = CGRectMake(20,0, 200,45);
        img_din_in_area.frame = CGRectMake(15,CGRectGetMaxY(lbl_dine_in_area.frame)-5, WIDTH-33,100);
        icon_no_of_seats.frame = CGRectMake(25,CGRectGetMaxY(img_din_in_area.frame)+25, 35,20);
        lbl_no_of_seats.frame = CGRectMake(CGRectGetMaxX(icon_no_of_seats.frame)+10,CGRectGetMaxY(img_din_in_area.frame)+5, 200,45);
        lbl_no_of_seats_val.frame = CGRectMake(CGRectGetMaxX(icon_no_of_seats.frame)+10,CGRectGetMaxY(lbl_no_of_seats.frame)-25, 200,45);
        img_line2.frame =  CGRectMake(20,CGRectGetMaxY(lbl_no_of_seats_val.frame)-10, 330, 0.5);
        icon_parking.frame = CGRectMake(25,CGRectGetMaxY(img_line2.frame)+10, 35,35);
        lbl_parking.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(img_line2.frame)-2, 200,45);
        lbl_parking_val.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-25, 200,45);
        
        //       img_strip3.frame = CGRectMake(CGRectGetMaxX(img_strip2.frame)+3,CGRectGetMaxY(collView_for_icons.frame)+2,80, 3);
        view_for_menu.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_for_dietary.frame = CGRectMake(0,10, WIDTH+2,120);
        table_for_menu.frame  = CGRectMake(8,4,355,120);
        img_bg_for_menu_items.frame = CGRectMake(0,CGRectGetMaxY(img_bg_for_dietary.frame)+35, WIDTH+2,200);
        //  collView_for_menu_items = [[UICollectionView alloc] initWithFrame:CGRectMake(10,5,300,50)
        //  collectionViewLayout:layout3];
        view_for_schedule.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_calender.frame = CGRectMake(0,10, WIDTH, 200);
        img_bg_for_schedule.frame = CGRectMake(0,CGRectGetMaxY(img_calender.frame), WIDTH, 155);
        table_for_schedule.frame  = CGRectMake(10,0,355,155);
        btn_on_schedule_img.frame = CGRectMake(20,CGRectGetMaxY(img_bg_for_schedule.frame)+10, WIDTH-40, 45);
        view_for_schedule_items.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_for_selected_schedule_date.frame = CGRectMake(0,05, WIDTH, 55);
        table_for_selected_schedule_deate.frame  = CGRectMake(10,0,355,55);
        table_for_dish_items.frame  = CGRectMake(10,CGRectGetMaxY(img_bg_for_selected_schedule_date.frame)+10,355,300);
        btn_img_edit.frame = CGRectMake(10,CGRectGetMaxY(table_for_dish_items.frame)+10,165,40);
        btn_img_delete.frame = CGRectMake(CGRectGetMaxX(btn_img_edit.frame)+20,CGRectGetMaxY(table_for_dish_items.frame)+10,165,40);
        view_for_on_request.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        lbl_currently_accept_orders.frame = CGRectMake(90,02, 300, 45);
        lbl_orders_accept.frame = CGRectMake(90,CGRectGetMaxY(lbl_currently_accept_orders.frame)+5, 300, 45);
        img_green_percentage .frame = CGRectMake(20,CGRectGetMaxY(lbl_orders_accept.frame),260,15);
        lbl_percentage_val.frame = CGRectMake(CGRectGetMaxX(img_green_percentage.frame)+10,CGRectGetMaxY(lbl_orders_accept.frame)-15, 300, 45);
        lbl_avg_respond_time.frame = CGRectMake(20,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
        avg_respond_time_val.frame = CGRectMake(CGRectGetMidX(lbl_avg_respond_time.frame)+35,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
        view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_in_reviews.frame = CGRectMake(10,20, WIDTH-20, 250);
        label_favorite_by_user.frame = CGRectMake(20,10, 200,30);
        label_favorite_value.frame = CGRectMake(CGRectGetMaxX(label_favorite_by_user.frame)+55,-2, 295,60);
        img_line_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_favorite_by_user.frame)+10, WIDTH-50, 0.5);
        label_total_reviews.frame = CGRectMake(20,CGRectGetMaxY(img_line_in_reviews.frame)+10, 200,30);
        label_total_reviews_value.frame = CGRectMake(CGRectGetMaxX(label_total_reviews.frame)+45,CGRectGetMaxY(img_line_in_reviews.frame)+10, 150,30);
        icon_red_right_arrow.frame = CGRectMake(CGRectGetMaxX(label_total_reviews_value.frame)-100,CGRectGetMaxY( img_line_in_reviews.frame)+15,20,20);
        btn_on_right_arrow.frame = CGRectMake(4,CGRectGetMaxY(img_line_in_reviews.frame),290,50);
        img_line2_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_total_reviews.frame)+10, WIDTH-50, 0.5);
        
        label_overall_rating.frame = CGRectMake(20,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 200,30);
        img_thumb .frame = CGRectMake(CGRectGetMaxX(label_overall_rating.frame)+30,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 25,25);
        label_overall_rating_value.frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+5,CGRectGetMaxY(img_line2_in_reviews.frame)-5, 150,60);
        img_line3_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_overall_rating.frame)+10, WIDTH-50, 0.5);
        label_food_ratings.frame = CGRectMake(20,CGRectGetMaxY(img_line3_in_reviews.frame)+10, 250,30);
        label_taste.frame = CGRectMake(20,CGRectGetMaxY(label_food_ratings.frame)+10, 50,30);
        taste_value.frame = CGRectMake(CGRectGetMaxX(label_taste.frame)-10,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
        label_appeal.frame = CGRectMake(CGRectGetMaxX(taste_value.frame),CGRectGetMaxY(label_food_ratings.frame)+10,70,30);
        appeal_value.frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-15,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
        label_value.frame = CGRectMake(CGRectGetMaxX(appeal_value.frame),CGRectGetMaxY(label_food_ratings.frame)+10, 50,30);
        value_value.frame = CGRectMake(CGRectGetMaxX(label_value.frame)-5,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
        view_for_delivery.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        
        view_for_note.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_in_note.frame = CGRectMake(10,20, WIDTH-20, 240);
        txtview_note.frame = CGRectMake(5,0,355,100);
        btn_img_write_note.frame = CGRectMake(20,CGRectGetMaxY(img_bg_in_note.frame)+15,WIDTH-40,50);
        
    }
    else
    {
        scroll.frame = CGRectMake(0,0, WIDTH, 600);
        img_bg .frame = CGRectMake(0,25, WIDTH, 210);
        chef_img.frame = CGRectMake(110,30,80,80);
        img_black.frame = CGRectMake(0,67,80,20);
        lbl_on_reuest.frame = CGRectMake(9,0, 100,20);
        
        lbl_chef_name.frame = CGRectMake(110,CGRectGetMaxY(chef_img.frame), 200,45);
        img_pencil.frame = CGRectMake(280,35,20,20);
        //collView_for_icons = [[UICollectionView alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(chef_img.frame)+45,WIDTH,50)
        //collectionViewLayout:layout];
        //      img_strip.frame = CGRectMake(0,CGRectGetMaxY(collView_for_icons.frame)+2,78, 3);
        view_personal.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg1.frame = CGRectMake(0,10, WIDTH+5, 450);
        
        txtview_adddescription.frame = CGRectMake(20,20, 295,60);
        img_line .frame = CGRectMake(20,CGRectGetMaxY(txtview_adddescription.frame)+5, 280, 0.5);
        table_for_personal_info.frame  = CGRectMake(13,CGRectGetMaxY(img_line.frame),294,200);
        //       img_strip1.frame = CGRectMake(CGRectGetMaxX(img_strip.frame),CGRectGetMaxY(collView_for_icons.frame)+2,77, 3);
        view_for_kitchen.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg2.frame = CGRectMake(0,10, WIDTH+2, 150);
        table_for_kitchen_info.frame  = CGRectMake(8,4,305,140);
        img_bg_for_kitchen_imgs.frame = CGRectMake(0,CGRectGetMaxY(img_bg2.frame)+5, WIDTH+2, 250);
        img_video.frame = CGRectMake(8,10,302,100);
        // collView_for_kitchen_imgs = [[UICollectionView alloc] initWithFrame:CGRectMake(10,CGRectGetMaxY(img_video.frame),300,50)
        //     collectionViewLayout:layout2];
        img_bg_for_food_storage.frame = CGRectMake(0,CGRectGetMaxY(img_bg_for_kitchen_imgs.frame)+5, WIDTH+2, 150);
        table_for_food_storage.frame  = CGRectMake(8,4,305,140);
        //       img_strip2.frame = CGRectMake(CGRectGetMaxX(img_strip1.frame)+7,CGRectGetMaxY(collView_for_icons.frame)+2,77, 3);
        view_for_dine_in.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_for_din_in_area.frame = CGRectMake(0,10, WIDTH+2,270);
        lbl_dine_in_area.frame = CGRectMake(20,0, 200,45);
        img_din_in_area.frame = CGRectMake(15,CGRectGetMaxY(lbl_dine_in_area.frame)-5, WIDTH-33,100);
        icon_no_of_seats.frame = CGRectMake(25,CGRectGetMaxY(img_din_in_area.frame)+25, 35,20);
        lbl_no_of_seats.frame = CGRectMake(CGRectGetMaxX(icon_no_of_seats.frame)+10,CGRectGetMaxY(img_din_in_area.frame)+5, 200,45);
        lbl_no_of_seats_val.frame = CGRectMake(CGRectGetMaxX(icon_no_of_seats.frame)+10,CGRectGetMaxY(lbl_no_of_seats.frame)-25, 200,45);
        img_line2.frame =  CGRectMake(20,CGRectGetMaxY(lbl_no_of_seats_val.frame)-10, 280, 0.5);
        icon_parking.frame = CGRectMake(25,CGRectGetMaxY(img_line2.frame)+10, 35,35);
        lbl_parking.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(img_line2.frame)-2, 200,45);
        lbl_parking_val.frame = CGRectMake(CGRectGetMaxX(icon_parking.frame)+10,CGRectGetMaxY(lbl_parking.frame)-25, 200,45);
        //       img_strip3.frame = CGRectMake(CGRectGetMaxX(img_strip2.frame)+3,CGRectGetMaxY(collView_for_icons.frame)+2,80, 3);
        view_for_menu.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_for_dietary.frame = CGRectMake(0,10, WIDTH+2,100);
        table_for_menu.frame  = CGRectMake(8,4,305,120);
        img_bg_for_menu_items.frame = CGRectMake(0,CGRectGetMaxY(img_bg_for_dietary.frame)+35, WIDTH+2,200);
        //  collView_for_menu_items = [[UICollectionView alloc] initWithFrame:CGRectMake(10,5,300,50)
        //    collectionViewLayout:layout3];
        view_for_schedule.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_calender.frame = CGRectMake(0,10, WIDTH, 200);
        img_bg_for_schedule.frame = CGRectMake(0,CGRectGetMaxY(img_calender.frame), WIDTH, 155);
        table_for_schedule.frame  = CGRectMake(10,0,300,155);
        btn_on_schedule_img.frame = CGRectMake(20,CGRectGetMaxY(img_bg_for_schedule.frame)+10, WIDTH-40, 45);
        view_for_schedule_items.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_for_selected_schedule_date.frame = CGRectMake(0,05, WIDTH, 55);
        table_for_selected_schedule_deate.frame  = CGRectMake(10,0,300,55);
        table_for_dish_items.frame  = CGRectMake(10,CGRectGetMaxY(img_bg_for_selected_schedule_date.frame)+10,300,300);
        btn_img_edit.frame = CGRectMake(10,CGRectGetMaxY(table_for_dish_items.frame)+10,140,40);
        btn_img_delete.frame = CGRectMake(CGRectGetMaxX(btn_img_edit.frame)+20,CGRectGetMaxY(table_for_dish_items.frame)+10,140,40);
        view_for_on_request.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        lbl_currently_accept_orders.frame = CGRectMake(90,02, 300, 45);
        lbl_orders_accept.frame = CGRectMake(90,CGRectGetMaxY(lbl_currently_accept_orders.frame)+5, 300, 45);
        img_green_percentage .frame = CGRectMake(20,CGRectGetMaxY(lbl_orders_accept.frame),260,15);
        lbl_percentage_val.frame = CGRectMake(CGRectGetMaxX(img_green_percentage.frame)+10,CGRectGetMaxY(lbl_orders_accept.frame)-15, 300, 45);
        lbl_avg_respond_time.frame = CGRectMake(20,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
        avg_respond_time_val.frame = CGRectMake(CGRectGetMidX(lbl_avg_respond_time.frame)+35,CGRectGetMaxY(img_green_percentage.frame), 300, 45);
        view_for_reviews.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_in_reviews.frame = CGRectMake(10,20, WIDTH-20, 250);
        label_favorite_by_user.frame = CGRectMake(20,10, 200,30);
        label_favorite_value.frame = CGRectMake(CGRectGetMaxX(label_favorite_by_user.frame)+3,-2, 295,60);
        img_line_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_favorite_by_user.frame)+10, WIDTH-50, 0.5);
        label_total_reviews.frame = CGRectMake(20,CGRectGetMaxY(img_line_in_reviews.frame)+10, 200,30);
        label_total_reviews_value.frame = CGRectMake(CGRectGetMaxX(label_total_reviews.frame)-5,CGRectGetMaxY(img_line_in_reviews.frame)+10, 150,30);
        icon_red_right_arrow.frame = CGRectMake(CGRectGetMaxX(label_total_reviews_value.frame)-100,CGRectGetMaxY( img_line_in_reviews.frame)+15,20,20);
        btn_on_right_arrow.frame = CGRectMake(4,CGRectGetMaxY(img_line_in_reviews.frame),290,50);
        img_line2_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_total_reviews.frame)+10, WIDTH-50, 0.5);
        label_overall_rating.frame = CGRectMake(20,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 200,30);
        img_thumb .frame = CGRectMake(CGRectGetMaxX(label_overall_rating.frame)-15,CGRectGetMaxY(img_line2_in_reviews.frame)+10, 25,25);
        label_overall_rating_value.frame = CGRectMake(CGRectGetMaxX(img_thumb.frame)+5,CGRectGetMaxY(img_line2_in_reviews.frame)-5, 150,60);
        img_line3_in_reviews.frame = CGRectMake(10,CGRectGetMaxY(label_overall_rating.frame)+10, WIDTH-50, 0.5);
        label_food_ratings.frame = CGRectMake(20,CGRectGetMaxY(img_line3_in_reviews.frame)+10, 250,30);
        label_taste.frame = CGRectMake(10,CGRectGetMaxY(label_food_ratings.frame)+10, 50,30);
        taste_value.frame = CGRectMake(CGRectGetMaxX(label_taste.frame)-10,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
        label_appeal.frame = CGRectMake(CGRectGetMaxX(taste_value.frame)-15,CGRectGetMaxY(label_food_ratings.frame)+10,70,30);
        appeal_value.frame = CGRectMake(CGRectGetMaxX(label_appeal.frame)-15,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
        label_value.frame = CGRectMake(CGRectGetMaxX(appeal_value.frame)-20,CGRectGetMaxY(label_food_ratings.frame)+10, 50,30);
        value_value.frame = CGRectMake(CGRectGetMaxX(label_value.frame)-5,CGRectGetMaxY(label_food_ratings.frame)+10, 70,30);
        view_for_delivery.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        view_for_note.frame=CGRectMake(0,CGRectGetMaxY(img_bg.frame),WIDTH,600);
        img_bg_in_note.frame = CGRectMake(10,20, WIDTH-20, 240);
        txtview_note.frame = CGRectMake(5,0,295,100);
        btn_img_write_note.frame = CGRectMake(20,CGRectGetMaxY(img_bg_in_note.frame)+15,WIDTH-40,50);
        
    }
    
    
    [scroll setContentSize:CGSizeMake(0,900)];
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == table_for_personal_info)
    {
        //        return [ary_Personal_Information count];
        
        if ([ary_Personal_Information count]==0)
        {
            return 0;
        }
        else{
            return [ary_Personal_Information count]+12;
            
        }
        
    }
    else if (tableView == table_for_kitchen_info)
    {
        //       return [ary_Kitchen_info count];
        
        if ([ary_Kitchen_info count]==0)
        {
            return 0;
        }
        else
        {
            return [ary_Kitchen_info count]+1;
            
        }
        
    }
    else if (tableView == table_for_food_storage)
    {
        //        return [array_head_names_in_food_storage count];
        if ([ary_Kitchen_info count]==0)
        {
            return 0;
        }
        else
        {
            return [ary_Kitchen_info count]+1;
            
        }
    }
    else if (tableView == table_for_menu)
    {
        //        return [ary_Menu count];
        if ([ary_Menu count]==0)
        {
            return 0;
        }
        else
        {
            return [ary_Menu count]+1;
            
        }
    }
    else if(tableView == table_for_schedule)
    {
        return [array_serving_img count];
    }
    else if (tableView == table_for_selected_schedule_deate)
    {
        return [array_serving_date_time count];
    }
    else if (tableView == table_for_dish_items)
    {
        return [ary_displaynames count];
        
    }
    
    
    return  0;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == table_for_personal_info)
    {
        return 1;
    }
    else if (tableView == table_for_kitchen_info)
    {
        return 1;
    }
    else if (tableView == table_for_food_storage)
    {
        return 1;
    }
    else if (tableView == table_for_menu)
    {
        return 1;
    }
    else if(tableView == table_for_schedule)
    {
        return 1;
    }
    else if (tableView == table_for_selected_schedule_deate)
    {
        return 1;
    }
    else if (tableView == table_for_dish_items)
    {
        return 1;
    }
    
    
    
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table_for_personal_info)
    {
        return 50;
    }
    else if (tableView == table_for_kitchen_info)
    {
        return 70;
    }
    else if (tableView == table_for_food_storage)
    {
        
        return 70;
    }
    else if (tableView == table_for_menu)
    {
        return 65;
    }
    else if (tableView == table_for_schedule)
    {
        return 50;
    }
    else if (tableView == table_for_selected_schedule_deate)
    {
        return 50;
    }
    else if (tableView == table_for_dish_items)
    {
        
        return 170;
    }
    
    
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    if (tableView == table_for_personal_info)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        //   img_cellBackGnd.frame =  CGRectMake(0,2, 294, 50);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor whiteColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UIImageView *img_line = [[UIImageView alloc]init];
        //      img_line.frame =  CGRectMake(7,49, 280, 0.5);
        img_line.backgroundColor =[UIColor grayColor];
        [img_line setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line];
        
        UIImageView *info_imges = [[UIImageView alloc]init];
        //     info_imges.frame =  CGRectMake(10,10, 30, 30);
        [info_imges setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_img objectAtIndex:indexPath.row]]]];
        info_imges.backgroundColor =[UIColor clearColor];
        [info_imges setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:info_imges];
        
        UILabel *chef_info_titiles = [[UILabel alloc]init];
        //   chef_info_titiles .frame = CGRectMake(50,10,200, 15);
        chef_info_titiles .text = [NSString stringWithFormat:@"%@",[ary_chef_info_titles objectAtIndex:indexPath.row]];
        chef_info_titiles .font = [UIFont fontWithName:kFontBold size:12];
        chef_info_titiles .textColor = [UIColor blackColor];
        chef_info_titiles .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:chef_info_titiles ];
        
        UILabel *chef_detailes = [[UILabel alloc]init];
        chef_detailes .font = [UIFont fontWithName:kFont size:10];

        if ([array_chef_details count]==0)
        {
        }
        else{
            if (indexPath.row == 0)
            {
                
                
                NSLog(@" user name %@",[NSString stringWithFormat:@"%@",[[[ary_Personal_Information objectAtIndex:0] valueForKey:@"Personal_Information"] valueForKey:@"UserName"]]);
                
                chef_detailes .text = [NSString stringWithFormat:@"%@",[[ary_Personal_Information objectAtIndex:0] valueForKey:@"UserName"]];
                
            }
            else if (indexPath.row == 1)
            {
                
                chef_detailes .text = [NSString stringWithFormat:@"%@",[[ary_Personal_Information objectAtIndex:0] valueForKey:@"Full_Name"]];
                
            }
            else if (indexPath.row == 2)
            {
                
                chef_detailes .text = [NSString stringWithFormat:@"%@",[[ary_Personal_Information objectAtIndex:0] valueForKey:@"Chef_Type"]];
                
            }
            else if (indexPath.row == 3)
            {
                
                chef_detailes .text = [NSString stringWithFormat:@"%@",[[ary_Personal_Information objectAtIndex:0] valueForKey:@"DateOfBirth"]];
                
            }
            else if (indexPath.row == 4)
            {
                
                chef_detailes .text = [NSString stringWithFormat:@"%@",[[ary_Personal_Information objectAtIndex:0] valueForKey:@"DateOfBirth"]];
                
            }
            else if (indexPath.row == 5)
            {
                
                chef_detailes .text = [NSString stringWithFormat:@"%@",[[ary_Personal_Information objectAtIndex:0] valueForKey:@"Email_Address"]];
                
            }
            else if (indexPath.row == 6)
            {
                
                chef_detailes .text = [NSString stringWithFormat:@"%@",[[ary_Personal_Information objectAtIndex:0] valueForKey:@"Mobile_Number"]];
                
            }
            
            else if (indexPath.row == 7)
            {
                
                chef_detailes .text = [NSString stringWithFormat:@"%@",[[ary_Personal_Information objectAtIndex:0] valueForKey:@"Home_Address"]];
                
            }
            else if (indexPath.row == 8)
            {
                
                chef_detailes .text = [NSString stringWithFormat:@"%@",[[ary_Personal_Information objectAtIndex:0] valueForKey:@"Serving_Type"]];
                
            }
            else if (indexPath.row == 9)
            {
                
                chef_detailes .text = [NSString stringWithFormat:@"%@",[[ary_Personal_Information objectAtIndex:0] valueForKey:@"Cooking_Qualification"]];
                
            }
            else if (indexPath.row == 10)
            {
                
                chef_detailes .text = [NSString stringWithFormat:@"%@",[[ary_Personal_Information objectAtIndex:0] valueForKey:@"Cooking_Experience"]];
                
            }
            else if (indexPath.row == 11)
            {
                
                chef_detailes .text = [NSString stringWithFormat:@"%@",[[ary_Personal_Information objectAtIndex:0] valueForKey:@"Paypal_Account"]];
                
            }
            else if (indexPath.row == 12)
            {
                
                chef_detailes .text = [NSString stringWithFormat:@"%@",[[ary_Personal_Information objectAtIndex:0] valueForKey:@"Payment_Recieved"]];
                
            }
            
            // NSLog(@" user name %@",[NSString stringWithFormat:@"%@",[[[ary_Chefprofile_info objectAtIndex:0] valueForKey:@"Personal_Information"] valueForKey:@"UserName"]]);
            
        }
        
        
        chef_detailes .font = [UIFont fontWithName:kFont size:10];
        chef_detailes .textColor = [UIColor blackColor];
        chef_detailes.backgroundColor = [UIColor redColor];
        [img_cellBackGnd addSubview:chef_detailes ];
        
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, 385, 50);
            img_line.frame =  CGRectMake(7,49,360, 0.5);
            info_imges.frame =  CGRectMake(10,10, 30, 30);
            chef_info_titiles .frame = CGRectMake(50,10,200, 15);
            chef_detailes .frame = CGRectMake(50,30,200, 15);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, 350, 50);
            img_line.frame =  CGRectMake(7,49, 330, 0.5);
            info_imges.frame =  CGRectMake(10,10, 30, 30);
            chef_info_titiles .frame = CGRectMake(50,10,200, 15);
            chef_detailes .frame = CGRectMake(50,30,200, 15);
            
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, 294, 50);
            img_line.frame =  CGRectMake(7,49, 280, 0.5);
            info_imges.frame =  CGRectMake(10,10, 30, 30);
            chef_info_titiles .frame = CGRectMake(50,10,200, 15);
            chef_detailes .frame = CGRectMake(50,30,200, 15);
            
            
        }
    }
    else if (tableView == table_for_kitchen_info)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        img_cellBackGnd.frame =  CGRectMake(0,2, 310, 70);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor whiteColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UIImageView *kitchen_icon_in_cell = [[UIImageView alloc]init];
        kitchen_icon_in_cell.frame =  CGRectMake(20,10, 40, 40);
        [kitchen_icon_in_cell setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_icon_kitchen objectAtIndex:indexPath.row]]]];
        kitchen_icon_in_cell.backgroundColor =[UIColor clearColor];
        [kitchen_icon_in_cell setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:kitchen_icon_in_cell];
        
        UILabel *lbl_head_in_kitchen_cell = [[UILabel alloc]init];
        lbl_head_in_kitchen_cell .frame = CGRectMake(CGRectGetMaxX(kitchen_icon_in_cell.frame)+15,10,200, 15);
        lbl_head_in_kitchen_cell .text = [NSString stringWithFormat:@"%@",[array_head_names_in_kitchen objectAtIndex:indexPath.row]];
        lbl_head_in_kitchen_cell .font = [UIFont fontWithName:kFontBold size:15];
        lbl_head_in_kitchen_cell .textColor = [UIColor blackColor];
        lbl_head_in_kitchen_cell .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_head_in_kitchen_cell ];
        
        
//        NSString *ImagePath1 =[[NSString stringWithFormat:@"%@",[[[TheDict valueForKey:@"profile_info"] valueForKey:@"Dine-In"] valueForKey:@"Dine_In_Pictures"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        [img_din_in_area setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@""]];
        
        UILabel *lbl_kitchen_details = [[UILabel alloc]init];
        lbl_kitchen_details .frame = CGRectMake(CGRectGetMaxX(kitchen_icon_in_cell.frame)+15,CGRectGetMaxY(lbl_head_in_kitchen_cell.frame)+10,200, 15);
                
        if ([ary_Kitchen_info count]==0)
        {
        }
        else{
            if (indexPath.row == 0)
            {
                
                
              
                
                lbl_kitchen_details .text = [NSString stringWithFormat:@"%@",[[ary_Kitchen_info objectAtIndex:0] valueForKey:@"Kitchen_Name"]];
                
            }
            else
            {
                
                lbl_kitchen_details .text = [NSString stringWithFormat:@"%@",[[ary_Kitchen_info objectAtIndex:0] valueForKey:@"Kitchen_Address"]];
                
            }
            
        }
        
        
        //        lbl_kitchen_details .text = [NSString stringWithFormat:@"%@",[array_kitchen_name objectAtIndex:indexPath.row]];
        lbl_kitchen_details .font = [UIFont fontWithName:kFont size:12];
        lbl_kitchen_details .textColor = [UIColor blackColor];
        lbl_kitchen_details.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_kitchen_details];
        
        UIImageView *img_line = [[UIImageView alloc]init];
        img_line.frame =  CGRectMake(7,67, 280, 0.5);
        img_line.backgroundColor =[UIColor grayColor];
        [img_line setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line];
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, 390, 70);
            kitchen_icon_in_cell.frame =  CGRectMake(20,10, 40, 40);
            lbl_head_in_kitchen_cell .frame = CGRectMake(CGRectGetMaxX(kitchen_icon_in_cell.frame)+15,10,200, 15);
            lbl_kitchen_details .frame = CGRectMake(CGRectGetMaxX(kitchen_icon_in_cell.frame)+15,CGRectGetMaxY(lbl_head_in_kitchen_cell.frame)+10,200, 15);
            
            img_line.frame =  CGRectMake(7,67,360, 0.5);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, 355, 70);
            kitchen_icon_in_cell.frame =  CGRectMake(20,10, 40, 40);
            lbl_head_in_kitchen_cell .frame = CGRectMake(CGRectGetMaxX(kitchen_icon_in_cell.frame)+15,10,200, 15);
            lbl_kitchen_details .frame = CGRectMake(CGRectGetMaxX(kitchen_icon_in_cell.frame)+15,CGRectGetMaxY(lbl_head_in_kitchen_cell.frame)+10,200, 15);
            
            img_line.frame =  CGRectMake(7,67, 330, 0.5);
            
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, 310, 70);
            kitchen_icon_in_cell.frame =  CGRectMake(20,10, 40, 40);
            lbl_head_in_kitchen_cell .frame = CGRectMake(CGRectGetMaxX(kitchen_icon_in_cell.frame)+15,10,200, 15);
            lbl_kitchen_details .frame = CGRectMake(CGRectGetMaxX(kitchen_icon_in_cell.frame)+15,CGRectGetMaxY(lbl_head_in_kitchen_cell.frame)+10,200, 15);
            
            img_line.frame =  CGRectMake(7,67, 280, 0.5);
            
        }
        
    }
    else if (tableView == table_for_food_storage)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        img_cellBackGnd.frame =  CGRectMake(0,2, 310, 70);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor whiteColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UIImageView *kitchen_storage_icons = [[UIImageView alloc]init];
        kitchen_storage_icons.frame =  CGRectMake(20,10, 30, 35);
        [kitchen_storage_icons setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_icon_storages_hygiene objectAtIndex:indexPath.row]]]];
        kitchen_storage_icons.backgroundColor =[UIColor clearColor];
        [kitchen_storage_icons setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:kitchen_storage_icons];
        
        UILabel *lbl_head_in_kitchen_storage_cell = [[UILabel alloc]init];
        lbl_head_in_kitchen_storage_cell .frame = CGRectMake(CGRectGetMaxX(kitchen_storage_icons.frame)+15,10,200, 15);
        lbl_head_in_kitchen_storage_cell .text = [NSString stringWithFormat:@"%@",[array_head_names_in_food_storage objectAtIndex:indexPath.row]];
        lbl_head_in_kitchen_storage_cell .font = [UIFont fontWithName:kFontBold size:15];
        lbl_head_in_kitchen_storage_cell .textColor = [UIColor blackColor];
        lbl_head_in_kitchen_storage_cell .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_head_in_kitchen_storage_cell ];
        
        txtview_adddescription = [[UITextView alloc]init];
        txtview_adddescription.frame = CGRectMake(CGRectGetMaxX(kitchen_storage_icons.frame)+15,CGRectGetMaxY(lbl_head_in_kitchen_storage_cell.frame)-5, 240,40);
        txtview_adddescription.scrollEnabled = YES;
        txtview_adddescription.userInteractionEnabled = NO;
        txtview_adddescription.font = [UIFont fontWithName:kFont size:12];
        txtview_adddescription.backgroundColor = [UIColor clearColor];
        txtview_adddescription.delegate = self;
        txtview_adddescription.textColor = [UIColor blackColor];
        if ([ary_Kitchen_info count]==0)
        {
        }
        else{
            if (indexPath.row == 0)
            {
                
                
                //                NSLog(@" user name %@",[NSString stringWithFormat:@"%@",[[[ary_Personal_Information objectAtIndex:0] valueForKey:@"Personal_Information"] valueForKey:@"UserName"]]);
                
                txtview_adddescription .text = [NSString stringWithFormat:@"%@",[[ary_Kitchen_info objectAtIndex:0] valueForKey:@"Food_Storage"]];
                
            }
            else
            {
                
                txtview_adddescription .text = [NSString stringWithFormat:@"%@",[[ary_Kitchen_info objectAtIndex:0] valueForKey:@"Kitchen_Hygien_Standard"]];
                
            }
            
        }
        
        
        //        txtview_adddescription.text =@"LOrem ipsumdotor sit amet.ne dicit virits corpor a per,ut illum liberavisse vet.At mei vidit conse cteture.in vitae definitionem per.tate vetear cliquado ei vim.";
        [img_cellBackGnd addSubview:txtview_adddescription];
        
        UIImageView *img_line = [[UIImageView alloc]init];
        img_line.frame =  CGRectMake(7,CGRectGetMaxY(txtview_adddescription.frame)+10, 280, 0.5);
        img_line.backgroundColor =[UIColor grayColor];
        [img_line setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line];
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, 390, 70);
            kitchen_storage_icons.frame =  CGRectMake(20,10, 30, 35);
            lbl_head_in_kitchen_storage_cell .frame = CGRectMake(CGRectGetMaxX(kitchen_storage_icons.frame)+15,10,200, 15);
            txtview_adddescription.frame = CGRectMake(CGRectGetMaxX(kitchen_storage_icons.frame)+15,CGRectGetMaxY(lbl_head_in_kitchen_storage_cell.frame)-5, 240,40);
            img_line.frame =  CGRectMake(7,CGRectGetMaxY(txtview_adddescription.frame)+10,360, 0.5);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, 355, 70);
            kitchen_storage_icons.frame =  CGRectMake(20,10, 30, 35);
            lbl_head_in_kitchen_storage_cell .frame = CGRectMake(CGRectGetMaxX(kitchen_storage_icons.frame)+15,10,200, 15);
            txtview_adddescription.frame = CGRectMake(CGRectGetMaxX(kitchen_storage_icons.frame)+15,CGRectGetMaxY(lbl_head_in_kitchen_storage_cell.frame)-5, 240,40);
            img_line.frame =  CGRectMake(7,CGRectGetMaxY(txtview_adddescription.frame)+10, 330, 0.5);
            
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, 310, 70);
            kitchen_storage_icons.frame =  CGRectMake(20,10, 30, 35);
            lbl_head_in_kitchen_storage_cell .frame = CGRectMake(CGRectGetMaxX(kitchen_storage_icons.frame)+15,10,200, 15);
            txtview_adddescription.frame = CGRectMake(CGRectGetMaxX(kitchen_storage_icons.frame)+15,CGRectGetMaxY(lbl_head_in_kitchen_storage_cell.frame)-5, 240,40);
            img_line.frame =  CGRectMake(7,CGRectGetMaxY(txtview_adddescription.frame)+10, 280, 0.5);
            
        }
        
        
        
        
    }
    else if (tableView == table_for_menu)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        img_cellBackGnd.frame =  CGRectMake(0,2, 310, 65);
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor whiteColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UIImageView *dietary_icons = [[UIImageView alloc]init];
        dietary_icons.frame =  CGRectMake(20,10, 30, 30);
        [dietary_icons setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_dietary_icons objectAtIndex:indexPath.row]]]];
        dietary_icons.backgroundColor =[UIColor clearColor];
        [dietary_icons setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:dietary_icons];
        
        UILabel *lbl_head_in_dietary_cell = [[UILabel alloc]init];
        lbl_head_in_dietary_cell .frame = CGRectMake(CGRectGetMaxX(dietary_icons.frame)+15,10,250, 15);
        lbl_head_in_dietary_cell .text = [NSString stringWithFormat:@"%@",[array_head_names_in_dietary objectAtIndex:indexPath.row]];
        lbl_head_in_dietary_cell .font = [UIFont fontWithName:kFontBold size:12];
        lbl_head_in_dietary_cell .textColor = [UIColor blackColor];
        lbl_head_in_dietary_cell .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_head_in_dietary_cell];
        
        UILabel *text_in_dietary_cell = [[UILabel alloc]init];
        text_in_dietary_cell .frame = CGRectMake(CGRectGetMaxX(dietary_icons.frame)+15,CGRectGetMaxY(lbl_head_in_dietary_cell.frame)+5,250, 15);
        text_in_dietary_cell .text = [NSString stringWithFormat:@"%@",[array_text_in_dietary_cell objectAtIndex:indexPath.row]];
        
        
        if ([ary_Kitchen_info count]==0)
        {
        }
        else{
            if (indexPath.row == 0)
            {
                
                
                //                NSLog(@" user name %@",[NSString stringWithFormat:@"%@",[[[ary_Personal_Information objectAtIndex:0] valueForKey:@"Personal_Information"] valueForKey:@"UserName"]]);
                
                text_in_dietary_cell .text = [NSString stringWithFormat:@"%@",[[ary_Menu objectAtIndex:0] valueForKey:@"Cuisine_Cooked"]];
                
            }
            else
            {
                
                text_in_dietary_cell .text = [NSString stringWithFormat:@"%@",[[ary_Menu objectAtIndex:0] valueForKey:@"Dietary_Restrictions_Followed"]];
                
            }
            
        }
        
        text_in_dietary_cell .font = [UIFont fontWithName:kFont size:12];
        text_in_dietary_cell .textColor = [UIColor blackColor];
        text_in_dietary_cell .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:text_in_dietary_cell];
        
        UIImageView *img_line = [[UIImageView alloc]init];
        img_line.frame =  CGRectMake(7,CGRectGetMaxY(text_in_dietary_cell.frame)+15, 280, 0.5);
        img_line.backgroundColor =[UIColor grayColor];
        [img_line setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line];
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, 390, 65);
            dietary_icons.frame =  CGRectMake(20,10, 30, 30);
            lbl_head_in_dietary_cell .frame = CGRectMake(CGRectGetMaxX(dietary_icons.frame)+15,10,250, 15);
            text_in_dietary_cell .frame = CGRectMake(CGRectGetMaxX(dietary_icons.frame)+15,CGRectGetMaxY(lbl_head_in_dietary_cell.frame)+5,250, 15);
            img_line.frame =  CGRectMake(7,CGRectGetMaxY(text_in_dietary_cell.frame)+15, 280, 0.5);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(3,0,355,65);
            dietary_icons.frame =  CGRectMake(20,10, 30, 30);
            lbl_head_in_dietary_cell .frame = CGRectMake(CGRectGetMaxX(dietary_icons.frame)+15,10,250, 15);
            text_in_dietary_cell .frame = CGRectMake(CGRectGetMaxX(dietary_icons.frame)+15,CGRectGetMaxY(lbl_head_in_dietary_cell.frame)+5,250, 15);
            img_line.frame =  CGRectMake(7,CGRectGetMaxY(text_in_dietary_cell.frame)+15, 280, 0.5);
            
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, 310, 65);
            dietary_icons.frame =  CGRectMake(20,10, 30, 30);
            lbl_head_in_dietary_cell .frame = CGRectMake(CGRectGetMaxX(dietary_icons.frame)+15,10,250, 15);
            text_in_dietary_cell .frame = CGRectMake(CGRectGetMaxX(dietary_icons.frame)+15,CGRectGetMaxY(lbl_head_in_dietary_cell.frame)+5,250, 15);
            img_line.frame =  CGRectMake(7,CGRectGetMaxY(text_in_dietary_cell.frame)+15, 280, 0.5);
            
        }
        
    }
    
    else if (tableView == table_for_schedule)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        //    img_cellBackGnd.frame =  CGRectMake(0,2, 320, 50);
        //    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor whiteColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UIImageView *img_line = [[UIImageView alloc]init];
        //    img_line.frame =  CGRectMake(7,49, 280, 0.5);
        img_line.backgroundColor =[UIColor grayColor];
        [img_line setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line];
        
        UIImageView *servingimges = [[UIImageView alloc]init];
        //   servingimges.frame =  CGRectMake(10,10, 30, 30);
        [servingimges setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_serving_img objectAtIndex:indexPath.row]]]];
        servingimges.backgroundColor =[UIColor clearColor];
        [servingimges setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:servingimges];
        
        UILabel *serving_timeings = [[UILabel alloc]init];
        //    serving_timeings .frame = CGRectMake(50,10,200, 15);
        serving_timeings .text = [NSString stringWithFormat:@"%@",[array_serving_time objectAtIndex:indexPath.row]];
        serving_timeings .font = [UIFont fontWithName:kFontBold size:12];
        serving_timeings .textColor = [UIColor blackColor];
        serving_timeings .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:serving_timeings ];
        
        UILabel *serving_items = [[UILabel alloc]init];
        //    serving_items .frame = CGRectMake(50,30,200, 15);
        serving_items .text = [NSString stringWithFormat:@"%@",[ array_serving_items objectAtIndex:indexPath.row]];
        serving_items .font = [UIFont fontWithName:kFont size:10];
        serving_items .textColor = [UIColor blackColor];
        serving_items.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:serving_items ];
        
        
        UIButton *icon_right_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
        //    icon_right_arrow.frame = CGRectMake(260,10, 20, 20);
        //icon_thumb .backgroundColor = [UIColor clearColor];
//          [icon_right_arrow addTarget:self action:@selector(icon_right_arrow_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [icon_right_arrow setImage:[UIImage imageNamed:@"right-arrow-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_right_arrow];
        
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-20, 50);
            img_line.frame =  CGRectMake(7,49, WIDTH-20, 0.5);
            servingimges.frame =  CGRectMake(10,10, 30, 30);
            serving_timeings .frame = CGRectMake(50,10,200, 15);
            serving_items .frame = CGRectMake(50,30,300, 15);
            icon_right_arrow.frame = CGRectMake(WIDTH-68,15, 20, 20);
            //  btn_on_scedule_cell.frame = CGRectMake(0,0, 300, 49);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, 355, 50);
            img_line.frame =  CGRectMake(7,49,335, 0.5);
            servingimges.frame =  CGRectMake(10,10, 30, 30);
            serving_timeings .frame = CGRectMake(50,10,200, 15);
            serving_items .frame = CGRectMake(50,30,300, 15);
            icon_right_arrow.frame = CGRectMake(WIDTH-50,15, 20, 20);
            //  btn_on_scedule_cell.frame = CGRectMake(0,0, 300, 49);
            
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, 320, 50);
            img_line.frame =  CGRectMake(7,49, 280, 0.5);
            servingimges.frame =  CGRectMake(10,10, 30, 30);
            serving_timeings .frame = CGRectMake(50,10,200, 15);
            serving_items .frame = CGRectMake(50,30,200, 15);
            icon_right_arrow.frame = CGRectMake(260,15, 20, 20);
            //  btn_on_scedule_cell.frame = CGRectMake(0,0, 300,49);
            
        }
        
        
    }
    else if (tableView == table_for_selected_schedule_deate)
    {
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        //    img_cellBackGnd.frame =  CGRectMake(0,2, 320, 50);
        //    [img_cellBackGnd setImage:[UIImage imageNamed:@"bg@2x.png"]];
        img_cellBackGnd.backgroundColor =[UIColor whiteColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        UIImageView *img_line = [[UIImageView alloc]init];
        //    img_line.frame =  CGRectMake(7,49, 280, 0.5);
        img_line.backgroundColor =[UIColor grayColor];
        [img_line setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:img_line];
        
        UIButton *icon_left_arrow = [UIButton buttonWithType:UIButtonTypeCustom];
        // icon_left_arrow.frame = CGRectMake(260,10, 20, 20);
        //icon_left_arrow .backgroundColor = [UIColor clearColor];
        // [icon_left_arrow addTarget:self action:@selector(icon_right_arrow_BtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [icon_left_arrow setImage:[UIImage imageNamed:@"left-arrow-img@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_left_arrow];
        
        
        UIImageView *servingimges = [[UIImageView alloc]init];
        //   servingimges.frame =  CGRectMake(10,10, 30, 30);
        [servingimges setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_serving_img objectAtIndex:indexPath.row]]]];
        servingimges.backgroundColor =[UIColor clearColor];
        [servingimges setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:servingimges];
        
        UILabel *serving_date_time = [[UILabel alloc]init];
        //    serving_date_time .frame = CGRectMake(50,10,200, 15);
        serving_date_time .text = [NSString stringWithFormat:@"%@",[array_serving_date_time objectAtIndex:indexPath.row]];
        serving_date_time .font = [UIFont fontWithName:kFontBold size:12];
        serving_date_time .textColor = [UIColor blackColor];
        serving_date_time .backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:serving_date_time ];
        
        UIButton *btn_on_scedule_cell = [UIButton buttonWithType:UIButtonTypeCustom];
        btn_on_scedule_cell.frame = CGRectMake(5,5, 200, 20);
        btn_on_scedule_cell .backgroundColor = [UIColor clearColor];
        [btn_on_scedule_cell addTarget:self action:@selector(click_on_left_arrow:) forControlEvents:UIControlEventTouchUpInside];
        //[btn_on_scedule_cell setImage:[UIImage imageNamed:@"right-arrow-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:btn_on_scedule_cell];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(5,2, WIDTH-20, 50);
            img_line.frame =  CGRectMake(7,49, WIDTH-20, 0.5);
            
            icon_left_arrow.frame = CGRectMake(05,15, 15, 15);
            servingimges.frame =  CGRectMake(CGRectGetMaxX(icon_left_arrow.frame)+10,10, 30, 30);
            serving_date_time .frame = CGRectMake(70,15,200, 15);
            
            btn_on_scedule_cell.frame = CGRectMake(0,0, 300, 49);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, 355, 50);
            img_line.frame =  CGRectMake(7,49, 355, 0.5);
            
            icon_left_arrow.frame = CGRectMake(05,15, 15, 15);
            servingimges.frame =  CGRectMake(CGRectGetMaxX(icon_left_arrow.frame)+10,10, 30, 30);
            serving_date_time .frame = CGRectMake(70,15,200, 15);
            
            btn_on_scedule_cell.frame = CGRectMake(0,0, 300, 49);
            
        }
        else
        {
            img_cellBackGnd.frame =  CGRectMake(0,2, 320, 50);
            img_line.frame =  CGRectMake(7,49, 280, 0.5);
            
            icon_left_arrow.frame = CGRectMake(05,15, 15, 15);
            servingimges.frame =  CGRectMake(CGRectGetMaxX(icon_left_arrow.frame)+10,10, 30, 30);
            serving_date_time .frame = CGRectMake(70,15,200, 15);
            
            btn_on_scedule_cell.frame = CGRectMake(0,0, 300,49);
            
        }
        
    }
    
    else if (tableView == table_for_dish_items)
    {
        
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus)
        {
            img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH+5, 170);
            
        }
        else if (IS_IPHONE_6)
        {
            img_cellBackGnd.frame =  CGRectMake(0,0, WIDTH-5, 170);
            
        }
        else
        {    img_cellBackGnd.frame =  CGRectMake(0,15, WIDTH-10,170);
            
        }
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        img_cellBackGnd.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        
        UIImageView *img_dish = [[UIImageView alloc] init];
        //    img_dish.frame = CGRectMake(7,7, 90,  95 );
        [img_dish setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_dish_img objectAtIndex:indexPath.row]]]];
        [img_cellBackGnd addSubview:img_dish];
        
        
        UILabel *dish_name = [[UILabel alloc]init];
        //   dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
        dish_name.text = [NSString stringWithFormat:@"%@",[ary_displaynames objectAtIndex:indexPath.row]];
        dish_name.font = [UIFont fontWithName:kFontBold size:8];
        dish_name.textColor = [UIColor blackColor];
        dish_name.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:dish_name];
        
        
        UIImageView *icon_location = [[UIImageView alloc] init];
        //    icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
        [icon_location setImage:[UIImage imageNamed:@"location-icon@2x.png"]];
        [img_cellBackGnd  addSubview:icon_location];
        
        UILabel *meters = [[UILabel alloc]init];
        //  meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
        meters.text = @"5km";
        meters.font = [UIFont fontWithName:kFont size:15];
        meters.textColor = [UIColor blackColor];
        meters.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:meters];
        
        UIButton *icon_server = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
        //icon_server .backgroundColor = [UIColor clearColor];
        [icon_server  addTarget:self action:@selector(click_on_seve_icon:) forControlEvents:UIControlEventTouchUpInside];
        [icon_server setImage:[UIImage imageNamed:@"serving-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_server];
        
        UIButton *icon_halal = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
        //icon_halal .backgroundColor = [UIColor clearColor];
        [icon_halal addTarget:self action:@selector(click_on_halal:) forControlEvents:UIControlEventTouchUpInside];
        [icon_halal setImage:[UIImage imageNamed:@"halal-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_halal];
        
        UIImageView *img_non_veg = [[UIImageView alloc] init];
        //  img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
        [img_non_veg setImage:[UIImage imageNamed:@"non-veg bt@2x.png"]];
        [img_cellBackGnd addSubview:img_non_veg];
        
        UIButton *icon_cow = [UIButton buttonWithType:UIButtonTypeCustom];
        //  icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_cow addTarget:self action:@selector(click_on_cow_icon:) forControlEvents:UIControlEventTouchUpInside];
        [icon_cow setImage:[UIImage imageNamed:@"cow-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_cow];
        
        UIButton *icon_fronce = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
        //icon_cow .backgroundColor = [UIColor clearColor];
        [icon_fronce addTarget:self action:@selector(click_on_icon_fronce:) forControlEvents:UIControlEventTouchUpInside];
        [icon_fronce setImage:[UIImage imageNamed:@"fronce-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_fronce];
        
        UILabel *doller_rate = [[UILabel alloc]init];
        //   doller_rate.frame = CGRectMake(WIDTH-70,85,200, 15);
        doller_rate.text = @"$14.90";
        doller_rate.font = [UIFont fontWithName:kFontBold size:16];
        doller_rate.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        doller_rate.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:doller_rate];
        
        UIImageView *img_line = [[UIImageView alloc] init];
        //   img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
        [img_line setImage:[UIImage imageNamed:@"line-img@2x.png"]];
        [img_cellBackGnd addSubview:img_line];
        
        
        UIButton *icon_take = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_take.frame = CGRectMake(10, CGRectGetMaxY(img_line.frame)+5, 30, 30);
        //icon_take .backgroundColor = [UIColor clearColor];
        [icon_take addTarget:self action:@selector(clik_on_take_out:) forControlEvents:UIControlEventTouchUpInside];
        [icon_take setImage:[UIImage imageNamed:@"take-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_take];
        
        UIButton *icon_deliver = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //icon_deliver .backgroundColor = [UIColor clearColor];
        [icon_deliver addTarget:self action:@selector(click_on_delivery:) forControlEvents:UIControlEventTouchUpInside];
        [icon_deliver setImage:[UIImage imageNamed:@"deliver-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_deliver];
        
        
        UIButton *img_btn_seving_Now = [UIButton buttonWithType:UIButtonTypeCustom];
        //   img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
        //img_btn_deliver_Now .backgroundColor = [UIColor clearColor];
        [img_btn_seving_Now addTarget:self action:@selector(click_on_serving_now_icon:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_seving_Now setImage:[UIImage imageNamed:@"icon-serving-time@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_seving_Now];
        
        
        UIButton *img_btn_chef_menu = [UIButton buttonWithType:UIButtonTypeCustom];
        //    img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
        //img_btn_chef_menu .backgroundColor = [UIColor clearColor];
        [img_btn_chef_menu addTarget:self action:@selector(btn_chef_menu_click:) forControlEvents:UIControlEventTouchUpInside];
        [img_btn_chef_menu setImage:[UIImage imageNamed:@"chef-menu-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:img_btn_chef_menu];
        
        UILabel *lbl_total_servings = [[UILabel alloc]init];
        lbl_total_servings.frame = CGRectMake(WIDTH-70,85,200, 15);
        lbl_total_servings.text = @"Total Servings";
        lbl_total_servings.font = [UIFont fontWithName:kFontBold size:11];
        lbl_total_servings.textColor = [UIColor blackColor];
        lbl_total_servings.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_total_servings];
        
        UILabel *lbl_total_servings_val = [[UILabel alloc]init];
        lbl_total_servings_val.frame = CGRectMake(WIDTH-70,85,200, 15);
        lbl_total_servings_val.text = [NSString stringWithFormat:@"%@",[arraY_serving_val objectAtIndex:indexPath.row]];
        lbl_total_servings_val.font = [UIFont fontWithName:kFontBold size:11];
        lbl_total_servings_val.textColor = [UIColor blackColor];
        lbl_total_servings_val.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:lbl_total_servings_val];
        
        
        UIButton *icon_thumb = [UIButton buttonWithType:UIButtonTypeCustom];
        //   icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+5, 20, 20);
        //icon_thumb .backgroundColor = [UIColor clearColor];
        [icon_thumb addTarget:self action:@selector(click_on_thumb_icon:) forControlEvents:UIControlEventTouchUpInside];
        [icon_thumb setImage:[UIImage imageNamed:@"thumb-icon@2x.png"] forState:UIControlStateNormal];
        [img_cellBackGnd   addSubview:icon_thumb];
        
        UILabel *likes = [[UILabel alloc]init];
        //  likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
        likes.text = @"87.4%";
        likes.font = [UIFont fontWithName:kFont size:10];
        likes.textColor = [UIColor colorWithRed:152/255.0f green:0/255.0f blue:34/255.0f alpha:1];
        likes.backgroundColor = [UIColor clearColor];
        [img_cellBackGnd addSubview:likes];
        
        if (IS_IPHONE_6Plus)
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(WIDTH-80,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            lbl_total_servings.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)-5,CGRectGetMaxY(img_line.frame)+9,200, 15);
            lbl_total_servings_val.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)-5,CGRectGetMaxY(img_line.frame)+23,200, 15);
            icon_thumb.frame = CGRectMake(330,CGRectGetMaxY(img_line.frame)+9, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame),133,100, 10);
            
            dish_name.font = [UIFont fontWithName:kFontBold size:18];
            
        }
        else if (IS_IPHONE_6)
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(WIDTH-90,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-60, 0.5 );
            icon_take.frame = CGRectMake(20, CGRectGetMaxY(img_line.frame)+5, 30, 30);
            icon_deliver.frame = CGRectMake(65,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_seving_Now.frame = CGRectMake(130,CGRectGetMaxY(img_line.frame)+5,  30, 30);
            img_btn_chef_menu.frame = CGRectMake(160,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            lbl_total_servings.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)-5,CGRectGetMaxY(img_line.frame)+9,200, 15);
            lbl_total_servings_val.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)-5,CGRectGetMaxY(img_line.frame)+23,200, 15);
            icon_thumb.frame = CGRectMake(290,CGRectGetMaxY(img_line.frame)+5, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,128,100, 10);
            
            
            dish_name.font = [UIFont fontWithName:kFontBold size:18];
            
        }
        else
        {
            img_dish.frame = CGRectMake(7,7, 90,  95 );
            
            
            dish_name.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+4,5,200, 15);
            icon_location.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+10,CGRectGetMaxY(dish_name.frame)+15,15, 25);
            meters.frame = CGRectMake(CGRectGetMaxX(icon_location.frame)+8,40,200, 15);
            icon_server.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+5, 80, 25, 25);
            icon_halal.frame = CGRectMake( CGRectGetMaxX(icon_server.frame)+5,80, 25, 25);
            img_non_veg.frame = CGRectMake(CGRectGetMaxX(img_dish.frame)+58,61, 40, 20 );
            icon_cow.frame = CGRectMake(CGRectGetMaxX(icon_halal.frame)+5,80, 25, 25);
            icon_fronce.frame = CGRectMake(CGRectGetMaxX(icon_cow.frame)+5,80, 25, 25);
            doller_rate.frame = CGRectMake(IS_IPHONE_5?WIDTH-80:WIDTH-80,85,200, 15);
            img_line.frame = CGRectMake(15,CGRectGetMaxX(img_dish.frame)+20, WIDTH-50, 0.5 );
            icon_take.frame = CGRectMake(15, CGRectGetMaxY(img_line.frame)+5, 25, 25);
            icon_deliver.frame = CGRectMake(50,CGRectGetMaxY(img_line.frame)+5,  25, 25);
            img_btn_seving_Now.frame = CGRectMake(90,CGRectGetMaxY(img_line.frame)+10,  20, 20);
            img_btn_chef_menu.frame = CGRectMake(100,CGRectGetMaxY(img_line.frame)-10,  60, 60);
            lbl_total_servings.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)-5,CGRectGetMaxY(img_line.frame)+9,200, 15);
            lbl_total_servings_val.frame = CGRectMake(CGRectGetMaxX(img_btn_chef_menu.frame)-5,CGRectGetMaxY(img_line.frame)+23,200, 15);
            icon_thumb.frame = CGRectMake(IS_IPHONE_5?235:230,CGRectGetMaxY(img_line.frame)+9, 20, 20);
            likes.frame = CGRectMake(CGRectGetMaxX(icon_thumb.frame)+5,132,100, 10);
            
            dish_name.font = [UIFont fontWithName:kFontBold size:IS_IPHONE_5?15:15];
            
        }
        
    }
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    lbl_food_now.text =[array_head_names objectAtIndex:indexPath.row];
    //    [table_on_drop_down setHidden:YES];
    if (tableView ==  table_for_schedule)
    {
        if (indexPath.row == 0)
        {
            view_for_schedule.hidden = YES;
            view_for_schedule_items.hidden = NO;
        }
        else if (indexPath.row == 1)
        {
            view_for_schedule.hidden = YES;
            view_for_schedule_items.hidden = NO;
            
        }
        else if (indexPath.row == 2)
        {
            
            view_for_schedule.hidden = YES;
            view_for_schedule_items.hidden = NO;
        }
        [self AFChefMyProfileSchedule];
    }
    //    else if (tableView == table_for_selected_schedule_deate)
    //    {
    //        if (indexPath.row == 0)
    //        {
    //            view_for_schedule.hidden = NO;
    //            view_for_schedule_items.hidden = YES;
    //
    //        }
    //    }
    
}



#pragma mark UiCollectionView Delegate Methods for dish

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(collectionView == collView_for_icons)
    {
        return [array_for_icons count];
    }
    else if (collectionView == collView_for_kitchen_imgs)
    {
        return [ary_KitchenImages count];
        //        return [array_kitchen_imgs count];
    }
    else if (collectionView == collView_for_menu_items)
    {
        return [ary_Menuimages count];
        
        
        //        return [array_menu_items count];
    }
    
    return 0;
    
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if(collectionView == collView_for_icons)
    {
        return 1;
    }
    else if (collectionView == collView_for_kitchen_imgs)
    {
        return 1;
    }
    else if (collectionView == collView_for_menu_items)
    {
        return 1;
    }
    
    return 1;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView1 dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    
    if (collectionView1 == collView_for_icons)
    {
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,-5,80,103);
        //[cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        UIImageView *chef_pro_icons= [[UIImageView alloc]init];
        
        if (indexPath.row ==7)
        {
            chef_pro_icons .frame = CGRectMake(20,27,25,35);
            
        }
        else{
            chef_pro_icons .frame = CGRectMake(20,30,35,30);
            
            
        }
        [chef_pro_icons setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[array_for_icons objectAtIndex:indexPath.row]]]];
        //[img_dish  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        chef_pro_icons .backgroundColor = [UIColor clearColor];
        [chef_pro_icons  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:chef_pro_icons];
        
        UILabel *icons_name = [[UILabel alloc]init];
        icons_name.frame = CGRectMake(10,CGRectGetMaxY(chef_pro_icons.frame),200, 15);
        icons_name.text = [NSString stringWithFormat:@"%@",[array_icons_name objectAtIndex:indexPath.row]];
        icons_name.font = [UIFont fontWithName:kFontBold size:10];
        icons_name.textColor = [UIColor whiteColor];
        icons_name.backgroundColor = [UIColor clearColor];
        [cell_for_collection_view addSubview:icons_name];
        
        UIButton *btn_on_icons = [UIButton buttonWithType:UIButtonTypeCustom];
        btn_on_icons .frame = CGRectMake(0,0,30,30);
        btn_on_icons .backgroundColor = [UIColor clearColor];
        [btn_on_icons  addTarget:self action:@selector(btn_on_icons_click:) forControlEvents:UIControlEventTouchUpInside];
        //[btn_on_icons  setImage:[UIImage imageNamed:@"icon-w-location@2x.png"] forState:UIControlStateNormal];
        [cell_for_collection_view  addSubview:btn_on_icons ];
    }
    else if (collectionView1 == collView_for_kitchen_imgs)
    {
        
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,-5,80,103);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        UIImageView *kitchen_images = [[UIImageView alloc]init];
        kitchen_images .frame = CGRectMake(20,30,70,70);
        NSString *ImagePath1 =[[NSString stringWithFormat:@"%@",[ary_KitchenImages objectAtIndex:indexPath.row]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [kitchen_images setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@""]];
        
//       NSString *ImagePath1 =[[NSString stringWithFormat:@"%@",[[ary_Kitchen_info objectAtIndex:indexPath.row]valueForKey:@"KitchenImages"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        [kitchen_images setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@""]];
                //[img_dish  setImage:[UIImage imageNamed:@"dish1-img@2x.png"]];
        kitchen_images .backgroundColor = [UIColor clearColor];
        [kitchen_images  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:kitchen_images];
    }
    else if (collectionView1 == collView_for_menu_items)
    {
        UIImageView *cell_for_collection_view = [[UIImageView alloc]init];
        cell_for_collection_view .frame = CGRectMake(0,-5,80,103);
        [cell_for_collection_view  setImage:[UIImage imageNamed:@"bg-img@2x.png"]];
        cell_for_collection_view .backgroundColor = [UIColor clearColor];
        [cell_for_collection_view  setUserInteractionEnabled:YES];
        [cell.contentView addSubview:cell_for_collection_view];
        
        UIImageView *kitchen_images = [[UIImageView alloc]init];
        kitchen_images .frame = CGRectMake(02,20,75,70);
        NSString *ImagePath1 =[[NSString stringWithFormat:@"%@",[[ary_Menuimages objectAtIndex:indexPath.row]valueForKey:@"DishImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [kitchen_images setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@""]];
        kitchen_images .backgroundColor = [UIColor clearColor];
        [kitchen_images  setUserInteractionEnabled:YES];
        [cell_for_collection_view addSubview:kitchen_images];
        
        if (indexPath.row==[ary_Menuimages count])
        {
                    UIButton *btn_ImageClick = [UIButton buttonWithType:UIButtonTypeCustom];
                    btn_ImageClick .frame = CGRectMake(0,20,75,70);
                    [btn_ImageClick addTarget:self action:@selector(btn_ImageClick:) forControlEvents:UIControlEventTouchUpInside];
                    btn_ImageClick.tag=indexPath.row;
                    [btn_ImageClick setBackgroundImage:[UIImage imageNamed:@"img_multiplebookplus@2x.png"] forState:UIControlStateNormal];
                    btn_ImageClick.backgroundColor = [UIColor redColor];
                    [kitchen_images addSubview:btn_ImageClick];
        }
        else{
            
        }
        
//        return [ary_Menuimages count]+1;

    }
    
    
    
    
    
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((WIDTH/4), 90);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row ==0)
    {
        img_strip.hidden = NO;
        img_strip1.hidden =YES;
        img_strip2.hidden = YES;
        img_strip3.hidden = YES;
        view_personal.hidden = NO;
        view_for_kitchen.hidden = YES;
        view_for_dine_in.hidden = YES;
        view_for_menu.hidden = YES;
        view_for_schedule.hidden = YES;
        view_for_schedule_items.hidden = YES;
        view_for_reviews.hidden = YES;
        view_for_delivery.hidden = YES;
        view_for_note.hidden = YES;
        
    }
    else if (indexPath.row == 1)
    {
        img_strip.hidden = YES;
        img_strip1.hidden =NO;
        img_strip2.hidden = YES;
        img_strip3.hidden = YES;
        view_personal.hidden = YES;
        view_for_kitchen.hidden = NO;
        view_for_dine_in.hidden = YES;
        view_for_menu .hidden = YES;
        view_for_schedule.hidden = YES;
        view_for_schedule_items.hidden = YES;
        view_for_reviews.hidden = YES;
        view_for_delivery.hidden = YES;
        view_for_note.hidden = YES;
        
    }
    else if (indexPath.row == 2)
    {
        img_strip.hidden = YES;
        img_strip1.hidden =YES;
        img_strip2.hidden = NO;
        img_strip3.hidden = YES;
        view_personal.hidden = YES;
        view_for_kitchen.hidden = YES;
        view_for_dine_in.hidden = NO;
        view_for_menu.hidden = YES;
        view_for_schedule.hidden = YES;
        view_for_schedule_items.hidden = YES;
        view_for_reviews.hidden = YES;
        view_for_delivery.hidden = YES;
        view_for_note.hidden = YES;
        
    }
    else if (indexPath.row == 3)
    {
        
        img_strip.hidden = YES;
        img_strip1.hidden =YES;
        img_strip2.hidden = YES;
        img_strip3.hidden = NO;
        view_personal.hidden = YES;
        view_for_kitchen.hidden = YES;
        view_for_dine_in.hidden = YES;
        view_for_menu.hidden = NO;
        view_for_schedule.hidden = YES;
        view_for_schedule_items.hidden = YES;
        view_for_reviews.hidden = YES;
        view_for_delivery.hidden = YES;
        view_for_note.hidden = YES;
        [self AFChefMyProfileDishimages];
        
    }
    else if (indexPath.row == 4)
    {
        img_strip.hidden = YES;
        img_strip1.hidden =YES;
        img_strip2.hidden = YES;
        img_strip3.hidden = NO;
        view_personal.hidden = YES;
        view_for_kitchen.hidden = YES;
        view_for_dine_in.hidden = YES;
        view_for_menu.hidden = YES;
        view_for_schedule.hidden = NO;
        //   view_for_schedule_items.hidden = YES;
        view_for_reviews.hidden = YES;
        view_for_delivery.hidden = YES;
        view_for_note.hidden = YES;
        
    }
    else if (indexPath.row == 5)
    {
        img_strip.hidden = YES;
        img_strip1.hidden =YES;
        img_strip2.hidden = YES;
        img_strip3.hidden = NO;
        view_personal.hidden = YES;
        view_for_kitchen.hidden = YES;
        view_for_dine_in.hidden = YES;
        view_for_menu.hidden = YES;
        view_for_schedule.hidden = YES;
        view_for_schedule_items.hidden = YES;
        view_for_reviews.hidden = NO;
        view_for_delivery.hidden = YES;
        view_for_note.hidden = YES;
        [self AFChefMyProfileReviews];
        
    }
    else if (indexPath.row == 6)
    {
        img_strip.hidden = YES;
        img_strip1.hidden =YES;
        img_strip2.hidden = YES;
        img_strip3.hidden = NO;
        
        view_personal.hidden = YES;
        view_for_kitchen.hidden = YES;
        view_for_dine_in.hidden = YES;
        view_for_menu.hidden = YES;
        view_for_schedule.hidden = YES;
        view_for_schedule_items.hidden = YES;
        view_for_reviews.hidden = YES;
        view_for_delivery.hidden = NO;
        view_for_note.hidden = YES;
        
        
    }
    else if (indexPath.row == 7)
    {
        img_strip.hidden = YES;
        img_strip1.hidden =YES;
        img_strip2.hidden = YES;
        img_strip3.hidden = NO;
        
        view_personal.hidden = YES;
        view_for_kitchen.hidden = YES;
        view_for_dine_in.hidden = YES;
        view_for_menu.hidden = YES;
        view_for_schedule.hidden = YES;
        view_for_schedule_items.hidden = YES;
        view_for_reviews.hidden = YES;
        view_for_delivery.hidden = YES;
        view_for_note.hidden = NO;
        
        
    }
    
}


#pragma uibutton click events

-(void)btn_ImageClick:(UIButton *)sender
{
    [self AFChefMyProfileDishimages];
}



-(void)btn_Videoplay_click:(UIButton *)sender
{

        [self.moviePlayer play];
    
}
- (void) moviePlayBackDidFinish:(NSNotification *)notification
    {
      [self.moviePlayer stop];
   }


-(void)btn_menu_click:(UIButton *)sender
{
    NSLog(@"btn_menu_click:");
}
-(void)btn_img_user_click:(UIButton *)sender
{
    NSLog(@"btn_img_user_click:");
}
-(void)btn_img_pencil_click:(UIButton *)sender
{
    NSLog(@"btn_img_pencil_click:");
}
-(void)btn_on_icons_click:(UIButton *)sender
{
    NSLog(@"btn_on_icons_click:");
}
//-(void)icon_right_arrow_BtnClick:(UIButton * )sender
//{
//    NSLog(@"icon_right_arrow_BtnClick:");
//    view_for_schedule.hidden = YES;
//    view_for_schedule_items.hidden = NO;
//}
-(void)click_on_add_schedule_btn:(UIButton *)sender
{
    NSLog(@"click_on_add_schedule_btn:");
}
-(void)click_on_left_arrow:(UIButton *)sender
{
    NSLog(@"click_on_left_arrow:");
    view_for_schedule.hidden = NO;
    view_for_schedule_items.hidden = YES;
}
//click actions for dish items
-(void)click_on_seve_icon:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
}
-(void)click_on_halal:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
}
-(void)click_on_cow_icon:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
}
-(void)click_on_icon_fronce:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
}
-(void)clik_on_take_out:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
}
-(void)click_on_delivery:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
}
-(void)click_on_serving_now_icon:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
}
-(void)btn_chef_menu_click:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
}
-(void)click_on_thumb_icon:(UIButton *)sender
{
    NSLog(@"btn_favorite_click:");
}
-(void)click_on_edite_btn:(UIButton *)sender
{
    NSLog(@"click_on_edite_btn");
}
-(void)click_on_delete_btn:(UIButton *)sender
{
    NSLog(@"click_on_delete_btn");
}
-(void)click_on_right_arrow:(UIButton *)sender
{
    NSLog(@"click_on_right_arrow");
}
-(void)click_on_write_note_btn:(UIButton *)sender
{
    NSLog(@"click_on_write_note_btn");
}

#pragma Chef-profile-functionality

-(void)ChefProfileInfo
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            
                            @"uid"                               :  @"563",
                            @"role_type"                         :  @"chef_profile",
                            @"device_udid"                       :  UniqueAppID,
                            @"device_token"                      :  @"Dev",
                            @"device_type"                       :  @"1"
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:@"webservices/view-profile.json"  parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefProfileInfo:JSON];
    }
     
     
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error){
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self ChefProfileInfo];
                                         }
                                     }];
    [operation start];
    
}
-(void)ResponseChefProfileInfo:(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        [ary_Chefprofile_info addObject:[TheDict valueForKey:@"profile_info"]];
        
        
        [ary_Personal_Information addObject:[[TheDict valueForKey:@"profile_info"] valueForKey:@"Personal_Information"]];
        [ary_Serving_Type addObject:[[TheDict valueForKey:@"profile_info"] valueForKey:@"Serving_Type"]];
        [ary_Kitchen_info addObject:[[TheDict valueForKey:@"profile_info"] valueForKey:@"Kitchen_info"]];
        
        NSURL *movieURL1 = [NSURL URLWithString:[[ary_Kitchen_info objectAtIndex:0]valueForKey:@"KitchenVideo"]];
        [self.moviePlayer setContentURL:movieURL1];

        
        for (int i=0; i<[[[[TheDict valueForKey:@"profile_info"] valueForKey:@"Kitchen_info"]valueForKey:@"KitchenImages" ] count]; i++)
        {
            [ary_KitchenImages addObject:[[[[TheDict valueForKey:@"profile_info"] valueForKey:@"Kitchen_info"]valueForKey:@"KitchenImages" ] objectAtIndex:i]];
            
        }
        
        [ary_Dine_In addObject:[[TheDict valueForKey:@"profile_info"] valueForKey:@"Dine-In"]];
        [ary_Menu addObject:[[TheDict valueForKey:@"profile_info"] valueForKey:@"Menu"]];
        [ary_notes addObject:[[TheDict valueForKey:@"profile_info"] valueForKey:@"notes"]];
        //      txtview_adddescription.text =[[[ary_Personal_Information valueForKey:@"profile_info"] valueForKey:@"Personal_Information"] valueForKey:@"About_Us"];
        txtview_adddescription.text =[[[TheDict valueForKey:@"profile_info"] valueForKey:@"Personal_Information"] valueForKey:@"About_Us"];
        
        NSString *ImagePath1 =[[NSString stringWithFormat:@"%@",[[[TheDict valueForKey:@"profile_info"] valueForKey:@"Dine-In"] valueForKey:@"Dine_In_Pictures"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [img_din_in_area setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:[UIImage imageNamed:@""]];
//        img_din_in_area.text =[[[TheDict valueForKey:@"profile_info"] valueForKey:@"Dine-In"] valueForKey:@"Dine_In_Pictures"];
        lbl_no_of_seats_val.text =[[[TheDict valueForKey:@"profile_info"] valueForKey:@"Dine-In"] valueForKey:@"Number_Of_Seats"];
        lbl_parking_val.text =[[[TheDict valueForKey:@"profile_info"] valueForKey:@"Dine-In"] valueForKey:@"Parking_Available"];

        
        
        
        //        txtview_note.text =[[TheDict valueForKey:@"profile_info"] valueForKey:@"notes"];
        for (int j=0; j<[[[TheDict valueForKey:@"profile_info"] valueForKey:@"notes"] count]; j++)
        {
            [ary_notes addObject:[[[TheDict valueForKey:@"profile_info"] valueForKey:@"notes"] objectAtIndex:j]];
            
        }
        
        txtview_note .text = [NSString stringWithFormat:@"%@",[ary_notes objectAtIndex:0]];
        
        [table_for_personal_info reloadData];
        [table_for_kitchen_info reloadData];
        [table_for_food_storage reloadData];
        [collView_for_kitchen_imgs reloadData];
        [table_for_menu reloadData];
        
        
        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
}





# pragma mark ChefProfileReviews method

-(void)AFChefMyProfileReviews
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            
                            @"uid"                               :   @"563",
                            @"chef_id"                           :   @"563",
                            @"lat"                               :   @"",
                            @"long"                              :   @"",
                            @"role_type"                         :  @"chef_profile",
                            @"device_udid"                       :  UniqueAppID,
                            @"device_token"                      :  @"Dev",
                            @"device_type"                       :  @"1"
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kChefProfileReviews
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefProfileReviews:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFChefMyProfileReviews];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefProfileReviews :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        [ary_ReviewsList addObject:[TheDict valueForKey:@"ChefDetails"]];
        //        label_favorite_value.text =[[TheDict valueForKey:@"ChefDetails"] valueForKey:@"favorite_by_user"];
        
        
        //        label_favorite_value.text = [NSString stringWithFormat:@"%@",[[[ary_ReviewsList objectAtIndex:0] objectAtIndex:indexPath.row] valueForKey:@"favorite_by_user"]];
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    label_favorite_value.text = [NSString stringWithFormat:@"%@",[[ary_ReviewsList objectAtIndex:0] valueForKey:@"favorite_by_user"]];
    label_total_reviews_value.text = [NSString stringWithFormat:@"%@",[[ary_ReviewsList objectAtIndex:0] valueForKey:@"total_revies"]];
    label_overall_rating_value.text = [NSString stringWithFormat:@"%@",[[ary_ReviewsList objectAtIndex:0] valueForKey:@"overall_rating"]];
    taste_value.text = [NSString stringWithFormat:@"%@",[[ary_ReviewsList objectAtIndex:0] valueForKey:@"taste_rating"]];
    appeal_value.text = [NSString stringWithFormat:@"%@",[[ary_ReviewsList objectAtIndex:0] valueForKey:@"total_revies"]];
    value_value.text = [NSString stringWithFormat:@"%@",[[ary_ReviewsList objectAtIndex:0] valueForKey:@"value_rating"]];
    
}


# pragma mark ChefProfileSchedule method

-(void)AFChefMyProfileSchedule
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            
                            @"uid"                               :   @"563",
                            @"serve_date"                        :   @"2015-09-04",
                            @"serve_id"                          :   @"60",
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kChefProfileMyScheduleDetailList
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefProfileSchedule:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFChefMyProfileSchedule];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefProfileSchedule :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
}




# pragma mark AFChefMyProfileDishimages method

-(void)AFChefMyProfileDishimages
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
        str_device_token = @"dev-signup";
        
    }
    
    NSDictionary *params =@{
                            
                            @"uid"                               :   @"563",
                            
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kChefProfileDishImages
                                                      parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseChefProfileDishimages:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFChefMyProfileDishimages];
                                         }
                                     }];
    [operation start];
    
}
-(void) ResponseChefProfileDishimages :(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        for (int i=0; i<[[[TheDict valueForKey:@"DishsImagesList"] objectAtIndex:0] count]; i++)
        {
            [ary_Menuimages addObject:[[TheDict valueForKey:@"DishsImagesList"] objectAtIndex:0]];
            
        }
        NSMutableDictionary *dict_exp=[[NSMutableDictionary alloc] init];
        [dict_exp setValue:@"" forKey:@"DishUID"];
        [dict_exp setValue:@"" forKey:@"DishID"];
        [dict_exp setValue:@"" forKey:@"DishName"];
        [dict_exp setValue:@"" forKey:@"DishImage"];
        [ary_Menuimages addObject:dict_exp];

        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //        [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        
    }
    
}







- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
