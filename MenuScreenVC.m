//
//  MenuScreenVC.m
//  Not86
//
//  Created by Interwld on 8/18/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "MenuScreenVC.h"
#import "MenuScreenEditMealVC.h"
#import "AppDelegate.h"
#import "UIImageView+AFNetworking.h"
#import "AFHTTPClient.h"
#import "JWSlideMenuViewController.h"
#import "JWSlideMenuController.h"
#import "JWNavigationController.h"
#import "ViewMenuVC.h"

@interface MenuScreenVC ()<UIScrollViewDelegate,UITextFieldDelegate,UITextViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UIImagePickerControllerDelegate ,UIGestureRecognizerDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>
{
    UIImageView *img_topbar;
    UIImageView *img_BackgroundImg;
    UIScrollView *scrollviewMenu;
    UITextField *txt_cheftype;
    UITextField *txt_others;
    NSMutableArray *collectionarrImages;
    
    UICollectionViewFlowLayout *layout;
    UICollectionView *collView_serviceDirectory;
    
    UICollectionViewFlowLayout * layout2;
    UICollectionView * collView_serviceDirectory_for_meal;
    
    UIButton *btn_on_singledish;
    UIButton *btn_on_Meal;
    
    
    UIImageView * black_stripimg;
    UILabel * lbl_single_dish;
    UIImageView * black_stripimg2;
    UILabel * lbl_meal;
    
    UIView * view_for_dish;
    UIView * view_for_meal;
    UIImageView *img_backround;
    NSData * imgData;
    NSData * imgData_in_meal;
    
    
    UITableView * table_for_category;
    UITableView * table_for_course;
    UITableView * table_for_cuisine;
    
    NSMutableArray * array_course;
    
    UITextField *txt_Category;
    UITextField *txt_Cuisine;
    UITextField *txt_Course;
    
    UITableView * table_for_category_in_meal;
    UITableView * table_for_course_in_meal;
    UITableView * table_for_cuisine_in_meal;
    
    UITextField *txt_Category_in_meal;
    UITextField *txt_Cuisine_in_meal;
    UITextField *txt_Course_in_meal;
    
    NSString    * selected_img;
    UIImageView * img_backround_in_meal;
    
    CGFloat	animatedDistance;
    AppDelegate * delegate;
    NSData      * imageData;
    
    UITextField * txt_dishTitle;
    UITextField * txt_keyword;
    UITextField * txt_Price;
    UITextView * txt_view;
    
    NSString * str_selecte_dish_r_meal;
    
    
    UITextField *txt_MealTitle_in_meal;
    UITextField *txt_keyword_in_meal;
    UITextField *txt_Price_in_meal;
    UITextView * txt_view_in_meal;
    
    NSMutableArray * Arr_temp;
    NSMutableArray * arr_category;
    
    NSMutableArray * collectionarrImages_in_meal;
    NSMutableArray * Arr_temp_in_meal;
    
    
    NSString * str_cuisenCatId;
    NSString * str_cuisenCatId_in_meal;
    
    NSMutableArray * arrCuisines;
    NSMutableArray * aryPopularlist;
    
    NSMutableArray * array_course_list;
    NSString * str_cataogyid;
    NSMutableArray *array_selectedcategoryid;
    
    NSString * str_cuisineid;
    NSMutableArray *array_selectedcuisines;
    NSString * str_courseid;
    
    NSMutableArray * array_categorylist;
    NSMutableArray * array_selectedcategorylist;
    
    NSString * str_categoryid_in_meal;
    
    NSMutableArray * array_dietary_restryctionlist;
    
    NSMutableArray * arrCuisineslist_in_meal;
    NSMutableArray *array_selectedcuisineslist_in_meal;
    NSString * str_cuisinId_in_meal;
    
    NSMutableArray * array_course_list_in_meal;
    NSString * str_courseid_in_meal;
    
    NSMutableArray * array_dietary_restryctionlist_in_meal;
    
    UIView*alertviewBg;
    
    
    int selectedindex;
    NSIndexPath *indexSelected;
    
    UICollectionView*collectionView_Keywords;
    
    NSMutableArray*ary_Keywords;
    int int_SelectedKeyword;
    
    UIView*deletePopUpBg;
    UICollectionView*collectionView_Keywords_meal;
    NSMutableArray*Ary_keywordmeal;
    
    
}


@end

@implementation MenuScreenVC
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 180;//216
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1];
    
    str_cataogyid = [NSString new];
    str_cuisineid = [NSString new];
    str_courseid  = [NSString new];
    
    str_cuisenCatId = [NSString new];
    str_cuisenCatId_in_meal = [NSString new];
    
    array_dietary_restryctionlist = [[NSMutableArray alloc]init];
    array_selectedcategoryid = [[NSMutableArray alloc]init];
    array_selectedcuisines = [[NSMutableArray alloc]init];
    aryPopularlist= [[NSMutableArray alloc]init];
    arrCuisines=[[NSMutableArray alloc]init];
    array_course_list = [[NSMutableArray alloc]init];
    ary_Keywords = [[NSMutableArray alloc]init];
    Ary_keywordmeal= [[NSMutableArray alloc]init];
    
    array_dietary_restryctionlist_in_meal = [[NSMutableArray alloc]init];
    array_categorylist = [[NSMutableArray alloc]init];
    array_selectedcategorylist = [[NSMutableArray alloc]init];
    arrCuisineslist_in_meal = [[NSMutableArray alloc]init];
    array_selectedcuisineslist_in_meal = [[NSMutableArray alloc]init];
    array_course_list_in_meal = [[NSMutableArray alloc]init];
    
    str_categoryid_in_meal = [NSString new];
    str_cuisinId_in_meal = [NSString new];
    str_courseid_in_meal = [NSString new];
    
    
    
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    selected_img = [NSString new];
    str_selecte_dish_r_meal = [NSString new];
    str_selecte_dish_r_meal = @"Dish";
    Arr_temp = [[NSMutableArray alloc]init];
    Arr_temp_in_meal = [[NSMutableArray alloc]init];
    
    selectedindex=-1;
    indexSelected = nil;
    
    collectionarrImages=[NSMutableArray arrayWithObjects:@"img_halal@2x.png",@"img_Univer@2x.png",@"img_Organic@2x.png", nil];
    collectionarrImages_in_meal = [[NSMutableArray alloc]initWithObjects:@"img_halal@2x.png",@"img_Univer@2x.png",@"img_Organic@2x.png", nil];
    [self IntegrateHeaderDesign];
    [self IntegrateBodyDesign];
    
    array_course = [[NSMutableArray alloc]initWithObjects:@"Breakfast",@"Appetizer",@"Soup",@"Salad",@"Main Course",@"Side Dish",@"Sessert",@"Drinks", nil];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    
    
    [self AFDietary_restrictions];
    [self popularList];
    [self cuisineList];
    [self AFcourse];
    
    
    [self AFDietary_restrictions_in_meal];
    [self AFcategory_in_meal];
    [self cuisineList_in_meal];
    [self AFcourse_in_meal];
    
    
    
    
}
-(void)IntegrateHeaderDesign
{
    
    
    img_topbar=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,50)];
    [img_topbar setUserInteractionEnabled:YES];
    img_topbar.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:img_topbar];
    
    
    //    UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(10, 17, 15, 15)];
    //    [img_back setUserInteractionEnabled:YES];
    //    img_back.backgroundColor=[UIColor clearColor];
    //    img_back.image=[UIImage imageNamed:@"arrow@2x.png"];
    //    // [img_topbar addSubview:img_back];
    
    UIButton *icon_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_menu .frame = CGRectMake(10, 13,25,20);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_menu  addTarget:self action:@selector(click_on_Back_Btn:)forControlEvents:UIControlEventTouchUpInside];
    [icon_menu setImage:[UIImage imageNamed:@"img_arrow@2x"] forState:UIControlStateNormal];
    //    icon_menu.layer.borderWidth = 1.0;
    [icon_menu setBackgroundColor:[UIColor clearColor]];
    [img_topbar   addSubview:icon_menu ];
    
    
    UILabel  * lbl_heading = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(icon_menu.frame)+20,4, 220,40)];
    lbl_heading.text = @"Add Dish/Meal ";
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:kFont size:16];
    [img_topbar addSubview:lbl_heading];
    
    UIButton *btn_back = [[UIButton alloc] init];
    btn_back.frame = CGRectMake(10, 10, 20,20);
    btn_back.backgroundColor = [UIColor clearColor];
    [btn_back addTarget:self action:@selector(Back_btnClick:) forControlEvents:UIControlEventTouchUpInside ];
    // [img_topbar addSubview:btn_back];
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 10, 30, 30)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"img-logo.png"];
    [img_topbar addSubview:img_logo];
    
    scrollviewMenu=[[UIScrollView alloc]init];
    scrollviewMenu.frame = CGRectMake(0, 50, WIDTH, HEIGHT-100);
    [scrollviewMenu setShowsVerticalScrollIndicator:NO];
    scrollviewMenu.delegate = self;
    scrollviewMenu.scrollEnabled = YES;
    scrollviewMenu.showsVerticalScrollIndicator = NO;
    [scrollviewMenu setUserInteractionEnabled:YES];
    scrollviewMenu.backgroundColor = [UIColor clearColor];
    if (IS_IPHONE_6Plus)
    {
        scrollviewMenu.frame = CGRectMake(0, 50, WIDTH, HEIGHT-100);
        [scrollviewMenu setContentSize:CGSizeMake(0,HEIGHT+50)];
    }
    else if (IS_IPHONE_6)
    {
        scrollviewMenu.frame = CGRectMake(0, 50, WIDTH, HEIGHT-100);
        [scrollviewMenu setContentSize:CGSizeMake(0,HEIGHT+100)];
    }
    else
    {
        scrollviewMenu.frame = CGRectMake(0, 50, WIDTH, HEIGHT-100);
        [scrollviewMenu setContentSize:CGSizeMake(0,HEIGHT+200)];
    }
    
    [self.view addSubview:scrollviewMenu];
    
    
}
-(void)click_on_Back_Btn: (UIButton *)sender

{
    NSLog(@"click_on_Back_Btn Click");
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)IntegrateBodyDesign
{
    
    black_stripimg = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        black_stripimg.frame = CGRectMake(75, 15,120, 30);
        
    }
    else if (IS_IPHONE_6)
    {
        black_stripimg.frame = CGRectMake(70, 15,120, 30);
        
    }
    else
    {
        black_stripimg.frame = CGRectMake(70, 15,90, 30);
        
    }
    [black_stripimg setUserInteractionEnabled:YES];
    black_stripimg.backgroundColor = [UIColor blackColor];
    black_stripimg.image = [UIImage imageNamed:@""];
    [scrollviewMenu addSubview:black_stripimg];
    
    
    lbl_single_dish = [[UILabel alloc]init];
    lbl_single_dish.frame = CGRectMake(20, 0, 120, 30);
    lbl_single_dish.font = [UIFont fontWithName:kFont size:15];
    if (IS_IPHONE_6Plus)
    {
        lbl_single_dish.frame = CGRectMake(20, 0, 120, 30);
        
    }
    else if (IS_IPHONE_6)
    {
        lbl_single_dish.frame = CGRectMake(20, 0, 120, 30);
        
    }
    else
    {
        lbl_single_dish.frame = CGRectMake(15, 0, 120, 30);
        lbl_single_dish.font = [UIFont fontWithName:kFont size:12];
        
    }
    
    lbl_single_dish.text = @"Single Dish";
    lbl_single_dish.backgroundColor = [UIColor clearColor];
    lbl_single_dish.textColor = [UIColor whiteColor];
    [black_stripimg addSubview:lbl_single_dish];
    
    
    btn_on_singledish = [[UIButton alloc] init];
    btn_on_singledish.titleLabel.font = [UIFont fontWithName:kFont size:15];
    if (IS_IPHONE_6Plus)
    {
        btn_on_singledish.frame = CGRectMake(75, 15,120,30);
    }
    else if (IS_IPHONE_6)
    {
        btn_on_singledish.frame = CGRectMake(70, 15,120,30);
    }
    else
    {
        btn_on_singledish.frame = CGRectMake(70, 15,90,30);
        btn_on_singledish.titleLabel.font = [UIFont fontWithName:kFont size:12];
    }
    btn_on_singledish.backgroundColor = [UIColor lightGrayColor];
    [btn_on_singledish setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_on_singledish setTitle:@"Single Dish" forState:UIControlStateNormal];
    
    [btn_on_singledish addTarget:self action:@selector(click_on_singledish:) forControlEvents:UIControlEventTouchUpInside ];
    [scrollviewMenu addSubview:btn_on_singledish];
    btn_on_singledish.hidden = YES;
    
    
    btn_on_Meal = [[UIButton alloc] init];
    btn_on_Meal.titleLabel.font = [UIFont fontWithName:kFont size:15];
    if (IS_IPHONE_6Plus)
    {
        btn_on_Meal.frame = CGRectMake(CGRectGetMaxX(black_stripimg.frame),15, 120,30);
        
    }
    else if (IS_IPHONE_6)
    {
        btn_on_Meal.frame = CGRectMake(CGRectGetMaxX(black_stripimg.frame),15, 120,30);
    }
    else
    {
        btn_on_Meal.frame = CGRectMake(CGRectGetMaxX(black_stripimg.frame),15,90,30);
        btn_on_Meal.titleLabel.font = [UIFont fontWithName:kFont size:12];
    }
    btn_on_Meal.backgroundColor = [UIColor lightGrayColor];
    [btn_on_Meal setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_on_Meal setTitle:@"Meal" forState:UIControlStateNormal];
    
    [btn_on_Meal addTarget:self action:@selector(click_on_meal_btn:) forControlEvents:UIControlEventTouchUpInside ];
    [scrollviewMenu  addSubview:btn_on_Meal];
    
    
    
    black_stripimg2 = [[UIImageView alloc]init];
    black_stripimg2.frame = CGRectMake(CGRectGetMaxX(black_stripimg.frame), 15, 120, 30);
    if (IS_IPHONE_6Plus)
    {
        black_stripimg2.frame = CGRectMake(CGRectGetMaxX(black_stripimg.frame), 15, 120, 30);
        
    }
    else if (IS_IPHONE_6)
    {
        black_stripimg2.frame = CGRectMake(CGRectGetMaxX(black_stripimg.frame), 15, 120, 30);
    }
    else
    {
        black_stripimg2.frame = CGRectMake(CGRectGetMaxX(black_stripimg.frame), 15, 90, 30);
    }
    
    [black_stripimg2 setUserInteractionEnabled:YES];
    black_stripimg2.backgroundColor = [UIColor blackColor];
    //brown_stripimg.image = [UIImage imageNamed:@""];
    [scrollviewMenu addSubview:black_stripimg2];
    black_stripimg2.hidden = YES;
    
    
    lbl_meal = [[UILabel alloc]init];
    lbl_meal.frame = CGRectMake(40, 0, 260, 30);
    lbl_meal.font = [UIFont fontWithName:kFont size:15];
    if (IS_IPHONE_6Plus)
    {
        lbl_meal.frame = CGRectMake(40, 0, 260, 30);
        
    }
    else if (IS_IPHONE_6)
    {
        lbl_meal.frame = CGRectMake(40, 0, 260, 30);
    }
    else
    {
        lbl_meal.frame = CGRectMake(30, 0, 260, 30);
        lbl_meal.font = [UIFont fontWithName:kFont size:12];
    }
    
    lbl_meal.text = @"Meal";
    lbl_meal.backgroundColor = [UIColor clearColor];
    lbl_meal.textColor = [UIColor whiteColor];
    
    [black_stripimg2 addSubview:lbl_meal];
    lbl_meal.hidden = YES;
    
    
    
    view_for_dish = [[UIView alloc]init];
    view_for_dish.frame=CGRectMake(0,CGRectGetMaxY(black_stripimg.frame)+5,WIDTH,750);
    if (IS_IPHONE_6Plus)
    {
        view_for_dish.frame=CGRectMake(0,CGRectGetMaxY(black_stripimg.frame)+5,WIDTH,750);
        
    }
    else if (IS_IPHONE_6)
    {
        view_for_dish.frame=CGRectMake(0,CGRectGetMaxY(black_stripimg.frame)+5,WIDTH,750);
    }
    else
    {
        view_for_dish.frame=CGRectMake(0,CGRectGetMaxY(black_stripimg.frame)+5,WIDTH,750);
    }
    
    [view_for_dish setUserInteractionEnabled:YES];
    view_for_dish.backgroundColor=[UIColor clearColor];
    [scrollviewMenu  addSubview:  view_for_dish];
    
    
    
    
    UIImageView *img_Backgroundimage=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        img_Backgroundimage.frame = CGRectMake(02, 10, WIDTH, 80);
    }
    else if (IS_IPHONE_6)
    {
        img_Backgroundimage.frame = CGRectMake(02, 10, WIDTH, 80);
    }
    else
    {
        img_Backgroundimage.frame = CGRectMake(0, 10, WIDTH, 80);
    }
    
    [img_Backgroundimage setUserInteractionEnabled:YES];
    img_Backgroundimage.backgroundColor=[UIColor clearColor];
    img_Backgroundimage.image=[UIImage imageNamed:@"bg1.png"];
    [view_for_dish addSubview:img_Backgroundimage];
    
    
    UIImageView *Arrow_left=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        Arrow_left.frame=CGRectMake(5, 30, 10, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        Arrow_left.frame=CGRectMake(5, 25, 10, 15);
    }
    else
    {
        Arrow_left.frame=CGRectMake(5, 30, 10, 15);
        
    }
    [Arrow_left setUserInteractionEnabled:YES];
    Arrow_left.backgroundColor=[UIColor clearColor];
    Arrow_left.image=[UIImage imageNamed:@"arrow_left.png"];
    [img_Backgroundimage addSubview:Arrow_left];
    
    UIButton *btn_Arrowleft = [[UIButton alloc] init];
    if (IS_IPHONE_6Plus)
    {
        btn_Arrowleft.frame = CGRectMake(0, 25, 10, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        btn_Arrowleft.frame = CGRectMake(5, 25, 10, 15);
        
    }
    else
    {
        btn_Arrowleft.frame = CGRectMake(5, 25, 10, 15);
        
    }
    
    btn_Arrowleft.backgroundColor = [UIColor clearColor];
    [btn_Arrowleft setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    [btn_Arrowleft addTarget:self action:@selector(click_on_left_arrow:) forControlEvents:UIControlEventTouchUpInside ];
    [img_Backgroundimage addSubview:btn_Arrowleft];
    
    
    layout=[[UICollectionViewFlowLayout alloc] init];
    collView_serviceDirectory = [[UICollectionView alloc] initWithFrame:CGRectMake(30,3,WIDTH-65,70)
                                                   collectionViewLayout:layout];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_serviceDirectory setDataSource:self];
    [collView_serviceDirectory setDelegate:self];
    collView_serviceDirectory.scrollEnabled = YES;
    collView_serviceDirectory.showsVerticalScrollIndicator = NO;
    collView_serviceDirectory.showsHorizontalScrollIndicator = NO;
    collView_serviceDirectory.pagingEnabled = NO;
    [collView_serviceDirectory registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_serviceDirectory setBackgroundColor:[UIColor clearColor]];
    layout.minimumInteritemSpacing = 2;
    layout.minimumLineSpacing = 0;
    collView_serviceDirectory.userInteractionEnabled = YES;
    [img_Backgroundimage addSubview:collView_serviceDirectory];
    
    
    
    
    UIImageView *Arrow_right=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        Arrow_right.frame=CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+10,30, 10, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        Arrow_right.frame=CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
        
    }
    else
    {
        Arrow_right.frame=CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 30, 10, 15);
        
    }
    [Arrow_right setUserInteractionEnabled:YES];
    Arrow_right.backgroundColor=[UIColor clearColor];
    Arrow_right.image=[UIImage imageNamed:@"arrow_right.png"];
    [img_Backgroundimage addSubview:Arrow_right];
    
    
    UIButton *btn_Arrowright = [[UIButton alloc] init];
    if (IS_IPHONE_6Plus)
    {
        btn_Arrowright.frame = CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+1, 25, 10, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        btn_Arrowright.frame = CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
        
    }
    else
    {
        btn_Arrowright.frame = CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
        
    }
    btn_Arrowright.backgroundColor = [UIColor clearColor];
    //    [btn_Arrowleft setImage:[UIImage imageNamed:@"arrow_left.png"] forState:UIControlStateNormal];
    [btn_Arrowright setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    //    [btn_Arrowleft addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_Backgroundimage addSubview:btn_Arrowright];
    
    
    
    UILabel  * lbl_singledish_img = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        //lbl_singledish_img.frame = CGRectMake(130,CGRectGetMinX(img_Backgroundimage.frame)+90, 220,40);
        lbl_singledish_img.frame =  CGRectMake(145,CGRectGetMidX(img_Backgroundimage.frame)-120, 220,40);
        
    }
    else if (IS_IPHONE_6)
    {
        lbl_singledish_img.frame = CGRectMake(130,CGRectGetMinX(img_Backgroundimage.frame)+90, 220,40);
        
    }
    else
    {
        lbl_singledish_img.frame = CGRectMake(95,CGRectGetMidX(img_Backgroundimage.frame)-70, 220,40);
        
    }
    lbl_singledish_img.text = @"Upload Dish image ";
    lbl_singledish_img.backgroundColor=[UIColor clearColor];
    lbl_singledish_img.textColor=[UIColor blackColor];
    lbl_singledish_img.textAlignment=NSTextAlignmentLeft;
    lbl_singledish_img.font = [UIFont fontWithName:kFontBold size:13];
    [view_for_dish addSubview:lbl_singledish_img];
    
    // bg1-img@2x
    
    
    img_backround =[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        img_backround.frame=CGRectMake(115, CGRectGetMaxY(lbl_singledish_img.frame)+1, 180, 180);
        
    }
    else if (IS_IPHONE_6)
    {
        img_backround.frame=CGRectMake(105, CGRectGetMaxY(lbl_singledish_img.frame)+1, 170, 170);
        
    }
    else
    {
        img_backround.frame=CGRectMake(80, CGRectGetMaxY(lbl_singledish_img.frame)+1, 160, 160);
        
    }
    img_backround.image=[UIImage imageNamed:@"bg1-img@2x.png"];
    
    [img_backround setUserInteractionEnabled:YES];
    img_backround.backgroundColor=[UIColor clearColor];
    [view_for_dish addSubview:img_backround];
    
    UIButton * edite_img = [[UIButton alloc]init];
    if (IS_IPHONE_6Plus)
    {
        edite_img.frame=CGRectMake(150, 150, 15, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        edite_img.frame=CGRectMake(150, 150, 15, 15);
        
    }
    else
    {
        edite_img.frame=CGRectMake(130, 130, 15, 15);
        
        
    }
    
    edite_img .backgroundColor = [UIColor clearColor];
    [edite_img addTarget:self action:@selector(btn_edite_click:) forControlEvents:UIControlEventTouchUpInside];
    [edite_img setImage:[UIImage imageNamed:@"icon 2.png"] forState:UIControlStateNormal];
    [img_backround   addSubview:edite_img];
    
    
    
    UILabel  * lbl_Weidth = [[UILabel alloc]init];
    
    
    if (IS_IPHONE_6Plus)
    {
        lbl_Weidth.frame=CGRectMake(CGRectGetMaxX(img_backround.frame)+3,CGRectGetMaxY(lbl_singledish_img.frame)+80, 100,120);
    }
    else if (IS_IPHONE_6)
    {
        lbl_Weidth.frame=CGRectMake(CGRectGetMaxX(img_backround.frame)+2,CGRectGetMaxY(lbl_singledish_img.frame)+80, 100,120);
    }
    else
    {
        lbl_Weidth.frame=CGRectMake(CGRectGetMaxX(img_backround.frame)+2,CGRectGetMaxY(lbl_singledish_img.frame)+65, 100,120);
        
    }
    
    lbl_Weidth.text = @"Width:150px\nHeight:150px\nSize:5mb\n.jpg..png";
    lbl_Weidth.backgroundColor=[UIColor clearColor];
    lbl_Weidth.textColor=[UIColor blackColor];
    lbl_Weidth.textAlignment=NSTextAlignmentLeft;
    lbl_Weidth.numberOfLines=0;
    lbl_Weidth.font = [UIFont fontWithName:kFontBold size:10];
    [view_for_dish addSubview:lbl_Weidth];
    
    
    UIImageView *img_backgroundimages=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        img_backgroundimages.frame=CGRectMake(5, CGRectGetMaxY(img_backround.frame)+2, WIDTH-4, 400);
    }
    else if (IS_IPHONE_6)
    {
        img_backgroundimages.frame=CGRectMake(5, CGRectGetMaxY(img_backround.frame)+5, WIDTH-4, 400);
    }
    else
    {
        img_backgroundimages.frame=CGRectMake(5, CGRectGetMaxY(img_backround.frame)+2, WIDTH-4, 400);
        
    }
    img_backgroundimages.image=[UIImage imageNamed:@"bg1-img@2x.png"];
    [img_backgroundimages setUserInteractionEnabled:YES];
    img_backgroundimages.backgroundColor=[UIColor clearColor];
    [view_for_dish addSubview:img_backgroundimages];
    
    
    txt_dishTitle = [[UITextField alloc] init];
    if (IS_IPHONE_6Plus)
    {
        txt_dishTitle.frame=CGRectMake(30, 10, 350, 30);
    }
    else if (IS_IPHONE_6)
    {
        
        txt_dishTitle.frame=CGRectMake(30, 10, 300, 30);
    }
    else
    {
        txt_dishTitle.frame=CGRectMake(20, 10, 300, 30);
    }
    txt_dishTitle.borderStyle = UITextBorderStyleNone;
    txt_dishTitle.font = [UIFont fontWithName:kFont size:13];
    txt_dishTitle.placeholder = @"Dish Title";
    [txt_dishTitle setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_dishTitle setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_dishTitle.leftViewMode = UITextFieldViewModeAlways;
    txt_dishTitle.userInteractionEnabled=YES;
    txt_dishTitle.textAlignment = NSTextAlignmentLeft;
    txt_dishTitle.backgroundColor = [UIColor clearColor];
    txt_dishTitle.keyboardType = UIKeyboardTypeAlphabet;
    txt_dishTitle.delegate = self;
    [img_backgroundimages addSubview:txt_dishTitle];
    
    UIImageView *line_img=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        line_img.frame=CGRectMake(30, CGRectGetMaxY(txt_dishTitle.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6){
        line_img.frame=CGRectMake(30, CGRectGetMaxY(txt_dishTitle.frame)+1, WIDTH-60, 0.5);
    }
    else  {
        line_img.frame=CGRectMake(20, CGRectGetMaxY(txt_dishTitle.frame)+1, WIDTH-40, 0.5);
        
    }
    [line_img setUserInteractionEnabled:YES];
    line_img.backgroundColor=[UIColor clearColor];
    line_img.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:line_img];
    
    
    UILabel  * lbl_Maxchar = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        lbl_Maxchar.frame=CGRectMake(330,CGRectGetMaxY(txt_dishTitle.frame)+4, 100,5);
        
    }
    else if (IS_IPHONE_6)
    {
        lbl_Maxchar.frame=CGRectMake(300,CGRectGetMaxY(txt_dishTitle.frame)+4, 100,5);
        
    }
    else
    {
        lbl_Maxchar.frame=CGRectMake(240,CGRectGetMaxY(txt_dishTitle.frame)+4, 100,5);
        
        
    }
    lbl_Maxchar.text = @"(Max. 50 char)";
    lbl_Maxchar.backgroundColor=[UIColor clearColor];
    lbl_Maxchar.textColor=[UIColor lightGrayColor];
    lbl_Maxchar.textAlignment=NSTextAlignmentLeft;
    lbl_Maxchar.numberOfLines=0;
    lbl_Maxchar.font = [UIFont fontWithName:kFontBold size:7];
    [img_backgroundimages addSubview:lbl_Maxchar];
    
    
    
    
    
    txt_Category= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus)
    {
        txt_Category.frame=CGRectMake(30, CGRectGetMaxY(txt_dishTitle.frame)+5, 300, 30);
    }
    else if (IS_IPHONE_6)
    {
        txt_Category.frame=CGRectMake(30, CGRectGetMaxY(txt_dishTitle.frame)+5, 250, 30);
    }
    else
    {
        txt_Category.frame=CGRectMake(20, CGRectGetMaxY(txt_dishTitle.frame)+5, 200, 30);
    }
    txt_Category.borderStyle = UITextBorderStyleNone;
    txt_Category.font = [UIFont fontWithName:kFont size:13];
    txt_Category.placeholder = @"Category";
    [txt_Category setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Category setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Category.leftViewMode = UITextFieldViewModeAlways;
    txt_Category.userInteractionEnabled=YES;
    txt_Category.textAlignment = NSTextAlignmentLeft;
    txt_Category.backgroundColor = [UIColor clearColor];
    txt_Category.keyboardType = UIKeyboardTypeAlphabet;
    txt_Category.delegate = self;
    [img_backgroundimages addSubview:txt_Category];
    
    UIImageView *img_lineimg=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        img_lineimg.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        img_lineimg.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+1, WIDTH-60, 0.5);
    }
    else
    {
        img_lineimg.frame=CGRectMake(20, CGRectGetMaxY(txt_Category.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_lineimg setUserInteractionEnabled:YES];
    img_lineimg.backgroundColor=[UIColor clearColor];
    img_lineimg.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_lineimg];
    
    
    
    UIButton *img_dropbox = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus)
    {
        img_dropbox.frame=CGRectMake(370, CGRectGetMaxY(txt_dishTitle.frame)+20, 15, 10);
    }
    else if (IS_IPHONE_6)
    {
        img_dropbox.frame=CGRectMake(325, CGRectGetMaxY(txt_dishTitle.frame)+20, 15, 10);
    }
    else
    {
        img_dropbox.frame=CGRectMake(275, CGRectGetMaxY(txt_dishTitle.frame)+20, 15, 10);
        
    }
    img_dropbox.backgroundColor = [UIColor clearColor];
    [img_dropbox setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [img_dropbox setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [img_dropbox addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimages addSubview:img_dropbox];
    
    
    UIButton * btn_on_category = [[UIButton alloc]init];
    if (IS_IPHONE_6Plus)
    {
        btn_on_category.frame=CGRectMake(20, CGRectGetMaxY(txt_dishTitle.frame)+2,330, 35);
        
    }
    else if (IS_IPHONE_6)
    {
        btn_on_category.frame=CGRectMake(20, CGRectGetMaxY(txt_dishTitle.frame)+2,330, 35);
        
    }
    else
    {
        btn_on_category.frame=CGRectMake(20, CGRectGetMaxY(txt_dishTitle.frame)+2,330, 35);
        
    }
    
    btn_on_category .backgroundColor = [UIColor clearColor];
    [btn_on_category addTarget:self action:@selector(btn_category_click:) forControlEvents:UIControlEventTouchUpInside];
    //  [btn_on_dropdwon setImage:[UIImage imageNamed:@"icon 2.png"] forState:UIControlStateNormal];
    [img_backgroundimages   addSubview:btn_on_category];
    
    
    
    
    
    
    
    
    
    txt_Cuisine= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus)
    {
        txt_Cuisine.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+5, 300, 30);
    }
    else if (IS_IPHONE_6)
    {
        txt_Cuisine.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+5, 250, 30);
    }
    else
    {
        txt_Cuisine.frame=CGRectMake(20, CGRectGetMaxY(txt_Category.frame)+5, 200, 30);
    }
    txt_Cuisine.borderStyle = UITextBorderStyleNone;
    txt_Cuisine.font = [UIFont fontWithName:kFont size:13];
    txt_Cuisine.placeholder = @"Cuisine";
    [txt_Cuisine setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Cuisine setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Cuisine.leftViewMode = UITextFieldViewModeAlways;
    txt_Cuisine.userInteractionEnabled=YES;
    txt_Cuisine.textAlignment = NSTextAlignmentLeft;
    txt_Cuisine.backgroundColor = [UIColor clearColor];
    txt_Cuisine.keyboardType = UIKeyboardTypeAlphabet;
    txt_Cuisine.delegate = self;
    [img_backgroundimages addSubview:txt_Cuisine];
    
    UIImageView *img_line=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        img_line.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        img_line.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    }
    else
    {
        img_line.frame=CGRectMake(20, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_line setUserInteractionEnabled:YES];
    img_line.backgroundColor=[UIColor clearColor];
    img_line.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_line];
    
    
    
    UIButton *img_dropbox1mg = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus)
    {
        img_dropbox1mg.frame=CGRectMake(370, CGRectGetMaxY(txt_Category.frame)+20, 15, 10);
    }
    else if (IS_IPHONE_6)
    {
        img_dropbox1mg.frame=CGRectMake(325, CGRectGetMaxY(txt_Category.frame)+20, 15, 10);
    }
    else
    {
        img_dropbox1mg.frame=CGRectMake(275, CGRectGetMaxY(txt_Category.frame)+20, 15, 10);
        
    }
    img_dropbox1mg.backgroundColor = [UIColor clearColor];
    [img_dropbox1mg setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [img_dropbox1mg setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [img_dropbox1mg addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimages addSubview:img_dropbox1mg];
    
    
    
    UIButton * btn_on_cuisine = [[UIButton alloc]init];
    if (IS_IPHONE_6Plus)
    {
        btn_on_cuisine.frame=CGRectMake(20, CGRectGetMaxY(txt_Category.frame)+2,330, 35);
        
    }
    else if (IS_IPHONE_6)
    {
        btn_on_cuisine.frame=CGRectMake(20, CGRectGetMaxY(txt_Category.frame)+2,330, 35);
        
    }
    else
    {
        btn_on_cuisine.frame=CGRectMake(20, CGRectGetMaxY(txt_Category.frame)+2,330, 35);
        
    }
    
    btn_on_cuisine .backgroundColor = [UIColor clearColor];
    [btn_on_cuisine addTarget:self action:@selector(btn_cuisine_click:) forControlEvents:UIControlEventTouchUpInside];
    //  [btn_on_dropdwon setImage:[UIImage imageNamed:@"icon 2.png"] forState:UIControlStateNormal];
    [img_backgroundimages   addSubview:btn_on_cuisine];
    
    
    txt_Course= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus)
    {
        txt_Course.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+5, 300, 30);
    }
    else if (IS_IPHONE_6)
    {
        txt_Course.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+5, 250, 30);
    }
    else
    {
        txt_Course.frame=CGRectMake(20, CGRectGetMaxY(txt_Cuisine.frame)+5, 200, 30);
    }
    txt_Course.borderStyle = UITextBorderStyleNone;
    txt_Course.font = [UIFont fontWithName:kFont size:13];
    txt_Course.placeholder = @"Courses";
    [txt_Course setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Course setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Course.leftViewMode = UITextFieldViewModeAlways;
    txt_Course.userInteractionEnabled=YES;
    txt_Course.textAlignment = NSTextAlignmentLeft;
    txt_Course.backgroundColor = [UIColor clearColor];
    txt_Course.keyboardType = UIKeyboardTypeAlphabet;
    txt_Course.delegate = self;
    [img_backgroundimages addSubview:txt_Course];
    
    UIImageView *img_lineimge=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        img_lineimge.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        img_lineimge.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+1, WIDTH-60, 0.5);
    }
    else
    {
        img_lineimge.frame=CGRectMake(20, CGRectGetMaxY(txt_Course.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_lineimge setUserInteractionEnabled:YES];
    img_lineimge.backgroundColor=[UIColor clearColor];
    img_lineimge.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_lineimge];
    
    
    
    UIButton *img_dropbox1mgs = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus)
    {
        img_dropbox1mgs.frame=CGRectMake(370, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
    }
    else if (IS_IPHONE_6)
    {
        img_dropbox1mgs.frame=CGRectMake(325, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
    }
    else
    {
        img_dropbox1mgs.frame=CGRectMake(275, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
        
    }
    img_dropbox1mgs.backgroundColor = [UIColor clearColor];
    [img_dropbox1mgs setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [img_dropbox1mgs setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [img_dropbox1mgs addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimages addSubview:img_dropbox1mgs];
    
    
    UIButton * btn_on_courses = [[UIButton alloc]init];
    if (IS_IPHONE_6Plus)
    {
        btn_on_courses.frame=CGRectMake(20, CGRectGetMaxY(txt_Cuisine.frame)+2,330, 35);
        
    }
    else if (IS_IPHONE_6)
    {
        btn_on_courses.frame=CGRectMake(20, CGRectGetMaxY(txt_Cuisine.frame)+2,330, 35);
        
    }
    else
    {
        btn_on_courses.frame=CGRectMake(20, CGRectGetMaxY(txt_Cuisine.frame)+2,330, 35);
        
    }
    
    btn_on_courses .backgroundColor = [UIColor clearColor];
    [btn_on_courses addTarget:self action:@selector(btn_course_click:) forControlEvents:UIControlEventTouchUpInside];
    //  [btn_on_dropdwon setImage:[UIImage imageNamed:@"icon 2.png"] forState:UIControlStateNormal];
    [img_backgroundimages   addSubview:btn_on_courses];
    
    
    
    
    
    
    
    txt_keyword= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus)
    {
        txt_keyword.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+5, 300, 30);
    }
    else if (IS_IPHONE_6)
    {
        txt_keyword.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+5, 250, 30);
    }
    else
    {
        txt_keyword.frame=CGRectMake(20, CGRectGetMaxY(txt_Course.frame)+5, 200, 30);
    }
    txt_keyword.borderStyle = UITextBorderStyleNone;
    txt_keyword.font = [UIFont fontWithName:kFont size:13];
    txt_keyword.placeholder = @"Keyword";
    [txt_keyword setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_keyword setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_keyword.leftViewMode = UITextFieldViewModeAlways;
    txt_keyword.userInteractionEnabled=YES;
    txt_keyword.textAlignment = NSTextAlignmentLeft;
    txt_keyword.backgroundColor = [UIColor clearColor];
    txt_keyword.keyboardType = UIKeyboardTypeAlphabet;
    txt_keyword.delegate = self;
    [img_backgroundimages addSubview:txt_keyword];
    
    UICollectionViewFlowLayout *layout_Keyword = [[UICollectionViewFlowLayout alloc] init];
    collectionView_Keywords = [[UICollectionView alloc] initWithFrame:CGRectMake(0,0 ,200,35)
                                                 collectionViewLayout:layout_Keyword];
    [layout_Keyword setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collectionView_Keywords setDataSource:self];
    [collectionView_Keywords setDelegate:self];
    collectionView_Keywords.scrollEnabled = YES;
    collectionView_Keywords.showsHorizontalScrollIndicator = NO;
    [collectionView_Keywords registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collectionView_Keywords setBackgroundColor:[UIColor clearColor]];
    layout_Keyword.minimumInteritemSpacing = 0;
    layout_Keyword.minimumLineSpacing = 0;
    collectionView_Keywords.userInteractionEnabled = YES;
    [collectionView_Keywords setHidden:YES];
    [txt_keyword addSubview:collectionView_Keywords];
    
    
    UIImageView *img_lineimges=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        img_lineimges.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        img_lineimges.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+1, WIDTH-60, 0.5);
    }
    else
    {
        img_lineimges.frame=CGRectMake(20, CGRectGetMaxY(txt_keyword.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_lineimges setUserInteractionEnabled:YES];
    img_lineimges.backgroundColor=[UIColor clearColor];
    img_lineimges.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_lineimges];
    
    
    
    UILabel  * lbl_uptolimit = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        lbl_uptolimit.frame=CGRectMake(350, CGRectGetMaxY(txt_Course.frame)+20, 35, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        lbl_uptolimit.frame=CGRectMake(310, CGRectGetMaxY(txt_Course.frame)+20, 35, 15);
        
    }
    else
    {
        lbl_uptolimit.frame=CGRectMake(260, CGRectGetMaxY(txt_Course.frame)+20, 35, 15);
        
        
    }
    lbl_uptolimit.text = @"Up to 5";
    lbl_uptolimit.backgroundColor=[UIColor clearColor];
    lbl_uptolimit.textColor=[UIColor lightGrayColor];
    lbl_uptolimit.textAlignment=NSTextAlignmentLeft;
    lbl_uptolimit.numberOfLines=0;
    lbl_uptolimit.font = [UIFont fontWithName:kFontBold size:9];
    [img_backgroundimages addSubview:lbl_uptolimit];
    
    //    UIButton *btn_uptolimit = [[UIButton alloc] init];
    //
    //    if (IS_IPHONE_6Plus){
    //        btn_uptolimit.frame=CGRectMake(200, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    //    }
    //    else if (IS_IPHONE_6){
    //        btn_uptolimit.frame=CGRectMake(325, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
    //    }
    //    else  {
    //        btn_uptolimit.frame=CGRectMake(250, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    //
    //    }
    //    btn_uptolimit.backgroundColor = [UIColor clearColor];
    ////    [btn_uptolimit setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    //    [btn_uptolimit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    //    [btn_uptolimit addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    //    [img_backgroundimages addSubview:btn_uptolimit];
    //
    
    txt_Price= [[UITextField alloc] init];
    if (IS_IPHONE_6Plus)
    {
        txt_Price.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6)
    {
        txt_Price.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+5, 200, 30);
    }
    else
    {
        txt_Price.frame=CGRectMake(20, CGRectGetMaxY(txt_keyword.frame)+5, 200, 30);
    }
    txt_Price.borderStyle = UITextBorderStyleNone;
    txt_Price.font = [UIFont fontWithName:kFont size:13];
    txt_Price.placeholder = @"$ Price";
    [txt_Price setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Price setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Price.leftViewMode = UITextFieldViewModeAlways;
    txt_Price.userInteractionEnabled=YES;
    txt_Price.textAlignment = NSTextAlignmentLeft;
    txt_Price.backgroundColor = [UIColor clearColor];
    txt_Price.keyboardType = UIKeyboardTypeAlphabet;
    txt_Price .backgroundColor = [UIColor clearColor];
    txt_Price .keyboardType = UIKeyboardTypeNumberPad;
    txt_Price.delegate = self;
    [img_backgroundimages addSubview:txt_Price];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_Done_in_priceofdish:)],
                           nil];
    [numberToolbar sizeToFit];
    txt_Price.inputAccessoryView = numberToolbar;
    
    
    UIImageView *img_linesimge=[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        img_linesimge.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        img_linesimge.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+1, WIDTH-60, 0.5);
    }
    else
    {
        img_linesimge.frame=CGRectMake(20, CGRectGetMaxY(txt_Price.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_linesimge setUserInteractionEnabled:YES];
    img_linesimge.backgroundColor=[UIColor clearColor];
    img_linesimge.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages addSubview:img_linesimge];
    
    
    
    UILabel  * lbl_PerMeal = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        lbl_PerMeal.frame=CGRectMake(345, CGRectGetMaxY(txt_keyword.frame)+20, 50, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        lbl_PerMeal.frame=CGRectMake(300, CGRectGetMaxY(txt_keyword.frame)+20, 50, 15);
        
    }
    else
    {
        lbl_PerMeal.frame=CGRectMake(250, CGRectGetMaxY(txt_keyword.frame)+20, 50, 15);
        
        
    }
    lbl_PerMeal.text = @"Per Serving";
    lbl_PerMeal.backgroundColor=[UIColor clearColor];
    lbl_PerMeal.textColor=[UIColor lightGrayColor];
    lbl_PerMeal.textAlignment=NSTextAlignmentLeft;
    lbl_PerMeal.numberOfLines=0;
    lbl_PerMeal.font = [UIFont fontWithName:kFontBold size:9];
    [img_backgroundimages addSubview:lbl_PerMeal];
    
    
    
    
    
    UILabel  * lbl_Description = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        lbl_Description.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+10, 100, 25);
        
    }
    else if (IS_IPHONE_6)
    {
        lbl_Description.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+10, 100, 25);
        
    }
    else
    {
        lbl_Description.frame=CGRectMake(20, CGRectGetMaxY(txt_Price.frame)+10, 100, 25);
        
        
    }
    lbl_Description.text = @"Description";
    lbl_Description.backgroundColor=[UIColor clearColor];
    lbl_Description.textColor=[UIColor blackColor];
    lbl_Description.textAlignment=NSTextAlignmentLeft;
    lbl_Description.numberOfLines=0;
    lbl_Description.font = [UIFont fontWithName:kFont size:14];
    [img_backgroundimages addSubview:lbl_Description];
    
    
    
    txt_view =[[UITextView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        txt_view.frame=CGRectMake(30, CGRectGetMaxY(lbl_Description.frame)+5, WIDTH-60, 100);
        
    }
    else if (IS_IPHONE_6)
    {
        txt_view.frame=CGRectMake(30, CGRectGetMaxY(lbl_Description.frame)+5, WIDTH-60, 100);
        
    }
    else
    {
        txt_view.frame=CGRectMake(20, CGRectGetMaxY(lbl_Description.frame)+5, WIDTH-40, 100);
        
        
    }
    [txt_view setReturnKeyType:UIReturnKeyDone];
    txt_view.scrollEnabled=YES;
    [txt_view setDelegate:self];
    [txt_view setReturnKeyType:UIReturnKeyDone];
    [txt_view setFont:[UIFont fontWithName:kFont size:10]];
    [txt_view setTextColor:[UIColor lightTextColor]];
    txt_view.userInteractionEnabled=YES;
    txt_view.backgroundColor=[UIColor whiteColor];
    txt_view.delegate=self;
    txt_view.textColor=[UIColor blackColor];
    txt_view.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    txt_view.layer.borderWidth=1.0f;
    txt_view.clipsToBounds=YES;
    [img_backgroundimages addSubview:txt_view];
    
    
    
    UIToolbar *keyboard = nil;
    if(keyboard == nil)
    {
        keyboard = [[UIToolbar alloc] initWithFrame:CGRectMake(10, 0, self.view.bounds.size.width, 44)];
        [keyboard setBarStyle:UIBarStyleBlackTranslucent];
        
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_DoneremarksKeyboard:)];
        [keyboard setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    txt_view.inputAccessoryView = keyboard;
    
    
    
    
    
    UILabel  * lblmax = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        lblmax.frame=CGRectMake(320, CGRectGetMaxY(txt_view.frame)+1, 70, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        lblmax.frame=CGRectMake(280, CGRectGetMaxY(txt_view.frame)+1, 70, 15);
        
    }
    else
    {
        lblmax.frame=CGRectMake(240, CGRectGetMaxY(txt_view.frame)+1, 70, 15);
        
        
    }
    
    lblmax.text = @"(500 char. max.)";
    lblmax.backgroundColor=[UIColor clearColor];
    lblmax.textColor=[UIColor lightGrayColor];
    lblmax.numberOfLines = 0;
    lblmax.font = [UIFont fontWithName:kFont size:8];
    [img_backgroundimages addSubview:lblmax];
    
    
    view_for_meal = [[UIView alloc]init];
    view_for_meal.frame=CGRectMake(0,CGRectGetMaxY(black_stripimg.frame)+5,WIDTH,750);
    [view_for_meal setUserInteractionEnabled:YES];
    view_for_meal.backgroundColor=[UIColor clearColor];
    [scrollviewMenu  addSubview:  view_for_meal];
    view_for_meal.hidden =YES;
    
    
    
    
    UIImageView *img_Backgroundimage_for_meal =[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        img_Backgroundimage_for_meal.frame =  CGRectMake(02, 10, WIDTH, 80);
    }
    else if (IS_IPHONE_6)
    {
        img_Backgroundimage_for_meal.frame = CGRectMake(02, 10, WIDTH, 80);
    }
    else
    {
        img_Backgroundimage_for_meal.frame = CGRectMake(0, 10, WIDTH, 80);
    }
    
    [img_Backgroundimage_for_meal setUserInteractionEnabled:YES];
    img_Backgroundimage_for_meal.backgroundColor=[UIColor clearColor];
    img_Backgroundimage_for_meal.image=[UIImage imageNamed:@"bg1.png"];
    [view_for_meal addSubview:img_Backgroundimage_for_meal];
    
    
    UIImageView *Arrow_left_for_meal = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        Arrow_left_for_meal.frame=CGRectMake(5, 30, 10, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        Arrow_left_for_meal.frame=CGRectMake(5, 25, 10, 15);
    }
    else
    {
        Arrow_left_for_meal.frame=CGRectMake(5, 30, 10, 15);
        
    }
    [Arrow_left_for_meal setUserInteractionEnabled:YES];
    Arrow_left_for_meal.backgroundColor=[UIColor clearColor];
    Arrow_left_for_meal.image=[UIImage imageNamed:@"arrow_left.png"];
    [img_Backgroundimage_for_meal addSubview:Arrow_left_for_meal];
    
    
    UIButton *btn_Arrowleft_for_meal = [[UIButton alloc] init];
    if (IS_IPHONE_6Plus)
    {
        btn_Arrowleft_for_meal.frame = CGRectMake(15, 20, 10, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        btn_Arrowleft_for_meal.frame = CGRectMake(5, 25, 10, 15);
        
    }
    else
    {
        btn_Arrowleft_for_meal.frame = CGRectMake(5, 25, 10, 15);
        
    }
    
    btn_Arrowleft_for_meal.backgroundColor = [UIColor clearColor];
    [btn_Arrowleft_for_meal setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    [btn_Arrowleft_for_meal addTarget:self action:@selector(click_on_left_arrow:) forControlEvents:UIControlEventTouchUpInside ];
    [img_Backgroundimage_for_meal addSubview:btn_Arrowleft_for_meal];
    
    
    layout2=[[UICollectionViewFlowLayout alloc] init];
    collView_serviceDirectory_for_meal = [[UICollectionView alloc] initWithFrame:CGRectMake(30,3,WIDTH-65,70)
                                                            collectionViewLayout:layout2];
    [layout2 setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collView_serviceDirectory_for_meal setDataSource:self];
    [collView_serviceDirectory_for_meal setDelegate:self];
    collView_serviceDirectory_for_meal.scrollEnabled = YES;
    collView_serviceDirectory_for_meal.showsVerticalScrollIndicator = NO;
    collView_serviceDirectory_for_meal.showsHorizontalScrollIndicator = NO;
    collView_serviceDirectory_for_meal.pagingEnabled = NO;
    [collView_serviceDirectory_for_meal registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collView_serviceDirectory_for_meal setBackgroundColor:[UIColor clearColor]];
    layout2.minimumInteritemSpacing = 2;
    layout2.minimumLineSpacing = 0;
    collView_serviceDirectory_for_meal.userInteractionEnabled = YES;
    [img_Backgroundimage_for_meal addSubview:collView_serviceDirectory_for_meal];
    
    
    
    
    UIImageView *Arrow_right_in_meal =[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        Arrow_right_in_meal.frame=CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+10, 30, 10, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        Arrow_right_in_meal.frame=CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
        
    }
    else
    {
        Arrow_right_in_meal.frame=CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 30, 10, 15);
        
    }
    [Arrow_right_in_meal setUserInteractionEnabled:YES];
    Arrow_right_in_meal.backgroundColor=[UIColor clearColor];
    Arrow_right_in_meal.image=[UIImage imageNamed:@"arrow_right.png"];
    [img_Backgroundimage_for_meal addSubview:Arrow_right_in_meal];
    
    
    UIButton *btn_Arrowright_in_meal = [[UIButton alloc] init];
    if (IS_IPHONE_6Plus)
    {
        btn_Arrowright_in_meal.frame = CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+1, 25, 10, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        btn_Arrowright_in_meal.frame = CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
        
    }
    else
    {
        btn_Arrowright_in_meal.frame = CGRectMake(CGRectGetMaxX(collView_serviceDirectory.frame)+14, 25, 10, 15);
        
    }
    btn_Arrowright_in_meal.backgroundColor = [UIColor clearColor];
    //    [btn_Arrowleft setImage:[UIImage imageNamed:@"arrow_left.png"] forState:UIControlStateNormal];
    [btn_Arrowright_in_meal setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    //    [btn_Arrowleft addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_Backgroundimage addSubview:btn_Arrowright_in_meal];
    
    
    
    UILabel  * lbl_meal_img = [[UILabel alloc]init];
    if (IS_IPHONE_6Plus)
    {
        lbl_meal_img.frame = CGRectMake(145,CGRectGetMidX(img_Backgroundimage.frame)-120, 220,40);
        
    }
    else if (IS_IPHONE_6)
    {
        lbl_meal_img.frame = CGRectMake(130,CGRectGetMinX(img_Backgroundimage.frame)+90, 220,40);
        
    }
    else
    {
        lbl_meal_img.frame = CGRectMake(95,CGRectGetMidX(img_Backgroundimage.frame)-70, 220,40);
        
    }
    lbl_meal_img.text = @"Upload Meal image ";
    lbl_meal_img.backgroundColor=[UIColor clearColor];
    lbl_meal_img.textColor=[UIColor blackColor];
    lbl_meal_img.textAlignment=NSTextAlignmentLeft;
    lbl_meal_img.font = [UIFont fontWithName:kFontBold size:13];
    [view_for_meal addSubview:lbl_meal_img];
    
    // bg1-img@2x
    
    
    
    
    
    img_backround_in_meal = [[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        img_backround_in_meal.frame=CGRectMake(115, CGRectGetMaxY(lbl_singledish_img.frame)+1, 180, 180);
        
    }
    else if (IS_IPHONE_6)
    {
        img_backround_in_meal.frame=CGRectMake(105, CGRectGetMaxY(lbl_singledish_img.frame)+1, 170, 170);
        
    }
    else
    {
        img_backround_in_meal.frame=CGRectMake(80, CGRectGetMaxY(lbl_singledish_img.frame)+1, 160, 160);
        
        
    }
    img_backround_in_meal.image=[UIImage imageNamed:@"bg1-img@2x.png"];
    
    [img_backround_in_meal setUserInteractionEnabled:YES];
    img_backround_in_meal.backgroundColor=[UIColor clearColor];
    [view_for_meal addSubview:img_backround_in_meal];
    
    
    UIButton * edite_img_in_meal = [[UIButton alloc]init];
    if (IS_IPHONE_6Plus)
    {
        edite_img_in_meal.frame=CGRectMake(150, 150, 15, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        edite_img_in_meal.frame=CGRectMake(150, 150, 15, 15);
        
    }
    else
    {
        edite_img_in_meal.frame=CGRectMake(130, 130, 15, 15);
        
        
    }
    
    edite_img_in_meal .backgroundColor = [UIColor clearColor];
    [edite_img_in_meal addTarget:self action:@selector(btn_edite_in_meal_click:) forControlEvents:UIControlEventTouchUpInside];
    [edite_img_in_meal setImage:[UIImage imageNamed:@"icon 2.png"] forState:UIControlStateNormal];
    [img_backround_in_meal   addSubview:edite_img_in_meal];
    
    
    
    
    UILabel  * lbl_Weidth_in_meal = [[UILabel alloc]init];
    
    
    if (IS_IPHONE_6Plus)
    {
        lbl_Weidth_in_meal.frame=CGRectMake(CGRectGetMaxX(img_backround.frame)+3,CGRectGetMaxY(lbl_singledish_img.frame)+80, 100,120);
    }
    else if (IS_IPHONE_6)
    {
        lbl_Weidth_in_meal.frame=CGRectMake(CGRectGetMaxX(img_backround.frame)+2,CGRectGetMaxY(lbl_singledish_img.frame)+80, 100,120);
    }
    else
    {
        lbl_Weidth_in_meal.frame=CGRectMake(CGRectGetMaxX(img_backround.frame)+2,CGRectGetMaxY(lbl_singledish_img.frame)+65, 100,120);
        
    }
    
    lbl_Weidth_in_meal.text = @"Width:150px\nHeight:150px\nSize:5mb\n.jpg..png";
    lbl_Weidth_in_meal.backgroundColor=[UIColor clearColor];
    lbl_Weidth_in_meal.textColor=[UIColor blackColor];
    lbl_Weidth_in_meal.textAlignment=NSTextAlignmentLeft;
    lbl_Weidth_in_meal.numberOfLines=0;
    lbl_Weidth_in_meal.font = [UIFont fontWithName:kFontBold size:10];
    [view_for_meal addSubview:lbl_Weidth_in_meal];
    
    
    UIImageView *img_backgroundimages_in_meal =[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        img_backgroundimages_in_meal.frame=CGRectMake(5, CGRectGetMaxY(img_backround.frame)+2, WIDTH-4, 400);
    }
    else if (IS_IPHONE_6)
    {
        img_backgroundimages_in_meal.frame=CGRectMake(5, CGRectGetMaxY(img_backround.frame)+5, WIDTH-4, 400);
    }
    else
    {
        img_backgroundimages_in_meal.frame=CGRectMake(5, CGRectGetMaxY(img_backround.frame)+2, WIDTH-4, 400);
        
    }
    img_backgroundimages_in_meal.image=[UIImage imageNamed:@"bg1-img@2x.png"];
    [img_backgroundimages_in_meal setUserInteractionEnabled:YES];
    img_backgroundimages_in_meal.backgroundColor=[UIColor clearColor];
    [view_for_meal addSubview:img_backgroundimages_in_meal];
    
    
    txt_MealTitle_in_meal = [[UITextField alloc] init];
    if (IS_IPHONE_6Plus)
    {
        txt_MealTitle_in_meal.frame=CGRectMake(30, 10, 250, 30);
    }
    else if (IS_IPHONE_6)
    {
        
        txt_MealTitle_in_meal.frame=CGRectMake(30, 10, 250, 30);
    }
    else
    {
        txt_MealTitle_in_meal.frame=CGRectMake(20, 10, 250, 30);
    }
    txt_MealTitle_in_meal.borderStyle = UITextBorderStyleNone;
    txt_MealTitle_in_meal.font = [UIFont fontWithName:kFont size:13];
    txt_MealTitle_in_meal.placeholder = @"Meal Title";
    [txt_MealTitle_in_meal setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_MealTitle_in_meal setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_MealTitle_in_meal.leftViewMode = UITextFieldViewModeAlways;
    txt_MealTitle_in_meal.userInteractionEnabled=YES;
    txt_MealTitle_in_meal.textAlignment = NSTextAlignmentLeft;
    txt_MealTitle_in_meal.backgroundColor = [UIColor clearColor];
    txt_MealTitle_in_meal.keyboardType = UIKeyboardTypeAlphabet;
    txt_MealTitle_in_meal.delegate = self;
    [img_backgroundimages_in_meal addSubview:txt_MealTitle_in_meal];
    
    UIImageView *line_img_in_meal =[[UIImageView alloc]init];
    
    
    if (IS_IPHONE_6Plus)
    {
        line_img_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_dishTitle.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        line_img_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_dishTitle.frame)+1, WIDTH-60, 0.5);
    }
    else
    {
        line_img_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_dishTitle.frame)+1, WIDTH-40, 0.5);
        
    }
    [line_img_in_meal setUserInteractionEnabled:YES];
    line_img_in_meal.backgroundColor=[UIColor clearColor];
    line_img_in_meal.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages_in_meal addSubview:line_img_in_meal];
    
    
    UILabel  * lbl_Maxchar_in_meal = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        lbl_Maxchar_in_meal.frame=CGRectMake(330,CGRectGetMaxY(txt_dishTitle.frame)+4, 100,5);
        
    }
    else if (IS_IPHONE_6)
    {
        lbl_Maxchar_in_meal.frame=CGRectMake(300,CGRectGetMaxY(txt_dishTitle.frame)+4, 100,5);
        
    }
    else
    {
        lbl_Maxchar_in_meal.frame=CGRectMake(240,CGRectGetMaxY(txt_dishTitle.frame)+4, 100,5);
        
        
    }
    lbl_Maxchar_in_meal.text = @"(Max. 50 char)";
    lbl_Maxchar_in_meal.backgroundColor=[UIColor clearColor];
    lbl_Maxchar_in_meal.textColor=[UIColor lightGrayColor];
    lbl_Maxchar_in_meal.textAlignment=NSTextAlignmentLeft;
    lbl_Maxchar_in_meal.numberOfLines=0;
    lbl_Maxchar_in_meal.font = [UIFont fontWithName:kFontBold size:7];
    [img_backgroundimages_in_meal addSubview:lbl_Maxchar_in_meal];
    
    
    
    
    
    txt_Category_in_meal = [[UITextField alloc] init];
    if (IS_IPHONE_6Plus)
    {
        txt_Category_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_dishTitle.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6)
    {
        txt_Category_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_dishTitle.frame)+5, 200, 30);
    }
    else
    {
        txt_Category_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_dishTitle.frame)+5, 200, 30);
    }
    txt_Category_in_meal.borderStyle = UITextBorderStyleNone;
    txt_Category_in_meal.font = [UIFont fontWithName:kFont size:13];
    txt_Category_in_meal.placeholder = @"Category";
    [txt_Category_in_meal setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Category_in_meal setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Category_in_meal.leftViewMode = UITextFieldViewModeAlways;
    txt_Category_in_meal.userInteractionEnabled=YES;
    txt_Category_in_meal.textAlignment = NSTextAlignmentLeft;
    txt_Category_in_meal.backgroundColor = [UIColor clearColor];
    txt_Category_in_meal.keyboardType = UIKeyboardTypeAlphabet;
    txt_Category_in_meal.delegate = self;
    [img_backgroundimages_in_meal addSubview:txt_Category_in_meal];
    
    
    UIImageView *img_lineimg_in_meal =[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        img_lineimg_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        img_lineimg_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+1, WIDTH-60, 0.5);
    }
    else
    {
        img_lineimg_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_Category.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_lineimg_in_meal setUserInteractionEnabled:YES];
    img_lineimg_in_meal.backgroundColor=[UIColor clearColor];
    img_lineimg_in_meal.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages_in_meal addSubview:img_lineimg_in_meal];
    
    
    
    UIButton *img_dropbox_in_meal = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus)
    {
        img_dropbox_in_meal.frame=CGRectMake(370, CGRectGetMaxY(txt_dishTitle.frame)+20, 15, 10);
    }
    else if (IS_IPHONE_6)
    {
        img_dropbox_in_meal.frame=CGRectMake(325, CGRectGetMaxY(txt_dishTitle.frame)+20, 15, 10);
    }
    else
    {
        img_dropbox_in_meal.frame=CGRectMake(275, CGRectGetMaxY(txt_dishTitle.frame)+20, 15, 10);
        
    }
    img_dropbox_in_meal.backgroundColor = [UIColor clearColor];
    [img_dropbox_in_meal setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [img_dropbox_in_meal setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [img_dropbox addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimages_in_meal addSubview:img_dropbox_in_meal];
    
    
    UIButton * btn_on_category_in_meal = [[UIButton alloc]init];
    if (IS_IPHONE_6Plus)
    {
        btn_on_category_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_MealTitle_in_meal.frame)+2,330, 35);
        
    }
    else if (IS_IPHONE_6)
    {
        btn_on_category_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_MealTitle_in_meal.frame)+2,330, 35);
        
    }
    else
    {
        btn_on_category_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_MealTitle_in_meal.frame)+2,330, 35);
        
    }
    
    btn_on_category_in_meal .backgroundColor = [UIColor clearColor];
    [btn_on_category_in_meal addTarget:self action:@selector(btn_category_in_meal_click:) forControlEvents:UIControlEventTouchUpInside];
    //  [btn_on_dropdwon setImage:[UIImage imageNamed:@"icon 2.png"] forState:UIControlStateNormal];
    [img_backgroundimages_in_meal   addSubview:btn_on_category_in_meal];
    
    
    
    
    
    
    
    txt_Cuisine_in_meal = [[UITextField alloc] init];
    if (IS_IPHONE_6Plus)
    {
        txt_Cuisine_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6)
    {
        txt_Cuisine_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_Category.frame)+5, 200, 30);
    }
    else
    {
        txt_Cuisine_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_Category.frame)+5, 200, 30);
    }
    txt_Cuisine_in_meal.borderStyle = UITextBorderStyleNone;
    txt_Cuisine_in_meal.font = [UIFont fontWithName:kFont size:13];
    txt_Cuisine_in_meal.placeholder = @"Cuisine";
    [txt_Cuisine_in_meal setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Cuisine_in_meal setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Cuisine_in_meal.leftViewMode = UITextFieldViewModeAlways;
    txt_Cuisine_in_meal.userInteractionEnabled=YES;
    txt_Cuisine_in_meal.textAlignment = NSTextAlignmentLeft;
    txt_Cuisine_in_meal.backgroundColor = [UIColor clearColor];
    txt_Cuisine_in_meal.keyboardType = UIKeyboardTypeAlphabet;
    txt_Cuisine_in_meal.delegate = self;
    [img_backgroundimages_in_meal addSubview:txt_Cuisine_in_meal];
    
    
    UIImageView *img_line_in_meal =[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        img_line_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        img_line_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    }
    else
    {
        img_line_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_line_in_meal setUserInteractionEnabled:YES];
    img_line_in_meal.backgroundColor=[UIColor clearColor];
    img_line_in_meal.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages_in_meal addSubview:img_line_in_meal];
    
    
    
    UIButton *img_dropbox1mg_in_meal = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus)
    {
        img_dropbox1mg_in_meal.frame=CGRectMake(370, CGRectGetMaxY(txt_Category.frame)+20, 15, 10);
    }
    else if (IS_IPHONE_6)
    {
        img_dropbox1mg_in_meal.frame=CGRectMake(325, CGRectGetMaxY(txt_Category.frame)+20, 15, 10);
    }
    else
    {
        img_dropbox1mg_in_meal.frame=CGRectMake(275, CGRectGetMaxY(txt_Category.frame)+20, 15, 10);
        
    }
    img_dropbox1mg_in_meal.backgroundColor = [UIColor clearColor];
    [img_dropbox1mg_in_meal setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [img_dropbox1mg_in_meal setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [img_dropbox1mg addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimages_in_meal addSubview:img_dropbox1mg_in_meal];
    
    UIButton * btn_on_cuisine_in_meal = [[UIButton alloc]init];
    if (IS_IPHONE_6Plus)
    {
        btn_on_cuisine_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_Category_in_meal.frame)+2,330, 35);
        
    }
    else if (IS_IPHONE_6)
    {
        btn_on_cuisine_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_Category_in_meal.frame)+2,330, 35);
        
    }
    else
    {
        btn_on_cuisine_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_Category_in_meal.frame)+2,330, 35);
        
    }
    
    btn_on_cuisine_in_meal .backgroundColor = [UIColor clearColor];
    [btn_on_cuisine_in_meal addTarget:self action:@selector(btn_cuisine_in_meal_click:) forControlEvents:UIControlEventTouchUpInside];
    //  [btn_on_dropdwon setImage:[UIImage imageNamed:@"icon 2.png"] forState:UIControlStateNormal];
    [img_backgroundimages_in_meal   addSubview:btn_on_cuisine_in_meal];
    
    
    
    txt_Course_in_meal = [[UITextField alloc] init];
    if (IS_IPHONE_6Plus)
    {
        txt_Course_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6)
    {
        txt_Course_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_Cuisine.frame)+5, 200, 30);
    }
    else
    {
        txt_Course_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_Cuisine.frame)+5, 200, 30);
    }
    txt_Course_in_meal.borderStyle = UITextBorderStyleNone;
    txt_Course_in_meal.font = [UIFont fontWithName:kFont size:13];
    txt_Course_in_meal.placeholder = @"Courses";
    [txt_Course_in_meal setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Course_in_meal setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Course_in_meal.leftViewMode = UITextFieldViewModeAlways;
    txt_Course_in_meal.userInteractionEnabled=YES;
    txt_Course_in_meal.textAlignment = NSTextAlignmentLeft;
    txt_Course_in_meal.backgroundColor = [UIColor clearColor];
    txt_Course_in_meal.keyboardType = UIKeyboardTypeAlphabet;
    txt_Course_in_meal.delegate = self;
    [img_backgroundimages_in_meal addSubview:txt_Course_in_meal];
    
    
    UIImageView *img_lineimge_in_meal =[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        img_lineimge_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        img_lineimge_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+1, WIDTH-60, 0.5);
    }
    else
    {
        img_lineimge_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_Course.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_lineimge_in_meal setUserInteractionEnabled:YES];
    img_lineimge_in_meal.backgroundColor=[UIColor clearColor];
    img_lineimge_in_meal.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages_in_meal addSubview:img_lineimge_in_meal];
    
    
    
    UIButton *img_dropbox1mgs_in_meal = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus)
    {
        img_dropbox1mgs_in_meal.frame=CGRectMake(370, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
    }
    else if (IS_IPHONE_6)
    {
        img_dropbox1mgs_in_meal.frame=CGRectMake(325, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
    }
    else
    {
        img_dropbox1mgs_in_meal.frame=CGRectMake(275, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
        
    }
    img_dropbox1mgs_in_meal.backgroundColor = [UIColor clearColor];
    [img_dropbox1mgs_in_meal setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    [img_dropbox1mgs_in_meal setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [img_dropbox1mgs addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_backgroundimages_in_meal addSubview:img_dropbox1mgs_in_meal];
    
    
    UIButton * btn_on_courses_in_meal = [[UIButton alloc]init];
    if (IS_IPHONE_6Plus)
    {
        btn_on_courses_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_Cuisine_in_meal.frame)+2,330, 35);
        
    }
    else if (IS_IPHONE_6)
    {
        btn_on_courses_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_Cuisine_in_meal.frame)+2,330, 35);
        
    }
    else
    {
        btn_on_courses_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_Cuisine_in_meal.frame)+2,330, 35);
        
    }
    
    btn_on_courses_in_meal .backgroundColor = [UIColor clearColor];
    [btn_on_courses_in_meal addTarget:self action:@selector(btn_course_in_meal_click:) forControlEvents:UIControlEventTouchUpInside];
    //  [btn_on_dropdwon setImage:[UIImage imageNamed:@"icon 2.png"] forState:UIControlStateNormal];
    [img_backgroundimages_in_meal   addSubview:btn_on_courses_in_meal];
    
    
    
    
    
    txt_keyword_in_meal = [[UITextField alloc] init];
    if (IS_IPHONE_6Plus)
    {
        txt_keyword_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6)
    {
        txt_keyword_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_Course.frame)+5, 200, 30);
    }
    else
    {
        txt_keyword_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_Course.frame)+5, 200, 30);
    }
    
    txt_keyword_in_meal.borderStyle = UITextBorderStyleNone;
    txt_keyword_in_meal.font = [UIFont fontWithName:kFont size:13];
    txt_keyword_in_meal.placeholder = @"Keywords (eg: Tacos,fries)";
    [txt_keyword_in_meal setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_keyword_in_meal setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_keyword_in_meal.leftViewMode = UITextFieldViewModeAlways;
    txt_keyword_in_meal.userInteractionEnabled=YES;
    txt_keyword_in_meal.textAlignment = NSTextAlignmentLeft;
    txt_keyword_in_meal.backgroundColor = [UIColor clearColor];
    txt_keyword_in_meal.keyboardType = UIKeyboardTypeAlphabet;
    txt_keyword_in_meal.delegate = self;
    [img_backgroundimages_in_meal addSubview:txt_keyword_in_meal];
    
    
    
    UICollectionViewFlowLayout *layout_Keyword1 = [[UICollectionViewFlowLayout alloc] init];
    collectionView_Keywords_meal = [[UICollectionView alloc] initWithFrame:CGRectMake(0,0 ,200,35)
                                                      collectionViewLayout:layout_Keyword1];
    [layout_Keyword setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collectionView_Keywords_meal setDataSource:self];
    [collectionView_Keywords_meal setDelegate:self];
    collectionView_Keywords_meal.scrollEnabled = YES;
    collectionView_Keywords_meal.showsHorizontalScrollIndicator = NO;
    [collectionView_Keywords_meal registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collectionView_Keywords_meal setBackgroundColor:[UIColor clearColor]];
    layout_Keyword1.minimumInteritemSpacing = 0;
    layout_Keyword1.minimumLineSpacing = 0;
    collectionView_Keywords_meal.userInteractionEnabled = YES;
    [collectionView_Keywords setHidden:YES];
    [txt_keyword_in_meal addSubview:collectionView_Keywords_meal];
    
    UIImageView *img_lineimges_in_meal =[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        img_lineimges_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6)
        
    {
        img_lineimges_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+1, WIDTH-60, 0.5);
    }
    else
    {
        img_lineimges_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_keyword.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_lineimges_in_meal setUserInteractionEnabled:YES];
    img_lineimges_in_meal.backgroundColor=[UIColor clearColor];
    img_lineimges_in_meal.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages_in_meal addSubview:img_lineimges_in_meal];
    
    
    
    UILabel  * lbl_uptolimit_in_meal = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        lbl_uptolimit_in_meal.frame=CGRectMake(350, CGRectGetMaxY(txt_Course.frame)+20, 35, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        lbl_uptolimit_in_meal.frame=CGRectMake(310, CGRectGetMaxY(txt_Course.frame)+20, 35, 15);
        
    }
    else
    {
        lbl_uptolimit_in_meal.frame=CGRectMake(260, CGRectGetMaxY(txt_Course.frame)+20, 35, 15);
        
        
    }
    lbl_uptolimit_in_meal.text = @"Up to 5";
    lbl_uptolimit_in_meal.backgroundColor=[UIColor clearColor];
    lbl_uptolimit_in_meal.textColor=[UIColor lightGrayColor];
    lbl_uptolimit_in_meal.textAlignment=NSTextAlignmentLeft;
    lbl_uptolimit_in_meal.numberOfLines=0;
    lbl_uptolimit_in_meal.font = [UIFont fontWithName:kFontBold size:9];
    [img_backgroundimages_in_meal addSubview:lbl_uptolimit_in_meal];
    
    //    UIButton *btn_uptolimit = [[UIButton alloc] init];
    //
    //    if (IS_IPHONE_6Plus){
    //        btn_uptolimit.frame=CGRectMake(200, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    //    }
    //    else if (IS_IPHONE_6){
    //        btn_uptolimit.frame=CGRectMake(325, CGRectGetMaxY(txt_Cuisine.frame)+20, 15, 10);
    //    }
    //    else  {
    //        btn_uptolimit.frame=CGRectMake(250, CGRectGetMaxY(txt_Cuisine.frame)+1, WIDTH-60, 0.5);
    //
    //    }
    //    btn_uptolimit.backgroundColor = [UIColor clearColor];
    ////    [btn_uptolimit setImage:[UIImage imageNamed:@"drop down.png"] forState:UIControlStateNormal];
    //    [btn_uptolimit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    //    [btn_uptolimit addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    //    [img_backgroundimages addSubview:btn_uptolimit];
    //
    
    txt_Price_in_meal = [[UITextField alloc] init];
    if (IS_IPHONE_6Plus)
    {
        txt_Price_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+5, 200, 30);
    }
    else if (IS_IPHONE_6)
    {
        txt_Price_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_keyword.frame)+5, 200, 30);
    }
    else
    {
        txt_Price_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_keyword.frame)+5, 200, 30);
    }
    txt_Price_in_meal.borderStyle = UITextBorderStyleNone;
    txt_Price_in_meal.font = [UIFont fontWithName:kFont size:13];
    txt_Price_in_meal.placeholder = @"$ Price";
    [txt_Price_in_meal setValue:[UIFont fontWithName:kFont size: 13] forKeyPath:@"_placeholderLabel.font"];
    [txt_Price_in_meal setValue:[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    txt_Price_in_meal.leftViewMode = UITextFieldViewModeAlways;
    txt_Price_in_meal.userInteractionEnabled=YES;
    txt_Price_in_meal.textAlignment = NSTextAlignmentLeft;
    txt_Price_in_meal.backgroundColor = [UIColor clearColor];
    txt_Price_in_meal.keyboardType = UIKeyboardTypeAlphabet;
    txt_Price_in_meal .backgroundColor = [UIColor clearColor];
    txt_Price_in_meal .keyboardType = UIKeyboardTypeNumberPad;
    
    txt_Price_in_meal.delegate = self;
    [img_backgroundimages_in_meal addSubview:txt_Price_in_meal];
    
    
    //self.txt_MobileNo = txt_Price_in_meal;
    UIToolbar* numberToolbar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar1.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar1.items = [NSArray arrayWithObjects:
                            
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_Done_in_priceofmeal:)],
                            nil];
    [numberToolbar1 sizeToFit];
    txt_Price_in_meal.inputAccessoryView = numberToolbar1;
    
    
    
    
    UIImageView *img_linesimge_in_meal =[[UIImageView alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        img_linesimge_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+1, WIDTH-60, 0.5);
    }
    else if (IS_IPHONE_6)
    {
        img_linesimge_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+1, WIDTH-60, 0.5);
    }
    else
    {
        img_linesimge_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_Price.frame)+1, WIDTH-40, 0.5);
        
    }
    [img_linesimge_in_meal setUserInteractionEnabled:YES];
    img_linesimge_in_meal.backgroundColor=[UIColor clearColor];
    img_linesimge_in_meal.image=[UIImage imageNamed:@"line-2.png"];
    [img_backgroundimages_in_meal addSubview:img_linesimge_in_meal];
    
    
    
    UILabel  * lbl_PerMeal_in_meal = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        lbl_PerMeal_in_meal.frame=CGRectMake(345, CGRectGetMaxY(txt_keyword.frame)+20, 50, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        lbl_PerMeal_in_meal.frame=CGRectMake(300, CGRectGetMaxY(txt_keyword.frame)+20, 50, 15);
        
    }
    else
    {
        lbl_PerMeal_in_meal.frame=CGRectMake(250, CGRectGetMaxY(txt_keyword.frame)+20, 50, 15);
        
        
    }
    lbl_PerMeal_in_meal.text = @"Per Serving";
    lbl_PerMeal_in_meal.backgroundColor=[UIColor clearColor];
    lbl_PerMeal_in_meal.textColor=[UIColor lightGrayColor];
    lbl_PerMeal_in_meal.textAlignment=NSTextAlignmentLeft;
    lbl_PerMeal_in_meal.numberOfLines=0;
    lbl_PerMeal_in_meal.font = [UIFont fontWithName:kFontBold size:9];
    [img_backgroundimages_in_meal addSubview:lbl_PerMeal_in_meal];
    
    
    
    
    
    UILabel  * lbl_Description_in_meal = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        lbl_Description_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+10, 100, 25);
        
    }
    else if (IS_IPHONE_6)
    {
        lbl_Description_in_meal.frame=CGRectMake(30, CGRectGetMaxY(txt_Price.frame)+10, 100, 25);
        
    }
    else
    {
        lbl_Description_in_meal.frame=CGRectMake(20, CGRectGetMaxY(txt_Price.frame)+10, 100, 25);
        
        
    }
    lbl_Description_in_meal.text = @"Description";
    lbl_Description_in_meal.backgroundColor=[UIColor clearColor];
    lbl_Description_in_meal.textColor=[UIColor blackColor];
    lbl_Description_in_meal.textAlignment=NSTextAlignmentLeft;
    lbl_Description_in_meal.numberOfLines=0;
    lbl_Description_in_meal.font = [UIFont fontWithName:kFont size:14];
    [img_backgroundimages_in_meal addSubview:lbl_Description_in_meal];
    
    
    
    txt_view_in_meal =[[UITextView alloc]init];
    if (IS_IPHONE_6Plus)
    {
        txt_view_in_meal.frame=CGRectMake(30, CGRectGetMaxY(lbl_Description.frame)+5, WIDTH-60, 100);
        
    }
    else if (IS_IPHONE_6)
    {
        txt_view_in_meal.frame=CGRectMake(30, CGRectGetMaxY(lbl_Description.frame)+5, WIDTH-60, 100);
        
    }
    else
    {
        txt_view_in_meal.frame=CGRectMake(20, CGRectGetMaxY(lbl_Description.frame)+5, WIDTH-40, 100);
        
        
    }
    [txt_view_in_meal setReturnKeyType:UIReturnKeyDone];
    txt_view_in_meal.scrollEnabled=YES;
    [txt_view_in_meal setDelegate:self];
    [txt_view_in_meal setReturnKeyType:UIReturnKeyDone];
    [txt_view_in_meal setFont:[UIFont fontWithName:kFont size:10]];
    [txt_view_in_meal setTextColor:[UIColor lightTextColor]];
    txt_view_in_meal.userInteractionEnabled=YES;
    txt_view_in_meal.backgroundColor=[UIColor whiteColor];
    txt_view_in_meal.delegate=self;
    txt_view_in_meal.textColor=[UIColor blackColor];
    txt_view_in_meal.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    txt_view_in_meal.layer.borderWidth=1.0f;
    txt_view_in_meal.clipsToBounds=YES;
    [img_backgroundimages_in_meal addSubview:txt_view_in_meal];
    
    
    UIToolbar *keyboard1 = nil;
    if(keyboard1 == nil)
    {
        keyboard1 = [[UIToolbar alloc] initWithFrame:CGRectMake(10, 0, self.view.bounds.size.width, 44)];
        [keyboard1 setBarStyle:UIBarStyleBlackTranslucent];
        
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(click_DoneremarksKeyboard1:)];
        [keyboard1 setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    txt_view_in_meal.inputAccessoryView = keyboard1;
    
    
    
    UILabel  * lblmax_in_meal = [[UILabel alloc]init];
    if (IS_IPHONE_6Plus)
    {
        lblmax_in_meal.frame=CGRectMake(320, CGRectGetMaxY(txt_view.frame)+1, 70, 15);
        
    }
    else if (IS_IPHONE_6)
    {
        lblmax_in_meal.frame=CGRectMake(280, CGRectGetMaxY(txt_view.frame)+1, 70, 15);
        
    }
    else
    {
        lblmax_in_meal.frame=CGRectMake(240, CGRectGetMaxY(txt_view.frame)+1, 70, 15);
        
        
    }
    
    lblmax_in_meal.text = @"(500 char. max.)";
    lblmax_in_meal.backgroundColor=[UIColor clearColor];
    lblmax_in_meal.textColor=[UIColor lightGrayColor];
    lblmax_in_meal.numberOfLines = 0;
    lblmax_in_meal.font = [UIFont fontWithName:kFont size:8];
    [img_backgroundimages_in_meal addSubview:lblmax_in_meal];
    
    
    
    
    
    
    UIButton *btn_Save = [[UIButton alloc] init];
    
    if (IS_IPHONE_6Plus)
    {
        btn_Save.frame=CGRectMake(50, HEIGHT-50, WIDTH-80, 40);
    }
    else if (IS_IPHONE_6)
    {
        btn_Save.frame=CGRectMake(50, HEIGHT-50, WIDTH-80, 40);
    }
    else
    {
        btn_Save.frame=CGRectMake(30, HEIGHT-50, WIDTH-60, 40);
        
    }
    btn_Save.backgroundColor = [UIColor clearColor];
    [btn_Save setImage:[UIImage imageNamed:@"img_savebutton.png"] forState:UIControlStateNormal];
    [btn_Save setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Save addTarget:self action:@selector(btn_save_btnClick:) forControlEvents:UIControlEventTouchUpInside ];
    [self.view addSubview:btn_Save];
    
    
    table_for_category = [[UITableView alloc] init ];
    table_for_category.frame  = CGRectMake(30,CGRectGetMaxY(txt_Category.frame)+5,WIDTH-60,200);
    if (IS_IPHONE_6Plus)
    {
        table_for_category.frame  = CGRectMake(30,CGRectGetMaxY(txt_Category.frame)+5,WIDTH-60,200);
    }
    else if (IS_IPHONE_6)
    {
        table_for_category.frame  = CGRectMake(30,CGRectGetMaxY(txt_Category.frame)+5,WIDTH-60,200);
    }
    else
    {
        table_for_category.frame  = CGRectMake(20,CGRectGetMaxY(txt_Category.frame)+5,WIDTH-40,200);
        
    }
    
    [table_for_category setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_category.delegate = self;
    table_for_category.dataSource = self;
    table_for_category.showsVerticalScrollIndicator = NO;
    table_for_category.layer.borderColor = [[UIColor blackColor]CGColor];
    table_for_category.layer.borderWidth = 1.0f;
    table_for_category.clipsToBounds = YES;
    table_for_category.backgroundColor = [UIColor whiteColor];
    [img_backgroundimages addSubview:table_for_category];
    table_for_category.hidden = YES;
    
    
    
    table_for_course = [[UITableView alloc] init ];
    table_for_course.frame  = CGRectMake(30,CGRectGetMaxY(txt_Course.frame)+5,WIDTH-60,200);
    if (IS_IPHONE_6Plus)
    {
        table_for_course.frame  = CGRectMake(30,CGRectGetMaxY(txt_Course.frame)+5,WIDTH-60,200);
    }
    else if (IS_IPHONE_6)
    {
        table_for_course.frame  = CGRectMake(30,CGRectGetMaxY(txt_Course.frame)+5,WIDTH-60,200);
    }
    else
    {
        table_for_course.frame  = CGRectMake(20,CGRectGetMaxY(txt_Course.frame)+5,WIDTH-40,200);
        
    }
    
    [table_for_course setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_course.delegate = self;
    table_for_course.dataSource = self;
    table_for_course.showsVerticalScrollIndicator = NO;
    table_for_course.layer.borderColor = [[UIColor blackColor]CGColor];
    table_for_course.layer.borderWidth = 1.0f;
    table_for_course.clipsToBounds = YES;
    table_for_course.backgroundColor = [UIColor whiteColor];
    [img_backgroundimages addSubview:table_for_course];
    table_for_course.hidden = YES;
    
    
    table_for_cuisine = [[UITableView alloc] init ];
    table_for_cuisine.frame  = CGRectMake(30,CGRectGetMaxY(txt_Cuisine.frame)+5,WIDTH-60,200);
    if (IS_IPHONE_6Plus)
    {
        table_for_cuisine.frame  = CGRectMake(30,CGRectGetMaxY(txt_Cuisine.frame)+5,WIDTH-60,200);
    }
    else if (IS_IPHONE_6)
    {
        table_for_cuisine.frame  = CGRectMake(30,CGRectGetMaxY(txt_Cuisine.frame)+5,WIDTH-60,200);
    }
    else
    {
        table_for_cuisine.frame  = CGRectMake(20,CGRectGetMaxY(txt_Cuisine.frame)+5,WIDTH-40,200);
        
    }
    
    [table_for_cuisine setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_cuisine.delegate = self;
    table_for_cuisine.dataSource = self;
    table_for_cuisine.showsVerticalScrollIndicator = NO;
    table_for_cuisine.layer.borderColor = [[UIColor blackColor]CGColor];
    table_for_cuisine.layer.borderWidth = 1.0f;
    table_for_cuisine.clipsToBounds = YES;
    table_for_cuisine.backgroundColor = [UIColor whiteColor];
    [img_backgroundimages addSubview:table_for_cuisine];
    table_for_cuisine.hidden = YES;
    
    
    table_for_category_in_meal = [[UITableView alloc] init ];
    table_for_category_in_meal.frame  = CGRectMake(30,CGRectGetMaxY(txt_Category.frame)+5,WIDTH-60,200);
    if (IS_IPHONE_6Plus)
    {
        table_for_category_in_meal.frame  = CGRectMake(30,CGRectGetMaxY(txt_Category.frame)+5,WIDTH-60,200);
    }
    else if (IS_IPHONE_6)
    {
        table_for_category_in_meal.frame  = CGRectMake(30,CGRectGetMaxY(txt_Category.frame)+5,WIDTH-60,200);
    }
    else
    {
        table_for_category_in_meal.frame  = CGRectMake(20,CGRectGetMaxY(txt_Category.frame)+5,WIDTH-40,200);
        
    }
    
    [table_for_category_in_meal setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_category_in_meal.delegate = self;
    table_for_category_in_meal.dataSource = self;
    table_for_category_in_meal.showsVerticalScrollIndicator = NO;
    table_for_category_in_meal.layer.borderColor = [[UIColor blackColor]CGColor];
    table_for_category_in_meal.layer.borderWidth = 1.0f;
    table_for_category_in_meal.clipsToBounds = YES;
    table_for_category_in_meal.backgroundColor = [UIColor whiteColor];
    [img_backgroundimages_in_meal addSubview:table_for_category_in_meal];
    table_for_category_in_meal.hidden = YES;
    
    
    table_for_course_in_meal = [[UITableView alloc] init ];
    table_for_course_in_meal.frame  = CGRectMake(30,CGRectGetMaxY(txt_Course.frame)+5,WIDTH-60,200);
    if (IS_IPHONE_6Plus)
    {
        table_for_course_in_meal.frame  = CGRectMake(30,CGRectGetMaxY(txt_Course.frame)+5,WIDTH-60,200);
    }
    else if (IS_IPHONE_6)
    {
        table_for_course_in_meal.frame  = CGRectMake(30,CGRectGetMaxY(txt_Course.frame)+5,WIDTH-60,200);
    }
    else
    {
        table_for_course_in_meal.frame  = CGRectMake(20,CGRectGetMaxY(txt_Course.frame)+5,WIDTH-40,200);
        
    }
    [table_for_course_in_meal setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_course_in_meal.delegate = self;
    table_for_course_in_meal.dataSource = self;
    table_for_course_in_meal.showsVerticalScrollIndicator = NO;
    table_for_course_in_meal.layer.borderColor = [[UIColor blackColor]CGColor];
    table_for_course_in_meal.layer.borderWidth = 1.0f;
    table_for_course_in_meal.clipsToBounds = YES;
    table_for_course_in_meal.backgroundColor = [UIColor whiteColor];
    [img_backgroundimages_in_meal addSubview:table_for_course_in_meal];
    table_for_course_in_meal.hidden = YES;
    
    table_for_cuisine_in_meal = [[UITableView alloc] init ];
    table_for_cuisine_in_meal.frame  = CGRectMake(30,CGRectGetMaxY(txt_Cuisine.frame)+5,WIDTH-60,200);
    if (IS_IPHONE_6Plus)
    {
        table_for_cuisine_in_meal.frame  = CGRectMake(30,CGRectGetMaxY(txt_Cuisine.frame)+5,WIDTH-60,200);
    }
    else if (IS_IPHONE_6)
    {
        table_for_cuisine_in_meal.frame  = CGRectMake(30,CGRectGetMaxY(txt_Cuisine.frame)+5,WIDTH-60,200);
    }
    else
    {
        table_for_cuisine_in_meal.frame  = CGRectMake(20,CGRectGetMaxY(txt_Cuisine.frame)+5,WIDTH-40,200);
        
    }
    [table_for_cuisine_in_meal setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_for_cuisine_in_meal.delegate = self;
    table_for_cuisine_in_meal.dataSource = self;
    table_for_cuisine_in_meal.showsVerticalScrollIndicator = NO;
    table_for_cuisine_in_meal.layer.borderColor = [[UIColor blackColor]CGColor];
    table_for_cuisine_in_meal.layer.borderWidth = 1.0f;
    table_for_cuisine_in_meal.clipsToBounds = YES;
    table_for_cuisine_in_meal.backgroundColor = [UIColor whiteColor];
    [img_backgroundimages_in_meal addSubview:table_for_cuisine_in_meal];
    table_for_cuisine_in_meal.hidden = YES;
    
    
    
    
    
    
    
}

#pragma tableview delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == table_for_category)
    {
        return [aryPopularlist count];
        
    }
    else if (tableView == table_for_cuisine)
    {
        return [arrCuisines count];
        
    }
    else if (tableView == table_for_course)
    {
        return [array_course_list count];
    }
    else if (tableView == table_for_category_in_meal)
    {
        return [array_categorylist count];
    }
    else if (tableView == table_for_cuisine_in_meal)
    {
        return [arrCuisineslist_in_meal count];
        
    }
    else if (tableView == table_for_course_in_meal)
    {
        return [array_course_list_in_meal count];
        
    }
    
    
    return 0;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table_for_category)
    {
        return 40;
    }
    else if (tableView == table_for_cuisine)
    {
        return 40;
    }
    else if (tableView == table_for_course)
    {
        return 40;
    }
    else if (tableView == table_for_category_in_meal)
    {
        return 40;
    }
    else if (tableView == table_for_course_in_meal)
    {
        return 40;
        
    }
    else if (tableView == table_for_cuisine_in_meal)
    {
        return 40;
        
    }
    
    
    
    return 0;
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    if (tableView == table_for_category)
    {
        UIImageView * img_bg_for_first_tbl = [[UIImageView alloc]init];
        img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
        img_bg_for_first_tbl.image=[UIImage imageNamed:@"bg-img@2x.png"];
        img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_bg_for_first_tbl setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_first_tbl];
        
        UILabel  *lbl_Restrictions = [[UILabel alloc]init];
        lbl_Restrictions.frame  =CGRectMake(5,5,146,25);
        lbl_Restrictions.text=[[aryPopularlist objectAtIndex:indexPath.row] valueForKey:@"CuisineCatName"];
        lbl_Restrictions.backgroundColor=[UIColor clearColor];
        lbl_Restrictions.textColor=[UIColor blackColor];
        lbl_Restrictions.font = [UIFont fontWithName:kFont size:12];
        [cell.contentView addSubview:lbl_Restrictions];
        
        
            if ([array_selectedcategoryid  containsObject:[aryPopularlist objectAtIndex:indexPath.row]]) {
                
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else{
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,0, WIDTH, 43);
            lbl_Restrictions.frame  =CGRectMake(5,10,146,25);
            
        }
        else if(IS_IPHONE_6)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
            lbl_Restrictions.frame  =CGRectMake(5,10,146,25);
            
        }
        else
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
            lbl_Restrictions.frame  =CGRectMake(5,5,146,25);
            
        }
        
    }
    else if (tableView == table_for_cuisine)
    {
        UIImageView * img_bg_for_first_tbl = [[UIImageView alloc]init];
        img_bg_for_first_tbl.frame =  CGRectMake(0,5,WIDTH, 40);
        img_bg_for_first_tbl.image=[UIImage imageNamed:@"bg-img@2x.png"];
        img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_bg_for_first_tbl setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_first_tbl];
        
        
        UILabel  *lbl_Restrictions = [[UILabel alloc]init];
        lbl_Restrictions.frame = CGRectMake(23,-3,150,25);
        lbl_Restrictions.text = [[arrCuisines objectAtIndex:indexPath.row] valueForKey:@"CuisineName"];
        lbl_Restrictions.backgroundColor=[UIColor clearColor];
        lbl_Restrictions.textColor=[UIColor blackColor];
        lbl_Restrictions.font = [UIFont fontWithName:kFont size:12];
        [cell.contentView addSubview:lbl_Restrictions];
        
        
        if ([array_selectedcuisines  containsObject:[arrCuisines objectAtIndex:indexPath.row]]) {
            
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,0, WIDTH, 43);
            lbl_Restrictions.frame  =CGRectMake(5,10,146,25);
            
        }
        else if(IS_IPHONE_6)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
            lbl_Restrictions.frame  =CGRectMake(5,10,146,25);
            
        }
        else
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
            lbl_Restrictions.frame  =CGRectMake(5,5,146,25);
            
        }
        
        
        
    }
    
    else if (tableView == table_for_course)
    {
        UIImageView * img_bg_for_first_tbl = [[UIImageView alloc]init];
        img_bg_for_first_tbl.frame =  CGRectMake(0,5, WIDTH, 40);
        img_bg_for_first_tbl.image=[UIImage imageNamed:@"bg-img@2x.png"];
        img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_bg_for_first_tbl setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_first_tbl];
        
        UILabel * lbl_array_course = [[UILabel alloc]init];
        lbl_array_course .frame = CGRectMake(5,10,200, 15);
        lbl_array_course .text = [[array_course_list objectAtIndex:indexPath.row] valueForKey:@"DishCourseName"];
        lbl_array_course .font = [UIFont fontWithName:kFont size:12];
        lbl_array_course .textColor = [UIColor blackColor];
        lbl_array_course .backgroundColor = [UIColor clearColor];
        [img_bg_for_first_tbl addSubview: lbl_array_course ];
        
        
        
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,0, WIDTH, 42);
            lbl_array_course.frame  =CGRectMake(10,10,146,25);
            
        }
        else if(IS_IPHONE_6)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
            lbl_array_course.frame  =CGRectMake(10,10,146,25);
        }
        else
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
            lbl_array_course.frame  =CGRectMake(10,5,146,25);
            
        }
        
        
        
        
        
    }
    else if (tableView == table_for_category_in_meal)
    {
        UIImageView * img_bg_for_first_tbl = [[UIImageView alloc]init];
        img_bg_for_first_tbl.frame =  CGRectMake(0,5, WIDTH, 40);
        img_bg_for_first_tbl.image=[UIImage imageNamed:@"bg-img@2x.png"];
        img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_bg_for_first_tbl setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_first_tbl];
        
        UILabel * lbl_array_course = [[UILabel alloc]init];
        lbl_array_course .frame = CGRectMake(5,10,200, 15);
        lbl_array_course .text = [[array_categorylist objectAtIndex:indexPath.row] valueForKey:@"CuisineCatName"];
        lbl_array_course .font = [UIFont fontWithName:kFont size:12];
        lbl_array_course .textColor = [UIColor blackColor];
        lbl_array_course .backgroundColor = [UIColor clearColor];
        [img_bg_for_first_tbl addSubview: lbl_array_course ];
        
        if ([array_selectedcategorylist  containsObject:[array_categorylist objectAtIndex:indexPath.row]]) {
            
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,0, WIDTH, 42);
            lbl_array_course.frame  =CGRectMake(10,10,146,25);
            
        }
        else if(IS_IPHONE_6)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
            lbl_array_course.frame  =CGRectMake(10,10,146,25);
            
        }
        else
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
            lbl_array_course.frame  =CGRectMake(10,5,146,25);
            
        }
        
        
        
        
    }
    else if (tableView == table_for_cuisine_in_meal)
    {
        UIImageView * img_bg_for_first_tbl = [[UIImageView alloc]init];
        img_bg_for_first_tbl.frame =  CGRectMake(0,5, WIDTH, 40);
        img_bg_for_first_tbl.image=[UIImage imageNamed:@"bg-img@2x.png"];
        img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_bg_for_first_tbl setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_first_tbl];
        
        UILabel  * lbl_Restrictions = [[UILabel alloc]init];
        lbl_Restrictions.frame = CGRectMake(23,-3,150,25);
        lbl_Restrictions.text = [[arrCuisineslist_in_meal objectAtIndex:indexPath.row] valueForKey:@"CuisineName"];
        lbl_Restrictions.backgroundColor=[UIColor clearColor];
        lbl_Restrictions.textColor=[UIColor blackColor];
        lbl_Restrictions.font = [UIFont fontWithName:kFont size:12];
        [cell.contentView addSubview:lbl_Restrictions];
        
        if ([array_selectedcuisineslist_in_meal  containsObject:[arrCuisineslist_in_meal objectAtIndex:indexPath.row]]) {
            
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,0, WIDTH, 42);
            lbl_Restrictions.frame  =CGRectMake(10,10,146,25);
            
        }
        else if(IS_IPHONE_6)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
            lbl_Restrictions.frame  =CGRectMake(10,10,146,25);
            
            
        }
        else
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
            lbl_Restrictions.frame  =CGRectMake(10,5,146,25);
            
        }
        
        
        
        
        
    }
    else if (tableView == table_for_course_in_meal)
    {
        UIImageView * img_bg_for_first_tbl = [[UIImageView alloc]init];
        img_bg_for_first_tbl.frame =  CGRectMake(0,5, WIDTH, 40);
        img_bg_for_first_tbl.image=[UIImage imageNamed:@"bg-img@2x.png"];
        img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_bg_for_first_tbl setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_first_tbl];
        
        UILabel * lbl_array_course = [[UILabel alloc]init];
        lbl_array_course .frame = CGRectMake(5,10,200, 15);
        lbl_array_course .text = [[array_course_list_in_meal objectAtIndex:indexPath.row] valueForKey:@"DishCourseName"];
        lbl_array_course .font = [UIFont fontWithName:kFont size:12];
        lbl_array_course .textColor = [UIColor blackColor];
        lbl_array_course .backgroundColor = [UIColor clearColor];
        [img_bg_for_first_tbl addSubview: lbl_array_course ];
        
        
        if (IS_IPHONE_6Plus)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,0, WIDTH, 42);
            lbl_array_course.frame  =CGRectMake(10,10,146,25);
            
        }
        else if(IS_IPHONE_6)
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
            lbl_array_course.frame  =CGRectMake(10,10,146,25);
        }
        else
        {
            img_bg_for_first_tbl.frame =  CGRectMake(-5,5, WIDTH, 43);
            lbl_array_course.frame  =CGRectMake(10,5,146,25);
            
        }
        
    }
    
    
    return cell;
    
}
#pragma table view

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == table_for_category)
    {
        
        ////Gaurav
        
        if ([array_selectedcategoryid containsObject:[aryPopularlist objectAtIndex:indexPath.row]]) {
            [array_selectedcategoryid removeObject:[aryPopularlist objectAtIndex:indexPath.row]];
        }
        else{
            if ([array_selectedcategoryid count]<5) {
               [array_selectedcategoryid addObject:[aryPopularlist objectAtIndex:indexPath.row]];
            }
            else{
                [self popup_Alertview:@"limit reached (5 items max)"];
            }
            
        }
        [table_for_category reloadData];
        txt_Category.text =[[array_selectedcategoryid valueForKey:@"CuisineCatName"] componentsJoinedByString:@", "];
        str_cataogyid  =[[array_selectedcategoryid valueForKey:@"CuisineCatID"] componentsJoinedByString:@"||"];
        //////
        
        
        str_cuisenCatId =[[aryPopularlist objectAtIndex:indexPath.row] valueForKey:@"CuisineCatID"];
        [table_for_category setHidden:YES];
        
        [self cuisineList];
        
    }
    else if (tableView == table_for_cuisine)
    {
        
        if ([array_selectedcuisines containsObject:[arrCuisines objectAtIndex:indexPath.row]]) {
            [array_selectedcuisines removeObject:[arrCuisines objectAtIndex:indexPath.row]];
        }
        else{
            if ([array_selectedcuisines count]<5) {
                [array_selectedcuisines addObject:[arrCuisines objectAtIndex:indexPath.row]];
            }
            else{
                [self popup_Alertview:@"limit reached (5 items max)"];
            }
            
        }
        [[array_selectedcategoryid valueForKey:@"CuisineCatName"] componentsJoinedByString:@", "];
        txt_Cuisine.text =[[array_selectedcuisines valueForKey:@"CuisineName"] componentsJoinedByString:@", "];
        str_cuisineid = [[array_selectedcuisines valueForKey:@"CuisineID"] componentsJoinedByString:@"||"];
        [table_for_cuisine reloadData];
        [table_for_cuisine setHidden:YES];
    }
    else if (tableView == table_for_course)
    {
        txt_Course.text =[[array_course_list objectAtIndex:indexPath.row] valueForKey:@"DishCourseName"];
        str_courseid = [[array_course_list objectAtIndex:indexPath.row] valueForKey:@"DishCourseID"];
        
        [table_for_course setHidden:YES];
        
    }
    
    else if (tableView == table_for_category_in_meal)
    {
        ////Gaurav
        
        if ([array_selectedcategorylist containsObject:[array_categorylist objectAtIndex:indexPath.row]]) {
            [array_selectedcategorylist removeObject:[array_categorylist objectAtIndex:indexPath.row]];
        }
        else{
            if ([array_selectedcategorylist count]<5) {
                [array_selectedcategorylist addObject:[array_categorylist objectAtIndex:indexPath.row]];
            }
            else{
                [self popup_Alertview:@"limit reached (5 items max)"];
            }
            
        }
        
        str_cataogyid  =[[array_selectedcategoryid valueForKey:@"CuisineCatID"] componentsJoinedByString:@"||"];
        //////

        
        txt_Category_in_meal.text =[[array_selectedcategorylist valueForKey:@"CuisineCatName"] componentsJoinedByString:@", "];
        str_categoryid_in_meal = [[array_selectedcategorylist valueForKey:@"CuisineCatID"] componentsJoinedByString:@"||"];
        str_cuisenCatId_in_meal = [[array_selectedcategorylist valueForKey:@"CuisineCatID"] componentsJoinedByString:@"||"];
        [table_for_category_in_meal reloadData];
        [table_for_category_in_meal setHidden:YES];
        str_cuisenCatId =[[array_selectedcategoryid valueForKey:@"CuisineCatID"] componentsJoinedByString:@"||"];
        
        [self cuisineList];
        
        
        
        
    }
    else if (tableView == table_for_cuisine_in_meal)
    {
        
        if ([array_selectedcuisineslist_in_meal containsObject:[arrCuisineslist_in_meal objectAtIndex:indexPath.row]]) {
            [array_selectedcuisineslist_in_meal removeObject:[arrCuisineslist_in_meal objectAtIndex:indexPath.row]];
        }
        else{
            if ([array_selectedcuisineslist_in_meal count]<5) {
                [array_selectedcuisineslist_in_meal addObject:[arrCuisineslist_in_meal objectAtIndex:indexPath.row]];
            }
            else{
                [self popup_Alertview:@"limit reached (5 items max)"];
            }
            
        }
        
        txt_Cuisine_in_meal.text =[[array_selectedcuisineslist_in_meal valueForKey:@"CuisineName"] componentsJoinedByString:@", "];
        str_cuisinId_in_meal = [[array_selectedcuisineslist_in_meal valueForKey:@"CuisineID"] componentsJoinedByString:@"||"];
        [table_for_cuisine_in_meal reloadData];
        [table_for_cuisine_in_meal setHidden:YES];
        
        
    }
    else if (tableView == table_for_course_in_meal)
    {
        txt_Course_in_meal.text =[[array_course_list_in_meal objectAtIndex:indexPath.row] valueForKey:@"DishCourseName"];
        str_courseid_in_meal = [[array_course_list_in_meal objectAtIndex:indexPath.row] valueForKey:@"DishCourseID"];
        [table_for_course_in_meal setHidden:YES];
        
    }
    
    
    
}

#pragma keyword return method

-(void)keyboard_returnmethod
{
    
    
    
    [txt_dishTitle resignFirstResponder];
    //    txt_Category
    //    txt_Cuisine
    //    txt_Course
    [txt_keyword resignFirstResponder];
    [txt_Price  resignFirstResponder];
    [txt_view resignFirstResponder];
    
    [ txt_MealTitle_in_meal resignFirstResponder];
    
    //    txt_Category_in_meal
    //    txt_Cuisine_in_meal
    //    txt_Course_in_meal
    [txt_keyword_in_meal resignFirstResponder];
    [txt_Price_in_meal resignFirstResponder];
    [txt_view_in_meal resignFirstResponder];
    
}


#pragma mark UiCollectionView Delegate Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == collView_serviceDirectory)
    {
        return [array_dietary_restryctionlist count];
        
    }
    else if (collectionView == collectionView_Keywords)
    {
        return [ary_Keywords count];
    }
    else if (collectionView == collectionView_Keywords_meal)
    {
        return [Ary_keywordmeal count];
    }
    else
    {
        return [array_dietary_restryctionlist_in_meal count];
        
    }
    return 0;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (collectionView == collView_serviceDirectory)
    {
        return 1;
        
    }
    else
    {
        return 1;
        
    }
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView1 dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews)
        [view removeFromSuperview];
    
    UIImageView *img_backGnd = [[UIImageView alloc]init];
    
    
    if (collectionView1 == collView_serviceDirectory)
    {
        if (IS_IPHONE_6Plus)
        {
            img_backGnd.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        else if (IS_IPHONE_6)
        {
            img_backGnd.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        else
        {
            img_backGnd.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        img_backGnd.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:img_backGnd];
        
        
        UIImageView *img_Images = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus){
            img_Images.frame = CGRectMake(28, 0, 55,55);
            
        }
        else if (IS_IPHONE_6){
            img_Images.frame = CGRectMake(28, 0,55,55);
            
        }
        else  {
            img_Images.frame = CGRectMake(25, 0, 40,40);
            
        }
        
        //NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@",[[array_dietary_restryctionlist objectAtIndex:indexPath.row] valueForKey:@"DietaryImage"]]];
        
        //NSData *myData = [NSData dataWithContentsOfURL:url];
        
        //img_Images.image = [UIImage imageWithData:myData];
        
        NSString *ImagePath1 =[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[[array_dietary_restryctionlist objectAtIndex:indexPath.row] valueForKey:@"DietaryImage"]]];
        [img_Images setImageWithURL:[NSURL URLWithString:ImagePath1] placeholderImage:nil];
        
        //  NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[array_dietary_restryctionlist objectAtIndex:indexPath.row] valueForKey:@"DietaryImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //[img_Images setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@""]];
        [img_Images setUserInteractionEnabled:YES];
        [img_Images setContentMode:UIViewContentModeScaleAspectFill];
        [img_Images setClipsToBounds:YES];
        [img_Images setUserInteractionEnabled:YES];
        [img_backGnd addSubview:img_Images];
        
        
        UILabel  * lbl_headings = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(img_Images.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        else if (IS_IPHONE_6)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(img_Images.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        else
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(img_Images.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        
        lbl_headings.text =[[array_dietary_restryctionlist objectAtIndex:indexPath.row] valueForKey:@"DietaryName"];
        
        lbl_headings.backgroundColor=[UIColor clearColor];
        lbl_headings.textColor=[UIColor blackColor];
        lbl_headings.textAlignment=NSTextAlignmentCenter;
        lbl_headings.font = [UIFont fontWithName:kFont size:9];
        [img_backGnd addSubview:lbl_headings];
        
        
        UIButton *btn_incell_tikmark = [[UIButton alloc]init];
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 6,((WIDTH-65)/3),75);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 19,((WIDTH-60)/3),53);
        }
        else
        {
            btn_incell_tikmark.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        btn_incell_tikmark.backgroundColor=[UIColor clearColor];
        btn_incell_tikmark.layer.cornerRadius = 7.0;
        btn_incell_tikmark.layer.borderColor = [[UIColor whiteColor] CGColor];
        if (IS_IPHONE_6Plus)
        {
            //btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,30,((WIDTH-65)/4),0);
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        
        
        /*UIButton *btn_incell_tikmark = [[UIButton alloc]init];
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 10,((WIDTH-65)/3),70);
        }
        else
        {
            btn_incell_tikmark.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        btn_incell_tikmark.backgroundColor=[UIColor clearColor];
        btn_incell_tikmark.layer.cornerRadius = 7.0;
        btn_incell_tikmark.layer.borderColor = [[UIColor whiteColor] CGColor];
        //   btn_TableCell_CheckBox1.layer.borderWidth = 1.0;
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,30,((WIDTH-65)/3)-20,0);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,30,((WIDTH-65)/3)-20,0);
        }*/
        //btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,20,40,-40);
        //{top, left, bottom, right};
        //  btn_TableCell_CheckBox1.titleEdgeInsets = UIEdgeInsetsMake(0,-10,4.5,0);
        
        [btn_incell_tikmark addTarget:self action:@selector(click_selectObjectAt:) forControlEvents:UIControlEventTouchUpInside];
        
        if([Arr_temp containsObject:[array_dietary_restryctionlist objectAtIndex:indexPath.row]])
        {
            [btn_incell_tikmark setImage:[UIImage imageNamed:@"img_correct@2x.png"] forState:UIControlStateSelected];
            [btn_incell_tikmark setImage:[UIImage imageNamed:@"img_correct@2x.png"] forState:UIControlStateNormal];
        }
        else{
            [btn_incell_tikmark setImage:[UIImage imageNamed:@""] forState:UIControlStateSelected];
            [btn_incell_tikmark setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        }
        
        
        btn_incell_tikmark.tag = indexPath.row;
        //  [cell.contentView addSubview:btn_incell_tikmark];
        
        
       
        [cell.contentView addSubview:btn_incell_tikmark];
        
        //
        //        int selectedindex=(int)indexPath.row;
        //        NSLog(@"%d",selectedindex);
        
        
        //        if([Arr_temp containsObject:[array_dietary_restryctionlist objectAtIndex:indexPath.row]])
        //        {
        //            [btn_incell_tikmark setSelected:YES];
        //        }
        //        else{
        //            [btn_incell_tikmark setSelected:NO];
        //        }
        
        
    }
    else if (collectionView1 == collectionView_Keywords_meal)
    {
        CGRect sizeRect = [[ary_Keywords  objectAtIndex:indexPath.row] boundingRectWithSize:CGSizeMake(2000, 25)
                                                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                                                 attributes:@{NSFontAttributeName:[UIFont fontWithName:kFont size:11.0f]}
                                                                                    context:nil];
        
        UILabel *lbl_Keyword = [[UILabel alloc]initWithFrame:CGRectMake(2, 5, sizeRect.size.width+8, 25)];
        lbl_Keyword.text = [ary_Keywords  objectAtIndex:indexPath.row];
        lbl_Keyword.layer.cornerRadius = 4.0f;
        lbl_Keyword.clipsToBounds = YES;
        lbl_Keyword.font = [UIFont fontWithName:kFont size:11];
        lbl_Keyword.textAlignment = NSTextAlignmentCenter;
        lbl_Keyword.textColor = [UIColor whiteColor];
        lbl_Keyword.backgroundColor = [UIColor colorWithRed:20/255.0f green:58/255.0f blue:102/255.0f alpha:1];
        [cell.contentView addSubview:lbl_Keyword];
        
        
    }
    else if (collectionView1 ==collectionView_Keywords)
    {
        
        CGRect sizeRect = [[ary_Keywords  objectAtIndex:indexPath.row] boundingRectWithSize:CGSizeMake(2000, 25)
                                                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                                                 attributes:@{NSFontAttributeName:[UIFont fontWithName:kFont size:11.0f]}
                                                                                    context:nil];
        
        UILabel *lbl_Keyword = [[UILabel alloc]initWithFrame:CGRectMake(2, 5, sizeRect.size.width+8, 25)];
        lbl_Keyword.text = [ary_Keywords  objectAtIndex:indexPath.row];
        lbl_Keyword.layer.cornerRadius = 4.0f;
        lbl_Keyword.clipsToBounds = YES;
        lbl_Keyword.font = [UIFont fontWithName:kFont size:11];
        lbl_Keyword.textAlignment = NSTextAlignmentCenter;
        lbl_Keyword.textColor = [UIColor whiteColor];
        lbl_Keyword.backgroundColor = [UIColor colorWithRed:20/255.0f green:58/255.0f blue:102/255.0f alpha:1];
        [cell.contentView addSubview:lbl_Keyword];
        
    }
    else
    {
        if (IS_IPHONE_6Plus)
        {
            img_backGnd.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        else if (IS_IPHONE_6)
        {
            img_backGnd.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        else
        {
            img_backGnd.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        img_backGnd.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:img_backGnd];
        
        
        UIImageView *img_Images = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus){
            img_Images.frame = CGRectMake(28, 0, 55,55);
            
        }
        else if (IS_IPHONE_6){
            img_Images.frame = CGRectMake(28, 0,55,55);
            
        }
        else  {
            img_Images.frame = CGRectMake(25, 0, 40,40);
            
        }
        
        //        [img_Images setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[collectionarrImages_in_meal objectAtIndex:indexPath.row]]]];
        //        [img_Images setUserInteractionEnabled:YES];
        //        [img_Images setContentMode:UIViewContentModeScaleAspectFill];
        //        [img_Images setClipsToBounds:YES];
        //        [img_Images setUserInteractionEnabled:YES];
        //        [img_backGnd addSubview:img_Images];
        
        
        NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@",[[array_dietary_restryctionlist_in_meal objectAtIndex:indexPath.row] valueForKey:@"DietaryImage"]]];
        
        NSData *myData = [NSData dataWithContentsOfURL:url];
        
        img_Images.image = [UIImage imageWithData:myData];
        // NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[array_dietary_restryctionlist_in_meal objectAtIndex:indexPath.row] valueForKey:@"DietaryImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        // [img_Images setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@""]];
        [img_Images setUserInteractionEnabled:YES];
        [img_Images setContentMode:UIViewContentModeScaleAspectFill];
        [img_Images setClipsToBounds:YES];
        [img_Images setUserInteractionEnabled:YES];
        [img_backGnd addSubview:img_Images];
        
        
        
        
        UILabel  * lbl_headings = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(img_Images.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        else if (IS_IPHONE_6)
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(img_Images.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        else
        {
            lbl_headings.frame =CGRectMake(5,CGRectGetMaxY(img_Images.frame)+1, ((WIDTH-65)/3)-10,10);
            
        }
        
        if (indexPath.row == 0)
        {
            lbl_headings.text = @"Halal";
        }
        else if (indexPath.row==1)
        {
            lbl_headings.text=@"Kasher";
        }
        else
        {
            lbl_headings.text = @"Organic";
        }
        lbl_headings.backgroundColor=[UIColor clearColor];
        lbl_headings.textColor=[UIColor blackColor];
        lbl_headings.textAlignment=NSTextAlignmentCenter;
        lbl_headings.font = [UIFont fontWithName:kFont size:9];
        [img_backGnd addSubview:lbl_headings];
        
        
        
        UIButton *btn_incell_tikmark = [[UIButton alloc]init];
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.frame = CGRectMake(0, 5,((WIDTH-65)/3),70);
        }
        else
        {
            btn_incell_tikmark.frame = CGRectMake(0, 0,((WIDTH-65)/3),70);
        }
        btn_incell_tikmark.backgroundColor=[UIColor clearColor];
        btn_incell_tikmark.layer.cornerRadius = 7.0;
        btn_incell_tikmark.layer.borderColor = [[UIColor whiteColor] CGColor];
        //   btn_TableCell_CheckBox1.layer.borderWidth = 1.0;
        if (IS_IPHONE_6Plus)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,30,((WIDTH-65)/3)-20,0);
        }
        else if (IS_IPHONE_6)
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(20,20,10,10);
        }
        else
        {
            btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,30,((WIDTH-65)/3)-20,0);
        }
        //btn_incell_tikmark.imageEdgeInsets = UIEdgeInsetsMake(30,20,40,-40);
        //{top, left, bottom, right};
        //  btn_TableCell_CheckBox1.titleEdgeInsets = UIEdgeInsetsMake(0,-10,4.5,0);
        
        [btn_incell_tikmark addTarget:self action:@selector(click_selectObjectAt_in_meal:) forControlEvents:UIControlEventTouchUpInside];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@"img_correct@2x.png"] forState:UIControlStateSelected];
        [btn_incell_tikmark setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        btn_incell_tikmark.tag = indexPath.row;
        [cell.contentView addSubview:btn_incell_tikmark];
        
        
        if ([Arr_temp_in_meal containsObject:[array_dietary_restryctionlist_in_meal objectAtIndex:indexPath.row]])
        {
            [btn_incell_tikmark setSelected:YES];
        }
        else
        {
            [btn_incell_tikmark setSelected:NO];
            
        }
        btn_incell_tikmark.tag = indexPath.row;
        [cell.contentView addSubview:btn_incell_tikmark];
        
        
        if([Arr_temp_in_meal containsObject:[array_dietary_restryctionlist_in_meal objectAtIndex:indexPath.row]])
        {
            [btn_incell_tikmark setSelected:YES];
        }
        else{
            [btn_incell_tikmark setSelected:NO];
        }
        
    }
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == collView_serviceDirectory)
    {
        if (IS_IPHONE_6Plus)
        {
            return CGSizeMake((WIDTH-46)/3, 70);
        }
        else if (IS_IPHONE_6)
        {
            return CGSizeMake((WIDTH-46)/3, 70);
        }
        else
        {
            return CGSizeMake((WIDTH-46)/3, 60);
        }
    }
    else if (collectionView ==collectionView_Keywords)
    {
        CGRect sizeRect = [[ary_Keywords  objectAtIndex:indexPath.row] boundingRectWithSize:CGSizeMake(2000, 25)
                                                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                                                 attributes:@{NSFontAttributeName:[UIFont fontWithName:kFont size:11.0f]}
                                                                                    context:nil];
        
        
        return CGSizeMake(sizeRect.size.width+12, 35);
        
        
    }
    else if (collectionView == collectionView_Keywords_meal)
    {
        CGRect sizeRect = [[ary_Keywords  objectAtIndex:indexPath.row] boundingRectWithSize:CGSizeMake(2000, 25)
                                                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                                                 attributes:@{NSFontAttributeName:[UIFont fontWithName:kFont size:11.0f]}
                                                                                    context:nil];
        
        
        return CGSizeMake(sizeRect.size.width+12, 35);
    }
    else
    {
        if (IS_IPHONE_6Plus)
        {
            return CGSizeMake((WIDTH-46)/3, 70);
        }
        else if (IS_IPHONE_6)
        {
            return CGSizeMake((WIDTH-46)/3, 70);
        }
        else
        {
            return CGSizeMake((WIDTH-46)/3, 60);
        }
        
    }
    
    return CGSizeMake(0, 0);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == collectionView_Keywords)
    {
        int_SelectedKeyword = (int)indexPath.row;
        
        [self popup_Alertviewwithboth:@"Do you want to delete this Keyword?"];
        
    }
    else
    {
        
    }
    
}

-(void)popup_Alertviewwithboth:(NSString *)message
{
    [deletePopUpBg removeFromSuperview];
    deletePopUpBg=[[UIView alloc] init];
    deletePopUpBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    deletePopUpBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    deletePopUpBg.userInteractionEnabled=TRUE;
    [self.view addSubview:deletePopUpBg];
    
    
    UIImageView *img_bg_DeletePopUP = [[UIImageView alloc]init];
    img_bg_DeletePopUP.frame = CGRectMake(5,(HEIGHT-200)/2.0, WIDTH-10, 200);
    [img_bg_DeletePopUP  setImage:[UIImage imageNamed:@"img-bg@2x-1"]];
    [img_bg_DeletePopUP  setUserInteractionEnabled:YES];
    [deletePopUpBg addSubview:img_bg_DeletePopUP];
    //    img_bg_DeletePopUP.hidden = YES;
    
    
    UIButton   *btn_Cross = [[UIButton alloc] init];
    btn_Cross.frame = CGRectMake(WIDTH-41, 10, 15, 15);
    [btn_Cross setBackgroundColor:[UIColor clearColor]];
    [btn_Cross setImage:[UIImage imageNamed: @"icon-del"] forState:UIControlStateNormal];
    [btn_Cross addTarget:self action:@selector(btn_Cross_Method:) forControlEvents:UIControlEventTouchUpInside ];
    [img_bg_DeletePopUP addSubview:btn_Cross];
    
    
    UILabel *labl_Alert = [[UILabel alloc]init];
    labl_Alert.frame = CGRectMake(20,CGRectGetMaxY(btn_Cross.frame)+3, WIDTH-60,60);
    labl_Alert.text =message;
    //    labl_Alert.text = [NSString stringWithFormat:@"%@",[array_delivery_company_name objectAtIndex:indexPath.row]];
    labl_Alert.font = [UIFont fontWithName:kFontBold size:13];
    labl_Alert.textColor = [UIColor blackColor];
    labl_Alert. lineBreakMode = NSLineBreakByWordWrapping;
    labl_Alert. numberOfLines = 1.0;
    labl_Alert.backgroundColor = [UIColor clearColor];
    [img_bg_DeletePopUP addSubview:labl_Alert];
    
    
    
    UIButton   *btn_Yes = [[UIButton alloc] init];
    btn_Yes.frame = CGRectMake(30, CGRectGetMaxY(labl_Alert.frame)+15, (WIDTH-90)/2.0, 35);
    btn_Yes.layer.cornerRadius=4.0f;
    [btn_Yes setTitle:@"YES" forState:UIControlStateNormal];
    [btn_Yes setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Yes setBackgroundColor:[UIColor colorWithRed:152.0/255.0 green:0.0/255.0 blue:34.0/255.0 alpha:1.0f]];
    btn_Yes.titleLabel.font=[UIFont fontWithName:kFont size:16];
    [btn_Yes setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_Yes addTarget:self action:@selector(btn_DeleteCellYes_Method:) forControlEvents:UIControlEventTouchUpInside ];
    btn_Yes.tag = selectedindex;
    [img_bg_DeletePopUP addSubview:btn_Yes];
    
    
    UIButton   *btn_No = [[UIButton alloc] init];
    btn_No.frame = CGRectMake(CGRectGetMaxX(btn_Yes.frame)+10, CGRectGetMaxY(labl_Alert.frame)+15, (WIDTH-90)/2.0, 35);
    btn_No.layer.cornerRadius=4.0f;
    [btn_No setTitle:@"NO" forState:UIControlStateNormal];
    [btn_No setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_No setBackgroundColor:[UIColor colorWithRed:39.0/255.0 green:37.0/255.0 blue:48.0/255.0 alpha:1.0f]];
    btn_No.titleLabel.font=[UIFont fontWithName:kFont size:16];
    [btn_No setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn_No addTarget:self action:@selector(btn_No_Method:) forControlEvents:UIControlEventTouchUpInside ];
    [img_bg_DeletePopUP addSubview:btn_No];
    
}


-(void)btn_DeleteCellYes_Method:(UIButton *)sender
{
    [deletePopUpBg removeFromSuperview];
    
    [ary_Keywords removeObjectAtIndex:int_SelectedKeyword];
    
    if ([ary_Keywords count]==0)
    {
        txt_keyword.placeholder = @"Keywords (eg: bags,watch)";
    }
    
    if ([ary_Keywords count]*67 >= 200)
    {
        collectionView_Keywords.frame = CGRectMake(10,0 ,200,35);
    }
    else
    {
        collectionView_Keywords.frame = CGRectMake(10,0 ,[ary_Keywords count]*67,35);
    }
    
    [collectionView_Keywords reloadData];
    [deletePopUpBg removeFromSuperview];
    //    [self AFDeleteDishFromSchedule];
    
}

-(void)btn_No_Method:(UIButton *)sender
{
    NSLog(@"btn_No_Method clicked");
    [deletePopUpBg removeFromSuperview];
    
}
-(void)btn_Cross_Method:(UIButton *)sender
{
    NSLog(@"btn_Cross_Method clicked ");
    [deletePopUpBg removeFromSuperview] ;
    
}

-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFontBold size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}
-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}

#pragma click_events

-(void)btn_save_btnClick:(UIButton *)sender
{
    
    
    if ([str_selecte_dish_r_meal isEqualToString:@"Dish"])
    {
        if ([Arr_temp count] == 0 )
        {
            
            [self popup_Alertview:@"please select dietary restriction"];
            
        }
        
        else  if (imgData == nil)
        {
            [self popup_Alertview:@"please select dish image"];
        }
        
        
        else if(txt_dishTitle.text.length == 0)
        {
            
            [self popup_Alertview:@"please enter Dish Title"];
            
        }
        else if(str_cataogyid.length == 0)
        {
            
            [self popup_Alertview:@"please select category"];
            
        }
        
        else if(str_cuisineid.length  == 0)
        {
            
            [self popup_Alertview:@"please select Cuisines"];
            
        }
        
        else if(str_courseid.length  == 0)
        {
            
            [self popup_Alertview:@"please select courses"];
            
        }
        
        
        else if ([ary_Keywords count] == 0)
        {
            [self popup_Alertview:@"Please enter keywords."];
        }
        
        else if ([ary_Keywords count] > 5)
        {
            [self popup_Alertview:@"Maximum 5 keywords allwoed."];
        }
        
        else if(txt_Price.text.length == 0)
        {
            
            [self popup_Alertview:@"please enter price"];
            
        }
        
        else if(txt_view.text.length == 0)
        {
            
            [self popup_Alertview:@"please write description"];
            
        }
        else if(txt_view.text.length > 500)
        {
            
            [self popup_Alertview:@"please write description within 500 characters"];
            
        }
        else
        {
            
            [self AFforSingleDish_r_Meal];
            //     MenuScreenEditMealVC *menuScreenEdit = [[MenuScreenEditMealVC alloc]init];
            //     [self presentViewController:menuScreenEdit animated:NO completion:nil];
        }
    }
    else
    {
        if ([Arr_temp_in_meal count] == 0 )
        {
            
            [self popup_Alertview:@"please select dietary restriction"];
            
        }
        
        else if (imgData_in_meal == nil)
        {
            [self popup_Alertview:@"please select meal image"];
        }
        else if(txt_MealTitle_in_meal.text.length == 0)
        {
            
            [self popup_Alertview:@"please enter Meal Title"];
            
        }
        else if(str_categoryid_in_meal  == 0)
        {
            
            [self popup_Alertview:@"please select category"];
            
        }
        else if(str_cuisinId_in_meal  == 0)
        {
            
            [self popup_Alertview:@"please select Cuisines"];
            
        }
        else if(str_courseid_in_meal  == 0)
        {
            
            [self popup_Alertview:@"please select courses"];
            
        }
        else if ([Ary_keywordmeal count] == 0)
        {
            [self popup_Alertview:@"Please enter keywords."];
        }
        
        else if(txt_Price_in_meal.text.length == 0)
        {
            
            [self popup_Alertview:@"please enter price"];
            
        }
        else if(txt_view_in_meal.text.length == 0)
        {
            
            [self popup_Alertview:@"please write description"];
            
        }
        
        else
        {
            [self AFforSingleDish_r_Meal];
            //        MenuScreenEditMealVC *menuScreenEdit = [[MenuScreenEditMealVC alloc]init];
            //        [self presentViewController:menuScreenEdit animated:NO completion:nil];
            
        }
        
    }
    
    
    //    [self.navigationController pushViewController:menuScreenEdit animated:NO];
    
}





-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}
#pragma mark - ACTION SHEET DELEGATE

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 0)
    {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            UIImagePickerController*imgPicker = [[UIImagePickerController alloc] init];
            UIColor* color = [UIColor colorWithRed:46.0/255 green:127.0/255 blue:244.0/255 alpha:1];
            [imgPicker.navigationBar setTintColor:color];
            imgPicker.delegate = self;
            imgPicker.allowsEditing = YES;
            imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            [self presentViewController:imgPicker animated:NO completion:Nil];
        }
    }
    else if(buttonIndex == 1)
    {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController*imgPicker = [[UIImagePickerController alloc] init];
            UIColor* color = [UIColor colorWithRed:46.0/255 green:127.0/255 blue:244.0/255 alpha:1];
            [imgPicker.navigationBar setTintColor:color];
            imgPicker.delegate = self;
            imgPicker.allowsEditing = YES;
            imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:imgPicker animated:NO completion:Nil];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Not86" message:@"Device Does Not Support Camera" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
}


#pragma mark - IMAGEPICKER DELEGATE METHODS
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *imageOriginal =  [info objectForKey:UIImagePickerControllerEditedImage];
    if ([selected_img isEqualToString:@"single_dish_image"])
    {
        imgData =  UIImageJPEGRepresentation(imageOriginal,0.5);
        img_backround.image = imageOriginal;
    }
    else
    {
        
        imgData_in_meal =  UIImageJPEGRepresentation(imageOriginal,0.5);
        img_backround_in_meal.image = imageOriginal;
        
    }
    
    
    
    [picker dismissViewControllerAnimated:YES completion:Nil];
    
}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:Nil];
}
#pragma mark TextField Delegate methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == txt_keyword)
    {
        
        [collectionView_Keywords setHidden:YES];
        
        NSString *str_Keywords = @"";
        
        for (int i=0; i<[ary_Keywords count]; i++)
        {
            str_Keywords = [NSString stringWithFormat:@"%@,%@",str_Keywords,[ary_Keywords objectAtIndex:i]];
        }
        
        if ([str_Keywords hasPrefix:@","])
        {
            str_Keywords = [str_Keywords substringFromIndex:1];
        }
        if ([str_Keywords hasSuffix:@","])
        {
            str_Keywords = [str_Keywords substringToIndex:[str_Keywords length]-1];
        }
        
        txt_keyword.text = str_Keywords;
        
    }
    else if (textField == txt_keyword_in_meal)
    {
        [collectionView_Keywords_meal setHidden:YES];
        
        NSString *str_Keywords = @"";
        
        for (int i=0; i<[Ary_keywordmeal count]; i++)
        {
            str_Keywords = [NSString stringWithFormat:@"%@,%@",str_Keywords,[Ary_keywordmeal objectAtIndex:i]];
        }
        
        if ([str_Keywords hasPrefix:@","])
        {
            str_Keywords = [str_Keywords substringFromIndex:1];
        }
        if ([str_Keywords hasSuffix:@","])
        {
            str_Keywords = [str_Keywords substringToIndex:[str_Keywords length]-1];
        }
        
        txt_keyword_in_meal.text = str_Keywords;
        
        
    }
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == txt_keyword)
    {
        NSString *str_Keywords = txt_keyword.text;
        
        if ([str_Keywords hasPrefix:@","])
        {
            str_Keywords = [str_Keywords substringFromIndex:1];
        }
        if ([str_Keywords hasSuffix:@","])
        {
            str_Keywords = [str_Keywords substringToIndex:[str_Keywords length]-1];
        }
        
        
        txt_keyword.text = str_Keywords;
        
        NSArray *ary_TempKeyword = [[NSArray alloc]init];
        
        if ([str_Keywords length]>0)
        {
            ary_TempKeyword = [str_Keywords componentsSeparatedByString:@","];
        }
        
        //        if ([ary_TempKeyword count]<6 && [ary_TempKeyword count]>0)
        //        {
        [ary_Keywords removeAllObjects];
        
        int total = (int)[ary_TempKeyword count];
        
        if ([ary_TempKeyword count]>5)
        {
            total = 5;
        }
        for (int i=0; i<total; i++)
        {
            [ary_Keywords addObject:[ary_TempKeyword objectAtIndex:i]];
        }
        //    }
        
        
        
        if ([ary_Keywords count]>0)
        {
            txt_keyword.text = @"";
            txt_keyword.placeholder = @"";
            
            if ([ary_TempKeyword count] > 5)
            {
                [self popup_Alertview:@"You can enter max 5 keywords."];
            }
            
            
            int width_CollectionView = 0;
            for (int i=0; i<[ary_Keywords count]; i++)
            {
                CGRect sizeRect = [[ary_Keywords  objectAtIndex:i] boundingRectWithSize:CGSizeMake(2000, 25)
                                                                                options:NSStringDrawingUsesLineFragmentOrigin
                                                                             attributes:@{NSFontAttributeName:[UIFont fontWithName:kFont size:11.0f]}
                                                                                context:nil];
                
                
                
                width_CollectionView = width_CollectionView + sizeRect.size.width + 12;
                
                
            }
            
            if (width_CollectionView >= 275)
            {
                collectionView_Keywords.frame = CGRectMake(10,0 ,200,35);
            }
            else
            {
                collectionView_Keywords.frame = CGRectMake(10,0 ,width_CollectionView,35);
            }
            
            [collectionView_Keywords setHidden:NO];
            [collectionView_Keywords reloadData];
            //   }
            
        }
        else
        {
            txt_keyword.placeholder = @"Keywords (eg: Tacos,fries)";
            [collectionView_Keywords setHidden:YES];
        }
        
    }
    else if (textField ==txt_keyword_in_meal)
    {
        
        NSString *str_Keywords = txt_keyword_in_meal.text;
        
        if ([str_Keywords hasPrefix:@","])
        {
            str_Keywords = [str_Keywords substringFromIndex:1];
        }
        if ([str_Keywords hasSuffix:@","])
        {
            str_Keywords = [str_Keywords substringToIndex:[str_Keywords length]-1];
        }
        
        
        txt_keyword_in_meal.text = str_Keywords;
        
        NSArray *ary_TempKeyword = [[NSArray alloc]init];
        
        if ([str_Keywords length]>0)
        {
            ary_TempKeyword = [str_Keywords componentsSeparatedByString:@","];
        }
        
        //        if ([ary_TempKeyword count]<6 && [ary_TempKeyword count]>0)
        //        {
        [Ary_keywordmeal removeAllObjects];
        
        int total = (int)[ary_TempKeyword count];
        
        if ([ary_TempKeyword count]>5)
        {
            total = 5;
        }
        for (int i=0; i<total; i++)
        {
            [Ary_keywordmeal addObject:[ary_TempKeyword objectAtIndex:i]];
        }
        //    }
        
        
        
        if ([Ary_keywordmeal count]>0)
        {
            txt_keyword_in_meal.text = @"";
            txt_keyword_in_meal.placeholder = @"";
            
            if ([ary_TempKeyword count] > 5)
            {
                [self popup_Alertview:@"You can enter max 5 keywords."];
            }
            
            
            int width_CollectionView = 0;
            for (int i=0; i<[Ary_keywordmeal count]; i++)
            {
                CGRect sizeRect = [[Ary_keywordmeal  objectAtIndex:i] boundingRectWithSize:CGSizeMake(2000, 25)
                                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                                attributes:@{NSFontAttributeName:[UIFont fontWithName:kFont size:11.0f]}
                                                                                   context:nil];
                
                
                
                width_CollectionView = width_CollectionView + sizeRect.size.width + 12;
                
                
            }
            
            if (width_CollectionView >= 275)
            {
                collectionView_Keywords_meal.frame = CGRectMake(10,0 ,200,35);
            }
            else
            {
                collectionView_Keywords_meal.frame = CGRectMake(10,0 ,width_CollectionView,35);
            }
            
            [collectionView_Keywords_meal setHidden:NO];
            [collectionView_Keywords_meal reloadData];
            //   }
            
        }
        else
        {
            txt_keyword_in_meal.placeholder = @"Keywords (eg: Tacos,fries)";
            [collectionView_Keywords_meal setHidden:YES];
        }
        
        
    }
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == txt_keyword||textField ==txt_keyword_in_meal)
    {
        if ([string isEqualToString:@" "])
        {
            return NO;
        }
    }
    
    if (textField == txt_MealTitle_in_meal || textField == txt_dishTitle) {
        if (textField.text.length >= 50 && range.length == 0)
        {
            return NO; // return NO to not change text
        }
    }
    return YES;
}


#pragma click_events

-(void)Back_btnClick:(UIButton *)sender
{
    
    
    //[self.navigationController popViewController:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
    
    
}

-(void)click_DoneremarksKeyboard:(UIButton *)sender
{
    [txt_view resignFirstResponder];
}

-(void)click_DoneremarksKeyboard1:(UIButton *)sender
{
    [txt_view_in_meal resignFirstResponder];
}

-(void)click_Done_in_priceofmeal:(UIButton *)sender
{
    [txt_Price_in_meal resignFirstResponder];
}


-(void)click_Done_in_priceofdish:(UIButton *)sender
{
    [txt_Price resignFirstResponder];
}




-(void)click_on_left_arrow:(UIButton *)sender
{
    NSLog(@"click_on_left_arrow");
}


-(void)click_on_singledish:(UIButton *)sender
{
    NSLog(@"click_on_singledish_meal");
    btn_on_singledish.hidden = YES;
    black_stripimg.hidden = NO;
    lbl_single_dish.hidden = NO;
    btn_on_Meal.hidden = NO;
    black_stripimg2.hidden = YES;
    lbl_meal.hidden = YES;
    view_for_dish.hidden = NO;
    view_for_meal.hidden = YES;
    
    str_selecte_dish_r_meal = @"Dish";
    
    
    
    
}
-(void)click_on_meal_btn:(UIButton *)sender
{
    str_selecte_dish_r_meal = @"Meal";
    
    NSLog(@"click_on_singledish_meal");
    black_stripimg.hidden = YES;
    lbl_single_dish.hidden = YES;
    btn_on_singledish.hidden = NO;
    btn_on_Meal.hidden = YES;
    black_stripimg2.hidden = NO;
    lbl_meal.hidden = NO;
    
    view_for_dish.hidden = YES;
    view_for_meal.hidden = NO;
    
    
    
    
}
-(void)btn_edite_click:(UIButton *)sender
{
    selected_img = @"single_dish_image";
    NSLog(@"btn_edite_click:");
    
    UIActionSheet *actionSheet_popupQuery = [[UIActionSheet alloc] initWithTitle:@"Select A Image From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Camera", nil];
    actionSheet_popupQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet_popupQuery showInView:self.view];
    
    
}

-(void)btn_edite_in_meal_click:(UIButton *)sender
{
    NSLog(@"btn_edite_in_meal_click");
    selected_img = @"meal_image";
    
    UIActionSheet *actionSheet_popupQuery = [[UIActionSheet alloc] initWithTitle:@"Select A Image From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library", @"Camera", nil];
    actionSheet_popupQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet_popupQuery showInView:self.view];
    
    
    
    
}
-(void)btn_category_click:(UIButton *)sender
{
    
    [self keyboard_returnmethod];
    NSLog(@"btn_category_click");
    table_for_category.hidden = NO;
    table_for_cuisine.hidden = YES;
    table_for_course.hidden = YES;
}
-(void)btn_cuisine_click:(UIButton *)sender
{
    
    [self keyboard_returnmethod];
    NSLog(@"btn_cuisine_click");
    table_for_category.hidden = YES;
    table_for_cuisine.hidden = NO;
    table_for_course.hidden = YES;
}
-(void)btn_course_click:(UIButton *)sender
{
    
    [self keyboard_returnmethod];
    NSLog(@"btn_course_click");
    table_for_category.hidden = YES;
    table_for_cuisine.hidden = YES;
    table_for_course.hidden = NO;
}
-(void)btn_category_in_meal_click:(UIButton *)sender
{
    NSLog(@"btn_category_in_meal_click");
    table_for_category_in_meal.hidden = NO;
    table_for_cuisine_in_meal.hidden = YES;
    table_for_course_in_meal.hidden = YES;
    
}
-(void)btn_cuisine_in_meal_click:(UIButton *)sender
{
    
    [self keyboard_returnmethod];
    NSLog(@"btn_cuisine_in_meal_click");
    table_for_category_in_meal.hidden = YES;
    table_for_cuisine_in_meal.hidden = NO;
    table_for_course_in_meal.hidden = YES;
    
}
-(void)btn_course_in_meal_click:(UIButton *)sender
{
    
    [self keyboard_returnmethod];
    NSLog(@"btn_course_in_meal_click");
    table_for_category_in_meal.hidden = YES;
    table_for_cuisine_in_meal.hidden = YES;
    table_for_course_in_meal.hidden = NO;
    
}

-(void) click_selectObjectAt:(UIButton *) sender
{
    
    selectedindex= (int)sender.tag;
    
    
    if (sender.tag == selectedindex)
    {
        indexSelected = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        
    }
    
    
    
    if([Arr_temp containsObject:[array_dietary_restryctionlist objectAtIndex:sender.tag]])
    {
        [Arr_temp removeObject:[array_dietary_restryctionlist objectAtIndex:sender.tag]];
    }
    else
    {
        [Arr_temp addObject:[array_dietary_restryctionlist objectAtIndex:sender.tag]];
    }
    
    //[collectionView_FashionCategory reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:sender.tag inSection:0]]  withRowAnimation:UITableViewRowAnimationFade];
    [collView_serviceDirectory reloadData];
    //
    
}
-(void)click_selectObjectAt_in_meal:(UIButton *)sender
{
    selectedindex= (int)sender.tag;
    
    
    if (sender.tag == selectedindex)
    {
        indexSelected = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        
    }
    
    
    
    if([Arr_temp_in_meal containsObject:[array_dietary_restryctionlist objectAtIndex:sender.tag]])
    {
        [Arr_temp_in_meal removeObject:[array_dietary_restryctionlist objectAtIndex:sender.tag]];
    }
    else
    {
        [Arr_temp_in_meal addObject:[array_dietary_restryctionlist objectAtIndex:sender.tag]];
    }
    
    //[collectionView_FashionCategory reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:sender.tag inSection:0]]  withRowAnimation:UITableViewRowAnimationFade];
    [collView_serviceDirectory_for_meal reloadData];
    //
    
}






#pragma textview delegates

#pragma mark Textview Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if (textView == txt_view || textView == txt_view_in_meal) {
        
        if (textView.text.length >= 500 && range.length == 0)
        {
            return NO; // return NO to not change text
        }

    }
    
    return YES;
}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    if([textView isEqual:txt_view])
    {
        
    }
    
    CGRect textFieldRect = [self.view.window convertRect:textView.bounds fromView:textView];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    
}


- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    
    
    if([textView isEqual:txt_view])
    {
        if (textView.text.length==0)
        {
            
        }
    }
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    
}


- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    
}



#pragma mark - method SingleDish_r_meal

-(void)AFforSingleDish_r_Meal
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //    NSString *firstName = self.txt_FirstName.text;
    //    NSString *middleName = self.txt_MiddleName.text;
    //    NSString *lastName = self.txt_LastName.text;
    //    NSString *mobileCodeNo = str_countrycodetosend;
    //    NSString *mobileNo = self.txt_MobileNo.text;
    //    NSString *emailAddress = self.txt_Email.text;
    //
    //    [formatter setDateFormat:@"dd/MM/yyyy"];
    //    //    @"d  d   m  m   y  y  y  y"
    //    NSString *str=[formatter stringFromDate:datePicker.date];
    
    
    
    // NSString *dateOfBirth = self.txt_DateBirth.text;
    
    NSMutableString *str_dietryids = [[NSMutableString alloc] init];
    
    for (int i=0; i<[Arr_temp count]; i++)
    {
        
        
        if (i==[Arr_temp count]-1)
        {
            [str_dietryids  appendString:[NSString stringWithFormat:@"%@",[[Arr_temp objectAtIndex:i] valueForKey:@"DietaryID"]]];
        }
        else
        {
            [str_dietryids  appendString:[NSString stringWithFormat:@"%@||",[[Arr_temp objectAtIndex:i] valueForKey:@"DietaryID"]]];
        }
        
        
    }
    
    NSMutableString *str_dietryidsmeal = [[NSMutableString alloc] init];
    
    for (int i=0; i<[Arr_temp_in_meal count]; i++)
    {
        
        
        if (i==[Arr_temp_in_meal count]-1)
        {
            [str_dietryidsmeal  appendString:[NSString stringWithFormat:@"%@",[[Arr_temp_in_meal objectAtIndex:i] valueForKey:@"DietaryID"]]];
        }
        else
        {
            [str_dietryidsmeal  appendString:[NSString stringWithFormat:@"%@||",[[Arr_temp_in_meal objectAtIndex:i] valueForKey:@"DietaryID"]]];
        }
        
        
    }
    
    
    NSString *str_Keywords = @"";
    
    for (int i=0; i<[ary_Keywords count]; i++)
    {
        str_Keywords = [NSString stringWithFormat:@"%@,%@",str_Keywords,[ary_Keywords objectAtIndex:i]];
    }
    
    if ([str_Keywords hasPrefix:@","])
    {
        str_Keywords = [str_Keywords substringFromIndex:1];
    }
    
    NSString *str_Keywords_meal = @"";
    
    for (int i=0; i<[Ary_keywordmeal count]; i++)
    {
        str_Keywords_meal = [NSString stringWithFormat:@"%@,%@",str_Keywords_meal,[Ary_keywordmeal objectAtIndex:i]];
    }
    
    if ([str_Keywords_meal hasPrefix:@","])
    {
        str_Keywords_meal = [str_Keywords_meal substringFromIndex:1];
    }
    
    
    NSDictionary *params;
    if ([str_selecte_dish_r_meal isEqualToString:@"Dish"])
    {
        params =@{
                  
                  @"uid"                       : [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                  @"title"                     :  txt_dishTitle.text,
                  @"type"                      :  str_selecte_dish_r_meal,
                  
                  @"dietary_restrictions"      :  str_dietryids,
                  
                  @"cuisine_category_id"       :  str_cataogyid,
                  @"dish_cuisine_id"           :  str_cuisineid,
                  @"course_id"                 :  str_courseid,
                  
                  @"dish_tags"                 :  str_Keywords,
                  @"dish_price"                :  txt_Price.text,
                  @"description"               :  txt_view.text,
                  
                  };
        
    }
    else
    {
        params =@{
                  
                  @"uid"                       : [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                  @"type"                      :  str_selecte_dish_r_meal,
                  @"dietary_restrictions"      :  str_dietryidsmeal,
                  @"title"                     :  txt_MealTitle_in_meal.text,
                  
                  @"cuisine_category_id"       :  str_categoryid_in_meal,
                  @"dish_cuisine_id"           :  str_cuisinId_in_meal,
                  @"course_id"                 :  str_courseid_in_meal,
                  
                  @"dish_tags"                 :  str_Keywords_meal,
                  @"dish_price"                :  txt_Price_in_meal.text,
                  @"description"               :  txt_view_in_meal.text,
                  
                  };
        
    }
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request;
    
    
    if ([str_selecte_dish_r_meal isEqualToString:@"Dish"])
    {
        if (imgData != nil)
        {
            
            request = [httpClient multipartFormRequestWithMethod:@"POST" path:ksingledishandmeal  parameters:params constructingBodyWithBlock:^(id <AFMultipartFormData>formData)
                       {
                           NSTimeInterval timeInterval = [NSDate timeIntervalSinceReferenceDate];
                           [formData appendPartWithFileData:imgData name:@"dish_image" fileName:[NSString stringWithFormat:@"%lf-dishimage.png",timeInterval] mimeType:@"image/jpeg"];
                           
                       }];
        }
        
        else
        {
            request = [httpClient requestWithMethod:@"POST" path:ksingledishandmeal  parameters:params];
            
        }
        
        
    }
    else
    {
        if (imgData_in_meal != nil)
        {
            
            request = [httpClient multipartFormRequestWithMethod:@"POST" path:ksingledishandmeal  parameters:params constructingBodyWithBlock:^(id <AFMultipartFormData>formData)
                       {
                           NSTimeInterval timeInterval = [NSDate timeIntervalSinceReferenceDate];
                           [formData appendPartWithFileData:imgData_in_meal name:@"dish_image" fileName:[NSString stringWithFormat:@"%lf-dishimage.png",timeInterval] mimeType:@"image/jpeg"];
                           
                       }];
        }
        
        else
        {
            request = [httpClient requestWithMethod:@"POST" path:ksingledishandmeal  parameters:params];
            
        }
    }
    
    
    
    
    
    NSLog(@"request:%@",request);
    
    //====================================================RESPONSE
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        NSLog(@"JSON = %@",JSON);
        [delegate.activityIndicator stopAnimating];
        
        [self ResponseSingleDish_r_Meal:JSON];
        
    }
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         [self.view setUserInteractionEnabled:YES];
                                         if([operation.response statusCode] == 406){
                                             
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             
                                             
                                         }
                                         else if ([[operation error] code] == -1001)
                                         {
                                             NSLog(@"Successfully Registered");
                                             [self AFforSingleDish_r_Meal];
                                         }
                                     }];
    [operation start];
    
}


-(void)ResponseSingleDish_r_Meal:(NSDictionary * )TheDict
{
    NSLog(@"ResponseDefault: %@",TheDict);
    
    [self.view setUserInteractionEnabled:YES];
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        ViewMenuVC *menuScreenEdit = [[ViewMenuVC alloc]init];
        [self presentViewController:menuScreenEdit animated:NO completion:nil];
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        
    }
}




# pragma mark ditary_ristrictions method

-(void)AFDietary_restrictions
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kDietary_restryctions
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsediet:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFDietary_restrictions];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsediet :(NSDictionary * )TheDict
{
    [array_dietary_restryctionlist removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DietarytList"] count]; i++)
        {
            [array_dietary_restryctionlist addObject:[[TheDict valueForKey:@"DietarytList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //  txt_popular.text=@"";
        
        
    }
    
    [collView_serviceDirectory reloadData];
    
    
    
}



# pragma mark Popular method

-(void)popularList
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpCuisineCategoryList
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsepopularlist:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self popularList];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsepopularlist :(NSDictionary * )TheDict
{
    [aryPopularlist removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"CuisineCategoryList"] count]; i++)
        {
            [aryPopularlist addObject:[[TheDict valueForKey:@"CuisineCategoryList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //  txt_popular.text=@"";
        
        
    }
    
    //To add Popular by default Gaurav
    for (int i =0; i< [aryPopularlist count]; i++) {
        if ([[[aryPopularlist objectAtIndex:i] valueForKey:@"CuisineCatName"] isEqualToString:@"Popular"] && [array_selectedcategoryid count] == 0) {
            [array_selectedcategoryid addObject:[aryPopularlist objectAtIndex:i]];
            txt_Category.text =[[array_selectedcategoryid valueForKey:@"CuisineCatName"] componentsJoinedByString:@", "];
            break;
        }
    }
    
    
    [table_for_category reloadData];
    
    str_cuisenCatId = @"80";
    
    
    
    
}





# pragma mark  Cuisine list method

-(void)cuisineList
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    NSDictionary *params;
    
    if ([str_selecte_dish_r_meal isEqualToString:@"Dish"])
    {
        params =@{
                  
                  
                  @"cuisines_catid"            :  str_cataogyid,
                  
                  };
    }
    else{
        params =@{
                  
                  
                  @"cuisines_catid"            :  str_categoryid_in_meal,
                  
                  };
    }
    
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpCuisineList
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsecuisenlist:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self cuisineList];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsecuisenlist :(NSDictionary * )TheDict
{
    if ([str_selecte_dish_r_meal isEqualToString:@"Dish"]){
    
    [arrCuisines removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"CuisineList"] count]; i++)
        {
            [arrCuisines addObject:[[TheDict valueForKey:@"CuisineList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        // txt_popular.text=@"";
        
        
    }
    
    [table_for_cuisine reloadData];
    }
    else{
        
        [arrCuisineslist_in_meal removeAllObjects];
        
        
        NSLog(@"Login: %@",TheDict);
        
        if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
        {
            for (int i=0; i<[[TheDict valueForKey:@"CuisineList"] count]; i++)
            {
                [arrCuisineslist_in_meal addObject:[[TheDict valueForKey:@"CuisineList"] objectAtIndex:i]];
                
            }
            
            
        }
        else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
        {
            // txt_popular.text=@"";
            
            
        }
        
        [table_for_cuisine_in_meal reloadData];
    }
}




# pragma mark  Course list method

-(void)AFcourse
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kCourses
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsecousrelist:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFcourse];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsecousrelist :(NSDictionary * )TheDict
{
    [array_course_list removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DishCoursetList"] count]; i++)
        {
            [array_course_list addObject:[[TheDict valueForKey:@"DishCoursetList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        // txt_popular.text=@"";
        
        
    }
    
    [table_for_course reloadData];
    
}



# pragma mark ditary_ristrictions method

-(void)AFDietary_restrictions_in_meal
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kDietary_restryctions
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsediet_in_meal:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFDietary_restrictions_in_meal];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsediet_in_meal :(NSDictionary * )TheDict
{
    [array_dietary_restryctionlist_in_meal removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DietarytList"] count]; i++)
        {
            [array_dietary_restryctionlist_in_meal addObject:[[TheDict valueForKey:@"DietarytList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //  txt_popular.text=@"";
        
        
    }
    
    [collView_serviceDirectory_for_meal reloadData];
    
    
    
}






# pragma mark Popular method

-(void)AFcategory_in_meal
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpCuisineCategoryList
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsecatagoryinmeal:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFcategory_in_meal];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsecatagoryinmeal :(NSDictionary * )TheDict
{
    [array_categorylist removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"CuisineCategoryList"] count]; i++)
        {
            [array_categorylist addObject:[[TheDict valueForKey:@"CuisineCategoryList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        //  txt_popular.text=@"";
        
        
    }
    
    //To add Popular by default Gaurav
    for (int i =0; i< [array_categorylist count]; i++) {
        if ([[[array_categorylist objectAtIndex:i] valueForKey:@"CuisineCatName"] isEqualToString:@"Popular"] && [array_selectedcategorylist count] == 0) {
            [array_selectedcategorylist addObject:[array_categorylist objectAtIndex:i]];
            txt_Category_in_meal.text =[[array_selectedcategorylist valueForKey:@"CuisineCatName"] componentsJoinedByString:@", "];
            break;
        }
    }
    
    [table_for_category_in_meal reloadData];
    
    str_cuisenCatId_in_meal = @"80";
    
}

# pragma mark  Cuisine list method in meal

-(void)cuisineList_in_meal
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    NSDictionary *params =@{
                            
                            
                            @"cuisine_category_id"            :  str_cuisenCatId,
                            
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:AFChefSigUpCuisineList
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsecuisenlist_in_meal:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self cuisineList_in_meal];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsecuisenlist_in_meal :(NSDictionary * )TheDict
{
    [arrCuisineslist_in_meal removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"CuisineList"] count]; i++)
        {
            [arrCuisineslist_in_meal addObject:[[TheDict valueForKey:@"CuisineList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        // txt_popular.text=@"";
        
        
    }
    
    [table_for_cuisine_in_meal reloadData];
    
    
}

# pragma mark  Course list method

-(void)AFcourse_in_meal
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        delegate.devicestr = @"";
    }
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:kCourses
                                                      parameters:nil];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsecousrelist_in_meal:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFcourse_in_meal];
                                         }
                                     }];
    [operation start];
    
}
-(void) Responsecousrelist_in_meal :(NSDictionary * )TheDict
{
    [array_course_list_in_meal removeAllObjects];
    
    
    NSLog(@"Login: %@",TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[TheDict valueForKey:@"DishCoursetList"] count]; i++)
        {
            [array_course_list_in_meal addObject:[[TheDict valueForKey:@"DishCoursetList"] objectAtIndex:i]];
            
        }
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        // txt_popular.text=@"";
        
        
    }
    
    [table_for_course_in_meal reloadData];
    
}









- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
