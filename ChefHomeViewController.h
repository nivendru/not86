//
//  ChefHomeViewController.h
//  Not86
//
//  Created by Interwld on 8/3/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChefServeNowVC.h"

@interface ChefHomeViewController : JWSlideMenuViewController<UIScrollViewDelegate>{
    UIScrollView *scroll;
}

@end
