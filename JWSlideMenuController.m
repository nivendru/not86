//  JWSlideMenuController.m
//  JWSlideMenu
//  Created by Jeremie Weldin on 11/14/11.
//  Copyright (c) 2011 Jeremie Weldin. All rights reserved.
#import "JWSlideMenuController.h"
#import "JWNavigationController.h"
#import "JWSlideMenuViewController.h"
#import "Define.h"
#import "AppDelegate.h"
#import "MyProfileVC.h"

#import "ChefHomeViewController.h"
#import "MyProfileVC.h"
#import "MyHomescreenVC.h"
#import "MyScheduleVC.h"

#import "MyMenuVC.h"
#import "MyAccountVC.h"
#import "MessagesVC.h"
#import "SwitchtoUser.h"
#import "UserProfileVC.h"
#import "ChefMyProfileVC.h"
#import "UIImageView+AFNetworking.h"


typedef void (^CompletionBlock)(BOOL success);

@implementation JWSlideMenuController
{
    AppDelegate *delegate;
    int selectedSection;
    CGFloat tableHeight;
    NSMutableArray *ary_SectionTitle;
    UIImageView *imgView_ULine1;
    NSMutableArray *ary_Image;
    UITableView *table_Accessory;
    NSMutableArray *ary_DailyDeals;
    UIScrollView *scroll;
    NSMutableArray *ary_ContactTitle;
    NSMutableArray *ary_ContactUsImg;
    UIImageView *imgView_Profile;
    UILabel *lbl_Header;
    UILabel *lbl_UserName;
    int indexSelected;
    
    UIView *viewHeader;
    UILabel*lbl_headegmail;
    
    UIImageView *imgView_TopBar;
    
    BOOL isLogout;
    UIView *alertviewBg1;
    
    UIView *alertviewBg;
    UIView *alertviewBgBoth;
    int childIndex;
}

@synthesize menuTableView;
@synthesize menuView;

@synthesize contentView;
@synthesize menuLabelColor;
@synthesize searchController;
@synthesize homeButton;

-(id)init
{
    self = [super init];
    if (self)
    {
        
        isLogout=NO;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(UserLogout)
                                                     name:@"DeactivateNotification"
                                                   object:nil];
        indexSelected=0;
        selectedSection =-1;
        delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        CGRect masterRect = self.view.bounds;
        CGRect contentFrame = CGRectMake(0.0,0.0,masterRect.size.width,masterRect.size.height);
        homeButton = [[UIButton alloc] init];
        homeButton.showsTouchWhenHighlighted = YES;
        homeButton.layer.borderColor = [[UIColor clearColor] CGColor];
        [homeButton setImage:[UIImage imageNamed:@"icon_menu@2x.png"] forState:UIControlStateNormal];
        // [homeButton setBackgroundImage:[UIImage imageNamed:@"icon_menu@2x.png"] forState:UIControlStateNormal];
        [homeButton addTarget:self action:@selector(toggleMenu) forControlEvents:UIControlEventTouchUpInside];
        
        btn_TransHome = [[UIButton alloc] init];
        btn_TransHome.layer.borderColor = [[UIColor clearColor] CGColor];
        [btn_TransHome addTarget:self action:@selector(toggleMenu) forControlEvents:UIControlEventTouchUpInside];
        
        scroll = [[UIScrollView alloc]init];
        [scroll setShowsVerticalScrollIndicator:NO];
        [scroll setUserInteractionEnabled:YES];
        scroll.delegate = self;
        scroll.backgroundColor = [UIColor whiteColor];
        [self.view  addSubview:scroll];
        
        
        self.menuLabelColor = [UIColor blackColor];
        self.menuTableView=[[UITableView alloc] init];
        self.menuTableView.showsVerticalScrollIndicator =NO;
        self.menuTableView.showsHorizontalScrollIndicator =NO;
        self.menuTableView.bounces = YES;
        self.menuTableView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"img-head@2x.png"]];//[UIColor darkGrayColor];
        self.menuTableView.alpha=0.5f;
        self.menuTableView.delegate = self;
        self.menuTableView.dataSource = self;
        self.menuTableView.scrollEnabled=NO;
        
        if([self.menuTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            
            [self.menuTableView setSeparatorInset:UIEdgeInsetsZero];
        }
        [self.menuTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [scroll addSubview:self.menuTableView];
        
        UIImageView *DividerImageView = [[UIImageView alloc] init];
        [DividerImageView setImage:[UIImage imageNamed:@"JW_Divider@2x.png"]];
        [self.menuTableView addSubview:DividerImageView];
        
        self.menuView = [[UIView alloc] init];
        self.menuView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"img-head@2x.png"]];//[UIColor darkGrayColor];
        [self.menuView addSubview:self.menuTableView];
        
        
        lbl_UserName = [[UILabel alloc]init];
        [lbl_UserName setTextAlignment:NSTextAlignmentLeft];
        lbl_UserName.backgroundColor = [UIColor clearColor];
        lbl_UserName.textColor = [UIColor lightGrayColor];
        
        UIImageView *imgView_Verti = [[UIImageView alloc] init];
        [imgView_Verti setImage:[UIImage imageNamed:@""]];
        imgView_Verti.backgroundColor = [UIColor grayColor];
        
        self.contentView = [[UIView alloc] init];
        tableHeight = [ary_SectionTitle count]*40;
        self.navigationController.navigationBar.hidden=YES;
        float menuWidth =120;
        
        homeButton.frame = CGRectMake(10,14,30,17);
        if (IS_IPHONE_6) {
            btn_TransHome.frame = CGRectMake(0,0,80,45);
        }
        else if (IS_IPHONE_6Plus) {
            btn_TransHome.frame = CGRectMake(0,0,80,45);
        }
        else
        {
            btn_TransHome.frame = CGRectMake(0,0,50,45);
        }
        
        
        scroll.frame=CGRectMake(0,HEADER_HEIGHT,WIDTH, HEIGHT-(HEADER_HEIGHT));
        [scroll setContentSize:CGSizeMake(0, CGRectGetMaxY(menuTableView.frame)+50)];
        
        UIImageView *bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"JWBG@2x.png"]];
        [self.menuTableView setBackgroundView:bg];
        
        
        imgView_Profile.frame = CGRectMake(5,14,35,35);
        lbl_UserName.frame = CGRectMake(CGRectGetMaxX(imgView_Profile.frame)+5,0,206,60);
        lbl_UserName.font = [UIFont fontWithName:kFontBold size:18.0f];
        imgView_ULine1 = [[UIImageView alloc]init];
        imgView_ULine1.frame = CGRectMake(10,CGRectGetMaxY(lbl_UserName.frame)-8,530,1);
        imgView_ULine1.backgroundColor = [UIColor whiteColor];
        
        
        imgView_Verti.frame = CGRectMake(526,0,2,60);
        self.menuView.frame = CGRectMake(0.0, 0.0,menuWidth-5, masterRect.size.height+250);
        CGRect frameForTableview = CGRectMake(0.0,90, menuWidth, masterRect.size.height-100);
        self.menuTableView = [[UITableView alloc] initWithFrame:frameForTableview] ;
        self.menuTableView.dataSource = self;
        self.menuTableView.showsVerticalScrollIndicator = NO;
        self.menuTableView.delegate = self;
        self.menuTableView.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"img-head@2x.png"]];
        [self.menuTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        
        [self.menuTableView addSubview:DividerImageView];
        self.contentView.frame=contentFrame;
    }
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.contentView.backgroundColor = [UIColor clearColor];
    
    //Add header View
    [self integratedView];
    
    [self.menuView addSubview:self.menuTableView];
    [self.view addSubview:self.menuView];
    [self.view insertSubview:self.contentView aboveSubview:self.menuView];
    [self.view addSubview:homeButton];
    [self.view addSubview:btn_TransHome];
    [self.menuView addSubview:imgView_Profile];
    [lbl_UserName addSubview:imgView_ULine1];
    
    UISwipeGestureRecognizer *rightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandle:)];
    rightRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [rightRecognizer setNumberOfTouchesRequired:1];
    
    //add the your gestureRecognizer , where to detect the touch..
    [self.view addGestureRecognizer:rightRecognizer];
    
    UISwipeGestureRecognizer *leftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandle:)];
    leftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [leftRecognizer setNumberOfTouchesRequired:1];
    
    [self.view addGestureRecognizer:leftRecognizer];
    [self prefersStatusBarHidden];
    return self;
}

-(void)integratedView
{
    viewHeader = [[UIView alloc] init];
    viewHeader.frame=CGRectMake(0,0,WIDTH-58,90);
    viewHeader.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"img-head@2x.png"]];
    viewHeader.userInteractionEnabled = YES;
    [self.menuView addSubview:viewHeader];
    
    
    UIImageView *imgView_edit= [[UIImageView alloc] init];
    imgView_edit.frame=CGRectMake(130,10,10,10);
    [imgView_edit setImage:[UIImage imageNamed:@"img_editwhiteslider@2x.png"]];
    imgView_edit.backgroundColor=[UIColor greenColor];
    [imgView_edit setUserInteractionEnabled:YES];
    // [self.menuView addSubview:imgView_edit];
    
    imgView_Profile = [[UIImageView alloc] init];
    imgView_Profile.frame=CGRectMake(25,15,60,60);
    [imgView_Profile setContentMode:UIViewContentModeScaleAspectFill];
    [imgView_Profile setClipsToBounds:YES];
    //imgView_Profile.image = [UIImage imageNamed:@"img_Profile@2x.png"];
    //    NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"ProfileImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //    [imgView_Profile setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@"img_Profile@2x.png"]];
    [viewHeader addSubview:imgView_Profile];
    
    
    imgView_Profile.userInteractionEnabled = YES;
    
    
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(click_ImageProfileClicked:)];
    tap.numberOfTapsRequired=1;
    [imgView_Profile addGestureRecognizer:tap];
    
    
    //    [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserDetail"] valueForKey:@"Username"]
    lbl_Header=[[UILabel alloc]init];
    lbl_Header.frame=CGRectMake(CGRectGetMaxX(imgView_Profile.frame)+10,25,100,12);
    [lbl_Header setText:@"Rihanan"];
    [lbl_Header setTextAlignment:NSTextAlignmentLeft];
    lbl_Header.backgroundColor=[UIColor clearColor];
    lbl_Header.textColor=[UIColor whiteColor];
    lbl_Header.font=[UIFont fontWithName:kFontBold size:10.0f];
    [viewHeader addSubview:lbl_Header];
    
    lbl_headegmail=[[UILabel alloc]init];
    lbl_headegmail.frame=CGRectMake(CGRectGetMaxX(imgView_Profile.frame)+10,CGRectGetMaxY(lbl_Header.frame)+2,100,12);
    [lbl_headegmail setText:@"rihanan@gmail.com"];
    [lbl_headegmail setTextAlignment:NSTextAlignmentLeft];
    lbl_headegmail.backgroundColor=[UIColor clearColor];
    lbl_headegmail.textColor=[UIColor whiteColor];
    lbl_headegmail.font=[UIFont fontWithName:kFontBold size:8.0f];
    [viewHeader addSubview:lbl_headegmail];
    
    UIImageView *imgView_Undline= [[UIImageView alloc] init];
    imgView_Undline.frame=CGRectMake(13,CGRectGetMaxY(viewHeader.frame)-2,125,0.6);
    imgView_Undline.backgroundColor=[UIColor whiteColor];
    [imgView_Undline setUserInteractionEnabled:YES];
    // [self.menuView addSubview:imgView_Undline];
    
    UIButton *btn_GotToProfile = [[UIButton alloc] init];
    btn_GotToProfile.frame = CGRectMake(10,20,150,110);
    btn_GotToProfile.backgroundColor = [UIColor clearColor];
    [btn_GotToProfile addTarget:self action:@selector(click_BtnGotToProfile:) forControlEvents:UIControlEventTouchUpInside];
    [viewHeader addSubview:btn_GotToProfile];
}

-(void)click_BtnGotToProfile:(UIButton *)sender
{
    
}



#pragma mark - UIAction View
-(void)click_ImageProfileClicked:(UIGestureRecognizer *)recorgnize
{
    NSLog(@"click_ImageProfileClicked");
    if ([[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Role"]]isEqualToString:@"1"])
    {
        ChefMyProfileVC *myProfileVC = [[ChefMyProfileVC alloc]init];
        [self presentViewController:myProfileVC animated:NO completion:nil];
        
        
    }
    else
    {
        UserProfileVC *myProfileVC = [[UserProfileVC alloc]init];
        [self presentViewController:myProfileVC animated:NO completion:nil];
        
    }
    
    
    
    
}

#pragma mark Hide StatusBar Method

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark

- (void)rightSwipeHandle:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSLog(@"rightSwipeHandle");
    
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"is_slide"] isEqualToString:@"NO"])
    {
        
    }
    else
    {
        
        [UIView beginAnimations:@"Menu Slide" context:nil];
        [UIView setAnimationDuration:0.2];
        
        if(self.contentView.frame.origin.x == 0) //Menu is hidden
        {
            CGRect newFrame = CGRectOffset(self.contentView.frame, self.menuView.frame.size.width, 0.0);
            
            self.contentView.frame = newFrame;
            homeButton.frame = CGRectMake(self.menuView.frame.size.width,14,30,17);
            for (int i = 0; i < self.childViewControllers.count; i++) {
                JWSlideMenuViewController *vc = [[JWSlideMenuViewController alloc]init];
                vc = [self.childViewControllers objectAtIndex:i];
                [vc.view setUserInteractionEnabled:NO];
                
            }
            [menuTableView reloadData];
            
        }
        
        else //Menu is shown
        {
            
        }
        
        
    }
    [UIView commitAnimations];
    
    
}








- (void)leftSwipeHandle:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSLog(@"leftSwipeHandle");
    
    
    [UIView beginAnimations:@"Menu Slide" context:nil];
    [UIView setAnimationDuration:0.2];
    
    if(self.contentView.frame.origin.x == 0) //Menu is hidden
    {
        //        CGRect newFrame = CGRectOffset(self.contentView.frame, self.menuView.frame.size.width, 0.0);
        //        homeButton.frame = CGRectMake(242, 23,26,13);
        //        btn_TransHome.frame = CGRectMake(242,23,26,13);
        //        self.contentView.frame = newFrame;
        //
        //        for (int i = 0; i < self.childViewControllers.count; i++) {
        //            JWSlideMenuViewController *vc = [[JWSlideMenuViewController alloc]init];
        //            vc = [self.childViewControllers objectAtIndex:i];
        //            [vc.view setUserInteractionEnabled:NO];
        //
        //        }
        //        [menuTableView reloadData];
        
    }
    
    else //Menu is shown
    {
        CGRect newFrame = CGRectOffset(self.contentView.frame, -(self.menuView.frame.size.width), 0.0);
        
        
        homeButton.frame = CGRectMake(10,14,30,17);
        if (IS_IPHONE_6) {
            btn_TransHome.frame = CGRectMake(0,0,80,45);
            
            
        }
        else if (IS_IPHONE_6Plus) {
            btn_TransHome.frame = CGRectMake(0,0,80,45);
            
        }
        else
        {
            btn_TransHome.frame = CGRectMake(0,0,50,45);
            
        }
        
        self.contentView.frame = newFrame;
        
        for (int i = 0; i < self.childViewControllers.count; i++)
        {
            JWSlideMenuViewController *vc = [self.childViewControllers objectAtIndex:i];
            [vc.view setUserInteractionEnabled:YES];
        }
    }
    [UIView commitAnimations];
}

#pragma mark Toggling

-(void)ChangeToMenu:(int)index{
    
    
    menuTableView.alpha = 0.2f;
    [UIView transitionWithView:menuTableView
                      duration:1.0
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        menuTableView.alpha = 2.0;
                    } completion:NULL];
    
    for (UIView *subviews in contentView.subviews)
    {
        
        [subviews removeFromSuperview];
        
    }
    [menuTableView reloadData];
    
    indexSelected = index;
    
    UIViewController* controller = (UIViewController*)[self.childViewControllers objectAtIndex:index];
    [self.menuTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
    controller.view.frame = self.contentView.bounds;
    [contentView addSubview:controller.view];
    [self toggleMenu];
    
    
}

-(void)toggleMenu
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"UserDetail"])
    {
        
        //        NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"ProfileImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //        [imgView_Profile setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@""]];
        
        NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"ProfileImage"]]];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        // UIImage *image = [UIImage imageWithData:imageData];
        imgView_Profile.image = [UIImage imageWithData:imageData];
        
        // imgView_Profile.image = [UIImage imageNamed:@"img_Profile@2x.png"];
        
        //img_PlaceholderSquare@2x.png
        imgView_Profile.layer.cornerRadius =54/2;
        
        
        
        [lbl_Header setText:[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserDetail"] valueForKey:@"username"]];
        [lbl_headegmail setText:[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserDetail"] valueForKey:@"Email"]];
        
        //        [lbl_Header setText:@"Rihanan"];
        
    }
    else
    {
        
        lbl_UserName.text = @" Hi! Guest";
        // [imgView_Profile setImage:[UIImage imageNamed:@"img_PlaceholderSquare@2x.png"]];
        //imgView_Profile.image = [UIImage imageNamed:@"img_Profile@2x.png"];
    }
    UITextField *searchTextField = (UITextField *)[self.view viewWithTag:-2];
    [searchTextField resignFirstResponder];
    [UIView beginAnimations:@"Menu Slide" context:nil];
    [UIView setAnimationDuration:0.2];
    
    if(self.contentView.frame.origin.x == 0) //Menu is hidden
    {
        CGRect newFrame = CGRectOffset(self.contentView.frame, self.menuView.frame.size.width, 0.0);
        
        homeButton.frame = CGRectMake(130,14,30,17);
        if (IS_IPHONE_6) {
            btn_TransHome.frame = CGRectMake(WIDTH/2-50,0,90,45);
            
            
        }
        else if (IS_IPHONE_6Plus) {
            btn_TransHome.frame = CGRectMake(130,0,90,45);
            
        }
        else
        {
            btn_TransHome.frame = CGRectMake(WIDTH/2-70,0,50,45);
            
        }
        btn_TransHome.backgroundColor=[UIColor clearColor];
        self.contentView.frame = newFrame;
        
        for (int i = 0; i < self.childViewControllers.count; i++) {
            JWSlideMenuViewController *vc = [[JWSlideMenuViewController alloc]init];
            vc = [self.childViewControllers objectAtIndex:i];
            [vc.view setUserInteractionEnabled:NO];
        }
        [menuTableView reloadData];
    }
    else
    {
        isLogout=NO;
        CGRect newFrame=CGRectOffset(self.contentView.frame, -(self.menuView.frame.size.width), 0.0);
        homeButton.frame= CGRectMake(10,14,30,17);
        
        homeButton.backgroundColor=[UIColor clearColor];
        if (IS_IPHONE_6) {
            btn_TransHome.frame = CGRectMake(0,0,80,45);
            
            
        }
        else if (IS_IPHONE_6Plus) {
            btn_TransHome.frame = CGRectMake(0,0,80,45);
            
        }
        else
        {
            btn_TransHome.frame = CGRectMake(0,0,50,45);
            
        }
        btn_TransHome.backgroundColor=[UIColor clearColor];
        self.contentView.frame=newFrame;
        for (int i = 0; i < self.childViewControllers.count; i++)
        {
            JWSlideMenuViewController *vc = [self.childViewControllers objectAtIndex:i];
            [vc.view setUserInteractionEnabled:YES];
        }
    }
    [UIView commitAnimations];
    
}
-(JWNavigationController *)addViewController:(JWSlideMenuViewController *)controller withTitle:(NSString *)title andImage:(UIImage *)image
{
    
    JWNavigationController *navController = [[JWNavigationController alloc] initWithRootViewController:controller];
    navController.slideMenuController = self;
    navController.title = title;
    navController.tabBarItem.image = image;
    
    [self addChildViewController:navController];
    
    if([self.childViewControllers count] == 1)
    {
        [self.contentView addSubview:navController.view];
    }
    
    return navController;
}

#pragma mark TableView dataSource/delegate Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int abc;
    
    abc=0;
    
    if (IS_IPHONE_6) {
        abc=79;
        
        
    }
    else if (IS_IPHONE_6Plus)
    {
        abc=79;
        
    }
    else
    {
        
        abc=60;
    }
    return abc;}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *kReuseIndentifier = @"myCell";
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    if (cell==nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    }
    for (UIView *view in cell.contentView.subviews)
    {
        [view removeFromSuperview];
    }
    cell.backgroundColor=[UIColor clearColor];
    
    UILabel *categoryLabel=[[UILabel alloc] init];
    [categoryLabel setTextColor:[UIColor whiteColor]];
    [categoryLabel setBackgroundColor:[UIColor clearColor]];
    categoryLabel.textAlignment = NSTextAlignmentCenter;
    categoryLabel.numberOfLines=2;
    [categoryLabel setFont:[UIFont fontWithName:kFont size:12.0f]];
    [cell.contentView addSubview:categoryLabel];
    
    UIButton *cellImageView=[[UIButton alloc] init];
    [cellImageView setBackgroundColor:[UIColor clearColor]];
    [cellImageView setUserInteractionEnabled:NO];
    cellImageView.contentHorizontalAlignment = 1;
    [cell.contentView addSubview:cellImageView];
    
    UIImageView *imgView_Line=[[UIImageView alloc] init];
    [imgView_Line setUserInteractionEnabled:YES];
    [cell.contentView addSubview:imgView_Line];
    
    
    if (IS_IPHONE_6) {
        cellImageView.frame=CGRectMake(38,8,40,35);
        categoryLabel.frame=CGRectMake(0,CGRectGetMaxY(cellImageView.frame),110,35);
        categoryLabel.textColor=[UIColor whiteColor];
        [categoryLabel setFont:[UIFont fontWithName:kFont size:12.0f]];
        
        
    }
    else if (IS_IPHONE_6Plus)
    {
        cellImageView.frame=CGRectMake(42,2,40,40);
        categoryLabel.frame=CGRectMake(0,CGRectGetMaxY(cellImageView.frame),115,35);
        categoryLabel.textColor=[UIColor whiteColor];
        [categoryLabel setFont:[UIFont fontWithName:kFont size:12.0f]];
        
        
    }
    else
    {
        
        cellImageView.frame=CGRectMake(35,2,30,30);
        categoryLabel.frame=CGRectMake(0,CGRectGetMaxY(cellImageView.frame)-5,110,30);
        categoryLabel.textColor=[UIColor whiteColor];
        
    }
    
    
    UIViewController *controller=nil;
    //    else if (indexPath.row ==[self.childViewControllers count]-1) {
    //        controller = (UIViewController *)[self.childViewControllers objectAtIndex:indexPath.row];
    //        [categoryLabel setText:controller.title];
    //        [cellImageView setImage:controller.tabBarItem.image forState:UIControlStateNormal];
    //
    //    }
    //    else if (indexPath.row ==[self.childViewControllers count]-2) {
    //
    //        controller = (UIViewController *)[self.childViewControllers objectAtIndex:indexPath.row];
    //        [categoryLabel setText:controller.title];
    //        [cellImageView setImage:controller.tabBarItem.image forState:UIControlStateNormal];
    //    }
    
    controller = (UIViewController *)[self.childViewControllers objectAtIndex:indexPath.row];
    [categoryLabel setText:controller.title];
    [cellImageView setImage:controller.tabBarItem.image forState:UIControlStateNormal];
    return cell;
}

-(void)click_BtnArrow:(UIButton *)sender
{
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.childViewControllers count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    if ([delegate.str_comingfromtaxiarrived isEqualToString:@"YES"])
    //    {
    //        [[NSNotificationCenter defaultCenter] postNotificationName:@"NavigateCheck" object:nil userInfo:nil];
    //
    //    }
    if (indexPath.row==[self.childViewControllers count])
    {
        isLogout=YES;
        [self popup_AlertviewValidation:@"Do you want Logout?"];
    }
    else if (indexPath.row ==5)
    {
        for(UIView *subviews in contentView.subviews)
        {
            [subviews removeFromSuperview];
        }
        isLogout=NO;
        UIViewController *controller = (UIViewController*)[self.childViewControllers objectAtIndex:indexPath.row];
        controller.view.frame = self.contentView.bounds;
        [contentView addSubview:controller.view];
        [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
        tableView.alpha = 0.2f;
        [UIView transitionWithView:tableView
                          duration:1.0
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            tableView.alpha = 2.0;
                        } completion:NULL];
        [self toggleMenu];
        
    }
    else
    {
        for(UIView *subviews in contentView.subviews)
        {
            [subviews removeFromSuperview];
        }
        isLogout=NO;
        //        delegate. isSliderToBack=YES;
        UIViewController *controller = (UIViewController*)[self.childViewControllers objectAtIndex:indexPath.row];
        controller.view.frame = self.contentView.bounds;
        [contentView addSubview:controller.view];
        [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
        tableView.alpha = 0.2f;
        [UIView transitionWithView:tableView
                          duration:1.0
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            tableView.alpha = 2.0;
                        } completion:NULL];
        [self toggleMenu];
    }
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    //    NSString * ProfileImagePathcategory = [[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"ProfileImage"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //    [imgView_Profile setImageWithURL:[NSURL URLWithString:ProfileImagePathcategory] placeholderImage:[UIImage imageNamed:@"img_profile@2x.png"]];
    
    
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"ProfileImage"]]];
    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
    // UIImage *image = [UIImage imageWithData:imageData];
    imgView_Profile.image = [UIImage imageWithData:imageData];
    
    
    // imgView_Profile.image = [UIImage imageNamed:@"img_Profile@2x.png"];
    
    //img_PlaceholderSquare@2x.png
    imgView_Profile.layer.cornerRadius =60/2;
    
    
    
    [lbl_Header setText:[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserDetail"] valueForKey:@"username"]];
    [lbl_headegmail setText:[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserDetail"] valueForKey:@"Email"]];
    
    
}

- (void)viewDidUnload
{
    [self setMenuView:nil];
    [self setContentView:nil];
    [self setMenuTableView:nil];
    [self setMenuLabelColor:nil];
    [super viewDidUnload];
}




-(void)HideHomeButton
{
    homeButton.hidden = YES;
    btn_TransHome.hidden = YES;
}

-(void)UnhideHomeButton
{
    homeButton.hidden = NO;
    btn_TransHome.hidden = NO;
}

-(void)LogOutAlert
{
    [self popup_AlertviewForLogOut:@"Do you want to Logout?"];
}

-(void)UserLogout
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"UserDetail"];
    
    [defaults synchronize];
    
    
    
    //    [delegate resetJWSlider];
    
}

#pragma mark Alertview Popup
-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    //    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
    //                                   initWithTarget:self
    //                                   action:@selector(click_btnAlertviewOk:)];
    //    tap.numberOfTapsRequired=1;
    //    [alertviewBg addGestureRecognizer:tap];
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:196.0/255.0 green:0.0/255.0 blue:52.0/255.0 alpha:1.0];
    lab_alertViewTitle.text=@"Taxi";
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
    //    if(isSocialLogin==YES)
    //    {
    //        UserProfileVC *vc = [[UserProfileVC alloc] init];
    //        [self.navigationController pushViewController:vc animated:NO];
    //    }
}
#pragma mark POPUP VALIDATION

-(void)popup_AlertviewValidation:(NSString *)message
{
    [alertviewBgBoth removeFromSuperview];
    alertviewBgBoth=[[UIView alloc] init];
    alertviewBgBoth.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBgBoth.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBgBoth];
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    alertViewBody.userInteractionEnabled=TRUE;
    [alertviewBgBoth addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:196.0/255.0 green:0.0/255.0 blue:52.0/255.0 alpha:1.0];
    lab_alertViewTitle.text=@"Taxi";
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIImageView *imageview_divH=[[UIImageView alloc] init];
    imageview_divH.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_divH];
    
    UIButton *btn_alertviewCancel=[[UIButton alloc] init];
    btn_alertviewCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewCancel.backgroundColor=[UIColor clearColor];
    [btn_alertviewCancel setTitle:@"No" forState:UIControlStateNormal];
    [btn_alertviewCancel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewCancel addTarget:self action:@selector(click_btn_alertviewCancel:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewCancel];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"Yes" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewYES:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    
    
    
    alertviewBgBoth.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),(HEIGHT/2)-70,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(0,45,WIDTH-100,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:14.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    imageview_divH.frame= CGRectMake((WIDTH/2-50),102,1,38);
    btn_alertviewOk.frame = CGRectMake((WIDTH/2-50)+1,100,WIDTH/2-50,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
    btn_alertviewCancel.frame = CGRectMake(0,100,WIDTH/2-50,40);
    [btn_alertviewCancel.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
    
}

-(void)click_btnAlertviewYES:(UIButton *)sender
{
    [alertviewBgBoth removeFromSuperview];
    
    if (isLogout==YES) {
        [self AFLogOut];
        
        
        
        
    }
    else
    {
        //LoginVC *vc =[[LoginVC alloc]init];
        //[self.navigationController pushViewController:vc animated:NO];
    }
}

-(void)click_btn_alertviewCancel:(UIButton *)sender
{
    [alertviewBgBoth removeFromSuperview];
}
#pragma mark -AF

-(void)AFLogOut
{
    [self.menuTableView setUserInteractionEnabled:NO];
    
    [self.view addSubview:delegate.activityIndicator];
    delegate.activityIndicator.color = [UIColor colorWithRed:79.0/255.0 green:81.0/255.0 blue:80.0/255.0 alpha:1.0];
    [delegate.activityIndicator startAnimating];
    //=========================================BASE URL=================
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=========================================REQUIRED PARAMETERS(ONLY TEXT)
    
    NSLog(@"id:%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"UserDetail"]);
    
    NSDictionary *params =@{
                            @"id"     :[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserDetail"] valueForKey:@"UserId"]
                            };
    
    
    //==========================================AFNETWORKING HEADER=====
    
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //========================================REQUEST====================
    
    //SIMPLE REQUEST WITHOUT FILE
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:klogout
                                                      parameters:params];
    
    //=========================================RESPONSE===================
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError *error = nil;
         NSArray *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
         if([operation.response statusCode] == 200)
         {
             
             [self.menuTableView setUserInteractionEnabled:YES];
             [delegate.activityIndicator stopAnimating];
             
             [self ResponseLogOut:JSON];
         }
     }
     //========================================ERROR=====
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         [self.menuTableView setUserInteractionEnabled:YES];
                                         if([operation.response statusCode] == 406){
                                             
                                             return;
                                         }
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Taxi"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if([[operation error] code] == -1001) {
                                             [self AFLogOut];
                                         }
                                     }];
    [operation start];
}


-(void) ResponseLogOut:(NSArray * )TheDict
{
    //txt_FullName.text=
    if([[TheDict valueForKey:@"error"] intValue] == 1)
    {
        [self popup_AlertviewForLogOut:[TheDict valueForKey:@"Message"]];
    }
    else
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults removeObjectForKey:@"UserDetail"];
        [defaults synchronize];
        //        FirstVC *login_VC = [[FirstVC alloc]init];
        //        [self presentViewController:login_VC animated:NO completion:nil];
        //        [delegate resetJWSlider];
    }
}

#pragma mark Alertview Popup

-(void)popup_AlertviewForLogOut:(NSString *)message
{
    [alertviewBg1 removeFromSuperview];
    
    alertviewBg1=[[UIView alloc] init];
    alertviewBg1.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg1.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg1];
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg1 addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:196.0/255.0 green:0.0/255.0 blue:52.0/255.0 alpha:1.0];
    lab_alertViewTitle.text=@"Taxi";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk1=[[UIButton alloc] init];
    btn_alertviewOk1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk1.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk1 setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk1 addTarget:self action:@selector(click_btnAlertviewOk1:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk1];
    
    
    alertviewBg1.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk1.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk1.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
    
    
    
    
}

-(void)click_btnAlertviewOk1:(UIButton *)sender
{
    [alertviewBg1 removeFromSuperview];
    if (isLogout==YES) {
        [self UserLogout];
    }
    //    LoginVC *vc = [[LoginVC alloc]init];
    //    //delegate.isGoBackFromMasterControl=YES;
    //    [self.navigationController pushViewController:vc animated:NO];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
