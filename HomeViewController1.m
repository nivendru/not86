//
//  HomeViewController.m
//  Not86
//
//  Created by Interwld on 8/3/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "HomeViewController1.h"
#import "LoginViewController.h"
#import "SignupViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"
#import "ChefSignup.h"



@interface HomeViewController1 ()<UITextFieldDelegate,UITextViewDelegate,UIScrollViewDelegate,FBLoginViewDelegate>
{
    UIImageView *img_BackgroundImg;
    UIImageView *img_topbar;
    NSMutableArray *ary_SocialInfo;
    NSString*str_fblogin;
    FBLoginView *loginview;
    AppDelegate *delegate;
    FBLoginView *FBloginVC;

}

@end

@implementation HomeViewController1
//static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
//static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
//static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
//static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 160;//216
//static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;
- (void)viewDidLoad {
    [super viewDidLoad];
    ary_SocialInfo = [NSMutableArray new];
    str_fblogin = [NSString new];
    [self IntegrateHeaderDesign];
    [self IntegrateView];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)IntegrateHeaderDesign
{
    img_BackgroundImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    [img_BackgroundImg setUserInteractionEnabled:YES];
    img_BackgroundImg.image=[UIImage imageNamed:@"background@2x.png"];
    [self.view addSubview:img_BackgroundImg];
    img_topbar=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,WIDTH,45)];
    [img_topbar setUserInteractionEnabled:YES];
    img_topbar.image=[UIImage imageNamed:@"img-head@2x.png"];
    //    img_topbar.layer.shadowColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:1].CGColor;
    //    img_topbar.layer.shadowOffset = CGSizeMake(0,10);
    //    img_topbar.layer.shadowOpacity = 0.3;
    //    img_topbar.layer.shadowRadius =1.0;
    [self.view addSubview:img_topbar];
    
    
    UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(10, 15, 15, 15)];
    [img_back setUserInteractionEnabled:YES];
    img_back.backgroundColor=[UIColor clearColor];
    img_back.image=[UIImage imageNamed:@"arrow@2x.png"];
    [img_topbar addSubview:img_back];
    
    
    
    UILabel  * lbl_heading = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(img_back.frame)+25,0, 220,45)];
    lbl_heading.text = @"Cook";
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:kFont size:20];
    [img_topbar addSubview:lbl_heading];
    
    
    UIButton *btn_back = [[UIButton alloc] init];
    btn_back.frame = CGRectMake(0, 0, 60,35);
    btn_back.backgroundColor = [UIColor clearColor];
    [btn_back addTarget:self action:@selector(Back_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [img_topbar addSubview:btn_back];
    
    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 5, 34, 32)];
    [img_logo setUserInteractionEnabled:YES];
    img_logo.backgroundColor=[UIColor clearColor];
    img_logo.image=[UIImage imageNamed:@"log@2x.png"];
    [img_topbar addSubview:img_logo];
    
}
-(void)IntegrateView
{
    
    UIImageView *imageView1 = [[UIImageView alloc] init];
    if(IS_IPHONE_6Plus)
    {
        imageView1.frame=CGRectMake(60,HEIGHT-275, 75,75);
    }
    else if(IS_IPHONE_6)
    {
        imageView1.frame=CGRectMake(60,HEIGHT-247, 65,65);
    }
    else
    {
        imageView1.frame=CGRectMake(IS_IPHONE_5?40:47,IS_IPHONE_5?HEIGHT-212:HEIGHT-178, IS_IPHONE_5?60:53,IS_IPHONE_5?60:48);
    }
    
    imageView1.image = [UIImage imageNamed:@"face@2x.png"];
    [imageView1 setUserInteractionEnabled:YES];
    [img_BackgroundImg addSubview:imageView1];
    
    UIButton *fbbtn = [[UIButton alloc] init];
    if(IS_IPHONE_6Plus)
    {
        fbbtn.frame = CGRectMake(0,0, 119,111);
    }
    else if(IS_IPHONE_6)
    {
        fbbtn.frame = CGRectMake(0,0,65,65);
    }
    else
    {
        fbbtn.frame = CGRectMake(0,0, IS_IPHONE_5?60:50,IS_IPHONE_5?60:50);
    }
    fbbtn.backgroundColor = [UIColor clearColor];
    // [fbbtn setImage:[UIImage imageNamed:@"face@2x.png"] forState:UIControlStateNormal];
    [fbbtn addTarget:self action:@selector(Click_fbbtn) forControlEvents:UIControlEventTouchUpInside ];
    [fbbtn setUserInteractionEnabled:YES];
    [imageView1 addSubview:fbbtn];
    
    UIImageView *imageView2 = [[UIImageView alloc] init];
    if(IS_IPHONE_6Plus)
    {
        imageView2.frame=CGRectMake(WIDTH-145,HEIGHT-275, 75,75);
    }
    else if(IS_IPHONE_6)
    {
        imageView2.frame=CGRectMake(WIDTH-130,HEIGHT-247, 65,65);
    }
    else
    {
        imageView2.frame=CGRectMake(IS_IPHONE_5?WIDTH-115:WIDTH-108,IS_IPHONE_5?HEIGHT-212:HEIGHT-178,IS_IPHONE_5?60:53,IS_IPHONE_5?60:48);
    }
    
    imageView2.image = [UIImage imageNamed:@"gplus@2x.png"];
    [imageView2 setUserInteractionEnabled:YES];
    [img_BackgroundImg addSubview:imageView2];
    
    UIButton *googlebtn = [[UIButton alloc] init];
    if(IS_IPHONE_6Plus)
    {
        googlebtn.frame = CGRectMake(0,0, 90,90);
    }
    else if(IS_IPHONE_6)
    {
        googlebtn.frame = CGRectMake(0,0, 70,70);
    }
    else
    {
        googlebtn.frame = CGRectMake(0,0, IS_IPHONE_5?60:50,IS_IPHONE_5?60:50);
    }
    googlebtn.backgroundColor = [UIColor clearColor];
    // [fbbtn setImage:[UIImage imageNamed:@"face@2x.png"] forState:UIControlStateNormal];
//    [googlebtn addTarget:self action:@selector(Click_googlebtn) forControlEvents:UIControlEventTouchUpInside ];
    [googlebtn setUserInteractionEnabled:YES];
    [imageView2 addSubview:googlebtn];
    
    UIImageView *imageView3 = [[UIImageView alloc] init];
    if(IS_IPHONE_6Plus)
    {
        imageView3.frame=CGRectMake(WIDTH/2-4.5,HEIGHT-270, 1,100);
    }
    else if(IS_IPHONE_6)
    {
        imageView3.frame=CGRectMake(WIDTH/2-3.5,HEIGHT-260, 1,100);
    }
    else
    {
        imageView3.frame=CGRectMake(WIDTH/2-2.5,IS_IPHONE_5?HEIGHT-220:HEIGHT-185, 0.3,IS_IPHONE_5?85:70);
    }
    imageView3.backgroundColor=[UIColor grayColor];
    [imageView3 setUserInteractionEnabled:YES];
    [img_BackgroundImg addSubview:imageView3];
    
    UIImageView *imageView4 = [[UIImageView alloc] init];
    if(IS_IPHONE_6Plus)
    {
        imageView4.frame=CGRectMake(16,HEIGHT-150, WIDTH-29,20);
    }
    else if(IS_IPHONE_6)
    {
        imageView4.frame=CGRectMake(16,HEIGHT-140, WIDTH-29,20);
    }
    else
    {
        imageView4.frame=CGRectMake(13,IS_IPHONE_5?HEIGHT-120:HEIGHT-105, WIDTH-23,20);
    }
    imageView4.image = [UIImage imageNamed:@"img-log-in@2x.png"];
    //imageView4.backgroundColor=[UIColor blackColor];
    [imageView4 setUserInteractionEnabled:YES];
    [img_BackgroundImg addSubview:imageView4];
    
    UIImageView *imageView5 = [[UIImageView alloc] init];
    if(IS_IPHONE_6Plus)
    {
        imageView5.frame=CGRectMake(15,HEIGHT-110, WIDTH/2-20,60);
    }
    else if(IS_IPHONE_6)
    {
        imageView5.frame=CGRectMake(16,HEIGHT-100, WIDTH/2-20,60);
    }
    else
    {
        imageView5.frame=CGRectMake(13,IS_IPHONE_5?HEIGHT-85:HEIGHT-80, WIDTH/2-15,50);
        
    }
    
    imageView5.image = [UIImage imageNamed:@"login@2x.png"];
    [imageView5 setUserInteractionEnabled:YES];
    [img_BackgroundImg addSubview:imageView5];
    
    UIButton *loginbtn = [[UIButton alloc] init];
    if(IS_IPHONE_6Plus)
    {
        loginbtn.frame = CGRectMake(0,0, WIDTH/2-30,55);
    }
    else if(IS_IPHONE_6)
    {
        loginbtn.frame = CGRectMake(0,0, WIDTH/2-20,55);
    }
    else
    {
        loginbtn.frame = CGRectMake(0,0, WIDTH/2-30,50);
    }
    
    loginbtn.backgroundColor = [UIColor clearColor];
    // [fbbtn setImage:[UIImage imageNamed:@"face@2x.png"] forState:UIControlStateNormal];
    [loginbtn addTarget:self action:@selector(Click_loginbtn) forControlEvents:UIControlEventTouchUpInside ];
    [loginbtn setUserInteractionEnabled:YES];
    [imageView5 addSubview:loginbtn];
    
    UIImageView *imageView6 = [[UIImageView alloc] init];
    if(IS_IPHONE_6Plus)
    {
        imageView6.frame=CGRectMake(CGRectGetMaxX(imageView5.frame)+15,HEIGHT-110, WIDTH/2-20,60);
    }
    else if(IS_IPHONE_6)
    {
        imageView6.frame=CGRectMake(CGRectGetMaxX(imageView5.frame)+10,HEIGHT-100, WIDTH/2-20,60);
    }
    else{
        imageView6.frame=CGRectMake(CGRectGetMaxX(imageView5.frame)+11,IS_IPHONE_5?HEIGHT-85:HEIGHT-80, WIDTH/2-19,50);
    }
    imageView6.image = [UIImage imageNamed:@"sign@2x.png"];
    [imageView6 setUserInteractionEnabled:YES];
    [img_BackgroundImg addSubview:imageView6];
    
    UIButton *signupbtn = [[UIButton alloc] init];
    if(IS_IPHONE_6Plus)
    {
        signupbtn.frame = CGRectMake(0,0, WIDTH/2-30,55);
    }
    else if(IS_IPHONE_6)
    {
        signupbtn.frame = CGRectMake(0,0, WIDTH/2-30,55);
    }
    else
    {
        signupbtn.frame = CGRectMake(0,0, WIDTH/2-30,50);
    }
    signupbtn.backgroundColor = [UIColor clearColor];
    // [fbbtn setImage:[UIImage imageNamed:@"face@2x.png"] forState:UIControlStateNormal];
    [signupbtn addTarget:self action:@selector(Click_signupbtn) forControlEvents:UIControlEventTouchUpInside ];
    [signupbtn setUserInteractionEnabled:YES];
    [imageView6 addSubview:signupbtn];
    
    
    UILabel  * lbl_heading = [[UILabel alloc]init];
    if(IS_IPHONE_6Plus)
    {
        lbl_heading.frame=CGRectMake(24,HEIGHT-45, WIDTH/2-20,20);
        lbl_heading.font=[UIFont fontWithName:kFont size:12];
    }
    else if(IS_IPHONE_6)
    {
        lbl_heading.frame=CGRectMake(24,HEIGHT-35, WIDTH/2-20,20);
        lbl_heading.font=[UIFont fontWithName:kFont size:11];
    }
    else
    {
        lbl_heading.frame=CGRectMake(21,IS_IPHONE_5?HEIGHT-30:HEIGHT-25, WIDTH/2-20,15);
        lbl_heading.font=[UIFont fontWithName:kFont size:9];
    }
    lbl_heading.text = @"Sign in to your not86 account";
    lbl_heading.textColor=[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1];
    
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    [img_BackgroundImg addSubview:lbl_heading];
    
    UILabel  * lbl_heading1 = [[UILabel alloc]init];
    if(IS_IPHONE_6Plus)
    {
        lbl_heading1.frame=CGRectMake(CGRectGetMaxX(lbl_heading.frame)+12,HEIGHT-45, WIDTH/2-20,20);
        lbl_heading1.font=[UIFont fontWithName:kFont size:12];
    }
    else if(IS_IPHONE_6)
    {
        lbl_heading1.frame=CGRectMake(CGRectGetMaxX(lbl_heading.frame)+5,HEIGHT-35, WIDTH/2-20,20);
        lbl_heading1.font=[UIFont fontWithName:kFont size:11];
    }
    else
    {
        lbl_heading1.frame=CGRectMake(CGRectGetMaxX(lbl_heading.frame)+10,IS_IPHONE_5?HEIGHT-30:HEIGHT-25, WIDTH/2-20,15);
        lbl_heading1.font=[UIFont fontWithName:kFont size:9];
    }
    lbl_heading1.text = @"Apply to become a not86 chef";
    lbl_heading1.textColor=[UIColor colorWithRed:33/255.0f green:31/255.0f blue:40/255.0f alpha:1];
    lbl_heading1.textAlignment=NSTextAlignmentLeft;
    
    [img_BackgroundImg addSubview:lbl_heading1];
    
    
    
}
-(void)Back_btnClick
{
    NSLog(@"Back_btnClick");
//    [self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
}
-(void)Click_fbbtn
{
    NSLog(@"img_fb_click Btn Click");
    
    [FBSession.activeSession close];
    [FBSession.activeSession  closeAndClearTokenInformation];
    FBSession.activeSession=nil;
    
    FBloginVC = [[FBLoginView alloc] init];
    
    FBloginVC.frame = CGRectMake(0, 0, 0, 0);
    
    FBloginVC.delegate = self;
    FBloginVC.readPermissions = @[@"public_profile", @"email"];
    
    [self.view addSubview:FBloginVC];
    
    for (id obj in FBloginVC.subviews)
    {
        if ([obj isKindOfClass:[UIButton class]])
        {
            UIButton * loginButton =  obj;
            loginButton.frame =  CGRectMake(0, 0, 0, 0);
            
            [loginButton sendActionsForControlEvents: UIControlEventTouchUpInside];
            
        }
        if ([obj isKindOfClass:[UILabel class]])
        {
            UILabel * loginLabel =  obj;
            loginLabel.frame = CGRectMake(0, 0, 0, 0);
        }
    }
    
    
    
    
}
//-(void)Click_googlebtn
//{
//    [self AFSocialGoogleLogin];
//}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}
-(void)Click_loginbtn
{
    LoginViewController *vc=[[LoginViewController alloc]init];
    [self presentViewController:vc animated:NO completion:nil];
    
//    [self.navigationController pushViewController:vc animated:NO];
    
}
-(void)Click_signupbtn
{
    SignupViewController *vc=[[SignupViewController alloc]init];
   [self presentViewController:vc animated:NO completion:nil];

    //   vc.ary_fblogindetails = ary_SocialInfo;
//    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark - FBLoginViewDelegate


#pragma mark ------------------------- FACEBOOK INTEGRATION ------------------------Start

#pragma mark - FBLoginViewDelegate

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView
{
    

    
    // "Post Status" available when logged on and potentially when logged off.  Differentiate in the label.
}


- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user
{
    // here we use helper properties of FBGraphUser to dot-through to first_name and
    // id properties of the json response from the server; alternatively we could use
    // NSDictionary methods such as objectForKey to get values from the my json object
    
    NSLog(@"usr_id::%@",user.id);
    NSLog(@"usr_first_name::%@",user.first_name);
    NSLog(@"usr_middle_name::%@",user.middle_name);
    NSLog(@"usr_last_nmae::%@",user.last_name);
    NSLog(@"usr_Username::%@",user.username);
    NSLog(@"usr_b_day::%@",user.birthday);
    
    NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [user objectForKey:@"id"]];

    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [dict setObject:[user objectForKey:@"id"] forKey:@"id"];
    [dict setObject:[user objectForKey:@"name"] forKey:@"name"];
    //    NSString *displayName = [NSString stringWithFormat:@"%@%@",[user objectForKey:@"first_name"],[user objectForKey:@"id"]];
    //    [dict setObject:displayName forKey:@"username"];
    //    [dict setObject:[user objectForKey:@"gender"] forKey:@"gender"];
    //    [dict setObject:[user objectForKey:@"email"] forKey:@"email"];
    //    [dict setObject:userImageURL forKey:@"profilepic"];
    [dict setObject:@"facebook" forKey:@"type"];
    
    
    [ary_SocialInfo removeAllObjects];
    [ary_SocialInfo addObject:dict];
    [FBloginVC removeFromSuperview];
    
    delegate.login_type = @"FACEBOOK";
    ChefSignup *userSignUp = [[ChefSignup alloc]init];
    userSignUp.array_Facebook_Details = ary_SocialInfo;
    [self presentViewController:userSignUp animated:NO completion:nil];
//    [self.navigationController pushViewController:userSignUp animated:YES];

    //    appDelegate().isFBLogin = YES;
    //
    //    if (appDelegate().isFBLogin) {
    //        UserSignUpVC *userSignUp = [[UserSignUpVC alloc]init];
    //        [self.navigationController pushViewController:userSignUp animated:YES];
    //        userSignUp.array_Facebook_Details = ary_SocialInfo;
    //
    //    }
    
}
- (void)loginView:(FBLoginView *)loginView
      handleError:(NSError *)error
{
//       [delegate.load removeFromSuperview];
}




//- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView
//{
//    
//    
//    // "Post Status" available when logged on and potentially when logged off.  Differentiate in the label.
//}
//
//- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
//                            user:(id<FBGraphUser>)user
//{
//    
//    [self.view addSubview:delegate.activityIndicator];
//    [delegate.activityIndicator startAnimating];
//    NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [user objectForKey:@"id"]];
//    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
//    [dict setObject:[user objectForKey:@"id"] forKey:@"id"];
//    [dict setObject:[user objectForKey:@"name"] forKey:@"username"];
//    NSString *displayName = [NSString stringWithFormat:@"%@%@",[user objectForKey:@"first_name"],[user objectForKey:@"last_name"]];
//    [dict setObject:displayName forKey:@"DisplayName"];
//    [dict setObject:[user objectForKey:@"gender"] forKey:@"gender"];
//    [dict setObject:[user objectForKey:@"email"] forKey:@"email"];
//    [dict setObject:userImageURL forKey:@"profilepic"];
//    [dict setObject:@"facebook" forKey:@"type"];
//    [ary_SocialInfo removeAllObjects];
//    [ary_SocialInfo addObject:dict];
//    [loginview removeFromSuperview];
//    [self AFSocialLogin];
//    
//}


#pragma mark - AF SocialLogin

-(void) AFSocialLogin
{
    
    UIDevice *myDevice=[UIDevice currentDevice];
    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
    
    
    
    //=================================================================BASE URL
    NSURL *url = [NSURL URLWithString:@""];
    
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]])
    {
        str_device_token = @"dev-1234";
    }
    
    NSDictionary *params =@{
                            
                            @"username"       :  [[ary_SocialInfo objectAtIndex:0]valueForKey:@"username"],
                            @"mail"           :   [[ary_SocialInfo objectAtIndex:0]valueForKey:@"email"],
                            @"fbid"           :   [[ary_SocialInfo objectAtIndex:0]valueForKey:@"id"],
                            @"name"           :   [[ary_SocialInfo objectAtIndex:0]valueForKey:@"DisplayName"],
                            @"udid"           :  UniqueAppID,
                            @"token"          :  str_device_token,
                            @"role_type"     :   @"chef_profile",
                            @"type"           :  @"1",
                            
                            
                            };
    
    //  =================================================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //=================================================================USED FOR UNIQUE FILE NAME AND CONVERT TO DATA
    
    
    //REQUEST WITH FILE
    
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"" parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[ary_SocialInfo objectAtIndex:0]valueForKey:@"profilepic"]]];
        
        NSTimeInterval timeInterval = [NSDate timeIntervalSinceReferenceDate];
        NSString *photoName=[NSString stringWithFormat:@"%lf-Photo.jpeg",timeInterval];
        
        [formData appendPartWithFileData:data name:@"profile_img" fileName:photoName mimeType:@"image/jpeg"];
    }];
    
    //============================================================RESPONSE
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
         NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
     }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseAFSocialLogin:JSON];
    }
     //=================================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Metta"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001)
                                         {
                                             [self AFSocialLogin];
                                         }
                                     }];
    
    [operation start];
}


-(void) ResponseAFSocialLogin :(NSDictionary * ) TheDict
{
    //    NSLog(@"SocialLogin%@",TheDict);
    
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]]isEqualToString:@"0"])
    {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[TheDict valueForKey:@"user_info"]  forKey:@"UserInfo"];
        [defaults synchronize];
        
        
        
        ChefSignup *chefsignup=[[ChefSignup alloc]init];
        //     [self.navigationController pushViewController:chefsignup animated:NO];
        [self presentViewController:chefsignup animated:NO completion:nil];
        
        
        //[self AFSocialUserCheck];
        
        
        
    }
    else if([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]]isEqualToString:@"1"])
    {
        if (![[TheDict valueForKey:@"user_info"] isKindOfClass:[NSNull class]])
        {
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[TheDict valueForKey:@"user_info"]  forKey:@"UserInfo"];
            [defaults synchronize];
            
            delegate.isFBLogin=YES;
            
            
            //                        vc.ary_fblogindetails = ary_SocialInfo;
            //            [self.navigationController pushViewController:vc animated:NO];
            
        }
        if (![[TheDict valueForKey:@"user_info"] isKindOfClass:[NSNull class]])
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[TheDict valueForKey:@"user_info"]  forKey:@"UserInfo"];
            [defaults synchronize];
        }
    }
}


#pragma mark - AF SocialLogin


//-(void)Click_googlebtn
//{
//    delegate.str_LoginType = @"google";
//    
//    [self.view addSubview:delegate.activityIndicator];
//    [delegate.activityIndicator startAnimating];
//    
//    GPPSignIn *signIn = [GPPSignIn sharedInstance];
//    signIn.delegate = self;
//    signIn.shouldFetchGoogleUserID = YES;
//    signIn.shouldFetchGoogleUserEmail = YES;
//    signIn.clientID = kClientId;
//    signIn.scopes = [NSArray arrayWithObjects:kGTLAuthScopePlusLogin,kGTLAuthScopePlusMe,nil];
//    signIn.actions = [NSArray arrayWithObjects:@"http://schemas.google.com/ListenActivity",nil];
//    [GPPDeepLink setDelegate:self];
//    [GPPDeepLink readDeepLinkAfterInstall];
//    [signIn authenticate];
//}
//
//- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth error: (NSError *) error
//{
//    [delegate.activityIndicator stopAnimating];
//    
//    if(!error) {
//        
//        [self GetLoggedInUserInfoWithAccessToken:auth.accessToken];
//        
//    }
//    else {
//    }
//}
//
//- (void)didReceiveDeepLink:(GPPDeepLink *)deepLink
//{
//    //NSLog(@"%@", deepLink);
//}
//
//- (void)didDisconnectWithError:(NSError *)error {
//    if (error) {
//        //        _signInAuthStatus.text =
//        NSLog(@"-- %@",[NSString stringWithFormat:@"Status: Failed to disconnect: %@", error]);
//    } else {
//        //        _signInAuthStatus.text =
//        NSLog(@"-- %@",[NSString stringWithFormat:@"Status: Disconnected"]);
//    }
//    if (error) {
//        NSLog(@"Received error %@", error);
//    } else {
//        // The user is signed out and disconnected.
//        // Clean up user data as specified by the Google+ terms.
//        //        [self refreshInterfaceBasedOnSignIn];
//    }
//    
//}
//
//-(void) GetLoggedInUserInfoWithAccessToken:(NSString *) strToken{
//    
//    
//    NSString *PeopleMe = [NSString stringWithFormat:@"https://www.googleapis.com/plus/v1/people/me"];
//    NSURL *url = [NSURL URLWithString:PeopleMe];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    
//    [[GPPSignIn sharedInstance].authentication  authorizeRequest:request
//                                               completionHandler:^(NSError *error) {
//                                                   
//                                                   
//                                                   
//                                                   NSString *output = nil;
//                                                   if (error)
//                                                   {
//                                                       output = [error description];
//                                                   } else {
//                                                       // Synchronous fetches like this are a really bad idea in Cocoa applications
//                                                       //
//                                                       // For a very easy async alternative, we could use GTMHTTPFetcher
//                                                       NSURLResponse *response = nil;
//                                                       NSData *data = [NSURLConnection sendSynchronousRequest:request
//                                                                                            returningResponse:&response
//                                                                                                        error:&error];
//                                                       if (data) {
//                                                           // API fetch succeeded
//                                                           //                          output = [[NSString alloc] initWithData:data
//                                                           //                                                          encoding:NSUTF8StringEncoding];
//                                                           
//                                                           //[indicator stopAnimating];
//                                                           NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
//                                                           
//                                                           
//                                                           NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
//                                                           
//                                                           [dict setObject:[JSON objectForKey:@"id"] forKey:@"id"];
//                                                           [dict setObject:[JSON objectForKey:@"displayName"] forKey:@"username"];
//                                                           [dict setObject:[[[JSON valueForKey:@"emails"]valueForKey:@"value"]objectAtIndex:0] forKey:@"email"];
//                                                           [dict setObject:[[JSON valueForKey:@"image"] valueForKey:@"url"] forKey:@"profilepic"];
//                                                           
//                                                           
//                                                           [ary_SocialInfo removeAllObjects];
//                                                           
//                                                           [ary_SocialInfo addObject:dict];
//                                                           
//                                                           
//                                                           [self AFSocialLogin];
//                                                           
//                                                           
//                                                           
//                                                           
//                                                       } else {
//                                                           
//                                                           //[indicator stopAnimating];
//                                                           // fetch failed
//                                                           //  NSLog(@"error:%@",[error description]);
//                                                       }
//                                                   }
//                                                   
//                                               }];
//}
//
//#pragma mark ------------------------- GOOGLE PLUS DELEGATE METHODS INTEGRATION ---END
//
//#pragma mark - AF
//
//-(void) AFSocialGoogleLogin
//{
//    
//    UIDevice *myDevice=[UIDevice currentDevice];
//    NSString *UniqueAppID = [[myDevice identifierForVendor] UUIDString];
//    
//    
//    
//    //=================================================================BASE URL
//    NSURL *url = [NSURL URLWithString:kBaseUrl];
//    
//    //=================================================================USED PARAMETERS(ONLY TEXT)
//    
//    
//    NSString *str_device_token = [delegate.devicestr stringByReplacingOccurrencesOfString:@" " withString:@""];
//    
//    if (!str_device_token || [str_device_token isKindOfClass:[NSNull class]]){
//        str_device_token = @"";
//        
//    }
//    
//    NSDictionary *params =@{
//                            
//                            @"register_from"         :   @"2",
//                            @"socialsite_udid"    :   [[ary_SocialInfo objectAtIndex:0]valueForKey:@"id"],
//                            @"devicetoken"    :    str_device_token,
//                            @"deviceudid"    :    UniqueAppID,
//                            @"email"      :   [[ary_SocialInfo objectAtIndex:0]valueForKey:@"email"],
//                            @"username"      :  [[ary_SocialInfo objectAtIndex:0]valueForKey:@"username"],
//                            @"devicetype"    :@"1"
//                            
//                            };
//    
//    //  =================================================================AFNETWORKING HEADER
//    
//    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
//    httpClient.parameterEncoding = AFFormURLParameterEncoding;
//    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
//    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
//    
//    //=================================================================USED FOR UNIQUE FILE NAME AND CONVERT TO DATA
//    
//    
//    //REQUEST WITH FILE
//    
//    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:kFacebookLogin parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
//        
//        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[ary_SocialInfo objectAtIndex:0]valueForKey:@"profilepic"]]];
//        
//        NSTimeInterval timeInterval = [NSDate timeIntervalSinceReferenceDate];
//        NSString *photoName=[NSString stringWithFormat:@"%lf-Photo.jpeg",timeInterval];
//        
//        [formData appendPartWithFileData:data name:@"image" fileName:photoName mimeType:@"image/jpeg"];
//    }];
//    
//    //============================================================RESPONSE
//    
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
//    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
//     {
//         NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
//     }];
//    
//    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSError *error = nil;
//        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
//        
//        [delegate.activityIndicator stopAnimating];
//        [self ResponseAFSocialGoogleLogin:JSON];
//    }
//     //=================================================================ERROR
//                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                         
//                                         [delegate.activityIndicator stopAnimating];
//                                         
//                                         if([operation.response statusCode] == 403){
//                                             NSLog(@"Upload Failed");
//                                             return;
//                                         }
//                                         
//                                         if ([[operation error] code] == -1009) {
//                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"The List"
//                                                                                          message:@"Please check your internet connection"
//                                                                                         delegate:nil
//                                                                                cancelButtonTitle:@"OK"
//                                                                                otherButtonTitles:nil];
//                                             [av show];
//                                         }
//                                         else if ([[operation error] code] == -1001)
//                                         {
//                                             [self AFSocialGoogleLogin];
//                                         }
//                                     }];
//    
//    [operation start];
//}
//
//
//-(void) ResponseAFSocialGoogleLogin :(NSDictionary * ) TheDict{
//    
//    
//    NSLog(@"%@",TheDict);
//    
//    
//    if ([[TheDict valueForKey:@"error"] intValue] == 0) {
//        
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        [defaults setObject:[TheDict valueForKey:@"UserInfo"]  forKey:@"UserInfo"];
//        [defaults synchronize];
//        
//        
//        NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
//        [defaults1 setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:@"UserInfo"] valueForKey:@"second_degree_bookmarked_view"]  forKey:@"BookmarkSecondDegree"];
//        [defaults1 synchronize];
//        
//        [self integrateJWSlider];
//        
//    }
//    else
//    {
//        [SVProgressHUD showErrorWithStatus:[TheDict valueForKey:@"Message"]];
//        
//    }
//}



@end
