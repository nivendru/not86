//
//  MyProfileVC.h
//  Not86
//
//  Created by Interwld on 9/1/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

@interface MyProfileVC : JWSlideMenuViewController
{
    
}
@property (readwrite, retain) MPMoviePlayerController *moviePlayer;

@end
