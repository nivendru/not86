//
//  ChefHomeViewController.m
//  Not86
//
//  Created by Interwld on 8/3/15.
//  Copyright (c) 2015 com.interworld. All rights reserved.
//

#import "ChefHomeViewController.h"
#import "MenuScreenVC.h"
#import "MyScheduleScreenVC.h"
#import "MyProfileVC.h"
#import "MyHomescreenVC.h"
#import "MyScheduleVC.h"

#import "MyMenuVC.h"
#import "MyAccountVC.h"
#import "MessagesVC.h"
#import "SwitchtoUser.h"
#import "JWSlideMenuViewController.h"

#import "ChefSupportsVC.h"

#import "JWSlideMenuController.h"

#import "AccountVC.h"
#import "ChefServeLaterVC.h"
#import "MyordersVC.h"
#import "MenuScreenAddDishVC.h"
#import "ViewMenuVC.h"

#import "AppDelegate.h"
#import "ChefOrdersVC.h"
#import <Social/Social.h>
#import "MGInstagram.h"

@interface ChefHomeViewController ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UITextViewDelegate>
{
    UIImageView *img_topbar;
    UIImageView *img_BackgroundImg;
    UITableView *img_table;
    UITableView*table_on_drop_down;
    
    
    NSArray *menuItems1;
    UITableView *tableList;
    BOOL isVal;
    UIScrollView *scrollview5;
    
    NSMutableArray*ary_dishList;
    NSMutableArray*ary_DishRestrictions;
    NSMutableArray*ary_Serving_Type;
    
    AppDelegate*delegate;
    UIView*alertviewBg;
    NSMutableArray*ary_qualityalert;
    
    
    UIImageView *line_Emptyimage;
    UILabel  * lbl_Quality;
    UIButton *btn_Quality;
    UIButton *btn_delete;
    UIImageView *line;
    UILabel*lbl_ServingLow;
    UIImageView *Arrow_left;
    UIButton *btn_Arrowleft;
    UIImageView *Arrow_right;
    UIButton *btn_Arrowright;
    UILabel  * lbl_Todayorders;
    UIImageView *img_Background;
    UIView*view_popnoorderss;
    NSMutableArray*array_head_names;
    
    
    
}

@end

@implementation ChefHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ary_dishList = [NSMutableArray new];
    ary_DishRestrictions = [NSMutableArray new];
    ary_Serving_Type = [NSMutableArray new];
    ary_qualityalert = [NSMutableArray new];
    array_head_names = [[NSMutableArray alloc]initWithObjects:@"Facebook",@"Instagram",@"Twitter", nil];
    
    [self IntegrateBodyDesign];
    [self IntegrateHeaderDesign];
    // [self popup_noorders];
    
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //    [self AFQualityalert];
    //    [self AFUserDishLists];
    
}
-(void)IntegrateHeaderDesign{
    
    img_topbar=[[UIImageView alloc]init];
    img_topbar.frame=CGRectMake(0, 0,WIDTH,45);
    [img_topbar setUserInteractionEnabled:YES];
    img_topbar.image=[UIImage imageNamed:@"img-head@2x.png"];
    [self.view addSubview:img_topbar];
    
    
    UIImageView *img_back=[[UIImageView alloc]initWithFrame:CGRectMake(10, 13, 25, 17)];
    [img_back setUserInteractionEnabled:YES];
    img_back.backgroundColor=[UIColor clearColor];
    img_back.image=[UIImage imageNamed:@"icon_menu@2x.png"];
    // [img_topbar addSubview:img_back];
    
    UIButton *btn_HomeScreen = [[UIButton alloc] init];
    btn_HomeScreen.frame = CGRectMake(0, 0, 25, 17);
    btn_HomeScreen.backgroundColor = [UIColor clearColor];
    [btn_HomeScreen addTarget:self action:@selector(btn_HomeScreenClick:) forControlEvents:UIControlEventTouchUpInside ];
    // [img_back addSubview:btn_HomeScreen];
    
    
    
    
    UILabel  * lbl_heading = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(img_back.frame)+23,5, 220,30)];
    lbl_heading.text = @"Home";
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:kFont size:18];
    [img_topbar addSubview:lbl_heading];
    
    UIImageView *imgback=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-70, 10, 20, 25)];
    [imgback setUserInteractionEnabled:YES];
    imgback.backgroundColor=[UIColor clearColor];
    imgback.image=[UIImage imageNamed:@"img_icon.png"];
    [img_topbar addSubview:imgback];
    
    
    UIButton *icon_share = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_share .frame = CGRectMake(WIDTH-70, 10, 20, 25);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_share  addTarget:self action:@selector(click_on_share_btn:) forControlEvents:UIControlEventTouchUpInside];
    [img_topbar   addSubview:icon_share ];
    
    
    UIButton *icon_logo = [UIButton buttonWithType:UIButtonTypeCustom];
    icon_logo .frame = CGRectMake(WIDTH-40, 5,35,35);
    //icon_menu .backgroundColor = [UIColor clearColor];
    [icon_logo  addTarget:self action:@selector(click_on_logo_btn:) forControlEvents:UIControlEventTouchUpInside];
    [icon_logo setImage:[UIImage imageNamed:@"logo-img@2x.png"] forState:UIControlStateNormal];
    [img_topbar   addSubview:icon_logo ];
    
    
    
    //    UIButton *icon_drop_down = [UIButton buttonWithType:UIButtonTypeCustom];
    //    icon_drop_down.frame = CGRectMake(WIDTH-40, 5,35,35);
    //    icon_drop_down .backgroundColor = [UIColor clearColor];
    //    [icon_drop_down addTarget:self action:@selector(btn_drop_down:) forControlEvents:UIControlEventTouchUpInside];
    //    [img_topbar   addSubview:icon_drop_down];
    //
    
    
    
    table_on_drop_down = [[UITableView alloc] init ];
    table_on_drop_down .frame  = CGRectMake(WIDTH-160,45,120,120);
    [ table_on_drop_down  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table_on_drop_down .delegate = self;
    table_on_drop_down .dataSource = self;
    table_on_drop_down .showsVerticalScrollIndicator = NO;
    table_on_drop_down .backgroundColor = [UIColor clearColor];
    table_on_drop_down.hidden = YES;
    [self.view addSubview: table_on_drop_down ];
    
    
    
    //    UIImageView *img_logo=[[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-40, 5, 35, 35)];
    //    [img_logo setUserInteractionEnabled:YES];
    //    img_logo.backgroundColor=[UIColor clearColor];
    //    img_logo.image=[UIImage imageNamed:@"img-logo.png"];
    //    [img_topbar addSubview:img_logo];
    
}
-(void)IntegrateBodyDesign{
    
    img_Background=[[UIImageView alloc]initWithFrame:CGRectMake(0, 45, WIDTH, 135)];
    [img_Background setUserInteractionEnabled:YES];
    img_Background.backgroundColor=[UIColor clearColor];
    img_Background.image=[UIImage imageNamed:@"background_image.png"];
    [self.view addSubview:img_Background];
    
    
    
    UIButton *btn_savenow= [[UIButton alloc] init];
    if (IS_IPHONE_6Plus) {
        
        btn_savenow.frame = CGRectMake(25, 15, 155, 60);
    }else if (IS_IPHONE_6)
    {
        btn_savenow.frame = CGRectMake(25, 15, 145, 55);
    }
    else{
        btn_savenow.frame = CGRectMake(25, 15, 125, 45);
    }
    btn_savenow.backgroundColor = [UIColor clearColor];
    [btn_savenow setBackgroundImage:[UIImage imageNamed:@"img-save-now.png"] forState:UIControlStateNormal];
    //    [btn_Menu setTitle:@"MENU" forState:UIControlStateNormal];
    
    [btn_savenow addTarget:self action:@selector(clickedonServeNow) forControlEvents:UIControlEventTouchUpInside ];
    [img_Background addSubview:btn_savenow];
    
    //    These is servingTillnowbuttoncode
    
    //    UIButton *btn_savenow= [[UIButton alloc] init];
    //    if (IS_IPHONE_6Plus) {
    //
    //        btn_savenow.frame = CGRectMake(25, 20, 160, 70);
    //    }else if (IS_IPHONE_6)
    //    {
    //        btn_savenow.frame = CGRectMake(25, 15, 145, 55);
    //    }
    //    else{
    //        btn_savenow.frame = CGRectMake(15, 20, 130, 50);
    //    }
    //    btn_savenow.backgroundColor = [UIColor clearColor];
    //    [btn_savenow setBackgroundImage:[UIImage imageNamed:@"ServingNow_Till.png"] forState:UIControlStateNormal];
    //    //    [btn_Menu setTitle:@"MENU" forState:UIControlStateNormal];
    //
    //    [btn_savenow addTarget:self action:@selector(clickedonServeNow) forControlEvents:UIControlEventTouchUpInside ];
    //    [img_Background addSubview:btn_savenow];
    //
    
    
    
    
    
    UIButton *btn_savelater = [[UIButton alloc]init];
    if (IS_IPHONE_6Plus) {
        
        btn_savelater.frame = CGRectMake(self.view.frame.size.width-180, 15, 150, 60);
    }
    else if (IS_IPHONE_6)
    {
        btn_savelater.frame = CGRectMake(self.view.frame.size.width-175, 15, 145, 55);
    }
    else{
        btn_savelater.frame = CGRectMake(self.view.frame.size.width-150, 15, 125, 45);
    }
    
    
    
    btn_savelater.backgroundColor = [UIColor clearColor];
    [btn_savelater setBackgroundImage:[UIImage imageNamed:@"img-save-later.png"] forState:UIControlStateNormal];
    [btn_savelater addTarget:self action:@selector(btn_savelater_Method) forControlEvents:UIControlEventTouchUpInside ];
    [img_Background addSubview:btn_savelater];
    
    
    
    UILabel  * lbl_heading = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus) {
        
        lbl_heading.frame = CGRectMake(35,CGRectGetMaxY(btn_savenow.frame)+15, 60,35);
    }
    else if (IS_IPHONE_6)
    {
        lbl_heading.frame = CGRectMake(35,CGRectGetMaxY(btn_savenow.frame)+20, 60,35);
    }
    else{
        lbl_heading.frame =CGRectMake(35,CGRectGetMaxY(btn_savenow.frame)+20, 60,35);
    }
    
    lbl_heading.text = @"MENU";
    lbl_heading.backgroundColor=[UIColor clearColor];
    lbl_heading.textColor=[UIColor whiteColor];
    lbl_heading.textAlignment=NSTextAlignmentLeft;
    lbl_heading.font = [UIFont fontWithName:kFont size:16];
    [img_Background addSubview:lbl_heading];
    
    
    
    UIButton *btn_Menu= [[UIButton alloc] init];
    btn_Menu.frame = CGRectMake(35, CGRectGetMaxY(btn_savenow.frame)+20, 60,35);
    btn_Menu.backgroundColor = [UIColor clearColor];
    
    [btn_Menu addTarget:self action:@selector(click_MenuScreen) forControlEvents:UIControlEventTouchUpInside];
    [img_Background addSubview:btn_Menu];
    
    
    
    UIImageView *line_IMG=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus) {
        
        line_IMG.frame=CGRectMake(110, CGRectGetMaxY(btn_savenow.frame)+14, 2, 35);
    }else if (IS_IPHONE_6)
        
    {
        line_IMG.frame=CGRectMake(110, CGRectGetMaxY(btn_savenow.frame)+20, 2, 35);
    }
    else{
        line_IMG.frame=CGRectMake(95, CGRectGetMaxY(btn_savenow.frame)+22, 2, 35);
        
    }
    
    
    [line_IMG setUserInteractionEnabled:YES];
    line_IMG.backgroundColor=[UIColor lightGrayColor];
    [img_Background addSubview:line_IMG];
    
    
    
    
    UILabel  * lbl_Schedule = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus) {
        
        lbl_Schedule.frame =CGRectMake(self.view.frame.size.width/2-50,CGRectGetMaxY(btn_savenow.frame)+15, 100,35);
    }
    else if (IS_IPHONE_6)
    {
        lbl_Schedule.frame = CGRectMake(self.view.frame.size.width/2-50,CGRectGetMaxY(btn_savenow.frame)+20, 100,35);
    }
    else{
        lbl_Schedule.frame = CGRectMake(self.view.frame.size.width/2-45,CGRectGetMaxY(btn_savenow.frame)+20, 100,35);
    }
    
    lbl_Schedule.text = @"SCHEDULE";
    lbl_Schedule.backgroundColor=[UIColor clearColor];
    lbl_Schedule.textColor=[UIColor whiteColor];
    lbl_Schedule.textAlignment=NSTextAlignmentLeft;
    lbl_Schedule.font = [UIFont fontWithName:kFont size:16];
    [img_Background addSubview:lbl_Schedule];
    
    
    
    UIButton *btn_Schedule= [[UIButton alloc] init];
    btn_Schedule.frame = CGRectMake(135, CGRectGetMaxY(btn_savenow.frame)+20, 100,35);
    btn_Schedule.backgroundColor = [UIColor clearColor];
    
    [img_Background addSubview:btn_Schedule];
    [btn_Schedule addTarget:self action:@selector(click_ScheduleScreen) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIImageView *line_IMGE=[[UIImageView alloc]init];
    
    
    if (IS_IPHONE_6Plus) {
        
        line_IMGE.frame=CGRectMake(lbl_Schedule.frame.origin.x+lbl_Schedule.frame.size.width+20, CGRectGetMaxY(btn_savenow.frame)+14, 2, 35);
        
    }else if (IS_IPHONE_6)
        
    {
        line_IMGE.frame=CGRectMake(lbl_Schedule.frame.origin.x+lbl_Schedule.frame.size.width+10, CGRectGetMaxY(btn_savenow.frame)+17, 2, 35);
    }
    else{
        line_IMGE.frame=CGRectMake(lbl_Schedule.frame.origin.x+lbl_Schedule.frame.size.width-4, CGRectGetMaxY(btn_savenow.frame)+22, 2, 35);
        
    }
    
    [line_IMGE setUserInteractionEnabled:YES];
    line_IMGE.backgroundColor=[UIColor whiteColor];
    [img_Background addSubview:line_IMGE];
    
    
    
    UILabel  * lbl_Orders = [[UILabel alloc]init];
    if (IS_IPHONE_6Plus) {
        
        lbl_Orders.frame= CGRectMake(self.view.frame.size.width-120,CGRectGetMaxY(btn_savenow.frame)+15, 100,35);
        
    }else if (IS_IPHONE_6)
        
    {
        lbl_Orders.frame= CGRectMake(self.view.frame.size.width-120,CGRectGetMaxY(btn_savenow.frame)+20, 100,35);
    }
    else{
        lbl_Orders.frame= CGRectMake(self.view.frame.size.width-110,CGRectGetMaxY(btn_savenow.frame)+20, 100,35);
        
    }
    
    
    lbl_Orders.text = @"ORDERS";
    lbl_Orders.backgroundColor=[UIColor clearColor];
    lbl_Orders.textColor=[UIColor whiteColor];
    lbl_Orders.textAlignment=NSTextAlignmentCenter;
    lbl_Orders.font = [UIFont fontWithName:kFont size:16];
    [img_Background addSubview:lbl_Orders];
    
    
    UIButton *btn_Orders= [[UIButton alloc] init];
    btn_Orders.frame = CGRectMake(self.view.frame.size.width-120, CGRectGetMaxY(btn_savenow.frame)+10, 100,50);
    btn_Orders.backgroundColor = [UIColor clearColor];
    [btn_Orders addTarget:self action:@selector(click_orders) forControlEvents:UIControlEventTouchUpInside ];
    [img_Background addSubview:btn_Orders];
    
    
    //    These is quantity alert screen code /*
    
    
    
    
    
    
    
    
    scrollview5=[[UIScrollView alloc]init];
    
    if (IS_IPHONE_6Plus)
    {
        
        scrollview5.frame=CGRectMake(0, CGRectGetMaxY(img_Background.frame)+2, WIDTH, HEIGHT);
        
        
    }else if (IS_IPHONE_6)
        
    {
        scrollview5.frame=CGRectMake(0, CGRectGetMaxY(img_Background.frame)+2, WIDTH, HEIGHT);
        
        
    }
    else{
        scrollview5.frame=CGRectMake(0, CGRectGetMaxY(img_Background.frame)+2, WIDTH, HEIGHT);
        
    }
    
    [scrollview5 setShowsVerticalScrollIndicator:NO];
    scrollview5.delegate = self;
    scrollview5.scrollEnabled = YES;
    scrollview5.showsVerticalScrollIndicator = NO;
    [scrollview5 setUserInteractionEnabled:YES];
    scrollview5.backgroundColor =  [UIColor colorWithRed:231/255.0f green:231/255.0f blue:231/255.0f alpha:1];
    [self.view addSubview:scrollview5];
    [scrollview5 setContentSize:CGSizeMake(0,HEIGHT+250)];
    
    
    
    
    line_Emptyimage=[[UIImageView alloc]init];
    line_Emptyimage.frame=CGRectMake(0, 0, WIDTH-15, 150);
    if (IS_IPHONE_6Plus)
    {
        line_Emptyimage.frame=CGRectMake(5, 0, WIDTH-8, 150);
        
    }
    else if (IS_IPHONE_6)
    {
        line_Emptyimage.frame=CGRectMake(5, 0, WIDTH-8, 150);
        
    }
    else
    {
        line_Emptyimage.frame=CGRectMake(5, 0, WIDTH-8, 150);
        
    }
    [line_Emptyimage setUserInteractionEnabled:YES];
    line_Emptyimage.backgroundColor=[UIColor clearColor];
    line_Emptyimage.image=[UIImage imageNamed:@"bg1.png"];
    [scrollview5 addSubview:line_Emptyimage];
    
    
    lbl_Quality = [[UILabel alloc]init];
    
    if (IS_IPHONE_6Plus) {
        
        lbl_Quality.frame=CGRectMake(10,0,WIDTH-70,35);
        lbl_Quality.font = [UIFont fontWithName:kFontBold size:17];
        
        
    }else if (IS_IPHONE_6)
        
    {
        lbl_Quality.frame=CGRectMake(10,0,WIDTH-70,35);
        lbl_Quality.font = [UIFont fontWithName:kFontBold size:15];
        
    }
    else{
        lbl_Quality.frame=CGRectMake(10,0,WIDTH-70,35);
        lbl_Quality.font = [UIFont fontWithName:kFontBold size:15];
        
    }
    
    
    lbl_Quality.text = @"Quantity Alert";
    lbl_Quality.backgroundColor=[UIColor clearColor];
    lbl_Quality.textColor=[UIColor blackColor];
    lbl_Quality.textAlignment=NSTextAlignmentCenter;
    
    [line_Emptyimage addSubview:lbl_Quality];
    
    btn_Quality = [[UIButton alloc] init];
    if (IS_IPHONE_6Plus) {
        
        btn_Quality.frame=CGRectMake(10,0,WIDTH-70,35);
        //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
        
        
    }else if (IS_IPHONE_6)
        
    {
        btn_Quality.frame=CGRectMake(10,0,WIDTH-70,35);
        //       btn_Quality.font = [UIFont fontWithName:kFont size:17];
        
    }
    else{
        btn_Quality.frame=CGRectMake(10,0,WIDTH-70,35);
        //       btn_Quality.font = [UIFont fontWithName:kFontBold size:11];
    }
    
    [btn_Quality setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    //    [btn_Arrowleft addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [line_Emptyimage addSubview:btn_Quality];
    
    
    
    btn_delete = [[UIButton alloc] init];
    if (IS_IPHONE_6Plus) {
        
        
        btn_delete.frame = CGRectMake(CGRectGetMaxX(btn_Quality.frame)+13, 10, 12,12);
        
        
    }else if (IS_IPHONE_6)
        
    {
        btn_delete.frame = CGRectMake(CGRectGetMaxX(btn_Quality.frame)+1, 10, 12,12);
        
    }
    else{
        btn_delete.frame = CGRectMake(CGRectGetMaxX(btn_Quality.frame)+14, 10, 12,12);
        
        
    }
    
    [btn_delete setImage:[UIImage imageNamed:@"icon-del.png"] forState:UIControlStateNormal];
    [btn_delete addTarget:self action:@selector(click_cancelqualityalert) forControlEvents:UIControlEventTouchUpInside ];
    [line_Emptyimage addSubview:btn_delete];
    
    
    
    
    
    
    line=[[UIImageView alloc]init];
    if (IS_IPHONE_6Plus) {
        
        line.frame=CGRectMake(22, CGRectGetMaxY(lbl_Quality.frame)+1, self.view.frame.size.width-55, 0.5);
        
    }else if (IS_IPHONE_6)
        
    {
        line.frame=CGRectMake(22, CGRectGetMaxY(lbl_Quality.frame)+1, self.view.frame.size.width-63, 0.5);
    }
    else{
        line.frame=CGRectMake(22, CGRectGetMaxY(lbl_Quality.frame)+1, self.view.frame.size.width-50, 0.5);
    }
    [line setUserInteractionEnabled:YES];
    line.backgroundColor=[UIColor clearColor];
    line.image=[UIImage imageNamed:@"line1.png"];
    [line_Emptyimage addSubview:line];
    
    
    lbl_ServingLow = [[UILabel alloc]init];
    if (IS_IPHONE_6Plus) {
        
        lbl_ServingLow.frame = CGRectMake(280,CGRectGetMaxY(lbl_Quality.frame)+1, 110,35);
        lbl_ServingLow.font = [UIFont fontWithName:kFontBold size:15];
        //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
        
        
    }else if (IS_IPHONE_6)
        
    {
        lbl_ServingLow.frame = CGRectMake(230,CGRectGetMaxY(lbl_Quality.frame)+1, 120,30);
        lbl_ServingLow.font = [UIFont fontWithName:kFontBold size:14];
        //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
        
    }
    else{
        lbl_ServingLow.frame =CGRectMake(190,CGRectGetMaxY(lbl_Quality.frame)+1, 110,35);
        lbl_ServingLow.font = [UIFont fontWithName:kFontBold size:15];
        
        //        btn_Quality.font = [UIFont fontWithName:kFont size:17];
        
    }
    
    lbl_ServingLow.text = @"Serving Low";
    lbl_ServingLow.backgroundColor=[UIColor clearColor];
    lbl_ServingLow.textColor=[UIColor colorWithRed:157/255.0 green:17/255.0 blue:47/255.0 alpha:1];
    
    lbl_ServingLow.textAlignment=NSTextAlignmentCenter;
    
    [line_Emptyimage addSubview:lbl_ServingLow];
    
    
    Arrow_left=[[UIImageView alloc]init];
    Arrow_left.frame=CGRectMake(10, 95, 10, 15);
    [Arrow_left setUserInteractionEnabled:YES];
    Arrow_left.backgroundColor=[UIColor clearColor];
    Arrow_left.image=[UIImage imageNamed:@"arrow_left.png"];
    [line_Emptyimage addSubview:Arrow_left];
    
    btn_Arrowleft = [[UIButton alloc] init];
    btn_Arrowleft.frame = CGRectMake(10, 95, 10, 15);
    btn_Arrowleft.backgroundColor = [UIColor clearColor];
    [btn_Arrowleft setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    //    [btn_Arrowleft addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [line_Emptyimage addSubview:btn_Arrowleft];
    
    scroll = [[UIScrollView alloc]init];
    scroll.frame=CGRectMake(30,CGRectGetMaxY(lbl_ServingLow.frame), WIDTH-70, 60);
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setShowsHorizontalScrollIndicator:NO];
    [scroll setUserInteractionEnabled:YES];
    scroll.scrollEnabled=YES;
    scroll.pagingEnabled = YES;
    scroll.delegate = self;
    scroll.backgroundColor = [UIColor clearColor];
    [line_Emptyimage  addSubview:scroll];
    
    
    
    Arrow_right=[[UIImageView alloc]init];
    Arrow_right.frame=CGRectMake(CGRectGetMaxX(scroll.frame)+1, 95, 10, 15);
    [Arrow_right setUserInteractionEnabled:YES];
    Arrow_right.backgroundColor=[UIColor clearColor];
    Arrow_right.image=[UIImage imageNamed:@"arrow_right.png"];
    [line_Emptyimage addSubview:Arrow_right];
    
    
    btn_Arrowright = [[UIButton alloc] init];
    btn_Arrowright.frame = CGRectMake(CGRectGetMaxX(scroll.frame)+1, 95, 10, 15);
    btn_Arrowright.backgroundColor = [UIColor clearColor];
    //    [btn_Arrowleft setImage:[UIImage imageNamed:@"arrow_left.png"] forState:UIControlStateNormal];
    [btn_Arrowright setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    //    [btn_Arrowleft addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
    [line_Emptyimage addSubview:btn_Arrowright];
    
    
    
    
    
    
    //second screen for Order on request
    /*
     
     UIImageView *line_Emptyimage=[[UIImageView alloc]init];
     line_Emptyimage.frame=CGRectMake(10, CGRectGetMaxY(img_Background.frame)+2, WIDTH-15, 150);
     
     [line_Emptyimage setUserInteractionEnabled:YES];
     line_Emptyimage.backgroundColor=[UIColor clearColor];
     line_Emptyimage.image=[UIImage imageNamed:@"bg1.png"];
     [self.view addSubview:line_Emptyimage];
     
     
     UILabel  * lbl_orderonRequest = [[UILabel alloc]init];
     
     if (IS_IPHONE_6Plus) {
     
     lbl_orderonRequest.frame=CGRectMake(10,0,WIDTH-70,35);
     lbl_orderonRequest.font = [UIFont fontWithName:kFontBold size:15];
     
     
     }else if (IS_IPHONE_6)
     
     {
     lbl_orderonRequest.frame=CGRectMake(10,0,WIDTH-70,25);
     lbl_orderonRequest.font = [UIFont fontWithName:kFontBold size:14];
     
     }
     else{
     lbl_orderonRequest.frame=CGRectMake(10,0,WIDTH-70,35);
     lbl_orderonRequest.font = [UIFont fontWithName:kFontBold size:13];
     
     }
     
     
     lbl_orderonRequest.text = @"Order On Request";
     lbl_orderonRequest.backgroundColor=[UIColor clearColor];
     lbl_orderonRequest.textColor=[UIColor blackColor];
     lbl_orderonRequest.textAlignment=NSTextAlignmentCenter;
     
     [line_Emptyimage addSubview:lbl_orderonRequest];
     
     UIButton *btn_orderonRequest = [[UIButton alloc] init];
     if (IS_IPHONE_6Plus) {
     
     
     btn_orderonRequest.frame = CGRectMake(10,0,WIDTH-70,35);
     
     
     
     
     }else if (IS_IPHONE_6)
     
     {
     btn_orderonRequest.frame = CGRectMake(10,0,WIDTH-70,35);
     
     }
     else{
     btn_orderonRequest.frame = CGRectMake(10,0,WIDTH-70,35);
     
     
     
     }
     
     [btn_orderonRequest setBackgroundColor:[UIColor clearColor]];
     //    [btn_msg addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
     [line_Emptyimage addSubview:btn_orderonRequest];
     
     
     
     UIButton *btn_deleteforOrderRequest = [[UIButton alloc] init];
     if (IS_IPHONE_6Plus) {
     
     
     btn_deleteforOrderRequest.frame = CGRectMake(CGRectGetMaxX(btn_orderonRequest.frame)+15, 10, 12,12);
     
     }else if (IS_IPHONE_6)
     
     {
     btn_deleteforOrderRequest.frame = CGRectMake(CGRectGetMaxX(btn_orderonRequest.frame)+15, 10, 12,12) ;
     
     }
     else{
     btn_deleteforOrderRequest.frame = CGRectMake(CGRectGetMaxX(btn_orderonRequest.frame)+20, 10, 10,10);
     
     
     }
     
     [btn_deleteforOrderRequest setImage:[UIImage imageNamed:@"icon-del.png"] forState:UIControlStateNormal];
     [btn_deleteforOrderRequest setBackgroundColor:[UIColor clearColor]];
     //    [btn_msg addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
     [line_Emptyimage addSubview:btn_deleteforOrderRequest];
     
     UIImageView *line_img=[[UIImageView alloc]init];
     line_img.frame=CGRectMake(20, CGRectGetMaxY(btn_orderonRequest.frame)+1, self.view.frame.size.width-50, 0.5);
     [line_img setUserInteractionEnabled:YES];
     line_img.backgroundColor=[UIColor clearColor];
     line_img.image=[UIImage imageNamed:@"line1.png"];
     [line_Emptyimage addSubview:line_img];
     
     
     
     UILabel*lbl_requestedServingTime = [[UILabel alloc]init];
     if (IS_IPHONE_6Plus) {
     
     lbl_requestedServingTime.frame = CGRectMake(130,CGRectGetMaxY(btn_orderonRequest.frame)+1, 230,25);
     lbl_requestedServingTime.font = [UIFont fontWithName:kFont size:12];
     //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
     
     
     }else if (IS_IPHONE_6)
     
     {
     lbl_requestedServingTime.frame = CGRectMake(150,CGRectGetMaxY(btn_orderonRequest.frame)+1, 140,25);
     lbl_requestedServingTime.font = [UIFont fontWithName:kFont size:11];
     //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
     
     }
     else{
     lbl_requestedServingTime.frame =CGRectMake(90,CGRectGetMaxY(btn_orderonRequest.frame)+1, 140,35);
     lbl_requestedServingTime.font = [UIFont fontWithName:kFont size:11];
     
     //        btn_Quality.font = [UIFont fontWithName:kFont size:17];
     
     }
     
     lbl_requestedServingTime.text = @"Requested Serving Time";
     lbl_requestedServingTime.backgroundColor=[UIColor clearColor];
     lbl_requestedServingTime.textColor=[UIColor blackColor];
     
     lbl_requestedServingTime.textAlignment=NSTextAlignmentCenter;
     
     [line_Emptyimage addSubview:lbl_requestedServingTime];
     
     
     
     
     
     UILabel*lbl_ServingTimewithPM = [[UILabel alloc]init];
     if (IS_IPHONE_6Plus) {
     
     lbl_ServingTimewithPM.frame = CGRectMake(320,CGRectGetMaxY(btn_orderonRequest.frame)+1, 230,30);
     lbl_ServingTimewithPM.font = [UIFont fontWithName:kFontBold size:18];
     //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
     
     
     }else if (IS_IPHONE_6)
     
     {
     lbl_ServingTimewithPM.frame = CGRectMake(CGRectGetMaxX(lbl_requestedServingTime.frame)-5,CGRectGetMaxY(line_img.frame)+1, 200,25);
     lbl_ServingTimewithPM.font = [UIFont fontWithName:kFontBold size:5];
     
     }
     else{
     lbl_ServingTimewithPM.frame =CGRectMake(CGRectGetMaxX(lbl_requestedServingTime.frame)+1,CGRectGetMaxY(btn_orderonRequest.frame)+1, 80,35);
     lbl_ServingTimewithPM.font = [UIFont fontWithName:kFontBold size:3];
     
     
     }
     
     lbl_ServingTimewithPM.text = @"14:30:PM";
     lbl_ServingTimewithPM.backgroundColor=[UIColor clearColor];
     lbl_ServingTimewithPM.textColor=[UIColor colorWithRed:157/255.0 green:17/255.0 blue:47/255.0 alpha:1];
     
     lbl_ServingTimewithPM.textAlignment=NSTextAlignmentLeft;
     
     [line_Emptyimage addSubview:lbl_ServingTimewithPM];
     
     
     
     UIButton *btn_Arrowleft = [[UIButton alloc] init];
     if (IS_IPHONE_6Plus) {
     
     btn_Arrowleft.frame = CGRectMake(8, 90, 10, 15);
     
     
     }else if (IS_IPHONE_6)
     
     {
     btn_Arrowleft.frame = CGRectMake(8, 90, 10, 15);
     }
     else{
     btn_Arrowleft.frame = CGRectMake(8, 85, 10, 15);
     
     }
     btn_Arrowleft.backgroundColor = [UIColor clearColor];
     [btn_Arrowleft setImage:[UIImage imageNamed:@"arrow_left.png"] forState:UIControlStateNormal];
     
     //    [btn_Arrowleft addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
     [line_Emptyimage addSubview:btn_Arrowleft];
     
     
     
     
     
     UIScrollView *scroll_viewForRequest = [[UIScrollView alloc]init];
     
     if (IS_IPHONE_6Plus) {
     
     scroll_viewForRequest.frame=CGRectMake(20,CGRectGetMaxY(lbl_requestedServingTime.frame)+1, WIDTH-65, 65);        lbl_ServingTimewithPM.font = [UIFont fontWithName:kFontBold size:15];
     //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
     
     
     }else if (IS_IPHONE_6)
     
     {
     scroll_viewForRequest.frame=CGRectMake(20,CGRectGetMaxY(lbl_requestedServingTime.frame)+1, WIDTH-70, 65);        lbl_ServingTimewithPM.font = [UIFont fontWithName:kFontBold size:14];
     //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
     
     }
     else{
     scroll_viewForRequest.frame=CGRectMake(20,CGRectGetMaxY(lbl_requestedServingTime.frame), WIDTH-60, 55);        lbl_ServingTimewithPM.font = [UIFont fontWithName:kFontBold size:13];
     
     //        btn_Quality.font = [UIFont fontWithName:kFont size:17];
     
     }
     
     
     [scroll_viewForRequest setShowsVerticalScrollIndicator:NO];
     [scroll_viewForRequest setUserInteractionEnabled:YES];
     scroll_viewForRequest.delegate = self;
     scroll_viewForRequest.backgroundColor = [UIColor clearColor];
     [line_Emptyimage  addSubview:scroll_viewForRequest];
     
     
     
     UIView *imgView_Photos = [[UIView alloc] init];
     if (IS_IPHONE_6Plus) {
     
     
     imgView_Photos.frame=CGRectMake(0,0, WIDTH-50, 65);
     //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
     
     
     }else if (IS_IPHONE_6)
     
     {
     imgView_Photos.frame=CGRectMake(0,0,  WIDTH-50, 65);
     //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
     
     }
     else{
     imgView_Photos.frame=CGRectMake(0,0, WIDTH-50, 55);
     
     //        btn_Quality.font = [UIFont fontWithName:kFont size:17];
     
     }
     
     //    imgView_Photos.frame = CGRectMake(0,0, WIDTH-35, 70);
     [imgView_Photos setBackgroundColor:[UIColor clearColor]];
     imgView_Photos.userInteractionEnabled = YES;
     [scroll_viewForRequest addSubview:imgView_Photos];
     
     
     
     UIImageView *profileimage=[[UIImageView alloc]init];
     
     if (IS_IPHONE_6Plus) {
     
     profileimage.frame=CGRectMake(5, 0, 65, 65);
     }
     else if (IS_IPHONE_6)
     
     {
     profileimage.frame=CGRectMake(5, 0, 65, 65);
     }
     else{
     profileimage.frame=CGRectMake(5, 0, 55, 55);
     }
     [profileimage setUserInteractionEnabled:YES];
     profileimage.backgroundColor=[UIColor clearColor];
     profileimage.image=[UIImage imageNamed:@"use_imgprofile.png"];
     [imgView_Photos addSubview:profileimage];
     
     UILabel  * lbl_Personname= [[UILabel alloc]init];
     if (IS_IPHONE_6Plus) {
     
     lbl_Personname.frame=CGRectMake(CGRectGetMaxX(profileimage.frame)+10,8, 60,20);
     lbl_Personname.font = [UIFont fontWithName:kFontBold size:13];
     
     
     }else if (IS_IPHONE_6)
     
     {
     lbl_Personname.frame= CGRectMake(CGRectGetMaxX(profileimage.frame)+10,5, 60,20);
     lbl_Personname.font = [UIFont fontWithName:kFontBold size:13];
     }
     else{
     lbl_Personname.frame= CGRectMake(CGRectGetMaxX(profileimage.frame)+10,5,60,20);
     lbl_Personname.font = [UIFont fontWithName:kFontBold size:12];
     
     }
     lbl_Personname.text = @"John Doe";
     lbl_Personname.backgroundColor=[UIColor clearColor];
     lbl_Personname.textColor=[UIColor blackColor];
     lbl_Personname.textAlignment=NSTextAlignmentLeft;
     //    lbl_Todayorders.font = [UIFont fontWithName:@"Arial" size:20];
     [imgView_Photos addSubview:lbl_Personname];
     
     
     
     UILabel  * lbl_Ordervalue= [[UILabel alloc]init];
     if (IS_IPHONE_6Plus) {
     
     lbl_Ordervalue.frame=CGRectMake(CGRectGetMaxX(profileimage.frame)+10,CGRectGetMaxY(lbl_Personname.frame)+8, 80,20);
     lbl_Ordervalue.font = [UIFont fontWithName:kFont size:13];
     
     
     }else if (IS_IPHONE_6)
     
     {
     lbl_Ordervalue.frame= CGRectMake(CGRectGetMaxX(profileimage.frame)+10,CGRectGetMaxY(lbl_Personname.frame)+8, 80,20);
     lbl_Ordervalue.font = [UIFont fontWithName:kFont size:13];
     }
     else{
     lbl_Ordervalue.frame= CGRectMake(CGRectGetMaxX(profileimage.frame)+10,CGRectGetMaxY(lbl_Personname.frame)+8,68,20);
     lbl_Ordervalue.font = [UIFont fontWithName:kFont size:10];
     
     }
     lbl_Ordervalue.text = @"Order value:";
     lbl_Ordervalue.backgroundColor=[UIColor clearColor];
     lbl_Ordervalue.textColor=[UIColor blackColor];
     lbl_Ordervalue.textAlignment=NSTextAlignmentLeft;
     //    lbl_Todayorders.font = [UIFont fontWithName:@"Arial" size:20];
     [imgView_Photos addSubview:lbl_Ordervalue];
     
     
     
     UILabel  * lbl_Ordercost= [[UILabel alloc]init];
     if (IS_IPHONE_6Plus) {
     
     lbl_Ordercost.frame=CGRectMake(CGRectGetMaxX(lbl_Ordervalue.frame)+3,CGRectGetMaxY(lbl_Personname.frame)+8, 60,20);
     lbl_Ordercost.font = [UIFont fontWithName:kFontBold size:15];
     
     
     }else if (IS_IPHONE_6)
     
     {
     lbl_Ordercost.frame= CGRectMake(CGRectGetMaxX(lbl_Ordervalue.frame)+3,CGRectGetMaxY(lbl_Personname.frame)+8, 60,20);
     lbl_Ordercost.font = [UIFont fontWithName:kFontBold size:15];
     }
     else{
     lbl_Ordercost.frame= CGRectMake(CGRectGetMaxX(lbl_Ordervalue.frame)+1,CGRectGetMaxY(lbl_Personname.frame)+8,68,20);
     lbl_Ordercost.font = [UIFont fontWithName:kFontBold size:13];
     
     }
     lbl_Ordercost.text = @"$46.90";
     lbl_Ordercost.backgroundColor=[UIColor clearColor];
     lbl_Ordercost.textColor=[UIColor blackColor];
     lbl_Ordercost.textAlignment=NSTextAlignmentLeft;
     //    lbl_Todayorders.font = [UIFont fontWithName:@"Arial" size:20];
     [imgView_Photos addSubview:lbl_Ordercost];
     
     UIButton *btn_Arrowright = [[UIButton alloc] init];
     if (IS_IPHONE_6Plus) {
     
     btn_Arrowright.frame = CGRectMake(CGRectGetMaxX(scroll_viewForRequest.frame)+1, 90, 10, 15);
     
     }else if (IS_IPHONE_6)
     
     {
     btn_Arrowright.frame = CGRectMake(CGRectGetMaxX(scroll_viewForRequest.frame)+10, 90, 10, 15);
     }
     else{
     btn_Arrowright.frame = CGRectMake(CGRectGetMaxX(scroll_viewForRequest.frame)+1, 90, 10, 15);
     }
     
     btn_Arrowright.backgroundColor = [UIColor clearColor];
     //    [btn_Arrowleft setImage:[UIImage imageNamed:@"arrow_left.png"] forState:UIControlStateNormal];
     [btn_Arrowright setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
     [btn_Arrowright setImage:[UIImage imageNamed:@"arrow_right.png"] forState:UIControlStateNormal];
     
     //    [btn_Arrowleft addTarget:self action:@selector(drop_btnClick) forControlEvents:UIControlEventTouchUpInside ];
     [line_Emptyimage addSubview:btn_Arrowright];
     
     
     UILabel  * lbl_Tapvieworder= [[UILabel alloc]init];
     if (IS_IPHONE_6Plus) {
     
     lbl_Tapvieworder.frame=CGRectMake(270,CGRectGetMaxY(scroll_viewForRequest.frame)+2, 120,15);
     lbl_Tapvieworder.font = [UIFont fontWithName:kFont size:13];
     
     
     }else if (IS_IPHONE_6)
     
     {
     lbl_Tapvieworder.frame= CGRectMake(220,CGRectGetMaxY(scroll_viewForRequest.frame)+1, 120,13);
     lbl_Tapvieworder.font = [UIFont fontWithName:kFont size:12];
     }
     else{
     lbl_Tapvieworder.frame= CGRectMake(205,CGRectGetMaxY(scroll_viewForRequest.frame)+1, 100,10);
     lbl_Tapvieworder.font = [UIFont fontWithName:kFont size:10];
     
     }
     lbl_Tapvieworder.text = @"Tap to view order";
     lbl_Tapvieworder.backgroundColor=[UIColor clearColor];
     lbl_Tapvieworder.textColor=[UIColor blackColor];
     lbl_Tapvieworder.textAlignment=NSTextAlignmentLeft;
     //    lbl_Todayorders.font = [UIFont fontWithName:@"Arial" size:20];
     [line_Emptyimage addSubview:lbl_Tapvieworder];
     
     */
    
    lbl_Todayorders = [[UILabel alloc]init];
    if (IS_IPHONE_6Plus)
    {
        
        lbl_Todayorders.frame=CGRectMake(24,CGRectGetMaxY(line_Emptyimage.frame)+5, 250,35);
        lbl_Todayorders.font = [UIFont fontWithName:kFontBold size:14];
        //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
        
        
    }else if (IS_IPHONE_6)
        
    {
        lbl_Todayorders.frame= CGRectMake(13,CGRectGetMaxY(line_Emptyimage.frame)+5, 250,35);
        lbl_Todayorders.font = [UIFont fontWithName:kFontBold size:16];
    }
    else
    {
        lbl_Todayorders.frame= CGRectMake(8,CGRectGetMaxY(line_Emptyimage.frame)+5, 250,35);
        lbl_Todayorders.font = [UIFont fontWithName:kFontBold size:14];
        
    }
    lbl_Todayorders.text = @"Today's Orders";
    lbl_Todayorders.backgroundColor=[UIColor clearColor];
    lbl_Todayorders.textColor=[UIColor blackColor];
    lbl_Todayorders.textAlignment=NSTextAlignmentLeft;
    //    lbl_Todayorders.font = [UIFont fontWithName:@"Arial" size:20];
    [scrollview5 addSubview:lbl_Todayorders];
    
    
    
    
#pragma mark Tableview
    
    img_table= [[UITableView alloc]init];
    
    [img_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    img_table.delegate = self;
    img_table.dataSource = self;
    img_table.showsVerticalScrollIndicator = NO;
    img_table.backgroundColor = [UIColor clearColor];
    [scrollview5 addSubview:img_table];
    
    
    
    if (IS_IPHONE_6Plus)
    {
        img_table.frame  = CGRectMake(0,CGRectGetMaxY(lbl_Todayorders.frame)+5,self.view.frame.size.width,380);
    }
    else if (IS_IPHONE_6)
    {
        img_table.frame  = CGRectMake(0,CGRectGetMaxY(lbl_Todayorders.frame)+5,self.view.frame.size.width-10,300);
    }
    else
    {
        img_table.frame  = CGRectMake(0,CGRectGetMaxY(lbl_Todayorders.frame)+5,self.view.frame.size.width-10,220);
    }
}


//
//UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc]initWithTitle:@"Gotonext" style:UIBarButtonItemStyleBordered target:self action:@selector(gotothirdpage)];
//self.navigationItem.leftBarButtonItem = rightBtn;
//menuItems1 = @[@"Home",@"My Schedule",@"Orders",@"My Menu",@"Account" ,@"Notification",@"Messages",];
//tableList = [[UITableView alloc]initWithFrame:CGRectMake(-100, 0, 100, 580)];
//tableList.delegate = self;
//tableList.dataSource = self;
//tableList.backgroundColor = [UIColor blackColor];
//[self.view addSubview:tableList];
//
//
//// Do any additional setup after loading the view, typically from a nib.
//}
//-(void)gotothirdpage
//{
//    if (isVal) {
//
//        isVal = NO;
//
//        [UIView beginAnimations:@"bucketsOff" context:nil];
//        [UIView setAnimationDuration:0.4];
//        [UIView setAnimationDelegate:self];
//        //position off screen
//        CGRect tmpFram = self.navigationController.navigationBar.frame;
//        tmpFram.origin.x = 100;
//        self.navigationController.navigationBar.frame = tmpFram;
//        tableList.frame = CGRectMake(0, 0, 100, 580);
//        //animate off screen
//        [UIView commitAnimations];
//    }else
//    {
//
//        isVal = YES;
//
//
//        [UIView beginAnimations:@"bucketsOff" context:nil];
//        [UIView setAnimationDuration:0.4];
//        [UIView setAnimationDelegate:self];
//        //position off screen
//        CGRect tmpFram = self.navigationController.navigationBar.frame;
//        tmpFram.origin.x = 0;
//        self.navigationController.navigationBar.frame = tmpFram;
//        tableList.frame = CGRectMake(-100, 0, 100, 580) ;
//        //animate off screen
//        [UIView commitAnimations];
//    }
//
//}
//
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    // Return the number of rows in the section.
//    return [menuItems1 count];
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 100;
//}
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell==nil)
//    {
//        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//    }
//
//    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(30, 10, 40, 40)];
//    imgView.image = [UIImage imageNamed:@"img-login-logo@2x.png"];
//    [cell.contentView addSubview:imgView];
//    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 50, 80, 40)];
//    lbl.text = [menuItems1 objectAtIndex:indexPath.row];
//    lbl.numberOfLines = 2;
//    lbl.textAlignment = NSTextAlignmentCenter;
//    lbl.font = [UIFont fontWithName:@"Arial" size:15];
//    lbl.textColor = [UIColor whiteColor];
//    [cell.contentView addSubview:lbl];
//    //    cell.textLabel.text=[menuItems1 objectAtIndex:indexPath.row];
//
//    cell.textLabel.textColor = [UIColor whiteColor];
//    cell.backgroundColor = [UIColor blackColor];
//    return cell;
//}
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *vi = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
//
//    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(30, 10, 40, 40)];
//    imgView.image = [UIImage imageNamed:@"img-login-logo@2x.png"];
//    [vi addSubview:imgView];
//
//
//    imgView.layer.cornerRadius = imgView.frame.size.height /2;
//    imgView.layer.masksToBounds = YES;
//    imgView.layer.borderWidth = 0;
//    return vi;
//}
//
//

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == table_on_drop_down)
    {
        return 3;
    }
    else
        return [ary_dishList count];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table_on_drop_down)
    {
        return 40;
    }
    else
        if (IS_IPHONE_6Plus)
        {
            
            return 210;
            
            
        }else if (IS_IPHONE_6)
            
        {
            return 210;
        }
        else{
            return 180;
            
            
        }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *kReuseIndentifier = @"myCell";
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kReuseIndentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    if (tableView == table_on_drop_down)
    {
        UIImageView*img_bg_for_first_tbl;
        
        img_bg_for_first_tbl = [[UIImageView alloc]init];
        img_bg_for_first_tbl.frame =  CGRectMake(0,5, 150, 40);
        img_bg_for_first_tbl.image=[UIImage imageNamed:@"bg-img@2x.png"];
        // img_bg_for_first_tbl.backgroundColor = [UIColor whiteColor];
        [img_bg_for_first_tbl setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_bg_for_first_tbl];
        
        UILabel* lbl_food_now_r_later;
        
        lbl_food_now_r_later = [[UILabel alloc]init];
        lbl_food_now_r_later .frame = CGRectMake(5,10,200, 15);
        lbl_food_now_r_later .text = [NSString stringWithFormat:@"%@",[array_head_names objectAtIndex:indexPath.row]];
        lbl_food_now_r_later .font = [UIFont fontWithName:kFontBold size:15];
        lbl_food_now_r_later .textColor = [UIColor blackColor];
        lbl_food_now_r_later .backgroundColor = [UIColor clearColor];
        [img_bg_for_first_tbl addSubview: lbl_food_now_r_later ];
        
    }
    else{
        
        
        UIImageView *img_cellBackGnd = [[UIImageView alloc]init];
        
        
        if (IS_IPHONE_6Plus)
        {
            
            img_cellBackGnd.frame =  CGRectMake(5,0, WIDTH-8, 180);
            
            
            
        }else if (IS_IPHONE_6)
            
        {
            img_cellBackGnd.frame =  CGRectMake(8,0, WIDTH-10, 200);
        }
        else
        {
            
            img_cellBackGnd.frame =  CGRectMake(5,0, WIDTH-8, 160);
            
            
        }
        
        [img_cellBackGnd setImage:[UIImage imageNamed:@"bg1.png"]];
        [img_cellBackGnd setUserInteractionEnabled:YES];
        [cell.contentView addSubview:img_cellBackGnd];
        
        
        UILabel  * lbl_order = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus) {
            
            lbl_order.frame=CGRectMake(14,5, 105,35);
            lbl_order.font = [UIFont fontWithName:kFont size:14];
            //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
            
            
        }else if (IS_IPHONE_6)
            
        {
            lbl_order.frame= CGRectMake(5,5, 80,35);
            lbl_order.font = [UIFont fontWithName:kFont size:15];
        }
        else{
            lbl_order.frame= CGRectMake(7,5, 90,35);
            lbl_order.font = [UIFont fontWithName:kFont size:14];
            
        }
        lbl_order.text = @"Order no.:";
        lbl_order.backgroundColor=[UIColor clearColor];
        lbl_order.textColor=[UIColor blackColor];
        lbl_order.textAlignment=NSTextAlignmentLeft;
        //    lbl_order.font = [UIFont fontWithName:@"Arial" size:14];
        [img_cellBackGnd addSubview:lbl_order];
        
        
        UILabel  * lbl_number = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus) {
            
            lbl_number.frame=CGRectMake(90,5, 85,35);
            lbl_number.font = [UIFont fontWithName:kFontBold size:14];
            //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
            
            
        }else if (IS_IPHONE_6)
            
        {
            lbl_number.frame= CGRectMake(CGRectGetMaxX(lbl_order.frame)-1,5, 85,35);
            lbl_number.font = [UIFont fontWithName:kFontBold size:13];
        }
        else{
            lbl_number.frame= CGRectMake(82,5, 85,35);
            lbl_number.font = [UIFont fontWithName:kFontBold size:13];
            
        }
        
        lbl_number.text = [[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"orderno"];
        
        lbl_number.backgroundColor=[UIColor clearColor];
        lbl_number.textColor=[UIColor colorWithRed:157/255.0 green:17/255.0 blue:47/255.0 alpha:1];
        lbl_number.textAlignment=NSTextAlignmentLeft;
        //    lbl_number.font = [UIFont fontWithName:kFont size:18];
        [img_cellBackGnd addSubview:lbl_number];
        //lbl_number.numberOfLines = 0;
        
        
        
        
        UILabel  * lbl_Duein = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus) {
            
            lbl_Duein.frame=CGRectMake(288,5, 70,35);
            lbl_Duein.font = [UIFont fontWithName:kFont size:14];
            //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
            
            
        }else if (IS_IPHONE_6)
            
        {
            lbl_Duein.frame= CGRectMake(245,5, 70,35);
            lbl_Duein.font = [UIFont fontWithName:kFont size:14];
        }
        else{
            lbl_Duein.frame= CGRectMake(200,5, 40,25);
            lbl_Duein.font = [UIFont fontWithName:kFont size:12];
            
        }
        
        
        lbl_Duein.text = @"Due in";
        lbl_Duein.backgroundColor=[UIColor clearColor];
        lbl_Duein.textColor=[UIColor blackColor];
        lbl_Duein.textAlignment=NSTextAlignmentLeft;
        //    lbl_Duein.font = [UIFont fontWithName:@"Arial" size:18];
        [img_cellBackGnd addSubview:lbl_Duein];
        lbl_Duein.numberOfLines = 0;
        
        
        UILabel  * lblnumber = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus) {
            
            lblnumber.frame=CGRectMake(335,5, 70,35);
            lblnumber.font = [UIFont fontWithName:kFontBold size:14];
            //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
            
            
        }else if (IS_IPHONE_6)
            
        {
            lblnumber.frame= CGRectMake(290,5, 70,35);
            lblnumber.font = [UIFont fontWithName:kFontBold size:16];
        }
        else{
            lblnumber.frame= CGRectMake(CGRectGetMaxX(lbl_Duein.frame)+1,5, 60,25);
            lblnumber.font = [UIFont fontWithName:kFontBold size:15];
            
        }
        
        lblnumber.text = [[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"duein"];
        lblnumber.backgroundColor=[UIColor clearColor];
        lblnumber.textColor=[UIColor colorWithRed:157/255.0 green:17/255.0 blue:47/255.0 alpha:1];
        lblnumber.textAlignment=NSTextAlignmentLeft;
        //    lblnumber.font = [UIFont fontWithName:@"Arial" size:18];
        [img_cellBackGnd addSubview:lblnumber];
        lblnumber.numberOfLines = 0;
        
        
        UILabel  * lbl_value = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus) {
            
            lbl_value.frame=CGRectMake(35,CGRectGetMaxY(lbl_order.frame)-7, 50,35);
            lbl_value.font = [UIFont fontWithName:kFont size:14];
            //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
            
            
        }else if (IS_IPHONE_6)
            
        {
            lbl_value.frame= CGRectMake(30,CGRectGetMaxY(lbl_order.frame)+2, 45,35);
            lbl_value.font = [UIFont fontWithName:kFont size:14];
        }
        else{
            lbl_value.frame=CGRectMake(30,CGRectGetMaxY(lbl_order.frame)-8, 40,35);
            lbl_value.font = [UIFont fontWithName:kFont size:12];
            
        }
        
        lbl_value.text = @"Value:";
        lbl_value.backgroundColor=[UIColor clearColor];
        lbl_value.textColor=[UIColor blackColor];
        lbl_value.textAlignment=NSTextAlignmentLeft;
        //lbl_value.font = [UIFont fontWithName:@"Arial" size:18];
        [img_cellBackGnd addSubview:lbl_value];
        lbl_value.numberOfLines = 0;
        
        UILabel  * lbl_valueRupees = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus) {
            
            lbl_valueRupees.frame=CGRectMake(CGRectGetMaxX(lbl_value.frame)+3,CGRectGetMaxY(lbl_order.frame)-7, 150,35);
            lbl_valueRupees.font = [UIFont fontWithName:kFontBold size:14];
            //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
            
            
        }else if (IS_IPHONE_6)
            
        {
            lbl_valueRupees.frame= CGRectMake(CGRectGetMaxX(lbl_value.frame)+3,CGRectGetMaxY(lbl_order.frame)+3, 150,35);
            lbl_valueRupees.font = [UIFont fontWithName:kFontBold size:14];
        }
        else{
            lbl_valueRupees.frame=CGRectMake(CGRectGetMaxX(lbl_value.frame)+3,CGRectGetMaxY(lbl_order.frame)-8, 150,35);
            lbl_valueRupees.font = [UIFont fontWithName:kFontBold size:13];
            
        }
        
        lbl_valueRupees.text = [NSString stringWithFormat:@"$ %@",[[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"value"]];
        lbl_valueRupees.backgroundColor=[UIColor clearColor];
        lbl_valueRupees.textColor=[UIColor blackColor];
        lbl_valueRupees.textAlignment=NSTextAlignmentLeft;
        //lbl_value.font = [UIFont fontWithName:@"Arial" size:18];
        [img_cellBackGnd addSubview:lbl_valueRupees];
        lbl_valueRupees.numberOfLines = 0;
        
        
        
        UILabel  * lbl_Recieved= [[UILabel alloc]init];
        if (IS_IPHONE_6Plus) {
            
            lbl_Recieved.frame=CGRectMake(20,CGRectGetMaxY(lbl_value.frame)-7, 80,35);
            lbl_Recieved.font = [UIFont fontWithName:kFont size:14];
            //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
            
            
        }else if (IS_IPHONE_6)
            
        {
            lbl_Recieved.frame= CGRectMake(20,CGRectGetMaxY(lbl_value.frame)+3, 70,35);
            lbl_Recieved.font = [UIFont fontWithName:kFont size:14];
        }
        else{
            lbl_Recieved.frame=CGRectMake(20,CGRectGetMaxY(lbl_value.frame)-5, 60,25);
            lbl_Recieved.font = [UIFont fontWithName:kFont size:12];
            
        }
        
        
        
        lbl_Recieved.text = @"Received:";
        lbl_Recieved.backgroundColor=[UIColor clearColor];
        lbl_Recieved.textColor=[UIColor blackColor];
        lbl_Recieved.textAlignment=NSTextAlignmentLeft;
        //    lbl_Recieved.font = [UIFont fontWithName:@"Arial" size:18];
        [img_cellBackGnd addSubview:lbl_Recieved];
        lbl_Recieved.numberOfLines = 0;
        
        UILabel  * lbl_datetime= [[UILabel alloc]init];
        if (IS_IPHONE_6Plus) {
            
            lbl_datetime.frame=CGRectMake(CGRectGetMaxX(lbl_Recieved.frame)-6,CGRectGetMaxY(lbl_value.frame)-7, 200,35);
            lbl_datetime.font = [UIFont fontWithName:kFontBold size:14];
            //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
            
            
        }else if (IS_IPHONE_6)
            
        {
            lbl_datetime.frame= CGRectMake(CGRectGetMaxX(lbl_Recieved.frame)+3,CGRectGetMaxY(lbl_value.frame)+3, 200,35);
            lbl_datetime.font = [UIFont fontWithName:kFontBold size:13];
        }
        else{
            lbl_datetime.frame=CGRectMake(CGRectGetMaxX(lbl_Recieved.frame)+3,CGRectGetMaxY(lbl_value.frame)-5, 150,25);
            lbl_datetime.font = [UIFont fontWithName:kFontBold size:11];
            
        }
        lbl_datetime.text = [NSString stringWithFormat:@"%@",[[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"recieved"]];
        lbl_datetime.backgroundColor=[UIColor clearColor];
        lbl_datetime.textColor=[UIColor blackColor];
        lbl_datetime.textAlignment=NSTextAlignmentLeft;
        //    lbl_datetime.font = [UIFont fontWithName:@"Arial" size:18];
        [img_cellBackGnd addSubview:lbl_datetime];
        lbl_datetime.numberOfLines = 0;
        
        
        UIImageView *imr_icon = [[UIImageView alloc]init];
        if (IS_IPHONE_6Plus) {
            
            imr_icon.frame =  CGRectMake(350,CGRectGetMaxY(lbl_value.frame)-7, 30, 30);
            //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
            
            
        }else if (IS_IPHONE_6)
            
        {
            imr_icon.frame =  CGRectMake(310,CGRectGetMaxY(lbl_value.frame)+3, 30, 30);
            
        }
        else{
            imr_icon.frame =  CGRectMake(260,CGRectGetMaxY(lbl_value.frame)-5, 25, 25);
            ;
            
        }
        
        [imr_icon setImage:[UIImage imageNamed:@"icon-6.png"]];
        [imr_icon setUserInteractionEnabled:YES];
        [img_cellBackGnd addSubview:imr_icon];
        
        
        UIImageView *line1=[[UIImageView alloc]init];
        
        if (IS_IPHONE_6Plus) {
            
            line1.frame=CGRectMake(5, CGRectGetMaxY(imr_icon.frame)+1, img_table.frame.size.width-28, 0.5);
        }else if (IS_IPHONE_6)
            
        {
            line1.frame=CGRectMake(5, CGRectGetMaxY(imr_icon.frame)+3, img_table.frame.size.width-28, 0.5);
        }
        else{
            line1.frame=CGRectMake(5, CGRectGetMaxY(imr_icon.frame)+5, img_table.frame.size.width-28, 0.5);
        }
        [line1 setUserInteractionEnabled:YES];
        line1.backgroundColor=[UIColor clearColor];
        line1.image=[UIImage imageNamed:@"line1.png"];
        [img_cellBackGnd addSubview:line1];
        
        UIImageView *img_profile=[[UIImageView alloc]init];
        
        
        
        if (IS_IPHONE_6Plus) {
            
            img_profile.frame=CGRectMake(5, CGRectGetMaxY(line1.frame)+10, 60, 60);
        }else if (IS_IPHONE_6)
            
        {
            img_profile.frame=CGRectMake(5, CGRectGetMaxY(line1.frame)+10, 60, 60);
        }
        else{
            img_profile.frame=CGRectMake(15, CGRectGetMaxY(line1.frame)+10, 50, 40);
        }
        
        
        [img_profile setUserInteractionEnabled:YES];
        img_profile.backgroundColor=[UIColor clearColor];
        NSString *url_Img = [NSString stringWithFormat: @"%@", [[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"userimage"]];
        img_profile.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img]]];
        [img_cellBackGnd addSubview:img_profile];
        
        UILabel  * lbl_Name= [[UILabel alloc]init];
        
        if (IS_IPHONE_6Plus) {
            
            lbl_Name.frame=CGRectMake(90,CGRectGetMaxY(line1.frame)+20, 150,35);
            
        }else if (IS_IPHONE_6)
            
        {
            lbl_Name.frame=CGRectMake(90,CGRectGetMaxY(line1.frame)+20, 150,35);
            
        }
        else{
            lbl_Name.frame=CGRectMake(90,CGRectGetMaxY(line1.frame)+13, 150,35);
        }
        
        
        lbl_Name.text = [NSString stringWithFormat:@"%@",[[ary_dishList objectAtIndex:indexPath.row] valueForKey:@"username"]];
        lbl_Name.backgroundColor=[UIColor clearColor];
        lbl_Name.textColor=[UIColor blackColor];
        lbl_Name.textAlignment=NSTextAlignmentLeft;
        lbl_Name.font = [UIFont fontWithName:kFontBold size:14];
        [img_cellBackGnd addSubview:lbl_Name];
        lbl_Name.numberOfLines = 0;
        
        UIButton *btn_msg = [[UIButton alloc] init];
        if (IS_IPHONE_6Plus) {
            
            btn_msg.frame=CGRectMake(350, CGRectGetMaxY(line1.frame)+25, 30, 25);
            
        }else if (IS_IPHONE_6)
            
        {
            btn_msg.frame=CGRectMake(315, CGRectGetMaxY(line1.frame)+25, 25, 20);
            
        }
        else{
            btn_msg.frame=CGRectMake(260, CGRectGetMaxY(line1.frame)+20, 25, 20);
        }
        
        
        btn_msg.backgroundColor = [UIColor clearColor];
        [btn_msg setTitle:@"Next" forState:UIControlStateNormal];
        [btn_msg setImage:[UIImage imageNamed:@"msg_icon.png"] forState:UIControlStateNormal];
        [btn_msg setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //    [btn_msg addTarget:self action:@selector(Next_btnClick) forControlEvents:UIControlEventTouchUpInside ];
        btn_msg.layer.cornerRadius=4.0f;
        
        [img_cellBackGnd addSubview:btn_msg];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView == table_on_drop_down)
    {
        if (indexPath.row == 0)
        {
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
            {
                SLComposeViewController *facebook = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                
                SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result)
                {
                    if (result == SLComposeViewControllerResultDone)
                    {
                    }
                    [facebook dismissViewControllerAnimated:YES completion:Nil];
                };
                facebook.completionHandler =myBlock;
                
                
                [facebook addImage:[UIImage imageNamed:@"57x57.png"]];
                [facebook setInitialText:@"Not86"];
                [self presentViewController:facebook animated:YES completion:nil];
                
            }
            else
            {
                UIAlertView*alert=   [[UIAlertView alloc] initWithTitle:@"Alert" message:@"You can't send a share right now, make sure your device has an internet connection and you have at least one facebook account setup" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                
                [alert show];
                
                //            [SVProgressHUD showErrorWithStatus:@"You can't send a share right now, make sure your device has an internet connection and you have at least one facebook account setup"];
                
            }
            
            
        }
        else if (indexPath.row == 1)
        {
            
            
            if ([ChefHomeViewController isAppInstalled])
            {
                
                UIImageView * imageView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 200,200)];
                [imageView setUserInteractionEnabled:TRUE];
                //        [imageView setImageWithURL:[NSURL URLWithString:[[ary_dishdetaillist objectAtIndex:0] valueForKey:@"DishImage"]]];
                
                imageView.image =  [UIImage imageNamed:@"57x57.png"];
                
                [self.view addSubview:imageView];
                imageView.hidden=YES;
                
                
                
                
                UIImage *image = imageView.image;
                
                NSString *description=  @"Not86";
                
                if ([MGInstagram isAppInstalled])
                {
                    //[self AFProductShare:str_productID];
                    [MGInstagram postImage:image withCaption:description inView:self.view];
                }
                else
                    [self.notInstalledAlert show];
                
                
                
                
            }
            else
            {
                [self.notInstalledAlert show];
                NSLog(@"app not installed");
                //[self InstagramPopUp];
                
                
                //[SVProgressHUD showErrorWithStatus:@"Your Device should have the Instagrm Application"];
            }
            
        }
        else if (indexPath.row ==2)
        {
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
            {
                // Create a compose view controller for the service type Twitter
                SLComposeViewController *mulitpartPost = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                
                SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result)
                {
                    if (result == SLComposeViewControllerResultDone)
                    {
                    }
                    [mulitpartPost dismissViewControllerAnimated:YES completion:Nil];
                };
                mulitpartPost.completionHandler =myBlock;
                
                
                
                
                // Set the text of the tweet
                [mulitpartPost setInitialText:@"Not86"];
                
                [mulitpartPost addImage:[UIImage imageNamed:@"57x57.png"]];
                
                
                
                
                // Display the tweet sheet to the user
                [self presentViewController:mulitpartPost animated:YES completion:nil];
                
            }
            else
            {
                
                UIAlertView*alert=   [[UIAlertView alloc] initWithTitle:@"Alert" message:@"You can't send a share right now, make sure your device has an internet connection and you have at least one Twitter account setup" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                
                [alert show];
                
                //        [SVProgressHUD showErrorWithStatus:@"You can't send a share right now, make sure your device has an internet connection and you have at least one Twitter account setup"];
                
                //        [self callAlertView:@"Sorry" message:@"You can't send a share right now, make sure your device has an internet connection and you have at least one Twitter account setup"];
            }
            
        }
        
        
        [table_on_drop_down setHidden:YES];
    }
    
}

#pragma mark Instagram

/////instagram///
-(void)getimageBtnClick
{
    
    UIImagePickerController* imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

+ (BOOL) isAppInstalled
{
    NSURL *appURL = [NSURL URLWithString:kInstagramAppURLString];
    return [[UIApplication sharedApplication] canOpenURL:appURL];
}


-(UIImage*)thumbnailFromView:(UIView*)_myView{
    return [self thumbnailFromView:_myView withSize:_myView.frame.size];
}

-(UIImage*)thumbnailFromView:(UIView*)_myView withSize:(CGSize)viewsize{
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0)) {
        // Retina display
        CGSize newSize = viewsize;
        newSize.height=newSize.height*2;
        newSize.width=newSize.width*2;
        viewsize=newSize;
    }
    
    UIGraphicsBeginImageContext(_myView.bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGContextSetShouldAntialias(context, YES);
    [_myView.layer renderInContext: context];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    CGSize size = _myView.frame.size;
    CGFloat scale = MAX(viewsize.width / size.width, viewsize.height / size.height);
    
    UIGraphicsBeginImageContext(viewsize);
    CGFloat width = size.width * scale;
    CGFloat height = size.height * scale;
    float dwidth = ((viewsize.width - width) / 2.0f);
    float dheight = ((viewsize.height - height) / 2.0f);
    CGRect rect = CGRectMake(dwidth, dheight, size.width * scale, size.height * scale);
    [image drawInRect:rect];
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newimg;
}

- (UIAlertView*) notInstalledAlert
{
    return [[UIAlertView alloc] initWithTitle:@"Instagram Not Installed!" message:@"Instagram must be installed on the device in order to post images" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return [textField resignFirstResponder];
    
}
#pragma mark -integrateJWSlider
-(void)integrateJWSlider
{
    JWSlideMenuController *slideMenu=[[JWSlideMenuController alloc] init];
    JWSlideMenuViewController *vc_Home=[[ChefHomeViewController alloc] init];
    [slideMenu addViewController:vc_Home  withTitle:@"Home" andImage:[UIImage imageNamed:@"img_home@2x.png"]];
    
    JWSlideMenuViewController *vc_Appointment=[MyScheduleVC new];
    [slideMenu addViewController:vc_Appointment withTitle:@"My\n Schedule" andImage:[UIImage imageNamed:@"img_schedule@2x.png"]];
    
    JWSlideMenuViewController *vc_MyVeh=[MyordersVC new];
    [slideMenu addViewController:vc_MyVeh withTitle:@"Orders" andImage:[UIImage imageNamed:@"img_order@2x.png"]];
    
    JWSlideMenuViewController *vc_Accessories=[MyMenuVC new];
    [slideMenu addViewController:vc_Accessories withTitle:@"My Menu" andImage:[UIImage imageNamed:@"img_Menusrceen@2x.png"]];
    
    JWSlideMenuViewController *vc_Notification=[AccountVC new];
    [slideMenu addViewController:vc_Notification withTitle:@"Account" andImage:[UIImage imageNamed:@"img_account@2x.png"]];
    
    JWSlideMenuViewController *vc_Promo=[MessagesVC new];
    [slideMenu addViewController:vc_Promo withTitle:@"Notification" andImage:[UIImage imageNamed:@"img_notification@2x.png"]];
    
    JWSlideMenuViewController *vc_Promo1=[MessagesVC new];
    [slideMenu addViewController:vc_Promo1 withTitle:@"Messages" andImage:[UIImage imageNamed:@"img_msg@2x.png"]];
    
    
    JWSlideMenuViewController *vc_Promo2=[SwitchtoUser new];
    [slideMenu addViewController:vc_Promo2 withTitle:@"Switch to\n User" andImage:[UIImage imageNamed:@"img_swtich@2x.png"]];
    
    [self presentViewController:slideMenu animated:NO completion:nil];
    
}
-(void)btn_HomeScreenClick:(id)sender
{
    NSLog(@"btn_HomeScreenClick");
    
    
    
    //[self showMenu];
    
    [self integrateJWSlider];
    
    
}

-(void)click_on_logo_btn:(UIButton *)sender
{
    NSLog(@"click_on_logo_btn");
    
    
    
    ChefSupportsVC *vc = [[ChefSupportsVC alloc]init];
    [self presentViewController:vc animated:NO completion:nil];
}
-(void)click_on_share_btn:(UIButton *)sender
{
    table_on_drop_down.hidden=YES;
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        table_on_drop_down.hidden=NO;
        
        
    }
    else
    {
        [sender setSelected:NO];
        table_on_drop_down.hidden=YES;
    }
    
}


-(void)click_orders
{
    ChefOrdersVC *vc = [[ChefOrdersVC alloc]init];
    vc.str_comefrom = @"HOME";
    [self presentViewController:vc animated:NO completion:nil];
    
}

#pragma mark - Btn Actions


-(void)click_cancelqualityalert
{
    //scrollview5.hidden = YES;
    
    line_Emptyimage.hidden = YES;;
    lbl_Quality.hidden = YES;;
    btn_Quality.hidden = YES;;
    btn_delete.hidden = YES;;
    line.hidden = YES;;
    lbl_ServingLow.hidden = YES;;
    Arrow_left.hidden = YES;;
    btn_Arrowleft.hidden = YES;;
    Arrow_right.hidden = YES;;
    btn_Arrowright.hidden = YES;
    
    if (IS_IPHONE_6Plus)
    {
        
        lbl_Todayorders.frame=CGRectMake(24,CGRectGetMaxY(img_Background.frame)+5, 250,35);
        lbl_Todayorders.font = [UIFont fontWithName:kFontBold size:14];
        //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
        
        
    }else if (IS_IPHONE_6)
        
    {
        lbl_Todayorders.frame= CGRectMake(13,CGRectGetMaxY(img_Background.frame)+5, 250,35);
        lbl_Todayorders.font = [UIFont fontWithName:kFontBold size:16];
    }
    else
    {
        lbl_Todayorders.frame= CGRectMake(8,CGRectGetMaxY(img_Background.frame)+5, 250,35);
        lbl_Todayorders.font = [UIFont fontWithName:kFontBold size:14];
        
    }
    if (IS_IPHONE_6Plus)
    {
        img_table.frame  = CGRectMake(0,CGRectGetMaxY(lbl_Todayorders.frame)+5,self.view.frame.size.width,380);
    }
    else if (IS_IPHONE_6)
    {
        img_table.frame  = CGRectMake(0,CGRectGetMaxY(lbl_Todayorders.frame)+5,self.view.frame.size.width-10,300);
    }
    else
    {
        img_table.frame  = CGRectMake(0,CGRectGetMaxY(lbl_Todayorders.frame)+5,self.view.frame.size.width-10,220);
    }
    
    
}


-(void)clickedonServeNow
{
    ChefServeNowVC *chefServeVC = [[ChefServeNowVC alloc]init];
    [self presentViewController:chefServeVC animated:NO completion:nil];
    //    [self.navigationController pushViewController:chefServeVC animated:NO];
}

-(void)btn_savelater_Method
{
    ChefServeLaterVC *chefServeLaterVC = [[ChefServeLaterVC alloc]init];
    [self presentViewController:chefServeLaterVC animated:NO completion:nil];
    //    [self.navigationController pushViewController:chefServeVC animated:NO];
}

-(void)click_MenuScreen{
    
    ViewMenuVC *menuScreenvc = [[ViewMenuVC alloc]init];
    menuScreenvc.str_comefrom= @"HOME";
    
    [self presentViewController:menuScreenvc animated:NO completion:nil];
    
    //    [self.navigationController pushViewController:menuScreenvc animated:NO];
    
    
}
-(void)click_ScheduleScreen
{
    MyScheduleVC *scheduleScreenvc = [[MyScheduleVC alloc]init];
    scheduleScreenvc.str_comefrom= @"HOME";
    
    [self presentViewController:scheduleScreenvc animated:NO completion:nil];
    
    
}


#pragma userDishLists-functionality

-(void)AFUserDishLists
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    
    NSDictionary *params =@{
                            
                            @"uid"                                      :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            @"sort_time"                                :  @"0",
                            @"order_for"                                :  @"0"
                            };
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:Khomescreenlist  parameters:params];
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self ResponseUserDishLists:JSON];
    }
     
     
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error){
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not 86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFUserDishLists];
                                         }
                                     }];
    [operation start];
    
}
-(void)ResponseUserDishLists:(NSDictionary * )TheDict
{
    NSLog(@"Login: %@",TheDict);
    
    [ary_dishList removeAllObjects];
    [ary_DishRestrictions removeAllObjects];
    [ary_Serving_Type removeAllObjects];
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        
        for (int i=0; i<[[TheDict valueForKey:@"ChefDetails"] count]; i++)
        {
            [ary_dishList addObject:[[TheDict valueForKey:@"ChefDetails"]  objectAtIndex:i]];
            
        }
        
        
        
        
        // [self popup_Alertview:[TheDict valueForKey:@"message"]];
        
        [img_table reloadData];
        
        
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        // [self  popup_Alertview:[TheDict valueForKey:@"message"]];
        
        [self popup_noorders];
        
    }
    
}


# pragma mark ScheduleListServices

-(void) AFQualityalert
{
    
    [self.view addSubview:delegate.activityIndicator];
    [delegate.activityIndicator startAnimating];
    
    //=================================================================BASE URL
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    //=================================================================USED PARAMETERS(ONLY TEXT)
    
    NSDictionary *params =@{
                            @"uid"                          :  [[[NSUserDefaults standardUserDefaults]valueForKey:@"UserInfo"] valueForKey:@"Userid"],
                            };
    
    
    
    //===========================================AFNETWORKING HEADER
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    //===============================SIMPLE REQUEST
    
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:KQualityalert
                                                      parameters:params];
    
    
    
    
    //====================================================RESPONSE
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        
    }];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        
        [delegate.activityIndicator stopAnimating];
        [self Responsequalityalert:JSON];
    }
     
     //==================================================ERROR
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         
                                         
                                         [delegate.activityIndicator stopAnimating];
                                         
                                         if([operation.response statusCode] == 406){
                                             
                                             //                                             [SVProgressHUD showErrorWithStatus:@"Server error"];
                                             return;
                                         }
                                         
                                         if([operation.response statusCode] == 403){
                                             NSLog(@"Upload Failed");
                                             return;
                                         }
                                         if ([[operation error] code] == -1009) {
                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Not86"
                                                                                          message:@"Please check your internet connection"
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                             [av show];
                                         }
                                         else if ([[operation error] code] == -1001) {
                                             
                                             NSLog(@"Successfully Registered");
                                             [self AFQualityalert];
                                         }
                                     }];
    [operation start];
    
}



-(void) Responsequalityalert :(NSDictionary * )TheDict
{
    [ary_qualityalert removeAllObjects];
    NSLog(@"Dict is %@", TheDict);
    
    if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"0"])
    {
        for (int i=0; i<[[[TheDict valueForKey:@"0"] valueForKey:@"ProductDetail"] count]; i++)
        {
            
            [ary_qualityalert addObject:[[[TheDict valueForKey:@"0"] valueForKey:@"ProductDetail"] objectAtIndex:i]];
            
        }
        
        [self scrollviewmethod];
        
        NSLog(@"Array of Schedule: %@",ary_qualityalert );
    }
    else if ([[NSString stringWithFormat:@"%@",[TheDict valueForKey:@"error"]] isEqualToString:@"1"])
    {
        
        NSLog(@"Not recive data check service");
        
    }
    
    //[table_schedule reloadData];
}


-(void)scrollviewmethod
{
    
    for (UIScrollView *scroll1 in scroll.subviews)
    {
        [scroll1 removeFromSuperview];
    }
    
    
    int totalPage;
    
    totalPage = (int)[ary_qualityalert count];
    
    
    
    
    for (int j = 0; j<totalPage; j++)
    {
        UIImageView *imgView_Photos = [[UIImageView alloc] init];
        imgView_Photos.frame = CGRectMake((WIDTH-35)*j,0, WIDTH-35, 65);
        [imgView_Photos setBackgroundColor:[UIColor clearColor]];
        [imgView_Photos setContentMode:UIViewContentModeScaleAspectFill];
        [imgView_Photos setClipsToBounds:YES];
        imgView_Photos.userInteractionEnabled = YES;
        [scroll addSubview:imgView_Photos];
        
        
        UIImageView *img_Desh=[[UIImageView alloc]init];
        img_Desh.frame=CGRectMake(0, 0, 55, 65);
        [img_Desh setUserInteractionEnabled:YES];
        img_Desh.backgroundColor=[UIColor clearColor];
        NSString *url_Img = [NSString stringWithFormat: @"%@", [[ary_qualityalert objectAtIndex:j] valueForKey:@"ProductImage"]];
        img_Desh.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img]]];
        [img_Desh setContentMode:UIViewContentModeScaleAspectFill];
        img_Desh.clipsToBounds = YES;
        [imgView_Photos addSubview:img_Desh];
        
        
        
        
        UILabel  * spicy_Fish = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus) {
            
            spicy_Fish.frame=CGRectMake(CGRectGetMaxX(img_Desh.frame)+7,3, 110,20);
            spicy_Fish.font = [UIFont fontWithName:kFontBold size:14];
            //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
            
            
        }else if (IS_IPHONE_6)
        {
            spicy_Fish.frame= CGRectMake(CGRectGetMaxX(img_Desh.frame)+8,0, 140,20);
            spicy_Fish.font = [UIFont fontWithName:kFontBold size:14];
        }
        else{
            spicy_Fish.frame= CGRectMake(CGRectGetMaxX(img_Desh.frame)+3,0, 100,16);
            spicy_Fish.font = [UIFont fontWithName:kFontBold size:12];
            
        }
        
        
        spicy_Fish.text = [NSString stringWithFormat: @"%@", [[ary_qualityalert objectAtIndex:j] valueForKey:@"ProductName"]];
        spicy_Fish.backgroundColor=[UIColor clearColor];
        spicy_Fish.textColor=[UIColor blackColor];
        spicy_Fish.textAlignment=NSTextAlignmentLeft;
        [imgView_Photos addSubview:spicy_Fish];
        
        
        
        UILabel  * lbl_runninglow = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus) {
            
            lbl_runninglow.frame=CGRectMake(CGRectGetMaxX(spicy_Fish.frame)-5,3, WIDTH-40,20);
            lbl_runninglow.font = [UIFont fontWithName:kFont size:14];
            //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
            
            
        }else if (IS_IPHONE_6)
            
        {
            lbl_runninglow.frame= CGRectMake(CGRectGetMaxX(spicy_Fish.frame),0, 100,20);
            lbl_runninglow.font = [UIFont fontWithName:kFont size:14];
        }
        else{
            lbl_runninglow.frame= CGRectMake(CGRectGetMaxX(spicy_Fish.frame)+2,0, 80,16);
            lbl_runninglow.font = [UIFont fontWithName:kFont size:11];
            
        }
        
        lbl_runninglow.text = @" is running low";
        lbl_runninglow.backgroundColor=[UIColor clearColor];
        lbl_runninglow.textColor=[UIColor blackColor];
        lbl_runninglow.textAlignment=NSTextAlignmentLeft;
        
        [imgView_Photos addSubview:lbl_runninglow];
        
        UILabel  * lbl_foritem = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus) {
            
            lbl_foritem.frame=CGRectMake(CGRectGetMaxX(img_Desh.frame)+7,CGRectGetMaxY(spicy_Fish.frame)+2, 30,15);
            lbl_foritem.font = [UIFont fontWithName:kFont size:14];
            //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
            
            
        }else if (IS_IPHONE_6)
            
        {
            lbl_foritem.frame= CGRectMake(CGRectGetMaxX(img_Desh.frame)+8,CGRectGetMaxY(spicy_Fish.frame)+2,30,15);
            lbl_foritem.font = [UIFont fontWithName:kFont size:12];
        }
        else{
            lbl_foritem.frame= CGRectMake(CGRectGetMaxX(img_Desh.frame)+5,CGRectGetMaxY(spicy_Fish.frame)+2,16,16);
            lbl_foritem.font = [UIFont fontWithName:kFont size:11];
            
        }
        
        lbl_foritem.text = @"for ";
        lbl_foritem.backgroundColor=[UIColor clearColor];
        lbl_foritem.textColor=[UIColor blackColor];
        lbl_foritem.textAlignment=NSTextAlignmentLeft;
        //    spicy_Fish.font = [UIFont fontWithName:kFont size:13];
        
        [imgView_Photos addSubview:lbl_foritem];
        
        
        UILabel  * lbl_Delivery = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus) {
            
            lbl_Delivery.frame=CGRectMake(CGRectGetMaxX(lbl_foritem.frame)-5,CGRectGetMaxY(lbl_runninglow.frame)+2, 60,15);
            lbl_Delivery.font = [UIFont fontWithName:kFontBold size:12];
            //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
            
            
        }else if (IS_IPHONE_6)
            
        {
            lbl_Delivery.frame= CGRectMake(CGRectGetMaxX(lbl_foritem.frame)-8,CGRectGetMaxY(lbl_runninglow.frame)+2,60,15);
            lbl_Delivery.font = [UIFont fontWithName:kFontBold size:12];
        }
        else{
            lbl_Delivery.frame= CGRectMake(CGRectGetMaxX(lbl_foritem.frame)+2,CGRectGetMaxY(lbl_runninglow.frame)+2,60,16);
            lbl_Delivery.font = [UIFont fontWithName:kFontBold size:11];
            
        }
        
        lbl_Delivery.text = @"Delivery.";
        lbl_Delivery.backgroundColor=[UIColor clearColor];
        lbl_Delivery.textColor=[UIColor colorWithRed:8/255.0f green:0/255.0f blue:48/255.0f alpha:1];
        lbl_Delivery.textAlignment=NSTextAlignmentLeft;
        //    spicy_Fish.font = [UIFont fontWithName:kFont size:13];
        
        [imgView_Photos addSubview:lbl_Delivery];
        
        
        
        
        
        
        UILabel  * lbl_like = [[UILabel alloc]init];
        if (IS_IPHONE_6Plus) {
            
            lbl_like.frame=CGRectMake(CGRectGetMaxX(img_Desh.frame)+5,CGRectGetMaxY(lbl_Delivery.frame)+2, 200,20);
            lbl_like.font = [UIFont fontWithName:kFont size:14];
            //        btn_Quality.font = [UIFont fontWithName:kFont size:20];
            
            
        }else if (IS_IPHONE_6)
            
        {
            lbl_like.frame= CGRectMake(CGRectGetMaxX(img_Desh.frame)+5,CGRectGetMaxY(lbl_Delivery.frame)+2, 200,25);
            lbl_like.font = [UIFont fontWithName:kFont size:14];
        }
        else{
            lbl_like.frame= CGRectMake(CGRectGetMaxX(img_Desh.frame)+5,CGRectGetMaxY(lbl_Delivery.frame)+2, 200,16);
            lbl_like.font = [UIFont fontWithName:kFont size:11];
            
        }
        
        lbl_like.text = @"Would you like to top up?";
        lbl_like.backgroundColor=[UIColor clearColor];
        lbl_like.textColor=[UIColor blackColor];
        lbl_like.textAlignment=NSTextAlignmentLeft;
        //    lbl_like.font = [UIFont fontWithName:@"Arial" size:13];
        [imgView_Photos addSubview:lbl_like];
        //lbl_like.numberOfLines = 0;
    }
    [scroll setContentSize:CGSizeMake((WIDTH-70)*totalPage,scroll.frame.size.height)];
}


-(void)popup_Alertview:(NSString *)message
{
    [alertviewBg removeFromSuperview];
    alertviewBg=[[UIView alloc] init];
    alertviewBg.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.3];
    alertviewBg.userInteractionEnabled=TRUE;
    [self.view addSubview:alertviewBg];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [alertviewBg addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitle.text=@"NOT86";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    UILabel *lab_alertViewbody=[[UILabel alloc] init];
    lab_alertViewbody.textColor=[UIColor blackColor];
    lab_alertViewbody.text=message;
    lab_alertViewbody.numberOfLines=0;
    lab_alertViewbody.userInteractionEnabled=YES;
    lab_alertViewbody.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewbody];
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"OK" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewOk:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    alertviewBg.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,140);
    lab_alertViewTitle.frame = CGRectMake(0,20,WIDTH-100,21);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:22.0f];
    lab_alertViewbody.frame = CGRectMake(5,45,WIDTH-110,50);
    lab_alertViewbody.font = [UIFont fontWithName:kFont size:13.0f];
    imageview_div.frame= CGRectMake(0,100,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,100,WIDTH-100,40);
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
}

-(void)click_btnAlertviewOk:(UIButton *)sender
{
    [alertviewBg removeFromSuperview];
}

-(void)popup_noorders
{
    [view_popnoorderss removeFromSuperview];
    view_popnoorderss=[[UIView alloc] init];
    view_popnoorderss.backgroundColor=[UIColor colorWithRed:(0.0/225.0) green:(0.0/225.0) blue:(0.0/225.0) alpha:0.5];
    view_popnoorderss.userInteractionEnabled=TRUE;
    [self.view addSubview:view_popnoorderss];
    
    
    UIImageView *alertViewBody =[[UIImageView alloc] init];
    //    alertViewBody.layer.cornerRadius =10.0;
    alertViewBody.userInteractionEnabled=YES;
    alertViewBody.backgroundColor=[UIColor whiteColor];
    [view_popnoorderss addSubview:alertViewBody];
    
    UILabel *lab_alertViewTitlelbl=[[UILabel alloc] init];
    lab_alertViewTitlelbl.textColor=[UIColor colorWithRed:60/255.0f green:182/255.0f blue:229/255.0f alpha:1];
    lab_alertViewTitlelbl.text=@"NOT86";
    lab_alertViewTitlelbl.userInteractionEnabled=YES;
    lab_alertViewTitlelbl.font = [UIFont fontWithName:kFont size:20];
    lab_alertViewTitlelbl.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitlelbl];
    
    
    //    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"Would you like to edit your Menu(chef will tap on Menu to go to Menu screen) or your Schedule(chef will tap on Schedule to go to Schedule screen) to attract orders? Or would you like to tap on the Share icon next to the not86 logo to share your profile with your friends or go to one of your items that you are serving now and share it?"];
    //    [str addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(25,4)];
    //    [str addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(60,8)];
    
    UILabel *lab_alertViewTitle=[[UILabel alloc] init];
    lab_alertViewTitle.textColor=[UIColor blackColor];
    //lab_alertViewTitle.text=[NSMutableString stringWithFormat:@"%@",str];
    lab_alertViewTitle.text = @"Would you like to edit your Menu(chef will tap on Menu to go to Menu screen) or your Schedule(chef will tap on Schedule to go to Schedule screen) to attract orders? Or would you like to tap on the Share icon next to the not86 logo to share your profile with your friends or go to one of your items that you are serving now and share it?";
    lab_alertViewTitle.userInteractionEnabled=YES;
    lab_alertViewTitle.numberOfLines = 8;
    lab_alertViewTitle.textAlignment=NSTextAlignmentCenter;
    [alertViewBody addSubview:lab_alertViewTitle];
    
    
    
    
    
    
    
    
    UIImageView *imageview_div=[[UIImageView alloc] init];
    imageview_div.backgroundColor=[UIColor lightGrayColor];
    [alertViewBody addSubview:imageview_div];
    
    
    UIButton *btn_alertviewOk=[[UIButton alloc] init];
    btn_alertviewOk = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewOk.backgroundColor=[UIColor clearColor];
    [btn_alertviewOk setTitle:@"Menu" forState:UIControlStateNormal];
    [btn_alertviewOk setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewOk addTarget:self action:@selector(click_btnAlertviewmenu:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewOk];
    
    UIButton *btn_alertviewsh=[[UIButton alloc] init];
    btn_alertviewsh = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertviewsh.backgroundColor=[UIColor clearColor];
    [btn_alertviewsh setTitle:@"Schedule" forState:UIControlStateNormal];
    [btn_alertviewsh setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertviewsh addTarget:self action:@selector(click_btnAlertviewschedulw:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertviewsh];
    
    UIButton *btn_alertvieworders=[[UIButton alloc] init];
    btn_alertvieworders = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_alertvieworders.backgroundColor=[UIColor clearColor];
    [btn_alertvieworders setTitle:@"Orders" forState:UIControlStateNormal];
    [btn_alertvieworders setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_alertvieworders addTarget:self action:@selector(click_btnAlertvieworders:) forControlEvents:UIControlEventTouchUpInside];
    [alertViewBody addSubview:btn_alertvieworders];
    
    view_popnoorderss.frame = CGRectMake(0,0,WIDTH, HEIGHT);
    alertViewBody.frame = CGRectMake((WIDTH/2)-((WIDTH/2)-50),HEIGHT/2-100,WIDTH-100,250);
    lab_alertViewTitlelbl.frame = CGRectMake(10,10,WIDTH-120,30);
    lab_alertViewTitle.frame = CGRectMake(10,40,WIDTH-120,150);
    lab_alertViewTitle.font = [UIFont fontWithName:kFont size:11.0f];
    imageview_div.frame= CGRectMake(0,210,WIDTH-100,1);
    btn_alertviewOk.frame = CGRectMake(0,210,alertViewBody.frame.size.width/3,40);
    btn_alertviewsh.frame = CGRectMake(CGRectGetMaxX(btn_alertviewOk.frame),210,alertViewBody.frame.size.width/3,40);
    btn_alertvieworders.frame = CGRectMake(CGRectGetMaxX(btn_alertviewsh.frame),210,alertViewBody.frame.size.width/3,40);
    
    [btn_alertviewOk.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
    [btn_alertviewsh.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
    [btn_alertvieworders.titleLabel setFont:[UIFont fontWithName:kFont size:16.0f]];
    
}


-(void)click_btnAlertviewmenu:(UIButton *)sender
{
    [view_popnoorderss removeFromSuperview];
    
    ViewMenuVC *menuScreenvc = [[ViewMenuVC alloc]init];
    menuScreenvc.str_comefrom = @"HOME";
    [self presentViewController:menuScreenvc animated:NO completion:nil];
    
}
-(void)click_btnAlertviewschedulw:(UIButton *)sender
{
    [view_popnoorderss removeFromSuperview];
    
    MyScheduleVC *scheduleScreenvc = [[MyScheduleVC alloc]init];
    scheduleScreenvc.str_comefrom = @"HOME";
    [self presentViewController:scheduleScreenvc animated:NO completion:nil];
    
}
-(void)click_btnAlertvieworders:(UIButton *)sender
{
    [view_popnoorderss removeFromSuperview];
    
    ChefOrdersVC *vc = [[ChefOrdersVC alloc]init];
    vc.str_comefrom = @"HOME";
    
    [self presentViewController:vc animated:NO completion:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
